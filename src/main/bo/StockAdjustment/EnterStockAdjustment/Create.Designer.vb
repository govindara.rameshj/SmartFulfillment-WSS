<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Create
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label7 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Create))
        Me.xgdAdjusts = New DevExpress.XtraGrid.GridControl()
        Me.xgvAdjusts = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grpCode = New System.Windows.Forms.GroupBox()
        Me.pnlCode = New System.Windows.Forms.Panel()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblMarkWO = New System.Windows.Forms.Label()
        Me.lblSecurity = New System.Windows.Forms.Label()
        Me.lblAction = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lueCode = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtSku = New DevExpress.XtraEditors.TextEdit()
        Me.grpSku = New System.Windows.Forms.GroupBox()
        Me.txtSkuDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.txtDrlNumber = New DevExpress.XtraEditors.TextEdit()
        Me.pnlTransfer = New System.Windows.Forms.Panel()
        Me.txtTransEnd = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransPrice = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransAdj = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransEndValue = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransStart = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransAdjValue = New DevExpress.XtraEditors.TextEdit()
        Me.txtTransStartValue = New DevExpress.XtraEditors.TextEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbTrans = New System.Windows.Forms.ComboBox()
        Me.lblDrlNumber = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pnlSku = New System.Windows.Forms.Panel()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.pnlMwo = New System.Windows.Forms.Panel()
        Me.txtMwoEnd = New DevExpress.XtraEditors.TextEdit()
        Me.txtMwoAdj = New DevExpress.XtraEditors.TextEdit()
        Me.txtMwoStart = New DevExpress.XtraEditors.TextEdit()
        Me.lblMwo = New System.Windows.Forms.Label()
        Me.txtComment = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuPrice = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuEndValue = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuAdjustValue = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuStartValue = New DevExpress.XtraEditors.TextEdit()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.txtSkuEnd = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuAdjust = New DevExpress.XtraEditors.TextEdit()
        Me.txtSkuStart = New DevExpress.XtraEditors.TextEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label9 = New System.Windows.Forms.Label()
        Me.lblSku = New System.Windows.Forms.Label()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReset = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCreate = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMaintain = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReverse = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStockEnquiry = New DevExpress.XtraEditors.SimpleButton()
        Label7 = New System.Windows.Forms.Label()
        CType(Me.xgdAdjusts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgvAdjusts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCode.SuspendLayout()
        Me.pnlCode.SuspendLayout()
        CType(Me.lueCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSku.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSku.SuspendLayout()
        CType(Me.txtSkuDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDrlNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTransfer.SuspendLayout()
        CType(Me.txtTransEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransAdj.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransEndValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransAdjValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTransStartValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSku.SuspendLayout()
        Me.pnlMwo.SuspendLayout()
        CType(Me.txtMwoEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMwoAdj.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMwoStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuEndValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuAdjustValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuStartValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuAdjust.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSkuStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label7
        '
        Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Label7.Location = New System.Drawing.Point(9, 66)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(81, 20)
        Label7.TabIndex = 12
        Label7.Text = "Description"
        Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'xgdAdjusts
        '
        Me.xgdAdjusts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgdAdjusts.Location = New System.Drawing.Point(3, 276)
        Me.xgdAdjusts.MainView = Me.xgvAdjusts
        Me.xgdAdjusts.Name = "xgdAdjusts"
        Me.xgdAdjusts.Size = New System.Drawing.Size(944, 226)
        Me.xgdAdjusts.TabIndex = 2
        Me.xgdAdjusts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xgvAdjusts})
        Me.xgdAdjusts.Visible = False
        '
        'xgvAdjusts
        '
        Me.xgvAdjusts.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xgvAdjusts.Appearance.HeaderPanel.Options.UseFont = True
        Me.xgvAdjusts.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xgvAdjusts.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xgvAdjusts.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xgvAdjusts.GridControl = Me.xgdAdjusts
        Me.xgvAdjusts.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
        Me.xgvAdjusts.Name = "xgvAdjusts"
        Me.xgvAdjusts.OptionsBehavior.Editable = False
        Me.xgvAdjusts.OptionsDetail.ShowDetailTabs = False
        '
        'grpCode
        '
        Me.grpCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpCode.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpCode.Controls.Add(Me.pnlCode)
        Me.grpCode.Controls.Add(Me.lueCode)
        Me.grpCode.Location = New System.Drawing.Point(3, 3)
        Me.grpCode.Name = "grpCode"
        Me.grpCode.Size = New System.Drawing.Size(294, 267)
        Me.grpCode.TabIndex = 0
        Me.grpCode.TabStop = False
        Me.grpCode.Text = "Select Code"
        '
        'pnlCode
        '
        Me.pnlCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlCode.Controls.Add(Me.lblCode)
        Me.pnlCode.Controls.Add(Me.Label1)
        Me.pnlCode.Controls.Add(Me.lblMarkWO)
        Me.pnlCode.Controls.Add(Me.lblSecurity)
        Me.pnlCode.Controls.Add(Me.lblAction)
        Me.pnlCode.Controls.Add(Me.Label14)
        Me.pnlCode.Controls.Add(Me.Label13)
        Me.pnlCode.Location = New System.Drawing.Point(6, 45)
        Me.pnlCode.Name = "pnlCode"
        Me.pnlCode.Size = New System.Drawing.Size(282, 214)
        Me.pnlCode.TabIndex = 2
        Me.pnlCode.Visible = False
        '
        'lblCode
        '
        Me.lblCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCode.Location = New System.Drawing.Point(100, 0)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(179, 20)
        Me.lblCode.TabIndex = 1
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Code"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMarkWO
        '
        Me.lblMarkWO.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMarkWO.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMarkWO.Location = New System.Drawing.Point(132, 42)
        Me.lblMarkWO.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.lblMarkWO.Name = "lblMarkWO"
        Me.lblMarkWO.Size = New System.Drawing.Size(147, 20)
        Me.lblMarkWO.TabIndex = 6
        Me.lblMarkWO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSecurity
        '
        Me.lblSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSecurity.Location = New System.Drawing.Point(100, 42)
        Me.lblSecurity.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.lblSecurity.Name = "lblSecurity"
        Me.lblSecurity.Size = New System.Drawing.Size(26, 20)
        Me.lblSecurity.TabIndex = 5
        Me.lblSecurity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAction
        '
        Me.lblAction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAction.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblAction.Location = New System.Drawing.Point(100, 21)
        Me.lblAction.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(179, 20)
        Me.lblAction.TabIndex = 3
        Me.lblAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(0, 42)
        Me.Label14.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(94, 20)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Security Level"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(0, 21)
        Me.Label13.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(94, 20)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Action"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lueCode
        '
        Me.lueCode.AllowDrop = True
        Me.lueCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lueCode.EnterMoveNextControl = True
        Me.lueCode.Location = New System.Drawing.Point(9, 19)
        Me.lueCode.Name = "lueCode"
        Me.lueCode.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lueCode.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.lueCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueCode.Properties.ImmediatePopup = True
        Me.lueCode.Properties.NullText = ""
        Me.lueCode.Properties.PopupWidth = 600
        Me.lueCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.lueCode.Size = New System.Drawing.Size(279, 20)
        Me.lueCode.TabIndex = 0
        '
        'txtSku
        '
        Me.txtSku.EnterMoveNextControl = True
        Me.txtSku.Location = New System.Drawing.Point(96, 40)
        Me.txtSku.Name = "txtSku"
        Me.txtSku.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSku.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSku.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSku.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSku.Properties.Mask.EditMask = "\d{1,6}"
        Me.txtSku.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtSku.Properties.Mask.ShowPlaceHolders = False
        Me.txtSku.Properties.MaxLength = 6
        Me.txtSku.Size = New System.Drawing.Size(94, 20)
        Me.txtSku.TabIndex = 2
        '
        'grpSku
        '
        Me.grpSku.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSku.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpSku.Controls.Add(Label7)
        Me.grpSku.Controls.Add(Me.txtSkuDescription)
        Me.grpSku.Controls.Add(Me.txtDrlNumber)
        Me.grpSku.Controls.Add(Me.pnlTransfer)
        Me.grpSku.Controls.Add(Me.lblDrlNumber)
        Me.grpSku.Controls.Add(Me.txtSku)
        Me.grpSku.Controls.Add(Me.Label3)
        Me.grpSku.Controls.Add(Me.pnlSku)
        Me.grpSku.Controls.Add(Me.lblSku)
        Me.grpSku.Location = New System.Drawing.Point(306, 3)
        Me.grpSku.Name = "grpSku"
        Me.grpSku.Size = New System.Drawing.Size(644, 267)
        Me.grpSku.TabIndex = 1
        Me.grpSku.TabStop = False
        Me.grpSku.Text = "Select Item"
        Me.grpSku.Visible = False
        '
        'txtSkuDescription
        '
        Me.txtSkuDescription.Location = New System.Drawing.Point(96, 66)
        Me.txtSkuDescription.Name = "txtSkuDescription"
        Me.txtSkuDescription.Properties.ReadOnly = True
        Me.txtSkuDescription.Size = New System.Drawing.Size(180, 38)
        Me.txtSkuDescription.TabIndex = 11
        Me.txtSkuDescription.TabStop = False
        '
        'txtDrlNumber
        '
        Me.txtDrlNumber.EnterMoveNextControl = True
        Me.txtDrlNumber.Location = New System.Drawing.Point(282, 40)
        Me.txtDrlNumber.Name = "txtDrlNumber"
        Me.txtDrlNumber.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDrlNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDrlNumber.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtDrlNumber.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtDrlNumber.Properties.Mask.EditMask = "\d{0,6}"
        Me.txtDrlNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtDrlNumber.Properties.Mask.ShowPlaceHolders = False
        Me.txtDrlNumber.Properties.MaxLength = 6
        Me.txtDrlNumber.Size = New System.Drawing.Size(80, 20)
        Me.txtDrlNumber.TabIndex = 3
        '
        'pnlTransfer
        '
        Me.pnlTransfer.Controls.Add(Me.txtTransEnd)
        Me.pnlTransfer.Controls.Add(Me.txtTransPrice)
        Me.pnlTransfer.Controls.Add(Me.txtTransAdj)
        Me.pnlTransfer.Controls.Add(Me.txtTransEndValue)
        Me.pnlTransfer.Controls.Add(Me.txtTransStart)
        Me.pnlTransfer.Controls.Add(Me.txtTransAdjValue)
        Me.pnlTransfer.Controls.Add(Me.txtTransStartValue)
        Me.pnlTransfer.Controls.Add(Me.Label6)
        Me.pnlTransfer.Controls.Add(Me.Label2)
        Me.pnlTransfer.Controls.Add(Me.cmbTrans)
        Me.pnlTransfer.Location = New System.Drawing.Point(372, 17)
        Me.pnlTransfer.Name = "pnlTransfer"
        Me.pnlTransfer.Size = New System.Drawing.Size(188, 218)
        Me.pnlTransfer.TabIndex = 3
        Me.pnlTransfer.Visible = False
        '
        'txtTransEnd
        '
        Me.txtTransEnd.EnterMoveNextControl = True
        Me.txtTransEnd.Location = New System.Drawing.Point(0, 172)
        Me.txtTransEnd.Name = "txtTransEnd"
        Me.txtTransEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransEnd.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransEnd.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransEnd.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtTransEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtTransEnd.Properties.Mask.ShowPlaceHolders = False
        Me.txtTransEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTransEnd.Size = New System.Drawing.Size(94, 20)
        Me.txtTransEnd.TabIndex = 8
        '
        'txtTransPrice
        '
        Me.txtTransPrice.Enabled = False
        Me.txtTransPrice.EnterMoveNextControl = True
        Me.txtTransPrice.Location = New System.Drawing.Point(0, 94)
        Me.txtTransPrice.Name = "txtTransPrice"
        Me.txtTransPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransPrice.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransPrice.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransPrice.Properties.DisplayFormat.FormatString = "c2"
        Me.txtTransPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTransPrice.Size = New System.Drawing.Size(94, 20)
        Me.txtTransPrice.TabIndex = 2
        Me.txtTransPrice.TabStop = False
        '
        'txtTransAdj
        '
        Me.txtTransAdj.EnterMoveNextControl = True
        Me.txtTransAdj.Location = New System.Drawing.Point(0, 146)
        Me.txtTransAdj.Name = "txtTransAdj"
        Me.txtTransAdj.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransAdj.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransAdj.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransAdj.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransAdj.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtTransAdj.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtTransAdj.Properties.Mask.ShowPlaceHolders = False
        Me.txtTransAdj.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTransAdj.Size = New System.Drawing.Size(94, 20)
        Me.txtTransAdj.TabIndex = 6
        '
        'txtTransEndValue
        '
        Me.txtTransEndValue.Enabled = False
        Me.txtTransEndValue.EnterMoveNextControl = True
        Me.txtTransEndValue.Location = New System.Drawing.Point(100, 172)
        Me.txtTransEndValue.Name = "txtTransEndValue"
        Me.txtTransEndValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransEndValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransEndValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransEndValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransEndValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtTransEndValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTransEndValue.Size = New System.Drawing.Size(80, 20)
        Me.txtTransEndValue.TabIndex = 9
        Me.txtTransEndValue.TabStop = False
        '
        'txtTransStart
        '
        Me.txtTransStart.EnterMoveNextControl = True
        Me.txtTransStart.Location = New System.Drawing.Point(0, 120)
        Me.txtTransStart.Name = "txtTransStart"
        Me.txtTransStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransStart.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransStart.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransStart.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtTransStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtTransStart.Properties.Mask.ShowPlaceHolders = False
        Me.txtTransStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTransStart.Size = New System.Drawing.Size(94, 20)
        Me.txtTransStart.TabIndex = 4
        '
        'txtTransAdjValue
        '
        Me.txtTransAdjValue.Enabled = False
        Me.txtTransAdjValue.EnterMoveNextControl = True
        Me.txtTransAdjValue.Location = New System.Drawing.Point(100, 147)
        Me.txtTransAdjValue.Name = "txtTransAdjValue"
        Me.txtTransAdjValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransAdjValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransAdjValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransAdjValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransAdjValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtTransAdjValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTransAdjValue.Size = New System.Drawing.Size(80, 20)
        Me.txtTransAdjValue.TabIndex = 7
        Me.txtTransAdjValue.TabStop = False
        '
        'txtTransStartValue
        '
        Me.txtTransStartValue.Enabled = False
        Me.txtTransStartValue.EnterMoveNextControl = True
        Me.txtTransStartValue.Location = New System.Drawing.Point(100, 120)
        Me.txtTransStartValue.Name = "txtTransStartValue"
        Me.txtTransStartValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTransStartValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTransStartValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtTransStartValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtTransStartValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtTransStartValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTransStartValue.Size = New System.Drawing.Size(80, 20)
        Me.txtTransStartValue.TabIndex = 5
        Me.txtTransStartValue.TabStop = False
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(100, 94)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Value"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Select Transfer SKU"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbTrans
        '
        Me.cmbTrans.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbTrans.BackColor = System.Drawing.SystemColors.Window
        Me.cmbTrans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTrans.FormattingEnabled = True
        Me.cmbTrans.Location = New System.Drawing.Point(0, 23)
        Me.cmbTrans.MaxDropDownItems = 20
        Me.cmbTrans.Name = "cmbTrans"
        Me.cmbTrans.Size = New System.Drawing.Size(184, 21)
        Me.cmbTrans.TabIndex = 1
        '
        'lblDrlNumber
        '
        Me.lblDrlNumber.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblDrlNumber.Location = New System.Drawing.Point(195, 40)
        Me.lblDrlNumber.Name = "lblDrlNumber"
        Me.lblDrlNumber.Size = New System.Drawing.Size(81, 20)
        Me.lblDrlNumber.TabIndex = 10
        Me.lblDrlNumber.Text = "&DRL Number"
        Me.lblDrlNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(9, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "SKU &Number"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSku
        '
        Me.pnlSku.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSku.Controls.Add(Me.btnCancel)
        Me.pnlSku.Controls.Add(Me.btnSave)
        Me.pnlSku.Controls.Add(Me.pnlMwo)
        Me.pnlSku.Controls.Add(Me.txtComment)
        Me.pnlSku.Controls.Add(Me.txtSkuPrice)
        Me.pnlSku.Controls.Add(Me.txtSkuEndValue)
        Me.pnlSku.Controls.Add(Me.txtSkuAdjustValue)
        Me.pnlSku.Controls.Add(Me.txtSkuStartValue)
        Me.pnlSku.Controls.Add(Me.lblComment)
        Me.pnlSku.Controls.Add(Me.txtSkuEnd)
        Me.pnlSku.Controls.Add(Me.txtSkuAdjust)
        Me.pnlSku.Controls.Add(Me.txtSkuStart)
        Me.pnlSku.Controls.Add(Me.Label5)
        Me.pnlSku.Controls.Add(Me.Label4)
        Me.pnlSku.Controls.Add(Me.label11)
        Me.pnlSku.Controls.Add(Me.label10)
        Me.pnlSku.Controls.Add(Me.label9)
        Me.pnlSku.Location = New System.Drawing.Point(9, 111)
        Me.pnlSku.Name = "pnlSku"
        Me.pnlSku.Size = New System.Drawing.Size(629, 148)
        Me.pnlSku.TabIndex = 3
        Me.pnlSku.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Options.UseTextOptions = True
        Me.btnCancel.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.Location = New System.Drawing.Point(554, 110)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 39)
        Me.btnCancel.TabIndex = 17
        Me.btnCancel.Text = "F10 Cancel"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Appearance.Options.UseTextOptions = True
        Me.btnSave.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnSave.Location = New System.Drawing.Point(553, 65)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 39)
        Me.btnSave.TabIndex = 16
        Me.btnSave.Text = "F5 Save Adjustment"
        '
        'pnlMwo
        '
        Me.pnlMwo.BackColor = System.Drawing.Color.Transparent
        Me.pnlMwo.Controls.Add(Me.txtMwoEnd)
        Me.pnlMwo.Controls.Add(Me.txtMwoAdj)
        Me.pnlMwo.Controls.Add(Me.txtMwoStart)
        Me.pnlMwo.Controls.Add(Me.lblMwo)
        Me.pnlMwo.Location = New System.Drawing.Point(273, 0)
        Me.pnlMwo.Name = "pnlMwo"
        Me.pnlMwo.Size = New System.Drawing.Size(92, 108)
        Me.pnlMwo.TabIndex = 55
        Me.pnlMwo.Visible = False
        '
        'txtMwoEnd
        '
        Me.txtMwoEnd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMwoEnd.Enabled = False
        Me.txtMwoEnd.EnterMoveNextControl = True
        Me.txtMwoEnd.Location = New System.Drawing.Point(0, 78)
        Me.txtMwoEnd.Name = "txtMwoEnd"
        Me.txtMwoEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMwoEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMwoEnd.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMwoEnd.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtMwoEnd.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtMwoEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtMwoEnd.Properties.Mask.ShowPlaceHolders = False
        Me.txtMwoEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMwoEnd.Size = New System.Drawing.Size(92, 20)
        Me.txtMwoEnd.TabIndex = 3
        Me.txtMwoEnd.TabStop = False
        '
        'txtMwoAdj
        '
        Me.txtMwoAdj.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMwoAdj.Enabled = False
        Me.txtMwoAdj.EnterMoveNextControl = True
        Me.txtMwoAdj.Location = New System.Drawing.Point(0, 52)
        Me.txtMwoAdj.Name = "txtMwoAdj"
        Me.txtMwoAdj.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMwoAdj.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMwoAdj.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMwoAdj.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtMwoAdj.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtMwoAdj.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtMwoAdj.Properties.Mask.ShowPlaceHolders = False
        Me.txtMwoAdj.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMwoAdj.Size = New System.Drawing.Size(92, 20)
        Me.txtMwoAdj.TabIndex = 2
        Me.txtMwoAdj.TabStop = False
        '
        'txtMwoStart
        '
        Me.txtMwoStart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMwoStart.Enabled = False
        Me.txtMwoStart.EnterMoveNextControl = True
        Me.txtMwoStart.Location = New System.Drawing.Point(0, 26)
        Me.txtMwoStart.Name = "txtMwoStart"
        Me.txtMwoStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMwoStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMwoStart.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMwoStart.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtMwoStart.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtMwoStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtMwoStart.Properties.Mask.ShowPlaceHolders = False
        Me.txtMwoStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMwoStart.Size = New System.Drawing.Size(92, 20)
        Me.txtMwoStart.TabIndex = 1
        Me.txtMwoStart.TabStop = False
        '
        'lblMwo
        '
        Me.lblMwo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMwo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMwo.Location = New System.Drawing.Point(0, 4)
        Me.lblMwo.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.lblMwo.Name = "lblMwo"
        Me.lblMwo.Size = New System.Drawing.Size(88, 13)
        Me.lblMwo.TabIndex = 0
        Me.lblMwo.Text = "Markdown Qty"
        Me.lblMwo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtComment
        '
        Me.txtComment.EnterMoveNextControl = True
        Me.txtComment.Location = New System.Drawing.Point(87, 104)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtComment.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtComment.Properties.MaxLength = 20
        Me.txtComment.Size = New System.Drawing.Size(180, 20)
        Me.txtComment.TabIndex = 9
        Me.txtComment.Visible = False
        '
        'txtSkuPrice
        '
        Me.txtSkuPrice.Enabled = False
        Me.txtSkuPrice.EnterMoveNextControl = True
        Me.txtSkuPrice.Location = New System.Drawing.Point(87, 0)
        Me.txtSkuPrice.Name = "txtSkuPrice"
        Me.txtSkuPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuPrice.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuPrice.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuPrice.Properties.DisplayFormat.FormatString = "c2"
        Me.txtSkuPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSkuPrice.Size = New System.Drawing.Size(94, 20)
        Me.txtSkuPrice.TabIndex = 1
        Me.txtSkuPrice.TabStop = False
        '
        'txtSkuEndValue
        '
        Me.txtSkuEndValue.Enabled = False
        Me.txtSkuEndValue.EnterMoveNextControl = True
        Me.txtSkuEndValue.Location = New System.Drawing.Point(187, 78)
        Me.txtSkuEndValue.Name = "txtSkuEndValue"
        Me.txtSkuEndValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuEndValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuEndValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuEndValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuEndValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtSkuEndValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSkuEndValue.Size = New System.Drawing.Size(80, 20)
        Me.txtSkuEndValue.TabIndex = 15
        Me.txtSkuEndValue.TabStop = False
        '
        'txtSkuAdjustValue
        '
        Me.txtSkuAdjustValue.Enabled = False
        Me.txtSkuAdjustValue.EnterMoveNextControl = True
        Me.txtSkuAdjustValue.Location = New System.Drawing.Point(187, 52)
        Me.txtSkuAdjustValue.Name = "txtSkuAdjustValue"
        Me.txtSkuAdjustValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuAdjustValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuAdjustValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuAdjustValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuAdjustValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtSkuAdjustValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSkuAdjustValue.Size = New System.Drawing.Size(80, 20)
        Me.txtSkuAdjustValue.TabIndex = 14
        Me.txtSkuAdjustValue.TabStop = False
        '
        'txtSkuStartValue
        '
        Me.txtSkuStartValue.Enabled = False
        Me.txtSkuStartValue.EnterMoveNextControl = True
        Me.txtSkuStartValue.Location = New System.Drawing.Point(187, 26)
        Me.txtSkuStartValue.Name = "txtSkuStartValue"
        Me.txtSkuStartValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuStartValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuStartValue.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuStartValue.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuStartValue.Properties.DisplayFormat.FormatString = "c2"
        Me.txtSkuStartValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSkuStartValue.Size = New System.Drawing.Size(80, 20)
        Me.txtSkuStartValue.TabIndex = 13
        Me.txtSkuStartValue.TabStop = False
        '
        'lblComment
        '
        Me.lblComment.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblComment.Location = New System.Drawing.Point(0, 104)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(81, 20)
        Me.lblComment.TabIndex = 8
        Me.lblComment.Text = "&Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblComment.Visible = False
        '
        'txtSkuEnd
        '
        Me.txtSkuEnd.EnterMoveNextControl = True
        Me.txtSkuEnd.Location = New System.Drawing.Point(87, 78)
        Me.txtSkuEnd.Name = "txtSkuEnd"
        Me.txtSkuEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuEnd.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuEnd.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuEnd.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtSkuEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtSkuEnd.Properties.Mask.ShowPlaceHolders = False
        Me.txtSkuEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSkuEnd.Size = New System.Drawing.Size(94, 20)
        Me.txtSkuEnd.TabIndex = 7
        Me.txtSkuEnd.Tag = "false"
        '
        'txtSkuAdjust
        '
        Me.txtSkuAdjust.EnterMoveNextControl = True
        Me.txtSkuAdjust.Location = New System.Drawing.Point(87, 52)
        Me.txtSkuAdjust.Name = "txtSkuAdjust"
        Me.txtSkuAdjust.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuAdjust.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuAdjust.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuAdjust.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuAdjust.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtSkuAdjust.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtSkuAdjust.Properties.Mask.ShowPlaceHolders = False
        Me.txtSkuAdjust.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSkuAdjust.Size = New System.Drawing.Size(94, 20)
        Me.txtSkuAdjust.TabIndex = 5
        Me.txtSkuAdjust.Tag = "false"
        '
        'txtSkuStart
        '
        Me.txtSkuStart.EnterMoveNextControl = True
        Me.txtSkuStart.Location = New System.Drawing.Point(87, 26)
        Me.txtSkuStart.Name = "txtSkuStart"
        Me.txtSkuStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSkuStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSkuStart.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSkuStart.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txtSkuStart.Properties.Mask.EditMask = "-?[0-9]+"
        Me.txtSkuStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtSkuStart.Properties.Mask.ShowPlaceHolders = False
        Me.txtSkuStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSkuStart.Size = New System.Drawing.Size(94, 20)
        Me.txtSkuStart.TabIndex = 3
        Me.txtSkuStart.Tag = "false"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(187, 0)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 20)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Value"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(0, 0)
        Me.Label4.Margin = New System.Windows.Forms.Padding(3, 1, 3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 20)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Price"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label11
        '
        Me.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.label11.Location = New System.Drawing.Point(0, 78)
        Me.label11.Margin = New System.Windows.Forms.Padding(3, 2, 3, 0)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(81, 20)
        Me.label11.TabIndex = 6
        Me.label11.Text = "&New Balance"
        Me.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label10
        '
        Me.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.label10.Location = New System.Drawing.Point(0, 52)
        Me.label10.Margin = New System.Windows.Forms.Padding(3, 2, 3, 0)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(81, 20)
        Me.label10.TabIndex = 4
        Me.label10.Text = "&Adjustment"
        Me.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label9
        '
        Me.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.label9.Location = New System.Drawing.Point(0, 26)
        Me.label9.Margin = New System.Windows.Forms.Padding(3, 2, 3, 0)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(81, 20)
        Me.label9.TabIndex = 2
        Me.label9.Text = "&Start Stock"
        Me.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSku
        '
        Me.lblSku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSku.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSku.Location = New System.Drawing.Point(6, 16)
        Me.lblSku.Name = "lblSku"
        Me.lblSku.Size = New System.Drawing.Size(270, 20)
        Me.lblSku.TabIndex = 0
        Me.lblSku.Text = "Enter SKU  (Press F2 for lookup)"
        Me.lblSku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(872, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "F12 Exit"
        '
        'btnReset
        '
        Me.btnReset.AllowFocus = False
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(791, 508)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 39)
        Me.btnReset.TabIndex = 7
        Me.btnReset.Text = "F11 Reset"
        '
        'btnCreate
        '
        Me.btnCreate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCreate.Appearance.Options.UseTextOptions = True
        Me.btnCreate.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnCreate.Location = New System.Drawing.Point(4, 508)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(75, 39)
        Me.btnCreate.TabIndex = 3
        Me.btnCreate.Text = "F6 Create Adjustment"
        '
        'btnMaintain
        '
        Me.btnMaintain.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMaintain.Appearance.Options.UseTextOptions = True
        Me.btnMaintain.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnMaintain.Location = New System.Drawing.Point(85, 508)
        Me.btnMaintain.Name = "btnMaintain"
        Me.btnMaintain.Size = New System.Drawing.Size(75, 39)
        Me.btnMaintain.TabIndex = 4
        Me.btnMaintain.Text = "F7 Maintain Adjustment"
        '
        'btnReverse
        '
        Me.btnReverse.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReverse.Appearance.Options.UseTextOptions = True
        Me.btnReverse.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnReverse.Location = New System.Drawing.Point(166, 508)
        Me.btnReverse.Name = "btnReverse"
        Me.btnReverse.Size = New System.Drawing.Size(75, 39)
        Me.btnReverse.TabIndex = 5
        Me.btnReverse.Text = "F8 Reverse Adjustment"
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.AllowFocus = False
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Location = New System.Drawing.Point(681, 508)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(104, 39)
        Me.btnStockEnquiry.TabIndex = 6
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        '
        'Create
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.btnReverse)
        Me.Controls.Add(Me.btnMaintain)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.xgdAdjusts)
        Me.Controls.Add(Me.grpCode)
        Me.Controls.Add(Me.grpSku)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Create"
        Me.Size = New System.Drawing.Size(950, 550)
        CType(Me.xgdAdjusts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgvAdjusts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCode.ResumeLayout(False)
        Me.pnlCode.ResumeLayout(False)
        CType(Me.lueCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSku.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSku.ResumeLayout(False)
        CType(Me.txtSkuDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDrlNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTransfer.ResumeLayout(False)
        CType(Me.txtTransEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransAdj.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransEndValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransAdjValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTransStartValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSku.ResumeLayout(False)
        Me.pnlMwo.ResumeLayout(False)
        CType(Me.txtMwoEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMwoAdj.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMwoStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuEndValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuAdjustValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuStartValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuAdjust.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSkuStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub


    Friend WithEvents grpSku As System.Windows.Forms.GroupBox
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents label11 As System.Windows.Forms.Label
    Friend WithEvents label10 As System.Windows.Forms.Label
    Friend WithEvents label9 As System.Windows.Forms.Label
    Friend WithEvents cmbTrans As System.Windows.Forms.ComboBox
    Friend WithEvents grpCode As System.Windows.Forms.GroupBox
    Friend WithEvents lblSku As System.Windows.Forms.Label
    Friend WithEvents pnlSku As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lueCode As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents pnlCode As System.Windows.Forms.Panel
    Friend WithEvents lblMarkWO As System.Windows.Forms.Label
    Friend WithEvents lblSecurity As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pnlTransfer As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSku As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuEndValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuAdjustValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuStartValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuAdjust As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSkuStart As DevExpress.XtraEditors.TextEdit
    Friend WithEvents xgdAdjusts As DevExpress.XtraGrid.GridControl
    Friend WithEvents xgvAdjusts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtComment As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransAdj As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransEndValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransStart As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransAdjValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTransStartValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents pnlMwo As System.Windows.Forms.Panel
    Friend WithEvents txtMwoEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMwoAdj As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMwoStart As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblMwo As System.Windows.Forms.Label
    Friend WithEvents btnCreate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReverse As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMaintain As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtDrlNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDrlNumber As System.Windows.Forms.Label
    Friend WithEvents txtSkuDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnStockEnquiry As DevExpress.XtraEditors.SimpleButton

End Class
