﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TypeDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl = New System.Windows.Forms.Label
        Me.btnMarkdown = New System.Windows.Forms.Button
        Me.btnWriteOff = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lbl
        '
        Me.lbl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.Location = New System.Drawing.Point(12, 12)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(306, 30)
        Me.lbl.TabIndex = 13
        Me.lbl.Text = "Select either markdown or write off adjustment type"
        Me.lbl.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnMarkdown
        '
        Me.btnMarkdown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMarkdown.Location = New System.Drawing.Point(15, 46)
        Me.btnMarkdown.Name = "btnMarkdown"
        Me.btnMarkdown.Size = New System.Drawing.Size(83, 40)
        Me.btnMarkdown.TabIndex = 0
        Me.btnMarkdown.Text = "F5 Markdown"
        Me.btnMarkdown.UseVisualStyleBackColor = True
        '
        'btnWriteOff
        '
        Me.btnWriteOff.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnWriteOff.Location = New System.Drawing.Point(104, 46)
        Me.btnWriteOff.Name = "btnWriteOff"
        Me.btnWriteOff.Size = New System.Drawing.Size(83, 40)
        Me.btnWriteOff.TabIndex = 14
        Me.btnWriteOff.Text = "F6 Write Off"
        Me.btnWriteOff.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(235, 46)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(83, 40)
        Me.btnCancel.TabIndex = 15
        Me.btnCancel.Text = "F10 Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'AdjustTypeDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(330, 97)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnWriteOff)
        Me.Controls.Add(Me.btnMarkdown)
        Me.Controls.Add(Me.lbl)
        Me.KeyPreview = True
        Me.Name = "AdjustTypeDialog"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Please Select an Option"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents btnMarkdown As System.Windows.Forms.Button
    Friend WithEvents btnWriteOff As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
