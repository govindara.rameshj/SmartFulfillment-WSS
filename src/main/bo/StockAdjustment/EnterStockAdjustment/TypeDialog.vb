﻿Imports System.Windows.Forms

Public Class TypeDialog

    Private _type As String = String.Empty

    Public ReadOnly Property Type() As String
        Get
            Return _type
        End Get
    End Property

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub AdjustTypeDialog_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnMarkdown.PerformClick()
            Case Keys.F6 : btnWriteOff.PerformClick()
            Case Keys.F10 : btnCancel.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub btnMarkdown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarkdown.Click

        _type = "MarkDown"
        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub btnWriteOff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteOff.Click

        _type = "WriteOff"
        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

    End Sub

End Class