﻿Public Interface IManageStockAdjustments
    Function PrePopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As IAdjustments
    Function IsThisTheFirstStockAdjustmentForSkuToday(ByVal StockAdjustments As IAdjustments, ByVal Adjustment As IAdjustment, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As Boolean
    Function PostPopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As IAdjustments
End Interface
