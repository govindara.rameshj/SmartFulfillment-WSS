﻿Public Interface IClearButtons
    Function MakeButtonsInvisible(ByVal Button1 As SimpleButton, ByVal Button2 As SimpleButton) As Boolean
    Function ClearGrid(ByVal Grid As GridControl) As Boolean
End Interface
