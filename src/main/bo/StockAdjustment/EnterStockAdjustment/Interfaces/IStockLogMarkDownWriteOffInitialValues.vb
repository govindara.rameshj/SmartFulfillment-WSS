﻿Option Strict On
Option Explicit On

Public Interface IStockLogMarkDownWriteOffInitialValues

    Function GetInitialMarkDownValue(ByVal FromStockLog As StockAdjustments.IStockLog) As Decimal
    Function GetInitialWriteOffValue(ByVal FromStockLog As StockAdjustments.IStockLog) As Decimal
End Interface
