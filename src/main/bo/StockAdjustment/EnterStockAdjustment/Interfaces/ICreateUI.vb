﻿Option Strict On

Public Interface ICreateUI

    Enum ControlProcessedOrder
        Standard
        Reverse
    End Enum

    Sub SetControlFocus(ByVal Order As ControlProcessedOrder, _
                        ByRef StartingStock As TextEdit, ByRef AdjustmentValue As TextEdit, ByRef EndingStock As TextEdit, ByRef Comment As TextEdit)

    Sub SetControlValues(ByVal Value As IAdjustment, _
                         ByRef StockPrice As TextEdit, ByRef StartingStock As TextEdit, ByRef AdjustmentValue As TextEdit, ByRef EndingStock As TextEdit)

    Sub EnableAdjustmentValueControl(ByRef AdjustmentValue As TextEdit)

    Sub DisableAdjustmentValueControl(ByRef AdjustmentValue As TextEdit)


End Interface