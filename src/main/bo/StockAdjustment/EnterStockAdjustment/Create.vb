Public Class Create
    Implements ISkuNumberAndDescriptionContainer

#Region "Private Variables"

    Private _StockAdjustmentFixes As IStockAdjustmentFixes
    Private _CreateUI As ICreateUI

    Private _StockAdjustmentManager As IStockAdjustmentManager = StockAdjustmentManagerFactory.FactoryGet
    Private _StockAdjustmentCollection As IAdjustments = AdjustmentsFactory.FactoryGet
    Private _SpecificStockAdjustment As IAdjustment = AdjustmentFactory.FactoryGet
    Private _ExistingStockAdjustmentToBeReversed As IAdjustment

    Private _StockAdjustmentCode As StockAdjustments.ISaCode = StockAdjustments.SACodeFactory.FactoryGet
    Private _TomorrowSystemDate As Date = Nothing

    Private _dialogShown As Boolean
    Private _useMarkdownSchedule As Boolean
    Private _CreateWriteOff As Boolean = False
    Private _CreateMarkdown As Boolean = False
    Private _CancelSelected As Boolean = False

    Private _lastCode As String
    Private _lastComment As String

#End Region

#Region "Rest"

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)

        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        _StockAdjustmentFixes = (New StockAdjustmentFixesFactory).GetImplementation()
        _CreateUI = (New CreateUIFactory).GetImplementation

    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : If btnStockEnquiry.Visible And btnStockEnquiry.Enabled Then btnStockEnquiry_Click(sender, New EventArgs)
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F6 : btnCreate.PerformClick()
            Case Keys.F7 : btnMaintain.PerformClick()
            Case Keys.F8 : btnReverse.PerformClick()
            Case Keys.F10 : btnCancel.PerformClick()
            Case Keys.F11 : btnReset_Click(sender, New EventArgs)
            Case Keys.F12 : btnExit_Click(sender, New EventArgs)
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)

        If pnlSku.Visible Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.Confirmations.ConfirmExit, String.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result <> DialogResult.Yes Then e.Cancel = True
        End If

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            _TomorrowSystemDate = _SpecificStockAdjustment.ReadAdjustmentDate

            DisplayProgress(0, My.Resources.Messages.GetParameters)

            Dim SACodes As StockAdjustments.ISACodes = StockAdjustments.SACodesFactory.FactoryGet
            SACodes.GetSACodes(SecurityLevel)

            lueCode.Properties.DataSource = SACodes
            lueCode.Properties.ValueMember = "CodeNumber"
            lueCode.Properties.DisplayMember = "DataDisplay"

            ' Add columns to the dropdown.
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("CodeNumber", "Code", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("Description", "Description", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("CodeType", "Type", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("CodeSign", "Sign", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("SecurityLevel", "Security Level", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("IsMarkdown", "Markdown", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("IsWriteOff", "Write Off", 0))
            lueCode.Properties.Columns.Add(New LookUpColumnInfo("DataDisplay", "DataDisplay", 0))
            lueCode.Properties.AutoSearchColumnIndex = 0 '"CodeNumber"
            lueCode.Properties.Columns("DataDisplay").Visible = False
            lueCode.Properties.BestFit()

            DisplayProgress(100)
            DisplayProgress()

            lueCode.Focus()
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Protected Overrides Function DoSecurityCheck(ByRef Message As String) As Cts.Oasys.WinForm.Form.SecurityCheckResult
        Dim SACodes As StockAdjustments.ISACodes = StockAdjustments.SACodesFactory.FactoryGet
        Dim Codes As Cts.Oasys.Core.BaseCollection(Of StockAdjustments.SACode.SaCode) = CType(SACodes, Global.Cts.Oasys.Core.BaseCollection(Of Global.StockAdjustments.SACode.SaCode))

        SACodes.GetSACodes(SecurityLevel)
        If Codes.Count = 0 Then
            DoSecurityCheck = SecurityCheckResult.Failure
            Message = "Security level " & SecurityLevel.ToString & " cannot see any Stock Adjustment Codes"
        Else
            DoSecurityCheck = SecurityCheckResult.Success
            Message = "Security level " & SecurityLevel.ToString & " can see " & Codes.Count.ToString & " Stock Adjustment Codes"
        End If
    End Function

    Private Sub Create_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Validating
        e.Cancel = False
    End Sub

#End Region

#Region "Stock Code Dropdown Control Event Handlers"

    Private Sub lueCode_KeyDown(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles lueCode.KeyDown

        Select Case e.KeyData
            Case Keys.Down, Keys.Up
                If Not lueCode.IsPopupOpen Then
                    lueCode.ShowPopup()
                End If
        End Select

    End Sub

    Private Sub lueCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lueCode.KeyPress

        Select Case True
            Case e.KeyChar = ChrW(Keys.Enter)
                lueCode_Closed(sender, New ClosedEventArgs(DevExpress.XtraEditors.PopupCloseMode.Normal))
            Case IsNumeric(e.KeyChar)
                lueCode.Properties.AutoSearchColumnIndex = 0
                lueCode.Properties.DisplayMember = "CodeNumber"
            Case Else
                lueCode.Properties.AutoSearchColumnIndex = 1
                lueCode.Properties.DisplayMember = "Description"
        End Select

    End Sub

    Private Sub lueCode_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles lueCode.Closed

        Select Case e.CloseMode
            Case DevExpress.XtraEditors.PopupCloseMode.Cancel
            Case DevExpress.XtraEditors.PopupCloseMode.CloseUpKey
            Case DevExpress.XtraEditors.PopupCloseMode.Immediate
            Case Else
                'first check that something has been selected
                If _dialogShown Then Exit Sub
                If lueCode.EditValue Is Nothing Then Exit Sub

                ClearControls()

                lueCode.Properties.DisplayMember = "DataDisplay"

                'load adjust code object from row
                'check whether both markdown and write off and force choice if true
                _StockAdjustmentCode = StockAdjustments.SACodeFactory.FactoryGet
                _StockAdjustmentCode.Load(SecurityLevel, lueCode.EditValue.ToString)
                If _StockAdjustmentCode.IsWriteOff Then
                    _StockAdjustmentCode.MarkdownWriteoff = "W"
                End If
                If _StockAdjustmentCode.IsMarkdown Then
                    _StockAdjustmentCode.MarkdownWriteoff = "M"
                End If
                If _StockAdjustmentCode.IsWriteOff And _StockAdjustmentCode.IsMarkdown Then
                    _StockAdjustmentCode.MarkdownWriteoff = " "
                End If

                btnSave.Text = My.Resources.Controls.F5SaveAdjustment
                btnCreate.Text = My.Resources.Controls.F6CreateAdjustment
                btnMaintain.Text = My.Resources.Controls.F7MaintainAdjustment
                btnReverse.Text = My.Resources.Controls.F8ReverseAdjustment
                btnCancel.Text = My.Resources.Controls.F10CancelAdjustment

                'populate code labels
                lblCode.Text = _StockAdjustmentCode.CodeNumber & Space(2) & _StockAdjustmentCode.DisplayType
                lblAction.Text = _StockAdjustmentCode.DisplaySign
                lblSecurity.Text = _StockAdjustmentCode.SecurityLevel
                lblMarkWO.Text = _StockAdjustmentCode.MarkdownWriteoff
                lblMwo.Text = _StockAdjustmentCode.MarkdownWriteoff & " Qty"
                pnlCode.Visible = True

                lblComment.Enabled = _StockAdjustmentCode.IsCommentable
                txtComment.Enabled = _StockAdjustmentCode.IsCommentable

                grpCode.Enabled = False
                grpSku.Visible = True
                txtSku.Focus()

                Select Case _StockAdjustmentCode.CodeSign
                    Case "Calculate"
                        txtSkuStart.Enabled = True
                        txtSkuAdjust.Enabled = False
                        txtSkuEnd.Enabled = True
                        label11.Text = "End Stock"

                    Case "Replace"
                        txtSkuStart.Enabled = False
                        txtSkuAdjust.Enabled = False
                        txtSkuEnd.Enabled = True
                        label11.Text = "End Stock"

                    Case Else
                        txtSkuStart.Enabled = False
                        txtSkuAdjust.Enabled = True
                        txtSkuEnd.Enabled = False
                        label11.Text = "New Balance"
                End Select

                If pnlTransfer.Visible Then
                    '_stock.LoadReducedStocks()
                    cmbTrans_Initialise()
                End If

                'check if is receipt adjustment to show drl entry box
                lblDrlNumber.Visible = CBool(IIf(_StockAdjustmentCode.CodeNumber = "04", True, False))
                txtDrlNumber.Visible = CBool(IIf(_StockAdjustmentCode.CodeNumber = "04", True, False))

        End Select

    End Sub

#End Region

#Region "Sku Number Control Event Handlers"

    Private Sub txtSku_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSku.EditValueChanged
        Dim container As ISkuNumberAndDescriptionContainer = Me
        _StockAdjustmentManager.SetDescriptionForSkuNumber(container)
    End Sub

    Private Sub txtSku_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSku.GotFocus
        Dim ClearButtonsImplementation As IClearButtons = (New ClearButtonsFactory).GetImplementation()

        _CancelSelected = ClearButtonsImplementation.MakeButtonsInvisible(btnCreate, btnMaintain)
        ClearButtonsImplementation.ClearGrid(xgdAdjusts)



        _StockAdjustmentFixes.DisableReverseAdjustmentButton(btnReverse)


    End Sub

    Private Sub txtSku_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSku.Validating

        If _CancelSelected = True Then Exit Sub

        Dim errorMessage As String = String.Empty

        btnCreate.Text = "F6 Create Adjustment"
        btnMaintain.Text = "F7 Maintain Adjustment"
        'try and load stock item
        Dim sku As String = CStr(txtSku.EditValue)
        If sku IsNot Nothing AndAlso sku.Length > 0 Then
            If _StockAdjustmentManager.Stock.DoesSkuExist(sku) = False Then
                ' In this case there is already warning message in SKU Description, so dont show any more messages
                txtSku.SelectAll()
                txtSku.Focus()
                Exit Sub
            End If

            Dim Connect As StockAdjustments.IConnecxion = StockAdjustments.ConnexionFactory.FactoryGet
            Dim Con As Connection = Connect.Connecxion()



            'sku changed; reset stock adjustment collection
            _StockAdjustmentFixes.AllCodeFourStockAdjustments = Nothing




            If _StockAdjustmentManager.GetSku(_StockAdjustmentCode, sku, Now) Then
                _SpecificStockAdjustment = StockAdjustments.AdjustmentFactory.FactoryGet
                ' Restore comment from previous item
                If _StockAdjustmentCode.CodeNumber = _lastCode Then
                    _SpecificStockAdjustment.Comment_INFO = _lastComment
                End If

                If _SpecificStockAdjustment.ReadAdjustment(Con, sku, _TomorrowSystemDate, _StockAdjustmentCode.CodeNumber) Then

                    'Referral 909 make sure Till Markdown Sales are shown in reports, but not Maintained.
                    Dim ManageAdjustmentImplementation As IManageStockAdjustments = (New ManageStockAdjustmentFactory).GetImplementation()
                    Dim SkuAdjustments As IManageStockAdjustments = ManageAdjustmentImplementation
                    _StockAdjustmentCollection = SkuAdjustments.PrePopulateReadStockAdjustmentsForSkuToday(_StockAdjustmentCollection, sku, _TomorrowSystemDate, _StockAdjustmentCode.CodeNumber)

                    btnMaintain.Enabled = True
                    btnMaintain.Visible = True
                    btnMaintain.Focus()
                    'If _SpecificStockAdjustment.SEQN = "00" And _StockAdjustmentCode.IsMarkdown And _StockAdjustmentCode.IsWriteOff Then
                    If SkuAdjustments.IsThisTheFirstStockAdjustmentForSkuToday(_StockAdjustmentCollection, _SpecificStockAdjustment, sku, _TomorrowSystemDate, _StockAdjustmentCode.CodeNumber) And _StockAdjustmentCode.IsMarkdown And _StockAdjustmentCode.IsWriteOff Then
                        btnCreate.Enabled = True
                        btnCreate.Visible = True
                        btnCreate.Focus()
                        If _SpecificStockAdjustment.MarkDownOrWriteOff_MOWT = "M" Then
                            _CreateWriteOff = True
                            _CreateMarkdown = False
                            _StockAdjustmentCode.MarkdownWriteoff = "Markdown"
                            _StockAdjustmentCode.IsMarkdown = True
                            _StockAdjustmentCode.IsWriteOff = False
                            btnCreate.Text = "F6 Create Write Off"
                            btnMaintain.Text = "F7 Maintain Markdown"
                        Else
                            _CreateMarkdown = True
                            _StockAdjustmentCode.IsMarkdown = False
                            _StockAdjustmentCode.IsWriteOff = True
                            _StockAdjustmentCode.MarkdownWriteoff = "Write Off"
                            btnCreate.Text = "F6 Create Markdown"
                            btnMaintain.Text = "F7 Maintain Write Off"
                        End If
                        'Need to create a clean one for Creation if they choose it
                        _SpecificStockAdjustment = StockAdjustments.AdjustmentFactory.FactoryGet
                        _SpecificStockAdjustment.SEQN = "00"
                    End If

                    'User can Choose which one to maintain
                    _SpecificStockAdjustment.Price = _StockAdjustmentManager.Stock.Price_PRIC
                    _SpecificStockAdjustment.StartingStock_SSTK = _StockAdjustmentManager.Stock.OnHand_ONHA

                    _StockAdjustmentCollection = SkuAdjustments.PostPopulateReadStockAdjustmentsForSkuToday(_StockAdjustmentCollection, sku, _TomorrowSystemDate, _StockAdjustmentCode.CodeNumber)
                    '_StockAdjustmentCollection = StockAdjustments.AdjustmentsFactory.FactoryGet
                    '_StockAdjustmentCollection.ReadAdjustments(sku, _TomorrowSystemDate, _StockAdjustmentCode.CodeNumber)

                    _StockAdjustmentFixes.HighestSequenceNo(_StockAdjustmentCode, _StockAdjustmentCollection)
                    _StockAdjustmentFixes.AllCodeFourStockAdjustments = _StockAdjustmentCollection

                    LoadAdjustmentsToGrid()
                Else
                    'See if user needs to choose between markdown or writeoff
                    If _StockAdjustmentCode.IsMarkdown And _StockAdjustmentCode.IsWriteOff Then
                        _dialogShown = True
                        Using typeDialog As New TypeDialog
                            If typeDialog.ShowDialog(Me) = DialogResult.OK Then
                                _StockAdjustmentCode.MarkdownWriteoff = typeDialog.Type
                                If _StockAdjustmentCode.MarkdownWriteoff.Substring(0, 1) = "M" Then
                                    _StockAdjustmentCode.IsWriteOff = False
                                Else
                                    _StockAdjustmentCode.IsMarkdown = False
                                End If
                            Else
                                _dialogShown = False
                                txtSku.Focus()
                                Exit Sub
                            End If
                        End Using

                        _dialogShown = False
                        pnlMwo.Visible = True
                    Else
                        ' if this is 04 code
                        If _StockAdjustmentCode.CodeNumber = "04" Then
                            _SpecificStockAdjustment.Price = _StockAdjustmentManager.Stock.Price_PRIC
                            _SpecificStockAdjustment.StartingStock_SSTK = _StockAdjustmentManager.Stock.OnHand_ONHA
                            'txtDrlNumber.Focus()
                            Me.ActiveControl = txtDrlNumber
                            Exit Sub
                        End If
                    End If

                    btnCreate.Enabled = True
                    btnCreate.Visible = True
                    btnCreate.Focus()
                    _SpecificStockAdjustment.Price = _StockAdjustmentManager.Stock.Price_PRIC
                    _SpecificStockAdjustment.StartingStock_SSTK = _StockAdjustmentManager.Stock.OnHand_ONHA
                    'pnlSku.Visible = True
                End If

            Else
                txtSku.SelectAll()
                If e IsNot Nothing Then
                    e.Cancel = True
                End If
                txtSku.Focus()
            End If

            Con.Dispose()

        End If

    End Sub

#End Region

#Region "DRL Control Event Handlers"

    'PD 08/03/3012 - since the event handler has been commented out this code will never be called

    'Private Sub txtDrlNumber_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) 'Handles txtDrlNumber.KeyDown
    '    If e.KeyData = Keys.Return Then
    '        Dim mye As New System.ComponentModel.CancelEventArgs
    '        txtDrlNumber_Validating(sender, mye)
    '        If mye.Cancel Then
    '            txtDrlNumber.SelectAll()
    '            'txtDrlNumber.Focus()
    '            Me.ActiveControl = txtDrlNumber
    '        End If
    '    End If
    'End Sub

    Private Sub txtDrlNumber_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDrlNumber.Validating

        _StockAdjustmentFixes.DailyReceiverListingControlValidatingEvent(_StockAdjustmentCode, _
                                                                         _StockAdjustmentManager, _
                                                                         _SpecificStockAdjustment, _
                                                                         _StockAdjustmentCollection, _
                                                                         txtSku.Text, _
                                                                         _CancelSelected, _
                                                                         btnCreate, _
                                                                         btnMaintain, _
                                                                         btnReverse, _
                                                                         txtDrlNumber, _
                                                                         e)

        _StockAdjustmentFixes.GridControlRefreshDataSourceEvent(_StockAdjustmentCode, _
                                                                _StockAdjustmentCollection, _
                                                                xgdAdjusts)

    End Sub

#End Region

#Region "Transfer Sku Dropdown Control Event Handlers"

    Private Sub cmbTrans_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTrans.SelectedIndexChanged

        If cmbTrans.SelectedIndex >= 0 Then
            Dim stock As StockAdjustments.IStock = StockAdjustments.StockFactory.FactoryGet

            Dim Connect As StockAdjustments.IConnecxion = StockAdjustments.ConnexionFactory.FactoryGet
            Dim Con As Connection = Connect.Connecxion()

            stock.ReadSKU(Con, CStr(cmbTrans.SelectedValue))
            txtTransPrice.EditValue = stock.Price_PRIC
            txtTransStart.EditValue = stock.OnHand_ONHA
            txtTransStartValue.EditValue = CDec(txtTransPrice.EditValue) * CInt(txtTransStart.EditValue)
            txtTransAdj.EditValue = 0
            txtTransAdjValue.EditValue = 0
            txtTransEnd.EditValue = txtTransStart.EditValue
            txtTransEndValue.EditValue = txtTransStartValue.EditValue
            Con.Dispose()
        End If

    End Sub

#End Region

#Region "Sku Start/Adjustment/End Control(s) Event Handlers"

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSkuStart.GotFocus, txtSkuAdjust.GotFocus, txtSkuEnd.GotFocus
        CType(sender, DevExpress.XtraEditors.TextEdit).SelectAll()
    End Sub

    Private Sub txt_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles txtSkuStart.EditValueChanging, txtSkuAdjust.EditValueChanging, txtSkuEnd.EditValueChanging
        CType(sender, DevExpress.XtraEditors.TextEdit).Tag = True
    End Sub

    Private Sub txt_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSkuStart.EditValueChanged, txtSkuAdjust.EditValueChanged, txtSkuEnd.EditValueChanged

        Dim text As DevExpress.XtraEditors.TextEdit = CType(sender, TextEdit)
        Dim value As Decimal = 0

        If text.EditValue IsNot Nothing AndAlso text.EditValue.ToString.Length > 0 Then
            If IsNumeric(text.EditValue) Then
                value = CDec(txtSkuPrice.EditValue) * CInt(text.EditValue)
            End If
        End If

        Select Case text.Name
            Case txtSkuStart.Name : txtSkuStartValue.EditValue = value
            Case txtSkuAdjust.Name : txtSkuAdjustValue.EditValue = value
            Case txtSkuEnd.Name : txtSkuEndValue.EditValue = value
        End Select

    End Sub

    Private Sub txt_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) 'Handles txtSkuAdjust.KeyDown, txtSkuStart.KeyDown, txtSkuEnd.KeyDown
        If e.KeyData = Keys.Return Then
            Dim mye As New System.ComponentModel.CancelEventArgs
            txt_Validating(sender, mye)
        End If
    End Sub

    Private Sub txt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSkuAdjust.Validating, txtSkuStart.Validating, txtSkuEnd.Validating

        If _CancelSelected = True Then Exit Sub

        Dim txt As DevExpress.XtraEditors.TextEdit = CType(sender, TextEdit)

        Select Case txt.Name
            Case txtSkuAdjust.Name
                Dim AdjQty As Integer

                If Not String.IsNullOrEmpty(txtSkuAdjust.Text) Then
                    AdjQty = CInt(txtSkuAdjust.Text)

                    If _StockAdjustmentFixes.GetAdjustment(_StockAdjustmentCode, _StockAdjustmentManager, AdjQty, txtSku.Text, _SpecificStockAdjustment) = True Then
                        btnSave.Enabled = True

                        txt.EditValue = AdjQty
                        If pnlTransfer.Visible Then
                            txtTransAdj.EditValue = AdjQty * -1
                            txtTransAdjValue.EditValue = CStr(CDec(txtTransPrice.EditValue) * CInt(txtTransAdj.EditValue))
                            txtTransEnd.EditValue = CStr(CInt(txtTransStart.EditValue) + CInt(txtTransAdj.EditValue))
                            txtTransEndValue.EditValue = CStr(CDec(txtTransPrice.EditValue) * CInt(txtTransEnd.EditValue))
                        End If

                        txtSkuEnd.Text = CStr(CInt(txtSkuStart.Text) + AdjQty)
                        If _StockAdjustmentCode.IsWriteOff Or _StockAdjustmentCode.IsMarkdown Then
                            CalculateMarkdownWriteOff()
                        End If
                    Else
                        txtSkuAdjust.SelectAll()
                        txtSkuAdjust.Focus()
                    End If

                End If

            Case txtSkuStart.Name
            Case txtSkuEnd.Name

        End Select

    End Sub

    Private Sub txt_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSkuStart.Validated, txtSkuAdjust.Validated, txtSkuEnd.Validated
        'erAdjusts.SetError(sender, "")
        CType(sender, DevExpress.XtraEditors.TextEdit).Tag = False
    End Sub

#End Region

#Region "Sku Group Box Control Event Handlers"

    Private Sub grpSku_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grpSku.VisibleChanged

        If grpSku.Visible Then
        Else
            'reset buttons
            btnCreate.Visible = False
            btnMaintain.Visible = False
            btnReverse.Visible = False

            'reset grid
            xgdAdjusts.DataSource = Nothing
            xgvAdjusts.Columns.Clear()
            xgdAdjusts.Visible = False

            txtSku.ResetText()
            txtSku.Enabled = True
            btnStockEnquiry.Enabled = True
            lblSku.Text = My.Resources.Controls.lblSkuEnterSkuF2Lookup
            grpSku.Text = My.Resources.Controls.grpSkuSelectItem

            pnlSku.Visible = False
            pnlTransfer.Visible = False
        End If

    End Sub

#End Region

#Region "Panel Control(s) Event Handlers"

    Private Sub pnlCode_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlCode.VisibleChanged

        If pnlCode.Visible Then
        Else
            grpCode.Enabled = True
            lblCode.Text = String.Empty
            lblAction.Text = String.Empty
            lblSecurity.Text = String.Empty
            lblMarkWO.Text = String.Empty
            lueCode.Focus()
        End If

        btnReset.Visible = pnlCode.Visible
        btnStockEnquiry.Visible = pnlCode.Visible

    End Sub

    Private Sub pnlMwo_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlMwo.VisibleChanged
        CalculateMarkdownWriteOff()
    End Sub

    Private Sub pnlSku_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlSku.VisibleChanged

        If pnlSku.Visible Then

            'set comment visibility
            If lblComment.Enabled Then lblComment.Visible = True
            If txtComment.Enabled Then txtComment.Visible = True

            If _SpecificStockAdjustment.Comment_INFO <> String.Empty Then
                txtComment.Text = _SpecificStockAdjustment.Comment_INFO
            End If

            'set initial values
            txtSkuPrice.EditValue = _SpecificStockAdjustment.Price
            txtSkuStart.EditValue = _SpecificStockAdjustment.StartingStock_SSTK
            txtSkuAdjust.EditValue = _SpecificStockAdjustment.Quantity_QUAN
            txtSkuEnd.EditValue = _SpecificStockAdjustment.EndingStock

            If _StockAdjustmentCode.IsWriteOff Or _StockAdjustmentCode.IsMarkdown Then
                pnlMwo.Visible = True
            End If

            _CreateUI.SetControlFocus(ICreateUI.ControlProcessedOrder.Standard, txtSkuStart, txtSkuAdjust, txtSkuEnd, txtComment)

            'Check if code is transfer code
            If _StockAdjustmentCode.CodeType = "T" Then
                pnlTransfer.Visible = True
            End If

        Else
            'reset panel controls
            lblComment.Visible = False
            txtComment.Visible = False
            txtComment.ResetText()
            'reset controls if this isn't a DRL adjustment
            If _StockAdjustmentCode.CodeNumber <> "04" Then
                lblDrlNumber.Visible = False
                txtDrlNumber.Visible = False
                txtDrlNumber.ResetText()
                txtSkuAdjust.ResetText()
            End If

        End If

        'set buttons status
        btnCreate.Enabled = Not pnlSku.Visible
        If xgdAdjusts.Visible Then
            xgvAdjusts_SelectionChanged(xgdAdjusts, New FocusedRowChangedEventArgs(xgvAdjusts.FocusedRowHandle, xgvAdjusts.FocusedRowHandle))
        End If

    End Sub

    Private Sub pnlTransfer_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlTransfer.VisibleChanged

        If pnlTransfer.Visible Then
            cmbTrans_Initialise()
        Else
            'reset controls
            cmbTrans.DataSource = Nothing
            txtTransPrice.ResetText()
            txtTransStart.ResetText()
            txtTransAdj.ResetText()
            txtTransEnd.ResetText()
        End If

    End Sub

#End Region

#Region "Dev Express Grid View Control Event Handlers"

    Private Sub xgvAdjusts_SelectionChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs) Handles xgvAdjusts.FocusedRowChanged

        _StockAdjustmentFixes.CreateOrMaintainOrReverseAdjustmentOptions(_StockAdjustmentCode, _
                                                                         pnlSku.Visible, _
                                                                         xgvAdjusts.IsGroupRow(e.FocusedRowHandle), _
                                                                         CInt(xgvAdjusts.GetFocusedRowCellValue("AmendId")), _
                                                                         CType(xgvAdjusts.GetRow(xgvAdjusts.FocusedRowHandle), IAdjustment), _
                                                                         btnCreate, _
                                                                         btnMaintain, _
                                                                         btnReverse)

    End Sub

    Private Sub xgvAdjusts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles xgvAdjusts.DoubleClick
        xgvAdjusts_KeyPress(sender, New KeyPressEventArgs(ChrW(Keys.Enter)))
    End Sub

    Private Sub xgvAdjusts_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles xgvAdjusts.KeyPress

        _StockAdjustmentFixes.CodeFourReverseAdjustCompletedMessage(_StockAdjustmentCode, _
                                                                    CType(xgvAdjusts.GetRow(xgvAdjusts.FocusedRowHandle), IAdjustment), _
                                                                    e, _
                                                                    xgvAdjusts)

    End Sub

#End Region

#Region "Button Control(s) Event Handlers"

    Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCreate.Click

        _CancelSelected = False

        txtSku.Enabled = False
        btnStockEnquiry.Enabled = False

        If _CreateWriteOff Then
            _StockAdjustmentCode.MarkdownWriteoff = "W"
            _StockAdjustmentCode.IsMarkdown = False
            _StockAdjustmentCode.IsWriteOff = True
        End If

        If _CreateMarkdown Then
            _StockAdjustmentCode.MarkdownWriteoff = "M"
            _StockAdjustmentCode.IsMarkdown = True
            _StockAdjustmentCode.IsWriteOff = False
        End If

        _StockAdjustmentFixes.CreateAdjustmentCodeFourOnly(_StockAdjustmentCode, _StockAdjustmentManager, _SpecificStockAdjustment)

        'set adjust values accordingly
        grpSku.Text = My.Resources.Controls.grpSkuCreateAdjustment
        If _SpecificStockAdjustment IsNot Nothing Then
            _SpecificStockAdjustment.ExistsInDatabase = False
        End If
        pnlSku.Visible = True

    End Sub

    Private Sub btnMaintain_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMaintain.Click

        _CancelSelected = False

        txtSku.Enabled = False
        btnStockEnquiry.Enabled = False

        'Get adjustment to maintain
        _SpecificStockAdjustment = CType(xgvAdjusts.GetRow(xgvAdjusts.FocusedRowHandle), IAdjustment)
        _SpecificStockAdjustment.ExistsInDatabase = True
        _StockAdjustmentCode.MarkdownWriteoff = _SpecificStockAdjustment.MarkDownOrWriteOff_MOWT

        txtSkuPrice.EditValue = _SpecificStockAdjustment.Price
        txtSkuStart.EditValue = _SpecificStockAdjustment.StartingStock_SSTK
        txtSkuAdjust.EditValue = _SpecificStockAdjustment.Quantity_QUAN
        txtSkuEnd.EditValue = _SpecificStockAdjustment.EndingStock

        If _StockAdjustmentCode.IsWriteOff And _StockAdjustmentCode.MarkdownWriteoff = "M" Then
            _StockAdjustmentCode.IsWriteOff = False
        End If

        If _StockAdjustmentCode.IsMarkdown And _StockAdjustmentCode.MarkdownWriteoff = "W" Then
            _StockAdjustmentCode.IsMarkdown = False
        End If

        If _StockAdjustmentCode.IsWriteOff Or _StockAdjustmentCode.IsMarkdown Then
            pnlMwo.Visible = True
        End If

        'Load associated transfer sku.
        If _StockAdjustmentCode.CodeType = "T" Then
            cmbTrans.SelectedValue = _SpecificStockAdjustment.TransferSku_TSKU
            cmbTrans.Enabled = False
            txtTransPrice.Text = FormatNumber(_SpecificStockAdjustment.TransferPrice, 2)
            txtTransStart.Text = FormatNumber(_SpecificStockAdjustment.TransferStart, 0)
            txtTransAdj.Text = FormatNumber(_SpecificStockAdjustment.Quantity_QUAN * -1, 0)
            txtTransEnd.Text = FormatNumber(CDec(txtTransPrice.Text) + CInt(txtTransAdj.Text), 0)
        End If

        'Populate item fields
        grpSku.Text = My.Resources.Controls.grpSkuMaintainAdjustment & " Sequence: " & _SpecificStockAdjustment.SEQN
        pnlSku.Visible = True

    End Sub

    Private Sub btnReverse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReverse.Click

        _CancelSelected = False

        grpSku.Text = My.Resources.Controls.grpSkuMaintainAdjustment & " Sequence: " & _SpecificStockAdjustment.SEQN

        _SpecificStockAdjustment = CType(xgvAdjusts.GetRow(xgvAdjusts.FocusedRowHandle), IAdjustment)
        _StockAdjustmentCode.MarkdownWriteoff = _SpecificStockAdjustment.MarkDownOrWriteOff_MOWT
        _StockAdjustmentFixes.ConfigureExistingAdjustment(_StockAdjustmentManager, _StockAdjustmentCode, _SpecificStockAdjustment, _ExistingStockAdjustmentToBeReversed)
        _StockAdjustmentFixes.ConfigureNewAdjustment(_SpecificStockAdjustment)


        _CreateUI.DisableAdjustmentValueControl(txtSkuAdjust)

        pnlSku.Visible = True

        _CreateUI.SetControlValues(_SpecificStockAdjustment, txtSkuPrice, txtSkuStart, txtSkuAdjust, txtSkuEnd)

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            If Not AuthorisationOK() Then
                Exit Sub
            End If

            If txtSkuAdjust.EditValue.Equals(String.Empty) Then
                Exit Sub
            End If

            If Not AdjustmentConfirmed() Then

                _CreateUI.SetControlFocus(ICreateUI.ControlProcessedOrder.Reverse, txtSkuStart, txtSkuAdjust, txtSkuEnd, txtComment)
                Exit Sub

            End If

            Cursor = Cursors.WaitCursor
            If _StockAdjustmentCode.CodeType = "N" Then

                If _StockAdjustmentFixes.PersistNormalAdjustments(_StockAdjustmentManager, _
                                                                  _StockAdjustmentCode, _
                                                                  _SpecificStockAdjustment, _
                                                                  _ExistingStockAdjustmentToBeReversed, _
                                                                  txtSku.Text, _
                                                                  _TomorrowSystemDate, _
                                                                  txtDrlNumber.Text, _
                                                                  CInt(txtSkuAdjust.EditValue), _
                                                                  txtComment.Text, _
                                                                  UserId) Then

                    MessageBox.Show(My.Resources.Messages.AdjustmentSaved, "Saved", MessageBoxButtons.OK)

                End If

            ElseIf _StockAdjustmentCode.CodeType = "T" Then
                If _StockAdjustmentManager.TranserAdjustment(_StockAdjustmentCode, _SpecificStockAdjustment, txtSku.Text, CStr(cmbTrans.SelectedValue), CInt(txtSkuAdjust.EditValue), _TomorrowSystemDate, txtDrlNumber.Text, txtComment.Text, UserId) Then
                    MessageBox.Show(My.Resources.Messages.AdjustmentSaved, "Saved", MessageBoxButtons.OK)
                End If
            End If

            ' Save last code and corresponding comment
            _lastCode = _StockAdjustmentCode.CodeNumber
            _lastComment = txtComment.Text

            ' Ensure control contents cleared so any validation events fired from losing focus (when make grpSku not visible)
            ' does not cause problems (e.g. re-validation on values that no longer require validation because they have
            ' been altered programmatically)
            ClearControls()
            grpSku.Visible = False
            pnlCode.Visible = False
            pnlSku.Visible = False

            _CreateUI.EnableAdjustmentValueControl(txtSkuAdjust)

        Catch ex As Exception
            _CreateUI.SetControlFocus(ICreateUI.ControlProcessedOrder.Reverse, txtSkuStart, txtSkuAdjust, txtSkuEnd, txtComment)
            Throw

        Finally
            Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        _CreateUI.EnableAdjustmentValueControl(txtSkuAdjust)

        If MessageBox.Show("Are you sure you want to cancel adjustment.", "Cancel Adjustment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
            Exit Sub
        End If

        _CancelSelected = True
        txtSku.Enabled = True
        btnStockEnquiry.Enabled = True
        txtSkuAdjust.Text = String.Empty
        pnlSku.Visible = False
        pnlTransfer.Visible = False
        If btnCreate.Visible Then
            btnCreate.Enabled = True
            btnCreate.Focus()
        ElseIf btnMaintain.Visible Then
            btnMaintain.Enabled = True
            btnMaintain.Focus()
        End If

        'configure buttons based on active row selected in grid
        _StockAdjustmentFixes.CodeFourReverseAdjustCancel(_StockAdjustmentCode, _SpecificStockAdjustment)
        _StockAdjustmentFixes.GridControlRefreshDataSourceEvent(_StockAdjustmentCode, _StockAdjustmentCollection, xgdAdjusts)
        _StockAdjustmentFixes.GridControlSelectionChangedEvent(_StockAdjustmentCode, AddressOf xgvAdjusts_SelectionChanged, xgdAdjusts, xgvAdjusts)

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        _CancelSelected = True
        DisplayStatus()

        If pnlSku.Visible Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.Confirmations.ConfirmReset, String.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If result <> DialogResult.Yes Then Exit Sub
        End If

        grpSku.Visible = False
        pnlCode.Visible = False
        pnlTransfer.Visible = False
        pnlMwo.Visible = False
        xgdAdjusts.Visible = False
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        _CancelSelected = True
        If MessageBox.Show(My.Resources.Confirmations.WarnLeaving, "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
            Exit Sub
        End If

        FindForm.Close()
    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(UserId, WorkstationId, SecurityLevel, String.Empty)

            If _StockAdjustmentCode.CodeType = "Transfer" Then
                lookup.SetSaleType(Cts.Oasys.Core.Stock.SaleTypes.GoodPallet)
            End If
            lookup.ShowAccept()
            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    txtSku.EditValue = lookup.SkuNumbers(0)
                    txtSku.Focus()
                End If
            End Using

        End Using
    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub GridViewAddColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns.AddField(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub LoadAdjustmentsToGrid()

        Dim grid As GridControl = xgdAdjusts
        Dim view As GridView = xgvAdjusts
        Dim dt As New DataTable
        Dim dr As DataRow = dt.NewRow()

        Trace.WriteLine(_SpecificStockAdjustment.AdjustmentDate_DATE1.ToString("dd-MM-yyyy") & ", " & _SpecificStockAdjustment.SEQN & ", " & _SpecificStockAdjustment.EmployeeInitials_INIT & ", " & _SpecificStockAdjustment.StartingStock_SSTK.ToString & ", " & _SpecificStockAdjustment.Quantity_QUAN.ToString & ", " & _SpecificStockAdjustment.EndingStock.ToString & ", " & _SpecificStockAdjustment.Price.ToString & ", " & _SpecificStockAdjustment.Comment_INFO.ToString)

        If _SpecificStockAdjustment IsNot Nothing Then
            grid.DataSource = _StockAdjustmentCollection
            view = CType(grid.MainView, GridView)
            For Each col As GridColumn In view.Columns
                col.VisibleIndex = -1
            Next

            'set up columns
            GridViewAddColumn(view, "AdjustmentDate", "Date Created")
            GridViewAddColumn(view, "SEQN", "Sequence")
            GridViewAddColumn(view, "MarkDownOrWriteOff", "MDN/WTO")

            If _StockAdjustmentCode.CodeNumber = "04" Then
                GridViewAddColumn(view, "DeliveryNumber", "DRL Number")

                txtDrlNumber.EditValue = _StockAdjustmentFixes.DailyReceiverListingNumber(_StockAdjustmentCode, _SpecificStockAdjustment.DeliveryNumber_DRLN)
                If _StockAdjustmentFixes.GetDrlAdjustment(_StockAdjustmentCode, _StockAdjustmentManager, txtDrlNumber, _SpecificStockAdjustment.DeliveryNumber_DRLN, txtSku.Text) = False Then

                    btnMaintain.Visible = False
                    btnMaintain.Enabled = False
                    txtDrlNumber.Text = ""
                    'txtDrlNumber.Focus()
                    Me.ActiveControl = txtDrlNumber
                    Exit Sub

                End If

                If CDate(_SpecificStockAdjustment.AdjustmentDate_DATE1.ToString("yyyy-MM-dd")) < CDate(Now.ToString("yyyy-MM-dd")) Then
                    btnMaintain.Visible = False
                    btnMaintain.Enabled = False
                    btnReverse.Enabled = True
                    btnReverse.Visible = True
                    btnReverse.Focus()
                End If


            End If

            GridViewAddColumn(view, "EmployeeInitials", "Employee Id")
            GridViewAddColumn(view, "StartingStock", "Start Stock")
            GridViewAddColumn(view, "Quantity", "Qty Adjusted")
            GridViewAddColumn(view, "EndingStock", "End Stock")
            GridViewAddColumn(view, "Price", "Price")
            GridViewAddColumn(view, "Comment", "Comment")
            view.BestFitColumns()

            grid.Visible = True
            Debug.Print(CStr(view.RowCount))
            view.Focus()


            'configure buttons based on active row selected in grid
            _StockAdjustmentFixes.GridControlSelectionChangedEvent(_StockAdjustmentCode, AddressOf xgvAdjusts_SelectionChanged, xgdAdjusts, xgvAdjusts)

        End If

    End Sub

    Private Sub cmbTrans_Initialise()

        Dim dt As DataTable = _StockAdjustmentManager.Stock.GetReducedStocksDataTable
        cmbTrans.DisplayMember = "Name"
        cmbTrans.ValueMember = "Number"
        cmbTrans.DataSource = dt

    End Sub

    Private Sub ClearControls()
        txtSku.ResetText()
        txtSkuPrice.ResetText()
        txtSkuStart.ResetText()
        txtSkuAdjust.ResetText()
        txtSkuEnd.ResetText()
        txtComment.ResetText()
        txtDrlNumber.ResetText()
        txtSkuStartValue.ResetText()
        txtSkuAdjustValue.ResetText()
        txtSkuEndValue.ResetText()
        txtMwoStart.ResetText()
        txtMwoAdj.ResetText()
        txtMwoEnd.ResetText()
        txtTransPrice.ResetText()
        txtTransStart.ResetText()
        txtTransAdj.ResetText()
        txtTransEnd.ResetText()
        txtTransStartValue.ResetText()
        txtTransAdjValue.ResetText()
        txtTransEndValue.ResetText()

        _SpecificStockAdjustment = StockAdjustments.AdjustmentFactory.FactoryGet
        _StockAdjustmentManager = StockAdjustments.StockAdjustmentManagerFactory.FactoryGet
        _StockAdjustmentCollection = StockAdjustments.AdjustmentsFactory.FactoryGet

        _CreateMarkdown = False
        _CreateWriteOff = False
        pnlMwo.Visible = False
        _CancelSelected = False
    End Sub

    Private Sub CalculateMarkdownWriteOff()

        If pnlMwo.Visible = True Then
            Dim stockLog As StockAdjustments.IStockLog = StockAdjustments.StockLogFactory.FactoryGet
            stockLog.ReadStockLog(txtSku.Text)

            If _StockAdjustmentCode.MarkdownWriteoff.Substring(0, 1) = "M" Then
                lblMwo.Text = "Markdown Qty"
                txtMwoStart.EditValue = (New StockLogMarkDownWriteOffInitialValuesFactory).GetImplementation.GetInitialMarkDownValue(stockLog)
            Else
                lblMwo.Text = "Write Off Qty"
                txtMwoStart.Text = CStr((New StockLogMarkDownWriteOffInitialValuesFactory).GetImplementation.GetInitialWriteOffValue(stockLog))
            End If
            txtMwoAdj.EditValue = CInt(txtSkuAdjust.EditValue) * -1
            txtMwoEnd.Text = CStr(CInt(txtMwoStart.EditValue) + CInt(txtMwoAdj.EditValue))
        Else
            lblMwo.Text = "Markdown Qty"
            txtMwoStart.ResetText()
            txtMwoAdj.ResetText()
            txtMwoEnd.ResetText()
        End If

    End Sub

    Private Function AuthorisationOK() As Boolean

        Try
            'check whether authorisation required for this code and request password if true
            If _StockAdjustmentCode.IsAuthRequired Then
                Dim adjustForm As New Authorisation.Adjustment
                Dim Store As Integer = CInt(_StockAdjustmentManager.GetStoreNumber)

                adjustForm.txtStore.Text = Store.ToString
                adjustForm.txtSku.Text = txtSku.Text
                adjustForm.txtQty.Text = CStr(txtSkuAdjust.EditValue)

                Dim adjustHost As New Authorisation.StoreForm(adjustForm, _StockAdjustmentCode.IsPassRequired)
                Dim result As DialogResult = adjustHost.ShowDialog(Me)

                txtComment.Text = adjustForm.txtPO.Text & Space(1) & adjustForm.txtDepot.Text & Space(1) & adjustForm.txtContainer.Text.PadLeft(9, "0"c)
                adjustHost.Dispose()

                If result <> DialogResult.OK Then
                    Return False
                Else
                    Return True
                End If
            End If

            Return True

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function AdjustmentConfirmed() As Boolean

        Try
            Dim confirmed As Boolean = False

            If Not confirmed Then
                Dim result As DialogResult = MessageBox.Show(My.Resources.Confirmations.ConfirmAdjustment, Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                Select Case result
                    Case DialogResult.Yes : Return True
                    Case Else : Return False
                End Select
            End If

            Return True

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

#Region "ISkuNumberAndDescriptionContainer"

    Function GetSkuNumberText() As String Implements ISkuNumberAndDescriptionContainer.GetSkuNumberText
        Return CStr(txtSku.EditValue)
    End Function

    ''' <summary>
    ''' Set the description text and style
    ''' </summary>
    Sub SetSkuDescriptionText(ByVal text As String, ByVal skuDescriptionStyle As SkuDescriptionStyle) Implements ISkuNumberAndDescriptionContainer.SetSkuDescriptionText

        Select Case skuDescriptionStyle
            Case StockAdjustments.SkuDescriptionStyle.SkuNumberIsIncorrect
                txtSkuDescription.ForeColor = System.Drawing.Color.Red
            Case StockAdjustments.SkuDescriptionStyle.SkuNumberIsNotEntered
                txtSkuDescription.ForeColor = System.Drawing.Color.DarkGray
            Case StockAdjustments.SkuDescriptionStyle.SkuNumberIsCorrect
                txtSkuDescription.ForeColor = System.Drawing.Color.Black
            Case Else
                ' This is unexpected situation. Treat all unknown styles as correct. 
                txtSkuDescription.ForeColor = System.Drawing.Color.Black
        End Select

        txtSkuDescription.EditValue = text
    End Sub

#End Region

End Class