﻿Public Class ClearButtonsFactory
    Inherits BaseFactory(Of IClearButtons)

    Public Overrides Function Implementation() As IClearButtons

        Return New ClearButtons

    End Function

End Class