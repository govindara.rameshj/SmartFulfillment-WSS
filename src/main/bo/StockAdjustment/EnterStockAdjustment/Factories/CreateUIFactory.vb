﻿Option Strict On

Public Class CreateUIFactory
    Inherits RequirementSwitchFactory(Of ICreateUI)

    Private Const _RequirementSwitch901 As Integer = -901

    Public Overrides Function ImplementationA() As ICreateUI

        Return New CreateUICodeFourAdjustments

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As TpWickes.Library.IRequirementRepository = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitch901)

    End Function

    Public Overrides Function ImplementationB() As ICreateUI

        Return New CreateUIExisting

    End Function

End Class