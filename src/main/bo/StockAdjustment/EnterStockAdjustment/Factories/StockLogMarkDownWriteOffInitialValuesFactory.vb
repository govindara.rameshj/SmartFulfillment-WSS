﻿Option Strict On
Option Explicit On

Public Class StockLogMarkDownWriteOffInitialValuesFactory
    Inherits BaseFactory(Of IStockLogMarkDownWriteOffInitialValues)

    Public Overrides Function Implementation() As IStockLogMarkDownWriteOffInitialValues

        Return New StockLogMarkDownWriteOffInitialValuesUseEndValue()

    End Function

End Class
