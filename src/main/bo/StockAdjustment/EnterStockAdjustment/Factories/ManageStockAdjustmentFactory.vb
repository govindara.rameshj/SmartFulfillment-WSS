﻿Public Class ManageStockAdjustmentFactory

    Inherits RequirementSwitchFactory(Of IManageStockAdjustments)

    Public Overrides Function ImplementationA() As IManageStockAdjustments
        Return New ManageStockAdjustments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As TpWickes.Library.IRequirementRepository = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-909)
    End Function

    Public Overrides Function ImplementationB() As IManageStockAdjustments
        Return New ManageStockAdjustmentsOld
    End Function

End Class
