﻿Public Class ManageStockAdjustments

    Implements IManageStockAdjustments

    Private SACollection As IAdjustments = AdjustmentsFactory.FactoryGet

    Public Function IsThisTheFirstStockAdjustmentForSkuToday(ByVal StockAdjustments As StockAdjustments.IAdjustments, ByVal Adjustment As StockAdjustments.IAdjustment, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As Boolean Implements IManageStockAdjustments.IsThisTheFirstStockAdjustmentForSkuToday
        Dim result As Boolean = False

        If StockAdjustments.skuCount = 1 Then result = True
        Return result
    End Function

    Public Function PrePopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As StockAdjustments.IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As StockAdjustments.IAdjustments Implements IManageStockAdjustments.PrePopulateReadStockAdjustmentsForSkuToday
        SACollection = StockAdjustments.AdjustmentsFactory.FactoryGet
        SACollection.ReadAdjustments(Skun, AdjustmentDate, CodeNumber)
        Return SACollection
    End Function

    Public Function PostPopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As StockAdjustments.IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As StockAdjustments.IAdjustments Implements IManageStockAdjustments.PostPopulateReadStockAdjustmentsForSkuToday
        Return Adjustments
    End Function

End Class
