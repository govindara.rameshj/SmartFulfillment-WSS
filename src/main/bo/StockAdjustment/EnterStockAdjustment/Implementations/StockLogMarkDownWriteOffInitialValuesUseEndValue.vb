﻿Option Strict On
Option Explicit On

Public Class StockLogMarkDownWriteOffInitialValuesUseEndValue
    Implements IStockLogMarkDownWriteOffInitialValues

    Public Function GetInitialMarkDownValue(ByVal FromStockLog As StockAdjustments.IStockLog) As Decimal Implements IStockLogMarkDownWriteOffInitialValues.GetInitialMarkDownValue

        If FromStockLog IsNot Nothing AndAlso FromStockLog.StockLogId_TKEY <> 0 Then
            Return FromStockLog.EndingMarkDown_EMDN
        Else
            Return 0D
        End If
    End Function

    Public Function GetInitialWriteOffValue(ByVal FromStockLog As StockAdjustments.IStockLog) As Decimal Implements IStockLogMarkDownWriteOffInitialValues.GetInitialWriteOffValue

        If FromStockLog IsNot Nothing AndAlso FromStockLog.StockLogId_TKEY <> 0 Then
            Return FromStockLog.EndingWriteOff_EWTF
        Else
            Return 0D
        End If
    End Function
End Class
