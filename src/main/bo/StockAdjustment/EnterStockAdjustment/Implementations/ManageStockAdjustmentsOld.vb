﻿Public Class ManageStockAdjustmentsOld

    Implements IManageStockAdjustments

    Private SACollection As IAdjustments = AdjustmentsFactory.FactoryGet

    Public Function IsThisTheFirstStockAdjustmentForSkuToday(ByVal StockAdjustments As StockAdjustments.IAdjustments, ByVal Adjustment As StockAdjustments.IAdjustment, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As Boolean Implements IManageStockAdjustments.IsThisTheFirstStockAdjustmentForSkuToday

        If Adjustment.SEQN = "00" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PostPopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As StockAdjustments.IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As StockAdjustments.IAdjustments Implements IManageStockAdjustments.PostPopulateReadStockAdjustmentsForSkuToday
        SACollection = StockAdjustments.AdjustmentsFactory.FactoryGet
        SACollection.ReadAdjustments(Skun, AdjustmentDate, CodeNumber)
        Return SACollection
    End Function

    Public Function PrePopulateReadStockAdjustmentsForSkuToday(ByRef Adjustments As StockAdjustments.IAdjustments, ByVal Skun As String, ByVal AdjustmentDate As Date, ByVal CodeNumber As String) As StockAdjustments.IAdjustments Implements IManageStockAdjustments.PrePopulateReadStockAdjustmentsForSkuToday
        Return Adjustments
    End Function

End Class
