﻿Option Strict On

Public Class CreateUIExisting
    Implements ICreateUI

    Public Sub SetControlFocus(ByVal Order As ICreateUI.ControlProcessedOrder, _
                               ByRef StartingStock As TextEdit, _
                               ByRef AdjustmentValue As TextEdit, _
                               ByRef EndingStock As TextEdit, _
                               ByRef Comment As TextEdit) Implements ICreateUI.SetControlFocus

        Select Case Order
            Case ICreateUI.ControlProcessedOrder.Standard
                Select Case True
                    Case StartingStock.Enabled : StartingStock.Focus()
                    Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
                    Case EndingStock.Enabled : EndingStock.Focus()

                End Select

            Case ICreateUI.ControlProcessedOrder.Reverse
                Select Case True
                    Case EndingStock.Enabled : EndingStock.Focus()
                    Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
                    Case StartingStock.Enabled : StartingStock.Focus()

                End Select

        End Select

    End Sub

    Public Sub SetControlValues(ByVal Value As IAdjustment, _
                                ByRef StockPrice As TextEdit, _
                                ByRef StartingStock As TextEdit, _
                                ByRef AdjustmentValue As TextEdit, _
                                ByRef EndingStock As TextEdit) Implements ICreateUI.SetControlValues

        StockPrice.EditValue = Value.Price
        StartingStock.EditValue = Value.StartingStock_SSTK
        AdjustmentValue.EditValue = Value.Quantity_QUAN
        EndingStock.EditValue = Value.StartingStock_SSTK - Value.Quantity_QUAN

    End Sub

    Public Sub EnableAdjustmentValueControl(ByRef AdjustmentValue As TextEdit) Implements ICreateUI.EnableAdjustmentValueControl

        'do nothing

    End Sub

    Public Sub DisableAdjustmentValueControl(ByRef AdjustmentValue As TextEdit) Implements ICreateUI.DisableAdjustmentValueControl

        'do nothing

    End Sub

End Class