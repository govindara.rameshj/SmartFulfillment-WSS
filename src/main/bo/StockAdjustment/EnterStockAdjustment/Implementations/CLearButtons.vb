﻿Public Class ClearButtons
    Implements IClearButtons


    Public Function MakeButtonsInvisible(ByVal Button1 As SimpleButton, ByVal Button2 As SimpleButton) As Boolean Implements IClearButtons.MakeButtonsInvisible

        If Button1.Visible Then
            Button1.Visible = False
        End If

        If Button2.Visible Then
            Button2.Visible = False
        End If

        Return False
    End Function

    Public Function ClearGrid(ByVal Grid As GridControl) As Boolean Implements IClearButtons.ClearGrid
        Grid.Visible = False
        Return True
    End Function
End Class
