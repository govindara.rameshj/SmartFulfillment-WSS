﻿Option Strict On
Option Explicit On

Public Class RequirementRepository
    Implements IRequirementRepository

    Public Function RequirementEnabled(ByVal RequirementID As Integer) As Boolean? Implements IRequirementRepository.RequirementEnabled

        Return Parameter.GetBoolean(RequirementID)
    End Function
End Class