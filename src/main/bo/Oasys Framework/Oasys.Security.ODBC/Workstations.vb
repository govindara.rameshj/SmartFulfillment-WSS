﻿Public Class Workstations
    Implements IDisposable
    Private _oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)

    Public Sub New()

    End Sub

    Public Enum Column
        Id
        Description
        Display
    End Enum

    ''' <summary>
    ''' Returns datatable of all workstations
    ''' </summary>
    ''' <param name="withSelect"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetWorkstationsDataTable(ByVal withSelect As Boolean) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn(Column.Id.ToString, GetType(Integer)))
        dt.Columns.Add(New DataColumn(Column.Description.ToString, GetType(String)))
        dt.Columns.Add(New DataColumn(Column.Display.ToString, GetType(String)))
        dt.PrimaryKey = New DataColumn() {dt.Columns(Column.Id.ToString)}

        If withSelect Then
            dt.Rows.Add(0, "", "---Select---")
        End If

        'get all workstations
        Using sysTills As New BOSecurityProfile.cWorkStationConfig(_oasys3db)
            sysTills.LoadWorkstations()
            For Each ws As BOSecurityProfile.cWorkStationConfig In sysTills.Workstations
                Dim dr As DataRow = dt.NewRow
                dr(0) = ws.ID.Value
                dr(1) = ws.Description.Value
                dr(2) = ws.ID.Value & " - " & ws.Description.Value
                dt.Rows.Add(dr)
            Next
        End Using

        Return dt

    End Function

    ''' <summary>
    ''' Returns workstation number and name
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNumberName(ByVal id As Integer) As String

        Using workstation As New BOSecurityProfile.cWorkStationConfig(_oasys3db)
            workstation.LoadWorkstation(id)
            Return workstation.ID.Value & " - " & workstation.Description.Value
        End Using

    End Function

    Public Function MenuItemAccessAllowed(ByRef MenuID As Integer) As Boolean

        'If _MenuAccess Is Nothing Then
        '    Dim cMenuAccessBO As New BOSecurityProfile.cProfileMenuAccess(Oasys3DB)
        '    _MenuAccess = cMenuAccessBO.GetMenuAccess(Me.SecurityProfileID.Value)
        'End If

        'For Each profile As cProfileMenuAccess In _MenuAccess
        '    If profile.MenuConfigID.Value = MenuID Then
        '        Return profile.AccessAllowed.Value
        '    End If
        'Next

        'Return False

    End Function

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _oasys3db.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class
