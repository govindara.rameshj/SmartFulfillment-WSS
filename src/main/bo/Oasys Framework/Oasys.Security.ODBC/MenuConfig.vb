﻿Public Class MenuConfig
    Implements IDisposable
    Private _oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
    Private _menuConfig As BOSecurityProfile.cMenuConfig

    Public Enum MenuTypes
        System = 0
        Executable
        OasysFormOdbc
        MenuGroup
        OasysForm
    End Enum


    Public Property MenuType() As MenuTypes
        Get
            Return CType(_menuConfig.MenuType.Value, MenuTypes)
        End Get
        Set(ByVal value As MenuTypes)
            _menuConfig.MenuType.Value = value
        End Set
    End Property
    Public Property AssemblyName() As String
        Get
            Return _menuConfig.AssemblyName.Value
        End Get
        Set(ByVal value As String)
            _menuConfig.AssemblyName.Value = value
        End Set
    End Property
    Public Property ClassName() As String
        Get
            Return _menuConfig.ClassName.Value
        End Get
        Set(ByVal value As String)
            _menuConfig.ClassName.Value = value
        End Set
    End Property
    Public Property AppName() As String
        Get
            Return _menuConfig.AppName.Value
        End Get
        Set(ByVal value As String)
            _menuConfig.AppName.Value = value
        End Set
    End Property
    Public Property Parameters() As String
        Get
            Return _menuConfig.Parameters.Value
        End Get
        Set(ByVal value As String)
            _menuConfig.Parameters.Value = value
        End Set
    End Property
    Public ReadOnly Property SecurityLevel() As Integer
        Get
            Return _menuConfig.SecurityLevel
        End Get
    End Property
    Public ReadOnly Property ConnectionString() As String
        Get
            Return _menuConfig.ConnectionString
        End Get
    End Property
    Public ReadOnly Property WaitForExit() As Boolean
        Get
            Return _menuConfig.WaitForExit.Value
        End Get
    End Property
    Public ReadOnly Property Timeout() As Integer
        Get
            Return _menuConfig.Timeout.Value
        End Get
    End Property
    Public ReadOnly Property Description() As String
        Get
            Return _menuConfig.Description.Value.Trim
        End Get
    End Property
    Public ReadOnly Property AllowMultiple() As Boolean
        Get
            Return _menuConfig.AllowMultiple.Value
        End Get
    End Property
    Public ReadOnly Property TabName() As String
        Get
            Return _menuConfig.TabName.Value
        End Get
    End Property
    Public ReadOnly Property DoProcessingType() As Integer
        Get
            Return _menuConfig.DoProcessingType.Value
        End Get
    End Property

    Public Sub New()
        _menuConfig = New BOSecurityProfile.cMenuConfig(_oasys3db)
    End Sub
    Public Sub New(ByVal connectionString As String)
        _menuConfig = New BOSecurityProfile.cMenuConfig(connectionString)
    End Sub
    Friend Sub New(ByVal menu As BOSecurityProfile.cMenuConfig)
        _menuConfig = menu
    End Sub

    Public Sub Load(ByVal id As Integer, ByVal securityLevel As Integer)
        _menuConfig.Load(id)
        _menuConfig.SecurityLevel = securityLevel
    End Sub

    ''' <summary>
    ''' Retrieves all the Tabs available under the supplied menu id
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMenuTabs() As List(Of MenuConfig)

        Dim menus As New List(Of MenuConfig)

        Dim configs As List(Of BOSecurityProfile.cMenuConfig) = _menuConfig.GetTabOptions(_menuConfig.ID.Value)
        For Each config As BOSecurityProfile.cMenuConfig In configs
            Dim menu As New MenuConfig(config)
            menus.Add(menu)
        Next

        Return menus

    End Function


#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _oasys3db.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
