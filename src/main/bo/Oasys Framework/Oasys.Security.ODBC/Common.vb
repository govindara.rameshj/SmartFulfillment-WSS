﻿Public Module Common

    ''' <summary>
    ''' Get user number and name
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserNumberAndName(ByVal id As Integer) As String

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using sysUser As New BOSecurityProfile.cSystemUsers(oasys3db)
                Return sysUser.GetUserNameNumber(id)
            End Using
        End Using

    End Function

    ''' <summary>
    ''' Returns whether given user id is a manager
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsUserManager(ByVal id As Integer) As Boolean

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using sysUser As New BOSecurityProfile.cSystemUsers(oasys3db)
                sysUser.LoadUser(id)
                Return sysUser.IsManager.Value
            End Using
        End Using
        Return False

    End Function


    ''' <summary>
    ''' Returns workstation number and name
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetWorkstationNumberAndName(ByVal id As Integer) As String

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using workstation As New BOSecurityProfile.cWorkStationConfig(oasys3db)
                workstation.LoadWorkstation(id)
                Return workstation.ID.Value & " - " & workstation.Description.Value
            End Using
        End Using

    End Function

    ''' <summary>
    ''' Records accessed menu option in activity log and returns app id
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <param name="workstationId"></param>
    ''' <param name="menuconfigId"></param>
    ''' <remarks></remarks>
    Public Function RecordAppOpened(ByVal userId As Integer, ByVal workstationId As Integer, ByVal menuconfigId As Integer) As Integer

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using activityLog As New BOSecurityProfile.cActivityLog(oasys3db)
                activityLog.LogDate.Value = Now.Date
                activityLog.EmployeeID.Value = userId
                activityLog.WorkstationID.Value = workstationId.ToString
                activityLog.MenuOptionID.Value = menuconfigId
                activityLog.LoggedIn.Value = False
                activityLog.ForcedLogOut.Value = False
                activityLog.StartTime.Value = Now.ToString("HHmmss")
                activityLog.EndTime.Value = ""

                activityLog.SaveIfNew()
                Return activityLog.ID.Value
            End Using
        End Using

    End Function

    ''' <summary>
    ''' Records application being closed
    ''' </summary>
    ''' <param name="id"></param>
    ''' <remarks></remarks>
    Public Sub RecordAppClosed(ByVal id As Integer)

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using activityLog As New BOSecurityProfile.cActivityLog(oasys3db)
                activityLog.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, activityLog.ID, id)
                activityLog.LoadMatches()
                activityLog.EndTime.Value = Now.ToString("HHmmss")
                activityLog.SaveIfExists()
            End Using
        End Using

    End Sub

    ''' <summary>
    ''' Records user logging in in activity log
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <param name="workstationId"></param>
    ''' <remarks></remarks>
    Public Sub RecordUserLoggedIn(ByVal userId As Integer, ByVal workStationId As Integer)

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using activityLog As New BOSecurityProfile.cActivityLog(oasys3db)
                activityLog.LogDate.Value = Now.Date
                activityLog.EmployeeID.Value = userId
                activityLog.WorkstationID.Value = workStationId.ToString
                activityLog.MenuOptionID.Value = 0
                activityLog.LoggedIn.Value = True
                activityLog.ForcedLogOut.Value = False
                activityLog.StartTime.Value = Now.ToString("HHmmss") 'TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")
                activityLog.EndTime.Value = ""

                If Not activityLog.SaveIfNew() Then
                    Throw New Exception("User logged in not recorded in activity log")
                End If
            End Using
        End Using

    End Sub

    ''' <summary>
    ''' Records user logging out in activity log
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <param name="workstationId"></param>
    ''' <param name="forcedToLogOut"></param>
    ''' <remarks></remarks>
    Public Sub RecordUserLoggedOut(ByVal userId As String, ByVal workStationId As String, ByVal forcedToLogOut As Boolean)

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using activityLog As New BOSecurityProfile.cActivityLog(oasys3db)
                activityLog.LogDate.Value = Now.Date
                activityLog.EmployeeID.Value = CInt(userId)
                activityLog.WorkstationID.Value = workStationId
                activityLog.MenuOptionID.Value = 0
                activityLog.LoggedIn.Value = False
                activityLog.ForcedLogOut.Value = forcedToLogOut
                activityLog.StartTime.Value = Now.ToString("HHmmss") 'TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")
                activityLog.EndTime.Value = ""

                If Not activityLog.SaveIfNew() Then
                    Throw New Exception("User logged out not recorded in activity log")
                End If
            End Using
        End Using

    End Sub

End Module