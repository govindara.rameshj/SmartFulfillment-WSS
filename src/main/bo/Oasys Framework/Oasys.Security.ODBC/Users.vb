﻿Public Class Users
    Implements IDisposable
    Private _oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)

    Public Enum Column
        Id
        Initials
        Name
        Display
    End Enum

    Public Sub New()

    End Sub

    ''' <summary>
    ''' Returns datatable of system users
    ''' </summary>
    ''' <param name="withSelect"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSystemUsersDataTable(ByVal withSelect As Boolean) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn(Column.Id.ToString, GetType(Integer)))
        dt.Columns.Add(New DataColumn(Column.Initials.ToString, GetType(String)))
        dt.Columns.Add(New DataColumn(Column.Name.ToString, GetType(String)))
        dt.Columns.Add(New DataColumn(Column.Display.ToString, GetType(String)))
        dt.PrimaryKey = New DataColumn() {dt.Columns(Column.Id.ToString)}

        If withSelect Then
            dt.Rows.Add(0, "", "", "---Select---")
        End If

        'get system user information 
        Using sysUsers As New BOSecurityProfile.cSystemUsers(_oasys3db)
            sysUsers.LoadUsers()
            For Each user As BOSecurityProfile.cSystemUsers In sysUsers.Users
                Dim dr As DataRow = dt.NewRow
                dr(0) = user.ID.Value
                dr(1) = user.Initials.Value
                dr(2) = user.Name.Value
                dr(3) = user.ID.Value.ToString("000") & " - " & user.Initials.Value & Space(1) & user.Name.Value
                dt.Rows.Add(dr)
            Next
        End Using

        Return dt

    End Function

    ''' <summary>
    ''' Get user number and name
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNumberAndName(ByVal id As Integer) As String

        Using sysUser As New BOSecurityProfile.cSystemUsers(_oasys3db)
            Return sysUser.GetUserNameNumber(id)
        End Using

    End Function


#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _oasys3db.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class