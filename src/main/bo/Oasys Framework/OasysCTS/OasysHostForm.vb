Public Class OasysHostForm
    Friend CallBack As OasysCTS.App
    Private _menuConfig As Oasys.Security.MenuConfig = Nothing
    Private _ModuleList As Dictionary(Of String, ModuleStruct)
    Private _ModuleInfo As ModuleStruct

    Public Sub New()
        InitializeComponent()
        PCI.FormTools.SetFormToolStripRenderer(Me)
        VersionStatusLabel.Text = String.Concat(My.Resources.Strings.VersionAbbreviation, My.Application.Info.Version.ToString)
    End Sub

    Public Sub New(ByVal menuConfigId As Integer, ByVal UserID As Integer, ByVal WorkstationID As Integer, ByVal securityLevel As Integer)
        InitializeComponent()
        PCI.FormTools.SetFormToolStripRenderer(Me)
        VersionStatusLabel.Text = String.Concat(My.Resources.Strings.VersionAbbreviation, My.Application.Info.Version.ToString)

        'load menuconfig
        _menuConfig = New Oasys.Security.MenuConfig
        _menuConfig.Load(menuConfigId, securityLevel)

        'set security level


        'set menu config and host form properties only for type 2
        Select Case _menuConfig.MenuType
            Case Oasys.Security.MenuConfig.MenuTypes.OasysFormOdbc
                ModuleTabControl.Visible = False
                Dim app As OasysCTS.ModuleTabBase = CType(Activator.CreateInstanceFrom(_menuConfig.AssemblyName, _menuConfig.ClassName, True, Reflection.BindingFlags.Default, Nothing, New Object() {UserID, WorkstationID, _menuConfig.SecurityLevel, _menuConfig.ConnectionString, _menuConfig.Parameters, StatusLabel, StatusProgress}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysCTS.ModuleTabBase)
                app.Initialize()

                'check that minimum width is not less than that of containing panel
                Dim min As Integer = app.MinimumSize.Width - pnlModule.Width + Me.Padding.Left + Me.Padding.Right
                If min > 0 Then
                    Me.Left -= CInt(min / 2)
                    Me.Width += min
                End If

                'check that minimum height is not less than that of containing panel
                min = app.MinimumSize.Height - pnlModule.Height + Me.Padding.Top + Me.Padding.Bottom
                If min > 0 Then
                    Me.Top -= CInt(min / 2)
                    Me.Height += min
                End If

                pnlModule.Controls.Add(app)
                app.Dock = DockStyle.Fill
                app.Visible = True

                Me.Text = _menuConfig.AppName
                Me.Icon = app.Icon
        End Select

    End Sub


    Private Sub Form_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

        'If ModuleTabControl.TabCount = 0 Then

        '    If CType(pnlModule.Controls(0), ModuleTabBase).UserId <> CallBack.UserId Then
        '        'pass back the user id 
        '        CallBack.UserId = CShort(CType(pnlModule.Controls(0), ModuleTabBase).UserId)
        '    End If

        '    CType(pnlModule.Controls(0), ModuleTabBase).Form_Close(sender, e)
        'Else
        '    CType(ModuleTabControl.SelectedTab, ModuleTabPage).Control.Form_Close(sender, e)
        'End If

        If Me.MdiChildren.GetUpperBound(0) <> -1 Then
            MessageBox.Show(My.Resources.Strings.CloseAllForms, CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1, PCI.FormTools.MessageBoxRightToLeft(Me))
            e.Cancel = True
        End If
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If _MenuConfig Is Nothing Then
            Trace.WriteLine(My.Resources.GetMenuConfig, Me.Name)
            'Dim menuConfig As New BOSecurityProfile.cMenuConfig(CallBack.ConnectionString)
            _menuConfig = New Oasys.Security.MenuConfig(CallBack.ConnectionString)

            If CallBack.MenuID <> -1 Then
                _menuConfig.Load(CallBack.MenuID, CallBack.SecurityLevel)
            Else
                _menuConfig.MenuType = Oasys.Security.MenuConfig.MenuTypes.OasysFormOdbc
                _menuConfig.AssemblyName = CallBack.AssemblyName
                _menuConfig.ClassName = CallBack.ClassName
                _menuConfig.Parameters = CallBack.RunParamters
                _menuConfig.AppName = CallBack.ApplicationName
            End If

            Dim RunParameters As String = _menuConfig.Parameters
            If CallBack.RunParamters.Length > 0 Then RunParameters = CallBack.RunParamters
        End If

        Select Case _menuConfig.MenuType
            Case Oasys.Security.MenuConfig.MenuTypes.OasysFormOdbc, Oasys.Security.MenuConfig.MenuTypes.System
                Try
                    ModuleTabControl.Visible = False
                    Dim MainOption As OasysCTS.ModuleTabBase = CType(Activator.CreateInstanceFrom(_menuConfig.AssemblyName, _menuConfig.ClassName, True, Reflection.BindingFlags.Default, Nothing, New Object() {CallBack.UserId, CallBack.WorkstationId, CallBack.SecurityLevel, CallBack.ConnectionString, _menuConfig.Parameters, StatusLabel, StatusProgress}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysCTS.ModuleTabBase)
                    MainOption.Initialize()

                    'check that minimum width is not less than that of containing panel
                    Dim min As Integer = MainOption.MinimumSize.Width - pnlModule.Width + Me.Padding.Left + Me.Padding.Right
                    If min > 0 Then
                        Me.Left -= CInt(min / 2)
                        Me.Width += min
                        Me.Refresh()
                    End If

                    pnlModule.Controls.Add(MainOption)
                    MainOption.Dock = DockStyle.Fill
                    MainOption.Visible = True

                    Me.Text = _menuConfig.AppName
                    Me.Icon = MainOption.Icon

                Catch ex As ModuleLoadTabException
                    MessageBox.Show(ex.Message, _menuConfig.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)

                Catch ex As ModuleLoadFormException
                    If ex.AbortLoad Then
                        Throw
                    Else
                        Select Case MessageBox.Show(ex.Message, _menuConfig.AppName, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2, PCI.FormTools.MessageBoxRightToLeft(Me))
                            Case Windows.Forms.DialogResult.Abort : Throw
                            Case Windows.Forms.DialogResult.Retry : Exit Try
                            Case Windows.Forms.DialogResult.Ignore : Exit Try
                        End Select
                    End If
                End Try

            Case Oasys.Security.MenuConfig.MenuTypes.Executable
                Shell(_menuConfig.AssemblyName & Space(1) & _menuConfig.Parameters, AppWinStyle.NormalFocus, _menuConfig.WaitForExit, _menuConfig.Timeout)
                Me.Close()

            Case Else
                _ModuleList = New Dictionary(Of String, ModuleStruct)
                Dim ModuleName As String = _menuConfig.AppName

                If Not _ModuleList.ContainsKey(ModuleName) Then
                    _ModuleList.Add(ModuleName, New ModuleStruct(ModuleName, _menuConfig.Description, _menuConfig.SecurityLevel, _menuConfig.AllowMultiple))
                End If

                For Each tabOption As Oasys.Security.MenuConfig In _menuConfig.GetMenuTabs
                    Try
                        If tabOption.SecurityLevel <= CallBack.SecurityLevel Then
                            Dim tabName As String = tabOption.TabName
                            _ModuleList(ModuleName).AddTab(tabName, tabOption.Description, tabOption.SecurityLevel, CType(Activator.CreateInstanceFrom(tabOption.AssemblyName, tabOption.ClassName, True, Reflection.BindingFlags.Default, Nothing, New Object() {CallBack.UserId, CallBack.WorkstationId, CallBack.SecurityLevel, CallBack.ConnectionString, _menuConfig.Parameters}, CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, ModuleTabBase), tabOption.DoProcessingType)
                        End If

                    Catch ex As IO.FileNotFoundException
                        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.FileNotFound, tabOption.ClassName, tabOption.AssemblyName, tabOption.AppName), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Catch ex As InvalidCastException
                        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.NotModuleType, tabOption.ClassName, tabOption.AssemblyName, tabOption.AppName), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Catch ex As TypeLoadException
                        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.ClassNotFound, tabOption.ClassName, tabOption.AssemblyName, tabOption.AppName), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Catch ex As MissingMethodException
                        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.NoConstructor, tabOption.ClassName, tabOption.AssemblyName, tabOption.AppName), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Catch ex As Exception
                        EventLog.WriteEntry(CallBack.ApplicationName, ex.ToString, EventLogEntryType.Error)
                        Throw
                    End Try
                Next

                'Dim tabOptionsList As List(Of BOSecurityProfile.cMenuConfig) = menuConfig.GetTabOptions(menuConfig.ID.Value)
                'For Each tabOption As BOSecurityProfile.cMenuConfig In tabOptionsList
                '    Try
                '        If tabOption.SecurityLevel <= CallBack.SecurityLevel Then
                '            Dim tabName As String = tabOption.TabName.Value
                '            _ModuleList(ModuleName).AddTab(tabName, tabOption.Description.Value, tabOption.SecurityLevel, CType(Activator.CreateInstanceFrom(tabOption.AssemblyName.Value, tabOption.ClassName.Value, True, Reflection.BindingFlags.Default, Nothing, New Object() {CallBack.UserId, CallBack.WorkstationId, CallBack.SecurityLevel, CallBack.ConnectionString, menuConfig.Parameters.Value}, CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, ModuleTabBase), tabOption.DoProcessingType.Value)
                '        End If

                '    Catch ex As IO.FileNotFoundException
                '        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.FileNotFound, tabOption.ClassName.Value, tabOption.AssemblyName.Value, tabOption.AppName.Value), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Catch ex As InvalidCastException
                '        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.NotModuleType, tabOption.ClassName.Value, tabOption.AssemblyName.Value, tabOption.AppName.Value), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Catch ex As TypeLoadException
                '        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.ClassNotFound, tabOption.ClassName.Value, tabOption.AssemblyName.Value, tabOption.AppName.Value), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Catch ex As MissingMethodException
                '        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, My.Resources.NoConstructor, tabOption.ClassName.Value, tabOption.AssemblyName.Value, tabOption.AppName.Value), CallBack.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Catch ex As Exception
                '        EventLog.WriteEntry(CallBack.ApplicationName, ex.ToString, EventLogEntryType.Error)
                '        Throw
                '    End Try
                'Next

                DisplayTabs(_ModuleList(ModuleName), CallBack.UserId, CallBack.WorkstationId, CallBack.SecurityLevel, CallBack.ConnectionString)

        End Select

    End Sub

    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        MyBase.OnShown(e)
        Application.DoEvents()
        DoProcessing()
    End Sub


    Public Sub DoProcessing()

        If ModuleTabControl.Visible Then
            'is a tab control
            For Each MainOption As KeyValuePair(Of String, ModuleStruct) In _ModuleList


                For Each oOpt As ModuleTabStruct In MainOption.Value.Tabs
                    If oOpt.DoProcesssingType = 2 Then
                        CType(oOpt.Control, OasysCTS.ModuleTabBase).DoProcessing()
                    End If
                Next
            Next
        Else
            'dropped straight onto the oasys host form
            For Each ctl As Control In pnlModule.Controls
                If TypeOf ctl Is ModuleTabBase Then
                    CType(ctl, OasysCTS.ModuleTabBase).DoProcessing()
                End If
            Next
        End If

    End Sub

    Private Sub DisplayTabs(ByVal moduleInformation As ModuleStruct, ByVal userId As Int32, ByVal workstationId As Int32, ByVal securityLevel As Int32, ByRef TabConnectionString As String)

        _ModuleInfo = moduleInformation
        Refresh()

        For Each tab As ModuleTabStruct In _ModuleInfo.Tabs
            Dim newTab As ModuleTabPage

            Do
                Try
                    newTab = New ModuleTabPage(tab.Control, tab.Name, userId, workstationId, securityLevel, TabConnectionString, "")
                    newTab.Padding = New Padding(6)
                    ModuleTabControl.TabPages.Add(newTab)
                    Me.Controls.Add(newTab.Control)

                    If tab.SecurityLevel > securityLevel Then
                        ModuleTabControl.TabPages(ModuleTabControl.TabPages.Count - 1).Enabled = False
                    End If
                    Exit Do

                Catch ex As ModuleLoadTabException
                    MessageBox.Show(ex.Message, tab.Name, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Do
                Catch ex As ModuleLoadFormException
                    If ex.AbortLoad Then
                        Throw
                    Else
                        Select Case MessageBox.Show(ex.Message, moduleInformation.Name, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2, PCI.FormTools.MessageBoxRightToLeft(Me))
                            Case Windows.Forms.DialogResult.Abort : Throw
                            Case Windows.Forms.DialogResult.Retry : Continue Do
                            Case Windows.Forms.DialogResult.Ignore : Exit Do
                        End Select
                    End If
                End Try
            Loop

        Next

        For Each tab As ModuleTabPage In ModuleTabControl.TabPages
            If tab.Enabled Then
                ModuleTabControl.SelectedTab = tab
                Exit For
            End If
        Next

        If ModuleTabControl.TabPages.Count > 0 Then
            Me.Icon = _ModuleInfo.Tabs(0).Control.Icon
        End If

        Me.Text = _ModuleInfo.Name

    End Sub


    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        If ModuleTabControl.TabCount = 0 Then
            CType(pnlModule.Controls(0), ModuleTabBase).Form_ProcessCmdKey(msg, keyData)
        Else
            CType(ModuleTabControl.SelectedTab, ModuleTabPage).Control.Form_ProcessCmdKey(msg, keyData)
        End If

    End Function
    Private Sub OasysHostForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If ModuleTabControl.TabCount = 0 Then
            CType(pnlModule.Controls(0), ModuleTabBase).Form_KeyDown(sender, e)
        Else
            CType(ModuleTabControl.SelectedTab, ModuleTabPage).Control.Form_KeyDown(sender, e)
        End If

    End Sub
    Private Sub OasysHostForm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If ModuleTabControl.TabCount = 0 Then
            CType(pnlModule.Controls(0), ModuleTabBase).Form_KeyPress(sender, e)
        Else
            CType(ModuleTabControl.SelectedTab, ModuleTabPage).Control.Form_KeyPress(sender, e)
        End If

    End Sub
    Private Sub OasysHostForm_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If ModuleTabControl.TabCount = 0 Then
            CType(pnlModule.Controls(0), ModuleTabBase).Form_KeyUp(sender, e)
        Else
            CType(ModuleTabControl.SelectedTab, ModuleTabPage).Control.Form_KeyUp(sender, e)
        End If

    End Sub
    Private Sub ModuleTabControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ModuleTabControl.SelectedIndexChanged

        Dim tbase As PCI.Controls.TabControl
        Dim tabindex As Integer
        Dim count As Integer = 0

        tbase = CType(sender, PCI.Controls.TabControl)
        tabindex = tbase.SelectedIndex

        For Each MainOption As KeyValuePair(Of String, ModuleStruct) In _ModuleList
            For Each tab As ModuleTabStruct In MainOption.Value.Tabs
                If tabindex = count And tab.Initialised = False And tab.DoProcesssingType = 3 Then
                    CType(tab.Control, OasysCTS.ModuleTabBase).DoProcessing()
                    tab.Initialised = True
                End If
                count = count + 1
            Next
        Next

    End Sub

End Class

Public Class ModuleTabStruct
    Public Control As ModuleTabBase
    Public Name As String
    Public Description As String
    Public SecurityLevel As Integer
    Public DoProcesssingType As Integer
    Public Initialised As Boolean

    Public Sub New(ByVal name As String, ByVal description As String, ByVal securityLevel As Integer, ByRef control As ModuleTabBase, ByVal doProcessingType As Integer)
        Me.Name = name
        Me.Description = description
        Me.SecurityLevel = securityLevel
        Me.Control = control
        Me.DoProcesssingType = doProcessingType
        Me.Initialised = False

    End Sub
End Class

Public Structure ModuleStruct
    Public Name As String
    Public Description As String
    Public SecurityLevel As Integer
    Public AllowMultipleInstances As Boolean
    Public Tabs As List(Of ModuleTabStruct)

    Public Sub New(ByVal name As String, ByVal description As String, ByVal securityLevel As Integer, ByVal allowMultipleInstances As Boolean)
        Me.Name = name
        Me.Description = description
        Me.SecurityLevel = securityLevel
        Me.AllowMultipleInstances = allowMultipleInstances
        Me.Tabs = New List(Of ModuleTabStruct)
    End Sub

    Public Sub AddTab(ByVal name As String, ByVal description As String, ByVal securityLevel As Integer, ByRef control As ModuleTabBase, ByVal DoProcessingType As Integer)
        Tabs.Add(New ModuleTabStruct(name, description, securityLevel, control, DoProcessingType))
    End Sub

End Structure
