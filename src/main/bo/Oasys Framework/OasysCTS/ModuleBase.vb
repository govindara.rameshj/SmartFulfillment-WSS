<System.ComponentModel.ToolboxItem(False)> Public Class ModuleTabBase
    Inherits UserControl
    Private _UserId As Int32
    Private _WorkstationId As Int32
    Private _SecurityLevel As Int32
    Private _Icon As Icon
    Private _ConnectionString As String
    Private _RunParameters As String
    Private _StatusMessage As ToolStripLabel
    Private _StatusProgress As ToolStripProgressBar
    Private _controlkey As Boolean

    Protected ReadOnly Property ConnectionString() As String
        Get
            Return _ConnectionString
        End Get
    End Property
    Protected ReadOnly Property RunParameters() As String
        Get
            Return _RunParameters
        End Get
    End Property
    Public Property UserId() As Int32
        Get
            Return _UserId
        End Get
        Set(ByVal value As Int32)
            _UserId = value
        End Set
    End Property
    Protected ReadOnly Property WorkstationId() As Int32
        Get
            Return _WorkstationId
        End Get
    End Property
    Protected ReadOnly Property SecurityLevel() As Int32
        Get
            Return _SecurityLevel
        End Get
    End Property
    Public Property Icon() As Icon
        Get
            If _Icon Is Nothing Then
                Return New Icon(My.Resources.CTS, 16, 16)
            End If
            Return _Icon
        End Get
        Set(ByVal value As Icon)
            _Icon = value
        End Set
    End Property
    Public Property StatusMessage() As ToolStripLabel
        Get
            Return _StatusMessage
        End Get
        Set(ByVal value As ToolStripLabel)
            _StatusMessage = value
        End Set
    End Property
    Public Property StatusProgress() As ToolStripProgressBar
        Get
            Return _StatusProgress
        End Get
        Set(ByVal value As ToolStripProgressBar)
            _StatusProgress = value
        End Set
    End Property

    Public Sub New()
    End Sub
    Public Sub New(ByVal userId As Int32, ByVal workstationId As Int32, ByVal securityLevel As Int32, ByVal ConnectionString As String, ByVal RunParameters As String, ByRef StatusMessage As ToolStripLabel, ByRef StatusProgress As ToolStripProgressBar)
        MyBase.New()
        _UserId = userId
        _WorkstationId = workstationId
        _SecurityLevel = securityLevel
        _ConnectionString = ConnectionString
        _RunParameters = RunParameters
        _StatusMessage = StatusMessage
        _StatusProgress = StatusProgress
        Visible = False
    End Sub

    Protected Friend Overridable Sub Initialize()
    End Sub
    Protected Friend Overridable Sub DoProcessing()
    End Sub
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.Name = "ModuleTabBase"
        Me.ResumeLayout(False)
    End Sub


    Protected Friend Overridable Sub Form_Close(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)

    End Sub
    Protected Friend Overridable Sub Form_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)

    End Sub
    Protected Friend Overridable Sub Form_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs)

        e.Handled = True
        Select Case e.KeyCode
            Case Keys.ControlKey
                _controlkey = False
            Case Else
                e.Handled = False
        End Select

    End Sub
    Protected Friend Overridable Sub Form_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

    End Sub
    Protected Friend Overridable Function Form_ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

    End Function

    Public Enum DisplayTypes
        None
        IsWarning
        IsError
    End Enum

    ''' <summary>
    ''' Clears status bar
    ''' </summary>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayStatus()

        If _StatusMessage IsNot Nothing Then
            _StatusMessage.Text = String.Empty
            _StatusMessage.ToolTipText = String.Empty
            _StatusMessage.BackColor = Drawing.SystemColors.Control
            _StatusMessage.GetCurrentParent.Refresh()
        End If

    End Sub

    '''' <summary>
    '''' Displays error message in error dialog
    '''' </summary>
    '''' <param name="ex"></param>
    '''' <param name="Title">Dialog header text</param>
    '''' <remarks></remarks>
    'Protected Friend Sub DisplayStatus(ByVal ex As OasysDbException, ByVal Title As String)

    '    MessageBox.Show(ex.Message, Title, MessageBoxButtons.OK, MessageBoxIcon.Error)

    'End Sub

    ''' <summary>
    ''' Displays error message in error dialog
    ''' </summary>
    ''' <param name="ex"></param>
    ''' <param name="Title">Dialog header text</param>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayStatus(ByVal ex As Exception, ByVal Title As String)

        MessageBox.Show(ex.Message, Title, MessageBoxButtons.OK, MessageBoxIcon.Error)

        ''convert to oasys exception and follow routine
        'Dim exo As New OasysDbException(ex.Message, ex.InnerException)
        'DisplayStatus(exo, Title)

    End Sub

    ''' <summary>
    ''' Displays message in status bar
    ''' </summary>
    ''' <param name="message"></param>
    ''' <param name="Type"></param>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayStatus(ByVal message As String, Optional ByVal Type As DisplayTypes = DisplayTypes.None)

        If message IsNot Nothing Then Trace.WriteLine(message, Name)
        If _StatusMessage IsNot Nothing Then

            Select Case Type
                Case DisplayTypes.None
                    _StatusMessage.Text = message
                    _StatusMessage.ToolTipText = message
                    _StatusMessage.BackColor = Drawing.SystemColors.Control

                Case DisplayTypes.IsWarning
                    _StatusMessage.Text = message
                    _StatusMessage.ToolTipText = message
                    _StatusMessage.BackColor = Color.Yellow

                Case DisplayTypes.IsError
                    MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Select
        Else
            _StatusMessage.Text = ""
            _StatusMessage.ToolTipText = ""
            _StatusMessage.BackColor = Drawing.SystemColors.Control
        End If

        _StatusMessage.GetCurrentParent.Refresh()

    End Sub
    Protected Friend Sub DisplayStatus(ByVal message As String, ByVal ex As Exception)

        If ex IsNot Nothing Then
            If ex.InnerException IsNot Nothing Then
                Trace.WriteLine(message & " - " & ex.InnerException.Message, Name)
            Else
                Trace.WriteLine(message & " - " & ex.Message, Name)
            End If
        Else
            Trace.WriteLine(message, Name)
        End If

        MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub

    Protected Friend Sub DisplayProgress()

        If _StatusProgress IsNot Nothing Then
            _StatusProgress.Visible = False
            _StatusProgress.Value = 0

            'reset status label
            _StatusMessage.Text = ""
            _StatusMessage.ToolTipText = ""
            _StatusMessage.BackColor = Drawing.SystemColors.Control
        End If

    End Sub
    Protected Friend Sub DisplayProgress(ByVal percent As Integer, Optional ByVal message As String = "")

        If _StatusProgress IsNot Nothing Then
            _StatusProgress.Visible = True
            _StatusProgress.Value = percent

            If message <> "" Then DisplayStatus(message)
        End If

    End Sub

End Class

Public Class ModuleTabPage
    Inherits PCI.Controls.TabPage
    Private mControl As ModuleTabBase

    Public ReadOnly Property Control() As ModuleTabBase
        Get
            Return mControl
        End Get
    End Property

    Private Sub New()
    End Sub
    Public Sub New(ByVal moduleTab As ModuleTabBase, ByVal text As String, ByVal userId As Int32, ByVal workstationId As Int32, ByVal securityLevel As Int32, ByRef TabConnectionString As String, ByVal TabRunParameters As String)
        MyBase.New()

        If moduleTab Is Nothing Then
            Throw New ArgumentNullException("moduleTab")
        End If

        MyBase.Text = text
        With moduleTab
            Dim myInstance As ModuleTabBase = CType(Activator.CreateInstance(.GetType, New Object() {userId, workstationId, securityLevel, TabConnectionString, TabRunParameters}), ModuleTabBase)
            Dim ctrlArray(myInstance.Controls.Count - 1) As Control
            myInstance.Initialize()
            myInstance.Controls.CopyTo(ctrlArray, 0)
            Controls.AddRange(ctrlArray)
            mControl = myInstance
        End With
    End Sub

End Class

<Serializable()> _
Public Class ModuleLoadFormException
    Inherits Exception
    Private mAbortLoad As Boolean

    Public ReadOnly Property AbortLoad() As Boolean
        Get
            Return mAbortLoad
        End Get
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        info.AddValue("AbortLoad", False, GetType(Boolean))
    End Sub

    Public Sub New(ByVal abortLoad As Boolean)
        MyBase.New()
        mAbortLoad = abortLoad
    End Sub

    Public Sub New(ByVal abortLoad As Boolean, ByVal message As String)
        MyBase.New(message)
        mAbortLoad = abortLoad
    End Sub
End Class

<Serializable()> _
Public Class ModuleLoadTabException
    Inherits Exception
    Private mAbortLoad As Boolean

    Public ReadOnly Property AbortLoad() As Boolean
        Get
            Return mAbortLoad
        End Get
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        info.AddValue("AbortLoad", False, GetType(Boolean))
    End Sub

    Public Sub New(ByVal abortLoad As Boolean)
        MyBase.New()
        mAbortLoad = abortLoad
    End Sub

    Public Sub New(ByVal abortLoad As Boolean, ByVal message As String)
        MyBase.New(message)
        mAbortLoad = abortLoad
    End Sub
End Class

