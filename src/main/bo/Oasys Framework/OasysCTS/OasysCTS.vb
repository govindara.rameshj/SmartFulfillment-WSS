Imports System.Configuration.ConfigurationManager
Public NotInheritable Class App
    Inherits StandardClasses.StandardApp
    Private _AppName As String = String.Empty
    Private _MenuID As Integer = 1
    Private _ConnectionString As String = String.Empty
    Private _RunParameters As String = String.Empty
    Private _ClassName As String = String.Empty
    Private _AssemblyName As String = String.Empty
    Private _RunningFromNight As Boolean = False

    Public Overloads Property UserId() As Short
        Get
            Return MyBase.UserId
        End Get
        Set(ByVal value As Short)
            MyBase.UserId = value
        End Set
    End Property
    Public Overloads ReadOnly Property WorkstationId() As Int16
        Get
            Return MyBase.WorkstationId
        End Get

    End Property
    Public Overloads ReadOnly Property SecurityLevel() As Int32
        Get
            Return MyBase.SecurityLevel
        End Get
    End Property

    Public ReadOnly Property ApplicationName() As String
        Get
            Return _AppName
        End Get
    End Property
    Public ReadOnly Property MenuID() As Integer
        Get
            Return _MenuID
        End Get
    End Property
    Public ReadOnly Property ConnectionString() As String
        Get
            Return _ConnectionString
        End Get
    End Property
    Public ReadOnly Property RunParamters() As String
        Get
            Return _RunParameters
        End Get
    End Property
    Public ReadOnly Property ClassName() As String
        Get
            Return _ClassName
        End Get
    End Property
    Public ReadOnly Property AssemblyName() As String
        Get
            Return _AssemblyName
        End Get
    End Property
    Public ReadOnly Property RunningFromNight() As Boolean
        Get
            Return _RunningFromNight
        End Get
    End Property


    Public Sub New()
        MyBase.New(New OasysHostForm)
    End Sub

    Public Sub New(ByVal menuConfigId As Integer, ByVal UserID As Integer, ByVal WorkstationID As Integer, ByVal securityLevel As Integer)
        MyBase.New(New OasysHostForm(menuConfigId, UserID, WorkstationID, securityLevel))
    End Sub

    Public Overrides Function Show(ByVal parameterDocument As StandardClasses.ParameterDocument) As System.Windows.Forms.DialogResult

        If parameterDocument Is Nothing Then
            Throw New ArgumentNullException("parameterDocument")
        End If

        GetStandardParameters(parameterDocument)
        _AppName = parameterDocument.GetItem("AppName", GetType(String)).ToString
        _ConnectionString = parameterDocument.GetItem("ConnectionString", GetType(String)).ToString
        _RunParameters = parameterDocument.GetItem("RunParameters", GetType(String)).ToString.Trim
        CType(MainForm, OasysHostForm).CallBack = Me
        CType(MainForm, OasysHostForm).Text = _AppName

        'check if menuId is numeric
        Dim strMenuID As String = parameterDocument.GetItem("MenuID", GetType(String)).ToString
        If IsNumeric(strMenuID) Then
            _MenuID = CInt(parameterDocument.GetItem("MenuID", GetType(Integer)))
            _ClassName = String.Empty
        Else
            _MenuID = -1
            Dim strSplit() As String = Split(strMenuID, ",")
            _AssemblyName = strSplit(0)
            _ClassName = strSplit(1)
            _RunningFromNight = True
        End If


        Return MyBase.Show(parameterDocument)

    End Function

    Public Overrides Sub Show(ByVal parameterDocument As StandardClasses.ParameterDocument, ByVal mdiParent As System.Windows.Forms.Form)

        If parameterDocument Is Nothing Then
            Throw New ArgumentNullException("parameterDocument")
        End If

        GetStandardParameters(parameterDocument)
        _AppName = parameterDocument.GetItem("AppName", GetType(String)).ToString
        _ConnectionString = parameterDocument.GetItem("ConnectionString", GetType(String)).ToString
        _RunParameters = parameterDocument.GetItem("RunParameters", GetType(String)).ToString.Trim
        CType(MainForm, OasysHostForm).CallBack = Me
        CType(MainForm, OasysHostForm).Text = _AppName

        'check if menuId is numeric
        Dim strMenuId As String = parameterDocument.GetItem("MenuID", GetType(String)).ToString
        If IsNumeric(strMenuId) Then
            _MenuID = CInt(parameterDocument.GetItem("MenuID", GetType(Integer)))
            _ClassName = String.Empty
        Else
            _MenuID = -1
            Dim strSplit() As String = Split(strMenuId, ",")
            _AssemblyName = strSplit(0)
            _ClassName = strSplit(1)
            _RunningFromNight = True
        End If

        MyBase.Show(parameterDocument, mdiParent)

    End Sub

End Class
