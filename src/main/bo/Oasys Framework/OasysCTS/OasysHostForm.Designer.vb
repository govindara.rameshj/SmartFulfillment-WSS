<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OasysHostForm
    Inherits PCI.Controls.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OasysHostForm))
        Me.ModuleTabControl = New PCI.Controls.TabControl
        Me.pnlToolStrip = New OasysCTS.PanelCustomBorder
        Me.sspToolStrip = New System.Windows.Forms.StatusStrip
        Me.CtsStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.VersionStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.SelectPrinterStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.pnlModule = New OasysCTS.PanelCustomBorder
        Me.StatusProgress = New System.Windows.Forms.ToolStripProgressBar
        Me.pnlToolStrip.SuspendLayout()
        Me.sspToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'ModuleTabControl
        '
        resources.ApplyResources(Me.ModuleTabControl, "ModuleTabControl")
        Me.ModuleTabControl.Name = "ModuleTabControl"
        Me.ModuleTabControl.SelectedIndex = 0
        Me.ModuleTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.ModuleTabControl.TabStop = False
        '
        'pnlToolStrip
        '
        resources.ApplyResources(Me.pnlToolStrip, "pnlToolStrip")
        Me.pnlToolStrip.Controls.Add(Me.sspToolStrip)
        Me.pnlToolStrip.DrawBottom = False
        Me.pnlToolStrip.DrawLeft = True
        Me.pnlToolStrip.DrawRight = True
        Me.pnlToolStrip.DrawTop = False
        Me.pnlToolStrip.Name = "pnlToolStrip"
        '
        'sspToolStrip
        '
        resources.ApplyResources(Me.sspToolStrip, "sspToolStrip")
        Me.sspToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CtsStatusLabel, Me.VersionStatusLabel, Me.StatusLabel, Me.SelectPrinterStatusLabel, Me.StatusProgress})
        Me.sspToolStrip.Name = "sspToolStrip"
        Me.sspToolStrip.SizingGrip = False
        '
        'CtsStatusLabel
        '
        Me.CtsStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.CtsStatusLabel.Margin = New System.Windows.Forms.Padding(-1, 3, 2, -1)
        Me.CtsStatusLabel.Name = "CtsStatusLabel"
        resources.ApplyResources(Me.CtsStatusLabel, "CtsStatusLabel")
        '
        'VersionStatusLabel
        '
        Me.VersionStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.VersionStatusLabel.Margin = New System.Windows.Forms.Padding(-1, 3, 2, -1)
        Me.VersionStatusLabel.Name = "VersionStatusLabel"
        resources.ApplyResources(Me.VersionStatusLabel, "VersionStatusLabel")
        '
        'StatusLabel
        '
        Me.StatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.StatusLabel.Margin = New System.Windows.Forms.Padding(-1, 3, 2, -1)
        Me.StatusLabel.Name = "StatusLabel"
        resources.ApplyResources(Me.StatusLabel, "StatusLabel")
        Me.StatusLabel.Spring = True
        '
        'SelectPrinterStatusLabel
        '
        Me.SelectPrinterStatusLabel.Name = "SelectPrinterStatusLabel"
        resources.ApplyResources(Me.SelectPrinterStatusLabel, "SelectPrinterStatusLabel")
        '
        'pnlModule
        '
        resources.ApplyResources(Me.pnlModule, "pnlModule")
        Me.pnlModule.DrawBottom = False
        Me.pnlModule.DrawLeft = True
        Me.pnlModule.DrawRight = True
        Me.pnlModule.DrawTop = True
        Me.pnlModule.Name = "pnlModule"
        '
        'StatusProgress
        '
        Me.StatusProgress.Name = "StatusProgress"
        resources.ApplyResources(Me.StatusProgress, "StatusProgress")
        '
        'OasysHostForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnlToolStrip)
        Me.Controls.Add(Me.pnlModule)
        Me.Controls.Add(Me.ModuleTabControl)
        Me.KeyPreview = True
        Me.Name = "OasysHostForm"
        Me.pnlToolStrip.ResumeLayout(False)
        Me.sspToolStrip.ResumeLayout(False)
        Me.sspToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sspToolStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents CtsStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents VersionStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ModuleTabControl As PCI.Controls.TabControl
    Friend WithEvents SelectPrinterStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents pnlToolStrip As OasysCTS.PanelCustomBorder
    Friend WithEvents pnlModule As OasysCTS.PanelCustomBorder
    Friend WithEvents StatusProgress As System.Windows.Forms.ToolStripProgressBar

End Class
