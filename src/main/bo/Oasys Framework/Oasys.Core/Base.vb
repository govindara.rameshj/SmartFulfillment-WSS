﻿Imports System.Reflection

<CLSCompliant(True)> Public MustInherit Class Base
    Implements IDisposable

    Public Sub New()
    End Sub

    Public Sub New(ByVal dr As DataRow)

        For Each propInfo As PropertyInfo In Me.GetType.GetProperties((BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance))

            'check if any custom attributes present
            Dim mappingAttributes As Object() = propInfo.GetCustomAttributes(GetType(ColumnMappingAttribute), True)
            If mappingAttributes.Count > 0 Then
                'check that columnname attribute exists
                For Each mappingAttribute As Object In mappingAttributes
                    If TypeOf mappingAttribute Is ColumnMappingAttribute Then
                        Dim mappingName As String = CType(mappingAttribute, ColumnMappingAttribute).ColumnName

                        'check that column exists in datarow and not null and set property value
                        If dr.Table.Columns.Contains(mappingName) AndAlso Not IsDBNull(dr(mappingName)) Then
                            propInfo.SetValue(Me, dr(mappingName), Nothing)
                        End If
                    End If
                Next
            End If
        Next

    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class
