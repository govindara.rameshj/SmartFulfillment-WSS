﻿<AttributeUsage(AttributeTargets.Property, allowmultiple:=False)> Public Class ColumnMappingAttribute
    Inherits Attribute
    Private _columnName As String = String.Empty

    Public Sub New(ByVal columnName As String)
        _columnName = columnName
    End Sub

    Public Property ColumnName() As String
        Get
            Return _columnName
        End Get
        Set(ByVal value As String)
            _columnName = value
        End Set
    End Property

End Class