﻿<Serializable()> Public Class DBField
    Private _ColumnName As String = String.Empty
    Private _ColumnValue As Object = Nothing
    Private _ColumnValues() As Object = {}
    Private _ColumnType As ColumnTypes = ColumnTypes.[String]
    Private _NumValues As Integer = 0
    Private _FieldID As Long = 0
    Private _Operator As Integer = 0

    Public Sub New(ByVal DBColumnName As String, ByVal ColumnValue As Object, ByVal ColumnTypeCode As ColumnTypes)
        _ColumnName = DBColumnName
        _ColumnValue = ColumnValue
        _ColumnType = ColumnTypeCode
        _NumValues = 1
    End Sub

    Public Property FieldID() As Long
        Get
            Return _FieldID
        End Get
        Set(ByVal value As Long)
            _FieldID = value
        End Set
    End Property

    Public Property DBValue() As Object
        Get
            If (_NumValues > 1) Then
                Return _ColumnValues
            Else
                Return _ColumnValue
            End If
        End Get
        Set(ByVal value As Object)
            _ColumnValue = value
        End Set
    End Property

    Public Property ColumnName() As String
        Get
            Return _ColumnName
        End Get
        Set(ByVal value As String)
            _ColumnName = value
        End Set
    End Property

    Public Property NoValues() As Integer
        Get
            Return _NumValues
        End Get
        Set(ByVal value As Integer)
            _NumValues = value
        End Set
    End Property

    Public Property DBOperator() As Integer
        Get
            Return _Operator
        End Get
        Set(ByVal value As Integer)
            _Operator = value
        End Set
    End Property

End Class
