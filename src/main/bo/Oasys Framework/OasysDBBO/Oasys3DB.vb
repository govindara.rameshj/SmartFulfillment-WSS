﻿Imports System.Data.SqlClient
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core

Namespace Oasys3.DB
    <Serializable()> Public Class clsOasys3DB
        Implements IDisposable
        Public Enum eOrderByType
            Ascending = 0
            Descending
        End Enum
        Public Enum eOperator
            pAnd = 0
            pOr
            pGreaterThan
            pLessThan
            pEquals
            pGreaterThanOrEquals
            pLessThanOrEquals
            pLike
            pIs
            pNotEquals
            pIn
            pNotLike
            pNotIn
            pLikeNoCase
            pLikeNoCaseBegins
            pLikeNoCaseEnds
            pLikeBegins
            pLikeEnds
            pEqualsNoCase
            pInNoCase
            pNotInNoCase
        End Enum
        Public Enum eAggregates
            pAggSum = 0
            pAggCount
            pAggMin
            pAggMax
            pAggList
            pAggDistinct
        End Enum
        Public Enum eSqlQueryType
            pSelect = 0
            pInsert
            pUpdate
            pDelete
        End Enum
        Public Enum Trims
            Left
            Right
        End Enum

        Public ReadOnly Property OdbcTransaction() As Odbc.OdbcTransaction
            Get
                Return _OdbcTransaction
            End Get
        End Property

        <NonSerialized()> Private _OdbcConnection As New Odbc.OdbcConnection
        <NonSerialized()> Private _OdbcCommand As New Odbc.OdbcCommand
        <NonSerialized()> Private _OdbcTransaction As Odbc.OdbcTransaction

        Private _SqlServerConnectionString As String = String.Empty
        Private _SqlServer As Oasys3DbSqlServer = Nothing

        Private _ConnectionString As String = String.Empty
        Private _DatabaseType As String = String.Empty
        Private _IsTransaction As Boolean
        Private _InsertTableName As String = String.Empty
        Private _UpdateTableName As String = String.Empty
        Private _DeleteTableName As String = String.Empty

        Private _FromSources As ArrayList
        Private _Columns As List(Of String)
        Private _ColumnValues As ArrayList
        Private _ColumnNames As List(Of String)
        Private _ColumnAggregate As List(Of eAggregates)
        Private _ColumnDefaults As ArrayList
        Private _WhereColumns As List(Of String)
        Private _WhereComparison As New List(Of eOperator)
        Private _WhereValues As ArrayList
        Private _WhereJoins As New List(Of eOperator)
        Private _OrderByColumns As ArrayList
        Private _OrderByValues As New List(Of eOrderByType)

        Public ReadOnly Property WhereColumns() As List(Of String)
            Get
                Return _WhereColumns
            End Get
        End Property

        Public ReadOnly Property WhereComparison() As List(Of eOperator)
            Get
                Return _WhereComparison
            End Get
        End Property

        Public ReadOnly Property WhereValues() As ArrayList
            Get
                Return _WhereValues
            End Get
        End Property

        Public ReadOnly Property WhereJoins() As List(Of eOperator)
            Get
                Return _WhereJoins
            End Get
        End Property

        Public ReadOnly Property SqlServerConnectionString() As String
            Get
                Return _SqlServerConnectionString
            End Get
        End Property
        Public ReadOnly Property SqlServer() As Oasys3DbSqlServer
            Get
                If _SqlServer Is Nothing Then
                    If _SqlServerConnectionString = String.Empty Then
                        Throw New OasysDbException(My.Resources.Errors.SqlServerNoConnectionString)
                    Else
                        _SqlServer = New Oasys3DbSqlServer(_SqlServerConnectionString)
                    End If
                End If
                Return _SqlServer
            End Get
        End Property
        Public Property ConnectionString() As String
            Get
                ConnectionString = _ConnectionString
            End Get
            Set(ByVal Value As String)
                _ConnectionString = Value
            End Set
        End Property
        Public Property ConnectionKey() As String
            Get
                ConnectionKey = _ConnectionString
            End Get
            Set(ByVal Value As String)
                LoadConnectionFromConfig(Value)
            End Set
        End Property
        Public ReadOnly Property Connection() As Odbc.OdbcConnection
            Get
                Return _OdbcConnection
            End Get
        End Property
        Public ReadOnly Property DatabaseType() As String
            Get
                DatabaseType = _DatabaseType
            End Get
        End Property
        Public ReadOnly Property Transaction() As Boolean
            Get
                Transaction = _IsTransaction
            End Get
        End Property


        Public Sub New()
        End Sub
        Public Sub New(ByVal CommandTimeOut As Integer)

            LoadConnectionFromConfig()
            If _ConnectionString <> String.Empty Then
                OdbcConnectionSetup(CommandTimeOut)
            End If

        End Sub
        Public Sub New(ByVal DsnName As String, ByVal CommandTimeOut As Integer)

            LoadConnectionFromConfig(DsnName)
            If _ConnectionString <> String.Empty Then
                OdbcConnectionSetup(CommandTimeOut)
            End If

        End Sub

        Private Sub LoadConnectionFromConfig(Optional ByVal configKey As String = "default")

            If GlobalVars.IsTestEnvironment Then
                Dim env = GlobalVars.SystemEnvironment
                _SqlServerConnectionString = env.GetOasysSqlServerConnectionString()
                _ConnectionString = env.GetOasysOdbcConnectionString()
                _DatabaseType = env.GetOasysOdbcDatabaseType()
            Else
                Try
                    Trace.WriteLine("Get Connection String", Me.GetType.ToString)

                    'loadig config file
                    Dim resolver = New ConfigPathResolver()
                    Dim doc As XmlDocument = New XmlDocument()
                    doc.Load(resolver.ResolveConnectionXml())
                    Trace.WriteLine("Get Connection String: retrieving from " & doc.Value, Me.GetType.ToString)

                    'search for odbc connection string
                    For Each node As XmlNode In doc.SelectNodes("configuration/connectionStrings/string")
                        If node.Attributes.GetNamedItem("name").Value.ToLower = configKey.ToLower Then
                            _ConnectionString = node.Attributes.GetNamedItem("connectionString").Value
                            _DatabaseType = node.Attributes.GetNamedItem("DatabaseType").Value

                            Trace.WriteLine("Get Connection String (ODBC) " & _ConnectionString, Me.GetType.ToString)
                        End If
                    Next

                    'search for sql connection string
                    For Each node As XmlNode In doc.SelectNodes("configuration/connectionStrings/string")
                        If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then

                            _SqlServerConnectionString = node.Attributes.GetNamedItem("connectionString").Value
                            Trace.WriteLine("Get Connection String: (Sql Server) " & _SqlServerConnectionString, Me.GetType.ToString)

                        End If
                    Next

                Catch ex As System.IO.FileNotFoundException
                    Throw New OasysDbException(My.Resources.Errors.ConfigNotFound, ex)
                Catch ex As Exception
                    Throw New OasysDbException(My.Resources.Errors.ConnectionLoad, ex)
                End Try
            End If
        End Sub
        Private Sub OdbcConnectionSetup(ByVal timeOut As Integer)

            Try
                Trace.WriteLine("Odbc Connection Setup", Me.GetType.ToString)
                _OdbcConnection.ConnectionString = _ConnectionString
                _OdbcCommand.Connection = _OdbcConnection
                _OdbcCommand.CommandTimeout = timeOut

                Trace.WriteLine("Odbc Connection Setup: Testing", Me.GetType.ToString)
                If _OdbcConnection.State = ConnectionState.Closed Then
                    _OdbcConnection.Open()
                    _OdbcConnection.Close()
                    Trace.WriteLine("Odbc Connection Setup: Success", Me.GetType.ToString)
                End If

            Catch ex As Odbc.OdbcException
                Throw New OasysDbException(My.Resources.Errors.OdbcConnectionError, ex)

            Catch ex As Exception
                Throw New OasysDbException(My.Resources.Errors.OdbcConnectionError, ex)
            End Try

        End Sub


        Public Function GetConfigValue(ByVal strConfigLabel As String) As String
            If GlobalVars.IsTestEnvironment Then
                Dim env = GlobalVars.SystemEnvironment
                If String.Compare(strConfigLabel, "sqlConnection", True) = 0 Then
                    Return env.GetOasysSqlServerConnectionString()
                ElseIf String.Compare(strConfigLabel, "ProxyAssembly", True) = 0 Then
                    Return env.GetProxyAssemblyName()
                ElseIf String.Compare(strConfigLabel, "ProxyTypeName", True) = 0 Then
                    Return env.GetProxyTypeName()
                Else
                    Throw New OasysDbException("Config value for [" + strConfigLabel + "] was not found in TestEnviroment")
                End If

            Else
                Dim dsConfig As New DataSet

                Try
                    GetConfigValue = ""

                    Dim resolver = New ConfigPathResolver()
                    dsConfig.ReadXml(resolver.ResolveConnectionXml())

                    For Each row As DataRow In dsConfig.Tables("string").Rows
                        If (String.Compare(row.Item("name").ToString, strConfigLabel, True) = 0) Then
                            If String.Compare(strConfigLabel, "sqlConnection", True) = 0 Then

                                Return row.Item("connectionString").ToString
                            Else
                                Return row.Item("value").ToString
                            End If

                        End If
                    Next

                Catch ex As XmlException
                    Throw New OasysDbException("OASYS3DB (XML exception) " & ex.ToString)

                Catch ex As FileNotFoundException
                    Throw New OasysDbException("OASYS3DB (Config file not found) " & ex.Message)

                Catch ex As Exception
                    Throw New OasysDbException("OASYS3DB " & ex.ToString)

                End Try
            End If

        End Function


        Public Sub ClearAllParameters()

            ClearTableParameters()
            ClearColumnValueParameters()
            ClearWhereParameters()
            ClearWhereJoinParameters()
            ClearOrderByParameters()
            ClearColumnParameters()
            ClearAggregateParameters()

        End Sub
        Public Sub ClearTableParameters()

            If _FromSources IsNot Nothing Then _FromSources.Clear()
            _InsertTableName = String.Empty
            _UpdateTableName = String.Empty
            _DeleteTableName = String.Empty

        End Sub
        Public Sub ClearColumnValueParameters()

            If _ColumnValues IsNot Nothing Then _ColumnValues.Clear()

        End Sub
        Public Sub ClearAggregateParameters()

            If _ColumnAggregate IsNot Nothing Then _ColumnAggregate.Clear()

        End Sub
        Public Sub ClearWhereJoinParameters()

            _WhereJoins.Clear()

        End Sub
        Public Sub ClearWhereParameters()

            If _WhereColumns IsNot Nothing Then _WhereColumns.Clear()
            If _WhereValues IsNot Nothing Then _WhereValues.Clear()
            _WhereComparison.Clear()

        End Sub
        Public Sub ClearOrderByParameters()

            If _OrderByColumns IsNot Nothing Then _OrderByColumns.Clear()
            _OrderByValues.Clear()

        End Sub
        Public Sub ClearColumnParameters()

            If _ColumnAggregate IsNot Nothing Then _ColumnAggregate.Clear()
            If _Columns IsNot Nothing Then _Columns.Clear()
            If _ColumnDefaults IsNot Nothing Then _ColumnDefaults.Clear()
            If _ColumnNames IsNot Nothing Then _ColumnNames.Clear()

        End Sub


        Public Sub BeginTransaction()

            If _OdbcConnection.State = ConnectionState.Closed Then
                _OdbcConnection.Open()
            End If
            _OdbcTransaction = _OdbcConnection.BeginTransaction
            _OdbcCommand.Transaction = _OdbcTransaction
            _IsTransaction = True

        End Sub
        Public Sub CommitTransaction()

            _OdbcTransaction.Commit()
            _IsTransaction = False

        End Sub
        Public Sub RollBackTransaction()

            _OdbcTransaction.Rollback()
            _IsTransaction = False

        End Sub


        Public Sub SetTableParameter(ByVal TableName As String, ByVal [Query] As eSqlQueryType)

            Select Case Query
                Case eSqlQueryType.pInsert : _InsertTableName = TableName
                Case eSqlQueryType.pDelete : _DeleteTableName = TableName
                Case eSqlQueryType.pUpdate : _UpdateTableName = TableName
                Case eSqlQueryType.pSelect
                    If _FromSources Is Nothing Then _FromSources = New ArrayList(1)
                    _FromSources.Add(TableName)
            End Select

        End Sub
        Public Sub SetColumnValueParameter(ByVal ColumnValue As Object)

            If _ColumnValues Is Nothing Then _ColumnValues = New ArrayList(1)
            _ColumnValues.Add(ColumnValue)

        End Sub
        Public Sub SetWhereParameter(ByVal Column As String, ByVal [Operator] As eOperator, ByVal Value As Object)

            If _WhereColumns Is Nothing Then _WhereColumns = New List(Of String)(1)
            If _WhereValues Is Nothing Then _WhereValues = New ArrayList(1)

            _WhereColumns.Add("""" & Column & """")
            _WhereComparison.Add([Operator])
            _WhereValues.Add(Value)

        End Sub
        Public Sub SetWhereJoinParameter(ByVal op As eOperator)

            _WhereJoins.Add(op)

        End Sub
        Public Sub SetOrderByParameter(ByVal Column As String, ByVal op As eOrderByType)

            If _OrderByColumns Is Nothing Then _OrderByColumns = New ArrayList(1)
            _OrderByColumns.Add("""" & Column & """")
            _OrderByValues.Add(op)

        End Sub

        Public Sub SetColumnParameter(ByVal Column As String)

            SetColumnParameter(Column, Nothing, "")

        End Sub
        Public Sub SetColumnParameter(ByVal Column As String, ByVal Trim As Trims)

            If _Columns Is Nothing Then _Columns = New List(Of String)(1)
            If _ColumnDefaults Is Nothing Then _ColumnDefaults = New ArrayList(1)
            If _ColumnNames Is Nothing Then _ColumnNames = New List(Of String)(1)

            Select Case Trim
                Case Trims.Left : _Columns.Add("LTRIM(""" & Column & """) AS """ & Column & """")
                Case Trims.Right : _Columns.Add("RTRIM(""" & Column & """) AS """ & Column & """")
            End Select
            _ColumnDefaults.Add(Nothing)
            _ColumnNames.Add("")

        End Sub
        Public Sub SetColumnParameter(ByVal Column As String, ByVal DefaultValue As Object, ByVal CustomName As String)

            If _Columns Is Nothing Then _Columns = New List(Of String)(1)
            If _ColumnDefaults Is Nothing Then _ColumnDefaults = New ArrayList(1)
            If _ColumnNames Is Nothing Then _ColumnNames = New List(Of String)(1)

            _Columns.Add("""" & Column & """")
            _ColumnDefaults.Add(DefaultValue)
            _ColumnNames.Add(CustomName)

        End Sub

        Public Sub SetColumnAggregate(ByVal AggFunction As eAggregates, ByVal ColumnName As String, ByVal CustomName As String)

            If _Columns Is Nothing Then _Columns = New List(Of String)(1)
            If _ColumnDefaults Is Nothing Then _ColumnDefaults = New ArrayList(1)
            If _ColumnNames Is Nothing Then _ColumnNames = New List(Of String)(1)
            If _ColumnAggregate Is Nothing Then _ColumnAggregate = New List(Of eAggregates)(1)

            _ColumnAggregate.Add(AggFunction)
            _Columns.Add("""" & ColumnName & """")
            _ColumnDefaults.Add(Nothing)
            _ColumnNames.Add(CustomName)

        End Sub
        Public Sub SetColumnAndValueParameter(ByVal Column As String, ByVal ColumnValue As Object)

            SetColumnParameter(Column)
            SetColumnValueParameter(ColumnValue)

        End Sub


        ''' <summary>
        ''' Returns number of rows affected or -1 if unsuccessful
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Insert() As Integer

            Try
                'get category name
                Dim category As String = Me.GetType.ToString & " - " & System.Reflection.MethodBase.GetCurrentMethod.Name

                'do initial checks
                PerformFilterCheck(_InsertTableName)

                'check that connection is ok
                If _OdbcCommand.Connection Is Nothing Then _OdbcCommand.Connection = _OdbcConnection
                If _OdbcConnection.State = ConnectionState.Closed Then _OdbcConnection.Open()

                'create sql query
                Dim sbQuery As New Text.StringBuilder("INSERT INTO " & _InsertTableName)
                Dim sbCols As New Text.StringBuilder(" (")
                Dim sbVals As New Text.StringBuilder(" (")

                For index As Integer = 0 To _Columns.Count - 1
                    sbCols.Append(_Columns(index))
                    sbVals.Append(FormatValue(_ColumnValues(index)))
                    If index <> (_Columns.Count - 1) Then
                        sbCols.Append(", ")
                        sbVals.Append(", ")
                    End If
                Next

                sbQuery.Append(sbCols.ToString & ") VALUES " & sbVals.ToString & ")")
                Trace.WriteLine(sbQuery.ToString, category)

                'perform sql
                _OdbcCommand.CommandText = sbQuery.ToString
                Return _OdbcCommand.ExecuteNonQuery

            Catch ex As OasysDbException
                Throw ex
                Return -1
            Catch ex As Exception
                Throw New OasysDbException(ex.Message)
                Return -1
            Finally
                If _IsTransaction = False Then
                    If _OdbcConnection.State = ConnectionState.Open Then _OdbcConnection.Close()
                End If
            End Try

        End Function

        ''' <summary>
        ''' Returns number of rows affected or -1 if unsuccessful
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Update() As Integer

            Try
                'get category name
                Dim category As String = Me.GetType.ToString & " - " & System.Reflection.MethodBase.GetCurrentMethod.Name

                'do initial checks
                PerformFilterCheck(_UpdateTableName)

                'check that connection is ok
                If _OdbcCommand.Connection Is Nothing Then _OdbcCommand.Connection = _OdbcConnection
                If _OdbcConnection.State = ConnectionState.Closed Then _OdbcConnection.Open()

                'create sql query
                Dim sbQuery As New Text.StringBuilder("UPDATE " & _UpdateTableName & " SET ")

                For index As Integer = 0 To _Columns.Count - 1
                    sbQuery.Append(_Columns(index) & " = " & FormatValue(_ColumnValues(index)))
                    If index <> _Columns.Count - 1 Then sbQuery.Append(", ")
                Next

                If _WhereColumns IsNot Nothing Then
                    If _WhereColumns.Count > 0 Then
                        If _WhereValues Is Nothing Then
                            Throw New OasysDbException(My.Resources.Errors.MismatchWhereColumns)
                        Else
                            If _WhereValues.Count <> _WhereColumns.Count Then
                                Throw New OasysDbException(My.Resources.Errors.MismatchWhereColumnsValues)
                            Else

                                sbQuery.Append(" WHERE ")
                                For index As Integer = 0 To _WhereColumns.Count - 1
                                    Select Case CType(_WhereComparison(index), eOperator)
                                        Case eOperator.pLikeNoCase, eOperator.pLikeNoCaseBegins, eOperator.pLikeNoCaseEnds, eOperator.pInNoCase, eOperator.pNotInNoCase
                                            sbQuery.Append("(UPPER(" & _WhereColumns(index) & ")" & FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index)) + ")")
                                        Case Else
                                            sbQuery.Append("(" & _WhereColumns(index) & (FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index))).Replace("= NULL", "IS NULL") + ")")
                                    End Select
                                    If index <> (_WhereColumns.Count - 1) Then sbQuery.Append(FormatOperator(_WhereJoins(index)))
                                Next

                            End If
                        End If
                    End If
                End If

                'perform sql
                Trace.WriteLine(sbQuery.ToString, category)
                _OdbcCommand.CommandText = sbQuery.ToString
                Return _OdbcCommand.ExecuteNonQuery

            Catch ex As OasysDbException
                Throw ex
                Return -1
            Catch ex As Exception
                Throw New OasysDbException(ex.Message)
                Return -1
            Finally
                If Not _IsTransaction Then
                    If _OdbcConnection.State = ConnectionState.Open Then _OdbcConnection.Close()
                End If
            End Try

        End Function

        ''' <summary>
        ''' Returns number of rows affected or -1 if delete unsuccessful
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Delete() As Integer

            Try
                'get category name
                Dim category As String = Me.GetType.ToString & " - " & System.Reflection.MethodBase.GetCurrentMethod.Name

                If _DeleteTableName.Length = 0 Then
                    Throw New OasysDbException(My.Resources.Errors.NoTableDefined)
                    Return -1
                End If

                'check connection OK
                If _OdbcCommand.Connection Is Nothing Then _OdbcCommand.Connection = _OdbcConnection
                If _OdbcConnection.State = ConnectionState.Closed Then _OdbcConnection.Open()

                'create sql query
                Dim sbQuery As New Text.StringBuilder("DELETE FROM " & _DeleteTableName)

                If Not _WhereColumns Is Nothing Then
                    If _WhereColumns.Count > 0 Then
                        If _WhereValues Is Nothing Then
                            Throw New OasysDbException(My.Resources.Errors.MismatchWhereColumns)
                        Else
                            If _WhereValues.Count <> _WhereColumns.Count Then
                                Throw New OasysDbException(My.Resources.Errors.MismatchWhereColumnsValues)
                            Else
                                sbQuery.Append(" WHERE ")
                                For index As Integer = 0 To _WhereColumns.Count - 1
                                    Select Case CType(_WhereComparison(index), eOperator)
                                        Case eOperator.pLikeNoCase, eOperator.pLikeNoCaseBegins, eOperator.pLikeNoCaseEnds, eOperator.pInNoCase, eOperator.pNotInNoCase
                                            sbQuery.Append("(UPPER(" & _WhereColumns(index) & ")" & FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index)) + ")")
                                        Case Else
                                            sbQuery.Append("(" & _WhereColumns(index) & FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index)) + ")")
                                    End Select
                                    If index <> (_WhereColumns.Count - 1) Then sbQuery.Append(FormatOperator(_WhereJoins(index)))
                                Next
                            End If
                        End If
                    End If
                End If

                Trace.WriteLine(sbQuery.ToString, category)

                _OdbcCommand.CommandText = sbQuery.ToString
                Return _OdbcCommand.ExecuteNonQuery

            Catch ex As Exception
                Trace.WriteLine(ex.Message)
                Throw New OasysDbException(My.Resources.Errors.ProblemDelete, ex)
                Return -1

            Finally
                If _IsTransaction = False Then
                    If _OdbcConnection.State = ConnectionState.Open Then _OdbcConnection.Close()
                End If
            End Try


        End Function

        ''' <summary>
        ''' Returns dataset generated by sql select command as specified by previously defined load fields and filters.
        '''  Throws Oasys exception on error
        ''' </summary>
        ''' <param name="Count">If specified will return the top 'Count' records from database</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Query(Optional ByVal Count As Integer = -1) As DataSet

            Try
                'get category name
                Dim category As String = Me.GetType.ToString & " - " & System.Reflection.MethodBase.GetCurrentMethod.Name

                'check that connection is ok
                If _OdbcCommand.Connection Is Nothing Then
                    _OdbcConnection.ConnectionString = _ConnectionString
                    _OdbcCommand.Connection = _OdbcConnection
                End If
                If _OdbcConnection.State = ConnectionState.Closed Then _OdbcConnection.Open()

                Dim strQuery As String = BuildSql(Count)
                Trace.WriteLine(strQuery, category)
                _OdbcCommand.CommandText = strQuery

                Dim SqlReader As Odbc.OdbcDataReader = _OdbcCommand.ExecuteReader
                Dim ds As New DataSet
                Dim dt As New DataTable
                dt.TableName = "Data"

                'add columns to the datatable from the sql table column names
                For fieldIndex As Integer = 0 To SqlReader.FieldCount - 1
                    dt.Columns.Add(New DataColumn(SqlReader.GetName(fieldIndex)))
                Next

                ' then add the values to each column for each row in the data reader
                While SqlReader.Read
                    Dim dr As DataRow = dt.NewRow
                    For fieldIndex As Integer = 0 To SqlReader.FieldCount - 1
                        dr.Item(fieldIndex) = SqlReader.GetValue(fieldIndex)
                    Next
                    dt.Rows.Add(dr)
                End While

                If SqlReader IsNot Nothing Then
                    If Not SqlReader.IsClosed Then SqlReader.Close()
                End If

                Trace.WriteLine("Rows Returned = " & dt.Rows.Count, category)
                ds.Tables.Add(dt)
                Return ds

            Catch ex As OasysDbException
                Throw ex
            Catch ex As Exception
                Throw New OasysDbException(My.Resources.Errors.ProblemSelect, ex)
            End Try

        End Function
        Public Function BuildSql(Optional ByVal count As Integer = -1) As String

            Try
                Dim blnDefaults As Boolean = False
                Dim blnNames As Boolean = False
                Dim blnOrderBy As Boolean = False
                Dim GroupByString As String = String.Empty

                'check that datasource specified
                If _FromSources.Count = 0 Then
                    Throw New OasysDbException(My.Resources.Errors.NoDatasource)
                End If

                Dim sbQuery As New Text.StringBuilder("SELECT ")
                If count > 0 Then sbQuery.Append("TOP " & count.ToString & Space(1))

                If _ColumnAggregate Is Nothing OrElse _ColumnAggregate.Count = 0 Then

                    If _Columns Is Nothing Then
                        sbQuery.Append("*")
                    Else
                        If Not _ColumnNames Is Nothing Then blnNames = True
                        If Not _ColumnDefaults Is Nothing Then blnDefaults = False 'force the blnDefaults to false as not generating the correct code 

                        '_Columns.TrimToSize()
                        If _Columns.Count <> 0 Then
                            For i As Integer = 0 To _Columns.Count - 1

                                If blnDefaults Then
                                    sbQuery.Append("IF(" + _Columns(i) + " IS NULL, " + FormatValue(_ColumnDefaults(i)) + ", " + _Columns(i) + ")")
                                Else
                                    sbQuery.Append(_Columns(i))
                                End If

                                If blnNames Then
                                    If Not _ColumnNames(i) Is Nothing Then
                                        If _ColumnNames(i).Length <> 0 Then sbQuery.Append(" """ + _ColumnNames(i) + """")
                                    End If
                                End If

                                If i <> (_Columns.Count - 1) Then
                                    sbQuery.Append(", ")
                                End If
                            Next
                        Else
                            sbQuery.Append("*")
                        End If 'columns.count > 0
                    End If 'there are select columns

                Else
                    If _ColumnAggregate.Count <> 0 Then
                        Dim typeString As String = String.Empty
                        For i As Integer = 0 To _ColumnAggregate.Count - 1

                            Select Case _ColumnAggregate(i) 'findtype of agrregate
                                Case eAggregates.pAggCount : typeString = "Count"
                                Case eAggregates.pAggList : typeString = ""
                                Case eAggregates.pAggMax : typeString = "Max"
                                Case eAggregates.pAggMin : typeString = "Min"
                                Case eAggregates.pAggSum : typeString = "Sum"
                                Case eAggregates.pAggDistinct : typeString = "DISTINCT"
                            End Select

                            If typeString = String.Empty Then
                                sbQuery.Append(_Columns(i))
                            Else
                                sbQuery.Append(typeString & "(" & _Columns(i) + ")")
                            End If

                            If _ColumnNames(i) <> String.Empty Then sbQuery.Append(" as " & _ColumnNames(i))
                            If _ColumnAggregate(i) = eAggregates.pAggList Then GroupByString += _Columns(i) & ","
                            If i <> (_ColumnAggregate.Count - 1) Then sbQuery.Append(",")
                        Next

                        'remove last comma 
                        If GroupByString.Length > 0 Then GroupByString = Mid(GroupByString, 1, (Len(GroupByString) - 1))
                    End If
                End If


                sbQuery.Append(" FROM ")
                For index As Integer = 0 To _FromSources.Count - 1
                    sbQuery.Append(_FromSources(index))
                    If index <> (_FromSources.Count - 1) Then sbQuery.Append(", ")
                Next

                If Not _WhereColumns Is Nothing Then
                    If _WhereColumns.Count > 0 Then
                        If _WhereValues Is Nothing Then
                            Throw New OasysDbException("WHERE columns exist without matching values.")
                        Else
                            If _WhereValues.Count <> _WhereColumns.Count Then
                                Throw New OasysDbException("The number of WHERE columns does not match the number of WHERE matching values.")
                            Else

                                sbQuery.Append(" WHERE ")
                                For index As Integer = 0 To _WhereColumns.Count - 1
                                    Select Case CType(_WhereComparison(index), eOperator)
                                        Case eOperator.pLikeNoCase, eOperator.pLikeNoCaseBegins, eOperator.pLikeNoCaseEnds, eOperator.pInNoCase, eOperator.pNotInNoCase
                                            sbQuery.Append("(UPPER(" + _WhereColumns(index) & ")" & FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index)) + ")")
                                        Case Else
                                            sbQuery.Append("(" + _WhereColumns(index) & FormatOperator(_WhereComparison(index)) & FormatValue(_WhereValues(index), _WhereComparison(index)) + ")")
                                    End Select
                                    If index <> (_WhereColumns.Count - 1) Then sbQuery.Append(FormatOperator(_WhereJoins(index)))
                                Next
                                sbQuery.Replace(" = NULL", " IS NULL")
                                sbQuery.Replace(" <> NULL", " IS NOT NULL")

                            End If 'counts <>
                        End If 'where is nothing
                    End If 'where > 0
                End If ' not where is nothing


                If GroupByString > String.Empty Then
                    sbQuery.Append(" group by " & GroupByString)
                End If


                If Not _OrderByColumns Is Nothing Then
                    If _OrderByColumns.Count <> 0 Then
                        If Not _OrderByValues Is Nothing Then
                            If _OrderByValues.Count <> 0 Then
                                blnOrderBy = True
                            End If
                        End If

                        sbQuery.Append(" ORDER BY ")
                        For index As Integer = 0 To _OrderByColumns.Count - 1
                            sbQuery.Append(_OrderByColumns(index))
                            If blnOrderBy Then
                                sbQuery.Append(FormatOrderByType(_OrderByValues(index)))
                            Else
                                sbQuery.Append(FormatOrderByType(eOrderByType.Ascending))
                            End If
                            If index <> (_OrderByColumns.Count - 1) Then sbQuery.Append(", ")
                        Next
                    End If
                End If

                Return sbQuery.ToString

            Catch ex As Exception
                Throw New OasysDbException(My.Resources.Errors.ProblemGeneratingSql, ex)
            End Try

        End Function

        ''' <summary>
        ''' Returns dataset generated by given sql command.
        '''  Throws Oasys exception on error
        ''' </summary>
        ''' <param name="RawQueryString"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ExecuteSql(ByVal RawQueryString As String) As DataSet

            Try
                'get category name
                Dim category As String = Me.GetType.ToString & " - " & System.Reflection.MethodBase.GetCurrentMethod.Name

                If RawQueryString.Length = 0 Then
                    Throw New OasysDbException(My.Resources.Errors.NoQueryString)
                End If

                'check connection
                If _OdbcConnection.State = ConnectionState.Closed Then
                    _OdbcConnection.ConnectionString = _ConnectionString
                    _OdbcConnection.Open()
                End If

                'set sql to odbc connection
                _OdbcCommand.CommandText = RawQueryString
                Trace.WriteLine(RawQueryString, category)

                'perform sql statement
                Dim dt As New DataTable
                Dim sqlReader As Odbc.OdbcDataReader = _OdbcCommand.ExecuteReader()

                'add columns to the datatable from the sql table column names
                For index As Integer = 0 To sqlReader.FieldCount - 1
                    dt.Columns.Add(New DataColumn(sqlReader.GetName(index)))
                Next

                'then add the values to each column for each row in the data reader
                While sqlReader.Read
                    Dim dr As DataRow = dt.NewRow
                    For i As Integer = 0 To sqlReader.FieldCount - 1
                        dr.Item(i) = sqlReader.GetValue(i)
                    Next
                    dt.Rows.Add(dr)
                End While

                If sqlReader IsNot Nothing Then
                    If Not sqlReader.IsClosed Then sqlReader.Close()
                End If

                Trace.WriteLine("Rows Returned = " & dt.Rows.Count, category)

                Dim ds As New DataSet
                ds.Tables.Add(dt)
                ds.Tables(0).TableName = "Data"
                Return ds

            Catch ex As Exception
                Throw New OasysDbException(ex.Message)
            Finally
                If _OdbcConnection.State = ConnectionState.Open Then
                    _OdbcConnection.Close()
                End If
            End Try

        End Function

        ''' <summary>Executes a SqlCommand on the Main DB Connection. Usage: Dim ds As DataSet = ExecuteCMD(CMD)</summary>'''
        ''' <param name="CMD">The command type will be determined based upon whether or not the commandText has a space in it. If it has a space, it is a Text command ("select ... from .."),''' 
        ''' otherwise if there is just one token, it's a stored procedure command</param>''''
        Function ExecuteCMD(ByRef cmd As SqlCommand) As DataSet
            Dim connectionString As String = SharedFunctions.GetConnectionString()
            Dim ds As New DataSet()

            Try
                Dim connection As New SqlConnection(connectionString)
                cmd.Connection = connection

                'Assume that it's a stored procedure command type if there is no space in the command text. Example: "sp_Select_Customer" vs. "select * from Customers"
                If cmd.CommandText.Contains(" ") Then
                    cmd.CommandType = CommandType.Text
                Else
                    cmd.CommandType = CommandType.StoredProcedure
                End If

                Dim adapter As New SqlDataAdapter(cmd)
                adapter.SelectCommand.CommandTimeout = 300

                'fill the dataset'
                adapter.Fill(ds)
                connection.Close()

            Catch ex As Exception
                ' The connection failed. Display an error message.
                Throw New Exception("Database Error: " & ex.Message)
            End Try

            Return ds
        End Function

        ''' <summary>
        ''' Formats given object into correct string representation (taking eOperator into account if given).
        '''  Throws Oasys exception on error
        ''' </summary>
        ''' <param name="varValue"></param>
        ''' <param name="op"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function FormatValue(ByVal varValue As Object, Optional ByVal op As eOperator = Nothing) As String

            Try
                Dim sb As New Text.StringBuilder
                If op <> Nothing Then
                    Select Case op
                        Case eOperator.pIn, eOperator.pNotIn, eOperator.pInNoCase, eOperator.pNotInNoCase : sb.Append("(")
                    End Select
                End If


                Select Case TypeName(varValue)
                    Case "Nothing", "DBNull" : sb.Append("NULL")
                    Case "String"
                        varValue = varValue.ToString.Replace(Chr(0), "")
                        Select Case op
                            Case eOperator.pLike : sb.Append("'%" & varValue.ToString.Replace("'", "''") & "%'")
                            Case eOperator.pLikeBegins : sb.Append("'" & varValue.ToString.Replace("'", "''") & "%'")
                            Case eOperator.pLikeEnds : sb.Append("'%" & varValue.ToString.Replace("'", "''") & "'")
                            Case eOperator.pLikeNoCase : sb.Append("'%" & varValue.ToString.ToUpper.Replace("'", "''") & "%'")
                            Case eOperator.pLikeNoCaseBegins : sb.Append("'" & varValue.ToString.ToUpper.Replace("'", "''") & "%'")
                            Case eOperator.pLikeNoCaseEnds : sb.Append("'%" & varValue.ToString.ToUpper.Replace("'", "''") & "'")
                            Case eOperator.pEqualsNoCase, eOperator.pInNoCase, eOperator.pNotInNoCase
                                sb.Append("'" & varValue.ToString.ToUpper.Replace("'", "''") & "'")
                            Case Else
                                sb.Append("'" & varValue.ToString.Replace("'", "''") & "'")
                        End Select

                    Case "Boolean"
                        If CBool(varValue) Then
                            sb.Append("1")
                        Else
                            sb.Append("0")
                        End If

                    Case "String()"
                        For Each Val As String In CType(varValue, String())
                            sb.Append(FormatValue(Val))
                            sb.Append(",")
                        Next
                        sb.Remove(sb.Length - 1, 1)

                    Case "Date"
                        If (CType(varValue, Date).Year > 1) Then
                            sb.Append("{ts '" + Format(varValue, "yyyy-MM-dd") + " " + Format(varValue, "HH:mm:ss") + "'}")
                        Else
                            sb.Append("NULL")
                        End If

                    Case "Date()"
                        If CType(varValue, Date()).Count = 0 Then
                            sb.Append("NULL")
                        Else
                            For Each Val As Date In CType(varValue, Date())
                                sb.Append(FormatValue(Val))
                                sb.Append(",")
                            Next
                            sb.Remove(sb.Length - 1, 1)
                        End If

                    Case "ArrayList"
                        For Each Val As Object In CType(varValue, ArrayList)
                            sb.Append(FormatValue(Val))
                            sb.Append(",")
                        Next
                        sb.Remove(sb.Length - 1, 1)

                    Case "ColField(Of String)", "ColField(Of Integer)", "ColField(Of Decimal)", "ColField(Of Date)", "ColField(Of Boolean)"
                        sb.Append("""" & CType(varValue, IColField).ColumnName & """")

                    Case "Integer()"
                        For Each Val As Integer In CType(varValue, Integer())
                            sb.Append(FormatValue(Val))
                            sb.Append(",")
                        Next
                        sb.Remove(sb.Length - 1, 1)

                    Case Else
                        Select Case op
                            Case eOperator.pLike : sb.Append("'%" & varValue.ToString.Replace("'", "''") & "%'")
                            Case eOperator.pLikeBegins : sb.Append("'" & varValue.ToString.Replace("'", "''") & "%'")
                            Case eOperator.pLikeEnds : sb.Append("'%" & varValue.ToString.Replace("'", "''") & "'")
                            Case Else : sb.Append(varValue.ToString)
                        End Select
                End Select

                If op <> Nothing Then
                    Select Case op
                        Case eOperator.pIn, eOperator.pNotIn, eOperator.pInNoCase, eOperator.pNotInNoCase
                            sb.Append(")")
                    End Select
                End If

                Return sb.ToString

            Catch ex As Exception
                Throw New OasysDbException(My.Resources.Errors.ProblemFormattingValue, ex)
            End Try

        End Function

        ''' <summary>
        ''' Returns string representation of given operator enumeration
        ''' </summary>
        ''' <param name="op"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function OperatorString(ByVal op As eOperator) As String

            Select Case op
                Case eOperator.pEquals : Return My.Resources.Enumerations.opEqual
                Case eOperator.pNotEquals : Return My.Resources.Enumerations.opNotEqual
                Case eOperator.pGreaterThan : Return My.Resources.Enumerations.opGreaterThan
                Case eOperator.pGreaterThanOrEquals : Return My.Resources.Enumerations.opGreaterThanEquals
                Case eOperator.pLessThan : Return My.Resources.Enumerations.opLessThan
                Case eOperator.pLessThanOrEquals : Return My.Resources.Enumerations.opLessThanEqual
                Case eOperator.pIn, eOperator.pInNoCase : Return My.Resources.Enumerations.opEqual
                Case eOperator.pNotIn, eOperator.pNotInNoCase : Return My.Resources.Enumerations.opNotEqual
                Case eOperator.pLikeNoCase, eOperator.pLike : Return My.Resources.Enumerations.opLike
                Case Else : Return op.ToString
            End Select

        End Function

        Private Function FormatOperator(ByVal op As eOperator) As String

            Select Case op
                Case eOperator.pAnd : Return " AND "
                Case eOperator.pOr : Return " OR "
                Case eOperator.pIs : Return " IS "
                Case eOperator.pIn, eOperator.pInNoCase : Return " IN "
                Case eOperator.pNotIn, eOperator.pNotInNoCase : Return " NOT IN "
                Case eOperator.pEquals, eOperator.pEqualsNoCase : Return " = "
                Case eOperator.pGreaterThan : Return " > "
                Case eOperator.pLessThan : Return " < "
                Case eOperator.pGreaterThanOrEquals : Return " >= "
                Case eOperator.pLessThanOrEquals : Return " <= "
                Case eOperator.pNotEquals : Return " <> "
                Case eOperator.pNotLike : Return " NOT LIKE "
                Case eOperator.pLike, eOperator.pLikeBegins, eOperator.pLikeEnds, eOperator.pLikeNoCase, eOperator.pLikeNoCaseBegins, eOperator.pLikeNoCaseEnds
                    Return " LIKE "
                Case Else : Return ""
            End Select

        End Function
        Private Function FormatOrderByType(ByVal op As eOrderByType) As String

            Select Case op
                Case eOrderByType.Ascending : Return " ASC "
                Case eOrderByType.Descending : Return " DESC "
                Case Else : Return ""
            End Select

        End Function
        Private Sub PerformFilterCheck(ByVal TableName As String)

            Try
                'check table name
                If TableName Is Nothing Then Throw New OasysDbException(My.Resources.Errors.NoTableDefined)
                If TableName.Length = 0 Then Throw New OasysDbException(My.Resources.Errors.NoTableDefined)

                'check column and column values
                If _Columns.Count = 0 Then Throw New OasysDbException(My.Resources.Errors.NoColumnsDefined)
                If _ColumnValues.Count = 0 Then Throw New OasysDbException(My.Resources.Errors.NoColumnValuesDefined)
                If _Columns.Count <> _ColumnValues.Count Then Throw New OasysDbException(My.Resources.Errors.MismatchWhereColumnsValues)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Protected Overrides Sub Finalize()
            Try
                MyBase.Finalize()
                If _IsTransaction = False Then
                    If Not _OdbcConnection Is Nothing Then
                        If _OdbcConnection.State = ConnectionState.Open Then
                            _OdbcConnection.Close()
                        End If
                        _OdbcConnection = Nothing
                    End If
                    If Not _OdbcCommand Is Nothing Then _OdbcCommand = Nothing
                End If
            Catch   ' Catches dispose errors
            End Try
        End Sub

        ''' <summary>
        ''' New Banking - Dynamic SQL
        '''             - Used as part of an existing transaction so an connection object already exists
        ''' </summary>
        ''' <param name="strQueryString"></param>
        ''' <history></history>
        ''' <remarks></remarks>
        Public Sub NewBankingNonQueryExecuteSql(ByVal strQueryString As String)
            Try
                If strQueryString.Length = 0 Then Throw New OasysDbException(My.Resources.Errors.NoQueryString)

                Trace.WriteLine("NewBanking Execute Sql - " & strQueryString)

                _OdbcCommand.CommandText = strQueryString
                _OdbcCommand.ExecuteNonQuery()

                Trace.WriteLine("NewBanking Completed")
            Catch ex As Exception
                Throw New OasysDbException(ex.Message)
            End Try
        End Sub

#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If _OdbcConnection IsNot Nothing Then
                        If _OdbcConnection.State = ConnectionState.Open Then _OdbcConnection.Close()
                    End If
                    ' TODO: free unmanaged resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#End Region

    End Class

    <Serializable()> Public Class OasysDbException
        Inherits System.ApplicationException
        Private _LogName As String = "Application"
        Private _Logged As Boolean = False

        Public ReadOnly Property LogName() As String
            Get
                Return _LogName
            End Get
        End Property
        Public ReadOnly Property Logged() As Boolean
            Get
                Return _Logged
            End Get
        End Property

        Public Sub New()
        End Sub
        Public Sub New(ByVal Message As String)
            MyBase.New(Message)
            WriteToLog(Message)
        End Sub
        Public Sub New(ByVal Message As String, ByVal ex As Exception)
            MyBase.New(Message, ex)
            If ex IsNot Nothing Then WriteToLog(ex.ToString)
        End Sub
        Public Sub New(ByVal message As String, ByVal ex As SqlClient.SqlException)
            MyBase.New(message, ex.InnerException)

            'create error message
            Dim sb As New StringBuilder
            sb.Append(message & Environment.NewLine)
            sb.Append("Sql Error Code: " & ex.ErrorCode & Environment.NewLine)
            sb.Append("Sql Class: " & ex.Class.ToString & Environment.NewLine & Environment.NewLine)
            sb.Append("Sql Errors:" & Environment.NewLine)
            For Each e As SqlClient.SqlError In ex.Errors
                sb.Append(e.Number & " - " & e.Message & " at " & e.Procedure & ":" & e.LineNumber & " State:" & e.State & Environment.NewLine)
            Next

            'add general information
            sb.Append(Environment.NewLine & Environment.NewLine)
            sb.Append(ex.ToString)
            WriteToLog(sb.ToString)

        End Sub
        Public Sub New(ByVal message As String, ByVal ex As Odbc.OdbcException)
            MyBase.New(message, ex.InnerException)

            'create error message
            Dim sb As New StringBuilder
            sb.Append(message & Environment.NewLine)
            sb.Append("Odbc Error Code: " & ex.ErrorCode & Environment.NewLine)
            sb.Append("Sql Errors:" & Environment.NewLine)
            For Each e As Odbc.OdbcError In ex.Errors
                sb.Append(e.Message & " State:" & e.SQLState & Environment.NewLine)
            Next

            'add general information
            sb.Append(Environment.NewLine & Environment.NewLine)
            sb.Append(ex.ToString)

            'write into application log and trace for debugging
            WriteToLog(sb.ToString)

        End Sub

        Private Sub WriteToLog(ByVal message As String)

            EventLog.WriteEntry("Application", message, EventLogEntryType.Error)
            Trace.WriteLine(message, Me.Source)
            _Logged = True

        End Sub

    End Class


    ''' <summary>
    ''' Wrapper for sql server database connectivity
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()> Public Class Oasys3DbSqlServer
        Implements IDisposable
        Private _connection As SqlClient.SqlConnection = Nothing
        Private _dataAdapter As SqlClient.SqlDataAdapter = Nothing

        Public ReadOnly Property Connection() As SqlClient.SqlConnection
            Get
                Return _connection
            End Get
        End Property
        Public ReadOnly Property DataAdapter() As SqlClient.SqlDataAdapter
            Get
                Return _dataAdapter
            End Get
        End Property


        Public Sub New(ByVal connectionString As String)
            _connection = New SqlClient.SqlConnection(connectionString)
            _dataAdapter = New SqlClient.SqlDataAdapter
        End Sub


        ''' <summary>
        ''' Returns dataset of resulting of given query
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetDataset(ByVal sql As String) As DataSet

            Try
                Trace.WriteLine(sql, Me.GetType.ToString)
                Dim ds As New DataSet
                _dataAdapter.SelectCommand = New SqlClient.SqlCommand(sql, _connection)
                _dataAdapter.Fill(ds)
                Return ds

            Catch ex As Exception
                Throw New OasysDbException(My.Resources.Errors.sqlServerGetDataset, ex)
            End Try

        End Function


#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    'remove data adapter
                    If _dataAdapter IsNot Nothing Then
                        _dataAdapter.Dispose()
                    End If

                    'remove connection
                    If _connection IsNot Nothing Then
                        If _connection.State = ConnectionState.Open Then _connection.Close()
                        _connection.Dispose()
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace



