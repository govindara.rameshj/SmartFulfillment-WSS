﻿Imports System
Imports System.Reflection

<Serializable()> Public Class ColField(Of T)
    Implements IColField

    Private _ColumnName As String = String.Empty
    Private _ColumnValue As T = Nothing
    Private _ColumnDefault As T = Nothing
    Private _ColumnType As ColumnTypes = ColumnTypes.[String]
    Private _ColumnKey As Boolean = False
    Private _IdentityKey As Boolean = False
    Private _ShowInEditor As Boolean = False
    Private _Editable As Boolean = False
    Private _DisplayOrder As Integer = 0
    Private _OptionalField As Integer = 0
    Private _AllowType As Integer = 0
    Private _DisplayWidth As Integer = 0
    Private _AllowableValues As String = String.Empty
    Private _LookupList As Integer = 0
    Private _Length As Integer = 0
    Private _DecimalPlaces As Integer = 0
    Private _HeaderText As String = String.Empty

    Public Sub New(ByVal DBColumnName As String, ByVal DbColumnValue As T, Optional ByVal DBcolumnKey As Boolean = False, Optional ByVal DBidentityKey As Boolean = False)
        _ColumnName = DBColumnName
        _ColumnValue = DbColumnValue
        _ColumnDefault = DbColumnValue
        _ColumnKey = DBcolumnKey
        _IdentityKey = DBidentityKey

        DefineColumnType()
    End Sub
    Public Sub New(ByVal DBColumnName As String, ByVal DbColumnValue As T, ByVal DBcolumnKey As Boolean, ByVal DBidentityKey As Boolean, _
                   ByVal DBLength As Integer, ByVal DBDecimalPlaces As Integer, _
                   ByVal DBShowInEditor As Boolean, ByVal DBEditable As Boolean, ByVal DBDisplayOrder As Integer, ByVal DBOptionalField As Integer, _
                   ByVal DBAllowType As Integer, ByVal DBDisplayWidth As Integer, ByVal DBAllowableValues As String, ByVal DBLookupList As Integer, ByVal DBHeaderText As String)

        _ColumnName = DBColumnName
        _ColumnValue = DbColumnValue
        _ColumnDefault = DbColumnValue
        _ColumnKey = DBcolumnKey
        _IdentityKey = DBidentityKey
        _ShowInEditor = DBShowInEditor
        _Editable = DBEditable
        _DisplayOrder = DBDisplayOrder
        _OptionalField = DBOptionalField
        _AllowType = DBAllowType
        _DisplayWidth = DBDisplayWidth
        _AllowableValues = DBAllowableValues
        _LookupList = DBLookupList
        _DecimalPlaces = DBDecimalPlaces
        _Length = DBLength
        _HeaderText = DBHeaderText

        DefineColumnType()
    End Sub
    Public Sub New(ByVal ColumnName As String, ByVal DefaultValue As T, ByVal DisplayName As String, ByVal IsKey As Boolean, ByVal IsIdentity As Boolean, Optional ByVal DecimalPlaces As Integer = 0)
        _ColumnName = ColumnName
        _ColumnValue = DefaultValue
        _ColumnDefault = DefaultValue
        _ColumnKey = IsKey
        _IdentityKey = IsIdentity
        _DecimalPlaces = DecimalPlaces
        _HeaderText = DisplayName

        DefineColumnType()
    End Sub

    Private Sub DefineColumnType()
        Dim t1 As Type = Nullable.GetUnderlyingType(GetType(T))
        If t1 Is Nothing Then
            t1 = GetType(T)
        End If

        Select Case t1.ToString
            Case "System.String" : _ColumnType = ColumnTypes.String
            Case "System.Boolean" : _ColumnType = ColumnTypes.Boolean
            Case "System.Decimal", "System.Int32", "System.Integer" : _ColumnType = ColumnTypes.Number
            Case "System.Date", "System.DateTime" : _ColumnType = ColumnTypes.Date
        End Select
    End Sub

    Public Property Value() As T
        Get
            Return _ColumnValue
        End Get
        Set(ByVal value As T)
            _ColumnValue = value
            If _ColumnDefault Is Nothing Then _ColumnDefault = value
        End Set
    End Property

    Private Property UntypedValue() As Object Implements IColField.UntypedValue
        Get
            Return _ColumnValue
        End Get
        Set(ByVal value As Object)
            If (value Is Nothing) Then
                _ColumnValue = Nothing
            Else
                Dim t1 As Type = Nullable.GetUnderlyingType(GetType(T))
                If t1 Is Nothing Then
                    _ColumnValue = CType(value, T)
                Else
                    _ColumnValue = CType(Convert.ChangeType(value, t1), T)
                End If
            End If
        End Set
    End Property
    Public Property DefaultValue() As T
        Get
            Return _ColumnDefault
        End Get
        Set(ByVal value As T)
            _ColumnDefault = value
        End Set
    End Property
    Public Property ColumnName() As String Implements IColField.ColumnName
        Get
            Return _ColumnName
        End Get
        Set(ByVal value As String)
            _ColumnName = value
        End Set
    End Property
    Public Property ColumnKey() As Boolean Implements IColField.ColumnKey
        Get
            Return _ColumnKey
        End Get
        Set(ByVal value As Boolean)
            _ColumnKey = value
        End Set
    End Property
    Public ReadOnly Property ColumnType() As ColumnTypes Implements IColField.ColumnType
        Get
            Return _ColumnType
        End Get
    End Property
    Public Property IdentityKey() As Boolean Implements IColField.IdentityKey
        Get
            Return _IdentityKey
        End Get
        Set(ByVal value As Boolean)
            _IdentityKey = value
        End Set
    End Property
    Public Property ShowInEditor() As Boolean
        Get
            Return _ShowInEditor
        End Get
        Set(ByVal value As Boolean)
            _ShowInEditor = value
        End Set
    End Property
    Public Property Editable() As Boolean
        Get
            Return _Editable
        End Get
        Set(ByVal value As Boolean)
            _Editable = value
        End Set
    End Property
    Public Property DisplayOrder() As Integer
        Get
            Return _DisplayOrder
        End Get
        Set(ByVal value As Integer)
            _DisplayOrder = value
        End Set
    End Property
    Public Property AllowType() As Integer
        Get
            Return _AllowType
        End Get
        Set(ByVal value As Integer)
            _AllowType = value
        End Set
    End Property
    Public Property DisplayWidth() As Integer
        Get
            Return _DisplayWidth
        End Get
        Set(ByVal value As Integer)
            _DisplayWidth = value
        End Set
    End Property
    Public Property AllowableValues() As String
        Get
            Return _AllowableValues
        End Get
        Set(ByVal value As String)
            _AllowableValues = value
        End Set
    End Property
    Public Property OptionalField() As Integer
        Get
            Return _OptionalField
        End Get
        Set(ByVal value As Integer)
            _OptionalField = value
        End Set
    End Property
    Public Property LookupList() As Integer
        Get
            Return _LookupList
        End Get
        Set(ByVal value As Integer)
            _LookupList = value
        End Set
    End Property
    Public Property DecimalPlaces() As Integer Implements IColField.DecimalPlaces
        Get
            Return _DecimalPlaces
        End Get
        Set(ByVal value As Integer)
            _DecimalPlaces = value
        End Set
    End Property
    Public Property Length() As Integer
        Get
            Return _Length
        End Get
        Set(ByVal value As Integer)
            _Length = value
        End Set
    End Property
    Public Property HeaderText() As String Implements IColField.HeaderText
        Get
            Return _HeaderText
        End Get
        Set(ByVal value As String)
            _HeaderText = value
        End Set
    End Property

    Public Sub ResetValue() Implements IColField.ResetValue
        Me.Value = Me.DefaultValue
    End Sub
End Class

Public Enum ColumnTypes
    [Date]
    [String]
    Number
    [Boolean]
End Enum