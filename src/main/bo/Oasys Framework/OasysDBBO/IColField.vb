﻿Public Interface IColField
    Property ColumnName As String
    Property ColumnKey As Boolean
    ReadOnly Property ColumnType As ColumnTypes
    Property IdentityKey As Boolean
    Property HeaderText As String
    Property DecimalPlaces As Integer
    Property UntypedValue As Object
    Sub ResetValue()
End Interface
