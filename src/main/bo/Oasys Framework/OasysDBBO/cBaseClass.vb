﻿Imports OasysDBBO.Oasys3.DB.clsOasys3DB

<Serializable()> Public MustInherit Class cBaseClass
    Implements IDisposable

    Private _BOFields As New List(Of IColField)
    Private _BORecords As New List(Of cBaseClass)
    Private _BOConstraints As New List(Of Object())
    Private _Oasys3DB As New clsOasys3DB
    Private _ConnectionString As String = String.Empty
    Private _TableName As String = String.Empty

    Private _WhereFields As List(Of DBField) = Nothing
    Private _WhereJoins As List(Of Oasys3.DB.clsOasys3DB.eOperator) = Nothing
    Private _LoadFields As List(Of IColField) = Nothing
    Private _SortFields As List(Of DBField) = Nothing
    Private _AggregateFields As List(Of DBField) = Nothing
    Private _StaticWhereFields As List(Of DBField) = Nothing
    Private _StaticWhereJoins As List(Of Oasys3.DB.clsOasys3DB.eOperator) = Nothing

    Public MustOverride Sub Start()
    Public MustOverride Sub LoadBORecords(Optional ByVal count As Integer = -1)

    Public Property BOFields() As System.Collections.Generic.List(Of IColField)
        Get
            Return _BOFields
        End Get
        Set(ByVal value As System.Collections.Generic.List(Of IColField))
            _BOFields = value
        End Set
    End Property
    Public Property BORecords() As System.Collections.Generic.List(Of OasysDBBO.cBaseClass)
        Get
            Return _BORecords
        End Get
        Set(ByVal value As System.Collections.Generic.List(Of OasysDBBO.cBaseClass))
            _BORecords = value
        End Set
    End Property
    Public Property BOConstraints() As System.Collections.Generic.List(Of Object())
        Get
            Return _BOConstraints
        End Get
        Set(ByVal value As System.Collections.Generic.List(Of Object()))
            _BOConstraints = value
        End Set
    End Property
    Public Property Oasys3DB() As clsOasys3DB
        Get
            Return _Oasys3DB
        End Get
        Set(ByVal value As clsOasys3DB)
            _Oasys3DB = value
        End Set
    End Property
    Public Property ConnectionString() As String
        Get
            ConnectionString = _Oasys3DB.ConnectionString
        End Get
        Set(ByVal value As String)
            _Oasys3DB.ConnectionString = value
            _ConnectionString = value
        End Set
    End Property
    Public Property TableName() As String
        Get
            Return _TableName
        End Get
        Set(ByVal value As String)
            _TableName = value
        End Set
    End Property


    Public Sub New()
    End Sub
    Public Sub New(ByVal strConnection As String)
        ConnectionString = strConnection
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        _Oasys3DB = oasys3DB
    End Sub


    Public Function DebugString() As String
        Dim sb As New StringBuilder
        For Each field As IColField In _BOFields
            If field.ColumnKey = True Then sb.Append(field.ColumnName & "=" & field.UntypedValue.ToString & Space(2))
        Next
        Return sb.ToString
    End Function
    Public Function Version() As String
        Version = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductName _
        & " " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion
    End Function
    Public Sub ConnectionKey(ByVal key As String)

        _Oasys3DB.ConnectionKey = key
        _ConnectionString = _Oasys3DB.ConnectionString

    End Sub


    Public Function LoadCTSMatches(Optional ByVal count As Integer = -1) As List(Of cBaseClass)

        Try
            LoadBORecords(count)
            Return _BORecords

        Catch ex As Exception
            Dim o As New OasysDbException(My.Resources.Errors.ProblemLoadCtsMatches, ex)
            _Oasys3DB.Dispose()
            Return Nothing
        End Try

    End Function
    Public Overridable Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        Try
            For Each dcColumn As Data.DataColumn In drRow.Table.Columns
                If Not IsDBNull(drRow(dcColumn)) Then
                    For Each field As IColField In _BOFields
                        If field.ColumnName = dcColumn.ColumnName Then
                            If TypeOf drRow(dcColumn) Is String Then
                                field.UntypedValue = drRow(dcColumn).ToString.Trim
                            Else
                                field.UntypedValue = drRow(dcColumn)
                            End If
                            Exit For
                        End If
                    Next
                End If
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Overridable Function HKDelete() As Boolean

        Try
            _Oasys3DB.ClearAllParameters()
            _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)

            If (_WhereFields Is Nothing) Then _WhereFields = New List(Of DBField)
            For Each entry As DBField In _WhereFields
                _Oasys3DB.SetWhereParameter(entry.ColumnName, CType(entry.DBOperator, eOperator), entry.DBValue)
            Next

            If (_WhereJoins Is Nothing) Then _WhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
            For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In _WhereJoins
                _Oasys3DB.SetWhereJoinParameter(WhereJoin)
            Next

            If _Oasys3DB.Delete() = -1 Then Return False
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Overridable Function Delete() As Boolean

        Try
            _Oasys3DB.ClearAllParameters()
            _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            SetDBKeys()

            If _Oasys3DB.Delete > 0 Then Return True
            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Overridable Function Save(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType) As Boolean

        Try
            Select Case eSave
                Case clsOasys3DB.eSqlQueryType.pInsert
                    _Oasys3DB.ClearAllParameters()
                    _Oasys3DB.SetTableParameter(_TableName, eSave)
                    SetDBFieldValues(eSave)

                    If _Oasys3DB.Insert <= 0 Then Return False
                    GetDBIdentity()
                    Return True

                Case clsOasys3DB.eSqlQueryType.pUpdate
                    _Oasys3DB.ClearAllParameters()
                    _Oasys3DB.SetTableParameter(_TableName, eSave)
                    SetDBFieldValues(eSave)
                    SetDBKeys()

                    If _Oasys3DB.Update <= 0 Then Return False
                    Return True

                Case Else : Return False
            End Select

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Overridable Function SaveIfNew() As Boolean
        Return Save(Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
    End Function
    Public Overridable Function SaveIfExists() As Boolean
        Return Save(Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
    End Function

    ''' <summary>
    ''' Resets all BOField values back to default values for this instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub ResetValues()

        Try
            For Each field As IColField In BOFields
                field.ResetValue()
            Next

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Sub

    Public Sub SetDBFieldValues(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType)

        Try
            Select Case eSave
                Case clsOasys3DB.eSqlQueryType.pInsert
                    _Oasys3DB.ClearColumnParameters()
                    If _LoadFields Is Nothing Then _LoadFields = _BOFields
                    For Each field As IColField In _LoadFields
                        If field.IdentityKey Then Continue For
                        _Oasys3DB.SetColumnAndValueParameter(field.ColumnName, field.UntypedValue)
                    Next

                Case clsOasys3DB.eSqlQueryType.pUpdate
                    _Oasys3DB.ClearColumnParameters()
                    If _LoadFields Is Nothing Then _LoadFields = _BOFields
                    For Each field As IColField In _LoadFields
                        If field.IdentityKey Then Continue For
                        If field.ColumnKey Then Continue For
                        _Oasys3DB.SetColumnAndValueParameter(field.ColumnName, field.UntypedValue)
                    Next
            End Select

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Sub
    Public Sub SetDBKeys()

        Try
            Dim notFirst As Boolean = False
            _Oasys3DB.ClearWhereParameters()

            For Each field As IColField In _BOFields
                If field.ColumnKey = True Then
                    If notFirst Then _Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    _Oasys3DB.SetWhereParameter(field.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, field.UntypedValue)
                    notFirst = True
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Sub
    Public Sub GetDBIdentity()

        Try
            For Each field As IColField In _BOFields
                If field.IdentityKey = True Then
                    _Oasys3DB.ClearAllParameters()
                    _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
                    _Oasys3DB.SetColumnAggregate(Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, field.ColumnName, field.ColumnName)
                    Dim identity As Integer = CInt(_Oasys3DB.Query.Tables(0).Rows(0)(field.ColumnName))
                    If identity > 0 Then field.UntypedValue = identity
                    Exit Sub
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Sub


    Public Sub AddLoadField(FieldID As IColField)

        Try
            If _LoadFields Is Nothing Then _LoadFields = (New List(Of IColField))
            _LoadFields.Add(FieldID)

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

    End Sub
    Public Sub AddAggregateField(ByVal AggreateOperator As OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates, ByVal FieldId As IColField, ByVal AliasName As String)

        Try
            Dim field As DBField = GetDbField(AggreateOperator, FieldId, AliasName)

            'Add field to Row selector
            If _AggregateFields Is Nothing Then _AggregateFields = (New List(Of OasysDBBO.DBField))
            _AggregateFields.Add(field)

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

    End Sub

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of String), Value As String) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of String), Value() As String) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of String), Value As ArrayList) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Integer), Value As Integer) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Integer), Value() As Integer) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Integer), Value As ArrayList) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Decimal), Value As Decimal) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Date), Value As Date) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Date), Value() As Date) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Date), Value As ArrayList) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Boolean), Value As Object) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddLoadFilter(ByVal eOperator As clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Object), Value As Object) As Boolean
        AddUntypedLoadFilter(eOperator, FieldID, Value)
    End Function

    Public Function AddUntypedLoadFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, FieldID As IColField, Value As Object) As Boolean

        Try
            'Create field for storing Key into
            Dim field As New DBField(FieldID.ColumnName, FieldID.UntypedValue, FieldID.ColumnType)
            field.DBValue = Value
            field.DBOperator = eOperator

            'Add field to Row selector
            If _WhereFields Is Nothing Then _WhereFields = New List(Of DBField)
            _WhereFields.Add(field)
            Return True

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
            Return False
        End Try

    End Function
    Protected Function AddStaticFilter(ByVal eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Boolean), ByRef Value As Object) As Boolean

        Try
            'Create field for storing Key into
            Dim field As DBField = GetDbField(eOperator, FieldID, Value)

            'Add field to Row selector
            If _StaticWhereFields Is Nothing Then _StaticWhereFields = New List(Of DBField)
            _StaticWhereFields.Add(field)
            Return True

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
            Return False
        End Try

    End Function

    Protected Function JoinStaticFilter(ByVal DBOperator As Oasys3.DB.clsOasys3DB.eOperator) As Boolean

        Try
            If _StaticWhereJoins Is Nothing = True Then _StaticWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
            _StaticWhereJoins.Add(DBOperator)
            Return True

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
            Return False
        End Try

    End Function
    Public Function JoinLoadFilter(ByVal DBOperator As Oasys3.DB.clsOasys3DB.eOperator) As Boolean

        Try
            If _WhereJoins Is Nothing = True Then _WhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
            _WhereJoins.Add(DBOperator)
            Return True

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
            Return False
        End Try

    End Function
    Public Function SortBy(ByVal column As String, ByVal type As Oasys3.DB.clsOasys3DB.eOrderByType) As Boolean

        Try
            'Create field for storing Key into
            Dim field As New DBField(column, "", ColumnTypes.String)
            field.DBValue = column
            field.DBOperator = type

            'Add field to Row selector
            If _SortFields Is Nothing Then _SortFields = New List(Of DBField)
            _SortFields.Add(field)
            Return True

        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
            Return False
        End Try

    End Function

    Public Function GetAggregateDataSet() As DataSet

        Try ' Load up all properties from the database
            _Oasys3DB.ClearAllParameters()
            _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)

            If _WhereFields IsNot Nothing Then
                For Each entry As DBField In _WhereFields
                    _Oasys3DB.SetWhereParameter(entry.ColumnName, CType(entry.DBOperator, eOperator), entry.DBValue)
                Next
            End If

            If _WhereJoins IsNot Nothing Then
                For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In _WhereJoins
                    _Oasys3DB.SetWhereJoinParameter(WhereJoin)
                Next
            End If

            If _AggregateFields IsNot Nothing Then
                For Each field As DBField In _AggregateFields
                    _Oasys3DB.SetColumnAggregate(CType(field.DBOperator, eAggregates), field.ColumnName, CStr(field.DBValue))
                Next
            End If

            If _SortFields IsNot Nothing Then
                For Each field As DBField In _SortFields
                    _Oasys3DB.SetOrderByParameter(field.ColumnName, CType(field.DBOperator, eOrderByType))
                Next
            End If

            Return _Oasys3DB.Query()

        Catch ex As Oasys3.DB.OasysDbException
            _Oasys3DB.Dispose()
            Return Nothing

        Catch ex As Exception
            _Oasys3DB.Dispose()
            Return Nothing
        Finally
            ClearLists()
        End Try

    End Function
    Public Function GetAggregateField() As Object

        Try ' Load up all properties from the database
            _Oasys3DB.ClearAllParameters()
            _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)

            If _WhereFields IsNot Nothing Then
                For Each field As DBField In _WhereFields
                    _Oasys3DB.SetWhereParameter(field.ColumnName, CType(field.DBOperator, eOperator), field.DBValue)
                Next
            End If

            If _WhereJoins IsNot Nothing Then
                For Each join As Oasys3.DB.clsOasys3DB.eOperator In _WhereJoins
                    _Oasys3DB.SetWhereJoinParameter(join)
                Next
            End If

            If _AggregateFields IsNot Nothing Then
                For Each field As DBField In _AggregateFields
                    _Oasys3DB.SetColumnAggregate(CType(field.DBOperator, eAggregates), field.ColumnName, CStr(field.DBValue))
                Next
            End If

            If _SortFields IsNot Nothing Then
                For Each field As DBField In _SortFields
                    _Oasys3DB.SetOrderByParameter(field.ColumnName, CType(field.DBOperator, eOrderByType))
                Next
            End If

            Return _Oasys3DB.Query().Tables(0).Rows(0).Item(0)

        Catch ex As Oasys3.DB.OasysDbException
            _Oasys3DB.Dispose()
            Return Nothing

        Catch ex As Exception
            _Oasys3DB.Dispose()
            Return Nothing
        Finally
            ClearLists()
        End Try

    End Function


    Public Sub ClearLists()
        _WhereFields = Nothing
        _WhereJoins = Nothing
        _LoadFields = Nothing
        _SortFields = Nothing
        _AggregateFields = Nothing
    End Sub
    Public Sub ClearLoadFilter()

        _WhereFields = Nothing
        _WhereJoins = Nothing

    End Sub
    Public Sub ClearLoadField()

        _LoadFields = Nothing

    End Sub


    Public Sub GetSQLBase()

        Try
            _Oasys3DB.ClearAllParameters()
            _Oasys3DB.SetTableParameter(_TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)

            If _StaticWhereFields IsNot Nothing Then
                For Each field As DBField In _StaticWhereFields
                    Oasys3DB.SetWhereParameter(field.ColumnName, CType(field.DBOperator, eOperator), field.DBValue)
                Next
            End If

            If _WhereFields IsNot Nothing Then
                For Each field As DBField In _WhereFields
                    Oasys3DB.SetWhereParameter(field.ColumnName, CType(field.DBOperator, eOperator), field.DBValue)
                Next
            End If

            If _StaticWhereJoins IsNot Nothing Then
                For Each join As eOperator In _StaticWhereJoins
                    Oasys3DB.SetWhereJoinParameter(join)
                Next
            End If

            If _WhereJoins IsNot Nothing Then
                For Each join As eOperator In _WhereJoins
                    Oasys3DB.SetWhereJoinParameter(join)
                Next
            End If

            If _LoadFields IsNot Nothing Then
                For Each field As IColField In _LoadFields
                    Oasys3DB.SetColumnParameter(field.ColumnName)
                Next
            End If

            If _SortFields IsNot Nothing Then
                For Each entry As DBField In _SortFields
                    Oasys3DB.SetOrderByParameter(entry.ColumnName, CType(entry.DBOperator, eOrderByType))
                Next
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Sub
    Public Function GetSQLSelectDataSet() As DataSet


        Try
            GetSQLBase()
            Return _Oasys3DB.Query()

        Catch ex As Oasys3.DB.OasysDbException
            Throw ex
            _Oasys3DB.Dispose()
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
            _Oasys3DB.Dispose()
        End Try

    End Function
    Public Function GetSQLSelect() As String

        Try
            GetSQLBase()
            Return _Oasys3DB.BuildSql()

        Catch ex As Oasys3.DB.OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        End Try

    End Function
    Private Function GetDbField(ByVal Oper As Integer, ByVal lFieldID As IColField, ByVal Value As Object) As DBField

        Dim field As DBField = New DBField(lFieldID.ColumnName, lFieldID.UntypedValue, lFieldID.ColumnType)
        field.DBOperator = Oper
        field.DBValue = Value
        GetDbField = field

    End Function
    Public Function GetProperty(ByVal FieldName As String) As IColField

        ' return the property of the Field name passed in
        For intProperty As Integer = 0 To BOFields.Count - 1
            If CType(BOFields.Item(intProperty), IColField).ColumnName = FieldName Then
                Return _BOFields.Item(intProperty)
            End If
        Next
        Return Nothing

    End Function

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
