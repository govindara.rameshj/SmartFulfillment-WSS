﻿Imports System.Configuration
Imports Oasys.Core

Public Class SystemUser
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _employeeCode As String
    Private _name As String
    Private _initials As String
    Private _position As String
    Private _payrollId As String
    Private _password As String
    Private _passwordExpires As Nullable(Of Date)
    Private _isManager As Boolean
    Private _isSupervisor As Boolean
    Private _superPassword As String
    Private _superPasswordExpires As Nullable(Of Date)
    Private _outlet As String
    Private _isDeleted As Boolean
    Private _deletedDate As Nullable(Of Date)
    Private _deletedBy As String
    Private _deletedWhere As String
    Private _tillReceiptName As String
    Private _defaultAmount As Decimal
    Private _languageCode As String
    Private _securityProfileId As Integer
#End Region

#Region "Properties"
    <ColumnMapping("ID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("EmployeeCode")> Public Property EmployeeCode() As String
        Get
            Return _employeeCode
        End Get
        Private Set(ByVal value As String)
            _employeeCode = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("Initials")> Public Property Initials() As String
        Get
            Return _initials
        End Get
        Private Set(ByVal value As String)
            _initials = value
        End Set
    End Property
    <ColumnMapping("Position")> Public Property Position() As String
        Get
            Return _position
        End Get
        Private Set(ByVal value As String)
            _position = value
        End Set
    End Property
    <ColumnMapping("PayrollID")> Public Property PayrollID() As String
        Get
            Return _payrollId
        End Get
        Private Set(ByVal value As String)
            _payrollId = value
        End Set
    End Property
    <ColumnMapping("Password")> Public Property Password() As String
        Get
            Return _password
        End Get
        Private Set(ByVal value As String)
            _password = value
        End Set
    End Property
    <ColumnMapping("PasswordExpires")> Public Property PasswordExpires() As Nullable(Of Date)
        Get
            Return _passwordExpires
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _passwordExpires = value
        End Set
    End Property
    <ColumnMapping("IsManager")> Public Property IsManager() As Boolean
        Get
            Return _isManager
        End Get
        Private Set(ByVal value As Boolean)
            _isManager = value
        End Set
    End Property
    <ColumnMapping("IsSupervisor")> Public Property IsSupervisor() As Boolean
        Get
            Return _isSupervisor
        End Get
        Private Set(ByVal value As Boolean)
            _isSupervisor = value
        End Set
    End Property
    <ColumnMapping("SupervisorPassword")> Public Property SuperPassword() As String
        Get
            Return _superPassword
        End Get
        Private Set(ByVal value As String)
            _superPassword = value
        End Set
    End Property
    <ColumnMapping("SupervisorPwdExpires")> Public Property SuperPasswordExpires() As Nullable(Of Date)
        Get
            Return _superPasswordExpires
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _superPasswordExpires = value
        End Set
    End Property
    <ColumnMapping("Outlet")> Public Property Outlet() As String
        Get
            Return _outlet
        End Get
        Private Set(ByVal value As String)
            _outlet = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Private Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("DeletedDate")> Public Property DeletedDate() As Nullable(Of Date)
        Get
            Return _deletedDate
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _deletedDate = value
        End Set
    End Property
    <ColumnMapping("DeletedBy")> Public Property DeletedBy() As String
        Get
            Return _deletedBy
        End Get
        Private Set(ByVal value As String)
            _deletedBy = value
        End Set
    End Property
    <ColumnMapping("DeletedWhere")> Public Property DeletedWhere() As String
        Get
            Return _deletedWhere
        End Get
        Private Set(ByVal value As String)
            _deletedWhere = value
        End Set
    End Property
    <ColumnMapping("TillReceiptName")> Public Property TillReceiptName() As String
        Get
            Return _tillReceiptName
        End Get
        Private Set(ByVal value As String)
            _tillReceiptName = value
        End Set
    End Property
    <ColumnMapping("DefaultAmount")> Public Property DefaultAmount() As Decimal
        Get
            Return _defaultAmount
        End Get
        Private Set(ByVal value As Decimal)
            _defaultAmount = value
        End Set
    End Property
    <ColumnMapping("LanguageCode")> Public Property LanguageCode() As String
        Get
            Return _languageCode
        End Get
        Private Set(ByVal value As String)
            _languageCode = value
        End Set
    End Property
    <ColumnMapping("SecurityProfileID")> Public Property SecurityProfileId() As Integer
        Get
            Return _securityProfileId
        End Get
        Private Set(ByVal value As Integer)
            _securityProfileId = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    ''' <summary>
    ''' Returns system user for given id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetUser(ByVal id As Integer) As SystemUser

        Dim dt As DataTable = DataOperations.UserGetUser(id)
        If dt.Rows.Count > 0 Then
            Return New SystemUser(dt.Rows(0))
        End If
        Return Nothing

    End Function

    ''' <summary>
    ''' Returns user id and name for given id
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetUserIdName(ByVal id As Integer) As String

        Dim dt As DataTable = DataOperations.UserGetUser(id)
        If dt.Rows.Count > 0 Then
            Dim user As New SystemUser(dt.Rows(0))
            Return user.Id & Space(1) & user.Name.Trim
        End If
        Return String.Empty

    End Function

End Class
