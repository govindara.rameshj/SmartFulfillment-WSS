﻿Imports Oasys.Data

Friend Module DataOperations

    Friend Function UserGetUser(ByVal id As Integer) As DataTable

        Using com As New Command(New Connection, My.Resources.Procedures.UserGetUser)
            com.AddParameter(My.Resources.Parameters.Id, id)
            Return com.ExecuteDataTable
        End Using

    End Function

End Module
