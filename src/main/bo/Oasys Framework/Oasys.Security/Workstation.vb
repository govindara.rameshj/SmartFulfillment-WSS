﻿Imports System.Configuration
Imports Oasys.Core

Public Class Workstation
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _description As String
    Private _primaryFunction As String
    Private _isActive As Boolean
    Private _dateLastLoggedIn As Date
    Private _isBarcodeBroken As Boolean
    Private _useTouchScreen As Boolean
    Private _securityProfileID As Integer
#End Region

#Region "Properties"
    <ColumnMapping("ID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("PrimaryFunction")> Public Property PrimaryFunction() As String
        Get
            Return _primaryFunction
        End Get
        Private Set(ByVal value As String)
            _primaryFunction = value
        End Set
    End Property
    <ColumnMapping("IsActive")> Public Property IsActive() As Boolean
        Get
            Return _isActive
        End Get
        Private Set(ByVal value As Boolean)
            _isActive = value
        End Set
    End Property
    <ColumnMapping("DateLastLoggedIn")> Public Property DateLastLoggedIn() As Date
        Get
            Return _dateLastLoggedIn
        End Get
        Private Set(ByVal value As Date)
            _dateLastLoggedIn = value
        End Set
    End Property
    <ColumnMapping("IsBarcodeBroken")> Public Property IsBarcodeBroken() As Boolean
        Get
            Return _isBarcodeBroken
        End Get
        Private Set(ByVal value As Boolean)
            _isBarcodeBroken = value
        End Set
    End Property
    <ColumnMapping("UseTouchScreen")> Public Property UseTouchScreen() As Boolean
        Get
            Return _useTouchScreen
        End Get
        Private Set(ByVal value As Boolean)
            _useTouchScreen = value
        End Set
    End Property
    <ColumnMapping("SecurityProfileID")> Public Property SecurityProfileId() As Integer
        Get
            Return _securityProfileID
        End Get
        Private Set(ByVal value As Integer)
            _securityProfileID = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    Public Shared Function IsWorkstationActive(ByVal id As Integer) As Boolean

        Dim dt As DataTable = DataOperations.WorkstationGetWorkstation(id)
        If dt.Rows.Count > 0 Then
            Dim ws As New Workstation(dt.Rows(0))
            Return ws.IsActive
        End If
        Return False

    End Function

End Class
