﻿Imports System.Data.SqlClient
Imports System.Text

Namespace Oasys.Data

    Public Class Connection
        Implements IDisposable
        Private _connectionString As String = String.Empty
        Private _sqlConnection As SqlConnection = Nothing
        Private _sqlTransaction As SqlTransaction = Nothing

        Public Sub New()
            _connectionString = GetConnectionString()
            _sqlConnection = New SqlConnection(_connectionString)
            _sqlConnection.Open()
        End Sub

        Friend ReadOnly Property SqlConnection() As SqlConnection
            Get
                Return _sqlConnection
            End Get
        End Property

        Friend ReadOnly Property SqlTransaction() As SqlTransaction
            Get
                Return _sqlTransaction
            End Get
        End Property



        Public Sub StartTransaction()
            _sqlTransaction = _sqlConnection.BeginTransaction
        End Sub

        Public Sub CommitTransaction()
            If _sqlTransaction IsNot Nothing Then _sqlTransaction.Commit()
            _sqlTransaction.Dispose()
        End Sub

        Public Sub RollbackTransaction()
            If _sqlTransaction IsNot Nothing Then _sqlTransaction.Rollback()
            _sqlTransaction.Dispose()
        End Sub



#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If _sqlTransaction IsNot Nothing Then
                        _sqlTransaction.Dispose()
                    End If

                    If _sqlConnection IsNot Nothing Then
                        If _sqlConnection.State <> ConnectionState.Closed Then _sqlConnection.Close()
                        _sqlConnection.Dispose()
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

    Public Class Command
        Implements IDisposable
        Private _command As SqlCommand

        Public Sub New(ByRef connection As Connection)

            _command = New SqlCommand
            _command.Connection = connection.SqlConnection
            _command.CommandType = CommandType.Text
            _command.CommandTimeout = 0

            If connection.SqlTransaction IsNot Nothing Then
                _command.Transaction = connection.SqlTransaction
            End If

        End Sub

        Public Sub New(ByRef connection As Connection, ByVal storedProcedure As String)

            _command = New SqlCommand
            _command.CommandType = CommandType.StoredProcedure
            _command.Connection = connection.SqlConnection
            _command.CommandTimeout = 0
            _command.CommandText = storedProcedure

            If connection.SqlTransaction IsNot Nothing Then
                _command.Transaction = connection.SqlTransaction
            End If

        End Sub

        Public Property StoredProcedureName() As String
            Get
                Return _command.CommandText
            End Get
            Set(ByVal value As String)
                _command.CommandText = value
                _command.CommandType = CommandType.StoredProcedure
            End Set
        End Property

        Public Property CommandText() As String
            Get
                Return _command.CommandText
            End Get
            Set(ByVal value As String)
                _command.CommandText = value
                _command.CommandType = CommandType.Text
            End Set
        End Property

        Public Sub AddParameter(ByVal name As String, ByVal value As Object)

            Dim dbtype As SqlDbType = SqlDbType.Char
            Select Case value.GetType.ToString
                Case GetType(String).ToString
                Case GetType(Boolean).ToString : dbtype = SqlDbType.Bit
                Case GetType(Date).ToString : dbtype = SqlDbType.Date
                Case GetType(Integer).ToString : dbtype = SqlDbType.Int
                Case GetType(Decimal).ToString : dbtype = SqlDbType.Decimal
            End Select

            _command.Parameters.Add(New SqlParameter(name, dbtype)).Value = value

        End Sub

        Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType)

            _command.Parameters.Add(New SqlParameter(name, type)).Value = value

        End Sub

        Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer, ByVal direction As ParameterDirection)

            Dim param As New SqlParameter(name, type, size)
            param.Direction = direction
            param.Value = value

            _command.Parameters.Add(param)

        End Sub

        Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer)

            _command.Parameters.Add(New SqlParameter(name, type, size)).Value = value

        End Sub


        Public Sub AddParameterOutput(ByVal name As String, ByVal type As System.Type, ByVal size As Integer)

            Dim dbtype As SqlDbType = SqlDbType.Char
            Select Case type.ToString
                Case GetType(String).ToString
                Case GetType(Boolean).ToString : dbtype = SqlDbType.Bit
                Case GetType(Date).ToString : dbtype = SqlDbType.Date
                Case GetType(Integer).ToString : dbtype = SqlDbType.Int
                Case GetType(Decimal).ToString : dbtype = SqlDbType.Decimal
            End Select

            _command.Parameters.Add(New SqlParameter(name, dbtype, size)).Direction = ParameterDirection.Output

        End Sub

        Public Function GetParameterValue(ByVal name As String) As Object

            Return _command.Parameters(name).Value

        End Function



        Public Function ExecuteValue() As Object

            WriteTrace()
            Return _command.ExecuteScalar

        End Function

        Public Function ExecuteDataTable() As DataTable

            WriteTrace()
            Dim reader As SqlDataReader = _command.ExecuteReader
            Dim dt As New DataTable
            dt.Load(reader)

            reader.Close()
            Return dt

        End Function

        Public Function ExecuteDataSet() As DataSet

            WriteTrace()
            Dim ds As New DataSet

            Using da As New SqlDataAdapter(_command)
                da.Fill(ds)
            End Using

            Return ds

        End Function

        Public Function ExecuteNonQuery() As Integer

            WriteTrace()
            Return _command.ExecuteNonQuery()

        End Function

        Private Sub WriteTrace()

            'output trace
            Dim sb As New StringBuilder(_command.CommandText & Space(1))
            For Each param As SqlParameter In _command.Parameters
                If param.Direction <> ParameterDirection.Input Then Continue For
                If param.Value Is Nothing Then Continue For
                sb.Append(param.ParameterName & "=" & param.Value.ToString & ",")
            Next

            sb.Remove(sb.Length - 1, 1)
            Trace.WriteLine(sb.ToString, Me.GetType.ToString)

        End Sub

#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    _command.Dispose()
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace

