<Assembly: CLSCompliant(True)> 
Namespace Oasys

    Public MustInherit Class DataProxy

        Protected Sub New()
        End Sub

        Protected MustOverride Property Connection() As Object

        Protected MustOverride Sub CreateConnection(ByVal connectionString As String)

        Public MustOverride Function ExecuteDataTable(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As DataTable
        Public MustOverride Function ExecuteScalar(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As Object
        Public MustOverride Function ExecuteNonQuery(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As Integer

        Public MustOverride Sub BeginTransaction()
        Public MustOverride Sub CommitTransaction()
        Public MustOverride Sub RollbackTransaction()

    End Class

End Namespace

