﻿Imports System.Xml
Imports System.Reflection
Imports Cts.Oasys.Core

<HideModuleName()> Public Module SharedFunctions

    Private _strConnectionStrings As String = "configuration/connectionStrings/string"

    Public Function GetConnectionString() As String
        If GlobalVars.IsTestEnvironment Then
            Return GlobalVars.SystemEnvironment.GetOasysSqlServerConnectionString()
        Else

            'loadig config file
            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(GetConfigPath())

            'search for connection string
            For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                    Return node.Attributes.GetNamedItem("connectionString").Value
                End If
            Next

            Throw New Exception("No connection string found")
        End If

    End Function

    Private Function GetConfigPath() As String
        'get current location of this assembly
        Dim configFilename As String

        Dim codeBase As String = Assembly.GetExecutingAssembly().CodeBase
        Dim uriBuilder As New UriBuilder(codeBase)
        Dim assemblyPath As String = Uri.UnescapeDataString(uriBuilder.Path)

        'Dim configFilename As String = Directory.GetParent(assemblyPath).Parent.FullName & "\Resources\Connection.xml"
        'configFilename = Directory.GetParent(assemblyPath).FullName & "\Resources\Connection.xml"
        configFilename = System.AppDomain.CurrentDomain.BaseDirectory & "Resources\Connection.xml"



        If IO.File.Exists(configFilename) Then Return configFilename

        Throw New Exception("Connection.xml file not found. Please ensure that this file exists in the Resources folder")

    End Function

End Module