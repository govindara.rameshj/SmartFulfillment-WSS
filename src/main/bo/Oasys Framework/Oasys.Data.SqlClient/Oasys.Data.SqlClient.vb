﻿
Namespace Oasys
    Public Class SqlClientDataProxy
        Inherits DataProxy
        Implements IDisposable


        Private _Connection As System.Data.SqlClient.SqlConnection
        Private _Transaction As System.Data.SqlClient.SqlTransaction

        Public Sub New(ByVal connectionString As String)
            MyBase.New()
            CreateConnection(connectionString)
        End Sub

        Public Overrides Sub BeginTransaction()
            If _Connection Is Nothing Then
                Throw New InvalidOperationException("no connection available")
            End If
            If (_Connection.State And ConnectionState.Open) = 0 Then
                Throw New InvalidOperationException("connection is closed")
            End If

            _Transaction = _Connection.BeginTransaction
        End Sub

        Public Overrides Sub CommitTransaction()
            If _Transaction Is Nothing Then
                Throw New InvalidOperationException("no transaction available")
            End If
            _Transaction.Commit()

            _Transaction.Dispose()
            _Transaction = Nothing
        End Sub

        Protected Overrides Property Connection() As Object
            Get
                Return _Connection
            End Get
            Set(ByVal value As Object)
                value = DirectCast(value, System.Data.SqlClient.SqlConnection)
            End Set
        End Property

        Protected Overrides Sub CreateConnection(ByVal connectionString As String)
            _Connection = New System.Data.SqlClient.SqlConnection(connectionString)
        End Sub

        Public Overrides Function ExecuteDataTable(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As System.Data.DataTable
            Dim returnTable As DataTable
            returnTable = New DataTable
            selectCommand.Connection = DirectCast(Connection, SqlClient.SqlConnection)
            Using da As New System.Data.SqlClient.SqlDataAdapter(selectCommand)
                da.Fill(returnTable)
                Return returnTable
            End Using
        End Function

        Public Overrides Function ExecuteNonQuery(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As Integer
            selectCommand.Connection = DirectCast(Connection, SqlClient.SqlConnection)
            Dim saveState As System.Data.ConnectionState = selectCommand.Connection.State
            If saveState = ConnectionState.Closed Then
                selectCommand.Connection.Open()
            End If
            Try
                Return selectCommand.ExecuteNonQuery
            Finally
                If saveState = ConnectionState.Closed Then
                    selectCommand.Connection.Close()
                End If
            End Try
        End Function

        Public Overrides Function ExecuteScalar(ByVal selectCommand As System.Data.SqlClient.SqlCommand) As Object
            selectCommand.Connection = DirectCast(Connection, SqlClient.SqlConnection)
            Dim saveState As System.Data.ConnectionState = selectCommand.Connection.State
            If saveState = ConnectionState.Closed Then
                selectCommand.Connection.Open()
            End If
            Try
                Return selectCommand.ExecuteScalar
            Finally
                If saveState = ConnectionState.Closed Then
                    selectCommand.Connection.Close()
                End If
            End Try
        End Function

        Public Overrides Sub RollbackTransaction()
            If _Transaction Is Nothing Then
                Throw New InvalidOperationException("no transaction available")
            End If
            _Transaction.Rollback()
            _Transaction.Dispose()
            _Transaction = Nothing
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If _Transaction IsNot Nothing Then
                        _Transaction.Dispose()
                        _Transaction = Nothing
                    End If

                    If _Connection IsNot Nothing Then
                        _Connection.Dispose()
                        _Connection = Nothing
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace