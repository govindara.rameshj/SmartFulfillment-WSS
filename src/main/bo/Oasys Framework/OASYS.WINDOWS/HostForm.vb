Imports System.Drawing.Drawing2D

Public NotInheritable Class HostForm
    Private _loadMaximised As Boolean
    Private _doProcessing As Integer = 1

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal app As Form)
        InitializeComponent()
        AppPanel.Controls.Add(app)
    End Sub

    Public Sub New(ByVal assemblyName As String, ByVal className As String, ByVal appName As String, _
                   ByVal userId As Integer, ByVal workstationID As Integer, ByVal securityLevel As Integer, _
                   ByVal parameters As String, ByVal imagePath As String, ByVal loadMaximised As Boolean)
        InitializeComponent()

        'create instance of assembly and set status strip members
        Dim app As Form = CType(Activator.CreateInstanceFrom(assemblyName, className, True, Reflection.BindingFlags.Default, Nothing, New Object() {userId, workstationID, securityLevel, parameters}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, Form)
        app.AppName = appName

        If imagepath IsNot Nothing AndAlso imagePath.Length > 0 Then
            app.Icon = New System.Drawing.Icon(imagePath)
        End If

        AppPanel.Controls.Add(app)
        _loadMaximised = loadMaximised

    End Sub

    Public Sub New(ByVal assemblyName As String, ByVal className As String, ByVal appName As String, _
                   ByVal userId As Integer, ByVal workstationID As Integer, ByVal securityLevel As Integer, _
                   ByVal parameters As String, ByVal imagePath As String, ByVal loadMaximised As Boolean, ByVal doProcessing As Integer)
        InitializeComponent()

        'create instance of assembly and set status strip members
        Dim app As Form = CType(Activator.CreateInstanceFrom(assemblyName, className, True, Reflection.BindingFlags.Default, Nothing, New Object() {userId, workstationID, securityLevel, parameters}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, Form)
        app.AppName = appName

        If imagepath IsNot Nothing AndAlso imagePath.Length > 0 Then
            app.Icon = New System.Drawing.Icon(imagePath)
        End If

        AppPanel.Controls.Add(app)
        _loadMaximised = loadMaximised
        _doProcessing = doProcessing

    End Sub


    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        MyBase.OnShown(e)
        If _loadMaximised Then Me.WindowState = FormWindowState.Maximized
        Application.DoEvents()
        If _doProcessing = 1 Then DoProcessing()
    End Sub

    Protected Overrides Sub onPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
        Dim baseBackground As System.Drawing.Drawing2D.LinearGradientBrush
        baseBackground = New LinearGradientBrush(New Point(0, 0), New Point(ClientSize.Width, ClientSize.Height), Color.White, Color.LightBlue)
        e.Graphics.FillRectangle(baseBackground, ClientRectangle)
    End Sub


    Public Sub DoProcessing()

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).DoProcessing()
                Exit Sub
            End If
        Next

    End Sub



    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                Return CType(ctl, Form).Form_ProcessCmdKey(msg, keyData)
                Exit Function
            End If
        Next
    End Function

    Private Sub HostForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_Load(sender, e)
                Exit Sub
            End If
        Next
    End Sub

    Private Sub HostForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_Closed(sender, e)
                Exit Sub
            End If
        Next

    End Sub

    Private Sub HostForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_Closing(sender, e)
                Exit Sub
            End If
        Next

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_KeyDown(sender, e)
                Exit Sub
            End If
        Next

    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_KeyPress(sender, e)
                Exit Sub
            End If
        Next

    End Sub

    Private Sub Form_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        For Each ctl As Control In AppPanel.Controls
            If TypeOf ctl Is Form Then
                CType(ctl, Form).Form_KeyUp(sender, e)
                Exit Sub
            End If
        Next

    End Sub


    Private Sub AppPanel_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles AppPanel.ControlAdded

        If AppPanel.Controls.Count = 1 Then
            'get size of application to load and make app panel the same size
            Dim app As Form = CType(e.Control, Form)
            app.SetStatusMessage(MessageLabel)
            app.SetStatusProgress(Progress)

            Me.Text = app.AppName
            Me.Icon = app.Icon

            Dim moveWidth As Integer = app.Size.Width - AppPanel.Width
            Me.Left -= CInt(moveWidth / 2)
            Me.Width += moveWidth

            Dim moveHeight As Integer = app.Size.Height - AppPanel.Height
            Me.Top -= CInt(moveHeight / 2)
            Me.Height += moveHeight

            app.Dock = DockStyle.Fill
            app.Visible = True
        End If

    End Sub

End Class