﻿Public Class PanelCustomBorder

    Private _DrawTop As Boolean = True
    Private _DrawLeft As Boolean = True
    Private _DrawRight As Boolean = True
    Private _DrawBottom As Boolean = True

    Public Property DrawTop() As Boolean
        Get
            Return _DrawTop
        End Get
        Set(ByVal value As Boolean)
            _DrawTop = value
        End Set
    End Property
    Public Property DrawLeft() As Boolean
        Get
            Return _DrawLeft
        End Get
        Set(ByVal value As Boolean)
            _DrawLeft = value
        End Set
    End Property
    Public Property DrawRight() As Boolean
        Get
            Return _DrawRight
        End Get
        Set(ByVal value As Boolean)
            _DrawRight = value
        End Set
    End Property
    Public Property DrawBottom() As Boolean
        Get
            Return _DrawBottom
        End Get
        Set(ByVal value As Boolean)
            _DrawBottom = value
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        If _DrawTop Then e.Graphics.DrawLine(Pens.Black, New Point(0, 0), New Point(Width, 0))
        If _DrawLeft Then e.Graphics.DrawLine(Pens.Black, New Point(0, 0), New Point(0, Height))
        If _DrawRight Then e.Graphics.DrawLine(Pens.Black, New Point(Width - 1, 0), New Point(Width - 1, Height))
        If _DrawBottom Then e.Graphics.DrawLine(Pens.Black, New Point(0, Height - 1), New Point(Width, Height - 1))
        MyBase.OnPaint(e)
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

End Class
