﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class StoreSaleWeightCollection
    Inherits BindingList(Of StoreSaleWeight)

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal id As Integer)

        Dim dt As DataTable = DataOperations.GetStoreSaleWeightActive(id)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New StoreSaleWeight(dr))
        Next

    End Sub

    Public Sub AddActiveWeights(ByVal id As Integer)

        'check id not already in collection
        For Each saleWeight As StoreSaleWeight In Me.Items
            If saleWeight.Id = id Then Exit Sub
        Next

        'add new items
        Dim dt As DataTable = DataOperations.GetStoreSaleWeightActive(id)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New StoreSaleWeight(dr))
        Next

    End Sub

    Public Function Adjuster(ByVal week As Integer) As Decimal

        Dim adj As Decimal = 1
        For Each saleWeight As StoreSaleWeight In Me.Items
            If saleWeight.Week = week AndAlso saleWeight.Value <> 0 Then
                adj *= saleWeight.Value
            End If
        Next

        If adj = 0 Then adj = 1
        Return adj

    End Function

End Class