﻿Imports Oasys.Core

<TableMapping("Store")> Public Class Store
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _name As String
    Private _address1 As String
    Private _address2 As String
    Private _address3 As String
    Private _address4 As String
    Private _address5 As String
    Private _postcode As String
    Private _phoneNumber As String
    Private _faxNumber As String
    Private _manager As String
    Private _regionCode As String
    Private _countryCode As String
    Private _isClosed As Boolean
    Private _isOpenMon As Boolean
    Private _isOpenTue As Boolean
    Private _isOpenWed As Boolean
    Private _isOpenThu As Boolean
    Private _isOpenFri As Boolean
    Private _isOpenSat As Boolean
    Private _isOpenSun As Boolean
    Private _isHeadOffice As Boolean
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("Address1")> Public Property Address1() As String
        Get
            Return _address1
        End Get
        Private Set(ByVal value As String)
            _address1 = value
        End Set
    End Property
    <ColumnMapping("Address2")> Public Property Address2() As String
        Get
            Return _address2
        End Get
        Private Set(ByVal value As String)
            _address2 = value
        End Set
    End Property
    <ColumnMapping("Address3")> Public Property Address3() As String
        Get
            Return _address3
        End Get
        Private Set(ByVal value As String)
            _address3 = value
        End Set
    End Property
    <ColumnMapping("Address4")> Public Property Address4() As String
        Get
            Return _address4
        End Get
        Private Set(ByVal value As String)
            _address4 = value
        End Set
    End Property
    <ColumnMapping("Address5")> Public Property Address5() As String
        Get
            Return _address5
        End Get
        Private Set(ByVal value As String)
            _address5 = value
        End Set
    End Property
    <ColumnMapping("PostCode")> Public Property PostCode() As String
        Get
            Return _postcode
        End Get
        Private Set(ByVal value As String)
            _postcode = value
        End Set
    End Property
    <ColumnMapping("PhoneNumber")> Public Property PhoneNumber() As String
        Get
            Return _phoneNumber
        End Get
        Private Set(ByVal value As String)
            _phoneNumber = value
        End Set
    End Property
    <ColumnMapping("FaxNumber")> Public Property FaxNumber() As String
        Get
            Return _faxNumber
        End Get
        Private Set(ByVal value As String)
            _faxNumber = value
        End Set
    End Property
    <ColumnMapping("Manager")> Public Property Manager() As String
        Get
            Return _manager
        End Get
        Private Set(ByVal value As String)
            _manager = value
        End Set
    End Property
    <ColumnMapping("RegionCode")> Public Property RegionCode() As String
        Get
            Return _regionCode
        End Get
        Private Set(ByVal value As String)
            _regionCode = value
        End Set
    End Property
    <ColumnMapping("CountryCode")> Public Property CountryCode() As String
        Get
            Return _countryCode
        End Get
        Private Set(ByVal value As String)
            _countryCode = value
        End Set
    End Property
    <ColumnMapping("IsClosed")> Public Property IsClosed() As Boolean
        Get
            Return _isClosed
        End Get
        Private Set(ByVal value As Boolean)
            _isClosed = value
        End Set
    End Property
    <ColumnMapping("IsOpenMon")> Public Property IsOpenMon() As Boolean
        Get
            Return _isOpenMon
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenMon = value
        End Set
    End Property
    <ColumnMapping("IsOpenTue")> Public Property IsOpenTue() As Boolean
        Get
            Return _isOpenTue
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenTue = value
        End Set
    End Property
    <ColumnMapping("IsOpenWed")> Public Property IsOpenWed() As Boolean
        Get
            Return _isOpenWed
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenWed = value
        End Set
    End Property
    <ColumnMapping("IsOpenThu")> Public Property IsOpenThu() As Boolean
        Get
            Return _isOpenThu
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenThu = value
        End Set
    End Property
    <ColumnMapping("IsOpenFri")> Public Property IsOpenFri() As Boolean
        Get
            Return _isOpenFri
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenFri = value
        End Set
    End Property
    <ColumnMapping("IsOpenSat")> Public Property IsOpenSat() As Boolean
        Get
            Return _isOpenSat
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenSat = value
        End Set
    End Property
    <ColumnMapping("IsOpenSun")> Public Property IsOpenSun() As Boolean
        Get
            Return _isOpenSun
        End Get
        Private Set(ByVal value As Boolean)
            _isOpenSun = value
        End Set
    End Property
    <ColumnMapping("IsHeadOffice")> Public Property IsHeadOffice() As Boolean
        Get
            Return _isHeadOffice
        End Get
        Private Set(ByVal value As Boolean)
            _isHeadOffice = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    ''' <summary>
    ''' Returns store Id and name string representation for current store
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetIdName() As String

        Dim storeId As Integer = Parameter.GetInteger(100)
        Dim newStore As New Store(DataOperations.GetStore(storeId).Rows(0))
        Return newStore.Id & Space(1) & newStore.Name.Trim

    End Function

    ''' <summary>
    ''' Returns number of days in a week that current store is open
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDaysOpen() As Integer

        Dim storeId As Integer = Parameter.GetInteger(100)
        Dim newStore As New Store(DataOperations.GetStore(storeId).Rows(0))
        Dim days As Integer = 0
        If newStore.IsOpenMon Then days += 1
        If newStore.IsOpenTue Then days += 1
        If newStore.IsOpenWed Then days += 1
        If newStore.IsOpenThu Then days += 1
        If newStore.IsOpenFri Then days += 1
        If newStore.IsOpenSat Then days += 1
        If newStore.IsOpenSun Then days += 1
        Return days

    End Function

    ''' <summary>
    ''' Returns current store object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStoreCurrent() As Store

        Dim storeId As Integer = Parameter.GetInteger(100)
        Dim newStore As New Store(DataOperations.GetStore(storeId).Rows(0))
        Return newStore

    End Function

End Class
