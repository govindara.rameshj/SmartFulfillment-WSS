﻿Imports Oasys.Data

Friend Module DataOperations

    Friend Function GetParameter(ByVal id As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SystemGetParameter)
                com.AddParameter(My.Resources.Parameters.Id, id)
                Return com.ExecuteDataTable
            End Using
        End Using


    End Function

    Friend Function GetStore(ByVal id As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StoreGetStore)
                com.AddParameter(My.Resources.Parameters.Id, id)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetStoreSaleWeightActive(ByVal id As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StoreSaleWeightGetActiveForId)
                com.AddParameter(My.Resources.Parameters.Id, id)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetDates() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SystemDateGetDates)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

End Module
