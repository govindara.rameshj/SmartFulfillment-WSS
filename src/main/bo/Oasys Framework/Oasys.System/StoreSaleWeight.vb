﻿Imports Oasys.Core
Imports System.ComponentModel

<CLSCompliant(True)> Public Class StoreSaleWeight
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _dateActive As Date
    Private _week As Integer
    Private _value As Decimal
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("DateActive")> Public Property DateActive() As Date
        Get
            Return _dateActive
        End Get
        Private Set(ByVal value As Date)
            _dateActive = value
        End Set
    End Property
    <ColumnMapping("Week")> Public Property Week() As Integer
        Get
            Return _week
        End Get
        Private Set(ByVal value As Integer)
            _week = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Private Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region


    Public Shared Function GetActiveWeights(ByVal ids As Integer) As StoreSaleWeightCollection

        Return New StoreSaleWeightCollection(ids)

    End Function

End Class
