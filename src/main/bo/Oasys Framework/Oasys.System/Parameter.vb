﻿Imports Oasys.Core
Imports System.ComponentModel

<TableMapping("Parameter")> Public Class Parameter
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _description As String
    Private _integerValue As Nullable(Of Integer)
    Private _decimalValue As Nullable(Of Decimal)
    Private _stringValue As String
    Private _booleanValue As Boolean
    Private _valueType As Integer
    Private _isGlobal As Boolean
#End Region

#Region "Properties"
    <ColumnMapping("ParameterID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("LongValue")> Public Property IntegerValue() As Nullable(Of Integer)
        Get
            Return _integerValue
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _integerValue = value
        End Set
    End Property
    <ColumnMapping("DecimalValue")> Public Property DecimalValue() As Nullable(Of Decimal)
        Get
            Return _decimalValue
        End Get
        Private Set(ByVal value As Nullable(Of Decimal))
            _decimalValue = value
        End Set
    End Property
    <ColumnMapping("StringValue")> Public Property StringValue() As String
        Get
            Return _stringValue
        End Get
        Private Set(ByVal value As String)
            _stringValue = value
        End Set
    End Property
    <ColumnMapping("BooleanValue")> Public Property BooleanValue() As Boolean
        Get
            Return _booleanValue
        End Get
        Private Set(ByVal value As Boolean)
            _booleanValue = value
        End Set
    End Property
    <ColumnMapping("ValueType")> Public Property ValueType() As Integer
        Get
            Return _valueType
        End Get
        Private Set(ByVal value As Integer)
            _valueType = value
        End Set
    End Property
    <ColumnMapping("IsGlobal")> Public Property IsGlobal() As Boolean
        Get
            Return _isGlobal
        End Get
        Private Set(ByVal value As Boolean)
            _isGlobal = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    Public Shared Function GetInteger(ByVal id As Integer) As Integer
        Dim dr As DataRow = DataOperations.GetParameter(id).Rows(0)
        Return CInt(New Parameter(dr).IntegerValue)
    End Function

    Public Shared Function GetDecimal(ByVal id As Integer) As Decimal
        Dim dr As DataRow = DataOperations.GetParameter(id).Rows(0)
        Return CDec(New Parameter(dr).DecimalValue)
    End Function

    Public Shared Function GetString(ByVal id As Integer) As String
        Dim dr As DataRow = DataOperations.GetParameter(id).Rows(0)
        Return New Parameter(dr).StringValue
    End Function

    Public Shared Function GetBoolean(ByVal id As Integer) As Boolean
        Dim dr As DataRow = DataOperations.GetParameter(id).Rows(0)
        Return New Parameter(dr).BooleanValue
    End Function

End Class