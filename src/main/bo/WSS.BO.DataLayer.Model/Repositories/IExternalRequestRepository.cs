using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IExternalRequestRepository : IRepository
    {
        List<ExternalRequest> GetRequestsForProcessing(int retryPeriodMinutes);
        Int64 CreateRequest<T>(string type, T details) where T : IExternalRequestDetails;
        void UpdateRequest(Int64 requestId, int status);
        void UpdateRequest(Int64 requestId, int status, string errorDescription);
    }
}