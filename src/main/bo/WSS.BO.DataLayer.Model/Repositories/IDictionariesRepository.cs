using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IDictionariesRepository : IRepository
    {
        List<decimal> GetVatRatesListFromRetopt();
        string GetStoreId();
        int GetAbsoluteStoreId();
        string GetDefaultSystemCurrency();
        Settings GetSettings();
        string GetSettingStringValue(int parameterId);
        RetailOptions SelectRetailOptions();
        IDictionary<string, Syscod> SelectSystemCodes(string systemCodeType);
        IDictionary<string, SystemCode> SelectNewSystemCodes(string systemCodeType);
        int? GetVirtualCashierId(int tillNumber);
        List<DeliverySlot> GetDeliverySlots();
    }
}