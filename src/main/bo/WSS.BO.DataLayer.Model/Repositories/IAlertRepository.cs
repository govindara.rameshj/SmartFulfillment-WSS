using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IAlertRepository : IRepository
    {
        IList<Alert> GetUnauthorizedAlerts();
        int AuthorizeAlerts(IList<Alert> alerts, int userId);
    }
}