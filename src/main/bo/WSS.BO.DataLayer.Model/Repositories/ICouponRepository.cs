﻿using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface ICouponRepository : IRepository
    {
        CouponMaster GetCouponMaster(string couponMasterId);
        EventMaster GetEventByCouponNumber(string couponNumber);
    }
}
