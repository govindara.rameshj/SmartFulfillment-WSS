using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface ICashierBalanceRepository : IRepository
    {
        bool CashierBalanceExists(int periodId, int cashierId, string currencyId);
        void ApplyCashierBalanceUpdate(CashBalCashier update);

        bool CashierBalanceTenderExists(int tenderTypeId, int periodId, int cashierId, string currencyId);
        void ApplyCashierBalanceTenderUpdate(CashBalCashierTen update);

        bool CashierBalanceTenderVarianceExists(int tenderTypeId, int periodId, int cashierId, string currencyId, int tradingPeriodId);
        void ApplyCashierBalanceTenderVarianceUpdate(CashBalCashierTenVar update);
    }
}