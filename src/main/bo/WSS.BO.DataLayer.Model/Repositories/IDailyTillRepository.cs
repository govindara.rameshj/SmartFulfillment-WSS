using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IDailyTillRepository : IRepository
    {
        void InsertIntoDltots(Dltots dltots);
        Dltots SelectDailyTillTran(IDailyTillTranKey tran);
        bool IsDltotsExists(String sourceId, String source);
        bool IsDltotsExists(IDailyTillTranKey key);
        Dltots SelectDailyTotal(String sourceId);
        Dltots SelectDailyTotal(IDailyTillTranKey tran, string transactionCode);
        string SelectFirstDltotsOrderNumber(IDailyTillTranKey tran);
        int GetDailyTillTranCount(String sourceId, String source);
        Dltots SelectGeneratedDailyTillTran(String sourceTranNumber, String source, string transactionCode);
        void UpdateDltots(DltotsUpdate update);
        IDailyTillTranKey GetDailyTillTranKey(string sourceTransactionId, string source);

        void InsertIntoDlline(DailyTillLine dlline);
        IList<DailyTillLine> SelectDailyTillLines(IDailyTillTranKey tran);
        IList<DailyTillLine> SelectDailyTillLines(IDailyTillTranKey tran, string skuNumber);
        DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran);
        DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, string skuNumber);
        DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, int sequenceNumber);
        DailyTillLine SelectDailyTillLineBySourceItemId(IDailyTillTranKey tran, string sourceItemId);
        IList<DailyTillLine> SelectReversedDailyTillLines(IDailyTillTranKey tran, string skuNumber);
        DailyTillLine SelectDailyLineBySourceItemId(IDailyTillTranKey refundedDailyTillTranKey, string refundedTransactionItemId);
        DailyTillLine GetOriginalLineNumber(IDailyTillTranKey tran, string sourceItemId);

        void InsertIntoDlpaid(Dlpaid dlpaid);
        IList<Dlpaid> SelectDlpaids(IDailyTillTranKey tran);
        Dlpaid SelectDailyPayment(IDailyTillTranKey tran);
        Dlpaid SelectDailyPayment(IDailyTillTranKey tran, int tenderType);
        Dlpaid SelectDailyPayment(IDailyTillTranKey tran, int tenderType, bool isRefund);

        void InsertIntoDlrcus(DailyCustomer dlrcus);
        IList<DailyCustomer> SelectDlrcuses(IDailyTillTranKey tran);
        DailyCustomer SelectDailyCustomer(IDailyTillTranKey tran, int lineNumber);
        DailyCustomer SelectDailyCustomerWithNonZeroLineNumber(IDailyTillTranKey tran);
        DailyCustomer SelectDailyCustomersWithOriginalLineNumber(IDailyTillTranKey tran, int originalLineNumber);
        DailyCustomer SelectDailyCustomer(IDailyTillTranKey tran);
        int GetCountOfDailyCustomerWithNonZeroLineNumber(IDailyTillTranKey tran);

        void InsertIntoDlcomm(Dlcomm dlcomm);
        Dlcomm SelectDlcomm(IDailyTillTranKey tran);

        void InsertIntoDlGiftCard(DailyGiftCard giftCard);
        DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran);
        DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, string authorisationCode);
        DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber, decimal tenderAmount);
        DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber, decimal tenderAmount, string transaction);
        DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber);

        void InsertIntoDlevnt(Dlevnt dlevnt);
        IList<Dlevnt> SelectDlevnts(IDailyTillTranKey tran);
        Dlevnt SelectDailyEvent(IDailyTillTranKey tran);
        Dlevnt SelectDailyEvent(IDailyTillTranKey tran, string erosionType);
        Dlevnt SelectDailyEventBySequenceNumber(IDailyTillTranKey tran, string sequenceNumber);
        IList<Dlevnt> SelectDailyEventByDailyLinesSequenceNumber(IDailyTillTranKey tran, int sequenceNumber);

        void InsertIntoDlanas(Dlanas dlanas);
        IList<Dlanas> SelectDlanases(IDailyTillTranKey tran);

        void InsertIntoDlgift(DailyGiftToken dlgift);
        IList<DailyGiftToken> SelectDlgifts(IDailyTillTranKey tran);

        void InsertIntoDlocus(Dlocus dlocus);
        IList<Dlocus> SelectDlocuses(IDailyTillTranKey tran);

        void InsertIntoDlolin(Dlolin dlolin);
        IList<Dlolin> SelectDlolins(IDailyTillTranKey tran);
        Dlolin SelectDlolin(IDailyTillTranKey tran);
        Dlolin SelectDlolin(IDailyTillTranKey tran, short lineNumber);

        void InsertIntoDlreject(Dlreject dlreject);
        IList<Dlreject> SelectDlrejects(IDailyTillTranKey tran);
        Dlreject SelectDlreject(IDailyTillTranKey tran, string skuNumber);

        void InsertIntoDlcoupon(Dlcoupon dlcoupon);
        IList<Dlcoupon> SelectDlcoupons(IDailyTillTranKey tran);
        Dlcoupon SelectDailyCoupon(IDailyTillTranKey tran);

        /// <summary>
        /// Gets transaction number for the next transaction for given date and till number.
        /// </summary>
        /// <param name="date">Transaction date.</param>
        /// <param name="till">Transaction till number.</param>
        /// <returns>Number of next transaction.</returns>
        string GenerateNextTransactionNumber(string tillnumber);
        string GetOrderNumberByOriginalTransactionId(string refTransactionId, string source);
        string GetNextTillTranNumber();
        void SetOrderNumber(DailyTillTranKey dailyTillTranKey, string orderNumber);
        bool IsTransactionToCancelForOrder(string refundedTransactionId, string source);
    }
}