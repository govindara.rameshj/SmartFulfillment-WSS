using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface ICustomerOrderRepository : IRepository
    {
        void InsertIntoCorhdr(CustomerOrderHeader customerOrderHeader);
        CustomerOrderHeader SelectCorhdr(string orderNumber);
        string GetNextOrderNumber();
        void SetPreviousOrderNumber(string previousOrderNumber);

        void InsertIntoCorhdr4(CustomerOrderHeader4 customerOrderHeader4);
        CustomerOrderHeader4 SelectCorhdr4(string orderNumber);

        void InsertIntoCorlin(CustomerOrderLine customerOrderLin);
        CustomerOrderLine SelectCorlin(string orderNumber, string lineNumber);
        List<CustomerOrderLine> SelectCorlins(string orderNumber);

        void InsertIntoCorrefund(CustomerOrderRefund customerOrderRefund);
        List<CustomerOrderRefund> SelectCorrefunds(string orderNumber);
        List<CustomerOrderRefund> SelectCorrefunds(string orderNumber, string lineNumber);
        bool CheckIfTransactionIsCancellation(string orderNumber, IDailyTillTranKey dailyTillTranKey);

        void ApplyCorlinUpdate(CustomerOrderLine corlinUpdate);
        void ApplyCorhdrUpdate(CustomerOrderHeader corhdrUpdate);
        void ApplyCorhdr4Update(CustomerOrderHeader4 corhdr4Update);

        string GetCorlinLineNumberBySourceLineNumber(int sourceLineNumber, string orderNumber);

        void InsertIntoCorlin2(CustomerOrderLine2 corlin2);
        List<CustomerOrderLine2> SelectCorlins2(string orderNumber);

        void InsertIntoCortxt(CustomerOrderText cortxt);
        List<CustomerOrderText> SelectCortxts(string orderNumber);

        void InsertIntoCorhdr5(CustomerOrderHeader5 corhdr5);
        CustomerOrderHeader5 SelectCorhdr5(string orderNumber);

        bool CheckOrderIsDelivered(string orderNumber);
    }
}