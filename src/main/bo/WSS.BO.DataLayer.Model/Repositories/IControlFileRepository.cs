using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IControlFileRepository : IRepository
    {
        IList<ControlFile> GetControlFile(string fileTypeName, DateTime transactionDate, bool flag);
        void InsertControlFile(ControlFile controlFile);
        ControlFile GetLatestAvailableVersionRecord(string fileName);
        void Delete(string fileTypeName, string version);
        void UpdateLatestAvailableVersionRecord(ControlFile record);
    }
}
