﻿using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IRelatedItemRepository : IRepository
    {
        RelatedItem SelectRelatedItem(string packItemSku);
    }
}
