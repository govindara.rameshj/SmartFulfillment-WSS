using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IStockRepository : IRepository
    {
        SkuInfo GetSkuInfo(string skuNumber);
        StockMaster GetStockMaster(string skuNumber);
        decimal GetStockMasterPrice(string skuNumber);
        void ApplyStockUpdate(StockUpdate stockUpdate);
    }
}