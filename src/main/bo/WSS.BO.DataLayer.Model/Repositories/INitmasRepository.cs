using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface INitmasRepository : IRepository
    {
        bool NitmasStarted(DateTime nitmasPending);
        bool NitmasStopped(DateTime nitmasPending, int taskId);
        bool WasPickupExecuted(int virtualCashierNumber);
        bool IsPickupInProgress(int virtualCashierNumber);
        bool WasBankingExecuted(DateTime dateToCheck);
        bool IsBankingInProgress(DateTime dateToCheck);
        bool WasEodLockExecutedForDate(DateTime dateToCheck);
        List<GapWalk> SelectGapWalks(DateTime date);
        List<GapWalkReportLine> CreateGapWalkReport(DateTime reportDate);
    }
}
