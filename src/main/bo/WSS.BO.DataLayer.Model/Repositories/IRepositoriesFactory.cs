﻿namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IRepositoriesFactory
    {
        T Create<T>() where T : IRepository;
    }
}
