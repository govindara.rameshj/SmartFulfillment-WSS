﻿using System;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface ISafeRepository : IRepository
    {
        Safe GetSafe(DateTime currentDate);
    }
}