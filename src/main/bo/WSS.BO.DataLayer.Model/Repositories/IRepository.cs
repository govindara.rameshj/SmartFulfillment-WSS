﻿namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IRepository
    {
        void SetConnectionFactory(IRepositoryConnectionFactory repositoryConnectionFactory);
    }
}
