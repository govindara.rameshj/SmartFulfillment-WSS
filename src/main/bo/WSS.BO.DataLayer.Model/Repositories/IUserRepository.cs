using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IUserRepository: IRepository
    {
        int UserInsert(IUser user);
        int UserUpdate(IUser user);
        void UpdateSynchronizedWhen(int idUser);
        void UpdateSynchronizationFailedWhen(int idUser);
    }
}
