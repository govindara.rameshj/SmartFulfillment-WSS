﻿using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Repositories
{
    public interface IDeliveryChargeRepository: IRepository
    {
        DeliveryChargeContainer GetDeliveryChargeContainer();
    }
}
