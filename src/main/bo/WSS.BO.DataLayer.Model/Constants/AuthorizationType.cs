﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class AuthorizationType
    {
        public const string Online = "O";
        public const string Referral = "R";
    }
}
