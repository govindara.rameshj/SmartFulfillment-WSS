﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class ExternalRequestStatus
    {
        public const int New = 0;
        public const int Processed = 1;
        public const int Failed = 2;
        public const int Retry = 3;
    }
}
