﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class RestrictedItemConfirmationText
    {
        public const string RejectedCompletely = "Item cannot be sold to Customer.\r\nFailed Solvent Age Requirement.";
        public const string RejectedWithConfirmation = "Item cannot be sold to Customer.\r\nFailed to provide Proof of Age.";
    }
}
