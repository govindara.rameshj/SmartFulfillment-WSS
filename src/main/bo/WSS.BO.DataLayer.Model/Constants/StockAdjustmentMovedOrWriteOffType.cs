using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class StockAdjustmentMovedOrWriteOffType
    {
        public const string NormalStockType = "";
        public const string MovedStockType = "M";
        public const string WriteOffStockType = "W";
    }
}
