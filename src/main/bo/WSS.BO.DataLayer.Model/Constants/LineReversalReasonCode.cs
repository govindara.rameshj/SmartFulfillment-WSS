﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class LineReversalReasonCode
    {
        public const string NoLineReversal = "";
        public const string NotRequired = "01";
        public const string WrongScan = "02";
    }
}
