﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class NitmasTaskId
    {
        public const string NitmasStart = "005";
        public const string RollDates = "010";
        public const string Undefined405 = "405";
        public const string ProcessTransHostg = "300";
        public const string NitmasEnd = "426";
    }
}
