namespace WSS.BO.DataLayer.Model.Constants
{
    public static class TypeOfMovement
    {
        public const string NormalStockSaleItem = "01";  
        public const string NormalStockRefundItem = "02";
        public const string PssInStoreSaleItem = "03";
        public const string MarkdownStockSaleItem = "04";
        public const string BackdoorCollection = "05";
        public const string ClickAndCollectSale = "06";
        public const string ClickAndCollectRefund = "07";
        public const string PssSaleItem = "11";
        public const string PssRefundItem = "12";
        public const string SystemAdjustment91 = "91";
        public const string NormalStockAdjustmentWithPositiveAdjustmentQuantity = "32";
        public const string MarkdownStockAdjustmentWithPositiveAdjustmentQuantity = "67";
        public const string NormalStockAdjustmentWithNegativeAdjustmentQuantity = "31";
        public const string MarkdownStockAdjustmentWithNegativeAdjustmentQuantity = "66";
    }
}
