
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class ExternalRequestType
    {
        public const string Allocate = "Allocate";
        public const string Deallocate = "Deallocate";
        public const string AllocateDeallocate = "AllocateDeallocate";

        public const string NewConsignment = "NewConsignment";
        public const string UpdateConsignment = "UpdateConsignment";
        public const string CancelConsignment = "CancelConsignment";

        public const string InsertUser = "InsertUser";
        public const string UpdateUser = "UpdateUser";
        public const string DeleteUser = "DeleteUser";
    }

}
