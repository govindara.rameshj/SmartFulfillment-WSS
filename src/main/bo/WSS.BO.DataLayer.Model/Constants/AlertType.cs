﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class AlertType
    {
        public const byte CNC_new = 1;
        public const byte CNC_cancelled = 2;
        public const byte WHS_new = 3;
        public const byte WHS_cancelled = 4;
    }
}
