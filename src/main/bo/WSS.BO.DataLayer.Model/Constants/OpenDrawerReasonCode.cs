﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class OpenDrawerReasonCode
    {
        public const string CashPickup = "01";
        public const string CashExchange = "02";
        public const string SignOnOff = "03";
        public const string GiftVoucher = "04";
        public const string Other = "05";
        public const string Duress = "09";
    }
}
