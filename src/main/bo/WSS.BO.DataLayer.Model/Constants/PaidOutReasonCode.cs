﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class PaidOutReasonCode
    {
        public const int CustomerConcern = 1;
        public const int Stationery = 2;
        public const int BreakfastVoucher = 3;
        public const int SocialFund = 4;
        public const int Travel = 5;
        public const int Postage = 6;
        public const int MeetingFood = 7;
        public const int ColleagueWelfare = 8;
        public const int CouncilAccount = 9;
        public const int ChangeFund = 10;
        public const int Suspense = 11;
        public const int SafeUnders = 12;
        public const int Cleaning = 13;
        public const int Charity = 14;
        public const int RefundTill = 15;
        public const int HeadOfficeCheque = 16;
        public const int ForkliftFuel = 17;
        public const int ProjectLoan = 18;
    }
}
