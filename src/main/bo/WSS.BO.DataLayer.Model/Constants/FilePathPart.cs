﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class FilePathPart
    {
        public const string StorePlaceHolder = "{store}";
        public const string DatePlaceHolder = "{date}";
        public const string All = "*";
        public const string GroupExtractor = "(.*?)";
    }
}
