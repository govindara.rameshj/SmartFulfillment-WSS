﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class Global
    {
        public const int MiscTypesCount = 20;
        public const int LiveStoreIdBase = 8000;
        public const string NullEventCode = "000000";
        public const string NullOrderNumber = "000000";
        public const string NullUserCode = "000";
        public const string OvcOrderSource = "OV";
        public const string NullOriginalSaleStore = "000";
        public const int NullOriginalTenderType = 0;
        public const string NullOriginalTillNumber = "00";
        public const string NullRefundReasonCode = "00";
        public const string NullOriginalTransactionNumber = "0000";
        public const string LegacySource = "Legacy";
        public const int LegacySourcePrefixLength = 7;
        public const string BrandCode = "WIX";
        public const string NullPaymentCardDate = "0000";
    }
}