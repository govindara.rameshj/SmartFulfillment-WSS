﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class TenderType
    {
        public const int Cash = 1;
        public const int Change = 99;
        public const int CreditCard = 3; //also known as Just CrCd
        public const int AmericanExpress = 9;
        public const int Voucher = 6;
        public const int WebTender = 12;
        public const int Cheque = 2;
        public const int GiftCard = 13;
        public const int GiftToken = 7;
    }
}
