﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class TransactionTypeForCard
    {
        public const string SaleOfVoucher = "SA";
        public const string TenderInSale = "TS";
        public const string TenderInRefund = "TR";
        public const string RefundOfVoucher = "RR";
    }
}
