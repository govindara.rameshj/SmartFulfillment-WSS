﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class RefundReasonCode
    {
        public const string CustomerChangedMind = "01";
        public const string Damaged = "02";
        public const string WrongItem = "03";
        public const string PartsMissing = "04";
        public const string Faulty = "05";
        public const string Other = "06";
        public const string WebOrder = "07";
    }
}
