﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class SystemCodeType
    {
        public const string PaidOutReason = "M-";
        public const string MiscInReason = "M+";
        public const string OpenDrawerReason = "OD";
        public const string RefundReason = "RF";
        public const string PriceOverrideReason = "OV";
    }
}
