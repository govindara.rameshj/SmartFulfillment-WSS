﻿
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class MiscInReasonCode
    {
        public const int BadCheque = 1;
        public const int SocialFund = 2;
        public const int Suspense = 3;
        public const int CarParkTrading = 4;
        public const int SafeOvers = 5;
        public const int PhoneBox = 6;
        public const int VendingMachine = 7;
        public const int Rent = 8;
        public const int DisplaySale = 9;
        public const int SafeChange = 10;
        public const int CouncilAccount = 11;
        public const int GiftCard = 12;
        public const int Charity = 14;
        public const int RefundTill = 15;
        public const int HeadOfficeCheque = 16;
        public const int RepayProjectLoan = 18;
        public const int GiftVoucher = 19;
        public const int Deposits = 20;                           
    }
}
