﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class EventType
    {
        public const string QuantutyBreak = "QS";
        public const string DealGroup = "DG";
        public const string Multibuy = "MS";
        public const string HierarchySpend = "HS";
    }
}
