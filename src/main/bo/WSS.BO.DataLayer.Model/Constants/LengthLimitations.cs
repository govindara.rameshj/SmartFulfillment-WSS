﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class LengthLimitations
    {
        public const int DlrcusName = 30;
        public const int DlrcusTown = 30;
        public const int DlrcusAddress1 = 30;
        public const int DlrcusAddress2 = 30;
        public const int DlrcusHouseNumber = 4;
        public const int DlrcusPostcode = 8;
        public const int DlrcusEmail = 100;
        public const int DlrcusMobilePhone = 15;
        public const int DlrcusWorkPhone = 20;
        public const int DlrcusPhone = 15;
        public const int DlolinCompetitorName = 30;
        public const int DeliveryInstructionMaxLength = 200;
        public const int CouponBarcodeLength = 17;
    }
}
