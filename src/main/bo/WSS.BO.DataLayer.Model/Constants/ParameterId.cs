namespace WSS.BO.DataLayer.Model.Constants
{
    public static class ParameterId
    {
        public const int ApiAddressId = 7000;
        public const int MaxRetriesNumberId = 7005;
        public const int RetryPeriodMinutesId = 7006;
        public const int ReleaseVersion = 0;
        public const int GiftCardSku = 979;
        public const int DeliveruChargeSku = 169;
        public const int NotifyDeliveryService = 50;
        public const int StaAuditUploadFolderPath = 505;
        public const int StaAuditUploadDateFormat = 506;
        public const int WickesCommsPath = 914;
        public const int QuantityBreakEventDiscountCode = 6666;
        public const int HierarchySpendEventDiscountCode = 6667;
        public const int UserManagmentServiceUrl = 7007;
    }
}
