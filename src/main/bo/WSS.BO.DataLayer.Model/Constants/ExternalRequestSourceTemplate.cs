
namespace WSS.BO.DataLayer.Model.Constants
{
    public static class ExternalRequestSourceTemplate
    {
        public const string New = "New order {0}";
        public const string Refund = "Refund for order {0}";
        public const string UpdateDate = "Update date for order {0}";
        public const string UpdateDetails = "Update details for order {0}";

        public const string InsertUser = "New User {0}";
        public const string UpdateUser = "Update User {0}";
        public const string DeleteUser = "Delete User {0}";

    }

}
