using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class StockAdjustmentCodeNumber
    {
        public const string NormalAdjustment = "02";
        public const string Damages = "53";
    }
}
