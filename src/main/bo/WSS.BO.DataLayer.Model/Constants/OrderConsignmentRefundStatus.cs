﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class OrderConsignmentRefundStatus
    {
        public const int None = 0;

        public const int RefundFailedConnection = 10;
        public const int RefundFailedState = 20;
        public const int RefundFailedData = 30;

        public const int RefundOk = 99;

        public const int Created = 100;
        public const int CreatedOnHold = 105;

        public const int NotifyFailedConnection = 110;
        public const int NotifyFailedState = 120;
        public const int NotifyFailedData = 130;
        public const int NotifyOk = 150;

        public const int StatusUpdateFailedConnection = 160;
        public const int StatusUpdateFailedState = 170;
        public const int StatusUpdateFailedData = 180;
        public const int StatusUpdateOk = 199;
    }
}
