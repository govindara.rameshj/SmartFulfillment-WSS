﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class SkuSaleTypeAttribute
    {
        public const string BasicItem = "B";
        public const string PerformanceItem = "P";
        public const string AestheticItem = "A";
        public const string ShowroomItem = "S";
        public const string GoodPalletItem = "G";
        public const string ReducedPalletItem = "R";
        public const string DeliveryItem = "D";
        public const string I = "I"; // it is not described in DB help
        public const string W = "W"; // it is not described in DB help
        public const string V = "V"; // it is not described in DB help
    }
}
