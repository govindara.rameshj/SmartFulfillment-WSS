﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class PriceOverrideReasonCode
    {
        public const string NoPriceOverride = "00";
        public const string DamagedGoods = "01";
        public const string IncorrectPriceDisplayed = "02";
        public const string RefundPriceDifference = "03";
        public const string WrongPosSign = "04";
        public const string WrongBookletPrice = "05";
        public const string WebReturn = "06";
        public const string PriceMatchOrPromise = "07";
        public const string HdcReturn = "08";
        public const string ColleagueRefund = "09";
        public const string ManagersDiscretion = "10";
        public const string IncorrectDayPrice = "11";
        public const string TemporaryDealGroup = "12";
        public const string MarkdownDamages = "53";
    }
}
