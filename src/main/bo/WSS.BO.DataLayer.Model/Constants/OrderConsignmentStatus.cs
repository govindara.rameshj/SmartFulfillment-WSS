﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class OrderConsignmentStatus
    {
        public const int Null = -1;

        public const int OrderCreated = 0;
        public const int OrderFailedConnection = 10;
        public const int OrderFailedState = 20;
        public const int OrderFailedData = 30;
        public const int OrderOk = 99;

        public const int DeliveryRequest = 100;
        public const int DeliveryRequestFailedConnection = 110;
        public const int DeliveryRequestFailedState = 120;
        public const int DeliveryRequestFailedData = 130;

        public const int DeliveryRequestAccepted = 200;
        public const int FulfilRequestFailedConnection = 210;
        public const int FulfilRequestFailedState = 220;
        public const int FulfilRequestFailedData = 230;

        public const int IbtOutAllStock = 300;
        public const int IbtOutFailedConnection = 310;
        public const int IbtOutFailedState = 320;
        public const int IbtOutFailedData = 330;
        public const int IbtInAllStock = 399;

        public const int ReceiptNotifyCreate = 400;
        public const int ReceiptNotifyFailedConnection = 410;
        public const int ReceiptNotifyFailedState = 420;
        public const int ReceiptNotifyFailedData = 430;
        public const int ReceiptNotifyRecorded = 450;
        public const int ReceiptStatusFailedConnection = 460;
        public const int ReceiptStatusFailedState = 470;
        public const int ReceiptStatusFailedData = 480;
        public const int ReceiptStatusOk = 499;

        public const int PickingListCreated = 500;
        public const int PickingNotifyFailedConnection = 510;
        public const int PickingNotifyFailedState = 520;
        public const int PickingNotifyFailedData = 530;
        public const int PickingNotifyOk = 550;
        public const int PickingStatusFailedConnection = 560;
        public const int PickingStatusFailedState = 570;
        public const int PickingStatusFailedData = 580;
        public const int PickingStatusOk = 599;

        public const int PickingConfirmCreated = 600;
        public const int PickingConfirmNotifyFailedConnection = 610;
        public const int PickingConfirmNotifyFailedState = 620;
        public const int PickingConfirmNotifyFailedData = 630;
        public const int PickingConfirmNotifyOk = 650;
        public const int PickingConfirmStatusFailedConnection = 660;
        public const int PickingConfirmStatusFailedState = 670;
        public const int PickingConfirmStatusFailedData = 680;
        public const int PickingConfirmStatusOk = 699;

        public const int DespatchCreated = 700;
        public const int DespatchNotifyFailedConnection = 710;
        public const int DespatchNotifyFailedState = 720;
        public const int DespatchNotifyFailedData = 730;
        public const int DespatchNotifyOk = 750;
        public const int DespatchStatusFailedConnection = 760;
        public const int DespatchStatusFailedState = 770;
        public const int DespatchStatusFailedData = 780;
        public const int DespatchStatusOk = 799;

        public const int UndeliveredCreated = 800;
        public const int UndeliveredNotifyFailedConnection = 810;
        public const int UndeliveredNotifyFailedState = 820;
        public const int UndeliveredNotifyFailedData = 830;
        public const int UndeliveredNotifyOk = 850;
        public const int UndeliveredStatusFailedConnection = 860;
        public const int UndeliveredStatusFailedState = 870;
        public const int UndeliveredStatusFailedData = 880;
        public const int UndeliveredStatusOk = 899;

        public const int DeliveredCreated = 900;
        public const int DeliveredNotifyFailedConnection = 910;
        public const int DeliveredNotifyFailedState = 920;
        public const int DeliveredNotifyFailedData = 930;
        public const int DeliveredNotifyOk = 950;
        public const int DeliveredStatusFailedConnection = 960;
        public const int DeliveredStatusFailedState = 970;
        public const int DeliveredStatusFailedData = 980;
        public const int DeliveredStatusOk = 999;

        public const int Legacy = 9999;
    }
}
