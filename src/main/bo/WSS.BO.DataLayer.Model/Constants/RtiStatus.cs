﻿using System;

namespace WSS.BO.DataLayer.Model.Constants
{
    public static class RtiStatus
    {
        public const string NotSet = "";
        public const string NotSent = "N";
        public const string ToBeSent = "S";
    }
}
