﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class DeliverySlotType
    {
        public const string Weekday = "1";
        public const string Saturday = "3";
    }
}
