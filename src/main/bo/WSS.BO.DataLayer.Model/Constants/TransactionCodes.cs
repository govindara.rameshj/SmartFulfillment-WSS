﻿namespace WSS.BO.DataLayer.Model.Constants
{
    public static class TransactionCodes
    {
        public const string Sale = "SA";
        public const string Refund = "RF";
        public const string MiscIncome = "M+";
        public const string MiscIncomeCorrection = "C+";
        public const string PaidOut = "M-";
        public const string PaidOutCorrection = "C-";
        public const string OpenDrawer = "OD";
        public const string SignOn = "CO";
        public const string SignOff = "CC";
    }
}
