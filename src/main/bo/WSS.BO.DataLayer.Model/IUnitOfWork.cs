﻿using System;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Model
{
    public interface IUnitOfWork : IRepositoriesFactory, IDisposable
    {
        void Commit();
        void Rollback();
        void SetDeadlockPriority(string deadlockPriority);
    }
}
