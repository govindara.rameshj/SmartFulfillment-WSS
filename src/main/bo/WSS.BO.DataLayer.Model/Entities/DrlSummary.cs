using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    // DRL Summary (header) Record - 1 per DRL
    [TableName("DRLSUM")]
    public class DrlSummary : ISyncronizedToRti
    {
        public DrlSummary()
        {
            Value = 0M;
            ClassNumber = "00";
            IsAlreadyCommedToHeadOffice = false;
            IsReceivedFromBbcOrder = false;
            AssignedPoReleaseNumber = 0;
            IsNeededToBePrinted = false;
            IsProjectSalesDcOrder = false;
            EmployeeId = 0;
            Tkey = 0;
        }

        // DRL Number - assigned by the system 
        [MapField("NUMB")]
        public string DrlNumber { get; set; }

        // 0=receipt 1=IBT in 2=IBT out 3=return
        [MapField("TYPE")]
        public string Type { get; set; }

        // Total value of the DRL (all positive) 
        [MapField("VALU")]
        public decimal Value { get; set; }

        // Date DRL Number was assigned
        [MapField("DATE1")]
        public DateTime? CreatedDate { get; set; }

        // W03 Class number of 1st item in file - No longer Used
        [MapField("CLAS")]
        public string ClassNumber { get; set; }

        // ON = Already Commed to Head Office
        [MapField("COMM")]
        public bool IsAlreadyCommedToHeadOffice { get; set; }

        // Direct key to PURHDR or RETHDR (DIG key)
        [MapField("TKEY")]
        public int Tkey { get; set; }
 
        // Entry Person initials
        [MapField("INIT")]
        public string EntryPersonInitials { get; set; }

        // Free form comment/information 
        [MapField("INFO")]
        public string Comment { get; set; }

        // Received from a BBC order
        [MapField("0BBC")]
        public bool IsReceivedFromBbcOrder { get; set; }

        // Supplier Number of the order
        [MapField("0SUP")]
        public string OrderSupplierNumber { get; set; }

        // Assigned Consignment note number
        [MapField("0CON")]
        public string AssignedConsignmentNoteNumber { get; set; }
  
        // Purchase Order Number
        [MapField("0PON")]
        public string PurchaseOrderNumber { get; set; }

        // Purchase Order date
        [MapField("0DAT")]
        public DateTime? PurchaseOrderDate { get; set; }

        // Assigned P/O Release Number
        [MapField("0REL")]
        public int AssignedPoReleaseNumber { get; set; }

        // SOQ Control Number of the purchase Order
        [MapField("0SOQ")]
        public string PurchaseOrderSoqControlNumber { get; set; }

        // Supplier Delivery Note number 1, BBC = issue number - first 6
        [MapField("0DL1")]
        public string SupplierDeliveryNote1 { get; set; }

        // Supplier Delivery Note number 2, BBC = 1st character  Y=Matched N=Not matched
        [MapField("0DL2")]
        public string SupplierDeliveryNote2 { get; set; }
  
        // Supplier Delivery Note number 3, BBC = 1st 6 characters  "IMPORT"
        [MapField("0DL3")]
        public string SupplierDeliveryNote3 { get; set; }
  
        // Supplier Delivery Note number 4, BBC = 1st 6 characters  "COMMED"
        [MapField("0DL4")]
        public string SupplierDeliveryNote4 { get; set; }

        // Supplier Delivery Note number 5 
        [MapField("0DL5")]
        public string SupplierDeliveryNote5 { get; set; }

        // Supplier Delivery Note number 6
        [MapField("0DL6")]
        public string SupplierDeliveryNote6 { get; set; }

        // Supplier Delivery Note number 7
        [MapField("0DL7")]
        public string SupplierDeliveryNote7 { get; set; }
  
        // Supplier Delivery Note number 8
        [MapField("0DL8")]
        public string SupplierDeliveryNote8 { get; set; }

        // Supplier Delivery Note number 9
        [MapField("0DL9")]
        public string SupplierDeliveryNote9 { get; set; }
  
        // Store Number (in=from  out=to)
        [MapField("1STR")]
        public string StoreNumber { get; set; }
  
        // IN = from store DRL Number (keyed in)
        [MapField("1IBT")]
        public string SourceStoreDrlNumber { get; set; }

        // Needs to be printed
        [MapField("1PRT")]
        public bool IsNeededToBePrinted { get; set; }

        // Supplier Number 
        [MapField("3SUP")]
        public string SupplierNumber  { get; set; }

        // Date Return Entered
        [MapField("3DAT")]
        public DateTime? DateReturnEntered { get; set; }

        // Was Original P/O No. - Now Return Number
        [MapField("3PON")]
        public string OriginalPoNumberOrReturnNumber { get; set; }

        // Consignment note number for IBT IN
        [MapField("1CON")]
        public string ConsignmentNoteNumber { get; set; }

        // This is a Project Sales DC Order
        [MapField("IPSO")]
        public bool IsProjectSalesDcOrder { get; set; }

        // RTI Flag
        [MapField("RTI")]
        public string RtiFlag { get; set; }

        // no info
        [MapField("EmployeeId")]
        public int EmployeeId { get; set; }
    }
}
