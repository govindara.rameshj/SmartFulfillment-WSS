﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CashBalCashier")]
    public class CashBalCashier
    {
        public CashBalCashier()
        {
            GrossSalesAmount = 0;
            DiscountAmount = 0;
            SalesCount = 0;
            SalesAmount = 0;
            SalesCorrectCount = 0;
            SalesCorrectAmount = 0;
            RefundCount = 0;
            RefundAmount = 0;
            RefundCorrectCount = 0;
            RefundCorrectAmount = 0;
            NumTransactions = 0;
            NumCorrections = 0;
            NumVoids = 0;
            NumOpenDrawer = 0;
            NumLinesReversed = 0;
            NumLinesSold = 0;
            NumLinesScanned = 0;
            ExchangeRate = 0;
            ExchangePower = 0;
        }

        public int CashierId { get; set; }

        [MapField("PeriodID")]
        public int PeriodId { get; set; }

        [MapField("CurrencyID")]
        public string CurrencyId { get; set; }

        public string Comment { get; set; }

        public decimal GrossSalesAmount { get; set; }
        public decimal DiscountAmount { get; set; }

        public decimal SalesCount { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal SalesCorrectCount { get; set; }
        public decimal SalesCorrectAmount { get; set; }

        public decimal RefundCount { get; set; }
        public decimal RefundAmount { get; set; }
        public decimal RefundCorrectCount { get; set; }
        public decimal RefundCorrectAmount { get; set; }

        public int NumVoids { get; set; }
        public int NumOpenDrawer { get; set; }
        public int NumLinesReversed { get; set; }
        public int NumLinesSold { get; set; }
        public int NumLinesScanned { get; set; }
        public int NumTransactions { get; set; }
        public int NumCorrections { get; set; }

        [MapField("FloatIssued")]
        public decimal FloatIssued { get; set; }
        [MapField("FloatReturned")]
        public decimal FloatReturned { get; set; }
        public decimal FloatVariance { get; set; }

        [MapField("ExchangeRate")]
        public decimal ExchangeRate { get; set; }
        [MapField("ExchangePower")]
        public decimal ExchangePower { get; set; }

        #region MiscIncomeCountXX

        private readonly decimal[] _miscIncomeCounts = new decimal[Global.MiscTypesCount + 1]; //We dont use element with index 0.

        public void SetMiscIncomeCount(int index, decimal value)
        {
            _miscIncomeCounts[index] = value;
        }

        public decimal GetMiscIncomeCount(int index)
        {
            return _miscIncomeCounts[index];
        }

        public decimal[] GetMiscIncomeCountsArray()
        {
            return _miscIncomeCounts;
        }

        [MapField("MiscIncomeCount01")]
        public decimal MiscIncomeCount01
        {
            get { return _miscIncomeCounts[1]; }
            set { _miscIncomeCounts[1] = value; }
        }
        public decimal MiscIncomeCount02
        {
            get { return _miscIncomeCounts[2]; }
            set { _miscIncomeCounts[2] = value; }
        }
        public decimal MiscIncomeCount03
        {
            get { return _miscIncomeCounts[3]; }
            set { _miscIncomeCounts[3] = value; }
        }
        public decimal MiscIncomeCount04
        {
            get { return _miscIncomeCounts[4]; }
            set { _miscIncomeCounts[4] = value; }
        }
        public decimal MiscIncomeCount05
        {
            get { return _miscIncomeCounts[5]; }
            set { _miscIncomeCounts[5] = value; }
        }
        public decimal MiscIncomeCount06
        {
            get { return _miscIncomeCounts[6]; }
            set { _miscIncomeCounts[6] = value; }
        }
        public decimal MiscIncomeCount07
        {
            get { return _miscIncomeCounts[7]; }
            set { _miscIncomeCounts[7] = value; }
        }
        public decimal MiscIncomeCount08
        {
            get { return _miscIncomeCounts[8]; }
            set { _miscIncomeCounts[8] = value; }
        }
        public decimal MiscIncomeCount09
        {
            get { return _miscIncomeCounts[9]; }
            set { _miscIncomeCounts[9] = value; }
        }
        public decimal MiscIncomeCount10
        {
            get { return _miscIncomeCounts[10]; }
            set { _miscIncomeCounts[10] = value; }
        }
        public decimal MiscIncomeCount11
        {
            get { return _miscIncomeCounts[11]; }
            set { _miscIncomeCounts[11] = value; }
        }
        public decimal MiscIncomeCount12
        {
            get { return _miscIncomeCounts[12]; }
            set { _miscIncomeCounts[12] = value; }
        }
        public decimal MiscIncomeCount13
        {
            get { return _miscIncomeCounts[13]; }
            set { _miscIncomeCounts[13] = value; }
        }
        public decimal MiscIncomeCount14
        {
            get { return _miscIncomeCounts[14]; }
            set { _miscIncomeCounts[14] = value; }
        }
        public decimal MiscIncomeCount15
        {
            get { return _miscIncomeCounts[15]; }
            set { _miscIncomeCounts[15] = value; }
        }
        public decimal MiscIncomeCount16
        {
            get { return _miscIncomeCounts[16]; }
            set { _miscIncomeCounts[16] = value; }
        }
        public decimal MiscIncomeCount17
        {
            get { return _miscIncomeCounts[17]; }
            set { _miscIncomeCounts[17] = value; }
        }
        public decimal MiscIncomeCount18
        {
            get { return _miscIncomeCounts[18]; }
            set { _miscIncomeCounts[18] = value; }
        }
        public decimal MiscIncomeCount19
        {
            get { return _miscIncomeCounts[19]; }
            set { _miscIncomeCounts[19] = value; }
        }
        public decimal MiscIncomeCount20
        {
            get { return _miscIncomeCounts[20]; }
            set { _miscIncomeCounts[20] = value; }
        }

        #endregion

        #region MiscIncomeValueXX

        private readonly decimal[] _miscIncomeValues = new decimal[Global.MiscTypesCount + 1]; //We dont use element with index 0.

        public void SetMiscIncomeValue(int index, decimal value)
        {
            _miscIncomeValues[index] = value;
        }

        public decimal GetMiscIncomeValue(int index)
        {
            return _miscIncomeValues[index];
        }

        public decimal[] GetMiscIncomeValuesArray()
        {
            return _miscIncomeValues;
        }

        public decimal MiscIncomeValue01
        {
            get { return _miscIncomeValues[1]; }
            set { _miscIncomeValues[1] = value; }
        }
        public decimal MiscIncomeValue02
        {
            get { return _miscIncomeValues[2]; }
            set { _miscIncomeValues[2] = value; }
        }
        public decimal MiscIncomeValue03
        {
            get { return _miscIncomeValues[3]; }
            set { _miscIncomeValues[3] = value; }
        }
        public decimal MiscIncomeValue04
        {
            get { return _miscIncomeValues[4]; }
            set { _miscIncomeValues[4] = value; }
        }
        public decimal MiscIncomeValue05
        {
            get { return _miscIncomeValues[5]; }
            set { _miscIncomeValues[5] = value; }
        }
        public decimal MiscIncomeValue06
        {
            get { return _miscIncomeValues[6]; }
            set { _miscIncomeValues[6] = value; }
        }
        public decimal MiscIncomeValue07
        {
            get { return _miscIncomeValues[7]; }
            set { _miscIncomeValues[7] = value; }
        }
        public decimal MiscIncomeValue08
        {
            get { return _miscIncomeValues[8]; }
            set { _miscIncomeValues[8] = value; }
        }
        public decimal MiscIncomeValue09
        {
            get { return _miscIncomeValues[9]; }
            set { _miscIncomeValues[9] = value; }
        }
        public decimal MiscIncomeValue10
        {
            get { return _miscIncomeValues[10]; }
            set { _miscIncomeValues[10] = value; }
        }
        public decimal MiscIncomeValue11
        {
            get { return _miscIncomeValues[11]; }
            set { _miscIncomeValues[11] = value; }
        }
        public decimal MiscIncomeValue12
        {
            get { return _miscIncomeValues[12]; }
            set { _miscIncomeValues[12] = value; }
        }
        public decimal MiscIncomeValue13
        {
            get { return _miscIncomeValues[13]; }
            set { _miscIncomeValues[13] = value; }
        }
        public decimal MiscIncomeValue14
        {
            get { return _miscIncomeValues[14]; }
            set { _miscIncomeValues[14] = value; }
        }
        public decimal MiscIncomeValue15
        {
            get { return _miscIncomeValues[15]; }
            set { _miscIncomeValues[15] = value; }
        }
        public decimal MiscIncomeValue16
        {
            get { return _miscIncomeValues[16]; }
            set { _miscIncomeValues[16] = value; }
        }
        public decimal MiscIncomeValue17
        {
            get { return _miscIncomeValues[17]; }
            set { _miscIncomeValues[17] = value; }
        }
        public decimal MiscIncomeValue18
        {
            get { return _miscIncomeValues[18]; }
            set { _miscIncomeValues[18] = value; }
        }
        public decimal MiscIncomeValue19
        {
            get { return _miscIncomeValues[19]; }
            set { _miscIncomeValues[19] = value; }
        }
        public decimal MiscIncomeValue20
        {
            get { return _miscIncomeValues[20]; }
            set { _miscIncomeValues[20] = value; }
        }

        #endregion

        #region MiscOutCountXX

        private readonly decimal[] _miscOutCounts = new decimal[Global.MiscTypesCount + 1]; //We dont use element with index 0.

        public void SetMiscOutCount(int index, decimal value)
        {
            _miscOutCounts[index] = value;
        }

        public decimal GetMiscOutCount(int index)
        {
            return _miscOutCounts[index];
        }

        public decimal[] GetMiscOutCountsArray()
        {
            return _miscOutCounts;
        }

        public decimal MisOutCount01
        {
            get { return _miscOutCounts[1]; }
            set { _miscOutCounts[1] = value; }
        }
        public decimal MisOutCount02
        {
            get { return _miscOutCounts[2]; }
            set { _miscOutCounts[2] = value; }
        }
        public decimal MisOutCount03
        {
            get { return _miscOutCounts[3]; }
            set { _miscOutCounts[3] = value; }
        }
        public decimal MisOutCount04
        {
            get { return _miscOutCounts[4]; }
            set { _miscOutCounts[4] = value; }
        }
        public decimal MisOutCount05
        {
            get { return _miscOutCounts[5]; }
            set { _miscOutCounts[5] = value; }
        }
        public decimal MisOutCount06
        {
            get { return _miscOutCounts[6]; }
            set { _miscOutCounts[6] = value; }
        }
        public decimal MisOutCount07
        {
            get { return _miscOutCounts[7]; }
            set { _miscOutCounts[7] = value; }
        }
        public decimal MisOutCount08
        {
            get { return _miscOutCounts[8]; }
            set { _miscOutCounts[8] = value; }
        }
        public decimal MisOutCount09
        {
            get { return _miscOutCounts[9]; }
            set { _miscOutCounts[9] = value; }
        }
        public decimal MisOutCount10
        {
            get { return _miscOutCounts[10]; }
            set { _miscOutCounts[10] = value; }
        }
        public decimal MisOutCount11
        {
            get { return _miscOutCounts[11]; }
            set { _miscOutCounts[11] = value; }
        }
        public decimal MisOutCount12
        {
            get { return _miscOutCounts[12]; }
            set { _miscOutCounts[12] = value; }
        }
        public decimal MisOutCount13
        {
            get { return _miscOutCounts[13]; }
            set { _miscOutCounts[13] = value; }
        }
        public decimal MisOutCount14
        {
            get { return _miscOutCounts[14]; }
            set { _miscOutCounts[14] = value; }
        }
        public decimal MisOutCount15
        {
            get { return _miscOutCounts[15]; }
            set { _miscOutCounts[15] = value; }
        }
        public decimal MisOutCount16
        {
            get { return _miscOutCounts[16]; }
            set { _miscOutCounts[16] = value; }
        }
        public decimal MisOutCount17
        {
            get { return _miscOutCounts[17]; }
            set { _miscOutCounts[17] = value; }
        }
        public decimal MisOutCount18
        {
            get { return _miscOutCounts[18]; }
            set { _miscOutCounts[18] = value; }
        }
        public decimal MisOutCount19
        {
            get { return _miscOutCounts[19]; }
            set { _miscOutCounts[19] = value; }
        }
        public decimal MisOutCount20
        {
            get { return _miscOutCounts[20]; }
            set { _miscOutCounts[20] = value; }
        }

        #endregion

        #region MiscOutValueXX

        private readonly decimal[] _miscOutValues = new decimal[Global.MiscTypesCount + 1]; //We dont use element with index 0.

        public void SetMiscOutValue(int index, decimal value)
        {
            _miscOutValues[index] = value;
        }

        public decimal GetMiscOutValue(int index)
        {
            return _miscOutValues[index];
        }

        public decimal[] GetMiscOutValuesArray()
        {
            return _miscOutValues;
        }

        public decimal MisOutValue01
        {
            get { return _miscOutValues[1]; }
            set { _miscOutValues[1] = value; }
        }
        public decimal MisOutValue02
        {
            get { return _miscOutValues[2]; }
            set { _miscOutValues[2] = value; }
        }
        public decimal MisOutValue03
        {
            get { return _miscOutValues[3]; }
            set { _miscOutValues[3] = value; }
        }
        public decimal MisOutValue04
        {
            get { return _miscOutValues[4]; }
            set { _miscOutValues[4] = value; }
        }
        public decimal MisOutValue05
        {
            get { return _miscOutValues[5]; }
            set { _miscOutValues[5] = value; }
        }
        public decimal MisOutValue06
        {
            get { return _miscOutValues[6]; }
            set { _miscOutValues[6] = value; }
        }
        public decimal MisOutValue07
        {
            get { return _miscOutValues[7]; }
            set { _miscOutValues[7] = value; }
        }
        public decimal MisOutValue08
        {
            get { return _miscOutValues[8]; }
            set { _miscOutValues[8] = value; }
        }
        public decimal MisOutValue09
        {
            get { return _miscOutValues[9]; }
            set { _miscOutValues[9] = value; }
        }
        public decimal MisOutValue10
        {
            get { return _miscOutValues[10]; }
            set { _miscOutValues[10] = value; }
        }
        public decimal MisOutValue11
        {
            get { return _miscOutValues[11]; }
            set { _miscOutValues[11] = value; }
        }
        public decimal MisOutValue12
        {
            get { return _miscOutValues[12]; }
            set { _miscOutValues[12] = value; }
        }
        public decimal MisOutValue13
        {
            get { return _miscOutValues[13]; }
            set { _miscOutValues[13] = value; }
        }
        public decimal MisOutValue14
        {
            get { return _miscOutValues[14]; }
            set { _miscOutValues[14] = value; }
        }
        public decimal MisOutValue15
        {
            get { return _miscOutValues[15]; }
            set { _miscOutValues[15] = value; }
        }
        public decimal MisOutValue16
        {
            get { return _miscOutValues[16]; }
            set { _miscOutValues[16] = value; }
        }
        public decimal MisOutValue17
        {
            get { return _miscOutValues[17]; }
            set { _miscOutValues[17] = value; }
        }
        public decimal MisOutValue18
        {
            get { return _miscOutValues[18]; }
            set { _miscOutValues[18] = value; }
        }
        public decimal MisOutValue19
        {
            get { return _miscOutValues[19]; }
            set { _miscOutValues[19] = value; }
        }
        public decimal MisOutValue20
        {
            get { return _miscOutValues[20]; }
            set { _miscOutValues[20] = value; }
        }

        #endregion
    }
}
