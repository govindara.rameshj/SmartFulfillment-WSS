using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SUPMAS")]
    public class SupplierMaster
    {
        [MapField("SUPN")]
        public string SupplierNumber { get; set; }
        [MapField("ALPH")]
        public string AlphaKey { get; set; }
        [MapField("NAME")]
        public string SupplierName { get; set; }
        [MapField("DELC")]
        public bool DeletedByHeadOffice { get; set; }
        [MapField("HLIN")]
        public string HelpLineNumber { get; set; }
        [MapField("MERC")]
        public string PrimaryMerchant { get; set; }
        [MapField("TYPE")]
        public string SupplierTypeCode { get; set; }
        [MapField("ODNO")]
        public int OrderDepotNumber { get; set; }
        [MapField("BBCN")]
        public string BbcSiteNumber { get; set; }
        [MapField("QCTL")]
        public int SoqControlNumber { get; set; }
        [MapField("QFLG")]
        public bool ProcessSoq { get; set; }
        [MapField("RDNO")]
        public int ReturnsDepotNumber { get; set; }
        [MapField("DLPO")]
        public DateTime LastPurchaseOrderDate { get; set; }
        [MapField("DLRE")]
        public DateTime LastReceiptDate { get; set; }
        [MapField("OPON")]
        public decimal CurrentOutstandingPurchaseOrdersNumber { get; set; }
        [MapField("OPOV")]
        public decimal CurrentOutstandingPurchaseOrdersValue { get; set; }
        [MapField("QTYO1")]
        public decimal QuantitiesOrderedSum1 { get; set; }
        [MapField("QTYO2")]
        public decimal QuantitiesOrderedSum2 { get; set; }
        [MapField("QTYO3")]
        public decimal QuantitiesOrderedSum3 { get; set; }
        [MapField("QTYO4")]
        public decimal QuantitiesOrderedSum4 { get; set; }
        [MapField("QTYO5")]
        public decimal QuantitiesOrderedSum5 { get; set; }
        [MapField("QTYR1")]
        public decimal QuantitiesReceivedSum1 { get; set; }
        [MapField("QTYR2")]
        public decimal QuantitiesReceivedSum2 { get; set; }
        [MapField("QTYR3")]
        public decimal QuantitiesReceivedSum3 { get; set; }
        [MapField("QTYR4")]
        public decimal QuantitiesReceivedSum4 { get; set; }
        [MapField("QTYR5")]
        public decimal QuantitiesReceivedSum5 { get; set; }
        [MapField("LREC1")]
        public decimal LinesPoReceivedSum1 { get; set; }
        [MapField("LREC2")]
        public decimal LinesPoReceivedSum2 { get; set; }
        [MapField("LREC3")]
        public decimal LinesPoReceivedSum3 { get; set; }
        [MapField("LREC4")]
        public decimal LinesPoReceivedSum4 { get; set; }
        [MapField("LREC5")]
        public decimal LinesPoReceivedSum5 { get; set; }
        [MapField("LNRE1")]
        public decimal LinesPoNotReceivedSum1 { get; set; }
        [MapField("LNRE2")]
        public decimal LinesPoNotReceivedSum2 { get; set; }
        [MapField("LNRE3")]
        public decimal LinesPoNotReceivedSum3 { get; set; }
        [MapField("LNRE4")]
        public decimal LinesPoNotReceivedSum4 { get; set; }
        [MapField("LNRE5")]
        public decimal LinesPoNotReceivedSum5 { get; set; }
        [MapField("LOVR1")]
        public decimal LinesReceivedSumOverQtyOrdered1 { get; set; }
        [MapField("LOVR2")]
        public decimal LinesReceivedSumOverQtyOrdered2 { get; set; }
        [MapField("LOVR3")]
        public decimal LinesReceivedSumOverQtyOrdered3 { get; set; }
        [MapField("LOVR4")]
        public decimal LinesReceivedSumOverQtyOrdered4 { get; set; }
        [MapField("LOVR5")]
        public decimal LinesReceivedSumOverQtyOrdered5 { get; set; }
        [MapField("LUND1")]
        public decimal LinesReceivedSumUnderQtyOrdered1 { get; set; }
        [MapField("LUND2")]
        public decimal LinesReceivedSumUnderQtyOrdered2 { get; set; }
        [MapField("LUND3")]
        public decimal LinesReceivedSumUnderQtyOrdered3 { get; set; }
        [MapField("LUND4")]
        public decimal LinesReceivedSumUnderQtyOrdered4 { get; set; }
        [MapField("LUND5")]
        public decimal LinesReceivedSumUnderQtyOrdered5 { get; set; }
        [MapField("PREC1")]
        public decimal NumberPoReceived1 { get; set; }
        [MapField("PREC2")]
        public decimal NumberPoReceived2 { get; set; }
        [MapField("PREC3")]
        public decimal NumberPoReceived3 { get; set; }
        [MapField("PREC4")]
        public decimal NumberPoReceived4 { get; set; }
        [MapField("PREC5")]
        public decimal NumberPoReceived5 { get; set; }
        [MapField("VREC1")]
        public decimal ValuePoReceived1 { get; set; }
        [MapField("VREC2")]
        public decimal ValuePoReceived2 { get; set; }
        [MapField("VREC3")]
        public decimal ValuePoReceived3 { get; set; }
        [MapField("VREC4")]
        public decimal ValuePoReceived4 { get; set; }
        [MapField("VREC5")]
        public decimal ValuePoReceived5 { get; set; }
        [MapField("DYTR1")]
        public decimal DaysSumReceivedOrders1 { get; set; }
        [MapField("DYTR2")]
        public decimal DaysSumReceivedOrders2 { get; set; }
        [MapField("DYTR3")]
        public decimal DaysSumReceivedOrders3 { get; set; }
        [MapField("DYTR4")]
        public decimal DaysSumReceivedOrders4 { get; set; }
        [MapField("DYTR5")]
        public decimal DaysSumReceivedOrders5 { get; set; }
        [MapField("DYSH1")]
        public decimal ShortestNumberOfDays1 { get; set; }
        [MapField("DYSH2")]
        public decimal ShortestNumberOfDays2 { get; set; }
        [MapField("DYSH3")]
        public decimal ShortestNumberOfDays3 { get; set; }
        [MapField("DYSH4")]
        public decimal ShortestNumberOfDays4 { get; set; }
        [MapField("DYSH5")]
        public decimal ShortestNumberOfDays5 { get; set; }
        [MapField("DYLO1")]
        public decimal LongestNumberOfDays1 { get; set; }
        [MapField("DYLO2")]
        public decimal LongestNumberOfDays2 { get; set; }
        [MapField("DYLO3")]
        public decimal LongestNumberOfDays3 { get; set; }
        [MapField("DYLO4")]
        public decimal LongestNumberOfDays4 { get; set; }
        [MapField("DYLO5")]
        public decimal LongestNumberOfDays5 { get; set; }
        [MapField("VPCC")]
        public string VendorPrimaryCountCode { get; set; }
        [MapField("SODT")]
        public DateTime? SomeNotUsedDate { get; set; }
        [MapField("SOQDate")]
        public DateTime SoqDate { get; set; }
        [MapField("PalletCheck")]
        public bool PalletCheckNeeded { get; set; }
    }
}
