using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    // DRL Summary (header) Record - 1 per DRL
    [TableName("DRLDET")]
    public class DrlDetails
    {
        // DRL Number - assigned by the system 
        [MapField("NUMB")]
        public string DrlNumber { get; set; }

        // Program Assigned Sequence    (0001 to 9999) 
        [MapField("SEQN")]
        public string SequenceNumber { get; set; }
    }
}
