using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportRelation")]
    public class ReportRelation
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("Name")]
        public string Name { get; set; }

        [MapField("ParentTable")]
        public string ParentTable { get; set; }

        [MapField("ParentColumns")]
        public string ParentColumns { get; set; }

        [MapField("ChildTable")]
        public string ChildTable { get; set; }

        [MapField("ChildColumns")]
        public string ChildColumns { get; set; }
    }
}
