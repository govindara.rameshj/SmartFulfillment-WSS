﻿using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CompetitorLocalList")]
    public class CompetitorLocalList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
