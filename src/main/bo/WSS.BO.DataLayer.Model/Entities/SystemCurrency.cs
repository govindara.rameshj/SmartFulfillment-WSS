﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SystemCurrency")]
    public class SystemCurrency
    {
        public string Id { get; set; }

        public bool IsDefault { get; set; }
    }
}
