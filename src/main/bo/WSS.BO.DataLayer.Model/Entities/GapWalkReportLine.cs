﻿using System;

namespace WSS.BO.DataLayer.Model.Entities
{
    public class GapWalkReportLine
    {
        public string SkuNumber { get; set; }
        public string Description { get; set; }
        public int QtyOnHand { get; set; }
        public int QtyMarkdown { get; set; }
        public int QuantityRequired { get; set; }
        public string QtyPick { get; set; }
        public DateTime? DateLastSold { get; set; }
        public DateTime? DateLastReceived { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
    }
}
