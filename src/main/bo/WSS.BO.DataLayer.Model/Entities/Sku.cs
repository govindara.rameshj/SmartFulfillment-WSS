using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("sku")]
    public class Sku
    {
        // not all fields are here

        [MapField("id")]
        public string Id { get; set; }
    }
}
