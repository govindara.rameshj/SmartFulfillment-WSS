using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class ConsignmentRequestDetails : IExternalRequestDetails
    {
        public string ConsignmentNumber { get; set; }
        public int FulfillerNumber { get; set; }

        #region IExternalRequestDetails Members

        public virtual string GetDescription()
        {
            return string.Format("ConsignmentNumber='{0}', FulfillerNumber='{1}'", ConsignmentNumber, FulfillerNumber);
        }

        #endregion
    }
}
