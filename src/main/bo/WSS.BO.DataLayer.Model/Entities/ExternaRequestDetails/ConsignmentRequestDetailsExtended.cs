using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class ConsignmentRequestDetailsExtended : ConsignmentRequestDetails
    {
        public string FulfillerName { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string CustomerMobilePhone { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerHomePhone { get; set; }
        public string CustomerWorkPhone { get; set; }

        public string CustomerName { get; set; }

        public string PostCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }

        public int CustomerOrderNumber { get; set; }

        public int OriginatingStoreNumber { get; set; }
        public string OriginatingStoreName { get; set; }

        #region IExternalRequestDetails Members

        private readonly string _fieldTemplate = ", {0}='{1}'";
        public override string GetDescription()
        {
            StringBuilder sb = new StringBuilder(base.GetDescription());
            sb.AppendFormat(_fieldTemplate, "FulfillerName", FulfillerName);
            sb.AppendFormat(_fieldTemplate, "DeliveryDate", DeliveryDate);
            sb.AppendFormat(_fieldTemplate, "CustomerMobilePhone", CustomerMobilePhone);
            sb.AppendFormat(_fieldTemplate, "CustomerContactPhone", CustomerContactPhone);
            sb.AppendFormat(_fieldTemplate, "CustomerHomePhone", CustomerHomePhone);
            sb.AppendFormat(_fieldTemplate, "CustomerWorkPhone", CustomerWorkPhone);
            sb.AppendFormat(_fieldTemplate, "CustomerName", CustomerName);
            sb.AppendFormat(_fieldTemplate, "PostCode", PostCode);
            sb.AppendFormat(_fieldTemplate, "AddressLine1", AddressLine1);
            sb.AppendFormat(_fieldTemplate, "AddressLine2", AddressLine2);
            sb.AppendFormat(_fieldTemplate, "AddressLine3", AddressLine3);
            sb.AppendFormat(_fieldTemplate, "AddressLine4", AddressLine4);
            sb.AppendFormat(_fieldTemplate, "CustomerOrderNumber", CustomerOrderNumber);
            sb.AppendFormat(_fieldTemplate, "OriginatingStoreNumber", OriginatingStoreNumber);
            sb.AppendFormat(_fieldTemplate, "OriginatingStoreName", OriginatingStoreName);

            return sb.ToString();
        }

        #endregion
    }
}
