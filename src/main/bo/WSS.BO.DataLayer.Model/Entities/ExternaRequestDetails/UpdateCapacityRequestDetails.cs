using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class UpdateCapacityRequestDetails : IExternalRequestDetails
    {
        public string FulfillerId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int SlotId { get; set; }

        #region IExternalRequestDetails Members

        public virtual string GetDescription()
        {
            return string.Format("FulfillerId='{0}', Date='{1}', SlotId='{2}'", FulfillerId, DeliveryDate, SlotId);
        }

        #endregion
    }
}
