﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public interface IExternalRequestDetails
    {
        string GetDescription();
    }
}
