﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TOSTEN")]
    public class TillTosTenderAllowed
    {
        [MapField("DSEQ")]
        public string SequenceNumber { get; set; }

        [MapField("TEND")]
        public string TenderType { get; set; }
    }
}
