using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public interface IExternalRequestDetails
    {
        string GetDescription();
        string GenerateSourceDescription(string type);
    }
}
