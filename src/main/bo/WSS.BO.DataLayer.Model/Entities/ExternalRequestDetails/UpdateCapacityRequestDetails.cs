using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class UpdateCapacityRequestDetails : IExternalRequestDetails
    {
        public string FulfillerId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int SlotId { get; set; }
        public int OmOrderNumber { get; set; }

        #region IExternalRequestDetails Members

        public virtual string GetDescription()
        {
            return string.Format("FulfillerId='{0}', Date='{1}', SlotId='{2}'", FulfillerId, DeliveryDate, SlotId);
        }

        public string GenerateSourceDescription(string type)
        {
            switch (type)
            {
                case ExternalRequestType.Allocate:
                    return String.Format(ExternalRequestSourceTemplate.New, OmOrderNumber);
                case ExternalRequestType.Deallocate:
                    return String.Format(ExternalRequestSourceTemplate.Refund, OmOrderNumber);
                case ExternalRequestType.AllocateDeallocate:
                    return String.Format(ExternalRequestSourceTemplate.UpdateDate, OmOrderNumber);
                default:
                    throw new ArgumentException("Unknown type - " + type);
            }
        }
        #endregion
    }
}
