using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class UpdateCapacityRequestDetailsExtended : UpdateCapacityRequestDetails
    {
        public DateTime OldDeliveryDate { get; set; }
        public int OldSlotId { get; set; }

        #region IExternalRequestDetails Members

        public override string GetDescription()
        {
            return string.Format("{0}, OldDate='{1}', OldSlotId='{2}'", base.GetDescription(), OldDeliveryDate, OldSlotId);
        }

        #endregion
    }
}
