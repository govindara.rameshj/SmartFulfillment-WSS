using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class ConsignmentRequestDetails : IExternalRequestDetails
    {
        public string ConsignmentNumber { get; set; }
        public int FulfillerNumber { get; set; }
        public int OmOrderNumber { get; set; }

        #region IExternalRequestDetails Members

        public virtual string GetDescription()
        {
            return string.Format("ConsignmentNumber='{0}', FulfillerNumber='{1}'", ConsignmentNumber, FulfillerNumber);
        }

        public string GenerateSourceDescription(string type)
        {
            switch (type)
            {
                case ExternalRequestType.NewConsignment:
                    return String.Format(ExternalRequestSourceTemplate.New, OmOrderNumber);
                case ExternalRequestType.UpdateConsignment:
                    return String.Format(ExternalRequestSourceTemplate.UpdateDetails, OmOrderNumber);
                case ExternalRequestType.CancelConsignment:
                    return String.Format(ExternalRequestSourceTemplate.Refund, OmOrderNumber);
                default:
                    throw new ArgumentException("Unknown type - " + type);
            }
        }

        #endregion
    }
}
