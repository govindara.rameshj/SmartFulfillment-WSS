using System;
using System.Text;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
{
    public class UserManagmentRequestDetails : IExternalRequestDetails
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string SecurityProfileId { get; set; }
        public string StoreId { get; set; }
        public string PasswordHash { get; set; }

        #region IExternalRequestDetails Members

        public virtual string GetDescription()
        {
            return string.Format("StoreId='{0}', UserId='{1}', EmployeeCode='{2}', Name='{3}', IsDeleted='{4}', SecurityProfileId='{5}', PasswordHash='{6}'", StoreId, Id, Code, Name, IsDeleted, SecurityProfileId, PasswordHash);
        }

        public string GenerateSourceDescription(string type)
        { 
            switch (type) 
            {
                case ExternalRequestType.InsertUser:
                    return String.Format(ExternalRequestSourceTemplate.InsertUser, Id);
                case ExternalRequestType.UpdateUser:
                    return String.Format(ExternalRequestSourceTemplate.UpdateUser, Id);
                case ExternalRequestType.DeleteUser:
                    return String.Format(ExternalRequestSourceTemplate.DeleteUser, Id);
                default:
                    throw new ArgumentException("Unknown type - " + type);
            }
        }
        #endregion
    }
}
