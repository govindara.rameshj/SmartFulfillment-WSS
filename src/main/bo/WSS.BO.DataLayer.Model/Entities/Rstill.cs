﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("PSTILL")]
    internal class Rstill
    {
        [MapField("FTKY")]
        public decimal FlashTotalsKey { get; set; }

        // In Json
        [MapField("CASH")]
        public char CashierNumber { get; set; }

        [MapField("PCAS")]
        public string PriorCashierNumber { get; set; } // = CashierNumber for sale

        [MapField("PTIM")]
        public string LastTimeChashierSignedOff { get; set; } //??

        [MapField("NUMB")]
        public decimal NumberOfTransaction { get; set; } //current NUMB + 1

        [MapField("AMNT")]
        public decimal Amount { get; set; } // AMNT + TotalAmount (In Json)

        // In Json
        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("ZRED")]
        public bool IsZread { get; set; } //??? 0 for sale

        [MapField("IEVT")]
        public bool IsUsingEvt { get; set; } //??? 0 for sale

        // In Json
        [MapField("TILL")]
        public string TillNumber { get; set; }
    }
}
