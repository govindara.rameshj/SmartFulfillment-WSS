using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ProfilemenuAccess")]
    public class ProfilemenuAccess
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("MenuConfigID")]
        public int MenuConfigID { get; set; }

        [MapField("AccessAllowed")]
        public bool AccessAllowed { get; set; }

        [MapField("OverrideAllowed")]
        public bool OverrideAllowed { get; set; }

        [MapField("SecurityLevel")]
        public int SecurityLevel { get; set; }

        [MapField("IsManager")]
        public bool IsManager { get; set; }

        [MapField("IsSupervisor")]
        public bool IsSupervisor { get; set; }

        [MapField("LastEdited"), NullValue(null)]
        public string LastEdited { get; set; }

        [MapField("Parameters"), NullValue(null)]
        public string Parameters { get; set; }
    }
}
