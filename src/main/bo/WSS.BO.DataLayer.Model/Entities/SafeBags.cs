﻿using System;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SafeBags")]
    public class SafeBags
    {
        public int ID { get; set; }

        public string SealNumber { get; set; }

        public string Type { get; set; }

        public string State { get; set; }

        public decimal Value { get; set; }

        public int InPeriodID { get; set; }

        public DateTime InDate { get; set; }
        
        public int InUserID1 { get; set; }
        
        public int InUserID2 { get; set; }
        
        public int OutPeriodID { get; set; }

        public DateTime OutDate { get; set; }

        public int OutUserID1 { get; set; }

        public int OutUserID2 { get; set; }

        public string AccountabilityType { get; set; }

        public int AccountabilityID { get; set; }

        public decimal FloatValue { get; set; }

        public int PickupPeriodID { get; set; }

        public int RelatedBagId { get; set; }

        public string Comments { get; set; }

        public bool FloatChecked { get; set; }

        public int FloatCheckedUserID1 { get; set; }

        public int FloatCheckedUserID2 { get; set; }

        public bool CashDrop { get; set; }

        public string BagCollectionSlipNo { get; set; }

        public string BagCollectionComment { get; set; }
    }
}
