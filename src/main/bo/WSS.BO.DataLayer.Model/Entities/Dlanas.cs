﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLANAS")]
    public class Dlanas : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlanas()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            LineNumber = 0;
            SequenceNumber = String.Empty;
            EanNumber = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            FillerForFutureUse = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("NUMB")]
        public short LineNumber { get; set; }

        [MapField("SEQN")]
        public string SequenceNumber { get; set; }

        [MapField("EANN")]
        public string EanNumber { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
