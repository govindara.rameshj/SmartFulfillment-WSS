﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("QUOLIN")]
    public class QuoteLine
    {
        [MapField("NUMB")]
        public string Number { get; set; } //QUOHDR.NUMB

        [MapField("LINE")]
        public string Line { get; set; } // As in Dlline

        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("QUAN")]
        public int Quantity { get; set; }

        [MapField("WGHT")]
        public decimal Weight { get; set; } // always zero (marked as "for futer use")

        [MapField("VOLU")]
        public decimal Volume { get; set; } // always zero (marked as "for futer use")

        [MapField("SPARE")]
        public string Spare { get; set; } // always empty (marked as "for futer use")
    }
}
