﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORHDR")]
    public class CustomerOrderHeader
    {
        public CustomerOrderHeader()
        {
            AddressLine1 = string.Empty;
            AddressLine2 = string.Empty;
            Town = string.Empty;
            AddressLine4 = string.Empty; // default
            AmendingNumber = "0"; // default
            CancellationState = "0";
            CreateDate = new DateTime();
            SaleDate = DateTime.MinValue;
            CustomerNumber = string.Empty;
            CustomerName = string.Empty;
            DeliveryCharge = 0;
            DeliveryOrCollectionDate = new DateTime();
            DocumentRepprintingNumber = 0; // default
            HomePhoneNumber = null; // default
            IsForDelivery = false;
            IsDeliveryDispatched = false; // default
            IsDeliveryDocumentPrinted = false; // default
            MerchandiseValueStore = 0;
            MobilePhoneNumber = string.Empty;
            OrderNumber = string.Empty;
            PhoneNumber = string.Empty;
            PostCode = string.Empty;
            RefundDate = null;
            RefundTillNumber = string.Empty;
            RefundTransactionNumber = string.Empty;
            RevisionNumber = 0; // default
            TillNumber = string.Empty;
            TotalOrderUnitsNumber = 0;
            TotalUnitsTakenNumber = 0; //default
            TotalUnitsRefundedNumber = 0;
            TransactionNumber = string.Empty;
            Volume = 0; // default
            Weight = 0; // default
        }

        [MapField("NUMB")]
        public string OrderNumber { get; set; } // QUOHDR number

        //In Json
        [MapField("CUST")]
        public string CustomerNumber { get; set; } //Number in CUSMAS

        // In Json
        [MapField("DATE1")]
        public DateTime CreateDate { get; set; } // In DB is always equal to SDAT, I don't know why it's just "CreateDate"

        //In Json
        [MapField("DELD")]
        public DateTime DeliveryOrCollectionDate { get; set; } // If delivery order - mapped to json's deliveryDate field, if collection order - to collectionDate

        [MapField("CANC")]
        public string CancellationState { get; set; }
        //0 = Order is live
        //1 = Order is completely cancelled
        //2 = Order has been changed

        [MapField("DELI")]
        public bool IsForDelivery { get; set; } // FROM doc: On = is a DeliveryFulfillment class, Off = CollectionFulfillment class

        [MapField("DELC")]
        public bool IsDeliveryDispatched { get; set; } // 0 for sale. Used by BO 

        [MapField("AMDT")]
        public string AmendingNumber { get; set; } // Number of order amending. 0 for sale, 'cause we can amend the order only via BO

        // In Json
        [MapField("ADDR1")]
        public string AddressLine1 { get; set; } // String.Format("{0},{1}", HouseNumber, AddressLine1)

        // In Json
        [MapField("ADDR2")]
        public string AddressLine2 { get; set; } // AddressLine2

        //In Json
        [MapField("ADDR3")]
        public string Town { get; set; } // Town

        [MapField("ADDR4")]
        public string AddressLine4 { get; set; } // empty

        // In Json
        [MapField("PHON")]
        public string PhoneNumber { get; set; } // Phone

        [MapField("PRNT")]
        public bool IsDeliveryDocumentPrinted { get; set; } // 0 as default, is used by bo

        [MapField("RPRN")]
        public decimal DocumentRepprintingNumber { get; set; } // 0 as default; count of times document was reprinted. Don't touch it, case it can be change only in BO 

        [MapField("REVI")]
        public decimal RevisionNumber { get; set; } // 0 as default, is used by bo 

        [MapField("MVST")]
        public decimal MerchandiseValueStore { get; set; } // DLTOTS.TOTL

        //In Json
        [MapField("DCST")]
        public decimal DeliveryCharge { get; set; } //deliveryCharge

        [MapField("QTYO")]
        public decimal TotalOrderUnitsNumber { get; set; } // Count of all goods (sum of quantities from all dllines)

        [MapField("QTYT")]
        public decimal TotalUnitsTakenNumber { get; set; } // in old till we could state goods count which we can take right now. As in Json there is no field to show quantity is taken, lets state 0

        [MapField("QTYR")]
        public decimal TotalUnitsRefundedNumber { get; set; } // for refund; 0 by default for sale;

        [MapField("WGHT")]
        public decimal Weight { get; set; } // Is 0 in our DB, but we have WGHT column in STKMAS, so I think it's possible to use it in the future (as it's stated in the doc)

        [MapField("VOLU")]
        public decimal Volume { get; set; } // Is 0 in our DB, but we have VOLU column in STKMAS, so I think it's possible to use it in the future (as it's stated in the doc)

        //In Json
        [MapField("SDAT")]
        public DateTime SaleDate { get; set; }

        // In Json
        [MapField("STIL")]
        public string TillNumber { get; set; }

        // In Json
        [MapField("STRN")]
        public string TransactionNumber { get; set; }

        [MapField("RDAT")]
        public DateTime? RefundDate { get; set; } // For refund only

        [MapField("RTIL")]
        public string RefundTillNumber { get; set; } // For refund only

        [MapField("RTRN")]
        public string RefundTransactionNumber { get; set; } // For refund only

        //In Json 
        [MapField("MOBP")]
        public string MobilePhoneNumber { get; set; } // MobilePhone

        //In Json
        [MapField("POST")]
        public string PostCode { get; set; } // Postcode

        // In Json
        [MapField("NAME")]
        public string CustomerName { get; set; } // Name

        [MapField("HOMP")]
        public string HomePhoneNumber { get; set; } // It's entered in BO, so it's null for us

        public string RequiredDeliverySlotID { get; set; }

        public string RequiredDeliverySlotDescription { get; set; }

        public TimeSpan? RequiredDeliverySlotStartTime { get; set; }

        public TimeSpan? RequiredDeliverySlotEndTime { get; set; }

    }
}
