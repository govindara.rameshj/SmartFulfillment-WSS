using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("Report")]
    public class Report
    {
        public Report()
        {
            ShowResetButton = false;
        }

        [MapField("Id")]
        public int Id { get; set; }

        [MapField("Header")]
        public string Header { get; set; }

        [MapField("Title")]
        public string Title { get; set; }

        [MapField("ProcedureName")]
        public string ProcedureName { get; set; }

        [MapField("HideWhenNoData")]
        public bool HideWhenNoData { get; set; }

        [MapField("PrintLandscape")]
        public bool PrintLandscape { get; set; }

        [MapField("MinWidth")]
        public int MinWidth { get; set; }

        [MapField("MinHeight")]
        public int MinHeight { get; set; }

        [MapField("ShowResetButton")]
        public bool ShowResetButton { get; set; }
    }
}
