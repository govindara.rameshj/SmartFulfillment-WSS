﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORREFUND")]
    public class CustomerOrderRefund
    {
        public CustomerOrderRefund()
        {
            OrderNumber = "000000";
            LineNumber = "0000";
            RefundStoreId = 0;
            RefundDate = DateTime.MinValue;
            RefundTill = String.Empty;
            RefundTransaction = String.Empty;
            QtyReturned = 0;
            QtyCancelled = 0;
            RefundStatus = 0;
            SellingStoreIbtOut = String.Empty;
            FulfillingStoreIbtIn = String.Empty;
        }

        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        [MapField("LINE")]
        public string LineNumber { get; set; }

        public int RefundStoreId { get; set; }

        public DateTime RefundDate { get; set; }

        public string RefundTill { get; set; }

        public string RefundTransaction { get; set; }

        public int QtyReturned { get; set; }

        public int QtyCancelled { get; set; }

        public int RefundStatus { get; set; }

        public string SellingStoreIbtOut { get; set; }

        public string FulfillingStoreIbtIn { get; set; }
    }
}
