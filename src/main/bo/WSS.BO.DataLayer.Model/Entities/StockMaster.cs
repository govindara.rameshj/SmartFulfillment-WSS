using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("STKMAS")]
    public class StockMaster
    {
        public StockMaster()
        {
            Obsolete = "0";
            NonStocked = "0";
            AutoApply = "0";
            HierarchyCategory = "0";
            HierarchyGroup = "0";
            HierarchySubGroup = "0";
            HierarchyStyle = "0";
        }

        [PrimaryKey]
        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("PRIC")]
        public decimal Price { get; set; }

        [MapField("REVT")]
        public string EventNumber { get; set; }

        [MapField("RPRI")]
        public string Priority { get; set; }

        [MapField("IOBS")]
        public string Obsolete { get; set; }

        [MapField("ONHA")]
        public decimal StockOnHand { get; set; } // IsFromMarkDown (Json) ? ONHA : ONHA = ONHA - Qty

        [MapField("INON")]
        public string NonStocked { get; set; }

        [MapField("MDNQ")]
        public decimal MarkdownStock { get; set; } // IsFromMarkDown (Json) ? MDNQ = MDNQ - Qty : MDNQ

        [MapField("AAPC")]
        public string AutoApply { get; set; }

        [MapField("LABN")]
        public int LabelSmall { get; set; }

        [MapField("LABM")]
        public int LabelMedium { get; set; }

        [MapField("LABL")]
        public int LabelLarge { get; set; }

        [MapField("SUPP")]
        public string SupplierId { get; set; }

        [MapField("DESCR")]
        public string Description { get; set; }

        [MapField("SALU1")]
        public int SalesUnits1 { get; set; } // "UnitsSoldYesterday" -- IsFromMarkDown (Json) ? SALU1 = SALU1 + Qty : SALU1 = SALU1

        [MapField("SALU2")]
        public int SalesUnits2 { get; set; } // "UnitsSoldThisWeek" -- IsFromMarkDown (Json) ? SALU2 = SALU2 + Qty : SALU2 = SALU2

        [MapField("SALU4")]
        public int SalesUnits4 { get; set; } // "UnitsSoldThisPeriod" -- IsFromMarkDown (Json) ? SALU4 = SALU4 + Qty : SALU4 = SALU4

        [MapField("SALU6")]
        public int SalesUnits6 { get; set; } // "UnitsSoldThisYear" -- IsFromMarkDown (Json) ? SALU6 = SALU6 + Qty : SALU6 = SALU6

        [MapField("SALV1")]
        public decimal SalesValue1 { get; set; } // "ValueSoldYesterday" -- IsFromMarkDown (Json) ? SALV1 = SALV1 + cExtPrice : SALV1 = SALV1
        // cExtPrice = DLLINE.EXTP - DLLINE.QBPD - DLLINE.DGPD - DLLINE.MBPD - DLLINE.HSPD

        [MapField("SALV2")]
        public decimal SalesValue2 { get; set; } // "ValueSoldThisWeek" -- IsFromMarkDown (Json) ? SALV2 = SALV2 + cExtPrice : SALV2 = SALV2
        // cExtPrice = DLLINE.EXTP - DLLINE.QBPD - DLLINE.DGPD - DLLINE.MBPD - DLLINE.HSPD

        [MapField("SALV4")]
        public decimal SalesValue4 { get; set; } // "ValueSoldThisPeriod" -- IsFromMarkDown (Json) ? SALV4 = SALV4 + cExtPrice : SALV4 = SALV4
        // cExtPrice = DLLINE.EXTP - DLLINE.QBPD - DLLINE.DGPD - DLLINE.MBPD - DLLINE.HSPD

        [MapField("SALV6")]
        public decimal SalesValue6 { get; set; } // "ValueSoldThisYear" -- IsFromMarkDown (Json) ? SALV6 = SALV6 + cExtPrice : SALV6 = SALV6
        // cExtPrice = DLLINE.EXTP - DLLINE.QBPD - DLLINE.DGPD - DLLINE.MBPD - DLLINE.HSPD

        [MapField("RETQ")]
        public int OpenReturnQuantity { get; set; }

        [MapField("IRIS")]
        public bool IsRelatedItemSingle { get; set; }

        [MapField("IMDN")]
        public bool IsMarkdownItem { get; set; }

        [MapField("ICAT")]
        public bool IsTrackValueOnly { get; set; }

        [MapField("IDEL")]
        public bool IsHeadOfficeDeleted { get; set; }

        [MapField("ITAG")]
        public bool IsTaggedItem { get; set; }

        [MapField("CTGY")]
        public string HierarchyCategory { get; set; }

        [MapField("GRUP")]
        public string HierarchyGroup { get; set; }

        [MapField("SGRP")]
        public string HierarchySubGroup { get; set; }

        [MapField("STYL")]
        public string HierarchyStyle { get; set; }

        [MapField("SALT")]
        public string SaleTypeAttribute { get; set; }

        [MapField("WTFQ")]
        public decimal WriteOffStock { get; set; }

        [MapField("FLAG")]
        public bool HasWeeklyUpdateOccured { get; set; }

        [MapField("CFLG")]
        public bool  IsConfidentOfAverageWeeklySalesCalc { get; set; }

        [MapField("SQOR")]
        public bool IsOrderPlaced { get; set; }

        [MapField("IPSK")]
        public bool IsPalletedSkuCauses { get; set; }

        [MapField("WGHT")]
        public decimal Weight { get; set; }

        [MapField("VOLU")]
        public decimal Volume { get; set; }

        [MapField("DateLastSold")]
        public DateTime? DateLastSold { get; set; }

        [MapField("DPRC")]
        public DateTime? DateOfLastPriceChange { get; set; }

        [MapField("MADV1")]
        public decimal AdjustmentsValue1 { get; set; }
    }
}
