﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("RSCASH")]
    public class Rsflas
    {
        [MapField("FKEY")]
        public int KeyToFile { get; set; } // from rscash

        [MapField("FLOT")]
        public decimal StartingFloatAmount { get; set; } //??? 0 for sale

        [MapField("PICK")]
        public decimal PickupAmount { get; set; } //??? 0 for sale

        [MapField("NUMB1")]
        public decimal CountOfNumber1 { get; set; } // NUMB1 + 1 for sale

        [MapField("NUMB2")]
        public decimal CountOfNumber2 { get; set; }

        [MapField("NUMB3")]
        public decimal CountOfNumber3 { get; set; }

        [MapField("NUMB4")]
        public decimal CountOfNumber4 { get; set; }

        [MapField("NUMB5")]
        public decimal CountOfNumber5 { get; set; }

        [MapField("NUMB6")]
        public decimal CountOfNumber6 { get; set; }

        [MapField("NUMB7")]
        public decimal CountOfNumber7 { get; set; }

        [MapField("NUMB8")]
        public decimal CountOfNumber8 { get; set; }

        [MapField("NUMB9")]
        public decimal CountOfNumber9 { get; set; }

        [MapField("NUMB10")]
        public decimal CountOfNumber10 { get; set; }

        [MapField("NUMB11")]
        public decimal CountOfNumber11 { get; set; }

        [MapField("NUMB12")]
        public decimal CountOfNumber12 { get; set; }

        [MapField("NUMB13")]
        public decimal CountOfNumber13 { get; set; }

        [MapField("TIME1")]
        public string CurrentTime1 { get; set; } // Current time for sale

        [MapField("TIME2")]
        public string CurrentTime2 { get; set; }

        [MapField("TIME3")]
        public string CurrentTime3 { get; set; }

        [MapField("TIME4")]
        public string CurrentTime4 { get; set; }

        [MapField("TIME5")]
        public string CurrentTime5 { get; set; }

        [MapField("TIME6")]
        public string CurrentTime6 { get; set; }

        [MapField("TIME7")]
        public string CurrentTime7 { get; set; }

        [MapField("TIME8")]
        public string CurrentTime8 { get; set; }

        [MapField("TIME9")]
        public string CurrentTime9 { get; set; }

        [MapField("TIME10")]
        public string CurrentTime10 { get; set; }

        [MapField("TIME11")]
        public string CurrentTime11 { get; set; }

        [MapField("TIME12")]
        public string CurrentTime12 { get; set; }

        [MapField("TIME13")]
        public string CurrentTime13 { get; set; }

        [MapField("AMNT1")]
        public decimal TotalAmount1 { get; set; } // AMNT1 + TotalAmount (Json) for sale

        [MapField("AMNT2")]
        public decimal TotalAmount2 { get; set; }

        [MapField("AMNT3")]
        public decimal TotalAmount3 { get; set; }

        [MapField("AMNT4")]
        public decimal TotalAmount4 { get; set; }

        [MapField("AMNT5")]
        public decimal TotalAmount5 { get; set; }

        [MapField("AMNT6")]
        public decimal TotalAmount6 { get; set; }

        [MapField("AMNT7")]
        public decimal TotalAmount7 { get; set; }

        [MapField("AMNT8")]
        public decimal TotalAmount8 { get; set; }

        [MapField("AMNT9")]
        public decimal TotalAmount9 { get; set; }

        [MapField("AMNT10")]
        public decimal TotalAmount10 { get; set; }

        [MapField("AMNT11")]
        public decimal TotalAmount11 { get; set; }

        [MapField("AMNT12")]
        public decimal TotalAmount12 { get; set; }

        [MapField("AMNT13")]
        public decimal TotalAmount13 { get; set; }

        [MapField("TTCO1")]
        public decimal TenderTypeCount1 { get; set; } // TTCO1 + 1 for cash

        [MapField("TTCO2")]
        public decimal TenderTypeCount2 { get; set; }

        [MapField("TTCO3")]
        public decimal TenderTypeCount3 { get; set; }

        [MapField("TTCO4")]
        public decimal TenderTypeCount4 { get; set; }

        [MapField("TTCO5")]
        public decimal TenderTypeCount5 { get; set; }

        [MapField("TTCO6")]
        public decimal TenderTypeCount6 { get; set; }

        [MapField("TTCO7")]
        public decimal TenderTypeCount7 { get; set; }

        [MapField("TTCO8")]
        public decimal TenderTypeCount8 { get; set; }

        [MapField("TTCO9")]
        public decimal TenderTypeCount9 { get; set; }

        [MapField("TTCO10")]
        public decimal TenderTypeCount10 { get; set; }

        [MapField("TTCO11")]
        public decimal TenderTypeCount11 { get; set; }

        [MapField("TTCO12")]
        public decimal TenderTypeCount12 { get; set; }

        [MapField("TTCO13")]
        public decimal TenderTypeCount13 { get; set; }

        [MapField("TTCO14")]
        public decimal TenderTypeCount14 { get; set; }

        [MapField("TTCO15")]
        public decimal TenderTypeCount15 { get; set; }

        [MapField("TTCO16")]
        public decimal TenderTypeCount16 { get; set; }

        [MapField("TTCO17")]
        public decimal TenderTypeCount17 { get; set; }

        [MapField("TTCO18")]
        public decimal TenderTypeCount18 { get; set; }

        [MapField("TTCO19")]
        public decimal TenderTypeCount19 { get; set; }

        [MapField("TTCO20")]
        public decimal TenderTypeCount20 { get; set; }

        [MapField("TTAM1")]
        public decimal TenderTypeAmount1 { get; set; } // TTAM1 + TotalAmount for cash

        [MapField("TTAM2")]
        public decimal TenderTypeAmount2 { get; set; }

        [MapField("TTAM3")]
        public decimal TenderTypeAmount3 { get; set; }

        [MapField("TTAM4")]
        public decimal TenderTypeAmount4 { get; set; }

        [MapField("TTAM5")]
        public decimal TenderTypeAmount5 { get; set; }

        [MapField("TTAM6")]
        public decimal TenderTypeAmount6 { get; set; }

        [MapField("TTAM7")]
        public decimal TenderTypeAmount7 { get; set; }

        [MapField("TTAM8")]
        public decimal TenderTypeAmount8 { get; set; }

        [MapField("TTAM9")]
        public decimal TenderTypeAmount9 { get; set; }

        [MapField("TTAM10")]
        public decimal TenderTypeAmount10 { get; set; }

        [MapField("TTAM11")]
        public decimal TenderTypeAmount11 { get; set; }

        [MapField("TTAM12")]
        public decimal TenderTypeAmount12 { get; set; }

        [MapField("TTAM13")]
        public decimal TenderTypeAmount13 { get; set; }

        [MapField("TTAM14")]
        public decimal TenderTypeAmount14 { get; set; }

        [MapField("TTAM15")]
        public decimal TenderTypeAmount15 { get; set; }

        [MapField("TTAM16")]
        public decimal TenderTypeAmount16 { get; set; }

        [MapField("TTAM17")]
        public decimal TenderTypeAmount17 { get; set; }

        [MapField("TTAM18")]
        public decimal TenderTypeAmount18 { get; set; }

        [MapField("TTAM19")]
        public decimal TenderTypeAmount19 { get; set; }

        [MapField("TTAM20")]
        public decimal TenderTypeAmount20 { get; set; }

    }
}
