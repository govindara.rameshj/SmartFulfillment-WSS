using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SYSDAT")]
    public class SystemDate
    {
        [MapField("TODT")]
        public DateTime SysDate { get; set; }
    }
}
