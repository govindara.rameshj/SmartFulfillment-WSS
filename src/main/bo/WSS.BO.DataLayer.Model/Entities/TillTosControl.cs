﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TOSCTL")]
    public class TillTosControl
    {
        [MapField("TOSC")]
        public string SaleTypeCode { get; set; }

        [MapField("TOSD")]
        public string SaleTypeDescription { get; set; }

        [MapField("IUSE")]
        public bool InUse { get; set; }

        public bool UpdateZReads { get; set; }
    }
}
