﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("COUPONMASTER")]
    public class CouponMaster
    {
        [MapField("COUPONID")]
        public string CouponMasterId { get; set; }

        [MapField("EXCCOUPON")]
        public bool IsExclusiveCoupon { get; set; }

        [MapField("REUSABLE")]
        public bool IsCouponReUsable { get; set; }
    }
}
