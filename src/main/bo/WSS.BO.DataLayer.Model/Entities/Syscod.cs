using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SYSCOD")]
    public class Syscod
    {
        public string Code { get; set; }

        public string Type { get; set; }

        [MapField("DESCR")]
        public string Description { get; set; }
    }
}
