﻿using System;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ActivityLog")]
    public class ActivityLog
    {
        public int ID { get; set; }

        public DateTime LogDate { get; set; }

        public int EmployeeID { get; set; }

        public string WorkstationID { get; set; }

        public int MenuOptionID { get; set; }

        public bool LoggedIn { get; set; }
        
        public bool ForcedLogOut { get; set; }

        public string StartTime { get; set; }
        
        public string EndTime { get; set; }

        public DateTime AppStart { get; set; }

        public DateTime AppEnd { get; set; }
    }
}
