﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLRCUS")]
    public class DailyCustomer : IDailyTillTranKey, ISyncronizedToRti
    {
        public DailyCustomer()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            AddressLine1 = String.Empty;
            AddressLine2 = String.Empty;
            CustomerName = String.Empty;
            Email = String.Empty;
            HouseNumber = String.Empty;
            IsOriginalTransactionValidated = false; 
            LabelsRequired = false;
            LineNumber = 0;
            MobilePhone = String.Empty;
            OriginalSaleStore = Global.NullOriginalSaleStore;
            OriginalTenderType = Global.NullOriginalTenderType;
            OriginalTillNumber = Global.NullOriginalTillNumber;
            OriginalTransactionNumber = Global.NullOriginalTransactionNumber;
            OriginalSaleDate = null;
            PhoneNumber = String.Empty;
            Postcode = String.Empty;
            RefundReasonCode = Global.NullRefundReasonCode;
            Spare = null;
            Town = String.Empty;
            WebOrderNumber = String.Empty;
            WorkPhone = String.Empty;
        }
        
        // In Json
        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        // In Json
        [MapField("TILL")]
        public string TillNumber { get; set; }

        // In Json
        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        // In Json
        [MapField("NAME")]
        public string CustomerName { get; set; }

        [MapField("HNUM")]
        public string OldHouseNumber { get; set; } // No longer used. Empty as default in the DB

        // In Json
        [MapField("POST")]
        public string Postcode { get; set; }

        [MapField("OSTR")]
        public string OriginalSaleStore { get; set; } // "000" for sale

        [MapField("ODAT")]
        public DateTime? OriginalSaleDate { get; set; } // null

        [MapField("OTIL")]
        public string OriginalTillNumber { get; set; } // "00" for sale

        [MapField("OTRN")]
        public string OriginalTransactionNumber { get; set; } // "0000" for sale

        // In Json
        [MapField("HNAM")]
        public string HouseNumber { get; set; }

        // In Json
        [MapField("HAD1")]
        public string AddressLine1 { get; set; }

        // In Json
        [MapField("HAD2")]
        public string AddressLine2 { get; set; }

        // In Json
        [MapField("HAD3")]
        public string Town { get; set; }

        // In Json
        [MapField("PHON")]
        public string PhoneNumber { get; set; }

        [MapField("NUMB")]
        public int LineNumber { get; set; } // as in dlline

        [MapField("OVAL")]
        public bool IsOriginalTransactionValidated { get; set; }

        [MapField("OTEN")]
        public decimal OriginalTenderType { get; set; } // 0 by default. From doc: "Only if validated by system"

        [MapField("RCOD")]
        public string RefundReasonCode { get; set; } // 0 for sale

        [MapField("LABL")]
        public bool LabelsRequired { get; set; } // From table's extended properties: always 1

        // In Json
        [MapField("MOBP")]
        public string MobilePhone { get; set; }

        // In Json
        [MapField("PhoneNumberWork")]
        public string WorkPhone { get; set; }

        // In Json
        [MapField("EmailAddress")]
        public string Email { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; } // S for sale

        [MapField("SPARE")]
        public string Spare { get; set; } // Always NULL

        [MapField("WEBONO")]
        public string WebOrderNumber { get; set; } // Empty in the db,

        [MapField("ONUM")]
        public int? OriginalLineNumber { get; set; }

        public DailyCustomer Clone()
        {
            return Map.ObjectToObject<DailyCustomer>(this);
        }
    }
}
