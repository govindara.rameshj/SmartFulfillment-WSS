﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.IO;
using System.Xml.Serialization;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ExternalRequests")]
    public class ExternalRequest
    {
        [PrimaryKey, Identity]
        public Int64 Id;

        [NotNull]
        public DateTime CreateTime;

        [NotNull]
        public string RequestDetailsXml;

        [NotNull]
        public int Status;

        [NotNull]
        public string Type;

        [NotNull]
        public string Source;

        [Nullable]
        public string Error;

        [NotNull]
        public int RetriesCount;

        [Nullable]
        public DateTime? LastRetryDateTime;

        public void SetDetails<T>(T details) where T : IExternalRequestDetails
        {
            XmlSerializer x = new XmlSerializer(typeof(T));
            using (var stringWriter = new StringWriter())
            {
                x.Serialize(stringWriter, details);
                RequestDetailsXml = stringWriter.ToString();
            }
        }

        public T GetDetails<T>() where T : IExternalRequestDetails
        {
            XmlSerializer x = new XmlSerializer(typeof(T));
            using (var stringReader = new StringReader(RequestDetailsXml))
            {
                return (T)x.Deserialize(stringReader);
            }
        }
    }
}
