using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLCOMM")]
    public class Dlcomm : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlcomm()
        {
            CreateDate = DateTime.MinValue;
            TokenId = String.Empty;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            PaymentIndex = 0;
            PanHash = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            Spare = null;
        }
        
        // In Json
        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        // In Json
        [MapField("TILL")]
        public string TillNumber { get; set; }

        // In Json
        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        // In Json
        [MapField("NUMB")]
        public int PaymentIndex { get; set; }

        // In Json
        [MapField("TOKENID")]
        public string TokenId { get; set; }

        // In Json
        [MapField("CNHASH")]
        public string PanHash { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; } // From Dltots

        [MapField("SPARE")]
        public string Spare { get; set; } // always null
    }
}
