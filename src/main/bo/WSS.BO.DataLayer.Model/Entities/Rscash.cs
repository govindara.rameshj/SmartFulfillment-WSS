﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("RSCASH")]
    public class Rscash
    {
        //In Json
        [MapField("CASH")]
        public string CashierNumber { get; set; }

        [MapField("FTKY")]
        public decimal FlashTotalsKey { get; set; } // SELECT FTKY FROM RSCASH where CASH = CashierNumber (Json)

        //In Json
        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("NAME")]
        public string CashierName { get; set; } // SELECT Name FROM SystemUsers where EmployeeCode = CashierNumber (Json)

        [MapField("SECC")]
        public string SecurityCode { get; set; } // SELECT PAssword FROM SystemUsers where EmployeeCode = CashierNumber (Json)

        [MapField("NUMB")]
        public decimal NumberOfTranForCashier { get; set; } // (SELECT NUMB FROM RSCASH where CASH = CashierNumber (Json)) + 1

        [MapField("AMNT")]
        public decimal Amount { get; set; } // (SELECT Amount FROM RSCASH where CASH = CashierNumber (Json)) + TotalAmount (Json)
    }
}
