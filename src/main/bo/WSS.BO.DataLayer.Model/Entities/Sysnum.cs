﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SYSNUM")]
    public class Sysnum
    {
        [MapField("FKEY")]
        public int KeyToFile { get; set; } // Always set to 01 (It's not me, it's column extended property told me so)

        [MapField("NEXT15")]
        public string Next15 { get; set; } // Tgis part needs further investigations
    }
}
