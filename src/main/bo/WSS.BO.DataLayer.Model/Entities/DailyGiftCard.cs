using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLGIFTCARD")]
    public class DailyGiftCard : IDailyTillTranKey, ISyncronizedToRti
    {
        public DailyGiftCard()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            CardNumber = String.Empty;
            CashierNumber = String.Empty;
            TransactionType = String.Empty;
            TenderAmount = 0;
            AuthCode = String.Empty;
            MessageNumber = String.Empty;
            ExternalTransactionNumber = 0;
            RtiFlag = RtiStatus.NotSet;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("CARDNUM")]
        public string CardNumber { get; set; }

        [MapField("EEID")]
        public string CashierNumber { get; set; }

        [MapField("TYPE")]
        public string TransactionType { get; set; }

        [MapField("AMNT")]
        public decimal TenderAmount { get; set; }

        [MapField("AUTH")]
        public string AuthCode { get; set; }

        [MapField("MSGNUM")]
        public string MessageNumber { get; set; }

        [MapField("TRANID")]
        public decimal ExternalTransactionNumber { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SEQN")]
        public int SequenceNumber { get; set; }
    }
}
