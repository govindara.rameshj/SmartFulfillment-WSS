using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SYSOPT")]
    public class SystemOption
    {
        [MapField("STOR")]
        public string StoreNumber { get; set; }
    }
}
