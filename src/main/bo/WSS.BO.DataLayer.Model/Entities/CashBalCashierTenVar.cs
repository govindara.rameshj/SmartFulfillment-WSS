﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CashBalCashierTenVar")]
    public class CashBalCashierTenVar
    {
        public CashBalCashierTenVar()
        {
            Quantity = 0;
            Amount = 0;
            PickUp = 0;
        }

        public int PeriodId { get; set; }

        public int TradingPeriodId { get; set; }

        public int CashierId { get; set; }

        public string CurrencyId { get; set; }

        [MapField("ID")]
        public int TenderTypeId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Amount { get; set; }

        public decimal PickUp { get; set; }
    }
}
