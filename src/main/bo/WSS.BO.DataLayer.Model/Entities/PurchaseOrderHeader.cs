﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("PURHDR")]
    public class PurchaseOrderHeader
    {
        [MapField("TKEY")]
        [Identity]
        public int DigCompilerKey { get; set; }

        [MapField("NUMB")]
        public int ResponseType { get; set; }

        [MapField("SUPP")]
        public string SupplierNumber { get; set; }

        [MapField("ODAT")]
        public DateTime CreationDate { get; set; }

        [MapField("DDAT")]
        public DateTime ExpectedDeliveryDate { get; set; }

        [MapField("RELN")]
        public int ReleaseNumber { get; set; }

        [MapField("RAIS")]
        public string RaiserInitials { get; set; }

        [MapField("EmployeeId")]
        public int? EmployeeId { get; set; }

        [MapField("SRCE")]
        public string EntrySource { get; set; }

        [MapField("HONO")]
        public string HeadOfficePurchaseOrderNumber { get; set; }

        [MapField("SOQN")]
        public int SoqNumber { get; set; }

        [MapField("NOCR")]
        public int Nocr { get; set; }

        [MapField("QTYO")]
        public int TotalQuantity { get; set; }

        [MapField("VALU")]
        public decimal OrderValue { get; set; }

        [MapField("PRFL")]
        public string PrintFlag { get; set; }

        [MapField("NORE")]
        public short ReprintsNumber { get; set; }

        [MapField("RNUM")]
        public int AssignedReceiptNumber { get; set; }

        [MapField("PNUM")]
        public int AssignedPricingDocNumber { get; set; }

        [MapField("RCOM")]
        public bool ReceivedComplete { get; set; }

        [MapField("RPAR")]
        public bool ReceivedPartial { get; set; }

        [MapField("DELM")]
        public bool DeletedByMaintenance { get; set; }

        [MapField("BBCC")]
        public string BbcSupplierCode { get; set; }

        [MapField("COMM")]
        public bool PreparedForCommsToBbc { get; set; }

        [MapField("TNET")]
        public bool TraderNetSupplier { get; set; }

        [MapField("CONF")]
        public string ConfirmationIndicator { get; set; }

        [MapField("REAS")]
        public short ReasonCodes { get; set; }

        [MapField("MES1")]
        public string FreeFormMessage1 { get; set; }

        [MapField("MES2")]
        public string FreeFormMessage2 { get; set; }

        [MapField("OKIS")]
        public bool IssueFileValidated { get; set; }

        [MapField("LastUpdated")]
        public DateTime LastUpdated { get; set; }
    }
}
