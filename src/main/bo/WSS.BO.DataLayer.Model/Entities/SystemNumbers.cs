﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SystemNumbers")]
    public class SystemNumbers
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("NextNumber")]
        public int NextNumber { get; set; }

        [MapField("Min")]
        public int MinNumber { get; set; }

        [MapField("Max")]
        public int MaxNumber { get; set; }
    }
}
