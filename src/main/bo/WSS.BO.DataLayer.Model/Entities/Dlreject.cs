﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLREJECT")]
    public class Dlreject : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlreject()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            SequenceNumber = 0;
            EmployeeId = String.Empty;
            SkuNumber = String.Empty;
            RejectText = String.Empty;
            ItemGroupId = String.Empty;
            ItemPromptId = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            FillerForFutureUse = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("SEQN")]
        public decimal SequenceNumber { get; set; }

        [MapField("EEID")]
        public string EmployeeId { get; set; }

        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("REJECTTEXT")]
        public string RejectText { get; set; }

        [MapField("GROUPID")]
        public string ItemGroupId { get; set; }

        [MapField("PROMPTID")]
        public string ItemPromptId { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
