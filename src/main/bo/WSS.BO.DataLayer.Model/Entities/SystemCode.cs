﻿using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SystemCodes")]
    public class SystemCode
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
