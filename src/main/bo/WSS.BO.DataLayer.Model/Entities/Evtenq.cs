using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("EVTENQ")]
    public class Evtenq
    {
        // not all fields are here

        [MapField("PRIO")]
        public string Prio { get; set; }
    }
}
