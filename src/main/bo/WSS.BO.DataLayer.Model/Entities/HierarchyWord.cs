using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("HierarchyWord")]
    public class HierarchyWord
    {
        // not all fields are here

        [MapField("NUMB")]
        public string Number { get; set; }
    }
}
