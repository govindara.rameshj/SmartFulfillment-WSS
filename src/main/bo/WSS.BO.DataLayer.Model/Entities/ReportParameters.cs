using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportParameters")]
    public class ReportParameters
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("ParameterId")]
        public int ParameterId { get; set; }

        [MapField("Sequence")]
        public int Sequence { get; set; }

        [MapField("AllowMultiple")]
        public bool AllowMultiple { get; set; }

        [MapField("DefaultValue"), NullValue(null)]
        public string DefaultValue { get; set; }
    }
}
