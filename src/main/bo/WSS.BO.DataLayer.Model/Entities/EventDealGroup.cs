using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("EVTDLG")]
    public class EventDealGroup
    {
        [MapField("NUMB")]
        public string EventNumber { get; set; }

        [MapField("DLGN")]
        public string DealGroupNumber { get; set; }

        [MapField("TYPE")]
        public string DealType  { get; set; }

        [MapField("KEY1")]
        public string ItemKey { get; set; }
        
        [MapField("IDEL")]
        public bool IsDeleted { get; set; }
        
        [MapField("PERO")]
        public decimal PercentageOfErosionAllocated { get; set; }
        
        [MapField("QUAN")]
        public int Quantity { get; set; }
        
        [MapField("SPARE")]
        public string Spare { get; set; }
        
        [MapField("VERO")]
        public decimal ValueErosionAllocated  { get; set; }
    }
}
