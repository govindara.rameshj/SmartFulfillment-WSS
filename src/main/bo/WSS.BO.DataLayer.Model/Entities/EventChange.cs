using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("EVTCHG")]
    public class EventChange
    {
        [MapField("SKUN")]
        public string SkuNumber { get; set; }
        [MapField("SDAT")]
        public DateTime StartDate { get; set; }
        [MapField("PRIO")]
        public string Priority { get; set; }
        [MapField("NUMB")]
        public string EventNumber { get; set; }
        [MapField("EDAT")]
        public DateTime? EndingDate { get; set; }
        [MapField("PRIC")]
        public decimal Price { get; set; }
        [MapField("IDEL")]
        public bool IsDeleted { get; set; }
    }
}
