﻿using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DeliveryChargeGroupValue")]
    public class DeliveryChargeGroupValue
    {
        public int Id { get; set; }
        public string Group { get; set; }
        public decimal Value { get; set; }
    }
}
