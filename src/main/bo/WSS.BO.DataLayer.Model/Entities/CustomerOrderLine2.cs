﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORLIN2")]
    public class CustomerOrderLine2
    {
        public CustomerOrderLine2()
        {
            OrderNumber = "000000";
            LineNumber = "0000";
            SourceOrderLineNo = 0;
        }

        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        [MapField("LINE")]
        public string LineNumber { get; set; }

        public int SourceOrderLineNo { get; set; }
    }
}
