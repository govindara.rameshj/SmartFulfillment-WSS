using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    [TableName("VisionPayment")]
    public class VisionPayment
    {
        public VisionPayment()
        {
            ValueTender = 0;
            TranDate = new DateTime();
            TillId = 1;
            TranNumber = 1;
            Number = 1;
            TenderTypeId = 1;
            IsPivotal = false;
        }

        public decimal ValueTender { get; set; }
        public DateTime TranDate { get; set; }
        public int TillId { get; set; }
        public int TranNumber { get; set; }
        public int Number { get; set; }
        public int TenderTypeId { get; set; }
        public bool IsPivotal { get; set; }
    }
}
