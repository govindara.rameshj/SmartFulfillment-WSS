﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("PURLIN")]
    public class PurchaseOrderLine
    {
        [MapField("TKEY")]
        [Identity]
        public int DigCompilerKey { get; set; }

        [MapField("HKEY")]
        public int HeaderKey { get; set; }

        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("KDES")]
        public string KeyedDescription { get; set; }

        [MapField("PROD")]
        public string SupplierProductCode { get; set; }

        [MapField("QTYO")]
        public int QuantityOrdered { get; set; }

        [MapField("PRIC")]
        public decimal Price { get; set; }

        [MapField("COST")]
        public decimal Cost { get; set; }

        [MapField("DLOR")]
        public DateTime LastOrderDate { get; set; }

        [MapField("RQTY")]
        public int QuantityReceived { get; set; }

        [MapField("RDAT")]
        public DateTime? LastReceiptDate { get; set; }

        [MapField("DELE")]
        public bool DeleteLine { get; set; }

        [MapField("WRQT")]
        public int QuantityOfReceipt { get; set; }

        [MapField("WQTB")]
        public decimal QuantityBackordered { get; set; }

        [MapField("WPRI")]
        public decimal? ReceiptPrice { get; set; }

        [MapField("WCOS")]
        public decimal? ReceiptCost { get; set; }

        [MapField("WSHI")]
        public string ShipperNumber { get; set; }

        [MapField("CONF")]
        public string OrderConfirmedIndicator { get; set; }

        [MapField("REAS")]
        public short ReasonCode { get; set; }

        [MapField("MESS")]
        public string FreeFormMessage { get; set; }
    }
}
