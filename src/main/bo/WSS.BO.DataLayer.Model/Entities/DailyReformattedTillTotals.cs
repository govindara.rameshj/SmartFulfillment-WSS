using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DRLDET")]
    public class DailyReformattedTillTotals
    {
        // not all fields are here

        [MapField("NUMB")]
        public string DrlNumber { get; set; }
    }
}
