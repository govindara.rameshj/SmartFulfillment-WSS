﻿using System;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("Locks")]
    public class Locks
    {
        public string EntityId { get; set; }

        public string OwnerId { get; set; }

        public DateTime LockTime { get; set; }
    }
}
