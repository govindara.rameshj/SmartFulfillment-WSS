﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("RELITM")]
    public class RelatedItem
    {
        [MapField("SPOS")]
        public string SingleItemNumber {get; set; }

        [MapField("PPOS")]
        public string PackageItemNumber { get; set; }

        [MapField("NSPP")]
        public decimal NumberOfSinglesPerPack { get; set; }

        [MapField("WTDS")]
        public decimal ThisWeekUnitsTransfered { get; set; }
        
        [MapField("WTDM")]
        public decimal ThisWeekMarkup { get; set; }
        
        [MapField("PTDM")]
        public decimal PeriodToDateMarkup { get; set; }
        
        [MapField("YTDM")]
        public decimal YearToDateMarkup { get; set; }

        [MapField("QTYS")]
        public decimal NumberOfSinglesSoldNotUpdated { get; set; }
    }
}
