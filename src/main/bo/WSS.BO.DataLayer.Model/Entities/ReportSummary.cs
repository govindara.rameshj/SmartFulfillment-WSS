using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportSummary")]
    public class ReportSummary
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("TableId")]
        public int TableId { get; set; }

        [MapField("ColumnId")]
        public int ColumnId { get; set; }

        [MapField("SummaryType")]
        public int SummaryType { get; set; }

        [MapField("ApplyToGroups")]
        public bool ApplyToGroups { get; set; }

        [MapField("Format")]
        public string Format { get; set; }

        [MapField("DenominatorId")]
        public int DenominatorId { get; set; }

        [MapField("NumeratorId")]
        public int NumeratorId { get; set; }
    }
}
