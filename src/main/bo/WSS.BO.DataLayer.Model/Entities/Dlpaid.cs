﻿using System;
using BLToolkit.Mapping;
using BLToolkit.DataAccess;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLPAID")]
    public class Dlpaid : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlpaid()
        {
            Amount = 0;
            AuthCode = String.Empty;
            AuthDescription = String.Empty;
            AuthType = String.Empty;
            CardAcceptType = false;
            CardDescription = String.Empty;
            CardExpireDate = Global.NullPaymentCardDate;
            CardNumber = "0000000000000000000";
            CardStartDate = Global.NullPaymentCardDate;
            TenderType = Constants.TenderType.Cash;
            CashBackAmount = 0;
            ChequeAccountNumber = "0000000000";
            ClassOfCoupon = "00";
            ConversionFactor = 0;
            ConversionRate = 0;
            CouponNumber = "000000";
            CustomerPostCode = String.Empty;
            CreateDate = DateTime.MinValue;
            DigitCount = 0;
            EftposCommsFileIsPrepared = true;
            FilterForFutureUse = String.Empty;
            IsCollected = String.Empty;
            IsProcessedForDbi = false;
            IssueNumberSwitch = String.Empty;
            MerchantNumber = String.Empty;
            Number = "000000";
            PaymentIndex = 0;
            RtiFlag = RtiStatus.NotSet;
            SecurityCode = String.Empty;
            SortCode = "000000";
            SupervisorNumber = Global.NullUserCode;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            TenderStatusCode = String.Empty;
            TransactionUid = String.Empty;
            Value = 0;
            VoucherSequenceNumber = "0000";
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        // In Json
        [MapField("NUMB")]
        public short PaymentIndex { get; set; }

        // Has mapped field in Json, but not for cash. It is 1 for cash.
        [MapField("TYPE")]
        public decimal TenderType { get; set; }

        //In Json
        [MapField("AMNT")]
        public decimal Amount { get; set; }

        // Has mapped field in Json, but for EftPayment (CrCards).
        [MapField("CARD")]
        public string CardNumber { get; set; } // 0000000000000000000 by default for cash

        // Has mapped field in Json (but for EftPayment (CrCards))
        [MapField("EXDT")]
        public string CardExpireDate { get; set; } // 0000 by default for cash

        // Has mapped field in Json, but is used only in Voucher payment
        [MapField("COPN")]
        public string CouponNumber { get; set; } // 000000 for all sales except a coupon payment 

        [MapField("CLAS")]
        public string ClassOfCoupon { get; set; } // 00 by default for all payments

        // Has mapped field in Json, but for EftPayment (CrCards)  and GiftCardPayment.
        // !!! There is a fild with the same name in DLGIFTCARD in Json. I think that code for both cases is the same. Anyway, mapping's doc says so
        [MapField("AUTH")]
        public string AuthCode { get; set; } // empty for non-card and non-gift card payments 

        // Has mapped field in Json, but for EftPayment (CrCards). 
        [MapField("CKEY")]
        public bool CardAcceptType { get; set; } // 0 by default for non-card payments

        [MapField("SUPV")]
        public string SupervisorNumber { get; set; } // In document there no field is stated in "Mapped field" column, so I think that 000 by default is fine

        // Has mapped field in Json, but for vouchers
        [MapField("POST")]
        public string CustomerPostCode { get; set; } // empty by default for non-voucher payments

        [MapField("CKAC")]
        public string ChequeAccountNumber { get; set; } // 0000000000 by default

        [MapField("CKSC")]
        public string SortCode { get; set; } // 000000 by default

        [MapField("CKNO")]
        public string Number { get; set; } // 000000 by default

        [MapField("DBRF")]
        public bool IsProcessedForDbi { get; set; } // 0 by default

        [MapField("SEQN")]
        public string VoucherSequenceNumber { get; set; } // 0000 by default

        // Has mapped field in Json, but for EftPayment (CrCards).
        [MapField("ISSU")]
        public string IssueNumberSwitch { get; set; } // empty by default for non-card payments

        // Has mapped field in Json, but for EftPayment (CrCards) and GiftCardPayment.
        [MapField("ATYP")]
        public string AuthType { get; set; } // empty for non-card and non-gift card payments 

        // Has mapped field in Json, but for EftPayment (CrCards).
        [MapField("MERC")]
        public string MerchantNumber { get; set; } // empty by default for non-card payments

        [MapField("CBAM")]
        public decimal CashBackAmount { get; set; } // 0.00 by default

        [MapField("DIGC")]
        public short DigitCount { get; set; } // 0 by default

        [MapField("ECOM")]
        public bool EftposCommsFileIsPrepared { get; set; } // 1 by default, I have no idea why

        // Has mapped field in Json, but for EftPayment (CrCards) and GiftCardPayment.
        [MapField("CTYP")]
        public string CardDescription { get; set; } // empty for non-card and non-gift card payments 

        // Has mapped field in Json, but for EftPayment (CrCards).
        [MapField("EFID")]
        public string TransactionUid { get; set; } // empty by default for non-card payments

        [MapField("EFTC")]
        public string IsCollected { get; set; } // empty by default. Strange naiming

        // Has mapped field in Json, but for EftPayment (CrCards)
        [MapField("STDT")]
        public string CardStartDate { get; set; } // 0000 by default for non-card payments

        // Has mapped field in Json, but for EftPayment (CrCards)
        [MapField("AUTHDESC")]
        public string AuthDescription { get; set; } // empty by default for non-card payments

        [MapField("SECCODE")]
        public string SecurityCode { get; set; } // empty by default

        [MapField("TENC")]
        public string TenderStatusCode { get; set; } // empty by default. Marked as *W15 NO LONGER USED 

        [MapField("MPOW")]
        public int ConversionFactor { get; set; } // 0 by default. Marked as *W15 NO LONGER USED 

        [MapField("MRAT")]
        public decimal ConversionRate { get; set; } // 0.00000 by default. Marked as *W15 NO LONGER USED 

        [MapField("TENV")]
        public decimal Value { get; set; } //0.00 by default. Marked as *W15 NO LONGER USED 

        [MapField("RTI")]
        public string RtiFlag { get; set; }
        // N for cash 
        // NULL or S or C or N for change
        // empty or N or C for visa/mastercard
        // N or C for maestro
        // C for amex
        // C for ProjectLoan
        // N or C for Voucher
        // C for H/O cheque
        // C or S for webtender
        // N or C for cheque
        // NULL or C for gift card
        // N or C for gift token

        // Stupid naming, but, anyway
        [MapField("SPARE")]
        public string FilterForFutureUse { get; set; } // null for webtender, empty for others by default
    }
}
