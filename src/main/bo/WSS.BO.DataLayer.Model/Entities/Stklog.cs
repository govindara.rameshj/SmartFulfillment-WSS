﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("STKLOG")]
    public class Stklog
    {
        [MapField("TKEY")]
        [Identity]
        public int Tkey { get; set; }

        [MapField("SKUN")]
        public string ItemNumber { get; set; }

        [MapField("DAYN")]
        public decimal DayNumber { get; set; }

        [MapField("TYPE")]
        public string TypeOfMovement { get; set; }
        // If Qty > 0 then TYPE = 01 
        //else (refund, as I understand) TYPE = 02

        //If IsFromMarkDown (Json) = 1 => TYPE = 04

        //If IsBackDoor (Json) = 1 => TYPE 05 

        // TODO change for date1 from dltots
        [MapField("DATE1")]
        public DateTime DateOfMovement { get; set; } // Dltots.CreateDateTime

        [MapField("TIME")]
        public string TimeOfMovement { get; set; } // dltots.TimeOfTransaction

        [MapField("KEYS")]
        public string MovementKeys { get; set; } // Is got from DLLINE.DATE1(DD/MM/YY), DLLINE.TILL, DLLINE.TRAN, DLLINE.NUMB; Not sure how this formula works

        // In Json
        [MapField("EEID")]
        public string CashierNumber { get; set; } //I think, it's fine if we take this value from json, no matter that in doc it's not mapped

        [MapField("ICOM")]
        public bool IsSentToHo { get; set; } // 0 by default

        [MapField("SSTK")]
        public decimal StartingStockOnHand { get; set; } // STKMAS.ONHA

        [MapField("SRET")]
        public decimal OpenReturnsQuantity { get; set; } // STKMAS.RETQ

        [MapField("SPRI")]
        public decimal StartingPrice { get; set; } // STKMAS.PRIC

        [MapField("ESTK")]
        public decimal EndingStockOnHand { get; set; } // IsFromMarkDown (Json) ? STKMAS.ONHA : STKMAS.ONHA - Qty

        [MapField("ERET")]
        public decimal EndingReturnsQuantity { get; set; } // STKMAS.RETQ

        [MapField("EPRI")]
        public decimal EndingPrice { get; set; } // !!!!!!!! Comment in doc : STKMAS.PRIC. Maybe the price should be calculated with discount.
        // Seryoga has recommended to set STKMAS.PRIC value here

        [MapField("SMDN")]
        public decimal StartingMarkdownStockOnHand { get; set; } // STKMAS.MDNQ

        [MapField("SWTF")]
        public decimal StartingWriteOffStockOnHand { get; set; } // STKMAS.WTFQ

        [MapField("EMDN")]
        public decimal EndingMarkdownStockOnHand { get; set; } // IsFromMarkDown (Json) ? STKMAS.MDNQ - Qty : STKMAS.MDNQ

        [MapField("EWTF")]
        public decimal EndingWriteOffStockOnHand { get; set; } // This field is filled by old till in the way of updating. As we won't update smth and will just have "final" result, just set STKMAS.WTFQ value here

        [MapField("RTI")]
        public string RtiStatus { get; set; } // always S for sale

        [MapField("SPARE")]
        public string FilterForFutureUse { get; set; } // null; Just null, Seryoga said that this field is useless

        public static string GenerateMovementKeys(DailyTillLine dlline)
        {
            return dlline.CreateDate.ToString("dd/MM/yy") + dlline.TillNumber + dlline.TransactionNumber +
                   dlline.SequenceNumber;
        }
    }
}
