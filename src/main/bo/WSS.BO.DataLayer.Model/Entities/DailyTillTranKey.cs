﻿using System;

namespace WSS.BO.DataLayer.Model.Entities
{
    public class DailyTillTranKey : IDailyTillTranKey
    {
        public DateTime CreateDate { get; set; }
        public string TillNumber { get; set; }
        public string TransactionNumber { get; set; }
    }
}
