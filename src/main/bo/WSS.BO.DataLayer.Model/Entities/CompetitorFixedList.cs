﻿using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CompetitorFixedList")]
    public class CompetitorFixedList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
