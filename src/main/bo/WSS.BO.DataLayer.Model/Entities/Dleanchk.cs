﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLEANCHK")]
    internal class Dleanchk : IDailyTillTranKey
    {
        [MapField("DATE1")]
        public DateTime CreateDate { get; set; } // From DLTOTS

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("SEQN")]
        public decimal SequenceNumber { get; set; }

        [MapField("TIME")]
        public string Time { get; set; } // Current time

        [MapField("ENTRY")]
        public string EntryMethId { get; set; }

        // Keyed or Scanned; depends on if SKU was read by barcode scanner or not

        [MapField("REASONCODE")]
        public string ReasonCode { get; set; } // Keyed reason code ; Seryoga promised to work with it

        [MapField("KREASON")]
        public string KeyReason { get; set; }

        // Reason of keying entry (as I understand it should come in Jsone or smth); empty by default

        public string Item { get; set; } // number that was keyed or scanned (500300, for example)

        [MapField("TYPE")]
        public string TypeOfItem { get; set; } // VAlue depends on ITEM column
        // ITEM.Length = 5 => SKU
        // ITEM.Length = 8 and ITEM.StartsWith("22") => 'In-house'
        // EAN fr others

        //In Json
        [MapField("EEID")]
        public string CashierNumber { get; set; }

        [MapField("EAFOUND")]
        public bool SkuIsInEanmas { get; set; } // EAN record found in EANMAS if TYPE N= 'SKU'

        [MapField("EASKUN")]
        public string EanSkuNumber { get; set; } // EAFOUND ? Sku Number : NULL

        [MapField("EASKUFOUND")]
        public bool EanSkuFound { get; set; } // in our db is aways zero

        [MapField("SKUN")]
        public string KeyedSku { get; set; } // SKU that was keyed by cashier

        [MapField("RTI")]
        public string RtiFlag { get; set; } // S for sale

        [MapField("SPARE")]
        public string Spare { get; set; } //always null
    }
}
