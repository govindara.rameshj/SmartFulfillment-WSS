using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLGIFT")]
    public class DailyGiftToken : IDailyTillTranKey, ISyncronizedToRti
    {
        public DailyGiftToken()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            LineNumber = 0;
            SerialNumber = String.Empty;
            EmployeeId = String.Empty;
            TransactionType = String.Empty;
            TenderAmount = 0;
            CommedToHo = false;
            RtiFlag = RtiStatus.NotSet;
            FillerForFutureUse = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("LINE")]
        public short LineNumber { get; set; }

        [MapField("SERI")]
        public string SerialNumber { get; set; }

        [MapField("EEID")]
        public string EmployeeId { get; set; }

        [MapField("TYPE")]
        public string TransactionType { get; set; }

        [MapField("AMNT")]
        public decimal TenderAmount { get; set; }

        [MapField("COMM")]
        public bool CommedToHo { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
