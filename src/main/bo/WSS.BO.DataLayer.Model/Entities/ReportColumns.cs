using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportColumns")]
    public class ReportColumns
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("TableId")]
        public int TableId { get; set; }

        [MapField("ColumnId")]
        public int ColumnId { get; set; }

        [MapField("Sequence")]
        public int Sequence { get; set; }

        [MapField("IsVisible")]
        public bool IsVisible { get; set; }

        [MapField("IsBold")]
        public bool IsBold { get; set; }

        [MapField("IsHighlight")]
        public bool IsHighlight { get; set; }

        [MapField("HighlightColour"), NullValue(null)]
        public string HighlightColour { get; set; }

        [MapField("Fontsize"), NullValue(null)]
        public Single Fontsize { get; set; }
    }
}
