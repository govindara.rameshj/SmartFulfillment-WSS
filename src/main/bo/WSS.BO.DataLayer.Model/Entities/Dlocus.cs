﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLOCUS")]
    public class Dlocus : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlocus()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            Name = String.Empty;
            AddressLine1 = String.Empty;
            AddressLine2 = String.Empty;
            AddressLine3 = String.Empty;
            AddressLine4 = String.Empty;
            PostCode = String.Empty;
            PhoneNumber = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            FillerForFutureUse = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("NAME")]
        public string Name { get; set; }

        [MapField("ADD1")]
        public string AddressLine1 { get; set; }

        [MapField("ADD2")]
        public string AddressLine2 { get; set; }

        [MapField("ADD3")]
        public string AddressLine3 { get; set; }

        [MapField("ADD4")]
        public string AddressLine4 { get; set; }

        [MapField("POST")]
        public string PostCode { get; set; }

        [MapField("PHON")]
        public string PhoneNumber { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
