﻿
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SafeDenoms")]
    public class SafeDenomination
    {
        public SafeDenomination()
        {
            TenderId = 1;
            ChangeValue = 0;
            SuggestedValue = 0;
        }

        public int PeriodId { get; set; }

        public string CurrencyId { get; set; }

        public int TenderId { get; set; }

        [MapField("ID")]
        public int DenominationTypeId { get; set; }

        public decimal SafeValue { get; set; }

        public decimal ChangeValue { get; set; }

        public decimal SystemValue { get; set; }

        public decimal SuggestedValue { get; set; }
    }
}
