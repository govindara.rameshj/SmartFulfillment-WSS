using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("HHTHDR")]
    public class PicCountHeaderFile
    {
        // not all fields are here

        [MapField("DATE1")]
        public DateTime Date { get; set; }
    }
}
