using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CARD_SCHEME")]
    public class CardScheme
    {
        [MapField("DISCOUNT")]
        public decimal Discount { get; set; }

        [MapField("SCHEME_NAME")]
        public string Name { get; set; }
    }
}
