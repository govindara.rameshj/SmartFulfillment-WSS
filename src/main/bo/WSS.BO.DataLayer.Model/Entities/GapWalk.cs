﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("GapWalk")]
    public class GapWalk
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("Sequence")]
        public int Sequence { get; set; }

        [MapField("DateCreated")]
        public DateTime DateCreated { get; set; }

        [MapField("Location")]
        public String Location { get; set; }

        [MapField("SkuNumber")]
        public String SkuNumber { get; set; }

        [MapField("QuantityRequired")]
        public int QuantityRequired { get; set; }

        [MapField("IsPrinted")]
        public bool IsPrinted{ get; set; }
    }
}
