﻿using System;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("Safe")]
    public class Safe
    {
        public int PeriodId { get; set; }
        public DateTime PeriodDate { get; set; }
        public int UserID1 { get; set; }
        public int UserID2 { get; set; }
        public DateTime LastAmended { get; set; }
        public bool IsClosed { get; set; }
        public bool SafeChecked { get; set; }
        public bool EndOfDayCheckDone { get; set; }
        public DateTime EndOfDayCheckCompleted { get; set; }
        public int EndOfDayCheckManagerID { get; set; }
        public int EndOfDayCheckWitnessID { get; set; }
        public DateTime EndOfDayCheckLockedFrom { get; set; }
        public DateTime EndOfDayCheckLockedTo { get; set; }
    }
}
