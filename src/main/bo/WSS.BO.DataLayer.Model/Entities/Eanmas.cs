﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("EANMAS")]
    public class Eanmas
    {
        [MapField("SKUN")]
        public string SkuNumber { get; set; }
    }
}
