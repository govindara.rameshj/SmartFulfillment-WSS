using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("MenuConfig")]
    public class MenuConfig
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("MasterID")]
        public int MasterId { get; set; }

        [MapField("AppName"), NullValue(null)]
        public string AppName { get; set; }

        [MapField("AssemblyName"), NullValue(null)]
        public string AssemblyName { get; set; }

        [MapField("ClassName"), NullValue(null)]
        public string ClassName { get; set; }

        [MapField("MenuType")]
        public int MenuType { get; set; }

        [MapField("Parameters"), NullValue(null)]
        public string Parameters { get; set; }

        [MapField("ImagePath"), NullValue(null)]
        public string ImagePathWoTrim
        {
            get { return ImagePath; }
            set
            {
                ImagePath = !String.IsNullOrEmpty(value) ? value.Trim() : null;
            }
        }

        [MapIgnore]
        public string ImagePath { get; set; }

        [MapField("LoadMaximised")]
        public bool LoadMaximised { get; set; }

        [MapField("IsModal")]
        public bool IsModal { get; set; }

        [MapField("DisplaySequence")]
        public int DisplaySequence { get; set; }

        [MapField("AllowMultiple")]
        public bool AllowMultiple { get; set; }

        [MapField("WaitForExit")]
        public bool WaitForExit { get; set; }

        [MapField("TabName"), NullValue(null)]
        public string TabName { get; set; }

        [MapField("Description"), NullValue(null)]
        public string Description { get; set; }

        [MapField("ImageKey"), NullValue(null)]
        public string ImageKeyTrimmed
        {
            get { return ImageKey; }
            set
            {
                ImageKey = !String.IsNullOrEmpty(value) ? value.Trim() : null;
            }
        }

        [MapIgnore]
        public string ImageKey { get; set; }

        [MapField("Timeout")]
        public int Timeout { get; set; }

        [MapField("DoProcessingType")]
        public int DoProcessingType { get; set; }
    }
}
