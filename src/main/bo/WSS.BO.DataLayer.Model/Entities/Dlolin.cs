﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLOLIN")]
    public class Dlolin : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlolin()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            LineNumber = 0;
            Name = String.Empty;
            AddressLine1 = String.Empty;
            AddressLine2 = String.Empty;
            AddressLine3 = String.Empty;
            AddressLine4 = String.Empty;
            PostCode = String.Empty;
            PhoneNumber = String.Empty;
            FaxNumber = String.Empty;
            EnteredPricesAreVatInclusive = true; //TODO: Check if true.
            CompetitorsPriceConvertedForComparison = 0;
            IsPromiseOnly = false;
            OriginalStoreNumber = String.Empty;
            OriginalTillNumber = String.Empty;
            OriginalTransactionNumber = String.Empty;
            IsCompatableWithDdf = false;
            OriginalWickesSellingPrice = 0;
            RtiFlag = RtiStatus.NotSet;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }
        
        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("NUMB")]
        public short LineNumber { get; set; }

        [MapField("CNAM")]
        public string Name { get; set; }

        [MapField("CAD1")]
        public string AddressLine1 { get; set; }

        [MapField("CAD2")]
        public string AddressLine2 { get; set; }

        [MapField("CAD3")]
        public string AddressLine3 { get; set; }

        [MapField("CAD4")]
        public string AddressLine4 { get; set; }

        [MapField("CPST")]
        public string PostCode { get; set; }

        [MapField("CPHN")]
        public string PhoneNumber { get; set; }

        [MapField("CFAX")]
        public string FaxNumber { get; set; }

        [MapField("EPRI")]
        public decimal CompetitorsPriceEntered { get; set; }

        [MapField("EVAT")]
        public bool EnteredPricesAreVatInclusive { get; set; }

        [MapField("CPRI")]
        public decimal CompetitorsPriceConvertedForComparison { get; set; }

        [MapField("PREV")]
        public bool IsPromiseOnly { get; set; }

        [MapField("OSTR")]
        public string OriginalStoreNumber { get; set; }

        [MapField("OTIL")]
        public string OriginalTillNumber { get; set; }

        [MapField("OTRN")]
        public string OriginalTransactionNumber { get; set; }

        [MapField("FILL")]
        public bool IsCompatableWithDdf { get; set; }

        [MapField("ODAT")]
        public DateTime? OriginalDate { get; set; }

        [MapField("OPRI")]
        public decimal OriginalWickesSellingPrice { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
