﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TVCSTR")]
    public class ControlFile
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ControlFile()
        {
            FileName = string.Empty;
            VersionNumber = "00";
            SequenceNumber = "000000";
            LastestAvailableVersionNumber = "00";
            LastestAvailableSequenceNumber = "000000";
            PVersionNumber = "00";
            PSequenceNumber = "000000";
            TransactionTime = string.Empty;

            RecordType1 = string.Empty;
            RecordType2 = string.Empty;
            RecordType3 = string.Empty;
            RecordType4 = string.Empty;
            RecordType5 = string.Empty;
            RecordType6 = string.Empty;
            RecordType7 = string.Empty;
            RecordType8 = string.Empty;
            RecordType9 = string.Empty;
            RecordType10 = string.Empty;
            RecordType11 = string.Empty;
            RecordType12 = string.Empty;
            RecordType13 = string.Empty;
            RecordType14 = string.Empty;
            RecordType15 = string.Empty;

            RecordHash1 = decimal.Zero;
            RecordHash2 = decimal.Zero;
            RecordHash3 = decimal.Zero;
            RecordHash4 = decimal.Zero;
            RecordHash5 = decimal.Zero;
            RecordHash6 = decimal.Zero;
            RecordHash7 = decimal.Zero;
            RecordHash8 = decimal.Zero;
            RecordHash9 = decimal.Zero;
            RecordHash10 = decimal.Zero;
            RecordHash11 = decimal.Zero;
            RecordHash12 = decimal.Zero;
            RecordHash13 = decimal.Zero;
            RecordHash14 = decimal.Zero;
            RecordHash15 = decimal.Zero;

            ValuePerHash1 = decimal.Zero;
            ValuePerHash2 = decimal.Zero;
            ValuePerHash3 = decimal.Zero;
            ValuePerHash4 = decimal.Zero;
            ValuePerHash5 = decimal.Zero;
            ValuePerHash6 = decimal.Zero;
            ValuePerHash7 = decimal.Zero;
            ValuePerHash8 = decimal.Zero;
            ValuePerHash9 = decimal.Zero;
            ValuePerHash10 = decimal.Zero;
            ValuePerHash11 = decimal.Zero;
            ValuePerHash12 = decimal.Zero;
            ValuePerHash13 = decimal.Zero;
            ValuePerHash14 = decimal.Zero;
            ValuePerHash15 = decimal.Zero;

            Flag = false;
            ErrorCode = string.Empty;
        }

        [MapField("FILE")]
        public string FileName { get; set; }

        [MapField("VRSN")]
        public string VersionNumber { get; set; }

        [MapField("SEQN")]
        public string SequenceNumber { get; set; }

        [MapField("AVER")]
        public string LastestAvailableVersionNumber { get; set; }

        [MapField("ASEQ")]
        public string LastestAvailableSequenceNumber { get; set; }

        [MapField("PVER")]
        public string PVersionNumber { get; set; }

        [MapField("PSEQ")]
        public string PSequenceNumber { get; set; }

        [MapField("DATE1")]
        public DateTime? TransactionDate { get; set; }

        [MapField("TIME")]
        public string TransactionTime { get; set; }

        [MapField("RTYP1")]
        public string RecordType1 { get; set; }

        [MapField("RTYP2")]
        public string RecordType2 { get; set; }

        [MapField("RTYP3")]
        public string RecordType3 { get; set; }

        [MapField("RTYP4")]
        public string RecordType4 { get; set; }

        [MapField("RTYP5")]
        public string RecordType5 { get; set; }

        [MapField("RTYP6")]
        public string RecordType6 { get; set; }

        [MapField("RTYP7")]
        public string RecordType7 { get; set; }

        [MapField("RTYP8")]
        public string RecordType8 { get; set; }

        [MapField("RTYP9")]
        public string RecordType9 { get; set; }

        [MapField("RTYP10")]
        public string RecordType10 { get; set; }

        [MapField("RTYP11")]
        public string RecordType11 { get; set; }

        [MapField("RTYP12")]
        public string RecordType12 { get; set; }

        [MapField("RTYP13")]
        public string RecordType13 { get; set; }

        [MapField("RTYP14")]
        public string RecordType14 { get; set; }

        [MapField("RTYP15")]
        public string RecordType15 { get; set; }

        [MapField("HREC1")]
        public decimal RecordHash1 { get; set; }

        [MapField("HREC2")]
        public decimal RecordHash2 { get; set; }

        [MapField("HREC3")]
        public decimal RecordHash3 { get; set; }

        [MapField("HREC4")]
        public decimal RecordHash4 { get; set; }

        [MapField("HREC5")]
        public decimal RecordHash5 { get; set; }

        [MapField("HREC6")]
        public decimal RecordHash6 { get; set; }

        [MapField("HREC7")]
        public decimal RecordHash7 { get; set; }

        [MapField("HREC8")]
        public decimal RecordHash8 { get; set; }

        [MapField("HREC9")]
        public decimal RecordHash9 { get; set; }

        [MapField("HREC10")]
        public decimal RecordHash10 { get; set; }

        [MapField("HREC11")]
        public decimal RecordHash11 { get; set; }

        [MapField("HREC12")]
        public decimal RecordHash12 { get; set; }

        [MapField("HREC13")]
        public decimal RecordHash13 { get; set; }

        [MapField("HREC14")]
        public decimal RecordHash14 { get; set; }

        [MapField("HREC15")]
        public decimal RecordHash15 { get; set; }

        [MapField("HVAL1")]
        public decimal ValuePerHash1 { get; set; }

        [MapField("HVAL2")]
        public decimal ValuePerHash2 { get; set; }

        [MapField("HVAL3")]
        public decimal ValuePerHash3 { get; set; }

        [MapField("HVAL4")]
        public decimal ValuePerHash4 { get; set; }

        [MapField("HVAL5")]
        public decimal ValuePerHash5 { get; set; }

        [MapField("HVAL6")]
        public decimal ValuePerHash6 { get; set; }

        [MapField("HVAL7")]
        public decimal ValuePerHash7 { get; set; }

        [MapField("HVAL8")]
        public decimal ValuePerHash8 { get; set; }

        [MapField("HVAL9")]
        public decimal ValuePerHash9 { get; set; }

        [MapField("HVAL10")]
        public decimal ValuePerHash10 { get; set; }

        [MapField("HVAL11")]
        public decimal ValuePerHash11 { get; set; }

        [MapField("HVAL12")]
        public decimal ValuePerHash12 { get; set; }

        [MapField("HVAL13")]
        public decimal ValuePerHash13 { get; set; }

        [MapField("HVAL14")]
        public decimal ValuePerHash14 { get; set; }

        [MapField("HVAL15")]
        public decimal ValuePerHash15 { get; set; }

        [MapField("FLAG")]
        public bool Flag { get; set; }

        [MapField("ECOD")]
        public string ErrorCode { get; set; }
    }
}
