﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("QUOHDR")]
    public class QuoteHeader
    {
        [MapField("NUMB")]
public string Number {get; set;} //??? As I understand, just next number

[MapField("CUST")]
public string Customer {get; set;} //??? It's written, that we need to check CUSMAS. It's empty. The field is empty.

        //In Json
[MapField("DATE1")]
public DateTime DateEntered {get; set;}

[MapField("CANC")]
public bool IsCancelled {get; set;} //??? I think it's connected to "cancellations". Need to check

[MapField("EXPD")]
public DateTime ExpiryDate {get; set;} //???

[MapField("SOLD")]
public bool IsSold {get; set;} //???

[MapField("DELI")]
public bool IsForDelivery {get; set;} //???

[MapField("MVST")]
public decimal MerchandiseValueStore {get; set;}

[MapField("DCST")]
public decimal DeliveryChargeStore {get; set;}

[MapField("WGHT")]
public decimal Weight {get; set;} // Is 0 in our DB, but we have WGHT column in STKMAS, so I think it's possible to use it in the future (as it's stated in the doc)

[MapField("VOLU")]
public decimal Volume { get; set; } // Is 0 in our DB, but we have VOLU column in STKMAS, so I think it's possible to use it in the future (as it's stated in the doc)

        // In Json
[MapField("QDAT")]
public DateTime CreateDateTime { get; set; } // CreateDateTime from Dltots

        //In Json
[MapField("QTIL")]
public string TillNumber {get; set;} // TillNumber from Dltots

//In Json
[MapField("QTRN")]
public string TransactionNumber { get; set; } // TransactionNumber from Dltots

[MapField("CARD_NO")]
public string DiscountCardNumber {get; set;} // ???

[MapField("FOCDEL")]
public string FreeOfChargeDeliverySupervisor {get; set;} // ???

[MapField("SPARE")]
public string Spare {get; set;} // 
    }
}
