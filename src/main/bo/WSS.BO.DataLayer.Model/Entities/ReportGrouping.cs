using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportGrouping")]
    public class ReportGrouping
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("TableId")]
        public int TableId { get; set; }

        [MapField("ColumnId")]
        public int ColumnId { get; set; }

        [MapField("Sequence")]
        public int Sequence { get; set; }

        [MapField("IsDescending")]
        public bool IsDescending { get; set; }

        [MapField("SummaryType")]
        public int SummaryType { get; set; }

        [MapField("ShowExpanded")]
        public bool ShowExpanded { get; set; }
    }
}
