﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORHDR4")]
    public class CustomerOrderHeader4
    {
        public CustomerOrderHeader4()
        {
            AddressLine2 = string.Empty;
            CashierNumber = string.Empty;
            CreateDate = DateTime.MinValue;
            CustomerAddress1 = string.Empty;
            CustomerAddress4 = string.Empty; // default
            ContactInfoIndex = string.Empty;
            DeliveryOrCollectionDate = null;
            DeliveryStatus = OrderConsignmentStatus.DeliveryRequest; // default
            DispatchedDate = null; // default
            Email = string.Empty;
            FreeDeliveryAuthorizerNumber = string.Empty;
            IsSuspended = false; // default
            Name = string.Empty;
            OMOrderNumber = 0; // default
            OrderNumber = string.Empty;
            PostCode = string.Empty;
            RefundStatus = OrderConsignmentRefundStatus.None;
            SellingStoreId = 8000;
            SellingStoreOrderId = 0;
            Town = string.Empty;
            WorkPhone = string.Empty;
        }

        // In Json
        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        // In Json
        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        // In Json
        [MapField("DELD")]
        public DateTime? DeliveryOrCollectionDate { get; set; }

        // In Json
        public string Name { get; set; }

        // In Json
        [MapField("CUST")]
        public string ContactInfoIndex { get; set; }

        [MapField("DDAT")]
        public DateTime? DispatchedDate { get; set; } // Null by default. Till has no ability to insert the value. Only BO has. 

        // In Json
        [MapField("OEID")]
        public string CashierNumber { get; set; }

        // In Json
        [MapField("FDID")]
        public string FreeDeliveryAuthorizerNumber { get; set; }

        public int DeliveryStatus { get; set; } // 100 by default. can be changed only via BO

        public int RefundStatus { get; set; } // 0 by default when sale

        public int SellingStoreId { get; set; } // SellingStoreId (= 8000) + GetStoreId() (This function is already created)

        // In Json
        public int SellingStoreOrderId { get; set; } // OrderNumber

        public int OMOrderNumber { get; set; } // 0 by default

        // In Json
        public string CustomerAddress1 { get; set; } // string.Format("{0},{1}", Json.houseNumber, Json.AddressLine1

        // In Json
        [MapField("CustomerAddress2")]
        public string AddressLine2 { get; set; }

        // In Json
        [MapField("CustomerAddress3")]
        public string Town { get; set; }

        public string CustomerAddress4 { get; set; } // empty by default

        // In Json
        [MapField("CustomerPostcode")]
        public string PostCode { get; set; }

        // In Json
        [MapField("CustomerEmail")]
        public string Email { get; set; }

        // In Json
        [MapField("PhoneNumberWork")]
        public string WorkPhone { get; set; }

        [MapField("IsSuspended")]
        public bool IsSuspended { get; set; } // 0 by default. In our db there is no "true" values

    }
}
