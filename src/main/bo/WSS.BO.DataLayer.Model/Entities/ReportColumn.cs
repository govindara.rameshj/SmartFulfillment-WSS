using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportColumn")]
    public class ReportColumn
    {
        [MapField("Id")]
        public int Id { get; set; }

        [MapField("Name")]
        public string Name { get; set; }

        [MapField("Caption"), NullValue(null)]
        public string Caption { get; set; }

        [MapField("FormatType")]
        public int FormatType { get; set; }

        [MapField("Format"), NullValue(null)]
        public string Format { get; set; }

        [MapField("IsImagePath")]
        public bool IsImagePath { get; set; }

        [MapField("MinWidth")]
        public int MinWidth { get; set; }

        [MapField("MaxWidth")]
        public int MaxWidth { get; set; }

        [MapField("Alignment")]
        public int Alignment { get; set; }

    }
}
