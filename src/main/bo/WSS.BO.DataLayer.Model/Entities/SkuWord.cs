using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("skuword")]
    public class SkuWord
    {
        // not all fields are here

        [MapField("TenderTypeId")]
        public string Id { get; set; }
    }
}
