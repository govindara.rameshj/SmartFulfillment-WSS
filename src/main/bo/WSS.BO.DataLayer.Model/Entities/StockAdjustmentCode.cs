using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SACODE")]
    public class StockAdjustmentCode
    {
        [MapField("NUMB")]
        public string CodeNumber { get; set; }

        public string Type { get; set; }
    }
}
