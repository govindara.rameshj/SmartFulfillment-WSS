﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TENOPT")]
    public class TenderOption
    {
        [MapField("TEND")]
        public decimal TenderNumber { get; set; }

        [MapField("TTID")]
        public decimal TenderTypeIdentifier { get; set; }

        [MapField("TTDE")]
        public string TenderTypeDescription { get; set; }

        [MapField("TTDS")]
        public decimal TenderTypeDS { get; set; }

        [MapField("TTOT")]
        public bool IsOverTenderAllowed { get; set; }

        [MapField("TTCC")]
        public bool CaptureCreditCardDetailsOn { get; set; }

        [MapField("TTEF")]
        public bool EFTPoSIsUsed { get; set; }

        [MapField("TODR")]
        public bool IsOpenCashDrawerNeeded { get; set; }

        [MapField("TADA")]
        public bool HasDefaultRemainingAmount { get; set; }

        [MapField("PFOC")]
        public bool PrintChequeFrontChequeIsAllowed { get; set; }

        [MapField("PBOC")]
        public bool PrintChequeBackIsForced { get; set; }

        [MapField("MINV")]
        public decimal MinimumValue { get; set; }

        [MapField("MAXV")]
        public decimal MaximumValue { get; set; }

        [MapField("FLIM")]
        public decimal FLimit { get; set; }

        public bool InUse { get; set; }
    }
}
