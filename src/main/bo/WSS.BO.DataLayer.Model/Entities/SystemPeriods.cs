﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SystemPeriods")]
    public class SystemPeriods
    {
        [MapField("ID")]
        public int PeriodId {get; set;}

        [MapField("StartDate")]
        public DateTime StartDate {get; set;}
    }
}
