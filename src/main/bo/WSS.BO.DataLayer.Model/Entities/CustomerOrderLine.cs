﻿using BLToolkit.Mapping;
using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORLIN")]
    public class CustomerOrderLine
    {
        public CustomerOrderLine()
        {
            DeliverySource = string.Empty; // default
            DeliverySourceIbtOut = string.Empty; // default
            DeliveryStatus = Constants.OrderConsignmentStatus.DeliveryRequest; // default
            IsDeliveryChargeItem = false;
            LineNumber = string.Empty;
            OrderNumber = string.Empty;
            Price = 0;
            PriceOverrideCode = 0;
            Quantity = 0;
            QuantityScanned = 0; // default
            QuantityTaken = 0; // default
            QtyToBeDelivered = 0;
            RefundQuantity = 0;
            SellingStoreIbtIn = string.Empty; // default
            SellingStoreId = 8000;
            SellingStoreOrderId = 0;
            SkuNumber = string.Empty;
            Volume = 0; // default
            Weight = 0; // default
        }

        // In Json
        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        // In Json
        [MapField("LINE")]
        public string LineNumber { get; set; }

        // In Json
        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        // In Json
        [MapField("QTYO")]
        public decimal Quantity { get; set; }

        [MapField("QTYT")]
        public decimal QuantityTaken { get; set; }

        [MapField("QTYR")]
        public decimal RefundQuantity { get; set; } // not for sale

        [MapField("WGHT")]
        public decimal Weight { get; set; } // 0 by default. Marked as "for future use"

        [MapField("VOLU")]
        public decimal Volume { get; set; } // 0 by default. Marked as "for future use"

        // In Json
        [MapField("PORC")]
        public int PriceOverrideCode { get; set; } 

        public int DeliveryStatus { get; set; } // 100 when just sale and not taken

        public int SellingStoreId { get; set; } // SellingStoreId (is already 8000) + GetStoreId() (This function is already created)

        // In Json
        public int SellingStoreOrderId { get; set; } // OrderNumber. I don't know why is it duplicated

        public int QtyToBeDelivered { get; set; } // Is calculated as QTYO-QTYT-QTYR. But as we don't know QTYT, so I think it's always QTYO-QTYR

        public string DeliverySource { get; set; } // empty by default

        public string DeliverySourceIbtOut { get; set; }

        public string SellingStoreIbtIn { get; set; }

        public decimal Price { get; set; }

        public bool IsDeliveryChargeItem { get; set; } // GetSaleTypeAttributeFromStockMaster(cusmas.SkuNumber) (method in dlf) == "D" ? true : false 

        public int QuantityScanned { get; set; } // 0 by default (always 0 in our db)

        public int? RequiredFulfiller { get; set; }
    }
}
