using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORHDR5")]
    public class CustomerOrderHeader5
    {
        public CustomerOrderHeader5()
        {
            OrderNumber = string.Empty;
            Source = string.Empty;
            SourceOrderNumber = string.Empty;
            ExtendedLeadTime = false;
            DeliveryContactName = null;
            DeliveryContactPhone = null;
            CustomerAccountNo = string.Empty;
            IsClickAndCollect = false;
        }

        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        public string Source { get; set; }

        public string SourceOrderNumber { get; set; }

        public bool ExtendedLeadTime { get; set; }

        public string DeliveryContactName { get; set; }

        public string DeliveryContactPhone { get; set; }

        public string CustomerAccountNo { get; set; }

        public bool IsClickAndCollect { get; set; }
    }
}
