using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("Parameters")]
    public class Parameter
    {
        [PrimaryKey, Identity]
        [MapField("ParameterID")]
        public int Id { get; set; }

        [MapField("Description"), NullValue(null)]
        public string Description { get; set; }

        [MapField("StringValue"), NullValue(null)]
        public string StringValue { get; set; }

        [MapField("LongValue")]
        public int LongValue { get; set; }

        [MapField("BooleanValue"), NullValue(null)]
        public bool BooleanValue { get; set; }

        [MapField("DecimalValue")]
        public Decimal DecimalValue { get; set; }

        [MapField("ValueType")]
        public int ValueType { get; set; }
    }
}
