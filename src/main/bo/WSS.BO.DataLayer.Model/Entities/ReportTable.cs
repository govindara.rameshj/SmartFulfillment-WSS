using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportTable")]
    public class ReportTable
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("Id")]
        public int Id { get; set; }

        [MapField("Name")]
        public string Name { get; set; }

        [MapField("AllowGrouping")]
        public bool AllowGrouping { get; set; }

        [MapField("AllowSelection")]
        public bool AllowSelection { get; set; }

        [MapField("ShowHeaders")]
        public bool ShowHeaders { get; set; }

        [MapField("ShowIndicator")]
        public bool ShowIndicator { get; set; }

        [MapField("ShowLinesVertical")]
        public bool ShowLinesVertical { get; set; }

        [MapField("ShowLinesHorizontal")]
        public bool ShowLinesHorizontal { get; set; }

        [MapField("ShowBorder")]
        public bool ShowBorder { get; set; }

        [MapField("ShowInPrintOnly")]
        public bool ShowInPrintOnly { get; set; }

        [MapField("AutoFitColumns")]
        public bool AutoFitColumns { get; set; }

        [MapField("HeaderHeight")]
        public int HeaderHeight { get; set; }

        [MapField("RowHeight")]
        public int RowHeight { get; set; }

        [MapField("AnchorTop")]
        public bool AnchorTop { get; set; }

        [MapField("AnchorBottom")]
        public bool AnchorBottom { get; set; }
    }
}
