﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ZREADS")]
    public class Zreads
    {
        [MapField("COUN")]
        public decimal Count { get; set; } // COUN = COUN + 1

        [MapField("AMNT")]
        public decimal Amount { get; set; } // AMNT = AMNT - PRICE

        [MapField("WSID")]
        public string WorkstationId { get; set; } //WSID = '01'

        [MapField("TYPE")]
        public string Type { get; set; } // TYPE = 'TE' for sale

        [MapField("STYP")]
        public string SubType { get; set; } // STYP = '01 ' for sale

    }
}
