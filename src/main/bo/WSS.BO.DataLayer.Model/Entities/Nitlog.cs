﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("NITLOG")]
    public class Nitlog
    {
        public Nitlog()
        {
            Date1 = DateTime.MinValue;
            Nset = String.Empty;
            TaskNumber = String.Empty;
            Description = String.Empty;
            ProgramToRun = String.Empty;
            TaskStartedDate = DateTime.MinValue;
            TaskStartedTime = String.Empty;
            TaskEndDate = DateTime.MinValue;
            TaskEndTime = String.Empty;
            TaskAbortedDate = DateTime.MinValue;
            TaskAbortedTime = String.Empty;
            JobError = String.Empty;
            ErrorMessage = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime Date1 { get; set; }

        [MapField("NSET")]
        public string Nset { get; set; }

        [MapField("TASK")]
        public string TaskNumber { get; set; }

        [MapField("DESCR")]
        public string Description { get; set; }

        [MapField("PROG")]
        public string ProgramToRun { get; set; }

        [MapField("SDAT")]
        public DateTime TaskStartedDate { get; set; }

        [MapField("STIM")]
        public string TaskStartedTime { get; set; }

        [MapField("EDAT")]
        public DateTime TaskEndDate { get; set; }

        [MapField("ETIM")]
        public string TaskEndTime { get; set; }

        [MapField("ADAT")]
        public DateTime TaskAbortedDate { get; set; }

        [MapField("ATIM")]
        public string TaskAbortedTime { get; set; }

        [MapField("JERR")]
        public string JobError { get; set; }

        [MapField("EMES")]
        public string ErrorMessage { get; set; }
    }
}
