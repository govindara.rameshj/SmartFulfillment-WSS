﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TOSOPT")]
    public class TillTosOption
    {
        [MapField("DSEQ")]
        public string SequenceNumber { get; set; }

        [MapField("TOSC")]
        public string SaleTypeCode { get; set; }

        [MapField("TOSD")]
        public string SaleTypeDescription { get; set; }

        [MapField("DOCN")]
        public bool DocumentNumberIsAsked { get; set; }

        [MapField("SPRT")]
        public bool SpecialPrintIsOn { get; set; }

        [MapField("OPEN")]
        public bool OpenCashDrawerOn { get; set; }

        [MapField("SPEC")]
        public bool SpecialAccountTenderIsAllowed { get; set; }

        [MapField("SUPV")]
        public bool SupervisorRequired { get; set; }
    }
}
