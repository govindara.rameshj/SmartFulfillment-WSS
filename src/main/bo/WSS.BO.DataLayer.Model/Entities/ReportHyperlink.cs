using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("ReportHyperlink")]
    public class ReportHyperlink
    {
        [MapField("ReportId")]
        public int ReportId { get; set; }

        [MapField("TableId")]
        public int TableId { get; set; }

        [MapField("RowId")]
        public int RowId { get; set; }

        [MapField("ColumnName")]
        public string ColumnName { get; set; }

        [MapField("Value")]
        public string Value { get; set; }
    }
}
