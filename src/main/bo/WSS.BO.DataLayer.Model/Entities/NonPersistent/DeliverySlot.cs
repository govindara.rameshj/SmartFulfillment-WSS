﻿using System;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class DeliverySlot
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}
