﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class StockUpdateForPriceChanges
    {
        public string SkuNumber { get; set; }

        public decimal OnHandQuantity { get; set; }
        public decimal MarkdownStockQuanty { get; set; }

        public bool IsObsolete { get; set; }
        public bool IsNonStock { get; set; }
        public bool IsAutoApplyPriceChanges { get; set; }
    }
}
