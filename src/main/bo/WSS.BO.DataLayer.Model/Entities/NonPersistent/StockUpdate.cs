﻿using System;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class StockUpdate : ISyncronizedToRti
    {
        public string SkuNumber { get; set; }
        public string CashierNumber { get; set; }
        public DateTime DateOfMovement { get; set; }
        public string TimeOfMovement { get; set; }
        public decimal DayNumber { get; set; }
        public string TypeOfMovement { get; set; }
        public string MovementKeys { get; set; }

        public decimal StockOnHandDelta { get; set; }
        
        public decimal MarkdownStockDelta { get; set; }
        
        public int SalesUnits1Delta { get; set; }
        public int SalesUnits2Delta { get; set; }
        public int SalesUnits4Delta { get; set; }
        public int SalesUnits6Delta { get; set; }
        
        public decimal SalesValue1Delta { get; set; }
        public decimal SalesValue2Delta { get; set; }
        public decimal SalesValue4Delta { get; set; }
        public decimal SalesValue6Delta { get; set; }

        public decimal AdjustmentsValue1Delta { get; set; }
        public string RtiFlag { get; set; }
    }
}