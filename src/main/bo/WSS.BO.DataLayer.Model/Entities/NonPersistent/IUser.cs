using System;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public interface IUser
    {
        int GetId();
        string GetCode();
        string Name { get; set; }
        string Initials { get; set; }
        string Position { get; set; }
        string PayrollId { get; set; }
        string Outlet { get; set; }
        string TillReceiptName { get; set; }
        string LanguageCode { get; set; }
        int ProfileId { get; set; }
        bool GetIsDeleted(); 
        DateTime? GetDeletedDate();
        string GetDeletedBy();
        string GetDeletedWhere();
        string Password { get; set; }
        DateTime PasswordExpires { get; set; }
        string SuperPassword { get; set; }
        DateTime? SuperPasswordExpires { get; set; }
        bool IsManager { get; set; }
        bool IsSupervisor { get; set; }
    }
}
