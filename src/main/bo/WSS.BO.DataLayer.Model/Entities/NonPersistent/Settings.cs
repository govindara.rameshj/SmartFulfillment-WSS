namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class Settings
    {
        public string ApiAddress { get; set; }
        public int MaxRetriesNumber { get; set; }
        public int RetryPeriodMinutes { get; set; }
        public string UserManagmentServiceUrl { get; set; }
    }
}
