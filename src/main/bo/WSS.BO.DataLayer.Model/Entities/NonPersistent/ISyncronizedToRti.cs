﻿namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public interface ISyncronizedToRti
    {
        string RtiFlag { get; set; }
    }
}
