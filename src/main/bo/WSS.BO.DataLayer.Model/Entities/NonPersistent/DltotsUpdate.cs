﻿namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class DltotsUpdate
    {
        public bool IsParked { get; set; }
        public bool IsRecalled { get; set; }
        public string StatusSequence { get; set; }
        public string SaveStatus { get; set; }

        public string SourceTransactionId { get; set; }
        public string Source { get; set; }
    }
}
