﻿using System.Collections.Generic;
using System.Linq;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class DeliveryChargeContainer
    {
        readonly IList<DeliveryChargeGroup> deliveryChargeGroups;
        readonly IList<DeliveryChargeGroupValue> deliveryChargeValues;

        public DeliveryChargeContainer(IList<DeliveryChargeGroup> deliveryChargeGroups, IList<DeliveryChargeGroupValue> deliveryChargeValues)
        {
            this.deliveryChargeGroups = deliveryChargeGroups;
            this.deliveryChargeValues = deliveryChargeValues;
        }

        public string GetDeliveryChargeSku(decimal amount)
        {
            var deliveryChargeValue = deliveryChargeValues.FirstOrDefault(x => x.Value == amount);

            if (deliveryChargeValue != null)
            {
                return deliveryChargeGroups.Single(x => x.Id == deliveryChargeValue.Id).SkuNumber;
            }
            else
            {
                return deliveryChargeGroups.Single(x => x.IsDefault).SkuNumber;
            }
        }
    }
}
