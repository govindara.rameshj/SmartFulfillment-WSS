using System;
using BLToolkit.Mapping;
using BLToolkit.DataAccess;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("RETOPT")]
    public class RetailOptions
    {
        // not all fields are here

        [MapField("FKEY")]
        public string KeyToFile { get; set; }

        [MapField("STOR")]
        public string StoreId { get; set; }

        [MapField("SNAM")]
        public string StoreName { get; set; }

        [MapField("VATR1")]
        public decimal VatRate1 { get; set; }

        [MapField("VATR2")]
        public decimal VatRate2 { get; set; }

        [MapField("VATR3")]
        public decimal VatRate3 { get; set; }

        [MapField("VATR4")]
        public decimal VatRate4 { get; set; }

        [MapField("VATR5")]
        public decimal VatRate5 { get; set; }

        [MapField("VATR6")]
        public decimal VatRate6 { get; set; }

        [MapField("VATR7")]
        public decimal VatRate7 { get; set; }

        [MapField("VATR8")]
        public decimal VatRate8 { get; set; }

        [MapField("VATR9")]
        public decimal VatRate9 { get; set; }

        public static int GetAbsoluiteStoreId(int storeId)
        {
            return Global.LiveStoreIdBase + storeId;
        }

        public static int GetAbsoluiteStoreId(string storeId)
        {
            return GetAbsoluiteStoreId(Int32.Parse(storeId));
        }
    }
}
