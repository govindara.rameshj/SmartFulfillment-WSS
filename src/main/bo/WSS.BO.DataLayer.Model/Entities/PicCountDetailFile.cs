using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("HHTDET")]
    public class PicCountDetailFile
    {
        // not all fields are here

        [MapField("DATE1")]
        public DateTime Date { get; set; }

        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("TPRE")]
        public decimal TotalPresoldStock { get; set; }

        [MapField("TSCN")]
        public decimal TotalShopFloorCounts { get; set; }

        [MapField("TWCN")]
        public decimal TotalWarehouseCounts { get; set; }

        [MapField("ONHA")]
        public decimal NormalOnHandStockStartDay { get; set; }
       
    }
}
