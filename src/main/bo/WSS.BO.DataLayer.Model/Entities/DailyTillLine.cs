using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLLINE")]
    public class DailyTillLine : IDailyTillTranKey, ISyncronizedToRti
    {
        public DailyTillLine()
        {
            Amount = 0;
            BackDoorCollected = "N";
            CreateDate = DateTime.MinValue;
            DealGroupChangePriceDifference = 0;
            DealGroupMarginErosionCode = Global.NullEventCode;
            DepartmentNumber = "00";
            EmployeeDiscountAmount = 0;
            ExtendedCost = 0;
            FileForFutureUse = null;
            HierachyChangePriceDifference = 0;
            HiearchyMarginErosionCode = Global.NullEventCode;
            HierarchyCategory = String.Empty;
            HierarchyGroup = String.Empty;
            HierarchyStyle = String.Empty;
            HierarchySubGroup = String.Empty;
            InitialPrice = 0;
            IsBackDoor = false;
            IsFromMarkDown = false;
            IsRelatedItemSingle = false;
            IsReversed = false;
            IsScanned = false;
            ItemPriceVatExclusive = 0;
            LastEventSequence = "000000";
            MarginErosionCode = "000000";
            MultibuyChangePriceDifference = 0;
            MultibuyMarginErosionCode = Global.NullEventCode;
            PartitialQuarantine = "000";
            Price = 0;
            PriceOverrideChangePriceDifference = 0;
            PriceOverrideMarginErosionCode = Global.NullEventCode;
            PriceOverrideCode = 0;
            Quantity = 0;
            QuantityBreakChangePriceDifference = 0;
            QuantityBreakMarginErosionCode = Global.NullEventCode;
            ReversalReasonCode = String.Empty;
            RtiFlag = RtiStatus.NotSet;
            SkuNumber = String.Empty;
            SaleTypeAttribute = String.Empty;
            SecondaryEmployeeSaleErosionValue = 0;
            SequenceNumber = 0;
            SupervisorNumber = String.Empty;
            TransactionNumber = String.Empty;
            TemporaryPriceChangePriceDifference = 0;
            TemporaryPriceMarginErosionCode = Global.NullEventCode;
            TillNumber = String.Empty;
            VatCode = 0;
            VatValue = 0;
            VatRate = String.Empty;
            IsCatchAllItem = false;
            IsTaggetItem = false;
            WeeeRate = "00";
            WeeeSequence = "000";
            WeeeValue = 0;           
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("NUMB")]
        public int SequenceNumber { get; set; } // Should be named as "ItemIndex", but wasn't 'cause it can brake BO logic that uses this field

        [MapField("SKUN")]
        public string SkuNumber { get; set; } // Should be named as "SkuNumber", but wasn't 'cause it can brake BO logic that uses this field

        [MapField("QUAN")]
        public decimal Quantity { get; set; }

        [MapField("PRIC")]
        public decimal Price { get; set; } // (SPRI-TPPD-POPD)*(1 - empl disc rate)

        [MapField("DEPT")]
        public string DepartmentNumber { get; set; } // 00 by default

        //In Json
        [MapField("IBAR")]
        public bool IsScanned { get; set; }

        //In Json
        [MapField("SUPV")]
        public string SupervisorNumber { get; set; }

        //In Json
        [MapField("SPRI")]
        public decimal InitialPrice { get; set; }

        // In our database it's ALWAYS equal to 0
        [MapField("PRVE")]
        public decimal ItemPriceVatExclusive {get; set;} // 0.00 by default 

        [MapField("EXTP")]
        public decimal Amount { get; set; }

        // disc = QBPD + DGPD + MBPD + HSPD
        // pric = SPRI - TPPD - POPD
        // (pric * QUAN - disc)*(1 - empl disc rate) + disc

        [MapField("EXTC")]
        public decimal ExtendedCost { get; set; } // 0 by default

        [MapField("RITM")]
        public bool IsRelatedItemSingle { get; set; } // STKMAS.IRIS 

        //In Json
        [MapField("PORC")]
        public short PriceOverrideCode { get; set; }

        [MapField("ITAG")]
        public bool IsTaggetItem { get; set; } // STKMAS.ITAG

        [MapField("CATA")]
        public bool IsCatchAllItem { get; set; } // 0 by default

        //In Json
        [MapField("VSYM")]
        public string VatRate { get; set; } // About conversion logic ask Alexandra

        // Comes from Json's events -> amount
        [MapField("TPPD")]
        public decimal TemporaryPriceChangePriceDifference { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("TPME")]
        public string TemporaryPriceMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> amount
        [MapField("POPD")]
        public decimal PriceOverrideChangePriceDifference { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("POME")]
        public string PriceOverrideMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> amount
        [MapField("QBPD")]
        public decimal QuantityBreakChangePriceDifference { get; set;}  // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("QBME")]
        public string QuantityBreakMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> amount
        [MapField("DGPD")]
        public decimal DealGroupChangePriceDifference { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("DGME")]
        public string DealGroupMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> amount
        [MapField("MBPD")]
        public decimal MultibuyChangePriceDifference { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("MBME")]
        public string MultibuyMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> amount
        [MapField("HSPD")]
        public decimal HierachyChangePriceDifference { get; set; } // as I understand, if it's not stated then set 0 by default

        // Comes from Json's events -> code
        [MapField("HSME")]
        public string HiearchyMarginErosionCode { get; set; } // as I understand, if it's not stated then set 0 by default

        // In Json
        [MapField("ESPD")]
        public decimal EmployeeDiscountAmount { get; set; } // SPRI - EXTP/QUAN

        [MapField("ESME")]
        public string MarginErosionCode { get; set; } // 000000 by default

        //In Json 
        [MapField("LREV")]
        public bool IsReversed { get; set; }

        [MapField("ESEQ")]
        public string LastEventSequence { get; set; } //000000 by default

        [MapField("CTGY")]
        public string HierarchyCategory { get; set; } // STKMAS.CTGY

        [MapField("GRUP")]
        public string HierarchyGroup { get; set; } // STKMAS.GRUP

        [MapField("SGRP")]
        public string HierarchySubGroup { get; set; } // STKMAS.SGRP

        [MapField("STYL")]
        public string HierarchyStyle { get; set; } // STKMAS.STYL

        [MapField("SALT")]
        public string SaleTypeAttribute { get; set; } // STKMAS.SALT

        [MapField("QSUP")]
        public string PartitialQuarantine { get; set; } // 000 by default for sale; Sergey will research values for other sale types later

        [MapField("ESEV")]
        public decimal  SecondaryEmployeeSaleErosionValue {get; set;} // default is 000, source is unknown

        // In Json
        [MapField("IMDN")]
        public bool IsFromMarkDown { get; set; }

        [MapField("VATN")]
        public decimal VatCode { get; set; } // Depends on VSYM value. Ask Alexandra about logic

        // In Json
        [MapField("VATV")]
        public decimal VatValue { get; set; } // amount - (amount / (1 + VatRate (Json))), amount = EXTP - QBPD - DGPD - MBPD - HSPD

        [MapField("BDCO")]
        public string BackDoorCollected { get; set; } // IsBackDoor ? Y : N

        // In Json
        [MapField("BDCOInd")]
        public bool IsBackDoor { get; set; }

        // In Json
        [MapField("RCOD")]
        public string ReversalReasonCode { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; } // Always S for sale

        [MapField("WEERATE")]
        public string WeeeRate { get; set; } // 00 by default

        [MapField("WEESEQN")]
        public string WeeeSequence { get; set; } // 00 or 000 by default

        [MapField("WEECOST")]
        public decimal WeeeValue { get; set; } // 0.00 by default

        [MapField("SPARE")]
        public string FileForFutureUse { get; set; } // null; Just null, Seryoga said that this field is useless

        public string SourceItemId { get; set; }

        public bool IsExplosive { get; set; }

        public DailyTillLine Clone()
        {
            return Map.ObjectToObject<DailyTillLine>(this);
        }
    }
}
