using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("STKADJ")]
    public class StockAdjustmentFile
    {
        public StockAdjustmentFile()
        {
            Code = "53";
            SkuNumber = "000000";
            SequenceNumber = "00";
            AmendId = 0;
            HasBeenCommitedToHO = true;
            IsReversed = false;
            ParentSeq = "00";
            AdjustmentCount = 1;
            MovedOrWriteOff = "";
        }

        [MapField("DATE1")]
        public DateTime Date { get; set; }

        [MapField("SKUN")]
        public string SkuNumber { get; set; }

        [MapField("INIT")]
        public string AdjustmentAuthoriser { get; set; }

        [MapField("QUAN")]
        public decimal Quantity { get; set; }

        [MapField("PRIC")]
        public decimal Price { get; set; }

        [MapField("WAUT")]
        public string WriteOffCashier { get; set; }

        public string Code { get; set; }

        public string Type { get; set; }

        [MapField("SEQN")]
        public string SequenceNumber { get; set; }

        public int AmendId { get; set; }

        [MapField("COMM")]
        public bool HasBeenCommitedToHO { get; set; }

        [MapField("MOWT")]
        public string MovedOrWriteOff { get; set; }

        public bool IsReversed { get; set; }

        public string ParentSeq { get; set; }

        public int AdjustmentCount { get; set; }

        [MapField("INFO")]
        public string AdjustmentComment { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SSTK")]
        public decimal StartingStockOnHand { get; set; }

        public bool IsStaAuditUpload { get; set; }
    }
}
