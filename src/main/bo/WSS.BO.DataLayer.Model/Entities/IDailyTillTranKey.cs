﻿using System;

namespace WSS.BO.DataLayer.Model.Entities
{
    public interface IDailyTillTranKey
    {
        DateTime CreateDate { get; set; }

        string TillNumber { get; set; }

        string TransactionNumber { get; set; }
    }
}
