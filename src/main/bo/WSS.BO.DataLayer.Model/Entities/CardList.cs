using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CARD_LIST")]
    public class CardList
    {
        [MapField("DELETED")]
        public bool IsDeleted { get; set; }

        [MapField("Card_NO")]
        public string CardNumber { get; set; }

        [MapField("CUST_NAME")]
        public string CustomerName { get; set; }
    }
}
