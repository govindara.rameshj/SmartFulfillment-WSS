﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("TENCTL")]
    public class TenderControl
    {
        [MapField("TTID")]
        public decimal TenderTypeIdentifier { get; set; }

        [MapField("TTDE")]
        public string TenderTypeDescription { get; set; }

        [MapField("IUSE")]
        public bool InUse { get; set; }
    }
}
