﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("STKMAS")]
    public class SkuInfo
    {
        [MapField("SKUN")]
        public string SKU { get; set; }

        [MapField("SALT")]
        public string SaleTypeAttribute { get; set; }

        [MapField("CTGY")]
        public string HierarchyCategory { get; set; }

        [MapField("GRUP")]
        public string HierarchyGroup { get; set; }

        [MapField("SGRP")]
        public string HierarchySubGroup { get; set; }

        [MapField("STYL")]
        public string HierarchyStyle { get; set; }

        [MapField("IRIS")]
        public bool IsRelatedItemSingle { get; set; }

        [MapField("ITAG")]
        public bool IsTaggedItem { get; set; }
    }
}
