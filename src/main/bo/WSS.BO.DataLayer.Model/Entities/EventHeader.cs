using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("EVTHDR")]
    public class EventHeader
    {
        [MapField("NUMB")]
        public string EventNumber  { get; set; }

        [MapField("DESCR")]
        public string Description { get; set; }

        [MapField("PRIO")]
        public string Priority { get; set; }

        [MapField("SDAT")]
        public DateTime StartDate  { get; set; }

        [MapField("EDAT")]
        public DateTime EndDate  { get; set; }

        [MapField("STIM")]
        public string StartTime  { get; set; }

        [MapField("ETIM")]
        public string EndTime { get; set; }
 
        [MapField("DACT1")]
        public bool ActiveDay1  { get; set; }

        [MapField("DACT2")]
        public bool ActiveDay2 { get; set; }

        [MapField("DACT3")]
        public bool ActiveDay3 { get; set; }

        [MapField("DACT4")]
        public bool ActiveDay4 { get; set; }

        [MapField("DACT5")]
        public bool ActiveDay5 { get; set; }

        [MapField("DACT6")]
        public bool ActiveDay6 { get; set; }

        [MapField("DACT7")]
        public bool ActiveDay7 { get; set; }

        [MapField("IDEL")]
        public bool IsDeleted { get; set; }

        [MapField("SPARE")]
        public string Spare { get; set; }
    }
}
