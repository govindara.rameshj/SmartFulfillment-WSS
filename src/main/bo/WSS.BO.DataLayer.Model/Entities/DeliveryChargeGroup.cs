﻿using BLToolkit.DataAccess;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DeliveryChargeGroup")]
    public class DeliveryChargeGroup
    {
        public int Id { get; set; }
        public string Group { get; set; }
        public string SkuNumber { get; set; }
        public bool IsDefault { get; set; }
    }
}
