﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLCOUPON")]
    public class Dlcoupon : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlcoupon()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            SequenceNumber = "0000";
            CouponStatus = "0";
            ManagerId = string.Empty;
            MarketingReferenceNumber = "0000";
            IssueStoreNumber = "000";
            CouponSerialNumber = "000000";
            RtiFlag = RtiStatus.NotSet;
        }

        [MapField("TranDate")]
        public DateTime CreateDate { get; set; }

        [MapField("TranTillID")]
        public string TillNumber { get; set; }

        [MapField("TranNo")]
        public string TransactionNumber { get; set; }

        [MapField("Sequence")]
        public string SequenceNumber { get; set; }

        [MapField("CouponID")]
        public string CouponId { get; set; }

        [MapField("Status")]
        public string CouponStatus { get; set; }

        [MapField("ExcCoupon")]
        public bool IsExclusiveCoupon { get; set; }

        [MapField("MarketRef")]
        public string MarketingReferenceNumber { get; set; }

        [MapField("ReUsable")]
        public bool IsCouponReUsable { get; set; }

        [MapField("IssueStoreNo")]
        public string IssueStoreNumber { get; set; }

        [MapField("SerialNo")]
        public string CouponSerialNumber { get; set; }

        [MapField("ManagerID")]
        public string ManagerId { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }
    }
}
