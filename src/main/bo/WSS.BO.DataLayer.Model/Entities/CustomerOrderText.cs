﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("CORTXT")]
    public class CustomerOrderText
    {
        [MapField("NUMB")]
        public string OrderNumber { get; set; }

        public string Type { get; set; }

        [MapField("LINE")]
        public string LineNumber { get; set; }

        public string Text { get; set; }

        public int SellingStoreId { get; set; }

        public int SellingStoreOrderId { get; set; }
    }
}
