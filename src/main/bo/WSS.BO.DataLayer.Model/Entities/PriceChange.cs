using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("PRCCHG")]
    public class PriceChange
    {
        [MapField("SKUN")]
        public string SkuNumber { get; set; }
        [MapField("PDAT")]
        public DateTime Date { get; set; }
        [MapField("PRIC")]
        public decimal Price { get; set; }
        [MapField("EVNT")]
        public string EventNumber { get; set; }
        [MapField("PRIO")]
        public string Priority { get; set; }
        [MapField("PSTA")]
        public string Status { get; set; }
        [MapField("SHEL")]
        public string ShelfEdge { get; set; }
        [MapField("AUDT")]
        public string ApplyDate { get; set; }
        [MapField("AUAP")]
        public string ApplyPriceDate { get; set; }
        [MapField("MARK")]
        public string Markup { get; set; }
        [MapField("MCOM")]
        public string MarkupComment { get; set; }
        [MapField("LABS")]
        public string LabelSmall { get; set; }
        [MapField("LABM")]
        public string LabelMedium { get; set; }
        [MapField("LABL")]
        public string LabelLarge { get; set; }
        [MapField("EEID")]
        public string EmployeeId { get; set; }
        [MapField("MEID")]
        public string ManagerId { get; set; }
    }
}
