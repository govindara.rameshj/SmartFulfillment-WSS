using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLTOTS")]
    public class Dltots : IDailyTillTranKey, ISyncronizedToRti
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Dltots()
        {
            AuthorizerNumber = Global.NullUserCode;
            CashierNumber = String.Empty;
            CollegueNumber = "000000000";
            CustomerAccountCardHolderNumber = "000000";
            CustomerAccountNumber = "000000";
            CreateDate = DateTime.MinValue;
            Description = String.Empty;
            DiscountAmount = 0M;
            DiscountSupervisor = Global.NullUserCode;
            DocumentNumber = "00000000";
            EmployeeDiscountCard = String.Empty;
            ExVatValue1 = 0;
            ExVatValue2 = 0;
            ExVatValue3 = 0;
            ExVatValue4 = 0;
            ExVatValue5 = 0;
            ExVatValue6 = 0;
            ExVatValue7 = 0;
            ExVatValue8 = 0;
            ExVatValue9 = 0;
            GiftTokensPrinted = 0;
            IsAccountSale = false;
            IsAccountUpdateComplete = false;
            IsEmployeeDiscount = false;
            IsOnline = false;
            IsParked = false;
            IsRecalled = false;
            IsSaleOrRefund = false;
            IsSupervisorUsed = false;
            IsTraining = false;
            IsTransactionComplete = false;
            IsVoid = false;
            MerchandiseAmount = 0M;
            Misc = 0;
            NonMerchandiseAmount = 0;
            OpenDrawerReasonCode = 0;
            OrderNumber = Global.NullOrderNumber;
            ReceivedDate = new DateTime();
            RefundAuthorisationSupervisorNumber = Global.NullUserCode;
            RefundCashier = Global.NullUserCode;
            RefundManager = Global.NullUserCode;
            RtiFlag = RtiStatus.NotSet;
            StatusSequence = String.Empty;
            SaveStatus = "9b";
            //This field is needed only for technical needs in layers before our service in order to understand where to send request. 
            //So in db it can be null or empty.
            StoreNumber = String.Empty;
            TaxAmount = 0M;
            TenderOverrideCode = "00";
            TillNumber = String.Empty;
            TimeOfTransaction = String.Empty;
            TotalSaleAmount = 0M;
            TransactionNumber = String.Empty;
            TransactionCode = String.Empty;
            VatRate1 = 0;
            VatRate2 = 0;
            VatRate3 = 0;
            VatRate4 = 0;
            VatRate5 = 0;
            VatRate6 = 0;
            VatRate7 = 0;
            VatRate8 = 0;
            VatRate9 = 0;
            VatSymbol1 = "a";
            VatSymbol2 = "b";
            VatSymbol3 = "c";
            VatSymbol4 = "d";
            VatSymbol5 = "e";
            VatSymbol6 = "f";
            VatSymbol7 = "g";
            VatSymbol8 = "h";
            VatSymbol9 = "i";
            VatValue1 = 0; 
            VatValue2 = 0;
            VatValue3 = 0;
            VatValue4 = 0;
            VatValue5 = 0;
            VatValue6 = 0;
            VatValue7 = 0;
            VatValue8 = 0;
            VatValue9 = 0;
            VoidSupervisor = Global.NullUserCode;
            WasCbbupdUpdated = null;
            WasRsbupdUpdated = false;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        // In Json
        [MapField("CASH")]
        public string CashierNumber { get; set; }

        // Is got from Date1 Time part
        [MapField("TIME")]
        public string TimeOfTransaction { get; set; }

        // In Json
        [MapField("TCOD")]
        public string TransactionCode { get; set; }

        //In Json, but only in opendrawersale
        // In Json - ReasonCode field. Had to take the name from DB 'cause here would be two ReasonCode fields
        [MapField("OPEN")]
        public short OpenDrawerReasonCode { get; set; } // 0 by default for non-opendrawer trans

        //In Json, but only in M+ Misc. Income, C+ Misc. Income, M- Paid Out, C- Paid Out
        // In Json - ReasonCode field. Had to take the name from DB 'cause here would be two ReasonCode fields
        public short Misc { get; set; } // 0 by default in other trans

        // In Json
        [MapField("ORDN")]
        public string OrderNumber { get; set; }

        // In Json
        [MapField("VOID")]
        public bool IsVoid { get; set; }

        // In Json
        [MapField("TMOD")]
        public bool IsTraining{ get; set; }

        [MapField("PROC")]
        public bool WasRsbupdUpdated { get; set; } // post processing. Set 0 here

        // In Json
        [MapField("PARK")]
        public bool IsParked { get; set; }

        // In Json
        [MapField("SUPV")]
        public string AuthorizerNumber { get; set; }

        [MapField("SUSE")]
        public bool IsSupervisorUsed { get; set; } // SupervisorNumber != 000 ? 1 : 0 

        // In Json
        [MapField("VSUP")]
        public string VoidSupervisor { get; set; }

        // In Json
        [MapField("DSUP")]
        public string DiscountSupervisor { get; set; }

        // In Json
        [MapField("MERC")]
        public decimal MerchandiseAmount { get; set; }

        [MapField("NMER")]
        public decimal NonMerchandiseAmount { get; set; } // 0.00 for sale; About value for other types we'll know after Sergey's investigation

        // In Json
        [MapField("TAXA")]
        public decimal TaxAmount { get; set; }

        // In Json
        [MapField("DISC")]
        public decimal DiscountAmount { get; set; }

        // In Json, but under name TotalAmount. Can't rename, 'cause it can brake BO logic that uses this field
        [MapField("TOTL")]
        public decimal TotalSaleAmount { get; set; }

        #region VatRateX

        private readonly decimal[] vatRates = new decimal[10]; //We dont use element with index 0.

        public void SetVatRate(int code, decimal value)
        {
            vatRates[code] = value;
        }

        [MapField("VATR1")]
        public decimal VatRate1 // RETOPT.VATR1
        {
            get { return vatRates[1]; }
            set { vatRates[1] = value; }
        }

        [MapField("VATR2")]
        public decimal VatRate2 // RETOPT.VATR2
        {
            get { return vatRates[2]; }
            set { vatRates[2] = value; }
        }

        [MapField("VATR3")]
        public decimal VatRate3 // RETOPT.VATR3
        {
            get { return vatRates[3]; }
            set { vatRates[3] = value; }
        }

        [MapField("VATR4")]
        public decimal VatRate4 // RETOPT.VATR4
        {
            get { return vatRates[4]; }
            set { vatRates[4] = value; }
        }

        [MapField("VATR5")]
        public decimal VatRate5 // RETOPT.VATR5
        {
            get { return vatRates[5]; }
            set { vatRates[5] = value; }
        }

        [MapField("VATR6")]
        public decimal VatRate6 // RETOPT.VATR6
        {
            get { return vatRates[6]; }
            set { vatRates[6] = value; }
        }

        [MapField("VATR7")]
        public decimal VatRate7 // RETOPT.VATR7
        {
            get { return vatRates[7]; }
            set { vatRates[7] = value; }
        }

        [MapField("VATR8")]
        public decimal VatRate8 // RETOPT.VATR8
        {
            get { return vatRates[8]; }
            set { vatRates[8] = value; }
        }

        [MapField("VATR9")]
        public decimal VatRate9 // RETOPT.VATR9
        {
            get { return vatRates[9]; }
            set { vatRates[9] = value; }
        }

        #endregion

        [MapField("VSYM1")]
        public string VatSymbol1 { get; set; } // a

        [MapField("VSYM2")]
        public string VatSymbol2 { get; set; } // b

        [MapField("VSYM3")]
        public string VatSymbol3 { get; set; } // c

        [MapField("VSYM4")]
        public string VatSymbol4 { get; set; } // d

        [MapField("VSYM5")]
        public string VatSymbol5 { get; set; } // e

        [MapField("VSYM6")]
        public string VatSymbol6 { get; set; } // f

        [MapField("VSYM7")]
        public string VatSymbol7 { get; set; } // g

        [MapField("VSYM8")]
        public string VatSymbol8 { get; set; } // h

        [MapField("VSYM9")]
        public string VatSymbol9 { get; set; } // i

        #region ExVatValueX

        private readonly decimal[] exVatValues = new decimal[10]; //We dont use element with index 0.

        public void SetExVatValue(int code, decimal value)
        {
            exVatValues[code] = value;
        }

        [MapField("XVAT1")]
        public decimal ExVatValue1
        {
            get { return exVatValues[1]; }
            set { exVatValues[1] = value; }
        }

        [MapField("XVAT2")]
        public decimal ExVatValue2
        {
            get { return exVatValues[2]; }
            set { exVatValues[2] = value; }
        }

        [MapField("XVAT3")]
        public decimal ExVatValue3
        {
            get { return exVatValues[3]; }
            set { exVatValues[3] = value; }
        }

        [MapField("XVAT4")]
        public decimal ExVatValue4
        {
            get { return exVatValues[4]; }
            set { exVatValues[4] = value; }
        }

        [MapField("XVAT5")]
        public decimal ExVatValue5
        {
            get { return exVatValues[5]; }
            set { exVatValues[5] = value; }
        }

        [MapField("XVAT6")]
        public decimal ExVatValue6
        {
            get { return exVatValues[6]; }
            set { exVatValues[6] = value; }
        }

        [MapField("XVAT7")]
        public decimal ExVatValue7
        {
            get { return exVatValues[7]; }
            set { exVatValues[7] = value; }
        }

        [MapField("XVAT8")]
        public decimal ExVatValue8
        {
            get { return exVatValues[8]; }
            set { exVatValues[8] = value; }
        }

        [MapField("XVAT9")]
        public decimal ExVatValue9
        {
            get { return exVatValues[9]; }
            set { exVatValues[9] = value; }
        }

        #endregion

        #region VatValueX

        private readonly decimal[] vatValues = new decimal[10]; //We dont use element with index 0.

        public void SetVatValue(int code, decimal value)
        {
            vatValues[code] = value;
        }

        public decimal GetVatValue(int code)
        {
            return vatValues[code];
        }

        [MapField("VATV1")]
        public decimal VatValue1
        {
            get { return vatValues[1]; }
            set { vatValues[1] = value; }
        }

        [MapField("VATV2")]
        public decimal VatValue2
        {
            get { return vatValues[2]; }
            set { vatValues[2] = value; }
        }

        [MapField("VATV3")]
        public decimal VatValue3
        {
            get { return vatValues[3]; }
            set { vatValues[3] = value; }
        }

        [MapField("VATV4")]
        public decimal VatValue4
        {
            get { return vatValues[4]; }
            set { vatValues[4] = value; }
        }

        [MapField("VATV5")]
        public decimal VatValue5
        {
            get { return vatValues[5]; }
            set { vatValues[5] = value; }
        }

        [MapField("VATV6")]
        public decimal VatValue6
        {
            get { return vatValues[6]; }
            set { vatValues[6] = value; }
        }

        [MapField("VATV7")]
        public decimal VatValue7
        {
            get { return vatValues[7]; }
            set { vatValues[7] = value; }
        }

        [MapField("VATV8")]
        public decimal VatValue8
        {
            get { return vatValues[8]; }
            set { vatValues[8] = value; }
        }

        [MapField("VATV9")]
        public decimal VatValue9
        {
            get { return vatValues[9]; }
            set { vatValues[9] = value; }
        }

        #endregion

        [MapField("ICOM")]
        public bool IsTransactionComplete { get; set; } // 0 in the insert, 1 in the update

        [MapField("ACCN")]
        public string CustomerAccountNumber { get; set; } // 00 or 000000

        [MapField("CARD")]
        public string CustomerAccountCardHolderNumber { get; set; } // 00 or 000000

        [MapField("AUPD")]
        public bool IsAccountUpdateComplete { get; set; } // 0 for sale; About value for other types we'll know after Sergey's investigation

        [MapField("BACK")]
        public bool IsSaleOrRefund { get; set; } // 0 as default

        // In Json
        [MapField("IEMP")]
        public bool IsEmployeeDiscount { get; set; }

        // In Json, but only for refund
        [MapField("RCAS")]
        public string RefundCashier { get; set; } // 000 by default for non-refund sales

        [MapField("RSUP")]
        public string RefundAuthorisationSupervisorNumber { get; set; } // 000 by default for non-refund sales

        // In Json, but only for for refund
        [MapField("RMAN")]
        public string RefundManager { get; set; }  // 000 by default for non-refund sales

        [MapField("TOCD")]
        public string TenderOverrideCode { get; set; } // OBSOLETE so just write 00 here

        // In Json, but only for Sale
        [MapField("PKRC")]
        public bool IsRecalled { get; set; } // 0 by default for non-sale

        // In Json
        [MapField("REMO")]
        public bool IsOnline { get; set; }

        [MapField("GTPN")]
        public decimal GiftTokensPrinted { get; set; } // OBSOLETE, so just set 0

        [MapField("DESCR")]
        public string Description { get; set; } // User comments for OD, M+, M-, C+, C-; I think we can use null by default

        [MapField("ACCT")]
        public bool IsAccountSale { get; set; } // 0 by default

        // In Json
        [MapField("DOCN")]
        public string DocumentNumber { get; set; } // 000 default for OD OpenDrawerReasonCode Drawer, CO Sign On, CC Sign Off

        //In Json
        [MapField("STOR")]
        public string StoreNumber { get; set; }

        [MapField("CCRD")]
        public string CollegueNumber { get; set; } // 000000000 by default

        [MapField("SSTA")]
        public string SaveStatus { get; set; } // 99 for void and parked transactions. 9b - in other situations for sale transaction

        [MapField("SSEQ")]
        public string StatusSequence { get; set; } //empty

        //In Json, but only for sale
        [MapField("CARD_NO")]
        public string EmployeeDiscountCard { get; set; } // empty or digits (111000000027680712 ) for Refund, empty for others

        // NULL, empty or date for M+ Misc Income, M- Paid Out, C+ Misc Inc Correct and C- Paid Out Correct; empty or null for others.
        [MapField("CBBU")]
        public string WasCbbupdUpdated { get; set; } 

        [MapField("RTI")]
        public string RtiFlag { get; set; } // S for sale

        public DateTime ReceivedDate { get; set; } // NULL

        public Dltots Clone()
        {
            return Map.ObjectToObject<Dltots>(this);
        }

        public string SourceTranId { get; set; }
        public string SourceTranNumber { get; set; }
        public string Source { get; set; }
        public string ReceiptBarcode { get; set; }
    }
}
