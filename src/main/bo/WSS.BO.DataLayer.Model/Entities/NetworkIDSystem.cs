using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("SYSNID")]
    public class NetworkIdSystem
    {
        [MapField("DATE1")]
        public DateTime SystemDate { get; set; }
    }
}
