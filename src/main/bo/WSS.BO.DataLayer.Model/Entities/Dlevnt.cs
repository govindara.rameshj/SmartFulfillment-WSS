﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Model.Entities
{
    [TableName("DLEVNT")]
    public class Dlevnt : IDailyTillTranKey, ISyncronizedToRti
    {
        public Dlevnt()
        {
            CreateDate = DateTime.MinValue;
            TillNumber = String.Empty;
            TransactionNumber = String.Empty;
            TransactionLineNumber = 0;
            EventSequenceNumber = String.Empty;
            ErosionType = String.Empty;
            DiscountAmount = 0;
            RtiFlag = RtiStatus.NotSet;
            FillerForFutureUse = String.Empty;
        }

        [MapField("DATE1")]
        public DateTime CreateDate { get; set; }

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }

        [MapField("NUMB")]
        public short TransactionLineNumber { get; set; }

        [MapField("ESEQ")]
        public string EventSequenceNumber { get; set; }

        [MapField("TYPE")]
        public string ErosionType { get; set; }

        [MapField("AMNT")]
        public decimal DiscountAmount { get; set; }

        [MapField("RTI")]
        public string RtiFlag { get; set; }

        [MapField("SPARE")]
        public string FillerForFutureUse { get; set; }
    }
}
