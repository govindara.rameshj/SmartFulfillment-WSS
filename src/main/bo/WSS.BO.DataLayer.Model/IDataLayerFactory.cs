﻿using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Model
{
    public interface IDataLayerFactory : IRepositoriesFactory
    {
        IUnitOfWork CreateUnitOfWork();
    }
}
