﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseCollectionFulfillmentConverter : ReverseOrderFulfillmentConverter<CollectionFulfillment>
    {
        public ReverseCollectionFulfillmentConverter(OrderDataCollections orderData)
            : base(orderData)
        {
        }
        
        protected override void InnerRestoreFulfillmentHeader(CollectionFulfillment fulfillment)
        {
            base.InnerRestoreFulfillmentHeader(fulfillment);

            fulfillment.CollectionDate = OrderData.Corhdr.DeliveryOrCollectionDate;
        }
    }
}
