﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseFulfillmentConvertersFactory
    {
        public IReverseFulfillmentConverter CreateReverseOrderFulfillmentConverter(OrderDataCollections dataCollections)
        {
            if (dataCollections.Corhdr.IsForDelivery)
            {
                return new ReverseDeliveryFulfillmentConverter(dataCollections);
            }
            return new ReverseCollectionFulfillmentConverter(dataCollections);
        }

        public ReverseInstantFulfillmentConverter CreateReverseInstantFulfillmentConverter(
            OrderDataCollections dataCollections)
        {
            return new ReverseInstantFulfillmentConverter(dataCollections);
        }
    }
}
