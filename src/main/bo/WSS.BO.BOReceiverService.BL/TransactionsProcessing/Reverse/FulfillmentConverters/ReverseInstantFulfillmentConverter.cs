﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseInstantFulfillmentConverter : ReverseFulfillmentConverter<InstantFulfillment>
    {
        public ReverseInstantFulfillmentConverter(OrderDataCollections orderData) : base(orderData)
        {
        }
    }
}
