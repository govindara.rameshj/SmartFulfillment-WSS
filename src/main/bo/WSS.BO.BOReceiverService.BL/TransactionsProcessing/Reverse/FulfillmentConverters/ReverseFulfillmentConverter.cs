﻿using System.Collections.Generic;
using slf4net;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseFulfillmentConverter<T> : IReverseFulfillmentConverter where T: Fulfillment, new()
    {
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ReverseFulfillmentConverter<T>));
        protected OrderDataCollections OrderData;
        protected string ConverterName;

        protected ReverseFulfillmentConverter(OrderDataCollections orderData)
        {
            ConverterName = GetType().Name;
            OrderData = orderData;
        }
        
        public Fulfillment RestoreFulfillmentHeader()
        {
            T fulfillment = new T();
            Log.Info("{0}.RestoreFulfillment is called", ConverterName);
            InnerRestoreFulfillmentHeader(fulfillment);
            Log.Info("{0}.RestoreFulfillment is completed", ConverterName);
            return fulfillment;
        }

        protected virtual void InnerRestoreFulfillmentHeader(T fulfillment)
        {
            fulfillment.Items = new List<FulfillmentItem>();
        }
    }
}
