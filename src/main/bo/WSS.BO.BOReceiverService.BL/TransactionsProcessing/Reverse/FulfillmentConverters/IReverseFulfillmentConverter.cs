﻿using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public interface IReverseFulfillmentConverter
    {
        Fulfillment RestoreFulfillmentHeader();
    }
}
