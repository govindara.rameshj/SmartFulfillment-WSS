﻿using System;
using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseDeliveryFulfillmentConverter : ReverseOrderFulfillmentConverter<DeliveryFulfillment>
    {
        public ReverseDeliveryFulfillmentConverter(OrderDataCollections orderData)
            : base(orderData)
        {
        }
        
        protected override void InnerRestoreFulfillmentHeader(DeliveryFulfillment fulfillment)
        {
            base.InnerRestoreFulfillmentHeader(fulfillment);

            fulfillment.DeliveryDate = OrderData.Corhdr.DeliveryOrCollectionDate;
            fulfillment.DeliveryType = DeliveryType.NORMAL; //Not implemented as not needed
            fulfillment.FreeDeliveryAuthorizerNumber = null; //Not implemented as not needed

            fulfillment.DeliverySlot = new DeliverySlot 
            {
                Description = OrderData.Corhdr.RequiredDeliverySlotDescription,
                Id = OrderData.Corhdr.RequiredDeliverySlotID,
                EndTime = OrderData.Corhdr.RequiredDeliverySlotEndTime ?? default(TimeSpan),
                StartTime = OrderData.Corhdr.RequiredDeliverySlotStartTime ?? default(TimeSpan)
            };

            fulfillment.DeliveryInstructions = new List<string>();
            if (OrderData.Cortxts.Count != 0)
            {
                foreach (var cortxt in OrderData.Cortxts)
                {
                    fulfillment.DeliveryInstructions.Add(cortxt.Text);
                }
            }
        }
    }
}
