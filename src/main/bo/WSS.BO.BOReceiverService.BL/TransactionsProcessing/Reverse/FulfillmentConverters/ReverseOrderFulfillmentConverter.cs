using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters
{
    public class ReverseOrderFulfillmentConverter<T> : ReverseFulfillmentConverter<T> where T:OrderFulfillment, new()
    {
        protected ReverseOrderFulfillmentConverter(OrderDataCollections orderData) : base(orderData)
        {
        }

        protected override void InnerRestoreFulfillmentHeader(T fulfillment)
        {
            base.InnerRestoreFulfillmentHeader(fulfillment);

            if (OrderData.WasMadeInOvcTill())
            {
                fulfillment.Number = OrderData.Corhdr5.SourceOrderNumber.Trim();
            }
            else
            {
                // for legacy orders take OMOrderNumber
                int omOrderNumber = OrderData.Corhdr4.OMOrderNumber;
                fulfillment.Number = omOrderNumber == 0 ? null : omOrderNumber.ToString();
            }
        }
    }
}
