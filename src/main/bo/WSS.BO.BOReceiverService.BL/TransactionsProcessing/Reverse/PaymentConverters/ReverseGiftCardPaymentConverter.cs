﻿using System.Linq;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReverseGiftCardPaymentConverter : ReversePaymentConverter<GiftCardPayment>
    {
        public ReverseGiftCardPaymentConverter(Dlpaid dlpaid, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) 
            : base(dlpaid, dataLayerFactory, key)
        {
        }

        protected override void InnerFillPayment(GiftCardPayment payment)
        {
            base.InnerFillPayment(payment);

            payment.CardAcceptType = ModelMapper.GetCardAcceptType(dlpaid.AuthType);
            payment.CardDescription = dlpaid.CardDescription.Trim();

            var giftCard = dataLayerFactory.Create<DailyTillRepository>().SelectDailyGiftCard(key);

            if (giftCard != null)
            {
                payment.GiftCardOperation = new GiftCardOperation();
                payment.GiftCardOperation.AuthCode = giftCard.AuthCode.Trim();
                payment.GiftCardOperation.CardNumber = giftCard.CardNumber.Trim();
            }
        }
    }
}
