﻿using System;
using slf4net;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReversePaymentConverter<T> : IReversePaymentConverter where T : Payment, new()
    {
        protected Dlpaid dlpaid;
        protected readonly IDataLayerFactory dataLayerFactory;
        protected readonly IDailyTillTranKey key;
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ReversePaymentConverter<T>));
        protected string ConverterName;

        protected ReversePaymentConverter(Dlpaid dlpaid, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key)
        {
            this.dlpaid = dlpaid;
            this.dataLayerFactory = dataLayerFactory;
            this.key = key;
            ConverterName = GetType().Name;
        }

        public Payment RestorePayment()
        {
            T payment = new T();
            Log.Info("{0}.RestorePayment is called", ConverterName);
            InnerFillPayment(payment);
            Log.Info("{0}.RestorePayment is completed", ConverterName);
            return payment;
        }

        protected virtual void InnerFillPayment(T payment)
        {
            payment.AbsAmount = Math.Abs(dlpaid.Amount);
            payment.Direction = dlpaid.Amount < 0 ? Direction.FROM_CUSTOMER : Direction.TO_CUSTOMER;
        }
    }
}
