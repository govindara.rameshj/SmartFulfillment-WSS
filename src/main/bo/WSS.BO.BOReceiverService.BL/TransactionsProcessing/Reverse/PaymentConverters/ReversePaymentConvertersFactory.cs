﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReversePaymentConvertersFactory
    {
        public IReversePaymentConverter CreateReversePaymentConverter(Dlpaid dlpaid, IDataLayerFactory factory, IDailyTillTranKey key)
        {
            if (dlpaid.TenderType == TenderType.Cash)
            {
                return new ReverseCashPaymentConverter(dlpaid, factory, key);
            }
            if (dlpaid.TenderType == TenderType.Cheque)
            {
                return new ReverseChequePaymentConverter(dlpaid, factory, key);
            }
            if (dlpaid.TenderType == TenderType.AmericanExpress || dlpaid.TenderType == TenderType.CreditCard)
            {
                return new ReverseEftPaymentConverter(dlpaid, factory, key);
            }
            if (dlpaid.TenderType == TenderType.Change)
            {
                return new ReverseChangePaymentConverter(dlpaid, factory, key);
            }
            if (dlpaid.TenderType == TenderType.GiftCard)
            {
                return new ReverseGiftCardPaymentConverter(dlpaid, factory, key);
            }

            throw new ArgumentException(String.Format("Unknown tender type found in database : {0}", dlpaid.TenderType));
        }
    }
}
