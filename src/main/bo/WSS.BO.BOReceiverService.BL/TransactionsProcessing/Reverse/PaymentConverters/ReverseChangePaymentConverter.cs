﻿using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReverseChangePaymentConverter : ReversePaymentConverter<ChangePayment>
    {
        public ReverseChangePaymentConverter(Dlpaid dlpaid, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) 
            : base(dlpaid, dataLayerFactory, key)
        {
        }
    }
}
