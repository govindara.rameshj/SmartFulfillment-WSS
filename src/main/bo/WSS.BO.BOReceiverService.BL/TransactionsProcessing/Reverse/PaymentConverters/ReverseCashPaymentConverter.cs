﻿using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReverseCashPaymentConverter : ReversePaymentConverter<CashPayment>
    {
        public ReverseCashPaymentConverter(Dlpaid dlpaid, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) 
            : base(dlpaid, dataLayerFactory, key)
        {
        }
    }
}
