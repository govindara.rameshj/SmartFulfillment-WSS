using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public class ReverseEftPaymentConverter : ReversePaymentConverter<EftPayment>
    {
        public ReverseEftPaymentConverter(Dlpaid dlpaid, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key)
            : base(dlpaid, dataLayerFactory, key)
        {
        }

        protected override void InnerFillPayment(EftPayment payment)
        {
            base.InnerFillPayment(payment);

            payment.AuthCode = dlpaid.AuthCode.Trim();
            payment.AuthDescription = dlpaid.AuthDescription.Trim();
            payment.CardAcceptType = ModelMapper.GetCardAcceptType(dlpaid.AuthType);
            payment.MerchantNumber = dlpaid.MerchantNumber.Trim();
            payment.TransactionUid = dlpaid.TransactionUid.Trim();

            var dlcomm = dataLayerFactory.Create<DailyTillRepository>().SelectDlcomm(key, dlpaid.PaymentIndex);
            payment.TokenId = dlcomm.TokenId.Trim();

            payment.PaymentCard = RestorePaymentCard(dlcomm);
        }

        private PaymentCard RestorePaymentCard(Dlcomm dlcomm)
        {
            var paymentCard = new PaymentCard();
            paymentCard.CardDescription = dlpaid.CardDescription.Trim();
            paymentCard.CardExpireDate = ConversionUtils.OasysStringToMonth(dlpaid.CardExpireDate);
            if (dlpaid.CardStartDate == null || dlpaid.CardStartDate == Global.NullPaymentCardDate)
            {
                paymentCard.CardStartDate = null;
            }
            else
            {
                paymentCard.CardStartDate = ConversionUtils.OasysStringToMonth(dlpaid.CardStartDate);
            }
            paymentCard.CardType = ModelMapper.GetCardTypeByTenderType(dlpaid.TenderType, dlpaid.CardDescription);
            paymentCard.IssueNumberSwitch = dlpaid.IssueNumberSwitch.Trim();
            paymentCard.MaskedCardNumber = dlpaid.CardNumber.Trim();
            paymentCard.PanHash = dlcomm.PanHash.Trim();
            return paymentCard;
        }
    }
}
