﻿using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters
{
    public interface IReversePaymentConverter
    {
        Payment RestorePayment();
    }
}
