﻿using slf4net;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public class ReverseProductConverter<T> : IReverseProductConverter where T:Product, new()
    {
        protected DailyTillLine dlline;
        protected readonly IDataLayerFactory dataLayerFactory;
        protected readonly IDailyTillTranKey key;
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ReverseProductConverter<T>));
        protected string ConverterName;

        protected ReverseProductConverter(DailyTillLine dlline, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key)
        {
            this.dlline = dlline;
            this.dataLayerFactory = dataLayerFactory;
            this.key = key;
            ConverterName = GetType().Name;
        }
        
        public Product RestoreProduct()
        {
            T product = new T();
            Log.Info("{0}.RestoreProduct is called", ConverterName);
            InnerRestoreProduct(product);
            Log.Info("{0}.RestoreProduct is completed", ConverterName);
            return product;
        }

        protected virtual void InnerRestoreProduct(T product)
        {
        }
    }
}
