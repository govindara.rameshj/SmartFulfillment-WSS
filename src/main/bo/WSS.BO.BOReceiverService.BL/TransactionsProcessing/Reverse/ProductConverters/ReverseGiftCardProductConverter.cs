﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public class ReverseGiftCardProductConverter : ReverseProductConverter<GiftCardProduct>
    {
        public ReverseGiftCardProductConverter(DailyTillLine dlline, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) 
            : base(dlline, dataLayerFactory, key)
        {
        }

        protected override void InnerRestoreProduct( GiftCardProduct product)
        {
            base.InnerRestoreProduct(product);

            product.AbsAmount = Math.Abs(dlline.InitialPrice);

            var giftCard = dataLayerFactory.Create<DailyTillRepository>().SelectDailyGiftCard(key);

            if (giftCard != null)
            {
                product.GiftCardOperation = new GiftCardOperation();
                product.GiftCardOperation.AuthCode = giftCard.AuthCode.Trim();
                product.GiftCardOperation.CardNumber = giftCard.CardNumber.Trim();
            }
        }
    }
}
