﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public class ReverseProductConvertersFactory
    {
        public IReverseProductConverter CreateReverseProductConverter(DailyTillLine dlline, IReadOnlyDataRetriever readOnlyDataRetriever, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key)
        {
            if (dlline.SkuNumber == readOnlyDataRetriever.GetGiftCardSkuFromParameters())
            {
                return new ReverseGiftCardProductConverter(dlline, dataLayerFactory, key);
            }
            if (dlline.SkuNumber == readOnlyDataRetriever.GetDeliveryChargeSkuFromParameters())
            {
                return new ReverseDeliveryProductConverter(dlline, dataLayerFactory, key);
            }

            return new ReverseSkuProductConverter(dlline, dataLayerFactory, key);
        }
    }
}
