﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public interface IReverseProductConverter
    {
        Product RestoreProduct();
    }
}
