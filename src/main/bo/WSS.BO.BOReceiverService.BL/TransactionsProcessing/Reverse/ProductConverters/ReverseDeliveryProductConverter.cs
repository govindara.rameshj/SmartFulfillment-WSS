﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public class ReverseDeliveryProductConverter : ReverseProductConverter<DeliveryProduct>
    {
        public ReverseDeliveryProductConverter(DailyTillLine dlline, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) 
            : base(dlline, dataLayerFactory, key)
        {
        }

        protected override void InnerRestoreProduct(DeliveryProduct product)
        {
            base.InnerRestoreProduct(product);

            product.AbsAmount = dlline.Amount;
        }
    }
}
