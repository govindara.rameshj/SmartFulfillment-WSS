﻿using System;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Enums;
using EventType = WSS.BO.Model.Enums.EventType;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters
{
    public class ReverseSkuProductConverter : ReverseProductConverter<SkuProduct>
    {
        public ReverseSkuProductConverter(DailyTillLine dlline, IDataLayerFactory dataLayerFactory, IDailyTillTranKey key) : base(dlline, dataLayerFactory, key)
        {
        }

        protected override void InnerRestoreProduct(SkuProduct product)
        {
            base.InnerRestoreProduct(product);
            var repo = dataLayerFactory.Create<DailyTillRepository>();
            product.GroupProductId = null;
            product.BrandProductId = dlline.SkuNumber.Trim();
            product.ProductInputType = dlline.IsScanned ? ProductInputType.SCANNED : ProductInputType.KEYED;
            product.AbsQuantity = Convert.ToInt32(Math.Abs(dlline.Quantity));
            product.InitialPrice = dlline.InitialPrice;

            RestoreDiscounts(dlline, product);

            var dltots = repo.SelectDailyTillTran(key);

            if (dltots.IsEmployeeDiscount)
            {
                product.Discounts.Add(
                    new EmployeeDiscount
                    {
                        SavingAmount = dlline.EmployeeDiscountAmount * dlline.Quantity,
                        CardNumber = dltots.EmployeeDiscountCard.Trim(),
                        SupervisorNumber = dltots.DiscountSupervisor == Global.NullUserCode ? null : dltots.DiscountSupervisor.Trim()
                    });
            }            
        }

        private void RestoreDiscounts(DailyTillLine dlline, SkuProduct product)
        {
            RestorePriceOverride(dlline, product);
            RestoreEvents(product);
        }

        private void RestorePriceOverride(DailyTillLine dlline, SkuProduct product)
        {
            if (dlline.PriceOverrideCode != Convert.ToInt16(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.NoPriceOverride))
            {
                if (product.Discounts == null)
                {
                    product.Discounts = new List<Discount>();
                }

                if (dlline.PriceOverrideCode == Convert.ToInt16(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.PriceMatchOrPromise))
                {
                    var repo = dataLayerFactory.Create<DailyTillRepository>();

                    var dlolin = repo.SelectDlolin(key);

                    if (dlolin != null)
                    {
                        product.Discounts.Add(
                            new PriceMatchDiscount
                            {
                                SavingAmount = dlline.PriceOverrideChangePriceDifference * dlline.Quantity,
                                SupervisorNumber = dlline.SupervisorNumber == Global.NullUserCode ? null : dlline.SupervisorNumber.Trim(),
                                CompetitorName = dlolin.Name.Trim(),
                                NewPrice = dlolin.CompetitorsPriceEntered,
                                IsVATIncluded = dlolin.EnteredPricesAreVatInclusive
                            });
                    }
                }
                else
                {
                    product.Discounts.Add(
                            new PriceOverrideDiscount
                            {
                                SavingAmount = Math.Abs(dlline.PriceOverrideChangePriceDifference * dlline.Quantity),
                                SupervisorNumber = dlline.SupervisorNumber == Global.NullUserCode ? null : dlline.SupervisorNumber.Trim(),
                                PriceOverrideCode = ModelMapper.GetPriceOverrideReasonCode(dlline.PriceOverrideCode.ToString("D2")),
                                Description = dataLayerFactory.Create<DictionariesRepository>()
                                    .SelectSystemCodes(SystemCodeType.PriceOverrideReason)[dlline.PriceOverrideCode.ToString("D2")].Description.Trim()
                            });
                }
            }
        }

        private void RestoreEvents(SkuProduct product)
        {
            if (product.Discounts == null)
            {
                product.Discounts = new List<Discount>();
            }

            var repo = dataLayerFactory.Create<DailyTillRepository>();
            var dlevents = repo.SelectDailyEventByDailyLinesSequenceNumber(key, dlline.SequenceNumber);
            if (dlevents.Count() != 0)
            {
                foreach (var dlevent in dlevents)
                {
                    var eventType = ModelMapper.GetEventTypeByCode(dlevent.ErosionType);

                    EventDiscount eventDiscount;

                    if (product.Discounts.OfType<EventDiscount>().Any(x => x.EventDescriptor.EventType == eventType))
                    {
                        eventDiscount = product.Discounts.OfType<EventDiscount>().First(x => x.EventDescriptor.EventType == eventType);
                        
                    }
                    else
                    {
                        eventDiscount = new EventDiscount
                        {
                            HitAmounts = new List<decimal>(),
                            EventDescriptor = new EventDescriptor
                            {
                                EventType = eventType,
                                EventCode = GetEventCodeFromDlline(eventType)
                            }
                        };

                        product.Discounts.Add(eventDiscount);
                    }

                    eventDiscount.HitAmounts.Add(dlevent.DiscountAmount);
                }
            }
        }

        private string GetEventCodeFromDlline(EventType eventType)
        {
            switch (eventType)
            {
                case EventType.DEAL_GROUP:
                    return dlline.DealGroupMarginErosionCode;
                case EventType.HIERARCHY_SPEND:
                    return dlline.HiearchyMarginErosionCode;
                case EventType.MULTIBUY:
                    return dlline.MultibuyMarginErosionCode;
                case EventType.QUANTITY_BREAK:
                    return dlline.QuantityBreakMarginErosionCode;
                default:
                    throw new ArgumentException(String.Format("Unknown EventType [{0}]", eventType));
            }
        }
    }
}
