using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.FulfillmentConverters;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.PaymentConverters;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse.ProductConverters;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse
{
    public class RetailTransactionReverseConverter
    {
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly IReadOnlyDataRetriever readOnlyDataRetriever;
        private VatRates vatRates;

        public RetailTransactionReverseConverter(IDataLayerFactory dataLayerFactory, IReadOnlyDataRetriever readOnlyDataRetriever)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.readOnlyDataRetriever = readOnlyDataRetriever;
        }

        public RetailTransaction RecoverRetailTransaction(IDailyTillTranKey dailyTillTranKey)
        {
            var retailTransaction = new RetailTransaction();
            RecoverDltots(retailTransaction, dailyTillTranKey);
            RecoverLines(retailTransaction, dailyTillTranKey);
            RecoverTenders(retailTransaction, dailyTillTranKey);
            RecoverContactFromDlrcus(retailTransaction, dailyTillTranKey);
            RecoverCoupons(retailTransaction, dailyTillTranKey);
            if (IsOriginalTransactionAnOrder(dailyTillTranKey))
            {
                var dataCollections = new OrderDataCollections(dataLayerFactory);
                RecoverFulfillments(retailTransaction, dailyTillTranKey, dataCollections);
                RecoverContactFromCorhdr(retailTransaction, dataCollections);
            }
            RecoverCancellation(retailTransaction, dailyTillTranKey);

            RecoverRelatedTransactions(retailTransaction, dailyTillTranKey);
            return retailTransaction;
        }

        private void RecoverCoupons(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var repo = dataLayerFactory.Create<DailyTillRepository>();
            var dlcoupons = repo.SelectDlcoupons(dailyTillTranKey);
            if (dlcoupons != null)
            {
                retailTransaction.Coupons = new List<Coupon>();
                foreach (var dlcoupon in dlcoupons)
                {
                    var couponRepo = dataLayerFactory.Create<CouponRepository>();
                    var eventMaster = couponRepo.GetEventByCouponNumber(dlcoupon.CouponId);
                    if (eventMaster == null)
                    {
                        throw new ArgumentException(String.Format("There is no event master row in EVTMAS table for coupon with number = '{0}'", dlcoupon.CouponId));
                    }
                    Coupon coupon = new Coupon
                    {
                        EventDescriptor = new EventDescriptor
                        {
                            EventType = ModelMapper.GetEventTypeByCode(eventMaster.EventType),
                            EventCode = eventMaster.EventNumber.Trim()
                        },
                        CouponBarcode = GetTrimmedStringFieldValue(dlcoupon.CouponId)
                    };
                    retailTransaction.Coupons.Add(coupon);
                }
            }
        }

        private void RecoverDltots(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var repo = dataLayerFactory.Create<DailyTillRepository>();

            var dltots = repo.SelectDailyTillTran(dailyTillTranKey);
            if (dltots != null && !(dltots.IsParked || dltots.IsVoid || dltots.IsTraining))
            {
                RecoverTransactionSourceFields(retailTransaction, dltots);

                retailTransaction.StoreNumber = readOnlyDataRetriever.GetAbsoluteStoreId().ToString();
                retailTransaction.CreateDateTime = dltots.CreateDate + ConversionUtils.OasysStringToTime(dltots.TimeOfTransaction);
                retailTransaction.TillNumber = dltots.TillNumber.TrimStart('0');
                retailTransaction.TransactionNumber = GetTrimmedStringFieldValue(dltots.SourceTranNumber);
                retailTransaction.CashierNumber = dltots.CashierNumber.TrimStart('0');
                retailTransaction.IsTraining = dltots.IsTraining;
                retailTransaction.BrandCode = Global.BrandCode;

                //FIELDS FOR VOIDABLE TRANSACTION
                retailTransaction.AuthorizerNumber = dltots.AuthorizerNumber == Global.NullUserCode ? null : dltots.AuthorizerNumber.Trim();  
                retailTransaction.VoidSupervisor = dltots.VoidSupervisor == Global.NullUserCode ? null : dltots.VoidSupervisor.Trim();

                RecoverTransactionStatus(retailTransaction, dltots);

                //FIELDS FOR PAYABLE TRANSACTION
                retailTransaction.TotalAmount = dltots.TotalSaleAmount;

                //FIELDS FOR RETAIL TRANSACTION
                //retailTransaction.Cancellation = 

                //Not implemented as not needed: If transaction status = not completed, then we always have 0 tax amount in dltots, so we cant recover this value...
                retailTransaction.TaxAmount = dltots.TaxAmount;

                //Not implemented as not needed: Here is some difficult logic, let it be empty for now
                retailTransaction.RecalledTransactionId = String.Empty; 
                retailTransaction.Manager = dltots.RefundManager == Global.NullUserCode ? null : dltots.RefundManager.Trim();
                retailTransaction.RefundCashier = dltots.RefundCashier == Global.NullUserCode ? null : dltots.RefundCashier.Trim();
                vatRates = new VatRates(dltots);
            }
            else
            {

                throw new TransactionNotFoundException();
            }
        }

        private void RecoverTransactionSourceFields(RetailTransaction retailTransaction, Dltots dltots)
        {
            if (CheckIsStringValueOfLegacyTransaction(dltots.Source) || CheckIsStringValueOfLegacyTransaction(dltots.SourceTranId))
            {
                RecoverLegacyTransactionSourceFields(retailTransaction, dltots);
            }
            else
            {
                RecoverOvcTransactionSourceFields(retailTransaction, dltots);
            }
        }

        private void RecoverLegacyTransactionSourceFields(RetailTransaction retailTransaction, Dltots dltots)
        {
            retailTransaction.Source = Global.LegacySource;
            retailTransaction.SourceTransactionId =
                LegacyReceiptBarcodeHelper.GenerateLegacyTranId(readOnlyDataRetriever.GetStoreId(),
                    dltots.TransactionNumber, dltots.CreateDate, dltots.TillNumber);
            retailTransaction.ReceiptBarcode =
                LegacyReceiptBarcodeHelper.GenerateReceiptBarcode(readOnlyDataRetriever.GetStoreId(),
                    dltots.TransactionNumber, dltots.CreateDate, dltots.TillNumber);
        }

        private void RecoverOvcTransactionSourceFields(RetailTransaction retailTransaction, Dltots dltots)
        {
            retailTransaction.Source = dltots.Source.Trim();
            retailTransaction.SourceTransactionId = dltots.SourceTranId.Trim();
            retailTransaction.ReceiptBarcode = dltots.ReceiptBarcode.Trim();
        }

        private void RecoverTransactionStatus(RetailTransaction retailTransaction, Dltots dltots)
        {
            if (dltots.IsVoid)
            {
                retailTransaction.TransactionStatus = TransactionStatus.VOIDED;
            }
            else if (dltots.IsParked)
            {
                retailTransaction.TransactionStatus = TransactionStatus.PARKED;
            }
            else
            {
                retailTransaction.TransactionStatus = TransactionStatus.COMPLETED;
            }
        }

        private void RecoverLines(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var repo = dataLayerFactory.Create<DailyTillRepository>();

            var dllines = repo.SelectDailyTillLines(dailyTillTranKey);

            retailTransaction.Items = new List<RetailTransactionItem>();
            foreach (var dlline in dllines)
            {
                if (!dlline.IsReversed)
                {
                    var transactionItem = new RetailTransactionItem();
                    transactionItem.IsReversed = dlline.IsReversed;
                    transactionItem.ItemIndex = dlline.SequenceNumber;
                    transactionItem.Direction = dlline.Quantity < 0 ? Direction.FROM_CUSTOMER : Direction.TO_CUSTOMER;
                    transactionItem.ItemRefund = null;
                    transactionItem.SourceItemId = dlline.SequenceNumber.ToString(); // there was dlline.SourceItemId before, but as legacy transactions have it = NULL, it was illogical to use it
                    transactionItem.VatRate = vatRates.GetRateBySymbol(dlline.VatRate[0]);
                    transactionItem.AbsVatAmount = dlline.VatValue;

                    var factory = new ReverseProductConvertersFactory();
                    var reverseConverter = factory.CreateReverseProductConverter(dlline, readOnlyDataRetriever, dataLayerFactory, dailyTillTranKey);
                    var product = reverseConverter.RestoreProduct();
                    transactionItem.Product = product;

                    //if we have some fake lines in dlrcus for restored item - then we are restoring refund transaction,
                    //and we should restore itemrefund for transaction item.
                    var refundInfoFromDlrcus = repo.SelectDailyCustomer(dailyTillTranKey, dlline.SequenceNumber);
                    if (refundInfoFromDlrcus != null)
                    {
                        transactionItem.ItemRefund = RecoverItemRefund(refundInfoFromDlrcus, repo);
                    }

                    retailTransaction.Items.Add(transactionItem);
                }
            }
        }

        private ItemRefund RecoverItemRefund(DailyCustomer refundInfoFromDlrcus, DailyTillRepository repo)
        {
            var itemRefund = new ItemRefund();
            itemRefund.RefundReasonCode = ModelMapper.GetRefundReasonCodeByDbModelCode(refundInfoFromDlrcus.RefundReasonCode.Trim());
            itemRefund.Description = dataLayerFactory.Create<DictionariesRepository>().SelectSystemCodes(SystemCodeType.RefundReason)[refundInfoFromDlrcus.RefundReasonCode.Trim()].Description.Trim();

            if (refundInfoFromDlrcus.OriginalTransactionNumber.Trim() != Global.NullOriginalTransactionNumber)
            {
                var originalTransaction = repo.SelectDailyTotal(refundInfoFromDlrcus.OriginalTransactionNumber,
                    refundInfoFromDlrcus.OriginalTillNumber,
                    refundInfoFromDlrcus.OriginalSaleDate);

                var refundDailyTillTranKey = new DailyTillTranKey
                {
                    TransactionNumber = refundInfoFromDlrcus.TransactionNumber,
                    TillNumber = refundInfoFromDlrcus.TillNumber,
                    CreateDate = refundInfoFromDlrcus.CreateDate
                };

                string refundedTransactionId;
                //if we didnt find referenced transaction, then we set refundedTransactionId = null and refundedTransactionItemId = null so it will be processed as refund without receipt.
                if (originalTransaction == null)
                {
                    refundedTransactionId = null;
                }
                else
                {
                    //If we found transaction, but it has empty SourceTranId, then its legacy transaction, and we should generate complex Legacy-SSSTTNNNNDDMMYY id for it
                    //Also legacy transactions has no SourceItemId for DLLINE, so we mark it as stringed SequenceNumber of DailyCustomer for ItemRefund
                    if (CheckIsStringValueOfLegacyTransaction(originalTransaction.SourceTranId))
                    {
                        refundedTransactionId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(
                            readOnlyDataRetriever.GetStoreId(), originalTransaction.TransactionNumber, originalTransaction.CreateDate, originalTransaction.TillNumber);
                    }
                    //If we found transaction, and it has normal SourceTranId, then we should use it.
                    else
                    {
                        refundedTransactionId = originalTransaction.SourceTranId.Trim();
                    }
                }

                itemRefund.RefundedTransactionId = refundedTransactionId;
                itemRefund.RefundedTransactionItemId = refundInfoFromDlrcus.OriginalLineNumber != null && refundInfoFromDlrcus.OriginalLineNumber != 0 ?
                    refundInfoFromDlrcus.OriginalLineNumber.ToString() : GetRefundTransactionItemId(refundInfoFromDlrcus, refundDailyTillTranKey, repo);                
            }
            else
            {
                itemRefund.RefundedTransactionId = null;
                itemRefund.RefundedTransactionItemId = null;
            }

            return itemRefund;
        }

        private void RecoverTenders(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var repo = dataLayerFactory.Create<DailyTillRepository>();

            var dlpaids = repo.SelectDlpaids(dailyTillTranKey);

            retailTransaction.Payments = new List<Payment>();
            foreach (var dlpaid in dlpaids)
            {
                var factory = new ReversePaymentConvertersFactory();
                var reverseConverter = factory.CreateReversePaymentConverter(dlpaid, dataLayerFactory, dailyTillTranKey);
                var payment = reverseConverter.RestorePayment();
                retailTransaction.Payments.Add(payment);
            }
        }

        private bool IsOriginalTransactionAnOrder(IDailyTillTranKey dailyTillTranKey)
        {
            var orderNumber = dataLayerFactory.Create<DailyTillRepository>().SelectFirstDltotsOrderNumber(dailyTillTranKey);

            return orderNumber != Global.NullOrderNumber && !IsTransactionCancellation(dailyTillTranKey, orderNumber);            
        }

        private void RecoverFulfillments(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey, OrderDataCollections dataCollections)
        {
            retailTransaction.Fulfillments = new List<Fulfillment>();
            var orderNumber = dataLayerFactory.Create<DailyTillRepository>().SelectFirstDltotsOrderNumber(dailyTillTranKey);
            dataCollections.RecoverCollectionsFromDatabase(orderNumber);

            var factory = new ReverseFulfillmentConvertersFactory(); //Here we restore and initialize collection or delivery fulfillment
            var converter = factory.CreateReverseOrderFulfillmentConverter(dataCollections);
            var orderFulfillment = converter.RestoreFulfillmentHeader();
            retailTransaction.Fulfillments.Add(orderFulfillment);

            if (dataCollections.Corhdr.TotalUnitsTakenNumber != 0 || dataCollections.Corlins.Any(x => x.QuantityTaken != 0)) //If any product was taken, we should create instant fulfillment
            {
                converter = factory.CreateReverseInstantFulfillmentConverter(dataCollections);
                var instantFulfillment = converter.RestoreFulfillmentHeader();
                retailTransaction.Fulfillments.Add(instantFulfillment);
            }

            RecoverFulfillmentItems(retailTransaction, dataCollections, dailyTillTranKey);
        }

        private void RecoverFulfillmentItems(RetailTransaction retailTransaction, OrderDataCollections dataCollections, IDailyTillTranKey dailyTillTranKey)
        {
            foreach (var corlin in dataCollections.Corlins)
            {
                if (corlin.QuantityTaken != 0)
                {
                    var instantFulfillmentItem = new FulfillmentItem();

                    var repo = dataLayerFactory.Create<DailyTillRepository>();

                    instantFulfillmentItem.AbsQuantity = Convert.ToInt32(corlin.QuantityTaken);
                    instantFulfillmentItem.FulfillerNumber = corlin.RequiredFulfiller != null ? corlin.RequiredFulfiller.ToString() : null;
                    instantFulfillmentItem.TransactionItemIndex = Convert.ToInt32(GetOriginalItemIndex(dailyTillTranKey, repo, corlin.SkuNumber));
                    retailTransaction.Fulfillments.First(x => x is InstantFulfillment).Items.Add(instantFulfillmentItem);
                }

                if (corlin.QtyToBeDelivered != 0)
                {
                    var orderFulfillmentItem = new FulfillmentItem();

                    var repo = dataLayerFactory.Create<DailyTillRepository>();

                    orderFulfillmentItem.AbsQuantity = corlin.QtyToBeDelivered;
                    orderFulfillmentItem.FulfillerNumber = corlin.RequiredFulfiller != null ? corlin.RequiredFulfiller.ToString() : null;
                    orderFulfillmentItem.TransactionItemIndex = Convert.ToInt32(GetOriginalItemIndex(dailyTillTranKey, repo, corlin.SkuNumber));
                    retailTransaction.Fulfillments.First(x => x is OrderFulfillment).Items.Add(orderFulfillmentItem);
                }
            }
        }

        private void RecoverCancellation(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var orderNumber = dataLayerFactory.Create<DailyTillRepository>().SelectFirstDltotsOrderNumber(dailyTillTranKey);

            var dataCollections = new OrderDataCollections(dataLayerFactory);
            dataCollections.RecoverCollectionsFromDatabase(orderNumber);

            if (IsTransactionCancellation(dailyTillTranKey, orderNumber) && dataCollections.Correfunds.Any())
            {
                retailTransaction.Cancellation = new Fulfillment();
                retailTransaction.Cancellation.Items = new List<FulfillmentItem>();

                foreach (var correfund in dataCollections.Correfunds.Where(x => x.QtyCancelled > 0))
                {
                    var requiredFulfiller = dataCollections.Corlins.First(x => x.LineNumber == correfund.LineNumber).RequiredFulfiller;

                    retailTransaction.Cancellation.Items.Add(
                        new FulfillmentItem
                        {
                            TransactionItemIndex = int.Parse(correfund.LineNumber),
                            AbsQuantity = correfund.QtyCancelled,
                            FulfillerNumber = requiredFulfiller != null ? requiredFulfiller.ToString() : null
                        });
                }
            }
        }

        private void RecoverContactFromDlrcus(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            //If we have any record for order in dlrcus where customer name is not null = we restore contact info
            var dlrcus = dataLayerFactory.Create<DailyTillRepository>().SelectDailyCustomerWithFulfilledCustomerName(dailyTillTranKey);
            if (dlrcus != null)
            {
                var contact = new ContactInfo
                {
                    ContactAddress = new Address
                    {
                        AddressLine1 = GetTrimmedStringFieldValue(dlrcus.AddressLine1),
                        AddressLine2 = GetTrimmedStringFieldValue(dlrcus.AddressLine2),
                        PostCode = GetTrimmedStringFieldValue(dlrcus.Postcode),
                        Town = GetTrimmedStringFieldValue(dlrcus.Town)
                    },
                    Email = GetTrimmedStringFieldValue(dlrcus.Email),
                    MobilePhone = GetTrimmedStringFieldValue(dlrcus.MobilePhone),
                    Name = GetTrimmedStringFieldValue(dlrcus.CustomerName),
                    Phone = GetTrimmedStringFieldValue(dlrcus.PhoneNumber),
                    WorkPhone = GetTrimmedStringFieldValue(dlrcus.WorkPhone)
                };

                retailTransaction.AddContactIfUnique(contact);
                var customer = new Customer
                {
                    ContactInfoIndex = retailTransaction.Contacts.Count - 1
                };
                retailTransaction.Customer = customer;
            }
        }

        private void RecoverContactFromCorhdr(RetailTransaction retailTransaction, OrderDataCollections dataCollections)
        {
            var address = GetTrimmedStringFieldValue(dataCollections.Corhdr.AddressLine1);
            var commaPosition = address.IndexOf(',');
            var addressLine1 = commaPosition == -1 ? address : address.Substring(commaPosition + 1);

            var contactInfo = new ContactInfo
            {
                ContactAddress = new Address
                {
                    AddressLine1 = GetTrimmedStringFieldValue(addressLine1),
                    AddressLine2 = GetTrimmedStringFieldValue(dataCollections.Corhdr.AddressLine2),
                    PostCode = GetTrimmedStringFieldValue(dataCollections.Corhdr.PostCode),
                    Town = GetTrimmedStringFieldValue(dataCollections.Corhdr.Town)
                },
                MobilePhone = GetTrimmedStringFieldValue(dataCollections.Corhdr.MobilePhoneNumber),
                Email = GetTrimmedStringFieldValue(dataCollections.Corhdr4.Email),
                Name = GetTrimmedStringFieldValue(dataCollections.Corhdr.CustomerName),
                Phone = GetTrimmedStringFieldValue(dataCollections.Corhdr.PhoneNumber),
                WorkPhone = GetTrimmedStringFieldValue(dataCollections.Corhdr4.WorkPhone)
            };

            retailTransaction.AddContactIfUnique(contactInfo);

            ((OrderFulfillment) retailTransaction.Fulfillments.First(x => x is OrderFulfillment)).ContactInfoIndex = retailTransaction.Contacts.Count - 1;
        }

        private void RecoverRelatedTransactions(RetailTransaction retailTransaction, IDailyTillTranKey dailyTillTranKey)
        {
            var relatedDlrcuses = dataLayerFactory.Create<DailyTillRepository>().SelectDlrcusesByOriginalTransactionKey(dailyTillTranKey);

            if (relatedDlrcuses != null && relatedDlrcuses.Count != 0)
            {
                retailTransaction.RelatedTransactions = new List<Transaction>();
                foreach (var relatedDlrcus in relatedDlrcuses)
                {
                    var relatedTranKey = new DailyTillTranKey
                        {
                            CreateDate = relatedDlrcus.CreateDate,
                            TillNumber = relatedDlrcus.TillNumber,
                            TransactionNumber = relatedDlrcus.TransactionNumber
                        };
                    var relatedTran = RecoverRetailTransaction(relatedTranKey);
                    retailTransaction.RelatedTransactions.Add(relatedTran);
                }
            }
        }

        private bool CheckIsStringValueOfLegacyTransaction(string value)
        {
            return string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim());
        }

        private string GetOriginalItemIndex(IDailyTillTranKey dailyTillTranKey, DailyTillRepository repo, string skuNumber)
        {
            return repo.GetItemLineNumberBySku(dailyTillTranKey, skuNumber).ToString();
        }

        private string GetRefundTransactionItemId(DailyCustomer refundInfoFromDlrcus, DailyTillTranKey refundDailyTillTranKey, DailyTillRepository repo)
        {
            var refundedSkuNumber = repo.GetItemSkuByItemLineNumber(refundDailyTillTranKey, refundInfoFromDlrcus.LineNumber);

            var originalDailyTillTranKey = new DailyTillTranKey
            {
                TransactionNumber = refundInfoFromDlrcus.OriginalTransactionNumber,
                TillNumber = refundInfoFromDlrcus.OriginalTillNumber,
                CreateDate = (DateTime)refundInfoFromDlrcus.OriginalSaleDate
            };

            return GetOriginalItemIndex(originalDailyTillTranKey, repo, refundedSkuNumber);
        }

        private string GetTrimmedStringFieldValue(string fieldValue)
        {
            return !string.IsNullOrEmpty(fieldValue) ? fieldValue.Trim() : string.Empty;
        }

        private bool IsTransactionCancellation(IDailyTillTranKey dailyTillTranKey, string orderNumber)
        {
            var repo = dataLayerFactory.Create<CustomerOrderRepository>();

            return repo.CheckIfTransactionIsCancellation(orderNumber, dailyTillTranKey);
        }
    }
}
