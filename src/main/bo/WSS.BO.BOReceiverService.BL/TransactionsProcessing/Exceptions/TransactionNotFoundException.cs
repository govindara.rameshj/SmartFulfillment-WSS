﻿using System;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions
{
    public class TransactionNotFoundException : ApplicationException
    {
        public TransactionNotFoundException()
        {
        }
    }
}
