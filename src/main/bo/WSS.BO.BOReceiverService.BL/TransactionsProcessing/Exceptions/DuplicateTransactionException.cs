﻿using System;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions
{
    public class DuplicateTransactionException : ApplicationException
    {
        public DuplicateTransactionException()
        {
        }
    }
}
