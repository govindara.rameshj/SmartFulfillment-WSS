﻿using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Enums;
using MiscInReasonCode = WSS.BO.Model.Enums.MiscInReasonCode;
using PaidOutReasonCode = WSS.BO.Model.Enums.PaidOutReasonCode;
using PriceOverrideReasonCode = WSS.BO.Model.Enums.PriceOverrideReasonCode;
using RefundReasonCode = WSS.BO.Model.Enums.RefundReasonCode;
using EventType = WSS.BO.Model.Enums.EventType;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    public static class ModelMapper
    {
        public static string GetDbModelCode(PriceOverrideReasonCode reasonCode)
        {
            switch (reasonCode)
            {
                case PriceOverrideReasonCode.DAMAGED_GOODS:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.DamagedGoods;
                case PriceOverrideReasonCode.INCORRECT_PRICE_DISPLAYED:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.IncorrectPriceDisplayed;
                case PriceOverrideReasonCode.REFUND_PRICE_DIFFERENCE:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.RefundPriceDifference;
                case PriceOverrideReasonCode.WEB_RETURN:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.WebReturn;
                case PriceOverrideReasonCode.HDC_RETURN:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.HdcReturn;
                case PriceOverrideReasonCode.MANAGERS_DISCRETION:
                    return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.ManagersDiscretion;
                default:
                    throw new ArgumentException(String.Format("Unknown PriceOverrideReasonCode [{0}]", reasonCode));
            }
        }

        public static PriceOverrideReasonCode GetPriceOverrideReasonCode(string dllineCode)
        {
            switch (dllineCode)
            {
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.DamagedGoods:
                    return PriceOverrideReasonCode.DAMAGED_GOODS;
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.IncorrectPriceDisplayed:
                    return PriceOverrideReasonCode.INCORRECT_PRICE_DISPLAYED;
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.RefundPriceDifference:
                    return PriceOverrideReasonCode.REFUND_PRICE_DIFFERENCE;
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.WebReturn:
                    return PriceOverrideReasonCode.WEB_RETURN;
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.HdcReturn:
                    return PriceOverrideReasonCode.HDC_RETURN;
                case BO.DataLayer.Model.Constants.PriceOverrideReasonCode.ManagersDiscretion:
                    return PriceOverrideReasonCode.MANAGERS_DISCRETION;
                default:
                    return PriceOverrideReasonCode.UNKNOWN;
            }
        }

        public static string GetSafeDbModelCode(PriceOverrideDiscount priceOverrideDiscount)
        {
            return priceOverrideDiscount != null ? GetDbModelCode(priceOverrideDiscount.PriceOverrideCode) : BO.DataLayer.Model.Constants.PriceOverrideReasonCode.NoPriceOverride;
        }

        public static string GetDbModelCode(RefundReasonCode reasonCode)
        {
            switch (reasonCode)
            {
                case RefundReasonCode.CUSTOMER_CHANGED_MIND:
                    return BO.DataLayer.Model.Constants.RefundReasonCode.CustomerChangedMind;
                case RefundReasonCode.DAMAGED:
                    return BO.DataLayer.Model.Constants.RefundReasonCode.Damaged;
                case RefundReasonCode.PARTS_MISSING:
                    return BO.DataLayer.Model.Constants.RefundReasonCode.PartsMissing;
                case RefundReasonCode.FAULTY:
                    return BO.DataLayer.Model.Constants.RefundReasonCode.Faulty;
                case RefundReasonCode.WEB_ORDER:
                    return BO.DataLayer.Model.Constants.RefundReasonCode.WebOrder;
                default:
                    throw new ArgumentException(String.Format("Unknown ReversalReasonCode [{0}]", reasonCode));
            }
        }

        public static RefundReasonCode GetRefundReasonCodeByDbModelCode(string dbModelCode)
        {
            switch (dbModelCode)
            {
                case BO.DataLayer.Model.Constants.RefundReasonCode.CustomerChangedMind:
                    return RefundReasonCode.CUSTOMER_CHANGED_MIND;
                case BO.DataLayer.Model.Constants.RefundReasonCode.Damaged:
                    return RefundReasonCode.DAMAGED;
                case BO.DataLayer.Model.Constants.RefundReasonCode.PartsMissing:
                    return RefundReasonCode.PARTS_MISSING;
                case  BO.DataLayer.Model.Constants.RefundReasonCode.Faulty:
                    return RefundReasonCode.FAULTY;
                case  BO.DataLayer.Model.Constants.RefundReasonCode.WebOrder:
                    return RefundReasonCode.WEB_ORDER;
                default:
                    return RefundReasonCode.UNKNOWN;
            }
        }

        public static string GetDbModelCode(PaidOutReasonCode reasonCode)
        {
            switch (reasonCode)
            {
                case PaidOutReasonCode.CUSTOMER_CONCERN:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.CustomerConcern.ToString();
                case PaidOutReasonCode.STATIONERY:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Stationery.ToString();
                case PaidOutReasonCode.BREAKFAST_VOUCHER:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.BreakfastVoucher.ToString();
                case PaidOutReasonCode.SOCIAL_FUND:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.SocialFund.ToString();
                case PaidOutReasonCode.TRAVEL:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Travel.ToString();
                case PaidOutReasonCode.POSTAGE:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Postage.ToString();
                case PaidOutReasonCode.MEETING_FOOD:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.MeetingFood.ToString();
                case PaidOutReasonCode.COLLEAGUE_WELFARE:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.ColleagueWelfare.ToString();
                case PaidOutReasonCode.COUNCIL_ACCOUNT:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.CouncilAccount.ToString();
                case PaidOutReasonCode.SUSPENSE:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Suspense.ToString();
                case PaidOutReasonCode.SAFE_UNDERS:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.SafeUnders.ToString();
                case PaidOutReasonCode.CLEANING:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Cleaning.ToString();
                case PaidOutReasonCode.CHARITY:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.Charity.ToString();
                case PaidOutReasonCode.REFUND_TILL:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.RefundTill.ToString();
                case PaidOutReasonCode.H_O_CHEQUE:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.HeadOfficeCheque.ToString();
                case PaidOutReasonCode.FORKLIFT_FUEL:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.ForkliftFuel.ToString();
                case PaidOutReasonCode.PROJECT_LOAN:
                    return BO.DataLayer.Model.Constants.PaidOutReasonCode.ProjectLoan.ToString();                
                default:
                    throw new ArgumentException(String.Format("Unknown PaidOutReasonCode [{0}]", reasonCode));
            }
        }

        public static int GetDbModelCode(MiscInReasonCode reasonCode)
        {
            switch (reasonCode)
            {
                case MiscInReasonCode.BAD_CHEQUE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.BadCheque;
                case MiscInReasonCode.CAR_PARK_TRADING:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.CarParkTrading;
                case MiscInReasonCode.SOCIAL_FUND:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.SocialFund;
                case MiscInReasonCode.SUSPENSE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.Suspense;
                case MiscInReasonCode.SAFE_OVERS:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.SafeOvers;
                case MiscInReasonCode.VENDING_MACHINE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.VendingMachine;
                case MiscInReasonCode.DISPLAY_SALE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.DisplaySale;
                case MiscInReasonCode.SAFE_CHANGE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.SafeChange;
                case MiscInReasonCode.COUNCIL_ACCOUNT:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.CouncilAccount;
                case MiscInReasonCode.CHARITY:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.Charity;
                case MiscInReasonCode.REFUND_TILL:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.RefundTill;
                case MiscInReasonCode.H_O_CHEQUE:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.HeadOfficeCheque;
                case MiscInReasonCode.REPAY_PROJECT_LOAN:
                    return BO.DataLayer.Model.Constants.MiscInReasonCode.RepayProjectLoan;
                default:
                    throw new ArgumentException(String.Format("Unknown PaidOutReasonCode [{0}]", reasonCode));
            }
        }

        public static string GetDbModelCode(EventType eventType)
        {
            switch (eventType)
            {
                case EventType.DEAL_GROUP:
                    return BO.DataLayer.Model.Constants.EventType.DealGroup;
                case EventType.HIERARCHY_SPEND:
                    return BO.DataLayer.Model.Constants.EventType.HierarchySpend;
                case EventType.MULTIBUY:
                    return BO.DataLayer.Model.Constants.EventType.Multibuy;
                case EventType.QUANTITY_BREAK:
                    return BO.DataLayer.Model.Constants.EventType.QuantutyBreak;
                default:
                    throw new ArgumentException(String.Format("Unknown EventType [{0}]", eventType));
            }
        }

        public static EventType GetEventTypeByCode(string eventCode)
        {
            switch (eventCode)
            {
                case BO.DataLayer.Model.Constants.EventType.DealGroup:
                    return EventType.DEAL_GROUP;
                case BO.DataLayer.Model.Constants.EventType.HierarchySpend:
                    return EventType.HIERARCHY_SPEND;
                case BO.DataLayer.Model.Constants.EventType.Multibuy:
                    return EventType.MULTIBUY;
                case BO.DataLayer.Model.Constants.EventType.QuantutyBreak:
                    return EventType.QUANTITY_BREAK;
                default:
                    throw new ArgumentException(String.Format("Unknown EventCode [{0}]", eventCode));
            }
        }

        public static int GetTenderTypeCode(CardType type)
        {
            if (type == CardType.AMERICAN_EXPRESS)
            {
                return TenderType.AmericanExpress;
            }
            return TenderType.CreditCard;
        }

        internal static CardType GetCardTypeByTenderType(decimal tenderType, string cardDescription)
        {
            if (tenderType == TenderType.CreditCard)
            {
                if (cardDescription.Contains("Mastercard"))
                {
                    return CardType.MASTER;
                }
                if (cardDescription.Contains("Maestro"))
                {
                    return CardType.MAESTRO;
                }
                if (cardDescription.Contains("Visa"))
                {
                    return CardType.VISA;
                }
                return CardType.UNKNOWN;
            }
            if (tenderType == TenderType.AmericanExpress)
            {
                return CardType.AMERICAN_EXPRESS;
            }

            throw new ArgumentException(String.Format("Unknown TenderType=[{0}]", tenderType));
        }

        // taken from frmTill.ProcessPaymentType
        public static string GetAuthorizationTypeCode(CardAcceptType cardAcceptType)
        {
            return cardAcceptType != CardAcceptType.KEYED ? AuthorizationType.Online : AuthorizationType.Referral;
        }

        public static CardAcceptType GetCardAcceptType(string authType)
        {
            return authType == AuthorizationType.Online ? CardAcceptType.SWIPED : CardAcceptType.KEYED;
        }

        internal static string ToSaveStatus(TransactionStatus transactionStatus)
        {
            return transactionStatus == TransactionStatus.COMPLETED ? "9b" : "99";
        }

        internal static string GetConfirmationText(RestrictedItem restrictedItem)
        {
            switch (restrictedItem.AcceptType)
            {
                case RestrictedItemAcceptType.ACCEPTED_WITH_CONFIRMATION:
                    return string.Format("\r\n{0}", restrictedItem.Confirmation);
                case RestrictedItemAcceptType.REJECTED_COMPLETELY:
                    return RestrictedItemConfirmationText.RejectedCompletely;
                case RestrictedItemAcceptType.REJECTED_NOT_CONFIRMED:
                    return RestrictedItemConfirmationText.RejectedWithConfirmation;
                default:
                    return null;
            }
        }
    }
}
