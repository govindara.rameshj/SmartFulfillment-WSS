﻿using System;
using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation
{
    public static class LegacyReceiptBarcodeHelper
    {
        private const int StoreNumberLength = 3;
        private const int TillIdLength = 2;
        private const int TranNumberLength = 4;
        private const int CreateDateLength = 6;

        public static string GenerateLegacyTranId(string storeNumber, string tranId, DateTime tranDate, string tillId)
        {
            var result = string.Format("{0}-{1}", Global.LegacySource, GenerateReceiptBarcode(storeNumber, tranId, tranDate, tillId));
            return result;
        }

        public static bool IsRefundForLegacyTransaction(string tranSourceId)
        {
            return !String.IsNullOrEmpty(tranSourceId) && tranSourceId.Trim().StartsWith(Global.LegacySource);
        }

        public static string GenerateReceiptBarcode(string storeNumber, string tranId, DateTime tranDate, string tillId)
        {
            var result = string.Format("{0}{1}{2}{3:ddMMyy}",
                storeNumber.PadLeft(StoreNumberLength, '0'),
                tillId.PadLeft(TillIdLength, '0'),
                tranId.PadLeft(TranNumberLength, '0'), tranDate);
            return result;
        }

        public static IDailyTillTranKey ParseTransactionKey(string tranKey)
        {
            return ParseTransactionKey(tranKey, 0);
        }        

        /// <summary>
        /// Parses string into dailytilltran key
        /// </summary>
        /// <param name="tranKey">String to parse</param>
        /// <param name="startKeyPosition">Not every key starts straihgt from store number (for example, legacy keys have "Legacy-SSSTTNNNNDDMMYY" format)</param>
        /// <returns></returns>
        public static IDailyTillTranKey ParseTransactionKey(string tranKey, int startKeyPosition)
        {
            var tillId = tranKey.Substring(startKeyPosition + StoreNumberLength, TillIdLength);
            var tranNo = tranKey.Substring(startKeyPosition + StoreNumberLength + TillIdLength, TranNumberLength);
            var createDate = ParseDateFromBarcode(tranKey.Substring(startKeyPosition + StoreNumberLength + TillIdLength + TranNumberLength, CreateDateLength));
            return new DailyTillTranKey
            {
                TillNumber = tillId,
                TransactionNumber = tranNo,
                CreateDate = createDate
            };
        }

        public static DateTime ParseDateFromBarcode(string barcodeDate)
        {
            try
            {
                return DateTime.ParseExact(barcodeDate, "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                var error = new ValidationError
                {
                    ErrorMessage = String.Format("The service can't process invalid date [{0}]", barcodeDate),
                    PropertyPath = "ReceiptBarcodeDate"
                };

                List<ValidationError> errors = new List<ValidationError> { error };
                throw new ValidationException("Incoming request fields are not valid.", errors);
            }
        }
    }
}
