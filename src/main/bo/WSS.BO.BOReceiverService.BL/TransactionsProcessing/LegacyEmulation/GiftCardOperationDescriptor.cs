﻿using WSS.BO.Model.Entity;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation
{
    public class GiftCardOperationDescriptor
    {
        public GiftCardOperation GiftCardOperation { get; set; }

        public decimal Amount { get; set; }

        public string TransactionType { get; set; }
    }
}
