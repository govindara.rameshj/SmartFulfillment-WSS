﻿using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation
{
    class LegacyTransactionSplittingEmulator
    {
        private readonly ITransactionProcessingContext originalCtx;

        public LegacyTransactionSplittingEmulator(ITransactionProcessingContext originalCtx)
        {
            this.originalCtx = originalCtx;
        }

        public List<ITransactionProcessingContext> TryPerformSplit()
        {
            List<ITransactionProcessingContext> trans = new List<ITransactionProcessingContext>();

            var refundToGiftCardCtx = ProcessRefundToGiftCard();

            if (refundToGiftCardCtx != null)
            {
                trans.Add(originalCtx);
                trans.Add(refundToGiftCardCtx);
            }

            var saleOfGiftCardCtxList = ProcessGiftCardSaleTransaction();

            if (saleOfGiftCardCtxList != null)
            {
                trans.Add(originalCtx);
                trans.AddRange(saleOfGiftCardCtxList);
            }

            if (!trans.Any())
            {
                trans.Add(originalCtx);
            }

            return trans;
        }

        private ITransactionProcessingContext ProcessRefundToGiftCard()
        {
            ITransactionProcessingContext result = null;

            var tran = originalCtx.GetTransaction() as RetailTransaction;

            if (tran != null)
            {
                var refundsToGiftCard = tran.Payments
                    .Where(x => x is GiftCardPayment && x.Direction == Direction.TO_CUSTOMER)
                    .ToList();

                if (refundsToGiftCard.Any())
                {
                    var newCashPayment = refundsToGiftCard.Select(x => ConvertGiftPaymentToCashPayment(x, Direction.TO_CUSTOMER)).ToList();
                    var reversedCashPayment = refundsToGiftCard.Select(x => ConvertGiftPaymentToCashPayment(x, Direction.FROM_CUSTOMER)).ToList();

                    tran.Payments = tran.Payments.Where(x => !(x is GiftCardPayment)).Concat(newCashPayment).ToList();

                    var giftcardOperationDescriptors = GetGiftCardOperationDesscriptorsList(refundsToGiftCard);

                    originalCtx.GiftCardOperationDescriptors = giftcardOperationDescriptors;

                    result = GenerateMiscInTransaction(tran, giftcardOperationDescriptors, reversedCashPayment);
                }
            }

            return result;
        }

        private IList<ITransactionProcessingContext> ProcessGiftCardSaleTransaction()
        {
            IList<ITransactionProcessingContext> result = null;

            var tran = originalCtx.GetTransaction() as RetailTransaction;

            if (tran != null && tran.TransactionStatus != TransactionStatus.VOIDED)
            {
                var soldGiftCardItems = tran.Items
                    .Where(x => !x.IsReversed && x.Product is GiftCardProduct && ((GiftCardProduct)x.Product).AbsAmount != 0).ToList();

                if (soldGiftCardItems.Any())
                {
                    var giftcardOperationDescriptor = soldGiftCardItems.Select(
                        item => new GiftCardOperationDescriptor
                        {
                            GiftCardOperation = ((GiftCardProduct)item.Product).GiftCardOperation,
                            Amount = item.Product.CalculateAbsAmount(),
                            TransactionType = TransactionTypeForCard.SaleOfVoucher
                        }).ToList();

                    originalCtx.GiftCardOperationDescriptors = giftcardOperationDescriptor;

                    result = new List<ITransactionProcessingContext>
                    {
                        GetRefundTransactionForGiftCard(tran, giftcardOperationDescriptor.Sum(x => x.Amount)),
                        GenerateMiscInTransaction(tran, giftcardOperationDescriptor),
                    };
                }
            }

            return result;
        }

        private ITransactionProcessingContext GenerateMiscInTransaction(RetailTransaction tran, IList<GiftCardOperationDescriptor> giftcardOperationDescriptors, List<Payment> payments = null)
        {
            var transaction = new MiscInTransaction
            {
                TotalAmount = giftcardOperationDescriptors.Sum(x => x.Amount),
                AuthorizerNumber = null,
                BrandCode = tran.BrandCode,
                CashierNumber = tran.CashierNumber,
                CreateDateTime = tran.CreateDateTime,
                InvoiceNumber = "00000000",
                IsTraining = tran.IsTraining,
                TransactionStatus = tran.TransactionStatus,
                Payments = payments ?? new List<Payment> { new CashPayment { AbsAmount = giftcardOperationDescriptors.Sum(x => x.Amount), Direction = Direction.FROM_CUSTOMER } },
                Source = tran.Source,
                SourceTransactionId = null,
                StoreNumber = tran.StoreNumber,
                TillNumber = tran.TillNumber,
                TransactionNumber = tran.TransactionNumber,
                ReceiptBarcode = tran.ReceiptBarcode,
                VoidSupervisor = tran.VoidSupervisor
            };

            return CreateTransactionContext(transaction, true);
        }

        private ITransactionProcessingContext GetRefundTransactionForGiftCard(RetailTransaction saleTran, decimal totalAmount)
        {
            var transaction = new RetailTransaction
            {
                TotalAmount = -totalAmount,
                TaxAmount = 0,
                AuthorizerNumber = saleTran.AuthorizerNumber,
                BrandCode = saleTran.BrandCode,
                Cancellation = null,
                CashierNumber = saleTran.CashierNumber,
                CreateDateTime = saleTran.CreateDateTime,
                Customer = saleTran.Customer,
                IsTraining = saleTran.IsTraining,
                Manager = saleTran.Manager,
                Payments = new List<Payment> { new CashPayment { AbsAmount = totalAmount, Direction = Direction.TO_CUSTOMER } },
                RecalledTransactionId = saleTran.RecalledTransactionId,
                RefundCashier = null,
                Source = saleTran.Source,
                SourceTransactionId = null,
                StoreNumber = saleTran.StoreNumber,
                TillNumber = saleTran.TillNumber,
                ReceiptBarcode = saleTran.ReceiptBarcode,
                TransactionNumber = saleTran.TransactionNumber,
                TransactionStatus = saleTran.TransactionStatus,
                VoidSupervisor = saleTran.VoidSupervisor,
                Fulfillments = new List<Fulfillment>(),
                Coupons = new List<Coupon>(),
                RestrictedItems = new List<RestrictedItem>()
            };

            //Here we clone GiftCardItems from original sale transaction into created refund transaction with changed direction
            var originalGiftCardItems = saleTran.Items.Where(x => x.Product is GiftCardProduct).ToList();
            var processedGiftCardItems = new List<RetailTransactionItem>();
            foreach (var originalItem in originalGiftCardItems)
            {
                var newItem = Mapper.ObjectToObject(originalItem);
                newItem.Direction = Direction.FROM_CUSTOMER;
                var originalItemProduct = (GiftCardProduct)originalItem.Product;
                var newProduct = Mapper.ObjectToObject(originalItemProduct);
                var newGiftCardOperation = Mapper.ObjectToObject(originalItemProduct.GiftCardOperation);
                newProduct.GiftCardOperation = newGiftCardOperation;
                newItem.Product = newProduct;
                if (originalItem.ItemRefund != null)
                {
                    newItem.ItemRefund = Mapper.ObjectToObject(originalItem.ItemRefund);
                }
                processedGiftCardItems.Add(newItem);
            }
            transaction.Items = processedGiftCardItems;

            return CreateTransactionContext(transaction, false);
        }

        private ITransactionProcessingContext CreateTransactionContext(Transaction transaction, bool isGeneratedByGiftCardPreprocessing, IList<GiftCardOperationDescriptor> giftcardOperationDescriptors = null)
        {
            return new TransactionProcessingContextFactory().CreateChildContext(transaction,
                originalCtx,
                isGeneratedByGiftCardPreprocessing,
                giftcardOperationDescriptors);
        }

        private Payment ConvertGiftPaymentToCashPayment(Payment payment, Direction direction)
        {
            return new CashPayment { AbsAmount = payment.AbsAmount, Direction = direction };
        }

        private List<GiftCardOperationDescriptor> GetGiftCardOperationDesscriptorsList(List<Payment> giftCardPayments)
        {
            return giftCardPayments.Select(x => new GiftCardOperationDescriptor
            {
                GiftCardOperation = ((GiftCardPayment)x).GiftCardOperation,
                Amount = x.AbsAmount,
                TransactionType = TransactionTypeForCard.TenderInRefund
            }).ToList();
        }
    }
}
