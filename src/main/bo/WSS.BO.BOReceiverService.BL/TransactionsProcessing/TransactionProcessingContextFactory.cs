using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Net4Replacements;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    public class TransactionProcessingContextFactory
    {
        public ITransactionProcessingContext CreateMainContext(Transaction transaction,
            DateTime receivedWhen, DateTime effectiveProcessingDate,
            IReadOnlyDataRetriever cachableDataRetriever, IRuntimeDataRetriever nonCachableDataRetriever,
            IConfiguration cfg)
        {
            return CreateContext(transaction, receivedWhen, effectiveProcessingDate, cachableDataRetriever, nonCachableDataRetriever, 
                new StagingDb(), cfg, false, null, new Dictionary<Tuple<string, string>, IDailyTillTranKey>());
        }

        public ITransactionProcessingContext CreateChildContext(Transaction transaction, ITransactionProcessingContext mainContext, 
            bool isGeneratedByGiftCardPreprocessing, IList<GiftCardOperationDescriptor> giftcardOperationDescriptors)
        {
            return CreateContext(transaction,
                mainContext.ReceivedWhen, mainContext.EffectiveProcessingDate,
                mainContext.ReadOnlyDataRetriever, mainContext.RuntimeDataRetriever,
                new StagingDb(),
                mainContext.Config,
                isGeneratedByGiftCardPreprocessing,
                giftcardOperationDescriptors, mainContext.CachedDailyTillTranKeys);
        }


        private ITransactionProcessingContext CreateContext(Transaction transaction,
            DateTime receivedWhen, DateTime effectiveProcessingDate,
            IReadOnlyDataRetriever cachableDataRetriever, IRuntimeDataRetriever nonCachableDataRetriever,
            StagingDb stagingDb, IConfiguration cfg, bool isGeneratedByGiftCardPreprocessing,
            IList<GiftCardOperationDescriptor> giftcardOperationDescriptors, Dictionary<Tuple<string, string>, IDailyTillTranKey> cachedDailyTillTranKeys)
        {
            TransactionProcessingContext result = EnumeratePossibleContextCreators(transaction).FirstOrDefault(createdContext => createdContext != null);

            if (result == null)
            {
                throw new ArgumentException(String.Format("Unknown transaction type : {0}", transaction.GetType()));
            }

            result.ReadOnlyDataRetriever = cachableDataRetriever;
            result.RuntimeDataRetriever = nonCachableDataRetriever;
            result.ReceivedWhen = receivedWhen;
            result.StagingDb = stagingDb;
            result.EffectiveProcessingDate = effectiveProcessingDate;
            result.Config = cfg;
            result.GiftCardOperationDescriptors = giftcardOperationDescriptors;
            result.IsGeneratedByGiftCardPreprocessing = isGeneratedByGiftCardPreprocessing;
            result.CachedDailyTillTranKeys = cachedDailyTillTranKeys;
            return result;
        }

        private static IEnumerable<TransactionProcessingContext> EnumeratePossibleContextCreators(Transaction tran)
        {
            yield return TryCreateContext<RetailTransaction>(tran);
            yield return TryCreateContext<MiscInTransaction>(tran);
            yield return TryCreateContext<PaidOutTransaction>(tran);
            yield return TryCreateContext<OpenDrawerTransaction>(tran);
            yield return TryCreateContext<AuditTransaction>(tran);
        }

        private static TransactionProcessingContext TryCreateContext<T>(Transaction transaction) where T : Transaction
        {
            TransactionProcessingContext result;
            var typed = transaction as T;
            if (typed != null)
            {
                result = new TransactionProcessingContext<T>
                {
                    Transaction = typed
                };
            }
            else
            {
                result = null;
            }

            return result;
        }
    }
}