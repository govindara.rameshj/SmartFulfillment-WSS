﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class TransactionStagingData
    {
        private readonly Dictionary<RetailTransactionItem, DailyTillLine> dailyTillLines =
            new Dictionary<RetailTransactionItem, DailyTillLine>();

        private CashBalCashier cashierBalanceUpdate;
        private readonly List<CashBalCashierTen> cashierBalanceTenderUpdates = new List<CashBalCashierTen>();

        private readonly List<CashBalCashierTenVar> cashierBalanceTenderVarianceUpdates =
            new List<CashBalCashierTenVar>();

        public TransactionStagingData()
        {
            Dltots = null;
            Dlpaids = new List<Dlpaid>();
            Dlrcuses = new List<DailyCustomer>();
            StockUpdates = new Dictionary<int, StockUpdate>();
            Dlcommes = new List<Dlcomm>();
            Corlins = new List<CustomerOrderLine>();
            Corhdr = null;
            Corhdr4 = null;
            Corhdr5 = null;
            DlGiftCards = new List<DailyGiftCard>();
            CompensatingDllines = new List<DailyTillLine>();
            Dlevnts = new List<Dlevnt>();
            Dlanases = new List<Dlanas>();
            Dlgifts = new List<DailyGiftToken>();
            Dlocuses = new List<Dlocus>();
            Dlolins = new List<Dlolin>();
            Dlrejects = new List<Dlreject>();
            Dlcoupons = new List<Dlcoupon>();
            Correfunds = new List<CustomerOrderRefund>();
            Corlins2 = new List<CustomerOrderLine2>();
            Cortxts = new List<CustomerOrderText>();
        }

        public Dltots Dltots { get; set; }
        public List<Dlpaid> Dlpaids { get; set; }
        public List<DailyCustomer> Dlrcuses { get; set; }
        public List<Dlcomm> Dlcommes { get; set; }
        public List<CustomerOrderLine> Corlins { get; set; }
        public Dictionary<int, StockUpdate> StockUpdates { get; set; }
        public DltotsUpdate DltotsUpdate { get; set; }
        public CustomerOrderHeader Corhdr { get; set; }
        public CustomerOrderHeader4 Corhdr4 { get; set; }
        public CustomerOrderHeader5 Corhdr5 { get; set; }
        public List<DailyGiftCard> DlGiftCards { get; set; }
        public List<DailyTillLine> CompensatingDllines { get; set; }
        public List<Dlevnt> Dlevnts { get; set; }
        public List<Dlanas> Dlanases { get; set; }
        public List<DailyGiftToken> Dlgifts { get; set; }
        public List<Dlocus> Dlocuses { get; set; }
        public List<Dlolin> Dlolins { get; set; }
        public List<Dlreject> Dlrejects { get; set; }
        public List<Dlcoupon> Dlcoupons { get; set; }
        public List<CustomerOrderRefund> Correfunds { get; set; }
        public List<CustomerOrderHeader> CorhdrUpdatesForOrderCancellation = new List<CustomerOrderHeader>();
        public List<CustomerOrderHeader4> Corhdr4UpdatesForOrderCancellation = new List<CustomerOrderHeader4>();
        public List<CustomerOrderLine> CorlinUpdatesForOrderCancellation = new List<CustomerOrderLine>();
        public List<CustomerOrderLine2> Corlins2 { get; set; }
        public List<CustomerOrderText> Cortxts { get; set; }
        public DailyTillLine deliveryChargeDlline { get; set; }

        public IEnumerable<DailyTillLine> GetDailyTillLines()
        {
            return dailyTillLines.Values;
        }

        public void AddDailyTillLine(DailyTillLine dlline, RetailTransactionItem item)
        {
            dailyTillLines.Add(item, dlline);
        }

        public void AddDailyGiftCard(List<DailyGiftCard> dlgiftcards)
        {
            DlGiftCards.AddRange(dlgiftcards);
        }

        public void AddCashierBalanceUpdate(CashBalCashier cashbalBusinessRuleUpdate,
            IEnumerable<CashBalCashierTen> tenderUpdates,
            IEnumerable<CashBalCashierTenVar> tenderVarianceUpdates)
        {
            cashierBalanceUpdate = cashbalBusinessRuleUpdate;
            cashierBalanceTenderUpdates.AddRange(tenderUpdates);
            cashierBalanceTenderVarianceUpdates.AddRange(tenderVarianceUpdates);
        }

        internal DailyTillLine GetDailyTillLine(RetailTransactionItem tranItem)
        {
            return dailyTillLines[tranItem];
        }

        public void Save(IRepositoriesFactory repoFactory)
        {
            SetupTransactionNumber(repoFactory);
            SetupOrderNumber(repoFactory);

            #region DL*

            {
                var repo = repoFactory.Create<IDailyTillRepository>();

                try
                {
                    if (Dltots != null)
                    {
                        repo.InsertIntoDltots(Dltots);
                    }
                }
                catch (DataException)
                {
                    if (IsDltotsExists(repoFactory, Dltots.SourceTranId, Dltots.Source))
                    {
                        throw new DuplicateTransactionException();
                    }
                    else
                    {
                        throw;
                    }
                }

                foreach (var dlline in dailyTillLines)
                {
                    repo.InsertIntoDlline(dlline.Value);
                }

                foreach (var dlpaid in Dlpaids)
                {
                    repo.InsertIntoDlpaid(dlpaid);
                }

                foreach (var dlrcus in Dlrcuses)
                {
                    repo.InsertIntoDlrcus(dlrcus);
                }

                foreach (var dlcomm in Dlcommes)
                {
                    repo.InsertIntoDlcomm(dlcomm);
                }

                int dlgiftcardSequenceNumber = 1;
                foreach (var dlgiftcard in DlGiftCards)
                {
                    dlgiftcard.SequenceNumber = dlgiftcardSequenceNumber;
                    repo.InsertIntoDlGiftCard(dlgiftcard);
                    dlgiftcardSequenceNumber++;
                }

                foreach (var hackDlline in CompensatingDllines)
                {
                    repo.InsertIntoDlline(hackDlline);
                }

                if (deliveryChargeDlline != null)
                {
                    repo.InsertIntoDlline(deliveryChargeDlline);
                }

                foreach (var dlolin in Dlolins)
                {
                    repo.InsertIntoDlolin(dlolin);
                }

                foreach (var dlevnt in Dlevnts)
                {
                    repo.InsertIntoDlevnt(dlevnt);
                }

                int dlcouponSequenceNumber = 1;
                foreach (var dlcoupon in Dlcoupons)
                {
                    dlcoupon.SequenceNumber = dlcouponSequenceNumber.ToString().PadLeft(4, '0');
                    repo.InsertIntoDlcoupon(dlcoupon);
                }

                int dlrejectSequenceNumber = 1;
                foreach (var dlreject in Dlrejects)
                {
                    dlreject.SequenceNumber = dlrejectSequenceNumber;
                    repo.InsertIntoDlreject(dlreject);
                    dlrejectSequenceNumber++;
                }
            }

            #endregion

            {
                var repo = repoFactory.Create<IStockRepository>();
                foreach (var stockUpdate in StockUpdates)
                {
                    stockUpdate.Value.MovementKeys =
                        Stklog.GenerateMovementKeys(
                            dailyTillLines.FirstOrDefault(x => x.Key.ItemIndex == stockUpdate.Key).Value);
                    repo.ApplyStockUpdate(stockUpdate.Value);
                }
            }

            {
                var repo = repoFactory.Create<IDailyTillRepository>();

                if (DltotsUpdate != null)
                {
                    repo.UpdateDltots(DltotsUpdate);
                }
            }

            #region COR*

            {
                var repo = repoFactory.Create<ICustomerOrderRepository>();

                foreach (var corlin in Corlins)
                {
                    repo.InsertIntoCorlin(corlin);
                }

                if (Corhdr != null)
                {
                    repo.InsertIntoCorhdr(Corhdr);
                }

                if (Corhdr4 != null)
                {
                    repo.InsertIntoCorhdr4(Corhdr4);
                }

                if (Corhdr5 != null)
                {
                    repo.InsertIntoCorhdr5(Corhdr5);
                }

                foreach (var correfund in Correfunds)
                {
                    repo.InsertIntoCorrefund(correfund);
                }

                foreach (var corlinUpdate in CorlinUpdatesForOrderCancellation)
                {
                    repo.ApplyCorlinUpdate(corlinUpdate);
                }

                foreach (var corhdrUpdate in CorhdrUpdatesForOrderCancellation)
                {
                    repo.ApplyCorhdrUpdate(corhdrUpdate);
                }

                foreach (var corhdr4Update in Corhdr4UpdatesForOrderCancellation)
                {
                    repo.ApplyCorhdr4Update(corhdr4Update);
                }

                foreach (var corlin2 in Corlins2)
                {
                    repo.InsertIntoCorlin2(corlin2);
                }

                foreach (var cortxt in Cortxts)
                {
                    repo.InsertIntoCortxt(cortxt);
                }
            }

            #endregion

            if (cashierBalanceUpdate != null)
            {
                var repo = repoFactory.Create<ICashierBalanceRepository>();

                repo.ApplyCashierBalanceUpdate(cashierBalanceUpdate);
                foreach (var update in cashierBalanceTenderUpdates)
                {
                    repo.ApplyCashierBalanceTenderUpdate(update);
                }
                foreach (var update in cashierBalanceTenderVarianceUpdates)
                {
                    repo.ApplyCashierBalanceTenderVarianceUpdate(update);
                }
            }
        }

        private void SetupTransactionNumber(IRepositoriesFactory repoFactory)
        {
            var transactionNumber = GetTransactionNumberByDltots(repoFactory);

            if (Dltots != null)
            {
                Dltots.TransactionNumber = transactionNumber;
            }

            foreach (var dlline in dailyTillLines)
            {
                dlline.Value.TransactionNumber = transactionNumber;
            }

            foreach (var dlpaid in Dlpaids)
            {
                dlpaid.TransactionNumber = transactionNumber;
            }

            foreach (var dlrcus in Dlrcuses)
            {
                dlrcus.TransactionNumber = transactionNumber;
            }

            foreach (var dlcomm in Dlcommes)
            {
                dlcomm.TransactionNumber = transactionNumber;
            }

            foreach (var dlgiftcard in DlGiftCards)
            {
                dlgiftcard.TransactionNumber = transactionNumber;
            }

            foreach (var hackDlline in CompensatingDllines)
            {
                hackDlline.TransactionNumber = transactionNumber;
            }

            if (deliveryChargeDlline != null)
            {
                deliveryChargeDlline.TransactionNumber = transactionNumber;
            }

            foreach (var dlolin in Dlolins)
            {
                dlolin.TransactionNumber = transactionNumber;
            }

            foreach (var dlevnt in Dlevnts)
            {
                dlevnt.TransactionNumber = transactionNumber;
            }

            foreach (var dlcoupon in Dlcoupons)
            {
                dlcoupon.TransactionNumber = transactionNumber;
            }

            foreach (var dlreject in Dlrejects)
            {
                dlreject.TransactionNumber = transactionNumber;
            }

            if (Corhdr != null)
            {
                Corhdr.TransactionNumber = transactionNumber;
            }

            foreach (var cancellationUpdate in CorhdrUpdatesForOrderCancellation)
            {
                cancellationUpdate.RefundTransactionNumber = transactionNumber;
            }

            foreach (var correfund in Correfunds)
            {
                correfund.RefundTransaction = transactionNumber;
            }
        }

        private void SetupOrderNumber(IRepositoriesFactory repoFactory)
        {
            var orderNumber = Global.NullOrderNumber;

            //If transaction we process is order
            if (Corhdr != null)
            {
                orderNumber = GetNextOrderNumber(repoFactory);
            }

            //If it is not order cancellation (there are no corhdr updates), setup orderNumber for dltots.
            //Otherwise it was already setted during convertation, and we should not override the value.
            if (Dltots != null && !CorhdrUpdatesForOrderCancellation.Any())
            {
                Dltots.OrderNumber = orderNumber;
            }

            foreach (var corlin in Corlins)
            {
                corlin.OrderNumber = orderNumber;
                corlin.SellingStoreOrderId = Int32.Parse(orderNumber);
            }

            if (Corhdr != null)
            {
                Corhdr.OrderNumber = orderNumber;
            }

            if (Corhdr4 != null)
            {
                Corhdr4.OrderNumber = orderNumber;
                Corhdr4.SellingStoreOrderId = Int32.Parse(orderNumber);
            }

            if (Corhdr5 != null)
            {
                Corhdr5.OrderNumber = orderNumber;
            }

            foreach (var corlin2 in Corlins2)
            {
                corlin2.OrderNumber = orderNumber;
            }

            foreach (var cortxt in Cortxts)
            {
                cortxt.OrderNumber = orderNumber;
                cortxt.SellingStoreOrderId = Int32.Parse(orderNumber);
            }
        }

        private string GetTransactionNumberByDltots(IRepositoriesFactory repoFactory)
        {
            if (Dltots != null)
            {
                var repo = repoFactory.Create<IDailyTillRepository>();

                return repo.GenerateNextTransactionNumber(Dltots.TillNumber);
            }
            return string.Empty;
        }

        private string GetNextOrderNumber(IRepositoriesFactory repoFactory)
        {
            var repo = repoFactory.Create<ICustomerOrderRepository>();
            return repo.GetNextOrderNumber();
        }

        private bool IsDltotsExists(IRepositoriesFactory repoFactory, string sourceTransactionId, string source)
        {
            var repo = repoFactory.Create<IDailyTillRepository>();
            return repo.IsDltotsExists(sourceTransactionId, source);
        }
    }
}
