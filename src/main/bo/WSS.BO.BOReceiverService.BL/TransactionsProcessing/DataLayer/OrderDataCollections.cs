﻿using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class OrderDataCollections
    {
        private readonly IDataLayerFactory _factory;

        public CustomerOrderHeader Corhdr = new CustomerOrderHeader();
        public CustomerOrderHeader4 Corhdr4 = new CustomerOrderHeader4();
        public CustomerOrderHeader5 Corhdr5 = new CustomerOrderHeader5();
        public List<CustomerOrderLine> Corlins = new List<CustomerOrderLine>();
        public List<CustomerOrderLine2> Corlins2 = new List<CustomerOrderLine2>();
        public List<CustomerOrderRefund> Correfunds = new List<CustomerOrderRefund>();
        public List<CustomerOrderText> Cortxts = new List<CustomerOrderText>(); 

        public OrderDataCollections(IDataLayerFactory factory)
        {
            _factory = factory;
        }

        public void RecoverCollectionsFromDatabase(string orderNumber)
        {
            var repo = _factory.Create<CustomerOrderRepository>();
            Corhdr = repo.SelectCorhdr(orderNumber);
            Corhdr4 = repo.SelectCorhdr4(orderNumber);
            Corhdr5 = repo.SelectCorhdr5(orderNumber);
            Corlins = repo.SelectCorlins(orderNumber);
            Corlins2 = repo.SelectCorlins2(orderNumber);
            Correfunds = repo.SelectCorrefunds(orderNumber);
            Cortxts = repo.SelectCortxts(orderNumber);
        }

        public bool WasMadeInOvcTill()
        {
            return Corhdr5 != null && Corhdr5.Source == Global.OvcOrderSource;
        }
    }
}
