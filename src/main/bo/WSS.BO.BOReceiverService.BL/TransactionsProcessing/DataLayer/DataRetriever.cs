﻿using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class DataRetriever : IReadOnlyDataRetriever, IRuntimeDataRetriever
    {
        private readonly IDataLayerFactory dlFactory;

        public DataRetriever(IDataLayerFactory dlFactory)
        {
            this.dlFactory = dlFactory;
        }

        public VatRates GetVatRates()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            var rates = repo.GetVatRatesListFromRetopt();
            return new VatRates(rates);
        }

        public SkuInfo GetSkuInfo(string skuNumber)
        {
            var repo = dlFactory.Create<IStockRepository>();
            return repo.GetSkuInfo(skuNumber);
        }

        public decimal GetStockMasterPrice(string skuNumber)
        {
            var repo = dlFactory.Create<IStockRepository>();
            return repo.GetStockMasterPrice(skuNumber);
        }

        public string GetGiftCardSkuFromParameters()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetSettingStringValue(ParameterId.GiftCardSku);
        }

        public string GetDeliveryChargeSkuFromParameters()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetSettingStringValue(ParameterId.DeliveruChargeSku);
        }

        public DeliveryChargeContainer GetDeliveryChargeContainer()
        {
            var repo = dlFactory.Create<IDeliveryChargeRepository>();
            return repo.GetDeliveryChargeContainer();
        }

        public string GetDefaultSystemCurrency()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetDefaultSystemCurrency();
        }

        public int GetTradingPeriodId(DateTime createDateTime)
        {
            var repo = dlFactory.Create<ISafeRepository>();
            var safe = repo.GetSafe(createDateTime);
            if (safe == null)
            {
                throw new ApplicationException(String.Format("No safe record found for period [{0:yyyy-MM-dd}]", createDateTime));
            }
            return safe.PeriodId;
        }

        public string GetStoreId()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetStoreId();
        }

        public int GetAbsoluteStoreId()
        {
            return dlFactory.Create<IDictionariesRepository>().GetAbsoluteStoreId();
        }

        public string GetOrderNumberByOriginalTransactionId(string refTransactionId, string source)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.GetOrderNumberByOriginalTransactionId(refTransactionId, source);
        }

        public Dltots GetDltotsByKey(IDailyTillTranKey key)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.SelectDailyTillTran(key);
        }

        public bool IsDltotsExists(IDailyTillTranKey key)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.IsDltotsExists(key);
        }

        public bool IsDltotsExists(string refTransactionId, string source)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.IsDltotsExists(refTransactionId, source);
        }

        public string GetNextOrderNumber()
        {
            var repo = dlFactory.Create<ICustomerOrderRepository>();
            return repo.GetNextOrderNumber();
        }

        public string GetCorlinLineNumberBySourceLineNumber(int sourceLineNumber, string orderNumber)
        {
            var repo = dlFactory.Create<ICustomerOrderRepository>();
            return repo.GetCorlinLineNumberBySourceLineNumber(sourceLineNumber, orderNumber);
        }

        public IDictionary<string, Syscod> GetSystemCodes(string systemCodeType)
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.SelectSystemCodes(systemCodeType);
        }

        public IDictionary<string, SystemCode> GetNewSystemCodes(string systemCodeType)
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.SelectNewSystemCodes(systemCodeType);
        }

        public int? GetVirtualCashierId(int tillNumber)
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetVirtualCashierId(tillNumber);
        }

        public IEnumerable<DeliverySlot> GetDeliverySlots()
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetDeliverySlots();
        }

        public bool CheckOrderIsDelivered(string orderNumber)
        {
            var repo = dlFactory.Create<ICustomerOrderRepository>();
            return repo.CheckOrderIsDelivered(orderNumber);
        }

        public CouponMaster SelectCouponMaster(string couponMasterId)
        {
            var repo = dlFactory.Create<ICouponRepository>();
            return repo.GetCouponMaster(couponMasterId);
        }

        public DailyTillLine SelectDailyLineBySourceItemId(IDailyTillTranKey refundedDailyTillTranKey, string refundedTransactionItemId)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.SelectDailyLineBySourceItemId(refundedDailyTillTranKey, refundedTransactionItemId);
        }

        public DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, int sequenceNumber)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.SelectDailyTillLine(tran, sequenceNumber);
        }

        public bool IsTransactionToCancelForOrder(string refundedTransactionId, string source)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.IsTransactionToCancelForOrder(refundedTransactionId, source);
        }

        public IDailyTillTranKey GetDailyTillTranKey(string sourceTransactionId, string source)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.GetDailyTillTranKey(sourceTransactionId, source);
        }

        public string GetSettingStringValue(int parameterId)
        {
            var repo = dlFactory.Create<IDictionariesRepository>();
            return repo.GetSettingStringValue(parameterId);
        }

        public DailyTillLine GetOriginalLineNumber(IDailyTillTranKey tran, string sourceItemId)
        {
            var repo = dlFactory.Create<IDailyTillRepository>();
            return repo.GetOriginalLineNumber(tran, sourceItemId);
        }

        public RelatedItem SelectRelatedItem(string packageItemNumber)
        {
            var repo = dlFactory.Create<IRelatedItemRepository>();
            return repo.SelectRelatedItem(packageItemNumber);
        }
    }
}
