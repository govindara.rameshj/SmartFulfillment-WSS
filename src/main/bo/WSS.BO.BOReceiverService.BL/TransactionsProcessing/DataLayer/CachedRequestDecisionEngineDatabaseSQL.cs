﻿using System;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Hubs.Core.System.EndOfDayLock;
using WSS.BO.DataLayer.Model;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class CachedRequestDecisionEngineDatabaseSql : RequestDecisionEngineDatabaseSQL
    {
        private readonly IReadOnlyDataRetriever _readOnlyDataRetriever;
        private readonly ISystemEnvironment _systemEnvironment;

        public CachedRequestDecisionEngineDatabaseSql(IDataLayerFactory factory, IReadOnlyDataRetriever readOnlyDataRetriever, ISystemEnvironment systemEnvironment) : base(factory)
        {
            _readOnlyDataRetriever = readOnlyDataRetriever;
            _systemEnvironment = systemEnvironment;
        }

        public override int NitmasCompleteTaskID()
        {
            return Int32.Parse(_readOnlyDataRetriever.GetSettingStringValue(6001));
        }

        public override DateTime NitmasPendingTime()
        {
            var strSplit = _readOnlyDataRetriever.GetSettingStringValue(6000).Split(':');
            var now = _systemEnvironment.GetDateTimeNow();
            return new DateTime(now.Year, now.Month, now.Day, Int32.Parse(strSplit[0]), Int32.Parse(strSplit[1]), 0);
        }
    }
}
