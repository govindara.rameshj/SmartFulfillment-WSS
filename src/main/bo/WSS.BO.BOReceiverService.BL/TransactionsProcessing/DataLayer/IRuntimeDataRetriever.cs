﻿using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public interface IRuntimeDataRetriever
    {
        string GetOrderNumberByOriginalTransactionId(string refTransactionId, string source);
        string GetNextOrderNumber();
        string GetCorlinLineNumberBySourceLineNumber(int sourceLineNumber, string orderNumber);
        bool CheckOrderIsDelivered(string orderNumber);
        CouponMaster SelectCouponMaster(string couponMasterId);
        Dltots GetDltotsByKey(IDailyTillTranKey key);
        bool IsDltotsExists(IDailyTillTranKey key);
        bool IsDltotsExists(string refTransactionId, string source);
        decimal GetStockMasterPrice(string skuNumber);
        DailyTillLine SelectDailyLineBySourceItemId(IDailyTillTranKey refundedDailyTillTranKey, string refundedTransactionItemId);
        IDailyTillTranKey GetDailyTillTranKey(string sourceTransactionId, string source);
        DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, int sequenceNumber);
        bool IsTransactionToCancelForOrder(string refundedTransactionId, string source);
        DailyTillLine GetOriginalLineNumber(IDailyTillTranKey tran, string sourceItemId);
        RelatedItem SelectRelatedItem(string packageItemNumber);
    }
}
