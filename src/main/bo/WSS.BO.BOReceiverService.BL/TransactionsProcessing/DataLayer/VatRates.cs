﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class VatRates : IEnumerable<VatRate>
    {
        private readonly IList<VatRate> items = new List<VatRate>();

        private const string Symbols = "abcdefghi";

        public VatRates(IList<decimal> orderedValues)
        {
            for (int i = 0; i < orderedValues.Count; i++)
            {
                items.Add(new VatRate(i + 1, Symbols[i], orderedValues[i]));
            }
        }

        public VatRates(Dltots dltots)
        {
            items.Add(new VatRate(1, dltots.VatSymbol1[0], dltots.VatRate1));
            items.Add(new VatRate(2, dltots.VatSymbol2[0], dltots.VatRate2));
            items.Add(new VatRate(3, dltots.VatSymbol3[0], dltots.VatRate3));
            items.Add(new VatRate(4, dltots.VatSymbol4[0], dltots.VatRate4));
            items.Add(new VatRate(5, dltots.VatSymbol5[0], dltots.VatRate5));
            items.Add(new VatRate(6, dltots.VatSymbol6[0], dltots.VatRate6));
            items.Add(new VatRate(7, dltots.VatSymbol7[0], dltots.VatRate7));
            items.Add(new VatRate(8, dltots.VatSymbol8[0], dltots.VatRate8));
            items.Add(new VatRate(9, dltots.VatSymbol9[0], dltots.VatRate9));
        }

        public VatRate GetByRateValue(decimal rateValue)
        {
            var found = items.FirstOrDefault(vr => vr.Value == rateValue);
            if (found == null)
            {
                throw new ArgumentException(String.Format("Unexpected rate = [{0}]", rateValue));
            }
            return found;
        }

        public decimal GetRateBySymbol(char symbol)
        {
            var found = items.First(vr => vr.Symbol == symbol);
            if (found == null)
            {
                throw new ArgumentException(String.Format("Unexpected symbol for vat = [{0}]", symbol));
            }
            return found.Value;
        }

        public IEnumerator<VatRate> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
