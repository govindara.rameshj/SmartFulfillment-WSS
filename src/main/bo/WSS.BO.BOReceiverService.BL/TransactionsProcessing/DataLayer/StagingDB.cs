﻿using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class StagingDb
    {
        public StagingDb()
        {
            MainData = new TransactionStagingData();
        }

        public TransactionStagingData MainData { get; set; }

        public void Save(IRepositoriesFactory dlFactory)
        {
            MainData.Save(dlFactory);
        }
    }
}
