﻿using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public interface IReadOnlyDataRetriever
    {
        VatRates GetVatRates();
        SkuInfo GetSkuInfo(string skuNumber);
        string GetDefaultSystemCurrency();
        int GetTradingPeriodId(DateTime createDateTime);
        string GetStoreId();
        int GetAbsoluteStoreId();
        IDictionary<string, Syscod> GetSystemCodes(string systemCodeType);
        IDictionary<string, SystemCode> GetNewSystemCodes(string systemCodeType);
        int? GetVirtualCashierId(int tillNumber);
        IEnumerable<DeliverySlot> GetDeliverySlots();
        string GetGiftCardSkuFromParameters();
        string GetDeliveryChargeSkuFromParameters();
        DeliveryChargeContainer GetDeliveryChargeContainer();
        string GetSettingStringValue(int parameterId);
    }
}
