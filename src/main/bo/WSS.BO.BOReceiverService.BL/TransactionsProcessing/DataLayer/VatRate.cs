﻿namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer
{
    public class VatRate
    {
        public VatRate(int code, char symbol, decimal value)
        {
            Code = code;
            Symbol = symbol;
            Value = value;
        }

        public int Code { get; private set; }
        public char Symbol { get; private set; }
        public decimal Value { get; private set; }
    }
}
