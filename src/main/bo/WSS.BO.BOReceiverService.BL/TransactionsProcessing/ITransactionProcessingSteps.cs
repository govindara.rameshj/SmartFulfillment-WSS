﻿using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    /// <summary>
    /// The purpose of the interface to create injectoin point for AoP - logging and statistics gathering.
    /// </summary>
    public interface ITransactionProcessingSteps
    {
        void SetTransaction(Transaction tran);
        Transaction GetTransaction();

        void CheckForDublicates();
        void CheckForLocksBeforeProcessing();
        void CreateContext();
        void PreProcess();
        void Validate();
        void EmulateLegacyTransactionSplitting();
        void Convert();
        void ApplyBusinessRules();
        void CheckForLocksBeforeSavingToDb();
        void SaveToDb();
    }
}
