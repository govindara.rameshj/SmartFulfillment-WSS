﻿using System;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using slf4net;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Products;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.OrderCancellation
{
    public class OrderCancellationConverter
    {
        protected ITransactionProcessingContext Ctx { get; private set; }
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(OrderCancellationConverter));

        public OrderCancellationConverter(ITransactionProcessingContext ctx)
        {
            Ctx = ctx;
        }

        public void ProcessOrderCancellationForItem(RetailTransactionItem item, Fulfillment cancellationFulfillment)
        {
            var itemIndex = item.ItemIndex;
            var cancellationItem = cancellationFulfillment.Items.FirstOrDefault(x => x.TransactionItemIndex == itemIndex);

            var orderNumber = Ctx.RuntimeDataRetriever.GetOrderNumberByOriginalTransactionId(item.ItemRefund.RefundedTransactionId, Ctx.GetTransaction().Source);
            Ctx.StagingDb.MainData.Dltots.OrderNumber = orderNumber;

            var qtyRefunded = ((SkuProduct)item.Product).AbsQuantity;
            var qtyCancelled = cancellationItem == null ? 0 : cancellationItem.AbsQuantity;
            var qtyReturned = qtyRefunded - qtyCancelled;

            if (!OrderIsDelivered(orderNumber))
            {
                var itemRefundDailyLine = GetRefundedItemDailyTillLine(item);
                if (itemRefundDailyLine == null)
                {
                    Log.Warn(
                        string.Format(
                            "Daily Line with Source Item Id = {0} for Transaction with Source Id = {1} doesn't exist.\nRefunded Item will be processed as Refunded without Receipt",
                            item.ItemRefund.RefundedTransactionItemId,
                            item.ItemRefund.RefundedTransactionId));
                }
                else
                {
                    ProcessCorrefund(orderNumber, qtyCancelled, qtyReturned, itemRefundDailyLine.SequenceNumber);
                    ProcessCorlinUpdate(qtyCancelled, qtyRefunded, orderNumber, qtyReturned, itemRefundDailyLine.SequenceNumber);
                }
                ProcessCorhdrUpdate(orderNumber, qtyRefunded, qtyReturned);
                ProcessCorhdr4Update(orderNumber);
            }
        }

        private bool OrderIsDelivered(string orderNumber)
        {
            return Ctx.RuntimeDataRetriever.CheckOrderIsDelivered(orderNumber);
        }

        private void ProcessCorhdr4Update(string originalOrderNumber)
        {
            if (Ctx.StagingDb.MainData.Corhdr4UpdatesForOrderCancellation.Count(x => x.OrderNumber == originalOrderNumber) == 0)
            {
                var corhdr4 = new CustomerOrderHeader4
                {
                    OrderNumber = originalOrderNumber,
                    RefundStatus = OrderConsignmentRefundStatus.Created
                };
                Ctx.StagingDb.MainData.Corhdr4UpdatesForOrderCancellation.Add(corhdr4);
            }
        }

        private void ProcessCorhdrUpdate(string orderNumber, int qtyRefunded, int qtyReturned)
        {
            var corhdr = Ctx.StagingDb.MainData.CorhdrUpdatesForOrderCancellation.FirstOrDefault(x => x.OrderNumber == orderNumber);
            //We create new update object only if we don't have any before. That way we can execute only one query to the database.
            if (corhdr == null)
            {
                var newCorhdr = new CustomerOrderHeader
                {
                    OrderNumber = orderNumber,
                    RefundDate = Ctx.GetTransaction().CreateDateTime,
                    RefundTillNumber = ConversionUtils.GetCanonicalTillNumber(Ctx.GetTransaction().TillNumber),
                    TotalUnitsRefundedNumber = -qtyRefunded,
                    TotalUnitsTakenNumber = -qtyReturned,
                    RevisionNumber = 1
                };
                Ctx.StagingDb.MainData.CorhdrUpdatesForOrderCancellation.Add(newCorhdr);
            }
            else
            {
                corhdr.TotalUnitsRefundedNumber -= qtyRefunded;
                corhdr.TotalUnitsTakenNumber -= qtyReturned;
            }
        }

        private void ProcessCorlinUpdate(int qtyCancelled, int qtyRefunded, string originalOrderNumber, int qtyReturned, int itemRefundSequenceNumber)
        {
            var updateCorlin = new CustomerOrderLine
            {
                QtyToBeDelivered = -qtyCancelled, // Should be with => 0 in db
                RefundQuantity = -qtyRefunded, // Should be with =< 0 in db
                LineNumber = Ctx.RuntimeDataRetriever.GetCorlinLineNumberBySourceLineNumber(itemRefundSequenceNumber, originalOrderNumber),
                OrderNumber = originalOrderNumber,
                QuantityTaken = -qtyReturned
            };
            Ctx.StagingDb.MainData.CorlinUpdatesForOrderCancellation.Add(updateCorlin);
        }

        private void ProcessCorrefund(string originalOrderNumber, int qtyCancelled, int qtyReturned, int itemRefundSequenceNumber)
        {
            var correfund = new CustomerOrderRefund
            {
                OrderNumber = originalOrderNumber,
                LineNumber = Ctx.RuntimeDataRetriever.GetCorlinLineNumberBySourceLineNumber(itemRefundSequenceNumber, originalOrderNumber),
                RefundDate = Ctx.GetTransaction().CreateDateTime.Date,
                RefundTill = ConversionUtils.GetCanonicalTillNumber(Ctx.GetTransaction().TillNumber),
                RefundStoreId = Ctx.ReadOnlyDataRetriever.GetAbsoluteStoreId(),
                QtyCancelled = qtyCancelled,
                QtyReturned = qtyReturned,
                RefundStatus = OrderConsignmentRefundStatus.Created
            };

            Ctx.StagingDb.MainData.Correfunds.Add(correfund);
        }

        private DailyTillLine GetRefundedItemDailyTillLine(RetailTransactionItem item)
        {
            var dailyTillTranKey = Ctx.GetDailyTillTranKeyBySource(item.ItemRefund.RefundedTransactionId, Ctx.GetTransaction().Source);
            var itemRefundDailyLine = Ctx.RuntimeDataRetriever.SelectDailyLineBySourceItemId(dailyTillTranKey, item.ItemRefund.RefundedTransactionItemId);

            if (itemRefundDailyLine == null)
            {
                int sequenceNumber;
                if (Int32.TryParse(item.ItemRefund.RefundedTransactionItemId, out sequenceNumber))
                {
                    itemRefundDailyLine = Ctx.RuntimeDataRetriever.SelectDailyTillLine(dailyTillTranKey, sequenceNumber);
                }
            }

            return itemRefundDailyLine;
        }
    }
}
