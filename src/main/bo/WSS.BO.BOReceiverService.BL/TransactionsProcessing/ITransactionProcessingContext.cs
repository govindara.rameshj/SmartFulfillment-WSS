﻿using System;
using System.Collections.Generic;
using Cts.Oasys.Core.Net4Replacements;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    public interface ITransactionProcessingContext
    {
        Transaction GetTransaction();
        DateTime ReceivedWhen { get; }
        DateTime EffectiveProcessingDate { get; }
        IReadOnlyDataRetriever ReadOnlyDataRetriever { get; }
        IRuntimeDataRetriever RuntimeDataRetriever { get; }
        StagingDb StagingDb { get; }
        IList<GiftCardOperationDescriptor> GiftCardOperationDescriptors { get; set; }
        IConfiguration Config { get; }
        bool IsGeneratedByGiftCardPreprocessing { get; }
        Dictionary<Tuple<string,string>, IDailyTillTranKey> CachedDailyTillTranKeys { get; }
        IDailyTillTranKey GetDailyTillTranKeyBySource(string sourceTranId, string source);
        IDailyTillTranKey GetLegacyTransactionDailyTillTranKey(string sourceTranId);
    }

    public interface ITransactionProcessingContext<out T> : ITransactionProcessingContext
        where T : Transaction
    {
        T Transaction { get; }
    }
}
