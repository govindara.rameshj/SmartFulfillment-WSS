﻿using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    public interface ITransactionProcessor
    {
        void ProcessTransaction(Transaction transaction);
        RetailTransaction FindTransaction(string receiptBarcode, string storeNumber);
    }
}
