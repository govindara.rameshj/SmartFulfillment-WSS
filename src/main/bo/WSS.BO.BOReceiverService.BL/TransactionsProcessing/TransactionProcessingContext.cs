using System;
using System.Collections.Generic;
using Cts.Oasys.Core.Net4Replacements;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    abstract class TransactionProcessingContext : ITransactionProcessingContext
    {
        public abstract Transaction GetTransaction();
        public DateTime ReceivedWhen { get; set; }
        public DateTime EffectiveProcessingDate { get; set; }
        public IReadOnlyDataRetriever ReadOnlyDataRetriever { get; set; }
        public IRuntimeDataRetriever RuntimeDataRetriever { get; set; }
        public StagingDb StagingDb { get; set; }
        public IConfiguration Config { get; set; }
        public IList<GiftCardOperationDescriptor> GiftCardOperationDescriptors { get; set; }
        public bool IsGeneratedByGiftCardPreprocessing { get; set; }
        public Dictionary<Tuple<string, string>, IDailyTillTranKey> CachedDailyTillTranKeys { get; set; }
        public abstract IDailyTillTranKey GetDailyTillTranKeyBySource(string sourceTranId, string source);
        public abstract IDailyTillTranKey GetLegacyTransactionDailyTillTranKey(string sourceTranId);
    }

    class TransactionProcessingContext<T> : TransactionProcessingContext, ITransactionProcessingContext<T>
        where T : Transaction
    {
        public T Transaction { get; set; }
        private readonly object _lockObject = new object();

        public override Transaction GetTransaction()
        {
            return Transaction;
        }

        public override IDailyTillTranKey GetDailyTillTranKeyBySource(string sourceTranId, string source)
        {
            lock (_lockObject)
            {
                IDailyTillTranKey result;
                var key = new Tuple<string, string>(sourceTranId, source);
                if (CachedDailyTillTranKeys.TryGetValue(key, out result))
                {
                    return result;
                }

                result = RuntimeDataRetriever.GetDailyTillTranKey(sourceTranId, source);
                CachedDailyTillTranKeys.Add(key, result);
                return result;
            }
        }

        public override IDailyTillTranKey GetLegacyTransactionDailyTillTranKey(string sourceTranId)
        {
            return LegacyReceiptBarcodeHelper.ParseTransactionKey(sourceTranId, Global.LegacySourcePrefixLength);
        }
    }
}
