﻿using System;
using System.Diagnostics.CodeAnalysis;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    public class ProductConvertersFactory
    {
        private readonly ITransactionProcessingContext<RetailTransaction> ctx;

        public ProductConvertersFactory(ITransactionProcessingContext<RetailTransaction> ctx)
        {
            this.ctx = ctx;
        }

        [SuppressMessage("ReSharper", "CanBeReplacedWithTryCastAndCheckForNull")]
        public IProductConverter CreateProductConverter(Product product)
        {
            IProductConverter result;

            if (product is SkuProduct)
            {
                result = new SkuProductConverter((SkuProduct)product, ctx);
            }
            else if (product is GiftCardProduct)
            {
                result = new GiftCardProductConverter((GiftCardProduct) product, ctx);
            }
            else if (product is DeliveryProduct)
            {
                result = new DeliveryProductConverter((DeliveryProduct) product, ctx);
            }
            else
            {
                throw new ArgumentException(String.Format("Unknown product type : {0}", product.GetType()));
            }

            return result;
        }

    }
}
