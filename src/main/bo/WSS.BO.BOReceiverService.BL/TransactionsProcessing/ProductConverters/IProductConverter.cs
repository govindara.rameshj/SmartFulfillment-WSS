﻿using WSS.BO.BOReceiverService.BL.PreProcessing;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    public interface IProductConverter
    {
        void Validate(ValidationContext<RetailTransactionItem> parentCtx);
        void PreProcess(PreProcessingContext<RetailTransactionItem> preProcessingContext);
        void Convert(RetailTransactionItem item, DailyTillLine dlline);
        string GetSku();
        bool IsScanned();
        void FillCustomerOrderLine(CustomerOrderLine corlin, RetailTransactionItem transactionItem, int corlinLineNumber);
    }
}
