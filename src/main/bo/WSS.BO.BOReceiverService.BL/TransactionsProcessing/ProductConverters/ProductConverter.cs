﻿using slf4net;
using WSS.BO.BOReceiverService.BL.PreProcessing;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    public abstract class ProductConverter<T> : IProductConverter
        where T: Product
    {
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ProductConverter<T>));
        protected string ConverterName;

        protected T Product { get; private set; }
        protected ITransactionProcessingContext<RetailTransaction> Ctx { get; private set; }

        protected ProductConverter(T product, ITransactionProcessingContext<RetailTransaction> ctx)
        {
            Product = product;
            Ctx = ctx;
            ConverterName = GetType().Name;
        }

        public void Convert(RetailTransactionItem item, DailyTillLine dlline)
        {
            Log.Info("{0}.Convert is called", ConverterName);
            InnerConvert(item, dlline);
            Log.Info("{0}.Convert is completed", ConverterName);
        }

        public void Validate(ValidationContext<RetailTransactionItem> parentCtx)
        {
            Log.Info("{0}.Validate is called", ConverterName);
            var validationCtx = parentCtx.CreateChildContext<T, Product>(x => x.Product);
            InnerValidate(parentCtx, validationCtx);
            Log.Info("{0}.Validate is completed", ConverterName);
        }

        public void PreProcess(PreProcessingContext<RetailTransactionItem> preProcessingContext)
        {
            Log.Info("{0}.PreProcess is called", ConverterName);
            InnerPreProcess(preProcessingContext);
            Log.Info("{0}.PreProcess is completed", ConverterName);
        }

        public abstract string GetSku();

        public abstract bool IsScanned();

        public virtual void FillCustomerOrderLine(CustomerOrderLine corlin, RetailTransactionItem transactionItem, int corlinLineNumber)
        {
            var quantityToBeTaken = Ctx.Transaction.GetInstantFulfullmentQuantity(transactionItem.ItemIndex);
            corlin.LineNumber = corlinLineNumber.ToString("D4");
            corlin.QuantityTaken = quantityToBeTaken;

            corlin.RefundQuantity = 0;
            corlin.SellingStoreId = Ctx.ReadOnlyDataRetriever.GetAbsoluteStoreId();
            corlin.RequiredFulfiller = Ctx.Transaction.GetRequiredFulfuller(transactionItem.ItemIndex);
        }

        protected virtual void InnerValidateTotalLineFulfilledQuantity(ValidationContext<T> validationContext, int totalFulfilledQuantity)
        {
        }

        protected virtual void InnerValidate(ValidationContext<RetailTransactionItem> parentCtx, ValidationContext<T> validationContext)
        {
        }

        protected virtual void InnerPreProcess(PreProcessingContext<RetailTransactionItem> preProcessingContext)
        {
        }

        protected virtual void InnerConvert(RetailTransactionItem item, DailyTillLine dlline)
        {
            dlline.SkuNumber = GetSku();
            dlline.IsScanned = IsScanned();
            dlline.SourceItemId = item.SourceItemId;
        }
    }
}
