﻿using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    public class DeliveryProductConverter : ProductConverter<DeliveryProduct>
    {
        public DeliveryProductConverter(DeliveryProduct product, ITransactionProcessingContext<RetailTransaction> ctx)
            :base(product, ctx)
        {
        }

        protected override void InnerConvert(RetailTransactionItem item, DailyTillLine dlline)
        {
            base.InnerConvert(item, dlline);
            dlline.Amount = Product.AbsAmount;
            dlline.Price = Product.AbsAmount;
            dlline.Quantity = 1;
            dlline.SupervisorNumber = Global.NullUserCode;
        }

        protected override void InnerValidate(ValidationContext<RetailTransactionItem> parentCtx, ValidationContext<DeliveryProduct> validationContext)
        {
            base.InnerValidate(parentCtx, validationContext);

            validationContext.IsNotNegative(x => x.AbsAmount);
        }

        public override string GetSku()
        {
            return Ctx.ReadOnlyDataRetriever.GetDeliveryChargeContainer().GetDeliveryChargeSku(Product.AbsAmount);
        }

        public override bool IsScanned()
        {
            return false;
        }

        public override void FillCustomerOrderLine(CustomerOrderLine corlin, RetailTransactionItem transactionItem, int corlinLineNumber)
        {
            base.FillCustomerOrderLine(corlin, transactionItem, corlinLineNumber);
            var sku = GetSku();
            corlin.SkuNumber = sku;
            corlin.QtyToBeDelivered = 0;
            corlin.Quantity = 1; // as was decided, to get full DeliveryCharge amount we change the 805000 price, not the quantity  
            corlin.DeliveryStatus = OrderConsignmentStatus.DeliveredStatusOk;
            corlin.IsDeliveryChargeItem = Ctx.ReadOnlyDataRetriever.GetSkuInfo(sku).SaleTypeAttribute == SkuSaleTypeAttribute.DeliveryItem;
            corlin.Price = Product.AbsAmount;
        }
    }
}
