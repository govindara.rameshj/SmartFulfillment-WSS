using System;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.PreProcessing;
using EventType = WSS.BO.Model.Enums.EventType;
using PriceOverrideReasonCode = WSS.BO.Model.Enums.PriceOverrideReasonCode;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    class SkuProductConverter : ProductConverter<SkuProduct>
    {
        public SkuProductConverter(SkuProduct product, ITransactionProcessingContext<RetailTransaction> ctx)
            :base(product, ctx)
        {
        }

        protected override void InnerValidate(ValidationContext<RetailTransactionItem> parentCtx, ValidationContext<SkuProduct> validationContext)
        {
            base.InnerValidate(parentCtx, validationContext);

            validationContext.IsNotNull(x => x.BrandProductId);
            if (Product.BrandProductId != null)
            {
                validationContext.CheckPredicate(x => x.BrandProductId, sku => Ctx.ReadOnlyDataRetriever.GetSkuInfo(sku) != null, "SKU not found in DB");
            }

            validationContext.IsNotNegative(x => x.AbsQuantity);

            if (Product.Discounts != null)
            {
                foreach (int index in Product.Discounts.EnumerateIndexes())
                {
                    var discount = Product.Discounts[index];

                    if (discount is AmountDiscount)
                    {
                        ValidateAmountDiscount(validationContext.CreateChildContext(x => x.Discounts, index), discount);
                    }
                }
            }

            if (parentCtx.Entity.Direction == Direction.TO_CUSTOMER)
            {
                int totalFulfilledQuantity = Ctx.Transaction.Fulfillments
                    .SelectMany(x => x.Items)
                    .Where(x => x.TransactionItemIndex == parentCtx.Entity.ItemIndex)
                    .Sum(x => x.AbsQuantity);

                validationContext.AreEqual(x => x.AbsQuantity, totalFulfilledQuantity,
                    "For transaction item with Direction=TO_CUSTOMER SkuProduct quantity should be equal to the total of related fulfilment items quantity.");
            }
        }

        private void ValidateAmountDiscount(ValidationContext<Discount> discountContext, Discount discount)
        {
            discountContext.AreNotEqual(x => ((AmountDiscount)x).SupervisorNumber, Global.NullUserCode, "Empty supervisor should be equal null");

            if (discount is PriceMatchDiscount)
            {
                discountContext.IsNotEmpty(x => ((PriceMatchDiscount)x).CompetitorName, "Competitor Name for Price Match Promise override event should be set", true);
                discountContext.HasLengthLessOrEqual(x => ((PriceMatchDiscount)x).CompetitorName, LengthLimitations.DlolinCompetitorName, 
                    String.Format("Competitor name length is longer than {0}", LengthLimitations.DlolinCompetitorName));
            }
        }

        protected override void InnerPreProcess(PreProcessingContext<RetailTransactionItem> preProcessingContext)
        {
            base.InnerPreProcess(preProcessingContext);

            var productCtx = preProcessingContext.CreateChildContext<SkuProduct, Product>(x => x.Product);
            productCtx.NullListToEmpty(x => x.Discounts);

            if (Product.Discounts.Any(x => x is PriceMatchDiscount))
            {
                foreach (int index in Product.Discounts.EnumerateIndexes())
                {
                    if (Product.Discounts[index] is PriceMatchDiscount)
                    {
                        var discountCtx = productCtx.CreateChildContext(x => x.Discounts, index);
                        discountCtx.CheckAndTruncate(x => ((PriceMatchDiscount) x).CompetitorName, LengthLimitations.DlolinCompetitorName);
                    }

                    if (Product.Discounts[index] is EventDiscount)
                    {
                        var discountCtx = productCtx.CreateChildContext(x => x.Discounts, index);
                        discountCtx.NullListToEmpty(x => ((EventDiscount) x).HitAmounts);
                    }
                }
            }
        }

        public override string GetSku()
        {
            return Product.BrandProductId;
        }

        public override bool IsScanned()
        {
            return Product.ProductInputType == ProductInputType.SCANNED;
        }

        public override void FillCustomerOrderLine(CustomerOrderLine corlin, RetailTransactionItem transactionItem, int corlinLineNumber)
        {
            base.FillCustomerOrderLine(corlin, transactionItem, corlinLineNumber);

            corlin.SkuNumber = Product.BrandProductId;
            corlin.IsDeliveryChargeItem = Ctx.ReadOnlyDataRetriever.GetSkuInfo(Product.BrandProductId).SaleTypeAttribute == SkuSaleTypeAttribute.DeliveryItem;
            corlin.Price = Math.Round((Ctx.Transaction.CalculateExtpAmount(Product) - Product.CalculateEventsDiscount()) / Product.AbsQuantity, 2);
            corlin.Quantity = transactionItem.Direction == Direction.TO_CUSTOMER ? Product.AbsQuantity : -Product.AbsQuantity;
            corlin.QuantityTaken = transactionItem.Direction == Direction.TO_CUSTOMER ? corlin.QuantityTaken : corlin.Quantity;

            int quantityToBeDelivered = (int) corlin.Quantity - (int) corlin.QuantityTaken;
            corlin.QtyToBeDelivered = quantityToBeDelivered;
            corlin.PriceOverrideCode = Int32.Parse(Product.GetPriceOverrideCode());
            corlin.DeliveryStatus = quantityToBeDelivered == 0 ?
                OrderConsignmentStatus.DeliveredStatusOk : OrderConsignmentStatus.DeliveryRequest;
        }

        protected override void InnerConvert(RetailTransactionItem item, DailyTillLine dlline)
        {
            base.InnerConvert(item, dlline);
            dlline.IsFromMarkDown = Product.StockType == StockType.MARKDOWN;
            dlline.SupervisorNumber = GetSupervisorNumber(Product);
            var stkmasPrice = Ctx.RuntimeDataRetriever.GetStockMasterPrice(dlline.SkuNumber);
            dlline.InitialPrice = stkmasPrice;
            dlline.Quantity = item.Direction == Direction.TO_CUSTOMER ? Product.AbsQuantity : -Product.AbsQuantity;
            var extpAmount = Ctx.Transaction.CalculateExtpAmount(Product); //Correct calculation
            dlline.Amount = item.Direction == Direction.TO_CUSTOMER ? extpAmount : -extpAmount;
            dlline.IsExplosive = Ctx.Transaction.RestrictedItems.Any(x => x.BrandProductId == Product.BrandProductId &&
                                                            x.RestrictionType == RestrictionType.EXPLOSIVES_POISONS);
            dlline.PartitialQuarantine = GetQuarantineSupervisorNumber();
            ProcessDiscounts(dlline, item, stkmasPrice);
            dlline.Price = Ctx.Transaction.CalculatePrice(Product); //Correct calculation
        }

        private string GetQuarantineSupervisorNumber()
        {
            var lastAuthorizedQuarantineItem =
                Ctx.Transaction.RestrictedItems.LastOrDefault(x => x.BrandProductId == Product.BrandProductId
                                                                   && x.RestrictionType == RestrictionType.QUARANTINE &&
                                                                   !string.IsNullOrEmpty(x.AuthorizerNumber));
            return lastAuthorizedQuarantineItem == null
                ? Global.NullUserCode
                : lastAuthorizedQuarantineItem.AuthorizerNumber;
        }

        private string GetSupervisorNumber(SkuProduct product)
        {
            if (Ctx.Transaction.RestrictedItems.Any(x => x.RestrictionType != RestrictionType.QUARANTINE && x.BrandProductId == product.BrandProductId && !string.IsNullOrEmpty(x.AuthorizerNumber)))
            {
                return Ctx.Transaction.RestrictedItems.First(x => x.BrandProductId == product.BrandProductId).AuthorizerNumber;
            }
            else if (product.Discounts != null && (product.Discounts.Any(x => x is PriceOverrideDiscount) || product.Discounts.Any(x => x is PriceMatchDiscount)))
            {
                return GetPriceOverrideSupervisorNumber(product);
            }
            else
            {
                return Global.NullUserCode;
            }
        }

        private string GetPriceOverrideSupervisorNumber(SkuProduct product)
        {
            if (product.Discounts.Any(x => x is PriceMatchDiscount) && product.Discounts.OfType<PriceMatchDiscount>().Any(x => !string.IsNullOrEmpty(x.SupervisorNumber)))
            {
                return product.Discounts.OfType<PriceMatchDiscount>().Last(x => !string.IsNullOrEmpty(x.SupervisorNumber)).SupervisorNumber;
            }
            else if (product.Discounts.Any(x => x is PriceOverrideDiscount) && product.Discounts.OfType<PriceOverrideDiscount>().Any(x => x.SupervisorNumber != null))
            {
                return product.Discounts.OfType<PriceOverrideDiscount>().Last(x => !string.IsNullOrEmpty(x.SupervisorNumber)).SupervisorNumber;
            }
            else
            {
                return Global.NullUserCode;
            }
        }

        private void ProcessDiscounts(DailyTillLine dlline, RetailTransactionItem item, decimal stkmasPrice)
        {
            if (Product.Discounts != null)
            {
                PreProcessSkuProductDiscounts();
                if (Product.Discounts.Any(x => x is EventDiscount))
                {
                    ProcessEvents(dlline);
                }
                if (Product.Discounts.Any(x => x is EmployeeDiscount))
                {
                    dlline.EmployeeDiscountAmount = Math.Round(Product.Discounts.OfType<EmployeeDiscount>().Sum(x => x.SavingAmount) / Product.AbsQuantity, 2);
                }
            }
            ProcessPriceOverrideDiscounts(dlline, item, stkmasPrice);
        }

        private void PreProcessSkuProductDiscounts()
        {
            var amountDiscounts = Product.Discounts.OfType<AmountDiscount>();
            var discountsReminder = ProcessAmountDiscounts(amountDiscounts, Product.AbsQuantity);
            if (discountsReminder != 0)
            {
                Product.Discounts.Add(
                    new EventDiscount
                    {
                        EventDescriptor = new EventDescriptor { EventType = EventType.HIERARCHY_SPEND },
                        HitAmounts = new List<decimal> { discountsReminder }
                    });
            }
        }

        private decimal ProcessAmountDiscounts(IEnumerable<AmountDiscount> amountDiscounts, decimal absQuantity)
        {
            Func<decimal, decimal> getReducedSingleItemDiscount = x => (Math.Truncate(x / absQuantity * 100) / 100);
            Func<decimal, decimal> getRoundedSingleItemDiscount = x => Math.Round(x / absQuantity, 2);

            var discountsIndivisibleRemainder = amountDiscounts.Select(x => x.SavingAmount - getReducedSingleItemDiscount(x.SavingAmount) * absQuantity).Sum();

            foreach (var amountDiscount in amountDiscounts)
            {
                if (getReducedSingleItemDiscount(amountDiscount.SavingAmount) * absQuantity < amountDiscount.SavingAmount)
                {
                    decimal lackingAmount = (getRoundedSingleItemDiscount(amountDiscount.SavingAmount) - getReducedSingleItemDiscount(amountDiscount.SavingAmount)) * absQuantity;

                    if (discountsIndivisibleRemainder >= lackingAmount)
                    {
                        amountDiscount.SavingAmount = getRoundedSingleItemDiscount(amountDiscount.SavingAmount) * absQuantity;
                        discountsIndivisibleRemainder -= lackingAmount;
                    }
                    else
                    {
                        amountDiscount.SavingAmount = getReducedSingleItemDiscount(amountDiscount.SavingAmount) * absQuantity;
                    }
                }
            }

            return discountsIndivisibleRemainder;
        }

        private void ProcessEvents(DailyTillLine dlline)
        {
            if (Product.Discounts != null && Product.Discounts.Any(x => x is EventDiscount))
            {
                var eventDiscounts = Product.Discounts.OfType<EventDiscount>().ToList();

            dlline.DealGroupChangePriceDifference = Product.CalculateDealGroupChangePriceDifference();
            dlline.DealGroupMarginErosionCode = GetEventCodeOrDefault(eventDiscounts, EventType.DEAL_GROUP);
            dlline.MultibuyChangePriceDifference = Product.CalculateMultibuyChangePriceDifference();
            dlline.MultibuyMarginErosionCode = GetEventCodeOrDefault(eventDiscounts, EventType.MULTIBUY);
            dlline.HierachyChangePriceDifference = Product.CalculateHierachyChangePriceDifference();
            dlline.HiearchyMarginErosionCode = GetEventCodeFromDataBaseOrDefault(eventDiscounts, EventType.HIERARCHY_SPEND, ParameterId.HierarchySpendEventDiscountCode);
            dlline.QuantityBreakChangePriceDifference = Product.CalculateQuantityBreakChangePriceDifference();
            dlline.QuantityBreakMarginErosionCode = GetEventCodeFromDataBaseOrDefault(eventDiscounts, EventType.QUANTITY_BREAK, ParameterId.QuantityBreakEventDiscountCode);
            ProcessDlevent(dlline);
            }
        }

        private void ProcessDlevent(DailyTillLine dlline)
        {
            var discountsEvents = Product.Discounts.OfType<EventDiscount>().Select(x => new { x.HitAmounts, Type = x.EventDescriptor.EventType });
            short dlevntSequenceNumber = 1;
            foreach (var discountEvent in discountsEvents)
            {
                foreach (var hit in discountEvent.HitAmounts)
                {
                    var dlevent = new Dlevnt
                    {
                        CreateDate = dlline.CreateDate,
                        TillNumber = dlline.TillNumber,
                        DiscountAmount = hit,
                        ErosionType = ModelMapper.GetDbModelCode(discountEvent.Type),
                        TransactionLineNumber = (short) dlline.SequenceNumber,
                        EventSequenceNumber = dlevntSequenceNumber.ToString().PadLeft(6, '0')
                    };

                    Ctx.StagingDb.MainData.Dlevnts.Add(dlevent);

                    dlevntSequenceNumber++;
                }
            }
        }

        private void ProcessPriceOverrideDiscounts(DailyTillLine dlline, RetailTransactionItem item, decimal stkmasPrice)
        {
            bool isPriceOverrideNeeded = Product.InitialPrice - stkmasPrice != Product.CalculatePriceOverrideChangePriceDifference();
            decimal priceErrosion = stkmasPrice - Product.InitialPrice;

            dlline.PriceOverrideChangePriceDifference = isPriceOverrideNeeded ? Product.CalculatePriceOverrideChangePriceDifference() + priceErrosion : 0;
            dlline.PriceOverrideCode = isPriceOverrideNeeded ? System.Convert.ToInt16(GetProductPriceOverrideCode(priceErrosion, item)) : System.Convert.ToInt16(ModelMapper.GetSafeDbModelCode(null));
            dlline.PriceOverrideMarginErosionCode = isPriceOverrideNeeded && Product.StockType == StockType.NORMAL? 
                ConversionUtils.ToEventCode(GetProductPriceOverrideCode(priceErrosion, item)) : ConversionUtils.ToEventCode(Global.NullEventCode);
        }    

        private static string GetEventCodeOrDefault(List<EventDiscount> discounts, EventType discountType)
        {
            return (discounts.Any(x => x.EventDescriptor.EventType == discountType)) ? 
                discounts.Last(x => x.EventDescriptor.EventType == discountType).EventDescriptor.EventCode : Global.NullEventCode;
        }

        private string GetEventCodeFromDataBaseOrDefault(List<EventDiscount> discounts, EventType discountType, int parameterId)
        {
            return (discounts.Any(x => x.EventDescriptor.EventType == discountType)) ? Ctx.ReadOnlyDataRetriever.GetSettingStringValue(parameterId) : Global.NullEventCode;
        }

        private string GetProductPriceOverrideCode(decimal priceErrosion, RetailTransactionItem item)
        {
            if (Product.StockType == StockType.MARKDOWN)
            {
                return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.MarkdownDamages;
            }
            if (Product.Discounts != null && Product.Discounts.Any(x => x is PriceMatchDiscount))
            {
                return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.PriceMatchOrPromise;
            }
            if (Product.Discounts != null && Product.Discounts.Any(x => x is PriceOverrideDiscount))
            {
                return GetPriceOverrideDiscountCode(item);
            }
            if (priceErrosion != 0)
            {
                return GetPriceErosionOverrideCode(item);
            }                

            return Global.NullEventCode;
        }

        private string GetPriceOverrideDiscountCode(RetailTransactionItem item)
        {
            if (item.Direction == Direction.TO_CUSTOMER)
            {
                var lastOverrideDiscount = Product.Discounts.OfType<PriceOverrideDiscount>().Last();

                return ModelMapper.GetDbModelCode(lastOverrideDiscount.PriceOverrideCode);
            }

            return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.RefundPriceDifference;
        }

        private string GetPriceErosionOverrideCode(RetailTransactionItem item)
        {
            if (item.Direction == Direction.TO_CUSTOMER)
            {
                return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.TemporaryDealGroup;
            }

            return BO.DataLayer.Model.Constants.PriceOverrideReasonCode.RefundPriceDifference;
        }
    }
}
