﻿using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using PriceOverrideReasonCode = WSS.BO.DataLayer.Model.Constants.PriceOverrideReasonCode;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters
{
    class GiftCardProductConverter : ProductConverter<GiftCardProduct>
    {
        public GiftCardProductConverter(GiftCardProduct product, ITransactionProcessingContext<RetailTransaction> ctx)
            :base(product, ctx)
        {
        }

        protected override void InnerValidate(ValidationContext<RetailTransactionItem> parentCtx, ValidationContext<GiftCardProduct> validationContext)
        {
            base.InnerValidate(parentCtx, validationContext);
            parentCtx.AreEqual(x => x.Direction, Direction.TO_CUSTOMER, "Refund of Gift Card is not possible");

            if (parentCtx.Entity.IsReversed)
            {
                validationContext.IsNull(x => x.GiftCardOperation, "GifcCard operation info is not supported for reversed line");
            }
            else
            {
                validationContext.IsNotNull(x => x.GiftCardOperation, "GiftCard operation should be set for non reversed line");
            }

            validationContext.IsNotNegative(x => x.AbsAmount);
        }

        public override string GetSku()
        {
            return Ctx.ReadOnlyDataRetriever.GetGiftCardSkuFromParameters();
        }

        public override bool IsScanned()
        {
            return false; // No scan for GC
        }

        protected override void InnerConvert(RetailTransactionItem item, DailyTillLine dlline)
        {
            base.InnerConvert(item, dlline);

            bool isRefund = Ctx.Transaction.TotalAmount < 0;
            // No backdoor for GC
            dlline.BackDoorCollected = ConversionUtils.BooleanToYorN(false);
            dlline.IsBackDoor = false;

            dlline.IsFromMarkDown = false;

            dlline.SupervisorNumber = Global.NullUserCode;

            // line sku
            dlline.Quantity = isRefund ? -1 : 1; //always 1 for GC

            //line pricing
            dlline.InitialPrice = Product.AbsAmount; // Price for GC is equal to amount
            dlline.PriceOverrideCode = System.Convert.ToInt16(PriceOverrideReasonCode.NoPriceOverride);

            dlline.Amount = isRefund ? -Product.AbsAmount : Product.AbsAmount; // Refund of GC is not possible, so always positive

            dlline.Price = isRefund ? -Product.AbsAmount : Product.AbsAmount;
            dlline.EmployeeDiscountAmount = 0; // no discount for GC
            dlline.VatValue = 0; //no vat for GC
        }
    }
}
