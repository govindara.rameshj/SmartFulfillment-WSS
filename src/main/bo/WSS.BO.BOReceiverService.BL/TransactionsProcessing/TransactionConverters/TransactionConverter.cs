﻿using System;
using System.Collections.Generic;
using Cts.Oasys.Core.Helpers;
using slf4net;
using WSS.BO.BOReceiverService.BL.PreProcessing;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public abstract class TransactionConverter<T> : ITransactionConverter
        where T : Transaction
    {
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(TransactionConverter<T>));
        
        protected string ConverterName;
        protected ITransactionProcessingContext<T> Ctx { get; private set; }

        protected T Transaction
        {
            get { return Ctx.Transaction;  }
        }

        protected TransactionConverter(ITransactionProcessingContext<T> ctx)
        {
            Ctx = ctx;
            ConverterName = GetType().Name;
        }

        public void Convert()
        {
            Log.Info("{0}.Convert is called", ConverterName);
            InnerConvert();
            Log.Info("{0}.Convert is completed", ConverterName);
        }

        public void ProcessCashierBalanceUpdate(CashBalCashier update)
        {
            Log.Info("{0}.ProcessCashierBalanceUpdate is called", ConverterName);
            InnerProcessCashierBalanceUpdate(update);
            Log.Info("{0}.ProcessCashierBalanceUpdate is completed", ConverterName);
        }

        public IList<ValidationError> Validate()
        {
            Log.Info("{0}.Validate is called", ConverterName);
            var validationContext = new ValidationContext<T>(Transaction);
            InnerValidate(validationContext);
            Log.Info("{0}.Validate is completed", ConverterName);
            return validationContext.GetValidationErrors();
        }

        public void PreProcess()
        {
            Log.Info("{0}.PreProcess is called", ConverterName);
            var preProcessingContext = new PreProcessingContext<T>(Transaction, Ctx.Config);
            InnerPreProcess(preProcessingContext);
            Log.Info("{0}.PreProcess is completed", ConverterName);
        }

        protected virtual void InnerValidate(ValidationContext<T> validationContext)
        {
            validationContext.AreEqual(x => x.BrandCode, Global.BrandCode,
                "The service processes only retail transactions for brand [{0}]");
            validationContext.AreEqual(x => x.StoreNumber, Ctx.ReadOnlyDataRetriever.GetAbsoluteStoreId().ToString(),
                "The service processes only retail transactions for store [{0}]");
            validationContext.HasLengthLessOrEqual(x => x.TillNumber, 2,
                "The service processes only retail transactions with Till number length equal to [{0}]");
            validationContext.IsConvertableToInt(x => x.CashierNumber,
                "The service processes only Cashier Numbers that are convertible to integer");
        }

        protected virtual void InnerPreProcess(PreProcessingContext<T> preProcessingContext)
        {
        }

        protected virtual void InnerConvert()
        {
            var dltots = new Dltots();
            ProcessDltots(dltots);
            Ctx.StagingDb.MainData.Dltots = dltots;
        }

        protected virtual void InnerProcessCashierBalanceUpdate(CashBalCashier update)
        {
        }

        protected virtual void ProcessDltots(Dltots dltots)
        {
            dltots.CashierNumber = ConversionUtils.IntValueToCharField(Transaction.CashierNumber, 3);
            dltots.CreateDate = Transaction.CreateDateTime.Date;
            dltots.IsOnline = true;
            dltots.ReceivedDate = Ctx.ReceivedWhen;
            dltots.SourceTranId = Transaction.SourceTransactionId;
            dltots.Source = Transaction.Source;
            dltots.SourceTranNumber = Transaction.TransactionNumber;
            dltots.ReceiptBarcode = Transaction.ReceiptBarcode;
            dltots.TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber);
            dltots.TimeOfTransaction = ConversionUtils.TimeToOasysString(Transaction.CreateDateTime);
            dltots.StoreNumber = String.Empty; //In db its 000 or String.Empty.
            dltots.IsTransactionComplete = true;
            dltots.IsTraining = Transaction.IsTraining;
            if (Transaction.CreateDateTime.Date != Ctx.EffectiveProcessingDate.Date)
                dltots.WasCbbupdUpdated = Ctx.EffectiveProcessingDate.ToString("dd/MM/yy");
        }
    }
}
