﻿using System;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public abstract class VoidableTransactionConverter<T> : TransactionConverter<T>
        where T : VoidableTransaction
    {
        protected VoidableTransactionConverter(ITransactionProcessingContext<T> ctx)
            : base(ctx)
        {
        }

        protected override void InnerValidate(ValidationContext<T> validationContext)
        {
            base.InnerValidate(validationContext);

            validationContext.AreNotEqual(x => x.AuthorizerNumber, Global.NullUserCode, "Empty authorizer number should be equal null");
            validationContext.AreNotEqual(x => x.VoidSupervisor, Global.NullUserCode, "Empty void supervisor should be equal null");
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);

            dltots.AuthorizerNumber = String.IsNullOrEmpty(Transaction.AuthorizerNumber) ? Global.NullUserCode : Transaction.AuthorizerNumber;
            dltots.IsVoid = Transaction.TransactionStatus != TransactionStatus.COMPLETED;
            dltots.VoidSupervisor = String.IsNullOrEmpty(Transaction.VoidSupervisor) ? Global.NullUserCode : Transaction.VoidSupervisor;
        }
    }
}
