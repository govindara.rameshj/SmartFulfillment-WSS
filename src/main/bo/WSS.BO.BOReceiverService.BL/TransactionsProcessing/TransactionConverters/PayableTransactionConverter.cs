﻿using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.PreProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public abstract class PayableTransactionConverter<T> : VoidableTransactionConverter<T>
        where T : PayableTransaction
    {
        protected PayableTransactionConverter(ITransactionProcessingContext<T> ctx)
            : base(ctx)
        {
        }

        protected override void InnerPreProcess(PreProcessingContext<T> preProcessingContext)
        {
            base.InnerPreProcess(preProcessingContext);

            preProcessingContext.NullListToEmpty(x => x.Payments);
        }

        protected override void InnerValidate(ValidationContext<T> validationContext)
        {
            base.InnerValidate(validationContext);
            
            PaymentConvertersFactory factory = new PaymentConvertersFactory();
            foreach (int index in Transaction.Payments.EnumerateIndexes())
            {
                var contextCreator = validationContext.GetChildContextCreator(x => x.Payments, index);
                IPaymentConverter paymentConverter = factory.CreatePaymentConverter(Transaction.Payments[index]);
                paymentConverter.Validate(contextCreator);
            }

            //Tran.AbsAmount != PaymentsSum when transaction is void, parked or when its gift card sale. In these cases we dont validate amounts.
            if (Transaction.TransactionStatus == TransactionStatus.COMPLETED)
            {
                var totalPaymentsAmount = Transaction.Payments.Sum(x => x.Direction == Direction.FROM_CUSTOMER ? x.AbsAmount : -x.AbsAmount);
                validationContext.AreEqual(x => x.TotalAmount, totalPaymentsAmount, "Total payments amount should be equal to total transaction amount.");
            }
        }

        protected override void InnerConvert()
        {
            base.InnerConvert();
            ProcessPayments();
        }

        protected virtual void ProcessPayments()
        {
            if (Transaction.TransactionStatus == TransactionStatus.COMPLETED)
            {
                PaymentConvertersFactory factory = new PaymentConvertersFactory();
                short paymentIndex = 1;

                foreach (var payment in Transaction.Payments)
                {
                    IPaymentConverter paymentConverter = factory.CreatePaymentConverter(payment);
                    paymentConverter.Convert(Ctx, paymentIndex);
                    paymentIndex++;
                }
            }
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);
            dltots.TotalSaleAmount = Transaction.TotalAmount;
        }
    }
}
