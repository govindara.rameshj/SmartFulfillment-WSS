﻿using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;
using MiscInReasonCode = WSS.BO.DataLayer.Model.Constants.MiscInReasonCode;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class MiscInTransactionConverter : MiscellaneousTransactionConverter<MiscInTransaction>
    {
        public MiscInTransactionConverter(ITransactionProcessingContext<MiscInTransaction> ctx)
            : base(ctx)
        {
        }

        protected override void InnerValidate(ValidationContext<MiscInTransaction> validationContext)
        {
            validationContext.AreNotEqual(x => x.ReasonCode, Model.Enums.MiscInReasonCode.UNKNOWN, "Type UNKNOWN is not allowed for Misc In Transactions");
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);

            var reasonCodeDescription = GetReasonCodeDescription();

            dltots.Misc = (short)reasonCodeDescription.Id;
            dltots.Description = reasonCodeDescription.Name;
            dltots.TransactionCode = Transaction.IsNormal() ? TransactionCodes.MiscIncome : TransactionCodes.MiscIncomeCorrection;

            if (reasonCodeDescription.Name == GetGiftCardReasonCodeDescriptionName())
            {
                dltots.IsSupervisorUsed = false;
            }
        }

        protected override void InnerProcessCashierBalanceUpdate(CashBalCashier update)
        {
            var reasonCodeDescription = GetReasonCodeDescription();

            if (Transaction.IsNormal())
            {
                update.SetMiscIncomeCount(reasonCodeDescription.Id, 1);
                update.NumTransactions = 1;
            }
            else
            {
                update.SetMiscIncomeCount(reasonCodeDescription.Id, -1);
                update.NumCorrections = 1;
            }

            update.SetMiscIncomeValue(reasonCodeDescription.Id, Transaction.TotalAmount);
            update.GrossSalesAmount = Transaction.TotalAmount;

        }

        protected SystemCode GetReasonCodeDescription()
        {
            int reasonCode = Ctx.IsGeneratedByGiftCardPreprocessing
                ? MiscInReasonCode.GiftCard
                : ModelMapper.GetDbModelCode(Transaction.ReasonCode);

            var codesDictionary = Ctx.ReadOnlyDataRetriever.GetNewSystemCodes(SystemCodeType.MiscInReason);
            var reasonCodeDescription = codesDictionary[reasonCode.ToString()];
            return reasonCodeDescription;
        }

        protected string GetGiftCardReasonCodeDescriptionName()
        {
            var codesDictionary = Ctx.ReadOnlyDataRetriever.GetNewSystemCodes(SystemCodeType.MiscInReason);
            return codesDictionary[MiscInReasonCode.GiftCard.ToString()].Name;
        }
    }
}
