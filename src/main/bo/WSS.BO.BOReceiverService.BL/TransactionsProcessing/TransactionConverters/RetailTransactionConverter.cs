using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.PreProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.OrderCancellation;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using WSS.BO.Model.Entity.Discounts;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class RetailTransactionConverter : PayableTransactionConverter<RetailTransaction>
    {
        public RetailTransactionConverter(ITransactionProcessingContext<RetailTransaction> ctx)
            : base(ctx)
        {
        }

        #region Validation

        protected override void InnerValidate(ValidationContext<RetailTransaction> validationContext)
        {
            base.InnerValidate(validationContext);

            validationContext.AreNotEqual(x => x.RefundCashier, Global.NullUserCode, "Empty refund cashier should be equal null");
            validationContext.AreNotEqual(x => x.Manager, Global.NullUserCode, "Empty manager should be equal null");

            ValidateEmployeeDiscounts(validationContext);
            ValidateTransactionContacts(validationContext);

            if (Ctx.Config.ValidateTotalAmounts && Transaction.TransactionStatus == TransactionStatus.COMPLETED)
            {
                ValidateTransactionAmountsAgainstItemTotals(validationContext);
            }

            ValidateFulfillments(validationContext);
            ValidateCancellations(validationContext);
            ValidateItems(validationContext);
            ValidateCoupons(validationContext);
        }

        private void ValidateTransactionAmountsAgainstItemTotals(ValidationContext<RetailTransaction> validationContext)
        {
            var skuProductItems = Transaction.Items.Where(x => !x.IsReversed && x.Product is SkuProduct);
            var giftCardItems = Transaction.Items.Where(x => !x.IsReversed).Select(x => x.Product).OfType<GiftCardProduct>().ToList();
            decimal totalProductAmount = 0;

            foreach (var item in skuProductItems)
            {
                var skuProduct = item.Product as SkuProduct;
                if (skuProduct != null)
                {
                    var productAmount = (item.Direction == Direction.TO_CUSTOMER ? 1 : -1) * skuProduct.InitialPrice * skuProduct.AbsQuantity;

                    if (skuProduct.Discounts != null)
                    {
                        productAmount -= (item.Direction == Direction.TO_CUSTOMER ? 1 : -1) * skuProduct.Discounts.OfType<AmountDiscount>().Sum(x => x.SavingAmount);
                        productAmount -= (item.Direction == Direction.TO_CUSTOMER ? 1 : -1) * skuProduct.CalculateEventsDiscount();
                    }

                    totalProductAmount += productAmount;
                }
            }

            var giftCardAmount = giftCardItems.Sum(x => x.AbsAmount);
            decimal deliveryCharge = 0;

            if (Transaction.Items.Any(x => x.Product is DeliveryProduct))
            {
                var deliveryProduct = Transaction.Items.First(x => x.Product is DeliveryProduct).Product as DeliveryProduct;
                if (deliveryProduct != null)
                {
                    deliveryCharge = deliveryProduct.AbsAmount;
                }
            }

            validationContext.AreEqual(x => x.TotalAmount, totalProductAmount + giftCardAmount + deliveryCharge, "Total items amount should be equal to transaction total amount.");
        }

        private void ValidateEmployeeDiscounts(ValidationContext<RetailTransaction> validationContext)
        {
            var transactionDiscounts = Transaction.GetDiscounts<EmployeeDiscount>();

            if (transactionDiscounts.Any())
            {
                foreach (var itemIndex in Transaction.Items.EnumerateIndexes())
                {
                    var retailTransactionItem = Transaction.Items[itemIndex];

                    if (retailTransactionItem != null)
                    {
                        ValidateItemEmployeeDiscounts(validationContext, retailTransactionItem, itemIndex);
                    }                        
                }
            }
        }

        private void ValidateItemEmployeeDiscounts(ValidationContext<RetailTransaction> validationContext, RetailTransactionItem item, int itemIndex)
        {
            var skuProduct = item.Product as SkuProduct;
            if (skuProduct != null && skuProduct.Discounts != null && skuProduct.Discounts.Any(x => x is EmployeeDiscount))
            {
                foreach (var discountIndex in skuProduct.Discounts.EnumerateIndexes())
                {
                    if (skuProduct.Discounts[discountIndex] is EmployeeDiscount)
                    {
                        var itemContext = validationContext.CreateChildContext(x => x.Items, itemIndex);
                        var productContext = itemContext.CreateChildContext(x => x.Product);
                        var discountContext = productContext.CreateChildContext(x => ((SkuProduct)x).Discounts, discountIndex);

                        discountContext.AreNotEqual(x => ((EmployeeDiscount)x).SupervisorNumber, Global.NullUserCode, "Empty discount supervisor should be equal null");
                    }
                }
            }
        }

        #region Contacts Validation

        private void ValidateTransactionContacts(ValidationContext<RetailTransaction> validationContext)
        {
            //If transaction is for parked or voided refund, customer information should not exist in the transaction, so we don't want to validate it
            if (Transaction.IsRefund() && Transaction.TransactionStatus == TransactionStatus.COMPLETED)
            {
                ValidateRefundContacts(validationContext);
            }

            if (!CheckIfSaleWithAllRevertedRefund())
            {
                ValidateContactsLengthLimitation(validationContext);

                ValidateCustomer(validationContext);
            }
        }

        private void ValidateRefundContacts(ValidationContext<RetailTransaction> validationContext)
        {
            validationContext.IsNotNull(x => x.Contacts, "Contacts should be specified for Refund transaction");
            validationContext.IsNotNull(x => x.Customer, "Customer should be specified for Refund transaction");
            var contactsInfoContext = validationContext.CreateChildContext(x => x.Contacts);
            contactsInfoContext.AreNotEqual(x => x.Count, 0, "Contacts should be specified for Refund transaction");
        }

        private void ValidateContactsLengthLimitation(ValidationContext<RetailTransaction> validationContext)
        {
            foreach (var contactIndex in Transaction.Contacts.EnumerateIndexes())
            {
                var contactInfoContext = validationContext.CreateChildContext(x => x.Contacts, contactIndex);
                contactInfoContext.HasLengthLessOrEqual(x => x.Name, LengthLimitations.DlrcusName, String.Format("Customer name field is longer than {0}", LengthLimitations.DlrcusName));
                contactInfoContext.HasLengthLessOrEqual(x => x.Email, LengthLimitations.DlrcusEmail, String.Format("Email field is longer than {0}", LengthLimitations.DlrcusEmail));
                contactInfoContext.HasLengthLessOrEqual(
                    x => x.MobilePhone, LengthLimitations.DlrcusMobilePhone, String.Format("Mobile phone field is longer than {0}", LengthLimitations.DlrcusMobilePhone));
                contactInfoContext.HasLengthLessOrEqual(x => x.WorkPhone, LengthLimitations.DlrcusWorkPhone, String.Format("Work phone field is longer than {0}", LengthLimitations.DlrcusWorkPhone));
                contactInfoContext.HasLengthLessOrEqual(x => x.Phone, LengthLimitations.DlrcusPhone, String.Format("Phone field is longer than {0}", LengthLimitations.DlrcusPhone));
                contactInfoContext.IsNotNull(x => x.ContactAddress);

                var contactAddressContext = contactInfoContext.CreateChildContext(x => x.ContactAddress);

                if (contactAddressContext != null)
                {
                    contactAddressContext.HasLengthLessOrEqual(x => x.Town, LengthLimitations.DlrcusTown, String.Format("Town field is longer than {0}", LengthLimitations.DlrcusTown));
                    contactAddressContext.HasLengthLessOrEqual(
                        x => x.AddressLine1, LengthLimitations.DlrcusAddress1, String.Format("AddressLine1 field is longer than {0}", LengthLimitations.DlrcusAddress1));
                    contactAddressContext.HasLengthLessOrEqual(
                        x => x.AddressLine2, LengthLimitations.DlrcusAddress2, String.Format("AddressLine2 field is longer than {0}", LengthLimitations.DlrcusAddress2));
                    contactAddressContext.HasLengthLessOrEqual(x => x.PostCode, LengthLimitations.DlrcusPostcode, String.Format("Postcode field is longer than {0}", LengthLimitations.DlrcusPostcode));
                }
            }
        }

        private void ValidateCustomer(ValidationContext<RetailTransaction> validationContext)
        {
            if (Transaction.Customer != null)
            {
                int contactsCount = Transaction.Contacts.Count;

                var contactInfoContext = validationContext.CreateChildContext(x => x.Customer);
                contactInfoContext.IsNotOutOfRange(x => x.ContactInfoIndex, contactsCount, "Index should be within Contacts array");

                if (contactsCount > Transaction.Customer.ContactInfoIndex)
                {
                    var contactsContext = validationContext.CreateChildContext(x => x.Contacts,
                        Transaction.Customer.ContactInfoIndex);

                    contactsContext.IsNotEmpty(x => x.Name);
                    contactsContext.IsNotEmpty(x => x.Phone);
                    contactsContext.IsNotNull(x => x.ContactAddress);

                    var contactAddressContext = contactsContext.CreateChildContext(x => x.ContactAddress);

                    if (contactAddressContext != null)
                    {
                        contactAddressContext.IsNotEmpty(x => x.PostCode);
                        contactAddressContext.IsNotEmpty(x => x.AddressLine1);
                        contactAddressContext.IsNotEmpty(x => x.Town);
                    }
                }
            }
        }

        #endregion 

        #region FulFillments Validation

        private void ValidateFulfillments(ValidationContext<RetailTransaction> validationContext)
        {
            foreach (var fulfilmentIndex in Transaction.Fulfillments.EnumerateIndexes())
            {
                var fulfillment = Transaction.Fulfillments[fulfilmentIndex];
                var fulfillmentContext = validationContext.CreateChildContext(x => x.Fulfillments, fulfilmentIndex);

                ValidateCollectionFulfillment(fulfillment, fulfillmentContext);
                ValidateDeliveryFulfillment(fulfillment, fulfillmentContext);
                ValidateOrderFulfillment(fulfillment, fulfillmentContext);              

                foreach (var fulfilmentItemIndex in fulfillment.Items.EnumerateIndexes())
                {
                    var fulfillmentItemContext = fulfillmentContext.CreateChildContext(x => x.Items, fulfilmentItemIndex);
                    fulfillmentItemContext.IsNotNegative(x => x.AbsQuantity);
                }
            }
        }

        private void ValidateCollectionFulfillment(Fulfillment fulfillment, ValidationContext<Fulfillment> fulfillmentContext)
        {
            if (fulfillment is CollectionFulfillment)
            {
                fulfillmentContext.HasEmptyTimePart(x => ((CollectionFulfillment)x).CollectionDate, "Non-zero Time part is not allowed for Collection Date");
            }
        }

        private void ValidateDeliveryFulfillment(Fulfillment fulfillment, ValidationContext<Fulfillment> fulfillmentContext)
        {
            if (fulfillment is DeliveryFulfillment)
            {
                fulfillmentContext.HasEmptyTimePart(x => ((DeliveryFulfillment)x).DeliveryDate, "Non-zero Time part is not allowed for Delivery Date");
                foreach (var deliveryInstructionIndex in ((DeliveryFulfillment)fulfillment).DeliveryInstructions.EnumerateIndexes())
                {
                    var deliveryInstructionContext = fulfillmentContext.CreateChildContext(x => ((DeliveryFulfillment)x).DeliveryInstructions, deliveryInstructionIndex);
                    deliveryInstructionContext.HasLengthLessOrEqual(
                        LengthLimitations.DeliveryInstructionMaxLength, String.Format("Delivery instruction length is longer than {0}", LengthLimitations.DeliveryInstructionMaxLength));
                }
            }
        }

        private void ValidateOrderFulfillment(Fulfillment fulfillment, ValidationContext<Fulfillment> fulfillmentContext)
        {
            if (fulfillment is OrderFulfillment)
            {
                int contactsCount = Transaction.Contacts.Count;

                fulfillmentContext.IsNotOutOfRange(x => ((OrderFulfillment)x).ContactInfoIndex, contactsCount, "Index should be within Contacts array");
            }
        }

        #endregion

        #region Cancellation Validation

        private void ValidateCancellations(ValidationContext<RetailTransaction> validationContext)
        {
            if (Transaction.Cancellation != null)
            {
                var cancellationcontext = validationContext.CreateChildContext(x => x.Cancellation);
                foreach (var cancellationIndex in Transaction.Cancellation.Items.EnumerateIndexes())
                {
                    ValidateCancellation(validationContext, cancellationcontext, cancellationIndex);
                }
            }
        }

        private void ValidateCancellation(ValidationContext<RetailTransaction> validationContext, ValidationContext<Fulfillment> cancellationContext, int cancellationIndex)
        {
            var cancellationItemContext = cancellationContext.CreateChildContext(x => x.Items, cancellationIndex);
            cancellationItemContext.IsNotOutOfRange(x => x.TransactionItemIndex, Transaction.Items.Count + 1);
            var cancellationitem = Transaction.Cancellation.Items[cancellationIndex];
            if (cancellationitem.TransactionItemIndex <= Transaction.Items.Count && Transaction.Items[cancellationitem.TransactionItemIndex - 1].ItemRefund != null)
            {
                ValidateCanellationItemRefund(validationContext, cancellationitem);
            }
        }

        private void ValidateCanellationItemRefund(ValidationContext<RetailTransaction> validationContext, FulfillmentItem cancellationItem)
        {
            var itemContext = validationContext.CreateChildContext(x => x.Items, cancellationItem.TransactionItemIndex - 1);
            var itemRefundContext = itemContext.CreateChildContext(x => x.ItemRefund);
            itemRefundContext.IsNotEmpty(x => x.RefundedTransactionId);
        }
        
        #endregion

        #region Items Validation

        private void ValidateItems(ValidationContext<RetailTransaction> validationContext)
        {
            var productConvertersFactory = new ProductConvertersFactory(Ctx);

            IEnumerable<RetailTransactionItem> leftItems = Transaction.Items;

            foreach (int index in Transaction.Items.EnumerateIndexes())
            {
                var itemContext = validationContext.CreateChildContext(x => x.Items, index);
                var item = Transaction.Items[index];
                var otherItems = leftItems.Skip(1);
                itemContext.CheckPredicate(x => x.ItemIndex, ii => otherItems.All(x => x.ItemIndex != ii), "Index should be unique");
                leftItems = otherItems;

                if (item.Direction == Direction.FROM_CUSTOMER)
                {
                    ValidateRefundedItem(validationContext, itemContext, item);
                }
                else
                {
                    ValidateSaleItem(itemContext, item);
                }

                itemContext.IsNotNull(x => x.Product);

                if (item.Product != null)
                {
                    var productConverter = productConvertersFactory.CreateProductConverter(item.Product);
                    productConverter.Validate(itemContext);
                }
            }
        }

        private void ValidateRefundedItem(ValidationContext<RetailTransaction> validationContext, ValidationContext<RetailTransactionItem> itemContext, RetailTransactionItem item)
        {
            itemContext.IsNotNull(x => x.ItemRefund, "For Direction=FROM_CUSTOMER value should be specified");
            var itemRefundContext = itemContext.CreateChildContext(x => x.ItemRefund);

            if (item.ItemRefund.RefundedTransactionId != null)
            {
                itemRefundContext.IsNotNull(x => x.RefundedTransactionItemId, "Refund Transaction Item Id should be specified when Refund Transaction Id is specified.");
            }

            if (item.ItemRefund.RefundedTransactionItemId != null)
            {
                ValidateItemRefundWithReceipt(validationContext, itemRefundContext, item);
            }

            itemRefundContext.AreNotEqual(x => x.RefundReasonCode, Model.Enums.RefundReasonCode.UNKNOWN, "Reason Code UNKNOWN is not allowed for Refund Item");
            itemRefundContext.IsEmpty(x => x.Description);

            var skuProduct = item.Product as SkuProduct;

            if (IsSkuProductWithDiscounts(skuProduct))
            {
                ValidateRefundedItemSkuProductDiscounts(itemContext, skuProduct);
            }
        }

        private void ValidateItemRefundWithReceipt(ValidationContext<RetailTransaction> validationContext, ValidationContext<ItemRefund> itemRefundContext, RetailTransactionItem item)
        {
            itemRefundContext.IsNotNull(x => x.RefundedTransactionId, "Refund Transaction Id should be specified when Refund Transaction Item Id is specified.");
            if (IsRefundItemForOrder(item))
            {
                validationContext.IsNotNull(x => x.Cancellation, "Processing of null Cancellation for Order Refund is not allowed");
            }
        }

        private void ValidateRefundedItemSkuProductDiscounts(ValidationContext<RetailTransactionItem> itemContext, SkuProduct skuProduct)
        {
            var productContext = itemContext.CreateChildContext(x => x.Product);

            foreach (var discountIndex in skuProduct.Discounts.EnumerateIndexes())
            {
                if (skuProduct.Discounts[discountIndex] is PriceOverrideDiscount)
                {
                    var priceOverrideEventContext = productContext.CreateChildContext(x => ((SkuProduct)x).Discounts, discountIndex);
                    priceOverrideEventContext.IsEmpty(x => ((PriceOverrideDiscount)x).Description);
                }

                if (skuProduct.Discounts[discountIndex] is EventDiscount)
                {
                    ValidateSkuProductEventDiscounts(productContext, discountIndex);
                }
            }
        }

        private void ValidateSaleItem(ValidationContext<RetailTransactionItem> itemContext, RetailTransactionItem item)
        {
            itemContext.IsNull(x => x.ItemRefund, "For Direction=TO_CUSTOMER value should not be specified");

            var skuProduct = item.Product as SkuProduct;

            if (IsSkuProductWithDiscounts(skuProduct))
            {
                ValidateSaleItemSkuProductDiscounts(itemContext, skuProduct);
            }
        }

        private void ValidateSaleItemSkuProductDiscounts(ValidationContext<RetailTransactionItem> itemContext, SkuProduct skuProduct)
        {
            var productContext = itemContext.CreateChildContext(x => x.Product);

            foreach (int discountIndex in skuProduct.Discounts.EnumerateIndexes())
            {
                if (skuProduct.Discounts[discountIndex] is PriceOverrideDiscount)
                {
                    var priceOverrideEventContext = productContext.CreateChildContext(x => ((SkuProduct)x).Discounts, discountIndex);

                    priceOverrideEventContext.IsNotNegative(x => ((PriceOverrideDiscount)x).SavingAmount, "Sale of item is allowed only with decreasing price override");
                    priceOverrideEventContext.AreNotEqual(x => ((PriceOverrideDiscount)x).PriceOverrideCode, Model.Enums.PriceOverrideReasonCode.UNKNOWN, 
                        "Reason Code UNKNOWN is not allowed for Price Override");
                    priceOverrideEventContext.IsEmpty(x => ((PriceOverrideDiscount)x).Description);
                }

                if (skuProduct.Discounts[discountIndex] is EventDiscount)
                {
                    ValidateSkuProductEventDiscounts(productContext, discountIndex);
                }
            }
        }

        private bool IsSkuProductWithDiscounts(SkuProduct skuProduct)
        {
            return skuProduct != null && skuProduct.Discounts != null && skuProduct.Discounts.Any(x => x is PriceOverrideDiscount || x is EventDiscount);
        }

        private void ValidateSkuProductEventDiscounts(ValidationContext<Product> productContext, int discountIndex)
        {
            var eventDiscountContext = productContext.CreateChildContext(x => ((SkuProduct)x).Discounts, discountIndex);
            var eventDescriptorContext = eventDiscountContext.CreateChildContext(x => ((EventDiscount)x).EventDescriptor);
            eventDescriptorContext.HasLengthLessOrEqual(x => x.EventCode, 6, "Event Discount Code field can't longer than 6 characters");
        }

        #endregion

        private void ValidateCoupons(ValidationContext<RetailTransaction> validationContext)
        {
            if (Transaction.Coupons.Any())
            {
                foreach (int index in Transaction.Coupons.EnumerateIndexes())
                {
                    var couponContext = validationContext.CreateChildContext(x => x.Coupons, index);
                    couponContext.IsNotNull(x => x.EventDescriptor, "An event discount should be specified for a coupon");
                    couponContext.HasLengthEqual(x => x.CouponBarcode, LengthLimitations.CouponBarcodeLength, "Coupon Barcode length must be equal to {0}. But it's equal to {1}");

                    var discountEventContext = couponContext.CreateChildContext(x => x.EventDescriptor);
                    var eventDiscounts = Transaction.GetDiscounts<EventDiscount>();
                    var eventCodes = eventDiscounts.Select(x => x.EventDescriptor.EventCode).ToList();
                    discountEventContext.IsContained(x => x.EventCode, eventCodes, "For a coupon should be at least one SKU with the same event descriptor");
                }
            }
        }

        #endregion

        #region PreProcess

        protected override void InnerPreProcess(PreProcessingContext<RetailTransaction> preProcessingContext)
        {
            base.InnerPreProcess(preProcessingContext);

            preProcessingContext.NullListToEmpty(x => x.Coupons);
            preProcessingContext.NullListToEmpty(x => x.RestrictedItems);

            #region Contacts

            preProcessingContext.NullListToEmpty(x => x.Contacts);

            foreach (var index in Transaction.Contacts.EnumerateIndexes())
            {
                var contactCtx = preProcessingContext.CreateChildContext(x => x.Contacts, index);

                contactCtx.CheckAndTruncate(x => x.Name, LengthLimitations.DlrcusName);
                contactCtx.CheckAndTruncate(x => x.Email, LengthLimitations.DlrcusEmail);
                contactCtx.CheckAndTruncate(x => x.MobilePhone, LengthLimitations.DlrcusMobilePhone);
                contactCtx.CheckAndTruncate(x => x.WorkPhone, LengthLimitations.DlrcusWorkPhone);
                contactCtx.CheckAndTruncate(x => x.Phone, LengthLimitations.DlrcusPhone);

                var contactAddressCtx = contactCtx.CreateChildContext(x => x.ContactAddress);

                if (contactAddressCtx != null)
                {                   
                    contactAddressCtx.CheckAndTruncate(x => x.Town, LengthLimitations.DlrcusTown);
                    contactAddressCtx.CheckAndTruncate(x => x.AddressLine1, LengthLimitations.DlrcusAddress1);
                    contactAddressCtx.CheckAndTruncate(x => x.AddressLine2, LengthLimitations.DlrcusAddress2);
                    contactAddressCtx.CheckAndTruncate(x => x.PostCode, LengthLimitations.DlrcusPostcode);
                }
            }

            #endregion

            #region Fulfillments

            preProcessingContext.NullListToEmpty(x => x.Fulfillments);

            foreach (var index in Transaction.Fulfillments.EnumerateIndexes())
            {
                var fulfillmentCtx = preProcessingContext.CreateChildContext(x => x.Fulfillments, index);
                fulfillmentCtx.NullListToEmpty(x => x.Items);

                var deliveryFulfillment = Transaction.Fulfillments[index] as DeliveryFulfillment;
                if (deliveryFulfillment != null)
                {
                    var deliveryFulfillmentCtx = preProcessingContext.CreateChildContext<DeliveryFulfillment, Fulfillment>(x => x.Fulfillments, index);
                    deliveryFulfillmentCtx.NullListToEmpty(x => x.DeliveryInstructions);
                    foreach (var i in deliveryFulfillment.DeliveryInstructions.EnumerateIndexes())
                    {
                        var instructionCtx = deliveryFulfillmentCtx.CreateChildContext(x => x.DeliveryInstructions, i);
                        instructionCtx.CheckAndTruncate(LengthLimitations.DeliveryInstructionMaxLength);
                    }
                }
            }

            #endregion

            #region Items

            preProcessingContext.NullListToEmpty(x => x.Items);

            var productConvertersFactory = new ProductConvertersFactory(Ctx);
            foreach (int index in Transaction.Items.EnumerateIndexes())
            {
                var item = Transaction.Items[index];
                if (item.Product != null)
                {
                    var itemContext = preProcessingContext.CreateChildContext(x => x.Items, index);
                    var productConverter = productConvertersFactory.CreateProductConverter(item.Product);
                    productConverter.PreProcess(itemContext);
                }
            }

            #endregion
        }

        #endregion

        protected override void InnerConvert()
        {
            base.InnerConvert();

            var lineNumber = 1;
            foreach (var item in Transaction.Items)
            {
                var dlline = ProcessDlline(item, lineNumber);
                Ctx.StagingDb.MainData.AddDailyTillLine(dlline, item);
                ProcessAdditionalCustomerInformationForRefund(item, lineNumber);
                lineNumber++;

                if (IsRefundItemForOrder(item) && !item.IsReversed)
                {
                    var orderCancellationConverter = new OrderCancellationConverter(Ctx);
                    orderCancellationConverter.ProcessOrderCancellationForItem(item, Transaction.Cancellation);
                }

                ProcessPriceMatchDiscounts(item, dlline.SequenceNumber);
            }

            GetDailyGiftCardFromDescriptor();
            ProcessCustomerInformation();
            ProcessOrder();
            ProcessCoupons();
            ProcessRestrictedItems();
        }

        /// <summary>
        /// If transaction is not parked or voided, item is not reversed and item is refunded(from customer), 
        /// then we should create additional dlrcus line for every item in transaction  
        /// with original transaction infomation such as OriginalSaleStore, OriginalTillId and so on.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="lineNumber"></param>
        private void ProcessAdditionalCustomerInformationForRefund(RetailTransactionItem item, int lineNumber)
        {
            if (Transaction.TransactionStatus == TransactionStatus.COMPLETED && !item.IsReversed &&
                item.Direction == Direction.FROM_CUSTOMER && Transaction.Customer != null && !CheckIfTransactionIsGeneratedRefundForGiftCard())
            {
                var dlrcus = ProcessContactInfo(Transaction.Contacts.ElementAt(Transaction.Customer.ContactInfoIndex));
                ProcessDailyCustomerForItem(dlrcus, item, lineNumber);
                Ctx.StagingDb.MainData.Dlrcuses.Add(dlrcus);
            }
        }

        private void ProcessCustomerInformation()
        {
            if (!string.IsNullOrEmpty(Transaction.SurveyPostcode))
            {
                var dlrcus = new DailyCustomer
                {
                    CreateDate = Transaction.CreateDateTime.Date,
                    TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                    Postcode = Transaction.SurveyPostcode
                };
                Ctx.StagingDb.MainData.Dlrcuses.Add(dlrcus);
            }
            else if (NeedSaveContacts()) //If we process sale where customer infomation exists, we should write it in dlrcus table.
            {
                var customer = Transaction.Contacts[Transaction.Customer.ContactInfoIndex];
                var dlrcus = ProcessContactInfo(customer);
                Ctx.StagingDb.MainData.Dlrcuses.Add(dlrcus);
            }
        }

        private void ProcessOrder()
        {
            if (IsTransactionForOrder() && Transaction.TransactionStatus == TransactionStatus.COMPLETED)
            {
                var orderFulfillment = (OrderFulfillment)Transaction.Fulfillments.First(x => x is OrderFulfillment);
                var factory = new OrderFulfillmentConvertersFactory(Ctx);
                var converter = factory.CreateFulfillmentConverter(orderFulfillment);
                converter.Convert(Ctx.StagingDb);
            }
        }

        private void ProcessPriceMatchDiscounts(RetailTransactionItem item, int sequenceNumber)
        {
            if (item.Product is SkuProduct && ((SkuProduct) item.Product).Discounts.Any(x => x is PriceMatchDiscount))
            {
                var skuProduct = (SkuProduct) item.Product;
                foreach (var priceMatchDiscount in skuProduct.Discounts.OfType<PriceMatchDiscount>())
                {
                    var dlolin = ProcessCompetitor(priceMatchDiscount, sequenceNumber);
                    Ctx.StagingDb.MainData.Dlolins.Add(dlolin);
                }
            }
        }

        private void ProcessCoupons()
        {
            foreach (var coupon in Transaction.Coupons)
            {
                if (Transaction.CouponEventDiscountExists(coupon.EventDescriptor.EventCode))
                {
                    var dlcoupon = ProcessCoupon(coupon);
                    Ctx.StagingDb.MainData.Dlcoupons.Add(dlcoupon);
                }
            }
        }

        private void ProcessRestrictedItems()
        {
            var ageCheckedCompletedRestrictedItems =
                Transaction.RestrictedItems.Where(
                    x =>
                        x.RestrictionType == RestrictionType.AGE_CHECK &&
                        x.AcceptType != RestrictedItemAcceptType.ACCEPTED_COMPLETELY);
            foreach (var restrictedItem in ageCheckedCompletedRestrictedItems)
            {
                var dlreject = ProcessDlreject(restrictedItem);
                Ctx.StagingDb.MainData.Dlrejects.Add(dlreject);
            }

        }

        private bool NeedSaveContacts()
        {
            return Transaction.Customer != null && (Transaction.IsSale() || Transaction.TransactionStatus != TransactionStatus.PARKED) 
                && !CheckIfSaleWithAllRevertedRefund() && !CheckIfTransactionIsGeneratedRefundForGiftCard();
        }

        private bool CheckIfSaleWithAllRevertedRefund()
        {
            return Transaction.TransactionStatus == TransactionStatus.COMPLETED 
                && !IsTransactionForOrder() && Transaction.Items.Any(x => x.ItemRefund != null) && !Transaction.Items.Any(x => x.ItemRefund != null && !x.IsReversed);
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);

            bool isParked = Transaction.TransactionStatus == TransactionStatus.PARKED;

            var saveAsRefund = Transaction.IsRefund() && !isParked; //parked refund is saved as sale
            dltots.TransactionCode = saveAsRefund ? TransactionCodes.Refund : TransactionCodes.Sale;
            dltots.IsSaleOrRefund = !saveAsRefund;

            //It is correct to use these values streight from transaction(and not calculate them from lines),
            //because we have validated them before transaction processing, and if validation failed - we will not process transaction.
            dltots.TaxAmount = Transaction.TransactionStatus == TransactionStatus.COMPLETED ? Transaction.TaxAmount : 0;

            var merchAmount = Transaction.GetMerchandiseAmount();
            dltots.MerchandiseAmount = merchAmount;
            dltots.DiscountAmount = Transaction.TotalAmount - merchAmount;
            dltots.IsParked = isParked;

            dltots.DiscountSupervisor = Transaction.GetDiscountSupervisor(Global.NullUserCode);
            dltots.IsRecalled = false; //false, because we update parked tran to recalled in business rule, and new transaction is not recalled of parked.
            dltots.IsSupervisorUsed = Transaction.CheckIsSupervisorUsed(Global.NullUserCode);

            //discount
            var transactionEmployeeDiscounts = Transaction.GetDiscounts<EmployeeDiscount>();
            if (transactionEmployeeDiscounts.Any())
            {
                dltots.StoreNumber = Ctx.ReadOnlyDataRetriever.GetStoreId();
                var employeeDiscount = transactionEmployeeDiscounts.LastOrDefault(x => !string.IsNullOrEmpty(x.CardNumber));
                dltots.EmployeeDiscountCard = employeeDiscount != null ? employeeDiscount.CardNumber : string.Empty;
                dltots.IsEmployeeDiscount = true;
            }

            //refund
            dltots.RefundCashier = GetUserOrEmpty(Transaction.RefundCashier);
            dltots.RefundManager = GetUserOrEmpty(Transaction.Manager);
            dltots.RefundAuthorisationSupervisorNumber = GetUserOrEmpty(Transaction.Manager);
            dltots.SaveStatus = ModelMapper.ToSaveStatus(Transaction.TransactionStatus);
        }

        private string GetUserOrEmpty(string user)
        {
            return String.IsNullOrEmpty(user) ? Global.NullUserCode : user;
        }

        protected virtual DailyTillLine ProcessDlline(RetailTransactionItem item, int itemIndex)
        {
            var dlline = new DailyTillLine
            {
                CreateDate = Transaction.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                SequenceNumber = itemIndex,
                IsReversed = item.IsReversed,
                ReversalReasonCode = LineReversalReasonCode.NoLineReversal,
                VatValue = item.AbsVatAmount
            };

            var factory = new ProductConvertersFactory(Ctx);
            var productConverter = factory.CreateProductConverter(item.Product);
            productConverter.Convert(item, dlline);

            FillLineFromStockMaster(dlline);
            return dlline;
        }

        private void FillLineFromStockMaster(DailyTillLine dlline)
        {
            var stkmasData = Ctx.ReadOnlyDataRetriever.GetSkuInfo(dlline.SkuNumber);
            dlline.HierarchyCategory = stkmasData.HierarchyCategory;
            dlline.HierarchyGroup = stkmasData.HierarchyGroup;
            dlline.HierarchyStyle = stkmasData.HierarchyStyle;
            dlline.HierarchySubGroup = stkmasData.HierarchySubGroup;
            dlline.IsRelatedItemSingle = stkmasData.IsRelatedItemSingle;
            dlline.SaleTypeAttribute = stkmasData.SaleTypeAttribute;
            dlline.IsTaggetItem = stkmasData.IsTaggedItem;
        }

        private DailyCustomer ProcessContactInfo(ContactInfo customer)
        {
            var dlrcus = new DailyCustomer
            {
                CreateDate = Transaction.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                AddressLine1 = customer.ContactAddress.AddressLine1,
                AddressLine2 = customer.ContactAddress.AddressLine2,
                CustomerName = customer.Name,
                Email = customer.Email,
                MobilePhone = customer.MobilePhone,
                PhoneNumber = customer.Phone,
                Postcode = customer.ContactAddress.PostCode,
                Town = customer.ContactAddress.Town,
                WorkPhone = customer.WorkPhone,
                IsOriginalTransactionValidated = Transaction.IsSale()
            };
            return dlrcus;
        }

        private void ProcessDailyCustomerForItem(DailyCustomer dlrcus, RetailTransactionItem item, int lineNumber)
        {
            var refundInfo = item.ItemRefund;

            dlrcus.RefundReasonCode = ModelMapper.GetDbModelCode(refundInfo.RefundReasonCode);

            dlrcus.OriginalTenderType = 0;
            dlrcus.LineNumber = lineNumber;

            //Values about customer in this additional dlrcus rows are empty, so we should set them to String.Empty
            dlrcus.CustomerName = String.Empty;
            dlrcus.Postcode = String.Empty;
            dlrcus.AddressLine1 = String.Empty;
            dlrcus.AddressLine2 = String.Empty;
            dlrcus.Town = String.Empty;
            dlrcus.PhoneNumber = String.Empty;
            dlrcus.MobilePhone = String.Empty;
            dlrcus.WorkPhone = String.Empty;
            dlrcus.Email = String.Empty;
            //BUT HOUSENUMBER IS NOT EMPTY IN DB FOR SOME REASON???

            var originalDltotsKey = GetOriginalTransactionKeyForRefund(refundInfo);
            dlrcus.OriginalSaleStore = Ctx.ReadOnlyDataRetriever.GetStoreId();
            if (originalDltotsKey != null)
            {
                dlrcus.OriginalSaleDate = originalDltotsKey.CreateDate;
                dlrcus.OriginalTillNumber = originalDltotsKey.TillNumber;
                dlrcus.OriginalTransactionNumber = originalDltotsKey.TransactionNumber;
                dlrcus.IsOriginalTransactionValidated = true;
                dlrcus.OriginalLineNumber = GetOriginalLineNumber(originalDltotsKey, item.ItemRefund.RefundedTransactionItemId);
            }
            else
            {
                dlrcus.OriginalSaleDate = dlrcus.CreateDate;
                dlrcus.IsOriginalTransactionValidated = false;
            }
        }

        private IDailyTillTranKey GetOriginalTransactionKeyForRefund(ItemRefund refundInfo)
        {
            IDailyTillTranKey result;
            //For legacy refund
            if (LegacyReceiptBarcodeHelper.IsRefundForLegacyTransaction(refundInfo.RefundedTransactionId))
            {
                result = Ctx.GetLegacyTransactionDailyTillTranKey(refundInfo.RefundedTransactionId);
                if (!Ctx.RuntimeDataRetriever.IsDltotsExists(result))
                {
                    Log.Warn(
                        "A transaction with Id {0} has a reference to a transaction with TransactionId = {1}, TillId = {2}, TransactionDate = {3}, which was not found in the Database",
                        Transaction.SourceTransactionId, result.TransactionNumber, result.TillNumber,
                        result.CreateDate.ToString("d"));
                }
            }
            else
            {
                //For refund with receipt
                if (!string.IsNullOrEmpty(refundInfo.RefundedTransactionId))
                {
                    result = Ctx.GetDailyTillTranKeyBySource(refundInfo.RefundedTransactionId, Transaction.Source);
                    if (result == null)
                    {
                        Log.Warn(
                            "A transaction with Id {0} has a reference to a transaction with Id {1} which was not found in the Database",
                            Transaction.SourceTransactionId, refundInfo.RefundedTransactionId);
                    }
                }
                //For refund without receipt
                else
                {
                    result = null;
                }
            }
            return result;
        }

        private Dlolin ProcessCompetitor(PriceMatchDiscount priceMatchDiscount, int lineNumber)
        {
            var dlolin = new Dlolin
            {
                CreateDate = Transaction.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                Name = priceMatchDiscount.CompetitorName,
                CompetitorsPriceEntered = priceMatchDiscount.NewPrice,
                EnteredPricesAreVatInclusive = priceMatchDiscount.IsVATIncluded,
                LineNumber = (short) lineNumber
            };
            return dlolin;
        }

        private Dlcoupon ProcessCoupon(Coupon coupon)
        {
            var dlcoupon = new Dlcoupon();
            var couponId = coupon.CouponBarcode.Substring(0, 7);
            var couponMaster = Ctx.RuntimeDataRetriever.SelectCouponMaster(couponId);
            dlcoupon.CreateDate = Transaction.CreateDateTime.Date;
            dlcoupon.TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber);
            dlcoupon.CouponId = couponId;
            dlcoupon.IsExclusiveCoupon = couponMaster != null && couponMaster.IsExclusiveCoupon;
            dlcoupon.IsCouponReUsable = couponMaster != null && couponMaster.IsCouponReUsable;
            dlcoupon.ManagerId = string.Empty;
            dlcoupon.MarketingReferenceNumber = coupon.CouponBarcode.Substring(7, 4);
            dlcoupon.CouponSerialNumber = coupon.CouponBarcode.Substring(11, 6);
            return dlcoupon;
        }

        private Dlreject ProcessDlreject(RestrictedItem restrictedItem)
        {
            var dlreject = new Dlreject
            {
                CreateDate = Transaction.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                SkuNumber = restrictedItem.BrandProductId,
                EmployeeId =
                    string.IsNullOrEmpty(restrictedItem.AuthorizerNumber)
                        ? Global.NullUserCode
                        : restrictedItem.AuthorizerNumber.PadLeft(3, '0'),
                RejectText = ModelMapper.GetConfirmationText(restrictedItem)
            };
            return dlreject;
        }

        private bool IsTransactionForOrder()
        {
            var orderFulfillment = Transaction.Fulfillments.FirstOrDefault(x => x is OrderFulfillment);
            return orderFulfillment != null;
        }

        private bool IsRefundItemForOrder(RetailTransactionItem item)
        {
            if (item.Direction == Direction.FROM_CUSTOMER && item.Product.GetType() != typeof(GiftCardProduct) && !String.IsNullOrEmpty(item.ItemRefund.RefundedTransactionId))
            {
                return Ctx.RuntimeDataRetriever.IsTransactionToCancelForOrder(item.ItemRefund.RefundedTransactionId, Transaction.Source);
            }
            return false;
        }
 
        protected override void InnerProcessCashierBalanceUpdate(CashBalCashier update)
        {
            update.NumTransactions = 1;
            update.GrossSalesAmount = Transaction.TotalAmount;

            if (Transaction.IsRefund())
            {
                update.RefundCount = 1;
                update.RefundAmount = Transaction.TotalAmount;
            }
            else
            {
                update.SalesCount = 1;
                update.SalesAmount = Transaction.TotalAmount;
            }
        }

        private void GetDailyGiftCardFromDescriptor()
        {
            if (Ctx.GiftCardOperationDescriptors != null)
            {
                foreach (var operationDescriptor in Ctx.GiftCardOperationDescriptors)
                {
                    DailyGiftCard dailyGiftCard = new DailyGiftCard
                    {
                        CreateDate = Transaction.CreateDateTime.Date,
                        TillNumber = ConversionUtils.GetCanonicalTillNumber(Transaction.TillNumber),
                        CashierNumber = Transaction.CashierNumber,
                        AuthCode = operationDescriptor.GiftCardOperation.AuthCode ?? Global.NullUserCode,
                        CardNumber = operationDescriptor.GiftCardOperation.CardNumber,
                        TransactionType = operationDescriptor.TransactionType,
                        TenderAmount = -operationDescriptor.Amount // for some reason here it is with +, not like in dlpaid.
                    };

                    Ctx.StagingDb.MainData.DlGiftCards.Add(dailyGiftCard);
                }
            }
        }

        private int? GetOriginalLineNumber(IDailyTillTranKey originalKey, string refundedTransactionItemId)
        {
            var dlline = Ctx.RuntimeDataRetriever.GetOriginalLineNumber(originalKey, refundedTransactionItemId);

            return dlline == null ? null : (int?)dlline.SequenceNumber;
        }

        private bool CheckIfTransactionIsGeneratedRefundForGiftCard()
        {
            return Transaction.IsRefund() && !Transaction.Items.Any(x => !(x.Product is GiftCardProduct));
        }
    }
}
