﻿using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public interface ITransactionConverter
    {
        IList<ValidationError> Validate();
        void PreProcess();
        void Convert();
        void ProcessCashierBalanceUpdate(CashBalCashier update);
    }
}
