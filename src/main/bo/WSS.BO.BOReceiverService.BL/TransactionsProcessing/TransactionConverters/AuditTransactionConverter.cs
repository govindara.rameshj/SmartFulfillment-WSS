﻿using System;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class AuditTransactionConverter : TransactionConverter<AuditTransaction>
    {
        public AuditTransactionConverter(ITransactionProcessingContext<AuditTransaction> ctx)
            : base(ctx)
        {
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);

            var transactionCode = String.Empty;
            if (Transaction.AuditType == AuditType.SIGN_ON)
            {
                transactionCode = TransactionCodes.SignOn;
            }
            if (Transaction.AuditType == AuditType.SIGN_OFF)
            {
                transactionCode = TransactionCodes.SignOff;
            }
            dltots.TransactionCode = transactionCode;
            dltots.SaveStatus = "99";
        }
    }
}
