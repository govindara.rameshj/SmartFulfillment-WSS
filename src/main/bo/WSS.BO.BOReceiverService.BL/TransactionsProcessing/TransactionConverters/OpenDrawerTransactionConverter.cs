﻿using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class OpenDrawerTransactionConverter : VoidableTransactionConverter<OpenDrawerTransaction>
    {
        public OpenDrawerTransactionConverter(ITransactionProcessingContext<OpenDrawerTransaction> ctx)
            : base(ctx)
        {
        }

        protected override void InnerProcessCashierBalanceUpdate(CashBalCashier update)
        {
            update.NumOpenDrawer = 1;
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            base.ProcessDltots(dltots);

            const string code = OpenDrawerReasonCode.CashPickup;
            var codeInfo = Ctx.ReadOnlyDataRetriever.GetSystemCodes(SystemCodeType.OpenDrawerReason)[code];

            dltots.OpenDrawerReasonCode = System.Convert.ToInt16(codeInfo.Code);
            dltots.Description = codeInfo.Description;
            dltots.TransactionCode = TransactionCodes.OpenDrawer;
        }
    }
}
