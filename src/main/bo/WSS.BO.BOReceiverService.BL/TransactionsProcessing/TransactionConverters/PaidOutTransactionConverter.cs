﻿using System;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class PaidOutTransactionConverter : MiscellaneousTransactionConverter<PaidOutTransaction>
    {
        public PaidOutTransactionConverter(ITransactionProcessingContext<PaidOutTransaction> ctx)
            : base(ctx)
        {
        }

        protected override void InnerValidate(ValidationContext<PaidOutTransaction> validationContext)
        {
            validationContext.AreNotEqual(x => x.ReasonCode, Model.Enums.PaidOutReasonCode.UNKNOWN, "Type UNKNOWN is not allowed for Paid Out Transactions");
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            var codes = Ctx.ReadOnlyDataRetriever.GetNewSystemCodes(SystemCodeType.PaidOutReason);

            var code = codes[ModelMapper.GetDbModelCode(Transaction.ReasonCode)];

            dltots.TransactionCode = Transaction.IsCorrection() ? TransactionCodes.PaidOutCorrection : TransactionCodes.PaidOut;
            dltots.Misc = (Int16)code.Id;
            dltots.Description = code.Name;

            base.ProcessDltots(dltots);
        }

        protected override void InnerProcessCashierBalanceUpdate(CashBalCashier update)
        {
            short miscOutIndex = Int16.Parse(ModelMapper.GetDbModelCode(Transaction.ReasonCode));
            update.SetMiscOutValue(miscOutIndex, Transaction.TotalAmount);
            update.GrossSalesAmount = Transaction.TotalAmount;

            if (Transaction.IsNormal())
            {
                update.SetMiscOutCount(miscOutIndex, 1);
                update.NumTransactions = 1;
            }
            else
            {
                update.SetMiscOutCount(miscOutIndex, -1);
                update.NumCorrections = 1;
            }
        }
    }
}
