﻿using System;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public class TransactionConvertersFactory
    {
        public ITransactionConverter CreateTransactionConverter(ITransactionProcessingContext ctx)
        {
            ITransactionConverter result;

            var transaction = ctx.GetTransaction();

            if (transaction is AuditTransaction)
            {
                result = new AuditTransactionConverter((TransactionProcessingContext<AuditTransaction>)ctx);
            }
            else if (transaction is MiscInTransaction)
            {
                result = new MiscInTransactionConverter((TransactionProcessingContext<MiscInTransaction>) ctx);
            }
            else if (transaction is PaidOutTransaction)
            {
                result = new PaidOutTransactionConverter((TransactionProcessingContext<PaidOutTransaction>) ctx);
            }
            else if (transaction is OpenDrawerTransaction)
            {
                result = new OpenDrawerTransactionConverter((TransactionProcessingContext<OpenDrawerTransaction>)ctx);
            }
            else if (transaction is RetailTransaction)
            {
                result = new RetailTransactionConverter((TransactionProcessingContext<RetailTransaction>)ctx);
            }
            else
            {
                throw new ArgumentException(String.Format("Unknown transaction type : {0}", transaction.GetType()));
            }

            return result;
        }

    }
}
