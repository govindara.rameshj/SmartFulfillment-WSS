﻿using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters
{
    public abstract class MiscellaneousTransactionConverter<T> : PayableTransactionConverter<T>
        where T : MiscellaneousTransaction
    {
        protected MiscellaneousTransactionConverter(ITransactionProcessingContext<T> ctx)
            : base(ctx)
        {
        }

        protected override void ProcessDltots(Dltots dltots)
        {
            dltots.IsSupervisorUsed = true;
            dltots.SaveStatus = "99";
            dltots.DocumentNumber = Transaction.InvoiceNumber;

            base.ProcessDltots(dltots);

            dltots.NonMerchandiseAmount = dltots.TotalSaleAmount;
        }
    }
}
