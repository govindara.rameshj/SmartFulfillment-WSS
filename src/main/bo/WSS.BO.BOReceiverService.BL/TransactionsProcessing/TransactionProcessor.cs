using System;
using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Reverse;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    // TODO extract FindTransactionPart
    public class TransactionProcessor : ITransactionProcessor
    {
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly IReadOnlyDataRetriever readOnlyDataRetriever;
        private readonly ITransactionProcessingSteps steps;

        public TransactionProcessor(IDataLayerFactory dataLayerFactory,
            IReadOnlyDataRetriever readOnlyDataRetriever, 
            ITransactionProcessingSteps steps)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.readOnlyDataRetriever = readOnlyDataRetriever;
            this.steps = steps;
        }

        public void ProcessTransaction(Transaction transaction)
        {
            steps.SetTransaction(transaction);

            steps.CheckForDublicates();
            steps.CheckForLocksBeforeProcessing();

            steps.CreateContext();
            steps.PreProcess();
            steps.Validate();

            steps.EmulateLegacyTransactionSplitting();

            steps.Convert();
            steps.ApplyBusinessRules();

            steps.CheckForLocksBeforeSavingToDb();
            steps.SaveToDb();
        }

        public RetailTransaction FindTransaction(string receiptBarcode, string storeNumber)
        {
            ValidateStoreNumber(storeNumber);
            ValidateReceiptBarcodeLength(receiptBarcode);

            var dailyTillTranKey = LegacyReceiptBarcodeHelper.ParseTransactionKey(receiptBarcode);

            if (TransactionExists(dailyTillTranKey))
            {
                var converter = new RetailTransactionReverseConverter(dataLayerFactory, readOnlyDataRetriever);
                return converter.RecoverRetailTransaction(dailyTillTranKey);
            }

            throw new TransactionNotFoundException();
        }

        private void ValidateStoreNumber(string storeNumber)
        {
            var storeId = readOnlyDataRetriever.GetStoreId().Trim().PadLeft(3, '0');
            if (!String.Equals(storeNumber, storeId))
            {
                var error = new ValidationError
                {
                    ErrorMessage = String.Format("The service process only retail transactions for store [{0}]", storeId),
                    PropertyPath = "StoreNumber"
                };

                List<ValidationError> errors = new List<ValidationError> {error};
                throw new ValidationException("Incoming request fields are not valid.", errors);
            }
        }

        private void ValidateReceiptBarcodeLength(string receiptBarcode)
        {
            if (receiptBarcode.Length != 15)
            {
                var error = new ValidationError
                {
                    ErrorMessage = String.Format("The service can't process receipt barcode of length: [{0}]", receiptBarcode.Length),
                    PropertyPath = "ReceiptBarcodeLength"
                };

                List<ValidationError> errors = new List<ValidationError> { error };
                throw new ValidationException("Incoming request fields are not valid.", errors);
            }
        }

        private bool TransactionExists(IDailyTillTranKey dailyTillTranKey)
        {
            var repo = dataLayerFactory.Create<DailyTillRepository>();
            return repo.TransactionExistsInDb(dailyTillTranKey);
        }
    }
}
