﻿using System;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters
{
    public class OrderFulfillmentConvertersFactory
    {
        private readonly ITransactionProcessingContext<RetailTransaction> _ctx;

        public OrderFulfillmentConvertersFactory(ITransactionProcessingContext<RetailTransaction> ctx)
        {
            _ctx = ctx;
        }

        public IOrderFulfillmentConverter CreateFulfillmentConverter(OrderFulfillment fulfillment)
        {
            if (fulfillment is DeliveryFulfillment)
            {
                return new DeliveryFulfillmentConverter((DeliveryFulfillment)fulfillment, _ctx);
            }
            if (fulfillment is CollectionFulfillment)
            {
                return new CollectionFulfillmentConverter((CollectionFulfillment)fulfillment, _ctx);
            }
            throw new ArgumentException(String.Format("Unknown order fulfillment type : {0}", fulfillment.GetType()));
        }
    }
}
