﻿using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters
{
    public class CollectionFulfillmentConverter : OrderFulfillmentConverter
    {
        private readonly CollectionFulfillment _fulfillment;

        public CollectionFulfillmentConverter(CollectionFulfillment fulfillment, ITransactionProcessingContext<RetailTransaction> ctx)
            : base(fulfillment, ctx)
        {
            _fulfillment = fulfillment;
        }

        protected override void ProcessCorhdr(CustomerOrderHeader corhdr)
        {
            base.ProcessCorhdr(corhdr);

            corhdr.DeliveryOrCollectionDate = _fulfillment.CollectionDate;
            corhdr.IsForDelivery = false;
        }
    }
}
