﻿using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using slf4net;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters
{
    public abstract class OrderFulfillmentConverter : IOrderFulfillmentConverter
    {
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(OrderFulfillmentConverter));
        protected string ConverterName;
        protected ITransactionProcessingContext<RetailTransaction> Ctx;

        private readonly OrderFulfillment _fulfillment;

        protected OrderFulfillmentConverter(OrderFulfillment fulfillment, ITransactionProcessingContext<RetailTransaction> ctx)
        {
            Ctx = ctx;
            ConverterName = GetType().Name;
            _fulfillment = fulfillment;
        }

        public virtual void Convert(StagingDb stagingDb)
        {
            Log.Info("{0}.Convert is called", ConverterName);
            InnerConvert(stagingDb);
            Log.Info("{0}.Convert is completed", ConverterName);
        }

        protected virtual void InnerConvert(StagingDb stagingDb)
        {
            var corhdr = new CustomerOrderHeader();
            ProcessCorhdr(corhdr);
            stagingDb.MainData.Corhdr = corhdr;

            var corhdr4 = new CustomerOrderHeader4();
            ProcessCorhdr4(corhdr4);
            stagingDb.MainData.Corhdr4 = corhdr4;

            var corhdr5 = new CustomerOrderHeader5();
            ProcessCorhdr5(corhdr5);
            stagingDb.MainData.Corhdr5 = corhdr5;

            var cortxts = new List<CustomerOrderText>();
            ProcessCortxt(cortxts);
            stagingDb.MainData.Cortxts.AddRange(cortxts);

            var corlinLineNumber = 1;
            foreach (var transactionItem in Ctx.Transaction.Items.Where(x => !x.IsReversed))
            {
                var corlin = new CustomerOrderLine();
                ProcessCorlin(corlin, transactionItem, corlinLineNumber);
                stagingDb.MainData.Corlins.Add(corlin);

                var corlin2 = new CustomerOrderLine2();
                ProcessCorlin2(corlin2, transactionItem, corlinLineNumber);
                stagingDb.MainData.Corlins2.Add(corlin2);

                corlinLineNumber++;
            }
        }

        protected virtual void ProcessCorhdr(CustomerOrderHeader corhdr)
        {
            corhdr.CancellationState = "0";
            corhdr.CreateDate = Ctx.Transaction.CreateDateTime.Date;
            corhdr.SaleDate = Ctx.Transaction.CreateDateTime.Date;
            corhdr.MerchandiseValueStore = Ctx.Transaction.TotalAmount;
            corhdr.TillNumber = ConversionUtils.GetCanonicalTillNumber(Ctx.Transaction.TillNumber);
            corhdr.TotalOrderUnitsNumber = GetTotalOrderUnitsNumber(Ctx.Transaction.Items);
            var instantFulfillment = Ctx.Transaction.Fulfillments.FirstOrDefault(x => x is InstantFulfillment);
            corhdr.TotalUnitsTakenNumber = instantFulfillment != null ? GetTotalUnitsTakenNumber(instantFulfillment.Items) : 0;
            corhdr.TotalUnitsRefundedNumber = 0;

            var contact = Ctx.Transaction.Contacts[_fulfillment.ContactInfoIndex];
            var contactAddress = contact.ContactAddress;
            corhdr.AddressLine1 = contactAddress.AddressLine1;
            corhdr.AddressLine2 = contactAddress.AddressLine2;
            corhdr.Town = contactAddress.Town;
            corhdr.CustomerNumber = "0"; //TODO: We should decide what value to write here
            corhdr.CustomerName = contact.Name;
            corhdr.MobilePhoneNumber = contact.MobilePhone;
            corhdr.PhoneNumber = contact.Phone;
            corhdr.PostCode = contactAddress.PostCode;
        }

        protected virtual void ProcessCorhdr4(CustomerOrderHeader4 corhdr4)
        {
            corhdr4.CashierNumber = Ctx.Transaction.CashierNumber;
            corhdr4.CreateDate = Ctx.Transaction.CreateDateTime.Date;

            var contacts = Ctx.Transaction.Contacts[_fulfillment.ContactInfoIndex];
            var contactAddress = contacts.ContactAddress;
            corhdr4.AddressLine2 = contactAddress.AddressLine2;
            corhdr4.CustomerAddress1 = contactAddress.AddressLine1;
            corhdr4.ContactInfoIndex = "0"; //TODO: We should decide what value to write here
            corhdr4.Email = contacts.Email;
            corhdr4.Name = contacts.Name;
            corhdr4.PostCode = contactAddress.PostCode;
            corhdr4.SellingStoreId = Ctx.ReadOnlyDataRetriever.GetAbsoluteStoreId();
            corhdr4.Town = contactAddress.Town;
            corhdr4.WorkPhone = contacts.WorkPhone;
        }

        protected virtual void ProcessCorhdr5(CustomerOrderHeader5 corhdr5)
        {
            corhdr5.Source = Global.OvcOrderSource;
            corhdr5.SourceOrderNumber = _fulfillment.Number;
        }

        protected virtual void ProcessCorlin(CustomerOrderLine corlin, RetailTransactionItem transactionItem, int corlinLineNumber)
        {
            var factory = new ProductConvertersFactory(Ctx);
            var productConverter = factory.CreateProductConverter(transactionItem.Product);
            productConverter.FillCustomerOrderLine(corlin, transactionItem, corlinLineNumber);
        }

        protected virtual void ProcessCorlin2(CustomerOrderLine2 corlin2, RetailTransactionItem transactionItem, int corlinLineNumber)
        {
            corlin2.LineNumber = corlinLineNumber.ToString("D4");
            corlin2.SourceOrderLineNo = Ctx.StagingDb.MainData.GetDailyTillLine(transactionItem).SequenceNumber;
        }

        protected virtual void ProcessCortxt(List<CustomerOrderText> cortxts)
        {
        }

        private decimal GetTotalOrderUnitsNumber(IEnumerable<RetailTransactionItem> items)
        {
            return items.Where(item => item.Direction == Direction.TO_CUSTOMER)
                .Select(item => item.Product).OfType<SkuProduct>()
                .Sum(x => x.AbsQuantity);
        }

        private decimal GetTotalUnitsTakenNumber(IList<FulfillmentItem> fulfillmentItems)
        {
            return fulfillmentItems.Sum(x => x.AbsQuantity);
        }
    }
}
