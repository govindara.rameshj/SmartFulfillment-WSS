﻿using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters
{
    public interface IOrderFulfillmentConverter
    {
        void Convert(StagingDb stagingDb);
    }
}
