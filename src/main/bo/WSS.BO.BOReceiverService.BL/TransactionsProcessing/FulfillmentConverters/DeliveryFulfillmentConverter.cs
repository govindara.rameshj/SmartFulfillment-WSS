﻿using System;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.FulfillmentConverters
{
    public class DeliveryFulfillmentConverter : OrderFulfillmentConverter
    {
        private readonly DeliveryFulfillment _fulfillment;

        public DeliveryFulfillmentConverter(DeliveryFulfillment fulfillment, ITransactionProcessingContext<RetailTransaction> ctx)
            : base(fulfillment, ctx)
        {
            _fulfillment = fulfillment;
        }

        protected override void ProcessCorhdr(CustomerOrderHeader corhdr)
        {
            base.ProcessCorhdr(corhdr);

            decimal deliveryCharge = 0;
            if (Ctx.Transaction.Items.Any(x => x.Product is DeliveryProduct))
            {
                deliveryCharge = Ctx.Transaction.Items.First(x => x.Product is DeliveryProduct).Product.CalculateAbsAmount();
            }
            corhdr.DeliveryCharge = deliveryCharge;
            corhdr.DeliveryOrCollectionDate = _fulfillment.DeliveryDate;
            corhdr.IsForDelivery = true;
            if (_fulfillment.DeliverySlot != null)
            {
                corhdr.RequiredDeliverySlotID = _fulfillment.DeliverySlot.Id;
                corhdr.RequiredDeliverySlotDescription = _fulfillment.DeliverySlot.Description;
                corhdr.RequiredDeliverySlotStartTime = _fulfillment.DeliverySlot.StartTime;
                corhdr.RequiredDeliverySlotEndTime = _fulfillment.DeliverySlot.EndTime;
            }
            else
            {
                var slotId = (_fulfillment.DeliveryDate.DayOfWeek == DayOfWeek.Saturday) ? DeliverySlotType.Saturday : DeliverySlotType.Weekday;
                var slot = Ctx.ReadOnlyDataRetriever.GetDeliverySlots().First(s => s.Id == slotId);
                corhdr.RequiredDeliverySlotID = slot.Id;
                corhdr.RequiredDeliverySlotDescription = slot.Description;
                corhdr.RequiredDeliverySlotStartTime = slot.StartTime;
                corhdr.RequiredDeliverySlotEndTime = slot.EndTime;
            }
        }

        protected override void ProcessCortxt(List<CustomerOrderText> cortxts)
        {
            base.ProcessCortxt(cortxts);

            int lineNumber = 1;
            foreach (var instruction in _fulfillment.DeliveryInstructions)
            {
                var cortxt = new CustomerOrderText
                {
                    LineNumber = lineNumber.ToString(),
                    SellingStoreId = Ctx.ReadOnlyDataRetriever.GetAbsoluteStoreId(),
                    Text = instruction,
                    Type = "DI"
                };

                cortxts.Add(cortxt);
                lineNumber++;
            }
        }
    }
}
