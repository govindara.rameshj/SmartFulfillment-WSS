﻿using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public class ChequePaymentConverter : PaymentConverter<ChequePayment>
    {
        public ChequePaymentConverter(ChequePayment payment)
            : base(payment)
        {
        }

        protected override int GetTenderType()
        {
            return TenderType.Cheque;
        }

        protected override void ProcessDlpaid(Dlpaid dlpaid)
        {
            base.ProcessDlpaid(dlpaid);

            dlpaid.AuthCode = "999";
            dlpaid.AuthType = AuthorizationType.Online;
        }
    }
}
