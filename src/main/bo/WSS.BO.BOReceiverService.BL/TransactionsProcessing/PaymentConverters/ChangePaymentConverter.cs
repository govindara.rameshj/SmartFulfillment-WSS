﻿using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public class ChangePaymentConverter : PaymentConverter<ChangePayment>
    {
        public ChangePaymentConverter(ChangePayment payment)
            : base(payment)
        {
        }

        protected override int GetTenderType()
        {
            return TenderType.Change;
        }

        protected override void InnerValidate(ValidationContext<ChangePayment> validationContext)
        {
            base.InnerValidate(validationContext);
            validationContext.AreEqual(x => x.Direction, Direction.TO_CUSTOMER, "Change tender is always to customer");
        }
    }
}
