﻿using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public class GiftCardPaymentConverter : PaymentConverter<GiftCardPayment>
    {
        public GiftCardPaymentConverter(GiftCardPayment payment)
            : base(payment)
        {
        }

        protected override void InnerConvert(ITransactionProcessingContext ctx, short paymentIndex)
        {
            base.InnerConvert(ctx, paymentIndex);

            var tran = ctx.GetTransaction();

            var dlcomm = new Dlcomm
            {
                CreateDate = tran.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(tran.TillNumber),
                PaymentIndex = paymentIndex,
            };

            ctx.StagingDb.MainData.Dlcommes.Add(dlcomm);

            DailyGiftCard giftCard = new DailyGiftCard
            {
                CreateDate = tran.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(tran.TillNumber),
                CashierNumber = tran.CashierNumber,
            };

            ProcessDlGiftCard(giftCard);
            ctx.StagingDb.MainData.DlGiftCards.Add(giftCard);
        }

        protected override void ProcessDlpaid(Dlpaid dlpaid)
        {
            base.ProcessDlpaid(dlpaid);

            dlpaid.AuthCode = Payment.GiftCardOperation.AuthCode;
            dlpaid.AuthType = ModelMapper.GetAuthorizationTypeCode(Payment.CardAcceptType);
            dlpaid.CardDescription = Payment.CardDescription;
            dlpaid.TransactionUid = "0000";
            dlpaid.CardNumber = "000000000######0000";
            dlpaid.RtiFlag = RtiStatus.NotSet;
        }

        protected override int GetTenderType()
        {
            return TenderType.GiftCard;
        }

        protected virtual void ProcessDlGiftCard(DailyGiftCard giftCard)
        {
            giftCard.AuthCode = Payment.GiftCardOperation.AuthCode;
            giftCard.CardNumber = Payment.GiftCardOperation.CardNumber;
            giftCard.RtiFlag = RtiStatus.NotSet;
            giftCard.TransactionType = TransactionTypeForCard.TenderInSale;
            giftCard.TenderAmount = Payment.Direction == Direction.FROM_CUSTOMER ? Payment.AbsAmount : -Payment.AbsAmount;
        }
    }
}
