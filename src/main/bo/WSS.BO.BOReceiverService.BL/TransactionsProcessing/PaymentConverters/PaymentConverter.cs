﻿using Cts.Oasys.Core.Helpers;
using slf4net;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public abstract class PaymentConverter<T> : IPaymentConverter
        where T: Payment
    {
        protected static readonly ILogger Log = LoggerFactory.GetLogger(typeof(PaymentConverter<T>));
        protected string ConverterName;

        protected T Payment { get; private set; }

        protected PaymentConverter(T payment)
        {
            Payment = payment;
            ConverterName = GetType().Name;
        }

        public void Convert(ITransactionProcessingContext ctx, short paymentIndex)
        {
            Log.Info("{0}.Convert is called", ConverterName);
            InnerConvert(ctx, paymentIndex);
            Log.Info("{0}.Convert is completed", ConverterName);
        }

        public void ProcessCashierBalanceTenderUpdate(CashBalCashierTen tenderUpdate)
        {
            Log.Info("{0}.ProcessCashierBalanceTenderUpdate is called", ConverterName);
            InnerProcessCashierBalanceTenderUpdate(tenderUpdate);
            Log.Info("{0}.ProcessCashierBalanceTenderUpdate is completed", ConverterName);
        }

        public void Validate(ValidationContextCreator<Payment> contextCreator)
        {
            Log.Info("{0}.Validate is called", ConverterName);
            var validationContext = contextCreator.Create<T>();
            InnerValidate(validationContext);
            Log.Info("{0}.Validate is completed", ConverterName);
        }

        protected virtual void InnerValidate(ValidationContext<T> validationContext)
        {
            validationContext.IsNotNegative(x => x.AbsAmount);
        }

        protected virtual void InnerConvert(ITransactionProcessingContext ctx, short paymentIndex)
        {
            var tran = ctx.GetTransaction();
            var dlpaid = new Dlpaid
            {
                CreateDate = tran.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(tran.TillNumber),
                PaymentIndex = paymentIndex
            };

            ProcessDlpaid(dlpaid);
            ctx.StagingDb.MainData.Dlpaids.Add(dlpaid);
        }

        protected virtual void InnerProcessCashierBalanceTenderUpdate(CashBalCashierTen tenderUpdate)
        {
            tenderUpdate.TenderTypeId = GetTenderType();
        }

        protected abstract int GetTenderType();

        protected virtual void ProcessDlpaid(Dlpaid dlpaid)
        {
            dlpaid.Amount = Payment.Direction == Direction.FROM_CUSTOMER ? -Payment.AbsAmount : Payment.AbsAmount;
            dlpaid.TenderType = GetTenderType();
        }
    }
}
