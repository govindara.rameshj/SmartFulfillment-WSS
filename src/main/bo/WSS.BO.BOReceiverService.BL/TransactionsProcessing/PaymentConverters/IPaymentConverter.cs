﻿using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public interface IPaymentConverter
    {
        void Convert(ITransactionProcessingContext ctx, short paymentIndex);
        void ProcessCashierBalanceTenderUpdate(CashBalCashierTen tenderUpdate);
        void Validate(ValidationContextCreator<Payment> creator);
    }
}
