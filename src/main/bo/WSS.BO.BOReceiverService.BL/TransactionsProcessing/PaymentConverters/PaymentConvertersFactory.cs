﻿using System;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    class PaymentConvertersFactory
    {
        public IPaymentConverter CreatePaymentConverter(Payment payment)
        {
            if (payment is CashPayment)
            {
                return new CashPaymentConverter((CashPayment)payment);
            }
            if (payment is ChequePayment)
            {
                return new ChequePaymentConverter((ChequePayment)payment);
            }
            if (payment is EftPayment)
            {
                return new EftPaymentConverter((EftPayment)payment);
            }
            if (payment is ChangePayment)
            {
                return new ChangePaymentConverter((ChangePayment)payment);
            }
            if (payment is GiftCardPayment)
            {
                return new GiftCardPaymentConverter((GiftCardPayment)payment);
            }

            throw new ArgumentException(String.Format("Unknown payment type : {0}", payment.GetType()));
        }
    }
}
