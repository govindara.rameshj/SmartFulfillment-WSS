﻿using Cts.Oasys.Core.Helpers;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public class EftPaymentConverter : PaymentConverter<EftPayment>
    {
        public EftPaymentConverter(EftPayment payment)
            : base(payment)
        {
        }

        protected override void InnerConvert(ITransactionProcessingContext ctx, short paymentIndex)
        {
            base.InnerConvert(ctx, paymentIndex);

            var tran = ctx.GetTransaction();

            var dlcomm = new Dlcomm
            {
                CreateDate = tran.CreateDateTime.Date,
                TillNumber = ConversionUtils.GetCanonicalTillNumber(tran.TillNumber),
                PaymentIndex = paymentIndex
            };

            ProcessDlcomm(dlcomm);
            ctx.StagingDb.MainData.Dlcommes.Add(dlcomm);
        }

        protected override int GetTenderType()
        {
            return ModelMapper.GetTenderTypeCode(Payment.PaymentCard.CardType);
        }

        protected override void ProcessDlpaid(Dlpaid dlpaid)
        {
            base.ProcessDlpaid(dlpaid);

            dlpaid.AuthCode = Payment.AuthCode;
            dlpaid.AuthDescription = Payment.AuthDescription;
            dlpaid.AuthType = ModelMapper.GetAuthorizationTypeCode(Payment.CardAcceptType);
            dlpaid.CardAcceptType = Payment.CardAcceptType == CardAcceptType.KEYED;

            var paymentCard = Payment.PaymentCard;
            dlpaid.CardDescription = paymentCard.CardDescription;
            dlpaid.CardExpireDate = ConversionUtils.MonthToOasysString(paymentCard.CardExpireDate);
            dlpaid.CardNumber = paymentCard.MaskedCardNumber;
            dlpaid.CardStartDate = paymentCard.CardStartDate != null ? ConversionUtils.MonthToOasysString(paymentCard.CardStartDate.Value) : Global.NullPaymentCardDate;
            dlpaid.IssueNumberSwitch = paymentCard.IssueNumberSwitch ?? string.Empty;

            dlpaid.MerchantNumber = Payment.MerchantNumber;
            dlpaid.TransactionUid = Payment.TransactionUid;
        }

        protected virtual void ProcessDlcomm(Dlcomm dlcomm)
        {
            dlcomm.RtiFlag = RtiStatus.NotSet;
            dlcomm.Spare = null;
            dlcomm.PanHash = Payment.PaymentCard.PanHash;
            dlcomm.TokenId = Payment.TokenId;
        }

        protected override void InnerValidate(ValidationContext<EftPayment> validationContext)
        {
            base.InnerValidate(validationContext);

            validationContext.IsNotEmpty(x => x.TokenId, "Card Token field should be specified for Card Payment", false);
            validationContext.IsNotEmpty(x => x.TransactionUid, "Eft Serial Number should be specified for Card Payment", false);

            var paymentCardContext = validationContext.CreateChildContext(x => x.PaymentCard);

            paymentCardContext.HasEmptyTimePart(x => x.CardStartDate, "Non-zero Time part is not allowed for Card Start Date");
            paymentCardContext.HasEmptyTimePart(x => x.CardExpireDate, "Non-zero Time part is not allowed for Card Expire Date");

            paymentCardContext.AreNotEqual(x => x.CardType, Model.Enums.CardType.UNKNOWN, "Card Type UNKNOWN is not allowed for Eft Payment");
        }
    }
}
