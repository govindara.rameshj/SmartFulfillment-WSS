﻿using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters
{
    public class CashPaymentConverter : PaymentConverter<CashPayment>
    {
        public CashPaymentConverter(CashPayment payment)
            : base(payment)
        {
        }

        protected override int GetTenderType()
        {
            return TenderType.Cash;
        }
    }
}
