﻿using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    class UpdateRecalledTransactionRule : BusinessRule<RetailTransaction>
    {
        protected override bool IsApplicable(RetailTransaction tran)
        {
            return !string.IsNullOrEmpty(tran.RecalledTransactionId);
        }

        protected override void ApplyToTyped(RetailTransaction tran, ITransactionProcessingContext ctx)
        {
            var dltotsUpdate = new DltotsUpdate
            {
                IsParked = false,
                IsRecalled = true,
                SourceTransactionId = tran.RecalledTransactionId,
                SaveStatus = "20",
                Source = tran.Source,
                StatusSequence = "0001"
            };

            ctx.StagingDb.MainData.DltotsUpdate = dltotsUpdate;
        }
    }
}
