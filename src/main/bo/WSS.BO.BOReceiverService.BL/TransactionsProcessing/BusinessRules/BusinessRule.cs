﻿using slf4net;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    abstract class BusinessRule<T> : IBusinessRule
        where T: Transaction
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(BusinessRule<T>));

        public void Apply(ITransactionProcessingContext ctx)
        {
            var ruleName = GetType().Name;
            Log.Info("Start {0} applying", ruleName);

            var typedTran = ctx.GetTransaction() as T;
            if (typedTran != null)
            {
                if (IsApplicable(typedTran))
                {
                    Log.Info("{0} is applicable", ruleName);
                    ApplyToTyped(typedTran, ctx);
                    Log.Info("{0} is applied", ruleName);
                }
                else
                {
                    Log.Info("{0} is not applicable", ruleName);
                }
            }
            else
            {
                Log.Info("Incorrect transaction type for {0}", ruleName);
            }
        }

        protected abstract bool IsApplicable(T tran);
        protected abstract void ApplyToTyped(T tran, ITransactionProcessingContext ctx);
    }
}
