﻿using System.Collections.Generic;
using System.Linq;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    class VatsDenormalizationRule : BusinessRule<PayableTransaction>
    {
        protected override bool IsApplicable(PayableTransaction tran)
        {
            //In Void or Parked transactions VatValues are 0
            return tran.TransactionStatus == TransactionStatus.COMPLETED;
        }

        protected override void ApplyToTyped(PayableTransaction tran, ITransactionProcessingContext ctx)
        {
            var vatRates = ctx.ReadOnlyDataRetriever.GetVatRates();

            int sizeOfListForVats = vatRates.Count() + 1; // dont use 0th element
            var vatValues = new List<decimal>(Enumerable.Repeat(0M, sizeOfListForVats));
            var exVatValues = new List<decimal>(Enumerable.Repeat(0M, sizeOfListForVats));

            var retailTransaction = tran as RetailTransaction;
            if (retailTransaction != null)
            {
                foreach (var tranItem in retailTransaction.Items)
                {
                    var line = ctx.StagingDb.MainData.GetDailyTillLine(tranItem);
                    // line.VatValue is already set by converter

                    var vatRate = vatRates.GetByRateValue(tranItem.VatRate);
                    var code = vatRate.Code;

                    line.VatCode = code;
                    line.VatRate = vatRate.Symbol.ToString();

                    if (!tranItem.IsReversed)
                    {
                        exVatValues[code] += retailTransaction.CalculateChargedAbsAmount(tranItem.Product) - tranItem.AbsVatAmount;
                        vatValues[code] += tranItem.AbsVatAmount;
                    }
                }
            }

            var dlTran = ctx.StagingDb.MainData.Dltots;

            foreach (var vatRate in vatRates)
            {
                var code = vatRate.Code;
                dlTran.SetVatRate(code, vatRate.Value);
                dlTran.SetVatValue(code, vatValues[code]);
                dlTran.SetExVatValue(code, exVatValues[code]);
            }
        }
    }
}
