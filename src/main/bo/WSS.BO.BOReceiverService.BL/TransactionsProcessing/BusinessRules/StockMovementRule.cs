using System;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    internal class StockMovementRule : BusinessRule<RetailTransaction>
    {
        protected override bool IsApplicable(RetailTransaction tran)
        {
            return tran.TransactionStatus == TransactionStatus.COMPLETED;
        }

        protected override void ApplyToTyped(RetailTransaction tran, ITransactionProcessingContext ctx)
        {
            foreach (var tranItem in tran.Items.Where(x => !x.IsReversed))
            {
                var skuProduct = tranItem.Product as SkuProduct;
                if (skuProduct != null)
                {
                    var skuPrice = tran.CalculatePrice(skuProduct);
                    var stockUpdate = CreateStockUpdate(tranItem, skuProduct, ctx, skuPrice);
                    ctx.StagingDb.MainData.StockUpdates.Add(tranItem.ItemIndex, stockUpdate);
                }
            }
        }

        private StockUpdate CreateStockUpdate(RetailTransactionItem tranItem, SkuProduct skuProduct, ITransactionProcessingContext ctx, decimal skuPrice)
        {
            var update = new StockUpdate
            {
                SkuNumber = skuProduct.BrandProductId,
                CashierNumber = ConversionUtils.IntValueToCharField(ctx.GetTransaction().CashierNumber, 3),
                TypeOfMovement = CalculateTypeOfMovement(tranItem, skuProduct),
                DateOfMovement = ctx.EffectiveProcessingDate.Date,
                TimeOfMovement = ConversionUtils.TimeToOasysString(ctx.EffectiveProcessingDate),
                RtiFlag = RtiStatus.NotSet,
                DayNumber = ConversionUtils.GetDaysSinceStartOf1900(DateTime.Now.Date),
                StockOnHandDelta = GetStockOnHandDelta(skuProduct, tranItem.Direction),
                MarkdownStockDelta = GetMarkdownStockDelta(skuProduct, tranItem.Direction),
                AdjustmentsValue1Delta = -GetMarkdownStockDelta(skuProduct, tranItem.Direction)
            };

            if (skuProduct.StockType == StockType.MARKDOWN)
            {
                SetSalesValues(update, skuProduct, tranItem.Direction, skuPrice);
            }

            return update;
        }

        /// <summary>
        /// Calculates type of movement for tranItem based on some tranItem fields.
        /// </summary>
        private string CalculateTypeOfMovement(RetailTransactionItem tranItem, SkuProduct skuProduct)
        {
            if (tranItem.Direction == Direction.TO_CUSTOMER)
            {
                if (skuProduct.StockType == StockType.NORMAL)
                {
                    return TypeOfMovement.NormalStockSaleItem;
                }
                else
                {
                    return TypeOfMovement.MarkdownStockSaleItem;
                }
            }
            return TypeOfMovement.NormalStockRefundItem;
        }

        private decimal GetStockOnHandDelta(SkuProduct skuProduct, Direction direction)
        {
            if (skuProduct.StockType == StockType.NORMAL)
            {
                return direction == Direction.TO_CUSTOMER ? -skuProduct.AbsQuantity : skuProduct.AbsQuantity;
            }
            else
            {
                return 0;
            }
        }

        private decimal GetMarkdownStockDelta(SkuProduct skuProduct, Direction itemDirection)
        {
            if (skuProduct.StockType == StockType.MARKDOWN)
            {
                return itemDirection == Direction.TO_CUSTOMER ? -skuProduct.AbsQuantity : skuProduct.AbsQuantity;
            }
            else
            {
                return 0;
            }
        }

        private void SetSalesValues(StockUpdate stockUpdate, SkuProduct skuProduct, Direction itemDirection, decimal price)
        {
            var skuQuantity = itemDirection == Direction.TO_CUSTOMER ? skuProduct.AbsQuantity : -skuProduct.AbsQuantity;

            stockUpdate.SalesUnits1Delta = skuQuantity;
            stockUpdate.SalesUnits2Delta = skuQuantity;
            stockUpdate.SalesUnits4Delta = skuQuantity;
            stockUpdate.SalesUnits6Delta = skuQuantity;

            stockUpdate.SalesValue1Delta = price * skuQuantity;
            stockUpdate.SalesValue2Delta = price * skuQuantity;
            stockUpdate.SalesValue4Delta = price * skuQuantity;
            stockUpdate.SalesValue6Delta = price * skuQuantity;
        }
    }
}
