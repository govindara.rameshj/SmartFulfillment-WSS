﻿using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    class RtiFlagToBeSentRule : BusinessRule<Transaction>
    {
        protected override bool IsApplicable(Transaction tran)
        {
            return true;
        }

        protected override void ApplyToTyped(Transaction tran, ITransactionProcessingContext ctx)
        {
            //will add dleanchik and dlcoupon later
            var data = ctx.StagingDb.MainData;

            var toUpdate = Enumerable.Empty<ISyncronizedToRti>()
                .ConcatCasted(data.Dlcommes)
                .ConcatCasted(data.Dlrcuses)
                .ConcatCasted(data.Dlevnts)
                .ConcatCasted(data.Dlanases)
                .ConcatCasted(data.Dlgifts)
                .ConcatCasted(data.Dlocuses)
                .ConcatCasted(data.Dlolins)
                .ConcatCasted(data.Dlrejects)
                .ConcatCasted(data.Dltots.ToEnumerable())
                .ConcatCasted(data.Dlpaids)
                .ConcatCasted(data.GetDailyTillLines())
                .ConcatCasted(data.StockUpdates.Values.ToList())
                .ConcatCasted(data.CompensatingDllines)
                .ConcatCasted(data.DlGiftCards)
                .ConcatCasted(data.Dlcoupons);

            foreach (var item in toUpdate)
            {
                item.RtiFlag = RtiStatus.ToBeSent;
            }
        }
    }
}
