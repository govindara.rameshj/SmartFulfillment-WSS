﻿using System;
using System.Collections.Generic;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.PaymentConverters;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.ProductConverters;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    internal class UpdateCashierBalanceRule : BusinessRule<VoidableTransaction>
    {
        protected override bool IsApplicable(VoidableTransaction tran)
        {
            return true;
        }

        protected override void ApplyToTyped(VoidableTransaction tran, ITransactionProcessingContext ctx)
        {
            int? virtualCashierId = ctx.ReadOnlyDataRetriever.GetVirtualCashierId(Int32.Parse(tran.TillNumber));
            if (virtualCashierId == null)
            {
                throw new ApplicationException(String.Format("Virtual cashier Id is not found for till [{0}]", tran.TillNumber));
            }

            var update = new CashBalCashier
            {
                PeriodId = ctx.ReadOnlyDataRetriever.GetTradingPeriodId(ctx.EffectiveProcessingDate.Date),
                CashierId = virtualCashierId.Value,
                CurrencyId = ctx.ReadOnlyDataRetriever.GetDefaultSystemCurrency(),
            };

            ProcessCashierBalanceUpdate(update, tran, ctx);

            List<CashBalCashierTen> tenderUpdates = new List<CashBalCashierTen>();
            List<CashBalCashierTenVar> tenderVarianceUpdates = new List<CashBalCashierTenVar>();

            var payableTransaction = tran as PayableTransaction;
            if (payableTransaction != null)
            {
                foreach (var payment in payableTransaction.Payments)
                {
                    var tenderUpdate = CreateTenderUpdate(payment, update);
                    tenderUpdates.Add(tenderUpdate);

                    var tenderVarianceUpdate = CreateTenderVarianceUpdate(tenderUpdate, ctx);
                    tenderVarianceUpdates.Add(tenderVarianceUpdate);
                }
            }
            ctx.StagingDb.MainData.AddCashierBalanceUpdate(update, tenderUpdates, tenderVarianceUpdates);
        }

        private static CashBalCashierTenVar CreateTenderVarianceUpdate(CashBalCashierTen tenderUpdate, ITransactionProcessingContext ctx)
        {
            var tenderVarianceUpdate = new CashBalCashierTenVar
            {
                PeriodId = ctx.ReadOnlyDataRetriever.GetTradingPeriodId(ctx.GetTransaction().CreateDateTime.Date),
                CashierId = tenderUpdate.CashierId,
                CurrencyId = tenderUpdate.CurrencyId,
                TradingPeriodId = tenderUpdate.PeriodId,

                TenderTypeId = tenderUpdate.TenderTypeId,
                Quantity = tenderUpdate.Quantity,
                Amount = tenderUpdate.Amount
            };
            return tenderVarianceUpdate;
        }

        private static CashBalCashierTen CreateTenderUpdate(Payment payment, CashBalCashier cashier)
        {
            var factory = new PaymentConvertersFactory();
            var converter = factory.CreatePaymentConverter(payment);

            var tenderUpdate = new CashBalCashierTen
            {
                PeriodId = cashier.PeriodId,
                CashierId = cashier.CashierId,
                CurrencyId = cashier.CurrencyId,
                Quantity = 1,
                Amount = payment.Direction == Direction.FROM_CUSTOMER ? payment.AbsAmount : -payment.AbsAmount,
            };

            converter.ProcessCashierBalanceTenderUpdate(tenderUpdate); // Set TenderTypeId, actually.
            return tenderUpdate;
        }

        private void ProcessCashierBalanceUpdate(CashBalCashier update, VoidableTransaction tran, ITransactionProcessingContext ctx)
        {
            if (tran.TransactionStatus != TransactionStatus.COMPLETED)
            {
                update.NumVoids = 1;
            }
            else
            {
                UpdateCommonAmounts(update, tran, ctx);

                var factory = new TransactionConvertersFactory();
                var converter = factory.CreateTransactionConverter(ctx);
                converter.ProcessCashierBalanceUpdate(update);
            }
        }

        private void UpdateCommonAmounts(CashBalCashier update, VoidableTransaction tran, ITransactionProcessingContext ctx)
        {
            var totalDiscount = 0.0M;
            var linesReversed = 0;
            var linesSold = 0;
            var linesScanned = 0;

            var transaction = tran as RetailTransaction;
            if (transaction != null)
            {
                var factory = new ProductConvertersFactory((ITransactionProcessingContext<RetailTransaction>)ctx);

                foreach (var tranItem in transaction.Items)
                {
                    var productConverter = factory.CreateProductConverter(tranItem.Product);
                    var sku = productConverter.GetSku();

                    string saleType = ctx.ReadOnlyDataRetriever.GetSkuInfo(sku).SaleTypeAttribute;

                    if (saleType != SkuSaleTypeAttribute.W)
                    {
                        if (tranItem.IsReversed)
                        {
                            linesReversed += 1;
                        }
                        else
                        {
                            if (tranItem.Direction == Direction.TO_CUSTOMER)
                            {
                                if (saleType != SkuSaleTypeAttribute.DeliveryItem &&
                                    saleType != SkuSaleTypeAttribute.V)
                                {
                                    linesSold += 1;
                                }
                                if (productConverter.IsScanned())
                                {
                                    linesScanned += 1;
                                }
                            }
                        }
                    }
                }

                totalDiscount = (transaction.CalculateEmployeeDiscountAmount() + transaction.CalculateEventDiscountsAmount()) * (-1);
            }

            update.DiscountAmount = totalDiscount;
            update.NumLinesSold = linesSold;
            update.NumLinesReversed = linesReversed;
            update.NumLinesScanned = linesScanned;
        }
    }
}
