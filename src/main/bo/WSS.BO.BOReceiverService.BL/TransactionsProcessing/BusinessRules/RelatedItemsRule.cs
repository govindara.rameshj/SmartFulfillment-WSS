﻿using Cts.Oasys.Core.Helpers;
using System;
using System.Linq;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    class RelatedItemsRule : BusinessRule<RetailTransaction>
    {
        protected override bool IsApplicable(RetailTransaction tran)
        {
            return tran.Items.Any(x => x.ItemRefund != null && !string.IsNullOrEmpty(x.ItemRefund.RefundedTransactionId) && x.Product is SkuProduct);
        }

        protected override void ApplyToTyped(RetailTransaction tran, ITransactionProcessingContext ctx)
        {
            var refundedItems = tran.Items.Where(x => x.ItemRefund != null && !string.IsNullOrEmpty(x.ItemRefund.RefundedTransactionId) && x.Product is SkuProduct);

            foreach (var item in refundedItems)
            {
                var dailyTillTranKey = GetDailyTillTranKey(ctx, item.ItemRefund.RefundedTransactionId, tran.Source);

                if (dailyTillTranKey != null)
                {
                    var initialLine = ctx.RuntimeDataRetriever.SelectDailyLineBySourceItemId(dailyTillTranKey, item.ItemRefund.RefundedTransactionItemId);
                    var refundLine = ctx.StagingDb.MainData.GetDailyTillLine(item);

                    if (IsItemSingleItemFromPackage(initialLine, refundLine))
                    {
                        UpdateSingleItemFromPackage(ctx, initialLine, refundLine);
                    }
                }
            }
        }

        private void UpdateSingleItemFromPackage(ITransactionProcessingContext ctx, DailyTillLine initialLine, DailyTillLine refundLine)
        {
            var relatedItem = ctx.RuntimeDataRetriever.SelectRelatedItem(initialLine.SkuNumber);

            if (relatedItem != null && relatedItem.SingleItemNumber == refundLine.SkuNumber)
            {
                var price = Math.Round(ctx.RuntimeDataRetriever.GetStockMasterPrice(initialLine.SkuNumber) / relatedItem.NumberOfSinglesPerPack, 2);

                refundLine.PriceOverrideChangePriceDifference -= refundLine.InitialPrice - price;

                if (refundLine.PriceOverrideChangePriceDifference == 0)
                {
                    refundLine.InitialPrice = price;
                    refundLine.PriceOverrideCode = System.Convert.ToInt16(PriceOverrideReasonCode.NoPriceOverride);
                    refundLine.PriceOverrideMarginErosionCode = ConversionUtils.ToEventCode(Global.NullEventCode);
                }
            }
        }

        private IDailyTillTranKey GetDailyTillTranKey(ITransactionProcessingContext ctx, string refundedTransactionId, string source)
        {
            IDailyTillTranKey dailyTillTranKey = null;

            if (LegacyReceiptBarcodeHelper.IsRefundForLegacyTransaction(refundedTransactionId))
            {
                dailyTillTranKey = ctx.GetLegacyTransactionDailyTillTranKey(refundedTransactionId);
            }
            else
            {
                dailyTillTranKey = ctx.GetDailyTillTranKeyBySource(refundedTransactionId, source);
            }

            return dailyTillTranKey;
        }

        private bool IsItemSingleItemFromPackage(DailyTillLine initialLine, DailyTillLine refundLine)
        {
            return initialLine != null && refundLine != null ? refundLine.SkuNumber != initialLine.SkuNumber : false;
        }
    }
}
