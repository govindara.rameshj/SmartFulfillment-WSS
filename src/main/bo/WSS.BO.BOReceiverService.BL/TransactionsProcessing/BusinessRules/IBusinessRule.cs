﻿namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    public interface IBusinessRule
    {
        void Apply(ITransactionProcessingContext ctx);
    }
}
