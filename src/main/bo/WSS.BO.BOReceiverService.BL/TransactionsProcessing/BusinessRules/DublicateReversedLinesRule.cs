﻿using System.Linq;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules
{
    class DublicateReversedLinesRule : BusinessRule<RetailTransaction>
    {
        protected override bool IsApplicable(RetailTransaction tran)
        {
            return true; //we should always process reversed lines
        }

        protected override void ApplyToTyped(RetailTransaction tran, ITransactionProcessingContext ctx)
        {
            int lastLineSequenceNumber = ctx.StagingDb.MainData.GetDailyTillLines()
                .Select(x => x.SequenceNumber)
                .DefaultIfEmpty().Max();

            foreach (var item in tran.Items.Where(item => item.IsReversed))
            {
                var originalLine = ctx.StagingDb.MainData.GetDailyTillLine(item);

                var reversedLine = originalLine.Clone();

                ++lastLineSequenceNumber;

                reversedLine.SequenceNumber = lastLineSequenceNumber;
                reversedLine.Amount = -originalLine.Amount;
                reversedLine.Quantity = -originalLine.Quantity;
                reversedLine.VatValue = -originalLine.VatValue;

                ctx.StagingDb.MainData.CompensatingDllines.Add(reversedLine);
            }
        }
    }
}
