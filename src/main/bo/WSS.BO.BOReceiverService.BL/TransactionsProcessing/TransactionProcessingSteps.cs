﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Hubs.Core.System.EndOfDayLock;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.TransactionConverters;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.BusinessRules;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.TransactionsProcessing
{
    public class TransactionProcessingSteps : ITransactionProcessingSteps
    {
        private readonly IDataLayerFactory _dataLayerFactory;
        private readonly IReadOnlyDataRetriever _readOnlyDataRetriever;
        private readonly IRuntimeDataRetriever _runtimeDataRetriever;
        private readonly IConfiguration _cfg;
        private readonly ISystemEnvironment _environment;
        private readonly RequestDecisionEngine _engine;

        private Transaction _transaction;
        private ITransactionProcessingContext _mainContext;
        private IList<ITransactionProcessingContext> _childContexts;

        public TransactionProcessingSteps(IDataLayerFactory dataLayerFactory, ISystemEnvironment environment, IReadOnlyDataRetriever readOnlyDataRetriever, IRuntimeDataRetriever runtimeDataRetriever, IConfiguration cfg)
        {
            _dataLayerFactory = dataLayerFactory;
            _readOnlyDataRetriever = readOnlyDataRetriever;
            _runtimeDataRetriever = runtimeDataRetriever;
            _cfg = cfg;
            _environment = environment;

            _engine = new RequestDecisionEngine(new CachedRequestDecisionEngineDatabaseSql(dataLayerFactory, _readOnlyDataRetriever, environment), environment);
        }

        #region ITransactionProcessingSteps implementation

        public void SetTransaction(Transaction transaction)
        {
            _transaction = transaction;
        }

        public Transaction GetTransaction()
        {
            return _transaction;
        }

        public void CheckForDublicates()
        {
            var repo = _dataLayerFactory.Create<IDailyTillRepository>();
            if (repo.IsDltotsExists(_transaction.SourceTransactionId, _transaction.Source))
            {
                throw new DuplicateTransactionException();
            }
        }

        public void CheckForLocksBeforeProcessing()
        {
            CheckForEodLock();

            int virtualCashierId = GetVirtualCashierId(_transaction.TillNumber);
            CheckForPickUpLock(virtualCashierId);
        }

        public void CheckForLocksBeforeSavingToDb()
        {
            CheckForEodLock();
        }

        public void CreateContext()
        {
            var receivedWhen = _environment.GetDateTimeNow();
            var effectiveProcessingDate = receivedWhen;
            if (EodLockHasBeenExecutedForDate(receivedWhen))
            {
                effectiveProcessingDate = effectiveProcessingDate.AddDays(1);
            }
            
            _mainContext = new TransactionProcessingContextFactory().CreateMainContext(_transaction, receivedWhen, effectiveProcessingDate, _readOnlyDataRetriever, _runtimeDataRetriever, _cfg);
        }

        public void PreProcess()
        {
            var factory = new TransactionConvertersFactory();
            var processor = factory.CreateTransactionConverter(_mainContext);
            processor.PreProcess();
        }

        public void Validate()
        {
            var factory = new TransactionConvertersFactory();
            var processor = factory.CreateTransactionConverter(_mainContext);
            var errors = processor.Validate();
            if (errors.Any())
            {
                throw new ValidationException("Incoming request fields are not valid.", errors);
            }
        }

        public void EmulateLegacyTransactionSplitting()
        {
            LegacyTransactionSplittingEmulator emulator = new LegacyTransactionSplittingEmulator(_mainContext);
            _childContexts = emulator.TryPerformSplit();
        }

        public void Convert()
        {
            var factory = new TransactionConvertersFactory();
            foreach (var ctx in _childContexts)
            {
                ITransactionConverter converter = factory.CreateTransactionConverter(ctx);
                converter.Convert();
            }
        }

        public void SaveToDb()
        {
            using (var unitOfWork = _dataLayerFactory.CreateUnitOfWork())
            {
                unitOfWork.SetDeadlockPriority(_cfg.DeadlockPriorityForSavingTransaction);
                foreach (var ctx in _childContexts)
                {
                    ctx.StagingDb.Save(unitOfWork);
                }
                unitOfWork.Commit();
            }
        }

        public void ApplyBusinessRules()
        {
            var businessRules = GetBusinessRules();
            foreach (var ctx in _childContexts)
            {
                foreach (var businessRule in businessRules)
                {
                    businessRule.Apply(ctx);
                }
            }
        }

        #endregion

        #region Internal

        private List<IBusinessRule> GetBusinessRules()
        {
            var businessRules = new List<IBusinessRule>
            {
                new RelatedItemsRule(),
                new VatsDenormalizationRule(),
                new DublicateReversedLinesRule(),
                new StockMovementRule(),
                new UpdateCashierBalanceRule(),
                new RtiFlagToBeSentRule(),
                new UpdateRecalledTransactionRule()
            };
            return businessRules;
        }

        private void CheckForEodLock()
        {
            if (!_engine.AcceptRequest())
            {
                throw new EodLockException();
            }
        }

        private int GetVirtualCashierId(string tillNumber)
        {
            var cashier = _readOnlyDataRetriever.GetVirtualCashierId(Int32.Parse(tillNumber));
            if (cashier == null)
            {
                var error = new ValidationError
                {
                    ErrorMessage = String.Format("Virtual cashier Id is not found for till [{0}]", tillNumber),
                    PropertyPath = "VirtualCashier"
                };

                List<ValidationError> errors = new List<ValidationError> { error };
                throw new ValidationException("Incoming request fields are not valid.", errors);
            }

            return cashier.Value;
        }

        private void CheckForPickUpLock(int virtualCashierId)
        {
            if (PickupIsInProgress(virtualCashierId))
            {
                throw new PickupException();
            }
        }

        private bool PickupIsInProgress(int virtualCashierNumber)
        {
            var repo = _dataLayerFactory.Create<NitmasRepository>();
            return repo.IsPickupInProgress(virtualCashierNumber);
        }

        private bool EodLockHasBeenExecutedForDate(DateTime dateToCheck)
        {
            var repo = _dataLayerFactory.Create<NitmasRepository>();
            return repo.WasEodLockExecutedForDate(dateToCheck);
        }

        #endregion
    }
}
