﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cts.Oasys.Core.HierarchyNavigation;
using Cts.Oasys.Core.HierarchyNavigation.Context;
using Cts.Oasys.Core.HierarchyNavigation.Propertites;
using slf4net;
using WSS.BO.BOReceiverService.BL.Configuration;

namespace WSS.BO.BOReceiverService.BL.PreProcessing
{
    public class PreProcessingContext<TEntity> : HierarchyNavigationContext<TEntity>
    {
        private readonly IConfiguration cfg;
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(PreProcessingContext<TEntity>));

        #region Init

        public PreProcessingContext(TEntity entity, IConfiguration cfg)
            : base(entity)
        {
            this.cfg = cfg;
        }

        private PreProcessingContext(HierarchyNavigator<TEntity> navigator, IConfiguration cfg)
            : base(navigator)
        {
            this.cfg = cfg;
        }

        #endregion

        public void CheckAndTruncate(Expression<Func<TEntity, string>> memberAccessExpression, int maxLength)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var value = property.GetValue();
            Truncate(maxLength, value, property);
        }

        public void CheckAndTruncate(int maxLength)
        {
            string value = Entity != null ? Entity.ToString() : null;
            Truncate(maxLength, value, (IPropertyAccessor<string>)Navigator.ParentProperty);
        }

        private void Truncate(int maxLength, string value, IPropertyAccessor<string> property)
        {
            if (value != null && value.Length > maxLength)
            {
                if (cfg.TruncateTooLongStrings)
                {
                    Log.Warn(String.Format("{0} was too long and was truncated to {1} symbols",
                        property.HierarchyFullName,
                        maxLength));
                    property.SetValue(value.Substring(0, maxLength));
                }
                else
                {
                    Log.Warn(String.Format("{0} exceeds {1} symbols but it was not truncated as TruncateTooLongStrings=False",
                        property.HierarchyFullName,
                        maxLength));
                }
            }
        }

        public void NullListToEmpty<TValue>(Expression<Func<TEntity, IList<TValue>>> memberAccessExpression)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            if (property.GetValue() == null)
            {
                property.SetValue(new List<TValue>());
            }
        }

        #region Navigation

        public new PreProcessingContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return new PreProcessingContextCreator<TChildEntity>(base.GetChildContextCreator(memberAccessExpression, index));
        }

        public new PreProcessingContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return (PreProcessingContext<TChildEntity>)base.CreateChildContext(memberAccessExpression, index);
        }

        public new PreProcessingContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
            where TResultEntity : TChildEntity
        {
            return (PreProcessingContext<TResultEntity>)base.CreateChildContext<TResultEntity, TChildEntity>(memberAccessExpression, index);
        }

        public new PreProcessingContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return (PreProcessingContext<TChildEntity>)base.CreateChildContext(memberAccessExpression);
        }

        public new PreProcessingContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
            where TResultEntity : TChildEntity
        {
            return (PreProcessingContext<TResultEntity>)base.CreateChildContext<TResultEntity, TChildEntity>(memberAccessExpression);
        }

        public new PreProcessingContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return new PreProcessingContextCreator<TChildEntity>(base.GetChildContextCreator(memberAccessExpression));
        }

        protected override HierarchyNavigationContext<TChildEntity> InnerCreateChildContext<TChildEntity>(HierarchyNavigator<TChildEntity> childNavigator)
        {
            return childNavigator.Entity != null ? new PreProcessingContext<TChildEntity>(childNavigator, cfg) : null;
        }

        #endregion
    }
}
