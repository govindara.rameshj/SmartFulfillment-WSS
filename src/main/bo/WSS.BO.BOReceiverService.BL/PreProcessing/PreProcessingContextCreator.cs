using Cts.Oasys.Core.HierarchyNavigation.Context;

namespace WSS.BO.BOReceiverService.BL.PreProcessing
{
    public class PreProcessingContextCreator<TBaseEntity>
    {
        private readonly IHierarchyNavigationContextCreator<TBaseEntity> innerCreator;

        public PreProcessingContextCreator(IHierarchyNavigationContextCreator<TBaseEntity> innerCreator)
        {
            this.innerCreator = innerCreator;
        }

        public PreProcessingContext<T> Create<T>() where T : TBaseEntity
        {
            return (PreProcessingContext<T>)innerCreator.Create<T>();
        }
    }
}