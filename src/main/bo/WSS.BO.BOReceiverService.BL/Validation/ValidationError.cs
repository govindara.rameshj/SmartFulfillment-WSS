﻿namespace WSS.BO.BOReceiverService.BL.Validation
{
    public class ValidationError
    {
        public string PropertyPath { get; set; }
        public string ErrorMessage { get; set; }
    }
}