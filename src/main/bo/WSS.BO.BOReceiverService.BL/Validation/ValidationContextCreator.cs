using Cts.Oasys.Core.HierarchyNavigation.Context;

namespace WSS.BO.BOReceiverService.BL.Validation
{
    public class ValidationContextCreator<TBaseEntity>
    {
        private readonly IHierarchyNavigationContextCreator<TBaseEntity> innerCreator;

        public ValidationContextCreator(IHierarchyNavigationContextCreator<TBaseEntity> innerCreator)
        {
            this.innerCreator = innerCreator;
        }

        public ValidationContext<T> Create<T>() where T : TBaseEntity
        {
            return (ValidationContext<T>) innerCreator.Create<T>();
        }
    }
}