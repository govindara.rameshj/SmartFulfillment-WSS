using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cts.Oasys.Core.HierarchyNavigation;
using Cts.Oasys.Core.HierarchyNavigation.Context;
using Cts.Oasys.Core.HierarchyNavigation.Propertites;

namespace WSS.BO.BOReceiverService.BL.Validation
{
    public class ValidationContext<TEntity> : HierarchyNavigationContext<TEntity>
    {
        private readonly IList<ValidationError> errors;

        public ValidationContext(TEntity entity)
            : base(entity)
        {
            errors = new List<ValidationError>();
        }

        private ValidationContext(HierarchyNavigator<TEntity> navigator, IList<ValidationError> errors)
            : base(navigator)
        {
            this.errors = errors;
        }

        #region Validation assertions

        public void CheckPredicate<TValue>(Expression<Func<TEntity, TValue>> memberAccessExpression,
            Predicate<TValue> predicate,
            string errorMessageFormat)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (!predicate(actual))
            {
                AddError(property,
                    String.Format("Condition check failed for value [{0}]. {1}", actual, errorMessageFormat));
            }
        }

        public void AreEqual<TValue>(Expression<Func<TEntity, TValue>> memberAccessExpression,
            TValue expected,
            string errorMessageFormat)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (!expected.Equals(actual))
            {
                AddError(property,
                    String.Format("Expected: [{0}], actual [{1}]. {2}", expected, actual, String.Format(errorMessageFormat, expected)));
            }
        }

        public void HasLengthLessOrEqual(Expression<Func<TEntity, string>> memberAccessExpression,
            int expected,
            string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();

            if (actual != null && actual.Length > expected)
            {
                AddError(property, errorMessage);
            }
        }

        public void HasLengthEqual(Expression<Func<TEntity, string>> memberAccessExpression,
            int expected,
            string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();

            if (actual != null && actual.Length != expected)
            {
                AddError(property, string.Format(errorMessage, expected, actual.Length));
            }
        }

        public void HasLengthLessOrEqual(int expected,
            string errorMessage)
        {
            var entity = Navigator.Entity;
            
            string actual = null;
            
            if ((!Equals(entity, default(TEntity))))
            {
                actual = entity.ToString();
            }

            if (actual != null && actual.Length > expected)
            {
                AddError(errorMessage);
            }
        }

        public void HasEmptyTimePart(Expression<Func<TEntity, DateTime?>> memberAccessExpression,
            string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var date = property.GetValue();

            if (date != null)
            {
                CheckEmptyTimePart(errorMessage, date.Value, property);
            }
        }

        public void HasEmptyTimePart(Expression<Func<TEntity, DateTime>> memberAccessExpression,
            string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var date = property.GetValue();
            CheckEmptyTimePart(errorMessage, date, property);
        }

        private void CheckEmptyTimePart(string errorMessage, DateTime date, IPropertyAccessor property)
        {
            if (date.TimeOfDay.TotalSeconds > 0 && date.ToUniversalTime().TimeOfDay.TotalSeconds > 0)
            {
                AddError(property, errorMessage);
            }
        }

        public void AreNotEqual<TValue>(Expression<Func<TEntity, TValue>> memberAccessExpression,
            TValue expected,
            string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (expected.Equals(actual))
            {
                AddError(property,
                    String.Format("Actual value: [{0}]. {1}", actual, errorMessage));
            }
        }

        public void IsNotNegative(Expression<Func<TEntity, int>> memberAccessExpression)
        {
            IsNotNegative(memberAccessExpression, "The value can not be negative");
        }

        public void IsNotNegative(Expression<Func<TEntity, int>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (actual.CompareTo(0) < 0)
            {
                AddErrorWithDescriptionOfActualValue(property, errorMessage, actual);
            }
        }

        public void IsNotNegative(Expression<Func<TEntity, decimal>> memberAccessExpression)
        {
            IsNotNegative(memberAccessExpression, "The value can not be negative");
        }

        public void IsNotNegative(Expression<Func<TEntity, decimal>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (actual.CompareTo(0) < 0)
            {
                AddErrorWithDescriptionOfActualValue(property, errorMessage, actual);
            }
        }

        public void IsNotNull(Expression<Func<TEntity, object>> memberAccessExpression)
        {
            IsNotNull(memberAccessExpression, "Value should be specified");
        }

        public void IsNotNull(Expression<Func<TEntity, object>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (actual == null)
            {
                AddError(property, errorMessage);
            }
        }

        public void IsNull(Expression<Func<TEntity, object>> memberAccessExpression)
        {
            IsNull(memberAccessExpression, "Value should not be specified");
        }

        public void IsNull(Expression<Func<TEntity, object>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (actual != null)
            {
                AddErrorWithDescriptionOfActualValue(property, errorMessage, actual);
            }
        }

        public void IsEmpty(Expression<Func<TEntity, string>> memberAccessExpression)
        {
            IsEmpty(memberAccessExpression,"Value should not be specified");
        }

        public void IsEmpty(Expression<Func<TEntity, string>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (!string.IsNullOrEmpty(actual))
            {
                AddErrorWithDescriptionOfActualValue(property, errorMessage, actual);
            }
        }

        public void IsNotEmpty(Expression<Func<TEntity, string>> memberAccessExpression)
        {
            IsNotEmpty(memberAccessExpression, "Value should be specified", true);
        }

        public void IsNotEmpty(Expression<Func<TEntity, string>> memberAccessExpression, string errorMessage, bool showPropertyName)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();
            if (string.IsNullOrEmpty(actual))
            {
                if (showPropertyName)
                {
                    AddError(property, errorMessage);
                }
                else
                {
                    AddError(errorMessage);
                }
            }
        }

        public void IsNotOutOfRange(Expression<Func<TEntity, int>> memberAccessExpression, int contactsCount)
        {
            IsNotOutOfRange(memberAccessExpression, contactsCount, "Index is out of range");
        }

        public void IsNotOutOfRange(Expression<Func<TEntity, int>> memberAccessExpression, int contactsCount, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var index = property.GetValue();

            if (contactsCount <= index || index < 0)
            {
                AddError(property, errorMessage);
            }
        }

        public void IsContained(Expression<Func<TEntity, string>> memberAccessExpression, List<string> listOfString)
        {
            IsContained(memberAccessExpression, listOfString, "List should contain the value");
        }

        public void IsContained(Expression<Func<TEntity, string>> memberAccessExpression, List<string> listOfString, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();

            if (!listOfString.Contains(actual))
            {
                AddError(property, errorMessage);
            }
        }

        public void IsConvertableToInt(Expression<Func<TEntity, string>> memberAccessExpression)
        {
            IsConvertableToInt(memberAccessExpression, "Value should be convertible to integer");
        }

        public void IsConvertableToInt(Expression<Func<TEntity, string>> memberAccessExpression, string errorMessage)
        {
            var property = Navigator.GetPropertyAccessor(memberAccessExpression);
            var actual = property.GetValue();

            int value;

            if (!Int32.TryParse(actual, out value))
            {
                AddError(property, errorMessage);
            }
        }

        #endregion

        #region Navigation

        public new ValidationContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return new ValidationContextCreator<TChildEntity>(base.GetChildContextCreator(memberAccessExpression, index));
        }

        public new ValidationContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return (ValidationContext<TChildEntity>) base.CreateChildContext(memberAccessExpression, index);
        }

        public new ValidationContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
            where TResultEntity : TChildEntity
        {
            return (ValidationContext<TResultEntity>) base.CreateChildContext<TResultEntity, TChildEntity>(memberAccessExpression, index);
        }

        public new ValidationContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return (ValidationContext<TChildEntity>) base.CreateChildContext(memberAccessExpression);
        }

        public new ValidationContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
            where TResultEntity : TChildEntity
        {
            return (ValidationContext<TResultEntity>) base.CreateChildContext<TResultEntity, TChildEntity>(memberAccessExpression);
        }

        public new ValidationContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return new ValidationContextCreator<TChildEntity>(base.GetChildContextCreator(memberAccessExpression));
        }

        protected override HierarchyNavigationContext<TChildEntity> InnerCreateChildContext<TChildEntity>(HierarchyNavigator<TChildEntity> childNavigator)
        {
            return childNavigator.Entity != null ? 
                new ValidationContext<TChildEntity>(childNavigator, errors) : null;
        }

        #endregion

        public IList<ValidationError> GetValidationErrors()
        {
            return errors;
        }

        private void AddError(IPropertyAccessor property, string errorMessage)
        {
            errors.Add(new ValidationError
            {
                PropertyPath = property.HierarchyFullName,
                ErrorMessage = errorMessage,
            });
        }

        private void AddError(string errorMessage)
        {
            errors.Add(new ValidationError
            {
                ErrorMessage = errorMessage,
            });
        }

        private void AddErrorWithDescriptionOfActualValue(IPropertyAccessor property, string errorMessage, object actual)
        {
            AddError(property, String.Format("Value: {0}. {1}", actual, errorMessage));
        }
    }
}
