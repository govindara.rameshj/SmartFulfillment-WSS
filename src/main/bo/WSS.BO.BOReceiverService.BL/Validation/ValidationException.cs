using System;
using System.Collections.Generic;
using System.Text;

namespace WSS.BO.BOReceiverService.BL.Validation
{
    public class ValidationException : ApplicationException
    {
        public ValidationException(string message, IEnumerable<ValidationError> errors)
            :base(BuildMessage(message, errors))
        {
        }

        private static string BuildMessage(string message, IEnumerable<ValidationError> errors)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(message);
            foreach (var error in errors)
            {
                sb.AppendFormat("\t{0}: {1}", error.PropertyPath, error.ErrorMessage);
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}