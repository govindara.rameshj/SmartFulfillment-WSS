﻿namespace WSS.BO.BOReceiverService.BL.ServiceInfoProcessing
{
    public interface IServiceInfoProcessor
    {
        ServiceInfo GetServiceInfo();
    }
}