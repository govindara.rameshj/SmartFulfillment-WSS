﻿using System.Linq;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Metainfo;

namespace WSS.BO.BOReceiverService.BL.ServiceInfoProcessing
{
    public class ServiceInfoProcessor : IServiceInfoProcessor
    {
        private readonly IDataLayerFactory dlFactory;
        private readonly IConfiguration cfg;

        public ServiceInfoProcessor(IDataLayerFactory dlFactory, IConfiguration cfg)
        {
            this.dlFactory = dlFactory;
            this.cfg = cfg;
        }

        public ServiceInfo GetServiceInfo()
        {
            var result = new ServiceInfo
            {
                ServiceName = cfg.ServiceName,
                ServiceVersion = cfg.ServiceVersion.ToString(),
                ModelVersion = ModelMetaInfo.Version,
            };

            try
            {
                var repo = dlFactory.Create<IDictionariesRepository>();

                result.StoreDatabaseVersion = repo.GetSettingStringValue(ParameterId.ReleaseVersion);

                var retailOptions = repo.SelectRetailOptions();
                result.StoreId = RetailOptions.GetAbsoluiteStoreId(retailOptions.StoreId).ToString();
                result.StoreName = retailOptions.StoreName.TrimEnd();
            }
            catch
            {
                const string dbIsNotAvailableMessage = "DB is not available";
                result.StoreDatabaseVersion = dbIsNotAvailableMessage;
                result.StoreId = dbIsNotAvailableMessage;
                result.StoreName = dbIsNotAvailableMessage;
            }

            return result;
        }
    }
}
