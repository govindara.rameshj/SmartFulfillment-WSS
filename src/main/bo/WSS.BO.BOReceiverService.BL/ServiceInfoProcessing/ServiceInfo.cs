﻿namespace WSS.BO.BOReceiverService.BL.ServiceInfoProcessing
{
    public class ServiceInfo
    {
        public string ServiceName { get; set; }
        public string ServiceVersion { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public string StoreDatabaseVersion { get; set; }
        public string ModelVersion { get; set; }
    }
}