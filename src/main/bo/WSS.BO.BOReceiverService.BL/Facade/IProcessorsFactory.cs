﻿using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;

namespace WSS.BO.BOReceiverService.BL.Facade
{
    public interface IProcessorsFactory
    {
        ITransactionProcessor CreateTransactionProcessor();
        IServiceInfoProcessor CreateServiceInfoProcessor();
    }
}
