﻿using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.Facade
{
    public interface IBusinessLogicFacade
    {
        void AddTransaction(Transaction transaction);
        ServiceInfo GetServiceInfo();
        RetailTransaction FindTransaction(string receiptBarcode, string storeNumber);
    }
}
