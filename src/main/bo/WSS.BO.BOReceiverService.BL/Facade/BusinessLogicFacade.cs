﻿using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.BO.BOReceiverService.BL.Facade
{
    public class BusinessLogicFacade : IBusinessLogicFacade
    {
        private readonly IProcessorsFactory factory;

        public BusinessLogicFacade(IProcessorsFactory factory)
        {
            this.factory = factory;
        }

        /// <summary>
        /// Receives transaction from controller, gets its type and call suitable business logic for given type of transaction.
        /// </summary>
        /// <param name="transaction"></param>
        public void AddTransaction(Transaction transaction)
        {
            var processor = factory.CreateTransactionProcessor();
            processor.ProcessTransaction(transaction);
        }

        public ServiceInfo GetServiceInfo()
        {
            var processor = factory.CreateServiceInfoProcessor();
            return processor.GetServiceInfo();
        }

        public RetailTransaction FindTransaction(string receiptBarcode, string storeNumber)
        {
            var processor = factory.CreateTransactionProcessor();
            return processor.FindTransaction(receiptBarcode, storeNumber);
        }
    }
}
