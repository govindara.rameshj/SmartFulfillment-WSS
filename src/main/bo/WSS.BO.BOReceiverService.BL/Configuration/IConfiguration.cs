﻿using System;

namespace WSS.BO.BOReceiverService.BL.Configuration
{
    public interface IConfiguration
    {
        string ServiceName {get; }
        Version ServiceVersion {get; }
        string DeadlockPriorityForSavingTransaction { get; }
        bool ValidateTotalAmounts { get; }
        bool TruncateTooLongStrings { get; }
        bool GatherStatistics { get; }
        int CacheAbsoluteExpirationMinutes { get; }
    }
}
