﻿<Serializable()> Public Class cSystemTenders
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "TENOPT"
        BOFields.Add(_ID)
        BOFields.Add(_TypeID)
        BOFields.Add(_Description)
        BOFields.Add(_DisplayOrder)
        BOFields.Add(_MinAccepted)
        BOFields.Add(_MaxAccepted)
        BOFields.Add(_CreditCardFloor)
        BOFields.Add(_CaptureCreditCard)
        BOFields.Add(_OverTenderAllowed)
        BOFields.Add(_UseEFTPOS)
        BOFields.Add(_OpenCashDrawer)
        BOFields.Add(_PrintChequeFront)
        BOFields.Add(_PrintChequeBack)
        BOFields.Add(_DefaultRemainAmount)
        BOFields.Add(_InUse)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemTenders)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemTenders)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemTenders))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemTenders(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("TEND", "00", "ID", True, False)
    Private _TypeID As New ColField(Of Integer)("TTID", 0, "Type ID", False, False)
    Private _Description As New ColField(Of String)("TTDE", "", "Description", False, False)
    Private _DisplayOrder As New ColField(Of Integer)("TTDS", 0, "Display Order", False, False)
    Private _CreditCardFloor As New ColField(Of Decimal)("FLIM", 0, "Credit Card Floor", False, False, 2)
    Private _CaptureCreditCard As New ColField(Of Boolean)("TTCC", False, "Capture Credit Card", False, False)
    Private _OverTenderAllowed As New ColField(Of Boolean)("TTOT", False, "Over Tender Allowed", False, False)
    Private _UseEFTPOS As New ColField(Of Boolean)("TTEF", False, "Use EFTPOS", False, False)
    Private _OpenCashDrawer As New ColField(Of Boolean)("TODR", False, "Open Cash Drawer", False, False)
    Private _PrintChequeFront As New ColField(Of Boolean)("PFOC", False, "Print Cheque Front", False, False)
    Private _PrintChequeBack As New ColField(Of Boolean)("PBOC", False, "Print Cheque Back", False, False)
    Private _DefaultRemainAmount As New ColField(Of Boolean)("TADA", False, "Default Remain Amount", False, False)
    Private _MinAccepted As New ColField(Of Decimal)("MINV", 0, "Minimum Accepted", False, False, 2)
    Private _MaxAccepted As New ColField(Of Decimal)("MAXV", 0, "Maximum Accepted", False, False, 2)
    Private _InUse As New ColField(Of Boolean)("InUse", False, "In Use", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of String)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property TypeID() As ColField(Of Integer)
        Get
            Return _TypeID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TypeID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property DisplayOrder() As ColField(Of Integer)
        Get
            Return _DisplayOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DisplayOrder = value
        End Set
    End Property
    Public Property MinAccepted() As ColField(Of Decimal)
        Get
            Return _MinAccepted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MinAccepted = value
        End Set
    End Property
    Public Property MaxAccepted() As ColField(Of Decimal)
        Get
            Return _MaxAccepted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MaxAccepted = value
        End Set
    End Property
    Public Property CreditCardFloor() As ColField(Of Decimal)
        Get
            Return _CreditCardFloor
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CreditCardFloor = value
        End Set
    End Property
    Public Property CaptureCreditCard() As ColField(Of Boolean)
        Get
            Return _CaptureCreditCard
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CaptureCreditCard = value
        End Set
    End Property
    Public Property OverTenderAllowed() As ColField(Of Boolean)
        Get
            Return _OverTenderAllowed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OverTenderAllowed = value
        End Set
    End Property
    Public Property UseEFTPOS() As ColField(Of Boolean)
        Get
            Return _UseEFTPOS
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _UseEFTPOS = value
        End Set
    End Property
    Public Property OpenCashDrawer() As ColField(Of Boolean)
        Get
            Return _OpenCashDrawer
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OpenCashDrawer = value
        End Set
    End Property
    Public Property PrintChequeFront() As ColField(Of Boolean)
        Get
            Return _PrintChequeFront
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintChequeFront = value
        End Set
    End Property
    Public Property PrintChequeBack() As ColField(Of Boolean)
        Get
            Return _PrintChequeBack
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintChequeBack = value
        End Set
    End Property
    Public Property DefaultRemainAmount() As ColField(Of Boolean)
        Get
            Return _DefaultRemainAmount
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DefaultRemainAmount = value
        End Set
    End Property
    Public Property InUse() As ColField(Of Boolean)
        Get
            Return _InUse
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _InUse = value
        End Set
    End Property

#End Region

End Class
