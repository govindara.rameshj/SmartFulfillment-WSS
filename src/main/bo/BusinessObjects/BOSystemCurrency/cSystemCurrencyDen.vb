﻿<Serializable()> Public Class cSystemCurrencyDen
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemCurrencyDen"
        BOFields.Add(_CurrencyID)
        BOFields.Add(_ID)
        BOFields.Add(_TenderID)
        BOFields.Add(_DisplayText)
        BOFields.Add(_BullionMultiple)
        BOFields.Add(_BankingBagLimit)
        BOFields.Add(_SafeMinimum)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemCurrencyDen)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemCurrencyDen)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemCurrencyDen))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemCurrencyDen(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Decimal)("ID", 0, "ID", True, False, 2)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _TenderID As New ColField(Of Integer)("TenderID", 0, "Tender ID", False, False)
    Private _DisplayText As New ColField(Of String)("DisplayText", "", "Display Text", False, False)
    Private _BullionMultiple As New ColField(Of Decimal)("BullionMultiple", 0, "Bullion Multiple", False, False)
    Private _BankingBagLimit As New ColField(Of Decimal)("BankingBagLimit", 0, "Banking Bag Limit", False, False, 2)
    Private _SafeMinimum As New ColField(Of Decimal)("SafeMinimum", 0, "Safe Minimum", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Decimal)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property TenderID() As ColField(Of Integer)
        Get
            Return _TenderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TenderID = value
        End Set
    End Property
    Public Property DisplayText() As ColField(Of String)
        Get
            Return _DisplayText
        End Get
        Set(ByVal value As ColField(Of String))
            _DisplayText = value
        End Set
    End Property
    Public Property BullionMultiple() As ColField(Of Decimal)
        Get
            BullionMultiple = _BullionMultiple
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BullionMultiple = value
        End Set
    End Property
    Public Property BankingBagLimit() As ColField(Of Decimal)
        Get
            BankingBagLimit = _BankingBagLimit
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BankingBagLimit = value
        End Set
    End Property
    Public Property SafeMinimum() As ColField(Of Decimal)
        Get
            Return _SafeMinimum
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SafeMinimum = value
        End Set
    End Property

#End Region

#Region "Enums"

    Public Enum ColumnNames
        ID
        CurrencyID
        TenderID
        DisplayText
        BullionMultiple
        BankingBagLimit
        SafeMinimum
        Total
        System
        Variance
    End Enum

#End Region

#Region "Methods"

    Public Function GetDenoms(ByVal CurrencyID As String) As List(Of cSystemCurrencyDen)

        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CurrencyID, CurrencyID)
        Return LoadMatches()

    End Function

    Public Function IsMultiple(ByVal value As Decimal) As Boolean

        Try
            Return (value Mod _ID.Value = 0)

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

End Class
