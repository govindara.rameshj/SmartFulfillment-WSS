﻿<Serializable()> Public Class cSystemCurrency
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemCurrency"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOFields.Add(_Symbol)
        BOFields.Add(_PrintSymbol)
        BOFields.Add(_ActiveDate)
        BOFields.Add(_InactiveDate)
        BOFields.Add(_LargestDenom)
        BOFields.Add(_MinCashMultiple)
        BOFields.Add(_DisplayOrder)
        BOFields.Add(_IsDefault)
        BOFields.Add(_MaxAccepted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemCurrency)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemCurrency)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemCurrency))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemCurrency(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("ID", "", "Currency ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _Symbol As New ColField(Of String)("Symbol", "", "Symbol", False, False)
    Private _PrintSymbol As New ColField(Of String)("PrintSymbol", "", "Print Symbol", False, False)
    Private _ActiveDate As New ColField(Of Date)("ActiveDate", Nothing, "Active Date", False, False)
    Private _InactiveDate As New ColField(Of Date)("InactiveDate", Nothing, "Inactive Date", False, False)
    Private _LargestDenom As New ColField(Of Integer)("LargestDenom", 0, "Largest Denom", False, False)
    Private _MinCashMultiple As New ColField(Of Decimal)("MinCashMultiple", 1, "Min Cash Multiple", False, False)
    Private _DisplayOrder As New ColField(Of Integer)("DisplayOrder", 0, "Display Order", False, False)
    Private _IsDefault As New ColField(Of Boolean)("IsDefault", False, "Is Default", False, False)
    Private _MaxAccepted As New ColField(Of Decimal)("MaxAccepted", 0, "Max Accepted", False, False, 2)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of String)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Symbol() As ColField(Of String)
        Get
            Symbol = _Symbol
        End Get
        Set(ByVal value As ColField(Of String))
            _Symbol = value
        End Set
    End Property
    Public Property PrintSymbol() As ColField(Of String)
        Get
            PrintSymbol = _PrintSymbol
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintSymbol = value
        End Set
    End Property
    Public Property ActiveDate() As ColField(Of Date)
        Get
            Return _ActiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _ActiveDate = value
        End Set
    End Property
    Public Property InactiveDate() As ColField(Of Date)
        Get
            Return _InactiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _InactiveDate = value
        End Set
    End Property
    Public Property LargestDenom() As ColField(Of Integer)
        Get
            Return _LargestDenom
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LargestDenom = value
        End Set
    End Property
    Public Property MinCashMultiple() As ColField(Of Decimal)
        Get
            Return _MinCashMultiple
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MinCashMultiple = value
        End Set
    End Property
    Public Property DisplayOrder() As ColField(Of Integer)
        Get
            Return _DisplayOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DisplayOrder = value
        End Set
    End Property
    Public Property IsDefault() As ColField(Of Boolean)
        Get
            IsDefault = _IsDefault
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDefault = value
        End Set
    End Property
    Public Property MaxAccepted() As ColField(Of Decimal)
        Get
            MaxAccepted = _MaxAccepted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MaxAccepted = value
        End Set
    End Property

#End Region

#Region "Data Set"
    Private _ds As New DataSet
    Public ReadOnly Property CurrencyDataset() As DataSet
        Get
            Return _ds
        End Get
    End Property
    Public ReadOnly Property CurrencyTable() As DataTable
        Get
            Return _ds.Tables(0)
        End Get
    End Property
    Public ReadOnly Property CurrencyDenomsTable() As DataTable
        Get
            Return _ds.Tables(1)
        End Get
    End Property

#End Region

#Region "Enums"

    Public Enum ColumnNames
        ID
        Description
        Symbol
        PrintSymbol
        ActiveDate
        InactiveDate
        LargestDenom
        MinCashMultiple
        DisplayOrder
        IsDefault
        MaxAccepted
        Total
        System
        Variance
    End Enum

#End Region

#Region "Entities"
    Private _Currencies As List(Of cSystemCurrency) = Nothing
    Private _Denoms As List(Of cSystemCurrencyDen) = Nothing

    Public Property Currency(ByVal CurrencyID As String) As cSystemCurrency
        Get
            For Each cur As cSystemCurrency In Currencies
                If cur.ID.Value = CurrencyID Then Return cur
            Next
            Return Nothing
        End Get
        Set(ByVal value As cSystemCurrency)
            For Each cur As cSystemCurrency In Currencies
                If cur.ID.Value = CurrencyID Then
                    cur = value
                End If
            Next
        End Set
    End Property
    Public Property Currencies() As List(Of cSystemCurrency)
        Get
            If _Currencies Is Nothing Then Load()
            Return _Currencies
        End Get
        Set(ByVal value As List(Of cSystemCurrency))
            _Currencies = value
        End Set
    End Property

    Public Property Denom(ByVal CurrencyDenID As Decimal, ByVal TenderID As Integer) As cSystemCurrencyDen
        Get
            For Each den As cSystemCurrencyDen In Denoms
                If (den.ID.Value = CurrencyDenID) And (den.TenderID.Value = TenderID) Then Return den
            Next
            Return New cSystemCurrencyDen(Oasys3DB) 'else return empty den
        End Get
        Set(ByVal value As cSystemCurrencyDen)
            For Each den As cSystemCurrencyDen In _Denoms
                If (den.ID.Value = CurrencyDenID) And (den.TenderID.Value = TenderID) Then
                    den = value
                End If
            Next
        End Set
    End Property
    Public Property Denoms() As List(Of cSystemCurrencyDen)
        Get
            If _Denoms Is Nothing Then
                Dim den As New cSystemCurrencyDen(Oasys3DB)
                den.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, den.CurrencyID, _ID.Value)
                _Denoms = den.LoadMatches
            End If
            Return _Denoms
        End Get
        Set(ByVal value As List(Of cSystemCurrencyDen))
            _Denoms = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all active currencies and denominations into CurrencyDataset in display order
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CurrencyLoad()

        Try
            'need to hardcode as oasys dbbo cannot recreate this type of query
            Dim sb As New StringBuilder("SELECT * from 	SystemCurrency sc ")
            sb.Append("where sc.ActiveDate <= curdate() ")
            sb.Append("and (sc.InactiveDate is null or sc.InactiveDate > curdate())")

            'get currencies
            Oasys3DB.ClearAllParameters()
            _ds = Oasys3DB.ExecuteSql(sb.ToString)
            _ds.Tables(0).TableName = "Currencies"

            'add total, system and variance columns
            Dim colTotal As New DataColumn(ColumnNames.Total.ToString, GetType(Decimal))
            Dim colSystem As New DataColumn(ColumnNames.System.ToString, GetType(Decimal))
            Dim colVariance As New DataColumn(ColumnNames.Variance.ToString, GetType(Decimal), ColumnNames.Total.ToString & "-" & ColumnNames.System.ToString)

            _ds.Tables(0).Columns.Add(colTotal)
            _ds.Tables(0).Columns.Add(colSystem)
            _ds.Tables(0).Columns.Add(colVariance)


            'get denominations
            Dim den As New cSystemCurrencyDen(Oasys3DB)
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(den.TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0).Copy
            dt.TableName = "Denominations"
            _ds.Tables.Add(dt)

            'add total, system and variance columns
            Dim colTotal1 As New DataColumn(ColumnNames.Total.ToString, GetType(Decimal))
            Dim colSystem1 As New DataColumn(ColumnNames.System.ToString, GetType(Decimal))
            Dim colVariance1 As New DataColumn(ColumnNames.Variance.ToString, GetType(Decimal), ColumnNames.Total.ToString & "-" & ColumnNames.System.ToString)

            _ds.Tables(1).Columns.Add(colTotal1)
            _ds.Tables(1).Columns.Add(colSystem1)
            _ds.Tables(1).Columns.Add(colVariance1)

            'set initial values
            For Each dr As DataRow In _ds.Tables(1).Rows
                dr(ColumnNames.Total.ToString) = 0
                dr(ColumnNames.System.ToString) = 0
            Next

            'set up relation
            Dim parentCols As DataColumn() = New DataColumn() {CurrencyTable.Columns(_ID.ColumnName)}
            Dim childCols As DataColumn() = New DataColumn() {CurrencyDenomsTable.Columns(den.CurrencyID.ColumnName)}

            _ds.Relations.Add("Denominations", parentCols, childCols)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CurrencyLoad, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Loads system currencies into Currencies collection of this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load()

        Try
            _Currencies = New List(Of cSystemCurrency)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            For Each dr As DataRow In dt.Rows
                Dim cur As New cSystemCurrency(Oasys3DB)
                cur.LoadFromRow(dr)
                _Currencies.Add(cur)
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CurrencyLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads default currency into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="loadAll">Set to true to load currencies into Currencies collection</param>
    ''' <remarks></remarks>
    Public Sub LoadDefaultCurrency(Optional ByVal loadAll As Boolean = False)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_IsDefault.ColumnName, clsOasys3DB.eOperator.pEquals, True)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If dt.Rows.Count = 0 Then
                Throw New OasysDbException(My.Resources.Errors.CurrencyNoDefaultSet)
            Else
                LoadFromRow(dt.Rows(0))
                If loadAll Then Load()
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CurrencyLoadDefault, ex)
        End Try

    End Sub


    ''' <summary>
    ''' Returns list of all system currencies.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCurrencies() As List(Of cSystemCurrency)

        Try
            ClearLists()
            Return LoadMatches()

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CurrencyLoad, ex)
        End Try

    End Function

    Public Function DenomsTenderCount(ByVal TenderList As ArrayList) As Integer

        Dim total As Integer = 0
        Dim tender As IEnumerator = TenderList.GetEnumerator
        While tender.MoveNext
            For Each den As cSystemCurrencyDen In _Denoms
                If (den.TenderID.Value = CInt(tender.Current)) Then total += 1
            Next
        End While
        Return total

    End Function
    Public Function DenomsFirstTenderID(ByVal TenderList As ArrayList) As Integer

        Dim tender As IEnumerator = TenderList.GetEnumerator
        While tender.MoveNext
            For Each den As cSystemCurrencyDen In _Denoms
                If (den.TenderID.Value = CInt(tender.Current)) Then Return CInt(tender.Current)
            Next
        End While
        Return 0

    End Function



    Public Function GetCurrencySymbol(ByVal CurrencyID As String) As String

        If _Currencies IsNot Nothing Then
            For Each Currency As cSystemCurrency In _Currencies
                If Currency.ID.Value = CurrencyID Then Return Currency.Symbol.Value
            Next
        End If

        Try
            ClearLoadField()
            ClearLoadFilter()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, CurrencyID)
            LoadMatches()
            Return _Symbol.Value

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try

    End Function
    Public Function GetDefaultCurrencyID() As String

        If _Currencies IsNot Nothing Then
            For Each Currency As cSystemCurrency In _Currencies
                If Currency.IsDefault.Value Then Return Currency.ID.Value
            Next
        End If

        Try 'if here then load from database
            ClearLoadField()
            ClearLoadFilter()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDefault, True)
            LoadMatches()
            Return _ID.Value

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try

    End Function
    Public Function GetDefaultCurrencySymbol() As String

        If _Currencies IsNot Nothing Then
            For Each Currency As cSystemCurrency In _Currencies
                If Currency.IsDefault.Value Then Return Currency.Symbol.Value
            Next
        End If

        Try
            ClearLoadField()
            ClearLoadFilter()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDefault, True)
            LoadMatches()
            Return _Symbol.Value

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try

    End Function

#End Region

End Class


