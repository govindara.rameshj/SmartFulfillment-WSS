﻿<Serializable()> Public Class cExchangeRate
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EXRATE"
        BOFields.Add(_CurrencyID)
        BOFields.Add(_EffectiveDate)
        BOFields.Add(_Rate)
        BOFields.Add(_Power)
        BOFields.Add(_CommissionPercent)
        BOFields.Add(_CommissionValue)
        BOFields.Add(_CommissionRule)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cExchangeRate)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cExchangeRate)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cExchangeRate))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cExchangeRate(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CurrencyID As New ColField(Of String)("CURR", "", "Currency ID", True, False)
    Private _EffectiveDate As New ColField(Of Date)("EFFD", Now, "Effective Date", True, False)
    Private _Rate As New ColField(Of Decimal)("RATE", 0, "Exchange Rate", False, False, 4)
    Private _Power As New ColField(Of Integer)("POWR", 0, "Exchange Power", False, False)
    Private _CommissionPercent As New ColField(Of Decimal)("COMM", 0, "Commission %", False, False, 2)
    Private _CommissionValue As New ColField(Of Decimal)("MINC", 0, "Commission Value", False, False, 2)
    Private _CommissionRule As New ColField(Of Boolean)("IMIN", False, "Commission Rule", False, False)

#End Region

#Region "Field Properties"

    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property EffectiveDate() As ColField(Of Date)
        Get
            Return _EffectiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EffectiveDate = value
        End Set
    End Property
    Public Property Rate() As ColField(Of Decimal)
        Get
            Return _Rate
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Rate = value
        End Set
    End Property
    Public Property Power() As ColField(Of Integer)
        Get
            Return _Power
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Power = value
        End Set
    End Property
    Public Property CommissionPercent() As ColField(Of Decimal)
        Get
            Return _CommissionPercent
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CommissionPercent = value
        End Set
    End Property
    Public Property CommissionValue() As ColField(Of Decimal)
        Get
            Return _CommissionValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CommissionValue = value
        End Set
    End Property
    Public Property CommissionRule() As ColField(Of Boolean)
        Get
            Return _CommissionRule
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommissionRule = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _ExchangeRates As List(Of cExchangeRate) = Nothing

    Public Property ExchangeRates() As List(Of cExchangeRate)
        Get
            If _ExchangeRates Is Nothing Then
                LoadValidExchangeRates()
            End If
            Return _ExchangeRates
        End Get
        Set(ByVal value As List(Of cExchangeRate))
            _ExchangeRates = value
        End Set
    End Property
    Public Property ExchangeRate(ByVal CurrencyID As String) As cExchangeRate
        Get
            For Each exc As cExchangeRate In ExchangeRates
                If exc.CurrencyID.Value = CurrencyID Then Return exc
            Next
            Return Nothing
        End Get
        Set(ByVal value As cExchangeRate)
            For Each exc As cExchangeRate In ExchangeRates
                If exc.CurrencyID.Value = CurrencyID Then exc = value
            Next
        End Set
    End Property

    ''' <summary>
    ''' Loads all effective exchange rates into ExchangeRates collection for this instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadValidExchangeRates()

        Try
            Dim exc As New cExchangeRate(Oasys3DB)
            exc.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, _EffectiveDate, Now.Date)
            _ExchangeRates = exc.LoadMatches

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Loads current exchange rate for given currency into this instance
    ''' </summary>
    ''' <param name="CurrencyID"></param>
    ''' <remarks></remarks>
    Public Sub LoadExchangeRate(ByVal CurrencyID As String)

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CurrencyID, CurrencyID)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, _EffectiveDate, Now.Date)
            SortBy(_EffectiveDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            LoadMatches()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

End Class
