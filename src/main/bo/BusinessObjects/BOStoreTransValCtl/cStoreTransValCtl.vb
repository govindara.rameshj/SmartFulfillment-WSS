﻿<Serializable()> Public Class cStoreTransValCtl
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "TVCSTR"
        BOFields.Add(mFileName)
        BOFields.Add(mVersionNo)
        BOFields.Add(mSequence)
        BOFields.Add(mLastAvailVersionNo)
        BOFields.Add(mLastAvailSeqNo)
        BOFields.Add(mPVersion)
        BOFields.Add(mPSequence)
        BOFields.Add(mTransactionDate)
        BOFields.Add(mTransactionTime)
        BOFields.Add(mRecordType1)
        BOFields.Add(mRecordType2)
        BOFields.Add(mRecordType3)
        BOFields.Add(mRecordType4)
        BOFields.Add(mRecordType5)
        BOFields.Add(mRecordType6)
        BOFields.Add(mRecordType7)
        BOFields.Add(mRecordType8)
        BOFields.Add(mRecordType9)
        BOFields.Add(mRecordType10)
        BOFields.Add(mRecordType11)
        BOFields.Add(mRecordType12)
        BOFields.Add(mRecordType13)
        BOFields.Add(mRecordType14)
        BOFields.Add(mRecordType15)
        BOFields.Add(mRecordHash1)
        BOFields.Add(mRecordHash2)
        BOFields.Add(mRecordHash3)
        BOFields.Add(mRecordHash4)
        BOFields.Add(mRecordHash5)
        BOFields.Add(mRecordHash6)
        BOFields.Add(mRecordHash7)
        BOFields.Add(mRecordHash8)
        BOFields.Add(mRecordHash9)
        BOFields.Add(mRecordHash10)
        BOFields.Add(mRecordHash11)
        BOFields.Add(mRecordHash12)
        BOFields.Add(mRecordHash13)
        BOFields.Add(mRecordHash14)
        BOFields.Add(mRecordHash15)
        BOFields.Add(mValuePerHash1)
        BOFields.Add(mValuePerHash2)
        BOFields.Add(mValuePerHash3)
        BOFields.Add(mValuePerHash4)
        BOFields.Add(mValuePerHash5)
        BOFields.Add(mValuePerHash6)
        BOFields.Add(mValuePerHash7)
        BOFields.Add(mValuePerHash8)
        BOFields.Add(mValuePerHash9)
        BOFields.Add(mValuePerHash10)
        BOFields.Add(mValuePerHash11)
        BOFields.Add(mValuePerHash12)
        BOFields.Add(mValuePerHash13)
        BOFields.Add(mValuePerHash14)
        BOFields.Add(mValuePerHash15)
        BOFields.Add(mFlag)
        BOFields.Add(mErrCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStoreTransValCtl)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStoreTransValCtl)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStoreTransValCtl))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStoreTransValCtl(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private mFileName As New ColField(Of String)("FILE", "", True)
    Private mVersionNo As New ColField(Of String)("VRSN", "00", True)
    Private mSequence As New ColField(Of String)("SEQN", "000000")
    Private mLastAvailVersionNo As New ColField(Of String)("AVER", "00")
    Private mLastAvailSeqNo As New ColField(Of String)("ASEQ", "000000")
    Private mPVersion As New ColField(Of String)("PVER", "00")
    Private mPSequence As New ColField(Of String)("PSEQ", "000000")
    Private mTransactionDate As New ColField(Of Date)("DATE1", Nothing)
    Private mTransactionTime As New ColField(Of String)("TIME", "")
    Private mRecordType1 As New ColField(Of String)("RTYP1", "")
    Private mRecordType2 As New ColField(Of String)("RTYP2", "")
    Private mRecordType3 As New ColField(Of String)("RTYP3", "")
    Private mRecordType4 As New ColField(Of String)("RTYP4", "")
    Private mRecordType5 As New ColField(Of String)("RTYP5", "")
    Private mRecordType6 As New ColField(Of String)("RTYP6", "")
    Private mRecordType7 As New ColField(Of String)("RTYP7", "")
    Private mRecordType8 As New ColField(Of String)("RTYP8", "")
    Private mRecordType9 As New ColField(Of String)("RTYP9", "")
    Private mRecordType10 As New ColField(Of String)("RTYP10", "")
    Private mRecordType11 As New ColField(Of String)("RTYP11", "")
    Private mRecordType12 As New ColField(Of String)("RTYP12", "")
    Private mRecordType13 As New ColField(Of String)("RTYP13", "")
    Private mRecordType14 As New ColField(Of String)("RTYP14", "")
    Private mRecordType15 As New ColField(Of String)("RTYP15", "")
    Private mRecordHash1 As New ColField(Of Decimal)("HREC1", 0)
    Private mRecordHash2 As New ColField(Of Decimal)("HREC2", 0)
    Private mRecordHash3 As New ColField(Of Decimal)("HREC3", 0)
    Private mRecordHash4 As New ColField(Of Decimal)("HREC4", 0)
    Private mRecordHash5 As New ColField(Of Decimal)("HREC5", 0)
    Private mRecordHash6 As New ColField(Of Decimal)("HREC6", 0)
    Private mRecordHash7 As New ColField(Of Decimal)("HREC7", 0)
    Private mRecordHash8 As New ColField(Of Decimal)("HREC8", 0)
    Private mRecordHash9 As New ColField(Of Decimal)("HREC9", 0)
    Private mRecordHash10 As New ColField(Of Decimal)("HREC10", 0)
    Private mRecordHash11 As New ColField(Of Decimal)("HREC11", 0)
    Private mRecordHash12 As New ColField(Of Decimal)("HREC12", 0)
    Private mRecordHash13 As New ColField(Of Decimal)("HREC13", 0)
    Private mRecordHash14 As New ColField(Of Decimal)("HREC14", 0)
    Private mRecordHash15 As New ColField(Of Decimal)("HREC15", 0)
    Private mValuePerHash1 As New ColField(Of Decimal)("HVAL1", 0)
    Private mValuePerHash2 As New ColField(Of Decimal)("HVAL2", 0)
    Private mValuePerHash3 As New ColField(Of Decimal)("HVAL3", 0)
    Private mValuePerHash4 As New ColField(Of Decimal)("HVAL4", 0)
    Private mValuePerHash5 As New ColField(Of Decimal)("HVAL5", 0)
    Private mValuePerHash6 As New ColField(Of Decimal)("HVAL6", 0)
    Private mValuePerHash7 As New ColField(Of Decimal)("HVAL7", 0)
    Private mValuePerHash8 As New ColField(Of Decimal)("HVAL8", 0)
    Private mValuePerHash9 As New ColField(Of Decimal)("HVAL9", 0)
    Private mValuePerHash10 As New ColField(Of Decimal)("HVAL10", 0)
    Private mValuePerHash11 As New ColField(Of Decimal)("HVAL11", 0)
    Private mValuePerHash12 As New ColField(Of Decimal)("HVAL12", 0)
    Private mValuePerHash13 As New ColField(Of Decimal)("HVAL13", 0)
    Private mValuePerHash14 As New ColField(Of Decimal)("HVAL14", 0)
    Private mValuePerHash15 As New ColField(Of Decimal)("HVAL15", 0)
    Private mFlag As New ColField(Of Boolean)("FLAG", False)
    Private mErrCode As New ColField(Of String)("ECOD", "")

#End Region

#Region "Field Properties"

    Public Property FileName() As ColField(Of String)
        Get
            FileName = mFileName
        End Get
        Set(ByVal value As ColField(Of String))
            mFileName = value
        End Set
    End Property
    Public Property VersionNo() As ColField(Of String)
        Get
            VersionNo = mVersionNo
        End Get
        Set(ByVal value As ColField(Of String))
            mVersionNo = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of String)
        Get
            Sequence = mSequence
        End Get
        Set(ByVal value As ColField(Of String))
            mSequence = value
        End Set
    End Property
    Public Property LastAvailVersionNo() As ColField(Of String)
        Get
            LastAvailVersionNo = mLastAvailVersionNo
        End Get
        Set(ByVal value As ColField(Of String))
            mLastAvailVersionNo = value
        End Set
    End Property
    Public Property LastAvailSeqNo() As ColField(Of String)
        Get
            LastAvailSeqNo = mLastAvailSeqNo
        End Get
        Set(ByVal value As ColField(Of String))
            mLastAvailSeqNo = value
        End Set
    End Property
    Public Property PVersion() As ColField(Of String)
        Get
            PVersion = mPVersion
        End Get
        Set(ByVal value As ColField(Of String))
            mPVersion = value
        End Set
    End Property
    Public Property PSequence() As ColField(Of String)
        Get
            PSequence = mPSequence
        End Get
        Set(ByVal value As ColField(Of String))
            mPSequence = value
        End Set
    End Property
    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = mTransactionDate
        End Get
        Set(ByVal value As ColField(Of Date))
            mTransactionDate = value
        End Set
    End Property
    Public Property TransactionTime() As ColField(Of String)
        Get
            TransactionTime = mTransactionTime
        End Get
        Set(ByVal value As ColField(Of String))
            mTransactionTime = value
        End Set
    End Property
    Public Property RecordType(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = RecordType1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = RecordType1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property

    Public Property RecordType1() As ColField(Of String)
        Get
            RecordType1 = mRecordType1
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType1 = value
        End Set
    End Property
    Public Property RecordType2() As ColField(Of String)
        Get
            RecordType2 = mRecordType2
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType2 = value
        End Set
    End Property
    Public Property RecordType3() As ColField(Of String)
        Get
            RecordType3 = mRecordType3
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType3 = value
        End Set
    End Property
    Public Property RecordType4() As ColField(Of String)
        Get
            RecordType4 = mRecordType4
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType4 = value
        End Set
    End Property
    Public Property RecordType5() As ColField(Of String)
        Get
            RecordType5 = mRecordType5
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType5 = value
        End Set
    End Property
    Public Property RecordType6() As ColField(Of String)
        Get
            RecordType6 = mRecordType6
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType6 = value
        End Set
    End Property
    Public Property RecordType7() As ColField(Of String)
        Get
            RecordType7 = mRecordType7
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType7 = value
        End Set
    End Property
    Public Property RecordType8() As ColField(Of String)
        Get
            RecordType8 = mRecordType8
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType8 = value
        End Set
    End Property
    Public Property RecordType9() As ColField(Of String)
        Get
            RecordType9 = mRecordType9
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType9 = value
        End Set
    End Property
    Public Property RecordType10() As ColField(Of String)
        Get
            RecordType10 = mRecordType10
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType10 = value
        End Set
    End Property
    Public Property RecordType11() As ColField(Of String)
        Get
            RecordType11 = mRecordType11
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType11 = value
        End Set
    End Property
    Public Property RecordType12() As ColField(Of String)
        Get
            RecordType12 = mRecordType12
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType12 = value
        End Set
    End Property
    Public Property RecordType13() As ColField(Of String)
        Get
            RecordType13 = mRecordType13
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType13 = value
        End Set
    End Property
    Public Property RecordType14() As ColField(Of String)
        Get
            RecordType14 = mRecordType14
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType14 = value
        End Set
    End Property
    Public Property RecordType15() As ColField(Of String)
        Get
            RecordType15 = mRecordType15
        End Get
        Set(ByVal value As ColField(Of String))
            mRecordType15 = value
        End Set
    End Property
    Public Property RecordHash(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            'Extract Column Name
            Dim ColName As String = RecordHash1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = RecordHash1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property

    Public Property RecordHash1() As ColField(Of Decimal)
        Get
            RecordHash1 = mRecordHash1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash1 = value
        End Set
    End Property
    Public Property RecordHash2() As ColField(Of Decimal)
        Get
            RecordHash2 = mRecordHash2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash2 = value
        End Set
    End Property
    Public Property RecordHash3() As ColField(Of Decimal)
        Get
            RecordHash3 = mRecordHash3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash3 = value
        End Set
    End Property
    Public Property RecordHash4() As ColField(Of Decimal)
        Get
            RecordHash4 = mRecordHash4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash4 = value
        End Set
    End Property
    Public Property RecordHash5() As ColField(Of Decimal)
        Get
            RecordHash5 = mRecordHash5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash5 = value
        End Set
    End Property
    Public Property RecordHash6() As ColField(Of Decimal)
        Get
            RecordHash6 = mRecordHash6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash6 = value
        End Set
    End Property
    Public Property RecordHash7() As ColField(Of Decimal)
        Get
            RecordHash7 = mRecordHash7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash7 = value
        End Set
    End Property
    Public Property RecordHash8() As ColField(Of Decimal)
        Get
            RecordHash8 = mRecordHash8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash8 = value
        End Set
    End Property
    Public Property RecordHash9() As ColField(Of Decimal)
        Get
            RecordHash9 = mRecordHash9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash9 = value
        End Set
    End Property
    Public Property RecordHash10() As ColField(Of Decimal)
        Get
            RecordHash10 = mRecordHash10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash10 = value
        End Set
    End Property
    Public Property RecordHash11() As ColField(Of Decimal)
        Get
            RecordHash11 = mRecordHash11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash11 = value
        End Set
    End Property
    Public Property RecordHash12() As ColField(Of Decimal)
        Get
            RecordHash12 = mRecordHash12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash12 = value
        End Set
    End Property
    Public Property RecordHash13() As ColField(Of Decimal)
        Get
            RecordHash13 = mRecordHash13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash13 = value
        End Set
    End Property
    Public Property RecordHash14() As ColField(Of Decimal)
        Get
            RecordHash14 = mRecordHash14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash14 = value
        End Set
    End Property
    Public Property RecordHash15() As ColField(Of Decimal)
        Get
            RecordHash15 = mRecordHash15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRecordHash15 = value
        End Set
    End Property
    Public Property ValuePerHash(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            'Extract Column Name
            Dim ColName As String = ValuePerHash1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = ValuePerHash1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property

    Public Property ValuePerHash1() As ColField(Of Decimal)
        Get
            ValuePerHash1 = mValuePerHash1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash1 = value
        End Set
    End Property
    Public Property ValuePerHash2() As ColField(Of Decimal)
        Get
            ValuePerHash2 = mValuePerHash2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash2 = value
        End Set
    End Property
    Public Property ValuePerHash3() As ColField(Of Decimal)
        Get
            ValuePerHash3 = mValuePerHash3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash3 = value
        End Set
    End Property
    Public Property ValuePerHash4() As ColField(Of Decimal)
        Get
            ValuePerHash4 = mValuePerHash4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash4 = value
        End Set
    End Property
    Public Property ValuePerHash5() As ColField(Of Decimal)
        Get
            ValuePerHash5 = mValuePerHash5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash5 = value
        End Set
    End Property
    Public Property ValuePerHash6() As ColField(Of Decimal)
        Get
            ValuePerHash6 = mValuePerHash6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash6 = value
        End Set
    End Property
    Public Property ValuePerHash7() As ColField(Of Decimal)
        Get
            ValuePerHash7 = mValuePerHash7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash7 = value
        End Set
    End Property
    Public Property ValuePerHash8() As ColField(Of Decimal)
        Get
            ValuePerHash8 = mValuePerHash8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash8 = value
        End Set
    End Property
    Public Property ValuePerHash9() As ColField(Of Decimal)
        Get
            ValuePerHash9 = mValuePerHash9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash9 = value
        End Set
    End Property
    Public Property ValuePerHash10() As ColField(Of Decimal)
        Get
            ValuePerHash10 = mValuePerHash10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash10 = value
        End Set
    End Property
    Public Property ValuePerHash11() As ColField(Of Decimal)
        Get
            ValuePerHash11 = mValuePerHash11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash11 = value
        End Set
    End Property
    Public Property ValuePerHash12() As ColField(Of Decimal)
        Get
            ValuePerHash12 = mValuePerHash12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash12 = value
        End Set
    End Property
    Public Property ValuePerHash13() As ColField(Of Decimal)
        Get
            ValuePerHash13 = mValuePerHash13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash13 = value
        End Set
    End Property
    Public Property ValuePerHash14() As ColField(Of Decimal)
        Get
            ValuePerHash14 = mValuePerHash14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash14 = value
        End Set
    End Property
    Public Property ValuePerHash15() As ColField(Of Decimal)
        Get
            ValuePerHash15 = mValuePerHash15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mValuePerHash15 = value
        End Set
    End Property
    Public Property Flag() As ColField(Of Boolean)
        Get
            Flag = mFlag
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mFlag = value
        End Set
    End Property
    Public Property ErrCode() As ColField(Of String)
        Get
            ErrCode = mErrCode
        End Get
        Set(ByVal value As ColField(Of String))
            mErrCode = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function LoadControlRecord(ByVal FileType As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sequence, "000000")
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, "00")
        If (LoadMatches(1).Count = 0) Then
            mFileName.Value = FileType
            SaveIfNew()
        End If
        Return True

    End Function

    Public Function LoadVersionRecord(ByVal FileType As String, ByVal FileVersionNo As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, FileVersionNo)
        If (LoadMatches(1).Count = 0) Then
            mFileName.Value = FileType
            mVersionNo.Value = FileVersionNo
            SaveIfNew()
        End If
        Return True

    End Function

    Public Function RecordVersionRecord(ByVal FileType As String, ByVal FileVersionNo As String, ByVal FileSequenceNo As String, ByVal Result As String) As Boolean

        Try
            'First attempt to load File version Record
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
            Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, FileVersionNo)
            If (LoadMatches(1).Count = 0) Then 'Create new file if not exists
                mFileName.Value = FileType
                mVersionNo.Value = FileVersionNo
                mSequence.Value = "000000"
                SaveIfNew()
            End If

            'Prepare to update Version Record
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)

            mFileName.Value = FileType
            mVersionNo.Value = FileVersionNo
            mErrCode.Value = Result
            'If Passed in Sequence No filled, then in correct sequence so update file details
            If FileSequenceNo <> String.Empty Then
                mSequence.Value = FileSequenceNo
                mTransactionDate.Value = Today
                mTransactionTime.Value = TimeOfDay.Hour.ToString("00") & ":" & TimeOfDay.Minute.ToString("00") & ":" & TimeOfDay.Second.ToString("00")
                Oasys3DB.SetColumnAndValueParameter(mFileName.ColumnName, mFileName.Value)
                Oasys3DB.SetColumnAndValueParameter(mSequence.ColumnName, mSequence.Value)
                Oasys3DB.SetColumnAndValueParameter(mTransactionDate.ColumnName, mTransactionDate.Value)
                Oasys3DB.SetColumnAndValueParameter(mTransactionTime.ColumnName, mTransactionTime.Value)
            End If
            Oasys3DB.SetColumnAndValueParameter(mErrCode.ColumnName, mErrCode.Value)
            If (Result = String.Empty) Then 'no validation error so mark as 
                mFlag.Value = True
                Oasys3DB.SetColumnAndValueParameter(mFlag.ColumnName, mFlag.Value)
            Else
                mFlag.Value = False
                Oasys3DB.SetColumnAndValueParameter(mFlag.ColumnName, mFlag.Value)
            End If
            SetDBKeys()
            If Oasys3DB.Update > 0 Then 'update Version Record
                'Prepare to update Control Record
                Oasys3DB.ClearAllParameters()
                Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
                Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, "00")
                LoadMatches()
                'Once loaded update values ready for saving
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)

                If (Result = String.Empty) Then
                    mLastAvailVersionNo.Value = FileVersionNo
                    Oasys3DB.SetColumnAndValueParameter(mLastAvailVersionNo.ColumnName, mLastAvailVersionNo.Value)
                    If (FileSequenceNo <> String.Empty) Then
                        mLastAvailSeqNo.Value = FileSequenceNo
                        Oasys3DB.SetColumnAndValueParameter(mLastAvailSeqNo.ColumnName, mLastAvailSeqNo.Value)
                    End If
                End If
                mErrCode.Value = Result
                Oasys3DB.SetColumnAndValueParameter(mErrCode.ColumnName, mErrCode.Value)

                SetDBKeys()
                If Oasys3DB.Update > 0 Then
                    Return True
                Else
                    'Unable to update Control Record
                    Return False
                End If
            Else
                'Unable to update Version Record
                Return False
            End If

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function RecordActualRecordCounts(ByVal FileType As String, ByVal FileVersionNo As String, ByVal RecordTypes() As String, ByVal RecordCounts() As Integer, ByVal RecordTotals() As Decimal) As Boolean

        Try
            'First attempt to load File version Record
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
            Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, FileVersionNo)
            If (LoadMatches(1).Count = 0) Then 'Create new file if not exists
                mFileName.Value = FileType
                mVersionNo.Value = FileVersionNo
                mSequence.Value = "000000"
                SaveIfNew()
            End If

            'Prepare to update Version Record
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)

            mFileName.Value = FileType
            mVersionNo.Value = FileVersionNo

            For RecTypePos = 1 To 15 Step 1
                RecordHash(RecTypePos).Value = RecordCounts(RecTypePos)
                RecordType(RecTypePos).Value = RecordTypes(RecTypePos)
                ValuePerHash(RecTypePos).Value = RecordTotals(RecTypePos)
                Oasys3DB.SetColumnAndValueParameter(RecordHash(RecTypePos).ColumnName, RecordHash(RecTypePos).Value)
                Oasys3DB.SetColumnAndValueParameter(RecordType(RecTypePos).ColumnName, RecordType(RecTypePos).Value)
                Oasys3DB.SetColumnAndValueParameter(ValuePerHash(RecTypePos).ColumnName, ValuePerHash(RecTypePos).Value)
            Next
            SetDBKeys()
            If Oasys3DB.Update > 0 Then 'update Version Record
            Else
                'Unable to update Version Record
                Return False
            End If

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function RecordVersionProcessed(ByVal FileType As String, ByVal FileVersionNo As String, ByVal FileSequenceNo As String) As Boolean

        Try
            'First attempt to load File version Record
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, FileName, FileType)
            Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, VersionNo, "00")
            LoadMatches()

            'Prepare to update Version Record
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)

            mFileName.Value = FileType
            mVersionNo.Value = "00"
            'Prepare to update Control Record

            mPVersion.Value = FileVersionNo
            Oasys3DB.SetColumnAndValueParameter(mPVersion.ColumnName, mPVersion.Value)
            mPSequence.Value = FileSequenceNo
            Oasys3DB.SetColumnAndValueParameter(mPSequence.ColumnName, mPSequence.Value)
            mErrCode.Value = ""
            Oasys3DB.SetColumnAndValueParameter(mErrCode.ColumnName, mErrCode.Value)

            SetDBKeys()
            If Oasys3DB.Update > 0 Then
                Return True
            Else
                'Unable to update Control Record
                Return False
            End If

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

#End Region

End Class
