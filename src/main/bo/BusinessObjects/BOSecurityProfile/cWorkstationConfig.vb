﻿<Serializable()> Public Class cWorkStationConfig
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "WorkStationConfig"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOFields.Add(_PrimaryFunction)
        BOFields.Add(_IsActive)
        BOFields.Add(_DateLastLoggedIn)
        BOFields.Add(_IsBarcodeBroken)
        BOFields.Add(_UseTouchScreen)
        BOFields.Add(_SecurityProfileID)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cWorkStationConfig)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cWorkStationConfig)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cWorkStationConfig))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cWorkStationConfig(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _PrimaryFunction As New ColField(Of String)("PrimaryFunction", "", "Primary Function", False, False)
    Private _IsActive As New ColField(Of Boolean)("IsActive", True, False, False)
    Private _DateLastLoggedIn As New ColField(Of Date)("DateLastLoggedIn", Nothing, "Last Logged In", False, False)
    Private _IsBarcodeBroken As New ColField(Of Boolean)("IsBarcodeBroken", False, "Is Barcode Broken", False, False)
    Private _UseTouchScreen As New ColField(Of Boolean)("UseTouchScreen", False, "Use Touch Screen", False, False)
    Private _SecurityProfileID As New ColField(Of Integer)("SecurityProfileID", 0, "Security Profile ID", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property PrimaryFunction() As ColField(Of String)
        Get
            PrimaryFunction = _PrimaryFunction
        End Get
        Set(ByVal value As ColField(Of String))
            _PrimaryFunction = value
        End Set
    End Property
    Public Property IsActive() As ColField(Of Boolean)
        Get
            IsActive = _IsActive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsActive = value
        End Set
    End Property
    Public Property DateLastLoggedIn() As ColField(Of Date)
        Get
            DateLastLoggedIn = _DateLastLoggedIn
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastLoggedIn = value
        End Set
    End Property
    Public Property IsBarcodeBroken() As ColField(Of Boolean)
        Get
            IsBarcodeBroken = _IsBarcodeBroken
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsBarcodeBroken = value
        End Set
    End Property
    Public Property UseTouchScreen() As ColField(Of Boolean)
        Get
            UseTouchScreen = _UseTouchScreen
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _UseTouchScreen = value
        End Set
    End Property
    Public Property SecurityProfileID() As ColField(Of Integer)
        Get
            SecurityProfileID = _SecurityProfileID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SecurityProfileID = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _MenuAccess As List(Of BOSecurityProfile.cProfileMenuAccess) = Nothing
    Private _Workstations As List(Of cWorkStationConfig) = Nothing

    Public Property Workstations() As List(Of cWorkStationConfig)
        Get
            If _Workstations Is Nothing Then LoadWorkstations()
            Return _Workstations
        End Get
        Set(ByVal value As List(Of cWorkStationConfig))
            _Workstations = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads workstation into current instance given ID
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <remarks></remarks>
    Public Sub LoadWorkstation(ByRef ID As Integer)

        ClearLists()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ID)
        LoadMatches()

    End Sub

    ''' <summary>
    ''' Load all tills into workstations collection of this instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadWorkstations()

        ClearLists()
        SortBy(_ID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        _Workstations = LoadMatches()

    End Sub

    Public Function MenuItemAccessAllowed(ByRef MenuID As Integer) As Boolean

        If _MenuAccess Is Nothing Then
            Dim cMenuAccessBO As New BOSecurityProfile.cProfileMenuAccess(Oasys3DB)
            _MenuAccess = cMenuAccessBO.GetMenuAccess(Me.SecurityProfileID.Value)
        End If

        For Each profile As cProfileMenuAccess In _MenuAccess
            If profile.MenuConfigID.Value = MenuID Then
                Return profile.AccessAllowed.Value
            End If
        Next

        Return False

    End Function

    Public Function ActivateWKStation(ByVal num As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsActive.ColumnName, True)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, num)
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function

    Public Function ActivateWKStation(ByVal num As Integer) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsActive.ColumnName, True)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, num)
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function

    Public Function DeActivateWKStation(ByVal num As String) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IsActive.ColumnName, False)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, num)
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
            Throw ex
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Returns datatable with all workstations (loaded into workstations collection) with columns (id,description,display)
    ''' </summary>
    ''' <param name="WithSelect">Set true to insert ---Select--- row at top of table</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetWorkstationsDatatable(Optional ByVal WithSelect As Boolean = False) As DataTable

        If _Workstations Is Nothing Then LoadWorkstations()

        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("id", GetType(Integer)))
        dt.Columns.Add(New DataColumn("description", GetType(String)))
        dt.Columns.Add(New DataColumn("display", GetType(String)))

        If WithSelect Then
            dt.Rows.Add(0, "", "---Select---")
        End If

        'add rows
        For Each ws As cWorkStationConfig In _Workstations
            Dim dr As DataRow = dt.NewRow
            dr(0) = ws.ID.Value
            dr(1) = ws.Description.Value
            dr(2) = ws.ID.Value & " - " & ws.Description.Value
            dt.Rows.Add(dr)
        Next

        Return dt

    End Function

    ''' <summary>
    ''' Returns security profile id of workstation for given id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetProfileId(ByVal id As Integer) As Integer

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnParameter(_SecurityProfileID.ColumnName)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, id)
        Return CInt(Oasys3DB.Query.Tables(0).Rows(0)(0))

    End Function

#End Region

End Class

