<Serializable()> Public Class cMenuConfig
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "MenuConfig"
        BOFields.Add(_ID)
        BOFields.Add(_AppName)
        BOFields.Add(_TabName)
        BOFields.Add(_Description)
        BOFields.Add(_AssemblyName)
        BOFields.Add(_ClassName)
        BOFields.Add(_AllowMultiple)
        BOFields.Add(_MasterID)
        BOFields.Add(_MenuType)
        BOFields.Add(_Parameters)
        BOFields.Add(_ImagePath)
        BOFields.Add(_ImageKey)
        BOFields.Add(_WaitForExit)
        BOFields.Add(_Timeout)
        BOFields.Add(_DoProcessingType)
        BOFields.Add(_DisplaySequence)
        BOFields.Add(_LoadMaximised)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cMenuConfig)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cMenuConfig)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cMenuConfig))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cMenuConfig(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _AppName As New ColField(Of String)("AppName", "", "Application Name", False, False)
    Private _TabName As New ColField(Of String)("TabName", "", "Tab Name", False, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _AssemblyName As New ColField(Of String)("AssemblyName", "", "Assembly Name", False, False)
    Private _ClassName As New ColField(Of String)("ClassName", "", "Class Name", False, False)
    Private _AllowMultiple As New ColField(Of Boolean)("AllowMultiple", False, "Allow Multiple", False, False)
    Private _MasterID As New ColField(Of Integer)("MasterID", 0, "Master ID", False, False)
    Private _MenuType As New ColField(Of Integer)("MenuType", 0, "Menu Type", False, False)
    Private _Parameters As New ColField(Of String)("Parameters", "", "Parameters", False, False)
    Private _ImagePath As New ColField(Of String)("ImagePath", "", "Image Path", False, False)
    Private _ImageKey As New ColField(Of String)("ImageKey", "", "Image Key", False, False)
    Private _WaitForExit As New ColField(Of Boolean)("WaitForExit", False, "Wait for Exit", False, False)
    Private _Timeout As New ColField(Of Integer)("Timeout", 0, "Timeout", False, False)
    Private _DoProcessingType As New ColField(Of Integer)("DoProcessingType", 0, "Do Processing Type", False, False)
    Private _DisplaySequence As New ColField(Of Integer)("DisplaySequence", 0, "Display Sequence", False, False)
    Private _LoadMaximised As New ColField(Of Boolean)("LoadMaximised", False, "Load Maximised", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property AppName() As ColField(Of String)
        Get
            AppName = _AppName
        End Get
        Set(ByVal value As ColField(Of String))
            _AppName = value
        End Set
    End Property
    Public Property TabName() As ColField(Of String)
        Get
            TabName = _TabName
        End Get
        Set(ByVal value As ColField(Of String))
            _TabName = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property AssemblyName() As ColField(Of String)
        Get
            AssemblyName = _AssemblyName
        End Get
        Set(ByVal value As ColField(Of String))
            _AssemblyName = value
        End Set
    End Property
    Public Property ClassName() As ColField(Of String)
        Get
            ClassName = _ClassName
        End Get
        Set(ByVal value As ColField(Of String))
            _ClassName = value
        End Set
    End Property
    Public Property AllowMultiple() As ColField(Of Boolean)
        Get
            AllowMultiple = _AllowMultiple
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AllowMultiple = value
        End Set
    End Property
    Public Property MasterID() As ColField(Of Integer)
        Get
            MasterID = _MasterID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MasterID = value
        End Set
    End Property
    Public Property MenuType() As ColField(Of Integer)
        Get
            MenuType = _MenuType
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MenuType = value
        End Set
    End Property
    Public Property Parameters() As ColField(Of String)
        Get
            Parameters = _Parameters
        End Get
        Set(ByVal value As ColField(Of String))
            _Parameters = value
        End Set
    End Property
    Public Property ImagePath() As ColField(Of String)
        Get
            ImagePath = _ImagePath
        End Get
        Set(ByVal value As ColField(Of String))
            _ImagePath = value
        End Set
    End Property
    Public Property ImageKey() As ColField(Of String)
        Get
            ImageKey = _ImageKey
        End Get
        Set(ByVal value As ColField(Of String))
            _ImageKey = value
        End Set
    End Property
    Public Property WaitForExit() As ColField(Of Boolean)
        Get
            WaitForExit = _WaitForExit
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _WaitForExit = value
        End Set
    End Property
    Public Property Timeout() As ColField(Of Integer)
        Get
            Timeout = _Timeout
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Timeout = value
        End Set
    End Property
    Public Property DoProcessingType() As ColField(Of Integer)
        Get
            DoProcessingType = _DoProcessingType
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DoProcessingType = value
        End Set
    End Property
    Public Property DisplaySequence() As ColField(Of Integer)
        Get
            DisplaySequence = _DisplaySequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DisplaySequence = value
        End Set
    End Property
    Public Property LoadMaximised() As ColField(Of Boolean)
        Get
            Return _LoadMaximised
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LoadMaximised = value
        End Set
    End Property

#End Region

#Region "Entities"

    Private _MenuConfigs As List(Of cMenuConfig) = Nothing
    Public Property MenuConfigs() As List(Of cMenuConfig)
        Get
            If _MenuConfigs Is Nothing Then
                LoadMenuConfigs()
            End If
            Return _MenuConfigs
        End Get
        Set(ByVal value As List(Of cMenuConfig))
            _MenuConfigs = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private Enum MenuTypes
        System = 0
        [Module]
        MainForm
        MenuGroup
        TabOption
    End Enum
    Private _SecurityLevel As Integer = 0


    Public Property SecurityLevel() As Integer
        Get
            Return _SecurityLevel
        End Get
        Set(ByVal value As Integer)
            _SecurityLevel = value
        End Set
    End Property

    ''' <summary>
    ''' Loads Master App Config for menu.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadMenuConfigs()

        Try
            Dim menu As New cMenuConfig(Oasys3DB)
            menu.AddLoadFilter(clsOasys3DB.eOperator.pEquals, menu.MasterID, _ID.Value)
            _MenuConfigs = menu.LoadMatches

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ProblemLoadingConfig, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads menu config for given ID into this instance. Throws Oasys error on exception
    ''' </summary>
    ''' <param name="ConfigID"></param>
    ''' <remarks></remarks>
    Public Sub Load(ByVal ConfigID As Integer)

        Try
            ClearLists()
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _ID, ConfigID)
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ProblemLoadingConfig, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns list of master menu config items
    ''' </summary>
    ''' <param name="ids"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMasterConfigs(ByVal ids As ArrayList) As List(Of cMenuConfig)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_MasterID.ColumnName, clsOasys3DB.eOperator.pEquals, 0)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pIn, ids)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        Dim menus As New List(Of cMenuConfig)
        For Each dr As DataRow In dt.Rows
            Dim m As New cMenuConfig(Oasys3DB)
            m.LoadFromRow(dr)
            menus.Add(m)
        Next

        Return menus

    End Function


    '''<summary>Retrieves System Name from database</summary>
    Public Function GetMenuName(ByRef MenuMasterID As Integer) As String
        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MenuType, MenuTypes.System)
        LoadMatches()

        MenuMasterID = _ID.Value
        Return _Description.Value

    End Function

    '''<summary>Updates the system name only</summary>
    Public Function UpdateNameLevel(ByRef UpdateMenuID As Integer, ByRef NewMenuName As String, ByVal NewLevel As Integer) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_AppName.ColumnName, NewMenuName)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, UpdateMenuID)
            Oasys3DB.Update()

            Return True

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
            Return False
        End Try

    End Function
    Public Function UpdateNameLevel(ByRef UpdateMenuID As Integer, ByRef NewMenuName As String, ByVal NewImagePath As String) As Boolean

        '   Save all properties to the database
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_AppName.ColumnName, NewMenuName)
        Oasys3DB.SetColumnAndValueParameter(_ImagePath.ColumnName, NewImagePath)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, UpdateMenuID)
        Oasys3DB.Update()

        Return True

    End Function


    '''<summary>Retrieves all sub groups and menu options available under the supplied menu id</summary>
    Public Function GetMenuSubOptions(ByVal MenuID As Integer) As List(Of cMenuConfig)

        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MasterID, MenuID)

        Return LoadMatches()

    End Function

    '''<summary>Retrieves only the sub groups available under the supplied menu id - mainly used by the editor</summary>
    Public Function GetMenuSubGroups(ByVal MenuID As Integer) As List(Of cMenuConfig)

        Dim lOptions As List(Of cMenuConfig) = Nothing

        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MasterID, MenuID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MenuType, MenuTypes.MenuGroup)


        Return LoadMatches()

    End Function

    '''<summary>Retrieves only the menu options available under the supplied menu id - mainly used by the editor</summary>
    Public Function GetMenuGroupOptions(ByVal MenuID As Integer) As List(Of cMenuConfig)

        Dim lOptions As List(Of cMenuConfig) = Nothing

        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MasterID, MenuID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MenuType, MenuTypes.MainForm)

        lOptions = LoadMatches()

        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MasterID, MenuID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MenuType, MenuTypes.Module)

        lOptions.AddRange(LoadMatches)

        Return lOptions

    End Function

    '''<summary>Populates the current Menu Object with the retrieved info for the supplied menu id</summary>
    Public Function GetMenuOption(ByVal MenuID As Integer) As Boolean

        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, MenuID)
        LoadMatches()

        Return True

    End Function

    '''<summary>Retrieves all the Tabs available under the supplied menu id</summary>
    Public Function GetTabOptions(ByVal MenuID As Integer) As List(Of cMenuConfig)

        Dim lOptions As List(Of cMenuConfig) = Nothing

        'Get all Main programs with application name
        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MasterID, MenuID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MenuType, MenuTypes.TabOption)

        lOptions = LoadMatches()

        Return lOptions

    End Function


    '''<summary>Returns the Display Name for the current Menu Option dependent on the Menu Type</summary>
    Public Function DisplayName() As String

        Select Case CType(_MenuType.Value, MenuTypes)
            Case MenuTypes.Module : Return _Description.Value
            Case MenuTypes.MainForm : Return _AppName.Value
            Case MenuTypes.MenuGroup : Return _AppName.Value
            Case MenuTypes.TabOption : Return _TabName.Value
            Case Else : Return "Unknown"
        End Select

    End Function




    Public Function CreateNewMenuGroup(ByVal GroupName As String, ByVal SequenceNo As Integer, ByVal GroupID As Integer) As Boolean

        Try
            _ID.Value = 0
            _AppName.Value = GroupName
            _TabName.Value = ""
            _Description.Value = ""
            _AssemblyName.Value = ""
            _ClassName.Value = ""
            _AllowMultiple.Value = False
            _MasterID.Value = GroupID
            _MenuType.Value = MenuTypes.MenuGroup
            _Parameters.Value = ""
            _ImagePath.Value = ""
            _ImageKey.Value = ""
            _WaitForExit.Value = True
            _Timeout.Value = 0
            _DisplaySequence.Value = SequenceNo

            SaveIfNew()

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
            Return False
        End Try

    End Function

    Public Function DeleteRecord(ByVal MenuID As Integer) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, MenuID)

        NoRecs = Oasys3DB.Delete()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function PreviousSequence(ByVal ID As Integer, ByVal seq As Integer) As List(Of cMenuConfig)

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._MasterID, ID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pLessThan, Me._DisplaySequence, seq)
        SortBy(_DisplaySequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)

        Return LoadMatches()

    End Function

    Public Function NextSequence(ByVal ID As Integer, ByVal seq As Integer) As List(Of cMenuConfig)

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._MasterID, ID)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Me._DisplaySequence, seq)
        SortBy(_DisplaySequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

        Return LoadMatches()

    End Function

    Public Function HighestSequence(ByVal ID As Integer) As Integer
        Dim maxSeqNo As Integer
        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.MasterID, ID)

        If Me.LoadMatches.Count > 0 Then
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.MasterID, ID)
            Me.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, Me.DisplaySequence, "MaxSeqNo")
            maxSeqNo = CInt(Me.GetAggregateField())
            HighestSequence = maxSeqNo
        Else
            HighestSequence = 0
        End If

    End Function

    Public Function UpdateDisplaySequence(ByVal ID As Integer, ByVal DisplaySequence As Integer) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, ID)
        Oasys3DB.SetColumnAndValueParameter(_DisplaySequence.ColumnName, DisplaySequence)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function InsertRecord(ByVal AppName As String, ByVal ModuleName As String, ByVal ModuleSequence As Integer, _
        ByVal TabName As String, ByVal TabSequence As Integer, ByVal Description As String, ByVal SecurityLevel As Integer, _
        ByVal AssemblyName As String, ByVal ClassName As String, ByVal AllowMultiple As Boolean, ByVal MasterId As Integer, _
        ByVal MenuType As Integer, ByVal Parameters As String, ByVal ImagePath As String, ByVal ImageKey As String, _
        ByVal WaitForExit As Boolean, ByVal Timeout As Integer, ByVal DoProcessing As Integer, ByVal DisplaySequence As Integer _
                                                                            ) As Boolean
        _AppName.Value = AppName
        _TabName.Value = TabName
        _DisplaySequence.Value = TabSequence
        _Description.Value = Description
        _AssemblyName.Value = AssemblyName
        _ClassName.Value = ClassName
        _AllowMultiple.Value = AllowMultiple
        _MasterID.Value = MasterId
        _MenuType.Value = MenuType
        _Parameters.Value = Parameters
        _ImagePath.Value = ImagePath
        _ImageKey.Value = ImageKey
        _WaitForExit.Value = WaitForExit
        _Timeout.Value = Timeout
        _DoProcessingType.Value = DoProcessing
        _DisplaySequence.Value = DisplaySequence

        Return SaveIfNew()

    End Function

    Public Function UpdateRecord(ByVal MenuID As Integer, ByVal AppName As String, ByVal ModuleName As String, ByVal ModuleSequence As Integer, _
        ByVal TabName As String, ByVal TabSequence As Integer, ByVal Description As String, ByVal SecurityLevel As Integer, _
        ByVal AssemblyName As String, ByVal ClassName As String, ByVal AllowMultiple As Boolean, ByVal MasterId As Integer, _
        ByVal MenuType As Integer, ByVal Parameters As String, ByVal ImagePath As String, ByVal ImageKey As String, _
        ByVal WaitForExit As Boolean, ByVal Timeout As Integer, ByVal DoProcessing As Integer _
                                                                            ) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetWhereParameter(ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, MenuID)
        Oasys3DB.SetColumnAndValueParameter(_AppName.ColumnName, AppName)
        Oasys3DB.SetColumnAndValueParameter(_TabName.ColumnName, TabName)
        Oasys3DB.SetColumnAndValueParameter(_DisplaySequence.ColumnName, TabSequence)
        Oasys3DB.SetColumnAndValueParameter(_Description.ColumnName, Description)
        Oasys3DB.SetColumnAndValueParameter(_AssemblyName.ColumnName, AssemblyName)
        Oasys3DB.SetColumnAndValueParameter(_ClassName.ColumnName, ClassName)
        Oasys3DB.SetColumnAndValueParameter(_AllowMultiple.ColumnName, AllowMultiple)
        Oasys3DB.SetColumnAndValueParameter(_MasterID.ColumnName, MasterId)
        Oasys3DB.SetColumnAndValueParameter(_MenuType.ColumnName, MenuType)
        Oasys3DB.SetColumnAndValueParameter(_Parameters.ColumnName, Parameters)
        Oasys3DB.SetColumnAndValueParameter(_ImagePath.ColumnName, ImagePath)
        Oasys3DB.SetColumnAndValueParameter(_ImageKey.ColumnName, ImageKey)
        Oasys3DB.SetColumnAndValueParameter(_WaitForExit.ColumnName, WaitForExit)
        Oasys3DB.SetColumnAndValueParameter(_Timeout.ColumnName, Timeout)
        Oasys3DB.SetColumnAndValueParameter(_DoProcessingType.ColumnName, DoProcessing)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function DeleteLinked(ByVal ID As Integer) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(MasterID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, ID)

        NoRecs = Oasys3DB.Delete()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

#End Region

End Class

