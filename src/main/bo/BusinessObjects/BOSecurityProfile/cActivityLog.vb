﻿<Serializable()> Public Class cActivityLog
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ActivityLog"
        BOFields.Add(_ID)
        BOFields.Add(_LogDate)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_WorkstationID)
        BOFields.Add(_MenuOptionID)
        BOFields.Add(_LoggedIn)
        BOFields.Add(_ForcedLogOut)
        BOFields.Add(_StartTime)
        BOFields.Add(_EndTime)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cActivityLog)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cActivityLog)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cActivityLog))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cActivityLog(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _LogDate As New ColField(Of Date)("LogDate", Nothing, "Date", False, False)
    Private _EmployeeID As New ColField(Of Integer)("EmployeeID", 0, "Employee ID", False, False)
    Private _WorkstationID As New ColField(Of String)("WorkstationID", "", "Workstation ID", False, False)
    Private _MenuOptionID As New ColField(Of Integer)("MenuOptionID", 0, "Menu Option ID", False, False)
    Private _LoggedIn As New ColField(Of Boolean)("LoggedIn", False, "Logged In", False, False)
    Private _ForcedLogOut As New ColField(Of Boolean)("ForcedLogOut", False, "Forced Log Out", False, False)
    Private _StartTime As New ColField(Of String)("StartTime", "000000", "Start Time", False, False)
    Private _EndTime As New ColField(Of String)("EndTime", "000000", "End Time", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property LogDate() As ColField(Of Date)
        Get
            LogDate = _LogDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LogDate = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of Integer)
        Get
            EmployeeID = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _EmployeeID = value
        End Set
    End Property
    Public Property WorkstationID() As ColField(Of String)
        Get
            WorkstationID = _WorkstationID
        End Get
        Set(ByVal value As ColField(Of String))
            _WorkstationID = value
        End Set
    End Property
    Public Property MenuOptionID() As ColField(Of Integer)
        Get
            MenuOptionID = _MenuOptionID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MenuOptionID = value
        End Set
    End Property
    Public Property LoggedIn() As ColField(Of Boolean)
        Get
            LoggedIn = _LoggedIn
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LoggedIn = value
        End Set
    End Property
    Public Property ForcedLogOut() As ColField(Of Boolean)
        Get
            ForcedLogOut = _ForcedLogOut
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ForcedLogOut = value
        End Set
    End Property
    Public Property StartTime() As ColField(Of String)
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal value As ColField(Of String))
            _StartTime = value
        End Set
    End Property
    Public Property EndTime() As ColField(Of String)
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal value As ColField(Of String))
            _EndTime = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function RecordUserLoggedIn(ByRef UserID As String, ByVal LogWorkStationID As String) As Boolean

        _LogDate.Value = Date.Now()
        _EmployeeID.Value = CInt(UserID)
        _WorkstationID.Value = LogWorkStationID
        _MenuOptionID.Value = 0
        _LoggedIn.Value = True
        _ForcedLogOut.Value = False
        _StartTime.Value = TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")
        _EndTime.Value = ""

        Return SaveIfNew()

    End Function

    Public Function RecordUserLoggedOut(ByRef UserID As String, ByVal LogWorkStationID As String, ByVal ForcedToLogOut As Boolean) As Boolean

        _LogDate.Value = Date.Now()
        _EmployeeID.Value = CInt(UserID)
        _WorkstationID.Value = LogWorkStationID
        _MenuOptionID.Value = 0
        _LoggedIn.Value = False
        _ForcedLogOut.Value = ForcedToLogOut
        _StartTime.Value = TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")
        _EndTime.Value = ""

        Return SaveIfNew()

    End Function

    Public Function RecordAccessedOption(ByRef UserID As String, ByVal LogWorkStationID As String, ByVal MenuOption As Integer) As Boolean

        _LogDate.Value = Date.Now()
        _EmployeeID.Value = CInt(UserID)
        _WorkstationID.Value = LogWorkStationID
        _MenuOptionID.Value = MenuOption
        _LoggedIn.Value = False
        _ForcedLogOut.Value = False
        _StartTime.Value = TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")
        _EndTime.Value = ""

        Return SaveIfNew()

    End Function

    Public Function UpdateOptionExited() As Boolean

        _EndTime.Value = TimeOfDay.Hour.ToString("00") & TimeOfDay.Minute.ToString("00") & TimeOfDay.Second.ToString("00")

        Return SaveIfExists()

    End Function

#End Region

End Class













