﻿<Serializable()> Public Class cProfileMenuAccess
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ProfileMenuAccess"
        BOFields.Add(_ID)
        BOFields.Add(_MenuConfigID)
        BOFields.Add(_AccessAllowed)
        BOFields.Add(_OverrideAllowed)
        BOFields.Add(_SecurityLevel)
        BOFields.Add(_IsManager)
        BOFields.Add(_IsSupervisor)
        BOFields.Add(_LastEdited)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cProfileMenuAccess)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cProfileMenuAccess)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cProfileMenuAccess))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cProfileMenuAccess(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _MenuConfigID As New ColField(Of Integer)("MenuConfigID", 0, "Menu Config ID", True, False)
    Private _AccessAllowed As New ColField(Of Boolean)("AccessAllowed", False, "Access Allowed", False, False)
    Private _OverrideAllowed As New ColField(Of Boolean)("OverrideAllowed", False, "Override Allowed", False, False)
    Private _SecurityLevel As New ColField(Of Integer)("SecurityLevel", 0, "Security Level", False, False)
    Private _IsManager As New ColField(Of Boolean)("IsManager", False, "Is Manager", False, False)
    Private _IsSupervisor As New ColField(Of Boolean)("IsSupervisor", False, "Is Supervisor", False, False)
    Private _LastEdited As New ColField(Of Date)("LastEdited", Nothing, "Last Edited", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property MenuConfigID() As ColField(Of Integer)
        Get
            MenuConfigID = _MenuConfigID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MenuConfigID = value
        End Set
    End Property
    Public Property AccessAllowed() As ColField(Of Boolean)
        Get
            AccessAllowed = _AccessAllowed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AccessAllowed = value
        End Set
    End Property
    Public Property OverrideAllowed() As ColField(Of Boolean)
        Get
            OverrideAllowed = _OverrideAllowed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OverrideAllowed = value
        End Set
    End Property
    Public Property SecurityLevel() As ColField(Of Integer)
        Get
            SecurityLevel = _SecurityLevel
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SecurityLevel = value
        End Set
    End Property
    Public Property IsManager() As ColField(Of Boolean)
        Get
            IsManager = _IsManager
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsManager = value
        End Set
    End Property
    Public Property IsSupervisor() As ColField(Of Boolean)
        Get
            IsSupervisor = _IsSupervisor
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsSupervisor = value
        End Set
    End Property
    Public Property LastEdited() As ColField(Of Date)
        Get
            LastEdited = _LastEdited
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastEdited = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetMenuAccess(ByVal id As Integer) As List(Of cProfileMenuAccess)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, id)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        Dim menus As New List(Of cProfileMenuAccess)
        For Each dr As DataRow In dt.Rows
            Dim p As New cProfileMenuAccess(Oasys3DB)
            p.LoadFromRow(dr)
            menus.Add(p)
        Next

        Return menus

    End Function

    Public Function GetMenuAccessIds(ByVal id As Integer) As ArrayList

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, id)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_AccessAllowed.ColumnName, clsOasys3DB.eOperator.pEquals, True)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        Dim menus As New ArrayList
        For Each dr As DataRow In dt.Rows
            menus.Add(dr(_MenuConfigID.ColumnName))
        Next

        Return menus

    End Function

    Public Function InsertRecord(ByVal MenuID As Integer, ByVal Userid As Integer) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(ID.ColumnName, Userid)
        Oasys3DB.SetColumnAndValueParameter(MenuConfigID.ColumnName, MenuID)
        Oasys3DB.SetColumnAndValueParameter(AccessAllowed.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(OverrideAllowed.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(SecurityLevel.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(IsSupervisor.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(IsManager.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(LastEdited.ColumnName, Now)

        If Oasys3DB.Insert() = -1 Then Return False
        Return True

    End Function

#End Region

End Class
