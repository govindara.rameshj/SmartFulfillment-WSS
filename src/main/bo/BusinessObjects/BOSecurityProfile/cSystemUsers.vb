<Serializable()> Public Class cSystemUsers
    Inherits OasysDBBO.cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemUsers"
        BOFields.Add(_ID)
        BOFields.Add(_EmployeeCode)
        BOFields.Add(_Name)
        BOFields.Add(_Initials)
        BOFields.Add(_Position)
        BOFields.Add(_PayrollID)
        BOFields.Add(_Password)
        BOFields.Add(_PasswordExpires)
        BOFields.Add(_IsSupervisor)
        BOFields.Add(_SupervisorPassword)
        BOFields.Add(_SupervisorPwdExpires)
        BOFields.Add(_Outlet)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_DeletedDate)
        BOFields.Add(_DeletedTime)
        BOFields.Add(_DeletedBy)
        BOFields.Add(_DeletedWhere)
        BOFields.Add(_TillReceiptName)
        BOFields.Add(_DefaultAmount)
        BOFields.Add(_LanguageCode)
        BOFields.Add(_IsManager)
        BOFields.Add(_SecurityProfileID)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemUsers)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemUsers)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemUsers))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemUsers(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "User ID", True, True)
    Private _EmployeeCode As New ColField(Of String)("EmployeeCode", "", "Employee Code", False, False)
    Private _Name As New ColField(Of String)("Name", "", "Name", False, False)
    Private _Initials As New ColField(Of String)("Initials", "", "Initials", False, False)
    Private _Position As New ColField(Of String)("Position", "", "Position", False, False)
    Private _PayrollID As New ColField(Of String)("PayrollID", "", "Payroll ID", False, False)
    Private _Password As New ColField(Of String)("Password", "", "Password", False, False)
    Private _PasswordExpires As New ColField(Of Date)("PasswordExpires", CDate("1900/01/01"), "Password Expires", False, False)
    Private _IsManager As New ColField(Of Boolean)("IsManager", False, "Manager", False, False)
    Private _IsSupervisor As New ColField(Of Boolean)("IsSupervisor", False, "Supervisor", False, False)
    Private _SupervisorPassword As New ColField(Of String)("SupervisorPassword", "", "Supervisor Password", False, False)
    Private _SupervisorPwdExpires As New ColField(Of Date)("SupervisorPwdExpires", CDate("1900/01/01"), "Supervisor Password Expires", False, False)
    Private _Outlet As New ColField(Of String)("Outlet", "", "Outlet", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IsDeleted", False, "Deleted", False, False)
    Private _DeletedDate As New ColField(Of Date)("DeletedDate", CDate("1900/01/01"), "Deleted Date", False, False)
    Private _DeletedTime As New ColField(Of String)("DeletedTime", "000000", "Deleted Time", False, False)
    Private _DeletedBy As New ColField(Of String)("DeletedBy", "", "Deleted By", False, False)
    Private _DeletedWhere As New ColField(Of String)("DeletedWhere", "", "Deleted Where", False, False)
    Private _TillReceiptName As New ColField(Of String)("TillReceiptName", "", "Till Receipt Name", False, False)
    Private _DefaultAmount As New ColField(Of Decimal)("DefaultAmount", 0, "Default Amount", False, False)
    Private _LanguageCode As New ColField(Of String)("LanguageCode", "", "Language Code", False, False)
    Private _SecurityProfileID As New ColField(Of Integer)("SecurityProfileID", 0, "Security Profile ID", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property EmployeeCode() As ColField(Of String)
        Get
            EmployeeCode = _EmployeeCode
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeCode = value
        End Set
    End Property
    Public Property Name() As ColField(Of String)
        Get
            Name = _Name
        End Get
        Set(ByVal value As ColField(Of String))
            _Name = value
        End Set
    End Property
    Public Property Initials() As ColField(Of String)
        Get
            Initials = _Initials
        End Get
        Set(ByVal value As ColField(Of String))
            _Initials = value
        End Set
    End Property
    Public Property Position() As ColField(Of String)
        Get
            Position = _Position
        End Get
        Set(ByVal value As ColField(Of String))
            _Position = value
        End Set
    End Property
    Public Property PayrollID() As ColField(Of String)
        Get
            PayrollID = _PayrollID
        End Get
        Set(ByVal value As ColField(Of String))
            _PayrollID = value
        End Set
    End Property
    Public Property Password() As ColField(Of String)
        Get
            Password = _Password
        End Get
        Set(ByVal value As ColField(Of String))
            _Password = value
        End Set
    End Property
    Public Property PasswordExpires() As ColField(Of Date)
        Get
            PasswordExpires = _PasswordExpires
        End Get
        Set(ByVal value As ColField(Of Date))
            _PasswordExpires = value
        End Set
    End Property
    Public Property IsManager() As ColField(Of Boolean)
        Get
            IsManager = _IsManager
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsManager = value
        End Set
    End Property
    Public Property IsSupervisor() As ColField(Of Boolean)
        Get
            IsSupervisor = _IsSupervisor
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsSupervisor = value
        End Set
    End Property
    Public Property SupervisorPassword() As ColField(Of String)
        Get
            SupervisorPassword = _SupervisorPassword
        End Get
        Set(ByVal value As ColField(Of String))
            _SupervisorPassword = value
        End Set
    End Property
    Public Property SupervisorPwdExpires() As ColField(Of Date)
        Get
            SupervisorPwdExpires = _SupervisorPwdExpires
        End Get
        Set(ByVal value As ColField(Of Date))
            _SupervisorPwdExpires = value
        End Set
    End Property
    Public Property Outlet() As ColField(Of String)
        Get
            Outlet = _Outlet
        End Get
        Set(ByVal value As ColField(Of String))
            _Outlet = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property DeletedDate() As ColField(Of Date)
        Get
            DeletedDate = _DeletedDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DeletedDate = value
        End Set
    End Property
    Public Property DeletedTime() As ColField(Of String)
        Get
            DeletedTime = _DeletedTime
        End Get
        Set(ByVal value As ColField(Of String))
            _DeletedTime = value
        End Set
    End Property
    Public Property DeletedBy() As ColField(Of String)
        Get
            DeletedBy = _DeletedBy
        End Get
        Set(ByVal value As ColField(Of String))
            _DeletedBy = value
        End Set
    End Property
    Public Property DeletedWhere() As ColField(Of String)
        Get
            DeletedWhere = _DeletedWhere
        End Get
        Set(ByVal value As ColField(Of String))
            _DeletedWhere = value
        End Set
    End Property
    Public Property TillReceiptName() As ColField(Of String)
        Get
            TillReceiptName = _TillReceiptName
        End Get
        Set(ByVal value As ColField(Of String))
            _TillReceiptName = value
        End Set
    End Property
    Public Property DefaultAmount() As ColField(Of Decimal)
        Get
            DefaultAmount = _DefaultAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DefaultAmount = value
        End Set
    End Property
    Public Property LanguageCode() As ColField(Of String)
        Get
            LanguageCode = _LanguageCode
        End Get
        Set(ByVal value As ColField(Of String))
            _LanguageCode = value
        End Set
    End Property
    Public Property SecurityProfileID() As ColField(Of Integer)
        Get
            SecurityProfileID = _SecurityProfileID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SecurityProfileID = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Profiles As List(Of BOSecurityProfile.cProfileMenuAccess) = Nothing
    Private _SecurityProfile As BOSecurityProfile.cSecurityProfile
    Private _Users As List(Of cSystemUsers) = Nothing

    Public Property Profiles() As List(Of cProfileMenuAccess)
        Get
            If _Profiles Is Nothing Then LoadProfiles()
            Return _Profiles
        End Get
        Set(ByVal value As List(Of cProfileMenuAccess))
            _Profiles = value
        End Set
    End Property
    Public Property User(ByVal UserID As Integer) As cSystemUsers
        Get
            For Each u As cSystemUsers In _Users
                If u.ID.Value = UserID Then Return u
            Next
            Return New cSystemUsers(Oasys3DB)
        End Get
        Set(ByVal value As cSystemUsers)
            For Each u As cSystemUsers In _Users
                If u.ID.Value = UserID Then
                    u = value
                End If
            Next
        End Set
    End Property
    Public Property Users() As List(Of cSystemUsers)
        Get
            If _Users Is Nothing Then
                LoadUsers()
            End If
            Return _Users
        End Get
        Set(ByVal value As List(Of cSystemUsers))
            _Users = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetUser(ByVal UserID As Integer) As cSystemUsers
        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, UserID)
        Dim users As List(Of cSystemUsers) = LoadMatches()

        If users.Count > 0 Then Return users(0)
        Return Nothing

    End Function

    Public Function GetUserByPayroll(ByVal PayrollID As Integer) As cSystemUsers
        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _PayrollID, CStr(PayrollID))
        Dim users As List(Of cSystemUsers) = LoadMatches()

        If users.Count > 0 Then Return users(0)
        Return Nothing

    End Function

    ''' <summary>
    ''' Loads user into current instance if exists and returns true, else returns false. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="UserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadUser(ByRef UserID As Integer) As Boolean

        Try
            ClearLoadField()
            ClearLoadFilter()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, UserID)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            If LoadMatches.Count = 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ProblemLoadingUsers, ex)
        End Try

    End Function

    ''' <summary>
    ''' Loads all users into users collection of this instance ordered by user ID. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadUsers()

        Try
            ClearLists()
            SortBy(_ID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            _Users = LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ProblemLoadingUsers, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all profile men access into profiles collection of this instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadProfiles()

        Try
            Dim p As New cProfileMenuAccess(Oasys3DB)
            p.AddLoadFilter(clsOasys3DB.eOperator.pEquals, p.ID, _SecurityProfileID.Value)
            _Profiles = p.LoadMatches

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Function CheckPassword(ByRef PasswordValue As String) As Boolean

        If (PasswordValue = _Password.Value) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CheckAuthPassword(ByRef PasswordValue As String) As Boolean

        If (PasswordValue = _SupervisorPassword.Value) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CheckPassword(ByVal UserID As Integer, ByRef PasswordValue As String) As Boolean

        LoadUser(UserID)
        Return CheckPassword(PasswordValue)

    End Function

    Public Function CheckAuthorisationPWD(ByVal UserID As Integer, ByRef PasswordValue As String) As Boolean
        LoadUser(UserID)
        Return CheckAuthPassword(PasswordValue)
    End Function

    Public Function MenuItemAccessAllowed(ByRef MenuConfigID As Integer, ByRef SecurityLevel As Integer) As Boolean

        Try
            For Each p As cProfileMenuAccess In Profiles
                If p.MenuConfigID.Value = MenuConfigID Then
                    SecurityLevel = p.SecurityLevel.Value
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function SecurityProfileDesc() As String

        Dim cSecurityProfileBO As New BOSecurityProfile.cSecurityProfile(Oasys3DB)

        If (_SecurityProfile Is Nothing = True) Then
            _SecurityProfile = cSecurityProfileBO.LoadProfile(CStr(Me.SecurityProfileID.Value))
        End If
        Return (cSecurityProfileBO.Description.Value)

    End Function

    Public Function GetUserNameNumber(ByVal UserID As Integer) As String
        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, UserID)
        LoadMatches()

        Return _ID.Value.ToString & " - " & _Initials.Value & " " & _Name.Value

    End Function

    ''' <summary>
    ''' Returns datatable with all users (loaded into users collection) with columns (id,initials,name,display). 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="WithSelect">Set true to insert ---Select--- row at top of table</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUsersDatatable(Optional ByVal WithSelect As Boolean = False) As DataTable

        Try
            If _Users Is Nothing Then LoadUsers()

            Dim dt As New DataTable
            dt.Columns.Add(New DataColumn("id", GetType(Integer)))
            dt.Columns.Add(New DataColumn("initials", GetType(String)))
            dt.Columns.Add(New DataColumn("name", GetType(String)))
            dt.Columns.Add(New DataColumn("display", GetType(String)))

            If WithSelect Then
                dt.Rows.Add(0, "", "", "---Select---")
            End If

            'add rows
            For Each u As cSystemUsers In _Users
                Dim dr As DataRow = dt.NewRow
                dr(0) = u.ID.Value
                dr(1) = u.Initials.Value
                dr(2) = u.Name.Value
                dr(3) = u.ID.Value.ToString("000") & " - " & u.Initials.Value & Space(1) & u.Name.Value
                dt.Rows.Add(dr)
            Next

            Return dt

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ProblemLoadingUsers, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns string representation of user ID, initials and name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UserNameNumber() As String
        Return _ID.Value.ToString & " - " & _Initials.Value & " " & _Name.Value
    End Function


    ''' <summary>
    ''' Checks to see if the user is able to login then sends the relevant response back 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ISISUserValidation(ByVal strUserID As String, ByVal strPassword As String) As String

        Const METHOD_NAME As String = "UserValidation"
        Dim BOUser As New BOSecurityProfile.cSystemUsers(Oasys3DB)
        Dim BOSystem As New BOSystem.cParameter(Oasys3DB)
        Dim BOSecurity As New BOSecurityProfile.cProfileMenuAccess(Oasys3DB)
        Dim strUserSecurity As String = String.Empty
        Dim strMessageResponse As String = String.Empty
        Try
            BOUser.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOUser.PayrollID, strUserID)
            If BOUser.LoadMatches.Count > 0 Then
                If strPassword = BOUser.Password.Value Then
                    Trace.WriteLine(METHOD_NAME & " Username, password is valid and permitted access")

                    strUserSecurity = CStr(BOUser.SecurityProfileID.Value)
                    If strUserSecurity.Length = 1 Then
                        strUserSecurity = "0" & strUserSecurity
                    End If
                    BOSecurity.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOSecurity.ID, CInt(strUserSecurity))
                    BOSecurity.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOSecurity.MenuConfigID, BOSystem.GetParameterInteger(275001))
                    BOSecurity.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                    If BOSecurity.LoadMatches.Count > 0 Then
                        strMessageResponse += "Y" 'Access granted
                        strMessageResponse += BOSecurity.SecurityLevel.Value.ToString 'Authorisation level
                    Else
                        strMessageResponse += "X" 'Access granted
                        strMessageResponse += "0" 'Authorisation level
                    End If

                    Return strMessageResponse
                End If 'strPassword = BOUser.Password.Value 
                Trace.WriteLine(METHOD_NAME & " username is valid but password is not")
                strMessageResponse += "N" 'UserID is found but not password
                strMessageResponse += "0" 'Authorisation level
                Return strMessageResponse
            Else
                Trace.WriteLine(METHOD_NAME & " unknown username")
                strMessageResponse += "?" 'User Not found
                strMessageResponse += "0" 'Authorisation level
                Return strMessageResponse
            End If 'BOUser.LoadMatches.Count > 0
        Catch ex As Exception
            Trace.WriteLine(METHOD_NAME & " An error has occured error: " & ex.Message)
            strMessageResponse += "?" 'UserID is found but not password
            strMessageResponse += "0" 'Authorisation level
            Return strMessageResponse
        End Try
    End Function

    Public Function ISISGetUserEmpoyeeID(ByVal strPayRollID As String) As String
        Const METHOD_NAME As String = "GetUserID"
        Dim BOUser As New BOSecurityProfile.cSystemUsers(Oasys3DB)
        Trace.WriteLine(METHOD_NAME & " Start")

        BOUser.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOUser.PayrollID, strPayRollID)
        If BOUser.LoadMatches.Count > 0 Then
            Return BOUser.EmployeeCode.Value
        Else
            Return "000"
        End If
    End Function

    Public Function GetSpecificEmployee(ByVal strEmployeeNum As String) As DataTable
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_EmployeeCode.ColumnName)
            Oasys3DB.SetColumnParameter(_Name.ColumnName)
            Oasys3DB.SetColumnParameter(_Initials.ColumnName)
            Oasys3DB.SetColumnParameter(_Position.ColumnName)
            Oasys3DB.SetColumnParameter(_PayrollID.ColumnName)
            Oasys3DB.SetColumnParameter(_TillReceiptName.ColumnName)
            Oasys3DB.SetColumnParameter(_DefaultAmount.ColumnName)
            Oasys3DB.SetColumnParameter(_IsSupervisor.ColumnName)
            Oasys3DB.SetWhereParameter(_EmployeeCode.ColumnName, clsOasys3DB.eOperator.pEquals, strEmployeeNum)
            Oasys3DB.SetOrderByParameter(_EmployeeCode.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)
            Return dt

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting Orders " & ex.Message, ex)
        End Try

    End Function

    Public Function GetEmployees() As DataTable
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_EmployeeCode.ColumnName)
            Oasys3DB.SetColumnParameter(_Name.ColumnName)
            Oasys3DB.SetColumnParameter(_Initials.ColumnName)
            Oasys3DB.SetColumnParameter(_Position.ColumnName)
            Oasys3DB.SetColumnParameter(_PayrollID.ColumnName)
            Oasys3DB.SetColumnParameter(_TillReceiptName.ColumnName)
            Oasys3DB.SetColumnParameter(_DefaultAmount.ColumnName)
            Oasys3DB.SetColumnParameter(_IsSupervisor.ColumnName)
            'Oasys3DB.SetWhereParameter(_EmployeeCode.ColumnName, clsOasys3DB.eOperator.pEquals, strEmployeeNum)
            Oasys3DB.SetOrderByParameter(_EmployeeCode.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)
            Return dt

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting Orders " & ex.Message, ex)
        End Try

    End Function

    Public Function ResetPassword(ByVal strEmployeeCode As String) As Boolean

        Dim strDate As String = Format(Now, "yyyy-MM-dd")
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            'Oasys3DB.SetColumnAndValueParameter(_Password.ColumnName, "")
            'Oasys3DB.SetColumnAndValueParameter(_SupervisorPassword.ColumnName, "")
            Oasys3DB.SetColumnAndValueParameter(_PasswordExpires.ColumnName, strDate)
            Oasys3DB.SetColumnAndValueParameter(_SupervisorPwdExpires.ColumnName, strDate)
            Oasys3DB.SetWhereParameter(_EmployeeCode.ColumnName, clsOasys3DB.eOperator.pEquals, strEmployeeCode)
            If Oasys3DB.Update() = -1 Then Return False
            Return True

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting Orders " & ex.Message, ex)
            Return False
        End Try
    End Function

    Public Function ChangePassword(ByVal intId As Integer) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_Password.ColumnName, _Password.Value)
            Oasys3DB.SetColumnAndValueParameter(_PasswordExpires.ColumnName, _PasswordExpires.Value)
            If _IsSupervisor.Value Then
                Oasys3DB.SetColumnAndValueParameter(_SupervisorPassword.ColumnName, _SupervisorPassword.Value)
                Oasys3DB.SetColumnAndValueParameter(_SupervisorPwdExpires.ColumnName, _SupervisorPwdExpires.Value)
            End If
            Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, intId)
            If Oasys3DB.Update() = -1 Then Return False
            Return True

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting Orders " & ex.Message, ex)
            Return False
        End Try
    End Function

#End Region

End Class



