﻿<Serializable()> Public Class cSecurityProfile
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub


    Public Overrides Sub Start()

        TableName = "SecurityProfile"
        BOFields.Add(_ID)
        BOFields.Add(_ProfileType)
        BOFields.Add(_Description)
        BOFields.Add(_Position)
        BOFields.Add(_IsAreaManager)
        BOFields.Add(_PasswordValidFor)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_DeletedBy)
        BOFields.Add(_DeletedDate)
        BOFields.Add(_DateLastEdited)
        BOFields.Add(_IsSystemAdmin)
        BOFields.Add(_IsHelpDeskUser)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSecurityProfile)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSecurityProfile)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSecurityProfile))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSecurityProfile(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _ProfileType As New ColField(Of String)("ProfileType", "", "Profile Type", False, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _Position As New ColField(Of String)("Position", "", "Position", False, False)
    Private _IsAreaManager As New ColField(Of Boolean)("IsAreaManager", False, "Is Area Manager", False, False)
    Private _PasswordValidFor As New ColField(Of Decimal)("PasswordValidFor", 0, "Password Valid For", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IsDeleted", False, "Is Deleled", False, False)
    Private _DeletedBy As New ColField(Of String)("DeletedBy", "", "Deleted By", False, False)
    Private _DeletedDate As New ColField(Of Date)("DeletedDate", Nothing, "Deleted Date", False, False)
    Private _DateLastEdited As New ColField(Of Date)("DateLastEdited", Nothing, "Date Last Edited", False, False)
    Private _IsSystemAdmin As New ColField(Of Boolean)("IsSystemAdmin", False, "Is System Admin", False, False)
    Private _IsHelpDeskUser As New ColField(Of Boolean)("IsHelpDeskUser", False, "Is Help Desk User", False, False)

#End Region

#Region "Public Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property ProfileType() As ColField(Of String)
        Get
            ProfileType = _ProfileType
        End Get
        Set(ByVal value As ColField(Of String))
            _ProfileType = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Position() As ColField(Of String)
        Get
            Position = _Position
        End Get
        Set(ByVal value As ColField(Of String))
            _Position = value
        End Set
    End Property
    Public Property IsAreaManager() As ColField(Of Boolean)
        Get
            IsAreaManager = _IsAreaManager
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsAreaManager = value
        End Set
    End Property
    Public Property PasswordValidFor() As ColField(Of Decimal)
        Get
            PasswordValidFor = _PasswordValidFor
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PasswordValidFor = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property DeletedBy() As ColField(Of String)
        Get
            DeletedBy = _DeletedBy
        End Get
        Set(ByVal value As ColField(Of String))
            _DeletedBy = value
        End Set
    End Property
    Public Property DeletedDate() As ColField(Of Date)
        Get
            DeletedDate = _DeletedDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DeletedDate = value
        End Set
    End Property
    Public Property DateLastEdited() As ColField(Of Date)
        Get
            DateLastEdited = _DateLastEdited
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastEdited = value
        End Set
    End Property
    Public Property IsSystemAdmin() As ColField(Of Boolean)
        Get
            IsSystemAdmin = _IsSystemAdmin
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsSystemAdmin = value
        End Set
    End Property
    Public Property IsHelpDeskUser() As ColField(Of Boolean)
        Get
            IsHelpDeskUser = _IsHelpDeskUser
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsHelpDeskUser = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function LoadProfile(ByRef GetProfileID As String) As cSecurityProfile

        Dim lstProfiles As List(Of cSecurityProfile) = Nothing

        Call ClearLoadField()
        Call ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.ID, CInt(GetProfileID))
        lstProfiles = LoadMatches()
        If (lstProfiles.Count = 0) Then
            Return Nothing
        Else
            Return lstProfiles(0)
        End If

    End Function

    Public Function GetAllProfiles() As List(Of cSecurityProfile)

        Dim lstProfiles As List(Of cSecurityProfile) = Nothing

        Call ClearLoadField()
        Call ClearLoadFilter()

        lstProfiles = LoadMatches()

        Return lstProfiles

    End Function

#End Region

End Class



