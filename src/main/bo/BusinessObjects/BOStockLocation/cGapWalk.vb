
<Serializable()> Public Class cGapWalk
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "GapWalk"
        BOFields.Add(_ID)
        BOFields.Add(_Sequence)
        BOFields.Add(_DateCreated)
        BOFields.Add(_Location)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_QuantityRequired)
        BOFields.Add(_IsPrinted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cGapWalk)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cGapWalk)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cGapWalk))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cGapWalk(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Function ISISInsertGapWalk(ByVal SKUList As List(Of String), ByVal SkuQuantity As List(Of String), ByVal strLocation As String) As Boolean
        Dim intcount As Integer = 0
        Dim intSequence As Integer = 0
        Dim listQuantityRequired As New List(Of String)
        Dim BOGapWalk As New BOStockLocation.cGapWalk(Oasys3DB)
        Dim Results As New Object

        Const METHOD_NAME As String = "GapWalk"

        'get the next gap walk sequence number
        BOGapWalk.AddAggregateField(clsOasys3DB.eAggregates.pAggMax, BOGapWalk.Sequence, "intSequence")
        Results = BOGapWalk.GetAggregateField
        If Results IsNot Nothing Then
            intSequence = CInt(Results.ToString)
        End If

        Do Until intcount = SKUList.Count

            'sets the gap walk sequence
            BOGapWalk.Sequence.Value = intSequence + 1

            'sets the location
            BOGapWalk.Location.Value = strLocation

            'sets the item SKU
            BOGapWalk.SkuNumber.Value = SKUList.Item(intcount).ToString

            'Sets the quantity required
            BOGapWalk.QuantityRequired.Value = CInt(listQuantityRequired.Item(intcount).ToString)

            'sets the date created to today
            BOGapWalk.DateCreated.Value = Date.Today

            'inserts the new record if it fails sends the error message back and exits the funcation
            If BOGapWalk.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                Trace.WriteLine(METHOD_NAME & " The insert of SKU " & SKUList.Item(intcount) & "was a failure")
                Return False
            End If

            intcount += 1
        Loop 'intCount = intQuantity
        Return True
    End Function
    Public Function ISISInsertOverstock(ByVal SKUList As List(Of String), ByVal SkuQuantity As List(Of String), ByVal strLocation As String) As Boolean
        Dim intCount As Integer = 0
        Dim intSequence As Integer = 0
        Dim BOGapWalk As New BOStockLocation.cGapWalk(Oasys3DB)
        Dim Results As New Object

        Const METHOD_NAME As String = "Overstock"

        BOGapWalk.AddAggregateField(clsOasys3DB.eAggregates.pAggMax, BOGapWalk.Sequence, "intSequence")
        Results = BOGapWalk.GetAggregateField
        If Results IsNot Nothing Then
            intSequence = CInt(Results.ToString)
        End If

        Do Until intCount = SKUList.Count
            'sets a load filter for both the SKU number and the location within the message
            BOGapWalk.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOGapWalk.SkuNumber, SKUList.Item(intCount))
            BOGapWalk.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOGapWalk.Location, strLocation)
            BOGapWalk.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)

            'if a record cannot be found a new one is inserted if one is found nothing is done
            If BOGapWalk.LoadMatches.Count = 0 Then
                BOGapWalk.Location.Value = strLocation
                BOGapWalk.SkuNumber.Value = SKUList.Item(intCount)
                BOGapWalk.QuantityRequired.Value = 0
                BOGapWalk.Sequence.Value = intSequence + 1
                BOGapWalk.DateCreated.Value = Date.Today
                If BOGapWalk.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                    Trace.WriteLine(METHOD_NAME & " The insert of SKU " & SKUList.Item(intCount) & "was a failure")
                    Return False
                End If
            Else
                Trace.WriteLine(METHOD_NAME & " The sku " & SKUList.Item(intCount) & " is already located to that position")
            End If
            intCount += 1
        Loop 'intCount = intQuantity
        Return True

    End Function

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Sequence As New ColField(Of Integer)("Sequence", 0, "Sequence", True, False)
    Private _DateCreated As New ColField(Of Date)("DateCreated", Nothing, "Date Created", False, False)
    Private _Location As New ColField(Of String)("Location", "", "Location", False, False)
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "SKU Number", False, False)
    Private _QuantityRequired As New ColField(Of Integer)("QuantityRequired", 0, "Qty Required", False, False)
    Private _IsPrinted As New ColField(Of Boolean)("IsPrinted", False, "Is Printed", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of Integer)
        Get
            Sequence = _Sequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Sequence = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property Location() As ColField(Of String)
        Get
            Location = _Location
        End Get
        Set(ByVal value As ColField(Of String))
            _Location = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property QuantityRequired() As ColField(Of Integer)
        Get
            QuantityRequired = _QuantityRequired
        End Get
        Set(ByVal value As ColField(Of Integer))
            _QuantityRequired = value
        End Set
    End Property
    Public Property IsPrinted() As ColField(Of Boolean)
        Get
            IsPrinted = _IsPrinted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsPrinted = value
        End Set
    End Property


#End Region

End Class
