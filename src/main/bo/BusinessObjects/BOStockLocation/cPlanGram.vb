﻿<Serializable()> Public Class cPlanGram
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PLANGRAM"
        BOFields.Add(_Store)
        BOFields.Add(_Number)
        BOFields.Add(_Name)
        BOFields.Add(_SegmentNumber)
        BOFields.Add(_FixtureNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_FixtureSequence)
        BOFields.Add(_Facings)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_Capacity)
        BOFields.Add(_Pack)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPlanGram)

        LoadBORecords(count)

        Dim col As New List(Of cPlanGram)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cPlanGram))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPlanGram(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Store As New ColField(Of String)("STORE", "", "Store", True, False)
    Private _Number As New ColField(Of String)("PLANNO", "", "Number", True, False)
    Private _Name As New ColField(Of String)("PLANNAME", "", "Name", False, False)
    Private _SegmentNumber As New ColField(Of String)("PLANSEGN", "", "Segment Number", True, False)
    Private _FixtureNumber As New ColField(Of String)("FIXTURENO", "", "Fixture Number", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU Number", True, False)
    Private _FixtureSequence As New ColField(Of String)("FIXTSEQN", "", "Fixture Sequence", True, False)
    Private _Facings As New ColField(Of String)("FACINGS", "", "Facings", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELE", False, "Deleted", False, False)
    Private _Capacity As New ColField(Of Integer)("CAPACITY", 0, "Capacity", False, False)
    Private _Pack As New ColField(Of Integer)("PACK", 0, "Pack", False, False)

#End Region

#Region "Field Properties"

    Public Property Store() As ColField(Of String)
        Get
            Return _Store
        End Get
        Set(ByVal value As ColField(Of String))
            _Store = value
        End Set
    End Property
    Public Property Number() As ColField(Of String)
        Get
            Return _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Name() As ColField(Of String)
        Get
            Return _Name
        End Get
        Set(ByVal value As ColField(Of String))
            _Name = value
        End Set
    End Property
    Public Property SegmentNumber() As ColField(Of String)
        Get
            Return _SegmentNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SegmentNumber = value
        End Set
    End Property
    Public Property FixtureNumber() As ColField(Of String)
        Get
            Return _FixtureNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _FixtureNumber = value
        End Set
    End Property
    Public Property SKUNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property FixtureSequence() As ColField(Of String)
        Get
            Return _FixtureSequence
        End Get
        Set(ByVal value As ColField(Of String))
            _FixtureSequence = value
        End Set
    End Property
    Public Property Facings() As ColField(Of String)
        Get
            Return _Facings
        End Get
        Set(ByVal value As ColField(Of String))
            _Facings = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            Return _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property Capacity() As ColField(Of Integer)
        Get
            Return _Capacity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Capacity = value
        End Set
    End Property
    Public Property Pack() As ColField(Of Integer)
        Get
            Return _Pack
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Pack = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Planograms As List(Of cPlanGram) = Nothing

    Public Property PlanGrams() As List(Of cPlanGram)
        Get
            If _Planograms Is Nothing Then
                _Planograms = LoadMatches()
            End If
            Return _Planograms
        End Get
        Set(ByVal value As List(Of cPlanGram))
            _Planograms = value
        End Set
    End Property
    Public ReadOnly Property PlanGrams(ByVal SkuNumber As String) As List(Of cPlanGram)
        Get
            If _Planograms Is Nothing Then
                _Planograms = LoadMatches()
            End If
            Return _Planograms.FindAll(Function(p As cPlanGram) p.SKUNumber.Value = SkuNumber)
        End Get
    End Property
    Public Property PlanogramInStore(ByVal Number As Integer, ByVal Name As String, ByVal SkuNumber As String) As cPlanGram
        Get
            If _Planograms Is Nothing Then Load(Number)
            For Each p As cPlanGram In _Planograms
                If CDbl(p.Number.Value) = Number AndAlso p.SKUNumber.Value = SkuNumber Then Return p
            Next

            'get store number
            Dim retOptions As New BOSystem.cRetailOptions(Oasys3DB)
            retOptions.LoadStoreAndAccountability()

            'create new planogram and save
            Dim sequence As Integer = PlanGrams.Count + 1
            Dim plan As New cPlanGram(Oasys3DB)
            plan.Store.Value = retOptions.Store.Value
            plan.Number.Value = Number.ToString("0000000000")
            plan.SegmentNumber.Value = "0000000001"
            plan.Name.Value = Name
            plan.FixtureNumber.Value = "0000000001"
            plan.FixtureSequence.Value = sequence.ToString("0000000000")
            plan.SKUNumber.Value = SkuNumber
            plan.SaveIfNew()

            _Planograms.Add(plan)
            Return plan
        End Get
        Set(ByVal value As cPlanGram)
            If _Planograms Is Nothing Then Load(Number)
            For Each p As cPlanGram In _Planograms
                If CDbl(p.Number.Value) = Number AndAlso p.SKUNumber.Value = SkuNumber Then p = value
            Next
        End Set
    End Property


#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all non-deleted planograms into this instance ordered by name, segment, fixture sequence, fixture number.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load()

        ClearLists()
        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
        SortBy(_Name.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_SegmentNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_FixtureSequence.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_FixtureNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        _Planograms = LoadMatches()

    End Sub

    ''' <summary>
    ''' Loads all non-deleted planograms into this instance ordered by name, segment, fixture sequence, fixture number
    '''  given planogram number.
    ''' </summary>
    ''' <param name="PlanogramNumber"></param>
    ''' <remarks></remarks>
    Public Sub Load(ByVal PlanogramNumber As Integer)

        ClearLists()
        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
        JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _Number, PlanogramNumber.ToString("0000000000"))
        SortBy(_Name.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_SegmentNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_FixtureSequence.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        SortBy(_FixtureNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        _Planograms = LoadMatches()

    End Sub

    ''' <summary>
    ''' Load given stock item planograms into Plangrams collection of this instance
    ''' </summary>
    ''' <param name="Stocks"></param>
    ''' <remarks></remarks>
    Public Sub LoadPlangrams(ByRef Stocks As List(Of BOStock.cStock))

        Dim al As New ArrayList
        For Each stock As BOStock.cStock In Stocks
            al.Add(stock.SkuNumber.Value)
        Next

        ClearLists()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, _SkuNumber, al)
        _Planograms = LoadMatches()

    End Sub

    Public Sub LoadPlangrams(ByRef Stocks As ArrayList)

        ClearLists()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, _SkuNumber, Stocks)
        _Planograms = LoadMatches()

    End Sub

    ''' <summary>
    ''' Deletes given in store location.
    ''' </summary>
    ''' <param name="Number"></param>
    ''' <remarks></remarks>
    Public Sub DeleteInStore(ByVal Number As Integer)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(_Number.ColumnName, clsOasys3DB.eOperator.pEquals, Number)
        Oasys3DB.Delete()

    End Sub

    ''' <summary>
    ''' Deletes given sku from given in store location.
    ''' </summary>
    ''' <param name="Number"></param>
    ''' <param name="SkuNumber"></param>
    ''' <remarks></remarks>
    Public Sub DeleteInStoreSku(ByVal Number As Integer, ByVal SkuNumber As String)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(_Number.ColumnName, clsOasys3DB.eOperator.pEquals, Number)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, SkuNumber)
        Oasys3DB.Delete()

    End Sub

    Public Function DeleteAll() As Boolean
        Oasys3DB.ExecuteSql("DELETE FROM PLANGRAM WHERE STORE > '   '")
    End Function

    ''' <summary>
    ''' Returns datatable of name, number, segment, sku with sorting.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNameNumberSegmentSkuDatatable() As DataTable

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggList, _Name.ColumnName, _Name.ColumnName)
        Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggList, _Number.ColumnName, _Number.ColumnName)
        Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggList, _SegmentNumber.ColumnName, _SegmentNumber.ColumnName)
        Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggList, _SkuNumber.ColumnName, _SkuNumber.ColumnName)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, False)
        Oasys3DB.SetOrderByParameter(_Name.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        Oasys3DB.SetOrderByParameter(_SegmentNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        Oasys3DB.SetOrderByParameter(_SkuNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        Return Oasys3DB.Query.Tables(0)

    End Function

#End Region

End Class











