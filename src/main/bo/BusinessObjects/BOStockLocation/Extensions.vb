﻿Public Module Extensions

    <System.Runtime.CompilerServices.Extension()> Function Capacity(ByRef Plangrams As List(Of cPlanGram)) As Integer
        Return Plangrams.Sum(Function(p As cPlanGram) p.Capacity.Value)
    End Function


End Module
