﻿Partial Class IslandDataSet
    Private _selectedType As IslandTypesRow = Nothing
    Shared _connectionString As String = String.Empty

    Public Property SelectedType() As IslandTypesRow
        Get
            Return _selectedType
        End Get
        Set(ByVal value As IslandTypesRow)
            _selectedType = value
        End Set
    End Property
    Public Property ConnectionString() As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property

    Public Sub New(ByVal connectionString As String)
        Me.New()
        _connectionString = connectionString
    End Sub

    ''' <summary>
    ''' Loads all data from tables into this dataset
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadData()

        Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
            Dim daIsland As New IslandDataSetTableAdapters.IslandsTableAdapter
            daIsland.Connection = sqlConnection
            daIsland.Fill(Me.Islands)

            Dim daTypes As New IslandDataSetTableAdapters.IslandTypesTableAdapter
            daTypes.Connection = sqlConnection
            daTypes.Fill(Me.IslandTypes)

            Dim daPlanNumbers As New IslandDataSetTableAdapters.PlanNumbersTableAdapter
            daPlanNumbers.Connection = sqlConnection
            daPlanNumbers.Fill(Me.PlanNumbers)

            Dim daPlanSegments As New IslandDataSetTableAdapters.PlanSegmentsTableAdapter
            daPlanSegments.Connection = sqlConnection
            daPlanSegments.Fill(Me.PlanSegments)

            Dim daPlanStocks As New IslandDataSetTableAdapters.PlanStocksTableAdapter
            daPlanStocks.Connection = sqlConnection
            daPlanStocks.Fill(Me.PlanStocks)
        End Using

    End Sub

    ''' <summary>
    ''' Performs deletions, inserts and updates for all tables in this dataset in that order
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PerformChanges()

        Me.Islands.PerformChanges()
        Me.PlanNumbers.PerformChanges()
        Me.PlanSegments.PerformChanges()
        Me.PlanStocks.PerformChanges()

    End Sub



    Partial Class IslandsDataTable

        ''' <summary>
        ''' Loads data for island segments
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub LoadData()

            Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                Dim daIsland As New IslandDataSetTableAdapters.IslandsTableAdapter
                daIsland.Connection = sqlConnection
                daIsland.Fill(Me)
            End Using

        End Sub

        ''' <summary>
        ''' Performs all deletions, inserts and updates in that order
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PerformChanges()

            Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                'delete islands
                Dim daIsland As New IslandDataSetTableAdapters.IslandsTableAdapter
                daIsland.Connection = sqlConnection
                Dim dt As BOStockLocation.IslandDataSet.IslandsDataTable = CType(Me.GetChanges(DataRowState.Deleted), IslandsDataTable)
                If dt IsNot Nothing Then
                    For index As Integer = dt.Rows.Count - 1 To 0 Step -1
                        daIsland.Delete(CInt(dt.Rows(index)(0, DataRowVersion.Original)), CStr(dt.Rows(index)(1, DataRowVersion.Original)), CInt(dt.Rows(index)(2, DataRowVersion.Original)))
                    Next
                End If

                'insert islands
                dt = CType(Me.GetChanges(DataRowState.Added), IslandsDataTable)
                If dt IsNot Nothing Then
                    For Each dr As IslandsRow In dt.Rows
                        daIsland.Insert(dr.IslandId, dr.Code, dr.Id, dr.PointX, dr.PointY, dr.PlanNumber, dr.PlanSegment, dr.WalkSequence)
                    Next
                End If

                'update altered islands
                dt = CType(Me.GetChanges(DataRowState.Modified), IslandsDataTable)
                If dt IsNot Nothing Then
                    daIsland.Update(dt)
                End If

                Me.AcceptChanges()
            End Using

        End Sub



        ''' <summary>
        ''' returns next island ID for given parameters. Used for creating new segment
        ''' </summary>
        ''' <param name="islandId"></param>
        ''' <param name="code"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextIslandId(ByVal islandId As Integer, ByVal code As String) As Integer

            Dim max As Integer = 0
            For Each dr As IslandsRow In Me.Where(Function(f As IslandsRow) f.IslandId = islandId And f.Code = code)
                max = Math.Max(max, dr.Id)
            Next
            Return max + 1

        End Function

        ''' <summary>
        ''' Returns next walk sequence number
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextWalkSequence() As Integer

            Return Me.Max(Function(f As IslandsRow) f.WalkSequence) + 1

        End Function

        ''' <summary>
        ''' Returns arraylist of all available walk sequence numbers
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function AvailableWalkSequence() As ArrayList

            'create walk sequence arraylist
            Dim walks As New ArrayList
            Dim walk As Integer = 1
            For Each dr As IslandsRow In Me.Where(Function(f As IslandsRow) f.IslandTypesRow.Zorder = 0)
                walks.Add(walk)
                walk += 1
            Next

            For Each dr As IslandsRow In Me.Rows
                If dr.WalkSequence <> 0 Then walks.Remove(dr.WalkSequence)
            Next

            Return walks

        End Function

        ''' <summary>
        ''' Returns island row for given plan number and segment
        ''' </summary>
        ''' <param name="number"></param>
        ''' <param name="segment"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetIslandByPlanogram(ByVal number As String, ByVal segment As String) As IslandsRow

            Dim row As IslandsRow = CType(Me.Rows.Find(Function(f As IslandsRow) f.PlanNumber = CInt(number) AndAlso f.PlanSegment = CByte(segment)), IslandsRow)
            Return row

        End Function

        ''' <summary>
        ''' Returns location string representation without separators of given message. Returns '00000?' on error or no record found
        ''' </summary>
        ''' <param name="header"></param>
        ''' <param name="message"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetLocationByPlanogram(ByVal header As String, ByVal message As String) As String

            Dim sb As New StringBuilder(header)

            Try
                'Location lookup(fetch the physical in-store location from the planogram "location code")
                Dim number As String = message.Substring(0, 6)
                Dim segment As String = message.Substring(6, 2)

                'find row
                Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                    Dim daIsland As New IslandDataSetTableAdapters.IslandsTableAdapter
                    daIsland.Connection = sqlConnection
                    Dim dt As IslandsDataTable = daIsland.GetDataByPlanogram(CInt(number), CByte(segment))
                    If dt.Rows.Count > 0 Then
                        Dim dr As IslandsRow = CType(dt.Rows(0), IslandsRow)
                        sb.Append(dr.ToStringNoSeparators & "E")
                    Else
                        'if nothing loads send the response message back with the error '?' sign
                        Trace.WriteLine("No record was found")
                        sb.Append("00000?")
                    End If
                End Using

            Catch ex As Exception
                'if a error happens send the response message back with the error '?' sign
                Trace.WriteLine("An error has occured with the location lookup error: " & ex.Message)
                sb.Append("00000?")
            End Try

            Return sb.ToString

        End Function

    End Class

    Partial Class IslandsRow

        Public Overrides Function ToString() As String
            Return Me.IslandId.ToString("00") & "/" & Me.Code & "/" & Me.Id.ToString("00")
        End Function

        Public Function ToStringNoSeparators() As String
            Return Me.IslandId.ToString("00") & Me.Code & Me.Id.ToString("00")
        End Function

        Public Function Point() As System.Windows.Point
            Return New System.Windows.Point(Me.PointX, Me.PointY)
        End Function

    End Class



    Partial Class PlanNumbersDataTable

        ''' <summary>
        ''' Returns next planogram number. Used for instore locations
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextNumber() As Integer

            Dim max As Integer = Math.Max(990000, Me.Max(Function(f As PlanNumbersRow) f.Number))
            Return max + 1

        End Function

        ''' <summary>
        ''' Performs all deletions, inserts and updates in that order
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PerformChanges()

            Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                Dim daPlanNumbers As New IslandDataSetTableAdapters.PlanNumbersTableAdapter
                daPlanNumbers.Connection = sqlConnection

                'deletions
                Dim dt As PlanNumbersDataTable = CType(Me.GetChanges(DataRowState.Deleted), PlanNumbersDataTable)
                If dt IsNot Nothing Then
                    For index As Integer = dt.Rows.Count - 1 To 0 Step -1
                        daPlanNumbers.Delete(CInt(dt.Rows(index)(0, DataRowVersion.Original)))
                    Next
                End If

                'insert plan numbers
                dt = CType(Me.GetChanges(DataRowState.Added), PlanNumbersDataTable)
                If dt IsNot Nothing Then
                    For Each dr As PlanNumbersRow In dt.Rows
                        daPlanNumbers.Insert(dr.Number, dr.Name, dr.IsActive, dr.Colour)
                    Next
                End If

                'update plan numbers if changed
                dt = CType(Me.GetChanges(DataRowState.Modified), PlanNumbersDataTable)
                If dt IsNot Nothing Then
                    daPlanNumbers.Update(dt)
                End If

                Me.AcceptChanges()

            End Using

        End Sub

    End Class

    Partial Class PlanNumbersRow

        ''' <summary>
        ''' Returns next segment number for this planogram row
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextSegment() As Integer

            Dim rows As PlanSegmentsRow() = Me.GetPlanSegmentsRows
            If rows Is Nothing Then Return 1
            Return rows.Count + 1

        End Function

    End Class



    Partial Class PlanSegmentsDataTable

        ''' <summary>
        ''' Performs all deletions, inserts and updates in that order
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PerformChanges()

            Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                Dim daPlanSegments As New IslandDataSetTableAdapters.PlanSegmentsTableAdapter
                daPlanSegments.Connection = sqlConnection

                'do deletions
                Dim dt As PlanSegmentsDataTable = CType(Me.GetChanges(DataRowState.Deleted), PlanSegmentsDataTable)
                If dt IsNot Nothing Then
                    For index As Integer = dt.Rows.Count - 1 To 0 Step -1
                        daPlanSegments.Delete(CInt(dt.Rows(index)(0, DataRowVersion.Original)), CByte(dt.Rows(index)(1, DataRowVersion.Original)))
                    Next
                End If

                'insert plan numbers
                dt = CType(Me.GetChanges(DataRowState.Added), PlanSegmentsDataTable)
                If dt IsNot Nothing Then
                    For Each dr As PlanSegmentsRow In dt.Rows
                        daPlanSegments.Insert(dr.Number, dr.Segment, dr.Name, dr.IsActive)
                    Next
                End If

                'update plan numbers if changed
                dt = CType(Me.GetChanges(DataRowState.Modified), PlanSegmentsDataTable)
                If dt IsNot Nothing Then
                    daPlanSegments.Update(dt)
                End If

                Me.AcceptChanges()
            End Using

        End Sub

    End Class

    Partial Class PlanSegmentsRow

        ''' <summary>
        ''' Returns string representation of segment number and segment padded with zeroes
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ToString() As String
            Return Me.Number.ToString("000000") & Space(1) & Me.Segment.ToString("00")
        End Function

        ''' <summary>
        ''' Returns next sequence number for fixture number 1. Used in instore stock item adding
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextSequence() As Integer

            Dim rows As PlanStocksRow() = Me.GetPlanStocksRows
            If rows Is Nothing Then Return 1
            Return rows.Count + 1

        End Function

    End Class



    Partial Class PlanStocksDataTable

        ''' <summary>
        ''' Performs all deletions, inserts and updates in that order
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PerformChanges()

            Using sqlConnection As New System.Data.SqlClient.SqlConnection(_connectionString)
                Dim daPlanStocks As New IslandDataSetTableAdapters.PlanStocksTableAdapter
                daPlanStocks.Connection = sqlConnection

                'do deletions first
                Dim dt As PlanStocksDataTable = CType(Me.GetChanges(DataRowState.Deleted), PlanStocksDataTable)
                If dt IsNot Nothing Then
                    For index As Integer = dt.Rows.Count - 1 To 0 Step -1
                        daPlanStocks.Delete(CInt(dt.Rows(index)(0, DataRowVersion.Original)), CByte(dt.Rows(index)(1, DataRowVersion.Original)), CInt(dt.Rows(index)(2, DataRowVersion.Original)), CInt(dt.Rows(index)(3, DataRowVersion.Original)), CStr(dt.Rows(index)(4, DataRowVersion.Original)))
                    Next
                End If

                'insert plan stocks
                dt = CType(Me.GetChanges(DataRowState.Added), PlanStocksDataTable)
                If dt IsNot Nothing Then
                    For Each dr As PlanStocksRow In dt.Rows
                        daPlanStocks.Insert(dr.Number, dr.Segment, dr.Fixture, CInt(dr.SkuNumber), dr.SkuNumber, dr.Facings, dr.Capacity, dr.Pack, dr.IsActive)
                    Next
                End If

                'update plan stocks if changed
                dt = CType(Me.GetChanges(DataRowState.Modified), PlanStocksDataTable)
                If dt IsNot Nothing Then
                    daPlanStocks.Update(dt)
                End If

                Me.AcceptChanges()
            End Using

        End Sub

    End Class


End Class
