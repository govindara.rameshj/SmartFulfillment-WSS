﻿<Serializable()> Public Class cHierarchyMaster
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HIEMAS"
        BOFields.Add(_Level)
        BOFields.Add(_MemberNumber)
        BOFields.Add(_Description)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_MaxOverride)
        BOFields.Add(_ReductionWeek1)
        BOFields.Add(_ReductionWeek2)
        BOFields.Add(_ReductionWeek3)
        BOFields.Add(_ReductionWeek4)
        BOFields.Add(_ReductionWeek5)
        BOFields.Add(_ReductionWeek6)
        BOFields.Add(_ReductionWeek7)
        BOFields.Add(_ReductionWeek8)
        BOFields.Add(_ReductionWeek9)
        BOFields.Add(_ReductionWeek10)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHierarchyMaster)

        LoadBORecords(count)

        Dim col As New List(Of cHierarchyMaster)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cHierarchyMaster))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHierarchyMaster(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Level As New ColField(Of String)("LEVL", "", "Level", True, False)
    Private _MemberNumber As New ColField(Of String)("NUMB", "", "Member Number", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Is Deleted", False, False)
    Private _MaxOverride As New ColField(Of Integer)("MaxOverride", 0, "Max Override", False, False)
    Private _ReductionWeek1 As New ColField(Of Decimal)("ReductionWeek1", 0, "Reduction Week 1", False, False, 2)
    Private _ReductionWeek2 As New ColField(Of Decimal)("ReductionWeek2", 0, "Reduction Week 2", False, False, 2)
    Private _ReductionWeek3 As New ColField(Of Decimal)("ReductionWeek3", 0, "Reduction Week 3", False, False, 2)
    Private _ReductionWeek4 As New ColField(Of Decimal)("ReductionWeek4", 0, "Reduction Week 4", False, False, 2)
    Private _ReductionWeek5 As New ColField(Of Decimal)("ReductionWeek5", 0, "Reduction Week 5", False, False, 2)
    Private _ReductionWeek6 As New ColField(Of Decimal)("ReductionWeek6", 0, "Reduction Week 6", False, False, 2)
    Private _ReductionWeek7 As New ColField(Of Decimal)("ReductionWeek7", 0, "Reduction Week 7", False, False, 2)
    Private _ReductionWeek8 As New ColField(Of Decimal)("ReductionWeek8", 0, "Reduction Week 8", False, False, 2)
    Private _ReductionWeek9 As New ColField(Of Decimal)("ReductionWeek9", 0, "Reduction Week 9", False, False, 2)
    Private _ReductionWeek10 As New ColField(Of Decimal)("ReductionWeek10", 0, "Reduction Week 10", False, False, 2)

#End Region

#Region "Fields Properties"

    Public Property Level() As ColField(Of String)
        Get
            Level = _Level
        End Get
        Set(ByVal value As ColField(Of String))
            _Level = value
        End Set
    End Property
    Public Property MemberNumber() As ColField(Of String)
        Get
            MemberNumber = _MemberNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _MemberNumber = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property MaxOverride() As ColField(Of Integer)
        Get
            MaxOverride = _MaxOverride
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MaxOverride = value
        End Set
    End Property

    Public Property ReductionWeek(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            'Extract Column Name
            Dim ColName As String = ReductionWeek1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = ReductionWeek1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property ReductionWeek1() As ColField(Of Decimal)
        Get
            ReductionWeek1 = _ReductionWeek1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek1 = value
        End Set
    End Property
    Public Property ReductionWeek2() As ColField(Of Decimal)
        Get
            ReductionWeek2 = _ReductionWeek2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek2 = value
        End Set
    End Property
    Public Property ReductionWeek3() As ColField(Of Decimal)
        Get
            ReductionWeek3 = _ReductionWeek3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek3 = value
        End Set
    End Property
    Public Property ReductionWeek4() As ColField(Of Decimal)
        Get
            ReductionWeek4 = _ReductionWeek4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek4 = value
        End Set
    End Property
    Public Property ReductionWeek5() As ColField(Of Decimal)
        Get
            ReductionWeek5 = _ReductionWeek5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek5 = value
        End Set
    End Property
    Public Property ReductionWeek6() As ColField(Of Decimal)
        Get
            ReductionWeek6 = _ReductionWeek6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek6 = value
        End Set
    End Property
    Public Property ReductionWeek7() As ColField(Of Decimal)
        Get
            ReductionWeek7 = _ReductionWeek7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek7 = value
        End Set
    End Property
    Public Property ReductionWeek8() As ColField(Of Decimal)
        Get
            ReductionWeek8 = _ReductionWeek8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek8 = value
        End Set
    End Property
    Public Property ReductionWeek9() As ColField(Of Decimal)
        Get
            ReductionWeek9 = _ReductionWeek9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek9 = value
        End Set
    End Property
    Public Property ReductionWeek10() As ColField(Of Decimal)
        Get
            ReductionWeek10 = _ReductionWeek10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek10 = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns markdown schedule for given hierarchy values or nothing if none found 
    ''' </summary>
    ''' <param name="Category"></param>
    ''' <param name="Group"></param>
    ''' <param name="SubGroup"></param>
    ''' <param name="Style"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSchedule(ByVal Category As String, ByVal Group As String, ByVal SubGroup As String, ByVal Style As String) As Decimal()

        Dim Hierachy() As String = {Style, SubGroup, Group, Category}
        Dim level As Integer = 2

        For Each number As String In Hierachy
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Level, CStr(level))
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _MemberNumber, number)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)

            If LoadMatches.Count > 0 Then
                'check that not all zero
                Dim allZero As Boolean = True
                Dim schedule(9) As Decimal

                For index As Integer = 0 To 9
                    schedule(index) = ReductionWeek(index + 1).Value
                    If ReductionWeek(index + 1).Value <> 0 Then allZero = False
                Next

                If Not allZero Then Return schedule
            End If

            level += 1
        Next

        Return Nothing

    End Function

#End Region

End Class
