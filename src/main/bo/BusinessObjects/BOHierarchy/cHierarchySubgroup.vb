﻿<Serializable()> Public Class cHierachySubgroup
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HIESGP"
        BOFields.Add(_Category)
        BOFields.Add(_Group)
        BOFields.Add(_SubGroup)
        BOFields.Add(_Description)
        BOFields.Add(_Alpha)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHierachySubgroup)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHierachySubgroup)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHierachySubgroup))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHierachySubgroup(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Category As New ColField(Of String)("NUMB", "", "Category", True, False)
    Private _Group As New ColField(Of String)("GROU", "", "Group", True, False)
    Private _SubGroup As New ColField(Of String)("SGRP", "", "Subgroup", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _Alpha As New ColField(Of String)("ALPH", "", "Aplha", False, False)

#End Region

#Region "Field Properties"

    Public Property Category() As ColField(Of String)
        Get
            Return _Category
        End Get
        Set(ByVal value As ColField(Of String))
            _Category = value
        End Set
    End Property
    Public Property Group() As ColField(Of String)
        Get
            Return _Group
        End Get
        Set(ByVal value As ColField(Of String))
            _Group = value
        End Set
    End Property
    Public Property SubGroup() As ColField(Of String)
        Get
            Return _SubGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _SubGroup = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Alpha() As ColField(Of String)
        Get
            Return _Alpha
        End Get
        Set(ByVal value As ColField(Of String))
            _Alpha = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _Hierarchies As List(Of cHierachySubgroup) = Nothing

    Public Property Hierarchies() As List(Of cHierachySubgroup)
        Get
            Return _Hierarchies
        End Get
        Set(ByVal value As List(Of cHierachySubgroup))
            _Hierarchies = value
        End Set
    End Property

    ''' <summary>
    ''' Get datatable of subgroups for given category, group with columns (number,name) and key=number
    ''' </summary>
    ''' <param name=" Group">Enter group string</param>
    ''' <param name="Category">Enter category string</param>
    ''' <param name="WithAll">Set to true to include '---All---' header with '000000' value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNumberNameTable(ByVal Category As String, ByVal Group As String, Optional ByVal WithAll As Boolean = False) As DataTable

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_SubGroup.ColumnName)
            Oasys3DB.SetColumnParameter(_Description.ColumnName, Oasys3.DB.clsOasys3DB.Trims.Right)
            Oasys3DB.SetWhereParameter(_Category.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Category)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_Group.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Group)
            Oasys3DB.SetOrderByParameter(_Description.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            dt.Columns(0).ColumnName = "number"
            dt.Columns(1).ColumnName = "name"
            dt.PrimaryKey = New DataColumn() {dt.Columns(0)}

            If WithAll Then
                Dim dr As DataRow = dt.NewRow
                dr(0) = "000000"
                dr(1) = "---All---"
                dt.Rows.InsertAt(dr, 0)
            End If

            Return dt

        Catch ex As Exception
            Throw New Exception("Problem getting hierarchy subgroups", ex)
        End Try

    End Function

    Public Function GetDescription(ByVal Category As String, ByVal Group As String, ByVal Subgroup As String) As String

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_Description.ColumnName)
            Oasys3DB.SetWhereParameter(_Category.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Category)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_Group.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Group)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_SubGroup.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Subgroup)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If IsDBNull(dt.Rows(0).Item(0)) Then Return String.Empty
            Return dt.Rows(0).Item(0).ToString.Trim

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

End Class
