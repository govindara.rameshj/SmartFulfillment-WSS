﻿<Serializable()> Public Class cHierarchyXref
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKHIR"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_HierarchyNumber)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHierarchyXref)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHierarchyXref)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHierarchyXref))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHierarchyXref(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "", "Sku Number", True, False)
    Private _HierarchyNumber As New ColField(Of String)("HIER", "", "Hierarchy Number", True, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Is Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property HierarchyNumber() As ColField(Of String)
        Get
            HierarchyNumber = _HierarchyNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _HierarchyNumber = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function DeleteAll() As Boolean
        Oasys3DB.ExecuteSql("DELETE FROM STKHIR WHERE SKUN > '000000'")
    End Function

#End Region

End Class
