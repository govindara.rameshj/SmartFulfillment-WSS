﻿Public Class vwHierarchies
    Public Enum col
        CategoryNumber
        CategoryName
        GroupNumber
        GroupName
        SubgroupNumber
        SubgroupName
        StyleNumber
        StyleName
    End Enum
    Private _oasys3DB As Oasys3.DB.clsOasys3DB
    Private _table As DataTable = Nothing
    Private _view As String = "vwHierarchies"

    Public ReadOnly Property Table() As DataTable
        Get
            Return _table
        End Get
    End Property


    Public Sub New()
        _oasys3DB = New Oasys3.DB.clsOasys3DB("Default", 0)
    End Sub
    Public Sub New(ByVal oasys3DB As Oasys3.DB.clsOasys3DB)
        _oasys3DB = oasys3DB
    End Sub


    ''' <summary>
    ''' Loads view table into Table property of this instance.
    '''  Throws exception on error
    ''' </summary>
    ''' <param name="withSorting"></param>
    ''' <remarks></remarks>
    Public Sub LoadView(Optional ByVal withSorting As Boolean = False)

        Try
            Dim sb As New StringBuilder("SELECT * FROM " & _view)
            If withSorting Then
                sb.Append(" ORDER BY ")
                sb.Append(col.CategoryName.ToString & ", ")
                sb.Append(col.GroupName.ToString & ", ")
                sb.Append(col.SubgroupName.ToString & ", ")
                sb.Append(col.StyleName.ToString)
            End If

            _table = _oasys3DB.ExecuteSql(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Public Function GetCategoryName(ByVal categoryNumber As String) As String

        Try
            If _table Is Nothing Then LoadView()
            For Each dr As DataRow In _table.Rows
                If CStr(dr(col.CategoryNumber)) = categoryNumber Then
                    Return dr(col.CategoryName).ToString.Trim
                End If
            Next

            Return My.Resources.WarnNoCategory

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetGroupName(ByVal groupNumber As String) As String

        Try
            If _table Is Nothing Then LoadView()
            For Each dr As DataRow In _table.Rows
                If CStr(dr(col.GroupNumber)) = groupNumber Then
                    Return dr(col.GroupName).ToString.Trim
                End If
            Next

            Return My.Resources.WarnNoGroup

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetSubgroupName(ByVal subgroupNumber As String) As String

        Try
            If _table Is Nothing Then LoadView()
            For Each dr As DataRow In _table.Rows
                If CStr(dr(col.SubgroupNumber)) = subgroupNumber Then
                    Return dr(col.SubgroupName).ToString.Trim
                End If
            Next

            Return My.Resources.WarnNoSubgroup

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetStyleName(ByVal styleNumber As String) As String

        Try
            If _table Is Nothing Then LoadView()
            For Each dr As DataRow In _table.Rows
                If CStr(dr(col.StyleNumber)) = styleNumber Then
                    Return dr(col.StyleName).ToString.Trim
                End If
            Next

            Return My.Resources.WarnNoStyle

        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class
