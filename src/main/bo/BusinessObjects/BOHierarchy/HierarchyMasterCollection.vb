﻿Imports System.ComponentModel

Public Class HierarchyMasterCollection
    Inherits BindingList(Of cHierarchyMaster)
    Private _Oasys3db As Oasys3.DB.clsOasys3DB

    Public Sub New(ByRef oasys3db As Oasys3.DB.clsOasys3DB)
        MyBase.New()
        _Oasys3db = oasys3db
    End Sub

    Public Sub LoadAll()

        Me.Items.Clear()
        Using hiemas As New cHierarchyMaster(_Oasys3db)
            For Each hie As cHierarchyMaster In hiemas.LoadMatches
                Me.Items.Add(hie)
            Next
        End Using

    End Sub

    Public Function GetCategoryName(ByVal number As String) As String

        For Each hie As cHierarchyMaster In Me.Items
            If CDbl(hie.Level.Value) = 5 Then
                If hie.MemberNumber.Value = number Then Return hie.Description.Value.Trim
            End If
        Next

        Return My.Resources.WarnNoCategory

    End Function

    Public Function GetGroupName(ByVal number As String) As String

        For Each hie As cHierarchyMaster In Me.Items
            If CDbl(hie.Level.Value) = 4 Then
                If hie.MemberNumber.Value = number Then Return hie.Description.Value.Trim
            End If
        Next

        Return My.Resources.WarnNoGroup

    End Function

    Public Function GetSubgroupName(ByVal number As String) As String

        For Each hie As cHierarchyMaster In Me.Items
            If CDbl(hie.Level.Value) = 3 Then
                If hie.MemberNumber.Value = number Then Return hie.Description.Value.Trim
            End If
        Next

        Return My.Resources.WarnNoSubgroup

    End Function

    Public Function GetStyleName(ByVal number As String) As String

        For Each hie As cHierarchyMaster In Me.Items
            If CDbl(hie.Level.Value) = 2 Then
                If hie.MemberNumber.Value = number Then Return hie.Description.Value.Trim
            End If
        Next

        Return My.Resources.WarnNoStyle

    End Function

End Class
