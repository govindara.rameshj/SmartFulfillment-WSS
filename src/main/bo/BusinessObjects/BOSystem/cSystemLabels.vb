<Serializable()> Public Class cSystemLabels
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemLabels"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOFields.Add(_Type)
        BOFields.Add(_Size)
        BOFields.Add(_Filename)
        BOFields.Add(_LabelsPerPage)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemLabels)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemLabels)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemLabels))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemLabels(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _Type As New ColField(Of Integer)("Type", 0, "Type", False, False)
    Private _Size As New ColField(Of Integer)("Size", 0, "Size", False, False)
    Private _Filename As New ColField(Of String)("Filename", "", "Filename", False, False)
    Private _LabelsPerPage As New ColField(Of Integer)("LabelsPerPage", 0, "Labels Per Page", False, False)

#End Region

#Region "Public Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Type() As ColField(Of Integer)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Type = value
        End Set
    End Property
    Public Property Size() As ColField(Of Integer)
        Get
            Size = _Size
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Size = value
        End Set
    End Property
    Public Property Filename() As ColField(Of String)
        Get
            Return _Filename
        End Get
        Set(ByVal value As ColField(Of String))
            _Filename = value
        End Set
    End Property
    Public Property LabelsPerPage() As ColField(Of Integer)
        Get
            Return _LabelsPerPage
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LabelsPerPage = value
        End Set
    End Property

#End Region

#Region "Enumerations"
    Public Enum Types
        None = 0
        Label
        Barcode
        Markdown
    End Enum
    Public Enum Sizes
        None = 0
        Small
        Medium
        Large
    End Enum

    Public Property TypeEnum() As Types
        Get
            Select Case _Type.Value
                Case 1 : Return Types.Label
                Case 2 : Return Types.Barcode
                Case 3 : Return Types.Markdown
                Case Else : Return Types.None
            End Select
        End Get
        Set(ByVal value As Types)
            Select Case value
                Case Types.Label : _Type.Value = 1
                Case Types.Barcode : _Type.Value = 2
                Case Types.Markdown : _Type.Value = 3
                Case Else : _Type.Value = 0
            End Select
        End Set
    End Property
    Public Property SizeEnum() As Sizes
        Get
            Select Case _Size.Value
                Case 1 : Return Sizes.Small
                Case 2 : Return Sizes.Medium
                Case 3 : Return Sizes.Large
                Case Else : Return Sizes.None
            End Select
        End Get
        Set(ByVal value As Sizes)
            Select Case value
                Case Sizes.Small : _Size.Value = 1
                Case Sizes.Medium : _Size.Value = 2
                Case Sizes.Large : _Size.Value = 3
                Case Else : _Size.Value = 0
            End Select
        End Set
    End Property


#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads markdown label into current instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadMarkdownLabel()

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Type, Types.Markdown)
            LoadMatches()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

End Class

