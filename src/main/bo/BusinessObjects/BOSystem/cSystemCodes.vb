﻿<Serializable()> Public Class cSystemCodes
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SYSCOD"
        BOFields.Add(_Type)
        BOFields.Add(_Number)
        BOFields.Add(_Description)
        BOFields.Add(_Attribute)
        BOFields.Add(_PrintReturnLabel)
        BOFields.Add(_RequireSuperAuth)
        BOFields.Add(_OverrideRefundPrice)
        BOFields.Add(_RequireManagerAuth)
        BOFields.Add(_Active)
        BOFields.Add(_IsPricePromise)
        BOFields.Add(_PricePromiseMulti)
        BOFields.Add(_PricePromiseMax)
        BOFields.Add(_FlexAllowed)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemCodes)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemCodes)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemCodes))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemCodes(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Type As New ColField(Of String)("TYPE", "00", "Type", True, False)
    Private _Number As New ColField(Of String)("CODE", "00", "Code", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _Attribute As New ColField(Of String)("ATTR", "", "Attribute", False, False)
    Private _PrintReturnLabel As New ColField(Of Boolean)("LABL", False, "Print Return Label", False, False)
    Private _OverrideRefundPrice As New ColField(Of Boolean)("IRPO", False, "Overide Return Price", False, False)
    Private _RequireSuperAuth As New ColField(Of Boolean)("SUPV", False, "Require Super Auth", False, False)
    Private _RequireManagerAuth As New ColField(Of Boolean)("MANA", False, "Require Manager Auth", False, False)
    Private _Active As New ColField(Of String)("ACTIVE", "", "Active", False, False)
    Private _IsPricePromise As New ColField(Of Boolean)("PMATCH", False, "Is Price Promise", False, False)
    Private _PricePromiseMulti As New ColField(Of Decimal)("VALUE1", 0, "Price Promise Multi", False, False)
    Private _PricePromiseMax As New ColField(Of Decimal)("VALUE2", 0, "Price Promise Max", False, False)
    Private _FlexAllowed As New ColField(Of Boolean)("MFLEX", False, "Flex Allowed", False, False)

#End Region

#Region "Field Properties"

    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Attribute() As ColField(Of String)
        Get
            Return _Attribute
        End Get
        Set(ByVal value As ColField(Of String))
            _Attribute = value
        End Set
    End Property
    Public Property PrintReturnLabel() As ColField(Of Boolean)
        Get
            Return _PrintReturnLabel
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintReturnLabel = value
        End Set
    End Property
    Public Property OverrideRefundPrice() As ColField(Of Boolean)
        Get
            Return _OverrideRefundPrice
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OverrideRefundPrice = value
        End Set
    End Property
    Public Property RequireSuperAuth() As ColField(Of Boolean)
        Get
            RequireSuperAuth = _RequireSuperAuth
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RequireSuperAuth = value
        End Set
    End Property
    Public Property RequireManagerAuth() As ColField(Of Boolean)
        Get
            RequireManagerAuth = _RequireManagerAuth
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RequireManagerAuth = value
        End Set
    End Property
    Public Property Active() As ColField(Of String)
        Get
            Active = _Active
        End Get
        Set(ByVal value As ColField(Of String))
            _Active = value
        End Set
    End Property
    Public Property IsPricePromise() As ColField(Of Boolean)
        Get
            Return _IsPricePromise
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsPricePromise = value
        End Set
    End Property
    Public Property PricePromiseMulti() As ColField(Of Decimal)
        Get
            Return _PricePromiseMulti
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PricePromiseMulti = value
        End Set
    End Property
    Public Property PricePromiseMax() As ColField(Of Decimal)
        Get
            Return _PricePromiseMax
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PricePromiseMax = value
        End Set
    End Property
    Public Property FlexAllowed() As ColField(Of Boolean)
        Get
            Return _FlexAllowed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _FlexAllowed = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Codes As List(Of cSystemCodes) = Nothing

    Public Property Code(ByVal Type As String, ByVal CodeNo As String) As cSystemCodes
        Get
            If CodeNo.Trim.Length < 2 Then CodeNo = CodeNo.Trim.PadLeft(2, "0"c)

            If _Codes Is Nothing Then
                _Codes = New List(Of cSystemCodes)
            End If

            For Each c As cSystemCodes In _Codes
                If c.Type.Value = Type And c.Number.Value = CodeNo Then Return c
            Next
            Return Nothing
        End Get
        Set(ByVal value As cSystemCodes)
            For Each c As cSystemCodes In _Codes
                If c.Type.Value = Type And c.Number.Value = CodeNo Then
                    c = value
                End If
            Next
        End Set
    End Property
    Public Property Codes() As List(Of cSystemCodes)
        Get
            Return _Codes
        End Get
        Set(ByVal value As List(Of cSystemCodes))
            _Codes = value
        End Set
    End Property


#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns datatable of codes for given type including 'Display' column.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="codeType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCodes(ByVal codeType As String) As DataTable

        Try
            Dim dt As DataTable = Oasys3DB.ExecuteSql(My.Resources.Procedures.SystemCodesSelectType & " '" & codeType & "'").Tables(0)
            Return dt

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodesLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns datatable of return codes including 'Display' column.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetReturnCodes() As DataTable

        Try
            Dim dt As DataTable = Oasys3DB.ExecuteSql(My.Resources.Procedures.SystemCodesSelectReturnCodes).Tables(0)
            Return dt

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodesLoad, ex)
        End Try

    End Function


    ''' <summary>
    ''' Loads all codes in to codes collection of this instance
    ''' </summary>
    ''' <remarks>Throws Oasys exception on error</remarks>
    Public Sub LoadCodes()

        Try
            Dim c As New cSystemCodes(Oasys3DB)
            _Codes = c.LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodesLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all miscellaneous codes into codes collection of this instance
    ''' </summary>
    ''' <remarks>Throws Oasys exception on error</remarks>
    Public Sub LoadMiscCodes()

        Try
            Dim c As New cSystemCodes(Oasys3DB)
            c.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pIn, c.Type, New String() {"M+", "M-", "C+", "C-"})
            _Codes = c.LoadMatches

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodesLoadMisc, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads supplier return code into codes collection of this instance
    ''' </summary>
    ''' <remarks>Throws Oasys exception on error</remarks>
    Public Sub LoadSupplierReturnCodes()

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, "SR")
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            _Codes = New List(Of cSystemCodes)
            For Each dr As DataRow In dt.Rows
                Dim c As New cSystemCodes(Oasys3DB)
                c.LoadFromRow(dr)
                _Codes.Add(c)
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodesLoadSupplierReturns, ex)
        End Try

    End Sub

#End Region

End Class


