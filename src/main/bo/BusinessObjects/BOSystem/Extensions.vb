﻿Public Module Extensions

    ''' <summary>
    ''' Returns a datatable of 3 columns ('Number', 'Name', 'Display')
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Function NumberNameArray(ByRef codes As List(Of cSystemCodes)) As String()

        Try
            'get system codes strings for combo box
            If codes.Count = 0 Then Return New String() {""}

            Dim al As New List(Of String)
            For Each code As BOSystem.cSystemCodes In codes
                al.Add(code.Number.Value & Space(1) & code.Description.Value.Trim)
            Next

            Return al.ToArray()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysCodeGetArray, ex)
        End Try

    End Function

End Module
