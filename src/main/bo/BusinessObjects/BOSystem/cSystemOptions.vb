﻿<Serializable()> Public Class cSystemOptions
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SYSOPT"
        BOFields.Add(_ID)
        BOFields.Add(_StoreNumber)
        BOFields.Add(_StoreName)
        BOFields.Add(_CompanyName)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_AddressLine4)
        BOFields.Add(_AddressLine5)
        BOFields.Add(_PostCode)
        BOFields.Add(_PhoneNo)
        BOFields.Add(mFaxNo)
        BOFields.Add(mCompanyVatNo)
        BOFields.Add(mMasterOutletNo)
        BOFields.Add(mHighValidInStrPFno)
        BOFields.Add(mMultipleSignon)
        BOFields.Add(mNoDaysPasswordValid)
        BOFields.Add(mNoDaysAuthPwdValid)
        BOFields.Add(mTrackStockOnHand)
        BOFields.Add(mAddressSwitch)
        BOFields.Add(mSecReqForCostFigs)
        BOFields.Add(mAdddressStyle)
        BOFields.Add(mDayCodeRefPicData)
        BOFields.Add(mHHTusedforICOff)
        BOFields.Add(mNoItemsCounted)
        BOFields.Add(mVerificationCount)
        BOFields.Add(mAutoApplyPriceChange)
        BOFields.Add(mPriceChangeDaysAfter)
        BOFields.Add(mPriceChgPrintSELlbl)
        BOFields.Add(mGSBulkUneeded)
        BOFields.Add(mLocalPricingAllowed)
        BOFields.Add(mCardtype)
        BOFields.Add(mCompanyGroup)
        BOFields.Add(mLocalDriveLetter)
        BOFields.Add(mRemoteDriveLetter)
        BOFields.Add(mPath)
        BOFields.Add(mNoOfCopies)
        BOFields.Add(mMinShelfallowInStore)
        BOFields.Add(mMaxShelfallowInStore)
        BOFields.Add(mMaxShelfCapMult)
        BOFields.Add(mAllowReceiptMaint)
        BOFields.Add(mAllowRecMaintDays)
        BOFields.Add(mRecreatePHL)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemOptions)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemOptions)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemOptions))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemOptions(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("FKEY") : _ID.Value = CStr(drRow(dcColumn))
                    Case ("STOR") : _StoreNumber.Value = CStr(drRow(dcColumn))
                    Case ("SNAM") : _StoreName.Value = drRow(dcColumn).ToString.Trim
                    Case ("HNAM") : _CompanyName.Value = drRow(dcColumn).ToString.Trim
                    Case ("HAD1") : _AddressLine1.Value = drRow(dcColumn).ToString.Trim
                    Case ("HAD2") : _AddressLine2.Value = drRow(dcColumn).ToString.Trim
                    Case ("HAD3") : _AddressLine3.Value = drRow(dcColumn).ToString.Trim
                    Case ("HAD4") : _AddressLine4.Value = drRow(dcColumn).ToString.Trim
                    Case ("HAD5") : _AddressLine5.Value = drRow(dcColumn).ToString.Trim
                    Case ("HPST") : _PostCode.Value = drRow(dcColumn).ToString.Trim
                    Case ("HPHN") : _PhoneNo.Value = drRow(dcColumn).ToString.Trim
                    Case ("HFAX") : mFaxNo.Value = drRow(dcColumn).ToString.Trim
                    Case ("CVAT") : mCompanyVatNo.Value = CStr(drRow(dcColumn))
                    Case ("MAST") : mMasterOutletNo.Value = CStr(drRow(dcColumn))
                    Case ("HVIS") : mHighValidInStrPFno.Value = CStr(drRow(dcColumn))
                    Case ("HSON") : mMultipleSignon.Value = CBool(drRow(dcColumn))
                    Case ("PDAY") : mNoDaysPasswordValid.Value = CDec(drRow(dcColumn))
                    Case ("ADAY") : mNoDaysAuthPwdValid.Value = CDec(drRow(dcColumn))
                    Case ("USTK") : mTrackStockOnHand.Value = CBool(drRow(dcColumn))
                    Case ("IHOS") : mAddressSwitch.Value = CBool(drRow(dcColumn))
                    Case ("COSL") : mSecReqForCostFigs.Value = CStr(drRow(dcColumn))
                    Case ("ADDS") : mAdddressStyle.Value = CStr(drRow(dcColumn))
                    Case ("DCLR") : mDayCodeRefPicData.Value = CStr(drRow(dcColumn))
                    Case ("IHHT") : mHHTusedforICOff.Value = CBool(drRow(dcColumn))
                    Case ("NCOU") : mNoItemsCounted.Value = CDec(drRow(dcColumn))
                    Case ("SELC") : mVerificationCount.Value = CDec(drRow(dcColumn))
                    Case ("PCAA") : mAutoApplyPriceChange.Value = CBool(drRow(dcColumn))
                    Case ("PCDA") : mPriceChangeDaysAfter.Value = CDec(drRow(dcColumn))
                    Case ("PSEL") : mPriceChgPrintSELlbl.Value = CBool(drRow(dcColumn))
                    Case ("BLKU") : mGSBulkUneeded.Value = CBool(drRow(dcColumn))
                    Case ("LPFL") : mLocalPricingAllowed.Value = CStr(drRow(dcColumn))
                    Case ("CTYP") : mCardtype.Value = CStr(drRow(dcColumn))
                    Case ("CGRP") : mCompanyGroup.Value = CStr(drRow(dcColumn))
                    Case ("LOCD") : mLocalDriveLetter.Value = CStr(drRow(dcColumn))
                    Case ("REMD") : mRemoteDriveLetter.Value = CStr(drRow(dcColumn))
                    Case ("PATH") : mPath.Value = CStr(drRow(dcColumn))
                    Case ("PCOP") : mNoOfCopies.Value = CDec(drRow(dcColumn))
                    Case ("MICI") : mMinShelfallowInStore.Value = CBool(drRow(dcColumn))
                    Case ("MACI") : mMaxShelfallowInStore.Value = CBool(drRow(dcColumn))
                    Case ("MACM") : mMaxShelfCapMult.Value = CDec(drRow(dcColumn))
                    Case ("MREC") : mAllowReceiptMaint.Value = CBool(drRow(dcColumn))
                    Case ("RDAY") : mAllowRecMaintDays.Value = CDec(drRow(dcColumn))
                    Case ("PHIE") : mRecreatePHL.Value = CBool(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("FKEY", "", False, False, 0, 0, True, True, 1, 0, 0, 1, "", 0, "System Options ID")
    Private _StoreNumber As New ColField(Of String)("STOR", "", False, False, 0, 0, True, True, 2, 0, 0, 1, "", 0, "Store No")
    Private _StoreName As New ColField(Of String)("SNAM", "", False, False, 0, 0, True, True, 3, 0, 0, 1, "", 0, "Store Name")
    Private _CompanyName As New ColField(Of String)("HNAM", "", False, False, 0, 0, True, True, 4, 0, 0, 1, "", 0, "Company Name")
    Private _AddressLine1 As New ColField(Of String)("HAD1", "", False, False, 0, 0, True, True, 5, 0, 0, 1, "", 0, "Address Line 1")
    Private _AddressLine2 As New ColField(Of String)("HAD2", "", False, False, 0, 0, True, True, 6, 0, 0, 1, "", 0, "Address Line 2")
    Private _AddressLine3 As New ColField(Of String)("HAD3", "", False, False, 0, 0, True, True, 7, 0, 0, 1, "", 0, "Address Line 3")
    Private _AddressLine4 As New ColField(Of String)("HAD4", "", False, False, 0, 0, True, True, 8, 0, 0, 1, "", 0, "Address Line 4")
    Private _AddressLine5 As New ColField(Of String)("HAD5", "", False, False, 0, 0, True, True, 9, 0, 0, 1, "", 0, "Address Line 5")
    Private _PostCode As New ColField(Of String)("HPST", "", False, False, 0, 0, True, True, 10, 0, 0, 1, "", 0, "Post Code")
    Private _PhoneNo As New ColField(Of String)("HPHN", "", False, False, 0, 0, True, True, 11, 0, 0, 1, "", 0, "Post No")
    Private mFaxNo As New ColField(Of String)("HFAX", "", False, False, 0, 0, True, True, 12, 0, 0, 1, "", 0, "Fax No")
    Private mCompanyVatNo As New ColField(Of String)("CVAT", "", False, False, 0, 0, True, True, 13, 0, 0, 1, "", 0, "Company Vat No")
    Private mMasterOutletNo As New ColField(Of String)("MAST", "", False, False, 0, 0, True, True, 14, 0, 0, 1, "", 0, "Master Outlet No")
    Private mHighValidInStrPFno As New ColField(Of String)("HVIS", "", False, False, 0, 0, True, True, 15, 0, 0, 1, "", 0, "High Valid In Str PF no")
    Private mMultipleSignon As New ColField(Of Boolean)("MSON", False, False, False, 0, 0, True, True, 16, 0, 0, 1, "", 0, "Multiple Signon")
    Private mNoDaysPasswordValid As New ColField(Of Decimal)("PDAY", 0, False, False, 0, 0, True, True, 17, 0, 0, 1, "", 0, "No Days Password Valid")
    Private mNoDaysAuthPwdValid As New ColField(Of Decimal)("ADAY", 0, False, False, 0, 0, True, True, 18, 0, 0, 1, "", 0, "No Days Auth Pwd Valid")
    Private mTrackStockOnHand As New ColField(Of Boolean)("USTK", False, False, False, 0, 0, True, True, 19, 0, 0, 1, "", 0, "Track Stock On Hand")
    Private mAddressSwitch As New ColField(Of Boolean)("IHOS", False, False, False, 0, 0, True, True, 20, 0, 0, 1, "", 0, "Address Switch")
    Private mSecReqForCostFigs As New ColField(Of String)("COSL", "", False, False, 0, 0, True, True, 21, 0, 0, 1, "", 0, "Sec Req For Cost Figs")
    Private mAdddressStyle As New ColField(Of String)("ADDS", "", False, False, 0, 0, True, True, 22, 0, 0, 1, "", 0, "Address Style")
    Private mDayCodeRefPicData As New ColField(Of String)("DCLR", "", False, False, 0, 0, True, True, 23, 0, 0, 1, "", 0, "Day Code Ref Pic Data")
    Private mHHTusedforICOff As New ColField(Of Boolean)("IHHT", False, False, False, 0, 0, True, True, 24, 0, 0, 1, "", 0, "HHT used for ICOff")
    Private mNoItemsCounted As New ColField(Of Decimal)("NCOU", 0, False, False, 0, 0, True, True, 25, 0, 0, 1, "", 0, "No Items Counted")
    Private mVerificationCount As New ColField(Of Decimal)("SELC", 0, False, False, 0, 0, True, True, 26, 0, 0, 1, "", 0, "Verification Count")
    Private mAutoApplyPriceChange As New ColField(Of Boolean)("PCAA", False, False, False, 0, 0, True, True, 27, 0, 0, 1, "", 0, "Auto Apply Price Change")
    Private mPriceChangeDaysAfter As New ColField(Of Decimal)("PCDA", 0, False, False, 0, 0, True, True, 28, 0, 0, 1, "", 0, "Price Change Days After")
    Private mPriceChgPrintSELlbl As New ColField(Of Boolean)("PSEL", False, False, False, 0, 0, True, True, 29, 0, 0, 1, "", 0, "Price Chg Print SEL lbl")
    Private mGSBulkUneeded As New ColField(Of Boolean)("BLKU", False, False, False, 0, 0, True, True, 30, 0, 0, 1, "", 0, "GS Bulk U needed")
    Private mLocalPricingAllowed As New ColField(Of String)("LPFL", "", False, False, 0, 0, True, True, 31, 0, 0, 1, "", 0, "Local Pricing Allowed")
    Private mCardtype As New ColField(Of String)("CTYP", "", False, False, 0, 0, True, True, 32, 0, 0, 1, "", 0, "Card type")
    Private mCompanyGroup As New ColField(Of String)("CGRP", "", False, False, 0, 0, True, True, 33, 0, 0, 1, "", 0, "Company Group")
    Private mLocalDriveLetter As New ColField(Of String)("LOCD", "", False, False, 0, 0, True, True, 34, 0, 0, 1, "", 0, "Local Drive Letter")
    Private mRemoteDriveLetter As New ColField(Of String)("REMD", "", False, False, 0, 0, True, True, 35, 0, 0, 1, "", 0, "Remote Drive Letter")
    Private mPath As New ColField(Of String)("PATH", "", False, False, 0, 0, True, True, 36, 0, 0, 1, "", 0, "Path")
    Private mNoOfCopies As New ColField(Of Decimal)("PCOP", 0, False, False, 0, 0, True, True, 37, 0, 0, 1, "", 0, "No Of Copies")
    Private mMinShelfallowInStore As New ColField(Of Boolean)("MICI", False, False, False, 0, 0, True, True, 38, 0, 0, 1, "", 0, "Min Shelf allow In Store")
    Private mMaxShelfallowInStore As New ColField(Of Boolean)("MACI", False, False, False, 0, 0, True, True, 39, 0, 0, 1, "", 0, "Max Shelf allow In Store")
    Private mMaxShelfCapMult As New ColField(Of Decimal)("MACM", 0, False, False, 0, 0, True, True, 40, 0, 0, 1, "", 0, "Max Shelf Cap Mult")
    Private mAllowReceiptMaint As New ColField(Of Boolean)("MREC", False, False, False, 0, 0, True, True, 41, 0, 0, 1, "", 0, "Allow Receipt Maint")
    Private mAllowRecMaintDays As New ColField(Of Decimal)("RDAY", 0, False, False, 0, 0, True, True, 42, 0, 0, 1, "", 0, "Allow Rec Maint Days")
    Private mRecreatePHL As New ColField(Of Boolean)("PHIE", False, False, False, 0, 0, True, True, 43, 0, 0, 1, "", 0, "Recreate PHL")


#End Region

#Region "Field Properties"

    Public Property SystemOptionsID() As ColField(Of String)
        Get
            SystemOptionsID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property StoreNo() As ColField(Of String)
        Get
            StoreNo = _StoreNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreNumber = value
        End Set
    End Property
    Public Property StoreName() As ColField(Of String)
        Get
            StoreName = _StoreName
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreName = value
        End Set
    End Property
    Public Property CompanyName() As ColField(Of String)
        Get
            CompanyName = _CompanyName
        End Get
        Set(ByVal value As ColField(Of String))
            _CompanyName = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property AddressLine4() As ColField(Of String)
        Get
            AddressLine4 = _AddressLine4
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine4 = value
        End Set
    End Property
    Public Property AddressLine5() As ColField(Of String)
        Get
            AddressLine5 = _AddressLine5
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine5 = value
        End Set
    End Property
    Public Property PostCode() As ColField(Of String)
        Get
            PostCode = _PostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _PostCode = value
        End Set
    End Property
    Public Property PhoneNo() As ColField(Of String)
        Get
            PhoneNo = _PhoneNo
        End Get
        Set(ByVal value As ColField(Of String))
            _PhoneNo = value
        End Set
    End Property
    Public Property FaxNo() As ColField(Of String)
        Get
            FaxNo = mFaxNo
        End Get
        Set(ByVal value As ColField(Of String))
            mFaxNo = value
        End Set
    End Property
    Public Property CompanyVatNo() As ColField(Of String)
        Get
            CompanyVatNo = mCompanyVatNo
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyVatNo = value
        End Set
    End Property
    Public Property MasterOutletNo() As ColField(Of String)
        Get
            MasterOutletNo = mMasterOutletNo
        End Get
        Set(ByVal value As ColField(Of String))
            mMasterOutletNo = value
        End Set
    End Property
    Public Property HighValidInStrPFno() As ColField(Of String)
        Get
            HighValidInStrPFno = mHighValidInStrPFno
        End Get
        Set(ByVal value As ColField(Of String))
            mHighValidInStrPFno = value
        End Set
    End Property
    Public Property MultipleSignon() As ColField(Of Boolean)
        Get
            MultipleSignon = mMultipleSignon
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mMultipleSignon = value
        End Set
    End Property
    Public Property NoDaysPasswordValid() As ColField(Of Decimal)
        Get
            NoDaysPasswordValid = mNoDaysPasswordValid
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoDaysPasswordValid = value
        End Set
    End Property
    Public Property NoDaysAuthPwdValid() As ColField(Of Decimal)
        Get
            NoDaysAuthPwdValid = mNoDaysAuthPwdValid
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoDaysAuthPwdValid = value
        End Set
    End Property
    Public Property TrackStockOnHand() As ColField(Of Boolean)
        Get
            TrackStockOnHand = mTrackStockOnHand
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mTrackStockOnHand = value
        End Set
    End Property
    Public Property AddressSwitch() As ColField(Of Boolean)
        Get
            AddressSwitch = mAddressSwitch
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mAddressSwitch = value
        End Set
    End Property
    Public Property SecReqForCostFigs() As ColField(Of String)
        Get
            SecReqForCostFigs = mSecReqForCostFigs
        End Get
        Set(ByVal value As ColField(Of String))
            mSecReqForCostFigs = value
        End Set
    End Property
    Public Property AdddressStyle() As ColField(Of String)
        Get
            AdddressStyle = mAdddressStyle
        End Get
        Set(ByVal value As ColField(Of String))
            mAdddressStyle = value
        End Set
    End Property
    Public Property DayCodeRefPicData() As ColField(Of String)
        Get
            DayCodeRefPicData = mDayCodeRefPicData
        End Get
        Set(ByVal value As ColField(Of String))
            mDayCodeRefPicData = value
        End Set
    End Property
    Public Property HHTusedforICOff() As ColField(Of Boolean)
        Get
            HHTusedforICOff = mHHTusedforICOff
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mHHTusedforICOff = value
        End Set
    End Property
    Public Property NoItemsCounted() As ColField(Of Decimal)
        Get
            NoItemsCounted = mNoItemsCounted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoItemsCounted = value
        End Set
    End Property
    Public Property VerificationCount() As ColField(Of Decimal)
        Get
            VerificationCount = mVerificationCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVerificationCount = value
        End Set
    End Property
    Public Property AutoApplyPriceChange() As ColField(Of Boolean)
        Get
            AutoApplyPriceChange = mAutoApplyPriceChange
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mAutoApplyPriceChange = value
        End Set
    End Property
    Public Property PriceChangeDaysAfter() As ColField(Of Decimal)
        Get
            PriceChangeDaysAfter = mPriceChangeDaysAfter
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPriceChangeDaysAfter = value
        End Set
    End Property
    Public Property PriceChgPrintSELlbl() As ColField(Of Boolean)
        Get
            PriceChgPrintSELlbl = mPriceChgPrintSELlbl
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mPriceChgPrintSELlbl = value
        End Set
    End Property
    Public Property GSBulkUneeded() As ColField(Of Boolean)
        Get
            GSBulkUneeded = mGSBulkUneeded
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mGSBulkUneeded = value
        End Set
    End Property
    Public Property LocalPricingAllowed() As ColField(Of String)
        Get
            LocalPricingAllowed = mLocalPricingAllowed
        End Get
        Set(ByVal value As ColField(Of String))
            mLocalPricingAllowed = value
        End Set
    End Property
    Public Property Cardtype() As ColField(Of String)
        Get
            Cardtype = mCardtype
        End Get
        Set(ByVal value As ColField(Of String))
            mCardtype = value
        End Set
    End Property
    Public Property CompanyGroup() As ColField(Of String)
        Get
            CompanyGroup = mCompanyGroup
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyGroup = value
        End Set
    End Property
    Public Property LocalDriveLetter() As ColField(Of String)
        Get
            LocalDriveLetter = mLocalDriveLetter
        End Get
        Set(ByVal value As ColField(Of String))
            mLocalDriveLetter = value
        End Set
    End Property
    Public Property RemoteDriveLetter() As ColField(Of String)
        Get
            RemoteDriveLetter = mRemoteDriveLetter
        End Get
        Set(ByVal value As ColField(Of String))
            mRemoteDriveLetter = value
        End Set
    End Property
    Public Property Path() As ColField(Of String)
        Get
            Path = mPath
        End Get
        Set(ByVal value As ColField(Of String))
            mPath = value
        End Set
    End Property
    Public Property NoOfCopies() As ColField(Of Decimal)
        Get
            NoOfCopies = mNoOfCopies
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoOfCopies = value
        End Set
    End Property
    Public Property MinShelfallowInStore() As ColField(Of Boolean)
        Get
            MinShelfallowInStore = mMinShelfallowInStore
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mMinShelfallowInStore = value
        End Set
    End Property
    Public Property MaxShelfallowInStore() As ColField(Of Boolean)
        Get
            MaxShelfallowInStore = mMaxShelfallowInStore
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mMaxShelfallowInStore = value
        End Set
    End Property
    Public Property MaxShelfCapMult() As ColField(Of Decimal)
        Get
            MaxShelfCapMult = mMaxShelfCapMult
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMaxShelfCapMult = value
        End Set
    End Property
    Public Property AllowReceiptMaint() As ColField(Of Boolean)
        Get
            AllowReceiptMaint = mAllowReceiptMaint
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mAllowReceiptMaint = value
        End Set
    End Property
    Public Property AllowRecMaintDays() As ColField(Of Decimal)
        Get
            AllowRecMaintDays = mAllowRecMaintDays
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAllowRecMaintDays = value
        End Set
    End Property
    Public Property RecreatePHL() As ColField(Of Boolean)
        Get
            RecreatePHL = mRecreatePHL
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mRecreatePHL = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function SystemOptions() As DataRow
        Dim ds As DataSet

        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.SystemOptionsID, "01")
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            Else
                Return Nothing
            End If
        End If
        Return Nothing

    End Function

    ''' <summary>
    ''' Loads system options into this instance. Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load()

        Try
            ClearLists()
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _ID, "01")
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysOptionsLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns company purchase ledger department. 
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PurchaseLedgerDepartment() As String

        Try
            Dim sb As New StringBuilder()
            sb.Append("Purchase Ledger Department" & Environment.NewLine)
            sb.Append(_CompanyName.Value & Environment.NewLine)
            If _AddressLine1.Value <> String.Empty Then sb.Append(_AddressLine1.Value & Environment.NewLine)
            If _AddressLine2.Value <> String.Empty Then sb.Append(_AddressLine2.Value & Environment.NewLine)
            If _AddressLine3.Value <> String.Empty Then sb.Append(_AddressLine3.Value & Environment.NewLine)
            If _AddressLine4.Value <> String.Empty Then sb.Append(_AddressLine4.Value & Environment.NewLine)
            If _AddressLine5.Value <> String.Empty Then sb.Append(_AddressLine5.Value & Environment.NewLine)
            If _PostCode.Value <> String.Empty Then sb.Append(_PostCode.Value & Environment.NewLine)

            'remove last new line entered
            sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length)

            Return sb.ToString

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysOptionsGetNameAddress, ex)
        End Try

    End Function

#End Region

End Class
