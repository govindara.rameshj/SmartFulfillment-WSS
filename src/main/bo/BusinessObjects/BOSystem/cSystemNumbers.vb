<Serializable()> Public Class cSystemNumbers
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemNumbers"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOFields.Add(_NextNumber)
        BOFields.Add(_Size)
        BOFields.Add(_Prefix)
        BOFields.Add(_Suffix)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemNumbers)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemNumbers)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemNumbers))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemNumbers(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, True, False, 0, 0, True, True, 1, 0, 0, 1, "", 0, "System Numbers ID")
    Private _Description As New ColField(Of String)("Description", "", False, False, 0, 0, True, True, 2, 0, 0, 1, "", 0, "Description")
    Private _NextNumber As New ColField(Of Integer)("NextNumber", 0, False, False, 0, 0, True, True, 3, 0, 0, 1, "", 0, "Next Number")
    Private _Size As New ColField(Of Integer)("Size", 0, False, False, 0, 0, True, True, 4, 0, 0, 1, "", 0, "Size")
    Private _Prefix As New ColField(Of String)("Prefix", "", False, False, 0, 0, True, True, 5, 0, 0, 1, "", 0, "Prefix")
    Private _Suffix As New ColField(Of String)("Suffix", "", False, False, 0, 0, True, True, 6, 0, 0, 1, "", 0, "Suffix")

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property NextNumber() As ColField(Of Integer)
        Get
            NextNumber = _NextNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NextNumber = value
        End Set
    End Property
    Public Property Size() As ColField(Of Integer)
        Get
            Size = _Size
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Size = value
        End Set
    End Property
    Public Property Prefix() As ColField(Of String)
        Get
            Prefix = _Prefix
        End Get
        Set(ByVal value As ColField(Of String))
            _Prefix = value
        End Set
    End Property
    Public Property Suffix() As ColField(Of String)
        Get
            Suffix = _Suffix
        End Get
        Set(ByVal value As ColField(Of String))
            _Suffix = value
        End Set
    End Property

#End Region

#Region "Methods"
    Public Enum Numbers
        PurchaseOrder = 1
        [Return] = 2
        InterStoreTransfer = 3
        Receipt = 4
        Consignment = 5
        Soq = 6
        ProjectSalesQuote = 7
        ProjectSalesOrder = 8
        VisionSales = 9
        CustomerOrderQuote = 10
    End Enum

    Public Function RetrieveNumber(ByVal ID As Integer) As Integer

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ID)
            LoadMatches()
            Return _NextNumber.Value

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysNumbersLoad, ex)
        End Try

    End Function

    Private Function UpdateNextNumber(ByVal ID As Integer, ByVal number As Integer) As Boolean

        Try
            'Save all properties to the database
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            If number = 999999 Then number = 0
            Oasys3DB.SetColumnAndValueParameter(_NextNumber.ColumnName, number + 1)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, ID)
            Oasys3DB.Update()
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function


    ''' <summary>
    ''' Increments number for loaded instance checking for rollover size and persists to database. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub IncrementNumber()

        Try
            'increment number whilst checking that not greater than size
            _NextNumber.Value += 1
            If _NextNumber.Value >= Math.Pow(10, _Size.Value) Then
                _NextNumber.Value = 1
            End If

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_NextNumber.ColumnName, _NextNumber.Value)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID.Value)
            Oasys3DB.Update()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysNumbersUpdate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns string representation of number padded to size with suffix and prefix attached. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNumberString() As String

        Try
            Dim sb As New StringBuilder(NextNumber.Value.ToString.PadLeft(_Size.Value, "0"c))
            sb.Insert(0, _Prefix.Value)
            sb.Append(_Suffix.Value)

            Return sb.ToString

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysNumbersGetString, ex)
        End Try

    End Function


    ''' <summary>
    ''' Loads return number row into this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadReturnNumber()

        Try
            Dim ID As Integer = Numbers.[Return]
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ID)
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysNumbersLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads return number row into this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadDrlNumber()

        Try
            Dim ID As Integer = Numbers.Receipt
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ID)
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysNumbersLoad, ex)
        End Try

    End Sub


    Public Function GetPONumber() As Integer

        Try
            Dim ID As Integer = Numbers.PurchaseOrder
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, number)
            Return number

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetSOQNumber() As Integer

        Try
            Dim ID As Integer = Numbers.Soq
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, number)
            Return number

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetConsignmentNumber() As Integer

        Try
            Dim ID As Integer = Numbers.Consignment
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, number)
            Return number

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetDRLNumber() As Integer

        Try
            Dim ID As Integer = Numbers.Receipt
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, number)
            Return number

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetReturnNumber() As Integer

        Try
            Dim ID As Integer = Numbers.Return
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, number)
            Return number

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetVisionSalesNumber(ByVal intNumber As Integer) As Integer

        Try
            Dim ID As Integer = Numbers.VisionSales
            Dim number As Integer = RetrieveNumber(ID)
            UpdateNextNumber(ID, intNumber)
            Return number

        Catch ex As Exception

        End Try

    End Function


#End Region

End Class
