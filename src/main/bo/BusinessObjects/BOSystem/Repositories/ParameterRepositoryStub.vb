﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ProcessTransmissions.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                       "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                       "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                       "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                       "54e0a4a4")> 
Namespace TpWickes

    Public Class ParameterRepositoryStub
        Implements IParameterRepository

#Region "Stub Code"

        Private _ParameterString As String

        Friend Sub ConfigureStub(ByVal ParameterString As String)

            _ParameterString = ParameterString

        End Sub

#End Region

#Region "Interface Code"

        Public Function GetParameterString(ByVal ParameterID As Integer) As String Implements IParameterRepository.GetParameterString

            Return _ParameterString

        End Function

#End Region

    End Class

End Namespace