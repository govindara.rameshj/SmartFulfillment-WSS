﻿<Serializable()> Public Class cSystemPeriods
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemPeriods"
        BOFields.Add(_ID)
        BOFields.Add(_StartDate)
        BOFields.Add(_EndDate)
        BOFields.Add(_IsClosed)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemPeriods)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemPeriods)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemPeriods))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemPeriods(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _StartDate As New ColField(Of Date)("StartDate", Nothing, False, False, 0, 0, True, True, 2, 0, 0, 1, "", 0, "Start Date")
    Private _EndDate As New ColField(Of Date)("EndDate", Nothing, False, False, 0, 0, True, True, 3, 0, 0, 1, "", 0, "End Date")
    Private _IsClosed As New ColField(Of Boolean)("IsClosed", False, False, False, 0, 0, True, True, 4, 0, 0, 1, "", 0, "Is Closed")

#End Region

#Region "Public Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            Return _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            Return _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property IsClosed() As ColField(Of Boolean)
        Get
            Return _IsClosed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsClosed = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Periods As New List(Of cSystemPeriods)

    Public Property Period(ByVal PeriodID As Integer) As cSystemPeriods
        Get
            For Each p As cSystemPeriods In _Periods
                If p.ID.Value = PeriodID Then Return p
            Next
            Return New cSystemPeriods(Oasys3DB)
        End Get
        Set(ByVal value As cSystemPeriods)
            For Each p As cSystemPeriods In _Periods
                If p.ID.Value = PeriodID Then
                    p = value
                End If
            Next
        End Set
    End Property
    Public Property Periods() As List(Of cSystemPeriods)
        Get
            Return _Periods
        End Get
        Set(ByVal value As List(Of cSystemPeriods))
            _Periods = value
        End Set
    End Property

#End Region

#Region "Enumerations"

    Public Enum ClosedStates
        All
        Open
        Closed
    End Enum

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads system period for given date into this instance.
    '''  If none found then create periods for the year.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="WithPeriodsToDate">Set to true to load previous periods into Periods collection of this instance</param>
    ''' <remarks></remarks>
    Public Sub Load(ByVal startDate As Date, Optional ByVal WithPeriodsToDate As Boolean = False)

        Try
            'get system period for current system date
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_StartDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, startDate)
            Oasys3DB.SetWhereJoinParameter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_EndDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, startDate.AddDays(1))
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'check if anything returned
            If dt.Rows.Count > 0 Then
                Me.LoadFromRow(dt.Rows(0))
            Else
                AddDatesForYear()
                Load(startDate, WithPeriodsToDate)
            End If

            'load previous periods if required
            If WithPeriodsToDate Then
                Dim sysPeriod As New BOSystem.cSystemPeriods(Oasys3DB)
                sysPeriod.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, sysPeriod.StartDate, _StartDate.Value)
                sysPeriod.SortBy(sysPeriod.StartDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                _Periods = sysPeriod.LoadMatches
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads system period for current date into this instance.
    '''  If none found then create periods for the year.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="WithPeriodsToDate">Set to true to load previous periods into Periods collection of this instance</param>
    ''' <remarks></remarks>
    Public Sub LoadCurrent(ByVal closedState As ClosedStates, Optional ByVal WithPeriodsToDate As Boolean = False)

        Try
            'get system period for current system date
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_StartDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Now.Date)
            Oasys3DB.SetWhereJoinParameter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_EndDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Now.Date)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'check if anything returned
            If dt.Rows.Count > 0 Then
                Select Case closedState
                    Case ClosedStates.Closed : If CBool(dt.Rows(0)(_IsClosed.ColumnName)) = True Then Me.LoadFromRow(dt.Rows(0))
                    Case ClosedStates.Open : If CBool(dt.Rows(0)(_IsClosed.ColumnName)) = False Then Me.LoadFromRow(dt.Rows(0))
                    Case Else : Me.LoadFromRow(dt.Rows(0))
                End Select
            Else
                AddDatesForYear()
                LoadCurrent(CType(WithPeriodsToDate, ClosedStates))
            End If

            'load previous periods if required
            If WithPeriodsToDate Then
                Dim sysPeriod As New BOSystem.cSystemPeriods(Oasys3DB)
                sysPeriod.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, sysPeriod.StartDate, Now.Date)

                Select Case closedState
                    Case ClosedStates.Closed
                        sysPeriod.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                        sysPeriod.AddLoadFilter(clsOasys3DB.eOperator.pEquals, sysPeriod.IsClosed, True)
                    Case ClosedStates.Open
                        sysPeriod.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                        sysPeriod.AddLoadFilter(clsOasys3DB.eOperator.pEquals, sysPeriod.IsClosed, False)
                End Select

                sysPeriod.SortBy(sysPeriod.StartDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                _Periods = sysPeriod.LoadMatches
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns list of system periods for given period ids
    ''' </summary>
    ''' <param name="periodIds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPeriods(ByVal periodIds As ArrayList) As List(Of cSystemPeriods)

        Try
            Dim list As New List(Of cSystemPeriods)

            If periodIds.Count > 0 Then
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
                Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pIn, periodIds)
                Oasys3DB.SetOrderByParameter(_ID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                Dim dt As DataTable = Oasys3DB.Query.Tables(0)

                For Each dr As DataRow In dt.Rows
                    Dim p As New cSystemPeriods(Oasys3DB)
                    p.LoadFromRow(dr)
                    list.Add(p)
                Next
            End If

            Return list

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsLoad, ex)
        End Try

    End Function


    ''' <summary>
    ''' Returns data table for all non-closed periods up to current date with added column "Display".
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetIDStartDateTable() As DataTable

        Try
            'get system period for current system date
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_StartDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Now.Date)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_IsClosed.ColumnName, clsOasys3DB.eOperator.pEquals, False)
            Oasys3DB.SetOrderByParameter(_ID.ColumnName, clsOasys3DB.eOrderByType.Descending)

            'create new column for returned table
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)
            dt.Columns.Add("display", GetType(String), _ID.ColumnName & " + '   ' + SUBSTRING(" & _StartDate.ColumnName & ",1,10)")

            Return dt

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsGetTable, ex)
        End Try

    End Function


    ''' <summary>
    ''' Generates new system period records for this year
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AddDatesForYear()

        Try
            'set start date as first day of the year
            Dim startDate As Date = CDate(Today.Year.ToString("0000") & "-01-01 00:00:00")

            'get max end date in table
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggMax, _EndDate.ColumnName, _EndDate.ColumnName)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'check if anything returned and set start date to this
            If Not IsDBNull(dt.Rows(0)(0)) Then
                If CDate(dt.Rows(0)(0)) > startDate Then
                    startDate = CDate(dt.Rows(0)(0))
                    startDate = CDate(startDate.ToString("yyyy-MM-dd") & " 00:00:00").AddDays(1)
                End If
            End If

            'get days to generate
            Dim daysToInsert As Integer = 365 - startDate.DayOfYear
            If Date.IsLeapYear(startDate.Year) Then daysToInsert += 1

            'start transaction
            Oasys3DB.BeginTransaction()

            Trace.WriteLine("cSystemPeriods:AddDatesForYear,No of Days=" & daysToInsert)
            'create new periods
            For dayNumber As Integer = 0 To daysToInsert
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pInsert)
                Oasys3DB.SetColumnAndValueParameter(_StartDate.ColumnName, startDate.AddDays(dayNumber))
                Oasys3DB.SetColumnAndValueParameter(_EndDate.ColumnName, startDate.AddDays(dayNumber).AddHours(23).AddMinutes(59).AddSeconds(59))
                Oasys3DB.SetColumnAndValueParameter(_IsClosed.ColumnName, (startDate.AddDays(dayNumber) < Now.Date))
                Oasys3DB.Insert()
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsGenerating, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Returns current period ID.
    '''  If none found then create periods for the year.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCurrentPeriodID() As Integer

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_ID.ColumnName)
            Oasys3DB.SetWhereParameter(_StartDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Now)
            Oasys3DB.SetWhereJoinParameter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_EndDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Now)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If IsDBNull(dt.Rows(0).Item(0)) Then
                AddDatesForYear()
                Return GetCurrentPeriodID()
            Else
                Return CInt(dt.Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns start date for given period ID
    ''' </summary>
    ''' <param name="periodId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetStartDate(ByVal periodId As Integer) As Date

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_StartDate.ColumnName)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If Not IsDBNull(dt.Rows(0)(0)) Then
                Return CType(dt.Rows(0)(0), Date)
            End If

            Return Nothing

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodGetStartDate, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns whether instance contains given date.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="testDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ContainsDate(ByVal testDate As Date) As Boolean

        Try
            If _StartDate.Value <= testDate And _EndDate.Value >= testDate Then
                Return True
            End If
            Return False

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysPeriodsTestDate, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns whether this instance is the current period
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsCurrent() As Boolean

        If (Now >= _StartDate.Value) And (Now <= _EndDate.Value) Then Return True
        Return False

    End Function


    Public Overrides Function ToString() As String
        Return _ID.Value & Space(3) & _StartDate.Value.ToShortDateString
    End Function

#End Region

End Class
