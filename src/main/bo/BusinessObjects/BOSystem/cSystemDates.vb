﻿<Serializable()> Public Class cSystemDates
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SYSDAT"
        BOFields.Add(_ID)
        BOFields.Add(_DaysOpen)
        BOFields.Add(_EndofWeekDay)
        BOFields.Add(mDaysOpenInPeriod01)
        BOFields.Add(mDaysOpenInPeriod02)
        BOFields.Add(mDaysOpenInPeriod03)
        BOFields.Add(mDaysOpenInPeriod04)
        BOFields.Add(mDaysOpenInPeriod05)
        BOFields.Add(mDaysOpenInPeriod06)
        BOFields.Add(mDaysOpenInPeriod07)
        BOFields.Add(mDaysOpenInPeriod08)
        BOFields.Add(mDaysOpenInPeriod09)
        BOFields.Add(mDaysOpenInPeriod10)
        BOFields.Add(mDaysOpenInPeriod11)
        BOFields.Add(mDaysOpenInPeriod12)
        BOFields.Add(mDaysOpenInPeriod13)
        BOFields.Add(mDaysOpenInPeriod14)
        BOFields.Add(_Today)
        BOFields.Add(_TodayDayCode)
        BOFields.Add(_Tomorrow)
        BOFields.Add(_TomorrowDayCode)
        BOFields.Add(mWeekFromToday)
        BOFields.Add(mWeekEnding01)
        BOFields.Add(mWeekEnding02)
        BOFields.Add(mWeekEnding03)
        BOFields.Add(mWeekEnding04)
        BOFields.Add(mWeekEnding05)
        BOFields.Add(mWeekEnding06)
        BOFields.Add(mWeekEnding07)
        BOFields.Add(mWeekEnding08)
        BOFields.Add(mWeekEnding09)
        BOFields.Add(mWeekEnding10)
        BOFields.Add(mWeekEnding11)
        BOFields.Add(mWeekEnding12)
        BOFields.Add(mWeekEnding13)
        BOFields.Add(_StoreLiveDate)
        BOFields.Add(_UseSet)
        BOFields.Add(mSet1PeriodEnd01)
        BOFields.Add(mSet1PeriodEnd02)
        BOFields.Add(mSet1PeriodEnd03)
        BOFields.Add(mSet1PeriodEnd04)
        BOFields.Add(mSet1PeriodEnd05)
        BOFields.Add(mSet1PeriodEnd06)
        BOFields.Add(mSet1PeriodEnd07)
        BOFields.Add(mSet1PeriodEnd08)
        BOFields.Add(mSet1PeriodEnd09)
        BOFields.Add(mSet1PeriodEnd10)
        BOFields.Add(mSet1PeriodEnd11)
        BOFields.Add(mSet1PeriodEnd12)
        BOFields.Add(mSet1PeriodEnd13)
        BOFields.Add(mSet1PeriodEnd14)
        BOFields.Add(mSet1YearEnd01)
        BOFields.Add(mSet1YearEnd02)
        BOFields.Add(mSet1YearEnd03)
        BOFields.Add(mSet1YearEnd04)
        BOFields.Add(mSet1YearEnd05)
        BOFields.Add(mSet1YearEnd06)
        BOFields.Add(mSet1YearEnd07)
        BOFields.Add(mSet1YearEnd08)
        BOFields.Add(mSet1YearEnd09)
        BOFields.Add(mSet1YearEnd10)
        BOFields.Add(mSet1YearEnd11)
        BOFields.Add(mSet1YearEnd12)
        BOFields.Add(mSet1YearEnd13)
        BOFields.Add(mSet1YearEnd14)
        BOFields.Add(mSet1Processed01)
        BOFields.Add(mSet1Processed02)
        BOFields.Add(mSet1Processed03)
        BOFields.Add(mSet1Processed04)
        BOFields.Add(mSet1Processed05)
        BOFields.Add(mSet1Processed06)
        BOFields.Add(mSet1Processed07)
        BOFields.Add(mSet1Processed08)
        BOFields.Add(mSet1Processed09)
        BOFields.Add(mSet1Processed10)
        BOFields.Add(mSet1Processed11)
        BOFields.Add(mSet1Processed12)
        BOFields.Add(mSet1Processed13)
        BOFields.Add(mSet1Processed14)
        BOFields.Add(mSet2PeriodEnd01)
        BOFields.Add(mSet2PeriodEnd02)
        BOFields.Add(mSet2PeriodEnd03)
        BOFields.Add(mSet2PeriodEnd04)
        BOFields.Add(mSet2PeriodEnd05)
        BOFields.Add(mSet2PeriodEnd06)
        BOFields.Add(mSet2PeriodEnd07)
        BOFields.Add(mSet2PeriodEnd08)
        BOFields.Add(mSet2PeriodEnd09)
        BOFields.Add(mSet2PeriodEnd10)
        BOFields.Add(mSet2PeriodEnd11)
        BOFields.Add(mSet2PeriodEnd12)
        BOFields.Add(mSet2PeriodEnd13)
        BOFields.Add(mSet2PeriodEnd14)
        BOFields.Add(mSet2YearEnd01)
        BOFields.Add(mSet2YearEnd02)
        BOFields.Add(mSet2YearEnd03)
        BOFields.Add(mSet2YearEnd04)
        BOFields.Add(mSet2YearEnd05)
        BOFields.Add(mSet2YearEnd06)
        BOFields.Add(mSet2YearEnd07)
        BOFields.Add(mSet2YearEnd08)
        BOFields.Add(mSet2YearEnd09)
        BOFields.Add(mSet2YearEnd10)
        BOFields.Add(mSet2YearEnd11)
        BOFields.Add(mSet2YearEnd12)
        BOFields.Add(mSet2YearEnd13)
        BOFields.Add(mSet2YearEnd14)
        BOFields.Add(mSet2Processed01)
        BOFields.Add(mSet2Processed02)
        BOFields.Add(mSet2Processed03)
        BOFields.Add(mSet2Processed04)
        BOFields.Add(mSet2Processed05)
        BOFields.Add(mSet2Processed06)
        BOFields.Add(mSet2Processed07)
        BOFields.Add(mSet2Processed08)
        BOFields.Add(mSet2Processed09)
        BOFields.Add(mSet2Processed10)
        BOFields.Add(mSet2Processed11)
        BOFields.Add(mSet2Processed12)
        BOFields.Add(mSet2Processed13)
        BOFields.Add(mSet2Processed14)
        BOFields.Add(_CurrentPeriodYearEndDate)
        BOFields.Add(_PriorPeriodYearEndDate)
        BOFields.Add(_CurrentEndIsYearEnd)
        BOFields.Add(_PeriodEndandNotProcessed)
        BOFields.Add(_WeekEndandNotProcessed)
        BOFields.Add(_NetSetNo)
        BOFields.Add(_NightSetNo)
        BOFields.Add(_RetryMode)
        BOFields.Add(_WeeksInCycle)
        BOFields.Add(_WeekCycleNumber)
        BOFields.Add(_AuditDate)
        BOFields.Add(_DaysPriorToPriceChange)
        BOFields.Add(_LastNightlyCloseDate)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemDates)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemDates)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemDates))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemDates(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            '            oField = oRow.Field(lFieldNo)
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("FKEY") : _ID.Value = CStr(drRow(dcColumn))
                    Case ("DAYS") : _DaysOpen.Value = CDec(drRow(dcColumn))
                    Case ("WEND") : _EndofWeekDay.Value = CDec(drRow(dcColumn))
                    Case ("OPEN1") : mDaysOpenInPeriod01.Value = CDec(drRow(dcColumn))
                    Case ("OPEN2") : mDaysOpenInPeriod02.Value = CDec(drRow(dcColumn))
                    Case ("OPEN3") : mDaysOpenInPeriod03.Value = CDec(drRow(dcColumn))
                    Case ("OPEN4") : mDaysOpenInPeriod04.Value = CDec(drRow(dcColumn))
                    Case ("OPEN5") : mDaysOpenInPeriod05.Value = CDec(drRow(dcColumn))
                    Case ("OPEN6") : mDaysOpenInPeriod06.Value = CDec(drRow(dcColumn))
                    Case ("OPEN7") : mDaysOpenInPeriod07.Value = CDec(drRow(dcColumn))
                    Case ("OPEN8") : mDaysOpenInPeriod08.Value = CDec(drRow(dcColumn))
                    Case ("OPEN9") : mDaysOpenInPeriod09.Value = CDec(drRow(dcColumn))
                    Case ("OPEN10") : mDaysOpenInPeriod10.Value = CDec(drRow(dcColumn))
                    Case ("OPEN11") : mDaysOpenInPeriod11.Value = CDec(drRow(dcColumn))
                    Case ("OPEN12") : mDaysOpenInPeriod12.Value = CDec(drRow(dcColumn))
                    Case ("OPEN13") : mDaysOpenInPeriod13.Value = CDec(drRow(dcColumn))
                    Case ("OPEN14") : mDaysOpenInPeriod14.Value = CDec(drRow(dcColumn))
                    Case ("TODT") : _Today.Value = CDate(drRow(dcColumn))
                    Case ("TODW") : _TodayDayCode.Value = CDec(drRow(dcColumn))
                    Case ("TMDT") : _Tomorrow.Value = CDate(drRow(dcColumn))
                    Case ("TMDW") : _TomorrowDayCode.Value = CDec(drRow(dcColumn))
                    Case ("WKDT") : mWeekFromToday.Value = CDate(drRow(dcColumn))
                    Case ("WK131") : mWeekEnding01.Value = CDate(drRow(dcColumn))
                    Case ("WK132") : mWeekEnding02.Value = CDate(drRow(dcColumn))
                    Case ("WK133") : mWeekEnding03.Value = CDate(drRow(dcColumn))
                    Case ("WK134") : mWeekEnding04.Value = CDate(drRow(dcColumn))
                    Case ("WK135") : mWeekEnding05.Value = CDate(drRow(dcColumn))
                    Case ("WK136") : mWeekEnding06.Value = CDate(drRow(dcColumn))
                    Case ("WK137") : mWeekEnding07.Value = CDate(drRow(dcColumn))
                    Case ("WK138") : mWeekEnding08.Value = CDate(drRow(dcColumn))
                    Case ("WK139") : mWeekEnding09.Value = CDate(drRow(dcColumn))
                    Case ("WK1310") : mWeekEnding10.Value = CDate(drRow(dcColumn))
                    Case ("WK1311") : mWeekEnding11.Value = CDate(drRow(dcColumn))
                    Case ("WK1312") : mWeekEnding12.Value = CDate(drRow(dcColumn))
                    Case ("WK1313") : mWeekEnding13.Value = CDate(drRow(dcColumn))
                    Case ("LIVE") : _StoreLiveDate.Value = CDate(drRow(dcColumn))
                    Case ("PSET") : _UseSet.Value = CDec(drRow(dcColumn))
                    Case ("S1DT1") : mSet1PeriodEnd01.Value = CDate(drRow(dcColumn))
                    Case ("S1DT2") : mSet1PeriodEnd02.Value = CDate(drRow(dcColumn))
                    Case ("S1DT3") : mSet1PeriodEnd03.Value = CDate(drRow(dcColumn))
                    Case ("S1DT4") : mSet1PeriodEnd04.Value = CDate(drRow(dcColumn))
                    Case ("S1DT5") : mSet1PeriodEnd05.Value = CDate(drRow(dcColumn))
                    Case ("S1DT6") : mSet1PeriodEnd06.Value = CDate(drRow(dcColumn))
                    Case ("S1DT7") : mSet1PeriodEnd07.Value = CDate(drRow(dcColumn))
                    Case ("S1DT8") : mSet1PeriodEnd08.Value = CDate(drRow(dcColumn))
                    Case ("S1DT9") : mSet1PeriodEnd09.Value = CDate(drRow(dcColumn))
                    Case ("S1DT10") : mSet1PeriodEnd10.Value = CDate(drRow(dcColumn))
                    Case ("S1DT11") : mSet1PeriodEnd11.Value = CDate(drRow(dcColumn))
                    Case ("S1DT12") : mSet1PeriodEnd12.Value = CDate(drRow(dcColumn))
                    Case ("S1DT13") : mSet1PeriodEnd13.Value = CDate(drRow(dcColumn))
                    Case ("S1DT14") : mSet1PeriodEnd14.Value = CDate(drRow(dcColumn))
                    Case ("S1YR1") : mSet1YearEnd01.Value = CBool(drRow(dcColumn))
                    Case ("S1YR2") : mSet1YearEnd02.Value = CBool(drRow(dcColumn))
                    Case ("S1YR3") : mSet1YearEnd03.Value = CBool(drRow(dcColumn))
                    Case ("S1YR4") : mSet1YearEnd04.Value = CBool(drRow(dcColumn))
                    Case ("S1YR5") : mSet1YearEnd05.Value = CBool(drRow(dcColumn))
                    Case ("S1YR6") : mSet1YearEnd06.Value = CBool(drRow(dcColumn))
                    Case ("S1YR7") : mSet1YearEnd07.Value = CBool(drRow(dcColumn))
                    Case ("S1YR8") : mSet1YearEnd08.Value = CBool(drRow(dcColumn))
                    Case ("S1YR9") : mSet1YearEnd09.Value = CBool(drRow(dcColumn))
                    Case ("S1YR10") : mSet1YearEnd10.Value = CBool(drRow(dcColumn))
                    Case ("S1YR11") : mSet1YearEnd11.Value = CBool(drRow(dcColumn))
                    Case ("S1YR12") : mSet1YearEnd12.Value = CBool(drRow(dcColumn))
                    Case ("S1YR13") : mSet1YearEnd13.Value = CBool(drRow(dcColumn))
                    Case ("S1YR14") : mSet1YearEnd14.Value = CBool(drRow(dcColumn))
                    Case ("S1PF1") : mSet1Processed01.Value = CBool(drRow(dcColumn))
                    Case ("S1PF2") : mSet1Processed02.Value = CBool(drRow(dcColumn))
                    Case ("S1PF3") : mSet1Processed03.Value = CBool(drRow(dcColumn))
                    Case ("S1PF4") : mSet1Processed04.Value = CBool(drRow(dcColumn))
                    Case ("S1PF5") : mSet1Processed05.Value = CBool(drRow(dcColumn))
                    Case ("S1PF6") : mSet1Processed06.Value = CBool(drRow(dcColumn))
                    Case ("S1PF7") : mSet1Processed07.Value = CBool(drRow(dcColumn))
                    Case ("S1PF8") : mSet1Processed08.Value = CBool(drRow(dcColumn))
                    Case ("S1PF9") : mSet1Processed09.Value = CBool(drRow(dcColumn))
                    Case ("S1PF10") : mSet1Processed10.Value = CBool(drRow(dcColumn))
                    Case ("S1PF11") : mSet1Processed11.Value = CBool(drRow(dcColumn))
                    Case ("S1PF12") : mSet1Processed12.Value = CBool(drRow(dcColumn))
                    Case ("S1PF13") : mSet1Processed13.Value = CBool(drRow(dcColumn))
                    Case ("S1PF14") : mSet1Processed14.Value = CBool(drRow(dcColumn))
                    Case ("S2DT1") : mSet2PeriodEnd01.Value = CDate(drRow(dcColumn))
                    Case ("S2DT2") : mSet2PeriodEnd02.Value = CDate(drRow(dcColumn))
                    Case ("S2DT3") : mSet2PeriodEnd03.Value = CDate(drRow(dcColumn))
                    Case ("S2DT4") : mSet2PeriodEnd04.Value = CDate(drRow(dcColumn))
                    Case ("S2DT5") : mSet2PeriodEnd05.Value = CDate(drRow(dcColumn))
                    Case ("S2DT6") : mSet2PeriodEnd06.Value = CDate(drRow(dcColumn))
                    Case ("S2DT7") : mSet2PeriodEnd07.Value = CDate(drRow(dcColumn))
                    Case ("S2DT8") : mSet2PeriodEnd08.Value = CDate(drRow(dcColumn))
                    Case ("S2DT9") : mSet2PeriodEnd09.Value = CDate(drRow(dcColumn))
                    Case ("S2DT10") : mSet2PeriodEnd10.Value = CDate(drRow(dcColumn))
                    Case ("S2DT11") : mSet2PeriodEnd11.Value = CDate(drRow(dcColumn))
                    Case ("S2DT12") : mSet2PeriodEnd12.Value = CDate(drRow(dcColumn))
                    Case ("S2DT13") : mSet2PeriodEnd13.Value = CDate(drRow(dcColumn))
                    Case ("S2DT14") : mSet2PeriodEnd14.Value = CDate(drRow(dcColumn))
                    Case ("S2YR1") : mSet2YearEnd01.Value = CBool(drRow(dcColumn))
                    Case ("S2YR2") : mSet2YearEnd02.Value = CBool(drRow(dcColumn))
                    Case ("S2YR3") : mSet2YearEnd03.Value = CBool(drRow(dcColumn))
                    Case ("S2YR4") : mSet2YearEnd04.Value = CBool(drRow(dcColumn))
                    Case ("S2YR5") : mSet2YearEnd05.Value = CBool(drRow(dcColumn))
                    Case ("S2YR6") : mSet2YearEnd06.Value = CBool(drRow(dcColumn))
                    Case ("S2YR7") : mSet2YearEnd07.Value = CBool(drRow(dcColumn))
                    Case ("S2YR8") : mSet2YearEnd08.Value = CBool(drRow(dcColumn))
                    Case ("S2YR9") : mSet2YearEnd09.Value = CBool(drRow(dcColumn))
                    Case ("S2YR10") : mSet2YearEnd10.Value = CBool(drRow(dcColumn))
                    Case ("S2YR11") : mSet2YearEnd11.Value = CBool(drRow(dcColumn))
                    Case ("S2YR12") : mSet2YearEnd12.Value = CBool(drRow(dcColumn))
                    Case ("S2YR13") : mSet2YearEnd13.Value = CBool(drRow(dcColumn))
                    Case ("S2YR14") : mSet2YearEnd14.Value = CBool(drRow(dcColumn))
                    Case ("S2PF1") : mSet2Processed01.Value = CBool(drRow(dcColumn))
                    Case ("S2PF2") : mSet2Processed02.Value = CBool(drRow(dcColumn))
                    Case ("S2PF3") : mSet2Processed03.Value = CBool(drRow(dcColumn))
                    Case ("S2PF4") : mSet2Processed04.Value = CBool(drRow(dcColumn))
                    Case ("S2PF5") : mSet2Processed05.Value = CBool(drRow(dcColumn))
                    Case ("S2PF6") : mSet2Processed06.Value = CBool(drRow(dcColumn))
                    Case ("S2PF7") : mSet2Processed07.Value = CBool(drRow(dcColumn))
                    Case ("S2PF8") : mSet2Processed08.Value = CBool(drRow(dcColumn))
                    Case ("S2PF9") : mSet2Processed09.Value = CBool(drRow(dcColumn))
                    Case ("S2PF10") : mSet2Processed10.Value = CBool(drRow(dcColumn))
                    Case ("S2PF11") : mSet2Processed11.Value = CBool(drRow(dcColumn))
                    Case ("S2PF12") : mSet2Processed12.Value = CBool(drRow(dcColumn))
                    Case ("S2PF13") : mSet2Processed13.Value = CBool(drRow(dcColumn))
                    Case ("S2PF14") : mSet2Processed14.Value = CBool(drRow(dcColumn))
                    Case ("TIDT") : _CurrentPeriodYearEndDate.Value = CDate(drRow(dcColumn))
                    Case ("LADT") : _PriorPeriodYearEndDate.Value = CDate(drRow(dcColumn))
                    Case ("TIYR") : _CurrentEndIsYearEnd.Value = CBool(drRow(dcColumn))
                    Case ("PPER") : _PeriodEndandNotProcessed.Value = CBool(drRow(dcColumn))
                    Case ("PWEK") : _WeekEndandNotProcessed.Value = CBool(drRow(dcColumn))
                    Case ("NSET") : _NetSetNo.Value = CStr(drRow(dcColumn))
                    Case ("NITE") : _NightSetNo.Value = CStr(drRow(dcColumn))
                    Case ("RETY") : _RetryMode.Value = CBool(drRow(dcColumn))
                    Case ("CYNO") : _WeeksInCycle.Value = CDec(drRow(dcColumn))
                    Case ("CYCW") : _WeekCycleNumber.Value = CDec(drRow(dcColumn))
                    Case ("AUDT") : _AuditDate.Value = CDate(drRow(dcColumn))
                    Case ("DPCA") : _DaysPriorToPriceChange.Value = CDec(drRow(dcColumn))
                    Case ("LDAT") : _LastNightlyCloseDate.Value = CDate(drRow(dcColumn))
                    Case ("SPARE")
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("FKEY", "", True, False, 0, 0, True, True, 1, 0, 0, 1, "", 0, "System Dates ID")
    Private _DaysOpen As New ColField(Of Decimal)("DAYS", 0, False, False, 0, 0, True, True, 2, 0, 0, 1, "", 0, "Days Open")
    Private _EndofWeekDay As New ColField(Of Decimal)("WEND", 0, False, False, 0, 0, True, True, 3, 0, 0, 1, "", 0, "End Of Week Day")
    Private _Today As New ColField(Of Date)("TODT", Nothing, False, False, 0, 0, True, True, 18, 0, 0, 1, "", 0, "Today")
    Private _TodayDayCode As New ColField(Of Decimal)("TODW", 0, False, False, 0, 0, True, True, 19, 0, 0, 1, "", 0, "Today Day Code")
    Private _Tomorrow As New ColField(Of Date)("TMDT", Nothing, False, False, 0, 0, True, True, 20, 0, 0, 1, "", 0, "Tomorrow")
    Private _TomorrowDayCode As New ColField(Of Decimal)("TMDW", 0, False, False, 0, 0, True, True, 21, 0, 0, 1, "", 0, "Tomorrows Day Code")
    Private _StoreLiveDate As New ColField(Of Date)("LIVE", Nothing, False, False, 0, 0, True, True, 36, 0, 0, 1, "", 0, "Store Live Date")
    Private _UseSet As New ColField(Of Decimal)("PSET", 0, False, False, 0, 0, True, True, 37, 0, 0, 1, "", 0, "Use Set")

    Private mDaysOpenInPeriod01 As New ColField(Of Decimal)("OPEN1", 0, False, False, 0, 0, True, True, 4, 0, 0, 1, "", 0, "Days Open In Period 01")
    Private mDaysOpenInPeriod02 As New ColField(Of Decimal)("OPEN2", 0, False, False, 0, 0, True, True, 5, 0, 0, 1, "", 0, "Days Open In Period 02")
    Private mDaysOpenInPeriod03 As New ColField(Of Decimal)("OPEN3", 0, False, False, 0, 0, True, True, 6, 0, 0, 1, "", 0, "Days Open In Period 03")
    Private mDaysOpenInPeriod04 As New ColField(Of Decimal)("OPEN4", 0, False, False, 0, 0, True, True, 7, 0, 0, 1, "", 0, "Days Open In Period 04")
    Private mDaysOpenInPeriod05 As New ColField(Of Decimal)("OPEN5", 0, False, False, 0, 0, True, True, 8, 0, 0, 1, "", 0, "Days Open In Period 05")
    Private mDaysOpenInPeriod06 As New ColField(Of Decimal)("OPEN6", 0, False, False, 0, 0, True, True, 9, 0, 0, 1, "", 0, "Days Open In Period 06")
    Private mDaysOpenInPeriod07 As New ColField(Of Decimal)("OPEN7", 0, False, False, 0, 0, True, True, 10, 0, 0, 1, "", 0, "Days Open In Period 07")
    Private mDaysOpenInPeriod08 As New ColField(Of Decimal)("OPEN8", 0, False, False, 0, 0, True, True, 11, 0, 0, 1, "", 0, "Days Open In Period 08")
    Private mDaysOpenInPeriod09 As New ColField(Of Decimal)("OPEN9", 0, False, False, 0, 0, True, True, 12, 0, 0, 1, "", 0, "Days Open In Period 09")
    Private mDaysOpenInPeriod10 As New ColField(Of Decimal)("OPEN10", 0, False, False, 0, 0, True, True, 13, 0, 0, 1, "", 0, "Days Open In Period 10")
    Private mDaysOpenInPeriod11 As New ColField(Of Decimal)("OPEN11", 0, False, False, 0, 0, True, True, 14, 0, 0, 1, "", 0, "Days Open In Period 11")
    Private mDaysOpenInPeriod12 As New ColField(Of Decimal)("OPEN12", 0, False, False, 0, 0, True, True, 15, 0, 0, 1, "", 0, "Days Open In Period 12")
    Private mDaysOpenInPeriod13 As New ColField(Of Decimal)("OPEN13", 0, False, False, 0, 0, True, True, 16, 0, 0, 1, "", 0, "Days Open In Period 13")
    Private mDaysOpenInPeriod14 As New ColField(Of Decimal)("OPEN14", 0, False, False, 0, 0, True, True, 17, 0, 0, 1, "", 0, "Days Open In Period 14")

    Private mWeekFromToday As New ColField(Of Date)("WKDT", Nothing, False, False, 0, 0, True, True, 22, 0, 0, 1, "", 0, "Week From Today")
    Private mWeekEnding01 As New ColField(Of Date)("WK131", Nothing, False, False, 0, 0, True, True, 23, 0, 0, 1, "", 0, "Week Ending 01")
    Private mWeekEnding02 As New ColField(Of Date)("WK132", Nothing, False, False, 0, 0, True, True, 24, 0, 0, 1, "", 0, "Week Ending 02")
    Private mWeekEnding03 As New ColField(Of Date)("WK133", Nothing, False, False, 0, 0, True, True, 25, 0, 0, 1, "", 0, "Week Ending 03")
    Private mWeekEnding04 As New ColField(Of Date)("WK134", Nothing, False, False, 0, 0, True, True, 26, 0, 0, 1, "", 0, "Week Ending 04")
    Private mWeekEnding05 As New ColField(Of Date)("WK135", Nothing, False, False, 0, 0, True, True, 27, 0, 0, 1, "", 0, "Week Ending 05")
    Private mWeekEnding06 As New ColField(Of Date)("WK136", Nothing, False, False, 0, 0, True, True, 28, 0, 0, 1, "", 0, "Week Ending 06")
    Private mWeekEnding07 As New ColField(Of Date)("WK137", Nothing, False, False, 0, 0, True, True, 29, 0, 0, 1, "", 0, "Week Ending 07")
    Private mWeekEnding08 As New ColField(Of Date)("WK138", Nothing, False, False, 0, 0, True, True, 30, 0, 0, 1, "", 0, "Week Ending 08")
    Private mWeekEnding09 As New ColField(Of Date)("WK139", Nothing, False, False, 0, 0, True, True, 31, 0, 0, 1, "", 0, "Week Ending 09")
    Private mWeekEnding10 As New ColField(Of Date)("WK1310", Nothing, False, False, 0, 0, True, True, 32, 0, 0, 1, "", 0, "Week Ending 10")
    Private mWeekEnding11 As New ColField(Of Date)("WK1311", Nothing, False, False, 0, 0, True, True, 33, 0, 0, 1, "", 0, "Week Ending 11")
    Private mWeekEnding12 As New ColField(Of Date)("WK1312", Nothing, False, False, 0, 0, True, True, 34, 0, 0, 1, "", 0, "Week Ending 12")
    Private mWeekEnding13 As New ColField(Of Date)("WK1313", Nothing, False, False, 0, 0, True, True, 35, 0, 0, 1, "", 0, "Week Ending 13")
    Private mSet1PeriodEnd01 As New ColField(Of Date)("S1DT1", Nothing, False, False, 0, 0, True, True, 38, 0, 0, 1, "", 0, "Set 1 Period End 01")
    Private mSet1PeriodEnd02 As New ColField(Of Date)("S1DT2", Nothing, False, False, 0, 0, True, True, 39, 0, 0, 1, "", 0, "Set 1 Period End 02")
    Private mSet1PeriodEnd03 As New ColField(Of Date)("S1DT3", Nothing, False, False, 0, 0, True, True, 40, 0, 0, 1, "", 0, "Set 1 Period End 03")
    Private mSet1PeriodEnd04 As New ColField(Of Date)("S1DT4", Nothing, False, False, 0, 0, True, True, 41, 0, 0, 1, "", 0, "Set 1 Period End 04")
    Private mSet1PeriodEnd05 As New ColField(Of Date)("S1DT5", Nothing, False, False, 0, 0, True, True, 42, 0, 0, 1, "", 0, "Set 1 Period End 05")
    Private mSet1PeriodEnd06 As New ColField(Of Date)("S1DT6", Nothing, False, False, 0, 0, True, True, 43, 0, 0, 1, "", 0, "Set 1 Period End 06")
    Private mSet1PeriodEnd07 As New ColField(Of Date)("S1DT7", Nothing, False, False, 0, 0, True, True, 44, 0, 0, 1, "", 0, "Set 1 Period End 07")
    Private mSet1PeriodEnd08 As New ColField(Of Date)("S1DT8", Nothing, False, False, 0, 0, True, True, 45, 0, 0, 1, "", 0, "Set 1 Period End 08")
    Private mSet1PeriodEnd09 As New ColField(Of Date)("S1DT9", Nothing, False, False, 0, 0, True, True, 46, 0, 0, 1, "", 0, "Set 1 Period End 09")
    Private mSet1PeriodEnd10 As New ColField(Of Date)("S1DT10", Nothing, False, False, 0, 0, True, True, 47, 0, 0, 1, "", 0, "Set 1 Period End 10")
    Private mSet1PeriodEnd11 As New ColField(Of Date)("S1DT11", Nothing, False, False, 0, 0, True, True, 48, 0, 0, 1, "", 0, "Set 1 Period End 11")
    Private mSet1PeriodEnd12 As New ColField(Of Date)("S1DT12", Nothing, False, False, 0, 0, True, True, 49, 0, 0, 1, "", 0, "Set 1 Period End 12")
    Private mSet1PeriodEnd13 As New ColField(Of Date)("S1DT13", Nothing, False, False, 0, 0, True, True, 50, 0, 0, 1, "", 0, "Set 1 Period End 13")
    Private mSet1PeriodEnd14 As New ColField(Of Date)("S1DT14", Nothing, False, False, 0, 0, True, True, 51, 0, 0, 1, "", 0, "Set 1 Period End 14")
    Private mSet1YearEnd01 As New ColField(Of Boolean)("S1YR1", False, False, False, 0, 0, True, True, 52, 0, 0, 1, "", 0, "Set 1 Year End 01")
    Private mSet1YearEnd02 As New ColField(Of Boolean)("S1YR2", False, False, False, 0, 0, True, True, 53, 0, 0, 1, "", 0, "Set 1 Year End 02")
    Private mSet1YearEnd03 As New ColField(Of Boolean)("S1YR3", False, False, False, 0, 0, True, True, 54, 0, 0, 1, "", 0, "Set 1 Year End 03")
    Private mSet1YearEnd04 As New ColField(Of Boolean)("S1YR4", False, False, False, 0, 0, True, True, 55, 0, 0, 1, "", 0, "Set 1 Year End 04")
    Private mSet1YearEnd05 As New ColField(Of Boolean)("S1YR5", False, False, False, 0, 0, True, True, 56, 0, 0, 1, "", 0, "Set 1 Year End 05")
    Private mSet1YearEnd06 As New ColField(Of Boolean)("S1YR6", False, False, False, 0, 0, True, True, 57, 0, 0, 1, "", 0, "Set 1 Year End 06")
    Private mSet1YearEnd07 As New ColField(Of Boolean)("S1YR7", False, False, False, 0, 0, True, True, 58, 0, 0, 1, "", 0, "Set 1 Year End 07")
    Private mSet1YearEnd08 As New ColField(Of Boolean)("S1YR8", False, False, False, 0, 0, True, True, 59, 0, 0, 1, "", 0, "Set 1 Year End 08")
    Private mSet1YearEnd09 As New ColField(Of Boolean)("S1YR9", False, False, False, 0, 0, True, True, 60, 0, 0, 1, "", 0, "Set 1 Year End 09")
    Private mSet1YearEnd10 As New ColField(Of Boolean)("S1YR10", False, False, False, 0, 0, True, True, 61, 0, 0, 1, "", 0, "Set 1 Year End 10")
    Private mSet1YearEnd11 As New ColField(Of Boolean)("S1YR11", False, False, False, 0, 0, True, True, 62, 0, 0, 1, "", 0, "Set 1 Year End 11")
    Private mSet1YearEnd12 As New ColField(Of Boolean)("S1YR12", False, False, False, 0, 0, True, True, 63, 0, 0, 1, "", 0, "Set 1 Year End 12")
    Private mSet1YearEnd13 As New ColField(Of Boolean)("S1YR13", False, False, False, 0, 0, True, True, 64, 0, 0, 1, "", 0, "Set 1 Year End 13")
    Private mSet1YearEnd14 As New ColField(Of Boolean)("S1YR14", False, False, False, 0, 0, True, True, 65, 0, 0, 1, "", 0, "Set 1 Year End 14")
    Private mSet1Processed01 As New ColField(Of Boolean)("S1PF1", False, False, False, 0, 0, True, True, 66, 0, 0, 1, "", 0, "Set 1 Year Processed 01")
    Private mSet1Processed02 As New ColField(Of Boolean)("S1PF2", False, False, False, 0, 0, True, True, 67, 0, 0, 1, "", 0, "Set 1 Year Processed 02")
    Private mSet1Processed03 As New ColField(Of Boolean)("S1PF3", False, False, False, 0, 0, True, True, 68, 0, 0, 1, "", 0, "Set 1 Year Processed 03")
    Private mSet1Processed04 As New ColField(Of Boolean)("S1PF4", False, False, False, 0, 0, True, True, 69, 0, 0, 1, "", 0, "Set 1 Year Processed 04")
    Private mSet1Processed05 As New ColField(Of Boolean)("S1PF5", False, False, False, 0, 0, True, True, 70, 0, 0, 1, "", 0, "Set 1 Year Processed 05")
    Private mSet1Processed06 As New ColField(Of Boolean)("S1PF6", False, False, False, 0, 0, True, True, 71, 0, 0, 1, "", 0, "Set 1 Year Processed 06")
    Private mSet1Processed07 As New ColField(Of Boolean)("S1PF7", False, False, False, 0, 0, True, True, 72, 0, 0, 1, "", 0, "Set 1 Year Processed 07")
    Private mSet1Processed08 As New ColField(Of Boolean)("S1PF8", False, False, False, 0, 0, True, True, 73, 0, 0, 1, "", 0, "Set 1 Year Processed 08")
    Private mSet1Processed09 As New ColField(Of Boolean)("S1PF9", False, False, False, 0, 0, True, True, 74, 0, 0, 1, "", 0, "Set 1 Year Processed 09")
    Private mSet1Processed10 As New ColField(Of Boolean)("S1PF10", False, False, False, 0, 0, True, True, 75, 0, 0, 1, "", 0, "Set 1 Year Processed 10")
    Private mSet1Processed11 As New ColField(Of Boolean)("S1PF11", False, False, False, 0, 0, True, True, 76, 0, 0, 1, "", 0, "Set 1 Year Processed 11")
    Private mSet1Processed12 As New ColField(Of Boolean)("S1PF12", False, False, False, 0, 0, True, True, 77, 0, 0, 1, "", 0, "Set 1 Year Processed 12")
    Private mSet1Processed13 As New ColField(Of Boolean)("S1PF13", False, False, False, 0, 0, True, True, 78, 0, 0, 1, "", 0, "Set 1 Year Processed 13")
    Private mSet1Processed14 As New ColField(Of Boolean)("S1PF14", False, False, False, 0, 0, True, True, 79, 0, 0, 1, "", 0, "Set 1 Year Processed 14")
    Private mSet2PeriodEnd01 As New ColField(Of Date)("S2DT1", Nothing, False, False, 0, 0, True, True, 80, 0, 0, 1, "", 0, "Set 2 Period End 01")
    Private mSet2PeriodEnd02 As New ColField(Of Date)("S2DT2", Nothing, False, False, 0, 0, True, True, 81, 0, 0, 1, "", 0, "Set 2 Period End 02")
    Private mSet2PeriodEnd03 As New ColField(Of Date)("S2DT3", Nothing, False, False, 0, 0, True, True, 82, 0, 0, 1, "", 0, "Set 2 Period End 03")
    Private mSet2PeriodEnd04 As New ColField(Of Date)("S2DT4", Nothing, False, False, 0, 0, True, True, 83, 0, 0, 1, "", 0, "Set 2 Period End 04")
    Private mSet2PeriodEnd05 As New ColField(Of Date)("S2DT5", Nothing, False, False, 0, 0, True, True, 84, 0, 0, 1, "", 0, "Set 2 Period End 05")
    Private mSet2PeriodEnd06 As New ColField(Of Date)("S2DT6", Nothing, False, False, 0, 0, True, True, 85, 0, 0, 1, "", 0, "Set 2 Period End 06")
    Private mSet2PeriodEnd07 As New ColField(Of Date)("S2DT7", Nothing, False, False, 0, 0, True, True, 86, 0, 0, 1, "", 0, "Set 2 Period End 07")
    Private mSet2PeriodEnd08 As New ColField(Of Date)("S2DT8", Nothing, False, False, 0, 0, True, True, 87, 0, 0, 1, "", 0, "Set 2 Period End 08")
    Private mSet2PeriodEnd09 As New ColField(Of Date)("S2DT9", Nothing, False, False, 0, 0, True, True, 88, 0, 0, 1, "", 0, "Set 2 Period End 09")
    Private mSet2PeriodEnd10 As New ColField(Of Date)("S2DT10", Nothing, False, False, 0, 0, True, True, 89, 0, 0, 1, "", 0, "Set 2 Period End 10")
    Private mSet2PeriodEnd11 As New ColField(Of Date)("S2DT11", Nothing, False, False, 0, 0, True, True, 90, 0, 0, 1, "", 0, "Set 2 Period End 11")
    Private mSet2PeriodEnd12 As New ColField(Of Date)("S2DT12", Nothing, False, False, 0, 0, True, True, 91, 0, 0, 1, "", 0, "Set 2 Period End 12")
    Private mSet2PeriodEnd13 As New ColField(Of Date)("S2DT13", Nothing, False, False, 0, 0, True, True, 92, 0, 0, 1, "", 0, "Set 2 Period End 13")
    Private mSet2PeriodEnd14 As New ColField(Of Date)("S2DT14", Nothing, False, False, 0, 0, True, True, 93, 0, 0, 1, "", 0, "Set 2 Period End 14")
    Private mSet2YearEnd01 As New ColField(Of Boolean)("S2YR1", False, False, False, 0, 0, True, True, 94, 0, 0, 1, "", 0, "Set 2 Year End 01")
    Private mSet2YearEnd02 As New ColField(Of Boolean)("S2YR2", False, False, False, 0, 0, True, True, 95, 0, 0, 1, "", 0, "Set 2 Year End 02")
    Private mSet2YearEnd03 As New ColField(Of Boolean)("S2YR3", False, False, False, 0, 0, True, True, 96, 0, 0, 1, "", 0, "Set 2 Year End 03")
    Private mSet2YearEnd04 As New ColField(Of Boolean)("S2YR4", False, False, False, 0, 0, True, True, 97, 0, 0, 1, "", 0, "Set 2 Year End 04")
    Private mSet2YearEnd05 As New ColField(Of Boolean)("S2YR5", False, False, False, 0, 0, True, True, 98, 0, 0, 1, "", 0, "Set 2 Year End 05")
    Private mSet2YearEnd06 As New ColField(Of Boolean)("S2YR6", False, False, False, 0, 0, True, True, 99, 0, 0, 1, "", 0, "Set 2 Year End 06")
    Private mSet2YearEnd07 As New ColField(Of Boolean)("S2YR7", False, False, False, 0, 0, True, True, 100, 0, 0, 1, "", 0, "Set 2 Year End 07")
    Private mSet2YearEnd08 As New ColField(Of Boolean)("S2YR8", False, False, False, 0, 0, True, True, 101, 0, 0, 1, "", 0, "Set 2 Year End 08")
    Private mSet2YearEnd09 As New ColField(Of Boolean)("S2YR9", False, False, False, 0, 0, True, True, 102, 0, 0, 1, "", 0, "Set 2 Year End 09")
    Private mSet2YearEnd10 As New ColField(Of Boolean)("S2YR10", False, False, False, 0, 0, True, True, 103, 0, 0, 1, "", 0, "Set 2 Year End 10")
    Private mSet2YearEnd11 As New ColField(Of Boolean)("S2YR11", False, False, False, 0, 0, True, True, 104, 0, 0, 1, "", 0, "Set 2 Year End 11")
    Private mSet2YearEnd12 As New ColField(Of Boolean)("S2YR12", False, False, False, 0, 0, True, True, 105, 0, 0, 1, "", 0, "Set 2 Year End 12")
    Private mSet2YearEnd13 As New ColField(Of Boolean)("S2YR13", False, False, False, 0, 0, True, True, 106, 0, 0, 1, "", 0, "Set 2 Year End 13")
    Private mSet2YearEnd14 As New ColField(Of Boolean)("S2YR14", False, False, False, 0, 0, True, True, 107, 0, 0, 1, "", 0, "Set 2 Year End 14")
    Private mSet2Processed01 As New ColField(Of Boolean)("S2PF1", False, False, False, 0, 0, True, True, 108, 0, 0, 1, "", 0, "Set 2 Processed 01")
    Private mSet2Processed02 As New ColField(Of Boolean)("S2PF2", False, False, False, 0, 0, True, True, 109, 0, 0, 1, "", 0, "Set 2 Processed 02")
    Private mSet2Processed03 As New ColField(Of Boolean)("S2PF3", False, False, False, 0, 0, True, True, 110, 0, 0, 1, "", 0, "Set 2 Processed 03")
    Private mSet2Processed04 As New ColField(Of Boolean)("S2PF4", False, False, False, 0, 0, True, True, 111, 0, 0, 1, "", 0, "Set 2 Processed 04")
    Private mSet2Processed05 As New ColField(Of Boolean)("S2PF5", False, False, False, 0, 0, True, True, 112, 0, 0, 1, "", 0, "Set 2 Processed 05")
    Private mSet2Processed06 As New ColField(Of Boolean)("S2PF6", False, False, False, 0, 0, True, True, 113, 0, 0, 1, "", 0, "Set 2 Processed 06")
    Private mSet2Processed07 As New ColField(Of Boolean)("S2PF7", False, False, False, 0, 0, True, True, 114, 0, 0, 1, "", 0, "Set 2 Processed 07")
    Private mSet2Processed08 As New ColField(Of Boolean)("S2PF8", False, False, False, 0, 0, True, True, 115, 0, 0, 1, "", 0, "Set 2 Processed 08")
    Private mSet2Processed09 As New ColField(Of Boolean)("S2PF9", False, False, False, 0, 0, True, True, 116, 0, 0, 1, "", 0, "Set 2 Processed 09")
    Private mSet2Processed10 As New ColField(Of Boolean)("S2PF10", False, False, False, 0, 0, True, True, 117, 0, 0, 1, "", 0, "Set 2 Processed 10")
    Private mSet2Processed11 As New ColField(Of Boolean)("S2PF11", False, False, False, 0, 0, True, True, 118, 0, 0, 1, "", 0, "Set 2 Processed 11")
    Private mSet2Processed12 As New ColField(Of Boolean)("S2PF12", False, False, False, 0, 0, True, True, 119, 0, 0, 1, "", 0, "Set 2 Processed 12")
    Private mSet2Processed13 As New ColField(Of Boolean)("S2PF13", False, False, False, 0, 0, True, True, 120, 0, 0, 1, "", 0, "Set 2 Processed 13")
    Private mSet2Processed14 As New ColField(Of Boolean)("S2PF14", False, False, False, 0, 0, True, True, 121, 0, 0, 1, "", 0, "Set 2 Processed 14")

    Private _CurrentPeriodYearEndDate As New ColField(Of Date)("TIDT", Nothing, False, False, 0, 0, True, True, 122, 0, 0, 1, "", 0, "Current Period Year End Date")
    Private _PriorPeriodYearEndDate As New ColField(Of Date)("LADT", Nothing, False, False, 0, 0, True, True, 123, 0, 0, 1, "", 0, "Prior Period Year End Date")
    Private _CurrentEndIsYearEnd As New ColField(Of Boolean)("TIYR", False, False, False, 0, 0, True, True, 124, 0, 0, 1, "", 0, "Current End Is Year End")
    Private _PeriodEndandNotProcessed As New ColField(Of Boolean)("PPER", False, False, False, 0, 0, True, True, 125, 0, 0, 1, "", 0, "Period End And Not Processed")
    Private _WeekEndandNotProcessed As New ColField(Of Boolean)("PWEK", False, False, False, 0, 0, True, True, 126, 0, 0, 1, "", 0, "Week End And Not Processed")
    Private _NetSetNo As New ColField(Of String)("NSET", "", False, False, 0, 0, True, True, 127, 0, 0, 1, "", 0, "Net Set No")
    Private _NightSetNo As New ColField(Of String)("NITE", "", False, False, 0, 0, True, True, 128, 0, 0, 1, "", 0, "Night Set No")
    Private _RetryMode As New ColField(Of Boolean)("RETY", False, False, False, 0, 0, True, True, 129, 0, 0, 1, "", 0, "Retry Mode")
    Private _WeeksInCycle As New ColField(Of Decimal)("CYNO", 0, False, False, 0, 0, True, True, 130, 0, 0, 1, "", 0, "Weeks In Cycle")
    Private _WeekCycleNumber As New ColField(Of Decimal)("CYCW", 0, False, False, 0, 0, True, True, 131, 0, 0, 1, "", 0, "Week Cycle Number")
    Private _AuditDate As New ColField(Of Date)("AUDT", Nothing, False, False, 0, 0, True, True, 132, 0, 0, 1, "", 0, "Audit Date")
    Private _DaysPriorToPriceChange As New ColField(Of Decimal)("DPCA", 0, False, False, 0, 0, True, True, 133, 0, 0, 1, "", 0, "Days Prior To Price Change")
    Private _LastNightlyCloseDate As New ColField(Of Date)("LDAT", Nothing, False, False, 0, 0, True, True, 134, 0, 0, 1, "", 0, "Last Nightly Close Date")

#End Region

#Region "Field Properties"

    Public Property SystemDatesID() As ColField(Of String)
        Get
            SystemDatesID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property DaysOpen() As ColField(Of Decimal)
        Get
            DaysOpen = _DaysOpen
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DaysOpen = value
        End Set
    End Property
    Public Property EndofWeekDay() As ColField(Of Decimal)
        Get
            EndofWeekDay = _EndofWeekDay
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EndofWeekDay = value
        End Set
    End Property
    Public Property DaysOpenInPeriod01() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod01 = mDaysOpenInPeriod01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod01 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod02() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod02 = mDaysOpenInPeriod02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod02 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod03() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod03 = mDaysOpenInPeriod03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod03 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod04() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod04 = mDaysOpenInPeriod04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod04 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod05() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod05 = mDaysOpenInPeriod05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod05 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod06() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod06 = mDaysOpenInPeriod06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod06 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod07() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod07 = mDaysOpenInPeriod07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod07 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod08() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod08 = mDaysOpenInPeriod08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod08 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod09() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod09 = mDaysOpenInPeriod09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod09 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod10() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod10 = mDaysOpenInPeriod10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod10 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod11() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod11 = mDaysOpenInPeriod11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod11 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod12() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod12 = mDaysOpenInPeriod12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod12 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod13() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod13 = mDaysOpenInPeriod13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod13 = value
        End Set
    End Property
    Public Property DaysOpenInPeriod14() As ColField(Of Decimal)
        Get
            DaysOpenInPeriod14 = mDaysOpenInPeriod14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysOpenInPeriod14 = value
        End Set
    End Property
    Public Property Today() As ColField(Of Date)
        Get
            Today = _Today
        End Get
        Set(ByVal value As ColField(Of Date))
            _Today = value
        End Set
    End Property
    Public Property TodayDayCode() As ColField(Of Decimal)
        Get
            TodayDayCode = _TodayDayCode
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TodayDayCode = value
        End Set
    End Property
    Public Property Tomorrow() As ColField(Of Date)
        Get
            Tomorrow = _Tomorrow
        End Get
        Set(ByVal value As ColField(Of Date))
            _Tomorrow = value
        End Set
    End Property
    Public Property TomorrowsDayCode() As ColField(Of Decimal)
        Get
            TomorrowsDayCode = _TomorrowDayCode
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TomorrowDayCode = value
        End Set
    End Property
    Public Property WeekFromToday() As ColField(Of Date)
        Get
            WeekFromToday = mWeekFromToday
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekFromToday = value
        End Set
    End Property

    Public Property WeekEnding(ByVal Index As Integer) As ColField(Of Date)
        Get
            Dim ColName As String = WeekEnding01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString("30")

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Date))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Date))

            If Index = 0 Then Index = 7
            Dim ColName As String = WeekEnding01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString("00")

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Date)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property WeekEnding01() As ColField(Of Date)
        Get
            WeekEnding01 = mWeekEnding01
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding01 = value
        End Set
    End Property
    Public Property WeekEnding02() As ColField(Of Date)
        Get
            WeekEnding02 = mWeekEnding02
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding02 = value
        End Set
    End Property
    Public Property WeekEnding03() As ColField(Of Date)
        Get
            WeekEnding03 = mWeekEnding03
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding03 = value
        End Set
    End Property
    Public Property WeekEnding04() As ColField(Of Date)
        Get
            WeekEnding04 = mWeekEnding04
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding04 = value
        End Set
    End Property
    Public Property WeekEnding05() As ColField(Of Date)
        Get
            WeekEnding05 = mWeekEnding05
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding05 = value
        End Set
    End Property
    Public Property WeekEnding06() As ColField(Of Date)
        Get
            WeekEnding06 = mWeekEnding06
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding06 = value
        End Set
    End Property
    Public Property WeekEnding07() As ColField(Of Date)
        Get
            WeekEnding07 = mWeekEnding07
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding07 = value
        End Set
    End Property
    Public Property WeekEnding08() As ColField(Of Date)
        Get
            WeekEnding08 = mWeekEnding08
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding08 = value
        End Set
    End Property
    Public Property WeekEnding09() As ColField(Of Date)
        Get
            WeekEnding09 = mWeekEnding09
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding09 = value
        End Set
    End Property
    Public Property WeekEnding10() As ColField(Of Date)
        Get
            WeekEnding10 = mWeekEnding10
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding10 = value
        End Set
    End Property
    Public Property WeekEnding11() As ColField(Of Date)
        Get
            WeekEnding11 = mWeekEnding11
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding11 = value
        End Set
    End Property
    Public Property WeekEnding12() As ColField(Of Date)
        Get
            WeekEnding12 = mWeekEnding12
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding12 = value
        End Set
    End Property
    Public Property WeekEnding13() As ColField(Of Date)
        Get
            WeekEnding13 = mWeekEnding13
        End Get
        Set(ByVal value As ColField(Of Date))
            mWeekEnding13 = value
        End Set
    End Property
    Public Property StoreLiveDate() As ColField(Of Date)
        Get
            StoreLiveDate = _StoreLiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StoreLiveDate = value
        End Set
    End Property
    Public Property UseSet() As ColField(Of Decimal)
        Get
            UseSet = _UseSet
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UseSet = value
        End Set
    End Property
    Public Property Set1PeriodEnd01() As ColField(Of Date)
        Get
            Set1PeriodEnd01 = mSet1PeriodEnd01
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd01 = value
        End Set
    End Property
    Public Property Set1PeriodEnd02() As ColField(Of Date)
        Get
            Set1PeriodEnd02 = mSet1PeriodEnd02
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd02 = value
        End Set
    End Property
    Public Property Set1PeriodEnd03() As ColField(Of Date)
        Get
            Set1PeriodEnd03 = mSet1PeriodEnd03
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd03 = value
        End Set
    End Property
    Public Property Set1PeriodEnd04() As ColField(Of Date)
        Get
            Set1PeriodEnd04 = mSet1PeriodEnd04
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd04 = value
        End Set
    End Property
    Public Property Set1PeriodEnd05() As ColField(Of Date)
        Get
            Set1PeriodEnd05 = mSet1PeriodEnd05
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd05 = value
        End Set
    End Property
    Public Property Set1PeriodEnd06() As ColField(Of Date)
        Get
            Set1PeriodEnd06 = mSet1PeriodEnd06
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd06 = value
        End Set
    End Property
    Public Property Set1PeriodEnd07() As ColField(Of Date)
        Get
            Set1PeriodEnd07 = mSet1PeriodEnd07
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd07 = value
        End Set
    End Property
    Public Property Set1PeriodEnd08() As ColField(Of Date)
        Get
            Set1PeriodEnd08 = mSet1PeriodEnd08
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd08 = value
        End Set
    End Property
    Public Property Set1PeriodEnd09() As ColField(Of Date)
        Get
            Set1PeriodEnd09 = mSet1PeriodEnd09
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd09 = value
        End Set
    End Property
    Public Property Set1PeriodEnd10() As ColField(Of Date)
        Get
            Set1PeriodEnd10 = mSet1PeriodEnd10
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd10 = value
        End Set
    End Property
    Public Property Set1PeriodEnd11() As ColField(Of Date)
        Get
            Set1PeriodEnd11 = mSet1PeriodEnd11
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd11 = value
        End Set
    End Property
    Public Property Set1PeriodEnd12() As ColField(Of Date)
        Get
            Set1PeriodEnd12 = mSet1PeriodEnd12
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd12 = value
        End Set
    End Property
    Public Property Set1PeriodEnd13() As ColField(Of Date)
        Get
            Set1PeriodEnd13 = mSet1PeriodEnd13
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd13 = value
        End Set
    End Property
    Public Property Set1PeriodEnd14() As ColField(Of Date)
        Get
            Set1PeriodEnd14 = mSet1PeriodEnd14
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet1PeriodEnd14 = value
        End Set
    End Property
    Public Property Set1YearEnd01() As ColField(Of Boolean)
        Get
            Set1YearEnd01 = mSet1YearEnd01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd01 = value
        End Set
    End Property
    Public Property Set1YearEnd02() As ColField(Of Boolean)
        Get
            Set1YearEnd02 = mSet1YearEnd02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd02 = value
        End Set
    End Property
    Public Property Set1YearEnd03() As ColField(Of Boolean)
        Get
            Set1YearEnd03 = mSet1YearEnd03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd03 = value
        End Set
    End Property
    Public Property Set1YearEnd04() As ColField(Of Boolean)
        Get
            Set1YearEnd04 = mSet1YearEnd04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd04 = value
        End Set
    End Property
    Public Property Set1YearEnd05() As ColField(Of Boolean)
        Get
            Set1YearEnd05 = mSet1YearEnd05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd05 = value
        End Set
    End Property
    Public Property Set1YearEnd06() As ColField(Of Boolean)
        Get
            Set1YearEnd06 = mSet1YearEnd06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd06 = value
        End Set
    End Property
    Public Property Set1YearEnd07() As ColField(Of Boolean)
        Get
            Set1YearEnd07 = mSet1YearEnd07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd07 = value
        End Set
    End Property
    Public Property Set1YearEnd08() As ColField(Of Boolean)
        Get
            Set1YearEnd08 = mSet1YearEnd08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd08 = value
        End Set
    End Property
    Public Property Set1YearEnd09() As ColField(Of Boolean)
        Get
            Set1YearEnd09 = mSet1YearEnd09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd09 = value
        End Set
    End Property
    Public Property Set1YearEnd10() As ColField(Of Boolean)
        Get
            Set1YearEnd10 = mSet1YearEnd10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd10 = value
        End Set
    End Property
    Public Property Set1YearEnd11() As ColField(Of Boolean)
        Get
            Set1YearEnd11 = mSet1YearEnd11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd11 = value
        End Set
    End Property
    Public Property Set1YearEnd12() As ColField(Of Boolean)
        Get
            Set1YearEnd12 = mSet1YearEnd12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd12 = value
        End Set
    End Property
    Public Property Set1YearEnd13() As ColField(Of Boolean)
        Get
            Set1YearEnd13 = mSet1YearEnd13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd13 = value
        End Set
    End Property
    Public Property Set1YearEnd14() As ColField(Of Boolean)
        Get
            Set1YearEnd14 = mSet1YearEnd14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1YearEnd14 = value
        End Set
    End Property
    Public Property Set1Processed01() As ColField(Of Boolean)
        Get
            Set1Processed01 = mSet1Processed01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed01 = value
        End Set
    End Property
    Public Property Set1Processed02() As ColField(Of Boolean)
        Get
            Set1Processed02 = mSet1Processed02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed02 = value
        End Set
    End Property
    Public Property Set1Processed03() As ColField(Of Boolean)
        Get
            Set1Processed03 = mSet1Processed03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed03 = value
        End Set
    End Property
    Public Property Set1Processed04() As ColField(Of Boolean)
        Get
            Set1Processed04 = mSet1Processed04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed04 = value
        End Set
    End Property
    Public Property Set1Processed05() As ColField(Of Boolean)
        Get
            Set1Processed05 = mSet1Processed05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed05 = value
        End Set
    End Property
    Public Property Set1Processed06() As ColField(Of Boolean)
        Get
            Set1Processed06 = mSet1Processed06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed06 = value
        End Set
    End Property
    Public Property Set1Processed07() As ColField(Of Boolean)
        Get
            Set1Processed07 = mSet1Processed07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed07 = value
        End Set
    End Property
    Public Property Set1Processed08() As ColField(Of Boolean)
        Get
            Set1Processed08 = mSet1Processed08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed08 = value
        End Set
    End Property
    Public Property Set1Processed09() As ColField(Of Boolean)
        Get
            Set1Processed09 = mSet1Processed09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed09 = value
        End Set
    End Property
    Public Property Set1Processed10() As ColField(Of Boolean)
        Get
            Set1Processed10 = mSet1Processed10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed10 = value
        End Set
    End Property
    Public Property Set1Processed11() As ColField(Of Boolean)
        Get
            Set1Processed11 = mSet1Processed11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed11 = value
        End Set
    End Property
    Public Property Set1Processed12() As ColField(Of Boolean)
        Get
            Set1Processed12 = mSet1Processed12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed12 = value
        End Set
    End Property
    Public Property Set1Processed13() As ColField(Of Boolean)
        Get
            Set1Processed13 = mSet1Processed13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed13 = value
        End Set
    End Property
    Public Property Set1Processed14() As ColField(Of Boolean)
        Get
            Set1Processed14 = mSet1Processed14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet1Processed14 = value
        End Set
    End Property
    Public Property Set2PeriodEnd01() As ColField(Of Date)
        Get
            Set2PeriodEnd01 = mSet2PeriodEnd01
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd01 = value
        End Set
    End Property
    Public Property Set2PeriodEnd02() As ColField(Of Date)
        Get
            Set2PeriodEnd02 = mSet2PeriodEnd02
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd02 = value
        End Set
    End Property
    Public Property Set2PeriodEnd03() As ColField(Of Date)
        Get
            Set2PeriodEnd03 = mSet2PeriodEnd03
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd03 = value
        End Set
    End Property
    Public Property Set2PeriodEnd04() As ColField(Of Date)
        Get
            Set2PeriodEnd04 = mSet2PeriodEnd04
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd04 = value
        End Set
    End Property
    Public Property Set2PeriodEnd05() As ColField(Of Date)
        Get
            Set2PeriodEnd05 = mSet2PeriodEnd05
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd05 = value
        End Set
    End Property
    Public Property Set2PeriodEnd06() As ColField(Of Date)
        Get
            Set2PeriodEnd06 = mSet2PeriodEnd06
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd06 = value
        End Set
    End Property
    Public Property Set2PeriodEnd07() As ColField(Of Date)
        Get
            Set2PeriodEnd07 = mSet2PeriodEnd07
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd07 = value
        End Set
    End Property
    Public Property Set2PeriodEnd08() As ColField(Of Date)
        Get
            Set2PeriodEnd08 = mSet2PeriodEnd08
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd08 = value
        End Set
    End Property
    Public Property Set2PeriodEnd09() As ColField(Of Date)
        Get
            Set2PeriodEnd09 = mSet2PeriodEnd09
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd09 = value
        End Set
    End Property
    Public Property Set2PeriodEnd10() As ColField(Of Date)
        Get
            Set2PeriodEnd10 = mSet2PeriodEnd10
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd10 = value
        End Set
    End Property
    Public Property Set2PeriodEnd11() As ColField(Of Date)
        Get
            Set2PeriodEnd11 = mSet2PeriodEnd11
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd11 = value
        End Set
    End Property
    Public Property Set2PeriodEnd12() As ColField(Of Date)
        Get
            Set2PeriodEnd12 = mSet2PeriodEnd12
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd12 = value
        End Set
    End Property
    Public Property Set2PeriodEnd13() As ColField(Of Date)
        Get
            Set2PeriodEnd13 = mSet2PeriodEnd13
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd13 = value
        End Set
    End Property
    Public Property Set2PeriodEnd14() As ColField(Of Date)
        Get
            Set2PeriodEnd14 = mSet2PeriodEnd14
        End Get
        Set(ByVal value As ColField(Of Date))
            mSet2PeriodEnd14 = value
        End Set
    End Property
    Public Property Set2YearEnd01() As ColField(Of Boolean)
        Get
            Set2YearEnd01 = mSet2YearEnd01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd01 = value
        End Set
    End Property
    Public Property Set2YearEnd02() As ColField(Of Boolean)
        Get
            Set2YearEnd02 = mSet2YearEnd02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd02 = value
        End Set
    End Property
    Public Property Set2YearEnd03() As ColField(Of Boolean)
        Get
            Set2YearEnd03 = mSet2YearEnd03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd03 = value
        End Set
    End Property
    Public Property Set2YearEnd04() As ColField(Of Boolean)
        Get
            Set2YearEnd04 = mSet2YearEnd04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd04 = value
        End Set
    End Property
    Public Property Set2YearEnd05() As ColField(Of Boolean)
        Get
            Set2YearEnd05 = mSet2YearEnd05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd05 = value
        End Set
    End Property
    Public Property Set2YearEnd06() As ColField(Of Boolean)
        Get
            Set2YearEnd06 = mSet2YearEnd06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd06 = value
        End Set
    End Property
    Public Property Set2YearEnd07() As ColField(Of Boolean)
        Get
            Set2YearEnd07 = mSet2YearEnd07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd07 = value
        End Set
    End Property
    Public Property Set2YearEnd08() As ColField(Of Boolean)
        Get
            Set2YearEnd08 = mSet2YearEnd08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd08 = value
        End Set
    End Property
    Public Property Set2YearEnd09() As ColField(Of Boolean)
        Get
            Set2YearEnd09 = mSet2YearEnd09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd09 = value
        End Set
    End Property
    Public Property Set2YearEnd10() As ColField(Of Boolean)
        Get
            Set2YearEnd10 = mSet2YearEnd10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd10 = value
        End Set
    End Property
    Public Property Set2YearEnd11() As ColField(Of Boolean)
        Get
            Set2YearEnd11 = mSet2YearEnd11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd11 = value
        End Set
    End Property
    Public Property Set2YearEnd12() As ColField(Of Boolean)
        Get
            Set2YearEnd12 = mSet2YearEnd12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd12 = value
        End Set
    End Property
    Public Property Set2YearEnd13() As ColField(Of Boolean)
        Get
            Set2YearEnd13 = mSet2YearEnd13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd13 = value
        End Set
    End Property
    Public Property Set2YearEnd14() As ColField(Of Boolean)
        Get
            Set2YearEnd14 = mSet2YearEnd14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2YearEnd14 = value
        End Set
    End Property
    Public Property Set2Processed01() As ColField(Of Boolean)
        Get
            Set2Processed01 = mSet2Processed01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed01 = value
        End Set
    End Property
    Public Property Set2Processed02() As ColField(Of Boolean)
        Get
            Set2Processed02 = mSet2Processed02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed02 = value
        End Set
    End Property
    Public Property Set2Processed03() As ColField(Of Boolean)
        Get
            Set2Processed03 = mSet2Processed03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed03 = value
        End Set
    End Property
    Public Property Set2Processed04() As ColField(Of Boolean)
        Get
            Set2Processed04 = mSet2Processed04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed04 = value
        End Set
    End Property
    Public Property Set2Processed05() As ColField(Of Boolean)
        Get
            Set2Processed05 = mSet2Processed05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed05 = value
        End Set
    End Property
    Public Property Set2Processed06() As ColField(Of Boolean)
        Get
            Set2Processed06 = mSet2Processed06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed06 = value
        End Set
    End Property
    Public Property Set2Processed07() As ColField(Of Boolean)
        Get
            Set2Processed07 = mSet2Processed07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed07 = value
        End Set
    End Property
    Public Property Set2Processed08() As ColField(Of Boolean)
        Get
            Set2Processed08 = mSet2Processed08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed08 = value
        End Set
    End Property
    Public Property Set2Processed09() As ColField(Of Boolean)
        Get
            Set2Processed09 = mSet2Processed09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed09 = value
        End Set
    End Property
    Public Property Set2Processed10() As ColField(Of Boolean)
        Get
            Set2Processed10 = mSet2Processed10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed10 = value
        End Set
    End Property
    Public Property Set2Processed11() As ColField(Of Boolean)
        Get
            Set2Processed11 = mSet2Processed11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed11 = value
        End Set
    End Property
    Public Property Set2Processed12() As ColField(Of Boolean)
        Get
            Set2Processed12 = mSet2Processed12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed12 = value
        End Set
    End Property
    Public Property Set2Processed13() As ColField(Of Boolean)
        Get
            Set2Processed13 = mSet2Processed13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed13 = value
        End Set
    End Property
    Public Property Set2Processed14() As ColField(Of Boolean)
        Get
            Set2Processed14 = mSet2Processed14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSet2Processed14 = value
        End Set
    End Property
    Public Property CurrentPeriodYearEndDate() As ColField(Of Date)
        Get
            CurrentPeriodYearEndDate = _CurrentPeriodYearEndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _CurrentPeriodYearEndDate = value
        End Set
    End Property
    Public Property PriorPeriodYearEndDate() As ColField(Of Date)
        Get
            PriorPeriodYearEndDate = _PriorPeriodYearEndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _PriorPeriodYearEndDate = value
        End Set
    End Property
    Public Property CurrentEndIsYearEnd() As ColField(Of Boolean)
        Get
            CurrentEndIsYearEnd = _CurrentEndIsYearEnd
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CurrentEndIsYearEnd = value
        End Set
    End Property
    Public Property PeriodEndandNotProcessed() As ColField(Of Boolean)
        Get
            PeriodEndandNotProcessed = _PeriodEndandNotProcessed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PeriodEndandNotProcessed = value
        End Set
    End Property
    Public Property WeekEndandNotProcessed() As ColField(Of Boolean)
        Get
            WeekEndandNotProcessed = _WeekEndandNotProcessed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _WeekEndandNotProcessed = value
        End Set
    End Property
    Public Property NetSetNo() As ColField(Of String)
        Get
            NetSetNo = _NetSetNo
        End Get
        Set(ByVal value As ColField(Of String))
            _NetSetNo = value
        End Set
    End Property
    Public Property NightSetNo() As ColField(Of String)
        Get
            NightSetNo = _NightSetNo
        End Get
        Set(ByVal value As ColField(Of String))
            _NightSetNo = value
        End Set
    End Property
    Public Property RetryMode() As ColField(Of Boolean)
        Get
            RetryMode = _RetryMode
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RetryMode = value
        End Set
    End Property
    Public Property WeeksInCycle() As ColField(Of Decimal)
        Get
            WeeksInCycle = _WeeksInCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _WeeksInCycle = value
        End Set
    End Property
    Public Property WeekCycleNumber() As ColField(Of Decimal)
        Get
            WeekCycleNumber = _WeekCycleNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _WeekCycleNumber = value
        End Set
    End Property
    Public Property AuditDate() As ColField(Of Date)
        Get
            AuditDate = _AuditDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AuditDate = value
        End Set
    End Property
    Public Property DaysPriorToPriceChange() As ColField(Of Decimal)
        Get
            DaysPriorToPriceChange = _DaysPriorToPriceChange
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DaysPriorToPriceChange = value
        End Set
    End Property
    Public Property LastNightlyCloseDate() As ColField(Of Date)
        Get
            LastNightlyCloseDate = _LastNightlyCloseDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastNightlyCloseDate = value
        End Set
    End Property


#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads system dates into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks>Throws Oasys exception on error</remarks>
    Public Sub Load()

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, "01")
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysDatesLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads dates required for soq into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadSoqDates()

        Try
            ClearLists()
            AddLoadField(_DaysOpen)
            AddLoadField(_WeekCycleNumber)
            AddLoadField(_WeeksInCycle)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, "01")
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysDatesLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns current system trading date.
    '''  Throws Oasys exception on error or if no date found
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCurrentTradingDate() As Date

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_Tomorrow.ColumnName)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, "01")
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If Not IsDBNull(dt.Rows(0)(0)) Then
                Return CDate(dt.Rows(0)(0))
            End If

            Throw New OasysDbException(My.Resources.Errors.SysDatesNoDateFound)

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SysDatesGetCurrentTrading, ex)
        End Try

    End Function

    Public Function SystemDates() As DataRow
        Dim ds As DataSet

        Me.ClearLoadFilter()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.SystemDatesID, "01")
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            End If
        End If
        Return Nothing

    End Function




    'Public Function DaysStoreOpen() As Integer
    '    ClearLoadField()
    '    ClearLoadFilter()

    '    AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, mSystemDatesID, "01")
    '    LoadMatches()

    '    Return mDaysOpen.Value

    'End Function

    Public Function GetNextOpenDay(ByVal StartDate As Date, ByRef ErrorMessage As String) As Date
        Dim strDateCheckFile As String = String.Empty
        Dim SystemDates As DataRow
        Dim strDateCheckData As String
        Dim strDateDdMmYy As String
        Dim dteNextDateOpen As Date
        Dim intDayofWeek As Integer
        Dim blnStoreOpen As Boolean
        Dim parmtable As New BOSystem.cParameter
        Const PRM_DATECHECK_FILE As Long = 143
        Dim rdr As IO.StreamReader
        ' get file of closed dates 
        ' find file name and path from parameter file

        Try
            dteNextDateOpen = StartDate
            parmtable.ConnectionString = Me.ConnectionString
            parmtable.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, parmtable.ParameterID, PRM_DATECHECK_FILE)
            parmtable.LoadMatches()
            strDateCheckFile = parmtable.StringValue.Value

            If strDateCheckFile = "" Then
                'default to c:\holidays.txt
                strDateCheckFile = "c:\holidays.txt"
            End If

            Trace.WriteLine("File to read = " & strDateCheckFile, Me.GetType.ToString & "GetNextOpenDay")

            '' open the date check file
            rdr = IO.File.OpenText(strDateCheckFile.ToString)
            '' read the data from the date check file and create a string
            ''   e.g.    24/12/02,25/12/02,26/12/02,01/01/03
            strDateCheckData = ""
            While Not rdr.EndOfStream
                strDateCheckData = strDateCheckData & "," & rdr.ReadLine
            End While
            rdr.Close()

            SystemDates = Me.SystemDates()
            If SystemDates IsNot Nothing Then

                Do
                    dteNextDateOpen = DateAdd("d", 1, dteNextDateOpen)
                    ' first day of week is Monday (by definition)
                    intDayofWeek = Weekday(dteNextDateOpen, vbMonday)
                    If intDayofWeek > CInt(SystemDates.Item(Me.DaysOpen.ColumnName)) Then
                        blnStoreOpen = False
                    Else
                        ' check this is not a holiday
                        strDateDdMmYy = Format(dteNextDateOpen, "dd/MM/yy")
                        If InStr(strDateCheckData, strDateDdMmYy) > 0 Then
                            blnStoreOpen = False
                        Else
                            blnStoreOpen = True
                        End If
                    End If

                Loop Until blnStoreOpen = True

                GetNextOpenDay = dteNextDateOpen
                'Else
                '    ErrorMessage = "Error - number of open days not set in SysDat"
                '    'As an error has occured force date to zero
                '    GetNextOpenDay = CDate(#12:00:00 AM#)
                '    Exit Function
                'End If
            Else
                ErrorMessage = "Path and name of the date check file missing -" & vbCrLf & _
                             "unable to increment date to the next working day" & vbCrLf & vbCrLf & _
                             "(parameter number " & "143" & " is not set.)"
                'As an error has occured force date to zero
                GetNextOpenDay = CDate(#12:00:00 AM#)
                Exit Function
            End If

        Catch ex As Exception
            ErrorMessage = ex.ToString
            Trace.WriteLine(ErrorMessage)
        End Try

    End Function

    Public Function StocklossCycleWeekRatio() As Decimal
        ClearLoadField()
        ClearLoadFilter()

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, "01")
        LoadMatches()

        Return _WeekCycleNumber.Value / (_WeeksInCycle.Value * _WeeksInCycle.Value)

    End Function

    Public Function UpdateWeekEnd(ByVal WeekEndDates() As Date, ByVal CurrentWeekNoInCycle As Integer, ByVal WEandNotProcessed As Boolean) As Boolean
        Dim SaveError As Boolean
        Dim norecs As Integer

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        Oasys3DB.SetColumnAndValueParameter(mWeekEnding01.ColumnName, WeekEndDates(0))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding02.ColumnName, WeekEndDates(1))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding03.ColumnName, WeekEndDates(2))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding04.ColumnName, WeekEndDates(3))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding05.ColumnName, WeekEndDates(4))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding06.ColumnName, WeekEndDates(5))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding07.ColumnName, WeekEndDates(6))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding08.ColumnName, WeekEndDates(7))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding09.ColumnName, WeekEndDates(8))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding10.ColumnName, WeekEndDates(9))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding11.ColumnName, WeekEndDates(10))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding12.ColumnName, WeekEndDates(11))
        Oasys3DB.SetColumnAndValueParameter(mWeekEnding13.ColumnName, WeekEndDates(12))
        Oasys3DB.SetColumnAndValueParameter(_WeekCycleNumber.ColumnName, CurrentWeekNoInCycle)
        Oasys3DB.SetColumnAndValueParameter(_WeekEndandNotProcessed.ColumnName, WEandNotProcessed)

        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

        norecs = Oasys3DB.Update()

        If norecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError


    End Function

    Public Function UpdateNightTaskOK() As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_NightSetNo.ColumnName, "000")
        Oasys3DB.SetColumnAndValueParameter(_RetryMode.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_PeriodEndandNotProcessed.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(_WeekEndandNotProcessed.ColumnName, False)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
        Return (Oasys3DB.Update > 0)

    End Function

    Public Function UpdateNightTaskNo(ByVal TaskNo As Integer) As Boolean
        Dim SaveError As Boolean
        Dim norecs As Integer

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        'move data to record
        Oasys3DB.SetColumnAndValueParameter(_NightSetNo.ColumnName, Format(TaskNo, "000"))

        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

        norecs = Oasys3DB.Update()

        If norecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function UpdateCurrentWeekNumber() As Boolean

        WeekCycleNumber.Value += 1
        If WeekCycleNumber.Value >= WeeksInCycle.Value Then
            WeekCycleNumber.Value = 0
        End If

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(WeekCycleNumber.ColumnName, WeekCycleNumber.Value)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function SetOnRetry() As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(RetryMode.ColumnName, True)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
        Return (Oasys3DB.Update() > 0)

    End Function

    Public Function SetOffRetry() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(RetryMode.ColumnName, False)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine("cSystemDates - SetRetry exception" & ex.Message)
            Throw ex
            Return False
        End Try

    End Function

    Public Function rolldate() As Boolean
        ' move the Tomorrows date to todays and move the tomorrows code to today.
        ' add 1 to tomorrows date and see if it is greater than end of week if true say tommorrow to start of the week
        ' find if there are any days to be excluded if so reset tommorrow.
        ' if today is end of week seton end of week 
        ' if today is end of month seton end of month
        Dim ErrorMessage As String = String.Empty
        Dim drSystemDates As DataRow = SystemDates()
        Dim week01 As Date = CDate(drSystemDates.Item(WeekEnding01.ColumnName))
        Dim week02 As Date = CDate(drSystemDates.Item(WeekEnding02.ColumnName))
        Dim week03 As Date = CDate(drSystemDates.Item(WeekEnding03.ColumnName))
        Dim week04 As Date = CDate(drSystemDates.Item(WeekEnding04.ColumnName))
        Dim week05 As Date = CDate(drSystemDates.Item(WeekEnding05.ColumnName))
        Dim week06 As Date = CDate(drSystemDates.Item(WeekEnding06.ColumnName))
        Dim week07 As Date = CDate(drSystemDates.Item(WeekEnding07.ColumnName))
        Dim week08 As Date = CDate(drSystemDates.Item(WeekEnding08.ColumnName))
        Dim week09 As Date = CDate(drSystemDates.Item(WeekEnding09.ColumnName))
        Dim week10 As Date = CDate(drSystemDates.Item(WeekEnding10.ColumnName))
        Dim week11 As Date = CDate(drSystemDates.Item(WeekEnding11.ColumnName))
        Dim week12 As Date = CDate(drSystemDates.Item(WeekEnding12.ColumnName))
        Dim week13 As Date = CDate(drSystemDates.Item(WeekEnding13.ColumnName))
        Dim currentPeriodEnd As Date = CDate(drSystemDates.Item(CurrentPeriodYearEndDate.ColumnName))

        'NB Today is really Yesterday and Tommorrow is really today
        Dim Today As Date = CDate(drSystemDates.Item(_Tomorrow.ColumnName))
        Dim todayCode As Integer = CInt(drSystemDates.Item(_TomorrowDayCode.ColumnName))
        Dim tomorrow As Date
        Dim tomorrowCode As Integer
        Dim weekfromtoday As Date = DateAdd(DateInterval.Day, 7, Today)

        ' find the next date for tommorrow now checking for holidays 
        Dim nextOpen As Date = GetNextOpenDay(Today, ErrorMessage)
        If ErrorMessage <> String.Empty Then
            ' error occured
            Trace.WriteLine("cSystemDates - GetNextOpenDay" & ErrorMessage)
            Dim exc As Exception = New Exception("cSystemDates - GetNextOpenDay" & ErrorMessage)
            Throw exc
        Else
            If nextOpen <> tomorrow Then
                tomorrow = nextOpen
                tomorrowCode = Weekday(nextOpen, vbMonday)
            End If
        End If

        ' now work out if we have a week end and move weeks back one if so and update cycle week number
        Dim weekEnd As Boolean = False
        Dim cycleWeek As Integer = CInt(drSystemDates.Item(WeekCycleNumber.ColumnName))
        If todayCode = CInt(drSystemDates.Item(EndofWeekDay.ColumnName)) Then
            weekEnd = True
            week13 = week12
            week12 = week11
            week11 = week10
            week10 = week09
            week09 = week08
            week08 = week07
            week07 = week06
            week06 = week05
            week05 = week04
            week04 = week03
            week03 = week02
            week02 = week01
            week01 = Today

            cycleWeek += 1
            If cycleWeek >= CInt(drSystemDates.Item(WeeksInCycle.ColumnName)) Then
                cycleWeek = 0
            End If
        End If

        ' now work out if we have a period end date 
        Dim lstPeriodEnd As New List(Of Date)
        Dim lstYearEnd As New List(Of Boolean)
        If CInt(drSystemDates.Item(UseSet.ColumnName)) = 1 Then
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd01.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd02.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd03.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd04.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd05.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd06.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd07.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd08.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd09.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd10.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd11.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd12.ColumnName)))

            If Not IsDBNull(drSystemDates.Item(Set1PeriodEnd13.ColumnName)) Then
                lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd13.ColumnName)))
            End If

            If Not IsDBNull(drSystemDates.Item(Set1PeriodEnd14.ColumnName)) Then
                lstPeriodEnd.Add(CDate(drSystemDates.Item(Set1PeriodEnd14.ColumnName)))
            End If

            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd01.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd02.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd03.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd04.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd05.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd06.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd07.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd08.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd09.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd10.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd11.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd12.ColumnName)))
            If Not IsDBNull(drSystemDates.Item(Set1YearEnd13.ColumnName)) Then
                lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd13.ColumnName)))
            End If
            If Not IsDBNull(drSystemDates.Item(Set1YearEnd14.ColumnName)) Then
                lstYearEnd.Add(CBool(drSystemDates.Item(Set1YearEnd14.ColumnName)))
            End If

        Else
            ' use set two 
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd01.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd02.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd03.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd04.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd05.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd06.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd07.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd08.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd09.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd10.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd11.ColumnName)))
            lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd12.ColumnName)))

            If Not IsDBNull(drSystemDates.Item(Set2PeriodEnd13.ColumnName)) Then
                lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd13.ColumnName)))
            End If

            If Not IsDBNull(drSystemDates.Item(Set2PeriodEnd14.ColumnName)) Then
                lstPeriodEnd.Add(CDate(drSystemDates.Item(Set2PeriodEnd14.ColumnName)))
            End If

            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd01.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd02.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd03.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd04.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd05.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd06.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd07.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd08.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd09.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd10.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd11.ColumnName)))
            lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd12.ColumnName)))

            If Not IsDBNull(drSystemDates.Item(Set2YearEnd13.ColumnName)) Then
                lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd13.ColumnName)))
            End If

            If Not IsDBNull(drSystemDates.Item(Set2YearEnd14.ColumnName)) Then
                lstYearEnd.Add(CBool(drSystemDates.Item(Set2YearEnd14.ColumnName)))
            End If
        End If

        Dim yearEnd As Boolean
        Dim nextSet As Integer = CInt(drSystemDates.Item(UseSet.ColumnName))
        Dim periodfound As Boolean = lstPeriodEnd.Contains(Today)
        If periodfound Then

            Dim index As Integer = lstPeriodEnd.IndexOf(Today)
            currentPeriodEnd = Today

            ' now work out if we have a year end date 
            If lstYearEnd.Item(index) = True Then
                yearEnd = True
                nextSet = nextSet + 1
                If nextSet = 3 Then nextSet = 1
            End If
        End If

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_Today.ColumnName, Today)
            Oasys3DB.SetColumnAndValueParameter(_TodayDayCode.ColumnName, todayCode)
            Oasys3DB.SetColumnAndValueParameter(_Tomorrow.ColumnName, tomorrow)
            Oasys3DB.SetColumnAndValueParameter(_TomorrowDayCode.ColumnName, tomorrowCode)
            Oasys3DB.SetColumnAndValueParameter(mWeekFromToday.ColumnName, weekfromtoday)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding01.ColumnName, week01)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding02.ColumnName, week02)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding03.ColumnName, week03)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding04.ColumnName, week04)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding05.ColumnName, week05)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding06.ColumnName, week06)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding07.ColumnName, week07)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding08.ColumnName, week08)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding09.ColumnName, week09)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding10.ColumnName, week10)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding11.ColumnName, week11)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding12.ColumnName, week12)
            Oasys3DB.SetColumnAndValueParameter(mWeekEnding13.ColumnName, week13)
            Oasys3DB.SetColumnAndValueParameter(_WeekEndandNotProcessed.ColumnName, weekEnd)
            Oasys3DB.SetColumnAndValueParameter(_PeriodEndandNotProcessed.ColumnName, periodfound)
            Oasys3DB.SetColumnAndValueParameter(_CurrentEndIsYearEnd.ColumnName, yearEnd)
            Oasys3DB.SetColumnAndValueParameter(_CurrentPeriodYearEndDate.ColumnName, currentPeriodEnd)
            Oasys3DB.SetColumnAndValueParameter(_WeekCycleNumber.ColumnName, cycleWeek)
            Oasys3DB.SetColumnAndValueParameter(_UseSet.ColumnName, nextSet)

            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine("cSystemDates - SetRetry exception" & ex.Message)
            Throw
            Return False
        End Try

    End Function

    Public Function UpdateLstNiteClsDate(ByVal closeDate As Date) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_LastNightlyCloseDate.ColumnName, closeDate)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine("cSystemDates - UpdateLstNiteClsDate exception" & ex.Message)
            Throw ex
            Return False
        End Try
    End Function

#End Region

End Class
