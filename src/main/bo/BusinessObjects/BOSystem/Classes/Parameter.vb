﻿Namespace TpWickes

    Public Class Parameter
        Inherits cParameter
        Implements IParameter

        Public Sub New()
            MyBase.New()
        End Sub

        'Public Sub New(ByVal strConnection As String)
        '    MyBase.New(strConnection)
        'End Sub

        Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
            MyBase.New(oasys3DB)
        End Sub

        Public Overrides Function GetParameterString(ByVal ParameterID As Integer) As String
            Dim Repository As TpWickes.IParameterRepository

            Repository = ParameterRepositoryFactory.FactoryGet
            Return Repository.GetParameterString(ParameterID)

        End Function

    End Class

End Namespace