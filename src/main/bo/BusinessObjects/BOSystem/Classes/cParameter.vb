<Serializable()> Public Class cParameter
    Inherits cBaseClass

    Implements TpWickes.IParameter

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start() Implements TpWickes.IParameter.Start

        TableName = "Parameters"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOFields.Add(_StringValue)
        BOFields.Add(_LongValue)
        BOFields.Add(_BooleanValue)
        BOFields.Add(_DecimalValue)
        BOFields.Add(_ValueType)

    End Sub

    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cParameter) Implements TpWickes.IParameter.LoadMatches

        Try
            LoadBORecords(count)

            Dim col As New List(Of cParameter)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cParameter))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1) Implements TpWickes.IParameter.LoadBORecords

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cParameter(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ParameterID", 0, "ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _StringValue As New ColField(Of String)("StringValue", "", "String Value", False, False)
    Private _LongValue As New ColField(Of Integer)("LongValue", 0, "Long Value", False, False)
    Private _BooleanValue As New ColField(Of Boolean)("BooleanValue", False, "Boolean Value", False, False)
    Private _DecimalValue As New ColField(Of Decimal)("DecimalValue", 0, "Decimal Value", False, False)
    Private _ValueType As New ColField(Of Integer)("ValueType", 0, "Value Type", False, False)

#End Region

#Region "Field Properties"

    Public Property ParameterID() As ColField(Of Integer) Implements TpWickes.IParameter.ParameterID
        Get
            ParameterID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property

    Public Property Description() As ColField(Of String) Implements TpWickes.IParameter.Description
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property

    Public Property StringValue() As ColField(Of String) Implements TpWickes.IParameter.StringValue
        Get
            StringValue = _StringValue
        End Get
        Set(ByVal value As ColField(Of String))
            _StringValue = value
        End Set
    End Property

    Public Property LongValue() As ColField(Of Integer) Implements TpWickes.IParameter.LongValue
        Get
            LongValue = _LongValue
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LongValue = value
        End Set
    End Property

    Public Property BooleanValue() As ColField(Of Boolean) Implements TpWickes.IParameter.BooleanValue

        Get
            BooleanValue = _BooleanValue
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _BooleanValue = value
        End Set
    End Property

    Public Property DecimalValue() As ColField(Of Decimal) Implements TpWickes.IParameter.DecimalValue
        Get
            DecimalValue = _DecimalValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DecimalValue = value
        End Set
    End Property

    Public Property ValueType() As ColField(Of Integer) Implements TpWickes.IParameter.ValueType
        Get
            ValueType = _ValueType
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ValueType = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Enum Type
        eString
        eLong
        eDouble
        eBoolean
        eColour
    End Enum

    Private _Parameters As New List(Of cParameter)

    Public ReadOnly Property Parameter(ByVal ID As Integer) As cParameter Implements TpWickes.IParameter.Parameter
        Get
            For Each p As cParameter In _Parameters
                If p.ParameterID.Value = ID Then Return p
            Next
            Return New cParameter(Oasys3DB)
        End Get
    End Property

    Public Property Parameters() As List(Of cParameter) Implements TpWickes.IParameter.Parameters
        Get
            Return _Parameters
        End Get
        Set(ByVal value As List(Of cParameter))
            _Parameters = value
        End Set
    End Property

#Region "Interface Not Implemented"

    ''' <summary>
    ''' Structure of row fore colours from parameter table
    ''' </summary>
    ''' <remarks></remarks>
    Public Structure RowForeColour
        Public Normal As Drawing.Color
        Public Add As Drawing.Color
        Public Changed As Drawing.Color
        Public ChangedDelete As Drawing.Color
        Public Delete As Drawing.Color
        Public Sub New(ByVal normal As String, ByVal add As String, ByVal changed As String, ByVal delete As String, ByVal changeDelete As String)
            Me.Normal = Drawing.ColorTranslator.FromHtml(normal)
            Me.Add = Drawing.ColorTranslator.FromHtml(add)
            Me.Changed = Drawing.ColorTranslator.FromHtml(changed)
            Me.Delete = Drawing.ColorTranslator.FromHtml(delete)
            Me.ChangedDelete = Drawing.ColorTranslator.FromHtml(changeDelete)
        End Sub
    End Structure

    ''' <summary>
    ''' Returns row fore colour structure loaded from parameter table. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetRowForeColours() As RowForeColour

        Try
            'get datatable of values required
            Dim p As New cParameter(Oasys3DB)
            Me.ClearLoadFilter()
            p.AddLoadField(p.ParameterID)
            p.AddLoadField(p.StringValue)
            p.AddLoadFilter(clsOasys3DB.eOperator.pIn, _ID, New Integer() {1000, 1001, 1002, 1003, 1004})
            Dim dt As DataTable = p.GetSQLSelectDataSet.Tables(0)

            Dim normal As String = String.Empty
            Dim add As String = String.Empty
            Dim changed As String = String.Empty
            Dim delete As String = String.Empty
            Dim changedDelete As String = String.Empty

            For Each dr As DataRow In dt.Rows
                If CInt(dr(0)) = 1000 Then normal = CStr(dr(1))
                If CInt(dr(0)) = 1001 Then add = CStr(dr(1))
                If CInt(dr(0)) = 1002 Then changed = CStr(dr(1))
                If CInt(dr(0)) = 1003 Then delete = CStr(dr(1))
                If CInt(dr(0)) = 1004 Then changedDelete = CStr(dr(1))
            Next

            Return New RowForeColour(normal, add, changed, delete, changedDelete)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

    ''' <summary>
    ''' Structure of row back colours from parameter table
    ''' </summary>
    ''' <remarks></remarks>
    Public Structure RowBackColour
        Public Normal As Drawing.Color
        Public Selection As Drawing.Color
        Public Alternate As Drawing.Color
        Public [Error] As Drawing.Color
        Public Total As Drawing.Color
        Public Sub New(ByVal normal As String, ByVal selection As String, ByVal alternate As String, ByVal [error] As String, ByVal total As String)
            Me.Normal = Drawing.ColorTranslator.FromHtml(normal)
            Me.Selection = Drawing.ColorTranslator.FromHtml(selection)
            Me.Alternate = Drawing.ColorTranslator.FromHtml(alternate)
            Me.Error = Drawing.ColorTranslator.FromHtml([error])
            Me.Total = Drawing.ColorTranslator.FromHtml(total)
        End Sub
    End Structure

    ''' <summary>
    ''' Returns row back colour structure loaded from parameter table. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetRowBackColours() As RowBackColour

        Try
            'get datatable of values required
            Dim p As New cParameter(Oasys3DB)
            Me.ClearLoadFilter()
            p.AddLoadField(p.ParameterID)
            p.AddLoadField(p.StringValue)
            p.AddLoadFilter(clsOasys3DB.eOperator.pIn, _ID, New Integer() {1100, 1101, 1102, 1103, 1104})
            Dim dt As DataTable = p.GetSQLSelectDataSet.Tables(0)

            Dim normal As String = String.Empty
            Dim selection As String = String.Empty
            Dim alternate As String = String.Empty
            Dim [error] As String = String.Empty
            Dim total As String = String.Empty

            For Each dr As DataRow In dt.Rows
                If CInt(dr(0)) = 1100 Then normal = CStr(dr(1))
                If CInt(dr(0)) = 1101 Then selection = CStr(dr(1))
                If CInt(dr(0)) = 1102 Then alternate = CStr(dr(1))
                If CInt(dr(0)) = 1103 Then [error] = CStr(dr(1))
                If CInt(dr(0)) = 1104 Then total = CStr(dr(1))
            Next

            Return New RowBackColour(normal, selection, alternate, [error], total)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

#End Region

    Public Function ParameterRow(ByVal ParameterId As Integer) As DataRow Implements TpWickes.IParameter.ParameterRow
        Dim ds As DataSet

        Me.ClearLoadFilter()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.ParameterID, ParameterId)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            End If
        End If
        Return Nothing

    End Function

    Public Sub GetParameter(ByVal id As Long, ByRef ReturnString As String, ByRef RaiseError As Boolean) Implements TpWickes.IParameter.GetParameter
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, CInt(id))
        If LoadMatches.Count > 0 Then

            If _ValueType.Value <> Type.eString Then
                RaiseError = False
                Throw New ApplicationException("Invalid Parameter Type")
            End If
            ReturnString = _StringValue.Value
        Else
            RaiseError = False
            Throw New ApplicationException("Record not found")
        End If
    End Sub

    Public Sub GetParameter(ByVal id As Long, ByRef ReturnNo As Decimal, ByRef RaiseError As Boolean) Implements TpWickes.IParameter.GetParameter
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, CInt(id))
        If LoadMatches.Count > 0 Then

            If _ValueType.Value <> Type.eDouble And ValueType.Value <> Type.eLong Then
                RaiseError = False
                Throw New ApplicationException("Invalid Parameter Type")
            End If
            If _ValueType.Value = Type.eLong Then
                ReturnNo = _LongValue.Value
            Else
                ReturnNo = _DecimalValue.Value
            End If
        Else
            RaiseError = False
            Throw New ApplicationException("Record not found")
        End If
    End Sub

    Public Sub GetParameter(ByVal id As Long, ByRef ReturnNo As Integer, ByRef RaiseError As Boolean) Implements TpWickes.IParameter.GetParameter
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, CInt(id))
        If LoadMatches.Count > 0 Then

            If _ValueType.Value <> Type.eDouble And ValueType.Value <> Type.eLong Then
                RaiseError = False
                Throw New ApplicationException("Invalid Parameter Type")
            End If
            If _ValueType.Value = Type.eLong Then
                ReturnNo = _LongValue.Value
            Else
                ReturnNo = CInt(_DecimalValue.Value)
            End If
        Else
            RaiseError = False
            Throw New ApplicationException("Record not found")
        End If
    End Sub

    Public Sub GetParameter(ByVal id As Long, ByRef ReturnBool As Boolean, ByRef RaiseError As Boolean) Implements TpWickes.IParameter.GetParameter
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, CInt(id))
        If LoadMatches.Count > 0 Then

            If _ValueType.Value <> Type.eBoolean Then
                RaiseError = False
                Throw New ApplicationException("Invalid Parameter Type")
            End If
            ReturnBool = _BooleanValue.Value
        Else
            RaiseError = False
            Throw New ApplicationException("Record not found")
        End If
    End Sub

    ''' <summary>
    ''' returns string parameter for given ID. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="ParameterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Function GetParameterString(ByVal ParameterID As Integer) As String Implements TpWickes.IParameter.GetParameterString

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ParameterID)

            If LoadMatches.Count > 0 Then
                Return _StringValue.Value
            End If
            Throw New ApplicationException(My.Resources.Errors.RecordNotFound)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

    ''' <summary>
    ''' returns integer parameter for given ID. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="ParameterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetParameterInteger(ByVal ParameterID As Integer) As Integer Implements TpWickes.IParameter.GetParameterInteger

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ParameterID)

            If LoadMatches.Count > 0 Then
                Return _LongValue.Value
            End If
            Throw New ApplicationException(My.Resources.Errors.RecordNotFound)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

    ''' <summary>
    ''' returns decimal parameter for given ID. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="ParameterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetParameterDecimal(ByVal ParameterID As Integer) As Decimal Implements TpWickes.IParameter.GetParameterDecimal

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ParameterID)

            If LoadMatches.Count > 0 Then
                Return _DecimalValue.Value
            End If
            Throw New ApplicationException(My.Resources.Errors.RecordNotFound)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

    ''' <summary>
    ''' returns boolean parameter for given ID. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="ParameterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetParameterBoolean(ByVal ParameterID As Integer) As Boolean Implements TpWickes.IParameter.GetParameterBoolean

        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ParameterID)

            If LoadMatches.Count > 0 Then
                Return _BooleanValue.Value
            End If
            Throw New ApplicationException(My.Resources.Errors.RecordNotFound)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet)
        End Try

    End Function

    ''' <summary>
    ''' returns boolean parameter for given ID. If error occures, then return default value
    ''' </summary>
    ''' <param name="ParameterID"></param>
    ''' <param name="DefaultValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetParameterBooleanWithDefault(ByVal ParameterID As Integer, ByVal DefaultValue As Boolean) As Boolean Implements TpWickes.IParameter.GetParameterBooleanWithDefault
        Try
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ParameterID)

            If LoadMatches.Count > 0 Then
                Return _BooleanValue.Value
            End If
            Return DefaultValue

        Catch ex As Exception
            Return DefaultValue
        End Try

    End Function

    ''' <summary>
    ''' Returns authorisation parameters. Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAuthorisationParameters() As Boolean() Implements TpWickes.IParameter.GetAuthorisationParameters

        Try
            Dim auths(15) As Boolean
            Dim params() As Integer = {2100, 2110, 2111, 2112, 2120, 2121, 2122, 2130, 2131, 2132, 2140, 2141, 2142, 2150, 2151, 2152}

            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pIn, _ID, params)
            _Parameters = LoadMatches()

            For Each parameter As BOSystem.cParameter In _Parameters
                For index As Integer = 0 To params.Length - 1
                    If params(index) = parameter.ParameterID.Value Then
                        auths(index) = parameter.BooleanValue.Value
                        Exit For
                    End If
                Next
            Next

            Return auths

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ParametersGet, ex)
        End Try

    End Function

#End Region

End Class