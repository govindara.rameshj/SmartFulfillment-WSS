﻿Namespace TpWickes

    Public Class ParameterFactory

        Private Shared m_FactoryMember As IParameter

        Public Shared Function FactoryGet(ByRef oasys3DB As Oasys3.DB.clsOasys3DB) As IParameter

            If m_FactoryMember Is Nothing Then
                Return New cParameter(oasys3DB)          'live implementation: existing
            Else
                Return m_FactoryMember                   'stub implementation
            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As TpWickes.IParameter)
            m_FactoryMember = obj
        End Sub

    End Class

End Namespace