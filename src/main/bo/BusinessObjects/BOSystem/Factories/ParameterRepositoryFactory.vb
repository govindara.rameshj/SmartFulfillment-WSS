﻿Namespace TpWickes

    Public Class ParameterRepositoryFactory

        Private Shared m_FactoryMember As IParameterRepository

        Public Shared Function FactoryGet() As IParameterRepository

            If m_FactoryMember Is Nothing Then
                Return New ParameterRepository               'not implemented
            Else
                Return m_FactoryMember                       'stub implementation
            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IParameterRepository)
            m_FactoryMember = obj
        End Sub

    End Class

End Namespace