﻿Namespace TpWickes

    Public Interface IParameter

        Property BooleanValue() As ColField(Of Boolean)
        Property DecimalValue() As ColField(Of Decimal)
        Property Description() As ColField(Of String)
        Property LongValue() As ColField(Of Integer)
        Property ParameterID() As ColField(Of Integer)
        Property Parameters() As List(Of cParameter)
        Property StringValue() As ColField(Of String)
        Property ValueType() As ColField(Of Integer)

        ReadOnly Property Parameter(ByVal ID As Integer) As cParameter

        Sub GetParameter(ByVal id As Long, ByRef ReturnString As String, ByRef RaiseError As Boolean)
        Sub GetParameter(ByVal id As Long, ByRef ReturnNo As Decimal, ByRef RaiseError As Boolean)
        Sub GetParameter(ByVal id As Long, ByRef ReturnNo As Integer, ByRef RaiseError As Boolean)
        Sub GetParameter(ByVal id As Long, ByRef ReturnBool As Boolean, ByRef RaiseError As Boolean)
        Sub LoadBORecords(Optional ByVal count As Integer = -1)
        Sub Start()

        Function GetParameterString(ByVal ParameterID As Integer) As String
        Function GetParameterInteger(ByVal ParameterID As Integer) As Integer
        Function GetParameterDecimal(ByVal ParameterID As Integer) As Decimal
        Function GetParameterBoolean(ByVal ParameterID As Integer) As Boolean
        Function GetParameterBooleanWithDefault(ByVal ParameterID As Integer, ByVal DefaultValue As Boolean) As Boolean
        Function GetAuthorisationParameters() As Boolean()
        Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cParameter)
        Function ParameterRow(ByVal ParameterId As Integer) As DataRow

    End Interface

End Namespace