﻿Namespace TpWickes

    Public Interface IParameterRepository

        Function GetParameterString(ByVal ParameterID As Integer) As String

    End Interface

End Namespace