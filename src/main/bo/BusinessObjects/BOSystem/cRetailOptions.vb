<Serializable()> Public Class cRetailOptions
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RETOPT"
        BOFields.Add(_ID)
        BOFields.Add(_Store)
        BOFields.Add(_StoreName)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_AccountabilityType)
        BOFields.Add(mHighestValidCashier)
        BOFields.Add(mDefaultFloatAmountAT)
        BOFields.Add(mLstAcctPerRetClosed)
        BOFields.Add(mNextAcctPeriodToOpen)
        BOFields.Add(mAccoutingPerToKeep)
        BOFields.Add(mTypeOfSaleNo01)
        BOFields.Add(mTypeOfSaleNo02)
        BOFields.Add(mTypeOfSaleNo03)
        BOFields.Add(mTypeOfSaleNo04)
        BOFields.Add(mTypeOfSaleNo05)
        BOFields.Add(mTypeOfSaleNo06)
        BOFields.Add(mTypeOfSaleNo07)
        BOFields.Add(mTypeOfSaleNo08)
        BOFields.Add(mTypeOfSaleNo09)
        BOFields.Add(mTypeOfSaleNo10)
        BOFields.Add(mTypeOfSaleNo11)
        BOFields.Add(mTypeOfSaleNo12)
        BOFields.Add(mTypeOfSaleNo13)
        BOFields.Add(mTypeOfSaleNo14)
        BOFields.Add(mTypeOfSaleNo15)
        BOFields.Add(mTypeOfSaleNo16)
        BOFields.Add(mTypeOfSaleNo17)
        BOFields.Add(mTypeOfSaleNo18)
        BOFields.Add(mTypeOfSaleNo19)
        BOFields.Add(mTypeOfSaleNo20)
        BOFields.Add(mTypeOfSaleNo21)
        BOFields.Add(mTypeOfSaleNo22)
        BOFields.Add(mTypeOfSaleNo23)
        BOFields.Add(mTypeOfSaleNo24)
        BOFields.Add(mTypeOfSaleNo25)
        BOFields.Add(mTypeOfSaleNo26)
        BOFields.Add(mTypeOfSaleNo27)
        BOFields.Add(mTypeOfSaleNo28)
        BOFields.Add(mTypeOfSaleNo29)
        BOFields.Add(mTypeOfSaleNo30)
        BOFields.Add(mTypeOfSale01)
        BOFields.Add(mTypeOfSale02)
        BOFields.Add(mTypeOfSale03)
        BOFields.Add(mTypeOfSale04)
        BOFields.Add(mTypeOfSale05)
        BOFields.Add(mTypeOfSale06)
        BOFields.Add(mTypeOfSale07)
        BOFields.Add(mTypeOfSale08)
        BOFields.Add(mTypeOfSale09)
        BOFields.Add(mTypeOfSale10)
        BOFields.Add(mTypeOfSale11)
        BOFields.Add(mTypeOfSale12)
        BOFields.Add(mTypeOfSale13)
        BOFields.Add(mTypeOfSale14)
        BOFields.Add(mTypeOfSale15)
        BOFields.Add(mTypeOfSale16)
        BOFields.Add(mTypeOfSale17)
        BOFields.Add(mTypeOfSale18)
        BOFields.Add(mTypeOfSale19)
        BOFields.Add(mTypeOfSale20)
        BOFields.Add(mTypeOfSale21)
        BOFields.Add(mTypeOfSale22)
        BOFields.Add(mTypeOfSale23)
        BOFields.Add(mTypeOfSale24)
        BOFields.Add(mTypeOfSale25)
        BOFields.Add(mTypeOfSale26)
        BOFields.Add(mTypeOfSale27)
        BOFields.Add(mTypeOfSale28)
        BOFields.Add(mTypeOfSale29)
        BOFields.Add(mTypeOfSale30)
        BOFields.Add(mTypeOfSalesDesc01)
        BOFields.Add(mTypeOfSalesDesc02)
        BOFields.Add(mTypeOfSalesDesc03)
        BOFields.Add(mTypeOfSalesDesc04)
        BOFields.Add(mTypeOfSalesDesc05)
        BOFields.Add(mTypeOfSalesDesc06)
        BOFields.Add(mTypeOfSalesDesc07)
        BOFields.Add(mTypeOfSalesDesc08)
        BOFields.Add(mTypeOfSalesDesc09)
        BOFields.Add(mTypeOfSalesDesc10)
        BOFields.Add(mTypeOfSalesDesc11)
        BOFields.Add(mTypeOfSalesDesc12)
        BOFields.Add(mTypeOfSalesDesc13)
        BOFields.Add(mTypeOfSalesDesc14)
        BOFields.Add(mTypeOfSalesDess15)
        BOFields.Add(mTypeOfSalesDesc16)
        BOFields.Add(mTypeOfSalesDesc17)
        BOFields.Add(mTypeOfSalesDesc18)
        BOFields.Add(mTypeOfSalesDesc19)
        BOFields.Add(mTypeOfSalesDesc20)
        BOFields.Add(mTypeOfSalesDesc21)
        BOFields.Add(mTypeOfSalesDesc22)
        BOFields.Add(mTypeOfSalesDesc23)
        BOFields.Add(mTypeOfSalesDesc24)
        BOFields.Add(mTypeOfSalesDesc25)
        BOFields.Add(mTypeOfSalesDesc26)
        BOFields.Add(mTypeOfSalesDesc27)
        BOFields.Add(mTypeOfSalesDesc28)
        BOFields.Add(mTypeOfSalesDesc29)
        BOFields.Add(mTypeOfSalesDesc30)
        BOFields.Add(mOverTenderAllowed01)
        BOFields.Add(mOverTenderAllowed02)
        BOFields.Add(mOverTenderAllowed03)
        BOFields.Add(mOverTenderAllowed04)
        BOFields.Add(mOverTenderAllowed05)
        BOFields.Add(mOverTenderAllowed06)
        BOFields.Add(mOverTenderAllowed07)
        BOFields.Add(mOverTenderAllowed08)
        BOFields.Add(mOverTenderAllowed09)
        BOFields.Add(mOverTenderAllowed10)
        BOFields.Add(mCreditCardCapture01)
        BOFields.Add(mCreditCardCapture02)
        BOFields.Add(mCreditCardCapture03)
        BOFields.Add(mCreditCardCapture04)
        BOFields.Add(mCreditCardCapture05)
        BOFields.Add(mCreditCardCapture06)
        BOFields.Add(mCreditCardCapture07)
        BOFields.Add(mCreditCardCapture08)
        BOFields.Add(mCreditCardCapture09)
        BOFields.Add(mCreditCardCapture10)
        BOFields.Add(mTenderTypeFlagDesc01)
        BOFields.Add(mTenderTypeFlagDesc02)
        BOFields.Add(mTenderTypeFlagDesc03)
        BOFields.Add(mTenderTypeFlagDesc04)
        BOFields.Add(mTenderTypeFlagDesc05)
        BOFields.Add(mTenderTypeFlagDesc06)
        BOFields.Add(mTenderTypeFlagDesc07)
        BOFields.Add(mTenderTypeFlagDesc08)
        BOFields.Add(mTenderTypeFlagDesc09)
        BOFields.Add(mTenderTypeFlagDesc10)
        BOFields.Add(mTenderTypeFlagDesc11)
        BOFields.Add(mTenderTypeFlagDesc12)
        BOFields.Add(mTenderTypeFlagDesc13)
        BOFields.Add(mTenderTypeFlagDesc14)
        BOFields.Add(mTenderTypeFlagDesc15)
        BOFields.Add(mTenderTypeFlagDesc16)
        BOFields.Add(mTenderTypeFlagDesc17)
        BOFields.Add(mTenderTypeFlagDesc18)
        BOFields.Add(mTenderTypeFlagDesc19)
        BOFields.Add(mTenderTypeFlagDesc20)
        BOFields.Add(mOpenDrawReasonCode01)
        BOFields.Add(mOpenDrawReasonCode02)
        BOFields.Add(mOpenDrawReasonCode03)
        BOFields.Add(mOpenDrawReasonCode04)
        BOFields.Add(mOpenDrawReasonCode05)
        BOFields.Add(mOpenDrawReasonCode06)
        BOFields.Add(mOpenDrawReasonCode07)
        BOFields.Add(mOpenDrawReasonCode08)
        BOFields.Add(mOpenDrawReasonCode09)
        BOFields.Add(mOpenDrawReasonCode10)
        BOFields.Add(mLastReformatDate)
        BOFields.Add(mLastUpdateDate)
        BOFields.Add(mAutoAppPriceChanges)
        BOFields.Add(mNoDayPstEffectDatePC)
        BOFields.Add(mPrintShelfEdgeLblPC)
        BOFields.Add(_CompanyName)
        BOFields.Add(mCompanyAddress1)
        BOFields.Add(mCompanyAddress2)
        BOFields.Add(mCompanyAddress3)
        BOFields.Add(mCompanyTelephone)
        BOFields.Add(mCompanyTelex)
        BOFields.Add(mTenderNoCheques)
        BOFields.Add(mTenderNoVisa)
        BOFields.Add(mTenderNoAccess)
        BOFields.Add(mTenderNoAmex)
        BOFields.Add(mTenderNoProjectLoan)
        BOFields.Add(mTenderNoStoreVouchs)
        BOFields.Add(mTenderNoSwitch)
        BOFields.Add(mTenderNoAccBuildMate)
        BOFields.Add(mMiscIncReasonCode01)
        BOFields.Add(mMiscIncReasonCode02)
        BOFields.Add(mMiscIncReasonCode03)
        BOFields.Add(mMiscIncReasonCode04)
        BOFields.Add(mMiscIncReasonCode05)
        BOFields.Add(mMiscIncReasonCode06)
        BOFields.Add(mMiscIncReasonCode07)
        BOFields.Add(mMiscIncReasonCode08)
        BOFields.Add(mMiscIncReasonCode09)
        BOFields.Add(mMiscIncReasonCode10)
        BOFields.Add(mMiscIncReasonCode11)
        BOFields.Add(mMiscIncReasonCode12)
        BOFields.Add(mMiscIncReasonCode13)
        BOFields.Add(mMiscIncReasonCode14)
        BOFields.Add(mMiscIncReasonCode15)
        BOFields.Add(mMiscIncReasonCode16)
        BOFields.Add(mMiscIncReasonCode17)
        BOFields.Add(mMiscIncReasonCode18)
        BOFields.Add(mMiscIncReasonCode19)
        BOFields.Add(mMiscIncReasonCode20)
        BOFields.Add(mStoreDepositsInNo)
        BOFields.Add(mMiscOutReasonCode01)
        BOFields.Add(mMiscOutReasonCode02)
        BOFields.Add(mMiscOutReasonCode03)
        BOFields.Add(mMiscOutReasonCode04)
        BOFields.Add(mMiscOutReasonCode05)
        BOFields.Add(mMiscOutReasonCode06)
        BOFields.Add(mMiscOutReasonCode07)
        BOFields.Add(mMiscOutReasonCode08)
        BOFields.Add(mMiscOutReasonCode09)
        BOFields.Add(mMiscOutReasonCode10)
        BOFields.Add(mMiscOutReasonCode11)
        BOFields.Add(mMiscOutReasonCode12)
        BOFields.Add(mMiscOutReasonCode13)
        BOFields.Add(mMiscOutReasonCode14)
        BOFields.Add(mMiscOutReasonCode15)
        BOFields.Add(mMiscOutReasonCode16)
        BOFields.Add(mMiscOutReasonCode17)
        BOFields.Add(mMiscOutReasonCode18)
        BOFields.Add(mMiscOutReasonCode19)
        BOFields.Add(mMiscOutReasonCode20)
        BOFields.Add(mStoreDespostisOutNo)
        BOFields.Add(mPriceOvrdReasonCde01)
        BOFields.Add(mPriceOvrdReasonCde02)
        BOFields.Add(mPriceOvrdReasonCde03)
        BOFields.Add(mPriceOvrdReasonCde04)
        BOFields.Add(mPriceOvrdReasonCde05)
        BOFields.Add(mPriceOvrdReasonCde06)
        BOFields.Add(mPriceOvrdReasonCde07)
        BOFields.Add(mPriceOvrdReasonCde08)
        BOFields.Add(mPriceOvrdReasonCde09)
        BOFields.Add(mPriceOvrdReasonCde10)
        BOFields.Add(mMaxCashChangeGiven)
        BOFields.Add(mCreditCardFloorLimit)
        BOFields.Add(mNoVATrates)
        BOFields.Add(mVATRatesInclusive)
        BOFields.Add(mVATRate01)
        BOFields.Add(mVATRate02)
        BOFields.Add(mVATRate03)
        BOFields.Add(mVATRate04)
        BOFields.Add(mVATRate05)
        BOFields.Add(mVATRate06)
        BOFields.Add(mVATRate07)
        BOFields.Add(mVATRate08)
        BOFields.Add(mVATRate09)
        BOFields.Add(mHighestValDiscPcent)
        BOFields.Add(mTenderTypeDesc01)
        BOFields.Add(mTenderTypeDesc02)
        BOFields.Add(mTenderTypeDesc03)
        BOFields.Add(mTenderTypeDesc04)
        BOFields.Add(mTenderTypeDesc05)
        BOFields.Add(mTenderTypeDesc06)
        BOFields.Add(mTenderTypeDesc07)
        BOFields.Add(mTenderTypeDesc08)
        BOFields.Add(mTenderTypeDesc09)
        BOFields.Add(mTenderTypeDesc10)
        BOFields.Add(mTenderTypeDesc11)
        BOFields.Add(mTenderTypeDesc12)
        BOFields.Add(mTenderTypeDesc13)
        BOFields.Add(mTenderTypeDesc14)
        BOFields.Add(mTenderTypeDesc15)
        BOFields.Add(mTenderTypeDesc16)
        BOFields.Add(mTenderTypeDesc17)
        BOFields.Add(mTenderTypeDesc18)
        BOFields.Add(mTenderTypeDesc19)
        BOFields.Add(mTenderTypeDesc20)
        BOFields.Add(mTenNoThisRelatesTo01)
        BOFields.Add(mTenNoThisRelatesTo02)
        BOFields.Add(mTenNoThisRelatesTo03)
        BOFields.Add(mTenNoThisRelatesTo04)
        BOFields.Add(mTenNoThisRelatesTo05)
        BOFields.Add(mTenNoThisRelatesTo06)
        BOFields.Add(mTenNoThisRelatesTo07)
        BOFields.Add(mTenNoThisRelatesTo08)
        BOFields.Add(mTenNoThisRelatesTo09)
        BOFields.Add(mTenNoThisRelatesTo10)
        BOFields.Add(mTenNoThisRelatesTo11)
        BOFields.Add(mTenNoThisRelatesTo12)
        BOFields.Add(mTenNoThisRelatesTo13)
        BOFields.Add(mTenNoThisRelatesTo14)
        BOFields.Add(mTenNoThisRelatesTo15)
        BOFields.Add(mTenNoThisRelatesTo16)
        BOFields.Add(mTenNoThisRelatesTo17)
        BOFields.Add(mTenNoThisRelatesTo18)
        BOFields.Add(mTenNoThisRelatesTo19)
        BOFields.Add(mTenNoThisRelatesTo20)
        BOFields.Add(mOpenCashDrawer01)
        BOFields.Add(mOpenCashDrawer02)
        BOFields.Add(mOpenCashDrawer03)
        BOFields.Add(mOpenCashDrawer04)
        BOFields.Add(mOpenCashDrawer05)
        BOFields.Add(mOpenCashDrawer06)
        BOFields.Add(mOpenCashDrawer07)
        BOFields.Add(mOpenCashDrawer08)
        BOFields.Add(mOpenCashDrawer09)
        BOFields.Add(mOpenCashDrawer10)
        BOFields.Add(mOpenCashDrawer11)
        BOFields.Add(mOpenCashDrawer12)
        BOFields.Add(mOpenCashDrawer13)
        BOFields.Add(mOpenCashDrawer14)
        BOFields.Add(mOpenCashDrawer15)
        BOFields.Add(mOpenCashDrawer16)
        BOFields.Add(mOpenCashDrawer17)
        BOFields.Add(mOpenCashDrawer18)
        BOFields.Add(mOpenCashDrawer19)
        BOFields.Add(mOpenCashDrawer20)
        BOFields.Add(mDefaultRemainAmt01)
        BOFields.Add(mDefaultRemainAmt02)
        BOFields.Add(mDefaultRemainAmt03)
        BOFields.Add(mDefaultRemainAmt04)
        BOFields.Add(mDefaultRemainAmt05)
        BOFields.Add(mDefaultRemainAm06)
        BOFields.Add(mDefaultRemainAmt07)
        BOFields.Add(mDefaultRemainAmt08)
        BOFields.Add(mDefaultRemainAmt09)
        BOFields.Add(mDefaultRemainAmt10)
        BOFields.Add(mDefaultRemainAmt11)
        BOFields.Add(mDefaultRemainAmt12)
        BOFields.Add(mDefaultRemainAmt13)
        BOFields.Add(mDefaultRemainAmt14)
        BOFields.Add(mDefaultRemainAmt15)
        BOFields.Add(mDefaultRemainAmt16)
        BOFields.Add(mDefaultRemainAmt17)
        BOFields.Add(mDefaultRemainAmt18)
        BOFields.Add(mDefaultRemainAmt19)
        BOFields.Add(mDefaultRemainAmt20)
        BOFields.Add(mMiscOpenDrawReasonNo)
        BOFields.Add(mMaxTillDrawFloat)
        BOFields.Add(mMinSavValBeforeMess)
        BOFields.Add(mMaxRefundAmtSelfAuth)
        BOFields.Add(mTillAccessEventFiles)
        BOFields.Add(mSecStaffDiscountPer)
        BOFields.Add(mVATPercDiscount)
        BOFields.Add(mVATDiscountActive)
        BOFields.Add(mFeeStatementOnSlips)
        BOFields.Add(mSupervisorReqTS01)
        BOFields.Add(mSupervisorReqTS02)
        BOFields.Add(mSupervisorReqTS03)
        BOFields.Add(mSupervisorReqTS04)
        BOFields.Add(mSupervisorReqTS05)
        BOFields.Add(mSupervisorReqTS06)
        BOFields.Add(mSupervisorReqTS07)
        BOFields.Add(mSupervisorReqTS08)
        BOFields.Add(mSupervisorReqTS09)
        BOFields.Add(mSupervisorReqTS10)
        BOFields.Add(mSupervisorReqTS11)
        BOFields.Add(mSupervisorReqTS12)
        BOFields.Add(mSupervisorReqTS13)
        BOFields.Add(mSupervisorReqTS14)
        BOFields.Add(mSupervisorReqTS15)
        BOFields.Add(mSupervisorReqTS16)
        BOFields.Add(mSupervisorReqTS17)
        BOFields.Add(mSupervisorReqTS18)
        BOFields.Add(mSupervisorReqTS19)
        BOFields.Add(mSupervisorReqTS20)
        BOFields.Add(mCustomerDtlReqTS01)
        BOFields.Add(mCustomerDtlReqTS02)
        BOFields.Add(mCustomerDtlReqTS03)
        BOFields.Add(mCustomerDtlReqTS04)
        BOFields.Add(mCustomerDtlReqTS05)
        BOFields.Add(mCustomerDtlReqTS06)
        BOFields.Add(mCustomerDtlReqTS07)
        BOFields.Add(mCustomerDtlReqTS08)
        BOFields.Add(mCustomerDtlReqTS09)
        BOFields.Add(mCustomerDtlReqTS10)
        BOFields.Add(mCustomerDtlReqTS11)
        BOFields.Add(mCustomerDtlReqTS12)
        BOFields.Add(mCustomerDtlReqTS13)
        BOFields.Add(mCustomerDtlReqTS14)
        BOFields.Add(mCustomerDtlReqTS15)
        BOFields.Add(mCustomerDtlReqTS16)
        BOFields.Add(mCustomerDtlReqTS17)
        BOFields.Add(mCustomerDtlReqTS18)
        BOFields.Add(mCustomerDtlReqTS19)
        BOFields.Add(mCustomerDtlReqTS20)
        BOFields.Add(mMinRefundRequiringSA)
        BOFields.Add(mMinRefundRequiringMA)
        BOFields.Add(mMinCollDiscTranValSA)
        BOFields.Add(mMinColDiscTranValMA)
        BOFields.Add(mMaxValGiftToken)
        BOFields.Add(mMinChgGivenGiftToken)
        BOFields.Add(mSalesMaxField)
        BOFields.Add(mAutoAppPriceChgAuth)
        BOFields.Add(mMgrAuthorisedAA)
        BOFields.Add(mHostuLoadInProgress)
        BOFields.Add(mCountryCode)
    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cRetailOptions)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cRetailOptions)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cRetailOptions))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cRetailOptions(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName.ToUpper)
                    Case ("FKEY") : _ID.Value = CStr(drRow(dcColumn))
                    Case ("STOR") : _Store.Value = CStr(drRow(dcColumn))
                    Case ("SNAM") : _StoreName.Value = drRow(dcColumn).ToString.Trim
                    Case ("SAD1") : _AddressLine1.Value = drRow(dcColumn).ToString.Trim
                    Case ("SAD2") : _AddressLine2.Value = drRow(dcColumn).ToString.Trim
                    Case ("SAD3") : _AddressLine3.Value = drRow(dcColumn).ToString.Trim
                    Case ("TYPE") : _AccountabilityType.Value = CStr(drRow(dcColumn))
                    Case ("HCAS") : mHighestValidCashier.Value = CStr(drRow(dcColumn))
                    Case ("FLOT") : mDefaultFloatAmountAT.Value = CDec(drRow(dcColumn))
                    Case ("LAST") : mLstAcctPerRetClosed.Value = CDec(drRow(dcColumn))
                    Case ("NEXT") : mNextAcctPeriodToOpen.Value = CDec(drRow(dcColumn))
                    Case ("KEEP") : mAccoutingPerToKeep.Value = CDec(drRow(dcColumn))
                    Case ("TOSN1") : mTypeOfSaleNo01.Value = CStr(drRow(dcColumn))
                    Case ("TOSN2") : mTypeOfSaleNo02.Value = CStr(drRow(dcColumn))
                    Case ("TOSN3") : mTypeOfSaleNo03.Value = CStr(drRow(dcColumn))
                    Case ("TOSN4") : mTypeOfSaleNo04.Value = CStr(drRow(dcColumn))
                    Case ("TOSN5") : mTypeOfSaleNo05.Value = CStr(drRow(dcColumn))
                    Case ("TOSN6") : mTypeOfSaleNo06.Value = CStr(drRow(dcColumn))
                    Case ("TOSN7") : mTypeOfSaleNo07.Value = CStr(drRow(dcColumn))
                    Case ("TOSN8") : mTypeOfSaleNo08.Value = CStr(drRow(dcColumn))
                    Case ("TOSN9") : mTypeOfSaleNo09.Value = CStr(drRow(dcColumn))
                    Case ("TOSN10") : mTypeOfSaleNo10.Value = CStr(drRow(dcColumn))
                    Case ("TOSN11") : mTypeOfSaleNo11.Value = CStr(drRow(dcColumn))
                    Case ("TOSN12") : mTypeOfSaleNo12.Value = CStr(drRow(dcColumn))
                    Case ("TOSN13") : mTypeOfSaleNo13.Value = CStr(drRow(dcColumn))
                    Case ("TOSN14") : mTypeOfSaleNo14.Value = CStr(drRow(dcColumn))
                    Case ("TOSN15") : mTypeOfSaleNo15.Value = CStr(drRow(dcColumn))
                    Case ("TOSN16") : mTypeOfSaleNo16.Value = CStr(drRow(dcColumn))
                    Case ("TOSN17") : mTypeOfSaleNo17.Value = CStr(drRow(dcColumn))
                    Case ("TOSN18") : mTypeOfSaleNo18.Value = CStr(drRow(dcColumn))
                    Case ("TOSN19") : mTypeOfSaleNo19.Value = CStr(drRow(dcColumn))
                    Case ("TOSN20") : mTypeOfSaleNo20.Value = CStr(drRow(dcColumn))
                    Case ("TOSN21") : mTypeOfSaleNo21.Value = CStr(drRow(dcColumn))
                    Case ("TOSN22") : mTypeOfSaleNo22.Value = CStr(drRow(dcColumn))
                    Case ("TOSN23") : mTypeOfSaleNo23.Value = CStr(drRow(dcColumn))
                    Case ("TOSN24") : mTypeOfSaleNo24.Value = CStr(drRow(dcColumn))
                    Case ("TOSN25") : mTypeOfSaleNo25.Value = CStr(drRow(dcColumn))
                    Case ("TOSN26") : mTypeOfSaleNo26.Value = CStr(drRow(dcColumn))
                    Case ("TOSN27") : mTypeOfSaleNo27.Value = CStr(drRow(dcColumn))
                    Case ("TOSN28") : mTypeOfSaleNo28.Value = CStr(drRow(dcColumn))
                    Case ("TOSN29") : mTypeOfSaleNo29.Value = CStr(drRow(dcColumn))
                    Case ("TOSN30") : mTypeOfSaleNo30.Value = CStr(drRow(dcColumn))
                    Case ("TOSC1") : mTypeOfSale01.Value = CStr(drRow(dcColumn))
                    Case ("TOSC2") : mTypeOfSale02.Value = CStr(drRow(dcColumn))
                    Case ("TOSC3") : mTypeOfSale03.Value = CStr(drRow(dcColumn))
                    Case ("TOSC4") : mTypeOfSale04.Value = CStr(drRow(dcColumn))
                    Case ("TOSC5") : mTypeOfSale05.Value = CStr(drRow(dcColumn))
                    Case ("TOSC6") : mTypeOfSale06.Value = CStr(drRow(dcColumn))
                    Case ("TOSC7") : mTypeOfSale07.Value = CStr(drRow(dcColumn))
                    Case ("TOSC8") : mTypeOfSale08.Value = CStr(drRow(dcColumn))
                    Case ("TOSC9") : mTypeOfSale09.Value = CStr(drRow(dcColumn))
                    Case ("TOSC10") : mTypeOfSale10.Value = CStr(drRow(dcColumn))
                    Case ("TOSC11") : mTypeOfSale11.Value = CStr(drRow(dcColumn))
                    Case ("TOSC12") : mTypeOfSale12.Value = CStr(drRow(dcColumn))
                    Case ("TOSC13") : mTypeOfSale13.Value = CStr(drRow(dcColumn))
                    Case ("TOSC14") : mTypeOfSale14.Value = CStr(drRow(dcColumn))
                    Case ("TOSC15") : mTypeOfSale15.Value = CStr(drRow(dcColumn))
                    Case ("TOSC16") : mTypeOfSale16.Value = CStr(drRow(dcColumn))
                    Case ("TOSC17") : mTypeOfSale17.Value = CStr(drRow(dcColumn))
                    Case ("TOSC18") : mTypeOfSale18.Value = CStr(drRow(dcColumn))
                    Case ("TOSC19") : mTypeOfSale19.Value = CStr(drRow(dcColumn))
                    Case ("TOSC20") : mTypeOfSale20.Value = CStr(drRow(dcColumn))
                    Case ("TOSC21") : mTypeOfSale21.Value = CStr(drRow(dcColumn))
                    Case ("TOSC22") : mTypeOfSale22.Value = CStr(drRow(dcColumn))
                    Case ("TOSC23") : mTypeOfSale23.Value = CStr(drRow(dcColumn))
                    Case ("TOSC24") : mTypeOfSale24.Value = CStr(drRow(dcColumn))
                    Case ("TOSC25") : mTypeOfSale25.Value = CStr(drRow(dcColumn))
                    Case ("TOSC26") : mTypeOfSale26.Value = CStr(drRow(dcColumn))
                    Case ("TOSC27") : mTypeOfSale27.Value = CStr(drRow(dcColumn))
                    Case ("TOSC28") : mTypeOfSale28.Value = CStr(drRow(dcColumn))
                    Case ("TOSC29") : mTypeOfSale29.Value = CStr(drRow(dcColumn))
                    Case ("TOSC30") : mTypeOfSale30.Value = CStr(drRow(dcColumn))
                    Case ("TOSD1") : mTypeOfSalesDesc01.Value = CStr(drRow(dcColumn))
                    Case ("TOSD2") : mTypeOfSalesDesc02.Value = CStr(drRow(dcColumn))
                    Case ("TOSD3") : mTypeOfSalesDesc03.Value = CStr(drRow(dcColumn))
                    Case ("TOSD4") : mTypeOfSalesDesc04.Value = CStr(drRow(dcColumn))
                    Case ("TOSD5") : mTypeOfSalesDesc05.Value = CStr(drRow(dcColumn))
                    Case ("TOSD6") : mTypeOfSalesDesc06.Value = CStr(drRow(dcColumn))
                    Case ("TOSD7") : mTypeOfSalesDesc07.Value = CStr(drRow(dcColumn))
                    Case ("TOSD8") : mTypeOfSalesDesc08.Value = CStr(drRow(dcColumn))
                    Case ("TOSD9") : mTypeOfSalesDesc09.Value = CStr(drRow(dcColumn))
                    Case ("TOSD10") : mTypeOfSalesDesc10.Value = CStr(drRow(dcColumn))
                    Case ("TOSD11") : mTypeOfSalesDesc11.Value = CStr(drRow(dcColumn))
                    Case ("TOSD12") : mTypeOfSalesDesc12.Value = CStr(drRow(dcColumn))
                    Case ("TOSD13") : mTypeOfSalesDesc13.Value = CStr(drRow(dcColumn))
                    Case ("TOSD14") : mTypeOfSalesDesc14.Value = CStr(drRow(dcColumn))
                    Case ("TOSD15") : mTypeOfSalesDess15.Value = CStr(drRow(dcColumn))
                    Case ("TOSD16") : mTypeOfSalesDesc16.Value = CStr(drRow(dcColumn))
                    Case ("TOSD17") : mTypeOfSalesDesc17.Value = CStr(drRow(dcColumn))
                    Case ("TOSD18") : mTypeOfSalesDesc18.Value = CStr(drRow(dcColumn))
                    Case ("TOSD19") : mTypeOfSalesDesc19.Value = CStr(drRow(dcColumn))
                    Case ("TOSD20") : mTypeOfSalesDesc20.Value = CStr(drRow(dcColumn))
                    Case ("TOSD21") : mTypeOfSalesDesc21.Value = CStr(drRow(dcColumn))
                    Case ("TOSD22") : mTypeOfSalesDesc22.Value = CStr(drRow(dcColumn))
                    Case ("TOSD23") : mTypeOfSalesDesc23.Value = CStr(drRow(dcColumn))
                    Case ("TOSD24") : mTypeOfSalesDesc24.Value = CStr(drRow(dcColumn))
                    Case ("TOSD25") : mTypeOfSalesDesc25.Value = CStr(drRow(dcColumn))
                    Case ("TOSD26") : mTypeOfSalesDesc26.Value = CStr(drRow(dcColumn))
                    Case ("TOSD27") : mTypeOfSalesDesc27.Value = CStr(drRow(dcColumn))
                    Case ("TOSD28") : mTypeOfSalesDesc28.Value = CStr(drRow(dcColumn))
                    Case ("TOSD29") : mTypeOfSalesDesc29.Value = CStr(drRow(dcColumn))
                    Case ("TOSD30") : mTypeOfSalesDesc30.Value = CStr(drRow(dcColumn))
                    Case ("TTOT1") : mOverTenderAllowed01.Value = CBool(drRow(dcColumn))
                    Case ("TTOT2") : mOverTenderAllowed02.Value = CBool(drRow(dcColumn))
                    Case ("TTOT3") : mOverTenderAllowed03.Value = CBool(drRow(dcColumn))
                    Case ("TTOT4") : mOverTenderAllowed04.Value = CBool(drRow(dcColumn))
                    Case ("TTOT5") : mOverTenderAllowed05.Value = CBool(drRow(dcColumn))
                    Case ("TTOT6") : mOverTenderAllowed06.Value = CBool(drRow(dcColumn))
                    Case ("TTOT7") : mOverTenderAllowed07.Value = CBool(drRow(dcColumn))
                    Case ("TTOT8") : mOverTenderAllowed08.Value = CBool(drRow(dcColumn))
                    Case ("TTOT9") : mOverTenderAllowed09.Value = CBool(drRow(dcColumn))
                    Case ("TTOT10") : mOverTenderAllowed10.Value = CBool(drRow(dcColumn))
                    Case ("TTCC1") : mCreditCardCapture01.Value = CBool(drRow(dcColumn))
                    Case ("TTCC2") : mCreditCardCapture02.Value = CBool(drRow(dcColumn))
                    Case ("TTCC3") : mCreditCardCapture03.Value = CBool(drRow(dcColumn))
                    Case ("TTCC4") : mCreditCardCapture04.Value = CBool(drRow(dcColumn))
                    Case ("TTCC5") : mCreditCardCapture05.Value = CBool(drRow(dcColumn))
                    Case ("TTCC6") : mCreditCardCapture06.Value = CBool(drRow(dcColumn))
                    Case ("TTCC7") : mCreditCardCapture07.Value = CBool(drRow(dcColumn))
                    Case ("TTCC8") : mCreditCardCapture08.Value = CBool(drRow(dcColumn))
                    Case ("TTCC9") : mCreditCardCapture09.Value = CBool(drRow(dcColumn))
                    Case ("TTCC10") : mCreditCardCapture10.Value = CBool(drRow(dcColumn))
                    Case ("TTDE1") : mTenderTypeFlagDesc01.Value = CStr(drRow(dcColumn))
                    Case ("TTDE2") : mTenderTypeFlagDesc02.Value = CStr(drRow(dcColumn))
                    Case ("TTDE3") : mTenderTypeFlagDesc03.Value = CStr(drRow(dcColumn))
                    Case ("TTDE4") : mTenderTypeFlagDesc04.Value = CStr(drRow(dcColumn))
                    Case ("TTDE5") : mTenderTypeFlagDesc05.Value = CStr(drRow(dcColumn))
                    Case ("TTDE6") : mTenderTypeFlagDesc06.Value = CStr(drRow(dcColumn))
                    Case ("TTDE7") : mTenderTypeFlagDesc07.Value = CStr(drRow(dcColumn))
                    Case ("TTDE8") : mTenderTypeFlagDesc08.Value = CStr(drRow(dcColumn))
                    Case ("TTDE9") : mTenderTypeFlagDesc09.Value = CStr(drRow(dcColumn))
                    Case ("TTDE10") : mTenderTypeFlagDesc10.Value = CStr(drRow(dcColumn))
                    Case ("TTDE11") : mTenderTypeFlagDesc11.Value = CStr(drRow(dcColumn))
                    Case ("TTDE12") : mTenderTypeFlagDesc12.Value = CStr(drRow(dcColumn))
                    Case ("TTDE13") : mTenderTypeFlagDesc13.Value = CStr(drRow(dcColumn))
                    Case ("TTDE14") : mTenderTypeFlagDesc14.Value = CStr(drRow(dcColumn))
                    Case ("TTDE15") : mTenderTypeFlagDesc15.Value = CStr(drRow(dcColumn))
                    Case ("TTDE16") : mTenderTypeFlagDesc16.Value = CStr(drRow(dcColumn))
                    Case ("TTDE17") : mTenderTypeFlagDesc17.Value = CStr(drRow(dcColumn))
                    Case ("TTDE18") : mTenderTypeFlagDesc18.Value = CStr(drRow(dcColumn))
                    Case ("TTDE19") : mTenderTypeFlagDesc19.Value = CStr(drRow(dcColumn))
                    Case ("TTDE20") : mTenderTypeFlagDesc20.Value = CStr(drRow(dcColumn))
                    Case ("ODRC1") : mOpenDrawReasonCode01.Value = CStr(drRow(dcColumn))
                    Case ("ODRC2") : mOpenDrawReasonCode02.Value = CStr(drRow(dcColumn))
                    Case ("ODRC3") : mOpenDrawReasonCode03.Value = CStr(drRow(dcColumn))
                    Case ("ODRC4") : mOpenDrawReasonCode04.Value = CStr(drRow(dcColumn))
                    Case ("ODRC5") : mOpenDrawReasonCode05.Value = CStr(drRow(dcColumn))
                    Case ("ODRC6") : mOpenDrawReasonCode06.Value = CStr(drRow(dcColumn))
                    Case ("ODRC7") : mOpenDrawReasonCode07.Value = CStr(drRow(dcColumn))
                    Case ("ODRC8") : mOpenDrawReasonCode08.Value = CStr(drRow(dcColumn))
                    Case ("ODRC9") : mOpenDrawReasonCode09.Value = CStr(drRow(dcColumn))
                    Case ("ODRC10") : mOpenDrawReasonCode10.Value = CStr(drRow(dcColumn))
                    Case ("DOLR") : mLastReformatDate.Value = CDate(drRow(dcColumn))
                    Case ("DOLU") : mLastUpdateDate.Value = CDate(drRow(dcColumn))
                    Case ("PCAA") : mAutoAppPriceChanges.Value = CBool(drRow(dcColumn))
                    Case ("PCDA") : mNoDayPstEffectDatePC.Value = CDec(drRow(dcColumn))
                    Case ("PSEL") : mPrintShelfEdgeLblPC.Value = CBool(drRow(dcColumn))
                    Case ("CNAM") : _CompanyName.Value = CStr(drRow(dcColumn))
                    Case ("CAD1") : mCompanyAddress1.Value = CStr(drRow(dcColumn))
                    Case ("CAD2") : mCompanyAddress2.Value = CStr(drRow(dcColumn))
                    Case ("CAD3") : mCompanyAddress3.Value = CStr(drRow(dcColumn))
                    Case ("CTEL") : mCompanyTelephone.Value = CStr(drRow(dcColumn))
                    Case ("CTLX") : mCompanyTelex.Value = CStr(drRow(dcColumn))
                    Case ("CHON") : mTenderNoCheques.Value = CDec(drRow(dcColumn))
                    Case ("VION") : mTenderNoVisa.Value = CDec(drRow(dcColumn))
                    Case ("MCON") : mTenderNoAccess.Value = CDec(drRow(dcColumn))
                    Case ("AXON") : mTenderNoAmex.Value = CDec(drRow(dcColumn))
                    Case ("WION") : mTenderNoProjectLoan.Value = CDec(drRow(dcColumn))
                    Case ("SVON") : mTenderNoStoreVouchs.Value = CDec(drRow(dcColumn))
                    Case ("SWON") : mTenderNoSwitch.Value = CDec(drRow(dcColumn))
                    Case ("ACON") : mTenderNoAccBuildMate.Value = CDec(drRow(dcColumn))
                    Case ("MPRC1") : mMiscIncReasonCode01.Value = CStr(drRow(dcColumn))
                    Case ("MPRC2") : mMiscIncReasonCode02.Value = CStr(drRow(dcColumn))
                    Case ("MPRC3") : mMiscIncReasonCode03.Value = CStr(drRow(dcColumn))
                    Case ("MPRC4") : mMiscIncReasonCode04.Value = CStr(drRow(dcColumn))
                    Case ("MPRC5") : mMiscIncReasonCode05.Value = CStr(drRow(dcColumn))
                    Case ("MPRC6") : mMiscIncReasonCode06.Value = CStr(drRow(dcColumn))
                    Case ("MPRC7") : mMiscIncReasonCode07.Value = CStr(drRow(dcColumn))
                    Case ("MPRC8") : mMiscIncReasonCode08.Value = CStr(drRow(dcColumn))
                    Case ("MPRC9") : mMiscIncReasonCode09.Value = CStr(drRow(dcColumn))
                    Case ("MPRC10") : mMiscIncReasonCode10.Value = CStr(drRow(dcColumn))
                    Case ("MPRC11") : mMiscIncReasonCode11.Value = CStr(drRow(dcColumn))
                    Case ("MPRC12") : mMiscIncReasonCode12.Value = CStr(drRow(dcColumn))
                    Case ("MPRC13") : mMiscIncReasonCode13.Value = CStr(drRow(dcColumn))
                    Case ("MPRC14") : mMiscIncReasonCode14.Value = CStr(drRow(dcColumn))
                    Case ("MPRC15") : mMiscIncReasonCode15.Value = CStr(drRow(dcColumn))
                    Case ("MPRC16") : mMiscIncReasonCode16.Value = CStr(drRow(dcColumn))
                    Case ("MPRC17") : mMiscIncReasonCode17.Value = CStr(drRow(dcColumn))
                    Case ("MPRC18") : mMiscIncReasonCode18.Value = CStr(drRow(dcColumn))
                    Case ("MPRC19") : mMiscIncReasonCode19.Value = CStr(drRow(dcColumn))
                    Case ("MPRC20") : mMiscIncReasonCode20.Value = CStr(drRow(dcColumn))
                    Case ("SDIO") : mStoreDepositsInNo.Value = CDec(drRow(dcColumn))
                    Case ("MMRC1") : mMiscOutReasonCode01.Value = CStr(drRow(dcColumn))
                    Case ("MMRC2") : mMiscOutReasonCode02.Value = CStr(drRow(dcColumn))
                    Case ("MMRC3") : mMiscOutReasonCode03.Value = CStr(drRow(dcColumn))
                    Case ("MMRC4") : mMiscOutReasonCode04.Value = CStr(drRow(dcColumn))
                    Case ("MMRC5") : mMiscOutReasonCode05.Value = CStr(drRow(dcColumn))
                    Case ("MMRC6") : mMiscOutReasonCode06.Value = CStr(drRow(dcColumn))
                    Case ("MMRC7") : mMiscOutReasonCode07.Value = CStr(drRow(dcColumn))
                    Case ("MMRC8") : mMiscOutReasonCode08.Value = CStr(drRow(dcColumn))
                    Case ("MMRC9") : mMiscOutReasonCode09.Value = CStr(drRow(dcColumn))
                    Case ("MMRC10") : mMiscOutReasonCode10.Value = CStr(drRow(dcColumn))
                    Case ("MMRC11") : mMiscOutReasonCode11.Value = CStr(drRow(dcColumn))
                    Case ("MMRC12") : mMiscOutReasonCode12.Value = CStr(drRow(dcColumn))
                    Case ("MMRC13") : mMiscOutReasonCode13.Value = CStr(drRow(dcColumn))
                    Case ("MMRC14") : mMiscOutReasonCode14.Value = CStr(drRow(dcColumn))
                    Case ("MMRC15") : mMiscOutReasonCode15.Value = CStr(drRow(dcColumn))
                    Case ("MMRC16") : mMiscOutReasonCode16.Value = CStr(drRow(dcColumn))
                    Case ("MMRC17") : mMiscOutReasonCode17.Value = CStr(drRow(dcColumn))
                    Case ("MMRC18") : mMiscOutReasonCode18.Value = CStr(drRow(dcColumn))
                    Case ("MMRC19") : mMiscOutReasonCode19.Value = CStr(drRow(dcColumn))
                    Case ("MMRC20") : mMiscOutReasonCode20.Value = CStr(drRow(dcColumn))
                    Case ("SDOO") : mStoreDespostisOutNo.Value = CDec(drRow(dcColumn))
                    Case ("PORC1") : mPriceOvrdReasonCde01.Value = CStr(drRow(dcColumn))
                    Case ("PORC2") : mPriceOvrdReasonCde02.Value = CStr(drRow(dcColumn))
                    Case ("PORC3") : mPriceOvrdReasonCde03.Value = CStr(drRow(dcColumn))
                    Case ("PORC4") : mPriceOvrdReasonCde04.Value = CStr(drRow(dcColumn))
                    Case ("PORC5") : mPriceOvrdReasonCde05.Value = CStr(drRow(dcColumn))
                    Case ("PORC6") : mPriceOvrdReasonCde06.Value = CStr(drRow(dcColumn))
                    Case ("PORC7") : mPriceOvrdReasonCde07.Value = CStr(drRow(dcColumn))
                    Case ("PORC8") : mPriceOvrdReasonCde08.Value = CStr(drRow(dcColumn))
                    Case ("PORC9") : mPriceOvrdReasonCde09.Value = CStr(drRow(dcColumn))
                    Case ("PORC10") : mPriceOvrdReasonCde10.Value = CStr(drRow(dcColumn))
                    Case ("MAXC") : mMaxCashChangeGiven.Value = CDec(drRow(dcColumn))
                    Case ("CCFL") : mCreditCardFloorLimit.Value = CDec(drRow(dcColumn))
                    Case ("VATN") : mNoVATrates.Value = CDec(drRow(dcColumn))
                    Case ("VATI") : mVATRatesInclusive.Value = CBool(drRow(dcColumn))
                    Case ("VATR1") : mVATRate01.Value = CDec(drRow(dcColumn))
                    Case ("VATR2") : mVATRate02.Value = CDec(drRow(dcColumn))
                    Case ("VATR3") : mVATRate03.Value = CDec(drRow(dcColumn))
                    Case ("VATR4") : mVATRate04.Value = CDec(drRow(dcColumn))
                    Case ("VATR5") : mVATRate05.Value = CDec(drRow(dcColumn))
                    Case ("VATR6") : mVATRate06.Value = CDec(drRow(dcColumn))
                    Case ("VATR7") : mVATRate07.Value = CDec(drRow(dcColumn))
                    Case ("VATR8") : mVATRate08.Value = CDec(drRow(dcColumn))
                    Case ("VATR9") : mVATRate09.Value = CDec(drRow(dcColumn))
                    Case ("VDIS") : mHighestValDiscPcent.Value = CDec(drRow(dcColumn))
                    Case ("TDES1") : mTenderTypeDesc01.Value = CStr(drRow(dcColumn))
                    Case ("TDES2") : mTenderTypeDesc02.Value = CStr(drRow(dcColumn))
                    Case ("TDES3") : mTenderTypeDesc03.Value = CStr(drRow(dcColumn))
                    Case ("TDES4") : mTenderTypeDesc04.Value = CStr(drRow(dcColumn))
                    Case ("TDES5") : mTenderTypeDesc05.Value = CStr(drRow(dcColumn))
                    Case ("TDES6") : mTenderTypeDesc06.Value = CStr(drRow(dcColumn))
                    Case ("TDES7") : mTenderTypeDesc07.Value = CStr(drRow(dcColumn))
                    Case ("TDES8") : mTenderTypeDesc08.Value = CStr(drRow(dcColumn))
                    Case ("TDES9") : mTenderTypeDesc09.Value = CStr(drRow(dcColumn))
                    Case ("TDES10") : mTenderTypeDesc10.Value = CStr(drRow(dcColumn))
                    Case ("TDES11") : mTenderTypeDesc11.Value = CStr(drRow(dcColumn))
                    Case ("TDES12") : mTenderTypeDesc12.Value = CStr(drRow(dcColumn))
                    Case ("TDES13") : mTenderTypeDesc13.Value = CStr(drRow(dcColumn))
                    Case ("TDES14") : mTenderTypeDesc14.Value = CStr(drRow(dcColumn))
                    Case ("TDES15") : mTenderTypeDesc15.Value = CStr(drRow(dcColumn))
                    Case ("TDES16") : mTenderTypeDesc16.Value = CStr(drRow(dcColumn))
                    Case ("TDES17") : mTenderTypeDesc17.Value = CStr(drRow(dcColumn))
                    Case ("TDES18") : mTenderTypeDesc18.Value = CStr(drRow(dcColumn))
                    Case ("TDES19") : mTenderTypeDesc19.Value = CStr(drRow(dcColumn))
                    Case ("TDES20") : mTenderTypeDesc20.Value = CStr(drRow(dcColumn))
                    Case ("TATC1") : mTenNoThisRelatesTo01.Value = CDec(drRow(dcColumn))
                    Case ("TATC2") : mTenNoThisRelatesTo02.Value = CDec(drRow(dcColumn))
                    Case ("TATC3") : mTenNoThisRelatesTo03.Value = CDec(drRow(dcColumn))
                    Case ("TATC4") : mTenNoThisRelatesTo04.Value = CDec(drRow(dcColumn))
                    Case ("TATC5") : mTenNoThisRelatesTo05.Value = CDec(drRow(dcColumn))
                    Case ("TATC6") : mTenNoThisRelatesTo06.Value = CDec(drRow(dcColumn))
                    Case ("TATC7") : mTenNoThisRelatesTo07.Value = CDec(drRow(dcColumn))
                    Case ("TATC8") : mTenNoThisRelatesTo08.Value = CDec(drRow(dcColumn))
                    Case ("TATC9") : mTenNoThisRelatesTo09.Value = CDec(drRow(dcColumn))
                    Case ("TATC10") : mTenNoThisRelatesTo10.Value = CDec(drRow(dcColumn))
                    Case ("TATC11") : mTenNoThisRelatesTo11.Value = CDec(drRow(dcColumn))
                    Case ("TATC12") : mTenNoThisRelatesTo12.Value = CDec(drRow(dcColumn))
                    Case ("TATC13") : mTenNoThisRelatesTo13.Value = CDec(drRow(dcColumn))
                    Case ("TATC14") : mTenNoThisRelatesTo14.Value = CDec(drRow(dcColumn))
                    Case ("TATC15") : mTenNoThisRelatesTo15.Value = CDec(drRow(dcColumn))
                    Case ("TATC16") : mTenNoThisRelatesTo16.Value = CDec(drRow(dcColumn))
                    Case ("TATC17") : mTenNoThisRelatesTo17.Value = CDec(drRow(dcColumn))
                    Case ("TATC18") : mTenNoThisRelatesTo18.Value = CDec(drRow(dcColumn))
                    Case ("TATC19") : mTenNoThisRelatesTo19.Value = CDec(drRow(dcColumn))
                    Case ("TATC20") : mTenNoThisRelatesTo20.Value = CDec(drRow(dcColumn))
                    Case ("TODR1") : mOpenCashDrawer01.Value = CBool(drRow(dcColumn))
                    Case ("TODR2") : mOpenCashDrawer02.Value = CBool(drRow(dcColumn))
                    Case ("TODR3") : mOpenCashDrawer03.Value = CBool(drRow(dcColumn))
                    Case ("TODR4") : mOpenCashDrawer04.Value = CBool(drRow(dcColumn))
                    Case ("TODR5") : mOpenCashDrawer05.Value = CBool(drRow(dcColumn))
                    Case ("TODR6") : mOpenCashDrawer06.Value = CBool(drRow(dcColumn))
                    Case ("TODR7") : mOpenCashDrawer07.Value = CBool(drRow(dcColumn))
                    Case ("TODR8") : mOpenCashDrawer08.Value = CBool(drRow(dcColumn))
                    Case ("TODR9") : mOpenCashDrawer09.Value = CBool(drRow(dcColumn))
                    Case ("TODR10") : mOpenCashDrawer10.Value = CBool(drRow(dcColumn))
                    Case ("TODR11") : mOpenCashDrawer11.Value = CBool(drRow(dcColumn))
                    Case ("TODR12") : mOpenCashDrawer12.Value = CBool(drRow(dcColumn))
                    Case ("TODR13") : mOpenCashDrawer13.Value = CBool(drRow(dcColumn))
                    Case ("TODR14") : mOpenCashDrawer14.Value = CBool(drRow(dcColumn))
                    Case ("TODR15") : mOpenCashDrawer15.Value = CBool(drRow(dcColumn))
                    Case ("TODR16") : mOpenCashDrawer16.Value = CBool(drRow(dcColumn))
                    Case ("TODR17") : mOpenCashDrawer17.Value = CBool(drRow(dcColumn))
                    Case ("TODR18") : mOpenCashDrawer18.Value = CBool(drRow(dcColumn))
                    Case ("TODR19") : mOpenCashDrawer19.Value = CBool(drRow(dcColumn))
                    Case ("TODR20") : mOpenCashDrawer20.Value = CBool(drRow(dcColumn))
                    Case ("TADA1") : mDefaultRemainAmt01.Value = CBool(drRow(dcColumn))
                    Case ("TADA2") : mDefaultRemainAmt02.Value = CBool(drRow(dcColumn))
                    Case ("TADA3") : mDefaultRemainAmt03.Value = CBool(drRow(dcColumn))
                    Case ("TADA4") : mDefaultRemainAmt04.Value = CBool(drRow(dcColumn))
                    Case ("TADA5") : mDefaultRemainAmt05.Value = CBool(drRow(dcColumn))
                    Case ("TADA6") : mDefaultRemainAm06.Value = CBool(drRow(dcColumn))
                    Case ("TADA7") : mDefaultRemainAmt07.Value = CBool(drRow(dcColumn))
                    Case ("TADA8") : mDefaultRemainAmt08.Value = CBool(drRow(dcColumn))
                    Case ("TADA9") : mDefaultRemainAmt09.Value = CBool(drRow(dcColumn))
                    Case ("TADA10") : mDefaultRemainAmt10.Value = CBool(drRow(dcColumn))
                    Case ("TADA11") : mDefaultRemainAmt11.Value = CBool(drRow(dcColumn))
                    Case ("TADA12") : mDefaultRemainAmt12.Value = CBool(drRow(dcColumn))
                    Case ("TADA13") : mDefaultRemainAmt13.Value = CBool(drRow(dcColumn))
                    Case ("TADA14") : mDefaultRemainAmt14.Value = CBool(drRow(dcColumn))
                    Case ("TADA15") : mDefaultRemainAmt15.Value = CBool(drRow(dcColumn))
                    Case ("TADA16") : mDefaultRemainAmt16.Value = CBool(drRow(dcColumn))
                    Case ("TADA17") : mDefaultRemainAmt17.Value = CBool(drRow(dcColumn))
                    Case ("TADA18") : mDefaultRemainAmt18.Value = CBool(drRow(dcColumn))
                    Case ("TADA19") : mDefaultRemainAmt19.Value = CBool(drRow(dcColumn))
                    Case ("TADA20") : mDefaultRemainAmt20.Value = CBool(drRow(dcColumn))
                    Case ("MODR") : mMiscOpenDrawReasonNo.Value = CDec(drRow(dcColumn))
                    Case ("MAXF") : mMaxTillDrawFloat.Value = CDec(drRow(dcColumn))
                    Case ("MSAV") : mMinSavValBeforeMess.Value = CDec(drRow(dcColumn))
                    Case ("MRSQ") : mMaxRefundAmtSelfAuth.Value = CDec(drRow(dcColumn))
                    Case ("IEVT") : mTillAccessEventFiles.Value = CBool(drRow(dcColumn))
                    Case ("SDIS") : mSecStaffDiscountPer.Value = CDec(drRow(dcColumn))
                    Case ("VATP") : mVATPercDiscount.Value = CDec(drRow(dcColumn))
                    Case ("VATDO") : mVATDiscountActive.Value = CBool(drRow(dcColumn))
                    Case ("VATL") : mFeeStatementOnSlips.Value = CBool(drRow(dcColumn))
                    Case ("TSUP1") : mSupervisorReqTS01.Value = CBool(drRow(dcColumn))
                    Case ("TSUP2") : mSupervisorReqTS02.Value = CBool(drRow(dcColumn))
                    Case ("TSUP3") : mSupervisorReqTS03.Value = CBool(drRow(dcColumn))
                    Case ("TSUP4") : mSupervisorReqTS04.Value = CBool(drRow(dcColumn))
                    Case ("TSUP5") : mSupervisorReqTS05.Value = CBool(drRow(dcColumn))
                    Case ("TSUP6") : mSupervisorReqTS06.Value = CBool(drRow(dcColumn))
                    Case ("TSUP7") : mSupervisorReqTS07.Value = CBool(drRow(dcColumn))
                    Case ("TSUP8") : mSupervisorReqTS08.Value = CBool(drRow(dcColumn))
                    Case ("TSUP9") : mSupervisorReqTS09.Value = CBool(drRow(dcColumn))
                    Case ("TSUP10") : mSupervisorReqTS10.Value = CBool(drRow(dcColumn))
                    Case ("TSUP11") : mSupervisorReqTS11.Value = CBool(drRow(dcColumn))
                    Case ("TSUP12") : mSupervisorReqTS12.Value = CBool(drRow(dcColumn))
                    Case ("TSUP13") : mSupervisorReqTS13.Value = CBool(drRow(dcColumn))
                    Case ("TSUP14") : mSupervisorReqTS14.Value = CBool(drRow(dcColumn))
                    Case ("TSUP15") : mSupervisorReqTS15.Value = CBool(drRow(dcColumn))
                    Case ("TSUP16") : mSupervisorReqTS16.Value = CBool(drRow(dcColumn))
                    Case ("TSUP17") : mSupervisorReqTS17.Value = CBool(drRow(dcColumn))
                    Case ("TSUP18") : mSupervisorReqTS18.Value = CBool(drRow(dcColumn))
                    Case ("TSUP19") : mSupervisorReqTS19.Value = CBool(drRow(dcColumn))
                    Case ("TSUP20") : mSupervisorReqTS20.Value = CBool(drRow(dcColumn))
                    Case ("TADR1") : mCustomerDtlReqTS01.Value = CBool(drRow(dcColumn))
                    Case ("TADR2") : mCustomerDtlReqTS02.Value = CBool(drRow(dcColumn))
                    Case ("TADR3") : mCustomerDtlReqTS03.Value = CBool(drRow(dcColumn))
                    Case ("TADR4") : mCustomerDtlReqTS04.Value = CBool(drRow(dcColumn))
                    Case ("TADR5") : mCustomerDtlReqTS05.Value = CBool(drRow(dcColumn))
                    Case ("TADR6") : mCustomerDtlReqTS06.Value = CBool(drRow(dcColumn))
                    Case ("TADR7") : mCustomerDtlReqTS07.Value = CBool(drRow(dcColumn))
                    Case ("TADR8") : mCustomerDtlReqTS08.Value = CBool(drRow(dcColumn))
                    Case ("TADR9") : mCustomerDtlReqTS09.Value = CBool(drRow(dcColumn))
                    Case ("TADR10") : mCustomerDtlReqTS10.Value = CBool(drRow(dcColumn))
                    Case ("TADR11") : mCustomerDtlReqTS11.Value = CBool(drRow(dcColumn))
                    Case ("TADR12") : mCustomerDtlReqTS12.Value = CBool(drRow(dcColumn))
                    Case ("TADR13") : mCustomerDtlReqTS13.Value = CBool(drRow(dcColumn))
                    Case ("TADR14") : mCustomerDtlReqTS14.Value = CBool(drRow(dcColumn))
                    Case ("TADR15") : mCustomerDtlReqTS15.Value = CBool(drRow(dcColumn))
                    Case ("TADR16") : mCustomerDtlReqTS16.Value = CBool(drRow(dcColumn))
                    Case ("TADR17") : mCustomerDtlReqTS17.Value = CBool(drRow(dcColumn))
                    Case ("TADR18") : mCustomerDtlReqTS18.Value = CBool(drRow(dcColumn))
                    Case ("TADR19") : mCustomerDtlReqTS19.Value = CBool(drRow(dcColumn))
                    Case ("TADR20") : mCustomerDtlReqTS20.Value = CBool(drRow(dcColumn))
                    Case ("SAU1") : mMinRefundRequiringSA.Value = CDec(drRow(dcColumn))
                    Case ("SAU2") : mMinRefundRequiringMA.Value = CDec(drRow(dcColumn))
                    Case ("SAU3") : mMinCollDiscTranValSA.Value = CDec(drRow(dcColumn))
                    Case ("SAU4") : mMinColDiscTranValMA.Value = CDec(drRow(dcColumn))
                    Case ("SAU5") : mMaxValGiftToken.Value = CDec(drRow(dcColumn))
                    Case ("MINC") : mMinChgGivenGiftToken.Value = CDec(drRow(dcColumn))
                    Case ("SMAX") : mSalesMaxField.Value = CDec(drRow(dcColumn))
                    Case ("PCAU") : mAutoAppPriceChgAuth.Value = CBool(drRow(dcColumn))
                    Case ("PCMA") : mMgrAuthorisedAA.Value = CStr(drRow(dcColumn))
                    Case ("LOAD") : mHostuLoadInProgress.Value = CStr(drRow(dcColumn))
                    Case ("COUNTRYCODE") : mCountryCode.Value = CStr(drRow(dcColumn))
                    Case ("SPARE")
                    Case Else
                        Trace.WriteLine("Column Missing: " & dcColumn.ColumnName)
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("FKEY", "", True, False, 0, 0, True, True, 1, 0, 0, 1, "", 0, "Retail Options ID")
    Private _Store As New ColField(Of String)("STOR", "", False, False, 0, 0, True, True, 2, 0, 0, 1, "", 0, "Store")
    Private _StoreName As New ColField(Of String)("SNAM", "", False, False, 0, 0, True, True, 3, 0, 0, 1, "", 0, "Store Name")
    Private _AddressLine1 As New ColField(Of String)("SAD1", "", False, False, 0, 0, True, True, 4, 0, 0, 1, "", 0, "Address Line 1")
    Private _AddressLine2 As New ColField(Of String)("SAD2", "", False, False, 0, 0, True, True, 5, 0, 0, 1, "", 0, "Address Line 2")
    Private _AddressLine3 As New ColField(Of String)("SAD3", "", False, False, 0, 0, True, True, 6, 0, 0, 1, "", 0, "Address Line 3")
    Private _AccountabilityType As New ColField(Of String)("TYPE", "", False, False, 0, 0, True, True, 7, 0, 0, 1, "", 0, "Accountability Type")

    Private mHighestValidCashier As New ColField(Of String)("HCAS", "", False, False, 0, 0, True, True, 8, 0, 0, 1, "", 0, "Highest Valid Cashier")
    Private mDefaultFloatAmountAT As New ColField(Of Decimal)("FLOT", 0, False, False, 0, 0, True, True, 9, 0, 0, 1, "", 0, "Default Float Amount AT")
    Private mLstAcctPerRetClosed As New ColField(Of Decimal)("LAST", 0, False, False, 0, 0, True, True, 10, 0, 0, 1, "", 0, "Lst Acct Per Ret Closed")
    Private mNextAcctPeriodToOpen As New ColField(Of Decimal)("NEXT", 0, False, False, 0, 0, True, True, 11, 0, 0, 1, "", 0, "Nxt Acct Period To Open")
    Private mAccoutingPerToKeep As New ColField(Of Decimal)("KEEP", 0, False, False, 0, 0, True, True, 12, 0, 0, 1, "", 0, "Accouting Per to Keep")

    Private mTypeOfSaleNo01 As New ColField(Of String)("TOSN1", "", False, False, 0, 0, True, True, 13, 0, 0, 1, "", 0, "Type Of Sale No 01")
    Private mTypeOfSaleNo02 As New ColField(Of String)("TOSN2", "", False, False, 0, 0, True, True, 14, 0, 0, 1, "", 0, "Type Of Sale No 02")
    Private mTypeOfSaleNo03 As New ColField(Of String)("TOSN3", "", False, False, 0, 0, True, True, 15, 0, 0, 1, "", 0, "Type Of Sale No 03")
    Private mTypeOfSaleNo04 As New ColField(Of String)("TOSN4", "", False, False, 0, 0, True, True, 1, 0, 0, 1, "", 0, "Type Of Sale No 04")
    Private mTypeOfSaleNo05 As New ColField(Of String)("TOSN5", "", False, False, 0, 0, True, True, 17, 0, 0, 1, "", 0, "Type Of Sale No 05")
    Private mTypeOfSaleNo06 As New ColField(Of String)("TOSN6", "", False, False, 0, 0, True, True, 18, 0, 0, 1, "", 0, "Type Of Sale No 06")
    Private mTypeOfSaleNo07 As New ColField(Of String)("TOSN7", "", False, False, 0, 0, True, True, 19, 0, 0, 1, "", 0, "Type Of Sale No 07")
    Private mTypeOfSaleNo08 As New ColField(Of String)("TOSN8", "", False, False, 0, 0, True, True, 20, 0, 0, 1, "", 0, "Type Of Sale No 08")
    Private mTypeOfSaleNo09 As New ColField(Of String)("TOSN9", "", False, False, 0, 0, True, True, 21, 0, 0, 1, "", 0, "Type Of Sale No 09")
    Private mTypeOfSaleNo10 As New ColField(Of String)("TOSN10", "", False, False, 0, 0, True, True, 22, 0, 0, 1, "", 0, "Type Of Sale No 10")
    Private mTypeOfSaleNo11 As New ColField(Of String)("TOSN11", "", False, False, 0, 0, True, True, 23, 0, 0, 1, "", 0, "Type Of Sale No 11")
    Private mTypeOfSaleNo12 As New ColField(Of String)("TOSN12", "", False, False, 0, 0, True, True, 24, 0, 0, 1, "", 0, "Type Of Sale No 12")
    Private mTypeOfSaleNo13 As New ColField(Of String)("TOSN13", "", False, False, 0, 0, True, True, 25, 0, 0, 1, "", 0, "Type Of Sale No 13")
    Private mTypeOfSaleNo14 As New ColField(Of String)("TOSN14", "", False, False, 0, 0, True, True, 26, 0, 0, 1, "", 0, "Type Of Sale No 14")
    Private mTypeOfSaleNo15 As New ColField(Of String)("TOSN15", "", False, False, 0, 0, True, True, 27, 0, 0, 1, "", 0, "Type Of Sale No 15")
    Private mTypeOfSaleNo16 As New ColField(Of String)("TOSN16", "", False, False, 0, 0, True, True, 28, 0, 0, 1, "", 0, "Type Of Sale No 16")
    Private mTypeOfSaleNo17 As New ColField(Of String)("TOSN17", "", False, False, 0, 0, True, True, 29, 0, 0, 1, "", 0, "Type Of Sale No 17")
    Private mTypeOfSaleNo18 As New ColField(Of String)("TOSN18", "", False, False, 0, 0, True, True, 30, 0, 0, 1, "", 0, "Type Of Sale No 18")
    Private mTypeOfSaleNo19 As New ColField(Of String)("TOSN19", "", False, False, 0, 0, True, True, 31, 0, 0, 1, "", 0, "Type Of Sale No 19")
    Private mTypeOfSaleNo20 As New ColField(Of String)("TOSN20", "", False, False, 0, 0, True, True, 32, 0, 0, 1, "", 0, "Type Of Sale No 20")
    Private mTypeOfSaleNo21 As New ColField(Of String)("TOSN21", "", False, False, 0, 0, True, True, 33, 0, 0, 1, "", 0, "Type Of Sale No 21")
    Private mTypeOfSaleNo22 As New ColField(Of String)("TOSN22", "", False, False, 0, 0, True, True, 34, 0, 0, 1, "", 0, "Type Of Sale No 22")
    Private mTypeOfSaleNo23 As New ColField(Of String)("TOSN23", "", False, False, 0, 0, True, True, 35, 0, 0, 1, "", 0, "Type Of Sale No 23")
    Private mTypeOfSaleNo24 As New ColField(Of String)("TOSN24", "", False, False, 0, 0, True, True, 36, 0, 0, 1, "", 0, "Type Of Sale No 24")
    Private mTypeOfSaleNo25 As New ColField(Of String)("TOSN25", "", False, False, 0, 0, True, True, 37, 0, 0, 1, "", 0, "Type Of Sale No 25")
    Private mTypeOfSaleNo26 As New ColField(Of String)("TOSN26", "", False, False, 0, 0, True, True, 38, 0, 0, 1, "", 0, "Type Of Sale No 26")
    Private mTypeOfSaleNo27 As New ColField(Of String)("TOSN27", "", False, False, 0, 0, True, True, 39, 0, 0, 1, "", 0, "Type Of Sale No 27")
    Private mTypeOfSaleNo28 As New ColField(Of String)("TOSN28", "", False, False, 0, 0, True, True, 40, 0, 0, 1, "", 0, "Type Of Sale No 28")
    Private mTypeOfSaleNo29 As New ColField(Of String)("TOSN29", "", False, False, 0, 0, True, True, 41, 0, 0, 1, "", 0, "Type Of Sale No 29")
    Private mTypeOfSaleNo30 As New ColField(Of String)("TOSN30", "", False, False, 0, 0, True, True, 42, 0, 0, 1, "", 0, "Type Of Sale No 30")
    Private mTypeOfSale01 As New ColField(Of String)("TOSC1", "", False, False, 0, 0, True, True, 43, 0, 0, 1, "", 0, "Type Of Sale 01")
    Private mTypeOfSale02 As New ColField(Of String)("TOSC2", "", False, False, 0, 0, True, True, 44, 0, 0, 1, "", 0, "Type Of Sale 02")
    Private mTypeOfSale03 As New ColField(Of String)("TOSC3", "", False, False, 0, 0, True, True, 45, 0, 0, 1, "", 0, "Type Of Sale 03")
    Private mTypeOfSale04 As New ColField(Of String)("TOSC4", "", False, False, 0, 0, True, True, 46, 0, 0, 1, "", 0, "Type Of Sale 04")
    Private mTypeOfSale05 As New ColField(Of String)("TOSC5", "", False, False, 0, 0, True, True, 47, 0, 0, 1, "", 0, "Type Of Sale 05")
    Private mTypeOfSale06 As New ColField(Of String)("TOSC6", "", False, False, 0, 0, True, True, 48, 0, 0, 1, "", 0, "Type Of Sale 06")
    Private mTypeOfSale07 As New ColField(Of String)("TOSC7", "", False, False, 0, 0, True, True, 49, 0, 0, 1, "", 0, "Type Of Sale 07")
    Private mTypeOfSale08 As New ColField(Of String)("TOSC8", "", False, False, 0, 0, True, True, 50, 0, 0, 1, "", 0, "Type Of Sale 08")
    Private mTypeOfSale09 As New ColField(Of String)("TOSC9", "", False, False, 0, 0, True, True, 51, 0, 0, 1, "", 0, "Type Of Sale 09")
    Private mTypeOfSale10 As New ColField(Of String)("TOSC10", "", False, False, 0, 0, True, True, 52, 0, 0, 1, "", 0, "Type Of Sale 10")
    Private mTypeOfSale11 As New ColField(Of String)("TOSC11", "", False, False, 0, 0, True, True, 53, 0, 0, 1, "", 0, "Type Of Sale 11")
    Private mTypeOfSale12 As New ColField(Of String)("TOSC12", "", False, False, 0, 0, True, True, 54, 0, 0, 1, "", 0, "Type Of Sale 12")
    Private mTypeOfSale13 As New ColField(Of String)("TOSC13", "", False, False, 0, 0, True, True, 55, 0, 0, 1, "", 0, "Type Of Sale 13")
    Private mTypeOfSale14 As New ColField(Of String)("TOSC14", "", False, False, 0, 0, True, True, 56, 0, 0, 1, "", 0, "Type Of Sale 14")
    Private mTypeOfSale15 As New ColField(Of String)("TOSC15", "", False, False, 0, 0, True, True, 57, 0, 0, 1, "", 0, "Type Of Sale 15")
    Private mTypeOfSale16 As New ColField(Of String)("TOSC16", "", False, False, 0, 0, True, True, 58, 0, 0, 1, "", 0, "Type Of Sale 16")
    Private mTypeOfSale17 As New ColField(Of String)("TOSC17", "", False, False, 0, 0, True, True, 59, 0, 0, 1, "", 0, "Type Of Sale 17")
    Private mTypeOfSale18 As New ColField(Of String)("TOSC18", "", False, False, 0, 0, True, True, 60, 0, 0, 1, "", 0, "Type Of Sale 18")
    Private mTypeOfSale19 As New ColField(Of String)("TOSC19", "", False, False, 0, 0, True, True, 61, 0, 0, 1, "", 0, "Type Of Sale 19")
    Private mTypeOfSale20 As New ColField(Of String)("TOSC20", "", False, False, 0, 0, True, True, 62, 0, 0, 1, "", 0, "Type Of Sale 20")
    Private mTypeOfSale21 As New ColField(Of String)("TOSC21", "", False, False, 0, 0, True, True, 63, 0, 0, 1, "", 0, "Type Of Sale 21")
    Private mTypeOfSale22 As New ColField(Of String)("TOSC22", "", False, False, 0, 0, True, True, 64, 0, 0, 1, "", 0, "Type Of Sale 22")
    Private mTypeOfSale23 As New ColField(Of String)("TOSC23", "", False, False, 0, 0, True, True, 65, 0, 0, 1, "", 0, "Type Of Sale 23")
    Private mTypeOfSale24 As New ColField(Of String)("TOSC24", "", False, False, 0, 0, True, True, 66, 0, 0, 1, "", 0, "Type Of Sale 24")
    Private mTypeOfSale25 As New ColField(Of String)("TOSC25", "", False, False, 0, 0, True, True, 67, 0, 0, 1, "", 0, "Type Of Sale 25")
    Private mTypeOfSale26 As New ColField(Of String)("TOSC26", "", False, False, 0, 0, True, True, 68, 0, 0, 1, "", 0, "Type Of Sale 26")
    Private mTypeOfSale27 As New ColField(Of String)("TOSC27", "", False, False, 0, 0, True, True, 69, 0, 0, 1, "", 0, "Type Of Sale 27")
    Private mTypeOfSale28 As New ColField(Of String)("TOSC28", "", False, False, 0, 0, True, True, 70, 0, 0, 1, "", 0, "Type Of Sale 28")
    Private mTypeOfSale29 As New ColField(Of String)("TOSC29", "", False, False, 0, 0, True, True, 71, 0, 0, 1, "", 0, "Type Of Sale 29")
    Private mTypeOfSale30 As New ColField(Of String)("TOSC30", "", False, False, 0, 0, True, True, 72, 0, 0, 1, "", 0, "Type Of Sale 30")
    Private mTypeOfSalesDesc01 As New ColField(Of String)("TOSCD1", "", False, False, 0, 0, True, True, 73, 0, 0, 1, "", 0, "Type Of Sales Desc 01")
    Private mTypeOfSalesDesc02 As New ColField(Of String)("TOSCD2", "", False, False, 0, 0, True, True, 74, 0, 0, 1, "", 0, "Type Of Sales Desc 02")
    Private mTypeOfSalesDesc03 As New ColField(Of String)("TOSCD3", "", False, False, 0, 0, True, True, 75, 0, 0, 1, "", 0, "Type Of Sales Desc 03")
    Private mTypeOfSalesDesc04 As New ColField(Of String)("TOSCD4", "", False, False, 0, 0, True, True, 76, 0, 0, 1, "", 0, "Type Of Sales Desc 04")
    Private mTypeOfSalesDesc05 As New ColField(Of String)("TOSCD5", "", False, False, 0, 0, True, True, 77, 0, 0, 1, "", 0, "Type Of Sales Desc 05")
    Private mTypeOfSalesDesc06 As New ColField(Of String)("TOSCD6", "", False, False, 0, 0, True, True, 78, 0, 0, 1, "", 0, "Type Of Sales Desc 06")
    Private mTypeOfSalesDesc07 As New ColField(Of String)("TOSCD7", "", False, False, 0, 0, True, True, 79, 0, 0, 1, "", 0, "Type Of Sales Desc 07")
    Private mTypeOfSalesDesc08 As New ColField(Of String)("TOSCD8", "", False, False, 0, 0, True, True, 80, 0, 0, 1, "", 0, "Type Of Sales Desc 08")
    Private mTypeOfSalesDesc09 As New ColField(Of String)("TOSCD9", "", False, False, 0, 0, True, True, 81, 0, 0, 1, "", 0, "Type Of Sales Desc 09")
    Private mTypeOfSalesDesc10 As New ColField(Of String)("TOSCD10", "", False, False, 0, 0, True, True, 82, 0, 0, 1, "", 0, "Type Of Sales Desc 10")
    Private mTypeOfSalesDesc11 As New ColField(Of String)("TOSCD11", "", False, False, 0, 0, True, True, 83, 0, 0, 1, "", 0, "Type Of Sales Desc 11")
    Private mTypeOfSalesDesc12 As New ColField(Of String)("TOSCD12", "", False, False, 0, 0, True, True, 84, 0, 0, 1, "", 0, "Type Of Sales Desc 12")
    Private mTypeOfSalesDesc13 As New ColField(Of String)("TOSCD13", "", False, False, 0, 0, True, True, 85, 0, 0, 1, "", 0, "Type Of Sales Desc 13")
    Private mTypeOfSalesDesc14 As New ColField(Of String)("TOSCD14", "", False, False, 0, 0, True, True, 86, 0, 0, 1, "", 0, "Type Of Sales Desc 14")
    Private mTypeOfSalesDess15 As New ColField(Of String)("TOSCD15", "", False, False, 0, 0, True, True, 87, 0, 0, 1, "", 0, "Type Of Sales Desc 15")
    Private mTypeOfSalesDesc16 As New ColField(Of String)("TOSCD16", "", False, False, 0, 0, True, True, 88, 0, 0, 1, "", 0, "Type Of Sales Desc 16")
    Private mTypeOfSalesDesc17 As New ColField(Of String)("TOSCD17", "", False, False, 0, 0, True, True, 89, 0, 0, 1, "", 0, "Type Of Sales Desc 17")
    Private mTypeOfSalesDesc18 As New ColField(Of String)("TOSCD18", "", False, False, 0, 0, True, True, 90, 0, 0, 1, "", 0, "Type Of Sales Desc 18")
    Private mTypeOfSalesDesc19 As New ColField(Of String)("TOSCD19", "", False, False, 0, 0, True, True, 91, 0, 0, 1, "", 0, "Type Of Sales Desc 19")
    Private mTypeOfSalesDesc20 As New ColField(Of String)("TOSCD20", "", False, False, 0, 0, True, True, 92, 0, 0, 1, "", 0, "Type Of Sales Desc 20")
    Private mTypeOfSalesDesc21 As New ColField(Of String)("TOSCD21", "", False, False, 0, 0, True, True, 93, 0, 0, 1, "", 0, "Type Of Sales Desc 21")
    Private mTypeOfSalesDesc22 As New ColField(Of String)("TOSCD22", "", False, False, 0, 0, True, True, 94, 0, 0, 1, "", 0, "Type Of Sales Desc 22")
    Private mTypeOfSalesDesc23 As New ColField(Of String)("TOSCD23", "", False, False, 0, 0, True, True, 95, 0, 0, 1, "", 0, "Type Of Sales Desc 23")
    Private mTypeOfSalesDesc24 As New ColField(Of String)("TOSCD24", "", False, False, 0, 0, True, True, 96, 0, 0, 1, "", 0, "Type Of Sales Desc 24")
    Private mTypeOfSalesDesc25 As New ColField(Of String)("TOSCD25", "", False, False, 0, 0, True, True, 97, 0, 0, 1, "", 0, "Type Of Sales Desc 25")
    Private mTypeOfSalesDesc26 As New ColField(Of String)("TOSCD26", "", False, False, 0, 0, True, True, 98, 0, 0, 1, "", 0, "Type Of Sales Desc 26")
    Private mTypeOfSalesDesc27 As New ColField(Of String)("TOSCD27", "", False, False, 0, 0, True, True, 99, 0, 0, 1, "", 0, "Type Of Sales Desc 27")
    Private mTypeOfSalesDesc28 As New ColField(Of String)("TOSCD28", "", False, False, 0, 0, True, True, 100, 0, 0, 1, "", 0, "Type Of Sales Desc 28")
    Private mTypeOfSalesDesc29 As New ColField(Of String)("TOSCD29", "", False, False, 0, 0, True, True, 101, 0, 0, 1, "", 0, "Type Of Sales Desc 29")
    Private mTypeOfSalesDesc30 As New ColField(Of String)("TOSCD30", "", False, False, 0, 0, True, True, 102, 0, 0, 1, "", 0, "Type Of Sales Desc 30")
    Private mOverTenderAllowed01 As New ColField(Of Boolean)("TTOT1", False, False, False, 0, 0, True, True, 103, 0, 0, 1, "", 0, "Tender Allowed 01")
    Private mOverTenderAllowed02 As New ColField(Of Boolean)("TTOT2", False, False, False, 0, 0, True, True, 104, 0, 0, 1, "", 0, "Tender Allowed 02")
    Private mOverTenderAllowed03 As New ColField(Of Boolean)("TTOT3", False, False, False, 0, 0, True, True, 105, 0, 0, 1, "", 0, "Tender Allowed 03")
    Private mOverTenderAllowed04 As New ColField(Of Boolean)("TTOT4", False, False, False, 0, 0, True, True, 106, 0, 0, 1, "", 0, "Tender Allowed 04")
    Private mOverTenderAllowed05 As New ColField(Of Boolean)("TTOT5", False, False, False, 0, 0, True, True, 107, 0, 0, 1, "", 0, "Tender Allowed 05")
    Private mOverTenderAllowed06 As New ColField(Of Boolean)("TTOT6", False, False, False, 0, 0, True, True, 108, 0, 0, 1, "", 0, "Tender Allowed 06")
    Private mOverTenderAllowed07 As New ColField(Of Boolean)("TTOT7", False, False, False, 0, 0, True, True, 109, 0, 0, 1, "", 0, "Tender Allowed 07")
    Private mOverTenderAllowed08 As New ColField(Of Boolean)("TTOT8", False, False, False, 0, 0, True, True, 110, 0, 0, 1, "", 0, "Tender Allowed 08")
    Private mOverTenderAllowed09 As New ColField(Of Boolean)("TTOT9", False, False, False, 0, 0, True, True, 111, 0, 0, 1, "", 0, "Tender Allowed 09")
    Private mOverTenderAllowed10 As New ColField(Of Boolean)("TTOT10", False, False, False, 0, 0, True, True, 112, 0, 0, 1, "", 0, "Tender Allowed 10")
    Private mCreditCardCapture01 As New ColField(Of Boolean)("TTCC1", False, False, False, 0, 0, True, True, 113, 0, 0, 1, "", 0, "Credit Card Capture 01")
    Private mCreditCardCapture02 As New ColField(Of Boolean)("TTCC2", False, False, False, 0, 0, True, True, 114, 0, 0, 1, "", 0, "Credit Card Capture 02")
    Private mCreditCardCapture03 As New ColField(Of Boolean)("TTCC3", False, False, False, 0, 0, True, True, 115, 0, 0, 1, "", 0, "Credit Card Capture 03")
    Private mCreditCardCapture04 As New ColField(Of Boolean)("TTCC4", False, False, False, 0, 0, True, True, 116, 0, 0, 1, "", 0, "Credit Card Capture 04")
    Private mCreditCardCapture05 As New ColField(Of Boolean)("TTCC5", False, False, False, 0, 0, True, True, 117, 0, 0, 1, "", 0, "Credit Card Capture 05")
    Private mCreditCardCapture06 As New ColField(Of Boolean)("TTCC6", False, False, False, 0, 0, True, True, 118, 0, 0, 1, "", 0, "Credit Card Capture 06")
    Private mCreditCardCapture07 As New ColField(Of Boolean)("TTCC7", False, False, False, 0, 0, True, True, 119, 0, 0, 1, "", 0, "Credit Card Capture 07")
    Private mCreditCardCapture08 As New ColField(Of Boolean)("TTCC8", False, False, False, 0, 0, True, True, 120, 0, 0, 1, "", 0, "Credit Card Capture 08")
    Private mCreditCardCapture09 As New ColField(Of Boolean)("TTCC9", False, False, False, 0, 0, True, True, 122, 0, 0, 1, "", 0, "Credit Card Capture 09")
    Private mCreditCardCapture10 As New ColField(Of Boolean)("TTCC10", False, False, False, 0, 0, True, True, 123, 0, 0, 1, "", 0, "Credit Card Capture 10")
    Private mTenderTypeFlagDesc01 As New ColField(Of String)("TTDE1", "", False, False, 0, 0, True, True, 124, 0, 0, 1, "", 0, "Tender Type Flag Desc 01")
    Private mTenderTypeFlagDesc02 As New ColField(Of String)("TTDE2", "", False, False, 0, 0, True, True, 125, 0, 0, 1, "", 0, "Tender Type Flag Desc 02")
    Private mTenderTypeFlagDesc03 As New ColField(Of String)("TTDE3", "", False, False, 0, 0, True, True, 126, 0, 0, 1, "", 0, "Tender Type Flag Desc 03")
    Private mTenderTypeFlagDesc04 As New ColField(Of String)("TTDE4", "", False, False, 0, 0, True, True, 127, 0, 0, 1, "", 0, "Tender Type Flag Desc 04")
    Private mTenderTypeFlagDesc05 As New ColField(Of String)("TTDE5", "", False, False, 0, 0, True, True, 128, 0, 0, 1, "", 0, "Tender Type Flag Desc 05")
    Private mTenderTypeFlagDesc06 As New ColField(Of String)("TTDE6", "", False, False, 0, 0, True, True, 129, 0, 0, 1, "", 0, "Tender Type Flag Desc 06")
    Private mTenderTypeFlagDesc07 As New ColField(Of String)("TTDE7", "", False, False, 0, 0, True, True, 130, 0, 0, 1, "", 0, "Tender Type Flag Desc 07")
    Private mTenderTypeFlagDesc08 As New ColField(Of String)("TTDE8", "", False, False, 0, 0, True, True, 131, 0, 0, 1, "", 0, "Tender Type Flag Desc 08")
    Private mTenderTypeFlagDesc09 As New ColField(Of String)("TTDE9", "", False, False, 0, 0, True, True, 132, 0, 0, 1, "", 0, "Tender Type Flag Desc 09")
    Private mTenderTypeFlagDesc10 As New ColField(Of String)("TTDE10", "", False, False, 0, 0, True, True, 133, 0, 0, 1, "", 0, "Tender Type Flag Desc 10")
    Private mTenderTypeFlagDesc11 As New ColField(Of String)("TTDE11", "", False, False, 0, 0, True, True, 369, 0, 0, 1, "", 0, "Tender Type Flag Desc 11")
    Private mTenderTypeFlagDesc12 As New ColField(Of String)("TTDE12", "", False, False, 0, 0, True, True, 370, 0, 0, 1, "", 0, "Tender Type Flag Desc 12")
    Private mTenderTypeFlagDesc13 As New ColField(Of String)("TTDE13", "", False, False, 0, 0, True, True, 371, 0, 0, 1, "", 0, "Tender Type Flag Desc 13")
    Private mTenderTypeFlagDesc14 As New ColField(Of String)("TTDE14", "", False, False, 0, 0, True, True, 372, 0, 0, 1, "", 0, "Tender Type Flag Desc 14")
    Private mTenderTypeFlagDesc15 As New ColField(Of String)("TTDE15", "", False, False, 0, 0, True, True, 373, 0, 0, 1, "", 0, "Tender Type Flag Desc 15")
    Private mTenderTypeFlagDesc16 As New ColField(Of String)("TTDE16", "", False, False, 0, 0, True, True, 374, 0, 0, 1, "", 0, "Tender Type Flag Desc 16")
    Private mTenderTypeFlagDesc17 As New ColField(Of String)("TTDE17", "", False, False, 0, 0, True, True, 375, 0, 0, 1, "", 0, "Tender Type Flag Desc 17")
    Private mTenderTypeFlagDesc18 As New ColField(Of String)("TTDE18", "", False, False, 0, 0, True, True, 376, 0, 0, 1, "", 0, "Tender Type Flag Desc 18")
    Private mTenderTypeFlagDesc19 As New ColField(Of String)("TTDE19", "", False, False, 0, 0, True, True, 377, 0, 0, 1, "", 0, "Tender Type Flag Desc 19")
    Private mTenderTypeFlagDesc20 As New ColField(Of String)("TTDE20", "", False, False, 0, 0, True, True, 378, 0, 0, 1, "", 0, "Tender Type Flag Desc 20")
    Private mOpenDrawReasonCode01 As New ColField(Of String)("ODRC1", "", False, False, 0, 0, True, True, 134, 0, 0, 1, "", 0, "Open Drawer Reason Code 01")
    Private mOpenDrawReasonCode02 As New ColField(Of String)("ODRC2", "", False, False, 0, 0, True, True, 135, 0, 0, 1, "", 0, "Open Drawer Reason Code 02")
    Private mOpenDrawReasonCode03 As New ColField(Of String)("ODRC3", "", False, False, 0, 0, True, True, 136, 0, 0, 1, "", 0, "Open Drawer Reason Code 03")
    Private mOpenDrawReasonCode04 As New ColField(Of String)("ODRC4", "", False, False, 0, 0, True, True, 137, 0, 0, 1, "", 0, "Open Drawer Reason Code 04")
    Private mOpenDrawReasonCode05 As New ColField(Of String)("ODRC5", "", False, False, 0, 0, True, True, 138, 0, 0, 1, "", 0, "Open Drawer Reason Code 05")
    Private mOpenDrawReasonCode06 As New ColField(Of String)("ODRC6", "", False, False, 0, 0, True, True, 139, 0, 0, 1, "", 0, "Open Drawer Reason Code 06")
    Private mOpenDrawReasonCode07 As New ColField(Of String)("ODRC7", "", False, False, 0, 0, True, True, 140, 0, 0, 1, "", 0, "Open Drawer Reason Code 07")
    Private mOpenDrawReasonCode08 As New ColField(Of String)("ODRC8", "", False, False, 0, 0, True, True, 141, 0, 0, 1, "", 0, "Open Drawer Reason Code 08")
    Private mOpenDrawReasonCode09 As New ColField(Of String)("ODRC9", "", False, False, 0, 0, True, True, 142, 0, 0, 1, "", 0, "Open Drawer Reason Code 09")
    Private mOpenDrawReasonCode10 As New ColField(Of String)("ODRC10", "", False, False, 0, 0, True, True, 143, 0, 0, 1, "", 0, "Open Drawer Reason Code 10")
    Private mLastReformatDate As New ColField(Of Date)("DOLR", Nothing, False, False, 0, 0, True, True, 144, 0, 0, 1, "", 0, "Last Reformat Date")
    Private mLastUpdateDate As New ColField(Of Date)("DOLU", Nothing, False, False, 0, 0, True, True, 145, 0, 0, 1, "", 0, "Last Update Date")
    Private mAutoAppPriceChanges As New ColField(Of Boolean)("PCAA", False, False, False, 0, 0, True, True, 146, 0, 0, 1, "", 0, "Auto App Price Changes")
    Private mNoDayPstEffectDatePC As New ColField(Of Decimal)("PCDA", 0, False, False, 0, 0, True, True, 147, 0, 0, 1, "", 0, "No Day Pst Effect Date PC")
    Private mPrintShelfEdgeLblPC As New ColField(Of Boolean)("PSEL", False, False, False, 0, 0, True, True, 148, 0, 0, 1, "", 0, "Print Shelf Edge Lbl PC")
    Private _CompanyName As New ColField(Of String)("CNAM", "", False, False, 0, 0, True, True, 149, 0, 0, 1, "", 0, "Company Name")
    Private mCompanyAddress1 As New ColField(Of String)("CAD1", "", False, False, 0, 0, True, True, 150, 0, 0, 1, "", 0, "Company Address 1")
    Private mCompanyAddress2 As New ColField(Of String)("CAD2", "", False, False, 0, 0, True, True, 151, 0, 0, 1, "", 0, "Company Address 2")
    Private mCompanyAddress3 As New ColField(Of String)("CAD3", "", False, False, 0, 0, True, True, 152, 0, 0, 1, "", 0, "Company Address 3")
    Private mCompanyTelephone As New ColField(Of String)("CTEL", "", False, False, 0, 0, True, True, 153, 0, 0, 1, "", 0, "Company Telephone")
    Private mCompanyTelex As New ColField(Of String)("CTLX", "", False, False, 0, 0, True, True, 154, 0, 0, 1, "", 0, "Company Telex")
    Private mTenderNoCheques As New ColField(Of Decimal)("CHON", 0, False, False, 0, 0, True, True, 155, 0, 0, 1, "", 0, "Tender No Cheques")
    Private mTenderNoVisa As New ColField(Of Decimal)("VION", 0, False, False, 0, 0, True, True, 156, 0, 0, 1, "", 0, "Tender No Visa")
    Private mTenderNoAccess As New ColField(Of Decimal)("MCON", 0, False, False, 0, 0, True, True, 157, 0, 0, 1, "", 0, "Tender No Access")
    Private mTenderNoAmex As New ColField(Of Decimal)("AXON", 0, False, False, 0, 0, True, True, 158, 0, 0, 1, "", 0, "Tender No Amex")
    Private mTenderNoProjectLoan As New ColField(Of Decimal)("WION", 0, False, False, 0, 0, True, True, 159, 0, 0, 1, "", 0, "Tender No Project Loan")
    Private mTenderNoStoreVouchs As New ColField(Of Decimal)("SVON", 0, False, False, 0, 0, True, True, 160, 0, 0, 1, "", 0, "Tender No Store Vouchs")
    Private mTenderNoSwitch As New ColField(Of Decimal)("SWON", 0, False, False, 0, 0, True, True, 161, 0, 0, 1, "", 0, "Tender No Switch")
    Private mTenderNoAccBuildMate As New ColField(Of Decimal)("ACON", 0, False, False, 0, 0, True, True, 162, 0, 0, 1, "", 0, "Tender No Acc Build Mate")
    Private mMiscIncReasonCode01 As New ColField(Of String)("MPRC1", "", False, False, 0, 0, True, True, 163, 0, 0, 1, "", 0, "Misc Inc Reason Code 01")
    Private mMiscIncReasonCode02 As New ColField(Of String)("MPRC2", "", False, False, 0, 0, True, True, 164, 0, 0, 1, "", 0, "Misc Inc Reason Code 02")
    Private mMiscIncReasonCode03 As New ColField(Of String)("MPRC3", "", False, False, 0, 0, True, True, 165, 0, 0, 1, "", 0, "Misc Inc Reason Code 03")
    Private mMiscIncReasonCode04 As New ColField(Of String)("MPRC4", "", False, False, 0, 0, True, True, 166, 0, 0, 1, "", 0, "Misc Inc Reason Code 04")
    Private mMiscIncReasonCode05 As New ColField(Of String)("MPRC5", "", False, False, 0, 0, True, True, 167, 0, 0, 1, "", 0, "Misc Inc Reason Code 05")
    Private mMiscIncReasonCode06 As New ColField(Of String)("MPRC6", "", False, False, 0, 0, True, True, 168, 0, 0, 1, "", 0, "Misc Inc Reason Code 06")
    Private mMiscIncReasonCode07 As New ColField(Of String)("MPRC7", "", False, False, 0, 0, True, True, 169, 0, 0, 1, "", 0, "Misc Inc Reason Code 07")
    Private mMiscIncReasonCode08 As New ColField(Of String)("MPRC8", "", False, False, 0, 0, True, True, 170, 0, 0, 1, "", 0, "Misc Inc Reason Code 08")
    Private mMiscIncReasonCode09 As New ColField(Of String)("MPRC9", "", False, False, 0, 0, True, True, 171, 0, 0, 1, "", 0, "Misc Inc Reason Code 09")
    Private mMiscIncReasonCode10 As New ColField(Of String)("MPRC10", "", False, False, 0, 0, True, True, 172, 0, 0, 1, "", 0, "Misc Inc Reason Code 10")
    Private mMiscIncReasonCode11 As New ColField(Of String)("MPRC11", "", False, False, 0, 0, True, True, 173, 0, 0, 1, "", 0, "Misc Inc Reason Code 11")
    Private mMiscIncReasonCode12 As New ColField(Of String)("MPRC12", "", False, False, 0, 0, True, True, 174, 0, 0, 1, "", 0, "Misc Inc Reason Code 12")
    Private mMiscIncReasonCode13 As New ColField(Of String)("MPRC13", "", False, False, 0, 0, True, True, 175, 0, 0, 1, "", 0, "Misc Inc Reason Code 13")
    Private mMiscIncReasonCode14 As New ColField(Of String)("MPRC14", "", False, False, 0, 0, True, True, 176, 0, 0, 1, "", 0, "Misc Inc Reason Code 14")
    Private mMiscIncReasonCode15 As New ColField(Of String)("MPRC15", "", False, False, 0, 0, True, True, 177, 0, 0, 1, "", 0, "Misc Inc Reason Code 15")
    Private mMiscIncReasonCode16 As New ColField(Of String)("MPRC16", "", False, False, 0, 0, True, True, 178, 0, 0, 1, "", 0, "Misc Inc Reason Code 16")
    Private mMiscIncReasonCode17 As New ColField(Of String)("MPRC17", "", False, False, 0, 0, True, True, 179, 0, 0, 1, "", 0, "Misc Inc Reason Code 17")
    Private mMiscIncReasonCode18 As New ColField(Of String)("MPRC18", "", False, False, 0, 0, True, True, 180, 0, 0, 1, "", 0, "Misc Inc Reason Code 18")
    Private mMiscIncReasonCode19 As New ColField(Of String)("MPRC19", "", False, False, 0, 0, True, True, 181, 0, 0, 1, "", 0, "Misc Inc Reason Code 19")
    Private mMiscIncReasonCode20 As New ColField(Of String)("MPRC20", "", False, False, 0, 0, True, True, 182, 0, 0, 1, "", 0, "Misc Inc Reason Code 20")
    Private mStoreDepositsInNo As New ColField(Of Decimal)("SDIO", 0, False, False, 0, 0, True, True, 183, 0, 0, 1, "", 0, "Store Deposits In Number")
    Private mMiscOutReasonCode01 As New ColField(Of String)("MMRC1", "", False, False, 0, 0, True, True, 184, 0, 0, 1, "", 0, "Misc Out Reason Code 01")
    Private mMiscOutReasonCode02 As New ColField(Of String)("MMRC2", "", False, False, 0, 0, True, True, 185, 0, 0, 1, "", 0, "Misc Out Reason Code 02")
    Private mMiscOutReasonCode03 As New ColField(Of String)("MMRC3", "", False, False, 0, 0, True, True, 186, 0, 0, 1, "", 0, "Misc Out Reason Code 03")
    Private mMiscOutReasonCode04 As New ColField(Of String)("MMRC4", "", False, False, 0, 0, True, True, 187, 0, 0, 1, "", 0, "Misc Out Reason Code 04")
    Private mMiscOutReasonCode05 As New ColField(Of String)("MMRC5", "", False, False, 0, 0, True, True, 188, 0, 0, 1, "", 0, "Misc Out Reason Code 05")
    Private mMiscOutReasonCode06 As New ColField(Of String)("MMRC6", "", False, False, 0, 0, True, True, 189, 0, 0, 1, "", 0, "Misc Out Reason Code 06")
    Private mMiscOutReasonCode07 As New ColField(Of String)("MMRC7", "", False, False, 0, 0, True, True, 190, 0, 0, 1, "", 0, "Misc Out Reason Code 07")
    Private mMiscOutReasonCode08 As New ColField(Of String)("MMRC8", "", False, False, 0, 0, True, True, 191, 0, 0, 1, "", 0, "Misc Out Reason Code 08")
    Private mMiscOutReasonCode09 As New ColField(Of String)("MMRC9", "", False, False, 0, 0, True, True, 192, 0, 0, 1, "", 0, "Misc Out Reason Code 09")
    Private mMiscOutReasonCode10 As New ColField(Of String)("MMRC10", "", False, False, 0, 0, True, True, 193, 0, 0, 1, "", 0, "Misc Out Reason Code 10")
    Private mMiscOutReasonCode11 As New ColField(Of String)("MMRC11", "", False, False, 0, 0, True, True, 194, 0, 0, 1, "", 0, "Misc Out Reason Code 11")
    Private mMiscOutReasonCode12 As New ColField(Of String)("MMRC12", "", False, False, 0, 0, True, True, 195, 0, 0, 1, "", 0, "Misc Out Reason Code 12")
    Private mMiscOutReasonCode13 As New ColField(Of String)("MMRC13", "", False, False, 0, 0, True, True, 196, 0, 0, 1, "", 0, "Misc Out Reason Code 13")
    Private mMiscOutReasonCode14 As New ColField(Of String)("MMRC14", "", False, False, 0, 0, True, True, 197, 0, 0, 1, "", 0, "Misc Out Reason Code 14")
    Private mMiscOutReasonCode15 As New ColField(Of String)("MMRC15", "", False, False, 0, 0, True, True, 198, 0, 0, 1, "", 0, "Misc Out Reason Code 15")
    Private mMiscOutReasonCode16 As New ColField(Of String)("MMRC16", "", False, False, 0, 0, True, True, 199, 0, 0, 1, "", 0, "Misc Out Reason Code 16")
    Private mMiscOutReasonCode17 As New ColField(Of String)("MMRC17", "", False, False, 0, 0, True, True, 200, 0, 0, 1, "", 0, "Misc Out Reason Code 17")
    Private mMiscOutReasonCode18 As New ColField(Of String)("MMRC18", "", False, False, 0, 0, True, True, 201, 0, 0, 1, "", 0, "Misc Out Reason Code 18")
    Private mMiscOutReasonCode19 As New ColField(Of String)("MMRC19", "", False, False, 0, 0, True, True, 202, 0, 0, 1, "", 0, "Misc Out Reason Code 19")
    Private mMiscOutReasonCode20 As New ColField(Of String)("MMRC20", "", False, False, 0, 0, True, True, 203, 0, 0, 1, "", 0, "Misc Out Reason Code 20")
    Private mStoreDespostisOutNo As New ColField(Of Decimal)("SDOO", 0, False, False, 0, 0, True, True, 204, 0, 0, 1, "", 0, "Store Depoasit Is Out No")
    Private mPriceOvrdReasonCde01 As New ColField(Of String)("PORC1", "", False, False, 0, 0, True, True, 205, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 01")
    Private mPriceOvrdReasonCde02 As New ColField(Of String)("PORC2", "", False, False, 0, 0, True, True, 206, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 02")
    Private mPriceOvrdReasonCde03 As New ColField(Of String)("PORC3", "", False, False, 0, 0, True, True, 207, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 03")
    Private mPriceOvrdReasonCde04 As New ColField(Of String)("PORC4", "", False, False, 0, 0, True, True, 208, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 04")
    Private mPriceOvrdReasonCde05 As New ColField(Of String)("PORC5", "", False, False, 0, 0, True, True, 209, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 05")
    Private mPriceOvrdReasonCde06 As New ColField(Of String)("PORC6", "", False, False, 0, 0, True, True, 210, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 06")
    Private mPriceOvrdReasonCde07 As New ColField(Of String)("PORC7", "", False, False, 0, 0, True, True, 211, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 07")
    Private mPriceOvrdReasonCde08 As New ColField(Of String)("PORC8", "", False, False, 0, 0, True, True, 212, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 08")
    Private mPriceOvrdReasonCde09 As New ColField(Of String)("PORC9", "", False, False, 0, 0, True, True, 213, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 09")
    Private mPriceOvrdReasonCde10 As New ColField(Of String)("PORC10", "", False, False, 0, 0, True, True, 214, 0, 0, 1, "", 0, "Price Ovrd Reason Cde 10")
    Private mMaxCashChangeGiven As New ColField(Of Decimal)("MAXC", 0, False, False, 0, 0, True, True, 215, 0, 0, 1, "", 0, "Max Cash Change Given")
    Private mCreditCardFloorLimit As New ColField(Of Decimal)("CCFL", 0, False, False, 0, 0, True, True, 216, 0, 0, 1, "", 0, "Credit Card Floor Limit")
    Private mNoVATrates As New ColField(Of Decimal)("VATN", 0, False, False, 0, 0, True, True, 217, 0, 0, 1, "", 0, "No VAT Rates")
    Private mVATRatesInclusive As New ColField(Of Boolean)("VATI", False, False, False, 0, 0, True, True, 218, 0, 0, 1, "", 0, "VAT Rates Inclusive")
    Private mVATRate01 As New ColField(Of Decimal)("VATR1", 0, False, False, 0, 0, True, True, 219, 0, 0, 1, "", 0, "VAT Rate 01")
    Private mVATRate02 As New ColField(Of Decimal)("VATR2", 0, False, False, 0, 0, True, True, 220, 0, 0, 1, "", 0, "VAT Rate 02")
    Private mVATRate03 As New ColField(Of Decimal)("VATR3", 0, False, False, 0, 0, True, True, 221, 0, 0, 1, "", 0, "VAT Rate 03")
    Private mVATRate04 As New ColField(Of Decimal)("VATR4", 0, False, False, 0, 0, True, True, 222, 0, 0, 1, "", 0, "VAT Rate 04")
    Private mVATRate05 As New ColField(Of Decimal)("VATR5", 0, False, False, 0, 0, True, True, 223, 0, 0, 1, "", 0, "VAT Rate 05")
    Private mVATRate06 As New ColField(Of Decimal)("VATR6", 0, False, False, 0, 0, True, True, 224, 0, 0, 1, "", 0, "VAT Rate 06")
    Private mVATRate07 As New ColField(Of Decimal)("VATR7", 0, False, False, 0, 0, True, True, 225, 0, 0, 1, "", 0, "VAT Rate 07")
    Private mVATRate08 As New ColField(Of Decimal)("VATR8", 0, False, False, 0, 0, True, True, 226, 0, 0, 1, "", 0, "VAT Rate 08")
    Private mVATRate09 As New ColField(Of Decimal)("VATR9", 0, False, False, 0, 0, True, True, 227, 0, 0, 1, "", 0, "VAT Rate 09")
    Private mHighestValDiscPcent As New ColField(Of Decimal)("VDIS", 0, False, False, 0, 0, True, True, 228, 0, 0, 1, "", 0, "Highest Val Disc Pcent")
    Private mTenderTypeDesc01 As New ColField(Of String)("TDES1", "", False, False, 0, 0, True, True, 229, 0, 0, 1, "", 0, "Tender Type Desc 01")
    Private mTenderTypeDesc02 As New ColField(Of String)("TDES2", "", False, False, 0, 0, True, True, 230, 0, 0, 1, "", 0, "Tender Type Desc 02")
    Private mTenderTypeDesc03 As New ColField(Of String)("TDES3", "", False, False, 0, 0, True, True, 231, 0, 0, 1, "", 0, "Tender Type Desc 03")
    Private mTenderTypeDesc04 As New ColField(Of String)("TDES4", "", False, False, 0, 0, True, True, 232, 0, 0, 1, "", 0, "Tender Type Desc 04")
    Private mTenderTypeDesc05 As New ColField(Of String)("TDES5", "", False, False, 0, 0, True, True, 233, 0, 0, 1, "", 0, "Tender Type Desc 05")
    Private mTenderTypeDesc06 As New ColField(Of String)("TDES6", "", False, False, 0, 0, True, True, 234, 0, 0, 1, "", 0, "Tender Type Desc 06")
    Private mTenderTypeDesc07 As New ColField(Of String)("TDES7", "", False, False, 0, 0, True, True, 235, 0, 0, 1, "", 0, "Tender Type Desc 07")
    Private mTenderTypeDesc08 As New ColField(Of String)("TDES8", "", False, False, 0, 0, True, True, 236, 0, 0, 1, "", 0, "Tender Type Desc 08")
    Private mTenderTypeDesc09 As New ColField(Of String)("TDES9", "", False, False, 0, 0, True, True, 237, 0, 0, 1, "", 0, "Tender Type Desc 09")
    Private mTenderTypeDesc10 As New ColField(Of String)("TDES10", "", False, False, 0, 0, True, True, 238, 0, 0, 1, "", 0, "Tender Type Desc 10")
    Private mTenderTypeDesc11 As New ColField(Of String)("TDES11", "", False, False, 0, 0, True, True, 239, 0, 0, 1, "", 0, "Tender Type Desc 11")
    Private mTenderTypeDesc12 As New ColField(Of String)("TDES12", "", False, False, 0, 0, True, True, 240, 0, 0, 1, "", 0, "Tender Type Desc 12")
    Private mTenderTypeDesc13 As New ColField(Of String)("TDES13", "", False, False, 0, 0, True, True, 241, 0, 0, 1, "", 0, "Tender Type Desc 13")
    Private mTenderTypeDesc14 As New ColField(Of String)("TDES14", "", False, False, 0, 0, True, True, 242, 0, 0, 1, "", 0, "Tender Type Desc 14")
    Private mTenderTypeDesc15 As New ColField(Of String)("TDES15", "", False, False, 0, 0, True, True, 243, 0, 0, 1, "", 0, "Tender Type Desc 15")
    Private mTenderTypeDesc16 As New ColField(Of String)("TDES16", "", False, False, 0, 0, True, True, 244, 0, 0, 1, "", 0, "Tender Type Desc 16")
    Private mTenderTypeDesc17 As New ColField(Of String)("TDES17", "", False, False, 0, 0, True, True, 245, 0, 0, 1, "", 0, "Tender Type Desc 17")
    Private mTenderTypeDesc18 As New ColField(Of String)("TDES18", "", False, False, 0, 0, True, True, 246, 0, 0, 1, "", 0, "Tender Type Desc 18")
    Private mTenderTypeDesc19 As New ColField(Of String)("TDES19", "", False, False, 0, 0, True, True, 247, 0, 0, 1, "", 0, "Tender Type Desc 19")
    Private mTenderTypeDesc20 As New ColField(Of String)("TDESC20", "", False, False, 0, 0, True, True, 248, 0, 0, 1, "", 0, "Tender Type Desc 20")
    Private mTenNoThisRelatesTo01 As New ColField(Of Decimal)("TATC1", 0, False, False, 0, 0, True, True, 249, 0, 0, 1, "", 0, "Ten No This Relates To 01")
    Private mTenNoThisRelatesTo02 As New ColField(Of Decimal)("TATC2", 0, False, False, 0, 0, True, True, 250, 0, 0, 1, "", 0, "Ten No This Relates To 02")
    Private mTenNoThisRelatesTo03 As New ColField(Of Decimal)("TATC3", 0, False, False, 0, 0, True, True, 251, 0, 0, 1, "", 0, "Ten No This Relates To 03")
    Private mTenNoThisRelatesTo04 As New ColField(Of Decimal)("TATC4", 0, False, False, 0, 0, True, True, 252, 0, 0, 1, "", 0, "Ten No This Relates To 04")
    Private mTenNoThisRelatesTo05 As New ColField(Of Decimal)("TATC5", 0, False, False, 0, 0, True, True, 253, 0, 0, 1, "", 0, "Ten No This Relates To 05")
    Private mTenNoThisRelatesTo06 As New ColField(Of Decimal)("TATC6", 0, False, False, 0, 0, True, True, 254, 0, 0, 1, "", 0, "Ten No This Relates To 06")
    Private mTenNoThisRelatesTo07 As New ColField(Of Decimal)("TATC7", 0, False, False, 0, 0, True, True, 255, 0, 0, 1, "", 0, "Ten No This Relates To 07")
    Private mTenNoThisRelatesTo08 As New ColField(Of Decimal)("TATC8", 0, False, False, 0, 0, True, True, 256, 0, 0, 1, "", 0, "Ten No This Relates To 08")
    Private mTenNoThisRelatesTo09 As New ColField(Of Decimal)("TATC9", 0, False, False, 0, 0, True, True, 257, 0, 0, 1, "", 0, "Ten No This Relates To 09")
    Private mTenNoThisRelatesTo10 As New ColField(Of Decimal)("TATC10", 0, False, False, 0, 0, True, True, 258, 0, 0, 1, "", 0, "Ten No This Relates To 10")
    Private mTenNoThisRelatesTo11 As New ColField(Of Decimal)("TATC11", 0, False, False, 0, 0, True, True, 259, 0, 0, 1, "", 0, "Ten No This Relates To 11")
    Private mTenNoThisRelatesTo12 As New ColField(Of Decimal)("TATC12", 0, False, False, 0, 0, True, True, 260, 0, 0, 1, "", 0, "Ten No This Relates To 12")
    Private mTenNoThisRelatesTo13 As New ColField(Of Decimal)("TATC13", 0, False, False, 0, 0, True, True, 261, 0, 0, 1, "", 0, "Ten No This Relates To 13")
    Private mTenNoThisRelatesTo14 As New ColField(Of Decimal)("TATC14", 0, False, False, 0, 0, True, True, 262, 0, 0, 1, "", 0, "Ten No This Relates To 14")
    Private mTenNoThisRelatesTo15 As New ColField(Of Decimal)("TATC15", 0, False, False, 0, 0, True, True, 263, 0, 0, 1, "", 0, "Ten No This Relates To 15")
    Private mTenNoThisRelatesTo16 As New ColField(Of Decimal)("TATC16", 0, False, False, 0, 0, True, True, 264, 0, 0, 1, "", 0, "Ten No This Relates To 16")
    Private mTenNoThisRelatesTo17 As New ColField(Of Decimal)("TATC17", 0, False, False, 0, 0, True, True, 265, 0, 0, 1, "", 0, "Ten No This Relates To 17")
    Private mTenNoThisRelatesTo18 As New ColField(Of Decimal)("TATC18", 0, False, False, 0, 0, True, True, 266, 0, 0, 1, "", 0, "Ten No This Relates To 18")
    Private mTenNoThisRelatesTo19 As New ColField(Of Decimal)("TATC19", 0, False, False, 0, 0, True, True, 267, 0, 0, 1, "", 0, "Ten No This Relates To 19")
    Private mTenNoThisRelatesTo20 As New ColField(Of Decimal)("TATC20", 0, False, False, 0, 0, True, True, 268, 0, 0, 1, "", 0, "Ten No This Relates To 20")
    Private mOpenCashDrawer01 As New ColField(Of Boolean)("TODR1", False, False, False, 0, 0, True, True, 269, 0, 0, 1, "", 0, "Open Cash Drawer 01")
    Private mOpenCashDrawer02 As New ColField(Of Boolean)("TODR2", False, False, False, 0, 0, True, True, 270, 0, 0, 1, "", 0, "Open Cash Drawer 02")
    Private mOpenCashDrawer03 As New ColField(Of Boolean)("TODR3", False, False, False, 0, 0, True, True, 271, 0, 0, 1, "", 0, "Open Cash Drawer 03")
    Private mOpenCashDrawer04 As New ColField(Of Boolean)("TODR4", False, False, False, 0, 0, True, True, 272, 0, 0, 1, "", 0, "Open Cash Drawer 04")
    Private mOpenCashDrawer05 As New ColField(Of Boolean)("TODR5", False, False, False, 0, 0, True, True, 273, 0, 0, 1, "", 0, "Open Cash Drawer 05")
    Private mOpenCashDrawer06 As New ColField(Of Boolean)("TODR6", False, False, False, 0, 0, True, True, 274, 0, 0, 1, "", 0, "Open Cash Drawer 06")
    Private mOpenCashDrawer07 As New ColField(Of Boolean)("TODR7", False, False, False, 0, 0, True, True, 275, 0, 0, 1, "", 0, "Open Cash Drawer 07")
    Private mOpenCashDrawer08 As New ColField(Of Boolean)("TODR8", False, False, False, 0, 0, True, True, 276, 0, 0, 1, "", 0, "Open Cash Drawer 08")
    Private mOpenCashDrawer09 As New ColField(Of Boolean)("TODR9", False, False, False, 0, 0, True, True, 277, 0, 0, 1, "", 0, "Open Cash Drawer 09")
    Private mOpenCashDrawer10 As New ColField(Of Boolean)("TODR10", False, False, False, 0, 0, True, True, 278, 0, 0, 1, "", 0, "Open Cash Drawer 10")
    Private mOpenCashDrawer11 As New ColField(Of Boolean)("TODR11", False, False, False, 0, 0, True, True, 279, 0, 0, 1, "", 0, "Open Cash Drawer 11")
    Private mOpenCashDrawer12 As New ColField(Of Boolean)("TODR12", False, False, False, 0, 0, True, True, 280, 0, 0, 1, "", 0, "Open Cash Drawer 12")
    Private mOpenCashDrawer13 As New ColField(Of Boolean)("TODR13", False, False, False, 0, 0, True, True, 281, 0, 0, 1, "", 0, "Open Cash Drawer 13")
    Private mOpenCashDrawer14 As New ColField(Of Boolean)("TODR14", False, False, False, 0, 0, True, True, 282, 0, 0, 1, "", 0, "Open Cash Drawer 14")
    Private mOpenCashDrawer15 As New ColField(Of Boolean)("TODR15", False, False, False, 0, 0, True, True, 283, 0, 0, 1, "", 0, "Open Cash Drawer 15")
    Private mOpenCashDrawer16 As New ColField(Of Boolean)("TODR16", False, False, False, 0, 0, True, True, 284, 0, 0, 1, "", 0, "Open Cash Drawer 16")
    Private mOpenCashDrawer17 As New ColField(Of Boolean)("TODR17", False, False, False, 0, 0, True, True, 285, 0, 0, 1, "", 0, "Open Cash Drawer 17")
    Private mOpenCashDrawer18 As New ColField(Of Boolean)("TODR18", False, False, False, 0, 0, True, True, 286, 0, 0, 1, "", 0, "Open Cash Drawer 18")
    Private mOpenCashDrawer19 As New ColField(Of Boolean)("TODR19", False, False, False, 0, 0, True, True, 287, 0, 0, 1, "", 0, "Open Cash Drawer 19")
    Private mOpenCashDrawer20 As New ColField(Of Boolean)("TODR20", False, False, False, 0, 0, True, True, 288, 0, 0, 1, "", 0, "Open Cash Drawer 20")
    Private mDefaultRemainAmt01 As New ColField(Of Boolean)("TADA1", False, False, False, 0, 0, True, True, 289, 0, 0, 1, "", 0, "Default Remain Amt 01")
    Private mDefaultRemainAmt02 As New ColField(Of Boolean)("TADA2", False, False, False, 0, 0, True, True, 290, 0, 0, 1, "", 0, "Default Remain Amt 02")
    Private mDefaultRemainAmt03 As New ColField(Of Boolean)("TADA3", False, False, False, 0, 0, True, True, 291, 0, 0, 1, "", 0, "Default Remain Amt 03")
    Private mDefaultRemainAmt04 As New ColField(Of Boolean)("TADA4", False, False, False, 0, 0, True, True, 292, 0, 0, 1, "", 0, "Default Remain Amt 04")
    Private mDefaultRemainAmt05 As New ColField(Of Boolean)("TADA5", False, False, False, 0, 0, True, True, 293, 0, 0, 1, "", 0, "Default Remain Amt 05")
    Private mDefaultRemainAm06 As New ColField(Of Boolean)("TADA6", False, False, False, 0, 0, True, True, 294, 0, 0, 1, "", 0, "Default Remain Amt 06")
    Private mDefaultRemainAmt07 As New ColField(Of Boolean)("TADA7", False, False, False, 0, 0, True, True, 295, 0, 0, 1, "", 0, "Default Remain Amt 07")
    Private mDefaultRemainAmt08 As New ColField(Of Boolean)("TADA8", False, False, False, 0, 0, True, True, 296, 0, 0, 1, "", 0, "Default Remain Amt 08")
    Private mDefaultRemainAmt09 As New ColField(Of Boolean)("TADA9", False, False, False, 0, 0, True, True, 297, 0, 0, 1, "", 0, "Default Remain Amt 09")
    Private mDefaultRemainAmt10 As New ColField(Of Boolean)("TADA10", False, False, False, 0, 0, True, True, 298, 0, 0, 1, "", 0, "Default Remain Amt 10")
    Private mDefaultRemainAmt11 As New ColField(Of Boolean)("TADA11", False, False, False, 0, 0, True, True, 299, 0, 0, 1, "", 0, "Default Remain Amt 11")
    Private mDefaultRemainAmt12 As New ColField(Of Boolean)("TADA12", False, False, False, 0, 0, True, True, 300, 0, 0, 1, "", 0, "Default Remain Amt 12")
    Private mDefaultRemainAmt13 As New ColField(Of Boolean)("TADA13", False, False, False, 0, 0, True, True, 301, 0, 0, 1, "", 0, "Default Remain Amt 13")
    Private mDefaultRemainAmt14 As New ColField(Of Boolean)("TADA14", False, False, False, 0, 0, True, True, 302, 0, 0, 1, "", 0, "Default Remain Amt 14")
    Private mDefaultRemainAmt15 As New ColField(Of Boolean)("TADA15", False, False, False, 0, 0, True, True, 303, 0, 0, 1, "", 0, "Default Remain Amt 15")
    Private mDefaultRemainAmt16 As New ColField(Of Boolean)("TADA16", False, False, False, 0, 0, True, True, 304, 0, 0, 1, "", 0, "Default Remain Amt 16")
    Private mDefaultRemainAmt17 As New ColField(Of Boolean)("TADA17", False, False, False, 0, 0, True, True, 305, 0, 0, 1, "", 0, "Default Remain Amt 17")
    Private mDefaultRemainAmt18 As New ColField(Of Boolean)("TADA18", False, False, False, 0, 0, True, True, 306, 0, 0, 1, "", 0, "Default Remain Amt 18")
    Private mDefaultRemainAmt19 As New ColField(Of Boolean)("TADA19", False, False, False, 0, 0, True, True, 307, 0, 0, 1, "", 0, "Default Remain Amt 19")
    Private mDefaultRemainAmt20 As New ColField(Of Boolean)("TADA20", False, False, False, 0, 0, True, True, 308, 0, 0, 1, "", 0, "Default Remain Amt 20")
    Private mMiscOpenDrawReasonNo As New ColField(Of Decimal)("MODR", 0, False, False, 0, 0, True, True, 309, 0, 0, 1, "", 0, "Misc open Drawer Reason No")
    Private mMaxTillDrawFloat As New ColField(Of Decimal)("MAXF", 0, False, False, 0, 0, True, True, 310, 0, 0, 1, "", 0, "Max Till Drawer Float")
    Private mMinSavValBeforeMess As New ColField(Of Decimal)("MSAV", 0, False, False, 0, 0, True, True, 311, 0, 0, 1, "", 0, "Min Sav Val Before Mess")
    Private mMaxRefundAmtSelfAuth As New ColField(Of Decimal)("MRSQ", 0, False, False, 0, 0, True, True, 312, 0, 0, 1, "", 0, "Max Refund Amt Self Auth")
    Private mTillAccessEventFiles As New ColField(Of Boolean)("IEVT", False, False, False, 0, 0, True, True, 313, 0, 0, 1, "", 0, "Till Access Event Files")
    Private mSecStaffDiscountPer As New ColField(Of Decimal)("SDIS", 0, False, False, 0, 0, True, True, 314, 0, 0, 1, "", 0, "Sec Staff Discount Per")
    Private mVATPercDiscount As New ColField(Of Decimal)("VATP", 0, False, False, 0, 0, True, True, 315, 0, 0, 1, "", 0, "VAT Per Discount")
    Private mVATDiscountActive As New ColField(Of Boolean)("VATO", False, False, False, 0, 0, True, True, 316, 0, 0, 1, "", 0, "VAT Discount Active")
    Private mFeeStatementOnSlips As New ColField(Of Boolean)("VATL", False, False, False, 0, 0, True, True, 317, 0, 0, 1, "", 0, "Fee Statement On Slips")
    Private mSupervisorReqTS01 As New ColField(Of Boolean)("TSUP1", False, False, False, 0, 0, True, True, 318, 0, 0, 1, "", 0, "Supervisor Req TS 01")
    Private mSupervisorReqTS02 As New ColField(Of Boolean)("TSUP2", False, False, False, 0, 0, True, True, 319, 0, 0, 1, "", 0, "Supervisor Req TS 02")
    Private mSupervisorReqTS03 As New ColField(Of Boolean)("TSUP3", False, False, False, 0, 0, True, True, 320, 0, 0, 1, "", 0, "Supervisor Req TS 03")
    Private mSupervisorReqTS04 As New ColField(Of Boolean)("TSUP4", False, False, False, 0, 0, True, True, 321, 0, 0, 1, "", 0, "Supervisor Req TS 04")
    Private mSupervisorReqTS05 As New ColField(Of Boolean)("TSUP5", False, False, False, 0, 0, True, True, 322, 0, 0, 1, "", 0, "Supervisor Req TS 05")
    Private mSupervisorReqTS06 As New ColField(Of Boolean)("TSUP6", False, False, False, 0, 0, True, True, 323, 0, 0, 1, "", 0, "Supervisor Req TS 06")
    Private mSupervisorReqTS07 As New ColField(Of Boolean)("TSUP7", False, False, False, 0, 0, True, True, 324, 0, 0, 1, "", 0, "Supervisor Req TS 07")
    Private mSupervisorReqTS08 As New ColField(Of Boolean)("TSUP8", False, False, False, 0, 0, True, True, 325, 0, 0, 1, "", 0, "Supervisor Req TS 08")
    Private mSupervisorReqTS09 As New ColField(Of Boolean)("TSUP9", False, False, False, 0, 0, True, True, 326, 0, 0, 1, "", 0, "Supervisor Req TS 09")
    Private mSupervisorReqTS10 As New ColField(Of Boolean)("TSUP10", False, False, False, 0, 0, True, True, 327, 0, 0, 1, "", 0, "Supervisor Req TS 10")
    Private mSupervisorReqTS11 As New ColField(Of Boolean)("TSUP11", False, False, False, 0, 0, True, True, 328, 0, 0, 1, "", 0, "Supervisor Req TS 11")
    Private mSupervisorReqTS12 As New ColField(Of Boolean)("TSUP12", False, False, False, 0, 0, True, True, 329, 0, 0, 1, "", 0, "Supervisor Req TS 12")
    Private mSupervisorReqTS13 As New ColField(Of Boolean)("TSUP13", False, False, False, 0, 0, True, True, 330, 0, 0, 1, "", 0, "Supervisor Req TS 13")
    Private mSupervisorReqTS14 As New ColField(Of Boolean)("TSUP14", False, False, False, 0, 0, True, True, 331, 0, 0, 1, "", 0, "Supervisor Req TS 14")
    Private mSupervisorReqTS15 As New ColField(Of Boolean)("TSUP15", False, False, False, 0, 0, True, True, 332, 0, 0, 1, "", 0, "Supervisor Req TS 15")
    Private mSupervisorReqTS16 As New ColField(Of Boolean)("TSUP16", False, False, False, 0, 0, True, True, 333, 0, 0, 1, "", 0, "Supervisor Req TS 16")
    Private mSupervisorReqTS17 As New ColField(Of Boolean)("TSUP17", False, False, False, 0, 0, True, True, 334, 0, 0, 1, "", 0, "Supervisor Req TS 17")
    Private mSupervisorReqTS18 As New ColField(Of Boolean)("TSUP18", False, False, False, 0, 0, True, True, 335, 0, 0, 1, "", 0, "Supervisor Req TS 18")
    Private mSupervisorReqTS19 As New ColField(Of Boolean)("TSUP19", False, False, False, 0, 0, True, True, 336, 0, 0, 1, "", 0, "Supervisor Req TS 19")
    Private mSupervisorReqTS20 As New ColField(Of Boolean)("TSUP20", False, False, False, 0, 0, True, True, 337, 0, 0, 1, "", 0, "Supervisor Req TS 20")
    Private mCustomerDtlReqTS01 As New ColField(Of Boolean)("TADR1", False, False, False, 0, 0, True, True, 338, 0, 0, 1, "", 0, "Customer Dtl Req TS 01")
    Private mCustomerDtlReqTS02 As New ColField(Of Boolean)("TADR2", False, False, False, 0, 0, True, True, 339, 0, 0, 1, "", 0, "Customer Dtl Req TS 02")
    Private mCustomerDtlReqTS03 As New ColField(Of Boolean)("TADR3", False, False, False, 0, 0, True, True, 340, 0, 0, 1, "", 0, "Customer Dtl Req TS 03")
    Private mCustomerDtlReqTS04 As New ColField(Of Boolean)("TADR4", False, False, False, 0, 0, True, True, 341, 0, 0, 1, "", 0, "Customer Dtl Req TS 04")
    Private mCustomerDtlReqTS05 As New ColField(Of Boolean)("TADR5", False, False, False, 0, 0, True, True, 342, 0, 0, 1, "", 0, "Customer Dtl Req TS 05")
    Private mCustomerDtlReqTS06 As New ColField(Of Boolean)("TADR6", False, False, False, 0, 0, True, True, 343, 0, 0, 1, "", 0, "Customer Dtl Req TS 06")
    Private mCustomerDtlReqTS07 As New ColField(Of Boolean)("TADR7", False, False, False, 0, 0, True, True, 344, 0, 0, 1, "", 0, "Customer Dtl Req TS 07")
    Private mCustomerDtlReqTS08 As New ColField(Of Boolean)("TADR8", False, False, False, 0, 0, True, True, 345, 0, 0, 1, "", 0, "Customer Dtl Req TS 08")
    Private mCustomerDtlReqTS09 As New ColField(Of Boolean)("TADR9", False, False, False, 0, 0, True, True, 346, 0, 0, 1, "", 0, "Customer Dtl Req TS 09")
    Private mCustomerDtlReqTS10 As New ColField(Of Boolean)("TADR10", False, False, False, 0, 0, True, True, 347, 0, 0, 1, "", 0, "Customer Dtl Req TS 10")
    Private mCustomerDtlReqTS11 As New ColField(Of Boolean)("TADR11", False, False, False, 0, 0, True, True, 348, 0, 0, 1, "", 0, "Customer Dtl Req TS 11")
    Private mCustomerDtlReqTS12 As New ColField(Of Boolean)("TADR12", False, False, False, 0, 0, True, True, 349, 0, 0, 1, "", 0, "Customer Dtl Req TS 12")
    Private mCustomerDtlReqTS13 As New ColField(Of Boolean)("TADR13", False, False, False, 0, 0, True, True, 350, 0, 0, 1, "", 0, "Customer Dtl Req TS 13")
    Private mCustomerDtlReqTS14 As New ColField(Of Boolean)("TADR14", False, False, False, 0, 0, True, True, 351, 0, 0, 1, "", 0, "Customer Dtl Req TS 14")
    Private mCustomerDtlReqTS15 As New ColField(Of Boolean)("TADR15", False, False, False, 0, 0, True, True, 352, 0, 0, 1, "", 0, "Customer Dtl Req TS 15")
    Private mCustomerDtlReqTS16 As New ColField(Of Boolean)("TADR16", False, False, False, 0, 0, True, True, 353, 0, 0, 1, "", 0, "Customer Dtl Req TS 16")
    Private mCustomerDtlReqTS17 As New ColField(Of Boolean)("TADR17", False, False, False, 0, 0, True, True, 354, 0, 0, 1, "", 0, "Customer Dtl Req TS 17")
    Private mCustomerDtlReqTS18 As New ColField(Of Boolean)("TADR18", False, False, False, 0, 0, True, True, 355, 0, 0, 1, "", 0, "Customer Dtl Req TS 18")
    Private mCustomerDtlReqTS19 As New ColField(Of Boolean)("TADR19", False, False, False, 0, 0, True, True, 356, 0, 0, 1, "", 0, "Customer Dtl Req TS 19")
    Private mCustomerDtlReqTS20 As New ColField(Of Boolean)("TADR20", False, False, False, 0, 0, True, True, 357, 0, 0, 1, "", 0, "Customer Dtl Req TS 20")
    Private mMinRefundRequiringSA As New ColField(Of Decimal)("SAU1", 0, False, False, 0, 0, True, True, 358, 0, 0, 1, "", 0, "Min Refund Requiring SA")
    Private mMinRefundRequiringMA As New ColField(Of Decimal)("SAU2", 0, False, False, 0, 0, True, True, 359, 0, 0, 1, "", 0, "Min Refund Requiring MA")
    Private mMinCollDiscTranValSA As New ColField(Of Decimal)("SAU3", 0, False, False, 0, 0, True, True, 360, 0, 0, 1, "", 0, "Min Coll Disc Tran Val SA")
    Private mMinColDiscTranValMA As New ColField(Of Decimal)("SAU4", 0, False, False, 0, 0, True, True, 361, 0, 0, 1, "", 0, "Min Col Disc Tran Val MA")
    Private mMaxValGiftToken As New ColField(Of Decimal)("SAU5", 0, False, False, 0, 0, True, True, 362, 0, 0, 1, "", 0, "Max Val Gift Token")
    Private mMinChgGivenGiftToken As New ColField(Of Decimal)("MINC", 0, False, False, 0, 0, True, True, 363, 0, 0, 1, "", 0, "Min Chg Given Gift Token")
    Private mSalesMaxField As New ColField(Of Decimal)("SMAX", 0, False, False, 0, 0, True, True, 364, 0, 0, 1, "", 0, "Sales Max Field")
    Private mAutoAppPriceChgAuth As New ColField(Of Boolean)("PCAU", False, False, False, 0, 0, True, True, 365, 0, 0, 1, "", 0, "Auto App Price Chg Auth")
    Private mMgrAuthorisedAA As New ColField(Of String)("PCMA", "", False, False, 0, 0, True, True, 366, 0, 0, 1, "", 0, "Mgr Authorised AA")
    Private mHostuLoadInProgress As New ColField(Of String)("LOAD", "False", False, False, 0, 0, True, True, 367, 0, 0, 1, "", 0, "Hostu Load In Progress")
    Private mCountryCode As New ColField(Of String)("COUNTRYCODE", "", False, False, 0, 0, True, True, 368, 0, 0, 1, "", 0, "CountryCode")


#End Region

#Region "Field Properties"

    Public Property RetailOptionsID() As ColField(Of String)
        Get
            RetailOptionsID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property Store() As ColField(Of String)
        Get
            Store = _Store
        End Get
        Set(ByVal value As ColField(Of String))
            _Store = value
        End Set
    End Property
    Public Property StoreName() As ColField(Of String)
        Get
            StoreName = _StoreName
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreName = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property AccountabilityType() As ColField(Of String)
        Get
            AccountabilityType = _AccountabilityType
        End Get
        Set(ByVal value As ColField(Of String))
            _AccountabilityType = value
        End Set
    End Property

    Public Property HighestValidCashier() As ColField(Of String)
        Get
            HighestValidCashier = mHighestValidCashier
        End Get
        Set(ByVal value As ColField(Of String))
            mHighestValidCashier = value
        End Set
    End Property
    Public Property DefaultFloatAmountAT() As ColField(Of Decimal)
        Get
            DefaultFloatAmountAT = mDefaultFloatAmountAT
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDefaultFloatAmountAT = value
        End Set
    End Property
    Public Property LstAcctPerRetClosed() As ColField(Of Decimal)
        Get
            LstAcctPerRetClosed = mLstAcctPerRetClosed
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mLstAcctPerRetClosed = value
        End Set
    End Property
    Public Property NextAcctPeriodToOpen() As ColField(Of Decimal)
        Get
            NextAcctPeriodToOpen = mNextAcctPeriodToOpen
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNextAcctPeriodToOpen = value
        End Set
    End Property
    Public Property AccoutingPerToKeep() As ColField(Of Decimal)
        Get
            AccoutingPerToKeep = mAccoutingPerToKeep
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAccoutingPerToKeep = value
        End Set
    End Property
    Public Property TypeOfSaleNo01() As ColField(Of String)
        Get
            TypeOfSaleNo01 = mTypeOfSaleNo01
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo01 = value
        End Set
    End Property
    Public Property TypeOfSaleNo02() As ColField(Of String)
        Get
            TypeOfSaleNo02 = mTypeOfSaleNo02
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo02 = value
        End Set
    End Property
    Public Property TypeOfSaleNo03() As ColField(Of String)
        Get
            TypeOfSaleNo03 = mTypeOfSaleNo03
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo03 = value
        End Set
    End Property
    Public Property TypeOfSaleNo04() As ColField(Of String)
        Get
            TypeOfSaleNo04 = mTypeOfSaleNo04
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo04 = value
        End Set
    End Property
    Public Property TypeOfSaleNo05() As ColField(Of String)
        Get
            TypeOfSaleNo05 = mTypeOfSaleNo05
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo05 = value
        End Set
    End Property
    Public Property TypeOfSaleNo06() As ColField(Of String)
        Get
            TypeOfSaleNo06 = mTypeOfSaleNo06
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo06 = value
        End Set
    End Property
    Public Property TypeOfSaleNo07() As ColField(Of String)
        Get
            TypeOfSaleNo07 = mTypeOfSaleNo07
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo07 = value
        End Set
    End Property
    Public Property TypeOfSaleNo08() As ColField(Of String)
        Get
            TypeOfSaleNo08 = mTypeOfSaleNo08
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo08 = value
        End Set
    End Property
    Public Property TypeOfSaleNo09() As ColField(Of String)
        Get
            TypeOfSaleNo09 = mTypeOfSaleNo09
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo09 = value
        End Set
    End Property
    Public Property TypeOfSaleNo10() As ColField(Of String)
        Get
            TypeOfSaleNo10 = mTypeOfSaleNo10
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo10 = value
        End Set
    End Property
    Public Property TypeOfSaleNo11() As ColField(Of String)
        Get
            TypeOfSaleNo11 = mTypeOfSaleNo11
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo11 = value
        End Set
    End Property
    Public Property TypeOfSaleNo12() As ColField(Of String)
        Get
            TypeOfSaleNo12 = mTypeOfSaleNo12
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo12 = value
        End Set
    End Property
    Public Property TypeOfSaleNo13() As ColField(Of String)
        Get
            TypeOfSaleNo13 = mTypeOfSaleNo13
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo13 = value
        End Set
    End Property
    Public Property TypeOfSaleNo14() As ColField(Of String)
        Get
            TypeOfSaleNo14 = mTypeOfSaleNo14
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo14 = value
        End Set
    End Property
    Public Property TypeOfSaleNo15() As ColField(Of String)
        Get
            TypeOfSaleNo15 = mTypeOfSaleNo15
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo15 = value
        End Set
    End Property
    Public Property TypeOfSaleNo16() As ColField(Of String)
        Get
            TypeOfSaleNo16 = mTypeOfSaleNo16
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo16 = value
        End Set
    End Property
    Public Property TypeOfSaleNo17() As ColField(Of String)
        Get
            TypeOfSaleNo17 = mTypeOfSaleNo17
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo17 = value
        End Set
    End Property
    Public Property TypeOfSaleNo18() As ColField(Of String)
        Get
            TypeOfSaleNo18 = mTypeOfSaleNo18
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo18 = value
        End Set
    End Property
    Public Property TypeOfSaleNo19() As ColField(Of String)
        Get
            TypeOfSaleNo19 = mTypeOfSaleNo19
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo19 = value
        End Set
    End Property
    Public Property TypeOfSaleNo20() As ColField(Of String)
        Get
            TypeOfSaleNo20 = mTypeOfSaleNo20
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo20 = value
        End Set
    End Property
    Public Property TypeOfSaleNo21() As ColField(Of String)
        Get
            TypeOfSaleNo21 = mTypeOfSaleNo21
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo21 = value
        End Set
    End Property
    Public Property TypeOfSaleNo22() As ColField(Of String)
        Get
            TypeOfSaleNo22 = mTypeOfSaleNo22
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo22 = value
        End Set
    End Property
    Public Property TypeOfSaleNo23() As ColField(Of String)
        Get
            TypeOfSaleNo23 = mTypeOfSaleNo23
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo23 = value
        End Set
    End Property
    Public Property TypeOfSaleNo24() As ColField(Of String)
        Get
            TypeOfSaleNo24 = mTypeOfSaleNo24
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo24 = value
        End Set
    End Property
    Public Property TypeOfSaleNo25() As ColField(Of String)
        Get
            TypeOfSaleNo25 = mTypeOfSaleNo25
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo25 = value
        End Set
    End Property
    Public Property TypeOfSaleNo26() As ColField(Of String)
        Get
            TypeOfSaleNo26 = mTypeOfSaleNo26
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo26 = value
        End Set
    End Property
    Public Property TypeOfSaleNo27() As ColField(Of String)
        Get
            TypeOfSaleNo27 = mTypeOfSaleNo27
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo27 = value
        End Set
    End Property
    Public Property TypeOfSaleNo28() As ColField(Of String)
        Get
            TypeOfSaleNo28 = mTypeOfSaleNo28
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo28 = value
        End Set
    End Property
    Public Property TypeOfSaleNo29() As ColField(Of String)
        Get
            TypeOfSaleNo29 = mTypeOfSaleNo29
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo29 = value
        End Set
    End Property
    Public Property TypeOfSaleNo30() As ColField(Of String)
        Get
            TypeOfSaleNo30 = mTypeOfSaleNo30
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSaleNo30 = value
        End Set
    End Property
    Public Property TypeOfSale01() As ColField(Of String)
        Get
            TypeOfSale01 = mTypeOfSale01
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale01 = value
        End Set
    End Property
    Public Property TypeOfSale02() As ColField(Of String)
        Get
            TypeOfSale02 = mTypeOfSale02
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale02 = value
        End Set
    End Property
    Public Property TypeOfSale03() As ColField(Of String)
        Get
            TypeOfSale03 = mTypeOfSale03
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale03 = value
        End Set
    End Property
    Public Property TypeOfSale04() As ColField(Of String)
        Get
            TypeOfSale04 = mTypeOfSale04
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale04 = value
        End Set
    End Property
    Public Property TypeOfSale05() As ColField(Of String)
        Get
            TypeOfSale05 = mTypeOfSale05
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale05 = value
        End Set
    End Property
    Public Property TypeOfSale06() As ColField(Of String)
        Get
            TypeOfSale06 = mTypeOfSale06
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale06 = value
        End Set
    End Property
    Public Property TypeOfSale07() As ColField(Of String)
        Get
            TypeOfSale07 = mTypeOfSale07
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale07 = value
        End Set
    End Property
    Public Property TypeOfSale08() As ColField(Of String)
        Get
            TypeOfSale08 = mTypeOfSale08
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale08 = value
        End Set
    End Property
    Public Property TypeOfSale09() As ColField(Of String)
        Get
            TypeOfSale09 = mTypeOfSale09
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale09 = value
        End Set
    End Property
    Public Property TypeOfSale10() As ColField(Of String)
        Get
            TypeOfSale10 = mTypeOfSale10
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale10 = value
        End Set
    End Property
    Public Property TypeOfSale11() As ColField(Of String)
        Get
            TypeOfSale11 = mTypeOfSale11
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale11 = value
        End Set
    End Property
    Public Property TypeOfSale12() As ColField(Of String)
        Get
            TypeOfSale12 = mTypeOfSale12
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale12 = value
        End Set
    End Property
    Public Property TypeOfSale13() As ColField(Of String)
        Get
            TypeOfSale13 = mTypeOfSale13
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale13 = value
        End Set
    End Property
    Public Property TypeOfSale14() As ColField(Of String)
        Get
            TypeOfSale14 = mTypeOfSale14
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale14 = value
        End Set
    End Property
    Public Property TypeOfSale15() As ColField(Of String)
        Get
            TypeOfSale15 = mTypeOfSale15
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale15 = value
        End Set
    End Property
    Public Property TypeOfSale16() As ColField(Of String)
        Get
            TypeOfSale16 = mTypeOfSale16
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale16 = value
        End Set
    End Property
    Public Property TypeOfSale17() As ColField(Of String)
        Get
            TypeOfSale17 = mTypeOfSale17
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale17 = value
        End Set
    End Property
    Public Property TypeOfSale18() As ColField(Of String)
        Get
            TypeOfSale18 = mTypeOfSale18
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale18 = value
        End Set
    End Property
    Public Property TypeOfSale19() As ColField(Of String)
        Get
            TypeOfSale19 = mTypeOfSale19
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale19 = value
        End Set
    End Property
    Public Property TypeOfSale20() As ColField(Of String)
        Get
            TypeOfSale20 = mTypeOfSale20
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale20 = value
        End Set
    End Property
    Public Property TypeOfSale21() As ColField(Of String)
        Get
            TypeOfSale21 = mTypeOfSale21
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale21 = value
        End Set
    End Property
    Public Property TypeOfSale22() As ColField(Of String)
        Get
            TypeOfSale22 = mTypeOfSale22
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale22 = value
        End Set
    End Property
    Public Property TypeOfSale23() As ColField(Of String)
        Get
            TypeOfSale23 = mTypeOfSale23
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale23 = value
        End Set
    End Property
    Public Property TypeOfSale24() As ColField(Of String)
        Get
            TypeOfSale24 = mTypeOfSale24
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale24 = value
        End Set
    End Property
    Public Property TypeOfSale25() As ColField(Of String)
        Get
            TypeOfSale25 = mTypeOfSale25
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale25 = value
        End Set
    End Property
    Public Property TypeOfSale26() As ColField(Of String)
        Get
            TypeOfSale26 = mTypeOfSale26
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale26 = value
        End Set
    End Property
    Public Property TypeOfSale27() As ColField(Of String)
        Get
            TypeOfSale27 = mTypeOfSale27
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale27 = value
        End Set
    End Property
    Public Property TypeOfSale28() As ColField(Of String)
        Get
            TypeOfSale28 = mTypeOfSale28
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale28 = value
        End Set
    End Property
    Public Property TypeOfSale29() As ColField(Of String)
        Get
            TypeOfSale29 = mTypeOfSale29
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale29 = value
        End Set
    End Property
    Public Property TypeOfSale30() As ColField(Of String)
        Get
            TypeOfSale30 = mTypeOfSale30
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSale30 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc01() As ColField(Of String)
        Get
            TypeOfSalesDesc01 = mTypeOfSalesDesc01
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc01 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc02() As ColField(Of String)
        Get
            TypeOfSalesDesc02 = mTypeOfSalesDesc02
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc02 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc03() As ColField(Of String)
        Get
            TypeOfSalesDesc03 = mTypeOfSalesDesc03
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc03 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc04() As ColField(Of String)
        Get
            TypeOfSalesDesc04 = mTypeOfSalesDesc04
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc04 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc05() As ColField(Of String)
        Get
            TypeOfSalesDesc05 = mTypeOfSalesDesc05
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc05 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc06() As ColField(Of String)
        Get
            TypeOfSalesDesc06 = mTypeOfSalesDesc06
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc06 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc07() As ColField(Of String)
        Get
            TypeOfSalesDesc07 = mTypeOfSalesDesc07
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc07 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc08() As ColField(Of String)
        Get
            TypeOfSalesDesc08 = mTypeOfSalesDesc08
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc08 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc09() As ColField(Of String)
        Get
            TypeOfSalesDesc09 = mTypeOfSalesDesc09
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc09 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc10() As ColField(Of String)
        Get
            TypeOfSalesDesc10 = mTypeOfSalesDesc10
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc10 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc11() As ColField(Of String)
        Get
            TypeOfSalesDesc11 = mTypeOfSalesDesc11
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc11 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc12() As ColField(Of String)
        Get
            TypeOfSalesDesc12 = mTypeOfSalesDesc12
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc12 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc13() As ColField(Of String)
        Get
            TypeOfSalesDesc13 = mTypeOfSalesDesc13
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc13 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc14() As ColField(Of String)
        Get
            TypeOfSalesDesc14 = mTypeOfSalesDesc14
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc14 = value
        End Set
    End Property
    Public Property TypeOfSalesDess15() As ColField(Of String)
        Get
            TypeOfSalesDess15 = mTypeOfSalesDess15
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDess15 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc16() As ColField(Of String)
        Get
            TypeOfSalesDesc16 = mTypeOfSalesDesc16
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc16 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc17() As ColField(Of String)
        Get
            TypeOfSalesDesc17 = mTypeOfSalesDesc17
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc17 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc18() As ColField(Of String)
        Get
            TypeOfSalesDesc18 = mTypeOfSalesDesc18
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc18 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc19() As ColField(Of String)
        Get
            TypeOfSalesDesc19 = mTypeOfSalesDesc19
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc19 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc20() As ColField(Of String)
        Get
            TypeOfSalesDesc20 = mTypeOfSalesDesc20
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc20 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc21() As ColField(Of String)
        Get
            TypeOfSalesDesc21 = mTypeOfSalesDesc21
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc21 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc22() As ColField(Of String)
        Get
            TypeOfSalesDesc22 = mTypeOfSalesDesc22
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc22 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc23() As ColField(Of String)
        Get
            TypeOfSalesDesc23 = mTypeOfSalesDesc23
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc23 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc24() As ColField(Of String)
        Get
            TypeOfSalesDesc24 = mTypeOfSalesDesc24
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc24 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc25() As ColField(Of String)
        Get
            TypeOfSalesDesc25 = mTypeOfSalesDesc25
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc25 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc26() As ColField(Of String)
        Get
            TypeOfSalesDesc26 = mTypeOfSalesDesc26
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc26 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc27() As ColField(Of String)
        Get
            TypeOfSalesDesc27 = mTypeOfSalesDesc27
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc27 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc28() As ColField(Of String)
        Get
            TypeOfSalesDesc28 = mTypeOfSalesDesc28
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc28 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc29() As ColField(Of String)
        Get
            TypeOfSalesDesc29 = mTypeOfSalesDesc29
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc29 = value
        End Set
    End Property
    Public Property TypeOfSalesDesc30() As ColField(Of String)
        Get
            TypeOfSalesDesc30 = mTypeOfSalesDesc30
        End Get
        Set(ByVal value As ColField(Of String))
            mTypeOfSalesDesc30 = value
        End Set
    End Property
    Public Property OverTenderAllowed01() As ColField(Of Boolean)
        Get
            OverTenderAllowed01 = mOverTenderAllowed01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed01 = value
        End Set
    End Property
    Public Property OverTenderAllowed02() As ColField(Of Boolean)
        Get
            OverTenderAllowed02 = mOverTenderAllowed02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed02 = value
        End Set
    End Property
    Public Property OverTenderAllowed03() As ColField(Of Boolean)
        Get
            OverTenderAllowed03 = mOverTenderAllowed03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed03 = value
        End Set
    End Property
    Public Property OverTenderAllowed04() As ColField(Of Boolean)
        Get
            OverTenderAllowed04 = mOverTenderAllowed04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed04 = value
        End Set
    End Property
    Public Property OverTenderAllowed05() As ColField(Of Boolean)
        Get
            OverTenderAllowed05 = mOverTenderAllowed05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed05 = value
        End Set
    End Property
    Public Property OverTenderAllowed06() As ColField(Of Boolean)
        Get
            OverTenderAllowed06 = mOverTenderAllowed06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed06 = value
        End Set
    End Property
    Public Property OverTenderAllowed07() As ColField(Of Boolean)
        Get
            OverTenderAllowed07 = mOverTenderAllowed07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed07 = value
        End Set
    End Property
    Public Property OverTenderAllowed08() As ColField(Of Boolean)
        Get
            OverTenderAllowed08 = mOverTenderAllowed08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed08 = value
        End Set
    End Property
    Public Property OverTenderAllowed09() As ColField(Of Boolean)
        Get
            OverTenderAllowed09 = mOverTenderAllowed09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed09 = value
        End Set
    End Property
    Public Property OverTenderAllowed10() As ColField(Of Boolean)
        Get
            OverTenderAllowed10 = mOverTenderAllowed10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOverTenderAllowed10 = value
        End Set
    End Property
    Public Property CreditCardCapture01() As ColField(Of Boolean)
        Get
            CreditCardCapture01 = mCreditCardCapture01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture01 = value
        End Set
    End Property
    Public Property CreditCardCapture02() As ColField(Of Boolean)
        Get
            CreditCardCapture02 = mCreditCardCapture02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture02 = value
        End Set
    End Property
    Public Property CreditCardCapture03() As ColField(Of Boolean)
        Get
            CreditCardCapture03 = mCreditCardCapture03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture03 = value
        End Set
    End Property
    Public Property CreditCardCapture04() As ColField(Of Boolean)
        Get
            CreditCardCapture04 = mCreditCardCapture04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture04 = value
        End Set
    End Property
    Public Property CreditCardCapture05() As ColField(Of Boolean)
        Get
            CreditCardCapture05 = mCreditCardCapture05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture05 = value
        End Set
    End Property
    Public Property CreditCardCapture06() As ColField(Of Boolean)
        Get
            CreditCardCapture06 = mCreditCardCapture06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture06 = value
        End Set
    End Property
    Public Property CreditCardCapture07() As ColField(Of Boolean)
        Get
            CreditCardCapture07 = mCreditCardCapture07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture07 = value
        End Set
    End Property
    Public Property CreditCardCapture08() As ColField(Of Boolean)
        Get
            CreditCardCapture08 = mCreditCardCapture08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture08 = value
        End Set
    End Property
    Public Property CreditCardCapture09() As ColField(Of Boolean)
        Get
            CreditCardCapture09 = mCreditCardCapture09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture09 = value
        End Set
    End Property
    Public Property CreditCardCapture10() As ColField(Of Boolean)
        Get
            CreditCardCapture10 = mCreditCardCapture10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCreditCardCapture10 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc01() As ColField(Of String)
        Get
            TenderTypeFlagDesc01 = mTenderTypeFlagDesc01
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc01 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc02() As ColField(Of String)
        Get
            TenderTypeFlagDesc02 = mTenderTypeFlagDesc02
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc02 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc03() As ColField(Of String)
        Get
            TenderTypeFlagDesc03 = mTenderTypeFlagDesc03
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc03 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc04() As ColField(Of String)
        Get
            TenderTypeFlagDesc04 = mTenderTypeFlagDesc04
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc04 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc05() As ColField(Of String)
        Get
            TenderTypeFlagDesc05 = mTenderTypeFlagDesc05
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc05 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc06() As ColField(Of String)
        Get
            TenderTypeFlagDesc06 = mTenderTypeFlagDesc06
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc06 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc07() As ColField(Of String)
        Get
            TenderTypeFlagDesc07 = mTenderTypeFlagDesc07
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc07 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc08() As ColField(Of String)
        Get
            TenderTypeFlagDesc08 = mTenderTypeFlagDesc08
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc08 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc09() As ColField(Of String)
        Get
            TenderTypeFlagDesc09 = mTenderTypeFlagDesc09
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc09 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc10() As ColField(Of String)
        Get
            TenderTypeFlagDesc10 = mTenderTypeFlagDesc10
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc10 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc11() As ColField(Of String)
        Get
            TenderTypeFlagDesc11 = mTenderTypeFlagDesc11
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc11 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc12() As ColField(Of String)
        Get
            TenderTypeFlagDesc12 = mTenderTypeFlagDesc12
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc12 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc13() As ColField(Of String)
        Get
            TenderTypeFlagDesc13 = mTenderTypeFlagDesc13
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc13 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc14() As ColField(Of String)
        Get
            TenderTypeFlagDesc14 = mTenderTypeFlagDesc14
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc14 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc15() As ColField(Of String)
        Get
            TenderTypeFlagDesc15 = mTenderTypeFlagDesc15
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc15 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc16() As ColField(Of String)
        Get
            TenderTypeFlagDesc16 = mTenderTypeFlagDesc16
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc16 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc17() As ColField(Of String)
        Get
            TenderTypeFlagDesc17 = mTenderTypeFlagDesc17
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc17 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc18() As ColField(Of String)
        Get
            TenderTypeFlagDesc18 = mTenderTypeFlagDesc18
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc18 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc19() As ColField(Of String)
        Get
            TenderTypeFlagDesc19 = mTenderTypeFlagDesc19
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc19 = value
        End Set
    End Property
    Public Property TenderTypeFlagDesc20() As ColField(Of String)
        Get
            TenderTypeFlagDesc20 = mTenderTypeFlagDesc20
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeFlagDesc20 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode01() As ColField(Of String)
        Get
            OpenDrawReasonCode01 = mOpenDrawReasonCode01
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode01 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode02() As ColField(Of String)
        Get
            OpenDrawReasonCode02 = mOpenDrawReasonCode02
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode02 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode03() As ColField(Of String)
        Get
            OpenDrawReasonCode03 = mOpenDrawReasonCode03
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode03 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode04() As ColField(Of String)
        Get
            OpenDrawReasonCode04 = mOpenDrawReasonCode04
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode04 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode05() As ColField(Of String)
        Get
            OpenDrawReasonCode05 = mOpenDrawReasonCode05
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode05 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode06() As ColField(Of String)
        Get
            OpenDrawReasonCode06 = mOpenDrawReasonCode06
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode06 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode07() As ColField(Of String)
        Get
            OpenDrawReasonCode07 = mOpenDrawReasonCode07
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode07 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode08() As ColField(Of String)
        Get
            OpenDrawReasonCode08 = mOpenDrawReasonCode08
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode08 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode09() As ColField(Of String)
        Get
            OpenDrawReasonCode09 = mOpenDrawReasonCode09
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode09 = value
        End Set
    End Property
    Public Property OpenDrawReasonCode10() As ColField(Of String)
        Get
            OpenDrawReasonCode10 = mOpenDrawReasonCode10
        End Get
        Set(ByVal value As ColField(Of String))
            mOpenDrawReasonCode10 = value
        End Set
    End Property
    Public Property LastReformatDate() As ColField(Of Date)
        Get
            LastReformatDate = mLastReformatDate
        End Get
        Set(ByVal value As ColField(Of Date))
            mLastReformatDate = value
        End Set
    End Property
    Public Property LastUpdateDate() As ColField(Of Date)
        Get
            LastUpdateDate = mLastUpdateDate
        End Get
        Set(ByVal value As ColField(Of Date))
            mLastUpdateDate = value
        End Set
    End Property
    Public Property AutoAppPriceChanges() As ColField(Of Boolean)
        Get
            AutoAppPriceChanges = mAutoAppPriceChanges
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mAutoAppPriceChanges = value
        End Set
    End Property
    Public Property NoDayPstEffectDatePC() As ColField(Of Decimal)
        Get
            NoDayPstEffectDatePC = mNoDayPstEffectDatePC
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoDayPstEffectDatePC = value
        End Set
    End Property
    Public Property PrintShelfEdgeLblPC() As ColField(Of Boolean)
        Get
            PrintShelfEdgeLblPC = mPrintShelfEdgeLblPC
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mPrintShelfEdgeLblPC = value
        End Set
    End Property
    Public Property CompanyName() As ColField(Of String)
        Get
            CompanyName = _CompanyName
        End Get
        Set(ByVal value As ColField(Of String))
            _CompanyName = value
        End Set
    End Property
    Public Property CompanyAddress1() As ColField(Of String)
        Get
            CompanyAddress1 = mCompanyAddress1
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyAddress1 = value
        End Set
    End Property
    Public Property CompanyAddress2() As ColField(Of String)
        Get
            CompanyAddress2 = mCompanyAddress2
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyAddress2 = value
        End Set
    End Property
    Public Property CompanyAddress3() As ColField(Of String)
        Get
            CompanyAddress3 = mCompanyAddress3
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyAddress3 = value
        End Set
    End Property
    Public Property CompanyTelephone() As ColField(Of String)
        Get
            CompanyTelephone = mCompanyTelephone
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyTelephone = value
        End Set
    End Property
    Public Property CompanyTelex() As ColField(Of String)
        Get
            CompanyTelex = mCompanyTelex
        End Get
        Set(ByVal value As ColField(Of String))
            mCompanyTelex = value
        End Set
    End Property
    Public Property TenderNoCheques() As ColField(Of Decimal)
        Get
            TenderNoCheques = mTenderNoCheques
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoCheques = value
        End Set
    End Property
    Public Property TenderNoVisa() As ColField(Of Decimal)
        Get
            TenderNoVisa = mTenderNoVisa
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoVisa = value
        End Set
    End Property
    Public Property TenderNoAccess() As ColField(Of Decimal)
        Get
            TenderNoAccess = mTenderNoAccess
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoAccess = value
        End Set
    End Property
    Public Property TenderNoAmex() As ColField(Of Decimal)
        Get
            TenderNoAmex = mTenderNoAmex
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoAmex = value
        End Set
    End Property
    Public Property TenderNoProjectLoan() As ColField(Of Decimal)
        Get
            TenderNoProjectLoan = mTenderNoProjectLoan
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoProjectLoan = value
        End Set
    End Property
    Public Property TenderNoStoreVouchs() As ColField(Of Decimal)
        Get
            TenderNoStoreVouchs = mTenderNoStoreVouchs
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoStoreVouchs = value
        End Set
    End Property
    Public Property TenderNoSwitch() As ColField(Of Decimal)
        Get
            TenderNoSwitch = mTenderNoSwitch
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoSwitch = value
        End Set
    End Property
    Public Property TenderNoAccBuildMate() As ColField(Of Decimal)
        Get
            TenderNoAccBuildMate = mTenderNoAccBuildMate
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenderNoAccBuildMate = value
        End Set
    End Property

    Public Property MiscIncReasonCode(ByVal Index As Integer) As ColField(Of String)
        Get
            Dim ColName As String = MiscIncReasonCode01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))

            If Index = 0 Then Index = 7
            Dim ColName As String = MiscIncReasonCode01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MiscIncReasonCode01() As ColField(Of String)
        Get
            MiscIncReasonCode01 = mMiscIncReasonCode01
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode01 = value
        End Set
    End Property
    Public Property MiscIncReasonCode02() As ColField(Of String)
        Get
            MiscIncReasonCode02 = mMiscIncReasonCode02
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode02 = value
        End Set
    End Property
    Public Property MiscIncReasonCode03() As ColField(Of String)
        Get
            MiscIncReasonCode03 = mMiscIncReasonCode03
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode03 = value
        End Set
    End Property
    Public Property MiscIncReasonCode04() As ColField(Of String)
        Get
            MiscIncReasonCode04 = mMiscIncReasonCode04
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode04 = value
        End Set
    End Property
    Public Property MiscIncReasonCode05() As ColField(Of String)
        Get
            MiscIncReasonCode05 = mMiscIncReasonCode05
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode05 = value
        End Set
    End Property
    Public Property MiscIncReasonCode06() As ColField(Of String)
        Get
            MiscIncReasonCode06 = mMiscIncReasonCode06
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode06 = value
        End Set
    End Property
    Public Property MiscIncReasonCode07() As ColField(Of String)
        Get
            MiscIncReasonCode07 = mMiscIncReasonCode07
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode07 = value
        End Set
    End Property
    Public Property MiscIncReasonCode08() As ColField(Of String)
        Get
            MiscIncReasonCode08 = mMiscIncReasonCode08
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode08 = value
        End Set
    End Property
    Public Property MiscIncReasonCode09() As ColField(Of String)
        Get
            MiscIncReasonCode09 = mMiscIncReasonCode09
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode09 = value
        End Set
    End Property
    Public Property MiscIncReasonCode10() As ColField(Of String)
        Get
            MiscIncReasonCode10 = mMiscIncReasonCode10
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode10 = value
        End Set
    End Property
    Public Property MiscIncReasonCode11() As ColField(Of String)
        Get
            MiscIncReasonCode11 = mMiscIncReasonCode11
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode11 = value
        End Set
    End Property
    Public Property MiscIncReasonCode12() As ColField(Of String)
        Get
            MiscIncReasonCode12 = mMiscIncReasonCode12
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode12 = value
        End Set
    End Property
    Public Property MiscIncReasonCode13() As ColField(Of String)
        Get
            MiscIncReasonCode13 = mMiscIncReasonCode13
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode13 = value
        End Set
    End Property
    Public Property MiscIncReasonCode14() As ColField(Of String)
        Get
            MiscIncReasonCode14 = mMiscIncReasonCode14
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode14 = value
        End Set
    End Property
    Public Property MiscIncReasonCode15() As ColField(Of String)
        Get
            MiscIncReasonCode15 = mMiscIncReasonCode15
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode15 = value
        End Set
    End Property
    Public Property MiscIncReasonCode16() As ColField(Of String)
        Get
            MiscIncReasonCode16 = mMiscIncReasonCode16
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode16 = value
        End Set
    End Property
    Public Property MiscIncReasonCode17() As ColField(Of String)
        Get
            MiscIncReasonCode17 = mMiscIncReasonCode17
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode17 = value
        End Set
    End Property
    Public Property MiscIncReasonCode18() As ColField(Of String)
        Get
            MiscIncReasonCode18 = mMiscIncReasonCode18
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode18 = value
        End Set
    End Property
    Public Property MiscIncReasonCode19() As ColField(Of String)
        Get
            MiscIncReasonCode19 = mMiscIncReasonCode19
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode19 = value
        End Set
    End Property
    Public Property MiscIncReasonCode20() As ColField(Of String)
        Get
            MiscIncReasonCode20 = mMiscIncReasonCode20
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscIncReasonCode20 = value
        End Set
    End Property
    Public Property StoreDepositsInNo() As ColField(Of Decimal)
        Get
            StoreDepositsInNo = mStoreDepositsInNo
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStoreDepositsInNo = value
        End Set
    End Property

    Public Property MiscOutReasonCode(ByVal Index As Integer) As ColField(Of String)
        Get
            Dim ColName As String = MiscOutReasonCode01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))

            If Index = 0 Then Index = 7
            Dim ColName As String = MiscOutReasonCode01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MiscOutReasonCode01() As ColField(Of String)
        Get
            MiscOutReasonCode01 = mMiscOutReasonCode01
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode01 = value
        End Set
    End Property
    Public Property MiscOutReasonCode02() As ColField(Of String)
        Get
            MiscOutReasonCode02 = mMiscOutReasonCode02
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode02 = value
        End Set
    End Property
    Public Property MiscOutReasonCode03() As ColField(Of String)
        Get
            MiscOutReasonCode03 = mMiscOutReasonCode03
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode03 = value
        End Set
    End Property
    Public Property MiscOutReasonCode04() As ColField(Of String)
        Get
            MiscOutReasonCode04 = mMiscOutReasonCode04
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode04 = value
        End Set
    End Property
    Public Property MiscOutReasonCode05() As ColField(Of String)
        Get
            MiscOutReasonCode05 = mMiscOutReasonCode05
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode05 = value
        End Set
    End Property
    Public Property MiscOutReasonCode06() As ColField(Of String)
        Get
            MiscOutReasonCode06 = mMiscOutReasonCode06
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode06 = value
        End Set
    End Property
    Public Property MiscOutReasonCode07() As ColField(Of String)
        Get
            MiscOutReasonCode07 = mMiscOutReasonCode07
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode07 = value
        End Set
    End Property
    Public Property MiscOutReasonCode08() As ColField(Of String)
        Get
            MiscOutReasonCode08 = mMiscOutReasonCode08
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode08 = value
        End Set
    End Property
    Public Property MiscOutReasonCode09() As ColField(Of String)
        Get
            MiscOutReasonCode09 = mMiscOutReasonCode09
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode09 = value
        End Set
    End Property
    Public Property MiscOutReasonCode10() As ColField(Of String)
        Get
            MiscOutReasonCode10 = mMiscOutReasonCode10
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode10 = value
        End Set
    End Property
    Public Property MiscOutReasonCode11() As ColField(Of String)
        Get
            MiscOutReasonCode11 = mMiscOutReasonCode11
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode11 = value
        End Set
    End Property
    Public Property MiscOutReasonCode12() As ColField(Of String)
        Get
            MiscOutReasonCode12 = mMiscOutReasonCode12
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode12 = value
        End Set
    End Property
    Public Property MiscOutReasonCode13() As ColField(Of String)
        Get
            MiscOutReasonCode13 = mMiscOutReasonCode13
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode13 = value
        End Set
    End Property
    Public Property MiscOutReasonCode14() As ColField(Of String)
        Get
            MiscOutReasonCode14 = mMiscOutReasonCode14
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode14 = value
        End Set
    End Property
    Public Property MiscOutReasonCode15() As ColField(Of String)
        Get
            MiscOutReasonCode15 = mMiscOutReasonCode15
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode15 = value
        End Set
    End Property
    Public Property MiscOutReasonCode16() As ColField(Of String)
        Get
            MiscOutReasonCode16 = mMiscOutReasonCode16
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode16 = value
        End Set
    End Property
    Public Property MiscOutReasonCode17() As ColField(Of String)
        Get
            MiscOutReasonCode17 = mMiscOutReasonCode17
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode17 = value
        End Set
    End Property
    Public Property MiscOutReasonCode18() As ColField(Of String)
        Get
            MiscOutReasonCode18 = mMiscOutReasonCode18
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode18 = value
        End Set
    End Property
    Public Property MiscOutReasonCode19() As ColField(Of String)
        Get
            MiscOutReasonCode19 = mMiscOutReasonCode19
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode19 = value
        End Set
    End Property
    Public Property MiscOutReasonCode20() As ColField(Of String)
        Get
            MiscOutReasonCode20 = mMiscOutReasonCode20
        End Get
        Set(ByVal value As ColField(Of String))
            mMiscOutReasonCode20 = value
        End Set
    End Property
    Public Property StoreDespostisOutNo() As ColField(Of Decimal)
        Get
            StoreDespostisOutNo = mStoreDespostisOutNo
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStoreDespostisOutNo = value
        End Set
    End Property


    Public Property PriceOvrdReasonCde01() As ColField(Of String)
        Get
            PriceOvrdReasonCde01 = mPriceOvrdReasonCde01
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde01 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde02() As ColField(Of String)
        Get
            PriceOvrdReasonCde02 = mPriceOvrdReasonCde02
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde02 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde03() As ColField(Of String)
        Get
            PriceOvrdReasonCde03 = mPriceOvrdReasonCde03
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde03 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde04() As ColField(Of String)
        Get
            PriceOvrdReasonCde04 = mPriceOvrdReasonCde04
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde04 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde05() As ColField(Of String)
        Get
            PriceOvrdReasonCde05 = mPriceOvrdReasonCde05
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde05 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde06() As ColField(Of String)
        Get
            PriceOvrdReasonCde06 = mPriceOvrdReasonCde06
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde06 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde07() As ColField(Of String)
        Get
            PriceOvrdReasonCde07 = mPriceOvrdReasonCde07
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde07 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde08() As ColField(Of String)
        Get
            PriceOvrdReasonCde08 = mPriceOvrdReasonCde08
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde08 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde09() As ColField(Of String)
        Get
            PriceOvrdReasonCde09 = mPriceOvrdReasonCde09
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde09 = value
        End Set
    End Property
    Public Property PriceOvrdReasonCde10() As ColField(Of String)
        Get
            PriceOvrdReasonCde10 = mPriceOvrdReasonCde10
        End Get
        Set(ByVal value As ColField(Of String))
            mPriceOvrdReasonCde10 = value
        End Set
    End Property
    Public Property MaxCashChangeGiven() As ColField(Of Decimal)
        Get
            MaxCashChangeGiven = mMaxCashChangeGiven
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMaxCashChangeGiven = value
        End Set
    End Property
    Public Property CreditCardFloorLimit() As ColField(Of Decimal)
        Get
            CreditCardFloorLimit = mCreditCardFloorLimit
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCreditCardFloorLimit = value
        End Set
    End Property
    Public Property NoVATrates() As ColField(Of Decimal)
        Get
            NoVATrates = mNoVATrates
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mNoVATrates = value
        End Set
    End Property
    Public Property VATRatesInclusive() As ColField(Of Boolean)
        Get
            VATRatesInclusive = mVATRatesInclusive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mVATRatesInclusive = value
        End Set
    End Property
    Public Property VATRate01() As ColField(Of Decimal)
        Get
            VATRate01 = mVATRate01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate01 = value
        End Set
    End Property
    Public Property VATRate02() As ColField(Of Decimal)
        Get
            VATRate02 = mVATRate02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate02 = value
        End Set
    End Property
    Public Property VATRate03() As ColField(Of Decimal)
        Get
            VATRate03 = mVATRate03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate03 = value
        End Set
    End Property
    Public Property VATRate04() As ColField(Of Decimal)
        Get
            VATRate04 = mVATRate04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate04 = value
        End Set
    End Property
    Public Property VATRate05() As ColField(Of Decimal)
        Get
            VATRate05 = mVATRate05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate05 = value
        End Set
    End Property
    Public Property VATRate06() As ColField(Of Decimal)
        Get
            VATRate06 = mVATRate06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate06 = value
        End Set
    End Property
    Public Property VATRate07() As ColField(Of Decimal)
        Get
            VATRate07 = mVATRate07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate07 = value
        End Set
    End Property
    Public Property VATRate08() As ColField(Of Decimal)
        Get
            VATRate08 = mVATRate08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate08 = value
        End Set
    End Property
    Public Property VATRate09() As ColField(Of Decimal)
        Get
            VATRate09 = mVATRate09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATRate09 = value
        End Set
    End Property
    Public Property HighestValDiscPcent() As ColField(Of Decimal)
        Get
            HighestValDiscPcent = mHighestValDiscPcent
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mHighestValDiscPcent = value
        End Set
    End Property


    Public Property TenderTypeDesc01() As ColField(Of String)
        Get
            TenderTypeDesc01 = mTenderTypeDesc01
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc01 = value
        End Set
    End Property
    Public Property TenderTypeDesc02() As ColField(Of String)
        Get
            TenderTypeDesc02 = mTenderTypeDesc02
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc02 = value
        End Set
    End Property
    Public Property TenderTypeDesc03() As ColField(Of String)
        Get
            TenderTypeDesc03 = mTenderTypeDesc03
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc03 = value
        End Set
    End Property
    Public Property TenderTypeDesc04() As ColField(Of String)
        Get
            TenderTypeDesc04 = mTenderTypeDesc04
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc04 = value
        End Set
    End Property
    Public Property TenderTypeDesc05() As ColField(Of String)
        Get
            TenderTypeDesc05 = mTenderTypeDesc05
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc05 = value
        End Set
    End Property
    Public Property TenderTypeDesc06() As ColField(Of String)
        Get
            TenderTypeDesc06 = mTenderTypeDesc06
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc06 = value
        End Set
    End Property
    Public Property TenderTypeDesc07() As ColField(Of String)
        Get
            TenderTypeDesc07 = mTenderTypeDesc07
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc07 = value
        End Set
    End Property
    Public Property TenderTypeDesc08() As ColField(Of String)
        Get
            TenderTypeDesc08 = mTenderTypeDesc08
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc08 = value
        End Set
    End Property
    Public Property TenderTypeDesc09() As ColField(Of String)
        Get
            TenderTypeDesc09 = mTenderTypeDesc09
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc09 = value
        End Set
    End Property
    Public Property TenderTypeDesc10() As ColField(Of String)
        Get
            TenderTypeDesc10 = mTenderTypeDesc10
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc10 = value
        End Set
    End Property
    Public Property TenderTypeDesc11() As ColField(Of String)
        Get
            TenderTypeDesc11 = mTenderTypeDesc11
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc11 = value
        End Set
    End Property
    Public Property TenderTypeDesc12() As ColField(Of String)
        Get
            TenderTypeDesc12 = mTenderTypeDesc12
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc12 = value
        End Set
    End Property
    Public Property TenderTypeDesc13() As ColField(Of String)
        Get
            TenderTypeDesc13 = mTenderTypeDesc13
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc13 = value
        End Set
    End Property
    Public Property TenderTypeDesc14() As ColField(Of String)
        Get
            TenderTypeDesc14 = mTenderTypeDesc14
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc14 = value
        End Set
    End Property
    Public Property TenderTypeDesc15() As ColField(Of String)
        Get
            TenderTypeDesc15 = mTenderTypeDesc15
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc15 = value
        End Set
    End Property
    Public Property TenderTypeDesc16() As ColField(Of String)
        Get
            TenderTypeDesc16 = mTenderTypeDesc16
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc16 = value
        End Set
    End Property
    Public Property TenderTypeDesc17() As ColField(Of String)
        Get
            TenderTypeDesc17 = mTenderTypeDesc17
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc17 = value
        End Set
    End Property
    Public Property TenderTypeDesc18() As ColField(Of String)
        Get
            TenderTypeDesc18 = mTenderTypeDesc18
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc18 = value
        End Set
    End Property
    Public Property TenderTypeDesc19() As ColField(Of String)
        Get
            TenderTypeDesc19 = mTenderTypeDesc19
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc19 = value
        End Set
    End Property
    Public Property TenderTypeDesc20() As ColField(Of String)
        Get
            TenderTypeDesc20 = mTenderTypeDesc20
        End Get
        Set(ByVal value As ColField(Of String))
            mTenderTypeDesc20 = value
        End Set
    End Property
    Public ReadOnly Property TenNoThisRelatesTo(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            Select Case (Index)
                Case (1) : Return mTenNoThisRelatesTo01
                Case (2) : Return mTenNoThisRelatesTo02
                Case (3) : Return mTenNoThisRelatesTo03
                Case (4) : Return mTenNoThisRelatesTo04
                Case (5) : Return mTenNoThisRelatesTo05
                Case (6) : Return mTenNoThisRelatesTo06
                Case (7) : Return mTenNoThisRelatesTo07
                Case (8) : Return mTenNoThisRelatesTo08
                Case (9) : Return mTenNoThisRelatesTo09
                Case (10) : Return mTenNoThisRelatesTo10
                Case (11) : Return mTenNoThisRelatesTo11
                Case (12) : Return mTenNoThisRelatesTo12
                Case (13) : Return mTenNoThisRelatesTo13
                Case (14) : Return mTenNoThisRelatesTo14
                Case (15) : Return mTenNoThisRelatesTo15
                Case (16) : Return mTenNoThisRelatesTo16
                Case (17) : Return mTenNoThisRelatesTo17
                Case (18) : Return mTenNoThisRelatesTo18
                Case (19) : Return mTenNoThisRelatesTo19
                Case (20) : Return mTenNoThisRelatesTo20
                Case Else : Return Nothing
            End Select
        End Get
    End Property
    Public Property TenNoThisRelatesTo01() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo01 = mTenNoThisRelatesTo01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo01 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo02() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo02 = mTenNoThisRelatesTo02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo02 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo03() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo03 = mTenNoThisRelatesTo03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo03 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo04() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo04 = mTenNoThisRelatesTo04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo04 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo05() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo05 = mTenNoThisRelatesTo05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo05 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo06() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo06 = mTenNoThisRelatesTo06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo06 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo07() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo07 = mTenNoThisRelatesTo07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo07 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo08() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo08 = mTenNoThisRelatesTo08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo08 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo09() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo09 = mTenNoThisRelatesTo09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo09 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo10() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo10 = mTenNoThisRelatesTo10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo10 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo11() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo11 = mTenNoThisRelatesTo11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo11 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo12() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo12 = mTenNoThisRelatesTo12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo12 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo13() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo13 = mTenNoThisRelatesTo13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo13 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo14() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo14 = mTenNoThisRelatesTo14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo14 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo15() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo15 = mTenNoThisRelatesTo15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo15 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo16() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo16 = mTenNoThisRelatesTo16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo16 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo17() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo17 = mTenNoThisRelatesTo17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo17 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo18() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo18 = mTenNoThisRelatesTo18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo18 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo19() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo19 = mTenNoThisRelatesTo19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo19 = value
        End Set
    End Property
    Public Property TenNoThisRelatesTo20() As ColField(Of Decimal)
        Get
            TenNoThisRelatesTo20 = mTenNoThisRelatesTo20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mTenNoThisRelatesTo20 = value
        End Set
    End Property
    Public Property OpenCashDrawer01() As ColField(Of Boolean)
        Get
            OpenCashDrawer01 = mOpenCashDrawer01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer01 = value
        End Set
    End Property
    Public Property OpenCashDrawer02() As ColField(Of Boolean)
        Get
            OpenCashDrawer02 = mOpenCashDrawer02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer02 = value
        End Set
    End Property
    Public Property OpenCashDrawer03() As ColField(Of Boolean)
        Get
            OpenCashDrawer03 = mOpenCashDrawer03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer03 = value
        End Set
    End Property
    Public Property OpenCashDrawer04() As ColField(Of Boolean)
        Get
            OpenCashDrawer04 = mOpenCashDrawer04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer04 = value
        End Set
    End Property
    Public Property OpenCashDrawer05() As ColField(Of Boolean)
        Get
            OpenCashDrawer05 = mOpenCashDrawer05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer05 = value
        End Set
    End Property
    Public Property OpenCashDrawer06() As ColField(Of Boolean)
        Get
            OpenCashDrawer06 = mOpenCashDrawer06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer06 = value
        End Set
    End Property
    Public Property OpenCashDrawer07() As ColField(Of Boolean)
        Get
            OpenCashDrawer07 = mOpenCashDrawer07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer07 = value
        End Set
    End Property
    Public Property OpenCashDrawer08() As ColField(Of Boolean)
        Get
            OpenCashDrawer08 = mOpenCashDrawer08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer08 = value
        End Set
    End Property
    Public Property OpenCashDrawer09() As ColField(Of Boolean)
        Get
            OpenCashDrawer09 = mOpenCashDrawer09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer09 = value
        End Set
    End Property
    Public Property OpenCashDrawer10() As ColField(Of Boolean)
        Get
            OpenCashDrawer10 = mOpenCashDrawer10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer10 = value
        End Set
    End Property
    Public Property OpenCashDrawer11() As ColField(Of Boolean)
        Get
            OpenCashDrawer11 = mOpenCashDrawer11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer11 = value
        End Set
    End Property
    Public Property OpenCashDrawer12() As ColField(Of Boolean)
        Get
            OpenCashDrawer12 = mOpenCashDrawer12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer12 = value
        End Set
    End Property
    Public Property OpenCashDrawer13() As ColField(Of Boolean)
        Get
            OpenCashDrawer13 = mOpenCashDrawer13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer13 = value
        End Set
    End Property
    Public Property OpenCashDrawer14() As ColField(Of Boolean)
        Get
            OpenCashDrawer14 = mOpenCashDrawer14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer14 = value
        End Set
    End Property
    Public Property OpenCashDrawer15() As ColField(Of Boolean)
        Get
            OpenCashDrawer15 = mOpenCashDrawer15
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer15 = value
        End Set
    End Property
    Public Property OpenCashDrawer16() As ColField(Of Boolean)
        Get
            OpenCashDrawer16 = mOpenCashDrawer16
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer16 = value
        End Set
    End Property
    Public Property OpenCashDrawer17() As ColField(Of Boolean)
        Get
            OpenCashDrawer17 = mOpenCashDrawer17
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer17 = value
        End Set
    End Property
    Public Property OpenCashDrawer18() As ColField(Of Boolean)
        Get
            OpenCashDrawer18 = mOpenCashDrawer18
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer18 = value
        End Set
    End Property
    Public Property OpenCashDrawer19() As ColField(Of Boolean)
        Get
            OpenCashDrawer19 = mOpenCashDrawer19
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer19 = value
        End Set
    End Property
    Public Property OpenCashDrawer20() As ColField(Of Boolean)
        Get
            OpenCashDrawer20 = mOpenCashDrawer20
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mOpenCashDrawer20 = value
        End Set
    End Property
    Public Property DefaultRemainAmt01() As ColField(Of Boolean)
        Get
            DefaultRemainAmt01 = mDefaultRemainAmt01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt01 = value
        End Set
    End Property
    Public Property DefaultRemainAmt02() As ColField(Of Boolean)
        Get
            DefaultRemainAmt02 = mDefaultRemainAmt02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt02 = value
        End Set
    End Property
    Public Property DefaultRemainAmt03() As ColField(Of Boolean)
        Get
            DefaultRemainAmt03 = mDefaultRemainAmt03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt03 = value
        End Set
    End Property
    Public Property DefaultRemainAmt04() As ColField(Of Boolean)
        Get
            DefaultRemainAmt04 = mDefaultRemainAmt04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt04 = value
        End Set
    End Property
    Public Property DefaultRemainAmt05() As ColField(Of Boolean)
        Get
            DefaultRemainAmt05 = mDefaultRemainAmt05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt05 = value
        End Set
    End Property
    Public Property DefaultRemainAm06() As ColField(Of Boolean)
        Get
            DefaultRemainAm06 = mDefaultRemainAm06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAm06 = value
        End Set
    End Property
    Public Property DefaultRemainAmt07() As ColField(Of Boolean)
        Get
            DefaultRemainAmt07 = mDefaultRemainAmt07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt07 = value
        End Set
    End Property
    Public Property DefaultRemainAmt08() As ColField(Of Boolean)
        Get
            DefaultRemainAmt08 = mDefaultRemainAmt08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt08 = value
        End Set
    End Property
    Public Property DefaultRemainAmt09() As ColField(Of Boolean)
        Get
            DefaultRemainAmt09 = mDefaultRemainAmt09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt09 = value
        End Set
    End Property
    Public Property DefaultRemainAmt10() As ColField(Of Boolean)
        Get
            DefaultRemainAmt10 = mDefaultRemainAmt10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt10 = value
        End Set
    End Property
    Public Property DefaultRemainAmt11() As ColField(Of Boolean)
        Get
            DefaultRemainAmt11 = mDefaultRemainAmt11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt11 = value
        End Set
    End Property
    Public Property DefaultRemainAmt12() As ColField(Of Boolean)
        Get
            DefaultRemainAmt12 = mDefaultRemainAmt12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt12 = value
        End Set
    End Property
    Public Property DefaultRemainAmt13() As ColField(Of Boolean)
        Get
            DefaultRemainAmt13 = mDefaultRemainAmt13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt13 = value
        End Set
    End Property
    Public Property DefaultRemainAmt14() As ColField(Of Boolean)
        Get
            DefaultRemainAmt14 = mDefaultRemainAmt14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt14 = value
        End Set
    End Property
    Public Property DefaultRemainAmt15() As ColField(Of Boolean)
        Get
            DefaultRemainAmt15 = mDefaultRemainAmt15
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt15 = value
        End Set
    End Property
    Public Property DefaultRemainAmt16() As ColField(Of Boolean)
        Get
            DefaultRemainAmt16 = mDefaultRemainAmt16
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt16 = value
        End Set
    End Property
    Public Property DefaultRemainAmt17() As ColField(Of Boolean)
        Get
            DefaultRemainAmt17 = mDefaultRemainAmt17
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt17 = value
        End Set
    End Property
    Public Property DefaultRemainAmt18() As ColField(Of Boolean)
        Get
            DefaultRemainAmt18 = mDefaultRemainAmt18
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt18 = value
        End Set
    End Property
    Public Property DefaultRemainAmt19() As ColField(Of Boolean)
        Get
            DefaultRemainAmt19 = mDefaultRemainAmt19
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt19 = value
        End Set
    End Property
    Public Property DefaultRemainAmt20() As ColField(Of Boolean)
        Get
            DefaultRemainAmt20 = mDefaultRemainAmt20
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDefaultRemainAmt20 = value
        End Set
    End Property
    Public Property MiscOpenDrawReasonNo() As ColField(Of Decimal)
        Get
            MiscOpenDrawReasonNo = mMiscOpenDrawReasonNo
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMiscOpenDrawReasonNo = value
        End Set
    End Property
    Public Property MaxTillDrawFloat() As ColField(Of Decimal)
        Get
            MaxTillDrawFloat = mMaxTillDrawFloat
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMaxTillDrawFloat = value
        End Set
    End Property
    Public Property MinSavValBeforeMess() As ColField(Of Decimal)
        Get
            MinSavValBeforeMess = mMinSavValBeforeMess
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinSavValBeforeMess = value
        End Set
    End Property
    Public Property MaxRefundAmtSelfAuth() As ColField(Of Decimal)
        Get
            MaxRefundAmtSelfAuth = mMaxRefundAmtSelfAuth
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMaxRefundAmtSelfAuth = value
        End Set
    End Property
    Public Property TillAccessEventFiles() As ColField(Of Boolean)
        Get
            TillAccessEventFiles = mTillAccessEventFiles
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mTillAccessEventFiles = value
        End Set
    End Property
    Public Property SecStaffDiscountPer() As ColField(Of Decimal)
        Get
            SecStaffDiscountPer = mSecStaffDiscountPer
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mSecStaffDiscountPer = value
        End Set
    End Property
    Public Property VATPercDiscount() As ColField(Of Decimal)
        Get
            VATPercDiscount = mVATPercDiscount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mVATPercDiscount = value
        End Set
    End Property
    Public Property VATDiscountActive() As ColField(Of Boolean)
        Get
            VATDiscountActive = mVATDiscountActive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mVATDiscountActive = value
        End Set
    End Property
    Public Property FeeStatementOnSlips() As ColField(Of Boolean)
        Get
            FeeStatementOnSlips = mFeeStatementOnSlips
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mFeeStatementOnSlips = value
        End Set
    End Property
    Public Property SupervisorReqTS01() As ColField(Of Boolean)
        Get
            SupervisorReqTS01 = mSupervisorReqTS01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS01 = value
        End Set
    End Property
    Public Property SupervisorReqTS02() As ColField(Of Boolean)
        Get
            SupervisorReqTS02 = mSupervisorReqTS02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS02 = value
        End Set
    End Property
    Public Property SupervisorReqTS03() As ColField(Of Boolean)
        Get
            SupervisorReqTS03 = mSupervisorReqTS03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS03 = value
        End Set
    End Property
    Public Property SupervisorReqTS04() As ColField(Of Boolean)
        Get
            SupervisorReqTS04 = mSupervisorReqTS04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS04 = value
        End Set
    End Property
    Public Property SupervisorReqTS05() As ColField(Of Boolean)
        Get
            SupervisorReqTS05 = mSupervisorReqTS05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS05 = value
        End Set
    End Property
    Public Property SupervisorReqTS06() As ColField(Of Boolean)
        Get
            SupervisorReqTS06 = mSupervisorReqTS06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS06 = value
        End Set
    End Property
    Public Property SupervisorReqTS07() As ColField(Of Boolean)
        Get
            SupervisorReqTS07 = mSupervisorReqTS07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS07 = value
        End Set
    End Property
    Public Property SupervisorReqTS08() As ColField(Of Boolean)
        Get
            SupervisorReqTS08 = mSupervisorReqTS08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS08 = value
        End Set
    End Property
    Public Property SupervisorReqTS09() As ColField(Of Boolean)
        Get
            SupervisorReqTS09 = mSupervisorReqTS09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS09 = value
        End Set
    End Property
    Public Property SupervisorReqTS10() As ColField(Of Boolean)
        Get
            SupervisorReqTS10 = mSupervisorReqTS10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS10 = value
        End Set
    End Property
    Public Property SupervisorReqTS11() As ColField(Of Boolean)
        Get
            SupervisorReqTS11 = mSupervisorReqTS11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS11 = value
        End Set
    End Property
    Public Property SupervisorReqTS12() As ColField(Of Boolean)
        Get
            SupervisorReqTS12 = mSupervisorReqTS12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS12 = value
        End Set
    End Property
    Public Property SupervisorReqTS13() As ColField(Of Boolean)
        Get
            SupervisorReqTS13 = mSupervisorReqTS13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS13 = value
        End Set
    End Property
    Public Property SupervisorReqTS14() As ColField(Of Boolean)
        Get
            SupervisorReqTS14 = mSupervisorReqTS14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS14 = value
        End Set
    End Property
    Public Property SupervisorReqTS15() As ColField(Of Boolean)
        Get
            SupervisorReqTS15 = mSupervisorReqTS15
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS15 = value
        End Set
    End Property
    Public Property SupervisorReqTS16() As ColField(Of Boolean)
        Get
            SupervisorReqTS16 = mSupervisorReqTS16
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS16 = value
        End Set
    End Property
    Public Property SupervisorReqTS17() As ColField(Of Boolean)
        Get
            SupervisorReqTS17 = mSupervisorReqTS17
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS17 = value
        End Set
    End Property
    Public Property SupervisorReqTS18() As ColField(Of Boolean)
        Get
            SupervisorReqTS18 = mSupervisorReqTS18
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS18 = value
        End Set
    End Property
    Public Property SupervisorReqTS19() As ColField(Of Boolean)
        Get
            SupervisorReqTS19 = mSupervisorReqTS19
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS19 = value
        End Set
    End Property
    Public Property SupervisorReqTS20() As ColField(Of Boolean)
        Get
            SupervisorReqTS20 = mSupervisorReqTS20
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mSupervisorReqTS20 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS01() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS01 = mCustomerDtlReqTS01
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS01 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS02() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS02 = mCustomerDtlReqTS02
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS02 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS03() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS03 = mCustomerDtlReqTS03
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS03 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS04() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS04 = mCustomerDtlReqTS04
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS04 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS05() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS05 = mCustomerDtlReqTS05
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS05 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS06() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS06 = mCustomerDtlReqTS06
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS06 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS07() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS07 = mCustomerDtlReqTS07
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS07 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS08() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS08 = mCustomerDtlReqTS08
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS08 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS09() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS09 = mCustomerDtlReqTS09
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS09 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS10() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS10 = mCustomerDtlReqTS10
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS10 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS11() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS11 = mCustomerDtlReqTS11
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS11 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS12() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS12 = mCustomerDtlReqTS12
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS12 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS13() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS13 = mCustomerDtlReqTS13
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS13 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS14() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS14 = mCustomerDtlReqTS14
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS14 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS15() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS15 = mCustomerDtlReqTS15
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS15 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS16() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS16 = mCustomerDtlReqTS16
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS16 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS17() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS17 = mCustomerDtlReqTS17
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS17 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS18() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS18 = mCustomerDtlReqTS18
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS18 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS19() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS19 = mCustomerDtlReqTS19
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS19 = value
        End Set
    End Property
    Public Property CustomerDtlReqTS20() As ColField(Of Boolean)
        Get
            CustomerDtlReqTS20 = mCustomerDtlReqTS20
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mCustomerDtlReqTS20 = value
        End Set
    End Property
    Public Property MinRefundRequiringSA() As ColField(Of Decimal)
        Get
            MinRefundRequiringSA = mMinRefundRequiringSA
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinRefundRequiringSA = value
        End Set
    End Property
    Public Property MinRefundRequiringMA() As ColField(Of Decimal)
        Get
            MinRefundRequiringMA = mMinRefundRequiringMA
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinRefundRequiringMA = value
        End Set
    End Property
    Public Property MinCollDiscTranValSA() As ColField(Of Decimal)
        Get
            MinCollDiscTranValSA = mMinCollDiscTranValSA
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinCollDiscTranValSA = value
        End Set
    End Property
    Public Property MinColDiscTranValMA() As ColField(Of Decimal)
        Get
            MinColDiscTranValMA = mMinColDiscTranValMA
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinColDiscTranValMA = value
        End Set
    End Property
    Public Property MaxValGiftToken() As ColField(Of Decimal)
        Get
            MaxValGiftToken = mMaxValGiftToken
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMaxValGiftToken = value
        End Set
    End Property
    Public Property MinChgGivenGiftToken() As ColField(Of Decimal)
        Get
            MinChgGivenGiftToken = mMinChgGivenGiftToken
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mMinChgGivenGiftToken = value
        End Set
    End Property
    Public Property SalesMaxField() As ColField(Of Decimal)
        Get
            SalesMaxField = mSalesMaxField
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mSalesMaxField = value
        End Set
    End Property
    Public Property AutoAppPriceChgAuth() As ColField(Of Boolean)
        Get
            AutoAppPriceChgAuth = mAutoAppPriceChgAuth
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mAutoAppPriceChgAuth = value
        End Set
    End Property
    Public Property MgrAuthorisedAA() As ColField(Of String)
        Get
            MgrAuthorisedAA = mMgrAuthorisedAA
        End Get
        Set(ByVal value As ColField(Of String))
            mMgrAuthorisedAA = value
        End Set
    End Property
    Public Property HostuLoadInProgress() As ColField(Of String)
        Get
            HostuLoadInProgress = mHostuLoadInProgress
        End Get
        Set(ByVal value As ColField(Of String))
            mHostuLoadInProgress = value
        End Set
    End Property
    Public Property CountryCode() As ColField(Of String)
        Get
            CountryCode = mCountryCode
        End Get
        Set(ByVal value As ColField(Of String))
            mCountryCode = value
        End Set
    End Property


#End Region

#Region "Enumerations"

    Public Enum Accountabilities
        None
        Cashier
        Till
    End Enum

    Public ReadOnly Property Accountability() As Accountabilities
        Get
            Select Case _AccountabilityType.Value
                Case "C" : Return Accountabilities.Cashier
                Case "T" : Return Accountabilities.Till
                Case Else : Return Accountabilities.None
            End Select
        End Get
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all fields of retail options into current instance. Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadRetailOptions()

        Try
            ClearLists()
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _ID, "01")
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.RetOptionsLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads store ID, name and accountability type into current instance. Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadStoreAndAccountability()

        Try
            ClearLists()
            AddLoadField(_Store)
            AddLoadField(_StoreName)
            AddLoadField(_AccountabilityType)
            AddLoadField(mTenNoThisRelatesTo01)
            AddLoadField(mTenNoThisRelatesTo02)
            AddLoadField(mTenNoThisRelatesTo03)
            AddLoadField(mTenNoThisRelatesTo04)
            AddLoadField(mTenNoThisRelatesTo05)
            AddLoadField(mTenNoThisRelatesTo06)
            AddLoadField(mTenNoThisRelatesTo07)
            AddLoadField(mTenNoThisRelatesTo08)
            AddLoadField(mTenNoThisRelatesTo09)
            AddLoadField(mTenNoThisRelatesTo10)
            AddLoadField(mTenNoThisRelatesTo11)
            AddLoadField(mTenNoThisRelatesTo12)
            AddLoadField(mTenNoThisRelatesTo13)
            AddLoadField(mTenNoThisRelatesTo14)
            AddLoadField(mTenNoThisRelatesTo15)
            AddLoadField(mTenNoThisRelatesTo16)
            AddLoadField(mTenNoThisRelatesTo17)
            AddLoadField(mTenNoThisRelatesTo18)
            AddLoadField(mTenNoThisRelatesTo19)
            AddLoadField(mTenNoThisRelatesTo20)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _ID, "01")
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.RetOptionsLoad, ex)
        End Try

    End Sub

    Public Function RetailOptions() As DataRow
        Dim ds As DataSet

        Me.ClearLoadFilter()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.RetailOptionsID, "01")
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            End If
        End If

        Return Nothing
    End Function

    Public Function GetDefaultFloat() As Decimal

        ClearLoadField()
        ClearLoadFilter()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.RetailOptionsID, "01")
        LoadMatches()

        Return DefaultFloatAmountAT.Value

    End Function

    Public Function SetAuthorisePriceChange(ByVal AutoApply As Boolean, ByVal Manager As String) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(mAutoAppPriceChgAuth.ColumnName, AutoApply)
            Oasys3DB.SetColumnAndValueParameter(mMgrAuthorisedAA.ColumnName, Manager)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            NoRecs = Oasys3DB.Update()
            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If
            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cRetailOptions - SetAuthorisePriceChange exception " & ex.Message)
            SaveError = True
            Return SaveError
        End Try


    End Function

    Public Function SetHOSTULoad(ByVal DoingLoad As Boolean) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(mHostuLoadInProgress.ColumnName, IIf(DoingLoad, "Y", "N"))
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")
            NoRecs = Oasys3DB.Update()
            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If
            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cRetailOptions - SetHOSTULoad exception " & ex.Message)
            SaveError = True
            Return SaveError
        End Try

    End Function

    ''' <summary>
    ''' Returns Store ID and name separated by one space of this instance
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StoreIdName() As String

        Return _Store.Value & Space(1) & _StoreName.Value.Trim

    End Function

    ''' <summary>
    ''' Returns store name and address separated on separate lines. Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StoreNameAddress() As String

        Try
            Dim sb As New StringBuilder
            If _CompanyName.Value <> String.Empty Then sb.Append(_CompanyName.Value & Environment.NewLine)
            If _StoreName.Value <> String.Empty Then sb.Append(_StoreName.Value & Environment.NewLine)
            If _AddressLine1.Value <> String.Empty Then sb.Append(_AddressLine1.Value & Environment.NewLine)
            If _AddressLine2.Value <> String.Empty Then sb.Append(_AddressLine2.Value & Environment.NewLine)
            If _AddressLine3.Value <> String.Empty Then sb.Append(_AddressLine3.Value & Environment.NewLine)
            Return sb.ToString

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StoreGetNameAddress, ex)
        End Try

    End Function


    Public Function UpdateLastReportDate(ByVal reportDate As Date) As Boolean

        mLastReformatDate.Value = reportDate
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(mLastReformatDate.ColumnName, mLastReformatDate.Value.ToString("dd/MM/yy"))
        Return (Oasys3DB.Update > 0)

    End Function

#End Region

End Class

