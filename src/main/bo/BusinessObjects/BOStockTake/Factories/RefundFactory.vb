﻿Public Class RefundFactory
    Inherits Global.TpWickes.Library.BaseFactory(Of IRefund)

    Public Overrides Function Implementation() As IRefund

        Return New RefundNew()
    End Function
End Class