﻿Public Class HHTHeaderFactory
    Inherits RequirementSwitchFactory(Of IHHTHeader)

    Public Overrides Function ImplementationA() As IHHTHeader

        Return New HHTHeaderNew_For_LoadRefundedItems
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(980081)
    End Function

    Public Overrides Function ImplementationB() As IHHTHeader

        Return New HHTHeaderExisting_For_LoadRefundedItems
    End Function
End Class
