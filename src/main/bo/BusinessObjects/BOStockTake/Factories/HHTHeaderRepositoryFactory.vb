﻿Public Class HHTHeaderRepositoryFactory

    Private Shared m_FactoryMember As IHHTHeaderRepository

    Public Shared Function FactoryGet() As IHHTHeaderRepository

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New HHTHeaderRepository           'live implementation
        Else
            Return m_FactoryMember                     'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IHHTHeaderRepository)

        m_FactoryMember = obj

    End Sub

End Class