﻿Namespace TpWickes

    Public Class RequirementRepositoryFactory

        Private Shared m_FactoryMember As IRequirementRepository

        Public Shared Function FactoryGet() As IRequirementRepository

            If Not m_FactoryMember Is Nothing Then

                Return m_FactoryMember                        'stub implementation

            End If
            Return Nothing
        End Function

        Public Shared Sub FactorySet(ByVal obj As IRequirementRepository)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace