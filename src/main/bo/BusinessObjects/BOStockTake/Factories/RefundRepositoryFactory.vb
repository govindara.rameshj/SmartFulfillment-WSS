﻿Public Class RefundRepositoryFactory
    Inherits Global.TpWickes.Library.BaseFactory(Of IRefundRepository)

    Public Overrides Function Implementation() As IRefundRepository

        Return New RefundRepositoryNew
    End Function
End Class