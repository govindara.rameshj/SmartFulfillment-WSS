﻿<Serializable()> Public Class cHhtDetail
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HHTDET"
        BOFields.Add(_DateCreated)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_NonStockItem)
        BOFields.Add(_PreSold)
        BOFields.Add(_StockOnHand)
        BOFields.Add(_StockSold)
        BOFields.Add(_Adjusted)
        BOFields.Add(_LabelDetailOK)
        BOFields.Add(_SkuOrigin)
        BOFields.Add(_markdownQty)
        BOFields.Add(_markdownCount)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHhtDetail)

        LoadBORecords(count)

        Dim col As New List(Of cHhtDetail)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cHhtDetail))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHhtDetail(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _DateCreated As New ColField(Of Date)("DATE1", Nothing, "Date Created", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "SKU Number", True, False)
    Private _NonStockItem As New ColField(Of Boolean)("INON", False, "Non Stock Item", False, False)
    Private _PreSold As New ColField(Of Integer)("TPRE", 0, "Pre Sold", False, False)
    Private _StockOnHand As New ColField(Of Integer)("ONHA", 0, "Stock On Hand", False, False)
    Private _StockSold As New ColField(Of Integer)("StockSold", 0, "Stock Sold", False, False)
    Private _Adjusted As New ColField(Of Boolean)("IADJ", False, "Adjusted", False, False)
    Private _LabelDetailOK As New ColField(Of Boolean)("LBOK", False, "Label Detail OK", False, False)
    Private _SkuOrigin As New ColField(Of String)("ORIG", "", "SKU Origin", False, False)
    Private _markdownQty As New ColField(Of Decimal)("MDNQ", 0, "Markdown Qty", False, False)
    Private _markdownCount As New ColField(Of Decimal)("TMDC", 0, "Markdown Count", False, False)

#End Region

#Region "Field Properties"

    Public Property DateCreated() As ColField(Of Date)
        Get
            Return _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property NonStockItem() As ColField(Of Boolean)
        Get
            Return _NonStockItem
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _NonStockItem = value
        End Set
    End Property
    Public Property PreSold() As ColField(Of Integer)
        Get
            Return _PreSold
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PreSold = value
        End Set
    End Property
    Public Property StockOnHand() As ColField(Of Integer)
        Get
            Return _StockOnHand
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnHand = value
        End Set
    End Property
    Public Property StockSold() As ColField(Of Integer)
        Get
            Return _StockSold
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockSold = value
        End Set
    End Property
    Public Property Adjusted() As ColField(Of Boolean)
        Get
            Return _Adjusted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Adjusted = value
        End Set
    End Property
    Public Property LabelDetailOK() As ColField(Of Boolean)
        Get
            Return _LabelDetailOK
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LabelDetailOK = value
        End Set
    End Property
    Public Property SkuOrigin() As ColField(Of String)
        Get
            Return _SkuOrigin
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuOrigin = value
        End Set
    End Property
    Public Property MarkdownQty() As ColField(Of Decimal)
        Get
            Return _markdownQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _markdownQty = value
        End Set
    End Property
    Public Property MarkdownCount() As ColField(Of Decimal)
        Get
            Return _markdownCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _markdownCount = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Stock As BOStock.cStock = Nothing
    Private _Locations As List(Of cHhtLocation) = Nothing

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property
    Public Property Locations() As List(Of cHhtLocation)
        Get
            If _Locations Is Nothing Then LoadLocations()
            Return _Locations
        End Get
        Set(ByVal value As List(Of cHhtLocation))
            _Locations = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads detail record into this instance for given parameters or creates if does not exist.
    ''' </summary>
    ''' <param name="dateCreated"></param>
    ''' <param name="skuNumber"></param>
    ''' <remarks></remarks>
    Public Sub Load(ByVal dateCreated As Date, ByVal skuNumber As String)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, dateCreated)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, skuNumber)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'load detail class from data row
        If dt.Rows.Count = 0 Then
            _DateCreated.Value = dateCreated
            _SkuNumber.Value = skuNumber
        Else
            Me.LoadFromRow(dt.Rows(0))
        End If

    End Sub

    ''' <summary>
    ''' Load all locations for this header into Locations collection.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadLocations(Optional ByVal excludeDeletions As Boolean = False)

        'get locations for this header
        Dim hhtLocation As New cHhtLocation(Oasys3DB)
        _Locations = hhtLocation.GetLocations(_DateCreated.Value, _SkuNumber.Value, excludeDeletions)

    End Sub

    ''' <summary>
    ''' Returns all hht detail records for given date.
    ''' </summary>
    ''' <param name="dateCreated"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDetails(ByVal dateCreated As Date) As List(Of cHhtDetail)

        Dim details As New List(Of cHhtDetail)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, dateCreated)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'load detail class from data row
        For Each dr As DataRow In dt.Rows
            Dim d As New cHhtDetail(Oasys3DB)
            d.LoadFromRow(dr)
            details.Add(d)
        Next

        Return details

    End Function

    ''' <summary>
    ''' Returns list of hht detail refund records with no location records
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDetailsNonCountedRefunds(ByVal startDate As Date) As List(Of cHhtDetail)

        'get all refund details
        Dim details As New List(Of cHhtDetail)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, startDate)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_SkuOrigin.ColumnName, clsOasys3DB.eOperator.pEquals, "R")
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'loop through rows only adding details with no counts
        Dim hhtlocation As New cHhtLocation(Oasys3DB)
        For Each dr As DataRow In dt.Rows
            Dim locs As List(Of cHhtLocation) = hhtlocation.GetLocations(startDate, CStr(dr.Item(_SkuNumber.ColumnName)))
            If locs.Count = 0 Then
                Dim d As New cHhtDetail(Oasys3DB)
                d.LoadFromRow(dr)
                details.Add(d)
            End If
        Next

        Return details

    End Function

    ''' <summary>
    ''' Returns total count of all hht location entries for this hht detail stock item.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TotalCount() As Integer

        'loop over locations to get total count (forcing load if necessary)
        Dim total As Integer = 0
        For Each l As cHhtLocation In Locations
            If l.IsDeleted.Value Then Continue For
            total += l.StockCount.Value
        Next

        Return total

    End Function

    Public Function TotalCount(ByVal type As CountType) As Integer

        Dim total As Integer = 0
        For Each l As cHhtLocation In Locations
            If l.IsDeleted.Value Then Continue For
            Select Case type
                Case CountType.All
                    total += l.StockCount.Value
                Case CountType.Normal
                    If l.IslandType.Value = "S" Then total += l.StockCount.Value
                    If l.IslandType.Value = "W" Then total += l.StockCount.Value
                Case CountType.Markdown
                    If l.IslandType.Value = "M" Then total += l.StockCount.Value
            End Select
        Next
        Return total

    End Function


    ''' <summary>
    ''' Loads stock item for this location into Stock object.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadStock()

        _Stock = New BOStock.cStock(Oasys3DB)
        _Stock.LoadStockItem(_SkuNumber.Value)

    End Sub


    ''' <summary>
    ''' Equivalent price for labelling
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ISISGetProductLookup(ByVal strSKU As String, ByVal strSerialNo As String) As String
        Dim intSerial As String = String.Empty
        Dim BOStockLookup As New BOStock.cStock(Oasys3DB)
        Dim BOMarkdown As New BOStock.cMarkdownStock(Oasys3DB)
        Dim BOStockDetail As New BOStockTake.cHhtDetail(Oasys3DB)
        Dim strMessageResponse As String = String.Empty
        Dim result As New Object

        BOStockLookup.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockLookup.SkuNumber, strSKU)
        If BOStockLookup.LoadMatches.Count > 0 Then
            'Quantity on Hand - Shop Floor Currently includes "Warehouse"
            strMessageResponse += BOStockLookup.StockOnHand.Value.ToString("000000;-00000")
            'Quantity on Hand - Warehouse Not currently used - set to "000000"
            strMessageResponse += "000000"
            'Quantity on Hand - Pre-sold Not currently used - set to "000000"
            strMessageResponse += "000000"
            'Quantity on Order
            strMessageResponse += BOStockLookup.StockOnOrder.Value.ToString("000000")
            'Current in-store price
            strMessageResponse += BOStockLookup.NormalSellPrice.Value.ToString("000000.00").Replace(".", "")
            'Full in-store price
            strMessageResponse += BOStockLookup.NormalSellPrice.Value.ToString("000000.00").Replace(".", "")
            'Average in-store weekly sale
            strMessageResponse += BOStockLookup.AverageWeeklySales.Value.ToString("0000;-000")

            'Serial Number for markdown label (if requested) or existing markdown serial number (from "request").Otherwise(-0)
            Select Case strSerialNo
                'no serial code selected defual all to 0
                Case "000000"
                    'serial code
                    strMessageResponse += "000000"
                    'markdown stage
                    strMessageResponse += "00"
                    'reason code
                    strMessageResponse += "00"
                Case "999999" 'reqeust new serial code

                    BOMarkdown.AddAggregateField(clsOasys3DB.eAggregates.pAggMax, BOMarkdown.Serial, "intSerial")
                    result = BOMarkdown.GetAggregateField

                    If result IsNot DBNull.Value Then
                        intSerial = CStr(CInt(result) + 1)
                    Else
                        intSerial = "1"
                    End If

                    Do Until intSerial.Length = 6
                        intSerial = "0" & intSerial
                    Loop
                    BOMarkdown.Serial.Value = intSerial
                    BOMarkdown.SkuNumber.Value = strSKU
                    BOMarkdown.IsReserved.Value = True
                    BOMarkdown.DateCreated.Value = Date.Today

                    If BOMarkdown.SaveIfNew = True Then
                        strMessageResponse += intSerial
                        strMessageResponse += "00"
                        strMessageResponse += "00"
                    End If

                Case Else 'find details on the serial code
                    BOMarkdown.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOMarkdown.SkuNumber, strSKU)
                    BOMarkdown.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOMarkdown.Serial, strSerialNo)
                    BOMarkdown.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                    If BOMarkdown.LoadMatches.Count > 0 Then
                        strMessageResponse += BOMarkdown.Serial.Value
                        'If a "markdown SKU" is being handled, the markdown stage - 01 to 10; otherwise 00
                        If BOMarkdown.WeekNumber.Value < 10 Then
                            strMessageResponse += "0" & BOMarkdown.WeekNumber.Value
                        Else
                            strMessageResponse += CStr(BOMarkdown.WeekNumber.Value)
                        End If
                        'If a "markdown SKU" is being handled, the Reason Code; otherwise(0)
                        strMessageResponse += BOMarkdown.ReasonCode.Value
                    End If
            End Select 'strSerialNo
            'Blank if not on promotion Currently just used as a "binary" flag
            strMessageResponse += "               "
            ''Y' if "SKU can be ordered"; otherwise() 'N'
            If BOStockLookup.DoNotOrder.Value = False Then
                strMessageResponse += "Y"
            Else
                strMessageResponse += "N"
            End If

            ''Y' if "SKU deleted from store"; otherwise() 'N'
            If BOStockLookup.ItemDeleted.Value = True Then
                strMessageResponse += "Y"
            Else
                strMessageResponse += "N"
            End If

            ''Y' if "SKU not stocked in store"; otherwise() 'N'
            If BOStockLookup.NonStockItem.Value = True Then
                strMessageResponse += "Y"
            Else
                strMessageResponse += "N"
            End If

            ''Y' if "SKU in today's PIC"; otherwise() 'N'
            BOStockDetail.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockDetail.DateCreated, Date.Today)
            BOStockDetail.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockDetail.SkuNumber, strSKU)
            BOStockDetail.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            If BOStockDetail.LoadMatches.Count > 0 Then
                strMessageResponse += "Y"
            Else
                strMessageResponse += "N"
            End If

            'Small Label.Standard quantities for SKU.Can be "0".Not currently used
            '  If BOStockLookup.NoOfSmallLabels.Value > 0 Then
            'strMessageResponse += BOStockLookup.NoOfSmallLabels.Value
            'Else
            strMessageResponse += "0 "
            ' End If

            'Medium label.Standard quantities for SKU.Can be "0".Not currently used
            'If BOStockLookup.NoOfMediumLabels.Value > 0 Then
            'strMessageResponse += BOStockLookup.NoOfMediumLabels.Value
            'Else
            strMessageResponse += "0 "
            'End If

            'Large label.Standard quantities for SKU.Can be "0".Not currently used
            'If BOStockLookup.NoOfLargeLabels.Value > 0 Then
            'strMessageResponse += BOStockLookup.NoOfLargeLabels.Value
            ' Else
            strMessageResponse += "0 "
            'End If
        End If
        Return strMessageResponse

    End Function


    'Public Sub DeleteDetails(ByVal dateToDelete As Date)

    '    Try
    '        Oasys3DB.ClearAllParameters()
    '        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
    '        Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, dateToDelete)
    '        Oasys3DB.Delete()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    Public Function ISISAddStockCheck(ByVal listSKU As List(Of String)) As Boolean

        Dim intCount As Integer = 0
        Dim BoStocktakeDetail As New BOStockTake.cHhtDetail(Oasys3DB)
        Dim BOStockTakeHeader As New BOStockTake.cHhtHeader(Oasys3DB)
        Dim BOStock As New BOStock.cStock(Oasys3DB)

        BOStockTakeHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockTakeHeader.DateCreated, CDate(Format(Today.AddDays(1), "yyyy-MM-dd")))
        If BOStockTakeHeader.LoadMatches.Count = 0 Then
            BOStockTakeHeader.DateCreated.Value = CDate(Format(Today.AddDays(1), "yyyy-MM-dd"))
            If BOStockTakeHeader.SaveIfNew = False Then
                Return False
            End If
        End If

        Do Until intCount = listSKU.Count

            BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.SkuNumber, listSKU.Item(intCount))
            If BOStock.LoadMatches.Count > 0 Then
                BOStockTakeHeader.AddDetail(BOStock)
            End If

            intCount += 1
        Loop

        BOStockTakeHeader.Update()
        Return True

    End Function

#End Region

End Class

Public Enum CountType
    All
    Normal
    Markdown
End Enum