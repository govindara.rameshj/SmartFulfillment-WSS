﻿<Serializable()> Public Class cHhtLocation
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HhtLocation"
        BOFields.Add(_DateCreated)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Sequence)
        BOFields.Add(_IslandId)
        BOFields.Add(_IslandSegment)
        BOFields.Add(_IslandType)
        BOFields.Add(_PlanogramId)
        BOFields.Add(_StockCount)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHhtLocation)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHhtLocation)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHhtLocation))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHhtLocation(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _DateCreated As New ColField(Of Date)("DateCreated", Nothing, "Date Created", True, False)
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "SKU Number", True, False)
    Private _Sequence As New ColField(Of Integer)("Sequence", 1, "Count Sequence", True, False)
    Private _IslandId As New ColField(Of Integer)("IslandId", 0, "Island ID", True, False)
    Private _IslandType As New ColField(Of String)("IslandType", "", "Island Type", True, False)
    Private _IslandSegment As New ColField(Of Integer)("IslandSegment", 0, "Island Segment", True, False)
    Private _PlanogramId As New ColField(Of Integer)("PlanogramId", 0, "Planogram ID", False, False)
    Private _StockCount As New ColField(Of Integer)("StockCount", 0, "Stock Count", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IsDeleted", False, "Is Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property DateCreated() As ColField(Of Date)
        Get
            Return _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of Integer)
        Get
            Return _Sequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Sequence = value
        End Set
    End Property
    Public Property IslandId() As ColField(Of Integer)
        Get
            Return _IslandId
        End Get
        Set(ByVal value As ColField(Of Integer))
            _IslandId = value
        End Set
    End Property
    Public Property IslandType() As ColField(Of String)
        Get
            Return _IslandType
        End Get
        Set(ByVal value As ColField(Of String))
            _IslandType = value
        End Set
    End Property
    Public Property IslandSegment() As ColField(Of Integer)
        Get
            Return _IslandSegment
        End Get
        Set(ByVal value As ColField(Of Integer))
            _IslandSegment = value
        End Set
    End Property
    Public Property PlanogramId() As ColField(Of Integer)
        Get
            Return _PlanogramId
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PlanogramId = value
        End Set
    End Property
    Public Property StockCount() As ColField(Of Integer)
        Get
            Return _StockCount
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockCount = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            Return _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns all hht location records for given date.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="dateCreated"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLocations(ByVal dateCreated As Date, ByVal skuNumber As String, Optional ByVal excludeDeletions As Boolean = False) As List(Of cHhtLocation)

        Try
            Dim details As New List(Of cHhtLocation)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, dateCreated)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, skuNumber)
            If excludeDeletions Then
                Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                Oasys3DB.SetWhereParameter(_IsDeleted.ColumnName, clsOasys3DB.eOperator.pEquals, False)
            End If

            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'load detail class from data row
            For Each dr As DataRow In dt.Rows
                Dim d As New cHhtLocation(Oasys3DB)
                d.LoadFromRow(dr)
                details.Add(d)
            Next

            Return details

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.hhtLocationLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns arraylist of sku numbers for all stock items counted within given dates.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSkuNumbersWithin(ByVal startDate As Date, ByVal endDate As Date) As ArrayList

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggList, _SkuNumber.ColumnName, _SkuNumber.ColumnName)
            Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pGreaterThanOrEquals, startDate)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, endDate)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim skus As New ArrayList
            For Each dr As DataRow In dt.Rows
                skus.Add(dr.Item(0))
            Next

            Return skus

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.hhtLocationGetSkus, ex)
        End Try

    End Function

    ''' <summary>
    ''' Perpetual Inventory update(send a list of SKUs for a location)
    ''' </summary>
    ''' <param name="listSKU"></param>
    ''' <param name="listQuantitySold"></param>
    ''' <param name="listQuantityCounted"></param>
    ''' <param name="number"></param>
    ''' <param name="segment"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ISISCreatePerpetualInventoryUpdate(ByVal listSKU As List(Of String), _
                                                       ByVal listQuantitySold As List(Of Integer), _
                                                       ByVal listQuantityCounted As List(Of Integer), _
                                                       ByVal number As String, ByVal segment As String) As Boolean
        Dim strLocation As String = String.Empty
        Dim intQuantity As Integer = 0
        Dim intCount As Integer = 0
        Dim intPlanoGramID As String = String.Empty
        Dim BOHHTLocation As New BOStockTake.cHhtLocation(Oasys3DB)
        Dim BOHHTDetails As New BOStockTake.cHhtDetail(Oasys3DB)
        Dim BOHeader As New BOStockTake.cHhtHeader(Oasys3DB)
        Dim islandId As String = String.Empty
        Dim islandCode As String = String.Empty
        Dim islandSegment As String = String.Empty
        Dim Locationlist As New List(Of BOStockTake.cHhtLocation)

        Using ds As New BOStockLocation.IslandDataSet(Oasys3DB.SqlServerConnectionString)
            'load all islands from into dataset
            ds.Islands.LoadData()

            Do Until intCount = listSKU.Count

                'Loads up the current hhtlocation records
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.SkuNumber, listSKU.Item(intCount))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandId, CInt(islandId))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandSegment, CInt(islandSegment))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandType, islandCode)
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IsDeleted, False)

                'puts them into a list
                Locationlist = BOHHTLocation.LoadMatches

                'checks to see if there are any records
                If Locationlist.Count > 0 Then
                    'Changes there Isdeleted value to true and then saves them.
                    For Each location In Locationlist
                        location.IsDeleted.Value = True
                        location.SaveIfExists()
                    Next
                End If

                'Loads the current island information 
                'changed to use the island typed data set (DH 17/08/2009)
                Dim island As BOStockLocation.IslandDataSet.IslandsRow = ds.Islands.GetIslandByPlanogram(number, segment)
                If island IsNot Nothing Then
                    islandId = CStr(island.IslandId)
                    islandCode = island.Code
                    islandSegment = CStr(island.Id)
                Else
                    Exit Function
                End If

                'Loads the current HHTDetail record or if one does not exsit creates one
                BOHHTDetails.Load(Date.Now, listSKU.Item(intCount))
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.SkuNumber, listSKU.Item(intCount))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandId, CInt(islandId))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandSegment, CInt(islandSegment))
                BOHHTLocation.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                BOHHTLocation.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHHTLocation.IslandType, islandCode)

                If BOHHTLocation.LoadMatches.Count > 0 Then
                    BOHHTLocation.DateCreated.Value = Date.Today
                    BOHHTLocation.SkuNumber.Value = listSKU.Item(intCount)
                    BOHHTLocation.PlanogramId.Value = CInt(intPlanoGramID)
                    BOHHTLocation.Sequence.Value = Locationlist.Count + 1
                    BOHHTLocation.IslandId.Value = CInt(islandId)
                    BOHHTLocation.IslandSegment.Value = CInt(islandSegment)
                    BOHHTLocation.IslandType.Value = islandCode
                    BOHHTLocation.StockCount.Value = listQuantityCounted(intCount)
                    If BOHHTLocation.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                        Trace.WriteLine("There is an error with the processing sku " & listSKU.Item(intCount))
                        Return False
                    End If

                Else
                    BOHHTLocation.DateCreated.Value = Date.Today
                    BOHHTLocation.SkuNumber.Value = listSKU.Item(intCount)
                    BOHHTLocation.PlanogramId.Value = CInt(intPlanoGramID)
                    BOHHTLocation.Sequence.Value = 0
                    BOHHTLocation.IslandId.Value = CInt(islandId)
                    BOHHTLocation.IslandSegment.Value = CInt(islandSegment)
                    BOHHTLocation.IslandType.Value = islandCode

                    If BOHHTLocation.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                        Trace.WriteLine("There is an error with the processing sku " & listSKU.Item(intCount))
                        Return False
                    End If
                End If

                BOHHTDetails.StockSold.Value = listQuantitySold(intCount)
                BOHHTDetails.SaveIfExists()

                intCount += 1

            Loop 'intCount = listSKU.Count

            BOHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHeader.DateCreated, Today.Date)
            If BOHeader.LoadMatches.Count > 0 Then
                BOHeader.NumberCounted.Value = intCount
                If BOHeader.SaveIfNew = False Then
                    Return False
                End If
            End If

            Return True

        End Using

    End Function


#End Region

End Class
