﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class Errors
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("BOStockTake.Errors", GetType(Errors).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem building count.
        '''</summary>
        Friend Shared ReadOnly Property BuildCount() As String
            Get
                Return ResourceManager.GetString("BuildCount", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem loading build details.
        '''</summary>
        Friend Shared ReadOnly Property BuildLoad() As String
            Get
                Return ResourceManager.GetString("BuildLoad", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem building external count.
        '''</summary>
        Friend Shared ReadOnly Property ExtCount() As String
            Get
                Return ResourceManager.GetString("ExtCount", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem retrieving total stock count for stock item.
        '''</summary>
        Friend Shared ReadOnly Property hhtDetailGetTotalCount() As String
            Get
                Return ResourceManager.GetString("hhtDetailGetTotalCount", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem loading hht detail records.
        '''</summary>
        Friend Shared ReadOnly Property hhtDetailLoad() As String
            Get
                Return ResourceManager.GetString("hhtDetailLoad", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem retrieving number of hht details that have been counted.
        '''</summary>
        Friend Shared ReadOnly Property hhtHeaderGetItemsCounted() As String
            Get
                Return ResourceManager.GetString("hhtHeaderGetItemsCounted", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem loading hht header record.
        '''</summary>
        Friend Shared ReadOnly Property hhtHeaderLoad() As String
            Get
                Return ResourceManager.GetString("hhtHeaderLoad", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem saving hht header and associated records.
        '''</summary>
        Friend Shared ReadOnly Property hhtHeaderSave() As String
            Get
                Return ResourceManager.GetString("hhtHeaderSave", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem retrieving stock items from hht location records.
        '''</summary>
        Friend Shared ReadOnly Property hhtLocationGetSkus() As String
            Get
                Return ResourceManager.GetString("hhtLocationGetSkus", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem loading hht location records.
        '''</summary>
        Friend Shared ReadOnly Property hhtLocationLoad() As String
            Get
                Return ResourceManager.GetString("hhtLocationLoad", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem loading stock item for hht location.
        '''</summary>
        Friend Shared ReadOnly Property hhtLocationLoadStock() As String
            Get
                Return ResourceManager.GetString("hhtLocationLoadStock", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to No stock items found.
        '''</summary>
        Friend Shared ReadOnly Property WarnNoItems() As String
            Get
                Return ResourceManager.GetString("WarnNoItems", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Stock item already exists in details.
        '''</summary>
        Friend Shared ReadOnly Property WarnStockItemAlreadyExists() As String
            Get
                Return ResourceManager.GetString("WarnStockItemAlreadyExists", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Stock item not valid.
        '''</summary>
        Friend Shared ReadOnly Property WarnStockItemNotValid() As String
            Get
                Return ResourceManager.GetString("WarnStockItemNotValid", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
