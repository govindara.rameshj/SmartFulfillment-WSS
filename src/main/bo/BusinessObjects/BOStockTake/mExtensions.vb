﻿Module mExtensions

    <System.Runtime.CompilerServices.Extension()> Sub RemoveAllExcept(ByRef Stocks As List(Of BOStock.cStock), ByVal SkuNumbers As ArrayList)

        For index As Integer = Stocks.Count - 1 To 0 Step -1
            If Not SkuNumbers.Contains(Stocks(index).SkuNumber.Value) Then
                Stocks.RemoveAt(index)
            End If
        Next

    End Sub

End Module
