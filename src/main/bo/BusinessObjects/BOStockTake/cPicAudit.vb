﻿<Serializable()> Public Class cPicAudit
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PICAUD"
        BOFields.Add(_AuditDate)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Deleted)
        BOFields.Add(_OriginCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPicAudit)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPicAudit)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPicAudit))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPicAudit(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _AuditDate As New ColField(Of Date)("DATE1", Nothing, "Audit Date", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "SKU Number", True, False)
    Private _Deleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)
    Private _OriginCode As New ColField(Of String)("ORIG", "", "Origin Code", False, False)

#End Region

#Region "Field Properties"

    Public Property AuditDate() As ColField(Of Date)
        Get
            AuditDate = _AuditDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AuditDate = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Deleted = value
        End Set
    End Property
    Public Property OriginCode() As ColField(Of String)
        Get
            OriginCode = _OriginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _OriginCode = value
        End Set
    End Property

#End Region

End Class
