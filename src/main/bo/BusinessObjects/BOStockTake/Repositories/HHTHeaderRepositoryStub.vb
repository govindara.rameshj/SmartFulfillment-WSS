﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("BOStockTake.UnitTest")> 
Public Class HHTHeaderRepositoryStub
    Implements IHHTHeaderRepository

#Region "Stub Code"

    Private _Requirement_RF0861_Enabled As System.Nullable(Of Boolean)

    Friend Sub ConfigureStub(ByVal RequirementEnabled As System.Nullable(Of Boolean))

        _Requirement_RF0861_Enabled = RequirementEnabled

    End Sub

#End Region

#Region "Interface"

    Public Function Requirement_RF0861_Enabled() As Boolean? Implements IHHTHeaderRepository.Requirement_RF0861_Enabled

        Return _Requirement_RF0861_Enabled

    End Function

#End Region

End Class