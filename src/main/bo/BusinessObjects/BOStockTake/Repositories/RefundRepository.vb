﻿Imports System.Text
Imports OasysDBBO.Oasys3.DB

Public Class RefundRepository
    Implements IRefundRepository

    Private skuItems As StringBuilder = New StringBuilder()
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Public Function RefundItem(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As System.Data.DataTable Implements IRefundRepository.RefundItem
        Dim DT As DataTable

        skuItems.Append("Select dl.SKUN From DLLINE as dl ")
        skuItems.Append("inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append("dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
        skuItems.Append("inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
        skuItems.Append("dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN] ")
        skuItems.Append("where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append("and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
        skuItems.Append("and dl.DATE1 = ")
        skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
        skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
        skuItems.Append(MinPrice)
        DT = _Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)
        If DT.Rows.Count > 0 Then Return DT
        Return Nothing
    End Function

    Public Function RefundDistinct(ByVal SelectedDate As Date) As System.Data.DataTable Implements IRefundRepository.RefundDistinct

        Dim DT As DataTable

        skuItems.Append("Select  Distinct SKUN  from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
        skuItems.Append("'" & Format(SelectedDate.AddDays(-6), "yyyy-MM-dd") & "'")
        DT = _Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)
        If DT.Rows.Count > 0 Then Return DT
        Return Nothing
    End Function
    Public Function RefundRandom(ByVal SelectedDate As Date, ByVal MinPrice As Decimal, ByVal RandPercent As Integer) As System.Data.DataTable Implements IRefundRepository.RefundRandom

        Dim DT As DataTable

        skuItems.Append(" SELECT TOP " & RandPercent)
        skuItems.Append(" PERCENT SKUN  From DLLINE as dl")
        skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN]  ")
        skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and  ")
        skuItems.Append(" dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN]  ")
        skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' and dl.DATE1 = ")
        skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
        skuItems.Append(" and dl.SKUN not in (Select SKUN  From DLLINE as dl ")
        skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
        skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN] ")
        skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
        skuItems.Append(" and dl.DATE1 = ")
        skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
        skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
        skuItems.Append(MinPrice)
        skuItems.Append(" )group by dl.SKUN, dl.[TRAN]  ORDER BY NEWID() ")
        DT = _Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)
        If DT.Rows.Count > 0 Then Return DT
        Return Nothing
    End Function
End Class


