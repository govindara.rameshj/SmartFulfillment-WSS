﻿Imports Cts.Oasys.Data

Public Class RefundRepositoryNew
    Implements IRefundRepository

    Public Function RefundItem(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As System.Data.DataTable Implements IRefundRepository.RefundItem

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedure.RefundItem
                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)
                com.AddParameter("@MinPrice", MinPrice, SqlDbType.Decimal)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using
        End Using

        Return Nothing
    End Function

    Public Function RefundDistinct(ByVal SelectedDate As Date) As System.Data.DataTable Implements IRefundRepository.RefundDistinct

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedure.RefundDistinct
                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using
        End Using

        Return Nothing
    End Function
    Public Function RefundRandom(ByVal SelectedDate As Date, ByVal MinPrice As Decimal, ByVal RandPercent As Integer) As System.Data.DataTable Implements IRefundRepository.RefundRandom

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedure.RefundRandom
                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)
                com.AddParameter("@MinPrice", MinPrice, SqlDbType.Decimal)
                com.AddParameter("@RandPercent", RandPercent, SqlDbType.Int)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using
        End Using

        Return Nothing

    End Function

End Class
