﻿Public Class HHTHeaderRepository
    Implements IHHTHeaderRepository

    Public Function Requirement_RF0861_Enabled() As Boolean? Implements IHHTHeaderRepository.Requirement_RF0861_Enabled

        Return Parameter.GetBoolean(-861)

    End Function

End Class