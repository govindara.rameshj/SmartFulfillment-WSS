﻿<Serializable()> Public Class cAudit
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "AuditMaster"
        BOFields.Add(_AuditDate)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_NormalStock)
        BOFields.Add(_NormalCount)
        BOFields.Add(_MarkdownStock)
        BOFields.Add(_MarkdownCount)
        BOFields.Add(_SalesYearToDate)
        BOFields.Add(_SalesPreviousYear)
        BOFields.Add(_EntryMade)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cAudit)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cAudit)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cAudit))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cAudit(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _AuditDate As New ColField(Of Date)("AuditDate", Nothing, "Audit Date", True, False)
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "SKU Number", True, False)
    Private _NormalStock As New ColField(Of Integer)("NormalStock", 0, "Normal Stock", False, False)
    Private _NormalCount As New ColField(Of Integer)("NormalCount", 0, "Normal Count", False, False)
    Private _MarkdownStock As New ColField(Of Integer)("MarkdownStock", 0, "Mardown Stock", False, False)
    Private _MarkdownCount As New ColField(Of Integer)("MarkdownCount", 0, "Markdown Count", False, False)
    Private _SalesYearToDate As New ColField(Of Decimal)("SalesYearToDate", 0, "Sales Year to Date", False, False, 2)
    Private _SalesPreviousYear As New ColField(Of Decimal)("SalesPreviousYear", 0, "Sales Previous Year", False, False, 2)
    Private _EntryMade As New ColField(Of Boolean)("EntryMade", False, "Entry Made", False, False)

#End Region

#Region "Field Properties"

    Public Property AuditDate() As ColField(Of Date)
        Get
            AuditDate = _AuditDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AuditDate = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property NormalStock() As ColField(Of Integer)
        Get
            Return _NormalStock
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NormalStock = value
        End Set
    End Property
    Public Property NormalCount() As ColField(Of Integer)
        Get
            Return _NormalCount
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NormalCount = value
        End Set
    End Property
    Public Property MarkdownStock() As ColField(Of Integer)
        Get
            Return _MarkdownStock
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MarkdownStock = value
        End Set
    End Property
    Public Property MarkdownCount() As ColField(Of Integer)
        Get
            Return _MarkdownCount
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MarkdownCount = value
        End Set
    End Property
    Public Property SalesYearToDate() As ColField(Of Decimal)
        Get
            Return _SalesYearToDate
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesYearToDate = value
        End Set
    End Property
    Public Property SalesPreviousYear() As ColField(Of Decimal)
        Get
            Return _SalesPreviousYear
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesPreviousYear = value
        End Set
    End Property
    Public Property EntryMade() As ColField(Of Boolean)
        Get
            EntryMade = _EntryMade
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _EntryMade = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Stock As BOStock.cStock = Nothing
    Private _Details As List(Of cAudit)

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property
    Public Property Details() As List(Of cAudit)
        Get
            If _Details Is Nothing Then
                Dim audit As New cAudit(Oasys3DB)
                audit.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, audit.AuditDate, _AuditDate.Value)
                _Details = audit.LoadMatches
            End If
            Return _Details
        End Get
        Set(ByVal value As List(Of cAudit))
            _Details = value
        End Set
    End Property
    Public Property Detail(ByVal Stock As BOStock.cStock) As cAudit
        Get
            For Each det As cAudit In Details
                If det.SkuNumber.Value = Stock.SkuNumber.Value Then Return det
            Next

            Dim newDet As New cAudit(Oasys3DB)
            newDet.Stock = Stock
            newDet.AuditDate.Value = _AuditDate.Value
            newDet.SkuNumber.Value = Stock.SkuNumber.Value
            newDet.NormalStock.Value = Stock.StockOnHand.Value
            newDet.MarkdownStock.Value = Stock.MarkDownQuantity.Value
            newDet.SalesYearToDate.Value = Stock.UnitsSoldThisYear.Value
            newDet.SalesPreviousYear.Value = Stock.UnitsSoldLastYear.Value
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cAudit)
            For Each det As cAudit In Details
                If det.SkuNumber.Value = Stock.SkuNumber.Value Then det = value
            Next
        End Set
    End Property
    Public Property Detail(ByVal SkuNumber As String) As cAudit
        Get
            For Each det As cAudit In Details
                If det.SkuNumber.Value = SkuNumber Then Return det
            Next

            Dim newDet As New cAudit(Oasys3DB)
            newDet.AuditDate.Value = _AuditDate.Value
            newDet.SkuNumber.Value = SkuNumber
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cAudit)
            For Each det As cAudit In Details
                If det.SkuNumber.Value = SkuNumber Then det = value
            Next
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetHhtItems(ByVal number As Integer, ByVal percent As Decimal) As Integer

        Try
            'load hht header checking that there are any locations for it
            Dim hhtHeader As New BOStockTake.cHhtHeader(Oasys3DB)
            hhtHeader.Load(_AuditDate.Value)
            If hhtHeader.Details.Count = 0 Then Return 0

            'get number of items to retrieve
            If number = 0 Then number = CInt((percent / 100) * hhtHeader.Details.Count)
            Dim rowSkip As Integer = CInt(Math.Ceiling(hhtHeader.Details.Count / number))

            'get sample percentage from parameters
            Dim rand As New Random
            Dim rowIndexes As New ArrayList
            Dim lower As Integer = 0
            Dim upper As Integer = rowSkip
            Dim skuNumbers As New ArrayList

            Do While upper <= hhtHeader.Details.Count
                Dim location As cHhtDetail = hhtHeader.Details(rand.Next(lower, upper))
                If Not skuNumbers.Contains(location.SkuNumber.Value) Then skuNumbers.Add(location.SkuNumber.Value)
                lower += rowSkip
                upper += rowSkip
            Loop

            'get stock items in the array
            If skuNumbers.Count = 0 Then Return 0
            Dim Stocks As New BOStock.cStock(Oasys3DB)
            Stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, Stocks.SkuNumber, skuNumbers)
            Stocks.Stocks = Stocks.LoadMatches

            'validate each stock record
            Dim StocksToAdd As New List(Of BOStock.cStock)
            For Each stock As BOStock.cStock In Stocks.Stocks
                If stock.PicValidate Then StocksToAdd.Add(stock)
            Next

            'check if any items left and get related items if exist
            If StocksToAdd.Count = 0 Then Return 0
            GetRelatedItems(StocksToAdd)

            'insert stock records into hhtHeader details
            For Each stock As BOStock.cStock In StocksToAdd
                Detail(stock).EntryMade.Value = False
            Next

            Return StocksToAdd.Count

        Catch ex As Exception
            Throw ex
            Return -1
        End Try

    End Function

    Public Function GetPicAuditItems() As Boolean

        Try
            Dim picAudit As New cPicAudit(Oasys3DB)
            picAudit.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, picAudit.AuditDate, _AuditDate.Value)
            picAudit.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            picAudit.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, picAudit.Deleted, False)
            Dim picAudits As List(Of BOStockTake.cPicAudit) = picAudit.LoadMatches
            If picAudits.Count = 0 Then Return False

            Dim skuNumbers As New ArrayList
            For Each pic As BOStockTake.cPicAudit In picAudits
                If Not skuNumbers.Contains(pic.SkuNumber.Value) Then skuNumbers.Add(pic.SkuNumber.Value)
            Next

            'get stock items in the array
            If skuNumbers.Count = 0 Then Return False
            Dim Stocks As New BOStock.cStock(Oasys3DB)
            Stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, Stocks.SkuNumber, skuNumbers)
            Stocks.Stocks = Stocks.LoadMatches

            'validate each stock record
            Dim StocksToAdd As New List(Of BOStock.cStock)
            For Each stock As BOStock.cStock In Stocks.Stocks
                If stock.PicValidate Then StocksToAdd.Add(stock)
            Next

            'check if any items left and get related items if exist
            If StocksToAdd.Count = 0 Then Return False
            GetRelatedItems(StocksToAdd)

            'insert stock records into hhtHeader details
            For Each stock As BOStock.cStock In StocksToAdd
                Detail(stock).EntryMade.Value = False
            Next

            Return StocksToAdd.Any()

        Catch ex As Exception
            Throw ex
            Return False
        End Try


    End Function

    Private Sub GetRelatedItems(ByRef stocksToAdd As List(Of BOStock.cStock), Optional ByRef skuNumbers As ArrayList = Nothing)

        Try
            If skuNumbers Is Nothing Then
                skuNumbers = New ArrayList
                For Each stock As BOStock.cStock In stocksToAdd
                    skuNumbers.Add(stock.SkuNumber.Value)
                Next
            End If

            Dim relatedSkus As New ArrayList
            Dim relatedItems As New BOStock.cRelatedItems(Oasys3DB)
            relatedItems.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, relatedItems.BulkOrPackageItem, skuNumbers)
            relatedItems.Items = relatedItems.LoadMatches
            For Each item As BOStock.cRelatedItems In relatedItems.Items
                If Not skuNumbers.Contains(item.SingleItem.Value) Then
                    If Not relatedSkus.Contains(item.SingleItem.Value) Then relatedSkus.Add(item.SingleItem.Value)
                End If
            Next

            'now get related bulk items
            relatedItems.ClearLoadFilter()
            relatedItems.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, relatedItems.SingleItem, skuNumbers)
            relatedItems.Items = relatedItems.LoadMatches
            For Each item As BOStock.cRelatedItems In relatedItems.Items
                If Not skuNumbers.Contains(item.BulkOrPackageItem.Value) Then
                    If Not relatedSkus.Contains(item.BulkOrPackageItem.Value) Then relatedSkus.Add(item.BulkOrPackageItem.Value)
                End If
            Next

            'get stocks for related item skus
            If relatedSkus.Count > 0 Then
                Dim stocks As New BOStock.cStock(Oasys3DB)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, relatedSkus)
                stocks.Stocks = stocks.LoadMatches

                'do validation on item and add to stocks collection
                For Each stock As BOStock.cStock In stocks.Stocks
                    If stock.PicValidate Then stocksToAdd.Add(stock)
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function Update() As Boolean

        Try
            'delete existing records first
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(_AuditDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _AuditDate.Value)
            Oasys3DB.Delete()

            'insert all details
            Oasys3DB.BeginTransaction()

            For Each det As cAudit In Details
                det.EntryMade.Value = True
                det.SaveIfNew()
            Next

            Oasys3DB.CommitTransaction()
            Return True

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw ex
            Return False
        End Try

    End Function

#End Region

End Class
