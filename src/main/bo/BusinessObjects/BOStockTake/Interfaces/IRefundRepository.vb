﻿''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author      : Dhanesh Ramachandran
' Date        : 01/08/2011
' Referral No : 0861
' Notes       : Added to make PIC Refund switchable 
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Interface IRefundRepository

    Function RefundItem(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As System.Data.DataTable
    Function RefundDistinct(ByVal SelectedDate As Date) As System.Data.DataTable
    Function RefundRandom(ByVal SelectedDate As Date, ByVal MinPrice As Decimal, ByVal RandPercent As Integer) As System.Data.DataTable


End Interface



