﻿''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author      : Dhanesh Ramachandran
' Date        : 01/08/2011
' Referral No : 0861
' Notes       : Added to make PIC Refund switchable 
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Interface IRefund
    Function GetRefundedLinesOverThresholdValue(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As DataTable
    Function GetRefundedLinesNotPreviouslyCounted(ByVal SelectedDate As Date) As DataTable
    Function GetRefundedLinesPseudoRandomSample(ByVal SelectedDate As Date, ByVal MinPrice As Decimal, ByVal RandPercent As Integer) As DataTable
End Interface

