﻿Public Interface IHHTHeader

    Function GetRefundSkus(ByVal minPrice As Decimal, ByVal randPercent As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As ArrayList
    Function GetRefundedLinesOverThresholdValue(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As ArrayList
End Interface
