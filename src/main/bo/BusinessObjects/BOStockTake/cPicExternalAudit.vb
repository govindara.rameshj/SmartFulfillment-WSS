﻿<Serializable()> Public Class cPicExternalAudit
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PicExternalAudit"
        BOFields.Add(_CountDate)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Price)
        BOFields.Add(_StockOnHand)
        BOFields.Add(_StockMarkdown)
        BOFields.Add(_RTI)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPicExternalAudit)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPicExternalAudit)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPicExternalAudit))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPicExternalAudit(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CountDate As New ColField(Of Date)("CountDate", Nothing, "Count Date", True, False)
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "Sku Number", True, False)
    Private _Price As New ColField(Of Decimal)("Price", 0, "Price", False, False)
    Private _StockOnHand As New ColField(Of Integer)("StockOnHand", 0, "Stock On Hand", False, False)
    Private _StockMarkdown As New ColField(Of Integer)("StockMarkdown", 0, "StockMarkdown", False, False)
    Private _RTI As New ColField(Of String)("RTI", "S", "RTI", False, False)

#End Region

#Region "Field Properties"

    Public Property CountDate() As ColField(Of Date)
        Get
            CountDate = _CountDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _CountDate = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Price() As ColField(Of Decimal)
        Get
            Return _Price
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Price = value
        End Set
    End Property
    Public Property StockOnHand() As ColField(Of Integer)
        Get
            Return _StockOnHand
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnHand = value
        End Set
    End Property
    Public Property StockMarkdown() As ColField(Of Integer)
        Get
            Return _StockMarkdown
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockMarkdown = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            RTI = _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub DeleteAllCountDate(ByVal DeleteDate As Date)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(_CountDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, DeleteDate)
        Oasys3DB.Delete()

    End Sub

#End Region

End Class
