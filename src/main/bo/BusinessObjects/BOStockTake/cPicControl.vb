﻿<Serializable()> Public Class cPicControl
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PICCTL"
        BOFields.Add(_CycleDayNumber)
        BOFields.Add(_HieCategory)
        BOFields.Add(_HieGroup)
        BOFields.Add(_HieSubgroup)
        BOFields.Add(_HieStyle)
        BOFields.Add(_Deleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPicControl)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPicControl)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPicControl))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPicControl(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CycleDayNumber As New ColField(Of Integer)("DAYN", 0, "Cycle Day Number", True, False)
    Private _HieCategory As New ColField(Of String)("CTGY", "000000", "Hierarchy Category", True, False)
    Private _HieGroup As New ColField(Of String)("GRUP", "000000", "Hierarchy Group", True, False)
    Private _HieSubgroup As New ColField(Of String)("SGRP", "000000", "Hierarchy Subgroup", True, False)
    Private _HieStyle As New ColField(Of String)("STYL", "000000", "Hierarchy Style", True, False)
    Private _Deleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property CycleDayNumber() As ColField(Of Integer)
        Get
            Return _CycleDayNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _CycleDayNumber = value
        End Set
    End Property
    Public Property HieCategory() As ColField(Of String)
        Get
            HieCategory = _HieCategory
        End Get
        Set(ByVal value As ColField(Of String))
            _HieCategory = value
        End Set
    End Property
    Public Property HieGroup() As ColField(Of String)
        Get
            HieGroup = _HieGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HieGroup = value
        End Set
    End Property
    Public Property HieSubgroup() As ColField(Of String)
        Get
            Return _HieSubgroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HieSubgroup = value
        End Set
    End Property
    Public Property HieStyle() As ColField(Of String)
        Get
            Return _HieStyle
        End Get
        Set(ByVal value As ColField(Of String))
            _HieStyle = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Return _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Deleted = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _groups As List(Of cPicControl) = Nothing

    Public Property Groups() As List(Of cPicControl)
        Get
            If _groups Is Nothing Then
                Dim det As New cPicControl(Oasys3DB)
                det.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, det.CycleDayNumber, _CycleDayNumber.Value)
                _groups = det.LoadMatches
            End If
            Return _groups
        End Get
        Set(ByVal value As List(Of cPicControl))
            _groups = value
        End Set
    End Property

    Public Function GetRecordsByDayNumb(ByVal DayNumber As Integer) As List(Of cPicControl)

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_CycleDayNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, DayNumber)
            Return LoadMatches()

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try

    End Function

    Public Function RemoveAllFlaggedDelete() As Boolean
        Oasys3DB.ExecuteSql("DELETE FROM PICCTL WHERE IDEL = '1'")
    End Function

    Public Function FlagAllAsDeleted() As Boolean
        Oasys3DB.ExecuteSql("UPDATE PICCTL SET IDEL = '1' where IDEL = '0' ")
    End Function
    Public Function ISISGetPerpetualInventoryHierarchy() As String

        Dim strQuantity As String = String.Empty
        Dim strResponseMessage As String = String.Empty
        Dim intCount As Integer = 0
        Dim intCurrentCycleDay As Integer = 0

        Dim BOHierarchy As New BOStockTake.cPicControl(Oasys3DB)
        Dim BOSystem As New BOSystem.cSystemDates(Oasys3DB)


        Const METHOD_NAME As String = "ISISGetPerpetualInventoryHierarchy"

        Try

            'gets and calculate the current day within the PIC cylce
            BOSystem.AddLoadField(BOSystem.TodayDayCode)
            BOSystem.AddLoadField(BOSystem.WeekCycleNumber)
            If BOSystem.LoadMatches.Count > 0 Then
                intCurrentCycleDay = CInt(BOSystem.WeekCycleNumber.Value * 7 + BOSystem.TodayDayCode.Value)
            End If

            'loads up the groups for the current cycle day 
            BOHierarchy.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOHierarchy.CycleDayNumber, intCurrentCycleDay)

            ''gets the number of records loaded
            BOHierarchy.LoadMatches()

            ''adds the number to the response message
            strResponseMessage += BOHierarchy.Groups.Count.ToString

            'if the number is below 10 adds a extra character
            If BOHierarchy.Groups.Count < 10 Then
                strResponseMessage += " "
            End If

            'if not records loaded sends a error response back and exit the funcation
            If BOHierarchy.Groups.Count = 0 Then
                strResponseMessage += "?"
                Return strResponseMessage
            End If

            'loads each record into the response message
            For Each Group As BOStockTake.cPicControl In BOHierarchy.Groups
                strResponseMessage += Group.HieCategory.Value
                strResponseMessage += Group.HieGroup.Value
            Next
            Return strResponseMessage
        Catch ex As Exception
            Trace.WriteLine(METHOD_NAME & " An error has occured with PerpetualInventoryHierarchy error: " & ex.Message)
            Return strResponseMessage
        End Try
    End Function 'PerpetualInventoryHierarchy
#End Region

End Class
