﻿Public Class HHTHeader
    Inherits cHhtHeader
    Implements IHHTHeader

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
    End Sub

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
    End Sub

#Region "Parent Procedures And Functions Overrided"

    Public Overrides Sub BuildCount(ByVal cycleDate As Date)

        Try

            MyBase.OnUpdateProgessEvent(0, My.Resources.BuildGetCycle)
            MyBase.LoadCycleItems(cycleDate)

            MyBase.OnUpdateProgessEvent(20, My.Resources.BuildGetRefunds)
            LoadRefundedItems()                                                 'call local version, not parent

            MyBase.OnUpdateProgessEvent(40, My.Resources.BuildGetParkedSales)
            MyBase.LoadParkedSaleItems()

            MyBase.OnUpdateProgessEvent(60, My.Resources.BuildGetStockLoss)

            MyBase.OnUpdateProgessEvent(80, My.Resources.BuildGetNegative)
            MyBase.LoadNegativeItems()

            MyBase.OnUpdateProgessEvent(100, String.Empty)

        Finally
            MyBase.OnClearProgressEvent()
        End Try

    End Sub

    Protected Overrides Function LoadRefundedItems() As Integer

        'check if today is a day that can run refund checks
        Dim minPrice As Decimal
        Dim saleSku As DataTable
        Dim skuItems As StringBuilder = New StringBuilder()
        Dim skuNumbersl As New ArrayList
        Dim skuNumbers As New ArrayList
        Dim randPercent As Integer

        Using param As New BOSystem.cParameter(Oasys3DB)
            If Not param.GetParameterBoolean(4430 + MyBase.DateCreated.Value.DayOfWeek) Then Return 0
            minPrice = param.GetParameterDecimal(4420)
            randPercent = param.GetParameterInteger(4421)
        End Using

        skuItems.Append("Select dl.SKUN From DLLINE as dl ")
        skuItems.Append("inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append("dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
        skuItems.Append("inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
        skuItems.Append("dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN] ")
        skuItems.Append("where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append("and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
        skuItems.Append("and dl.DATE1 = ")
        skuItems.Append("'" & Format(MyBase.DateCreated.Value.AddDays(-1), "yyyy-MM-dd") & "'")
        skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
        skuItems.Append(minPrice)
        saleSku = Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)

        'get skus from lines
        For indexelm As Integer = 0 To saleSku.Rows.Count - 1
            skuNumbers.Add(saleSku.Rows(indexelm)(0))
        Next

        ''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author       : Partha Dutta
        ' Create date  : 18/08/2011
        ' Referral No  : RF0861
        ' Description  : Existing code going back 7 days for refunded SKUs
        '                Amend code to go back 1 day only
        '                Refractor code and replace existing dynamic sequel code with a stored procedure replacement
        ''''''''''''''''''''''''''''''''''''''''''''''''

        'existing code
        'skuItems.Remove(0, skuItems.Length).Replace("'", "")
        'saleSku.Clear()
        'skuItems.Append("Select  Distinct SKUN  from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
        'skuItems.Append("'" & Format(MyBase.DateCreated.Value.AddDays(-7), "yyyy-MM-dd") & "'")
        'saleSku = Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)

        'new code
        Dim DetailCollection As IHandHeldTerminalDetailCollection

        DetailCollection = HandHeldTerminalDetailCollectionFactory.FactoryGet
        DetailCollection.ReadRefundedItems(MyBase.DateCreated.Value.AddDays(-1))

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        For indexelm As Integer = 0 To saleSku.Rows.Count - 1
            If Not skuNumbersl.Contains(saleSku.Rows(indexelm)(0)) Then skuNumbersl.Add(saleSku.Rows(indexelm)(0))
        Next

        ' collect the random refunded sku's
        ' randPercent
        skuItems.Remove(0, skuItems.Length).Replace("'", "")
        saleSku.Clear()
        skuItems.Append(" SELECT TOP " & randPercent)
        skuItems.Append(" PERCENT SKUN  From DLLINE as dl")
        skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN]  ")
        skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and  ")
        skuItems.Append(" dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN]  ")
        skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' and dl.DATE1 = ")
        skuItems.Append("'" & Format(MyBase.DateCreated.Value.AddDays(-1), "yyyy-MM-dd") & "'")
        skuItems.Append(" and dl.SKUN not in (Select SKUN  From DLLINE as dl ")
        skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
        skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
        skuItems.Append(" dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN] ")
        skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
        skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
        skuItems.Append(" and dl.DATE1 = ")
        skuItems.Append("'" & Format(MyBase.DateCreated.Value.AddDays(-1), "yyyy-MM-dd") & "'")
        skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
        skuItems.Append(minPrice)
        skuItems.Append(" )group by dl.SKUN, dl.[TRAN]  ORDER BY NEWID() ")
        saleSku = Oasys3DB.ExecuteSql(skuItems.ToString()).Tables(0)

        For indexelm As Integer = 0 To saleSku.Rows.Count - 1
            If Not skuNumbersl.Contains(saleSku.Rows(indexelm)(0)) Then skuNumbersl.Add(saleSku.Rows(indexelm)(0))
        Next
        Dim stocksl As New BOStock.cStock(Oasys3DB)
        Dim stocksToAdd As New List(Of BOStock.cStock)

        If skuNumbersl.Count > 0 Then
            stocksl.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocksl.SkuNumber, skuNumbersl)
            stocksl.Stocks = stocksl.LoadMatches
            If stocksl.Stocks.Count > 0 Then
                For Each stock As BOStock.cStock In stocksl.Stocks
                    stocksToAdd.Add(stock)
                Next
            End If
        End If

        'get stock items in the array

        ' If skuNumbers.Count = 0 Then Return 0
        If skuNumbers.Count > 0 Then
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
            stocks.Stocks = stocks.LoadMatches
            If stocks.Stocks.Count = 0 Then Return 0

            'validate each stock record and add to collection
            For Each stock As BOStock.cStock In stocks.Stocks
                If stock.PicValidate Then
                    stocksToAdd.Add(stock)
                End If
            Next
        End If

        'get related items if any stocks to add
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "R"
        Next

        'reset header item count
        MyBase.NumberItems.Value = MyBase.Details.Count
        Return stocksToAdd.Count

    End Function

#End Region

End Class