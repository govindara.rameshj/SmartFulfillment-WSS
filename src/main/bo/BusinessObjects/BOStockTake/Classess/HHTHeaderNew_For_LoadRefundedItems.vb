﻿Public Class HHTHeaderNew_For_LoadRefundedItems
    Implements IHHTHeader

    Public Function GetRefundSkus(ByVal minPrice As Decimal, ByVal randPercent As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As System.Collections.ArrayList Implements IHHTHeader.GetRefundSkus
        Dim GetRefundedSkuList As New RefundCount

        GetRefundedSkuList.Load(StartDate, EndDate)

        Return GetRefundedSkuList.Skus
    End Function

    Public Function GetRefundedLinesOverThresholdValue(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As ArrayList Implements IHHTHeader.GetRefundedLinesOverThresholdValue

        Return New ArrayList
    End Function
End Class
