﻿Public Class HHTHeaderExisting_For_LoadRefundedItems
    Implements IHHTHeader

    Public Function GetRefundSkus(ByVal minPrice As Decimal, ByVal randPercent As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As System.Collections.ArrayList Implements IHHTHeader.GetRefundSkus
        Dim RefundSkuData As DataTable
        Dim RefundSkus As New ArrayList
        Dim GetRefundedLinesPseudoRandomSampleImplementation As IRefund
        Dim RefundImplementationFactory As New RefundFactory

        If RefundImplementationFactory IsNot Nothing Then
            GetRefundedLinesPseudoRandomSampleImplementation = RefundImplementationFactory.GetImplementation()
            If GetRefundedLinesPseudoRandomSampleImplementation IsNot Nothing Then
                RefundSkuData = GetRefundedLinesPseudoRandomSampleImplementation.GetRefundedLinesPseudoRandomSample(StartDate, minPrice, randPercent)
                If Not RefundSkuData Is Nothing Then
                    For NextSku As Integer = 0 To RefundSkuData.Rows.Count - 1
                        If Not RefundSkus.Contains(RefundSkuData.Rows(NextSku)(0)) Then
                            RefundSkus.Add(RefundSkuData.Rows(NextSku)(0))
                        End If
                    Next
                End If
            End If
        End If

        Return RefundSkus
    End Function

    Public Function GetRefundedLinesOverThresholdValue(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As ArrayList Implements IHHTHeader.GetRefundedLinesOverThresholdValue
        Dim RefundSkuData As DataTable
        Dim RefundSkus As New ArrayList
        Dim GetRefundedLinesOverThresholdValueImplementation As IRefund
        Dim RefundImplementationFactory As New RefundFactory

        If RefundImplementationFactory IsNot Nothing Then
            GetRefundedLinesOverThresholdValueImplementation = RefundImplementationFactory.GetImplementation()
            If GetRefundedLinesOverThresholdValueImplementation IsNot Nothing Then
                RefundSkuData = GetRefundedLinesOverThresholdValueImplementation.GetRefundedLinesOverThresholdValue(SelectedDate, MinPrice)
                If Not RefundSkuData Is Nothing Then
                    For NextSku As Integer = 0 To RefundSkuData.Rows.Count - 1
                        RefundSkus.Add(RefundSkuData.Rows(NextSku)(0))
                    Next
                    RefundSkuData.Clear()
                End If
            End If
        End If

        Return RefundSkus
    End Function
End Class
