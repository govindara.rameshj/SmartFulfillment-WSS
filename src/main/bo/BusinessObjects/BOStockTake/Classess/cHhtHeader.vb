﻿<Serializable()> Public Class cHhtHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub

    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HHTHDR"
        BOFields.Add(_dateCreated)
        BOFields.Add(_numItems)
        BOFields.Add(_numCounted)
        BOFields.Add(_isAdjusted)
        BOFields.Add(_employeeID)
        BOFields.Add(_isLocked)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHhtHeader)


        LoadBORecords(count)

        Dim col As New List(Of cHhtHeader)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cHhtHeader))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)


        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHhtHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _dateCreated As New ColField(Of Date)("DATE1", Nothing, "Date Created", True, False)
    Private _numItems As New ColField(Of Integer)("NUMB", 0, "Number of Items", False, False)
    Private _numCounted As New ColField(Of Integer)("DONE", 0, "Number Counted", False, False)
    Private _isAdjusted As New ColField(Of Boolean)("IADJ", False, "Adjusted", False, False)
    Private _employeeID As New ColField(Of String)("AUTH", "000", "Employee ID", False, False)
    Private _isLocked As New ColField(Of Boolean)("IsLocked", False, "Is Locked", False, False)

#End Region

#Region "Field Properties"

    Public Property DateCreated() As ColField(Of Date)
        Get
            Return _dateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _dateCreated = value
        End Set
    End Property
    Public Property NumberItems() As ColField(Of Integer)

        Get
            Return _numItems
        End Get
        Set(ByVal value As ColField(Of Integer))
            _numItems = value
        End Set
    End Property
    Public Property NumberCounted() As ColField(Of Integer)

        Get
            Return _numCounted
        End Get
        Set(ByVal value As ColField(Of Integer))
            _numCounted = value
        End Set
    End Property
    Public Property Adjusted() As ColField(Of Boolean)

        Get
            Return _isAdjusted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _isAdjusted = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)

        Get
            Return _employeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _employeeID = value
        End Set
    End Property
    Public Property IsLocked() As ColField(Of Boolean)

        Get
            Return _isLocked
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _isLocked = value
        End Set
    End Property

#End Region

#Region "Events"
    Public Event ClearProgress()
    Public Event UpdateProgess(ByVal percent As Integer, ByVal message As String)
#End Region

#Region "Entities"
    Private _Details As List(Of cHhtDetail) = Nothing
    
    Public Property Details() As List(Of cHhtDetail)
        Get
            If _Details Is Nothing Then LoadDetails()
            Return _Details
        End Get
        Set(ByVal value As List(Of cHhtDetail))
            _Details = value
        End Set
    End Property
    Public Property Detail(ByVal Stock As BOStock.cStock) As cHhtDetail
        Get
            For Each det As cHhtDetail In Details
                If det.SkuNumber.Value = Stock.SkuNumber.Value Then Return det
            Next

            Dim newDet As New cHhtDetail(Oasys3DB)
            newDet.Stock = Stock
            newDet.DateCreated.Value = _dateCreated.Value
            newDet.SkuNumber.Value = Stock.SkuNumber.Value
            newDet.NonStockItem.Value = Stock.NonStockItem.Value
            newDet.StockOnHand.Value = Stock.StockOnHand.Value
            newDet.MarkdownQty.Value = Stock.MarkDownQuantity.Value
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cHhtDetail)
            For Each det As cHhtDetail In Details
                If det.SkuNumber.Value = Stock.SkuNumber.Value Then det = value
            Next
        End Set
    End Property
    Public Property Detail(ByVal SkuNumber As String) As cHhtDetail
        Get
            For Each det As cHhtDetail In Details
                If det.SkuNumber.Value = SkuNumber Then Return det
            Next

            Dim newDet As New cHhtDetail(Oasys3DB)
            newDet.SkuNumber.Value = SkuNumber
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cHhtDetail)
            For Each det As cHhtDetail In Details
                If det.SkuNumber.Value = SkuNumber Then det = value
            Next
        End Set
    End Property



#End Region

#Region "Methods"

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 18/05/2011
    ' Referral No : 560
    ' Notes       : Added to update ONHA and MDNQ during Nightly Run.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Updates ONHA and MDNQ fields in HHTDET from STKMAS 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReloadValues()

        Dim det As New cHhtDetail(Oasys3DB)
        Dim stock As New BOStock.cStock(Oasys3DB)
        Dim skuNumbers As New ArrayList
        _Details = det.GetDetails(_dateCreated.Value)
        'get stock items for these details 
        For Each detail As cHhtDetail In _Details
            skuNumbers.Add(detail.SkuNumber.Value)
        Next
        stock.LoadStockItems(skuNumbers)
        'assign ONHA and MDNQ in HHTDET
        For Each stocks As BOStock.cStock In stock.Stocks
            For Each detail As cHhtDetail In _Details
                If detail.SkuNumber.Value = stocks.SkuNumber.Value Then
                    detail.StockOnHand.Value = stocks.StockOnHand.Value
                    detail.MarkdownQty.Value = stocks.MarkDownQuantity.Value
                    Exit For
                End If
            Next
        Next
        Update()
    End Sub

    ''' <summary>
    ''' Loads hht header into this instance for given date or creates if one does not exist.
    ''' </summary>
    ''' <param name="dateCreated"></param>
    ''' <remarks></remarks>
    Public Sub Load(ByVal dateCreated As Date)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_dateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, dateCreated)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        If dt.Rows.Count > 0 Then
            Me.LoadFromRow(dt.Rows(0))
        Else
            _dateCreated.Value = dateCreated
            _numCounted.Value = 0
            _numCounted.Value = 0
            _isAdjusted.Value = False
            _employeeID.Value = "000"
        End If

    End Sub


    ''' <summary>
    ''' Loads all hht details into Details collection for this instance.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadDetails(Optional ByVal WithStocks As Boolean = False)

        Dim det As New cHhtDetail(Oasys3DB)
        _Details = det.GetDetails(_dateCreated.Value)

        'get stock items for these details if with stocks is selected
        If WithStocks AndAlso _Details.Count > 0 Then
            Dim skuNumbers As New ArrayList
            For Each d As cHhtDetail In _Details
                skuNumbers.Add(d.SkuNumber.Value)
            Next

            Dim stock As New BOStock.cStock(Oasys3DB)
            stock.LoadStockItems(skuNumbers)

            'assign stock to location
            For Each s As BOStock.cStock In stock.Stocks
                For Each d As cHhtDetail In _Details
                    If d.SkuNumber.Value = s.SkuNumber.Value Then
                        d.Stock = s
                        Exit For
                    End If
                Next
            Next
        End If

    End Sub

    ''' <summary>
    ''' Sets adjusted, number of items and counted items in hht header.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SetHeaderFields()

        Dim total As Integer = 0
        Dim allAdjusted As Boolean = True
        For Each det As cHhtDetail In Details
            If det.Locations.Count > 0 Then total += 1
            If Not det.Adjusted.Value Then allAdjusted = False
        Next

        _isAdjusted.Value = allAdjusted
        _numCounted.Value = total
        _numItems.Value = _Details.Count

    End Sub


    ''' <summary>
    ''' Builds external audit count from previously counted items.
    ''' </summary>
    ''' <param name="CountDate"></param>
    ''' <remarks></remarks>
    Public Sub BuildExternalAuditCount(ByVal countDate As Date)

        Try
            RaiseEvent UpdateProgess(0, My.Resources.ExtGetParameters)

            'get parameters to use
            Dim numWeeks As Integer
            Dim numItems As Integer
            Using param As New BOSystem.cParameter(Oasys3DB)
                numWeeks = param.GetParameterInteger(4490)
                numItems = param.GetParameterInteger(4491)
            End Using

            'get hht details for past numWeeks that have had an entry made
            RaiseEvent UpdateProgess(10, My.Resources.ExtGetHhtDetails)
            Dim hhtlocation As New cHhtLocation(Oasys3DB)
            Dim skuNumbers As ArrayList = hhtlocation.GetSkuNumbersWithin(countDate.AddDays(numWeeks * -7), countDate)

            'check if any returned
            If skuNumbers.Count = 0 Then
                Exit Sub
            End If

            'get stock items for these details excluding single related items and with no stock on hand
            RaiseEvent UpdateProgess(40, My.Resources.ExtGetStockItems)
            Dim stocks As New BOStock.cStock(Oasys3DB)
            If skuNumbers.Count < 6500 Then
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
                stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.RelatedItemSingle, False)
                stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, stocks.StockOnHand, 0)
                stocks.Stocks = stocks.LoadMatches
            Else
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.RelatedItemSingle, False)
                stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, stocks.StockOnHand, 0)
                stocks.Stocks = stocks.LoadMatches
                stocks.Stocks.RemoveAllExcept(skuNumbers)
            End If

            'get numItems random sample from stocks
            RaiseEvent UpdateProgess(70, My.Resources.ExtGetSample)
            Dim rowSkip As Integer = CInt(Math.Ceiling(stocks.Stocks.Count / numItems))
            Dim rand As New Random
            Dim rowIndexes As New ArrayList
            Dim lower As Integer = 0
            Dim upper As Integer = rowSkip

            Dim stocksToAdd As New List(Of BOStock.cStock)
            Do While upper <= stocks.Stocks.Count
                Dim stock As BOStock.cStock = stocks.Stocks(rand.Next(lower, upper))
                stocksToAdd.Add(stock)
                lower += rowSkip
                upper += rowSkip
            Loop

            RaiseEvent UpdateProgess(70, My.Resources.ExtGetRelatedItems)
            If stocksToAdd.Count = 0 Then Exit Sub
            Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
            relItem.GetRelatedItems(stocksToAdd)

            'start transaction
            Oasys3DB.BeginTransaction()

            'clear any skus from PicExternalAudit for this date
            RaiseEvent UpdateProgess(70, My.Resources.ExtSaving)
            Dim ext As New cPicExternalAudit(Oasys3DB)
            ext.DeleteAllCountDate(countDate)

            'add these stock items into PicExternalAudit
            For Each stock As BOStock.cStock In stocksToAdd.Distinct
                ext = New cPicExternalAudit(Oasys3DB)
                ext.CountDate.Value = countDate
                ext.SkuNumber.Value = stock.SkuNumber.Value
                ext.Price.Value = stock.NormalSellPrice.Value
                ext.StockOnHand.Value = stock.StockOnHand.Value
                ext.StockMarkdown.Value = stock.MarkDownQuantity.Value
                ext.SaveIfNew()
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        Finally
            RaiseEvent ClearProgress()
        End Try

    End Sub

    Public Sub BuildCount(ByVal cycleDate As Date)

        Try
            Dim count As Integer = 0

            RaiseEvent UpdateProgess(0, My.Resources.BuildGetCycle)
            count += LoadCycleItems(cycleDate)

            RaiseEvent UpdateProgess(25, My.Resources.BuildGetRefunds)
            count += LoadRefundedItems()

            RaiseEvent UpdateProgess(50, My.Resources.BuildGetParkedSales)
            count += LoadParkedSaleItems()

            RaiseEvent UpdateProgess(75, My.Resources.BuildGetNegative)
            count += LoadNegativeItems()

            RaiseEvent UpdateProgess(100, "")

        Finally
            RaiseEvent ClearProgress()
        End Try

    End Sub

    Public Sub ApplyCounts(ByVal UserID As Integer)

        'get the two adjustment codes required
        Dim code02 As New BOStock.cStockAdjustCode(Oasys3DB)
        code02.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, code02.Number, "02")
        code02.LoadMatches()

        Dim code53 As New BOStock.cStockAdjustCode(Oasys3DB)
        code53.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, code53.Number, "53")
        code53.LoadMatches()

        For Each det As cHhtDetail In Details
            RaiseEvent UpdateProgess(CInt(_Details.IndexOf(det) / _Details.Count * 100), My.Resources.ApplySaving)

            If det.Adjusted.Value Then Continue For

            Dim onHandVariance As Integer = det.TotalCount(CountType.Normal) - det.PreSold.Value - det.StockOnHand.Value
            If onHandVariance <> 0 Then
                det.Stock.Adjust.CreateNew(code02, UserID, "From PIC Count", onHandVariance, det.Stock, _dateCreated.Value)
            End If

            Dim markdownVariance As Integer = CInt(det.TotalCount(CountType.Markdown) - det.MarkdownQty.Value)
            If markdownVariance <> 0 Then
                det.Stock.AdjustMarkdown.CreateNewMarkdown(code53, UserID, "From PIC Count", markdownVariance, det.Stock, _dateCreated.Value)
            End If

            If onHandVariance <> 0 Then
                det.Adjusted.Value = True
                det.SaveIfExists()
            End If
        Next

        _employeeID.Value = UserID.ToString("000")
        _isAdjusted.Value = True
        SaveIfExists()

        RaiseEvent UpdateProgess(100, "")
        RaiseEvent ClearProgress()

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 18/05/2011
    ' Referral No : 560
    ' Notes       : Modified to not update ONHA and MDNQ except during Nightly Run.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ''' <summary>
    ''' Updates hht header, details and locations to database.
    ''' </summary>
    ''' <param name="boolrunApp"></param>
    ''' <remarks></remarks>   

    Public Sub Update(Optional ByVal boolrunApp As Boolean = False)

        Try
            'set header number items
            SetHeaderFields()

            'start transaction and add header
            Oasys3DB.BeginTransaction()
            If Not SaveIfExists() Then SaveIfNew()

            'add details and locations
            For Each det As cHhtDetail In _Details
                If boolrunApp Then
                    det.MarkdownQty.Value = 0
                    det.StockOnHand.Value = 0
                End If
                If Not det.SaveIfExists Then det.SaveIfNew()
                For Each l As cHhtLocation In det.Locations
                    If Not l.SaveIfExists Then l.SaveIfNew()
                Next
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        End Try

    End Sub


    Public Sub AddDetails(ByRef stocks As List(Of BOStock.cStock))

        For Each stock As BOStock.cStock In stocks
            AddDetail(stock)
        Next

    End Sub

    Public Sub AddDetail(ByRef stock As BOStock.cStock)

        'check that sku is not already in details collection - forcing load if necessary
        For Each det As cHhtDetail In Details
            If det.SkuNumber.Value = stock.SkuNumber.Value Then
                Throw New OasysDbException(My.Resources.Errors.WarnStockItemAlreadyExists)
            End If
        Next

        'check that stock item is valid
        If Not stock.PicValidate Then
            Exit Sub
        End If


        'create new detail and add to collection
        Dim newDet As New cHhtDetail(Oasys3DB)
        newDet.Stock = stock
        newDet.DateCreated.Value = _dateCreated.Value
        newDet.SkuNumber.Value = stock.SkuNumber.Value
        newDet.NonStockItem.Value = stock.NonStockItem.Value
        newDet.StockOnHand.Value = stock.StockOnHand.Value
        newDet.LabelDetailOK.Value = True
        newDet.SkuOrigin.Value = "S"
        _Details.Add(newDet)
        _numItems.Value += 1

        'get any related items
        Dim singleSkus As ArrayList = Nothing
        If stock.RelatedItemSingle.Value Then
            Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
            Dim bulkSku As String = relItem.GetBulkSkuNumber(stock.SkuNumber.Value)
            singleSkus = relItem.GetSingleSkuNumbers(bulkSku)
        End If

        If stock.RelatedNoItems.Value > 0 Then
            Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
            singleSkus = relItem.GetSingleSkuNumbers(stock.SkuNumber.Value)
        End If

        If singleSkus IsNot Nothing AndAlso singleSkus.Count > 0 Then
            For Each sku As String In singleSkus
                If sku <> stock.SkuNumber.Value Then
                    'load stock item and add to details
                    Dim newStock As New BOStock.cStock(Oasys3DB)
                    newStock.LoadStockItem(sku)
                    AddDetail(newStock)
                End If
            Next
        End If

    End Sub


    'Public Function GetStockItem(ByVal number As String) As Integer

    '    Try
    '        'if length >6 then search for sku in EANMAS
    '        If number.Length > 6 Then
    '            Dim BarCodes As New BOStock.cBarCodes(Oasys3DB)
    '            BarCodes.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BarCodes.Number, number.PadLeft(16, "0"c))
    '            If BarCodes.LoadMatches.Count > 0 Then
    '                number = BarCodes.SKUNumber.Value
    '            End If
    '        End If

    '        'check to see if sku already exists in details
    '        number = number.PadLeft(6, "0"c)
    '        For Each det As cHhtDetail In Details
    '            If det.SkuNumber.Value = number Then Return True
    '        Next

    '        'Get stock item
    '        Dim stocks As New BOStock.cStock(Oasys3DB)
    '        stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.SkuNumber, number)
    '        If stocks.LoadMatches.Count = 0 Then Return 0

    '        'validate stock
    '        Dim stocksToAdd As New List(Of BOStock.cStock)
    '        If stocks.PicValidate Then
    '            stocksToAdd.Add(stocks)
    '        End If

    '        'check if any items left and get related single items
    '        If stocksToAdd.Count = 0 Then Return 0
    '        GetRelatedItems(stocksToAdd)

    '        'insert stock records into hhtHeader details
    '        For Each stock As BOStock.cStock In stocksToAdd
    '            Detail(stock).LabelDetailOK.Value = True
    '            Detail(stock).SkuOrigin.Value = "H"
    '        Next

    '        'reset header item count
    '        _NumberItems.Value = _Details.Count
    '        Return stocksToAdd.Count

    '    Catch ex As Exception
    '        Throw ex
    '        Return -1
    '    End Try

    'End Function

    Public Function GetSupplierItems(ByVal supplierNumber As String) As Integer

        Dim stocks As New BOStock.cStock(Oasys3DB)
        stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.SupplierNo, supplierNumber)
        stocks.Stocks = stocks.LoadMatches
        If stocks.Stocks.Count = 0 Then Return 0

        'validate each stock record and add to collection
        Dim stocksToAdd As New List(Of BOStock.cStock)
        Dim skuNumbers As New ArrayList

        For Each stock As BOStock.cStock In stocks.Stocks
            If stock.PicValidate Then
                If Not skuNumbers.Contains(stock.SkuNumber.Value) Then
                    skuNumbers.Add(stock.SkuNumber.Value)
                    stocksToAdd.Add(stock)
                End If
            End If
        Next

        'check if any items left and get related single items
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "H"
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return stocksToAdd.Count

    End Function

    Public Function GetHierarchyItems(ByVal category As String, ByVal group As String, ByVal subgroup As String, ByVal style As String) As Integer

        Dim stocks As New BOStock.cStock(Oasys3DB)
        stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.HierCategory, category)

        If group <> "000000" Then
            stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.HierGroup, group)
        End If

        If subgroup <> "000000" Then
            stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.HierSubGroup, subgroup)
        End If

        If style <> "000000" Then
            stocks.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.HierStyle, style)
        End If

        stocks.Stocks = stocks.LoadMatches
        If stocks.Stocks.Count = 0 Then Return 0

        'validate each stock record and add to collection
        Dim stocksToAdd As New List(Of BOStock.cStock)
        Dim skuNumbers As New ArrayList

        For Each stock As BOStock.cStock In stocks.Stocks
            If stock.PicValidate Then
                If Not skuNumbers.Contains(stock.SkuNumber.Value) Then
                    skuNumbers.Add(stock.SkuNumber.Value)
                    stocksToAdd.Add(stock)
                End If
            End If
        Next

        'check if any items left and get related single items
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "H"
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return stocksToAdd.Count

    End Function

    Private Function LoadCycleItems(ByVal cycleDate As Date) As Integer

        'get current cyle day number to get hierarchies from PicControl
        Dim sysDates As New BOSystem.cSystemDates(Oasys3DB)
        sysDates.Load()

        Dim dayNumber As Integer = CInt(sysDates.TomorrowsDayCode.Value)
        'Dim dayNumber As Integer = sysDates.TodayDayCode.Value
        dayNumber += CInt(sysDates.WeekCycleNumber.Value * 7)

        'subtract to find how many days to go back
        dayNumber += CInt(DateDiff(DateInterval.Day, sysDates.Tomorrow.Value, cycleDate))
        If dayNumber < 0 Then dayNumber += 28

        'get PicControl record
        Dim PicControl As New cPicControl(Oasys3DB)
        PicControl.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.CycleDayNumber, dayNumber)
        PicControl.Groups = PicControl.LoadMatches

        'get stocks for each control record
        Dim stocks As New List(Of BOStock.cStock)

        For Each control As cPicControl In PicControl.Groups
            Using stock As New BOStock.cStock(Oasys3DB)
                stock.ClearLoadFilter()
                stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stock.HierCategory, control.HieCategory.Value)

                If control.HieGroup.Value <> "000000" Then
                    stock.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stock.HierGroup, control.HieGroup.Value)
                End If

                If control.HieSubgroup.Value <> "000000" Then
                    stock.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stock.HierSubGroup, control.HieSubgroup.Value)
                End If

                If control.HieStyle.Value <> "000000" Then
                    stock.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stock.HierStyle, control.HieStyle.Value)
                End If

                stocks.AddRange(stock.LoadMatches)
            End Using
        Next

        'check any stocks returned
        If stocks.Count = 0 Then
            Return 0
        End If

        'validate each stock record and add to collection
        Dim stocksToAdd As New List(Of BOStock.cStock)
        Dim skuNumbers As New ArrayList

        For Each stock As BOStock.cStock In stocks
            Trace.WriteLine("Processing stock item: " & stock.SkuNumber.Value)

            If stock.PicValidate Then
                If Not skuNumbers.Contains(stock.SkuNumber.Value) Then
                    skuNumbers.Add(stock.SkuNumber.Value)
                    stocksToAdd.Add(stock)
                End If
            Else
                Trace.WriteLine("Stock failed PicValidate routine: " & stock.SkuNumber.Value)
            End If
        Next

        'check if any items left and get related single items
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = ""
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return stocksToAdd.Count

    End Function

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 19/05/2011
    ' Referral No : 566
    ' Notes       : Modified to load proper Refund Items 
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 01/08/2011
    ' Referral No : 0861
    ' Notes       : Modified the code to make it switchable 
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Function LoadRefundedItems() As Integer
        Dim minPrice As Decimal
        Dim saleSku As DataTable
        Dim skuItems As StringBuilder = New StringBuilder()
        Dim skuNumbersl As New ArrayList
        Dim skuNumbers As New ArrayList
        Dim randPercent As Integer
        Dim refundCollection As IRefund
        Dim GetRefundedSkusFactory As New HHTHeaderFactory

        'check if today is a day that can run refund checks
        Using param As New BOSystem.cParameter(Oasys3DB)
            If Not param.GetParameterBoolean(4430 + DateCreated.Value.DayOfWeek) Then Return 0
            minPrice = param.GetParameterDecimal(4420)
            randPercent = param.GetParameterInteger(4421)
        End Using

        ' Now the GetRefundSkus (below) accounts for lines over a threshold, new way is to not do this, still have call for the old way
        skuNumbers = GetRefundedSkusFactory.GetImplementation.GetRefundedLinesOverThresholdValue(DateCreated.Value.AddDays(-1), minPrice)

        'Partha Dutta
        '
        'Existing functionality was taking seven days off
        'New functionality will take one day off
        'Pass yesterday's date and if existing fucntionality is being used then it will take a further 6 days off also
        refundCollection = (New RefundFactory).GetImplementation
        saleSku = refundCollection.GetRefundedLinesNotPreviouslyCounted(DateCreated.Value.AddDays(-1))

        If Not saleSku Is Nothing Then
            For indexelm As Integer = 0 To saleSku.Rows.Count - 1
                If Not skuNumbersl.Contains(saleSku.Rows(indexelm)(0)) Then skuNumbersl.Add(saleSku.Rows(indexelm)(0))
            Next
            saleSku.Clear()
        End If

        ' collect the refunded sku's (originally random, now targetted)
        skuNumbersl = GetRefundedSkusFactory.GetImplementation.GetRefundSkus(minPrice, randPercent, DateCreated.Value.AddDays(-1), DateCreated.Value.AddDays(-1))

        Dim stocksl As New BOStock.cStock(Oasys3DB)
        Dim stocksToAdd As New List(Of BOStock.cStock)

        If skuNumbersl.Count > 0 Then
            stocksl.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocksl.SkuNumber, skuNumbersl)
            stocksl.Stocks = stocksl.LoadMatches
            If stocksl.Stocks.Count > 0 Then
                For Each stock As BOStock.cStock In stocksl.Stocks
                    stocksToAdd.Add(stock)
                Next
            End If
        End If

        'get stock items in the array

        If skuNumbers.Count > 0 Then
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
            stocks.Stocks = stocks.LoadMatches
            If stocks.Stocks.Count = 0 Then Return 0

            'validate each stock record and add to collection
            For Each stock As BOStock.cStock In stocks.Stocks
                If stock.PicValidate Then
                    stocksToAdd.Add(stock)
                End If
            Next
        End If

        'get related items if any stocks to add
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "R"
        Next

        'reset header item count
        _numItems.Value = _Details.Count

        Return stocksToAdd.Count
    End Function

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 19/05/2011
    ' Referral No : 566
    ' Notes       : Modified to load all Negative Items 
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Function LoadNegativeItems() As Integer

        'get negative stock items 
        Dim stocks As New BOStock.cStock(Oasys3DB)
        stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pLessThan, stocks.StockOnHand, 0)
        stocks.Stocks = stocks.LoadMatches
        If stocks.Stocks.Count = 0 Then Return 0


        'get max number of stocks to add
        'Dim stocksMax As Integer = 0
        'Using param As New BOSystem.cParameter(Oasys3DB)
        '    stocksMax = param.GetParameterInteger(4440)
        'End Using

        'validate stock items
        Dim stocksToAdd As New List(Of BOStock.cStock)
        For Each stock As BOStock.cStock In stocks.Stocks
            If stock.PicValidate Then
                stocksToAdd.Add(stock)
                'If stocksToAdd.Count = stocksMax Then Exit For
            End If
        Next

        'get related items if any stocks to add
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "N"
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return stocksToAdd.Count

    End Function

    Private Function LoadParkedSaleItems() As Integer

        'get sales headers for previous day that have been parked and not recalled
        Dim SalesHeader As New BOSales.cSalesHeader(Oasys3DB)
        SalesHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SalesHeader.TransParked, True)
        SalesHeader.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SalesHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SalesHeader.OrderNo, "000000")
        SalesHeader.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SalesHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SalesHeader.RecoveredFromParked, False)
        SalesHeader.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SalesHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SalesHeader.TransDate, _dateCreated.Value.AddDays(-1))
        SalesHeader.Headers = SalesHeader.LoadMatches

        'check any headers returned
        If SalesHeader.Headers.Count = 0 Then Return 0

        'get stock items in these records that are not voucher types
        Dim skuNumbers As New ArrayList
        For Each header As BOSales.cSalesHeader In SalesHeader.Headers
            For Each line As BOSales.cSalesLine In header.Lines
                If line.SaleType.Value <> "V" Then
                    If Not skuNumbers.Contains(line.SkuNumber.Value) Then skuNumbers.Add(line.SkuNumber.Value)
                End If
            Next
        Next

        'get stock items in the array
        If skuNumbers.Count = 0 Then Return 0
        Dim Stocks As New BOStock.cStock(Oasys3DB)
        Stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, Stocks.SkuNumber, skuNumbers)
        Stocks.Stocks = Stocks.LoadMatches

        'validate each stock record
        Dim StocksToAdd As New List(Of BOStock.cStock)
        For Each stock As BOStock.cStock In Stocks.Stocks
            If stock.PicValidate Then StocksToAdd.Add(stock)
        Next

        'check if any items left and get related items if exist
        If StocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(StocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In StocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "P"
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return StocksToAdd.Count

    End Function

    Private Function LoadTopStockLossItems() As Integer

        'get stock loss codes from sacode table
        Dim saCode As New BOStock.cStockAdjustCode(Oasys3DB)
        Dim codes As ArrayList = saCode.GetStockLossCodes

        'get stock adjustments datatable
        Dim param As New BOSystem.cParameter(Oasys3DB)
        Dim stockAdjust As New BOStock.cStockAdjust(Oasys3DB)
        Dim adjusts As DataTable = stockAdjust.GetAdjustmentsTable(_dateCreated.Value.AddDays(param.GetParameterInteger(4460) * -7), codes)

        'check any adjustments returned
        If adjusts.Rows.Count = 0 Then Return 0

        'create temp table to hold sku, absolute stock movement and value
        Dim dt As New DataTable
        dt.Columns.Add("sku", GetType(String))
        dt.Columns.Add("movement", GetType(Integer))
        dt.Columns.Add("value", GetType(Decimal))
        dt.PrimaryKey = New DataColumn() {dt.Columns("sku")}

        'add skus from adjustments to table
        For Each adjust As DataRow In adjusts.Rows
            Dim sku As String = adjust(stockAdjust.SkuNumber.ColumnName).ToString
            Dim qty As Integer = Math.Abs(CInt(adjust(stockAdjust.QtyAdjusted.ColumnName)))
            Dim price As Decimal = Math.Abs(CDec(adjust(stockAdjust.Price.ColumnName)))

            Dim dr As DataRow = dt.Rows.Find(sku)
            If dr Is Nothing Then
                dt.Rows.Add(sku, qty, qty * price)
            Else
                dr("movement") = CInt(dr("movement")) + qty
                dr("value") = CDec(dr("value")) + CDec(qty * price)
            End If
        Next

        'sort by movement then value and take top param(4450) items and place in collection
        Dim stocksMax As Integer = param.GetParameterInteger(4450)
        Dim stocksToAdd As New List(Of BOStock.cStock)
        Dim dv As DataView = New DataView(dt)
        dv.Sort = "movement, value"

        'get stock item for sku and validate and add until limit is reached
        For rowIndex As Integer = 0 To dv.Count - 1
            Dim sku As String = dv.Item(rowIndex).Item("sku").ToString
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.ClearLoadFilter()
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.SkuNumber, sku)
            stocks.LoadMatches()

            If stocks.PicValidate Then
                stocksToAdd.Add(stocks)
                If stocksToAdd.Count = stocksMax Then Exit For
            End If
        Next

        'check if any items left and get related items if exist
        If stocksToAdd.Count = 0 Then Return 0
        Dim relItem As New BOStock.cRelatedItems(Oasys3DB)
        relItem.GetRelatedItems(stocksToAdd)

        'insert stock records into hhtHeader details
        For Each stock As BOStock.cStock In stocksToAdd
            Detail(stock).LabelDetailOK.Value = True
            Detail(stock).SkuOrigin.Value = "H"
        Next

        'reset header item count
        _numItems.Value = _Details.Count
        Return stocksToAdd.Count

    End Function

    Public Function ISISPerpetualInventorycheck() As String
        'Perpetual Inventory check count open()

        Dim BOStockTake As New BOStockTake.cHhtHeader(Oasys3DB)
        Dim strReturnMessage As String = String.Empty

        Const METHOD_NAME As String = "ISISPerpetualInventorycheck"

        Trace.WriteLine(METHOD_NAME & " Start")

        Try
            'loads the current PIC Check record for today
            BOStockTake.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockTake.DateCreated, Date.Today)

            'if there is not record one is created and a positive message is sent back
            If BOStockTake.LoadMatches.Count = 0 Then
                strReturnMessage = "NE"
                Return strReturnMessage
            Else
                If BOStockTake.EmployeeID.Value = "000" Then
                    strReturnMessage = "YE"
                Else
                    strReturnMessage = "NE"
                    Return strReturnMessage
                End If
                If BOStockTake.IsLocked.Value = False Then
                    BOStockTake.IsLocked.Value = True
                    BOStockTake.SaveIfExists()
                End If

            End If

            Return strReturnMessage

        Catch ex As Exception
            Trace.WriteLine("There is an error with the process PerpetualInventorycheck error " & ex.Message)
            strReturnMessage = "?"
            Return strReturnMessage
        End Try

    End Function

    Public Function ISISGetPerpetualInventorySKUs(ByRef intCount As Integer, ByRef strReturnMessage As String) As Boolean
        'Perpetual Inventory SKUs(request the full days list)

        Dim BOStockTake As New BOStockTake.cHhtHeader(Oasys3DB)
        Const METHOD_NAME As String = "ISISGetPerpetualInventorySKUs"

        Trace.WriteLine(METHOD_NAME & " Start")

        Try
            'checks to see if the SKU request has already been made by checking the NumberItems(NUMB) field on the HHTDET table
            BOStockTake.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockTake.DateCreated, Date.Today)
            If BOStockTake.LoadMatches.Count > 0 Then
                If BOStockTake.Adjusted.Value = True Then
                    Return False
                End If
            End If

            'clears down stocktake to be used again
            BOStockTake.ClearLists()

            'loads up all the skus for today that need to be checked
            BOStockTake.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockTake.DateCreated, Today.Date)
            strReturnMessage += String.Format("{0,-4}", BOStockTake.Details.Count)
            For Each detail As BOStockTake.cHhtDetail In BOStockTake.Details
                strReturnMessage += detail.SkuNumber.Value
                intCount += 1
                'check to make sure the MAX skus within the message is 1500
                If intCount = 1500 Then
                    Exit For
                End If
            Next

            Return True
            ''clears the stocktake to be used again
            'BOStockTake.ClearLists()

            ''loads up the current PIC check session and sets the Numberitems(NUMB)Field to the ammount of items within this message
            '    BOStockTake.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockTake.DateCreated, Format(Today, "yyyy-MM-dd"))
            '    If BOStockTake.LoadMatches.Count > 0 Then
            '        BOStockTake.Adjusted.Value = True
            '        If BOStockTake.SaveIfExists = True Then
            '            Return True
            '        Else
            '            Return False
            '        End If
            '    Else
            '        Return False
            '    End If
            '    Return strReturnMessage
        Catch ex As Exception
            Trace.WriteLine("There is an error with the process PerpetualInventorycheck error " & ex.Message)
            ISISGetPerpetualInventorySKUs = CBool("?")
        End Try



    End Function

#End Region

End Class

