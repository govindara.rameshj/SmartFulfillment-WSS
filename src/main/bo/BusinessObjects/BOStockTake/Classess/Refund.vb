﻿''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author      : Dhanesh Ramachandran
' Date        : 01/08/2011
' Referral No : 0861
' Notes       : Added to make PIC Refund switchable 
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Class Refund
    Implements IRefund

    Public Function GetRefundedLinesNotPreviouslyCounted(ByVal SelectedDate As Date) As DataTable Implements IRefund.GetRefundedLinesNotPreviouslyCounted
        Dim Repository As IRefundRepository
        Dim DT As DataTable

        Repository = RefundRepositoryFactory.FactoryGet
        DT = Repository.RefundDistinct(SelectedDate)
        Return DT

    End Function

    Public Function GetRefundedLinesOverThresholdValue(ByVal SelectedDate As Date, ByVal MinPrice As Decimal) As DataTable Implements IRefund.GetRefundedLinesOverThresholdValue
        Dim Repository As IRefundRepository
        Dim DT As DataTable

        Repository = RefundRepositoryFactory.FactoryGet
        DT = Repository.RefundItem(SelectedDate, MinPrice)
        Return DT
    End Function

    Public Function GetRefundedLinesPseudoRandomSample(ByVal SelectedDate As Date, ByVal MinPrice As Decimal, ByVal RandPercent As Integer) As DataTable Implements IRefund.GetRefundedLinesPseudoRandomSample
        Dim Repository As IRefundRepository
        Dim DT As DataTable

        Repository = RefundRepositoryFactory.FactoryGet
        DT = Repository.RefundRandom(SelectedDate, MinPrice, RandPercent)
        Return DT
    End Function

End Class


