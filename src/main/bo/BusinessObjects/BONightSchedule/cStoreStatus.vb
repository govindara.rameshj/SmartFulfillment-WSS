﻿<Serializable()> Public Class cStoreStatus
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SYSNID"
        BOFields.Add(_ID)
        BOFields.Add(_MasterOpen)
        BOFields.Add(_MasterDateOpen)
        BOFields.Add(_Slave1Open)
        BOFields.Add(_Slave2Open)
        BOFields.Add(_Slave3Open)
        BOFields.Add(_Slave4Open)
        BOFields.Add(_Slave5Open)
        BOFields.Add(_Slave6Open)
        BOFields.Add(_Slave7Open)
        BOFields.Add(_Slave8Open)
        BOFields.Add(_Slave9Open)
        BOFields.Add(_Slave10Open)
        BOFields.Add(_Slave11Open)
        BOFields.Add(_Slave12Open)
        BOFields.Add(_Slave13Open)
        BOFields.Add(_Slave14Open)
        BOFields.Add(_Slave15Open)
        BOFields.Add(_Slave16Open)
        BOFields.Add(_Slave17Open)
        BOFields.Add(_Slave18Open)
        BOFields.Add(_Slave19Open)
        BOFields.Add(_Slave20Open)
        BOFields.Add(_Slave21Open)
        BOFields.Add(_Slave22Open)
        BOFields.Add(_Slave23Open)
        BOFields.Add(_Slave24Open)
        BOFields.Add(_Slave25Open)
        BOFields.Add(_Slave26Open)
        BOFields.Add(_Slave27Open)
        BOFields.Add(_Slave28Open)
        BOFields.Add(_Slave29Open)
        BOFields.Add(_Slave30Open)
        BOFields.Add(_Slave31Open)
        BOFields.Add(_UXTransferSpeed)
        BOFields.Add(_MainPCSerialPort)
        BOFields.Add(_BuppcSerialPort)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStoreStatus)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStoreStatus)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStoreStatus))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStoreStatus(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("FKEY", "", True)
    Private _MasterOpen As New ColField(Of Boolean)("MAST", False)
    Private _MasterDateOpen As New ColField(Of Date)("DATE1", Nothing)
    Private _Slave1Open As New ColField(Of Boolean)("SLAV1", False)
    Private _Slave2Open As New ColField(Of Boolean)("SLAV2", False)
    Private _Slave3Open As New ColField(Of Boolean)("SLAV3", False)
    Private _Slave4Open As New ColField(Of Boolean)("SLAV4", False)
    Private _Slave5Open As New ColField(Of Boolean)("SLAV5", False)
    Private _Slave6Open As New ColField(Of Boolean)("SLAV6", False)
    Private _Slave7Open As New ColField(Of Boolean)("SLAV7", False)
    Private _Slave8Open As New ColField(Of Boolean)("SLAV8", False)
    Private _Slave9Open As New ColField(Of Boolean)("SLAV9", False)
    Private _Slave10Open As New ColField(Of Boolean)("SLAV10", False)
    Private _Slave11Open As New ColField(Of Boolean)("SLAV11", False)
    Private _Slave12Open As New ColField(Of Boolean)("SLAV12", False)
    Private _Slave13Open As New ColField(Of Boolean)("SLAV13", False)
    Private _Slave14Open As New ColField(Of Boolean)("SLAV14", False)
    Private _Slave15Open As New ColField(Of Boolean)("SLAV15", False)
    Private _Slave16Open As New ColField(Of Boolean)("SLAV16", False)
    Private _Slave17Open As New ColField(Of Boolean)("SLAV17", False)
    Private _Slave18Open As New ColField(Of Boolean)("SLAV18", False)
    Private _Slave19Open As New ColField(Of Boolean)("SLAV19", False)
    Private _Slave20Open As New ColField(Of Boolean)("SLAV20", False)
    Private _Slave21Open As New ColField(Of Boolean)("SLAV21", False)
    Private _Slave22Open As New ColField(Of Boolean)("SLAV22", False)
    Private _Slave23Open As New ColField(Of Boolean)("SLAV23", False)
    Private _Slave24Open As New ColField(Of Boolean)("SLAV24", False)
    Private _Slave25Open As New ColField(Of Boolean)("SLAV25", False)
    Private _Slave26Open As New ColField(Of Boolean)("SLAV26", False)
    Private _Slave27Open As New ColField(Of Boolean)("SLAV27", False)
    Private _Slave28Open As New ColField(Of Boolean)("SLAV28", False)
    Private _Slave29Open As New ColField(Of Boolean)("SLAV29", False)
    Private _Slave30Open As New ColField(Of Boolean)("SLAV30", False)
    Private _Slave31Open As New ColField(Of Boolean)("SLAV31", False)
    Private _UXTransferSpeed As New ColField(Of Decimal)("TSPD", 0)
    Private _MainPCSerialPort As New ColField(Of String)("MPRT", "")
    Private _BuppcSerialPort As New ColField(Of String)("BPRT", "")

#End Region

#Region "Field Properties"

    Public Property StoreStatusID() As ColField(Of String)
        Get
            StoreStatusID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property MasterOpen() As ColField(Of Boolean)
        Get
            MasterOpen = _MasterOpen
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _MasterOpen = value
        End Set
    End Property
    Public Property MasterDateOpen() As ColField(Of Date)
        Get
            MasterDateOpen = _MasterDateOpen
        End Get
        Set(ByVal value As ColField(Of Date))
            _MasterDateOpen = value
        End Set
    End Property
    Public Property Slave1Open() As ColField(Of Boolean)
        Get
            Slave1Open = _Slave1Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave1Open = value
        End Set
    End Property
    Public Property Slave2Open() As ColField(Of Boolean)
        Get
            Slave2Open = _Slave2Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave2Open = value
        End Set
    End Property
    Public Property Slave3Open() As ColField(Of Boolean)
        Get
            Slave3Open = _Slave3Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave3Open = value
        End Set
    End Property
    Public Property Slave4Open() As ColField(Of Boolean)
        Get
            Slave4Open = _Slave4Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave4Open = value
        End Set
    End Property
    Public Property Slave5Open() As ColField(Of Boolean)
        Get
            Slave5Open = _Slave5Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave5Open = value
        End Set
    End Property
    Public Property Slave6Open() As ColField(Of Boolean)
        Get
            Slave6Open = _Slave6Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave6Open = value
        End Set
    End Property
    Public Property Slave7Open() As ColField(Of Boolean)
        Get
            Slave7Open = _Slave7Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave7Open = value
        End Set
    End Property
    Public Property Slave8Open() As ColField(Of Boolean)
        Get
            Slave8Open = _Slave8Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave8Open = value
        End Set
    End Property
    Public Property Slave9Open() As ColField(Of Boolean)
        Get
            Slave9Open = _Slave9Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave9Open = value
        End Set
    End Property
    Public Property Slave10Open() As ColField(Of Boolean)
        Get
            Slave10Open = _Slave10Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave10Open = value
        End Set
    End Property
    Public Property Slave11Open() As ColField(Of Boolean)
        Get
            Slave11Open = _Slave11Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave11Open = value
        End Set
    End Property
    Public Property Slave12Open() As ColField(Of Boolean)
        Get
            Slave12Open = _Slave12Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave12Open = value
        End Set
    End Property
    Public Property Slave13Open() As ColField(Of Boolean)
        Get
            Slave13Open = _Slave13Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave13Open = value
        End Set
    End Property
    Public Property Slave14Open() As ColField(Of Boolean)
        Get
            Slave14Open = _Slave14Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave14Open = value
        End Set
    End Property
    Public Property Slave15Open() As ColField(Of Boolean)
        Get
            Slave15Open = _Slave15Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave15Open = value
        End Set
    End Property
    Public Property Slave16Open() As ColField(Of Boolean)
        Get
            Slave16Open = _Slave16Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave16Open = value
        End Set
    End Property
    Public Property Slave17Open() As ColField(Of Boolean)
        Get
            Slave17Open = _Slave17Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave17Open = value
        End Set
    End Property
    Public Property Slave18Open() As ColField(Of Boolean)
        Get
            Slave18Open = _Slave18Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave18Open = value
        End Set
    End Property
    Public Property Slave19Open() As ColField(Of Boolean)
        Get
            Slave19Open = _Slave19Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave19Open = value
        End Set
    End Property
    Public Property Slave20Open() As ColField(Of Boolean)
        Get
            Slave20Open = _Slave20Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave20Open = value
        End Set
    End Property
    Public Property Slave21Open() As ColField(Of Boolean)
        Get
            Slave21Open = _Slave21Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave21Open = value
        End Set
    End Property
    Public Property Slave22Open() As ColField(Of Boolean)
        Get
            Slave22Open = _Slave22Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave22Open = value
        End Set
    End Property
    Public Property Slave23Open() As ColField(Of Boolean)
        Get
            Slave23Open = _Slave23Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave23Open = value
        End Set
    End Property
    Public Property Slave24Open() As ColField(Of Boolean)
        Get
            Slave24Open = _Slave24Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave24Open = value
        End Set
    End Property
    Public Property Slave25Open() As ColField(Of Boolean)
        Get
            Slave25Open = _Slave25Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave25Open = value
        End Set
    End Property
    Public Property Slave26Open() As ColField(Of Boolean)
        Get
            Slave26Open = _Slave26Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave26Open = value
        End Set
    End Property
    Public Property Slave27Open() As ColField(Of Boolean)
        Get
            Slave27Open = _Slave27Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave27Open = value
        End Set
    End Property
    Public Property Slave28Open() As ColField(Of Boolean)
        Get
            Slave28Open = _Slave28Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave28Open = value
        End Set
    End Property
    Public Property Slave29Open() As ColField(Of Boolean)
        Get
            Slave29Open = _Slave29Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave29Open = value
        End Set
    End Property
    Public Property Slave30Open() As ColField(Of Boolean)
        Get
            Slave30Open = _Slave30Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave30Open = value
        End Set
    End Property
    Public Property Slave31Open() As ColField(Of Boolean)
        Get
            Slave31Open = _Slave31Open
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Slave31Open = value
        End Set
    End Property
    Public Property UXTransferSpeed() As ColField(Of Decimal)
        Get
            UXTransferSpeed = _UXTransferSpeed
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UXTransferSpeed = value
        End Set
    End Property
    Public Property MainPCSerialPort() As ColField(Of String)
        Get
            MainPCSerialPort = _MainPCSerialPort
        End Get
        Set(ByVal value As ColField(Of String))
            _MainPCSerialPort = value
        End Set
    End Property
    Public Property BUPPCserialPort() As ColField(Of String)
        Get
            BUPPCserialPort = _BuppcSerialPort
        End Get
        Set(ByVal value As ColField(Of String))
            _BuppcSerialPort = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function StoreStatus() As DataRow

        Try
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.StoreStatusID, "01")
            Dim ds As DataSet = GetSQLSelectDataSet()
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return ds.Tables(0).Rows(0)
                End If
            End If

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

        Return Nothing

    End Function

    Public Function LogOffAll() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_MasterOpen.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave1Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave2Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave3Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave4Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave5Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave6Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave7Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave8Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave9Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave10Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave11Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave12Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave13Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave14Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave15Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave16Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave17Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave18Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave19Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave20Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave21Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave22Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave23Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave24Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave25Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave26Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave27Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave28Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave29Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave30Open.ColumnName, False)
            Oasys3DB.SetColumnAndValueParameter(_Slave31Open.ColumnName, False)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

            If Oasys3DB.Update() <> -1 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

    End Function

    Public Function LogOffSlave(ByVal Number As String) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            For Each item In BOFields
                If item.ColumnName = "SLAV" & Number Then
                    Oasys3DB.SetColumnAndValueParameter(item.ColumnName, False)
                End If
            Next

            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

            If Oasys3DB.Update() <> -1 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try


    End Function

    Public Function LogOnSlave(ByVal Number As String) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            For Each item In BOFields
                If item.ColumnName = "SLAV" & Number Then
                    Oasys3DB.SetColumnAndValueParameter(item.ColumnName, True)
                End If
            Next

            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

            If Oasys3DB.Update() <> -1 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

    End Function

    Public Function LogOnMaster() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_MasterOpen.ColumnName, True)
            Oasys3DB.SetColumnAndValueParameter(_MasterDateOpen.ColumnName, Today)
            Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "01")

            If Oasys3DB.Update() <> -1 Then Return True
            Return False

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.GetType.ToString)
        End Try

    End Function

#End Region

End Class

