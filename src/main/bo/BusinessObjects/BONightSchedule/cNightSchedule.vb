﻿<Serializable()> Public Class cNightSchedule
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "NITMAS"
        BOFields.Add(_SetNumber)
        BOFields.Add(_TaskNumber)
        BOFields.Add(_Description)
        BOFields.Add(_ProgramName)
        BOFields.Add(_RunInNight)
        BOFields.Add(_RunInRetry)
        BOFields.Add(_WeeklyRun)
        BOFields.Add(_MonthlyRun)
        BOFields.Add(_RunBeforeStoreLive)
        BOFields.Add(_OptionNoMemoWrite)
        BOFields.Add(_AbortOnJobError)
        BOFields.Add(_RunStmtOnJobError)
        BOFields.Add(_DayWeek1)
        BOFields.Add(_DayWeek2)
        BOFields.Add(_DayWeek3)
        BOFields.Add(_DayWeek4)
        BOFields.Add(_DayWeek5)
        BOFields.Add(_DayWeek6)
        BOFields.Add(_DayWeek7)
        BOFields.Add(_StartDate)
        BOFields.Add(_EndDate)
        BOFields.Add(_LastRunStartDate)
        BOFields.Add(_LastRunStartTime)
        BOFields.Add(_LastRunEndDate)
        BOFields.Add(_LastRunEndTime)
        BOFields.Add(_LastAbortDate)
        BOFields.Add(_LastAbortTime)
        BOFields.Add(_TaskType)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cNightSchedule)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cNightSchedule)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cNightSchedule))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cNightSchedule(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SetNumber As New ColField(Of String)("NSET", "", "Set Number", True, False)
    Private _TaskNumber As New ColField(Of String)("TASK", "", "Task Number", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _ProgramName As New ColField(Of String)("PROG", "", "Program Name", False, False)
    Private _RunInNight As New ColField(Of Boolean)("NITE", False, "Run in Night", False, False)
    Private _RunInRetry As New ColField(Of Boolean)("RETY", False, "Run in Retry", False, False)
    Private _WeeklyRun As New ColField(Of Boolean)("WEEK", False, "Weekly Run", False, False)
    Private _MonthlyRun As New ColField(Of Boolean)("PEND", False, "Monthly Run", False, False)
    Private _RunBeforeStoreLive As New ColField(Of Boolean)("LIVE", False, "Run Before Store Live", False, False)
    Private _OptionNoMemoWrite As New ColField(Of Decimal)("OPTN", 0, "Option No Memo Write", False, False)
    Private _AbortOnJobError As New ColField(Of Boolean)("ABOR", False, "Abort on Job Error", False, False)
    Private _RunStmtOnJobError As New ColField(Of String)("JRUN", "", "Run stmt on Job Error", False, False)
    Private _DayWeek1 As New ColField(Of Boolean)("DAYS1", False, "Day Week 1", False, False)
    Private _DayWeek2 As New ColField(Of Boolean)("DAYS2", False, "Day Week 2", False, False)
    Private _DayWeek3 As New ColField(Of Boolean)("DAYS3", False, "Day Week 3", False, False)
    Private _DayWeek4 As New ColField(Of Boolean)("DAYS4", False, "Day Week 4", False, False)
    Private _DayWeek5 As New ColField(Of Boolean)("DAYS5", False, "Day Week 5", False, False)
    Private _DayWeek6 As New ColField(Of Boolean)("DAYS6", False, "Day Week 6", False, False)
    Private _DayWeek7 As New ColField(Of Boolean)("DAYS7", False, "Day Week 7", False, False)
    Private _StartDate As New ColField(Of Date)("BEGD", Nothing, "Start Date", False, False)
    Private _EndDate As New ColField(Of Date)("ENDD", Nothing, "End Date", False, False)
    Private _LastRunStartDate As New ColField(Of Date)("SDAT", Nothing, "Last Run Start Date", False, False)
    Private _LastRunStartTime As New ColField(Of String)("STIM", "", "Last Run Start Time", False, False)
    Private _LastRunEndDate As New ColField(Of Date)("EDAT", Nothing, "Last Run End Date", False, False)
    Private _LastRunEndTime As New ColField(Of String)("ETIM", "", "Last Run End Time", False, False)
    Private _LastAbortDate As New ColField(Of Date)("ADAT", Nothing, "Last Abort Date", False, False)
    Private _LastAbortTime As New ColField(Of String)("ATIM", "", "Last Abort Time", False, False)
    Private _TaskType As New ColField(Of Decimal)("TTYPE", 0, "Task Type", False, False)

#End Region

#Region "Field Properties"

    Public Property SetNo() As ColField(Of String)
        Get
            SetNo = _SetNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SetNumber = value
        End Set
    End Property
    Public Property TaskNo() As ColField(Of String)
        Get
            TaskNo = _TaskNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TaskNumber = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property ProgramName() As ColField(Of String)
        Get
            ProgramName = _ProgramName
        End Get
        Set(ByVal value As ColField(Of String))
            _ProgramName = value
        End Set
    End Property
    Public Property RunInNight() As ColField(Of Boolean)
        Get
            RunInNight = _RunInNight
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RunInNight = value
        End Set
    End Property
    Public Property RunInRetry() As ColField(Of Boolean)
        Get
            RunInRetry = _RunInRetry
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RunInRetry = value
        End Set
    End Property
    Public Property WeeklyRun() As ColField(Of Boolean)
        Get
            WeeklyRun = _WeeklyRun
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _WeeklyRun = value
        End Set
    End Property
    Public Property MonthlyRun() As ColField(Of Boolean)
        Get
            MonthlyRun = _MonthlyRun
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _MonthlyRun = value
        End Set
    End Property
    Public Property RunBeforeStoreLive() As ColField(Of Boolean)
        Get
            RunBeforeStoreLive = _RunBeforeStoreLive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RunBeforeStoreLive = value
        End Set
    End Property
    Public Property OptionNoMemoWrite() As ColField(Of Decimal)
        Get
            OptionNoMemoWrite = _OptionNoMemoWrite
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OptionNoMemoWrite = value
        End Set
    End Property
    Public Property AbortOnJobError() As ColField(Of Boolean)
        Get
            AbortOnJobError = _AbortOnJobError
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AbortOnJobError = value
        End Set
    End Property
    Public Property RunStmtOnJobError() As ColField(Of String)
        Get
            RunStmtOnJobError = _RunStmtOnJobError
        End Get
        Set(ByVal value As ColField(Of String))
            _RunStmtOnJobError = value
        End Set
    End Property
    Public Property DayWK1() As ColField(Of Boolean)
        Get
            DayWK1 = _DayWeek1
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek1 = value
        End Set
    End Property
    Public Property DayWK2() As ColField(Of Boolean)
        Get
            DayWK2 = _DayWeek2
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek2 = value
        End Set
    End Property
    Public Property DayWK3() As ColField(Of Boolean)
        Get
            DayWK3 = _DayWeek3
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek3 = value
        End Set
    End Property
    Public Property DayWK4() As ColField(Of Boolean)
        Get
            DayWK4 = _DayWeek4
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek4 = value
        End Set
    End Property
    Public Property DayWK5() As ColField(Of Boolean)
        Get
            DayWK5 = _DayWeek5
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek5 = value
        End Set
    End Property
    Public Property DayWK6() As ColField(Of Boolean)
        Get
            DayWK6 = _DayWeek6
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek6 = value
        End Set
    End Property
    Public Property DayWK7() As ColField(Of Boolean)
        Get
            DayWK7 = _DayWeek7
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DayWeek7 = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property LastRunStartDate() As ColField(Of Date)
        Get
            LastRunStartDate = _LastRunStartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastRunStartDate = value
        End Set
    End Property
    Public Property LastRunStartTime() As ColField(Of String)
        Get
            LastRunStartTime = _LastRunStartTime
        End Get
        Set(ByVal value As ColField(Of String))
            _LastRunStartTime = value
        End Set
    End Property
    Public Property LastRunEndDate() As ColField(Of Date)
        Get
            LastRunEndDate = _LastRunEndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastRunEndDate = value
        End Set
    End Property
    Public Property LastRunEndTime() As ColField(Of String)
        Get
            LastRunEndTime = _LastRunEndTime
        End Get
        Set(ByVal value As ColField(Of String))
            _LastRunEndTime = value
        End Set
    End Property
    Public Property LastAbortDate() As ColField(Of Date)
        Get
            LastAbortDate = _LastAbortDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastAbortDate = value
        End Set
    End Property
    Public Property LastAbortTime() As ColField(Of String)
        Get
            LastAbortTime = _LastAbortTime
        End Get
        Set(ByVal value As ColField(Of String))
            _LastAbortTime = value
        End Set
    End Property
    Public Property TaskType() As ColField(Of Decimal)
        Get
            TaskType = _TaskType
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TaskType = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function GetHseKeepSchedule(ByVal GroupName As String) As List(Of BONightSchedule.cNightSchedule)

        Dim retlist As New List(Of BONightSchedule.cNightSchedule)

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._ProgramName, GroupName)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._TaskType, 2)
        retlist = LoadMatches()
        If retlist.Count > 0 Then
            Return retlist
        Else
            Return Nothing
        End If

    End Function

    Public Function ListTasks(ByVal Setno As Char) As DataTable

        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._SetNumber, Setno)
        Me.SortBy(TaskNo.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Function ListRetryTasks(ByVal Setno As String, ByVal lastTask As String) As DataTable

        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._SetNumber, Setno)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._RunInRetry, True)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Me._TaskNumber, lastTask)
        Me.SortBy(TaskNo.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

#End Region

End Class

