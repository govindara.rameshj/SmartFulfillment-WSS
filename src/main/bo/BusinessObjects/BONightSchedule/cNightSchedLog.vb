<Serializable()> Public Class cNightSchedLog
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "NITLOG"
        BOFields.Add(_LogDate)
        BOFields.Add(_SetNumber)
        BOFields.Add(_TaskNumber)
        BOFields.Add(_Description)
        BOFields.Add(_ProgramName)
        BOFields.Add(_StartDate)
        BOFields.Add(_StartTime)
        BOFields.Add(_EndDate)
        BOFields.Add(_EndTime)
        BOFields.Add(_AbortDate)
        BOFields.Add(_AbortTime)
        BOFields.Add(_AbortError)
        BOFields.Add(_ErrorMessage)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cNightSchedLog)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cNightSchedLog)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cNightSchedLog))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cNightSchedLog(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _LogDate As New ColField(Of Date)("DATE1", Nothing, "Log Date", True, False)
    Private _SetNumber As New ColField(Of String)("NSET", "", "Set Number", True, False)
    Private _TaskNumber As New ColField(Of String)("TASK", "", "Task Number", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _ProgramName As New ColField(Of String)("PROG", "", "Program Name", False, False)
    Private _StartDate As New ColField(Of Date)("SDAT", Nothing, "Start Date", False, False)
    Private _StartTime As New ColField(Of String)("STIM", "000000", "Start Time", False, False)
    Private _EndDate As New ColField(Of Date)("EDAT", Nothing, "End Date", False, False)
    Private _EndTime As New ColField(Of String)("ETIM", "000000", "End Time", False, False)
    Private _AbortDate As New ColField(Of Date)("ADAT", Nothing, "Abort Date", False, False)
    Private _AbortTime As New ColField(Of String)("ATIM", "000000", "Abort Time", False, False)
    Private _AbortError As New ColField(Of String)("JERR", "", "Abort Error", False, False)
    Private _ErrorMessage As New ColField(Of String)("EMES", "", "Error Message", False, False)

#End Region

#Region "Field Properties"

    Public Property LogDate() As ColField(Of Date)
        Get
            LogDate = _LogDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LogDate = value
        End Set
    End Property
    Public Property SetNo() As ColField(Of String)
        Get
            SetNo = _SetNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SetNumber = value
        End Set
    End Property
    Public Property TaskNo() As ColField(Of String)
        Get
            TaskNo = _TaskNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TaskNumber = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property ProgramName() As ColField(Of String)
        Get
            ProgramName = _ProgramName
        End Get
        Set(ByVal value As ColField(Of String))
            _ProgramName = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property StartTime() As ColField(Of String)
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal value As ColField(Of String))
            _StartTime = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property EndTime() As ColField(Of String)
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal value As ColField(Of String))
            _EndTime = value
        End Set
    End Property
    Public Property AbortDate() As ColField(Of Date)
        Get
            AbortDate = _AbortDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AbortDate = value
        End Set
    End Property
    Public Property AbortTime() As ColField(Of String)
        Get
            AbortTime = _AbortTime
        End Get
        Set(ByVal value As ColField(Of String))
            _AbortTime = value
        End Set
    End Property
    Public Property AbortError() As ColField(Of String)
        Get
            AbortError = _AbortError
        End Get
        Set(ByVal value As ColField(Of String))
            _AbortError = value
        End Set
    End Property
    Public Property ErrorMessage() As ColField(Of String)
        Get
            ErrorMessage = _ErrorMessage
        End Get
        Set(ByVal value As ColField(Of String))
            _ErrorMessage = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function WriteLog(ByVal SetId As String, ByVal TaskNo As String, ByVal Description As String, ByVal ProgramName As String, ByVal StartDate As String, ByVal startTime As String) As Boolean

        Dim TodaysDate As Date = CDate(Oasys3DB.ExecuteSql("SELECT LDAT FROM SYSDAT WHERE FKEY='01'").Tables(0).Rows(0)(0))

        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.SetNo, SetId)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.TaskNo, TaskNo)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.LogDate, TodaysDate)
        Dim RecordExists As Boolean = (LoadMatches.Count > 0)

        'move data to record
        _LogDate.Value = TodaysDate
        _SetNumber.Value = SetId
        _TaskNumber.Value = TaskNo.ToString.PadRight(3, "0"c)
        _Description.Value = Description
        _ProgramName.Value = ProgramName
        _StartDate.Value = CDate(StartDate)
        _StartTime.Value = startTime

        Dim NoRecs As Integer
        If RecordExists Then
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_LogDate.ColumnName, TodaysDate)
            Oasys3DB.SetColumnAndValueParameter(_SetNumber.ColumnName, SetId)
            Oasys3DB.SetColumnAndValueParameter(_TaskNumber.ColumnName, _TaskNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_Description.ColumnName, _Description.Value)
            Oasys3DB.SetColumnAndValueParameter(_ProgramName.ColumnName, _ProgramName.Value)
            Oasys3DB.SetColumnAndValueParameter(_StartDate.ColumnName, _StartDate.Value)
            Oasys3DB.SetColumnAndValueParameter(_StartTime.ColumnName, _StartTime.Value)
            Oasys3DB.SetWhereParameter(_LogDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _LogDate.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_TaskNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TaskNumber.Value)
            NoRecs = Oasys3DB.Update()
        Else
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
            Oasys3DB.SetColumnAndValueParameter(_LogDate.ColumnName, TodaysDate)
            Oasys3DB.SetColumnAndValueParameter(_SetNumber.ColumnName, SetId)
            Oasys3DB.SetColumnAndValueParameter(_TaskNumber.ColumnName, _TaskNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_Description.ColumnName, _Description.Value)
            Oasys3DB.SetColumnAndValueParameter(_ProgramName.ColumnName, _ProgramName.Value)
            Oasys3DB.SetColumnAndValueParameter(_StartDate.ColumnName, _StartDate.Value)
            Oasys3DB.SetColumnAndValueParameter(_StartTime.ColumnName, _StartTime.Value)
            NoRecs = Oasys3DB.Insert()
        End If

        If NoRecs > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function UpdateAbort(ByVal SetId As String, ByVal TaskNo As String, ByVal AbortDate As String, ByVal AbortTime As String) As Boolean

        Dim TodaysDate As Date = CDate(Oasys3DB.ExecuteSql("SELECT LDAT FROM SYSDAT WHERE FKEY='01'").Tables(0).Rows(0)(0))

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_AbortDate.ColumnName, CDate(AbortDate))
        Oasys3DB.SetColumnAndValueParameter(_AbortTime.ColumnName, AbortTime)
        Oasys3DB.SetWhereParameter(_LogDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, TodaysDate)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_SetNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, SetId)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_TaskNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, TaskNo)
        Return (Oasys3DB.Update = -1)

    End Function

    Public Function UpdateEnd(ByVal SetId As String, ByVal TaskNo As String, ByVal EndDate As String, ByVal EndTime As String) As Boolean

        Dim TodaysDate As Date = CDate(Oasys3DB.ExecuteSql("SELECT LDAT FROM SYSDAT WHERE FKEY='01'").Tables(0).Rows(0)(0))

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_EndDate.ColumnName, CDate(EndDate))
        Oasys3DB.SetColumnAndValueParameter(_EndTime.ColumnName, EndTime)
        Oasys3DB.SetWhereParameter(_LogDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, TodaysDate)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_SetNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, SetId)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_TaskNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, TaskNo)
        Return (Oasys3DB.Update() = -1)

    End Function

#End Region

End Class

