<Serializable()> Public Class cRtiDetail
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RtiDetail"
        BOFields.Add(_ID)
        BOFields.Add(_HeaderID)
        BOFields.Add(_TableToSend)
        BOFields.Add(_HeaderKeys)
        BOFields.Add(_DetailKeys)
        BOFields.Add(_IsActive)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cRtiDetail)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cRtiDetail)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cRtiDetail))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cRtiDetail(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _HeaderID As New ColField(Of Integer)("HeaderID", 0, "Header ID", True, False)
    Private _TableToSend As New ColField(Of String)("TableToSend", "", "Table To Send", False, False)
    Private _HeaderKeys As New ColField(Of String)("HeaderKeys", "", "Header Keys", False, False)
    Private _DetailKeys As New ColField(Of String)("DetailKeys", "", "Detail Keys", False, False)
    Private _IsActive As New ColField(Of Boolean)("IsActive", False, "Is Active", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property HeaderID() As ColField(Of Integer)
        Get
            Return _HeaderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _HeaderID = value
        End Set
    End Property
    Public Property TableToSend() As ColField(Of String)
        Get
            Return _TableToSend
        End Get
        Set(ByVal value As ColField(Of String))
            _TableToSend = value
        End Set
    End Property
    Public Property HeaderKeys() As ColField(Of String)
        Get
            Return _HeaderKeys
        End Get
        Set(ByVal value As ColField(Of String))
            _HeaderKeys = value
        End Set
    End Property
    Public Property DetailKeys() As ColField(Of String)
        Get
            Return _DetailKeys
        End Get
        Set(ByVal value As ColField(Of String))
            _DetailKeys = value
        End Set
    End Property
    Public Property IsActive() As ColField(Of Boolean)
        Get
            Return _IsActive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsActive = value
        End Set
    End Property

#End Region

End Class