<Serializable()> Public Class cRtiHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RtiHeader"
        BOFields.Add(_ID)
        BOFields.Add(_TableToSend)
        BOFields.Add(_DateField)
        BOFields.Add(_DateLastSent)
        BOFields.Add(_DateOffset)
        BOFields.Add(_IsActive)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cRtiHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cRtiHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cRtiHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cRtiHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _TableToSend As New ColField(Of String)("TableToSend", "", "Table To Send", False, False)
    Private _DateField As New ColField(Of String)("DateField", "", "Date Field", False, False)
    Private _DateLastSent As New ColField(Of Date)("DateLastSent", Nothing, "Date Last Sent", False, False)
    Private _DateOffset As New ColField(Of Integer)("DateOffset", 0, "Date Offset", False, False)
    Private _IsActive As New ColField(Of Boolean)("IsActive", False, "Is Active", False, False)

#End Region

#Region "Public Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property TableToSend() As ColField(Of String)
        Get
            Return _TableToSend
        End Get
        Set(ByVal value As ColField(Of String))
            _TableToSend = value
        End Set
    End Property
    Public Property DateField() As ColField(Of String)
        Get
            Return _DateField
        End Get
        Set(ByVal value As ColField(Of String))
            _DateField = value
        End Set
    End Property
    Public Property DateLastSent() As ColField(Of Date)
        Get
            Return _DateLastSent
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastSent = value
        End Set
    End Property
    Public Property DateOffset() As ColField(Of Integer)
        Get
            Return _DateOffset
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DateOffset = value
        End Set
    End Property
    Public Property IsActive() As ColField(Of Boolean)
        Get
            Return _IsActive
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsActive = value
        End Set
    End Property

#End Region

#Region "Events"
    Public Event UpdateProcess(ByVal Message As String)

#End Region

#Region "Entities"
    Private _RtiHeaders As List(Of cRtiHeader) = Nothing
    Private _RtiDetails As List(Of cRtiDetail) = Nothing

    Public Property RtiHeaders() As List(Of cRtiHeader)
        Get
            If _RtiHeaders Is Nothing Then
                LoadActiveRtiTables()
            End If
            Return _RtiHeaders
        End Get
        Set(ByVal value As List(Of cRtiHeader))
            _RtiHeaders = value
        End Set
    End Property
    Public Property RtiDetails() As List(Of cRtiDetail)
        Get
            If _RtiDetails Is Nothing Then
                Dim det As New cRtiDetail(Oasys3DB)
                det.AddLoadFilter(clsOasys3DB.eOperator.pEquals, det.HeaderID, _ID.Value)
                det.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                det.AddLoadFilter(clsOasys3DB.eOperator.pEquals, det.IsActive, True)
                _RtiDetails = det.LoadMatches
            End If
            Return _RtiDetails
        End Get
        Set(ByVal value As List(Of cRtiDetail))
            _RtiDetails = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all active Rti tables into RtiTables collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadActiveRtiTables()

        Try
            Dim rti As New cRtiHeader(Oasys3DB)
            rti.AddLoadFilter(clsOasys3DB.eOperator.pEquals, rti.IsActive, True)
            _RtiHeaders = rti.LoadMatches

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Creates an xml file of records for date in DateToSend field and saves in given xml location
    ''' </summary>
    ''' <param name="StoreID"></param>
    ''' <param name="XmlLocation"></param>
    ''' <remarks></remarks>
    Public Sub CreateXml(ByVal StartDate As Date, ByVal EndDate As Date, ByVal StoreID As String, ByVal XmlLocation As String)

        Try
            'get sql statement to perform
            Dim sql As New StringBuilder("SELECT * FROM """ & _TableToSend.Value.Trim & """")
            sql.Append(" WHERE """ & _DateField.Value.Trim & """ >= " & Oasys3DB.FormatValue(StartDate.AddDays(_DateOffset.Value)))
            sql.Append(" AND """ & _DateField.Value.Trim & """ <= " & Oasys3DB.FormatValue(EndDate.AddDays(_DateOffset.Value)))

            'get results into datatable
            Dim dt As DataTable = GetRecordsDataTable(_TableToSend.Value, sql.ToString, StoreID)
            If dt.Rows.Count > 0 Then SaveXmlFile(dt, StoreID, XmlLocation)

            'update rti table
            _DateLastSent.Value = EndDate
            SaveIfExists()

            'loop through each detail and get detail records
            For Each detail As cRtiDetail In RtiDetails
                'get key columns
                Dim tableName As String = detail.TableToSend.Value.Trim
                Dim headerKeys() As String = detail.HeaderKeys.Value.Split(","c)
                Dim detailKeys() As String = detail.DetailKeys.Value.Split(","c)
                If headerKeys.Count <> detailKeys.Count Then
                    Trace.WriteLine(My.Resources.ErrKeyCountDiffer, Me.GetType.ToString)
                    Continue For
                End If

                'get records for header into list of datatables
                Dim dtDetail As DataTable = Nothing
                For Each row As DataRow In dt.Rows
                    Dim join As String = " WHERE "

                    'generate sql statement
                    sql = New StringBuilder("SELECT * FROM """ & detail.TableToSend.Value.Trim & """")
                    For index As Integer = 0 To headerKeys.Count - 1
                        If index > 0 Then join = " AND "
                        sql.Append(join & """" & detailKeys(index) & """ = " & Oasys3DB.FormatValue(row(headerKeys(index))))
                    Next

                    'get records
                    Dim dtRecords As DataTable = GetRecordsDataTable(tableName, sql.ToString, StoreID)

                    'import into dt detail
                    If dtDetail Is Nothing Then
                        dtDetail = dtRecords
                    Else
                        For Each rowRecord As DataRow In dtRecords.Rows
                            dtDetail.ImportRow(rowRecord)
                        Next
                    End If
                Next

                'create xml file
                If dtDetail IsNot Nothing Then
                    If dtDetail.Rows.Count > 0 Then SaveXmlFile(dtDetail, StoreID, XmlLocation)
                End If

            Next

            RaiseEvent UpdateProcess(My.Resources.ProcessComplete)


        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function GetRecordsDataTable(ByVal tableName As String, ByVal sql As String, ByVal StoreID As String) As DataTable

        Try
            RaiseEvent UpdateProcess(My.Resources.GetRecords & Space(1) & tableName)

            Dim dt As DataTable = Oasys3DB.ExecuteSql(sql).Tables(0)
            dt.DataSet.DataSetName = "DocumentElement"
            dt.TableName = tableName
            dt.Columns.Add("StoreID")
            dt.Columns("StoreID").SetOrdinal(0)
            If dt.Columns.Contains("RTI") Then dt.Columns.Remove("RTI")
            If dt.Columns.Contains("RTICount") Then dt.Columns.Remove("RTICount")
            If dt.Columns.Contains("SPARE") Then dt.Columns.Remove("SPARE")

            For Each row As DataRow In dt.Rows
                row(0) = StoreID
            Next

            Return dt

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub SaveXmlFile(ByRef dt As DataTable, ByVal StoreID As String, ByVal XmlLocation As String)

        Try
            'check datatable for column names starting with number and rename with number on the end
            For Each col As DataColumn In dt.Columns
                Dim firstchar As String = col.ColumnName.Substring(0, 1)
                If IsNumeric(firstchar) Then
                    col.ColumnName = col.ColumnName.Remove(0, 1)
                    col.ColumnName &= firstchar
                End If
            Next

            RaiseEvent UpdateProcess(My.Resources.CreatingXml & Space(1) & dt.TableName)
            Dim filename As String = dt.TableName.PadRight(20, "~"c) & "." & StoreID & "." & Format(Now, "dd.MM.yy-HH.mm.ss") & ".xml"
            dt.WriteXml(My.Application.Info.DirectoryPath & "\" & filename)
            File.Move(My.Application.Info.DirectoryPath & "\" & filename, XmlLocation & filename)
        Catch ex As Exception

        End Try

    End Sub

#End Region

End Class