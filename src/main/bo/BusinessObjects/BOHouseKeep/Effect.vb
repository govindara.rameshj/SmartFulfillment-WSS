﻿Public Class Effect
    Private _item As String
    Private _action As String
    Private _value As String

    Public Property Item() As String
        Get
            Item = _item
        End Get
        Set(ByVal value As String)
            _item = value
        End Set
    End Property
    Public Property Value() As String
        Get
            Value = _value
        End Get
        Set(ByVal value As String)
            _value = value
        End Set
    End Property
    Public Property Action() As String
        Get
            Action = _action
        End Get
        Set(ByVal value As String)
            _action = value
        End Set
    End Property

    Public Sub New(ByVal item As String, ByVal action As String, ByVal value As String)
        _item = item
        _action = action
        _value = value
    End Sub


End Class