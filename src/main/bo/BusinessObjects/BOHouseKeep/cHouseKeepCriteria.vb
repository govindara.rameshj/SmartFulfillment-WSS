<Serializable()> Public Class cHouseKeepCriteria
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HouseKeepCriteria"
        BOFields.Add(_GroupID)
        BOFields.Add(_Sequence)
        BOFields.Add(_Connected)
        BOFields.Add(_BOTableName)
        BOFields.Add(_Field)
        BOFields.Add(_TestType)
        BOFields.Add(_TestValueOrTable)
        BOFields.Add(_TestField)
        BOFields.Add(_SelectGroup)
        BOFields.Add(_Join)
        BOFields.Add(_Level)
        BOFields.Add(_Period)
        BOFields.Add(_PeriodValue)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHouseKeepCriteria)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHouseKeepCriteria)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHouseKeepCriteria))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHouseKeepCriteria(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _GroupID As New ColField(Of Integer)("GroupID", 0, "Group ID", True, False)
    Private _Sequence As New ColField(Of Integer)("Sequence", 0, "Sequence", True, False)
    Private _Connected As New ColField(Of String)("Connected", "", "Connected", False, False)
    Private _BOTableName As New ColField(Of String)("BOTableName", "", "BO Table Name", False, False)
    Private _Field As New ColField(Of String)("Field", "", "Field", False, False)
    Private _TestType As New ColField(Of String)("TestType", "", "Test Type", False, False)
    Private _TestValueOrTable As New ColField(Of String)("TestValueOrTable", "", "Test Value or Table", False, False)
    Private _TestField As New ColField(Of String)("TestField", "", "Test Field", False, False)
    Private _SelectGroup As New ColField(Of Integer)("SelectGroup", 0, "Select Group", False, False)
    Private _Join As New ColField(Of String)("WhereJoin", "0", "Where Join", False, False)
    Private _Level As New ColField(Of String)("AccessLevel", "", "Access Level", False, False)
    Private _Period As New ColField(Of String)("PeriodType", "", "Period Type", False, False)
    Private _PeriodValue As New ColField(Of Integer)("PeriodValue", 0, "Period Value", False, False)

#End Region

#Region "Field Properties"

    Public Property GroupID() As ColField(Of Integer)
        Get
            GroupID = _GroupID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _GroupID = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of Integer)
        Get
            Sequence = _Sequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Sequence = value
        End Set
    End Property
    Public Property Connected() As ColField(Of String)
        Get
            Connected = _Connected
        End Get
        Set(ByVal value As ColField(Of String))
            _Connected = value
        End Set
    End Property
    Public Property BOTableName() As ColField(Of String)
        Get
            BOTableName = _BOTableName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOTableName = value
        End Set
    End Property
    Public Property Field() As ColField(Of String)
        Get
            Field = _Field
        End Get
        Set(ByVal value As ColField(Of String))
            _Field = value
        End Set
    End Property
    Public Property TestType() As ColField(Of String)
        Get
            TestType = _TestType
        End Get
        Set(ByVal value As ColField(Of String))
            _TestType = value
        End Set
    End Property
    Public Property TestValueOrTable() As ColField(Of String)
        Get
            TestValueOrTable = _TestValueOrTable
        End Get
        Set(ByVal value As ColField(Of String))
            _TestValueOrTable = value
        End Set
    End Property
    Public Property TestField() As ColField(Of String)
        Get
            TestField = _TestField
        End Get
        Set(ByVal value As ColField(Of String))
            _TestField = value
        End Set
    End Property
    Public Property SelectGroup() As ColField(Of Integer)
        Get
            SelectGroup = _SelectGroup
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SelectGroup = value
        End Set
    End Property
    Public Property Join() As ColField(Of String)
        Get
            Join = _Join
        End Get
        Set(ByVal value As ColField(Of String))
            _Join = value
        End Set
    End Property
    Public Property Level() As ColField(Of String)
        Get
            Level = _Level
        End Get
        Set(ByVal value As ColField(Of String))
            _Level = value
        End Set
    End Property
    Public Property Period() As ColField(Of String)
        Get
            Period = _Period
        End Get
        Set(ByVal value As ColField(Of String))
            _Period = value
        End Set
    End Property
    Public Property PeriodValue() As ColField(Of Integer)
        Get
            PeriodValue = _PeriodValue
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodValue = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function GetCriteria(ByVal GroupID As Integer) As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        Me.SortBy(Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Public Function GetCriteriaSelectGroup(ByVal GroupID As Integer, ByVal SelectGroup As Integer) As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        Me.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._SelectGroup, SelectGroup)
        Me.SortBy(Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Function DeleteCriteria(ByVal GroupID As Integer, ByVal sequence As Integer) As Boolean

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._Sequence, sequence)
        LoadMatches()
        Return Me.Delete()

    End Function

    Public Function TableExists(ByVal Name As String) As Boolean
        Dim sql As String = String.Empty
        Dim sql1 As String = "select * from information_schema.tables where TableName = '"
        Dim sql2 As String = "select * from x$file where xf$name = '"
        Dim ds As DataSet

        Select Case Oasys3DB.DatabaseType
            Case Is = "Pervasive"
                sql = sql2
            Case Is = "MicrosoftSQL"
                sql = sql1
        End Select


        sql = sql & Name & "'"
        ds = Oasys3DB.ExecuteSql(sql)
        If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function FieldExists(ByVal TableName As String, ByVal fieldname As String) As Boolean
        Dim sql As String = String.Empty
        Dim sql1 As String = "select * from information_schema.columns where TableName = '"
        Dim sql2 As String = "select * from X$Field where xe$file = (select xf$Id from x$file where xf$name = '" '
        Dim ds As DataSet
        Select Case Oasys3DB.DatabaseType
            Case Is = "Pervasive"
                sql = sql2 & TableName & "') and xe$Name = '" & fieldname & "'"
            Case Is = "MicrosoftSQL"
                sql = sql & TableName & "' and column_name = '" & fieldname & "'"
        End Select

        ds = Oasys3DB.ExecuteSql(sql)
        If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function UpdateRecord(ByVal GroupID As Integer, ByVal Sequence As Integer, ByVal connected As String, ByVal tablename As String, _
                                 ByVal field As String, ByVal testtype As String, ByVal TablenameorField As String, ByVal testfield As String, _
                                 ByVal selectGroup As Integer, ByVal join As String, ByVal strLevel As String, ByVal strPeriod As String, ByVal intPeriodValue As Integer) As Boolean
        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._Sequence, Sequence)
        LoadMatches()

        'move data to record
        _Connected.Value = connected
        _BOTableName.Value = tablename
        _Field.Value = field
        _TestType.Value = testtype
        _TestValueOrTable.Value = TablenameorField
        _TestField.Value = testfield
        _SelectGroup.Value = selectGroup
        _Join.Value = join
        _Level.Value = strLevel
        _Period.Value = strPeriod
        _PeriodValue.Value = intPeriodValue


        Me.SaveIfExists()

    End Function

    Public Function AddRecord(ByVal GroupID As String, ByVal Sequence As Integer, ByVal connected As String, ByVal tablename As String, _
                                 ByVal field As String, ByVal testtype As String, ByVal TablenameorField As String, ByVal testfield As String, _
                                 ByVal selectGroup As Integer, ByVal join As String, ByVal strLevel As String, ByVal strPeriod As String, ByVal intPeriodValue As Integer) As Boolean
        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, CInt(GroupID))
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._Sequence, Sequence)
        LoadMatches()

        'move data to record
        _GroupID.Value = CInt(GroupID)
        _Sequence.Value = Sequence
        _Connected.Value = connected
        _BOTableName.Value = tablename
        _Field.Value = field
        _TestType.Value = testtype
        _TestValueOrTable.Value = TablenameorField
        _TestField.Value = testfield
        _SelectGroup.Value = selectGroup
        _Join.Value = join
        _Level.Value = strLevel
        _Period.Value = strPeriod
        _PeriodValue.Value = intPeriodValue

        Me.SaveIfNew()

    End Function
    Public Function GetNextSequence(ByVal GroupID As Integer) As Integer
        Dim maxSeqNo As Integer
        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.GroupID, GroupID)

        If Me.LoadMatches.Count > 0 Then
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.GroupID, GroupID)
            Me.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, Me.Sequence, "MaxSeqNo")
            maxSeqNo = CInt(Me.GetAggregateField())
            GetNextSequence = maxSeqNo
        Else
            GetNextSequence = 0
        End If



    End Function
#End Region

End Class

