<Serializable()> Public Class cHouseKeepGroup
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HouseKeepGroup"
        BOFields.Add(_ID)
        BOFields.Add(_Name)
        BOFields.Add(_BOName)
        BOFields.Add(_BOClass)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHouseKeepGroup)

        LoadBORecords(count)

        Dim col As New List(Of cHouseKeepGroup)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cHouseKeepGroup))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHouseKeepGroup(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _Name As New ColField(Of String)("Name", "", "Name", False, False)
    Private _BOName As New ColField(Of String)("BOName", "", "BOName", False, False)
    Private _BOClass As New ColField(Of String)("BOClass", "", "BOClass", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Name() As ColField(Of String)
        Get
            Name = _Name
        End Get
        Set(ByVal value As ColField(Of String))
            _Name = value
        End Set
    End Property
    Public Property BOName() As ColField(Of String)
        Get
            BOName = _BOName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOName = value
        End Set
    End Property
    Public Property BOClass() As ColField(Of String)
        Get
            BOClass = _BOClass
        End Get
        Set(ByVal value As ColField(Of String))
            _BOClass = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function ListGroups() As DataTable

        Try
            Oasys3DB.ClearAllParameters()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, _Name, String.Empty)

            Dim ds As DataSet = Me.GetSQLSelectDataSet
            If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetGroup(ByVal GroupID As Integer) As DataRow
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._ID, GroupID)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function SaveVariables(ByVal intGroupId As Integer, ByVal strBOName As String, ByVal strClassName As String) As Boolean
        ' get the record for updating
        'Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.mHseKeepID, intGroupId)
        '  Me.LoadMatches()

        '' having found the record move values to the fields
        'Me.mBOName.Value = strBOName
        'Me.mBOClassName.Value = strClassName

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_BOName.ColumnName, strBOName)
        Oasys3DB.SetColumnAndValueParameter(_BOClass.ColumnName, strClassName)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, intGroupId)

        ' save the data to the record 
        Oasys3DB.Update()

        Return True
    End Function

    Public Function AddGroup(ByVal GroupName As String) As Boolean
        ' get the record for updating
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._Name, GroupName)
        Me.LoadMatches()

        Me._Name.Value = GroupName

        ' save the data to the record 
        Me.SaveIfNew()
        Return True
    End Function



    Public Sub HouseKeeping(ByVal GroupId As Integer)
        Dim HKCri As New cHouseKeepCriteria(Me.Oasys3DB)
        Dim lsCri As List(Of cHouseKeepCriteria)
        Dim HKGroup As New cHouseKeepGroup(Me.Oasys3DB)
        Dim lsBO As List(Of cBaseClass)
        Dim value As String = String.Empty
        Dim Name As String = String.Empty
        Dim Compare As String = String.Empty
        Dim SelectGroup As Integer = 0
        Dim blnExist As Boolean = False
        Dim intMaxGroup As Integer = 0
        Dim strValue As String = String.Empty
        Dim dteHorizionDate As Date

        'create the Business Object 
        HKGroup.ClearLoadFilter()
        HKGroup.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKGroup.ID, GroupId)
        If HKGroup.LoadMatches(1).Count > 0 Then
            ' on first HKCriBO find the name and class from the tableXref
            'create the Business Object
            Dim BOObject = CType(Activator.CreateInstanceFrom(HKGroup.BOName.Value & ".dll", HKGroup.BOName.Value & "." & HKGroup.BOClass.Value, True, Reflection.BindingFlags.Default, Nothing, New Object() {Oasys3DB}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysDBBO.cBaseClass)

            ' Find the number of SelectGroups  
            HKCri.ClearLoadFilter()
            HKCri.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKCri.GroupID, GroupId)
            HKCri.AddAggregateField(Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, HKCri.SelectGroup, "MaxLevel")
            intMaxGroup = CInt(HKCri.GetAggregateField())

            ' Get all the business objects for the SelectGroup = 0 
            HKCri.ClearLoadFilter()
            HKCri.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKCri.GroupID, GroupId)
            HKCri.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            HKCri.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKCri.SelectGroup, 0)
            HKCri.SortBy(HKCri.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            lsCri = HKCri.LoadMatches()

            Me.ClearLoadFilter()
            'process each business object
            For Each HKCriBO As cHouseKeepCriteria In lsCri

                'Set up the JoinLoadFilter using the "AND" and "OR"
                Select Case Trim(HKCriBO.Join.Value)
                    Case "OR"
                        BOObject.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                    Case "AND"
                        BOObject.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                End Select

                If Not String.IsNullOrEmpty(HKCriBO.Period.Value) Then
                    Select Case Trim(HKCriBO.Period.Value)
                        Case "Days"
                            dteHorizionDate = DateAdd(DateInterval.Day, HKCriBO.PeriodValue.Value * -1, Now)
                        Case "Weeks"
                            dteHorizionDate = DateAdd(DateInterval.Day, HKCriBO.PeriodValue.Value * 7 * -1, Now)
                        Case "Months"
                            dteHorizionDate = DateAdd(DateInterval.Month, HKCriBO.PeriodValue.Value * -1, Now)
                        Case "Years"
                            dteHorizionDate = DateAdd(DateInterval.Year, HKCriBO.PeriodValue.Value * -1, Now)
                    End Select
                    strValue = dteHorizionDate.ToString("yyyy-MM-dd")
                Else
                    Select Case Trim(HKCriBO.TestValueOrTable.Value)
                        Case "#TODAY"
                            strValue = Today.ToString("yyyy-MM-dd")
                        Case "#NULL"
                            strValue = Nothing
                        Case Else
                            'set value
                            strValue = Trim(HKCriBO.TestValueOrTable.Value)
                    End Select
                End If

                'Set up the AddLoadFilter"
                BOObject.AddUntypedLoadFilter(ConvertOperator(Trim(HKCriBO.TestType.Value)), BOObject.GetProperty(Trim(HKCriBO.Field.Value)), strValue)
            Next

            lsBO = BOObject.LoadCTSMatches()
            If lsBO.Count > 0 Then
                ' For each Business Object process the other levels
                For Each BOItem As cBaseClass In lsBO
                    blnExist = True
                    SelectGroup = 0
                    Do
                        SelectGroup = SelectGroup + 1
                        ' check if the data fails the Data Test
                        CheckSelectGroup(blnExist, GroupId, BOItem, SelectGroup)

                    Loop Until (blnExist = False) Or (SelectGroup >= intMaxGroup)
                    If blnExist = True Then
                        HKEffects(GroupId, BOItem)
                    End If

                Next
            End If
        End If
    End Sub

    Private Sub CheckSelectGroup(ByRef blnExist As Boolean, ByVal GroupId As Integer, ByVal BOItem As cBaseClass, ByVal selectGroupId As Integer)

        Dim lsCri As List(Of cHouseKeepCriteria)
        Dim Xref As New cHouseKeepXref(Oasys3DB)
        Dim BOName As String
        Dim BOSplit() As String
        Dim Noof As Integer = 0
        Dim tempCount As Integer = 0
        Dim ConstructorCount As Integer = 0
        Dim BOObject As cBaseClass = Nothing

        Dim blnFirst As Boolean = True
        Dim strValue As String = String.Empty
        Dim strField As String = String.Empty
        Dim BOProperty As IColField = Nothing
        Dim intNumBO As Integer
        Dim strConnected As String = String.Empty
        Dim blnResult As Boolean
        Dim dteHorizionDate As Date


        'Get all the Business Objects for this selectGroup
        Dim HKCri As New cHouseKeepCriteria(Oasys3DB)
        HKCri.ClearLoadFilter()
        HKCri.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKCri.GroupID, GroupId)
        HKCri.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        HKCri.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKCri.SelectGroup, selectGroupId)
        HKCri.SortBy(HKCri.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        lsCri = HKCri.LoadMatches()

        'clear the Connected  
        strConnected = String.Empty

        Try
            'If there are some business objects
            If lsCri.Count > 0 Then
                ' Process each busines object
                For Each HKCriBO As cHouseKeepCriteria In lsCri
                    If blnFirst = True Then
                        ' Store the Connected type
                        strConnected = HKCriBO.Connected.Value
                        ' on first HKCriBO find the name and class from the tableXref
                        BOName = Xref.GetBusinessObject(Trim(HKCriBO.TestValueOrTable.Value))
                        If BOName <> "|" Then
                            BOSplit = Split(BOName, "|")
                            'create the Business Object
                            BOObject = CType(Activator.CreateInstanceFrom(BOSplit(0) & ".dll", BOSplit(0) & "." & BOSplit(1), True, Reflection.BindingFlags.Default, Nothing, New Object() {Oasys3DB}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysDBBO.cBaseClass)
                        Else
                            Trace.WriteLine("Business object name not found for table")
                            blnExist = False
                        End If
                        blnFirst = False
                    End If

                    'Process the "and" and "OR"
                    Select Case Trim(HKCriBO.Join.Value)
                        Case "OR"
                            BOObject.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                        Case "AND"
                            BOObject.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    End Select

                    'Use the table name to decide which BO to get the property from"
                    If TableName = HKCriBO.BOTableName.Value Then
                        strValue = CStr(BOItem.GetProperty(HKCriBO.Field.Value).UntypedValue)
                        BOProperty = BOObject.GetProperty(HKCriBO.TestField.Value)
                    Else
                        BOProperty = BOObject.GetProperty(HKCriBO.Field.Value)
                        'Calculate horizion date and set value 
                        If Not String.IsNullOrEmpty(HKCriBO.Period.Value) Then
                            Select Case Trim(HKCriBO.Period.Value)
                                Case "Days"
                                    dteHorizionDate = DateAdd(DateInterval.Day, HKCriBO.PeriodValue.Value * -1, Now)
                                Case "Weeks"
                                    dteHorizionDate = DateAdd(DateInterval.Day, HKCriBO.PeriodValue.Value * 7 * -1, Now)
                                Case "Months"
                                    dteHorizionDate = DateAdd(DateInterval.Month, HKCriBO.PeriodValue.Value * -1, Now)
                                Case "Years"
                                    dteHorizionDate = DateAdd(DateInterval.Year, HKCriBO.PeriodValue.Value * -1, Now)
                            End Select
                            strValue = dteHorizionDate.ToString("yyyy-MM-dd")
                        Else
                            Select Case Trim(HKCriBO.TestValueOrTable.Value)
                                Case "#TODAY"
                                    strValue = Today.ToString("yyyy-MM-dd")
                                Case "#NULL"
                                    strValue = Nothing
                                Case Else
                                    'set value
                                    strValue = HKCriBO.TestValueOrTable.Value
                            End Select
                        End If
                    End If

                    ' add 
                    BOObject.AddUntypedLoadFilter(ConvertOperator(Trim(HKCriBO.TestType.Value)), BOProperty, strValue)
                Next 'HKCriBO 
                ' get the Business Objects
                intNumBO = BOObject.LoadCTSMatches(1).Count
                ' Return result of the test
                If strConnected.EndsWith("NOT EXISTS") Then
                    If intNumBO = 0 Then
                        blnResult = True
                    Else
                        blnResult = False
                    End If
                Else
                    '"EXISTS"
                    If intNumBO = 0 Then
                        blnResult = False
                    Else
                        blnResult = True
                    End If
                End If
                ' Now Work out the final result 
                If strConnected.StartsWith("OR") Then
                    blnExist = blnExist Or blnResult
                Else
                    blnExist = blnExist And blnResult
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub HKEffects(ByVal GroupId As Integer, ByVal BusObj As cBaseClass)

        HKEffectUpdate(GroupId, BusObj)
        HKEffectPurge(GroupId, BusObj)

    End Sub

    Public Sub HKEffectUpdate(ByVal GroupId As Integer, ByVal BusObj As cBaseClass)
        Dim HKEff As New cHouseKeepEffects(Oasys3DB)
        Dim lsEff As List(Of cHouseKeepEffects)
        Dim TableField(2) As String
        Dim BOProperty As IColField = Nothing
        Dim strValue As String = String.Empty

        ' Get all the business objects for the groupId and update
        HKEff.ClearLoadFilter()
        HKEff.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKEff.GroupID, GroupId)
        HKEff.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        HKEff.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HKEff.Action, "Update")
        HKEff.SortBy(HKEff.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        lsEff = HKEff.LoadMatches()

        ' update the database if BOs found 
        If lsEff.Count > 0 Then
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            BusObj.SetDBKeys()

            For Each HKEffBO As cHouseKeepEffects In lsEff
                ' update selected 
                If HKEffBO.Action.Value = "Update" Then

                    'split the into table an field
                    TableField = Split(HKEffBO.BOTableName.Value, ":")
                    'Get the property using the field name 
                    If TableField(0) = TableName Then
                        BOProperty = BusObj.GetProperty(TableField(1))
                    End If

                    'Set up value  
                    If HKEffBO.Value.Value = "#Today" Then
                        strValue = Today.ToString("yyyy-MM-dd")
                    Else
                        strValue = HKEffBO.Value.Value
                    End If
                    ' set the field and value 
                    Oasys3DB.SetColumnAndValueParameter(BOProperty.ColumnName, strValue)
                End If
            Next

            Oasys3DB.Update()
        End If

    End Sub

    Private Sub HKEffectPurge(ByVal GroupId As Integer, ByVal BusObj As cBaseClass)

        Dim keepEffects As New cHouseKeepEffects(Oasys3DB)
        Dim keepGroup As New cHouseKeepGroup(Oasys3DB)

        Dim effects As List(Of cHouseKeepEffects)
        Dim tempCount As Integer = 0
        Dim ConstructorCount As Integer = 0
        Dim ValueFields() As String

        ReDim ValueFields(0)

        ' Get all the business objects for the groupId and Purge
        keepEffects.ClearLoadFilter()
        keepEffects.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, keepEffects.GroupID, GroupId)
        keepEffects.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        keepEffects.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, keepEffects.Action, "Purge")
        keepEffects.SortBy(keepEffects.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        effects = keepEffects.LoadMatches()


        If effects.Count > 0 Then
            'Delete the PartCode on all the tables listed in Effects
            For Each HKEffBO As cHouseKeepEffects In effects
                Dim TableField() As String = Split(HKEffBO.BOTableName.Value, ":")
                Dim Fields() As String = Split(TableField(1), ";")
                Dim Value() As String = Split(HKEffBO.Value.Value, ":")
                ValueFields = Split(Value(1), ";")
                Dim Xref As New cHouseKeepXref(Oasys3DB)

                Dim BOName As String = Xref.GetBusinessObject(Trim(TableField(0)))
                If BOName <> "|" Then
                    Dim BOSplit() As String = BOName.Split("|"c)
                    Dim BOObject = CType(Activator.CreateInstanceFrom(Trim(BOSplit(0)) & ".dll", Trim(BOSplit(0)) & "." & Trim(BOSplit(1)), True, Reflection.BindingFlags.Default, Nothing, New Object() {Oasys3DB}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysDBBO.cBaseClass)
                    BOObject.AddUntypedLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOObject.GetProperty(Trim(Fields(0))), BusObj.GetProperty(Trim(ValueFields(0))).UntypedValue)

                    For count As Integer = 1 To UBound(ValueFields)
                        BOObject.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        BOObject.AddUntypedLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOObject.GetProperty(Trim(Fields(count))), BusObj.GetProperty(Trim(ValueFields(count))).UntypedValue)
                    Next

                    BOObject.HKDelete()
                Else
                    Trace.WriteLine("Business object name not found for table")
                End If
            Next
        End If

        'Delete the record on the passed business object
        BusObj.AddUntypedLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BusObj.GetProperty(Trim(ValueFields(0))), BusObj.GetProperty(Trim(ValueFields(0))).UntypedValue)

        'allow for multiple fields
        For count As Integer = 1 To UBound(ValueFields)
            BusObj.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            BusObj.AddUntypedLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BusObj.GetProperty(Trim(ValueFields(count))), BusObj.GetProperty(Trim(ValueFields(count))).UntypedValue)
        Next

        BusObj.HKDelete()

    End Sub

    Private Function ConvertOperator(ByVal theOperator As String) As Oasys3.DB.clsOasys3DB.eOperator
        Select Case theOperator
            Case "="
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pEquals
            Case "<>"
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pNotEquals
            Case "<"
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pLessThan
            Case ">"
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan
            Case ">="
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals
            Case "<="
                ConvertOperator = Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals
        End Select
    End Function

#End Region

End Class

