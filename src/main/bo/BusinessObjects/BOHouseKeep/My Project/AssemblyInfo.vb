﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("BOHseKeepCriteria")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TP-Wickes")> 
<Assembly: AssemblyProduct("BOHseKeepCriteria")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("5b9f8e7f-e3cf-4f3d-825d-5c1f04011259")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.0")> 
<Assembly: AssemblyFileVersion("3.0.0.0")> 
