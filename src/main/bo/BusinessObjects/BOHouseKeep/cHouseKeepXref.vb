﻿<Serializable()> Public Class cHouseKeepXref
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HouseKeepXref"
        BOFields.Add(_ID)
        BOFields.Add(_BOTableName)
        BOFields.Add(_BOName)
        BOFields.Add(_BOClass)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHouseKeepXref)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHouseKeepXref)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHouseKeepXref))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHouseKeepXref(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _BOTableName As New ColField(Of String)("BOTableName", "", "BO Table", False, False)
    Private _BOName As New ColField(Of String)("BOName", "", "BO Name", False, False)
    Private _BOClass As New ColField(Of String)("BOClass", "", "BOClass", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property BOTableName() As ColField(Of String)
        Get
            BOTableName = _BOTableName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOTableName = value
        End Set
    End Property
    Public Property BOName() As ColField(Of String)
        Get
            BOName = _BOName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOName = value
        End Set
    End Property
    Public Property BOClass() As ColField(Of String)
        Get
            BOClass = _BOClass
        End Get
        Set(ByVal value As ColField(Of String))
            _BOClass = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetBusinessObject(ByVal BOTableName As String) As String

        Try
            ClearLoadFilter()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _BOTableName, BOTableName)
            If LoadMatches.Count > 0 Then Return _BOName.Value & "|" & _BOClass.Value

            Return String.Empty

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

End Class
