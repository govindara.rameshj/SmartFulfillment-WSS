<Serializable()> Public Class cHouseKeepEffects
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HouseKeepEffects"
        BOFields.Add(_GroupID)
        BOFields.Add(_BOTableName)
        BOFields.Add(_Sequence)
        BOFields.Add(_Action)
        BOFields.Add(_Value)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHouseKeepEffects)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHouseKeepEffects)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHouseKeepEffects))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHouseKeepEffects(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _GroupID As New ColField(Of Integer)("GroupID", 0, "Group ID", True, False)
    Private _BOTableName As New ColField(Of String)("BOTableName", "", "BO Table Name", True, False)
    Private _Sequence As New ColField(Of Integer)("Sequence", 0, "Sequence", False, False)
    Private _Action As New ColField(Of String)("Action", "", "Action", False, False)
    Private _Value As New ColField(Of String)("ActionValue", "", "ActionValue", False, False)

#End Region

#Region "Field Properties"

    Public Property GroupID() As ColField(Of Integer)
        Get
            GroupID = _GroupID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _GroupID = value
        End Set
    End Property
    Public Property BOTableName() As ColField(Of String)
        Get
            BOTableName = _BOTableName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOTableName = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of Integer)
        Get
            Sequence = _Sequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Sequence = value
        End Set
    End Property
    Public Property Action() As ColField(Of String)
        Get
            Action = _Action
        End Get
        Set(ByVal value As ColField(Of String))
            _Action = value
        End Set
    End Property
    Public Property Value() As ColField(Of String)
        Get
            Value = _Value
        End Get
        Set(ByVal value As ColField(Of String))
            _Value = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function GetTables(ByVal GroupID As Integer) As DataTable

        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        Me.SortBy(Me._Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Function TableExists(ByVal Name As String) As Boolean
        Dim sql As String = ""
        Dim sql1 As String = "select * from information_schema.tables where TableName = '"
        Dim sql2 As String = "select * from x$file where xf$name = '"
        Dim ds As DataSet

        Select Case Oasys3DB.DatabaseType
            Case Is = "Pervasive"
                sql = sql2
            Case Is = "MicrosoftSQL"
                sql = sql1
        End Select

        sql = sql & Name & "'"
        ds = Oasys3DB.ExecuteSql(sql)
        If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function RemoveTable(ByVal GroupID As Integer, ByVal Sequence As String) As Boolean
        Oasys3DB.ClearAllParameters()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._GroupID, GroupID)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._Sequence, CInt(Sequence))
        LoadMatches()
        Return Delete()

    End Function
    Public Function AddTable(ByVal GroupID As Integer, ByVal TableName As String, ByVal seqNo As Integer, ByVal strAction As String, ByVal strValue As String) As Boolean
        Oasys3DB.ClearAllParameters()
        Me._GroupID.Value = GroupID
        Me._BOTableName.Value = TableName
        Me._Sequence.Value = seqNo
        Me._Action.Value = strAction
        Me._Value.Value = strValue

        Return SaveIfNew()

    End Function
    Public Function UpdateTable(ByVal GroupID As Integer, ByVal TableName As String, ByVal seqNo As Integer, ByVal strAction As String, ByVal strValue As String) As Boolean
        Oasys3DB.ClearAllParameters()
        Me._GroupID.Value = GroupID
        Me._BOTableName.Value = TableName
        Me._Sequence.Value = seqNo
        Me._Action.Value = strAction
        Me._Value.Value = strValue

        Return SaveIfExists()

    End Function

    Public Function GetNextSequence(ByVal GroupID As Integer) As Integer

        Dim maxSeqNo As Integer
        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.GroupID, GroupID)

        If Me.LoadMatches.Count > 0 Then
            Oasys3DB.ClearAllParameters()
            Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me.GroupID, GroupID)
            Me.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, Me.Sequence, "MaxSeqNo")
            maxSeqNo = CInt(Me.GetAggregateField())
            GetNextSequence = maxSeqNo
        Else
            GetNextSequence = 0
        End If
    End Function
#End Region

End Class
