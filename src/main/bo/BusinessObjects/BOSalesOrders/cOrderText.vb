﻿<Serializable()> Public Class cOrderText
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CORTXT"
        BOFields.Add(_OrderNumber)
        BOFields.Add(_OrderType)
        BOFields.Add(_OrderLine)
        BOFields.Add(_Text)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cOrderText)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cOrderText)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cOrderText))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cOrderText(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _OrderNumber As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Number")
    Private _OrderType As New ColField(Of String)("TYPE", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Type")
    Private _OrderLine As New ColField(Of String)("LINE", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Line No")
    Private _Text As New ColField(Of String)("TEXT", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Comment")

#End Region

#Region "Field Properties"

    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property
    Public Property OrderType() As ColField(Of String)
        Get
            OrderType = _OrderType
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderType = value
        End Set
    End Property
    Public Property OrderLine() As ColField(Of String)
        Get
            OrderLine = _OrderLine
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderLine = value
        End Set
    End Property
    Public Property Text() As ColField(Of String)
        Get
            Text = _Text
        End Get
        Set(ByVal value As ColField(Of String))
            _Text = value
        End Set
    End Property

#End Region

End Class
