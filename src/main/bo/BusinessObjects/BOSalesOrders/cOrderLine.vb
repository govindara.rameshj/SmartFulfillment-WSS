﻿<Serializable()> Public Class cOrderLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CORLIN"
        BOFields.Add(_OrderNumber)
        BOFields.Add(_OrderLineNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_UnitsOrdered)
        BOFields.Add(_UnitsTaken)
        BOFields.Add(_UnitsRefunded)
        BOFields.Add(_LineWeight)
        BOFields.Add(_LineVolume)
        BOFields.Add(_PriceOverrideCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cOrderLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cOrderLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cOrderLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cOrderLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _OrderNumber As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Number")
    Private _OrderLineNumber As New ColField(Of String)("LINE", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Line No")
    Private _SkuNumber As New ColField(Of String)("SKUN", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Part Code")
    Private _UnitsOrdered As New ColField(Of Decimal)("QTYO", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quantity Ordered")
    Private _UnitsTaken As New ColField(Of Decimal)("QTYT", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Qty Taken")
    Private _UnitsRefunded As New ColField(Of Decimal)("QTYR", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Qty Returned")
    Private _LineWeight As New ColField(Of Decimal)("WGHT", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Weight")
    Private _LineVolume As New ColField(Of Decimal)("VOLU", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Volume")
    Private _PriceOverrideCode As New ColField(Of Integer)("PORC", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Price Override Code")

#End Region

#Region "Field Properties"

    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property
    Public Property OrderLineNo() As ColField(Of String)
        Get
            OrderLineNo = _OrderLineNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderLineNumber = value
        End Set
    End Property
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property UnitsOrdered() As ColField(Of Decimal)
        Get
            UnitsOrdered = _UnitsOrdered
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsOrdered = value
        End Set
    End Property
    Public Property UnitsTaken() As ColField(Of Decimal)
        Get
            UnitsTaken = _UnitsTaken
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsTaken = value
        End Set
    End Property
    Public Property UnitsRefunded() As ColField(Of Decimal)
        Get
            UnitsRefunded = _UnitsRefunded
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsRefunded = value
        End Set
    End Property
    Public Property LineWeight() As ColField(Of Decimal)
        Get
            LineWeight = _LineWeight
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _LineWeight = value
        End Set
    End Property
    Public Property LineVolume() As ColField(Of Decimal)
        Get
            LineVolume = _LineVolume
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _LineVolume = value
        End Set
    End Property
    Public Property PriceOverrideCode() As ColField(Of Integer)
        Get
            PriceOverrideCode = _PriceOverrideCode
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PriceOverrideCode = value
        End Set
    End Property

#End Region

End Class
