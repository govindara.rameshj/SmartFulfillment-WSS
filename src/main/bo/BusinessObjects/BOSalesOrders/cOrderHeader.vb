﻿
<Serializable()> Public Class cOrderHeader
    Inherits cBaseClass

    Public _OrdHeader4 As New cOrderHeaderOther(Oasys3DB)

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CORHDR"
        BOFields.Add(_OrderNumber)
        BOFields.Add(_CustomerNumber)
        BOFields.Add(_OrderDate)
        BOFields.Add(_DeliveryDate)
        BOFields.Add(_Cancelled)
        BOFields.Add(_IsForDelivery)
        BOFields.Add(_DeliveryConfirmed)
        BOFields.Add(_NoTimeAmended)
        BOFields.Add(_DeliveryAddress1)
        BOFields.Add(_DeliveryAddress2)
        BOFields.Add(_DeliveryAddress3)
        BOFields.Add(_DeliveryAddress4)
        BOFields.Add(_TelephoneNumber)
        BOFields.Add(_IsPrinted)
        BOFields.Add(_NumberReprints)
        BOFields.Add(_RevisionNumber)
        BOFields.Add(_MerchandiseValue)
        BOFields.Add(_DeliveryCharge)
        BOFields.Add(_UnitsOrdered)
        BOFields.Add(_UnitsTaken)
        BOFields.Add(_UnitsRefunded)
        BOFields.Add(_OrderWeight)
        BOFields.Add(_OrderVolume)
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_RefundTransDate)
        BOFields.Add(_RefundTillID)
        BOFields.Add(_RefundTransNumber)
        BOFields.Add(_MobileNumber)
        BOFields.Add(_PostCode)
        BOFields.Add(_CustomerName)

    End Sub

#Region "Data Sets"

    Public Enum ColumnNames
        NUMB
        DATE1
        DELC
        CUST
        NAME
        ADDR1
        ADDR2
        ADDR3
        ADDR4
        POST
        PHON
        DELI
        SDAT
        STIL
        STRN
    End Enum

    Public Function Column(ByVal name As ColumnNames) As String
        Return name.ToString
    End Function

#End Region

    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cOrderHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cOrderHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cOrderHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cOrderHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _OrderNumber As New ColField(Of String)("NUMB", "", "Order Number", True, False)
    Private _CustomerNumber As New ColField(Of String)("CUST", "", "Customer Number", False, False)
    Private _OrderDate As New ColField(Of Date)("DATE1", Nothing, "Order Date", False, False)
    Private _DeliveryDate As New ColField(Of Date)("DELD", Nothing, "Delivery Date", False, False)
    Private _Cancelled As New ColField(Of String)("CANC", "", "Date Cancelled", False, False)
    Private _IsForDelivery As New ColField(Of Boolean)("DELI", False, "For Delivery", False, False)
    Private _DeliveryConfirmed As New ColField(Of Boolean)("DELC", False, "Delivery Confirmed", False, False)
    Private _NoTimeAmended As New ColField(Of String)("AMDT", "", "Times Amended", False, False)
    Private _DeliveryAddress1 As New ColField(Of String)("ADDR1", "", "Del Address 1", False, False)
    Private _DeliveryAddress2 As New ColField(Of String)("ADDR2", "", "Del Address 2", False, False)
    Private _DeliveryAddress3 As New ColField(Of String)("ADDR3", "", "Del Address 3", False, False)
    Private _DeliveryAddress4 As New ColField(Of String)("ADDR4", "", "Del Address 4", False, False)
    Private _TelephoneNumber As New ColField(Of String)("PHON", "", "Phone Number", False, False)
    Private _IsPrinted As New ColField(Of Boolean)("PRNT", False, "Printed", False, False)
    Private _NumberReprints As New ColField(Of Decimal)("RPRN", 0, "Number Reprints", False, False)
    Private _RevisionNumber As New ColField(Of Decimal)("REVI", 0, "Revision Number", False, False)
    Private _MerchandiseValue As New ColField(Of Decimal)("MVST", 0, "Merchandise Value", False, False, 2)
    Private _DeliveryCharge As New ColField(Of Decimal)("DCST", 0, "Delivery Charge", False, False, 2)
    Private _UnitsOrdered As New ColField(Of Decimal)("QTYO", 0, "Quantity Ordered", False, False)
    Private _UnitsTaken As New ColField(Of Decimal)("QTYT", 0, "Quantity Taken", False, False)
    Private _UnitsRefunded As New ColField(Of Decimal)("QTYR", 0, "Quantity Returned", False, False)
    Private _OrderWeight As New ColField(Of Decimal)("WGHT", 0, "Weight", False, False)
    Private _OrderVolume As New ColField(Of Decimal)("VOLU", 0, "Volume", False, False)
    Private _TranDate As New ColField(Of Date)("SDAT", Nothing, "Order Tran Date", False, False)
    Private _TillID As New ColField(Of String)("STIL", "", "Order Till ID", False, False)
    Private _TranNumber As New ColField(Of String)("STRN", "", "Order Tran Number", False, False)
    Private _RefundTransDate As New ColField(Of Date)("RDAT", Nothing, "Refund Tran Date", False, False)
    Private _RefundTillID As New ColField(Of String)("RTIL", "", "Refund Till ID", False, False)
    Private _RefundTransNumber As New ColField(Of String)("RTRN", "", "Refund Tran Number", False, False)
    Private _MobileNumber As New ColField(Of String)("MOBP", "", "Mobile Number", False, False)
    Private _PostCode As New ColField(Of String)("POST", "", "Post Code", False, False)
    Private _CustomerName As New ColField(Of String)("NAME", "", "Customer Name", False, False)

#End Region

#Region "Field Properties"

    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property

    Public Property CustomerNo() As ColField(Of String)
        Get
            CustomerNo = _CustomerNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerNumber = value
        End Set
    End Property

    Public Property OrderDate() As ColField(Of Date)
        Get
            OrderDate = _OrderDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _OrderDate = value
        End Set
    End Property

    Public Property DeliveryDate() As ColField(Of Date)
        Get
            DeliveryDate = _DeliveryDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DeliveryDate = value
        End Set
    End Property

    Public Property Cancelled() As ColField(Of String)
        Get
            Cancelled = _Cancelled
        End Get
        Set(ByVal value As ColField(Of String))
            _Cancelled = value
        End Set
    End Property

    Public Property IsForDelivery() As ColField(Of Boolean)
        Get
            IsForDelivery = _IsForDelivery
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsForDelivery = value
        End Set
    End Property

    Public Property DeliveryConfirmed() As ColField(Of Boolean)
        Get
            DeliveryConfirmed = _DeliveryConfirmed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DeliveryConfirmed = value
        End Set
    End Property

    Public Property NoTimeAmended() As ColField(Of String)
        Get
            NoTimeAmended = _NoTimeAmended
        End Get
        Set(ByVal value As ColField(Of String))
            _NoTimeAmended = value
        End Set
    End Property

    Public Property DeliveryAddress1() As ColField(Of String)
        Get
            DeliveryAddress1 = _DeliveryAddress1
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryAddress1 = value
        End Set
    End Property

    Public Property DeliveryAddress2() As ColField(Of String)
        Get
            DeliveryAddress2 = _DeliveryAddress2
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryAddress2 = value
        End Set
    End Property

    Public Property DeliveryAddress3() As ColField(Of String)
        Get
            DeliveryAddress3 = _DeliveryAddress3
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryAddress3 = value
        End Set
    End Property

    Public Property DeliveryAddress4() As ColField(Of String)
        Get
            DeliveryAddress4 = _DeliveryAddress4
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryAddress4 = value
        End Set
    End Property

    Public Property TelephoneNo() As ColField(Of String)
        Get
            TelephoneNo = _TelephoneNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TelephoneNumber = value
        End Set
    End Property
    Public Property Printed() As ColField(Of Boolean)
        Get
            Printed = _IsPrinted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsPrinted = value
        End Set
    End Property

    Public Property NoOfReprints() As ColField(Of Decimal)
        Get
            NoOfReprints = _NumberReprints
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NumberReprints = value
        End Set
    End Property

    Public Property RevisionNo() As ColField(Of Decimal)
        Get
            RevisionNo = _RevisionNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RevisionNumber = value
        End Set
    End Property

    Public Property MerchandiseValue() As ColField(Of Decimal)
        Get
            MerchandiseValue = _MerchandiseValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MerchandiseValue = value
        End Set
    End Property

    Public Property DeliveryCharge() As ColField(Of Decimal)
        Get
            DeliveryCharge = _DeliveryCharge
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DeliveryCharge = value
        End Set
    End Property

    Public Property UnitsOrdered() As ColField(Of Decimal)
        Get
            UnitsOrdered = _UnitsOrdered
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsOrdered = value
        End Set
    End Property

    Public Property UnitsTaken() As ColField(Of Decimal)
        Get
            UnitsTaken = _UnitsTaken
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsTaken = value
        End Set
    End Property

    Public Property UnitsRefunded() As ColField(Of Decimal)
        Get
            UnitsRefunded = _UnitsRefunded
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsRefunded = value
        End Set
    End Property

    Public Property OrderWeight() As ColField(Of Decimal)
        Get
            OrderWeight = _OrderWeight
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderWeight = value
        End Set
    End Property
    Public Property OrderVolume() As ColField(Of Decimal)
        Get
            OrderVolume = _OrderVolume
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderVolume = value
        End Set
    End Property

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property

    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property

    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property RefundTransDate() As ColField(Of Date)
        Get
            RefundTransDate = _RefundTransDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _RefundTransDate = value
        End Set
    End Property

    Public Property RefundTillID() As ColField(Of String)
        Get
            RefundTillID = _RefundTillID
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundTillID = value
        End Set
    End Property

    Public Property RefundTransNo() As ColField(Of String)
        Get
            RefundTransNo = _RefundTransNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundTransNumber = value
        End Set
    End Property
    Public Property MobileNo() As ColField(Of String)
        Get
            MobileNo = _MobileNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _MobileNumber = value
        End Set
    End Property

    Public Property PostCode() As ColField(Of String)
        Get
            PostCode = _PostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _PostCode = value
        End Set
    End Property

    Public Property CustomerName() As ColField(Of String)
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerName = value
        End Set
    End Property

#End Region

#Region "Functions"
    Public Function GetSpecificOrder(ByVal strOrderNum As String) As DataTable
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_OrderNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_OrderDate.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryConfirmed.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryDate.ColumnName)
            Oasys3DB.SetColumnParameter(_CustomerNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_CustomerName.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress1.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress2.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress3.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress4.ColumnName)
            Oasys3DB.SetColumnParameter(_PostCode.ColumnName)
            Oasys3DB.SetColumnParameter(_TelephoneNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_IsForDelivery.ColumnName)
            Oasys3DB.SetColumnParameter(_TillID.ColumnName)
            Oasys3DB.SetColumnParameter(_TranDate.ColumnName)
            Oasys3DB.SetColumnParameter(_TranNumber.ColumnName)
            Oasys3DB.SetWhereParameter(_OrderNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, strOrderNum)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)
            Return dt

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting an Order " & ex.Message, ex)
        End Try

    End Function

    Public Function GetOrdersTable(ByVal IsDespatched As Boolean) As DataTable

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_OrderNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_OrderDate.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryConfirmed.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryDate.ColumnName)
            Oasys3DB.SetColumnParameter(_CustomerNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_CustomerName.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress1.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress2.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress3.ColumnName)
            Oasys3DB.SetColumnParameter(_DeliveryAddress4.ColumnName)
            Oasys3DB.SetColumnParameter(_PostCode.ColumnName)
            Oasys3DB.SetColumnParameter(_TelephoneNumber.ColumnName)
            Oasys3DB.SetColumnParameter(_IsForDelivery.ColumnName)
            Oasys3DB.SetColumnParameter(_TillID.ColumnName)
            Oasys3DB.SetColumnParameter(_TranDate.ColumnName)
            Oasys3DB.SetColumnParameter(_TranNumber.ColumnName)
            Oasys3DB.SetWhereParameter(_IsForDelivery.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, IsDespatched)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)
            Return dt

        Catch ex As Exception
            Throw New Oasys3.DB.OasysDbException("Exception Getting Orders " & ex.Message, ex)
        End Try

    End Function

    Public Function LoadAnOrder(ByVal strOrderNo As String) As Boolean
        'Load the main Header also
        ClearLists()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _OrderNumber, strOrderNo)
        LoadMatches()
        'Load the other Header also
        _OrdHeader4.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _OrdHeader4.OrderNo, strOrderNo)
        _OrdHeader4.LoadMatches()
        If _OrderNumber.Value <> "" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ChangeDateForOrder(ByVal strOrderNum As String, ByVal dteNewDate As String) As Boolean
        Dim strSQL As String = "UPDATE CORHDR SET DELD='" & dteNewDate.ToString & "', DELC=0 WHERE NUMB='" & strOrderNum & "'"
        Oasys3DB.ExecuteSql(strSQL)
        strSQL = "UPDATE CORHDR4 SET DDAT=NULL WHERE NUMB='" & strOrderNum & "'"
        Oasys3DB.ExecuteSql(strSQL)
    End Function
#End Region

End Class

