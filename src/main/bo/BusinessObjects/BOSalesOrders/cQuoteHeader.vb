﻿<Serializable()> Public Class cQuoteHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "QUOHDR"
        BOFields.Add(_QuoteNumber)
        BOFields.Add(_CustomerNumber)
        BOFields.Add(_QuoteDate)
        BOFields.Add(_IsCancelled)
        BOFields.Add(_ExpiryDate)
        BOFields.Add(_IsSold)
        BOFields.Add(_IsForDelivery)
        BOFields.Add(_MerchandiseValue)
        BOFields.Add(_DeliveryCharge)
        BOFields.Add(_QuoteWeight)
        BOFields.Add(_QuoteVolume)
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_DiscountCardNumber)
        BOFields.Add(_FreeOfChargeSupv)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cQuoteHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cQuoteHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cQuoteHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cQuoteHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _QuoteNumber As New ColField(Of String)("NUMB", "000000", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote No")
    Private _CustomerNumber As New ColField(Of String)("CUST", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Customer ID")
    Private _QuoteDate As New ColField(Of Date)("DATE1", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Date")
    Private _IsCancelled As New ColField(Of Boolean)("CANC", False, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Cancelled")
    Private _ExpiryDate As New ColField(Of Date)("EXPD", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Expiry Date")
    Private _IsSold As New ColField(Of Boolean)("SOLD", False, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Accepted")
    Private _IsForDelivery As New ColField(Of Boolean)("DELI", False, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Delivery Reqd")
    Private _MerchandiseValue As New ColField(Of Decimal)("MVST", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Merchandise Value")
    Private _DeliveryCharge As New ColField(Of Decimal)("DCST", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Delivery Charge")
    Private _QuoteWeight As New ColField(Of Decimal)("WGHT", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Weight")
    Private _QuoteVolume As New ColField(Of Decimal)("VOLU", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Volume")
    Private _TranDate As New ColField(Of Date)("QDAT", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Tran Date")
    Private _TillID As New ColField(Of String)("QTIL", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Till ID")
    Private _TranNumber As New ColField(Of String)("QTRN", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Tran ID")
    Private _DiscountCardNumber As New ColField(Of String)("CARD_NO", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Discount Card No")
    Private _FreeOfChargeSupv As New ColField(Of String)("FOCDEL", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "FOC Del Supervisor")

#End Region

#Region "Field Properties"

    Public Property QuoteNo() As ColField(Of String)
        Get
            QuoteNo = _QuoteNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _QuoteNumber = value
        End Set
    End Property
    Public Property CustomerNo() As ColField(Of String)
        Get
            CustomerNo = _CustomerNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerNumber = value
        End Set
    End Property
    Public Property QuoteDate() As ColField(Of Date)
        Get
            QuoteDate = _QuoteDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _QuoteDate = value
        End Set
    End Property
    Public Property IsCancelled() As ColField(Of Boolean)
        Get
            IsCancelled = _IsCancelled
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsCancelled = value
        End Set
    End Property
    Public Property ExpiryDate() As ColField(Of Date)
        Get
            ExpiryDate = _ExpiryDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _ExpiryDate = value
        End Set
    End Property
    Public Property IsSold() As ColField(Of Boolean)
        Get
            IsSold = _IsSold
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsSold = value
        End Set
    End Property
    Public Property IsForDelivery() As ColField(Of Boolean)
        Get
            IsForDelivery = _IsForDelivery
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsForDelivery = value
        End Set
    End Property
    Public Property MerchandiseValue() As ColField(Of Decimal)
        Get
            MerchandiseValue = _MerchandiseValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MerchandiseValue = value
        End Set
    End Property
    Public Property DeliveryCharge() As ColField(Of Decimal)
        Get
            DeliveryCharge = _DeliveryCharge
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DeliveryCharge = value
        End Set
    End Property
    Public Property QuoteWeight() As ColField(Of Decimal)
        Get
            QuoteWeight = _QuoteWeight
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QuoteWeight = value
        End Set
    End Property
    Public Property QuoteVolume() As ColField(Of Decimal)
        Get
            QuoteVolume = _QuoteVolume
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QuoteVolume = value
        End Set
    End Property
    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property

#End Region

End Class

