﻿
<Serializable()> Public Class cOrderHeaderShownInUI
    Inherits cOrderHeader

    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
    End Sub

    Public Overrides Sub Start()

        MyBase.Start()
        TableName = "vwCORHDRFull"

        Dim _IsShownInUI As New ColField(Of Boolean)("SHOW_IN_UI", False, "Show In UI", False, False)
        AddStaticFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsShownInUI, True)
        JoinStaticFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)

    End Sub

End Class

