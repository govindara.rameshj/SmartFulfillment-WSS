﻿Imports OasysDBBO

<Serializable()> Public Class cOrderHeaderOther
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CORHDR4"
        BOFields.Add(_OrderNumber)
        BOFields.Add(_CreatedDate)
        BOFields.Add(_DeliveryDate)
        BOFields.Add(_CustomerName)
        BOFields.Add(_CustomerNumber)
        BOFields.Add(_DespatchDate)
        BOFields.Add(_OrderTakerID)
        BOFields.Add(_ManagerID)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cOrderHeaderOther)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cOrderHeaderOther)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cOrderHeaderOther))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cOrderHeaderOther(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _OrderNumber As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Number")
    Private _CreatedDate As New ColField(Of Date)("DATE1", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Date")
    Private _DeliveryDate As New ColField(Of Date)("DELD", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Delivery Date")
    Private _CustomerName As New ColField(Of String)("NAME", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Customer Name")
    Private _CustomerNumber As New ColField(Of String)("CUST", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Customer No")
    Private _DespatchDate As New ColField(Of Date)("DDAT", Nothing, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Despatch Date")
    Private _OrderTakerID As New ColField(Of String)("OEID", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Order Taker ID")
    Private _ManagerID As New ColField(Of String)("FDID", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Manager ID")

#End Region

#Region "Field Properties"

    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property

    Public Property CreatedDate() As ColField(Of Date)
        Get
            CreatedDate = _CreatedDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _CreatedDate = value
        End Set
    End Property

    Public Property DeliveryDate() As ColField(Of Date)
        Get
            DeliveryDate = _DeliveryDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DeliveryDate = value
        End Set
    End Property

    Public Property CustomerName() As ColField(Of String)
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerName = value
        End Set
    End Property

    Public Property CustomerNo() As ColField(Of String)
        Get
            CustomerNo = _CustomerNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerNumber = value
        End Set
    End Property

    Public Property DespatchDate() As ColField(Of Date)
        Get
            DespatchDate = _DespatchDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DespatchDate = value
        End Set
    End Property

    Public Property OrderTakerID() As ColField(Of String)
        Get
            OrderTakerID = _OrderTakerID
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderTakerID = value
        End Set
    End Property

    Public Property ManagerID() As ColField(Of String)
        Get
            ManagerID = _ManagerID
        End Get
        Set(ByVal value As ColField(Of String))
            _ManagerID = value
        End Set
    End Property

#End Region

End Class
