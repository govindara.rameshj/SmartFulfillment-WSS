﻿<Serializable()> Public Class cQuoteLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "QUOLIN"
        BOFields.Add(_QuoteNumber)
        BOFields.Add(_QuoteLineNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_UnitsQuoted)
        BOFields.Add(_LineWeight)
        BOFields.Add(_LineVolume)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cQuoteLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cQuoteLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cQuoteLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cQuoteLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _QuoteNumber As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quote Number")
    Private _QuoteLineNumber As New ColField(Of String)("LINE", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Line No")
    Private _SkuNumber As New ColField(Of String)("SKUN", "", False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Part Code")
    Private _UnitsQuoted As New ColField(Of Decimal)("QUAN", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Quantity")
    Private _LineWeight As New ColField(Of Decimal)("WGHT", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Line Weight")
    Private _LineVolume As New ColField(Of Decimal)("VOLU", 0, False, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Line Volume")

#End Region

#Region "Field Properties"

    Public Property QuoteNo() As ColField(Of String)
        Get
            QuoteNo = _QuoteNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _QuoteNumber = value
        End Set
    End Property
    Public Property QuoteLineNo() As ColField(Of String)
        Get
            QuoteLineNo = _QuoteLineNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _QuoteLineNumber = value
        End Set
    End Property
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property UnitsQuoted() As ColField(Of Decimal)
        Get
            UnitsQuoted = _UnitsQuoted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnitsQuoted = value
        End Set
    End Property
    Public Property LineWeight() As ColField(Of Decimal)
        Get
            LineWeight = _LineWeight
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _LineWeight = value
        End Set
    End Property
    Public Property LineVolume() As ColField(Of Decimal)
        Get
            LineVolume = _LineVolume
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _LineVolume = value
        End Set
    End Property

#End Region

End Class
