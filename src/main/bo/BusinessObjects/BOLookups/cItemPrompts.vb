﻿<Serializable()> Public Class cItemPrompts
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ItemPrompts"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_PromptGroupID)
        BOFields.Add(_Sequence)
        BOFields.Add(_SentToStores)
        BOFields.Add(_DateDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cItemPrompts)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cItemPrompts)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cItemPrompts))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cItemPrompts(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "", "Sku Number", True, False)
    Private _PromptGroupID As New ColField(Of String)("GROUPPROMPTID", "", "Group Prompt ID", False, False)
    Private _Sequence As New ColField(Of String)("SEQUENCE", "", "Sequence", False, False)
    Private _SentToStores As New ColField(Of Boolean)("SENT", False, "Sent to Stores", False, False)
    Private _DateDeleted As New ColField(Of Date)("DATEDELETED", Nothing, "Date Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property PromptGroupID() As ColField(Of String)
        Get
            PromptGroupID = _PromptGroupID
        End Get
        Set(ByVal value As ColField(Of String))
            _PromptGroupID = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of String)
        Get
            Sequence = _Sequence
        End Get
        Set(ByVal value As ColField(Of String))
            _Sequence = value
        End Set
    End Property
    Public Property SentToStores() As ColField(Of Boolean)
        Get
            SentToStores = _SentToStores
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SentToStores = value
        End Set
    End Property
    Public Property DateDeleted() As ColField(Of Date)
        Get
            DateDeleted = _DateDeleted
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateDeleted = value
        End Set
    End Property


#End Region

End Class
