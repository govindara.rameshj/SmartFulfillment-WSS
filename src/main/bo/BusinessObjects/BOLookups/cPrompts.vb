﻿<Serializable()> Public Class cPrompts
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PROMPTS"
        BOFields.Add(_Type)
        BOFields.Add(_ID)
        BOFields.Add(_ImagePath)
        BOFields.Add(_ScreenText1)
        BOFields.Add(_ScreenText2)
        BOFields.Add(_ScreenText3)
        BOFields.Add(_ScreenText4)
        BOFields.Add(_PrintText1)
        BOFields.Add(_PrintText2)
        BOFields.Add(_PrintText3)
        BOFields.Add(_PrintText4)
        BOFields.Add(_RejectText1)
        BOFields.Add(_RejectText2)
        BOFields.Add(_DataItem)
        BOFields.Add(_DataGroup)
        BOFields.Add(_Condition)
        BOFields.Add(_Supervisor)
        BOFields.Add(_Manager)
        BOFields.Add(_Collect)
        BOFields.Add(_Sale)
        BOFields.Add(_Group)
        BOFields.Add(_Value)
        BOFields.Add(_ResponseType)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPrompts)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPrompts)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPrompts))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPrompts(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("PROMPTID", "", "ID", True, False)
    Private _Type As New ColField(Of String)("PROMPTTYPE", "", "Type", False, False)
    Private _ImagePath As New ColField(Of String)("IMAGEPATH", "", "Image Path", False, False)
    Private _ScreenText1 As New ColField(Of String)("SCREENTEXT1", "", "Screen Text 1", False, False)
    Private _ScreenText2 As New ColField(Of String)("SCREENTEXT2", "", "Screen Text 2", False, False)
    Private _ScreenText3 As New ColField(Of String)("SCREENTEXT3", "", "Screen Text 3", False, False)
    Private _ScreenText4 As New ColField(Of String)("SCREENTEXT4", "", "Screen Text 4", False, False)
    Private _PrintText1 As New ColField(Of String)("PRINTTEXT1", "", "Print Text 1", False, False)
    Private _PrintText2 As New ColField(Of String)("PRINTTEXT2", "", "Print Text 2", False, False)
    Private _PrintText3 As New ColField(Of String)("PRINTTEXT3", "", "Print Text 3", False, False)
    Private _PrintText4 As New ColField(Of String)("PRINTTEXT4", "", "Print Text 4", False, False)
    Private _RejectText1 As New ColField(Of String)("REJECTTEXT1", "", "Reject Text 1", False, False)
    Private _RejectText2 As New ColField(Of String)("REJECTTEXT2", "", "Reject Text 2", False, False)
    Private _DataItem As New ColField(Of String)("DATAITEM", "", "Data Item", False, False)
    Private _DataGroup As New ColField(Of String)("DATAGROUP", "", "Data Group", False, False)
    Private _Condition As New ColField(Of String)("CONDITION", "", "Condition", False, False)
    Private _Supervisor As New ColField(Of Boolean)("SUPV", False, "Supervisor", False, False)
    Private _Manager As New ColField(Of Boolean)("MANA", False, "Manager", False, False)
    Private _Collect As New ColField(Of String)("COLLECT", "", "Collect", False, False)
    Private _Sale As New ColField(Of Boolean)("SALE", False, "SALE", False, False)
    Private _Group As New ColField(Of Integer)("PROMPTGROUP", 0, "Prompt Group", False, False)
    Private _Value As New ColField(Of String)("PROMPTVALUE", "", "Prompt Value", False, False)
    Private _ResponseType As New ColField(Of String)("RESPONSETYPE", "", "Response Type", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of String)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property ImagePath() As ColField(Of String)
        Get
            ImagePath = _ImagePath
        End Get
        Set(ByVal value As ColField(Of String))
            _ImagePath = value
        End Set
    End Property
    Public Property ScreenText1() As ColField(Of String)
        Get
            Return _ScreenText1
        End Get
        Set(ByVal value As ColField(Of String))
            _ScreenText1 = value
        End Set
    End Property
    Public Property ScreenText2() As ColField(Of String)
        Get
            ScreenText2 = _ScreenText2
        End Get
        Set(ByVal value As ColField(Of String))
            _ScreenText2 = value
        End Set
    End Property
    Public Property ScreenText3() As ColField(Of String)
        Get
            Return _ScreenText3
        End Get
        Set(ByVal value As ColField(Of String))
            _ScreenText3 = value
        End Set
    End Property
    Public Property ScreenText4() As ColField(Of String)
        Get
            Return _ScreenText4
        End Get
        Set(ByVal value As ColField(Of String))
            _ScreenText4 = value
        End Set
    End Property
    Public Property PrintText1() As ColField(Of String)
        Get
            Return _PrintText1
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintText1 = value
        End Set
    End Property
    Public Property PrintText2() As ColField(Of String)
        Get
            PrintText2 = _PrintText2
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintText2 = value
        End Set
    End Property
    Public Property PrintText3() As ColField(Of String)
        Get
            Return _PrintText3
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintText3 = value
        End Set
    End Property
    Public Property PrintText4() As ColField(Of String)
        Get
            Return _PrintText4
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintText4 = value
        End Set
    End Property
    Public Property RejectText1() As ColField(Of String)
        Get
            Return _RejectText1
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectText1 = value
        End Set
    End Property
    Public Property RejectText2() As ColField(Of String)
        Get
            RejectText2 = _RejectText2
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectText2 = value
        End Set
    End Property
    Public Property DataItem() As ColField(Of String)
        Get
            Return _DataItem
        End Get
        Set(ByVal value As ColField(Of String))
            _DataItem = value
        End Set
    End Property
    Public Property DataGroup() As ColField(Of String)
        Get
            Return _DataGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _DataGroup = value
        End Set
    End Property
    Public Property Condition() As ColField(Of String)
        Get
            Return _Condition
        End Get
        Set(ByVal value As ColField(Of String))
            _Condition = value
        End Set
    End Property
    Public Property Supervisor() As ColField(Of Boolean)
        Get
            Supervisor = _Supervisor
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Supervisor = value
        End Set
    End Property
    Public Property Manager() As ColField(Of Boolean)
        Get
            Manager = _Manager
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Manager = value
        End Set
    End Property
    Public Property Collect() As ColField(Of String)
        Get
            Return _Collect
        End Get
        Set(ByVal value As ColField(Of String))
            _Collect = value
        End Set
    End Property
    Public Property Sale() As ColField(Of Boolean)
        Get
            Return _Sale
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Sale = value
        End Set
    End Property
    Public Property Group() As ColField(Of Integer)
        Get
            Return _Group
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Group = value
        End Set
    End Property
    Public Property Value() As ColField(Of String)
        Get
            Return _Value
        End Get
        Set(ByVal value As ColField(Of String))
            _Value = value
        End Set
    End Property
    Public Property ResponseType() As ColField(Of String)
        Get
            Return _ResponseType
        End Get
        Set(ByVal value As ColField(Of String))
            _ResponseType = value
        End Set
    End Property

#End Region

End Class


