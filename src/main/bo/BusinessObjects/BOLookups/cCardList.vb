<Serializable()> Public Class cCardList
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CARD_LIST"
        BOFields.Add(_CardNumber)
        BOFields.Add(_CustomerName)
        BOFields.Add(_CustomerAddr1)
        BOFields.Add(_CustomerAddr2)
        BOFields.Add(_CustomerAddr3)
        BOFields.Add(_CustomerAddr4)
        BOFields.Add(_CustomerPostCode)
        BOFields.Add(_HideAddress)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCardList)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cCardList)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cCardList))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCardList(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CardNumber As New ColField(Of String)("CARD_NO", "", "Card Number", True, False)
    Private _CustomerName As New ColField(Of String)("CUST_NAME", "", "Customer Name", False, False)
    Private _CustomerAddr1 As New ColField(Of String)("CUST_ADD1", "", "Address 1", False, False)
    Private _CustomerAddr2 As New ColField(Of String)("CUST_ADD2", "", "Address 2", False, False)
    Private _CustomerAddr3 As New ColField(Of String)("CUST_ADD3", "", "Address 3", False, False)
    Private _CustomerAddr4 As New ColField(Of String)("CUST_ADD4", "", "Address 4", False, False)
    Private _CustomerPostCode As New ColField(Of String)("CUST_POST", "", "Post Code", False, False)
    Private _HideAddress As New ColField(Of String)("HIDE_ADDR", "", "Hide Address", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELETED", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property CardNumber() As ColField(Of String)
        Get
            CardNumber = _CardNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CardNumber = value
        End Set
    End Property
    Public Property CustomerName() As ColField(Of String)
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerName = value
        End Set
    End Property
    Public Property CustomerAddr1() As ColField(Of String)
        Get
            CustomerAddr1 = _CustomerAddr1
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAddr1 = value
        End Set
    End Property
    Public Property CustomerAddr2() As ColField(Of String)
        Get
            CustomerAddr2 = _CustomerAddr2
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAddr2 = value
        End Set
    End Property
    Public Property CustomerAddr3() As ColField(Of String)
        Get
            CustomerAddr3 = _CustomerAddr3
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAddr3 = value
        End Set
    End Property
    Public Property CustomerAddr4() As ColField(Of String)
        Get
            CustomerAddr4 = _CustomerAddr4
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAddr4 = value
        End Set
    End Property
    Public Property CustomerPostCode() As ColField(Of String)
        Get
            CustomerPostCode = _CustomerPostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerPostCode = value
        End Set
    End Property
    Public Property HideAddress() As ColField(Of String)
        Get
            HideAddress = _HideAddress
        End Get
        Set(ByVal value As ColField(Of String))
            _HideAddress = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

End Class
