﻿<Serializable()> Public Class cCardScheme
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CARD_SCHEME"
        BOFields.Add(_ID)
        BOFields.Add(_Name)
        BOFields.Add(_ReceiptText)
        BOFields.Add(_CardNumberSize)
        BOFields.Add(_StartNumber)
        BOFields.Add(_EndNumber)
        BOFields.Add(_StartDate)
        BOFields.Add(_EndDate)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_Keyed)
        BOFields.Add(_CaptureName)
        BOFields.Add(_CaptureAdd1)
        BOFields.Add(_CaptureAdd2)
        BOFields.Add(_CaptureAdd3)
        BOFields.Add(_CaptureAdd4)
        BOFields.Add(_CapturePostCode)
        BOFields.Add(_CopyReceipt)
        BOFields.Add(_ErosionCode)
        BOFields.Add(_Discount)
        BOFields.Add(_CardPreference)
        BOFields.Add(_SupervisorAuthLevel)
        BOFields.Add(_ManagerAuthLevel)
        BOFields.Add(_PrintSigSlip)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCardScheme)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cCardScheme)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cCardScheme))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCardScheme(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("ID", "", "ID", True, False)
    Private _Name As New ColField(Of String)("SCHEME_NAME", "", "Name", False, False)
    Private _ReceiptText As New ColField(Of String)("RECEIPT_TEXT", "", "Receipt Text", False, False)
    Private _CardNumberSize As New ColField(Of Integer)("CARD_NO_SIZE", 0, "Card Number Size", False, False)
    Private _StartNumber As New ColField(Of String)("START_NO", "", "Start Number", False, False)
    Private _EndNumber As New ColField(Of String)("END_NO", "", "End Number", False, False)
    Private _StartDate As New ColField(Of Date)("START_DATE", Nothing, "Start Date", False, False)
    Private _EndDate As New ColField(Of Date)("END_DATE", Nothing, "End Date", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELETED", False, "Is Deleted", False, False)
    Private _Keyed As New ColField(Of String)("KEYED", "", "Keyed", False, False)
    Private _CaptureName As New ColField(Of String)("CAPT_NAME", "", "Capture Name", False, False)
    Private _CaptureAdd1 As New ColField(Of String)("CAPT_ADD1", "", "Capture Address 1", False, False)
    Private _CaptureAdd2 As New ColField(Of String)("CAPT_ADD2", "", "Capture Address 2", False, False)
    Private _CaptureAdd3 As New ColField(Of String)("CAPT_ADD3", "", "Capture Address 3", False, False)
    Private _CaptureAdd4 As New ColField(Of String)("CAPT_ADD4", "", "Capture Address 4", False, False)
    Private _CapturePostCode As New ColField(Of String)("CAPT_POST", "", "Capture Post Code", False, False)
    Private _CopyReceipt As New ColField(Of Boolean)("COPY_RECEIPT", False, "Copy Receipt", False, False)
    Private _ErosionCode As New ColField(Of String)("EROSION_CODE", "", "Erosion Code", False, False)
    Private _Discount As New ColField(Of Decimal)("DISCOUNT", 0, "Discount", False, False)
    Private _CardPreference As New ColField(Of String)("CARD_PREF", "", "Card Preference", False, False)
    Private _SupervisorAuthLevel As New ColField(Of Decimal)("SUPAUTHLEVEL", 0, "Supervisor Authorisation Level", False, False)
    Private _ManagerAuthLevel As New ColField(Of Decimal)("MNGRAUTHLEVEL", 0, "Manager Authorisation Level", False, False)
    Private _PrintSigSlip As New ColField(Of Boolean)("PRINT_SIG_SLIP", False, "Print Signature Slip", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of String)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property Name() As ColField(Of String)
        Get
            Name = _Name
        End Get
        Set(ByVal value As ColField(Of String))
            _Name = value
        End Set
    End Property
    Public Property ReceiptText() As ColField(Of String)
        Get
            ReceiptText = _ReceiptText
        End Get
        Set(ByVal value As ColField(Of String))
            _ReceiptText = value
        End Set
    End Property
    Public Property CardNumberSize() As ColField(Of Integer)
        Get
            CardNumberSize = _CardNumberSize
        End Get
        Set(ByVal value As ColField(Of Integer))
            _CardNumberSize = value
        End Set
    End Property
    Public Property StartNumber() As ColField(Of String)
        Get
            StartNumber = _StartNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StartNumber = value
        End Set
    End Property
    Public Property EndNumber() As ColField(Of String)
        Get
            EndNumber = _EndNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EndNumber = value
        End Set
    End Property
    Public Property STartDate() As ColField(Of Date)
        Get
            STartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property Keyed() As ColField(Of String)
        Get
            Keyed = _Keyed
        End Get
        Set(ByVal value As ColField(Of String))
            _Keyed = value
        End Set
    End Property
    Public Property CaptureName() As ColField(Of String)
        Get
            CaptureName = _CaptureName
        End Get
        Set(ByVal value As ColField(Of String))
            _CaptureName = value
        End Set
    End Property
    Public Property CaptureAdd1() As ColField(Of String)
        Get
            CaptureAdd1 = _CaptureAdd1
        End Get
        Set(ByVal value As ColField(Of String))
            _CaptureAdd1 = value
        End Set
    End Property
    Public Property CaptureAdd2() As ColField(Of String)
        Get
            CaptureAdd2 = _CaptureAdd2
        End Get
        Set(ByVal value As ColField(Of String))
            _CaptureAdd2 = value
        End Set
    End Property
    Public Property CaptureAdd3() As ColField(Of String)
        Get
            CaptureAdd3 = _CaptureAdd3
        End Get
        Set(ByVal value As ColField(Of String))
            _CaptureAdd3 = value
        End Set
    End Property
    Public Property CaptureAdd4() As ColField(Of String)
        Get
            CaptureAdd4 = _CaptureAdd4
        End Get
        Set(ByVal value As ColField(Of String))
            _CaptureAdd4 = value
        End Set
    End Property
    Public Property CapturePostCode() As ColField(Of String)
        Get
            CapturePostCode = _CapturePostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CapturePostCode = value
        End Set
    End Property
    Public Property CopyReceipt() As ColField(Of Boolean)
        Get
            CopyReceipt = _CopyReceipt
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CopyReceipt = value
        End Set
    End Property
    Public Property ErosionCode() As ColField(Of String)
        Get
            ErosionCode = _ErosionCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ErosionCode = value
        End Set
    End Property
    Public Property Discount() As ColField(Of Decimal)
        Get
            Discount = _Discount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Discount = value
        End Set
    End Property
    Public Property CardPref() As ColField(Of String)
        Get
            CardPref = _CardPreference
        End Get
        Set(ByVal value As ColField(Of String))
            _CardPreference = value
        End Set
    End Property
    Public Property SupervisorAuthLevel() As ColField(Of Decimal)
        Get
            SupervisorAuthLevel = _SupervisorAuthLevel
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SupervisorAuthLevel = value
        End Set
    End Property
    Public Property ManagerAuthLevel() As ColField(Of Decimal)
        Get
            ManagerAuthLevel = _ManagerAuthLevel
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ManagerAuthLevel = value
        End Set
    End Property
    Public Property PrintSigSlip() As ColField(Of Boolean)
        Get
            PrintSigSlip = _PrintSigSlip
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintSigSlip = value
        End Set
    End Property

#End Region

End Class

