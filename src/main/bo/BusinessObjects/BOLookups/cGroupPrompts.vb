﻿<Serializable()> Public Class cGroupPrompts
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "GroupPrompts"
        BOFields.Add(_GroupID)
        BOFields.Add(_PromptID)
        BOFields.Add(_Sequence)
        BOFields.Add(_OKGoto)
        BOFields.Add(_YesGoto)
        BOFields.Add(_NoGoto)
        BOFields.Add(_LogReject)
        BOFields.Add(_GroupDescription)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cGroupPrompts)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cGroupPrompts)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cGroupPrompts))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cGroupPrompts(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _GroupID As New ColField(Of String)("GROUPID", "", "Group ID", True, False)
    Private _PromptID As New ColField(Of String)("PROMPTID", "", "Prompt ID", False, False)
    Private _Sequence As New ColField(Of String)("SEQUENCE", "", "Sequence", False, False)
    Private _OKGoto As New ColField(Of String)("OKGOTO", "", "OK Goto", False, False)
    Private _YesGoto As New ColField(Of String)("YESGOTO", "", "Yes Goto", False, False)
    Private _NoGoto As New ColField(Of String)("NOGOTO", "", "No Goto", False, False)
    Private _LogReject As New ColField(Of String)("LOGREJECT", "", "Log Reject", False, False)
    Private _GroupDescription As New ColField(Of String)("GROUPDESC", "", "Group Description", False, False)

#End Region

#Region "Field Properties"

    Public Property GroupID() As ColField(Of String)
        Get
            GroupID = _GroupID
        End Get
        Set(ByVal value As ColField(Of String))
            _GroupID = value
        End Set
    End Property
    Public Property PromptID() As ColField(Of String)
        Get
            PromptID = _PromptID
        End Get
        Set(ByVal value As ColField(Of String))
            _PromptID = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of String)
        Get
            Sequence = _Sequence
        End Get
        Set(ByVal value As ColField(Of String))
            _Sequence = value
        End Set
    End Property
    Public Property OKGoto() As ColField(Of String)
        Get
            OKGoto = _OKGoto
        End Get
        Set(ByVal value As ColField(Of String))
            _OKGoto = value
        End Set
    End Property
    Public Property YesGoto() As ColField(Of String)
        Get
            YesGoto = _YesGoto
        End Get
        Set(ByVal value As ColField(Of String))
            _YesGoto = value
        End Set
    End Property
    Public Property NoGoto() As ColField(Of String)
        Get
            NoGoto = _NoGoto
        End Get
        Set(ByVal value As ColField(Of String))
            _NoGoto = value
        End Set
    End Property
    Public Property LogReject() As ColField(Of String)
        Get
            LogReject = _LogReject
        End Get
        Set(ByVal value As ColField(Of String))
            _LogReject = value
        End Set
    End Property
    Public Property GroupDescription() As ColField(Of String)
        Get
            GroupDescription = _GroupDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _GroupDescription = value
        End Set
    End Property


#End Region

End Class

