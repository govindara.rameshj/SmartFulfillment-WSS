Imports Cts.Oasys.Core

<Serializable()> Public Class cViewerConfig
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ViewerConfig"
        BOFields.Add(_ID)
        BOFields.Add(_Title)
        BOFields.Add(_BOName)
        BOFields.Add(_IsHorizontal)
        BOFields.Add(_RecordLimit)
        BOFields.Add(_AllowAddition)
        BOFields.Add(_AllowDeletion)
        BOFields.Add(_AllowUpdate)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cViewerConfig)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cViewerConfig)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cViewerConfig))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cViewerConfig(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "Config ID", True, True)
    Private _Title As New ColField(Of String)("Title", "", "Title", False, False)
    Private _BOName As New ColField(Of String)("BOName", "", "Business Object Name", False, False)
    Private _IsHorizontal As New ColField(Of Boolean)("IsHorizontal", False, "Is Horizontal", False, False)
    Private _RecordLimit As New ColField(Of Integer)("RecordLimit", -1, "Record Limit", False, False)
    Private _AllowAddition As New ColField(Of Boolean)("AllowAddition", False, "Allow Addition", False, False)
    Private _AllowDeletion As New ColField(Of Boolean)("AllowDeletion", False, "Allow Deletion", False, False)
    Private _AllowUpdate As New ColField(Of Boolean)("AllowUpdate", False, "Allow Update", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Title() As ColField(Of String)
        Get
            Title = _Title
        End Get
        Set(ByVal value As ColField(Of String))
            _Title = value
        End Set
    End Property
    Public Property BOName() As ColField(Of String)
        Get
            BOName = _BOName
        End Get
        Set(ByVal value As ColField(Of String))
            _BOName = value
        End Set
    End Property
    Public Property IsHorizontal() As ColField(Of Boolean)
        Get
            Return _IsHorizontal
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsHorizontal = value
        End Set
    End Property
    Public Property RecordLimit() As ColField(Of Integer)
        Get
            Return _RecordLimit
        End Get
        Set(ByVal value As ColField(Of Integer))
            _RecordLimit = value
        End Set
    End Property
    Public Property AllowAddition() As ColField(Of Boolean)
        Get
            Return _AllowAddition
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AllowAddition = value
        End Set
    End Property
    Public Property AllowDeletion() As ColField(Of Boolean)
        Get
            Return _AllowDeletion
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AllowDeletion = value
        End Set
    End Property
    Public Property AllowUpdate() As ColField(Of Boolean)
        Get
            Return _AllowUpdate
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AllowUpdate = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _BO As OasysDBBO.cBaseClass = Nothing
    Private _BOIndexes() As Integer
    Private _Columns As List(Of cViewerColumns) = Nothing
    Private _Tabs As List(Of cViewerTabs) = Nothing
    Private _Sorts As List(Of cViewerSort) = Nothing
    Private _Parameters As ArrayList = Nothing

    Public Property BO() As OasysDBBO.cBaseClass
        Get
            If _BO Is Nothing Then LoadBO()
            Return _BO
        End Get
        Set(ByVal value As OasysDBBO.cBaseClass)
            _BO = value
        End Set
    End Property
    Public Property BOIndex(ByVal index As Integer) As Integer
        Get
            Return _BOIndexes(index)
        End Get
        Set(ByVal value As Integer)
            _BOIndexes(index) = value
        End Set
    End Property

    Public Property Columns() As List(Of cViewerColumns)
        Get
            If _Columns Is Nothing Then 'Load from database
                Dim ViewColumns As New cViewerColumns(Oasys3DB)
                ViewColumns.AddLoadFilter(clsOasys3DB.eOperator.pEquals, ViewColumns.ConfigID, _ID.Value)
                ViewColumns.SortBy(ViewColumns.DisplayOrder.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                _Columns = ViewColumns.LoadMatches
            End If
            Return _Columns
        End Get
        Set(ByVal value As List(Of cViewerColumns))
            _Columns = value
        End Set
    End Property
    Public Property Column(ByVal ColumnName As String) As cViewerColumns
        Get
            For Each col As cViewerColumns In Columns
                If col.ColumnName.Value = ColumnName Then Return col
            Next
            Return Nothing
        End Get
        Set(ByVal value As cViewerColumns)
            If _Columns IsNot Nothing Then
                For Each col As cViewerColumns In _Columns
                    If col.ColumnName.Value = ColumnName Then
                        col = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Column(ByVal ID As Integer) As cViewerColumns
        Get
            For Each col As cViewerColumns In Columns
                If col.ID.Value = ID Then Return col
            Next
            Return Nothing
        End Get
        Set(ByVal value As cViewerColumns)
            If _Columns IsNot Nothing Then
                For Each col As cViewerColumns In _Columns
                    If col.ID.Value = ID Then
                        col = value
                    End If
                Next
            End If
        End Set
    End Property

    Public Property Tabs() As List(Of cViewerTabs)
        Get
            If _Tabs Is Nothing Then
                Dim ViewerTab As New cViewerTabs(Oasys3DB)
                ViewerTab.AddLoadFilter(clsOasys3DB.eOperator.pEquals, ViewerTab.ConfigID, _ID.Value)
                _Tabs = ViewerTab.LoadMatches
            End If
            Return _Tabs
        End Get
        Set(ByVal value As List(Of cViewerTabs))
            _Tabs = value
        End Set
    End Property
    Public Property Tab(ByVal ID As Integer) As cViewerTabs
        Get
            For Each t As cViewerTabs In Tabs
                If t.ID.Value = ID Then Return t
            Next
            Return Nothing
        End Get
        Set(ByVal value As cViewerTabs)
            If _Tabs IsNot Nothing Then
                For Each t As cViewerTabs In Tabs
                    If t.ID.Value = ID Then t = value
                Next
            End If
        End Set
    End Property

    Public Property Sorts() As List(Of cViewerSort)
        Get
            If _Sorts Is Nothing Then
                Dim s As New cViewerSort(Oasys3DB)
                s.AddLoadFilter(clsOasys3DB.eOperator.pEquals, s.ConfigID, _ID.Value)
                _Sorts = s.LoadMatches
            End If
            Return _Sorts
        End Get
        Set(ByVal value As List(Of cViewerSort))
            _Sorts = value
        End Set
    End Property
    Public Property Sort(ByVal SortID As Integer) As cViewerSort
        Get
            For Each s As cViewerSort In Sorts
                If s.ID.Value = SortID Then Return s
            Next
            Return Nothing
        End Get
        Set(ByVal value As cViewerSort)
            For Each s As cViewerSort In Sorts
                If s.ID.Value = SortID Then s = value
            Next
        End Set
    End Property

    Public Property Parameters() As ArrayList
        Get
            Return _Parameters
        End Get
        Set(ByVal value As ArrayList)
            _Parameters = value
        End Set
    End Property

    Public Function ParametersString() As String

        Try
            If _Parameters Is Nothing Then Return String.Empty
            Dim sb As New StringBuilder
            For Each s As String In Parameters
                Dim IDstart As Integer = s.IndexOf("[")
                Dim IDend As Integer = s.IndexOf("]") + 1 - IDstart
                s = s.Remove(IDstart, IDend)
                s = s.Replace("=", " = ")
                sb.Append(s & ", ")
            Next

            If sb.Length > 0 Then sb.Remove(sb.Length - 2, 2)
            Return sb.ToString

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub LoadBO()

        Try

            Dim strFile As String = _BOName.Value.Substring(0, _BOName.Value.IndexOf(".") + 1) & "dll"
            strFile = ConfigPathResolver.ResolveBusinessObjectsDllPath(strFile)


            _BO = CType(Activator.CreateInstanceFrom(strFile, _BOName.Value.Trim, True, Reflection.BindingFlags.Default, Nothing, New Object() {Oasys3DB}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysDBBO.cBaseClass)

            For Each viewcol As cViewerColumns In Columns
                For Each field As IColField In _BO.BOFields
                    If field.ColumnName = viewcol.ColumnName.Value Then
                        viewcol.BOColumn = field

                        'check for constraints
                        For Each boConstraint As Object() In _BO.BOConstraints
                            If boConstraint.Contains(field) Then
                                viewcol.BOColumnConstraints.Add(boConstraint)
                            End If
                        Next

                        Exit For
                    End If
                Next
            Next

            ReDim _BOIndexes(_BO.BOFields.Count)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Loads viewer config into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load(ByVal ConfigID As Integer)

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, ConfigID)
            LoadMatches()
            LoadBO()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.LoadConfig, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads records into this instance BO.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadRecords(ByRef firstFilter As Boolean)

        Try
            If _BO Is Nothing Then LoadBO()

            'add load filter for each parameter if they exist
            If _Parameters IsNot Nothing Then
                For Each s As String In _Parameters
                    Dim IDstart As Integer = s.IndexOf("[") + 1
                    Dim IDend As Integer = s.IndexOf("]")
                    Dim ID As Integer = CInt(s.Substring(IDstart, IDend - IDstart))

                    If Not firstFilter Then BO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    _BO.AddUntypedLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Column(ID).BOColumn, s.Substring(s.IndexOf("=") + 1))

                    firstFilter = False
                Next
            End If

            BO.LoadBORecords(_RecordLimit.Value)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.LoadBORecords, ex)
        End Try

    End Sub

#End Region

End Class