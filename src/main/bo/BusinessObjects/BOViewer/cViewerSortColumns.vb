<Serializable()> Public Class cViewerSortColumns
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ViewerSortColumns"
        BOFields.Add(_ID)
        BOFields.Add(_ConfigID)
        BOFields.Add(_SortID)
        BOFields.Add(_SortOrder)
        BOFields.Add(_Ascending)
        BOFields.Add(_SumIDs)
        BOFields.Add(_CountIDs)
        BOFields.Add(_MaxIDs)
        BOFields.Add(_MinIDs)
        BOFields.Add(_AveIDs)
        BOConstraints.Add(New Object() {_SortID, _ConfigID, _ID})
        BOConstraints.Add(New Object() {_SortID, _ConfigID, _SortOrder})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cViewerSortColumns)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cViewerSortColumns)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cViewerSortColumns))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cViewerSortColumns(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SortID As New ColField(Of Integer)("SortID", 0, "Sort ID", True, False)
    Private _ConfigID As New ColField(Of Integer)("ConfigID", 0, "Config ID", True, False)
    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _SortOrder As New ColField(Of Integer)("SortOrder", 0, "Sort Order", False, False)
    Private _Ascending As New ColField(Of Boolean)("Ascending", False, "Ascending", False, False)
    Private _SumIDs As New ColField(Of String)("SumIDs", "", "Sum IDs", False, False)
    Private _CountIDs As New ColField(Of String)("CountIDs", "", "Count IDs", False, False)
    Private _MaxIDs As New ColField(Of String)("MaxIDs", "", "Max IDs", False, False)
    Private _MinIDs As New ColField(Of String)("MinIDs", "", "Min IDs", False, False)
    Private _AveIDs As New ColField(Of String)("AveIDs", "", "Average IDs", False, False)

#End Region

#Region "Field Properties"

    Public Property SortID() As ColField(Of Integer)
        Get
            Return _SortID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SortID = value
        End Set
    End Property
    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property ConfigID() As ColField(Of Integer)
        Get
            Return _ConfigID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ConfigID = value
        End Set
    End Property
    Public Property SortOrder() As ColField(Of Integer)
        Get
            Return _SortOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SortOrder = value
        End Set
    End Property
    Public Property Ascending() As ColField(Of Boolean)
        Get
            Return _Ascending
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Ascending = value
        End Set
    End Property
    Public Property SumIDs() As ColField(Of String)
        Get
            Return _SumIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _SumIDs = value
        End Set
    End Property
    Public Property CountIDs() As ColField(Of String)
        Get
            Return _CountIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _CountIDs = value
        End Set
    End Property
    Public Property MaxIDs() As ColField(Of String)
        Get
            Return _MaxIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _MaxIDs = value
        End Set
    End Property
    Public Property MinIDs() As ColField(Of String)
        Get
            Return _MinIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _MinIDs = value
        End Set
    End Property
    Public Property AveIDs() As ColField(Of String)
        Get
            Return _AveIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _AveIDs = value
        End Set
    End Property

#End Region

End Class