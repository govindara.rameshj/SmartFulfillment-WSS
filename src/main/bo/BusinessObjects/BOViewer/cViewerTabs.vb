<Serializable()> Public Class cViewerTabs
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ViewerTabs"
        BOFields.Add(_ID)
        BOFields.Add(_ConfigID)
        BOFields.Add(_Description)
        BOFields.Add(_DisplayOrder)
        BOConstraints.Add(New Object() {_ConfigID, _ID})
        BOConstraints.Add(New Object() {_ConfigID, _DisplayOrder})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cViewerTabs)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cViewerTabs)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cViewerTabs))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cViewerTabs(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _ConfigID As New ColField(Of Integer)("ConfigID", 0, "Config ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _DisplayOrder As New ColField(Of Integer)("DisplayOrder", 0, "Display Order", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property ConfigID() As ColField(Of Integer)
        Get
            Return _ConfigID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ConfigID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
            _Description.Value = _Description.Value.Trim
        End Set
    End Property
    Public Property DisplayOrder() As ColField(Of Integer)
        Get
            Return _DisplayOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DisplayOrder = value
        End Set
    End Property

#End Region

End Class