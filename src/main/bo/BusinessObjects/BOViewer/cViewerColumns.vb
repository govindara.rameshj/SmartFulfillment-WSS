<Serializable()> Public Class cViewerColumns
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ViewerColumns"
        BOFields.Add(_ID)
        BOFields.Add(_ConfigID)
        BOFields.Add(_ColumnName)
        BOFields.Add(_ColumnText)
        BOFields.Add(_ColumnHelp)
        BOFields.Add(_HyperLink)
        BOFields.Add(_HyperLinkType)
        BOFields.Add(_SelectionType)
        BOFields.Add(_LookupConfigID)
        BOFields.Add(_LookupColumnID)
        BOFields.Add(_DisplayColumn)
        BOFields.Add(_DisplayOrder)
        BOFields.Add(_TabIDs)
        BOFields.Add(_IsEditable)
        BOConstraints.Add(New Object() {_ConfigID, _DisplayOrder})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cViewerColumns)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cViewerColumns)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cViewerColumns))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cViewerColumns(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "Column ID", True, True)
    Private _ConfigID As New ColField(Of Integer)("ConfigID", 0, "Config ID", False, False)
    Private _ColumnName As New ColField(Of String)("ColumnName", "", "Column Name", False, False)
    Private _ColumnText As New ColField(Of String)("ColumnText", "", "Column Text", False, False)
    Private _ColumnHelp As New ColField(Of String)("ColumnHelp", "", "Column Help", False, False)
    Private _HyperLink As New ColField(Of String)("HyperLink", "", "HyperLink", False, False)
    Private _HyperLinkType As New ColField(Of Integer)("HyperLinkType", 0, "HyperLink Type", False, False)
    Private _SelectionType As New ColField(Of Integer)("SelectionType", 0, "Selection Type", False, False)
    Private _LookupConfigID As New ColField(Of Integer)("LookupConfigID", 0, "Lookup", False, False)
    Private _LookupColumnID As New ColField(Of Integer)("LookupColumnID", 0, "Lookup ID", False, False)
    Private _DisplayColumn As New ColField(Of Boolean)("DisplayColumn", False, "Display Column", False, False)
    Private _DisplayOrder As New ColField(Of Integer)("DisplayOrder", 0, "Display Order", False, False)
    Private _TabIDs As New ColField(Of String)("TabIDs", "", "Tab IDs", False, False)
    Private _IsEditable As New ColField(Of Boolean)("IsEditable", False, "Editable", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property ConfigID() As ColField(Of Integer)
        Get
            ConfigID = _ConfigID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ConfigID = value
        End Set
    End Property
    Public Property ColumnName() As ColField(Of String)
        Get
            ColumnName = _ColumnName
        End Get
        Set(ByVal value As ColField(Of String))
            _ColumnName = value
        End Set
    End Property
    Public Property ColumnText() As ColField(Of String)
        Get
            Return _ColumnText
        End Get
        Set(ByVal value As ColField(Of String))
            _ColumnText = value
        End Set
    End Property
    Public Property ColumnHelp() As ColField(Of String)
        Get
            Return _ColumnHelp
        End Get
        Set(ByVal value As ColField(Of String))
            _ColumnHelp = value
        End Set
    End Property
    Public Property HyperLink() As ColField(Of String)
        Get
            HyperLink = _HyperLink
        End Get
        Set(ByVal value As ColField(Of String))
            _HyperLink = value
        End Set
    End Property
    Public Property HyperLinkType() As ColField(Of Integer)
        Get
            Return _HyperLinkType
        End Get
        Set(ByVal value As ColField(Of Integer))
            _HyperLinkType = value
        End Set
    End Property
    Public Property SelectionType() As ColField(Of Integer)
        Get
            SelectionType = _SelectionType
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SelectionType = value
        End Set
    End Property
    Public Property LookupConfigID() As ColField(Of Integer)
        Get
            Return _LookupConfigID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LookupConfigID = value
        End Set
    End Property
    Public Property LookupColumnID() As ColField(Of Integer)
        Get
            Return _LookupColumnID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LookupColumnID = value
        End Set
    End Property
    Public Property DisplayColumn() As ColField(Of Boolean)
        Get
            DisplayColumn = _DisplayColumn
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DisplayColumn = value
        End Set
    End Property
    Public Property DisplayOrder() As ColField(Of Integer)
        Get
            DisplayOrder = _DisplayOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DisplayOrder = value
        End Set
    End Property
    Public Property TabIDs() As ColField(Of String)
        Get
            Return _TabIDs
        End Get
        Set(ByVal value As ColField(Of String))
            _TabIDs = value
        End Set
    End Property
    Public Property IsEditable() As ColField(Of Boolean)
        Get
            IsEditable = _IsEditable
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsEditable = value
        End Set
    End Property

#End Region

#Region "Enumerations"
    Public Enum HyperLinkTypes
        [New] = 0
        Tab
        Split
    End Enum
    Public Enum SelectionTypes
        None = 0
        ComboBox
        ComboBoxDescriptionOnly
        DateTimePicker
        TextBox
        TrueFalse
    End Enum

    Public Property SelectionTypeValue() As SelectionTypes
        Get
            Return CType(_SelectionType.Value, SelectionTypes)
        End Get
        Set(ByVal value As SelectionTypes)
            _SelectionType.Value = value
        End Set
    End Property
    Public Property HyperLinkTypeValue() As HyperLinkTypes
        Get
            Return CType(_HyperLinkType.Value, HyperLinkTypes)
        End Get
        Set(ByVal value As HyperLinkTypes)
            _HyperLinkType.Value = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _BOColumn As IColField = Nothing
    Private _BOColumnConstraints As New List(Of Object())
    Private _Index As Integer = Nothing
    Private _Count As Integer = 0
    Private _Sum As Decimal = 0
    Private _Max As Decimal = Nothing
    Private _Min As Decimal = Nothing
    Private _AveValues As New List(Of Decimal)

    Public Property BOColumn() As IColField
        Get
            Return _BOColumn
        End Get
        Set(ByVal value As IColField)
            _BOColumn = value
        End Set
    End Property
    Public Property BOColumnConstraints() As List(Of Object())
        Get
            Return _BOColumnConstraints
        End Get
        Set(ByVal value As List(Of Object()))
            _BOColumnConstraints = value
        End Set
    End Property


    Public Property Index() As Integer
        Get
            Return _Index
        End Get
        Set(ByVal value As Integer)
            _Index = value
        End Set
    End Property
    Public Property Count() As Integer
        Get
            Return _Count
        End Get
        Set(ByVal value As Integer)
            _Count = value
        End Set
    End Property
    Public Property Sum() As Decimal
        Get
            Return _Sum
        End Get
        Set(ByVal value As Decimal)
            _Sum = value
        End Set
    End Property
    Public Property Max() As Decimal
        Get
            Return _Max
        End Get
        Set(ByVal value As Decimal)
            If _Max = Nothing Then
                _Max = value
            Else
                If value > _Max Then _Max = value
            End If
        End Set
    End Property
    Public Property Min() As Decimal
        Get
            Return _Min
        End Get
        Set(ByVal value As Decimal)
            If _Min = Nothing Then
                _Min = value
            Else
                If value < _Min Then _Min = value
            End If
        End Set
    End Property
    Public Property AveValues() As List(Of Decimal)
        Get
            Return _AveValues
        End Get
        Set(ByVal value As List(Of Decimal))
            _AveValues = value
        End Set
    End Property
    Public Function Average() As Decimal

        If _AveValues.Count = 0 Then Return 0
        Return CDec(FormatNumber(_AveValues.Sum / _AveValues.Count, 2))

    End Function

    Public Sub ResetGroupTotals()

        _Count = 0
        _Sum = 0
        _Max = Nothing
        _Min = Nothing

    End Sub

    Public Function HeaderText() As String

        If _ColumnText.Value.Length > 0 Then Return _ColumnText.Value
        If _BOColumn Is Nothing Then Return String.Empty
        Return _BOColumn.HeaderText.ToString.Trim

    End Function
    Public Function DecimalPlaces() As Integer

        If _BOColumn Is Nothing Then Return 0
        Return _BOColumn.DecimalPlaces

    End Function
    Public Function ColumnType() As OasysDBBO.ColumnTypes

        If _BOColumn Is Nothing Then Return Nothing
        Return _BOColumn.ColumnType

    End Function

#End Region

End Class
