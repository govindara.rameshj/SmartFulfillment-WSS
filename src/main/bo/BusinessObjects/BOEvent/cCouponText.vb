﻿<Serializable()> Public Class cCouponText

    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "COUPONTEXT"
        BOFields.Add(_CouponID)
        BOFields.Add(_SequenceNo)
        BOFields.Add(_PrintSize)
        BOFields.Add(_TextAlignment)
        BOFields.Add(_PrintText)
        BOFields.Add(_Deleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEvent)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEvent)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEvent))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEvent(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CouponID As New ColField(Of String)("COUPONID", "", "Coupon ID", True, False)
    Private _SequenceNo As New ColField(Of String)("SEQUENCENO", "", "Sequence No", True, False)
    Private _PrintSize As New ColField(Of String)("PRINTSIZE", "", "Print Size", False, False)
    Private _TextAlignment As New ColField(Of String)("TEXTALIGN", "", "Text Align", False, False)
    Private _PrintText As New ColField(Of String)("PRINTTEXT", "", "Print Text", False, False)
    Private _Deleted As New ColField(Of Boolean)("DELETED", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property CouponID() As ColField(Of String)
        Get
            CouponID = _CouponID
        End Get
        Set(ByVal value As ColField(Of String))
            _CouponID = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of String)
        Get
            SequenceNo = _SequenceNo
        End Get
        Set(ByVal value As ColField(Of String))
            _SequenceNo = value
        End Set
    End Property
    Public Property PrintSize() As ColField(Of String)
        Get
            PrintSize = _PrintSize
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintSize = value
        End Set
    End Property
    Public Property TextAlignment() As ColField(Of String)
        Get
            TextAlignment = _TextAlignment
        End Get
        Set(ByVal value As ColField(Of String))
            _TextAlignment = value
        End Set
    End Property
    Public Property PrintText() As ColField(Of String)
        Get
            PrintText = _PrintText
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintText = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Deleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_Deleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
