﻿<Serializable()> Public Class cEventMixMatch
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTMMG"
        BOFields.Add(_ID)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventMixMatch)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventMixMatch)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventMixMatch))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventMixMatch(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of String)("MMGN", "", "Mix Match Group", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU", True, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    ''' <summary>
    ''' Being deprecated.  Still used by pre referral 61 code changes.
    ''' Use MixAndMatchGroupNumber for all new functionality.
    ''' </summary>
    Public Property EventMixMatchID() As ColField(Of String)
        Get
            EventMixMatchID = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    ''' <summary>
    ''' Being deprecated.  Still used by pre referral 61 code changes.
    ''' Use SkuNumber for all new functionality.
    ''' </summary>
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property MixAndMatchGroupNumber() As ColField(Of String)
        Get
            MixAndMatchGroupNumber = _ID
        End Get
        Set(ByVal value As ColField(Of String))
            _ID = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetMixMatchGroup(ByVal MixAndMatchGroupNumber As String) As List(Of BOEvent.cEventMixMatch)
        Dim lstMMGroup As List(Of BOEvent.cEventMixMatch)
        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ID, MixAndMatchGroupNumber)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            lstMMGroup = LoadMatches()
            If lstMMGroup.Count > 0 Then
                Return lstMMGroup
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Trace.WriteLine("cEventMixMatch - GetMixMatchGroup exception " & ex.Message)
            Return Nothing
        End Try

    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
