<Serializable()> Public Class cEventEnquiry
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTENQ"
        BOFields.Add(_Priority)
        BOFields.Add(_EventNumber)
        BOFields.Add(_DealGroupNumber)
        BOFields.Add(_EventOrItemType)
        BOFields.Add(_MandMGrpNumOrHierEnt)
        BOFields.Add(_DatcKey)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_BuyQuantity)
        BOFields.Add(_SpendLvlOrGetQty)
        BOFields.Add(_DiscountType)
        BOFields.Add(_Various)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventEnquiry)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventEnquiry)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventEnquiry))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventEnquiry(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Priority As New ColField(Of String)("PRIO", "", "Priority", True, False)
    Private _EventNumber As New ColField(Of String)("NUMB", "", "Event Number", True, False)
    Private _DealGroupNumber As New ColField(Of String)("DLGN", "", "Deal Group No", False, False)
    Private _EventOrItemType As New ColField(Of String)("ETYP", "", "Event Type", False, False)
    Private _MandMGrpNumOrHierEnt As New ColField(Of String)("MMHS", "", "Group Type", False, False)
    Private _DatcKey As New ColField(Of String)("DATC", "", "DATC Key", False, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU", False, False)
    Private _BuyQuantity As New ColField(Of Decimal)("DAT1", 0, "Buy Quantity", False, False)
    Private _SpendLvlOrGetQty As New ColField(Of Decimal)("DAT2", 0, "Spend Level/ Get Quantity", False, False)
    Private _DiscountType As New ColField(Of String)("DTYP", "", "Discount Type", False, False)
    Private _Various As New ColField(Of Decimal)("DAT3", 0, "Various", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)
    Private _BuyCoupon As New ColField(Of String)("BUYCPN", "0000000", False, False)
    Private _GetCoupon As New ColField(Of String)("GetCPN", "0000000", False, False)
#End Region

#Region "Field Properties"

    Public Property Priority() As ColField(Of String)
        Get
            Priority = _Priority
        End Get
        Set(ByVal value As ColField(Of String))
            _Priority = value
        End Set
    End Property
    Public Property EventNumber() As ColField(Of String)
        Get
            EventNumber = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property DealGroupNum() As ColField(Of String)
        Get
            DealGroupNum = _DealGroupNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DealGroupNumber = value
        End Set
    End Property
    Public Property EventOrItemType() As ColField(Of String)
        Get
            EventOrItemType = _EventOrItemType
        End Get
        Set(ByVal value As ColField(Of String))
            _EventOrItemType = value
        End Set
    End Property
    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code changes.
    ''' Use MixAndMatchGroupNumberOrHierarchyEntry for all new functionality.
    ''' </summary>
    Public Property MandMGrpNumOrHierEnt() As ColField(Of String)
        Get
            MandMGrpNumOrHierEnt = _MandMGrpNumOrHierEnt
        End Get
        Set(ByVal value As ColField(Of String))
            _MandMGrpNumOrHierEnt = value
        End Set
    End Property
    Public Property MixAndMatchGroupNumberOrHierarchyEntry() As ColField(Of String)
        Get
            MixAndMatchGroupNumberOrHierarchyEntry = _MandMGrpNumOrHierEnt
        End Get
        Set(ByVal value As ColField(Of String))
            _MandMGrpNumOrHierEnt = value
        End Set
    End Property
    Public Property DATCKey() As ColField(Of String)
        Get
            DATCKey = _DatcKey
        End Get
        Set(ByVal value As ColField(Of String))
            _DatcKey = value
        End Set
    End Property
    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code changes.
    ''' Use SkuNumber for all new functionality.
    ''' </summary>
    Public Property SKUN() As ColField(Of String)
        Get
            SKUN = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property BuyQuantity() As ColField(Of Decimal)
        Get
            BuyQuantity = _BuyQuantity
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BuyQuantity = value
        End Set
    End Property
    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code changes.
    ''' Use SpendLevelOrGetQuantitty for all new functionality.
    ''' </summary>
    Public Property SpendLvlOrGetQty() As ColField(Of Decimal)
        Get
            SpendLvlOrGetQty = _SpendLvlOrGetQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SpendLvlOrGetQty = value
        End Set
    End Property
    Public Property SpendLevelOrGetQuantitty() As ColField(Of Decimal)
        Get
            SpendLevelOrGetQuantitty = _SpendLvlOrGetQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SpendLvlOrGetQty = value
        End Set
    End Property
    Public Property DiscountType() As ColField(Of String)
        Get
            DiscountType = _DiscountType
        End Get
        Set(ByVal value As ColField(Of String))
            _DiscountType = value
        End Set
    End Property
    Public Property Various() As ColField(Of Decimal)
        Get
            Various = _Various
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Various = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

    Public Property BuyCoupon() As ColField(Of String)
        Get
            BuyCoupon = _BuyCoupon
        End Get
        Set(ByVal value As ColField(Of String))
            _BuyCoupon = value
        End Set
    End Property

    Public Property GetCoupon() As ColField(Of String)
        Get
            GetCoupon = _GetCoupon
        End Get
        Set(ByVal value As ColField(Of String))
            _GetCoupon = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function DeleteAll() As Boolean
        'Delete all the records 
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)

            NoRecs = Oasys3DB.Delete()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cEventEnquiry - DeleteAll exception " & ex.Message)
            Return True
        End Try



    End Function

    Public Function AddRecord(ByVal PRIO As String, ByVal NUMB As String, ByVal DLGN As String, ByVal ETYP As String, _
                              ByVal MMHS As String, ByVal DATC As String, ByVal SKU As String, ByVal DAT1 As Decimal, _
                              ByVal DAT2 As Decimal, ByVal DTYP As String, ByVal DAT3 As Decimal) As Boolean
        'Add a record
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim Referral61AndAfter As Boolean

        Try
            Referral61AndAfter = Cts.Oasys.Core.System.Parameter.GetBoolean(-61)
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
            Oasys3DB.SetColumnAndValueParameter(Priority.ColumnName, PRIO)
            Oasys3DB.SetColumnAndValueParameter(EventNumber.ColumnName, NUMB)
            Oasys3DB.SetColumnAndValueParameter(DealGroupNum.ColumnName, DLGN)
            Oasys3DB.SetColumnAndValueParameter(EventOrItemType.ColumnName, ETYP)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(MixAndMatchGroupNumberOrHierarchyEntry.ColumnName, MMHS)
            Else
                Oasys3DB.SetColumnAndValueParameter(MandMGrpNumOrHierEnt.ColumnName, MMHS)
            End If
            Oasys3DB.SetColumnAndValueParameter(DATCKey.ColumnName, DATC)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(SkuNumber.ColumnName, SKU)
            Else
                Oasys3DB.SetColumnAndValueParameter(SKUN.ColumnName, SKU)
            End If
            Oasys3DB.SetColumnAndValueParameter(BuyQuantity.ColumnName, DAT1)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(SpendLevelOrGetQuantitty.ColumnName, DAT2)
            Else
                Oasys3DB.SetColumnAndValueParameter(SpendLvlOrGetQty.ColumnName, DAT2)
            End If
            Oasys3DB.SetColumnAndValueParameter(DiscountType.ColumnName, DTYP)
            Oasys3DB.SetColumnAndValueParameter(Various.ColumnName, DAT3)
            Oasys3DB.SetColumnAndValueParameter(Deleted.ColumnName, False)

            NoRecs = Oasys3DB.Insert()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cEventEnquiry - DeleteAll exception " & ex.Message)
            Return True
        End Try
    End Function

    Public Function AddRecord(ByVal PRIO As String, ByVal NUMB As String, ByVal DLGN As String, ByVal ETYP As String, _
                              ByVal MMHS As String, ByVal DATC As String, ByVal SKU As String, ByVal DAT1 As Decimal, _
                              ByVal DAT2 As Decimal, ByVal DTYP As String, ByVal DAT3 As Decimal, _
                              ByVal BuyCouponId As String, ByVal GetCouponId As String) As Boolean
        'Add a record
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim Referral61AndAfter As Boolean

        Try
            Referral61AndAfter = Cts.Oasys.Core.System.Parameter.GetBoolean(-61)
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
            Oasys3DB.SetColumnAndValueParameter(Priority.ColumnName, PRIO)
            Oasys3DB.SetColumnAndValueParameter(EventNumber.ColumnName, NUMB)
            Oasys3DB.SetColumnAndValueParameter(DealGroupNum.ColumnName, DLGN)
            Oasys3DB.SetColumnAndValueParameter(EventOrItemType.ColumnName, ETYP)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(MixAndMatchGroupNumberOrHierarchyEntry.ColumnName, MMHS)
            Else
                Oasys3DB.SetColumnAndValueParameter(MandMGrpNumOrHierEnt.ColumnName, MMHS)
            End If
            Oasys3DB.SetColumnAndValueParameter(DATCKey.ColumnName, DATC)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(SkuNumber.ColumnName, SKU)
            Else
                Oasys3DB.SetColumnAndValueParameter(SKUN.ColumnName, SKU)
            End If
            Oasys3DB.SetColumnAndValueParameter(BuyQuantity.ColumnName, DAT1)
            If Referral61AndAfter Then
                Oasys3DB.SetColumnAndValueParameter(SpendLevelOrGetQuantitty.ColumnName, DAT2)
            Else
                Oasys3DB.SetColumnAndValueParameter(SpendLvlOrGetQty.ColumnName, DAT2)
            End If
            Oasys3DB.SetColumnAndValueParameter(DiscountType.ColumnName, DTYP)
            Oasys3DB.SetColumnAndValueParameter(Various.ColumnName, DAT3)
            Oasys3DB.SetColumnAndValueParameter(Deleted.ColumnName, False)

            Oasys3DB.SetColumnAndValueParameter(BuyCoupon.ColumnName, BuyCouponId)
            Oasys3DB.SetColumnAndValueParameter(GetCoupon.ColumnName, GetCouponId)

            NoRecs = Oasys3DB.Insert()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cEventEnquiry - DeleteAll exception " & ex.Message)
            Return True
        End Try
    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
