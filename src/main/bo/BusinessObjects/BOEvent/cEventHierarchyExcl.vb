﻿<Serializable()> Public Class cEventHierarchyExcl
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTHEX"
        BOFields.Add(_EventNumber)
        BOFields.Add(_HieCategory)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventHierarchyExcl)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventHierarchyExcl)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventHierarchyExcl))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventHierarchyExcl(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _EventNumber As New ColField(Of String)("NUMB", "", "Event Number", True, False)
    Private _HieCategory As New ColField(Of String)("HIER", "", "Hierarchy Code", True, False)
    Private _SkuNumber As New ColField(Of String)("ITEM", "", "SKU", True, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property EventNo() As ColField(Of String)
        Get
            EventNo = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property HierCategory() As ColField(Of String)
        Get
            HierCategory = _HieCategory
        End Get
        Set(ByVal value As ColField(Of String))
            _HieCategory = value
        End Set
    End Property
    ''' <summary>
    '''  Being depreceated.  Still in use by pre referral 61 code changes.
    ''' Use SkuNumber instead for all new functionality.
    ''' </summary>
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code changes.
    ''' Use Exclude in all new functionality.
    ''' </summary>
    Public Function ExcludeOriginal(ByVal Hier As String) As DataTable
        Dim dt As DataTable
        Dim ds As DataSet
        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieCategory, Hier)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            ds = GetSQLSelectDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                dt = ds.Tables(0)
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cEventHierarchyExcl - Exclude exception" & ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function Exclude(ByVal strEventNumber As String) As List(Of BOEvent.cEventHierarchyExcl)
        Dim ExcludedSkuList As List(Of BOEvent.cEventHierarchyExcl)

        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EventNumber, strEventNumber)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            ExcludedSkuList = LoadMatches()
            If ExcludedSkuList.Count > 0 Then
                Return ExcludedSkuList
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cEventHierarchyExcl - Exclude exception" & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
