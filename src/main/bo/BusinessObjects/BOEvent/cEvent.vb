﻿<Serializable()> Public Class cEvent
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTMAS"
        BOFields.Add(_EventType)
        BOFields.Add(_EventKey1)
        BOFields.Add(_EventKey2)
        BOFields.Add(_EventNumber)
        BOFields.Add(_Priority)
        BOFields.Add(_Deleted)
        BOFields.Add(_TimeOrDayRelated)
        BOFields.Add(_StartDate)
        BOFields.Add(_EndDate)
        BOFields.Add(_SpecialPrice)
        BOFields.Add(_BuyQuantity)
        BOFields.Add(_GetQuantity)
        BOFields.Add(_DiscountAmount)
        BOFields.Add(_PercentageDiscount)
        BOFields.Add(_BuyCouponID)
        BOFields.Add(_SellCouponID)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEvent)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEvent)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEvent))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEvent(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _EventType As New ColField(Of String)("TYPE", "", "Event Type", True, False)
    Private _EventKey1 As New ColField(Of String)("KEY1", "", "Event Key 1", True, False)
    Private _EventKey2 As New ColField(Of String)("KEY2", "", "Event Key 2", True, False)
    Private _EventNumber As New ColField(Of String)("NUMB", "", "Event Number", True, False)
    Private _Priority As New ColField(Of String)("PRIO", "", "Priority", False, False)
    Private _Deleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)
    Private _TimeOrDayRelated As New ColField(Of Boolean)("IDOW", False, "Time/Day Related", False, False)
    Private _StartDate As New ColField(Of Date)("SDAT", Nothing, "Start Date", False, False)
    Private _EndDate As New ColField(Of Date)("EDAT", Nothing, "End Date", False, False)
    Private _SpecialPrice As New ColField(Of Decimal)("PRIC", 0, "Special Price", False, False, 2)
    Private _BuyQuantity As New ColField(Of Decimal)("BQTY", 0, "Buy Qty", False, False)
    Private _GetQuantity As New ColField(Of Decimal)("GQTY", 0, "Get Qty", False, False)
    Private _DiscountAmount As New ColField(Of Decimal)("VDIS", 0, "Discount Amount", False, False, 2)
    Private _PercentageDiscount As New ColField(Of Decimal)("PDIS", 0, "Discount %", False, False, 2)
    Private _BuyCouponID As New ColField(Of String)("BUYCPN", "0000000", "Buy Coupon ID", False, False)
    Private _SellCouponID As New ColField(Of String)("GETCPN", "0000000", "Sell Coupon ID", False, False)

#End Region

#Region "Field Properties"

    Public Property EventType() As ColField(Of String)
        Get
            EventType = _EventType
        End Get
        Set(ByVal value As ColField(Of String))
            _EventType = value
        End Set
    End Property
    Public Property EventKey1() As ColField(Of String)
        Get
            EventKey1 = _EventKey1
        End Get
        Set(ByVal value As ColField(Of String))
            _EventKey1 = value
        End Set
    End Property
    Public Property EventKey2() As ColField(Of String)
        Get
            EventKey2 = _EventKey2
        End Get
        Set(ByVal value As ColField(Of String))
            _EventKey2 = value
        End Set
    End Property
    Public Property EventNo() As ColField(Of String)
        Get
            EventNo = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property Priority() As ColField(Of String)
        Get
            Priority = _Priority
        End Get
        Set(ByVal value As ColField(Of String))
            _Priority = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Deleted = value
        End Set
    End Property
    Public Property TimeOrDayRelated() As ColField(Of Boolean)
        Get
            TimeOrDayRelated = _TimeOrDayRelated
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _TimeOrDayRelated = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property SpecialPrice() As ColField(Of Decimal)
        Get
            SpecialPrice = _SpecialPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SpecialPrice = value
        End Set
    End Property
    Public Property BuyQuantity() As ColField(Of Decimal)
        Get
            BuyQuantity = _BuyQuantity
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BuyQuantity = value
        End Set
    End Property
    Public Property GetQuantity() As ColField(Of Decimal)
        Get
            GetQuantity = _GetQuantity
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _GetQuantity = value
        End Set
    End Property
    Public Property DiscountAmount() As ColField(Of Decimal)
        Get
            DiscountAmount = _DiscountAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DiscountAmount = value
        End Set
    End Property
    ''' <summary>
    ''' Being deprecated.  Still in use for code pre-dating referral 61 changes.
    ''' Use DiscountPercentage for all new situation.
    ''' </summary>
    Public Property PercentageDiscount() As ColField(Of Decimal)
        Get
            PercentageDiscount = _PercentageDiscount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PercentageDiscount = value
        End Set
    End Property
    Public Property DiscountPercentage() As ColField(Of Decimal)
        Get
            DiscountPercentage = _PercentageDiscount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PercentageDiscount = value
        End Set
    End Property

    Public Property BuyCouponID() As ColField(Of String)
        Get
            BuyCouponID = _BuyCouponID
        End Get
        Set(ByVal value As ColField(Of String))
            _BuyCouponID = value
        End Set
    End Property
    Public Property SellCouponID() As ColField(Of String)
        Get
            SellCouponID = _SellCouponID
        End Get
        Set(ByVal value As ColField(Of String))
            _SellCouponID = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Function GetEventMaster(ByVal EventNum As String) As List(Of BOEvent.cEvent)
        Dim ListEvent As List(Of BOEvent.cEvent)

        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EventNumber, EventNum)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, Deleted, False)
            ListEvent = LoadMatches()
            If ListEvent.Count > 0 Then
                Return ListEvent
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Trace.WriteLine("cEvent - getEventMaster exception " & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_Deleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
