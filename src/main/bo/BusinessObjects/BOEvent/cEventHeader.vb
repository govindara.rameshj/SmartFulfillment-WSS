﻿<Serializable()> Public Class cEventHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTHDR"
        BOFields.Add(_Number)
        BOFields.Add(_Description)
        BOFields.Add(_Priority)
        BOFields.Add(_StartDate)
        BOFields.Add(_StartTime)
        BOFields.Add(_EndDate)
        BOFields.Add(_EndTime)
        BOFields.Add(_ActiveDays1)
        BOFields.Add(_ActiveDays2)
        BOFields.Add(_ActiveDays3)
        BOFields.Add(_ActiveDays4)
        BOFields.Add(_ActiveDays5)
        BOFields.Add(_ActiveDays6)
        BOFields.Add(_ActiveDays7)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, False, 1, 0, 0, 0, "", 0, "Event No")
    Private _Description As New ColField(Of String)("DESCR", "", False, False, 1, 0, True, False, 2, 0, 0, 0, "", 0, "Description")
    Private _Priority As New ColField(Of String)("PRIO", "", True, False, 1, 0, True, False, 3, 0, 0, 0, "", 0, "Priority")
    Private _StartDate As New ColField(Of Date)("SDAT", Nothing, False, False, 1, 0, True, False, 4, 0, 0, 0, "", 0, "Start Date")
    Private _StartTime As New ColField(Of String)("STIM", "", False, False, 1, 0, True, False, 5, 0, 0, 0, "", 0, "Start Time")
    Private _EndDate As New ColField(Of Date)("EDAT", Nothing, False, False, 1, 0, True, False, 6, 0, 0, 0, "", 0, "Ending Date")
    Private _EndTime As New ColField(Of String)("ETIM", "", False, False, 1, 0, True, False, 7, 0, 0, 0, "", 0, "Ending Time")
    Private _ActiveDays1 As New ColField(Of Boolean)("DACT1", False, False, False, 1, 0, True, False, 8, 0, 0, 0, "", 0, "Active Day1")
    Private _ActiveDays2 As New ColField(Of Boolean)("DACT2", False, False, False, 1, 0, True, False, 9, 0, 0, 0, "", 0, "Active Day2")
    Private _ActiveDays3 As New ColField(Of Boolean)("DACT3", False, False, False, 1, 0, True, False, 10, 0, 0, 0, "", 0, "Active Day3")
    Private _ActiveDays4 As New ColField(Of Boolean)("DACT4", False, False, False, 1, 0, True, False, 11, 0, 0, 0, "", 0, "Active Day4")
    Private _ActiveDays5 As New ColField(Of Boolean)("DACT5", False, False, False, 1, 0, True, False, 12, 0, 0, 0, "", 0, "Active Day5")
    Private _ActiveDays6 As New ColField(Of Boolean)("DACT6", False, False, False, 1, 0, True, False, 13, 0, 0, 0, "", 0, "Active Day6")
    Private _ActiveDays7 As New ColField(Of Boolean)("DACT7", False, False, False, 1, 0, True, False, 14, 0, 0, 0, "", 0, "Active Day7")
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, False, False, 1, 0, True, False, 15, 0, 0, 0, "", 0, "Deleted")

#End Region

#Region "Public Properties"

    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code changes.
    ''' Use EventNo for all new functionality.
    ''' </summary>
    ''' <value></value>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Property EventNoID() As ColField(Of String)
        Get
            EventNoID = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property EventNo() As ColField(Of String)
        Get
            EventNo = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Priority() As ColField(Of String)
        Get
            Priority = _Priority
        End Get
        Set(ByVal value As ColField(Of String))
            _Priority = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property StartTime() As ColField(Of String)
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal value As ColField(Of String))
            _StartTime = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndDate = value
        End Set
    End Property
    Public Property EndTime() As ColField(Of String)
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal value As ColField(Of String))
            _EndTime = value
        End Set
    End Property
    Public Property ActiveDays1() As ColField(Of Boolean)
        Get
            ActiveDays1 = _ActiveDays1
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays1 = value
        End Set
    End Property
    Public Property ActiveDays2() As ColField(Of Boolean)
        Get
            ActiveDays2 = _ActiveDays2
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays2 = value
        End Set
    End Property
    Public Property ActiveDays3() As ColField(Of Boolean)
        Get
            ActiveDays3 = _ActiveDays3
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays3 = value
        End Set
    End Property
    Public Property ActiveDays4() As ColField(Of Boolean)
        Get
            ActiveDays4 = _ActiveDays4
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays4 = value
        End Set
    End Property
    Public Property ActiveDays5() As ColField(Of Boolean)
        Get
            ActiveDays5 = _ActiveDays5
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays5 = value
        End Set
    End Property
    Public Property ActiveDays6() As ColField(Of Boolean)
        Get
            ActiveDays6 = _ActiveDays6
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays6 = value
        End Set
    End Property
    Public Property ActiveDays7() As ColField(Of Boolean)
        Get
            ActiveDays7 = _ActiveDays7
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActiveDays7 = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property


#End Region

#Region "Methods Class specific"



#End Region

#Region "Methods"

    Public Function GetRecordsToDelete(ByVal DateHorizion As Date) As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Me._EndDate, DateHorizion)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._IsDeleted, True)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Function GetEventHeaders(ByVal today As Date) As List(Of BOEvent.cEventHeader)
        Dim ListEventHeader As List(Of BOEvent.cEventHeader)

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Me._EndDate, today)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._IsDeleted, False)
        ListEventHeader = LoadMatches()
        If ListEventHeader.Count > 0 Then
            Return ListEventHeader
        Else
            Return Nothing
        End If

    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
