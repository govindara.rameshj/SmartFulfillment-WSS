﻿<Serializable()> Public Class cCouponMaster

    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "COUPONMASTER"
        BOFields.Add(_CouponID)
        BOFields.Add(_Description)
        BOFields.Add(_StoreGenerated)
        BOFields.Add(_SerialNo)
        BOFields.Add(_ExclusiveCoupon)
        BOFields.Add(_ReUsable)
        BOFields.Add(_CollectInfo)
        BOFields.Add(_Deleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEvent)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEvent)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEvent))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEvent(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CouponID As New ColField(Of String)("COUPONID", "", "Coupon ID", True, False)
    Private _Description As New ColField(Of String)("DESCRIPTION", "", "Description", False, False)
    Private _StoreGenerated As New ColField(Of Boolean)("STOREGEN", False, "Store Generated", False, False)
    Private _SerialNo As New ColField(Of Boolean)("SERIALNO", False, "Has Serial No", False, False)
    Private _ExclusiveCoupon As New ColField(Of Boolean)("EXCCOUPON", False, "Exclusive Coupon", False, False)
    Private _ReUsable As New ColField(Of Boolean)("REUSABLE", False, "Re-Usable", False, False)
    Private _CollectInfo As New ColField(Of Boolean)("COLLECTINFO", False, "Info Collected", False, False)
    Private _Deleted As New ColField(Of Boolean)("DELETED", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property CouponID() As ColField(Of String)
        Get
            CouponID = _CouponID
        End Get
        Set(ByVal value As ColField(Of String))
            _CouponID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property StoreGenerated() As ColField(Of Boolean)
        Get
            StoreGenerated = _StoreGenerated
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _StoreGenerated = value
        End Set
    End Property
    Public Property SerialNo() As ColField(Of Boolean)
        Get
            SerialNo = _SerialNo
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SerialNo = value
        End Set
    End Property
    Public Property ExclusiveCoupon() As ColField(Of Boolean)
        Get
            ExclusiveCoupon = _ExclusiveCoupon
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ExclusiveCoupon = value
        End Set
    End Property
    Public Property ReUsable() As ColField(Of Boolean)
        Get
            ReUsable = _ReUsable
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReUsable = value
        End Set
    End Property
    Public Property CollectInfo() As ColField(Of Boolean)
        Get
            CollectInfo = _CollectInfo
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CollectInfo = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReUsable = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_Deleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
