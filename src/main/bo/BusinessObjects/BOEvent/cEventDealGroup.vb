﻿<Serializable()> Public Class cEventDealGroup
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTDLG"
        BOFields.Add(_EventNumber)
        BOFields.Add(_DealGroupNumber)
        BOFields.Add(_DealType)
        BOFields.Add(_ItemKey)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_Quantity)
        BOFields.Add(_ErosionValue)
        BOFields.Add(_ErosionPercentage)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventDealGroup)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventDealGroup)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventDealGroup))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventDealGroup(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _EventNumber As New ColField(Of String)("NUMB", "", "Event Number", True, False)
    Private _DealGroupNumber As New ColField(Of String)("DLGN", "", "Deal Group No.", True, False)
    Private _DealType As New ColField(Of String)("TYPE", "", "Deal Type", True, False)
    Private _ItemKey As New ColField(Of String)("KEY1", "", "Item Key", True, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)
    Private _Quantity As New ColField(Of Decimal)("QUAN", 0, "Quantity", False, False)
    Private _ErosionValue As New ColField(Of Decimal)("VERO", 0, "Erosion Value", False, False)
    Private _ErosionPercentage As New ColField(Of Decimal)("PERO", 0, "Erosion %", False, False)

#End Region

#Region "Field Properties"

    Public Property EventNo() As ColField(Of String)
        Get
            EventNo = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property DealGroupNo() As ColField(Of String)
        Get
            DealGroupNo = _DealGroupNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DealGroupNumber = value
        End Set
    End Property
    Public Property DealType() As ColField(Of String)
        Get
            DealType = _DealType
        End Get
        Set(ByVal value As ColField(Of String))
            _DealType = value
        End Set
    End Property
    Public Property ItemKey() As ColField(Of String)
        Get
            ItemKey = _ItemKey
        End Get
        Set(ByVal value As ColField(Of String))
            _ItemKey = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Decimal)
        Get
            Quantity = _Quantity
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Quantity = value
        End Set
    End Property
    Public Property ErosionValue() As ColField(Of Decimal)
        Get
            ErosionValue = _ErosionValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ErosionValue = value
        End Set
    End Property
    Public Property ErorsionPercentage() As ColField(Of Decimal)
        Get
            ErorsionPercentage = _ErosionPercentage
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ErosionPercentage = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function HKGetMixMatch(ByVal EventNoId As String) As DataTable
        Dim ds As DataSet

        Me.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EventNumber, EventNoId)
        Me.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, DealType, "M")
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Being deprecated.  Still in use by pre referral 61 code.
    ''' Use GetDealGroup(ByVal strEventNumber As String, ByVal strDealGroupNumber As String) As List(Of BOEvent.cEventDealGroup)
    ''' overload for all new functionality
    ''' </summary>
    ''' <param name="DealGPNum"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function GetDealGroup(ByVal DealGPNum As String) As List(Of BOEvent.cEventDealGroup)
        Dim lstDealGroup As List(Of BOEvent.cEventDealGroup)
        Dim lngDealGroup As Long = CLng(Val(DealGPNum))

        Try
            DealGPNum = lngDealGroup.ToString.PadLeft(6, "0"c)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DealGroupNumber, DealGPNum)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            lstDealGroup = LoadMatches()
            If lstDealGroup.Count > 0 Then
                Return lstDealGroup
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cEventDealGroup - GetDealGroup exception " & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function GetDealGroup(ByVal strEventNumber As String, ByVal strDealGroupNumber As String) As List(Of BOEvent.cEventDealGroup)
        Dim lstDealGroup As List(Of BOEvent.cEventDealGroup)

        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EventNumber, strEventNumber)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DealGroupNumber, strDealGroupNumber.Remove(0, 2))
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            lstDealGroup = LoadMatches()
            If lstDealGroup.Count > 0 Then
                Return lstDealGroup
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cEventDealGroup - GetDealGroup exception " & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Sub FlagAllAsDeleted()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 06/10/2010
        ' Referral No : 467
        ' Notes       : Database connector was not being reset, executing incorrect DML code
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Oasys3DB.ClearAllParameters()


        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
