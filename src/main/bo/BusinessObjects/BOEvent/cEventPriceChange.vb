﻿<Serializable()> Public Class cEventPriceChange
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EVTCHG"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_StartDate)
        BOFields.Add(_Priority)
        BOFields.Add(_EventNumber)
        BOFields.Add(_EndDate)
        BOFields.Add(_EventPrice)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cEventPriceChange)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cEventPriceChange)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cEventPriceChange))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cEventPriceChange(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU", True, False)
    Private _StartDate As New ColField(Of Date)("SDAT", Nothing, "Start Date", True, False)
    Private _Priority As New ColField(Of String)("PRIO", "", "Priority", True, False)
    Private _EventNumber As New ColField(Of String)("NUMB", "", "Event Number", True, False)
    Private _EndDate As New ColField(Of Date?)("EDAT", Nothing, "End Date", False, False)
    Private _EventPrice As New ColField(Of Decimal)("PRIC", 0, "Event Price", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("IDEL", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property StartDate() As ColField(Of Date)
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _StartDate = value
        End Set
    End Property
    Public Property Priority() As ColField(Of String)
        Get
            Priority = _Priority
        End Get
        Set(ByVal value As ColField(Of String))
            _Priority = value
        End Set
    End Property
    Public Property EventNumber() As ColField(Of String)
        Get
            EventNumber = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property EndDate() As ColField(Of Date?)
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal value As ColField(Of Date?))
            _EndDate = value
        End Set
    End Property
    Public Property EventPrice() As ColField(Of Decimal)
        Get
            EventPrice = _EventPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EventPrice = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub DeleteChanges()
        Dim oPriceChange As New BOStock.cPriceChange(Oasys3DB)

        'Just get ones that can be deleted
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, True)
        Dim priceChanges As List(Of BOEvent.cEventPriceChange) = LoadMatches()
        If priceChanges.Count = 0 Then
            Exit Sub
        End If

        'get stock items for these price changes
        Dim skuNumbers As New ArrayList
        For Each priceChange As cEventPriceChange In priceChanges
            If Not skuNumbers.Contains(priceChange.PartCode.Value) Then skuNumbers.Add(priceChange.PartCode.Value)
        Next

        Dim stocks As New BOStock.cStock(Oasys3DB)
        stocks.AddLoadField(stocks.SkuNumber)
        stocks.AddLoadField(stocks.RetailPricePriority)
        stocks.AddLoadField(stocks.RetailPriceEventNo)
        stocks.AddLoadFilter(clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
        stocks.Stocks = stocks.LoadMatches

        'loop over all stock items and see if can delete it
        For Each item As BOEvent.cEventPriceChange In priceChanges
            Dim stock As BOStock.cStock = stocks.Stock(item.PartCode.Value)
            If Not IsDBNull(item.EndDate.Value) Then 'Or item.EndDate.Value <> ""
                If stock Is Nothing Then
                    oPriceChange.DeleteUnappliedBySku(item.PartCode.Value)
                    DeleteRecord(item.PartCode.Value)
                Else
                    If Not (stock.RetailPricePriority.Value = item.Priority.Value AndAlso stock.RetailPriceEventNo.Value = item.EventNumber.Value) Then
                        oPriceChange.DeleteUnappliedBySku(item.PartCode.Value)
                        DeleteRecord(item.PartCode.Value)
                    End If
                End If
            End If
        Next item

    End Sub

    Public Function DeleteRecord(ByVal PartCode As String) As Boolean 

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, PartCode)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_IsDeleted.ColumnName, clsOasys3DB.eOperator.pEquals, True)
        Return (Oasys3DB.Delete() <> -1)

    End Function

    Public Function GetPriceChangeSku() As DataSet

        Dim ds As DataSet

        AddAggregateField(Oasys3.DB.clsOasys3DB.eAggregates.pAggDistinct, _SkuNumber, "SKU")

        ds = GetAggregateDataSet()

        Return ds

    End Function

    Public Function GetPriceChangeEvents(ByVal startDate As Date) As DataTable

        Dim dateString As String = startDate.ToString("yyyy-MM-dd")
        Dim sb As New StringBuilder
        sb.Append("SELECT SKUN, SDAT, PRIO, NUMB, EDAT, PRIC, IDEL FROM EVTCHG ")
        sb.Append("WHERE IDEL=0 ")
        sb.Append("AND ((SDAT<='" & dateString & "' AND EDAT>='" & dateString & "') ")
        sb.Append("OR (SDAT<='" & dateString & "' AND EDAT is null)) ")
        sb.Append("ORDER BY SKUN, PRIO DESC, NUMB DESC")

        Return Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

    End Function

    Public Function GetPriceChangeEvents(ByVal startDate As Date, ByVal days As Integer) As DataTable

        Dim start As String = startDate.ToString("yyyy-MM-dd")
        Dim ending As String = DateAdd(DateInterval.Day, days, startDate).ToString("yyyy-MM-dd")

        Dim sb As New StringBuilder
        sb.Append("select * from dbo.udf_EffectiveEVTCHGRecordsForDateRange( '" & start.ToString & "', " & days.ToString & ")")

        Trace.WriteLine("GetPriceChange is in new area. SQL=" & sb.ToString)
        Return Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

    End Function

    Public Function GetPriceChangeEvents(ByVal mysku As String, ByVal MySDAT As Date, ByVal days As Integer) As DataSet

        Dim myEDAT As Date = DateAdd(DateInterval.Day, days + 7, MySDAT)
        Dim sql As String = "Select * from " & TableName & " WHERE skun=" & mysku & " and ((SDAT <= '" & Format(myEDAT, "yyyy-MM-dd") & "') and ((EDAT >= '" & Format(myEDAT, "yyyy-MM-dd") & "') or (edat is NULL))  or (EDAT >= '" & Format(MySDAT, "yyyy-MM-dd") & "' and SDAT  <=  '" & Format(myEDAT, "yyyy-MM-dd") & " '))and idel = 0 "
        Return Oasys3DB.ExecuteSql(sql)

    End Function

    Public Function GetPriceChangeEvents(ByVal mysku As String, ByVal myPrice As Decimal, ByVal MySDAT As Date, ByVal days As Integer) As DataSet

        Dim myEDAT As Date = DateAdd(DateInterval.Day, days + 7, MySDAT)

        Dim sb As New StringBuilder
        sb.Append("Select * from " & TableName)
        sb.Append(" WHERE skun=" & mysku)
        sb.Append(" and PRIC<>" & myPrice)
        sb.Append(" and ((SDAT <= '" & Format(myEDAT, "yyyy-MM-dd") & "')")
        sb.Append(" and ((EDAT >= '" & Format(myEDAT, "yyyy-MM-dd") & "')")
        sb.Append(" or (edat is NULL)")
        sb.Append(" or (EDAT >= '" & Format(MySDAT, "yyyy-MM-dd") & "' and SDAT  <=  '" & Format(myEDAT, "yyyy-MM-dd") & " '))")
        sb.Append(" and idel = 0")

        Return Oasys3DB.ExecuteSql(sb.ToString)

    End Function

    Public Sub FlagAllAsDeleted()

        Oasys3DB.ClearAllParameters()

        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, 1)
        Oasys3DB.Update()

    End Sub

#End Region

End Class
