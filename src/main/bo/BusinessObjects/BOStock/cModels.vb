﻿<Serializable()> Public Class cModels
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "MODMAS"
        BOFields.Add(_ModelNumber)
        BOFields.Add(_Description)
        BOFields.Add(_AlphaKey)
        BOFields.Add(_GroupCode)
        BOFields.Add(_NormalSellPrice)
        BOFields.Add(_PriorSellPrice)
        BOFields.Add(_LastSold)
        BOFields.Add(_PriceEffectiveDate)
        BOFields.Add(_DateCreated)
        BOFields.Add(_DateObsolete)
        BOFields.Add(_DateDeleted)
        BOFields.Add(_StockOnOrder)
        BOFields.Add(_Sold)
        BOFields.Add(_ItemStatus)
        BOFields.Add(_ItemObsolete)
        BOFields.Add(_ItemDeleted)
        BOFields.Add(_HOCheckDigit)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStock)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStock)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStock))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStock(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("NUMB") : _ModelNumber.Value = CStr(drRow(dcColumn))
                    Case ("DESCR") : _Description.Value = drRow(dcColumn).ToString.Trim
                    Case ("ALPH") : _AlphaKey.Value = CStr(drRow(dcColumn))
                    Case ("GROU") : _GroupCode.Value = CStr(drRow(dcColumn))
                    Case ("PRIC") : _NormalSellPrice.Value = CDec(drRow(dcColumn))
                    Case ("PPRI") : _PriorSellPrice.Value = CDec(drRow(dcColumn))
                    Case ("DSOL") : _LastSold.Value = CDate(drRow(dcColumn))
                    Case ("DPRC") : _PriceEffectiveDate.Value = CDate(drRow(dcColumn))
                    Case ("DSET") : _DateCreated.Value = CDate(drRow(dcColumn))
                    Case ("DOBS") : _DateObsolete.Value = CDate(drRow(dcColumn))
                    Case ("DDEL") : _DateDeleted.Value = CDate(drRow(dcColumn))
                    Case ("ONOR") : _StockOnOrder.Value = CInt(drRow(dcColumn))
                    Case ("SOLD") : _Sold.Value = CInt(drRow(dcColumn))
                    Case ("ISTA") : _ItemStatus.Value = CStr(drRow(dcColumn))
                    Case ("IOBS") : _ItemObsolete.Value = CBool(drRow(dcColumn))
                    Case ("IDEL") : _ItemDeleted.Value = CBool(drRow(dcColumn))
                    Case ("CHKD") : _HOCheckDigit.Value = CStr(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _ModelNumber As New ColField(Of String)("NUMB", "000000", "Model Number", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _AlphaKey As New ColField(Of String)("ALPH", "", "Alpha Key", False, False)
    Private _GroupCode As New ColField(Of String)("GROU", "", "Group Code", False, False)
    Private _NormalSellPrice As New ColField(Of Decimal)("PRIC", 0, "Normal Sell Price", False, False, 2)
    Private _PriorSellPrice As New ColField(Of Decimal)("PPRI", 0, "Prior Sell Price", False, False, 2)
    Private _LastSold As New ColField(Of Date)("DSOL", Nothing, "Last Sold", False, False)
    Private _PriceEffectiveDate As New ColField(Of Date)("DPRC", Nothing, "Price Effective Date", False, False)
    Private _DateCreated As New ColField(Of Date)("DSET", Nothing, "Date Created", False, False)
    Private _DateObsolete As New ColField(Of Date)("DOBS", Nothing, "Date Obselete", False, False)
    Private _DateDeleted As New ColField(Of Date)("DDEL", Nothing, "Date Deleted", False, False)
    Private _StockOnOrder As New ColField(Of Integer)("ONOR", 0, "Stock On Order", False, False)
    Private _Sold As New ColField(Of Integer)("SOLD", 0, "Stock Sold", False, False)
    Private _ItemStatus As New ColField(Of String)("ISTA", "", "Item Status", False, False)
    Private _ItemObsolete As New ColField(Of Boolean)("IOBS", False, "Item Obselete", False, False)
    Private _ItemDeleted As New ColField(Of Boolean)("IDEL", False, "Item Deleted", False, False)
    Private _HOCheckDigit As New ColField(Of String)("CHKD", "", "HO Check Digit", False, False)

#End Region

#Region "Field Properties"

    Public Property ModelNumber() As ColField(Of String)
        Get
            ModelNumber = _ModelNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ModelNumber = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property AlphaKey() As ColField(Of String)
        Get
            AlphaKey = _AlphaKey
        End Get
        Set(ByVal value As ColField(Of String))
            _AlphaKey = value
        End Set
    End Property
    Public Property GroupCode() As ColField(Of String)
        Get
            GroupCode = _GroupCode
        End Get
        Set(ByVal value As ColField(Of String))
            _GroupCode = value
        End Set
    End Property
    Public Property NormalSellPrice() As ColField(Of Decimal)
        Get
            NormalSellPrice = _NormalSellPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NormalSellPrice = value
        End Set
    End Property
    Public Property PriorSellPrice() As ColField(Of Decimal)
        Get
            PriorSellPrice = _PriorSellPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriorSellPrice = value
        End Set
    End Property
    Public Property LastSold() As ColField(Of Date)
        Get
            LastSold = _LastSold
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastSold = value
        End Set
    End Property
    Public Property PriceEffectiveDate() As ColField(Of Date)
        Get
            PriceEffectiveDate = _PriceEffectiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _PriceEffectiveDate = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property DateObsolete() As ColField(Of Date)
        Get
            DateObsolete = _DateObsolete
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateObsolete = value
        End Set
    End Property
    Public Property DateDeleted() As ColField(Of Date)
        Get
            DateDeleted = _DateDeleted
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateDeleted = value
        End Set
    End Property
    Public Property StockOnOrder() As ColField(Of Integer)
        Get
            StockOnOrder = _StockOnOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnOrder = value
        End Set
    End Property
    Public Property Sold() As ColField(Of Integer)
        Get
            Sold = _Sold
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Sold = value
        End Set
    End Property
    Public Property ItemStatus() As ColField(Of String)
        Get
            ItemStatus = _ItemStatus
        End Get
        Set(ByVal value As ColField(Of String))
            _ItemStatus = value
        End Set
    End Property
    Public Property ItemObsolete() As ColField(Of Boolean)
        Get
            ItemObsolete = _ItemObsolete
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemObsolete = value
        End Set
    End Property
    Public Property ItemDeleted() As ColField(Of Boolean)
        Get
            ItemDeleted = _ItemDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemDeleted = value
        End Set
    End Property
    Public Property HOCheckDigit() As ColField(Of String)
        Get
            HOCheckDigit = _HOCheckDigit
        End Get
        Set(ByVal value As ColField(Of String))
            _HOCheckDigit = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetAllRecords() As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemObsolete, True)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Function NumberOfActiveModels() As Integer

        Oasys3DB.ClearAllParameters()
        Dim retdata As Object
        ClearLoadField()
        ClearLoadFilter()
        AddAggregateField(Oasys3.DB.clsOasys3DB.eAggregates.pAggCount, _ModelNumber, _ModelNumber.ColumnName)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemObsolete, False)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemDeleted, False)
        retdata = GetAggregateField()

        If DBNull.Value.Equals(retdata) Then
            Return 0
        Else
            Return CType(retdata, Integer)
        End If

    End Function

    Public Sub FlagAllAsObselete(ByVal ObsDate As Date)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ItemObsolete.ColumnName, 1)
        Oasys3DB.SetColumnAndValueParameter(_DateObsolete.ColumnName, ObsDate)
        Oasys3DB.SetWhereParameter(_ItemObsolete.ColumnName, clsOasys3DB.eOperator.pEquals, 0)
        Oasys3DB.Update()

    End Sub


#End Region


End Class


