﻿<Serializable()> Public Class cWeeklyStockMovement
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "WeeklyStockMovement"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_WeekNumber)
        BOFields.Add(_WeekEndingDate)
        BOFields.Add(_SalesValue)
        BOFields.Add(_SalesUnits)
        BOFields.Add(_ReceiptsValue)
        BOFields.Add(_ReceiptsQty)
        BOFields.Add(_AdjustmentValue)
        BOFields.Add(_AdjustmentQty)
        BOFields.Add(_InOutNetValue)
        BOFields.Add(_InOutNetQty)
        BOFields.Add(_ReturnsValue)
        BOFields.Add(_ReturnsQty)
        BOFields.Add(_BulkToSingleTransVal)
        BOFields.Add(_BulkToSingleTransQty)
        BOFields.Add(_PriceViolationsValue)
        BOFields.Add(_PriceChgMarkUpDwnVal)
        BOFields.Add(_CyclicalCountValue)
        BOFields.Add(_DrlAdjustmentValue)
        BOFields.Add(_EndingPrice)
        BOFields.Add(_EndingQty)
        BOFields.Add(_SingleMarkUpValue)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cWeeklyStockMovement)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cWeeklyStockMovement)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cWeeklyStockMovement))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cWeeklyStockMovement(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Part Code")
    Private _WeekNumber As New ColField(Of Decimal)("WKNO", 0, True, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Week No")
    Private _WeekEndingDate As New ColField(Of Date)("WEDT", CDate("1900/01/01"), False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Weekending Date")
    Private _SalesValue As New ColField(Of Decimal)("SALV", 0, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Sales Value")
    Private _SalesUnits As New ColField(Of Decimal)("SALU", 0, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Sales Units")
    Private _ReceiptsValue As New ColField(Of Decimal)("MREV", 0, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Receipts Value")
    Private _ReceiptsQty As New ColField(Of Decimal)("MREQ", 0, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Receipts Quantity")
    Private _AdjustmentValue As New ColField(Of Decimal)("MADV", 0, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Adjustments Value")
    Private _AdjustmentQty As New ColField(Of Decimal)("MADQ", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Adjustments Quantity")
    Private _InOutNetValue As New ColField(Of Decimal)("MIBV", 0, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "In Out Net Value")
    Private _InOutNetQty As New ColField(Of Decimal)("MIBQ", 0, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "In Out Net Quantity")
    Private _ReturnsValue As New ColField(Of Decimal)("MRTV", 0, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Returns Value")
    Private _ReturnsQty As New ColField(Of Decimal)("MRTQ", 0, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Returns Quantity")
    Private _BulkToSingleTransVal As New ColField(Of Decimal)("MBSV", 0, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Bulk To Single Trans Val")
    Private _BulkToSingleTransQty As New ColField(Of Decimal)("MBSQ", 0, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Bulk To Single Trans Qty")
    Private _PriceViolationsValue As New ColField(Of Decimal)("MPVV", 0, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Prive Violations Value")
    Private _PriceChgMarkUpDwnVal As New ColField(Of Decimal)("MPCV", 0, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "Price Chg Mark Up Dwn Val")
    Private _CyclicalCountValue As New ColField(Of Decimal)("MCCV", 0, False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "Cyclical Count Value")
    Private _DrlAdjustmentValue As New ColField(Of Decimal)("MDRV", 0, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "DRL Adjustment Value")
    Private _EndingPrice As New ColField(Of Decimal)("MEPR", 0, False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "Ending Price")
    Private _EndingQty As New ColField(Of Decimal)("METQ", 0, False, False, 1, 0, True, True, 21, 0, 0, 0, "", 0, "Ending Quantity")
    Private _SingleMarkUpValue As New ColField(Of Decimal)("MARK", 0, False, False, 1, 0, True, True, 22, 0, 0, 0, "", 0, "Single Mark Up Value")

#End Region

#Region "Field Properties"

    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property WeekNo() As ColField(Of Decimal)
        Get
            WeekNo = _WeekNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _WeekNumber = value
        End Set
    End Property
    Public Property WeekendingDate() As ColField(Of Date)
        Get
            WeekendingDate = _WeekEndingDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _WeekEndingDate = value
        End Set
    End Property
    Public Property SalesValue() As ColField(Of Decimal)
        Get
            SalesValue = _SalesValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesValue = value
        End Set
    End Property
    Public Property SalesUnits() As ColField(Of Decimal)
        Get
            SalesUnits = _SalesUnits
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesUnits = value
        End Set
    End Property
    Public Property ReceiptsValue() As ColField(Of Decimal)
        Get
            ReceiptsValue = _ReceiptsValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReceiptsValue = value
        End Set
    End Property
    Public Property ReceiptsQuantity() As ColField(Of Decimal)
        Get
            ReceiptsQuantity = _ReceiptsQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReceiptsQty = value
        End Set
    End Property
    Public Property AdjustmentValue() As ColField(Of Decimal)
        Get
            AdjustmentValue = _AdjustmentValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustmentValue = value
        End Set
    End Property
    Public Property AdjustmentQuantity() As ColField(Of Decimal)
        Get
            AdjustmentQuantity = _AdjustmentQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustmentQty = value
        End Set
    End Property
    Public Property InOutNetValue() As ColField(Of Decimal)
        Get
            InOutNetValue = _InOutNetValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _InOutNetValue = value
        End Set
    End Property
    Public Property InOutNetQuantity() As ColField(Of Decimal)
        Get
            InOutNetQuantity = _InOutNetQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _InOutNetQty = value
        End Set
    End Property
    Public Property ReturnsValue() As ColField(Of Decimal)
        Get
            ReturnsValue = _ReturnsValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReturnsValue = value
        End Set
    End Property
    Public Property ReturnsQuantity() As ColField(Of Decimal)
        Get
            ReturnsQuantity = _ReturnsQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReturnsQty = value
        End Set
    End Property
    Public Property BulkToSingleTransVal() As ColField(Of Decimal)
        Get
            BulkToSingleTransVal = _BulkToSingleTransVal
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BulkToSingleTransVal = value
        End Set
    End Property
    Public Property BulkToSingleTransQty() As ColField(Of Decimal)
        Get
            BulkToSingleTransQty = _BulkToSingleTransQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BulkToSingleTransQty = value
        End Set
    End Property
    Public Property PriveViolationsValue() As ColField(Of Decimal)
        Get
            PriveViolationsValue = _PriceViolationsValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceViolationsValue = value
        End Set
    End Property
    Public Property PriceChgMarkUpDwnVal() As ColField(Of Decimal)
        Get
            PriceChgMarkUpDwnVal = _PriceChgMarkUpDwnVal
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceChgMarkUpDwnVal = value
        End Set
    End Property
    Public Property CyclicalCountValue() As ColField(Of Decimal)
        Get
            CyclicalCountValue = _CyclicalCountValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CyclicalCountValue = value
        End Set
    End Property
    Public Property DRLAdjustMentValue() As ColField(Of Decimal)
        Get
            DRLAdjustMentValue = _DrlAdjustmentValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DrlAdjustmentValue = value
        End Set
    End Property
    Public Property EndingPrice() As ColField(Of Decimal)
        Get
            EndingPrice = _EndingPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EndingPrice = value
        End Set
    End Property
    Public Property EndingQuantity() As ColField(Of Decimal)
        Get
            EndingQuantity = _EndingQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EndingQty = value
        End Set
    End Property
    Public Property SingleMarkUpValue() As ColField(Of Decimal)
        Get
            SingleMarkUpValue = _SingleMarkUpValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SingleMarkUpValue = value
        End Set
    End Property


#End Region

End Class

