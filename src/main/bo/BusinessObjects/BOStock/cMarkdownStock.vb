﻿<Serializable()> Public Class cMarkdownStock
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "MarkdownStock"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Serial)
        BOFields.Add(_Price)
        BOFields.Add(_IsLabelled)
        BOFields.Add(_IsReserved)
        BOFields.Add(_DateCreated)
        BOFields.Add(_DateWrittenOff)
        BOFields.Add(_DateLastRollover)
        BOFields.Add(_WeekNumber)
        BOFields.Add(_ReductionWeek1)
        BOFields.Add(_ReductionWeek2)
        BOFields.Add(_ReductionWeek3)
        BOFields.Add(_ReductionWeek4)
        BOFields.Add(_ReductionWeek5)
        BOFields.Add(_ReductionWeek6)
        BOFields.Add(_ReductionWeek7)
        BOFields.Add(_ReductionWeek8)
        BOFields.Add(_ReductionWeek9)
        BOFields.Add(_ReductionWeek10)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_SoldDate)
        BOFields.Add(_SoldTill)
        BOFields.Add(_SoldTransaction)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cMarkdownStock)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cMarkdownStock)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cMarkdownStock))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cMarkdownStock(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "SKU Number", True, False)
    Private _Serial As New ColField(Of String)("Serial", "000000", True, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Serial")
    Private _Price As New ColField(Of Decimal)("Price", 0, False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Price")
    Private _IsLabelled As New ColField(Of Boolean)("IsLabelled", False, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Labelled")
    Private _IsReserved As New ColField(Of Boolean)("IsReserved", False, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Reserved")
    Private _DateCreated As New ColField(Of Date)("DateCreated", Nothing, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Date Created")
    Private _DateWrittenOff As New ColField(Of Date)("DateWrittenOff", Nothing, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Date Written Off")
    Private _DateLastRollover As New ColField(Of Date)("DateLastRollover", Nothing, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Date Written Off")
    Private _WeekNumber As New ColField(Of Integer)("WeekNumber", 1, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Week Number")
    Private _ReductionWeek1 As New ColField(Of Decimal)("ReductionWeek1", 0, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Reduction Week 1")
    Private _ReductionWeek2 As New ColField(Of Decimal)("ReductionWeek2", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Reduction Week 2")
    Private _ReductionWeek3 As New ColField(Of Decimal)("ReductionWeek3", 0, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Reduction Week 3")
    Private _ReductionWeek4 As New ColField(Of Decimal)("ReductionWeek4", 0, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Reduction Week 4")
    Private _ReductionWeek5 As New ColField(Of Decimal)("ReductionWeek5", 0, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Reduction Week 5")
    Private _ReductionWeek6 As New ColField(Of Decimal)("ReductionWeek6", 0, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Reduction Week 6")
    Private _ReductionWeek7 As New ColField(Of Decimal)("ReductionWeek7", 0, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Reduction Week 7")
    Private _ReductionWeek8 As New ColField(Of Decimal)("ReductionWeek8", 0, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Reduction Week 8")
    Private _ReductionWeek9 As New ColField(Of Decimal)("ReductionWeek9", 0, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Reduction Week 9")
    Private _ReductionWeek10 As New ColField(Of Decimal)("ReductionWeek10", 0, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "Reduction Week 10")
    Private _ReasonCode As New ColField(Of String)("ReasonCode", "00", False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "Reason Code")
    Private _SoldDate As New ColField(Of Date)("SoldDate", Nothing, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "Sold Date")
    Private _SoldTill As New ColField(Of String)("SoldTill", "00", False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "Sold Till")
    Private _SoldTransaction As New ColField(Of String)("SoldTransaction", "0000", False, False, 1, 0, True, True, 21, 0, 0, 0, "", 0, "Sold Transaction")

#End Region

#Region "Field Properties"

    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Serial() As ColField(Of String)
        Get
            Serial = _Serial
        End Get
        Set(ByVal value As ColField(Of String))
            _Serial = value
        End Set
    End Property
    Public Property Price() As ColField(Of Decimal)
        Get
            Price = _Price
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Price = value
        End Set
    End Property
    Public Property IsLabelled() As ColField(Of Boolean)
        Get
            IsLabelled = _IsLabelled
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsLabelled = value
        End Set
    End Property
    Public Property IsReserved() As ColField(Of Boolean)
        Get
            Return _IsReserved
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsReserved = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property DateWrittenOff() As ColField(Of Date)
        Get
            DateWrittenOff = _DateWrittenOff
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateWrittenOff = value
        End Set
    End Property
    Public Property DateLastRollover() As ColField(Of Date)
        Get
            Return _DateLastRollover
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastRollover = value
        End Set
    End Property
    Public Property WeekNumber() As ColField(Of Integer)
        Get
            WeekNumber = _WeekNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _WeekNumber = value
        End Set
    End Property

    Public Property ReductionWeek(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            'Extract Column Name
            Dim ColName As String = ReductionWeek1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = ReductionWeek1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property ReductionWeek1() As ColField(Of Decimal)
        Get
            ReductionWeek1 = _ReductionWeek1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek1 = value
        End Set
    End Property
    Public Property ReductionWeek2() As ColField(Of Decimal)
        Get
            ReductionWeek2 = _ReductionWeek2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek2 = value
        End Set
    End Property
    Public Property ReductionWeek3() As ColField(Of Decimal)
        Get
            ReductionWeek3 = _ReductionWeek3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek3 = value
        End Set
    End Property
    Public Property ReductionWeek4() As ColField(Of Decimal)
        Get
            ReductionWeek4 = _ReductionWeek4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek4 = value
        End Set
    End Property
    Public Property ReductionWeek5() As ColField(Of Decimal)
        Get
            ReductionWeek5 = _ReductionWeek5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek5 = value
        End Set
    End Property
    Public Property ReductionWeek6() As ColField(Of Decimal)
        Get
            ReductionWeek6 = _ReductionWeek6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek6 = value
        End Set
    End Property
    Public Property ReductionWeek7() As ColField(Of Decimal)
        Get
            ReductionWeek7 = _ReductionWeek7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek7 = value
        End Set
    End Property
    Public Property ReductionWeek8() As ColField(Of Decimal)
        Get
            ReductionWeek8 = _ReductionWeek8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek8 = value
        End Set
    End Property
    Public Property ReductionWeek9() As ColField(Of Decimal)
        Get
            ReductionWeek9 = _ReductionWeek9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek9 = value
        End Set
    End Property
    Public Property ReductionWeek10() As ColField(Of Decimal)
        Get
            ReductionWeek10 = _ReductionWeek10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReductionWeek10 = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of String)
        Get
            ReasonCode = _ReasonCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ReasonCode = value
        End Set
    End Property
    Public Property SoldDate() As ColField(Of Date)
        Get
            SoldDate = _SoldDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _SoldDate = value
        End Set
    End Property
    Public Property SoldTill() As ColField(Of String)
        Get
            SoldTill = _SoldTill
        End Get
        Set(ByVal value As ColField(Of String))
            _SoldTill = value
        End Set
    End Property
    Public Property SoldTransaction() As ColField(Of String)
        Get
            SoldTransaction = _SoldTransaction
        End Get
        Set(ByVal value As ColField(Of String))
            _SoldTransaction = value
        End Set
    End Property

#End Region

#Region "Data Set"
    Public Enum ColumnNames
        Selected
        Barcode
        SkuDescription
        SkuPrice
        CategoryNumber
        CategoryDescription
        GroupNumber
        GroupDescription
        SubgroupNumber
        SubgroupDescription
        StyleNumber
        StyleDescription
        WeekCurrent
        WeekCurrent1
        WeekCurrent2
        WeekCurrent3
        WeekCurrent4
        WeekCurrent5
        WeekCurrent6
        WeekCurrent7
        WeekCurrent8
        WeekCurrent9
    End Enum
    Public Function Column(ByVal name As ColumnNames) As String
        Return name.ToString
    End Function

    Private _dtActive As DataTable
    Private _dtSold As DataTable
    Private _dtWrittenOff As DataTable
    Public ReadOnly Property ActiveTable() As DataTable
        Get
            Return _dtActive
        End Get
    End Property
    Public ReadOnly Property SoldTable() As DataTable
        Get
            Return _dtSold
        End Get
    End Property
    Public ReadOnly Property WrittenOffTable() As DataTable
        Get
            Return _dtWrittenOff
        End Get
    End Property

#End Region

#Region "Entities"
    Private _Stock As cStock = Nothing
    Private _Markdowns As List(Of cMarkdownStock) = Nothing

    Public Property Stock() As cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As cStock)
            _Stock = value
        End Set
    End Property
    Public Property Markdowns() As List(Of cMarkdownStock)
        Get
            If _Markdowns Is Nothing Then
                ClearLoadField()
                ClearLoadFilter()
                AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, _SkuNumber.Value)
                _Markdowns = LoadMatches()
            End If
            Return _Markdowns
        End Get
        Set(ByVal value As List(Of cMarkdownStock))
            _Markdowns = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads active markdowns in ActiveTable for this markdown sku number
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ActiveLoadForSku()

        Try
            'get table of markdowns
            _dtActive = Oasys3DB.SqlServer.GetDataset("EXEC MarkdownsSelectForSku '" & _SkuNumber.Value & "'").Tables(0)

            'add barcode column
            _dtActive.Columns.Add(New DataColumn(Column(ColumnNames.Barcode), GetType(String)))
            For Each dr As DataRow In _dtActive.Rows
                dr(Column(ColumnNames.Barcode)) = BarCode(CStr(dr(_SkuNumber.ColumnName)), CStr(dr(_Serial.ColumnName)))
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all active markdowns into ActiveTable
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ActiveLoad()

        Try
            'get table of markdowns
            _dtActive = Oasys3DB.SqlServer.GetDataset("EXEC MarkdownsSelectActive").Tables(0)

            'add barcode column
            _dtActive.Columns.Add(New DataColumn(Column(ColumnNames.Barcode), GetType(String)))
            For Each dr As DataRow In _dtActive.Rows
                dr(Column(ColumnNames.Barcode)) = BarCode(CStr(dr(_SkuNumber.ColumnName)), CStr(dr(_Serial.ColumnName)))
            Next

            Dim col As New DataColumn("Selected", GetType(Boolean))
            col.DefaultValue = False
            col.Caption = "Selected"
            _dtActive.Columns.Add(col)
            col.SetOrdinal(0)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Updates all modified active markdowns in ActiveTable to database
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ActiveUpdate()

        Try
            For Each dr As DataRow In _dtActive.Rows
                If dr.RowState = DataRowState.Modified Then
                    Me.LoadFromRow(dr)
                    SaveIfExists()
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownUpdate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns string required for label printing application for all selected markdowns and passes back
    '''  the number of labels required
    ''' </summary>
    ''' <remarks></remarks>
    Public Function ActivePrintLabelString(ByRef numLabels As Integer) As String

        'generate text file to send to label print routine
        Dim sb As New StringBuilder
        sb.Append("SEQN NO,SKU,DESCRIPTION,EAN,PRICE,PREV PRICE,EQUIV PRICE,EQUIV UNITS,OBSELETE,STOCK OFF SALE,FRAGILE,TIMBER,PACK QTY,PLANGRAM,PRIMARY PG,PRF")
        sb.Append(Environment.NewLine)

        'instantiate one copy of the stock object
        Dim stock As New cStock(Oasys3DB)

        'load all markdowns to be printed into string builder
        For Each dr As DataRow In _dtActive.Rows
            If CBool(dr(Column(ColumnNames.Selected))) = True Then
                stock.LoadStockItem(CStr(dr(_SkuNumber.ColumnName)))
                sb.Append(CStr(dr(_Serial.ColumnName)) & ",")
                sb.Append(CStr(dr(_SkuNumber.ColumnName)) & ",")
                sb.Append(CStr(dr(Column(ColumnNames.SkuDescription))) & ",")
                sb.Append(CStr(dr(Column(ColumnNames.Barcode))) & ",")
                sb.Append(CStr(dr(Column(ColumnNames.WeekCurrent))) & ",")
                sb.Append(stock.NormalSellPrice.Value + stock.WeeeStock.NormalSellPrice.Value & ",")
                sb.Append(stock.EquivPrice & ",")
                sb.Append(stock.EquivalentPriceUnit.Value & ",")
                sb.Append(stock.ItemObsolete.Value & ",")
                sb.Append(stock.StockHeldOffSale.Value & ",")
                sb.Append(stock.InsulationItem.Value & ",")
                sb.Append(stock.ManagedTimberCont.Value & ",")
                sb.Append(stock.SupplierPackSize.Value & ",,,")
                sb.Append(stock.WeeeStock.SkuNumber.Value & Environment.NewLine)
                numLabels += 1
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Updates all selected active markdowns in ActiveTable as labelled
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ActiveUpdateLabelled()

        Try
            For Each dr As DataRow In _dtActive.Rows
                If CBool(dr(Column(ColumnNames.Selected))) = True Then
                    dr(_IsLabelled.ColumnName) = True

                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
                    Oasys3DB.SetColumnAndValueParameter(_IsLabelled.ColumnName, True)
                    Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, dr(_SkuNumber.ColumnName))
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_Serial.ColumnName, clsOasys3DB.eOperator.pEquals, dr(_Serial.ColumnName))
                    Oasys3DB.Update()
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownUpdate, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Loads all sold markdowns into SoldTable for given date range
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SoldLoad(ByVal dateStart As Date, ByVal dateEnd As Date)

        Try
            'get table of markdowns
            Dim sb As New StringBuilder("EXEC MarkdownsSelectSold ")
            sb.Append("'" & dateStart.ToString("yyyy-MM-dd") & "', ")
            sb.Append("'" & dateEnd.ToString("yyyy-MM-dd") & "'")

            _dtSold = Oasys3DB.SqlServer.GetDataset(sb.ToString).Tables(0)

            'add barcode column
            _dtSold.Columns.Add(New DataColumn(Column(ColumnNames.Barcode), GetType(String)))
            For Each dr As DataRow In _dtSold.Rows
                dr(Column(ColumnNames.Barcode)) = BarCode(CStr(dr(_SkuNumber.ColumnName)), CStr(dr(_Serial.ColumnName)))
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all written off markdowns into WrittenOffTable for given date range 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub WrittenOffLoad(ByVal dateStart As Date, ByVal dateEnd As Date)

        Try
            'get table of markdowns
            Dim sb As New StringBuilder("EXEC MarkdownsSelectWrittenOff ")
            sb.Append("'" & dateStart.ToString("yyyy-MM-dd") & "', ")
            sb.Append("'" & dateEnd.ToString("yyyy-MM-dd") & "'")

            _dtWrittenOff = Oasys3DB.SqlServer.GetDataset(sb.ToString).Tables(0)

            'add barcode column
            _dtWrittenOff.Columns.Add(New DataColumn(Column(ColumnNames.Barcode), GetType(String)))
            For Each dr As DataRow In _dtWrittenOff.Rows
                dr(Column(ColumnNames.Barcode)) = BarCode(CStr(dr(_SkuNumber.ColumnName)), CStr(dr(_Serial.ColumnName)))
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.MarkdownLoad, ex)
        End Try

    End Sub


    '''' <summary>
    '''' Loads all sold markdowns into markdowns collection of this instance for given date range
    '''' </summary>
    '''' <remarks></remarks>
    'Public Sub LoadSoldMarkdowns(ByVal StartDate As Date, ByVal EndDate As Date)

    '    Try
    '        Dim mark = New BOStock.cMarkdownStock(Oasys3DB)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, mark.SoldDate, New Date() {})
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, mark.DateCreated, StartDate)
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, mark.DateCreated, EndDate)
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, mark.IsReserved, False)

    '        _Markdowns = mark.LoadMatches

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    '''' <summary>
    '''' Loads all written off markdowns into markdowns collection of this instance for given date range
    '''' </summary>
    '''' <param name="StartDate"></param>
    '''' <param name="EndDate"></param>
    '''' <remarks></remarks>
    'Public Sub LoadWrittenOffMarkdowns(ByVal StartDate As Date, ByVal EndDate As Date)

    '    Try
    '        Dim mark As New BOStock.cMarkdownStock(Oasys3DB)

    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, mark.DateWrittenOff, New Date() {})
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, mark.DateCreated, StartDate)
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, mark.DateCreated, EndDate)
    '        mark.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        mark.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, mark.IsReserved, False)

    '        _Markdowns = mark.LoadMatches

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub



    '''' <summary>
    '''' Assigns given stock items to markdowns in Markdowns collection of this instance.
    ''''  Throws Oasys exception on error
    '''' </summary>
    '''' <param name="stocks"></param>
    '''' <remarks></remarks>
    'Public Sub AssignStockItems(ByVal stocks As List(Of cStock))

    '    Try
    '        If _Markdowns Is Nothing Then Exit Sub

    '        For Each mark As cMarkdownStock In _Markdowns
    '            If mark.Stock IsNot Nothing Then Continue For
    '            For Each s As cStock In stocks
    '                If s.SkuNumber.Value = mark.SkuNumber.Value Then
    '                    mark.Stock = s
    '                    Exit For
    '                End If
    '            Next
    '        Next


    '    Catch ex As Exception
    '        Throw New OasysDbException(My.Resources.Errors.MarkdownsAssignStocks, ex)
    '    End Try

    'End Sub


    ''' <summary>
    ''' Updates the week number of this markdown and persists to database
    ''' </summary>
    ''' <param name="Week"></param>
    ''' <remarks></remarks>
    Public Sub UpdateWeekNumber(ByVal Week As Integer)

        Try
            If Week = _WeekNumber.Value Then Exit Sub
            _WeekNumber.Value = Week

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_WeekNumber.ColumnName, _WeekNumber.Value)

            SetDBKeys()
            Oasys3DB.Update()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Writes off this markdown
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub WriteOffMarkdown()

        Try
            _WeekNumber.Value = 10
            _DateWrittenOff.Value = Now

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_WeekNumber.ColumnName, _WeekNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_DateWrittenOff.ColumnName, _DateWrittenOff.Value)

            SetDBKeys()
            Oasys3DB.Update()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Public Function UpdateRollover() As Boolean

        Try
            _WeekNumber.Value += 1
            _IsLabelled.Value = False
            _DateLastRollover.Value = Now.Date

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_WeekNumber.ColumnName, _WeekNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_IsLabelled.ColumnName, _IsLabelled.Value)
            Oasys3DB.SetColumnAndValueParameter(_DateLastRollover.ColumnName, _DateLastRollover.Value)
            SetDBKeys()
            If CBool(Oasys3DB.Update) Then Return True
            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function BarCode() As String

        'calculate check digit Using a 313 weighted calculation,:
        'Consider the code 			M24032300000435?
        'Giving "M" value of 11 		11/ 2/ 4/ 0/ 3/ 2/ 3/ 0/ 0/ 0/ 0/ 0/ 4/ 3/ 5
        'Using 313 weighting			 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3
        'Multiplied out gives		33/ 3/12/ 0/ 9/ 2/ 9/ 0/ 0/ 0/ 0/ 0/12/ 3/15
        'Sum of products			33+03+12+00+09+02+09+00+00+00+00+00+12+03+15=97
        'Take Mod 26(i.e.range A to Z)	97 - 26 - 26 - 26 = 19
        'Convert to relative alpha		19 = T     Barcode = M24032300000435T

        Dim alphabet As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Dim skuSerial As String = _SkuNumber.Value & _Serial.Value.PadLeft(6, "0"c)
        Dim checkDigit As Integer = alphabet.IndexOf("M") * 3

        For index As Integer = 0 To skuSerial.Length - 1
            checkDigit += (CInt(skuSerial.Substring(index, 1)) * (((index Mod 2) * 2) + 1))
        Next

        Return "M" & skuSerial & "00" & alphabet.Chars(checkDigit Mod 26)

    End Function

    Private Function Barcode(ByVal sku As String, ByVal serial As String) As String

        Dim alphabet As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Dim skuSerial As String = sku & serial
        Dim checkDigit As Integer = alphabet.IndexOf("M") * 3

        For index As Integer = 0 To skuSerial.Length - 1
            checkDigit += (CInt(skuSerial.Substring(index, 1)) * (((index Mod 2) * 2) + 1))
        Next

        Return "M" & skuSerial & "00" & alphabet.Chars(checkDigit Mod 26)

    End Function

    Public Function ISISInsertConfirmMarkdown(ByVal strSKU As String, ByVal strSerialNo As String, _
                                               ByVal strMarkdownReason As String, ByVal strMarkdownCycle As String, ByVal intPrice As Double) As Boolean


        Dim BOStockMarkdown As New BOStock.cMarkdownStock(Oasys3DB)
        Dim BOStock As New BOStock.cStock(Oasys3DB)
        Dim BOHieracrchy As New BOHierarchy.cHierarchyMaster(Oasys3DB)
        Dim listOfcritria As New List(Of String)
        Dim BOcParameter As New BOSystem.cParameter(Oasys3DB)

        Const METHOD_NAME As String = "ConfirmMarkdown"

        Trace.WriteLine(METHOD_NAME & " Start")

        Try

            BOStockMarkdown.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStockMarkdown.Serial, strSerialNo)

            If BOStockMarkdown.LoadMatches.Count > 0 Then
                BOStockMarkdown.DateCreated.Value = BOStockMarkdown.DateCreated.Value
            Else
                BOStockMarkdown.DateCreated.Value = Date.Today
            End If
            BOStockMarkdown.SkuNumber.Value = strSKU
            BOStockMarkdown.Serial.Value = strSerialNo
            BOStockMarkdown.ReasonCode.Value = strMarkdownReason
            BOStockMarkdown.IsReserved.Value = False

            Select Case strMarkdownCycle
                Case "01"
                    BOStockMarkdown.WeekNumber.Value = 1
                Case "02"
                    BOStockMarkdown.WeekNumber.Value = 2
                Case "03"
                    BOStockMarkdown.WeekNumber.Value = 3
                Case "04"
                    BOStockMarkdown.WeekNumber.Value = 4
                Case "05"
                    BOStockMarkdown.WeekNumber.Value = 5
                Case "06"
                    BOStockMarkdown.WeekNumber.Value = 6
                Case "07"
                    BOStockMarkdown.WeekNumber.Value = 7
                Case "08"
                    BOStockMarkdown.WeekNumber.Value = 8
                Case "09"
                    BOStockMarkdown.WeekNumber.Value = 9
                Case "10"
                    BOStockMarkdown.WeekNumber.Value = 10
            End Select


            'load up the current price for the item
            BOStock.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOStock.SkuNumber, strSKU)
            If BOStock.LoadMatches.Count > 0 Then
                BOStockMarkdown.Price.Value = BOStock.NormalSellPrice.Value

                listOfcritria.Add(BOStock.HierStyle.Value)
                listOfcritria.Add(BOStock.HierSubGroup.Value)
                listOfcritria.Add(BOStock.HierGroup.Value)
                listOfcritria.Add(BOStock.HierCategory.Value)
                Dim level As Integer = 2

                For Each number As String In listOfcritria

                    BOHieracrchy.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOHieracrchy.Level, CStr(level))
                    BOHieracrchy.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    BOHieracrchy.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOHieracrchy.MemberNumber, number)
                    BOHieracrchy.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    BOHieracrchy.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOHieracrchy.IsDeleted, False)

                    If BOHieracrchy.LoadMatches.Count > 0 Then
                        'check that not all zero
                        Dim AllZero As Boolean = True
                        For index As Integer = 1 To 10
                            If BOHieracrchy.ReductionWeek(index).Value <> 0 Then
                                AllZero = False
                                Exit For
                            End If
                        Next

                        If Not AllZero Then
                            For index As Integer = 1 To 10
                                BOStockMarkdown.ReductionWeek(index).Value = BOHieracrchy.ReductionWeek(index).Value
                            Next index
                            Exit For
                        End If

                        If AllZero = True Then
                            Dim strDefault As String = BOcParameter.GetParameterString(4101)
                            Dim strDefaults() As String = strDefault.Split(","c)
                            Dim intMarkdownDefualt(9) As Decimal

                            For index As Integer = 0 To 9
                                If index <= strDefaults.GetUpperBound(0) Then
                                    intMarkdownDefualt(index) = CDec(strDefaults(index))
                                    BOStockMarkdown.ReductionWeek(index + 1).Value = intMarkdownDefualt(index)
                                End If
                            Next index
                        End If
                    End If
                    level += 1
                Next number
            End If
            'insets a new record if one dont exsist if one does then updates the current record


            If BOStockMarkdown.Save(clsOasys3DB.eSqlQueryType.pUpdate) = False Then
                BOStockMarkdown.DateCreated.Value = Date.Today
                If BOStockMarkdown.SaveIfNew = False Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return True
            End If
            Return True
        Catch ex As Exception
            Trace.WriteLine(METHOD_NAME & " Error:" & ex.Message)
            Return False
        End Try
    End Function

#End Region


End Class
