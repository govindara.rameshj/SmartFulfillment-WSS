﻿<Serializable()> Public Class cStockLog
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKLOG"
        BOFields.Add(_StockLogID)
        BOFields.Add(_LogDayNumber)
        BOFields.Add(_LogType)
        BOFields.Add(_LogDate)
        BOFields.Add(_LogTime)
        BOFields.Add(_LogKey)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_SentToHO)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_StockOnHandStart)
        BOFields.Add(_StockOnHandEnd)
        BOFields.Add(_StockReturnsStart)
        BOFields.Add(_StockReturnsEnd)
        BOFields.Add(_StockMarkdownStart)
        BOFields.Add(_StockMarkdownEnd)
        BOFields.Add(_StockWriteoffStart)
        BOFields.Add(_StockWriteoffEnd)
        BOFields.Add(_PriceStart)
        BOFields.Add(_PriceEnd)
        BOFields.Add(_RTI)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStockLog)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStockLog)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStockLog))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStockLog(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Function Save(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType) As Boolean

        Try
            If eSave = Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert Then
                ' only insert a '91' if it is not a '99'
                If _LogType.Value <> "99" AndAlso _LogType.Value <> "91" Then
                    'Check whether variance exists between this entry and last entry and create 91 record
                    Dim lastLogQty As Integer = LastQtyForSku(_SkuNumber.Value)
                    If _StockOnHandStart.Value <> lastLogQty Then
                        Dim log As New cStockLog(Oasys3DB)
                        log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                        log.LogType.Value = "91"
                        log.LogDate.Value = Now.Date
                        log.LogTime.Value = Format(Now, "HHmmss")
                        log.LogKey.Value = "System Adjustment 91"
                        log.EmployeeID.Value = _EmployeeID.Value
                        log.SentToHO.Value = False
                        log.SkuNumber.Value = _SkuNumber.Value
                        log.StockOnHandStart.Value = lastLogQty
                        log.StockOnHandEnd.Value = _StockOnHandStart.Value
                        log.StockReturnsStart.Value = _StockReturnsStart.Value
                        log.StockReturnsEnd.Value = _StockReturnsEnd.Value
                        log.StockMarkdownStart.Value = _StockMarkdownStart.Value
                        log.StockMarkdownEnd.Value = _StockMarkdownEnd.Value
                        log.StockWriteoffStart.Value = _StockWriteoffStart.Value
                        log.StockWriteoffEnd.Value = _StockWriteoffEnd.Value
                        log.PriceStart.Value = _PriceStart.Value
                        log.PriceEnd.Value = _PriceEnd.Value
                        log.SaveIfNew()
                    End If
                End If

                'Insert into table
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, eSave)
                SetDBFieldValues(clsOasys3DB.eSqlQueryType.pInsert)
                If Oasys3DB.Insert > 0 Then
                    Return True
                End If

            Else
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, eSave)
                SetDBFieldValues(clsOasys3DB.eSqlQueryType.pUpdate)
                SetDBKeys()
                If Oasys3DB.Update > 0 Then
                    Return True
                End If
            End If

            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

#Region "Fields"

    Private _StockLogID As New ColField(Of Integer)("TKEY", 0, True, True, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Stock Log ID")
    Private _LogDayNumber As New ColField(Of Integer)("DAYN", 0, False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Log Day Number")
    Private _LogType As New ColField(Of String)("TYPE", "", False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Log Type")
    Private _LogDate As New ColField(Of Date)("DATE1", Nothing, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Log Date")
    Private _LogTime As New ColField(Of String)("TIME", "000000", False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Log Time")
    Private _LogKey As New ColField(Of String)("KEYS", "", False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Log Key")
    Private _EmployeeID As New ColField(Of String)("EEID", "", False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Employee ID")
    Private _SentToHO As New ColField(Of Boolean)("ICOM", False, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Sent To HO")
    Private _SkuNumber As New ColField(Of String)("SKUN", "", False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Sku Number")
    Private _StockOnHandStart As New ColField(Of Integer)("SSTK", 0, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Stock On Hand Start")
    Private _StockOnHandEnd As New ColField(Of Integer)("ESTK", 0, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Stock On Hand End")
    Private _StockReturnsStart As New ColField(Of Integer)("SRET", 0, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Stock Returns Start")
    Private _StockReturnsEnd As New ColField(Of Integer)("ERET", 0, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Stock Returns End")
    Private _StockMarkdownStart As New ColField(Of Integer)("SMDN", 0, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Stock Markdown Start")
    Private _StockMarkdownEnd As New ColField(Of Integer)("EMDN", 0, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Stock Markdown End")
    Private _StockWriteoffStart As New ColField(Of Integer)("SWTF", 0, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Stock Write Off Start")
    Private _StockWriteoffEnd As New ColField(Of Integer)("EWTF", 0, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "Stock Write Off End")
    Private _PriceStart As New ColField(Of Decimal)("SPRI", 0, False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "Price Start")
    Private _PriceEnd As New ColField(Of Decimal)("EPRI", 0, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "Price End")
    Private _RTI As New ColField(Of String)("RTI", "S", False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "RTI")

#End Region

#Region "Field Properties"

    Public Property StockLogID() As ColField(Of Integer)
        Get
            Return _StockLogID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockLogID = value
        End Set
    End Property
    Public Property LogDayNumber() As ColField(Of Integer)
        Get
            Return _LogDayNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LogDayNumber = value
        End Set
    End Property
    Public Property LogType() As ColField(Of String)
        Get
            Return _LogType
        End Get
        Set(ByVal value As ColField(Of String))
            _LogTime = value
        End Set
    End Property
    Public Property LogDate() As ColField(Of Date)
        Get
            Return _LogDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _LogDate = value
        End Set
    End Property
    Public Property LogTime() As ColField(Of String)
        Get
            Return _LogTime
        End Get
        Set(ByVal value As ColField(Of String))
            _LogTime = value
        End Set
    End Property
    Public Property LogKey() As ColField(Of String)
        Get
            Return _LogKey
        End Get
        Set(ByVal value As ColField(Of String))
            _LogKey = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)
        Get
            Return _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property SentToHO() As ColField(Of Boolean)
        Get
            Return _SentToHO
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SentToHO = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property StockOnHandStart() As ColField(Of Integer)
        Get
            Return _StockOnHandStart
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnHandStart = value
        End Set
    End Property
    Public Property StockOnHandEnd() As ColField(Of Integer)
        Get
            Return _StockOnHandEnd
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnHandEnd = value
        End Set
    End Property
    Public Property StockReturnsStart() As ColField(Of Integer)
        Get
            Return _StockReturnsStart
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockReturnsStart = value
        End Set
    End Property
    Public Property StockReturnsEnd() As ColField(Of Integer)
        Get
            Return _StockReturnsEnd
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockReturnsEnd = value
        End Set
    End Property
    Public Property StockMarkdownStart() As ColField(Of Integer)
        Get
            Return _StockMarkdownStart
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockMarkdownStart = value
        End Set
    End Property
    Public Property StockMarkdownEnd() As ColField(Of Integer)
        Get
            Return _StockMarkdownEnd
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockMarkdownEnd = value
        End Set
    End Property
    Public Property StockWriteoffStart() As ColField(Of Integer)
        Get
            Return _StockWriteoffStart
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockWriteoffStart = value
        End Set
    End Property
    Public Property StockWriteoffEnd() As ColField(Of Integer)
        Get
            Return _StockWriteoffEnd
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockWriteoffEnd = value
        End Set
    End Property
    Public Property PriceStart() As ColField(Of Decimal)
        Get
            Return _PriceStart
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceStart = value
        End Set
    End Property
    Public Property PriceEnd() As ColField(Of Decimal)
        Get
            Return _PriceEnd
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceEnd = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function StockReceived(ByVal Key As String, ByVal UserID As Integer, ByVal SKUNumber As String, _
                                  ByVal StockStart As Integer, ByVal ReceivedQty As Integer, ByVal Price As Decimal, _
                                  ByVal Returns As Integer, ByVal Markdown As Integer, ByVal WriteOff As Integer) As Boolean

        _LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date))
        _LogType.Value = "72"
        _LogDate.Value = Now.Date
        _LogTime.Value = Format(Now, "HHMMss")
        _LogKey.Value = Key
        _EmployeeID.Value = UserID.ToString.PadLeft(3, "0"c)
        _SentToHO.Value = False
        _SkuNumber.Value = SKUNumber
        _StockOnHandStart.Value = StockStart
        _StockOnHandEnd.Value = StockStart + ReceivedQty
        _StockReturnsStart.Value = Returns
        _StockReturnsEnd.Value = Returns
        _StockMarkdownStart.Value = Markdown
        _StockMarkdownEnd.Value = Markdown
        _StockWriteoffStart.Value = WriteOff
        _StockWriteoffEnd.Value = WriteOff
        _PriceStart.Value = Price
        _PriceEnd.Value = Price
        If SaveIfNew() Then Return True
        Return False

    End Function

    Public Function InsertLog(ByVal LogType As String, ByVal Key As String, ByVal UserID As Integer, _
                              ByVal SKUNumber As String, ByVal StockStart As Integer, ByVal StockEnd As Integer, _
                              ByVal ReturnsStart As Integer, ByVal ReturnsEnd As Integer, _
                              ByVal MarkdownStart As Integer, ByVal MarkdownEnd As Integer, _
                              ByVal WriteoffStart As Integer, ByVal WriteoffEnd As Integer, _
                              ByVal PriceStart As Decimal, ByVal PriceEnd As Decimal) As Boolean

        _LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date))
        _LogDayNumber.Value = LogDayNumber.Value + 1
        _LogDate.Value = Now.Date
        _LogTime.Value = Format(Now, "HHmmss")
        _SentToHO.Value = False
        _RTI.Value = "S"

        _LogType.Value = LogType
        _LogKey.Value = Key
        _EmployeeID.Value = UserID.ToString.PadLeft(3, "0"c)
        _SkuNumber.Value = SKUNumber

        _StockOnHandStart.Value = StockStart
        _StockOnHandEnd.Value = StockEnd
        _StockReturnsStart.Value = ReturnsStart
        _StockReturnsEnd.Value = ReturnsEnd
        _StockMarkdownStart.Value = MarkdownStart
        _StockMarkdownEnd.Value = MarkdownEnd
        _StockWriteoffStart.Value = WriteoffStart
        _StockWriteoffEnd.Value = WriteoffEnd
        _PriceStart.Value = PriceStart
        _PriceEnd.Value = PriceEnd
        If SaveIfNew() Then Return True
        Return False

    End Function

    Public Function InsertStart99(ByVal LogDate As Date, ByVal Key As String, ByVal UserID As Integer, _
                              ByVal SKUNumber As String, ByVal StockStart As Integer, ByVal StockEnd As Integer, _
                              ByVal ReturnsStart As Integer, ByVal ReturnsEnd As Integer, _
                              ByVal MarkdownStart As Integer, ByVal MarkdownEnd As Integer, _
                              ByVal WriteoffStart As Integer, ByVal WriteoffEnd As Integer, _
                              ByVal PriceStart As Decimal, ByVal PriceEnd As Decimal) As Boolean

        _LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), LogDate))
        _LogDayNumber.Value = LogDayNumber.Value + 1
        _LogDate.Value = LogDate
        _LogTime.Value = Format(Now, "HHMMss")
        _SentToHO.Value = False
        _RTI.Value = "C"

        _LogType.Value = "99"
        _LogKey.Value = Key
        _EmployeeID.Value = UserID.ToString.PadLeft(3, "0"c)
        _SkuNumber.Value = SKUNumber

        _StockOnHandStart.Value = StockStart
        _StockOnHandEnd.Value = StockEnd
        _StockReturnsStart.Value = ReturnsStart
        _StockReturnsEnd.Value = ReturnsEnd
        _StockMarkdownStart.Value = MarkdownStart
        _StockMarkdownEnd.Value = MarkdownEnd
        _StockWriteoffStart.Value = WriteoffStart
        _StockWriteoffEnd.Value = WriteoffEnd
        _PriceStart.Value = PriceStart
        _PriceEnd.Value = PriceEnd
        If SaveIfNew() Then Return True
        Return False

    End Function

    Public Function LastQtyForSku(ByVal SkuNumber As String) As Integer

        Dim retdata As Object
        ClearLoadField()
        ClearLoadFilter()
        AddAggregateField(Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, _StockLogID, _StockLogID.ColumnName)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, SkuNumber)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, _LogDayNumber, 0)
        retdata = GetAggregateField()

        If Not DBNull.Value.Equals(retdata) Then
            Dim maxkey As Integer = CInt(retdata)
            Dim stklog As New cStockLog(Oasys3DB)
            stklog.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _StockLogID, maxkey)
            stklog.LoadMatches()
            Return stklog.StockOnHandEnd.Value
        Else
            Return 0
        End If

    End Function

    Public Function GetLatestLogRecordForPartCode(ByVal strPartCode As String) As List(Of cStockLog)

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SkuNumber, strPartCode)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, LogDayNumber, 0)
            SortBy(LogDate.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
            Return LoadMatches()

        Catch ex As Exception

            Throw ex
            Return Nothing

        End Try

    End Function

    Public Function Purge(ByVal endDate As Date) As Boolean
        Dim SaveError As Boolean = False
        Dim NoRecs As Integer = -1

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(LogDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, endDate)
            NoRecs = Oasys3DB.Delete()
            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If
            Return SaveError

        Catch ex As Exception

            Trace.WriteLine("cStockLog - Purge Exception " & ex.Message)
            Return False

        End Try
    End Function

#End Region

End Class

