﻿<Serializable()> Public Class cStockHistory
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKMAS"
        BOFields.Add(mSKUNumber)
        BOFields.Add(mStartPrice1)
        BOFields.Add(mStartPrice2)
        BOFields.Add(mStartPrice3)
        BOFields.Add(mStartPrice4)
        BOFields.Add(mStartQty1)
        BOFields.Add(mStartQty2)
        BOFields.Add(mStartQty3)
        BOFields.Add(mStartQty4)
        BOFields.Add(mRectQty1)
        BOFields.Add(mRectQty2)
        BOFields.Add(mRectQty3)
        BOFields.Add(mRectQty4)
        BOFields.Add(mRectVal1)
        BOFields.Add(mRectVal2)
        BOFields.Add(mRectVal3)
        BOFields.Add(mRectVal4)
        BOFields.Add(mAdjQty1)
        BOFields.Add(mAdjQty2)
        BOFields.Add(mAdjQty3)
        BOFields.Add(mAdjQty4)
        BOFields.Add(mAdjVal1)
        BOFields.Add(mAdjVal2)
        BOFields.Add(mAdjVal3)
        BOFields.Add(mAdjVal4)
        BOFields.Add(mPriceViol1)
        BOFields.Add(mPriceViol2)
        BOFields.Add(mPriceViol3)
        BOFields.Add(mPriceViol4)
        BOFields.Add(mIBTNetQty1)
        BOFields.Add(mIBTNetQty2)
        BOFields.Add(mIBTNetQty3)
        BOFields.Add(mIBTNetQty4)
        BOFields.Add(mIBTNetVal1)
        BOFields.Add(mIBTNetVal2)
        BOFields.Add(mIBTNetVal3)
        BOFields.Add(mIBTNetVal4)
        BOFields.Add(mRetnQty1)
        BOFields.Add(mRetnQty2)
        BOFields.Add(mRetnQty3)
        BOFields.Add(mRetnQty4)
        BOFields.Add(mRetnVal1)
        BOFields.Add(mRetnVal2)
        BOFields.Add(mRetnVal3)
        BOFields.Add(mRetnVal4)
        BOFields.Add(mCycCntVal1)
        BOFields.Add(mCycCntVal2)
        BOFields.Add(mCycCntVal3)
        BOFields.Add(mCyvCntVal4)
        BOFields.Add(mDRLAdjVal1)
        BOFields.Add(mDRLAdjVal2)
        BOFields.Add(mDRLAdjVal3)
        BOFields.Add(mDRLAdjVal4)
        BOFields.Add(mBlkSingQty1)
        BOFields.Add(mBlkSingQty2)
        BOFields.Add(mBlkSingQty3)
        BOFields.Add(mBlkSingQty4)
        BOFields.Add(mBlkSingVal1)
        BOFields.Add(mBlkSingVal2)
        BOFields.Add(mBlkSingVal3)
        BOFields.Add(mBlkSingVal4)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStockHistory)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStockHistory)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStockHistory))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStockHistory(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private mSKUNumber As New ColField(Of String)("SKUN", "", True)
    Private mStartPrice1 As New ColField(Of Decimal)("MSPR1", 0)
    Private mStartPrice2 As New ColField(Of Decimal)("MSPR2", 0)
    Private mStartPrice3 As New ColField(Of Decimal)("MSPR3", 0)
    Private mStartPrice4 As New ColField(Of Decimal)("MSPR4", 0)

    Private mStartQty1 As New ColField(Of Decimal)("MSTQ1", 0)
    Private mStartQty2 As New ColField(Of Decimal)("MSTQ2", 0)
    Private mStartQty3 As New ColField(Of Decimal)("MSTQ3", 0)
    Private mStartQty4 As New ColField(Of Decimal)("MSTQ4", 0)

    Private mRectQty1 As New ColField(Of Decimal)("MREQ1", 0)
    Private mRectQty2 As New ColField(Of Decimal)("MREQ2", 0)
    Private mRectQty3 As New ColField(Of Decimal)("MREQ3", 0)
    Private mRectQty4 As New ColField(Of Decimal)("MREQ4", 0)

    Private mRectVal1 As New ColField(Of Decimal)("MREV1", 0)
    Private mRectVal2 As New ColField(Of Decimal)("MREV2", 0)
    Private mRectVal3 As New ColField(Of Decimal)("MREV3", 0)
    Private mRectVal4 As New ColField(Of Decimal)("MREV4", 0)

    Private mAdjQty1 As New ColField(Of Decimal)("MADQ1", 0)
    Private mAdjQty2 As New ColField(Of Decimal)("MADQ2", 0)
    Private mAdjQty3 As New ColField(Of Decimal)("MADQ3", 0)
    Private mAdjQty4 As New ColField(Of Decimal)("MADQ4", 0)

    Private mAdjVal1 As New ColField(Of Decimal)("MADV1", 0)
    Private mAdjVal2 As New ColField(Of Decimal)("MADV2", 0)
    Private mAdjVal3 As New ColField(Of Decimal)("MADV3", 0)
    Private mAdjVal4 As New ColField(Of Decimal)("MADV4", 0)

    Private mPriceViol1 As New ColField(Of Decimal)("MPVV1", 0)
    Private mPriceViol2 As New ColField(Of Decimal)("MPVV2", 0)
    Private mPriceViol3 As New ColField(Of Decimal)("MPVV3", 0)
    Private mPriceViol4 As New ColField(Of Decimal)("MPVV4", 0)

    Private mIBTNetQty1 As New ColField(Of Decimal)("MIBQ1", 0)
    Private mIBTNetQty2 As New ColField(Of Decimal)("MIBQ2", 0)
    Private mIBTNetQty3 As New ColField(Of Decimal)("MIBQ3", 0)
    Private mIBTNetQty4 As New ColField(Of Decimal)("MIBQ4", 0)

    Private mIBTNetVal1 As New ColField(Of Decimal)("MIBV1", 0)
    Private mIBTNetVal2 As New ColField(Of Decimal)("MIBV2", 0)
    Private mIBTNetVal3 As New ColField(Of Decimal)("MIBV3", 0)
    Private mIBTNetVal4 As New ColField(Of Decimal)("MIBV4", 0)

    Private mRetnQty1 As New ColField(Of Decimal)("MRTQ1", 0)
    Private mRetnQty2 As New ColField(Of Decimal)("MRTQ2", 0)
    Private mRetnQty3 As New ColField(Of Decimal)("MRTQ3", 0)
    Private mRetnQty4 As New ColField(Of Decimal)("MRTQ4", 0)

    Private mRetnVal1 As New ColField(Of Decimal)("MRTV1", 0)
    Private mRetnVal2 As New ColField(Of Decimal)("MRTV2", 0)
    Private mRetnVal3 As New ColField(Of Decimal)("MRTV3", 0)
    Private mRetnVal4 As New ColField(Of Decimal)("MRTV4", 0)

    Private mCycCntVal1 As New ColField(Of Decimal)("MCCV1", 0)
    Private mCycCntVal2 As New ColField(Of Decimal)("MCCV2", 0)
    Private mCycCntVal3 As New ColField(Of Decimal)("MCCV3", 0)
    Private mCyvCntVal4 As New ColField(Of Decimal)("MCCV4", 0)

    Private mDRLAdjVal1 As New ColField(Of Decimal)("MDRV1", 0)
    Private mDRLAdjVal2 As New ColField(Of Decimal)("MDRV2", 0)
    Private mDRLAdjVal3 As New ColField(Of Decimal)("MDRV3", 0)
    Private mDRLAdjVal4 As New ColField(Of Decimal)("MDRV4", 0)

    Private mBlkSingQty1 As New ColField(Of Decimal)("MBSQ1", 0)
    Private mBlkSingQty2 As New ColField(Of Decimal)("MBSQ2", 0)
    Private mBlkSingQty3 As New ColField(Of Decimal)("MBSQ3", 0)
    Private mBlkSingQty4 As New ColField(Of Decimal)("MBSQ4", 0)

    Private mBlkSingVal1 As New ColField(Of Decimal)("MBSV1", 0)
    Private mBlkSingVal2 As New ColField(Of Decimal)("MBSV2", 0)
    Private mBlkSingVal3 As New ColField(Of Decimal)("MBSV3", 0)
    Private mBlkSingVal4 As New ColField(Of Decimal)("MBSV4", 0)

#End Region

#Region "Field Properties"

    Public Property SKUNumber() As ColField(Of String)
        Get
            SKUNumber = mSKUNumber
        End Get
        Set(ByVal value As ColField(Of String))
            mSKUNumber = value
        End Set
    End Property
    Public Property StartPrice1() As ColField(Of Decimal)
        Get
            StartPrice1 = mStartPrice1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartPrice1 = value
        End Set
    End Property
    Public Property StartPrice2() As ColField(Of Decimal)
        Get
            StartPrice2 = mStartPrice2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartPrice2 = value
        End Set
    End Property
    Public Property StartPrice3() As ColField(Of Decimal)
        Get
            StartPrice3 = mStartPrice3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartPrice3 = value
        End Set
    End Property
    Public Property StartPrice4() As ColField(Of Decimal)
        Get
            StartPrice4 = mStartPrice4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartPrice4 = value
        End Set
    End Property
    Public Property StartQty1() As ColField(Of Decimal)
        Get
            StartQty1 = mStartQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartQty1 = value
        End Set
    End Property
    Public Property StartQty2() As ColField(Of Decimal)
        Get
            StartQty2 = mStartQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartQty2 = value
        End Set
    End Property
    Public Property StartQty3() As ColField(Of Decimal)
        Get
            StartQty3 = mStartQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartQty3 = value
        End Set
    End Property
    Public Property StartQty4() As ColField(Of Decimal)
        Get
            StartQty4 = mStartQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mStartQty4 = value
        End Set
    End Property
    Public Property RectQty1() As ColField(Of Decimal)
        Get
            RectQty1 = mRectQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectQty1 = value
        End Set
    End Property
    Public Property RectQty2() As ColField(Of Decimal)
        Get
            RectQty2 = mRectQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectQty2 = value
        End Set
    End Property
    Public Property RectQty3() As ColField(Of Decimal)
        Get
            RectQty3 = mRectQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectQty3 = value
        End Set
    End Property
    Public Property RectQty4() As ColField(Of Decimal)
        Get
            RectQty4 = mRectQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectQty4 = value
        End Set
    End Property
    Public Property RectVal1() As ColField(Of Decimal)
        Get
            RectVal1 = mRectVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectVal1 = value
        End Set
    End Property
    Public Property RectVal2() As ColField(Of Decimal)
        Get
            RectVal2 = mRectVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectQty2 = value
        End Set
    End Property
    Public Property RectVal3() As ColField(Of Decimal)
        Get
            RectVal3 = mRectVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectVal3 = value
        End Set
    End Property
    Public Property RectVal4() As ColField(Of Decimal)
        Get
            RectVal4 = mRectVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectVal4 = value
        End Set
    End Property
    Public Property AdjQty1() As ColField(Of Decimal)
        Get
            AdjQty1 = mAdjQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjQty1 = value
        End Set
    End Property
    Public Property AdjQty2() As ColField(Of Decimal)
        Get
            AdjQty2 = mAdjQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjQty2 = value
        End Set
    End Property
    Public Property AdjQty3() As ColField(Of Decimal)
        Get
            AdjQty3 = mAdjQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjQty3 = value
        End Set
    End Property
    Public Property AdjQty4() As ColField(Of Decimal)
        Get
            AdjQty4 = mAdjQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjQty4 = value
        End Set
    End Property
    Public Property AdjVal1() As ColField(Of Decimal)
        Get
            AdjVal1 = mAdjVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjVal1 = value
        End Set
    End Property
    Public Property AdjVal2() As ColField(Of Decimal)
        Get
            AdjVal2 = mAdjVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjQty2 = value
        End Set
    End Property
    Public Property AdjVal3() As ColField(Of Decimal)
        Get
            AdjVal3 = mAdjVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjVal3 = value
        End Set
    End Property
    Public Property AdjVal4() As ColField(Of Decimal)
        Get
            AdjVal4 = mAdjVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mAdjVal4 = value
        End Set
    End Property
    Public Property PriceViol1() As ColField(Of Decimal)
        Get
            PriceViol1 = mPriceViol1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPriceViol1 = value
        End Set
    End Property
    Public Property PriceViol2() As ColField(Of Decimal)
        Get
            PriceViol2 = mPriceViol2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPriceViol2 = value
        End Set
    End Property
    Public Property PriceViol3() As ColField(Of Decimal)
        Get
            PriceViol3 = mPriceViol3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPriceViol3 = value
        End Set
    End Property
    Public Property PriceViol4() As ColField(Of Decimal)
        Get
            PriceViol4 = mPriceViol4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPriceViol4 = value
        End Set
    End Property
    Public Property IBTNetQty1() As ColField(Of Decimal)
        Get
            IBTNetQty1 = mIBTNetQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetQty1 = value
        End Set
    End Property
    Public Property IBTNetQty2() As ColField(Of Decimal)
        Get
            IBTNetQty2 = mIBTNetQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetQty2 = value
        End Set
    End Property
    Public Property IBTNetQty3() As ColField(Of Decimal)
        Get
            IBTNetQty3 = mIBTNetQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetQty3 = value
        End Set
    End Property
    Public Property IBTNetQty4() As ColField(Of Decimal)
        Get
            IBTNetQty4 = mIBTNetQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetQty4 = value
        End Set
    End Property
    Public Property IBTNetVal1() As ColField(Of Decimal)
        Get
            IBTNetVal1 = mIBTNetVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetVal1 = value
        End Set
    End Property
    Public Property IBTNetVal2() As ColField(Of Decimal)
        Get
            IBTNetVal2 = mIBTNetVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetVal2 = value
        End Set
    End Property
    Public Property IBTNetVal3() As ColField(Of Decimal)
        Get
            IBTNetVal3 = mIBTNetVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetQty3 = value
        End Set
    End Property
    Public Property IBTNetVal4() As ColField(Of Decimal)
        Get
            IBTNetVal4 = mIBTNetVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mIBTNetVal4 = value
        End Set
    End Property
    Public Property RetnQty1() As ColField(Of Decimal)
        Get
            RetnQty1 = mRetnQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnQty1 = value
        End Set
    End Property
    Public Property RetnQty2() As ColField(Of Decimal)
        Get
            RetnQty2 = mRetnQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnQty2 = value
        End Set
    End Property
    Public Property RetnQty3() As ColField(Of Decimal)
        Get
            RetnQty3 = mRetnQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnQty3 = value
        End Set
    End Property
    Public Property RetnQty4() As ColField(Of Decimal)
        Get
            RetnQty4 = mRetnQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnQty4 = value
        End Set
    End Property
    Public Property RetnVal1() As ColField(Of Decimal)
        Get
            RetnVal1 = mRectVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRectVal1 = value
        End Set
    End Property
    Public Property RetnVal2() As ColField(Of Decimal)
        Get
            RetnVal2 = mRetnVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnVal2 = value
        End Set
    End Property
    Public Property RetnVal3() As ColField(Of Decimal)
        Get
            RetnVal3 = mRetnVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnVal3 = value
        End Set
    End Property
    Public Property RetnVal4() As ColField(Of Decimal)
        Get
            RetnVal4 = mRetnVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mRetnVal4 = value
        End Set
    End Property
    Public Property CycCntVal1() As ColField(Of Decimal)
        Get
            CycCntVal1 = mCycCntVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCycCntVal1 = value
        End Set
    End Property
    Public Property CycCntVal2() As ColField(Of Decimal)
        Get
            CycCntVal2 = mCycCntVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCycCntVal2 = value
        End Set
    End Property
    Public Property CycCntVal3() As ColField(Of Decimal)
        Get
            CycCntVal3 = mCycCntVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCycCntVal3 = value
        End Set
    End Property
    Public Property CycCntVal4() As ColField(Of Decimal)
        Get
            CycCntVal4 = mCyvCntVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCyvCntVal4 = value
        End Set
    End Property
    Public Property DRLAdjVal1() As ColField(Of Decimal)
        Get
            DRLAdjVal1 = mDRLAdjVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDRLAdjVal1 = value
        End Set
    End Property
    Public Property DRLAdjVal2() As ColField(Of Decimal)
        Get
            DRLAdjVal2 = mDRLAdjVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mCycCntVal2 = value
        End Set
    End Property
    Public Property DRLAdjVal3() As ColField(Of Decimal)
        Get
            DRLAdjVal3 = mDRLAdjVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDRLAdjVal3 = value
        End Set
    End Property
    Public Property DRLAdjVal4() As ColField(Of Decimal)
        Get
            DRLAdjVal4 = mDRLAdjVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDRLAdjVal4 = value
        End Set
    End Property
    Public Property BlkSingQty1() As ColField(Of Decimal)
        Get
            BlkSingQty1 = mBlkSingQty1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingQty1 = value
        End Set
    End Property
    Public Property BlkSingQty2() As ColField(Of Decimal)
        Get
            BlkSingQty2 = mBlkSingQty2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingQty2 = value
        End Set
    End Property
    Public Property BlkSingQty3() As ColField(Of Decimal)
        Get
            BlkSingQty3 = mBlkSingQty3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingQty3 = value
        End Set
    End Property
    Public Property BlkSingQty4() As ColField(Of Decimal)
        Get
            BlkSingQty4 = mBlkSingQty4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingQty4 = value
        End Set
    End Property
    Public Property BlkSingVal1() As ColField(Of Decimal)
        Get
            BlkSingVal1 = mBlkSingVal1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingVal1 = value
        End Set
    End Property
    Public Property BlkSingVal2() As ColField(Of Decimal)
        Get
            BlkSingVal2 = mBlkSingVal2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingVal2 = value
        End Set
    End Property
    Public Property BlkSingVal3() As ColField(Of Decimal)
        Get
            BlkSingVal3 = mBlkSingVal3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingVal3 = value
        End Set
    End Property
    Public Property BlkSingVal4() As ColField(Of Decimal)
        Get
            BlkSingVal4 = mBlkSingVal4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mBlkSingVal4 = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function UpdateIBTNet(ByVal partno As String, ByVal qty As Integer, ByVal value As Decimal) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetWhereParameter(mSKUNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, partno)
        Oasys3DB.SetColumnAndValueParameter(mIBTNetQty1.ColumnName, qty)
        Oasys3DB.SetColumnAndValueParameter(mIBTNetVal1.ColumnName, value)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function ProcessPeriodEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean

        Try
            'Move data 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(StartPrice4.ColumnName, StartPrice3)
            Oasys3DB.SetColumnAndValueParameter(StartQty4.ColumnName, StartQty3)
            Oasys3DB.SetColumnAndValueParameter(RectQty4.ColumnName, RectQty3)
            Oasys3DB.SetColumnAndValueParameter(RectVal4.ColumnName, RectVal3)
            Oasys3DB.SetColumnAndValueParameter(AdjQty4.ColumnName, AdjQty3)
            Oasys3DB.SetColumnAndValueParameter(AdjVal4.ColumnName, AdjVal3)
            Oasys3DB.SetColumnAndValueParameter(PriceViol4.ColumnName, PriceViol3)
            Oasys3DB.SetColumnAndValueParameter(IBTNetQty4.ColumnName, IBTNetQty3)
            Oasys3DB.SetColumnAndValueParameter(IBTNetVal4.ColumnName, IBTNetVal3)
            Oasys3DB.SetColumnAndValueParameter(RetnQty4.ColumnName, RetnQty3)
            Oasys3DB.SetColumnAndValueParameter(RetnVal4.ColumnName, RetnVal3)
            Oasys3DB.SetColumnAndValueParameter(CycCntVal4.ColumnName, CycCntVal3)
            Oasys3DB.SetColumnAndValueParameter(DRLAdjVal4.ColumnName, DRLAdjVal3)
            Oasys3DB.SetColumnAndValueParameter(BlkSingQty4.ColumnName, BlkSingQty3)
            Oasys3DB.SetColumnAndValueParameter(BlkSingVal4.ColumnName, BlkSingVal3)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            If SaveError = False Then
                'Move data 
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                Oasys3DB.SetColumnAndValueParameter(StartPrice3.ColumnName, StartPrice1)
                Oasys3DB.SetColumnAndValueParameter(StartQty3.ColumnName, StartQty1)

                NoRecs = Oasys3DB.Update()

                If NoRecs = -1 Then
                    SaveError = True
                Else
                    SaveError = False
                End If

                If SaveError = False Then
                    'zeroise fields 
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                    Oasys3DB.SetColumnAndValueParameter(mRectQty3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mRectVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mAdjQty3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mAdjVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mPriceViol3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mIBTNetQty3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mIBTNetVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mRetnQty3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mRetnVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mCycCntVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mDRLAdjVal3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mBlkSingQty3.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(mBlkSingVal3.ColumnName, 0)

                    NoRecs = Oasys3DB.Update()

                    If NoRecs = -1 Then
                        SaveError = True
                    Else
                        SaveError = False
                    End If

                End If
            End If

            Return SaveError
        Catch ex As Exception
            Trace.WriteLine("cStockHistory - ProcessPeriodEnd failed with exception " & ex.Message)
            Return False
        End Try

    End Function

#End Region

End Class
