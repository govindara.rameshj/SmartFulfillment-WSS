﻿<Serializable()> Public Class cStockText
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKTXT"
        BOFields.Add(_ItemNumber)
        BOFields.Add(_Type)
        BOFields.Add(_Sequence)
        BOFields.Add(_Text)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStockText)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStockText)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStockText))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStockText(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ItemNumber As New ColField(Of String)("SKUN", "", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Item Number")
    Private _Type As New ColField(Of String)("TYPE", "", True, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Type")
    Private _Sequence As New ColField(Of String)("SEQN", "", True, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Seqno")
    Private _Text As New ColField(Of String)("TEXT", "", False, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Text")

#End Region

#Region "Field Properties"

    Public Property ItemNumber() As ColField(Of String)
        Get
            ItemNumber = _ItemNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ItemNumber = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property Seqno() As ColField(Of String)
        Get
            Seqno = _Sequence
        End Get
        Set(ByVal value As ColField(Of String))
            _Sequence = value
        End Set
    End Property
    Public Property Text() As ColField(Of String)
        Get
            Text = _Text
        End Get
        Set(ByVal value As ColField(Of String))
            _Text = value
        End Set
    End Property

#End Region

End Class
