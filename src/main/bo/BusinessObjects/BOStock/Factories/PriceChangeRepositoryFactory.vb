﻿Public Class PriceChangeRepositoryFactory

    Private Shared m_FactoryMember As IPriceChangeRepository

    Public Shared Function FactoryGet() As IPriceChangeRepository

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New PriceChangeRepository           'live implementation
        Else
            Return m_FactoryMember                     'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IPriceChangeRepository)

        m_FactoryMember = obj

    End Sub

End Class