﻿Public Class RelatedItemsFactory
    Inherits RequirementSwitchFactory(Of IRelatedItems)

    Public Overrides Function ImplementationA() As IRelatedItems

        Return New RelatedItemsNew_For_GetRelatedItems
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(980081)
    End Function

    Public Overrides Function ImplementationB() As IRelatedItems

        Return New RelatedItemsExisting_For_GetRelatedItems
    End Function
End Class
