﻿Public Class PriceChangeFactory

    Private Shared m_FactoryMember As IPriceChange

    Public Shared Function ThisStoreIsAnEStore() As Boolean
        Dim Repository As IPriceChangeRepository

        'get data
        Repository = PriceChangeRepositoryFactory.FactoryGet

        'estore shop or warehouse parameters does not exists
        If Repository.eStoreShopID = 0 And Repository.eStoreWarhouseID = 0 Then Return False

        'no store id
        If Repository.StoreID = 0 + 8000 Then Return False

        'store id match estore shop or warehouse
        If Repository.StoreID = Repository.eStoreShopID OrElse Repository.StoreID = Repository.eStoreWarhouseID Then Return True

        'NOT estore shop or warehouse
        Return False

    End Function

    Public Shared Function FactoryGet() As IPriceChange

        If m_FactoryMember Is Nothing Then

            If ThisStoreIsAnEStore() = True Then
                Return New PriceChange()           'live implementation: new
            Else
                Return New cPriceChange()          'live implementation: existing
            End If

        Else
            Return m_FactoryMember                 'stub implementation
        End If

    End Function

    Public Shared Function FactoryGet(ByRef oasys3DB As Oasys3.DB.clsOasys3DB) As IPriceChange

        If m_FactoryMember Is Nothing Then

            If ThisStoreIsAnEStore() = True Then
                Return New PriceChange(oasys3DB)           'live implementation: new
            Else
                Return New cPriceChange(oasys3DB)          'live implementation: existing
            End If

        Else
            Return m_FactoryMember                         'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IPriceChange)

        m_FactoryMember = obj

    End Sub

End Class