﻿Public Module Extensions

    <Runtime.CompilerServices.Extension()> Sub SetAll(ByRef BooleanValues() As Boolean, ByVal Value As Boolean)
        For Each v As Boolean In BooleanValues
            v = Value
        Next
    End Sub
    <Runtime.CompilerServices.Extension()> Sub SetFirst(ByRef BooleanValues() As Boolean, ByVal Number As Integer, ByVal Value As Boolean)

        Dim limit As Integer = Math.Min(Number, BooleanValues.GetUpperBound(0))
        For index As Integer = 0 To limit - 1
            BooleanValues(index) = Value
        Next

    End Sub
    <Runtime.CompilerServices.Extension()> Sub SetLast(ByRef BooleanValues() As Boolean, ByVal Number As Integer, ByVal Value As Boolean)

        Dim limit As Integer = Math.Max(0, BooleanValues.GetUpperBound(0) - Number)
        For index As Integer = BooleanValues.GetUpperBound(0) - 1 To limit Step -1
            BooleanValues(index) = Value
        Next

    End Sub


    <System.Runtime.CompilerServices.Extension()> Friend Function AverageSales(ByRef Periods As List(Of cStock.SoqPeriod)) As Decimal

        If Periods.Count = 0 Then Return 0
        Return CDec(Periods.Average(Function(p As cStock.SoqPeriod) p.Sales))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function AverageSalesAdjusted(ByRef Periods As List(Of cStock.SoqPeriod)) As Decimal

        If Periods.Count = 0 Then Return 0
        Return Periods.Average(Function(p As cStock.SoqPeriod) p.AdjustedSales)

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CountZeroSales(ByRef Periods As List(Of cStock.SoqPeriod)) As Integer

        Dim count As Integer = 0
        For Each period As cStock.SoqPeriod In Periods.Where(Function(p) p.Sales = 0)
            count += 1
        Next
        Return count

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CountZeroSalesConsecutive(ByRef Periods As List(Of cStock.SoqPeriod)) As Integer

        Dim count As Integer = 0
        For Each period As cStock.SoqPeriod In Periods
            If period.Sales = 0 Then
                count += 1
            Else
                Exit For
            End If
        Next
        Return count

    End Function


    <System.Runtime.CompilerServices.Extension()> Friend Function StdDevSales(ByRef Periods As List(Of cStock.SoqPeriod)) As Decimal

        If Periods.Count <= 1 Then Return 0
        Dim sumR As Double = 0
        Dim sumRR As Double = 0

        For Each period As cStock.SoqPeriod In Periods
            sumR += period.AdjustedSales
            sumRR += Math.Pow(period.AdjustedSales, 2)
        Next

        sumR = Math.Pow(sumR, 2)
        sumR /= Periods.Count

        Dim stdDev As Decimal = CDec(sumRR - sumR)
        stdDev /= (Periods.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function StdDevPeriods(ByRef Periods As List(Of cStock.SoqPeriod)) As Decimal

        If Periods.Count <= 1 Then Return 0
        Dim sumP As Double = 0
        Dim sumPP As Double = 0

        For Each period As cStock.SoqPeriod In Periods
            sumP += period.Index
            sumPP += Math.Pow(period.Index, 2)
        Next

        Dim stdDev As Decimal = CDec(sumPP - (Math.Pow(sumP, 2) / Periods.Count))
        stdDev /= (Periods.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CorrelationCoeff(ByRef Periods As List(Of cStock.SoqPeriod), ByVal StdDevPeriodSales As Decimal, ByVal StdDevPeriodPeriods As Decimal) As Decimal

        If Periods.Count <= 1 Then Return 0
        Dim sumR As Double = 0
        Dim sumP As Double = 0
        Dim sumRP As Double = 0

        For Each period As cStock.SoqPeriod In Periods
            sumR += period.AdjustedSales
            sumP += period.Index
            sumRP += (period.Index * period.Sales)
        Next

        Dim stdDev As Decimal = CDec(sumRP - (sumR * sumP / Periods.Count))
        stdDev /= (StdDevPeriodSales * StdDevPeriodPeriods * (Periods.Count - 1))
        Return stdDev

    End Function

    ''' <summary>
    ''' Returns arraylist of sku numbers in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function SkuNumbers(ByRef Adjusts As List(Of cStockAdjust)) As ArrayList

        If Adjusts Is Nothing Then Return Nothing
        Dim skus As New ArrayList
        For Each adj As cStockAdjust In Adjusts
            If Not skus.Contains(adj.SkuNumber.Value) Then skus.Add(adj.SkuNumber.Value)
        Next

        Return skus

    End Function

    ''' <summary>
    ''' Returns arraylist of sku numbers in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function SkuNumbers(ByRef markdowns As List(Of cMarkdownStock)) As ArrayList

        If markdowns Is Nothing Then Return Nothing
        Dim skus As New ArrayList
        For Each mark As cMarkdownStock In markdowns
            If Not skus.Contains(mark.SkuNumber.Value) Then skus.Add(mark.SkuNumber.Value)
        Next

        Return skus

    End Function

    <System.Runtime.CompilerServices.Extension()> Public Function CountActive(ByRef markdowns As List(Of cMarkdownStock)) As Integer

        Dim total As Integer = 0
        For Each mark As cMarkdownStock In markdowns
            If Not mark.IsReserved.Value And mark.SoldTill.Value <> "00" Then total += 1
        Next


    End Function

End Module
