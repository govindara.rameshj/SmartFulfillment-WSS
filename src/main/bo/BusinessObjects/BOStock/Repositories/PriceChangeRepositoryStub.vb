﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("BOStock.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                       "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                       "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                       "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                       "54e0a4a4")> 
<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("PriceChangeApply.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                       "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                       "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                       "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                       "54e0a4a4")> 
Public Class PriceChangeRepositoryStub
    Implements IPriceChangeRepository

#Region "Stub Code"

    Private _StoreID As Integer
    Private _eStoreShopID As Integer
    Private _eStoreWarehouseID As Integer
    Private _Data As New DataTable

    Friend Sub ConfigureStub(ByVal StoreID As Integer, ByVal eStoreShopID As Integer, ByVal eStoreWarehouseID As Integer, ByRef DT As DataTable)

        _StoreID = StoreID + 8000
        _eStoreShopID = eStoreShopID
        _eStoreWarehouseID = eStoreWarehouseID

        _Data = DT

    End Sub

#End Region

#Region "Interface"

    Public Function eStoreShopID() As Integer Implements IPriceChangeRepository.eStoreShopID

        Return _eStoreShopID

    End Function

    Public Function eStoreWarhouseID() As Integer Implements IPriceChangeRepository.eStoreWarhouseID

        Return _eStoreWarehouseID

    End Function

    Public Function StoreID() As Integer Implements IPriceChangeRepository.StoreID

        Return _StoreID

    End Function

    Public Function Load(ByVal DateSelected As Date) As System.Data.DataTable Implements IPriceChangeRepository.Load

        Return _Data

    End Function

#End Region

End Class
