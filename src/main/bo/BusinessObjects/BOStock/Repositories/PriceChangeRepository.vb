﻿Public Class PriceChangeRepository
    Implements IPriceChangeRepository

    Public Function eStoreShopID() As Integer Implements IPriceChangeRepository.eStoreShopID

        Return Parameter.GetInteger(3020)

    End Function

    Public Function eStoreWarhouseID() As Integer Implements IPriceChangeRepository.eStoreWarhouseID

        Return Parameter.GetInteger(3030)

    End Function

    Public Function StoreID() As Integer Implements IPriceChangeRepository.StoreID

        Return Store.GetId4()

    End Function

    Public Function Load(ByVal DateSelected As Date) As System.Data.DataTable Implements IPriceChangeRepository.Load

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "PriceChangeLoad"
                com.AddParameter("@Date", DateSelected)

                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

End Class