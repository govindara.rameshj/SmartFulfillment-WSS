﻿<Serializable()> Public Class cPimItems
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PIMMAS"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_CategoryCode)
        BOFields.Add(_CategoryDescription)
        BOFields.Add(_GroupCode)
        BOFields.Add(_GroupDescription)
        BOFields.Add(_SubgroupCode)
        BOFields.Add(_SubgroupDescription)
        BOFields.Add(_StyleCode)
        BOFields.Add(_StyleDescription)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPimItems)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPimItems)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPimItems))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPimItems(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "", "Sku number", True, False)
    Private _CategoryCode As New ColField(Of String)("CategoryCode", "", "Category Code", False, False)
    Private _CategoryDescription As New ColField(Of String)("CategoryDescr", "", "Category Description", False, False)
    Private _GroupCode As New ColField(Of String)("GroupCode", "", "Group Code", False, False)
    Private _GroupDescription As New ColField(Of String)("GroupDescr", "", "Group Description", False, False)
    Private _SubgroupCode As New ColField(Of String)("SubGroupCode", "", "Subgroup Code", False, False)
    Private _SubgroupDescription As New ColField(Of String)("SubGroupDescr", "", "Subgroup Description", False, False)
    Private _StyleCode As New ColField(Of String)("StyleCode", "", "Stlye Code", False, False)
    Private _StyleDescription As New ColField(Of String)("StyleDescr", "", "Style Description", False, False)

#End Region

#Region "Field Properties"

    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property CategoryCode() As ColField(Of String)
        Get
            CategoryCode = _CategoryCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CategoryCode = value
        End Set
    End Property
    Public Property CategoryDescription() As ColField(Of String)
        Get
            CategoryDescription = _CategoryDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _CategoryDescription = value
        End Set
    End Property
    Public Property GroupCode() As ColField(Of String)
        Get
            GroupCode = _GroupCode
        End Get
        Set(ByVal value As ColField(Of String))
            _GroupCode = value
        End Set
    End Property
    Public Property GroupDescription() As ColField(Of String)
        Get
            GroupDescription = _GroupDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _GroupDescription = value
        End Set
    End Property
    Public Property SubgroupCode() As ColField(Of String)
        Get
            SubgroupCode = _SubgroupCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SubgroupCode = value
        End Set
    End Property
    Public Property SubgroupDescription() As ColField(Of String)
        Get
            SubgroupDescription = _SubgroupDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _SubgroupDescription = value
        End Set
    End Property
    Public Property StyleCode() As ColField(Of String)
        Get
            StyleCode = _StyleCode
        End Get
        Set(ByVal value As ColField(Of String))
            _StyleCode = value
        End Set
    End Property
    Public Property StyleDescription() As ColField(Of String)
        Get
            StyleDescription = _StyleDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _StyleDescription = value
        End Set
    End Property

#End Region

End Class
