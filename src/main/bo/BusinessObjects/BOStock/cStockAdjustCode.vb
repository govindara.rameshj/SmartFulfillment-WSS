﻿<Serializable()> Public Class cStockAdjustCode
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SACODE"
        BOFields.Add(_Number)
        BOFields.Add(_Description)
        BOFields.Add(_Type)
        BOFields.Add(_Sign)
        BOFields.Add(_SecurityLevel)
        BOFields.Add(_IsMarkdown)
        BOFields.Add(_IsWriteOff)
        BOFields.Add(_IsPassRequired)
        BOFields.Add(_IsReserved)
        BOFields.Add(_IsCommentable)
        BOFields.Add(_IsAuthRequired)
        BOFields.Add(_IsStockLoss)
        BOFields.Add(_IsKnownTheft)
        BOFields.Add(_IsIssueQuery)
        BOFields.Add(_IsReceiptAdjustment)
        BOFields.Add(_AllowCodes)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStockAdjustCode)

        LoadBORecords(count)

        Dim col As New List(Of cStockAdjustCode)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cStockAdjustCode))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStockAdjustCode(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("NUMB", "", "Code", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _Type As New ColField(Of String)("TYPE", "", "Type", False, False)
    Private _Sign As New ColField(Of String)("SIGN", "", "Action", False, False)
    Private _SecurityLevel As New ColField(Of String)("SECL", "0", "Security Level", False, False)
    Private _IsMarkdown As New ColField(Of Boolean)("IMDN", False, "Is Markdown", False, False)
    Private _IsWriteOff As New ColField(Of Boolean)("IWTF", False, "Is Write Off", False, False)
    Private _IsAuthRequired As New ColField(Of Boolean)("IsAuthRequired", False, "Is Auth Required", False, False)
    Private _IsPassRequired As New ColField(Of Boolean)("IsPassRequired", False, "Is Pass Required", False, False)
    Private _IsReserved As New ColField(Of Boolean)("IsReserved", False, "Is Reversed", False, False)
    Private _IsCommentable As New ColField(Of Boolean)("IsCommentable", False, "Is Commentable", False, False)
    Private _IsStockLoss As New ColField(Of Boolean)("IsStockLoss", False, "Is Stock Loss", False, False)
    Private _IsKnownTheft As New ColField(Of Boolean)("IsKnownTheft", False, "Is Known Theft", False, False)
    Private _IsIssueQuery As New ColField(Of Boolean)("IsIssueQuery", False, "Is Issue Query", False, False)
    Private _IsReceiptAdjustment As New ColField(Of Boolean)("IsReceiptAdjustment", False, "Is Receipt Adjustment", False, False)
    Private _AllowCodes As New ColField(Of String)("AllowCodes", "", "Allow Codes", False, False)

#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Return _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Return _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property Sign() As ColField(Of String)
        Get
            Return _Sign
        End Get
        Set(ByVal value As ColField(Of String))
            _Sign = value
        End Set
    End Property
    Public Property SecurityLevel() As ColField(Of String)
        Get
            Return _SecurityLevel
        End Get
        Set(ByVal value As ColField(Of String))
            _SecurityLevel = value
        End Set
    End Property
    Public Property IsMarkdown() As ColField(Of Boolean)
        Get
            Return _IsMarkdown
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsMarkdown = value
        End Set
    End Property
    Public Property IsWriteOff() As ColField(Of Boolean)
        Get
            Return _IsWriteOff
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsWriteOff = value
        End Set
    End Property
    Public Property IsPassRequired() As ColField(Of Boolean)
        Get
            Return _IsPassRequired
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsPassRequired = value
        End Set
    End Property
    Public Property IsAuthRequired() As ColField(Of Boolean)
        Get
            Return _IsAuthRequired
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsAuthRequired = value
        End Set
    End Property
    Public Property IsReserved() As ColField(Of Boolean)
        Get
            Return _IsReserved
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsReserved = value
        End Set
    End Property
    Public Property IsCommentable() As ColField(Of Boolean)
        Get
            Return _IsCommentable
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsCommentable = value
        End Set
    End Property
    Public Property IsStockLoss() As ColField(Of Boolean)
        Get
            Return _IsStockLoss
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsStockLoss = value
        End Set
    End Property
    Public Property IsKnownTheft() As ColField(Of Boolean)
        Get
            Return _IsKnownTheft
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsKnownTheft = value
        End Set
    End Property
    Public Property IsIssueQuery() As ColField(Of Boolean)
        Get
            Return _IsIssueQuery
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsIssueQuery = value
        End Set
    End Property
    Public Property IsReceiptAdjustment() As ColField(Of Boolean)
        Get
            Return _IsReceiptAdjustment
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsReceiptAdjustment = value
        End Set
    End Property
    Public Property AllowCodes() As ColField(Of String)
        Get
            Return _AllowCodes
        End Get
        Set(ByVal value As ColField(Of String))
            _AllowCodes = value
        End Set
    End Property

#End Region

#Region "Enumerations"
    Private _MarkdownWriteOff As MarkdownWriteOffs = Nothing

    Public Enum Types
        Normal
        Transfer
    End Enum
    Public Enum Signs
        Add
        Subtract
        AddSubtract
        Replace
        Calculate
    End Enum
    Public Enum MarkdownWriteOffs
        None
        Both
        Markdown
        WriteOff
    End Enum


    Public Property MarkdownWriteOff() As MarkdownWriteOffs
        Get
            If _MarkdownWriteOff = Nothing Then
                If _IsMarkdown.Value And _IsWriteOff.Value Then
                    _MarkdownWriteOff = MarkdownWriteOffs.Both

                ElseIf _IsMarkdown.Value Then
                    _MarkdownWriteOff = MarkdownWriteOffs.Markdown

                ElseIf _IsWriteOff.Value Then
                    _MarkdownWriteOff = MarkdownWriteOffs.WriteOff
                Else
                    _MarkdownWriteOff = MarkdownWriteOffs.None
                End If
            End If
            Return _MarkdownWriteOff
        End Get
        Set(ByVal value As MarkdownWriteOffs)
            _MarkdownWriteOff = value
        End Set
    End Property
    Public Property TypeValue() As Types
        Get
            Select Case _Type.Value
                Case "N" : Return Types.Normal
                Case "T" : Return Types.Transfer
            End Select
        End Get
        Set(ByVal value As Types)
            Select Case value
                Case Types.Normal : _Type.Value = "N"
                Case Types.Transfer : _Type.Value = "T"
            End Select
        End Set
    End Property

#End Region

#Region "Data"
    Private _data As New DataTable
    Private _dataDisplay As New DataColumn("DataDisplay", GetType(String), _Number.ColumnName & " + ' ' + " & _Description.ColumnName)
    Private _typeDisplay As New DataColumn("TypeDisplay", GetType(String))
    Private _signDisplay As New DataColumn("SignDisplay", GetType(String))
    Private _markdownDisplay As New DataColumn("MarkdownDisplay", GetType(String))
    Private _writeOffDisplay As New DataColumn("WriteOffDisplay", GetType(String))

    Public ReadOnly Property Data() As DataTable
        Get
            Return _data
        End Get
    End Property
    Public ReadOnly Property TypeDisplay() As DataColumn
        Get
            Return _typeDisplay
        End Get
    End Property
    Public ReadOnly Property SignDisplay() As DataColumn
        Get
            Return _signDisplay
        End Get
    End Property
    Public ReadOnly Property MarkdownDisplay() As DataColumn
        Get
            Return _markdownDisplay
        End Get
    End Property
    Public ReadOnly Property WriteOffDisplay() As DataColumn
        Get
            Return _writeOffDisplay
        End Get
    End Property
    Public ReadOnly Property DataDisplay() As DataColumn
        Get
            Return _dataDisplay
        End Get
    End Property

    ''' <summary>
    ''' Loads all stock adjustment codes into data table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DataLoad(Optional ByVal securityLevel As Integer = 9)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnParameter(_Number.ColumnName)
        Oasys3DB.SetColumnParameter(_Description.ColumnName)
        Oasys3DB.SetColumnParameter(_Type.ColumnName)
        Oasys3DB.SetColumnParameter(_Sign.ColumnName)
        Oasys3DB.SetColumnParameter(_SecurityLevel.ColumnName)
        Oasys3DB.SetColumnParameter(_IsMarkdown.ColumnName)
        Oasys3DB.SetColumnParameter(_IsWriteOff.ColumnName)
        Oasys3DB.SetColumnParameter(_IsPassRequired.ColumnName)
        Oasys3DB.SetColumnParameter(_IsReserved.ColumnName)
        Oasys3DB.SetColumnParameter(_IsCommentable.ColumnName)
        Oasys3DB.SetColumnParameter(_IsAuthRequired.ColumnName)
        Oasys3DB.SetColumnParameter(_IsStockLoss.ColumnName)
        Oasys3DB.SetColumnParameter(_IsKnownTheft.ColumnName)
        Oasys3DB.SetColumnParameter(_IsIssueQuery.ColumnName)
        Oasys3DB.SetColumnParameter(_AllowCodes.ColumnName)
        Oasys3DB.SetColumnParameter(_IsReceiptAdjustment.ColumnName)
        Oasys3DB.SetWhereParameter(_SecurityLevel.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, securityLevel)
        Oasys3DB.SetOrderByParameter(_Number.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        _data = Oasys3DB.Query.Tables(0)
        _data.Columns.Add(_dataDisplay)
        _data.Columns.Add(_typeDisplay)
        _data.Columns.Add(_signDisplay)
        _data.Columns.Add(_markdownDisplay)
        _data.Columns.Add(_writeOffDisplay)

        For Each dr As DataRow In _data.Rows
            dr(_typeDisplay.ColumnName) = TypeString(CStr(dr(_Type.ColumnName)))
            dr(_signDisplay.ColumnName) = SignString(CStr(dr(_Sign.ColumnName)))
            If CBool(dr(_IsMarkdown.ColumnName)) Then
                dr(_markdownDisplay.ColumnName) = "Yes"
            Else
                dr(_markdownDisplay.ColumnName) = "No"
            End If
            If CBool(dr(_IsWriteOff.ColumnName)) Then
                dr(_writeOffDisplay.ColumnName) = "Yes"
            Else
                dr(_writeOffDisplay.ColumnName) = "No"
            End If
        Next

    End Sub
#End Region

#Region "Entities"
    Private _Codes As List(Of cStockAdjustCode) = Nothing

    Public Property Codes() As List(Of cStockAdjustCode)
        Get
            If _Codes Is Nothing Then
                LoadCodes(9)
            End If
            Return _Codes
        End Get
        Set(ByVal value As List(Of cStockAdjustCode))
            _Codes = value
        End Set
    End Property
    Public Property Code(ByVal number As String) As cStockAdjustCode
        Get
            If _Codes IsNot Nothing Then
                For Each c As cStockAdjustCode In _Codes
                    If c.Number.Value = number Then Return c
                Next
            End If
            Return Nothing
        End Get
        Set(ByVal value As cStockAdjustCode)
            If _Codes IsNot Nothing Then
                For Each c As cStockAdjustCode In _Codes
                    If c.Number.Value = number Then
                        c = value
                    End If
                Next
            End If
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function TypeString() As String

        Select Case _Type.Value
            Case "N" : Return "Normal"
            Case "T" : Return "Transfer"
            Case Else : Return String.Empty
        End Select

    End Function
    Public Function TypeString(ByVal type As String) As String

        Select Case type
            Case "N" : Return "Normal"
            Case "T" : Return "Transfer"
            Case Else : Return String.Empty
        End Select

    End Function

    Public Function SignValue() As Signs

        Select Case _Sign.Value
            Case "+" : Return Signs.Add
            Case "-" : Return Signs.Subtract
            Case "S", "T" : Return Signs.AddSubtract
            Case "R" : Return Signs.Replace
            Case "C" : Return Signs.Calculate
            Case Else : Return Nothing
        End Select

    End Function
    Public Function SignString() As String

        Select Case _Sign.Value
            Case "+" : Return My.Resources.Messages.AdjustCode_SignAdd
            Case "-" : Return My.Resources.Messages.AdjustCode_SignSubtract
            Case "S" : Return My.Resources.Messages.AdjustCode_SignAddSubtract
            Case "R" : Return My.Resources.Messages.AdjustCode_SignReplace
            Case "C" : Return My.Resources.Messages.AdjustCode_SignCalculate
            Case Else : Return String.Empty
        End Select

    End Function
    Public Function SignString(ByVal SignCharacter As String) As String

        Select Case SignCharacter
            Case "+" : Return My.Resources.Messages.AdjustCode_SignAdd
            Case "-" : Return My.Resources.Messages.AdjustCode_SignSubtract
            Case "S" : Return My.Resources.Messages.AdjustCode_SignAddSubtract
            Case "R" : Return My.Resources.Messages.AdjustCode_SignReplace
            Case "C" : Return My.Resources.Messages.AdjustCode_SignCalculate
            Case Else : Return String.Empty
        End Select

    End Function

    Public Function MarkdownWriteOffString() As String

        Select Case _MarkdownWriteOff
            Case MarkdownWriteOffs.Both : Return My.Resources.Messages.AdjustCode_Both
            Case MarkdownWriteOffs.Markdown : Return My.Resources.Messages.AdjustCode_Markdown
            Case MarkdownWriteOffs.WriteOff : Return My.Resources.Messages.AdjustCode_WriteOff
            Case Else : Return String.Empty
        End Select

    End Function
    Public Function MarkdownWriteOffString(ByVal MarkdownWriteOffValue As MarkdownWriteOffs) As String

        Select Case MarkdownWriteOffValue
            Case MarkdownWriteOffs.Both : Return My.Resources.Messages.AdjustCode_Both
            Case MarkdownWriteOffs.Markdown : Return My.Resources.Messages.AdjustCode_Markdown
            Case MarkdownWriteOffs.WriteOff : Return My.Resources.Messages.AdjustCode_WriteOff
            Case Else : Return String.Empty
        End Select

    End Function

    ''' <summary>
    ''' Loads valid adjustment codes into codes collection less than or equal to given security level
    ''' </summary>
    ''' <param name="SecurityLevel"></param>
    ''' <remarks></remarks>
    Public Sub LoadCodes(ByVal SecurityLevel As Integer)

        ClearLists()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsReserved, False)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, _SecurityLevel, SecurityLevel.ToString)
        _Codes = LoadMatches()

    End Sub

    Public Sub Load(ByVal codeNumber As String)

        ClearLists()
        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _Number, codeNumber)
        LoadMatches()

    End Sub

    ''' <summary>
    ''' Returns dataset of loaded codes
    ''' </summary>
    ''' <param name="WithAll">If true will insert ---All--- with number 0</param>
    ''' <returns></returns>
    ''' <remarks>returns full text for type and sign</remarks>
    Public Function GetCodeDatatable(Optional ByVal WithAll As Boolean = False) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add(_Number.ColumnName, GetType(String))
        dt.Columns.Add(_Description.ColumnName, GetType(String))
        dt.Columns.Add(_Type.ColumnName, GetType(String))
        dt.Columns.Add(_Sign.ColumnName, GetType(String))
        dt.Columns.Add(_SecurityLevel.ColumnName, GetType(String))
        dt.Columns.Add(_IsMarkdown.ColumnName, GetType(Boolean))
        dt.Columns.Add(_IsWriteOff.ColumnName, GetType(Boolean))
        dt.Columns.Add(_IsCommentable.ColumnName, GetType(Boolean))

        If WithAll Then
            Dim dr As DataRow = dt.NewRow
            dr(0) = "00"
            dr(1) = "---All---"
            dr(2) = ""
            dr(3) = ""
            dr(4) = ""
            dr(5) = DBNull.Value
            dr(6) = DBNull.Value
            dr(7) = DBNull.Value
            dt.Rows.Add(dr)
        End If

        'set type and sign text here
        For Each c As cStockAdjustCode In Codes
            Dim dr As DataRow = dt.NewRow
            dr(0) = c.Number.Value
            dr(1) = c.Description.Value.Trim
            dr(2) = TypeString(c.Type.Value)
            dr(3) = SignString(c.Sign.Value)
            dr(4) = c.SecurityLevel.Value
            dr(5) = c.IsMarkdown.Value
            dr(6) = c.IsWriteOff.Value
            dr(7) = c.IsCommentable.Value
            dt.Rows.Add(dr)
        Next

        Return dt

    End Function

    ''' <summary>
    ''' Returns arraylist of stock loss codes.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetStockLossCodes() As ArrayList

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnParameter(_Number.ColumnName)
            Oasys3DB.SetWhereParameter(_IsStockLoss.ColumnName, clsOasys3DB.eOperator.pEquals, True)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim al As New ArrayList
            For Each dr As DataRow In dt.Rows
                al.Add(dr(0))
            Next

            Return al

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustCodeLoad, ex)
        End Try

    End Function

#End Region

End Class





