﻿<Serializable()> Public Class cSOQPattern
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SOQPAT"
        BOFields.Add(_Number)
        BOFields.Add(_Year)
        BOFields.Add(_PatternWeek1)
        BOFields.Add(_PatternWeek2)
        BOFields.Add(_PatternWeek3)
        BOFields.Add(_PatternWeek4)
        BOFields.Add(_PatternWeek5)
        BOFields.Add(_PatternWeek6)
        BOFields.Add(_PatternWeek7)
        BOFields.Add(_PatternWeek8)
        BOFields.Add(_PatternWeek9)
        BOFields.Add(_PatternWeek10)
        BOFields.Add(_PatternWeek11)
        BOFields.Add(_PatternWeek12)
        BOFields.Add(_PatternWeek13)
        BOFields.Add(_PatternWeek14)
        BOFields.Add(_PatternWeek15)
        BOFields.Add(_PatternWeek16)
        BOFields.Add(_PatternWeek17)
        BOFields.Add(_PatternWeek18)
        BOFields.Add(_PatternWeek19)
        BOFields.Add(_PatternWeek20)
        BOFields.Add(_PatternWeek21)
        BOFields.Add(_PatternWeek22)
        BOFields.Add(_PatternWeek23)
        BOFields.Add(_PatternWeek24)
        BOFields.Add(_PatternWeek25)
        BOFields.Add(_PatternWeek26)
        BOFields.Add(_PatternWeek27)
        BOFields.Add(_PatternWeek28)
        BOFields.Add(_PatternWeek29)
        BOFields.Add(_PatternWeek30)
        BOFields.Add(_PatternWeek31)
        BOFields.Add(_PatternWeek32)
        BOFields.Add(_PatternWeek33)
        BOFields.Add(_PatternWeek34)
        BOFields.Add(_PatternWeek35)
        BOFields.Add(_PatternWeek36)
        BOFields.Add(_PatternWeek37)
        BOFields.Add(_PatternWeek38)
        BOFields.Add(_PatternWeek39)
        BOFields.Add(_PatternWeek40)
        BOFields.Add(_PatternWeek41)
        BOFields.Add(_PatternWeek42)
        BOFields.Add(_PatternWeek43)
        BOFields.Add(_PatternWeek44)
        BOFields.Add(_PatternWeek45)
        BOFields.Add(_PatternWeek46)
        BOFields.Add(_PatternWeek47)
        BOFields.Add(_PatternWeek48)
        BOFields.Add(_PatternWeek49)
        BOFields.Add(_PatternWeek50)
        BOFields.Add(_PatternWeek51)
        BOFields.Add(_PatternWeek52)
        BOFields.Add(_PatternWeek53)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSOQPattern)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSOQPattern)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSOQPattern))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSOQPattern(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("PATT") : _Number.Value = CStr(drRow(dcColumn))
                    Case ("YEAR") : _Year.Value = CDate(drRow(dcColumn))
                    Case ("WPAR1") : _PatternWeek1.Value = CDec(drRow(dcColumn))
                    Case ("WPAR2") : _PatternWeek2.Value = CDec(drRow(dcColumn))
                    Case ("WPAR3") : _PatternWeek3.Value = CDec(drRow(dcColumn))
                    Case ("WPAR4") : _PatternWeek4.Value = CDec(drRow(dcColumn))
                    Case ("WPAR5") : _PatternWeek5.Value = CDec(drRow(dcColumn))
                    Case ("WPAR6") : _PatternWeek6.Value = CDec(drRow(dcColumn))
                    Case ("WPAR7") : _PatternWeek7.Value = CDec(drRow(dcColumn))
                    Case ("WPAR8") : _PatternWeek8.Value = CDec(drRow(dcColumn))
                    Case ("WPAR9") : _PatternWeek9.Value = CDec(drRow(dcColumn))
                    Case ("WPAR10") : _PatternWeek10.Value = CDec(drRow(dcColumn))
                    Case ("WPAR11") : _PatternWeek11.Value = CDec(drRow(dcColumn))
                    Case ("WPAR12") : _PatternWeek12.Value = CDec(drRow(dcColumn))
                    Case ("WPAR13") : _PatternWeek13.Value = CDec(drRow(dcColumn))
                    Case ("WPAR14") : _PatternWeek14.Value = CDec(drRow(dcColumn))
                    Case ("WPAR15") : _PatternWeek15.Value = CDec(drRow(dcColumn))
                    Case ("WPAR16") : _PatternWeek16.Value = CDec(drRow(dcColumn))
                    Case ("WPAR17") : _PatternWeek17.Value = CDec(drRow(dcColumn))
                    Case ("WPAR18") : _PatternWeek18.Value = CDec(drRow(dcColumn))
                    Case ("WPAR19") : _PatternWeek19.Value = CDec(drRow(dcColumn))
                    Case ("WPAR20") : _PatternWeek20.Value = CDec(drRow(dcColumn))
                    Case ("WPAR21") : _PatternWeek21.Value = CDec(drRow(dcColumn))
                    Case ("WPAR22") : _PatternWeek22.Value = CDec(drRow(dcColumn))
                    Case ("WPAR23") : _PatternWeek23.Value = CDec(drRow(dcColumn))
                    Case ("WPAR24") : _PatternWeek24.Value = CDec(drRow(dcColumn))
                    Case ("WPAR25") : _PatternWeek25.Value = CDec(drRow(dcColumn))
                    Case ("WPAR26") : _PatternWeek26.Value = CDec(drRow(dcColumn))
                    Case ("WPAR27") : _PatternWeek27.Value = CDec(drRow(dcColumn))
                    Case ("WPAR28") : _PatternWeek28.Value = CDec(drRow(dcColumn))
                    Case ("WPAR29") : _PatternWeek29.Value = CDec(drRow(dcColumn))
                    Case ("WPAR30") : _PatternWeek30.Value = CDec(drRow(dcColumn))
                    Case ("WPAR31") : _PatternWeek31.Value = CDec(drRow(dcColumn))
                    Case ("WPAR32") : _PatternWeek32.Value = CDec(drRow(dcColumn))
                    Case ("WPAR33") : _PatternWeek33.Value = CDec(drRow(dcColumn))
                    Case ("WPAR34") : _PatternWeek34.Value = CDec(drRow(dcColumn))
                    Case ("WPAR35") : _PatternWeek35.Value = CDec(drRow(dcColumn))
                    Case ("WPAR36") : _PatternWeek36.Value = CDec(drRow(dcColumn))
                    Case ("WPAR37") : _PatternWeek37.Value = CDec(drRow(dcColumn))
                    Case ("WPAR38") : _PatternWeek38.Value = CDec(drRow(dcColumn))
                    Case ("WPAR39") : _PatternWeek39.Value = CDec(drRow(dcColumn))
                    Case ("WPAR40") : _PatternWeek40.Value = CDec(drRow(dcColumn))
                    Case ("WPAR41") : _PatternWeek41.Value = CDec(drRow(dcColumn))
                    Case ("WPAR42") : _PatternWeek42.Value = CDec(drRow(dcColumn))
                    Case ("WPAR43") : _PatternWeek43.Value = CDec(drRow(dcColumn))
                    Case ("WPAR44") : _PatternWeek44.Value = CDec(drRow(dcColumn))
                    Case ("WPAR45") : _PatternWeek45.Value = CDec(drRow(dcColumn))
                    Case ("WPAR46") : _PatternWeek46.Value = CDec(drRow(dcColumn))
                    Case ("WPAR47") : _PatternWeek47.Value = CDec(drRow(dcColumn))
                    Case ("WPAR48") : _PatternWeek48.Value = CDec(drRow(dcColumn))
                    Case ("WPAR49") : _PatternWeek49.Value = CDec(drRow(dcColumn))
                    Case ("WPAR50") : _PatternWeek50.Value = CDec(drRow(dcColumn))
                    Case ("WPAR51") : _PatternWeek51.Value = CDec(drRow(dcColumn))
                    Case ("WPAR52") : _PatternWeek52.Value = CDec(drRow(dcColumn))
                    Case ("WPAR53") : _PatternWeek53.Value = CDec(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("PATT", "0000", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Number")
    Private _Year As New ColField(Of Date)("YEAR", Nothing, True, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Year")
    Private _PatternWeek1 As New ColField(Of Decimal)("WPAR1", 1, False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Pattern Week 1")
    Private _PatternWeek2 As New ColField(Of Decimal)("WPAR2", 1, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Pattern Week 2")
    Private _PatternWeek3 As New ColField(Of Decimal)("WPAR3", 1, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Pattern Week 3")
    Private _PatternWeek4 As New ColField(Of Decimal)("WPAR4", 1, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Pattern Week 4")
    Private _PatternWeek5 As New ColField(Of Decimal)("WPAR5", 1, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Pattern Week 5")
    Private _PatternWeek6 As New ColField(Of Decimal)("WPAR6", 1, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Pattern Week 6")
    Private _PatternWeek7 As New ColField(Of Decimal)("WPAR7", 1, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Pattern Week 7")
    Private _PatternWeek8 As New ColField(Of Decimal)("WPAR8", 1, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Pattern Week 8")
    Private _PatternWeek9 As New ColField(Of Decimal)("WPAR9", 1, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Pattern Week 9")
    Private _PatternWeek10 As New ColField(Of Decimal)("WPAR10", 1, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Pattern Week 10")
    Private _PatternWeek11 As New ColField(Of Decimal)("WPAR11", 1, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Pattern Week 11")
    Private _PatternWeek12 As New ColField(Of Decimal)("WPAR12", 1, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Pattern Week 12")
    Private _PatternWeek13 As New ColField(Of Decimal)("WPAR13", 1, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Pattern Week 13")
    Private _PatternWeek14 As New ColField(Of Decimal)("WPAR14", 1, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Pattern Week 14")
    Private _PatternWeek15 As New ColField(Of Decimal)("WPAR15", 1, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "Pattern Week 15")
    Private _PatternWeek16 As New ColField(Of Decimal)("WPAR16", 1, False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "Pattern Week 16")
    Private _PatternWeek17 As New ColField(Of Decimal)("WPAR17", 1, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "Pattern Week 17")
    Private _PatternWeek18 As New ColField(Of Decimal)("WPAR18", 1, False, False, 1, 0, True, True, 21, 0, 0, 0, "", 0, "Pattern Week 18")
    Private _PatternWeek19 As New ColField(Of Decimal)("WPAR19", 1, False, False, 1, 0, True, True, 22, 0, 0, 0, "", 0, "Pattern Week 19")
    Private _PatternWeek20 As New ColField(Of Decimal)("WPAR20", 1, False, False, 1, 0, True, True, 23, 0, 0, 0, "", 0, "Pattern Week 20")
    Private _PatternWeek21 As New ColField(Of Decimal)("WPAR21", 1, False, False, 1, 0, True, True, 24, 0, 0, 0, "", 0, "Pattern Week 21")
    Private _PatternWeek22 As New ColField(Of Decimal)("WPAR22", 1, False, False, 1, 0, True, True, 25, 0, 0, 0, "", 0, "Pattern Week 22")
    Private _PatternWeek23 As New ColField(Of Decimal)("WPAR23", 1, False, False, 1, 0, True, True, 26, 0, 0, 0, "", 0, "Pattern Week 23")
    Private _PatternWeek24 As New ColField(Of Decimal)("WPAR24", 1, False, False, 1, 0, True, True, 27, 0, 0, 0, "", 0, "Pattern Week 24")
    Private _PatternWeek25 As New ColField(Of Decimal)("WPAR25", 1, False, False, 1, 0, True, True, 28, 0, 0, 0, "", 0, "Pattern Week 25")
    Private _PatternWeek26 As New ColField(Of Decimal)("WPAR26", 1, False, False, 1, 0, True, True, 29, 0, 0, 0, "", 0, "Pattern Week 26")
    Private _PatternWeek27 As New ColField(Of Decimal)("WPAR27", 1, False, False, 1, 0, True, True, 30, 0, 0, 0, "", 0, "Pattern Week 27")
    Private _PatternWeek28 As New ColField(Of Decimal)("WPAR28", 1, False, False, 1, 0, True, True, 31, 0, 0, 0, "", 0, "Pattern Week 28")
    Private _PatternWeek29 As New ColField(Of Decimal)("WPAR29", 1, False, False, 1, 0, True, True, 32, 0, 0, 0, "", 0, "Pattern Week 29")
    Private _PatternWeek30 As New ColField(Of Decimal)("WPAR30", 1, False, False, 1, 0, True, True, 33, 0, 0, 0, "", 0, "Pattern Week 30")
    Private _PatternWeek31 As New ColField(Of Decimal)("WPAR31", 1, False, False, 1, 0, True, True, 34, 0, 0, 0, "", 0, "Pattern Week 31")
    Private _PatternWeek32 As New ColField(Of Decimal)("WPAR32", 1, False, False, 1, 0, True, True, 35, 0, 0, 0, "", 0, "Pattern Week 32")
    Private _PatternWeek33 As New ColField(Of Decimal)("WPAR33", 1, False, False, 1, 0, True, True, 36, 0, 0, 0, "", 0, "Pattern Week 33")
    Private _PatternWeek34 As New ColField(Of Decimal)("WPAR34", 1, False, False, 1, 0, True, True, 37, 0, 0, 0, "", 0, "Pattern Week 34")
    Private _PatternWeek35 As New ColField(Of Decimal)("WPAR35", 1, False, False, 1, 0, True, True, 38, 0, 0, 0, "", 0, "Pattern Week 35")
    Private _PatternWeek36 As New ColField(Of Decimal)("WPAR36", 1, False, False, 1, 0, True, True, 39, 0, 0, 0, "", 0, "Pattern Week 36")
    Private _PatternWeek37 As New ColField(Of Decimal)("WPAR37", 1, False, False, 1, 0, True, True, 40, 0, 0, 0, "", 0, "Pattern Week 37")
    Private _PatternWeek38 As New ColField(Of Decimal)("WPAR38", 1, False, False, 1, 0, True, True, 41, 0, 0, 0, "", 0, "Pattern Week 38")
    Private _PatternWeek39 As New ColField(Of Decimal)("WPAR39", 1, False, False, 1, 0, True, True, 42, 0, 0, 0, "", 0, "Pattern Week 39")
    Private _PatternWeek40 As New ColField(Of Decimal)("WPAR40", 1, False, False, 1, 0, True, True, 43, 0, 0, 0, "", 0, "Pattern Week 40")
    Private _PatternWeek41 As New ColField(Of Decimal)("WPAR41", 1, False, False, 1, 0, True, True, 44, 0, 0, 0, "", 0, "Pattern Week 41")
    Private _PatternWeek42 As New ColField(Of Decimal)("WPAR42", 1, False, False, 1, 0, True, True, 45, 0, 0, 0, "", 0, "Pattern Week 42")
    Private _PatternWeek43 As New ColField(Of Decimal)("WPAR43", 1, False, False, 1, 0, True, True, 46, 0, 0, 0, "", 0, "Pattern Week 43")
    Private _PatternWeek44 As New ColField(Of Decimal)("WPAR44", 1, False, False, 1, 0, True, True, 47, 0, 0, 0, "", 0, "Pattern Week 44")
    Private _PatternWeek45 As New ColField(Of Decimal)("WPAR45", 1, False, False, 1, 0, True, True, 48, 0, 0, 0, "", 0, "Pattern Week 45")
    Private _PatternWeek46 As New ColField(Of Decimal)("WPAR46", 1, False, False, 1, 0, True, True, 49, 0, 0, 0, "", 0, "Pattern Week 46")
    Private _PatternWeek47 As New ColField(Of Decimal)("WPAR47", 1, False, False, 1, 0, True, True, 50, 0, 0, 0, "", 0, "Pattern Week 47")
    Private _PatternWeek48 As New ColField(Of Decimal)("WPAR48", 1, False, False, 1, 0, True, True, 51, 0, 0, 0, "", 0, "Pattern Week 48")
    Private _PatternWeek49 As New ColField(Of Decimal)("WPAR49", 1, False, False, 1, 0, True, True, 52, 0, 0, 0, "", 0, "Pattern Week 49")
    Private _PatternWeek50 As New ColField(Of Decimal)("WPAR50", 1, False, False, 1, 0, True, True, 53, 0, 0, 0, "", 0, "Pattern Week 50")
    Private _PatternWeek51 As New ColField(Of Decimal)("WPAR51", 1, False, False, 1, 0, True, True, 54, 0, 0, 0, "", 0, "Pattern Week 51")
    Private _PatternWeek52 As New ColField(Of Decimal)("WPAR52", 1, False, False, 1, 0, True, True, 55, 0, 0, 0, "", 0, "Pattern Week 52")
    Private _PatternWeek53 As New ColField(Of Decimal)("WPAR53", 1, False, False, 1, 0, True, True, 56, 0, 0, 0, "", 0, "Pattern Week 53")

#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Year() As ColField(Of Date)
        Get
            Year = _Year
        End Get
        Set(ByVal value As ColField(Of Date))
            _Year = value
        End Set
    End Property
    Public Property PatternWeek(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            If Index > 53 Then Index -= 53
            If Index < 1 Then Index += 53

            Dim ColName As String = PatternWeek1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = PatternWeek1.ColumnName
            If Index >= 10 Then
                ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString
            Else
                ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            End If
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property PatternWeek1() As ColField(Of Decimal)
        Get

            PatternWeek1 = _PatternWeek1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek1 = value
        End Set
    End Property
    Public Property PatternWeek2() As ColField(Of Decimal)
        Get
            PatternWeek2 = _PatternWeek2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek2 = value
        End Set
    End Property
    Public Property PatternWeek3() As ColField(Of Decimal)
        Get
            PatternWeek3 = _PatternWeek3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek3 = value
        End Set
    End Property
    Public Property PatternWeek4() As ColField(Of Decimal)
        Get
            PatternWeek4 = _PatternWeek4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek4 = value
        End Set
    End Property
    Public Property PatternWeek5() As ColField(Of Decimal)
        Get
            PatternWeek5 = _PatternWeek5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek5 = value
        End Set
    End Property
    Public Property PatternWeek6() As ColField(Of Decimal)
        Get
            PatternWeek6 = _PatternWeek6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek6 = value
        End Set
    End Property
    Public Property PatternWeek7() As ColField(Of Decimal)
        Get
            PatternWeek7 = _PatternWeek7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek7 = value
        End Set
    End Property
    Public Property PatternWeek8() As ColField(Of Decimal)
        Get
            PatternWeek8 = _PatternWeek8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek8 = value
        End Set
    End Property
    Public Property PatternWeek9() As ColField(Of Decimal)
        Get
            PatternWeek9 = _PatternWeek9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek9 = value
        End Set
    End Property
    Public Property PatternWeek10() As ColField(Of Decimal)
        Get
            PatternWeek10 = _PatternWeek10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek10 = value
        End Set
    End Property
    Public Property PatternWeek11() As ColField(Of Decimal)
        Get
            PatternWeek11 = _PatternWeek11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek11 = value
        End Set
    End Property
    Public Property PatternWeek12() As ColField(Of Decimal)
        Get
            PatternWeek12 = _PatternWeek12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek12 = value
        End Set
    End Property
    Public Property PatternWeek13() As ColField(Of Decimal)
        Get
            PatternWeek13 = _PatternWeek13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek13 = value
        End Set
    End Property
    Public Property PatternWeek14() As ColField(Of Decimal)
        Get
            PatternWeek14 = _PatternWeek14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek14 = value
        End Set
    End Property
    Public Property PatternWeek15() As ColField(Of Decimal)
        Get
            PatternWeek15 = _PatternWeek15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek15 = value
        End Set
    End Property
    Public Property PatternWeek16() As ColField(Of Decimal)
        Get
            PatternWeek16 = _PatternWeek16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek16 = value
        End Set
    End Property
    Public Property PatternWeek17() As ColField(Of Decimal)
        Get
            PatternWeek17 = _PatternWeek17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek17 = value
        End Set
    End Property
    Public Property PatternWeek18() As ColField(Of Decimal)
        Get
            PatternWeek18 = _PatternWeek18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek18 = value
        End Set
    End Property
    Public Property PatternWeek19() As ColField(Of Decimal)
        Get
            PatternWeek19 = _PatternWeek19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek19 = value
        End Set
    End Property
    Public Property PatternWeek20() As ColField(Of Decimal)
        Get
            PatternWeek20 = _PatternWeek20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek20 = value
        End Set
    End Property
    Public Property PatternWeek21() As ColField(Of Decimal)
        Get
            PatternWeek21 = _PatternWeek21
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek21 = value
        End Set
    End Property
    Public Property PatternWeek22() As ColField(Of Decimal)
        Get
            PatternWeek22 = _PatternWeek22
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek22 = value
        End Set
    End Property
    Public Property PatternWeek23() As ColField(Of Decimal)
        Get
            PatternWeek23 = _PatternWeek23
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek23 = value
        End Set
    End Property
    Public Property PatternWeek24() As ColField(Of Decimal)
        Get
            PatternWeek24 = _PatternWeek24
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek24 = value
        End Set
    End Property
    Public Property PatternWeek25() As ColField(Of Decimal)
        Get
            PatternWeek25 = _PatternWeek25
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek25 = value
        End Set
    End Property
    Public Property PatternWeek26() As ColField(Of Decimal)
        Get
            PatternWeek26 = _PatternWeek26
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek26 = value
        End Set
    End Property
    Public Property PatternWeek27() As ColField(Of Decimal)
        Get
            PatternWeek27 = _PatternWeek27
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek27 = value
        End Set
    End Property
    Public Property PatternWeek28() As ColField(Of Decimal)
        Get
            PatternWeek28 = _PatternWeek28
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek28 = value
        End Set
    End Property
    Public Property PatternWeek29() As ColField(Of Decimal)
        Get
            PatternWeek29 = _PatternWeek29
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek29 = value
        End Set
    End Property
    Public Property PatternWeek30() As ColField(Of Decimal)
        Get
            PatternWeek30 = _PatternWeek30
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek30 = value
        End Set
    End Property
    Public Property PatternWeek31() As ColField(Of Decimal)
        Get
            PatternWeek31 = _PatternWeek31
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek31 = value
        End Set
    End Property
    Public Property PatternWeek32() As ColField(Of Decimal)
        Get
            PatternWeek32 = _PatternWeek32
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek32 = value
        End Set
    End Property
    Public Property PatternWeek33() As ColField(Of Decimal)
        Get
            PatternWeek33 = _PatternWeek33
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek33 = value
        End Set
    End Property
    Public Property PatternWeek34() As ColField(Of Decimal)
        Get
            PatternWeek34 = _PatternWeek34
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek34 = value
        End Set
    End Property
    Public Property PatternWeek35() As ColField(Of Decimal)
        Get
            PatternWeek35 = _PatternWeek35
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek35 = value
        End Set
    End Property
    Public Property PatternWeek36() As ColField(Of Decimal)
        Get
            PatternWeek36 = _PatternWeek36
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek36 = value
        End Set
    End Property
    Public Property PatternWeek37() As ColField(Of Decimal)
        Get
            PatternWeek37 = _PatternWeek37
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek37 = value
        End Set
    End Property
    Public Property PatternWeek38() As ColField(Of Decimal)
        Get
            PatternWeek38 = _PatternWeek38
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek38 = value
        End Set
    End Property
    Public Property PatternWeek39() As ColField(Of Decimal)
        Get
            PatternWeek39 = _PatternWeek39
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek39 = value
        End Set
    End Property
    Public Property PatternWeek40() As ColField(Of Decimal)
        Get
            PatternWeek40 = _PatternWeek40
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek40 = value
        End Set
    End Property
    Public Property PatternWeek41() As ColField(Of Decimal)
        Get
            PatternWeek41 = _PatternWeek41
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek41 = value
        End Set
    End Property
    Public Property PatternWeek42() As ColField(Of Decimal)
        Get
            PatternWeek42 = _PatternWeek42
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek42 = value
        End Set
    End Property
    Public Property PatternWeek43() As ColField(Of Decimal)
        Get
            PatternWeek43 = _PatternWeek43
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek43 = value
        End Set
    End Property
    Public Property PatternWeek44() As ColField(Of Decimal)
        Get
            PatternWeek44 = _PatternWeek44
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek44 = value
        End Set
    End Property
    Public Property PatternWeek45() As ColField(Of Decimal)
        Get
            PatternWeek45 = _PatternWeek45
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek45 = value
        End Set
    End Property
    Public Property PatternWeek46() As ColField(Of Decimal)
        Get
            PatternWeek46 = _PatternWeek46
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek46 = value
        End Set
    End Property
    Public Property PatternWeek47() As ColField(Of Decimal)
        Get
            PatternWeek47 = _PatternWeek47
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek47 = value
        End Set
    End Property
    Public Property PatternWeek48() As ColField(Of Decimal)
        Get
            PatternWeek48 = _PatternWeek48
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek48 = value
        End Set
    End Property
    Public Property PatternWeek49() As ColField(Of Decimal)
        Get
            PatternWeek49 = _PatternWeek49
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek49 = value
        End Set
    End Property
    Public Property PatternWeek50() As ColField(Of Decimal)
        Get
            PatternWeek50 = _PatternWeek50
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek50 = value
        End Set
    End Property
    Public Property PatternWeek51() As ColField(Of Decimal)
        Get
            PatternWeek51 = _PatternWeek51
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek51 = value
        End Set
    End Property
    Public Property PatternWeek52() As ColField(Of Decimal)
        Get
            PatternWeek52 = _PatternWeek52
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek52 = value
        End Set
    End Property
    Public Property PatternWeek53() As ColField(Of Decimal)
        Get
            PatternWeek53 = _PatternWeek53
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PatternWeek53 = value
        End Set
    End Property


#End Region

#Region "Methods"
    Private _Patterns As List(Of cSOQPattern) = Nothing

    Public Property Pattern(ByVal num As String) As cSOQPattern
        Get
            For Each p As cSOQPattern In Patterns
                If p.Number.Value = num Then Return p
            Next
            Return New cSOQPattern(Oasys3DB)
        End Get
        Set(ByVal value As cSOQPattern)
            If _Patterns IsNot Nothing Then
                For Each p As cSOQPattern In _Patterns
                    If p.Number.Value = num Then
                        p = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Patterns() As List(Of cSOQPattern)
        Get
            If _Patterns Is Nothing Then
                Dim pat As New cSOQPattern(Oasys3DB)
                _Patterns = pat.LoadMatches
            End If
            Return _Patterns
        End Get
        Set(ByVal value As List(Of cSOQPattern))
            _Patterns = value
        End Set
    End Property

    Public Function PeriodAdjuster(ByVal Bank As String, ByVal Promo As String, ByVal Season As String, ByVal week As Integer) As Decimal

        Dim Wi As Double = Pattern(Bank).PatternWeek(week).Value * Pattern(Promo).PatternWeek(week).Value * Pattern(Season).PatternWeek(week).Value
        If Wi = 0 Then Wi = 1
        Return CDec(Wi)

    End Function

#End Region

End Class
