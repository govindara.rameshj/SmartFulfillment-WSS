﻿<Serializable()> Public Class cBarCodes
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "EANMAS"
        BOFields.Add(_Number)
        BOFields.Add(_SkuNumber)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cBarCodes)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cBarCodes)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cBarCodes))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cBarCodes(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("NUMB", "", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Number")
    Private _SkuNumber As New ColField(Of String)("SKUN", "", False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "SKU Number")


#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Return _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property SKUNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns sku number for given bar code or nothing if none found. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="EanNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSkuNumber(ByVal EanNumber As String) As String

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, EanNumber.PadLeft(16, "0"c))
            If LoadMatches.Count > 0 Then Return _SkuNumber.Value
            Return Nothing

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.BarCodesLoad, ex)
        End Try

    End Function

#End Region

End Class

