﻿Public Interface IPriceChange

    Property PriceChanges() As List(Of cPriceChange)
    Property Stock() As cStock

    Sub ApplyPriceChange(ByVal tomorrow As Date, ByVal today As Date, ByVal userID As Integer, ByVal autoChangePriceUpdate As Boolean)
    Sub AssignStocks(ByVal stocks As List(Of cStock))
    Sub Load(ByVal applyDate As Date)
    Sub LoadBORecords(Optional ByVal count As Integer = -1)
    Sub RemoveMultiples()
    Sub Start()

    Function AddRecord(ByVal Sku As String, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean
    Function AddRecord(ByVal stockItem As cStock, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean
    Function AddRecordUpdPriceChange(ByVal stockItem As cStock, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean
    Function DeleteUnapplied() As Boolean
    Function DeleteUnappliedBySku(ByVal Sku As String) As Boolean
    Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPriceChange)
    Function MarkUpRec() As List(Of cPriceChange)
End Interface