﻿Public Interface IRelatedItems

    Function GetBulkSkuNumber_Datatable_HasData(ByRef GetBulkSkuNumberDatatable As DataTable) As Boolean
    Function GetRelatedSkuList_FromBulkSkuNumber(ByRef relItems As cRelatedItems, ByVal BulkSkuNumber As String) As ArrayList
End Interface
