﻿<Serializable()> Public Class cStock
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKMAS"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Description)
        BOFields.Add(_VatRate)
        BOFields.Add(_SupplierUnitCode)
        BOFields.Add(_SupplierNumber)
        BOFields.Add(_SupplierPartCode)
        BOFields.Add(_SupplierPackSize)
        BOFields.Add(_NormalSellPrice)
        BOFields.Add(_PriorSellPrice)
        BOFields.Add(_CostPrice)
        BOFields.Add(_LastSold)
        BOFields.Add(_LastReceived)
        BOFields.Add(_LastOrdered)
        BOFields.Add(_PriceEffectiveDate)
        BOFields.Add(_DateCreated)
        BOFields.Add(_DateObsolete)
        BOFields.Add(_DateDeleted)
        BOFields.Add(_StockOnHand)
        BOFields.Add(_StockOnOrder)
        BOFields.Add(_MinQuantity)
        BOFields.Add(_MaxQuantity)
        BOFields.Add(_ItemStatus)
        BOFields.Add(_RelatedItemSingle)
        BOFields.Add(_RelatedItemsNum)
        BOFields.Add(_CatchAll)
        BOFields.Add(_ItemObsolete)
        BOFields.Add(_ItemDeleted)
        BOFields.Add(_ItemTagged)
        BOFields.Add(_Warranty)
        BOFields.Add(_Ean)
        BOFields.Add(_IsMarkdown)

        BOFields.Add(_ValueSoldYesterday)
        BOFields.Add(_UnitsSoldYesterday)
        BOFields.Add(_ValueSoldThisWeek)
        BOFields.Add(_UnitsSoldThisWeek)
        BOFields.Add(_ValueSoldLastWeek)
        BOFields.Add(_UnitsSoldLastWeek)
        BOFields.Add(_ValueSoldThisPeriod)
        BOFields.Add(_UnitsSoldThisPeriod)
        BOFields.Add(_ValueSoldLastPeriod)
        BOFields.Add(_UnitsSoldLastPeriod)
        BOFields.Add(_ValueSoldThisYear)
        BOFields.Add(_UnitsSoldThisYear)
        BOFields.Add(_ValueSoldLastYear)
        BOFields.Add(_UnitsSoldLastYear)
        BOFields.Add(_DaysOutStockPeriod)
        BOFields.Add(_AverageWeeklySales)
        BOFields.Add(_DateFirstStock)
        BOFields.Add(_NumWeeksUpdated)
        BOFields.Add(_WeeklyUpdate)
        BOFields.Add(_ConfidentWeeklyAve)
        BOFields.Add(_UnitsSoldCurWkPlus1)
        BOFields.Add(_UnitsSoldCurWkPlus2)
        BOFields.Add(_UnitsSoldCurWkPlus3)
        BOFields.Add(_UnitsSoldCurWkPlus4)
        BOFields.Add(_UnitsSoldCurWkPlus5)
        BOFields.Add(_UnitsSoldCurWkPlus6)
        BOFields.Add(_UnitsSoldCurWkPlus7)
        BOFields.Add(_UnitsSoldCurWkPlus8)
        BOFields.Add(_UnitsSoldCurWkPlus9)
        BOFields.Add(_UnitsSoldCurWkPlus10)
        BOFields.Add(_UnitsSoldCurWkPlus11)
        BOFields.Add(_UnitsSoldCurWkPlus12)
        BOFields.Add(_UnitsSoldCurWkPlus13)
        BOFields.Add(_UnitsSoldCurWkPlus14)

        BOFields.Add(_DaysOutStockPeriod1)
        BOFields.Add(_DaysOutStockPeriod2)
        BOFields.Add(_DaysOutStockPeriod3)
        BOFields.Add(_DaysOutStockPeriod4)
        BOFields.Add(_DaysOutStockPeriod5)
        BOFields.Add(_DaysOutStockPeriod6)
        BOFields.Add(_DaysOutStockPeriod7)
        BOFields.Add(_DaysOutStockPeriod8)
        BOFields.Add(_DaysOutStockPeriod9)
        BOFields.Add(_DaysOutStockPeriod10)
        BOFields.Add(_DaysOutStockPeriod11)
        BOFields.Add(_DaysOutStockPeriod12)
        BOFields.Add(_DaysOutStockPeriod13)
        BOFields.Add(_DaysOutStockPeriod14)

        BOFields.Add(_Weight)
        BOFields.Add(_ShortDescription)
        BOFields.Add(_NumAltLocations)
        BOFields.Add(_NumPriceChangeRecs)
        BOFields.Add(_NonStockItem)
        BOFields.Add(_DaysOutStock)
        BOFields.Add(_AveWeeklySales)

        BOFields.Add(_SoqCalcWeeks1)
        BOFields.Add(_SoqCalcWeeks4)
        BOFields.Add(_SoqCalcWeeks13)
        BOFields.Add(_SoqOrderPlaced)

        BOFields.Add(_UnitsReceivedToday)
        BOFields.Add(_ValueReceivedToday)
        BOFields.Add(_ActivityToday)
        BOFields.Add(_UnitsOpenReturns)
        BOFields.Add(_ValueOpenReturns)
        BOFields.Add(_HOCheckDigit)
        BOFields.Add(_NumSmallLabels)

        BOFields.Add(_StandingOrderNumber)
        BOFields.Add(_StandingOrderValue)
        BOFields.Add(_StandingOrderDay1)
        BOFields.Add(_StandingOrderDay2)
        BOFields.Add(_StandingOrderDay3)

        BOFields.Add(_NumMediumLabels)
        BOFields.Add(_NumLargeLabels)
        BOFields.Add(_ItemVolume)
        BOFields.Add(_DoNotOrder)
        BOFields.Add(_AlternateSupplier)
        BOFields.Add(_InitialOrderDate)
        BOFields.Add(_FinalOrderDate)
        BOFields.Add(_PromoMin)
        BOFields.Add(_PromoStart)
        BOFields.Add(_PromoEnd)
        BOFields.Add(_PromoMCP)
        BOFields.Add(_SpacemanDisplayFctr)
        BOFields.Add(_DateMinLastChanged)
        BOFields.Add(_ParticipatingInMCP)
        BOFields.Add(_ItemTagType)
        BOFields.Add(_ProductEquivDescr)
        BOFields.Add(_HieFilter)
        BOFields.Add(_RetailPriceEventNo)
        BOFields.Add(_RetailPricePriority)
        BOFields.Add(_EquivPriceMult)
        BOFields.Add(_EquivPriceUnit)
        BOFields.Add(_MarkDownQuantity)
        BOFields.Add(_WriteOffQuantity)
        BOFields.Add(_AutoApplyPriceChgs)
        BOFields.Add(_AllowAdjustments)
        BOFields.Add(_SavedSupplier)
        BOFields.Add(_WebOrderQuantity)
        BOFields.Add(_BallastItem)
        BOFields.Add(_WEEERate)
        BOFields.Add(_WEEESequence)
        BOFields.Add(_HieCategory)
        BOFields.Add(_HieGroup)
        BOFields.Add(_HieSubGroup)
        BOFields.Add(_HieStyle)
        BOFields.Add(_OffensiveWeaponAge)
        BOFields.Add(_SolventAge)
        BOFields.Add(_QuarantineFlag)
        BOFields.Add(_PricingDiscrepency)
        BOFields.Add(_SaleTypeAttribute)
        BOFields.Add(_PalletedSku)
        BOFields.Add(_ManagedTimberCont)
        BOFields.Add(_InsulationItem)
        BOFields.Add(_ElectricalItem)
        BOFields.Add(_StockHeldOffSale)
        BOFields.Add(_RecyclingSkuNumber)

        BOFields.Add(_DemandPattern)
        BOFields.Add(_PeriodDemand)
        BOFields.Add(_PeriodTrend)
        BOFields.Add(_ErrorForecast)
        BOFields.Add(_ErrorSmoothed)
        BOFields.Add(_ValueAnnualUsage)
        BOFields.Add(_BufferConversion)
        BOFields.Add(_ServiceLevelOverride)
        BOFields.Add(_StockLossPerWeek)
        BOFields.Add(_BufferStock)
        BOFields.Add(_FlierPeriod1)
        BOFields.Add(_FlierPeriod2)
        BOFields.Add(_FlierPeriod3)
        BOFields.Add(_FlierPeriod4)
        BOFields.Add(_FlierPeriod5)
        BOFields.Add(_FlierPeriod6)
        BOFields.Add(_FlierPeriod7)
        BOFields.Add(_FlierPeriod8)
        BOFields.Add(_FlierPeriod9)
        BOFields.Add(_FlierPeriod10)
        BOFields.Add(_FlierPeriod11)
        BOFields.Add(_FlierPeriod12)
        BOFields.Add(_PatternSeason)
        BOFields.Add(_OrderLevel)
        BOFields.Add(_IsInitialised)
        BOFields.Add(_PatternBankHol)
        BOFields.Add(_PatternPromo)
        BOFields.Add(_DateLastForeInit)
        BOFields.Add(_SalesBias1)
        BOFields.Add(_SalesBias2)
        BOFields.Add(_SalesBias3)
        BOFields.Add(_SalesBias4)
        BOFields.Add(_SalesBias5)
        BOFields.Add(_SalesBias6)
        BOFields.Add(_SalesBias0)

        BOFields.Add(_ToForecast)
        BOFields.Add(_DateLastSoldExcludingRefund)
    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStock)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStock)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStock))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStock(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("SKUN") : _SkuNumber.Value = CStr(drRow(dcColumn))
                    Case ("DESCR") : _Description.Value = drRow(dcColumn).ToString.Trim
                    Case ("VATC") : _VatRate.Value = CStr(drRow(dcColumn))
                    Case ("BUYU") : _SupplierUnitCode.Value = CStr(drRow(dcColumn))
                    Case ("SUPP") : _SupplierNumber.Value = CStr(drRow(dcColumn))
                    Case ("PROD") : _SupplierPartCode.Value = CStr(drRow(dcColumn))
                    Case ("PACK") : _SupplierPackSize.Value = CInt(drRow(dcColumn))
                    Case ("PRIC") : _NormalSellPrice.Value = CDec(drRow(dcColumn))
                    Case ("PPRI") : _PriorSellPrice.Value = CDec(drRow(dcColumn))
                    Case ("COST") : _CostPrice.Value = CDec(drRow(dcColumn))
                    Case ("DSOL") : _LastSold.Value = CDate(drRow(dcColumn))
                    Case ("DREC") : _LastReceived.Value = CDate(drRow(dcColumn))
                    Case ("DORD") : _LastOrdered.Value = CDate(drRow(dcColumn))
                    Case ("DPRC") : _PriceEffectiveDate.Value = CDate(drRow(dcColumn))
                    Case ("DSET") : _DateCreated.Value = CDate(drRow(dcColumn))
                    Case ("DOBS") : _DateObsolete.Value = CDate(drRow(dcColumn))
                    Case ("DDEL") : _DateDeleted.Value = CDate(drRow(dcColumn))
                    Case ("ONHA") : _StockOnHand.Value = CInt(drRow(dcColumn))
                    Case ("ONOR") : _StockOnOrder.Value = CInt(drRow(dcColumn))
                    Case ("MINI") : _MinQuantity.Value = CInt(drRow(dcColumn))
                    Case ("MAXI") : _MaxQuantity.Value = CInt(drRow(dcColumn))
                    Case ("ISTA") : _ItemStatus.Value = CStr(drRow(dcColumn))
                    Case ("IRIS") : _RelatedItemSingle.Value = CBool(drRow(dcColumn))
                    Case ("IRIB") : _RelatedItemsNum.Value = CInt(drRow(dcColumn))
                    Case ("ICAT") : _CatchAll.Value = CBool(drRow(dcColumn))
                    Case ("IOBS") : _ItemObsolete.Value = CBool(drRow(dcColumn))
                    Case ("IDEL") : _ItemDeleted.Value = CBool(drRow(dcColumn))
                    Case ("ITAG") : _ItemTagged.Value = CBool(drRow(dcColumn))
                    Case ("IWAR") : _Warranty.Value = CBool(drRow(dcColumn))
                    Case ("IEAN") : _Ean.Value = CInt(drRow(dcColumn))
                    Case ("IMDN") : _IsMarkdown.Value = CBool(drRow(dcColumn))

                    Case ("SALV1") : _ValueSoldYesterday.Value = CDec(drRow(dcColumn))
                    Case ("SALU1") : _UnitsSoldYesterday.Value = CInt(drRow(dcColumn))
                    Case ("SALV2") : _ValueSoldThisWeek.Value = CDec(drRow(dcColumn))
                    Case ("SALU2") : _UnitsSoldThisWeek.Value = CInt(drRow(dcColumn))
                    Case ("SALV3") : _ValueSoldLastWeek.Value = CDec(drRow(dcColumn))
                    Case ("SALU3") : _UnitsSoldLastWeek.Value = CInt(drRow(dcColumn))
                    Case ("SALV4") : _ValueSoldThisPeriod.Value = CDec(drRow(dcColumn))
                    Case ("SALU4") : _UnitsSoldThisPeriod.Value = CInt(drRow(dcColumn))
                    Case ("SALV5") : _ValueSoldLastPeriod.Value = CDec(drRow(dcColumn))
                    Case ("SALU5") : _UnitsSoldLastPeriod.Value = CInt(drRow(dcColumn))
                    Case ("SALV6") : _ValueSoldThisYear.Value = CDec(drRow(dcColumn))
                    Case ("SALU6") : _UnitsSoldThisYear.Value = CInt(drRow(dcColumn))
                    Case ("SALV7") : _ValueSoldLastYear.Value = CDec(drRow(dcColumn))
                    Case ("SALU7") : _UnitsSoldLastYear.Value = CInt(drRow(dcColumn))
                    Case ("CPDO") : _DaysOutStockPeriod.Value = CInt(drRow(dcColumn))
                    Case ("AWSF") : _AverageWeeklySales.Value = CDec(drRow(dcColumn))
                    Case ("DATS") : _DateFirstStock.Value = CDate(drRow(dcColumn))
                    Case ("UWEK") : _NumWeeksUpdated.Value = CInt(drRow(dcColumn))
                    Case ("FLAG") : _WeeklyUpdate.Value = CBool(drRow(dcColumn))
                    Case ("CFLG") : _ConfidentWeeklyAve.Value = CBool(drRow(dcColumn))

                    Case ("US001") : _UnitsSoldCurWkPlus1.Value = CInt(drRow(dcColumn))
                    Case ("US002") : _UnitsSoldCurWkPlus2.Value = CInt(drRow(dcColumn))
                    Case ("US003") : _UnitsSoldCurWkPlus3.Value = CInt(drRow(dcColumn))
                    Case ("US004") : _UnitsSoldCurWkPlus4.Value = CInt(drRow(dcColumn))
                    Case ("US005") : _UnitsSoldCurWkPlus5.Value = CInt(drRow(dcColumn))
                    Case ("US006") : _UnitsSoldCurWkPlus6.Value = CInt(drRow(dcColumn))
                    Case ("US007") : _UnitsSoldCurWkPlus7.Value = CInt(drRow(dcColumn))
                    Case ("US008") : _UnitsSoldCurWkPlus8.Value = CInt(drRow(dcColumn))
                    Case ("US009") : _UnitsSoldCurWkPlus9.Value = CInt(drRow(dcColumn))
                    Case ("US0010") : _UnitsSoldCurWkPlus10.Value = CInt(drRow(dcColumn))
                    Case ("US0011") : _UnitsSoldCurWkPlus11.Value = CInt(drRow(dcColumn))
                    Case ("US0012") : _UnitsSoldCurWkPlus12.Value = CInt(drRow(dcColumn))
                    Case ("US0013") : _UnitsSoldCurWkPlus13.Value = CInt(drRow(dcColumn))
                    Case ("US0014") : _UnitsSoldCurWkPlus14.Value = CInt(drRow(dcColumn))

                    Case ("DO001") : _DaysOutStockPeriod1.Value = CInt(drRow(dcColumn))
                    Case ("DO002") : _DaysOutStockPeriod2.Value = CInt(drRow(dcColumn))
                    Case ("DO003") : _DaysOutStockPeriod3.Value = CInt(drRow(dcColumn))
                    Case ("DO004") : _DaysOutStockPeriod4.Value = CInt(drRow(dcColumn))
                    Case ("DO005") : _DaysOutStockPeriod5.Value = CInt(drRow(dcColumn))
                    Case ("DO006") : _DaysOutStockPeriod6.Value = CInt(drRow(dcColumn))
                    Case ("DO007") : _DaysOutStockPeriod7.Value = CInt(drRow(dcColumn))
                    Case ("DO008") : _DaysOutStockPeriod8.Value = CInt(drRow(dcColumn))
                    Case ("DO009") : _DaysOutStockPeriod9.Value = CInt(drRow(dcColumn))
                    Case ("DO0010") : _DaysOutStockPeriod10.Value = CInt(drRow(dcColumn))
                    Case ("DO0011") : _DaysOutStockPeriod11.Value = CInt(drRow(dcColumn))
                    Case ("DO0012") : _DaysOutStockPeriod12.Value = CInt(drRow(dcColumn))
                    Case ("DO0013") : _DaysOutStockPeriod13.Value = CInt(drRow(dcColumn))
                    Case ("DO0014") : _DaysOutStockPeriod14.Value = CInt(drRow(dcColumn))

                    Case ("WGHT") : _Weight.Value = CDec(drRow(dcColumn))
                    Case ("PLUD") : _ShortDescription.Value = CStr(drRow(dcColumn))
                    Case ("ILOC") : _NumAltLocations.Value = CInt(drRow(dcColumn))
                    Case ("IPPC") : _NumPriceChangeRecs.Value = CInt(drRow(dcColumn))
                    Case ("INON") : _NonStockItem.Value = CBool(drRow(dcColumn))
                    Case ("CYDO") : _DaysOutStock.Value = CInt(drRow(dcColumn))
                    Case ("AW13") : _AveWeeklySales.Value = CDec(drRow(dcColumn))

                    Case ("SQ01") : _SoqCalcWeeks1.Value = CInt(drRow(dcColumn))
                    Case ("SQ04") : _SoqCalcWeeks4.Value = CInt(drRow(dcColumn))
                    Case ("SQ13") : _SoqCalcWeeks13.Value = CInt(drRow(dcColumn))
                    Case ("SQOR") : _SoqOrderPlaced.Value = CBool(drRow(dcColumn))

                    Case ("TREQ") : _UnitsReceivedToday.Value = CInt(drRow(dcColumn))
                    Case ("TREV") : _ValueReceivedToday.Value = CDec(drRow(dcColumn))
                    Case ("TACT") : _ActivityToday.Value = CBool(drRow(dcColumn))
                    Case ("RETQ") : _UnitsOpenReturns.Value = CInt(drRow(dcColumn))
                    Case ("RETV") : _ValueOpenReturns.Value = CDec(drRow(dcColumn))
                    Case ("CHKD") : _HOCheckDigit.Value = CStr(drRow(dcColumn))
                    Case ("LABN") : _NumSmallLabels.Value = CInt(drRow(dcColumn))

                    Case ("STON") : _StandingOrderNumber.Value = CStr(drRow(dcColumn))
                    Case ("STOQ") : _StandingOrderValue.Value = CInt(drRow(dcColumn))
                    Case ("SOD1") : _StandingOrderDay1.Value = CStr(drRow(dcColumn))
                    Case ("SOD2") : _StandingOrderDay2.Value = CStr(drRow(dcColumn))
                    Case ("SOD3") : _StandingOrderDay3.Value = CStr(drRow(dcColumn))

                    Case ("LABM") : _NumMediumLabels.Value = CInt(drRow(dcColumn))
                    Case ("LABL") : _NumLargeLabels.Value = CInt(drRow(dcColumn))
                    Case ("VOLU") : _ItemVolume.Value = CDec(drRow(dcColumn))
                    Case ("NOOR") : _DoNotOrder.Value = CBool(drRow(dcColumn))
                    Case ("SUP1") : _AlternateSupplier.Value = CStr(drRow(dcColumn))
                    Case ("IODT") : _InitialOrderDate.Value = CDate(drRow(dcColumn))
                    Case ("FODT") : _FinalOrderDate.Value = CDate(drRow(dcColumn))
                    Case ("PMIN") : _PromoMin.Value = CInt(drRow(dcColumn))
                    Case ("PMSD") : _PromoStart.Value = CDate(drRow(dcColumn))
                    Case ("PMED") : _PromoEnd.Value = CDate(drRow(dcColumn))
                    Case ("PMCP") : _PromoMCP.Value = CStr(drRow(dcColumn))
                    Case ("SMAN") : _SpacemanDisplayFctr.Value = CInt(drRow(dcColumn))
                    Case ("DFLC") : _DateMinLastChanged.Value = CDate(drRow(dcColumn))
                    Case ("IMCP") : _ParticipatingInMCP.Value = CBool(drRow(dcColumn))
                    Case ("TAGF") : _ItemTagType.Value = CStr(drRow(dcColumn))
                    Case ("EQUV") : _ProductEquivDescr.Value = CStr(drRow(dcColumn))
                    Case ("HFIL") : _HieFilter.Value = CStr(drRow(dcColumn))
                    Case ("REVT") : _RetailPriceEventNo.Value = CStr(drRow(dcColumn))
                    Case ("RPRI") : _RetailPricePriority.Value = CStr(drRow(dcColumn))
                    Case ("EQPM") : _EquivPriceMult.Value = CDec(drRow(dcColumn))
                    Case ("EQPU") : _EquivPriceUnit.Value = CStr(drRow(dcColumn))
                    Case ("MDNQ") : _MarkDownQuantity.Value = CInt(drRow(dcColumn))
                    Case ("WTFQ") : _WriteOffQuantity.Value = CInt(drRow(dcColumn))
                    Case ("AAPC") : _AutoApplyPriceChgs.Value = CBool(drRow(dcColumn))
                    Case ("AADJ") : _AllowAdjustments.Value = CStr(drRow(dcColumn))
                    Case ("SUP2") : _SavedSupplier.Value = CStr(drRow(dcColumn))
                    Case ("WQTY") : _WebOrderQuantity.Value = CInt(drRow(dcColumn))
                    Case ("BALI") : _BallastItem.Value = CStr(drRow(dcColumn))
                    Case ("WRAT") : _WEEERate.Value = CStr(drRow(dcColumn))
                    Case ("WSEQ") : _WEEESequence.Value = CStr(drRow(dcColumn))
                    Case ("CTGY") : _HieCategory.Value = CStr(drRow(dcColumn))
                    Case ("GRUP") : _HieGroup.Value = CStr(drRow(dcColumn))
                    Case ("SGRP") : _HieSubGroup.Value = CStr(drRow(dcColumn))
                    Case ("STYL") : _HieStyle.Value = CStr(drRow(dcColumn))
                    Case ("IOFF") : _OffensiveWeaponAge.Value = CInt(drRow(dcColumn))
                    Case ("ISOL") : _SolventAge.Value = CInt(drRow(dcColumn))
                    Case ("QUAR") : _QuarantineFlag.Value = CStr(drRow(dcColumn))
                    Case ("IPRD") : _PricingDiscrepency.Value = CBool(drRow(dcColumn))
                    Case ("SALT") : _SaleTypeAttribute.Value = CStr(drRow(dcColumn))
                    Case ("IPSK") : _PalletedSku.Value = CBool(drRow(dcColumn))
                    Case ("TIMB") : _ManagedTimberCont.Value = CInt(drRow(dcColumn))
                    Case ("FRAG") : _InsulationItem.Value = CBool(drRow(dcColumn))
                    Case ("ELEC") : _ElectricalItem.Value = CBool(drRow(dcColumn))
                    Case ("SOFS") : _StockHeldOffSale.Value = CInt(drRow(dcColumn))
                    Case ("PRFSKU") : _RecyclingSkuNumber.Value = CStr(drRow(dcColumn))

                    Case ("DemandPattern") : _DemandPattern.Value = CStr(drRow(dcColumn))
                    Case ("IsInitialised") : _IsInitialised.Value = CBool(drRow(dcColumn))
                    Case ("PeriodDemand") : _PeriodDemand.Value = CDec(drRow(dcColumn))
                    Case ("PeriodTrend") : _PeriodTrend.Value = CDec(drRow(dcColumn))
                    Case ("ErrorForecast") : _ErrorForecast.Value = CDec(drRow(dcColumn))
                    Case ("ErrorSmoothed") : _ErrorSmoothed.Value = CDec(drRow(dcColumn))
                    Case ("ValueAnnualUsage") : _ValueAnnualUsage.Value = CStr(drRow(dcColumn))
                    Case ("BufferConversion") : _BufferConversion.Value = CDec(drRow(dcColumn))
                    Case ("BufferStock") : _BufferStock.Value = CInt(drRow(dcColumn))
                    Case ("ServiceLevelOverride") : _ServiceLevelOverride.Value = CDec(drRow(dcColumn))
                    Case ("StockLossPerWeek") : _StockLossPerWeek.Value = CDec(drRow(dcColumn))
                    Case ("OrderLevel") : _OrderLevel.Value = CInt(drRow(dcColumn))
                    Case ("FlierPeriod1") : _FlierPeriod1.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod2") : _FlierPeriod2.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod3") : _FlierPeriod3.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod4") : _FlierPeriod4.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod5") : _FlierPeriod5.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod6") : _FlierPeriod6.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod7") : _FlierPeriod7.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod8") : _FlierPeriod8.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod9") : _FlierPeriod9.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod10") : _FlierPeriod10.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod11") : _FlierPeriod11.Value = drRow(dcColumn).ToString.Trim
                    Case ("FlierPeriod12") : _FlierPeriod12.Value = drRow(dcColumn).ToString.Trim
                    Case ("SaleWeightSeason") : _PatternSeason.Value = drRow(dcColumn).ToString.PadLeft(4, "0"c)
                    Case ("SaleWeightBank") : _PatternBankHol.Value = drRow(dcColumn).ToString.PadLeft(4, "0"c)
                    Case ("SaleWeightPromo") : _PatternPromo.Value = drRow(dcColumn).ToString.PadLeft(4, "0"c)
                    Case ("DateLastSoqInit") : _DateLastForeInit.Value = CDate(drRow(dcColumn))
                    Case ("SalesBias1") : _SalesBias1.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias2") : _SalesBias2.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias3") : _SalesBias3.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias4") : _SalesBias4.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias5") : _SalesBias5.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias6") : _SalesBias6.Value = CDec(drRow(dcColumn))
                    Case ("SalesBias0") : _SalesBias0.Value = CDec(drRow(dcColumn))
                    Case ("ToForecast") : _ToForecast.Value = CBool(drRow(dcColumn))
                    Case ("DateLastSold") : _DateLastSoldExcludingRefund.Value = CDate(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "Sku Number", True, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _VatRate As New ColField(Of String)("VATC", "", "VAT Rate", False, False)
    Private _SupplierUnitCode As New ColField(Of String)("BUYU", "", "Supplier Unit Code", False, False)
    Private _SupplierNumber As New ColField(Of String)("SUPP", "00000", "Supplier Number", False, False)
    Private _SupplierPartCode As New ColField(Of String)("PROD", "", "Supplier Part Code", False, False)
    Private _SupplierPackSize As New ColField(Of Integer)("PACK", 0, "Supplier Pack Size", False, False)
    Private _NormalSellPrice As New ColField(Of Decimal)("PRIC", 0, "Normal Sell Price", False, False, 2)
    Private _PriorSellPrice As New ColField(Of Decimal)("PPRI", 0, "Prior Sell Price", False, False, 2)
    Private _CostPrice As New ColField(Of Decimal)("COST", 0, "Cost Price", False, False, 2)

    Private _LastSold As New ColField(Of Date)("DSOL", Nothing, "Last Sold", False, False)
    Private _LastReceived As New ColField(Of Date)("DREC", Nothing, "Last Received", False, False)
    Private _LastOrdered As New ColField(Of Date)("DORD", Nothing, "Last Ordered", False, False)
    Private _PriceEffectiveDate As New ColField(Of Date)("DPRC", Nothing, "Price Effective Date", False, False)
    Private _DateCreated As New ColField(Of Date)("DSET", Nothing, "Date Created", False, False)
    Private _DateObsolete As New ColField(Of Date)("DOBS", Nothing, "Date Obselete", False, False)
    Private _DateDeleted As New ColField(Of Date)("DDEL", Nothing, "Date Deleted", False, False)

    Private _StockOnHand As New ColField(Of Integer)("ONHA", 0, "Stock On Hand", False, False)
    Private _StockOnOrder As New ColField(Of Integer)("ONOR", 0, "Stock On Order", False, False)
    Private _MinQuantity As New ColField(Of Integer)("MINI", 0, "Min Quantity", False, False)
    Private _MaxQuantity As New ColField(Of Integer)("MAXI", 0, "Max Quantity", False, False)
    Private _ItemStatus As New ColField(Of String)("ISTA", "", "Item Status", False, False)
    Private _RelatedItemSingle As New ColField(Of Boolean)("IRIS", False, "Related Item Single", False, False)
    Private _RelatedItemsNum As New ColField(Of Integer)("IRIB", 0, "Related No Items", False, False)
    Private _CatchAll As New ColField(Of Boolean)("ICAT", False, "Catch All", False, False)
    Private _ItemObsolete As New ColField(Of Boolean)("IOBS", False, "Item Obselete", False, False)
    Private _ItemDeleted As New ColField(Of Boolean)("IDEL", False, "Item Deleted", False, False)
    Private _ItemTagged As New ColField(Of Boolean)("ITAG", False, "Item Tagged", False, False)
    Private _Warranty As New ColField(Of Boolean)("IWAR", False, "Warranty", False, False)
    Private _Ean As New ColField(Of Integer)("IEAN", 0, "EAN", False, False)
    Private _NumAltLocations As New ColField(Of Integer)("ILOC", 0, "No Of Alt Locations", False, False)
    Private _NumPriceChangeRecs As New ColField(Of Integer)("IPPC", 0, "No Of Price Change Recs", False, False)
    Private _NonStockItem As New ColField(Of Boolean)("INON", False, "Non Stock Item", False, False)
    Private _DoNotOrder As New ColField(Of Boolean)("NOOR", False, "Non-Orderable", False, False)
    Private _IsMarkdown As New ColField(Of Boolean)("IMDN", False, "Is Markdown", False, False)

    Private _ValueSoldYesterday As New ColField(Of Decimal)("SALV1", 0, "Value Sold Yesterday", False, False, 2)
    Private _UnitsSoldYesterday As New ColField(Of Integer)("SALU1", 0, "Units Sold Yesterday", False, False)
    Private _ValueSoldThisWeek As New ColField(Of Decimal)("SALV2", 0, "Value Sold This Week", False, False, 2)
    Private _UnitsSoldThisWeek As New ColField(Of Integer)("SALU2", 0, "Units Sold This Week", False, False)
    Private _ValueSoldLastWeek As New ColField(Of Decimal)("SALV3", 0, "Value Sold Last Week", False, False, 2)
    Private _UnitsSoldLastWeek As New ColField(Of Integer)("SALU3", 0, "Units Sold Last Week", False, False)
    Private _ValueSoldThisPeriod As New ColField(Of Decimal)("SALV4", 0, "Value Sold This Period", False, False, 2)
    Private _UnitsSoldThisPeriod As New ColField(Of Integer)("SALU4", 0, "Units Sold This Period", False, False)
    Private _ValueSoldLastPeriod As New ColField(Of Decimal)("SALV5", 0, "Value Sold Last Period", False, False, 2)
    Private _UnitsSoldLastPeriod As New ColField(Of Integer)("SALU5", 0, "Units Sold Last Period", False, False)
    Private _ValueSoldThisYear As New ColField(Of Decimal)("SALV6", 0, "Value Sold This Year", False, False, 2)
    Private _UnitsSoldThisYear As New ColField(Of Integer)("SALU6", 0, "Units Sold This Year", False, False)
    Private _ValueSoldLastYear As New ColField(Of Decimal)("SALV7", 0, "Value Sold Last Year", False, False, 2)
    Private _UnitsSoldLastYear As New ColField(Of Integer)("SALU7", 0, "Units Sold Last Year", False, False)

    Private _UnitsSoldCurWkPlus1 As New ColField(Of Integer)("US001", 0, "Units Sold Cur Week Plus 1", False, False)
    Private _UnitsSoldCurWkPlus2 As New ColField(Of Integer)("US002", 0, "Units Sold Cur Week Plus 2", False, False)
    Private _UnitsSoldCurWkPlus3 As New ColField(Of Integer)("US003", 0, "Units Sold Cur Week Plus 3", False, False)
    Private _UnitsSoldCurWkPlus4 As New ColField(Of Integer)("US004", 0, "Units Sold Cur Week Plus 4", False, False)
    Private _UnitsSoldCurWkPlus5 As New ColField(Of Integer)("US005", 0, "Units Sold Cur Week Plus 5", False, False)
    Private _UnitsSoldCurWkPlus6 As New ColField(Of Integer)("US006", 0, "Units Sold Cur Week Plus 6", False, False)
    Private _UnitsSoldCurWkPlus7 As New ColField(Of Integer)("US007", 0, "Units Sold Cur Week Plus 7", False, False)
    Private _UnitsSoldCurWkPlus8 As New ColField(Of Integer)("US008", 0, "Units Sold Cur Week Plus 8", False, False)
    Private _UnitsSoldCurWkPlus9 As New ColField(Of Integer)("US009", 0, "Units Sold Cur Week Plus 9", False, False)
    Private _UnitsSoldCurWkPlus10 As New ColField(Of Integer)("US0010", 0, "Units Sold Cur Week Plus 10", False, False)
    Private _UnitsSoldCurWkPlus11 As New ColField(Of Integer)("US0011", 0, "Units Sold Cur Week Plus 11", False, False)
    Private _UnitsSoldCurWkPlus12 As New ColField(Of Integer)("US0012", 0, "Units Sold Cur Week Plus 12", False, False)
    Private _UnitsSoldCurWkPlus13 As New ColField(Of Integer)("US0013", 0, "Units Sold Cur Week Plus 13", False, False)
    Private _UnitsSoldCurWkPlus14 As New ColField(Of Integer)("US0014", 0, "Units Sold Cur Week Plus 14", False, False)

    Private _DaysOutStockPeriod1 As New ColField(Of Integer)("DO001", 0, "Days Out Stock Period 1", False, False)
    Private _DaysOutStockPeriod2 As New ColField(Of Integer)("DO002", 0, "Days Out Stock Period 2", False, False)
    Private _DaysOutStockPeriod3 As New ColField(Of Integer)("DO003", 0, "Days Out Stock Period 3", False, False)
    Private _DaysOutStockPeriod4 As New ColField(Of Integer)("DO004", 0, "Days Out Stock Period 4", False, False)
    Private _DaysOutStockPeriod5 As New ColField(Of Integer)("DO005", 0, "Days Out Stock Period 5", False, False)
    Private _DaysOutStockPeriod6 As New ColField(Of Integer)("DO006", 0, "Days Out Stock Period 6", False, False)
    Private _DaysOutStockPeriod7 As New ColField(Of Integer)("DO007", 0, "Days Out Stock Period 7", False, False)
    Private _DaysOutStockPeriod8 As New ColField(Of Integer)("DO008", 0, "Days Out Stock Period 8", False, False)
    Private _DaysOutStockPeriod9 As New ColField(Of Integer)("DO009", 0, "Days Out Stock Period 9", False, False)
    Private _DaysOutStockPeriod10 As New ColField(Of Integer)("DO0010", 0, "Days Out Stock Period 10", False, False)
    Private _DaysOutStockPeriod11 As New ColField(Of Integer)("DO0011", 0, "Days Out Stock Period 11", False, False)
    Private _DaysOutStockPeriod12 As New ColField(Of Integer)("DO0012", 0, "Days Out Stock Period 12", False, False)
    Private _DaysOutStockPeriod13 As New ColField(Of Integer)("DO0013", 0, "Days Out Stock Period 13", False, False)
    Private _DaysOutStockPeriod14 As New ColField(Of Integer)("DO0014", 0, "Days Out Stock Period 14", False, False)

    Private _DaysOutStockPeriod As New ColField(Of Integer)("CPDO", 0, "Days Out Stock Period", False, False)
    Private _AverageWeeklySales As New ColField(Of Decimal)("AWSF", 0, "Average Weekly Sales", False, False, 2)
    Private _DateFirstStock As New ColField(Of Date)("DATS", Nothing, "Date First Stock", False, False)
    Private _NumWeeksUpdated As New ColField(Of Integer)("UWEK", 0, "No Weeks Updated", False, False)
    Private _WeeklyUpdate As New ColField(Of Boolean)("FLAG", False, "Weekly Update", False, False)
    Private _ConfidentWeeklyAve As New ColField(Of Boolean)("CFLG", False, "Confident Weekly Average", False, False)
    Private _Weight As New ColField(Of Decimal)("WGHT", 0, "Weight", False, False, 2)
    Private _ShortDescription As New ColField(Of String)("PLUD", "", "Short Description", False, False)
    Private _DaysOutStock As New ColField(Of Integer)("CYDO", 0, "Days Out Of Stock", False, False)
    Private _AveWeeklySales As New ColField(Of Decimal)("AW13", 0, "Average Weekly Sales", False, False, 2)

    Private _SoqCalcWeeks1 As New ColField(Of Integer)("SQ01", 0, "SOQ Calc Weeks 1", False, False)
    Private _SoqCalcWeeks4 As New ColField(Of Integer)("SQ04", 0, "SOQ Calc Weeks 4", False, False)
    Private _SoqCalcWeeks13 As New ColField(Of Integer)("SQ13", 0, "SOQ Calc Weeks 13", False, False)
    Private _SoqOrderPlaced As New ColField(Of Boolean)("SQOR", False, "SOQ Order Placed", False, False)

    Private _UnitsReceivedToday As New ColField(Of Integer)("TREQ", 0, "Units Received Today", False, False)
    Private _UnitsOpenReturns As New ColField(Of Integer)("RETQ", 0, "Units In Open Return", False, False)
    Private _ValueReceivedToday As New ColField(Of Decimal)("TREV", 0, "Value Received Today", False, False, 2)
    Private _ValueOpenReturns As New ColField(Of Decimal)("RETV", 0, "Value In Open Returns", False, False, 2)
    Private _ActivityToday As New ColField(Of Boolean)("TACT", False, "Activity Today", False, False)

    Private _HOCheckDigit As New ColField(Of String)("CHKD", "", "HO Check Digit", False, False)
    Private _NumSmallLabels As New ColField(Of Integer)("LABN", 0, "No Of Peg Labels", False, False)
    Private _NumMediumLabels As New ColField(Of Integer)("LABM", 0, "No Of Small Labels", False, False)
    Private _NumLargeLabels As New ColField(Of Integer)("LABL", 0, "No Of Medium Labels", False, False)

    Private _StandingOrderNumber As New ColField(Of String)("STON", "", "Standing Order Number", False, False)
    Private _StandingOrderValue As New ColField(Of Integer)("STOQ", 0, "Standing Order Value", False, False)
    Private _StandingOrderDay1 As New ColField(Of String)("SOD1", "", "Standing Order Day 1", False, False)
    Private _StandingOrderDay2 As New ColField(Of String)("SOD2", "", "Standing Order Day 2", False, False)
    Private _StandingOrderDay3 As New ColField(Of String)("SOD3", "", "Standing Order Day 3", False, False)

    Private _ItemVolume As New ColField(Of Decimal)("VOLU", 0, "Item Volume", False, False, 2)
    Private _AlternateSupplier As New ColField(Of String)("SUP1", "", "Alternate Supplier", False, False)
    Private _InitialOrderDate As New ColField(Of Date)("IODT", Nothing, "Initial Order Date", False, False)
    Private _FinalOrderDate As New ColField(Of Date)("FODT", Nothing, "Final Order Date", False, False)

    Private _PromoMin As New ColField(Of Integer)("PMIN", 0, "Promotional Minimum", False, False)
    Private _PromoStart As New ColField(Of Date)("PMSD", Nothing, "Promotional Min Start", False, False)
    Private _PromoEnd As New ColField(Of Date)("PMED", Nothing, "Promotional Min End", False, False)
    Private _PromoMCP As New ColField(Of String)("PMCP", "", "Promotional Mini MCP", False, False)

    Private _SpacemanDisplayFctr As New ColField(Of Integer)("SMAN", 0, "Spaceman Display Factor", False, False)
    Private _DateMinLastChanged As New ColField(Of Date)("DFLC", Nothing, "Date Min Last Changed", False, False)
    Private _ParticipatingInMCP As New ColField(Of Boolean)("IMCP", False, "Participating In MCP", False, False)
    Private _ItemTagType As New ColField(Of String)("TAGF", "", "Item Tag Type", False, False)
    Private _ProductEquivDescr As New ColField(Of String)("EQUV", "", "Product Equivalent Description", False, False)
    Private _RetailPriceEventNo As New ColField(Of String)("REVT", "", "Retail Price Event No", False, False)
    Private _RetailPricePriority As New ColField(Of String)("RPRI", "", "Retail Price Priority", False, False)
    Private _EquivPriceMult As New ColField(Of Decimal)("EQPM", 0, "Equivalent Price Mult", False, False, 6)
    Private _EquivPriceUnit As New ColField(Of String)("EQPU", "", "Equivalent Price Unit", False, False)

    Private _MarkDownQuantity As New ColField(Of Integer)("MDNQ", 0, "Mark Down Quantity", False, False)
    Private _WriteOffQuantity As New ColField(Of Integer)("WTFQ", 0, "Write Off Quantity", False, False)
    Private _AutoApplyPriceChgs As New ColField(Of Boolean)("AAPC", False, "Auto Apply Price Chgs", False, False)
    Private _AllowAdjustments As New ColField(Of String)("AADJ", "", "Allow Adjustments", False, False)
    Private _SavedSupplier As New ColField(Of String)("SUP2", "", "Saved Supplier", False, False)
    Private _WebOrderQuantity As New ColField(Of Integer)("WQTY", 0, "Web Order Quantity", False, False)
    Private _BallastItem As New ColField(Of String)("BALI", "", "Ballast Item", False, False)
    Private _WEEERate As New ColField(Of String)("WRAT", "", "WEEE Rate", False, False)
    Private _WEEESequence As New ColField(Of String)("WSEQ", "", "WEEE Sequence", False, False)
    Private _RecyclingSkuNumber As New ColField(Of String)("PRFSKU", "000000", "Recycling SKU", False, False)

    Private _HieFilter As New ColField(Of String)("HFIL", "", "Hierarchy Filter", False, False)
    Private _HieCategory As New ColField(Of String)("CTGY", "000000", "Hierarchy Category", False, False)
    Private _HieGroup As New ColField(Of String)("GRUP", "000000", "Hierarchy Group", False, False)
    Private _HieSubGroup As New ColField(Of String)("SGRP", "000000", "Hierarchy Sub Group", False, False)
    Private _HieStyle As New ColField(Of String)("STYL", "000000", "Hierarchy Style", False, False)

    Private _OffensiveWeaponAge As New ColField(Of Integer)("IOFF", 0, "Offensive Weapon Age", False, False)
    Private _SolventAge As New ColField(Of Integer)("ISOL", 0, "Solvent Age", False, False)
    Private _QuarantineFlag As New ColField(Of String)("QUAR", "", "Quarantine Flag", False, False)
    Private _PricingDiscrepency As New ColField(Of Boolean)("IPRD", False, "Pricing Discrepency", False, False)
    Private _SaleTypeAttribute As New ColField(Of String)("SALT", "", "Sale Type Attribute", False, False)
    Private _PalletedSku As New ColField(Of Boolean)("IPSK", False, "Palleted Sku", False, False)
    Private _ManagedTimberCont As New ColField(Of Integer)("TIMB", 0, "Managed Timber Cont", False, False)
    Private _InsulationItem As New ColField(Of Boolean)("FRAG", False, "Insulation Item", False, False)
    Private _ElectricalItem As New ColField(Of Boolean)("ELEC", False, "Electrical Item", False, False)
    Private _StockHeldOffSale As New ColField(Of Integer)("SOFS", 0, "Stock Held Off Sale", False, False)

    Private _IsInitialised As New ColField(Of Boolean)("IsInitialised", False, "Is Initialised", False, False)
    Private _DemandPattern As New ColField(Of String)("DemandPattern", "", "Demand Pattern", False, False)
    Private _PeriodDemand As New ColField(Of Decimal)("PeriodDemand", 0, "Period Demand", False, False, 2)
    Private _PeriodTrend As New ColField(Of Decimal)("PeriodTrend", 0, "Period Trend", False, False, 2)
    Private _ErrorForecast As New ColField(Of Decimal)("ErrorForecast", 0, "Error Forecast", False, False, 2)
    Private _ErrorSmoothed As New ColField(Of Decimal)("ErrorSmoothed", 0, "Error Smoothed", False, False, 2)
    Private _ValueAnnualUsage As New ColField(Of String)("ValueAnnualUsage", "", "Value Annual Usage", False, False)
    Private _BufferConversion As New ColField(Of Decimal)("BufferConversion", 0, "Buffer Conversion", False, False, 2)
    Private _BufferStock As New ColField(Of Integer)("BufferStock", 0, "Buffer Stock", False, False)
    Private _ServiceLevelOverride As New ColField(Of Decimal)("ServiceLevelOverride", 0, "Service Level Overide", False, False, 2)
    Private _StockLossPerWeek As New ColField(Of Decimal)("StockLossPerWeek", 0, "Stock Loss Per Week", False, False, 4)
    Private _OrderLevel As New ColField(Of Integer)("OrderLevel", 0, "Order Level", False, False)
    Private _FlierPeriod1 As New ColField(Of String)("FlierPeriod1", "", "Flier Period 1", False, False)
    Private _FlierPeriod2 As New ColField(Of String)("FlierPeriod2", "", "Flier Period 2", False, False)
    Private _FlierPeriod3 As New ColField(Of String)("FlierPeriod3", "", "Flier Period 3", False, False)
    Private _FlierPeriod4 As New ColField(Of String)("FlierPeriod4", "", "Flier Period 4", False, False)
    Private _FlierPeriod5 As New ColField(Of String)("FlierPeriod5", "", "Flier Period 5", False, False)
    Private _FlierPeriod6 As New ColField(Of String)("FlierPeriod6", "", "Flier Period 6", False, False)
    Private _FlierPeriod7 As New ColField(Of String)("FlierPeriod7", "", "Flier Period 7", False, False)
    Private _FlierPeriod8 As New ColField(Of String)("FlierPeriod8", "", "Flier Period 8", False, False)
    Private _FlierPeriod9 As New ColField(Of String)("FlierPeriod9", "", "Flier Period 9", False, False)
    Private _FlierPeriod10 As New ColField(Of String)("FlierPeriod10", "", "Flier Period 10", False, False)
    Private _FlierPeriod11 As New ColField(Of String)("FlierPeriod11", "", "Flier Period 11", False, False)
    Private _FlierPeriod12 As New ColField(Of String)("FlierPeriod12", "", "Flier Period 12", False, False)
    Private _PatternSeason As New ColField(Of String)("SaleWeightSeason", "0", "Pattern Seasonal", False, False)
    Private _PatternBankHol As New ColField(Of String)("SaleWeightBank", "0", "Pattern Bank Holidays", False, False)
    Private _PatternPromo As New ColField(Of String)("SaleWeightPromo", "0", "Pattern Promotional", False, False)
    Private _SalesBias0 As New ColField(Of Decimal)("SalesBias0", 0, "Sales Bias Sunday", False, False, 3)
    Private _SalesBias1 As New ColField(Of Decimal)("SalesBias1", 0, "Sales Bias Monday", False, False, 3)
    Private _SalesBias2 As New ColField(Of Decimal)("SalesBias2", 0, "Sales Bias Tuesday", False, False, 3)
    Private _SalesBias3 As New ColField(Of Decimal)("SalesBias3", 0, "Sales Bias Wednesday", False, False, 3)
    Private _SalesBias4 As New ColField(Of Decimal)("SalesBias4", 0, "Sales Bias Thursday", False, False, 3)
    Private _SalesBias5 As New ColField(Of Decimal)("SalesBias5", 0, "Sales Bias Friday", False, False, 3)
    Private _SalesBias6 As New ColField(Of Decimal)("SalesBias6", 0, "Sales Bias Saturday", False, False, 3)
    Private _DateLastForeInit As New ColField(Of Date)("DateLastSoqInit", Nothing, "Date Last Forecast/Initialisation", False, False)
    Private _ToForecast As New ColField(Of Boolean)("ToForecast", False, "To Forecast", False, False)
    Private _DateLastSoldExcludingRefund As New ColField(Of Date)("DateLastSold", Nothing, "Last SKU sale date excluding refunds", False, False)
#End Region

#Region "Field Properties"

    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property VATRate() As ColField(Of String)
        Get
            VATRate = _VatRate
        End Get
        Set(ByVal value As ColField(Of String))
            _VatRate = value
        End Set
    End Property
    Public Property SupplierUnitCode() As ColField(Of String)
        Get
            SupplierUnitCode = _SupplierUnitCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierUnitCode = value
        End Set
    End Property
    Public Property SupplierNo() As ColField(Of String)
        Get
            SupplierNo = _SupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierNumber = value
        End Set
    End Property
    Public Property SupplierPartCode() As ColField(Of String)
        Get
            SupplierPartCode = _SupplierPartCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierPartCode = value
        End Set
    End Property
    Public Property SupplierPackSize() As ColField(Of Integer)
        Get
            SupplierPackSize = _SupplierPackSize
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SupplierPackSize = value
        End Set
    End Property
    Public Property NormalSellPrice() As ColField(Of Decimal)
        Get
            NormalSellPrice = _NormalSellPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NormalSellPrice = value
        End Set
    End Property
    Public Property PriorSellPrice() As ColField(Of Decimal)
        Get
            PriorSellPrice = _PriorSellPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriorSellPrice = value
        End Set
    End Property
    Public Property CostPrice() As ColField(Of Decimal)
        Get
            CostPrice = _CostPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CostPrice = value
        End Set
    End Property
    Public Property LastSold() As ColField(Of Date)
        Get
            LastSold = _LastSold
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastSold = value
        End Set
    End Property
    Public Property LastReceived() As ColField(Of Date)
        Get
            LastReceived = _LastReceived
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastReceived = value
        End Set
    End Property
    Public Property LastOrdered() As ColField(Of Date)
        Get
            LastOrdered = _LastOrdered
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastOrdered = value
        End Set
    End Property
    Public Property PriceEffectiveDate() As ColField(Of Date)
        Get
            PriceEffectiveDate = _PriceEffectiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _PriceEffectiveDate = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property DateObsolete() As ColField(Of Date)
        Get
            DateObsolete = _DateObsolete
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateObsolete = value
        End Set
    End Property
    Public Property DateDeleted() As ColField(Of Date)
        Get
            DateDeleted = _DateDeleted
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateDeleted = value
        End Set
    End Property
    Public Property StockOnHand() As ColField(Of Integer)
        Get
            StockOnHand = _StockOnHand
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnHand = value
        End Set
    End Property
    Public Property StockOnOrder() As ColField(Of Integer)
        Get
            StockOnOrder = _StockOnOrder
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockOnOrder = value
        End Set
    End Property
    Public Property MinQuantity() As ColField(Of Integer)
        Get
            MinQuantity = _MinQuantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MinQuantity = value
        End Set
    End Property
    Public Property MaxQuantity() As ColField(Of Integer)
        Get
            MaxQuantity = _MaxQuantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MaxQuantity = value
        End Set
    End Property
    Public Property ItemStatus() As ColField(Of String)
        Get
            ItemStatus = _ItemStatus
        End Get
        Set(ByVal value As ColField(Of String))
            _ItemStatus = value
        End Set
    End Property
    Public Property RelatedItemSingle() As ColField(Of Boolean)
        Get
            RelatedItemSingle = _RelatedItemSingle
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RelatedItemSingle = value
        End Set
    End Property
    Public Property RelatedNoItems() As ColField(Of Integer)
        Get
            RelatedNoItems = _RelatedItemsNum
        End Get
        Set(ByVal value As ColField(Of Integer))
            _RelatedItemsNum = value
        End Set
    End Property
    Public Property CatchAll() As ColField(Of Boolean)
        Get
            CatchAll = _CatchAll
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CatchAll = value
        End Set
    End Property
    Public Property ItemObsolete() As ColField(Of Boolean)
        Get
            ItemObsolete = _ItemObsolete
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemObsolete = value
        End Set
    End Property
    Public Property ItemDeleted() As ColField(Of Boolean)
        Get
            ItemDeleted = _ItemDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemDeleted = value
        End Set
    End Property
    Public Property ItemTagged() As ColField(Of Boolean)
        Get
            ItemTagged = _ItemTagged
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemTagged = value
        End Set
    End Property
    Public Property Warranty() As ColField(Of Boolean)
        Get
            Warranty = _Warranty
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Warranty = value
        End Set
    End Property
    Public Property EAN() As ColField(Of Integer)
        Get
            EAN = _Ean
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Ean = value
        End Set
    End Property
    Public Property IsMarkdown() As ColField(Of Boolean)
        Get
            Return _IsMarkdown
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsMarkdown = value
        End Set
    End Property

    Public Property ValueSoldYesterday() As ColField(Of Decimal)
        Get
            ValueSoldYesterday = _ValueSoldYesterday
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldYesterday = value
        End Set
    End Property
    Public Property UnitsSoldYesterday() As ColField(Of Integer)
        Get
            UnitsSoldYesterday = _UnitsSoldYesterday
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldYesterday = value
        End Set
    End Property
    Public Property ValueSoldThisWeek() As ColField(Of Decimal)
        Get
            ValueSoldThisWeek = _ValueSoldThisWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldThisWeek = value
        End Set
    End Property
    Public Property UnitsSoldThisWeek() As ColField(Of Integer)
        Get
            UnitsSoldThisWeek = _UnitsSoldThisWeek
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldThisWeek = value
        End Set
    End Property
    Public Property ValueSoldLastWeek() As ColField(Of Decimal)
        Get
            ValueSoldLastWeek = _ValueSoldLastWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldLastWeek = value
        End Set
    End Property
    Public Property UnitsSoldLastWeek() As ColField(Of Integer)
        Get
            UnitsSoldLastWeek = _UnitsSoldLastWeek
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldLastWeek = value
        End Set
    End Property
    Public Property ValueSoldThisPeriod() As ColField(Of Decimal)
        Get
            ValueSoldThisPeriod = _ValueSoldThisPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldThisPeriod = value
        End Set
    End Property
    Public Property UnitsSoldThisPeriod() As ColField(Of Integer)
        Get
            UnitsSoldThisPeriod = _UnitsSoldThisPeriod
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldThisPeriod = value
        End Set
    End Property
    Public Property ValueSoldLastPeriod() As ColField(Of Decimal)
        Get
            ValueSoldLastPeriod = _ValueSoldLastPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldLastPeriod = value
        End Set
    End Property
    Public Property UnitsSoldLastPeriod() As ColField(Of Integer)
        Get
            UnitsSoldLastPeriod = _UnitsSoldLastPeriod
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldLastPeriod = value
        End Set
    End Property
    Public Property ValueSoldThisYear() As ColField(Of Decimal)
        Get
            ValueSoldThisYear = _ValueSoldThisYear
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldThisYear = value
        End Set
    End Property
    Public Property UnitsSoldThisYear() As ColField(Of Integer)
        Get
            UnitsSoldThisYear = _UnitsSoldThisYear
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldThisYear = value
        End Set
    End Property
    Public Property ValueSoldLastYear() As ColField(Of Decimal)
        Get
            ValueSoldLastYear = _ValueSoldLastYear
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueSoldLastYear = value
        End Set
    End Property
    Public Property UnitsSoldLastYear() As ColField(Of Integer)
        Get
            UnitsSoldLastYear = _UnitsSoldLastYear
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldLastYear = value
        End Set
    End Property
    Public Property DaysOutStockPeriod() As ColField(Of Integer)
        Get
            DaysOutStockPeriod = _DaysOutStockPeriod
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod = value
        End Set
    End Property
    Public Property AverageWeeklySales() As ColField(Of Decimal)
        Get
            AverageWeeklySales = _AverageWeeklySales
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AverageWeeklySales = value
        End Set
    End Property
    Public Property DateFirstStock() As ColField(Of Date)
        Get
            DateFirstStock = _DateFirstStock
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateFirstStock = value
        End Set
    End Property
    Public Property NoWeeksUpdated() As ColField(Of Integer)
        Get
            NoWeeksUpdated = _NumWeeksUpdated
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumWeeksUpdated = value
        End Set
    End Property
    Public Property WeeklyUpdate() As ColField(Of Boolean)
        Get
            WeeklyUpdate = _WeeklyUpdate
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _WeeklyUpdate = value
        End Set
    End Property
    Public Property ConfidentWeeklyAvge() As ColField(Of Boolean)
        Get
            ConfidentWeeklyAvge = _ConfidentWeeklyAve
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ConfidentWeeklyAve = value
        End Set
    End Property

    Public Property UnitsSoldCurWkPlus(ByVal Index As Integer) As ColField(Of Integer)
        Get
            'Extract Column Name
            Dim ColName As String = UnitsSoldCurWkPlus1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Integer))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Integer))
            Dim ColName As String = UnitsSoldCurWkPlus1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Integer)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus1() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus1 = _UnitsSoldCurWkPlus1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus1 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus2() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus2 = _UnitsSoldCurWkPlus2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus2 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus3() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus3 = _UnitsSoldCurWkPlus3
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus3 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus4() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus4 = _UnitsSoldCurWkPlus4
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus4 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus5() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus5 = _UnitsSoldCurWkPlus5
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus5 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus6() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus6 = _UnitsSoldCurWkPlus6
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus6 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus7() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus7 = _UnitsSoldCurWkPlus7
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus7 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus8() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus8 = _UnitsSoldCurWkPlus8
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus8 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus9() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus9 = _UnitsSoldCurWkPlus9
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus9 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus10() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus10 = _UnitsSoldCurWkPlus10
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus10 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus11() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus11 = _UnitsSoldCurWkPlus11
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus11 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus12() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus12 = _UnitsSoldCurWkPlus12
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus12 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus13() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus13 = _UnitsSoldCurWkPlus13
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus13 = value
        End Set
    End Property
    Public Property UnitsSoldCurWkPlus14() As ColField(Of Integer)
        Get
            UnitsSoldCurWkPlus14 = _UnitsSoldCurWkPlus14
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsSoldCurWkPlus14 = value
        End Set
    End Property

    Public Property DaysOutStockPeriod(ByVal Index As Integer) As ColField(Of Integer)
        Get
            'Extract Column Name
            Dim ColName As String = DaysOutStockPeriod1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Integer))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Integer))
            Dim ColName As String = DaysOutStockPeriod1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Integer)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property DaysOutStockPeriod1() As ColField(Of Integer)
        Get
            DaysOutStockPeriod1 = _DaysOutStockPeriod1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod1 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod2() As ColField(Of Integer)
        Get
            DaysOutStockPeriod2 = _DaysOutStockPeriod2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod2 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod3() As ColField(Of Integer)
        Get
            DaysOutStockPeriod3 = _DaysOutStockPeriod3
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod3 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod4() As ColField(Of Integer)
        Get
            DaysOutStockPeriod4 = _DaysOutStockPeriod4
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod4 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod5() As ColField(Of Integer)
        Get
            DaysOutStockPeriod5 = _DaysOutStockPeriod5
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod5 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod6() As ColField(Of Integer)
        Get
            DaysOutStockPeriod6 = _DaysOutStockPeriod6
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod6 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod7() As ColField(Of Integer)
        Get
            DaysOutStockPeriod7 = _DaysOutStockPeriod7
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod7 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod8() As ColField(Of Integer)
        Get
            DaysOutStockPeriod8 = _DaysOutStockPeriod8
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod8 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod9() As ColField(Of Integer)
        Get
            DaysOutStockPeriod9 = _DaysOutStockPeriod9
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod9 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod10() As ColField(Of Integer)
        Get
            DaysOutStockPeriod10 = _DaysOutStockPeriod10
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod10 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod11() As ColField(Of Integer)
        Get
            DaysOutStockPeriod11 = _DaysOutStockPeriod11
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod11 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod12() As ColField(Of Integer)
        Get
            DaysOutStockPeriod12 = _DaysOutStockPeriod12
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod12 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod13() As ColField(Of Integer)
        Get
            DaysOutStockPeriod13 = _DaysOutStockPeriod13
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod13 = value
        End Set
    End Property
    Public Property DaysOutStockPeriod14() As ColField(Of Integer)
        Get
            DaysOutStockPeriod14 = _DaysOutStockPeriod14
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStockPeriod14 = value
        End Set
    End Property

    Public Property Weight() As ColField(Of Decimal)
        Get
            Weight = _Weight
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Weight = value
        End Set
    End Property
    Public Property ShortDescription() As ColField(Of String)
        Get
            ShortDescription = _ShortDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _ShortDescription = value
        End Set
    End Property
    Public Property NoOfPriceChangeRecs() As ColField(Of Integer)
        Get
            NoOfPriceChangeRecs = _NumPriceChangeRecs
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumPriceChangeRecs = value
        End Set
    End Property
    Public Property NoOfAltLocations() As ColField(Of Integer)
        Get
            NoOfAltLocations = _NumAltLocations
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumAltLocations = value
        End Set
    End Property
    Public Property NonStockItem() As ColField(Of Boolean)
        Get
            NonStockItem = _NonStockItem
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _NonStockItem = value
        End Set
    End Property
    Public Property DaysOutOfStock() As ColField(Of Integer)
        Get
            DaysOutOfStock = _DaysOutStock
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DaysOutStock = value
        End Set
    End Property
    Public Property AvgWeeklySales() As ColField(Of Decimal)
        Get
            AvgWeeklySales = _AveWeeklySales
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AveWeeklySales = value
        End Set
    End Property

    Public Property SOQCalcWeeks1() As ColField(Of Integer)
        Get
            SOQCalcWeeks1 = _SoqCalcWeeks1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SoqCalcWeeks1 = value
        End Set
    End Property
    Public Property SOQCalcWeeks4() As ColField(Of Integer)
        Get
            SOQCalcWeeks4 = _SoqCalcWeeks4
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SoqCalcWeeks4 = value
        End Set
    End Property
    Public Property SOQCalcWeeks13() As ColField(Of Integer)
        Get
            SOQCalcWeeks13 = _SoqCalcWeeks13
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SoqCalcWeeks13 = value
        End Set
    End Property
    Public Property SOQOrderPlaced() As ColField(Of Boolean)
        Get
            SOQOrderPlaced = _SoqOrderPlaced
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SoqOrderPlaced = value
        End Set
    End Property

    Public Property UnitsReceivedToday() As ColField(Of Integer)
        Get
            UnitsReceivedToday = _UnitsReceivedToday
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsReceivedToday = value
        End Set
    End Property
    Public Property ValueReceivedToday() As ColField(Of Decimal)
        Get
            ValueReceivedToday = _ValueReceivedToday
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueReceivedToday = value
        End Set
    End Property
    Public Property ActivityToday() As ColField(Of Boolean)
        Get
            ActivityToday = _ActivityToday
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ActivityToday = value
        End Set
    End Property
    Public Property UnitsInOpenReturns() As ColField(Of Integer)
        Get
            UnitsInOpenReturns = _UnitsOpenReturns
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UnitsOpenReturns = value
        End Set
    End Property
    Public Property ValueInOpenReturns() As ColField(Of Decimal)
        Get
            ValueInOpenReturns = _ValueOpenReturns
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueOpenReturns = value
        End Set
    End Property
    Public Property HOCheckDigit() As ColField(Of String)
        Get
            HOCheckDigit = _HOCheckDigit
        End Get
        Set(ByVal value As ColField(Of String))
            _HOCheckDigit = value
        End Set
    End Property
    Public Property NoOfSmallLabels() As ColField(Of Integer)
        Get
            NoOfSmallLabels = _NumSmallLabels
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumSmallLabels = value
        End Set
    End Property

    Public Property StandingOrderNumber() As ColField(Of String)
        Get
            StandingOrderNumber = _StandingOrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StandingOrderNumber = value
        End Set
    End Property
    Public Property StandingOrderValue() As ColField(Of Integer)
        Get
            StandingOrderValue = _StandingOrderValue
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StandingOrderValue = value
        End Set
    End Property
    Public Property StandingOrderDay1() As ColField(Of String)
        Get
            Return _StandingOrderDay1
        End Get
        Set(ByVal value As ColField(Of String))
            _StandingOrderDay1 = value
        End Set
    End Property
    Public Property StandingOrderDay2() As ColField(Of String)
        Get
            Return _StandingOrderDay2
        End Get
        Set(ByVal value As ColField(Of String))
            _StandingOrderDay2 = value
        End Set
    End Property
    Public Property StandingOrderDay3() As ColField(Of String)
        Get
            Return _StandingOrderDay3
        End Get
        Set(ByVal value As ColField(Of String))
            _StandingOrderDay3 = value
        End Set
    End Property

    Public Property NoOfMediumLabels() As ColField(Of Integer)
        Get
            NoOfMediumLabels = _NumMediumLabels
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumMediumLabels = value
        End Set
    End Property
    Public Property NoOfLargeLabels() As ColField(Of Integer)
        Get
            NoOfLargeLabels = _NumLargeLabels
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumLargeLabels = value
        End Set
    End Property
    Public Property ItemVolume() As ColField(Of Decimal)
        Get
            ItemVolume = _ItemVolume
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ItemVolume = value
        End Set
    End Property
    Public Property DoNotOrder() As ColField(Of Boolean)
        Get
            DoNotOrder = _DoNotOrder
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DoNotOrder = value
        End Set
    End Property
    Public Property AlternateSupplier() As ColField(Of String)
        Get
            AlternateSupplier = _AlternateSupplier
        End Get
        Set(ByVal value As ColField(Of String))
            _AlternateSupplier = value
        End Set
    End Property
    Public Property InitialOrderDate() As ColField(Of Date)
        Get
            InitialOrderDate = _InitialOrderDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _InitialOrderDate = value
        End Set
    End Property
    Public Property FinalOrderDate() As ColField(Of Date)
        Get
            FinalOrderDate = _FinalOrderDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _FinalOrderDate = value
        End Set
    End Property
    Public Property PromotionalMinimum() As ColField(Of Integer)
        Get
            PromotionalMinimum = _PromoMin
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PromoMin = value
        End Set
    End Property
    Public Property PromotionalMinStart() As ColField(Of Date)
        Get
            PromotionalMinStart = _PromoStart
        End Get
        Set(ByVal value As ColField(Of Date))
            _PromoStart = value
        End Set
    End Property
    Public Property PromotionalMinEnd() As ColField(Of Date)
        Get
            PromotionalMinEnd = _PromoEnd
        End Get
        Set(ByVal value As ColField(Of Date))
            _PromoEnd = value
        End Set
    End Property
    Public Property PromotionalMiniMCP() As ColField(Of String)
        Get
            PromotionalMiniMCP = _PromoMCP
        End Get
        Set(ByVal value As ColField(Of String))
            _PromoMCP = value
        End Set
    End Property
    Public Property SpacemanDisplayFctr() As ColField(Of Integer)
        Get
            SpacemanDisplayFctr = _SpacemanDisplayFctr
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SpacemanDisplayFctr = value
        End Set
    End Property
    Public Property DateMinLastChanged() As ColField(Of Date)
        Get
            DateMinLastChanged = _DateMinLastChanged
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateMinLastChanged = value
        End Set
    End Property
    Public Property ParticipatingInMCP() As ColField(Of Boolean)
        Get
            ParticipatingInMCP = _ParticipatingInMCP
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ParticipatingInMCP = value
        End Set
    End Property
    Public Property ItemTagType() As ColField(Of String)
        Get
            ItemTagType = _ItemTagType
        End Get
        Set(ByVal value As ColField(Of String))
            _ItemTagType = value
        End Set
    End Property
    Public Property ProductEquivDescr() As ColField(Of String)
        Get
            ProductEquivDescr = _ProductEquivDescr
        End Get
        Set(ByVal value As ColField(Of String))
            _ProductEquivDescr = value
        End Set
    End Property
    Public Property HierFilter() As ColField(Of String)
        Get
            HierFilter = _HieFilter
        End Get
        Set(ByVal value As ColField(Of String))
            _HieFilter = value
        End Set
    End Property
    Public Property RetailPriceEventNo() As ColField(Of String)
        Get
            RetailPriceEventNo = _RetailPriceEventNo
        End Get
        Set(ByVal value As ColField(Of String))
            _RetailPriceEventNo = value
        End Set
    End Property
    Public Property RetailPricePriority() As ColField(Of String)
        Get
            RetailPricePriority = _RetailPricePriority
        End Get
        Set(ByVal value As ColField(Of String))
            _RetailPricePriority = value
        End Set
    End Property
    Public Property EquivalentPriceMult() As ColField(Of Decimal)
        Get
            EquivalentPriceMult = _EquivPriceMult
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EquivPriceMult = value
        End Set
    End Property
    Public Property EquivalentPriceUnit() As ColField(Of String)
        Get
            EquivalentPriceUnit = _EquivPriceUnit
        End Get
        Set(ByVal value As ColField(Of String))
            _EquivPriceUnit = value
        End Set
    End Property
    Public Property MarkDownQuantity() As ColField(Of Integer)
        Get
            MarkDownQuantity = _MarkDownQuantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _MarkDownQuantity = value
        End Set
    End Property
    Public Property WriteOffQuantity() As ColField(Of Integer)
        Get
            WriteOffQuantity = _WriteOffQuantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _WriteOffQuantity = value
        End Set
    End Property
    Public Property AutoApplyPriceChgs() As ColField(Of Boolean)
        Get
            AutoApplyPriceChgs = _AutoApplyPriceChgs
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AutoApplyPriceChgs = value
        End Set
    End Property
    Public Property AllowAdjustments() As ColField(Of String)
        Get
            AllowAdjustments = _AllowAdjustments
        End Get
        Set(ByVal value As ColField(Of String))
            _AllowAdjustments = value
        End Set
    End Property
    Public Property SavedSupplier() As ColField(Of String)
        Get
            SavedSupplier = _SavedSupplier
        End Get
        Set(ByVal value As ColField(Of String))
            _SavedSupplier = value
        End Set
    End Property
    Public Property WebOrderQuantity() As ColField(Of Integer)
        Get
            WebOrderQuantity = _WebOrderQuantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _WebOrderQuantity = value
        End Set
    End Property
    Public Property BallastItem() As ColField(Of String)
        Get
            BallastItem = _BallastItem
        End Get
        Set(ByVal value As ColField(Of String))
            _BallastItem = value
        End Set
    End Property
    Public Property WEEERate() As ColField(Of String)
        Get
            WEEERate = _WEEERate
        End Get
        Set(ByVal value As ColField(Of String))
            _WEEERate = value
        End Set
    End Property
    Public Property WEEESequence() As ColField(Of String)
        Get
            WEEESequence = _WEEESequence
        End Get
        Set(ByVal value As ColField(Of String))
            _WEEESequence = value
        End Set
    End Property
    Public Property HierCategory() As ColField(Of String)
        Get
            HierCategory = _HieCategory
        End Get
        Set(ByVal value As ColField(Of String))
            _HieCategory = value
        End Set
    End Property
    Public Property HierGroup() As ColField(Of String)
        Get
            HierGroup = _HieGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HieGroup = value
        End Set
    End Property
    Public Property HierSubGroup() As ColField(Of String)
        Get
            HierSubGroup = _HieSubGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HieSubGroup = value
        End Set
    End Property
    Public Property HierStyle() As ColField(Of String)
        Get
            HierStyle = _HieStyle
        End Get
        Set(ByVal value As ColField(Of String))
            _HieStyle = value
        End Set
    End Property
    Public Property OffensiveWeaponAge() As ColField(Of Integer)
        Get
            OffensiveWeaponAge = _OffensiveWeaponAge
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OffensiveWeaponAge = value
        End Set
    End Property
    Public Property SolventAge() As ColField(Of Integer)
        Get
            SolventAge = _SolventAge
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SolventAge = value
        End Set
    End Property
    Public Property QuarantineFlag() As ColField(Of String)
        Get
            QuarantineFlag = _QuarantineFlag
        End Get
        Set(ByVal value As ColField(Of String))
            _QuarantineFlag = value
        End Set
    End Property
    Public Property PricingDiscrepency() As ColField(Of Boolean)
        Get
            PricingDiscrepency = _PricingDiscrepency
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PricingDiscrepency = value
        End Set
    End Property
    Public Property SaleTypeAttribute() As ColField(Of String)
        Get
            SaleTypeAttribute = _SaleTypeAttribute
        End Get
        Set(ByVal value As ColField(Of String))
            _SaleTypeAttribute = value
        End Set
    End Property
    Public Property PalletedSku() As ColField(Of Boolean)
        Get
            PalletedSku = _PalletedSku
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PalletedSku = value
        End Set
    End Property
    Public Property ManagedTimberCont() As ColField(Of Integer)
        Get
            ManagedTimberCont = _ManagedTimberCont
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ManagedTimberCont = value
        End Set
    End Property
    Public Property InsulationItem() As ColField(Of Boolean)
        Get
            InsulationItem = _InsulationItem
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _InsulationItem = value
        End Set
    End Property
    Public Property ELectricalItem() As ColField(Of Boolean)
        Get
            ELectricalItem = _ElectricalItem
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ElectricalItem = value
        End Set
    End Property
    Public Property StockHeldOffSale() As ColField(Of Integer)
        Get
            StockHeldOffSale = _StockHeldOffSale
        End Get
        Set(ByVal value As ColField(Of Integer))
            _StockHeldOffSale = value
        End Set
    End Property
    Public Property RecyclingSkuNumber() As ColField(Of String)
        Get
            Return _RecyclingSkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _RecyclingSkuNumber = value
        End Set
    End Property

    Public Property IsInitialised() As ColField(Of Boolean)
        Get
            Return _IsInitialised
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsInitialised = value
        End Set
    End Property
    Public Property DemandPattern() As ColField(Of String)
        Get
            Return _DemandPattern
        End Get
        Set(ByVal value As ColField(Of String))
            _DemandPattern = value
            Select Case value.Value
                Case "Obsolete", "O" : _DemandPatternEnum = DemandPatterns.Obsolete
                Case "Superceded", "D" : _DemandPatternEnum = DemandPatterns.Superceded
                Case "IsNew", "N" : _DemandPatternEnum = DemandPatterns.New
                Case "Erratic", "E" : _DemandPatternEnum = DemandPatterns.Erratic
                Case "Fast", "F" : _DemandPatternEnum = DemandPatterns.Fast
                Case "Slow", "S" : _DemandPatternEnum = DemandPatterns.Slow
                Case "Lumpy", "L" : _DemandPatternEnum = DemandPatterns.Lumpy
                Case "TrendUp", "T" : _DemandPatternEnum = DemandPatterns.TrendUp
                Case "TrendDown", "G" : _DemandPatternEnum = DemandPatterns.TrendDown
                Case "Normal", "M" : _DemandPatternEnum = DemandPatterns.Normal
                Case Else : _DemandPatternEnum = DemandPatterns.None
            End Select
        End Set
    End Property
    Public Property PeriodDemand() As ColField(Of Decimal)
        Get
            PeriodDemand = _PeriodDemand
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PeriodDemand = value
        End Set
    End Property
    Public Property PeriodTrend() As ColField(Of Decimal)
        Get
            PeriodTrend = _PeriodTrend
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PeriodTrend = value
        End Set
    End Property
    Public Property ErrorForecast() As ColField(Of Decimal)
        Get
            ErrorForecast = _ErrorForecast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ErrorForecast = value
        End Set
    End Property
    Public Property ErrorSmoothed() As ColField(Of Decimal)
        Get
            ErrorSmoothed = _ErrorSmoothed
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ErrorSmoothed = value
        End Set
    End Property
    Public Property ValueAnnualUsage() As ColField(Of String)
        Get
            ValueAnnualUsage = _ValueAnnualUsage
        End Get
        Set(ByVal value As ColField(Of String))
            _ValueAnnualUsage = value
        End Set
    End Property
    Public Property BufferConversion() As ColField(Of Decimal)
        Get
            BufferConversion = _BufferConversion
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BufferConversion = value
        End Set
    End Property
    Public Property BufferStock() As ColField(Of Integer)
        Get
            BufferStock = _BufferStock
        End Get
        Set(ByVal value As ColField(Of Integer))
            _BufferStock = value
        End Set
    End Property
    Public Property ServiceLevelOverride() As ColField(Of Decimal)
        Get
            ServiceLevelOverride = _ServiceLevelOverride
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ServiceLevelOverride = value
        End Set
    End Property
    Public Property StockLossPerWeek() As ColField(Of Decimal)
        Get
            Return _StockLossPerWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _StockLossPerWeek = value
        End Set
    End Property
    Public Property OrderLevel() As ColField(Of Integer)
        Get
            OrderLevel = _OrderLevel
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderLevel = value
        End Set
    End Property

    Public Property FlierPeriod(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = FlierPeriod1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = FlierPeriod1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property FlierPeriod1() As ColField(Of String)
        Get
            FlierPeriod1 = _FlierPeriod1
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod1 = value
        End Set
    End Property
    Public Property FlierPeriod2() As ColField(Of String)
        Get
            FlierPeriod2 = _FlierPeriod2
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod2 = value
        End Set
    End Property
    Public Property FlierPeriod3() As ColField(Of String)
        Get
            FlierPeriod3 = _FlierPeriod3
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod3 = value
        End Set
    End Property
    Public Property FlierPeriod4() As ColField(Of String)
        Get
            FlierPeriod4 = _FlierPeriod4
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod4 = value
        End Set
    End Property
    Public Property FlierPeriod5() As ColField(Of String)
        Get
            FlierPeriod5 = _FlierPeriod5
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod5 = value
        End Set
    End Property
    Public Property FlierPeriod6() As ColField(Of String)
        Get
            FlierPeriod6 = _FlierPeriod6
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod6 = value
        End Set
    End Property
    Public Property FlierPeriod7() As ColField(Of String)
        Get
            FlierPeriod7 = _FlierPeriod7
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod7 = value
        End Set
    End Property
    Public Property FlierPeriod8() As ColField(Of String)
        Get
            FlierPeriod8 = _FlierPeriod8
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod8 = value
        End Set
    End Property
    Public Property FlierPeriod9() As ColField(Of String)
        Get
            FlierPeriod9 = _FlierPeriod9
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod9 = value
        End Set
    End Property
    Public Property FlierPeriod10() As ColField(Of String)
        Get
            FlierPeriod10 = _FlierPeriod10
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod10 = value
        End Set
    End Property
    Public Property FlierPeriod11() As ColField(Of String)
        Get
            FlierPeriod11 = _FlierPeriod11
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod11 = value
        End Set
    End Property
    Public Property FlierPeriod12() As ColField(Of String)
        Get
            FlierPeriod12 = _FlierPeriod12
        End Get
        Set(ByVal value As ColField(Of String))
            _FlierPeriod12 = value
        End Set
    End Property
    Public Property PatternSeason() As ColField(Of String)
        Get
            PatternSeason = _PatternSeason
        End Get
        Set(ByVal value As ColField(Of String))
            _PatternSeason = value
        End Set
    End Property
    Public Property PatternBankHol() As ColField(Of String)
        Get
            PatternBankHol = _PatternBankHol
        End Get
        Set(ByVal value As ColField(Of String))
            _PatternBankHol = value
        End Set
    End Property
    Public Property PatternPromo() As ColField(Of String)
        Get
            PatternPromo = _PatternPromo
        End Get
        Set(ByVal value As ColField(Of String))
            _PatternPromo = value
        End Set
    End Property
    Public Property DateLastForeInit() As ColField(Of Date)
        Get
            DateLastForeInit = _DateLastForeInit
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastForeInit = value
        End Set
    End Property

    Public Property SalesBias(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            'Extract Column Name
            Dim ColName As String = SalesBias0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))
            Dim ColName As String = SalesBias0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property SalesBias0() As ColField(Of Decimal)
        Get
            SalesBias0 = _SalesBias0
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias0 = value
        End Set
    End Property
    Public Property SalesBias1() As ColField(Of Decimal)
        Get
            SalesBias1 = _SalesBias1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias1 = value
        End Set
    End Property
    Public Property SalesBias2() As ColField(Of Decimal)
        Get
            SalesBias2 = _SalesBias2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias2 = value
        End Set
    End Property
    Public Property SalesBias3() As ColField(Of Decimal)
        Get
            SalesBias3 = _SalesBias3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias3 = value
        End Set
    End Property
    Public Property SalesBias4() As ColField(Of Decimal)
        Get
            SalesBias4 = _SalesBias4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias4 = value
        End Set
    End Property
    Public Property SalesBias5() As ColField(Of Decimal)
        Get
            SalesBias5 = _SalesBias5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias5 = value
        End Set
    End Property
    Public Property SalesBias6() As ColField(Of Decimal)
        Get
            SalesBias6 = _SalesBias6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesBias6 = value
        End Set
    End Property

    Public Property ToForecast() As ColField(Of Boolean)
        Get
            ToForecast = _ToForecast
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ToForecast = value
        End Set
    End Property

    Public Property DateLastSoldExcludingRefund() As ColField(Of Date)
        Get
            DateLastSoldExcludingRefund = _DateLastSoldExcludingRefund
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastSoldExcludingRefund = value
        End Set
    End Property
#End Region

#Region "Entities"
    Private _Stocks As New List(Of cStock)
    Private _WeeeStock As cStock = Nothing
    Private _Adjust As cStockAdjust = Nothing
    Private _adjustMarkdown As cStockAdjust = Nothing
    Private _Markdown As cMarkdownStock = Nothing

    Private _Adjustments As List(Of cStockAdjust) = Nothing

    Public Property Stock(ByVal SkuNumber As String) As cStock
        Get
            For Each s As cStock In _Stocks
                If s.SkuNumber.Value = SkuNumber Then Return s
            Next
            Return Nothing
        End Get
        Set(ByVal value As cStock)
            For Each s As cStock In _Stocks
                If s.SkuNumber.Value = SkuNumber Then s = value
            Next
        End Set
    End Property
    Public Property Stocks() As List(Of cStock)
        Get
            Return _Stocks
        End Get
        Set(ByVal value As List(Of cStock))
            _Stocks = value
        End Set
    End Property
    Public Property WeeeStock() As cStock
        Get
            If _WeeeStock Is Nothing Then
                _WeeeStock = New cStock(Oasys3DB)
                _WeeeStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _WeeeStock.SkuNumber, _RecyclingSkuNumber.Value)
                _WeeeStock.LoadMatches()
            End If
            Return _WeeeStock
        End Get
        Set(ByVal value As cStock)
            _WeeeStock = value
        End Set
    End Property

    Public ReadOnly Property Adjust() As cStockAdjust
        Get
            If _Adjust Is Nothing Then
                _Adjust = New cStockAdjust(Oasys3DB)
                _Adjust.SkuNumber.Value = _SkuNumber.Value
                _Adjust.StartStock.Value = _StockOnHand.Value
                _Adjust.Price.Value = _NormalSellPrice.Value
                _Adjust.Cost.Value = _CostPrice.Value
            End If
            Return _Adjust
        End Get
    End Property
    Public ReadOnly Property AdjustMarkdown() As cStockAdjust
        Get
            If _adjustMarkdown Is Nothing Then
                _adjustMarkdown = New cStockAdjust(Oasys3DB)
                _adjustMarkdown.SkuNumber.Value = _SkuNumber.Value
                _adjustMarkdown.StartStock.Value = _MarkDownQuantity.Value
                _adjustMarkdown.Price.Value = _NormalSellPrice.Value
                _adjustMarkdown.Cost.Value = _CostPrice.Value
            End If
            Return _adjustMarkdown
        End Get
    End Property
    Public ReadOnly Property Markdown() As cMarkdownStock
        Get
            If _Markdown Is Nothing Then
                _Markdown = New cMarkdownStock(Oasys3DB)
                _Markdown.SkuNumber.Value = _SkuNumber.Value
                _Markdown.Price.Value = _NormalSellPrice.Value

                'get schedule
                Dim hieMaster As New BOHierarchy.cHierarchyMaster(Oasys3DB)
                Dim schedule As Decimal() = hieMaster.GetSchedule(_HieCategory.Value, _HieGroup.Value, _HieSubGroup.Value, _HieStyle.Value)

                'if no schedule found then get default from parameters
                If schedule Is Nothing Then
                    Dim Param As New BOSystem.cParameter(Oasys3DB)
                    Dim def As String = Param.GetParameterString(4101)
                    Dim defs() As String = def.Split(","c)

                    schedule = New Decimal(9) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                    For index As Integer = 0 To 9
                        If index <= defs.GetUpperBound(0) Then
                            schedule(index) = CDec(defs(index))
                        End If
                    Next
                End If

                For index As Integer = 0 To 9
                    _Markdown.ReductionWeek(index + 1).Value = schedule(index)
                Next

            End If
            Return _Markdown
        End Get
    End Property

    'Public Property Adjustments() As List(Of cStockAdjust)
    '    Get
    '        Return _Adjustments
    '    End Get
    '    Set(ByVal value As List(Of cStockAdjust))
    '        _Adjustments = value
    '    End Set
    'End Property
    'Public Property Markdowns() As List(Of cMarkdownStock)
    '    Get
    '        If _Markdowns Is Nothing Then LoadMarkdowns()
    '        Return _Markdowns
    '    End Get
    '    Set(ByVal value As List(Of cMarkdownStock))
    '        _Markdowns = value
    '    End Set
    'End Property
    'Public Property Markdown(ByVal Serial As String) As cMarkdownStock
    '    Get
    '        For Each mark As cMarkdownStock In Markdowns
    '            If mark.Serial.Value = Serial Then Return mark
    '        Next
    '        Return Nothing
    '    End Get
    '    Set(ByVal value As cMarkdownStock)
    '        For Each mark As cMarkdownStock In Markdowns
    '            If mark.Serial.Value = Serial Then mark = value
    '        Next
    '    End Set
    'End Property

#End Region

#Region "Delegates"
    Public Delegate Function PromptYesNo(ByVal message As String) As Boolean
    Private _PromptYesNo As PromptYesNo

#End Region

#Region "Methods"
    Private _TempQty As Integer = 0
    Public Property TempQty() As Integer
        Get
            Return _TempQty
        End Get
        Set(ByVal value As Integer)
            _TempQty = value
        End Set
    End Property

    ''' <summary>
    ''' Load stock item into current instance given sku number/inhouse barcode/ean barcode
    ''' </summary>
    ''' <param name="SkuNumber"></param>
    ''' <param name="ThrowError"></param>
    ''' <remarks></remarks>
    Public Sub LoadStockItem(ByVal SkuNumber As String, Optional ByVal ThrowError As Boolean = True)

        ClearLists()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, GetSkuNumber(SkuNumber))
        If LoadMatches.Count = 0 And ThrowError Then
            Throw New Exception(My.Resources.Errors.StockItemNotFound)
        End If

    End Sub

    ''' <summary>
    ''' Load stock item into current instance given sku number/inhouse barcode/ean barcode and supplier number.
    '''  Throws Oasys exception with message if no stock item found.
    '''  Throws an Oasys exception on error
    ''' </summary>
    ''' <param name="SkuNumber"></param>
    ''' <param name="SupplierNumber"></param>
    ''' <remarks></remarks>
    Public Sub LoadStockItem(ByVal SkuNumber As String, ByVal SupplierNumber As String)

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, GetSkuNumber(SkuNumber))
            JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _SupplierNumber, SupplierNumber)
            If LoadMatches.Count = 0 Then
                Throw New OasysDbException(My.Resources.Errors.StockItemNotFound)
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StockLoadItems, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads given skus into stocks collection of this instance
    ''' </summary>
    ''' <param name="SkuNumbers"></param>
    ''' <remarks></remarks>
    Public Sub LoadStockItems(ByVal SkuNumbers As ArrayList)

        Try
            If SkuNumbers Is Nothing Then Exit Sub
            If SkuNumbers.Count = 0 Then Exit Sub

            Dim s As New cStock(Oasys3DB)
            s.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pIn, s.SkuNumber, SkuNumbers)
            _Stocks = s.LoadMatches

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function LoadStockItemForPriceChange(ByVal SkuNumber As String) As Boolean

        LoadStockItemForPriceChange = True
        ClearLists()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, GetSkuNumber(SkuNumber))
        If LoadMatches.Count = 0 Then
            LoadStockItemForPriceChange = False
        End If

    End Function


    ''' <summary>
    ''' Load non deleted or obsolete stock item into current instance given sku number/inhouse barcode/ean barcode. 
    ''' Throws an Oasys exception on error
    ''' </summary>
    ''' <param name="SkuNumber"></param>
    ''' <remarks></remarks>
    Public Sub LoadValidStockItem(ByVal SkuNumber As String)

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemDeleted, False)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemObsolete, False)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, GetSkuNumber(SkuNumber))
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StockLoadItems, ex)
        End Try

    End Sub
    Public Sub LoadValidStockItem(ByVal SkuNumber As String, ByVal SupplierNumber As String)

        Try
            ClearLists()
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemDeleted, False)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemObsolete, False)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, GetSkuNumber(SkuNumber))
            JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _SupplierNumber, SupplierNumber)
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StockLoadItems, ex)
        End Try

    End Sub


    ''' <summary>
    ''' Get stock item given sku number/inhouse barcode/ean barcode. Returns nothing if not found
    ''' </summary>
    ''' <param name="SkuNumber"></param>
    ''' <remarks></remarks>
    Public Function GetStockItem(ByVal skuNumber As String) As BOStock.cStock

        Dim stock As New BOStock.cStock(Oasys3DB)
        stock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, stock.SkuNumber, GetSkuNumber(skuNumber))
        If stock.LoadMatches.Count = 0 Then
            Return Nothing
        Else
            Return stock
        End If

    End Function


    Public Function GetStocksForSupplierOrder(ByVal supplierNumber As String) As DataTable

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_AlternateSupplier.ColumnName, clsOasys3DB.eOperator.pEquals, supplierNumber)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Return dt
            'Dim StockBO As New BOStock.cStock(Oasys3DB)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.AlternateSupplier, supplierNumber)
            'StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.ItemDeleted, False)
            'StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.ItemObsolete, False)
            'StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.RelatedItemSingle, False)
            'StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.NonStockItem, False)
            'StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.DoNotOrder, False)


        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetSkuNumber(ByVal SkuNumber As String) As String

        Try
            If (SkuNumber.Length <= 6) Then
                SkuNumber = SkuNumber.PadLeft(6, "0"c)
            ElseIf SkuNumber.Length = 8 AndAlso SkuNumber.Substring(0, 1) = "2" Then
                SkuNumber = SkuNumber.Substring(1, 6)
            Else
                Dim BarCodes As New BOStock.cBarCodes(Oasys3DB)
                SkuNumber = BarCodes.GetSkuNumber(SkuNumber)
            End If

            Return SkuNumber

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    ''' <summary>
    ''' Returns whether or not there is an item loaded into this instance
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Loaded() As Boolean
        If _SkuNumber.Value <> "000000" Then Return True
        Return False
    End Function



    Public Function PicValidate() As Boolean

        If _ItemObsolete.Value Or _NonStockItem.Value Then
            If (_StockOnHand.Value = 0) And (_MarkDownQuantity.Value = 0) And (_WriteOffQuantity.Value = 0) Then
                Return False
            End If
        End If

        If _StockOnHand.Value < 0 Then
            If _RelatedItemSingle.Value Then Return False
            If _CatchAll.Value Then Return False
        End If

        Return True

    End Function

    Public Function SoqMinimumStock(ByVal OrderDue As Date, ByVal OrderNextDue As Date) As Integer

        If OrderDue >= _PromoStart.Value And OrderDue < _PromoEnd.Value Then
            Return _PromoMin.Value

        ElseIf OrderNextDue >= _PromoStart.Value And OrderNextDue < _PromoEnd.Value Then
            Return _PromoMin.Value

        Else
            Return _MinQuantity.Value
        End If


    End Function

    ''' <summary>
    ''' Returns current stock holding value for stock items with positive stock level.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOnHandValueAllStock() As Decimal

        Try
            'set up sql
            Dim sb As New StringBuilder("SELECT ")
            sb.Append("SUM(" & _StockOnHand.ColumnName & " * " & _NormalSellPrice.ColumnName & ") ")
            sb.Append("FROM " & TableName & " WHERE " & _StockOnHand.ColumnName & " > 0")

            Oasys3DB.ClearAllParameters()
            Dim dt As DataTable = Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

            If Not IsDBNull(dt.Rows(0)(0)) Then
                Return CDec(FormatNumber(dt.Rows(0)(0), 2))
            End If

            Return 0

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StockGetPositiveStockValue, ex)
        End Try

    End Function

    Public Function UpdateOrdered(ByVal QtyOrdered As Integer) As Boolean

        _StockOnOrder.Value += QtyOrdered
        _OrderLevel.Value = 0
        _LastOrdered.Value = Now.Date
        _ActivityToday.Value = True

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnOrder.ColumnName, _StockOnOrder.Value)
        Oasys3DB.SetColumnAndValueParameter(_OrderLevel.ColumnName, _OrderLevel.Value)
        Oasys3DB.SetColumnAndValueParameter(_LastOrdered.ColumnName, _LastOrdered.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function

    Public Function UpdateReceived(ByVal QtyOrdered As Integer, ByVal QtyReceived As Integer, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        _StockOnHand.Value += QtyReceived
        _StockOnOrder.Value -= QtyReceived
        If _StockOnOrder.Value < 0 Then _StockOnOrder.Value = 0

        _UnitsReceivedToday.Value += QtyReceived
        _ValueReceivedToday.Value += (QtyReceived * _NormalSellPrice.Value)
        _LastReceived.Value = Now.Date
        _ActivityToday.Value = True

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(_StockOnOrder.ColumnName, _StockOnOrder.Value)
        Oasys3DB.SetColumnAndValueParameter(_UnitsReceivedToday.ColumnName, _UnitsReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueReceivedToday.ColumnName, _ValueReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_LastReceived.ColumnName, _LastReceived.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            Dim log As New cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "72"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = startStock
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            log.SaveIfNew()
            Return True
        End If
        Return False

    End Function

    Public Function UpdateReceivedIssue(ByVal QtyOrdered As Integer, ByVal QtyReceived As Integer, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        _StockOnHand.Value += QtyReceived
        _StockOnOrder.Value -= QtyOrdered
        If _StockOnOrder.Value < 0 Then _StockOnOrder.Value = 0

        _UnitsReceivedToday.Value += QtyReceived
        _ValueReceivedToday.Value += (QtyReceived * _NormalSellPrice.Value)
        _LastReceived.Value = Now.Date
        _ActivityToday.Value = True

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(_StockOnOrder.ColumnName, _StockOnOrder.Value)
        Oasys3DB.SetColumnAndValueParameter(_UnitsReceivedToday.ColumnName, _UnitsReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueReceivedToday.ColumnName, _ValueReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_LastReceived.ColumnName, _LastReceived.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            Dim log As New cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "71"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = startStock
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            log.SaveIfNew()
            Return True
        End If
        Return False

    End Function

    Public Function UpdateReceivedMaintained(ByVal QtyDifference As Integer, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        _StockOnHand.Value += QtyDifference
        _UnitsReceivedToday.Value += QtyDifference
        _ValueReceivedToday.Value += (QtyDifference * _NormalSellPrice.Value)
        _ActivityToday.Value = True

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(_UnitsReceivedToday.ColumnName, _UnitsReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueReceivedToday.ColumnName, _ValueReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()
        If Oasys3DB.Update > 0 Then
            Dim log As New cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "73"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = startStock
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            log.SaveIfNew()
            Return True
        End If
        Return False

    End Function

    Public Function UpdateOrderLevel() As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_OrderLevel.ColumnName, _OrderLevel.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then Return True
        Return False

    End Function

    Public Function UpdateOrderQty(ByVal OrderQty As Integer) As Boolean

        _ActivityToday.Value = True
        _StockOnOrder.Value += OrderQty

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_StockOnOrder.ColumnName, _StockOnOrder.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then Return True
        Return False

    End Function

    Public Function UpdateSupplierOrderLevelZero(Optional ByVal ObsoleteSupercededOnly As Boolean = False) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_OrderLevel.ColumnName, 0)
        Oasys3DB.SetWhereParameter(_SupplierNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SupplierNumber.Value)
        If Oasys3DB.Update > 0 Then Return True
        Return False

    End Function


    Public Function UpdateReturnCreate(ByVal Qty As Decimal, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        Dim startReturn As Integer = _UnitsOpenReturns.Value

        _StockOnHand.Value -= CInt(Qty)
        _ActivityToday.Value = True
        _UnitsOpenReturns.Value += CInt(Qty)
        _ValueOpenReturns.Value += Qty * _NormalSellPrice.Value

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsInOpenReturns.ColumnName, _UnitsOpenReturns.Value)
        Oasys3DB.SetColumnAndValueParameter(ValueInOpenReturns.ColumnName, _ValueOpenReturns.Value)
        SetDBKeys()
        If Oasys3DB.Update > 0 Then
            Dim log As New BOStock.cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "21"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = startStock
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = startReturn
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            If log.SaveIfNew() Then Return True
        End If
        Return False

    End Function

    Public Function UpdateReturnRelease(ByVal Qty As Decimal, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        Dim startReturn As Integer = _UnitsOpenReturns.Value

        _ActivityToday.Value = True
        _UnitsOpenReturns.Value -= CInt(Qty)
        _ValueOpenReturns.Value -= Qty * _NormalSellPrice.Value
        _UnitsReceivedToday.Value -= CInt(Qty)
        _ValueReceivedToday.Value -= Qty * _NormalSellPrice.Value

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_UnitsOpenReturns.ColumnName, _UnitsOpenReturns.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueOpenReturns.ColumnName, _ValueOpenReturns.Value)
        Oasys3DB.SetColumnAndValueParameter(_UnitsReceivedToday.ColumnName, _UnitsReceivedToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueReceivedToday.ColumnName, _ValueReceivedToday.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            Dim log As New BOStock.cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "22"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = _StockOnHand.Value
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = startReturn
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            If log.SaveIfNew() Then Return True
        End If
        Return False

    End Function

    Public Function UpdateReturnDelete(ByVal Qty As Decimal, ByVal key As String, ByVal UserID As Integer) As Boolean

        Dim startStock As Integer = _StockOnHand.Value
        Dim startReturn As Integer = _UnitsOpenReturns.Value

        _StockOnHand.Value += CInt(Qty)
        _ActivityToday.Value = True
        _UnitsOpenReturns.Value -= CInt(Qty)
        _ValueOpenReturns.Value -= Qty * _NormalSellPrice.Value

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsInOpenReturns.ColumnName, _UnitsOpenReturns.Value)
        Oasys3DB.SetColumnAndValueParameter(ValueInOpenReturns.ColumnName, _ValueOpenReturns.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            Dim log As New BOStock.cStockLog(Oasys3DB)
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogType.Value = "21"
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = key
            log.EmployeeID.Value = UserID.ToString("000")
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = startStock
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = startReturn
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _NormalSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            If log.SaveIfNew() Then Return True
        End If
        Return False

    End Function


    Public Sub UpdatePriceChanges(ByRef PriceChange As cPriceChange)

        _PriorSellPrice.Value = _NormalSellPrice.Value
        _NormalSellPrice.Value = PriceChange.NewPrice.Value
        _PriceEffectiveDate.Value = PriceChange.AutoAppliedDate.Value
        _RetailPriceEventNo.Value = PriceChange.EventNo.Value
        _RetailPricePriority.Value = PriceChange.EventPriority.Value

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_PriorSellPrice.ColumnName, _PriorSellPrice.Value)
        Oasys3DB.SetColumnAndValueParameter(_PriceEffectiveDate.ColumnName, _PriceEffectiveDate.Value)
        Oasys3DB.SetColumnAndValueParameter(_NormalSellPrice.ColumnName, _NormalSellPrice.Value)
        Oasys3DB.SetColumnAndValueParameter(_RetailPriceEventNo.ColumnName, _RetailPriceEventNo.Value)
        Oasys3DB.SetColumnAndValueParameter(_RetailPricePriority.ColumnName, _RetailPricePriority.Value)
        SetDBKeys()

        Dim sb As New StringBuilder(SkuNumber.Value & Space(4))
        sb.Append(PriceChange.EffectiveDate.Value.ToString("dd/MM/yy") & Space(2))
        sb.Append(PriceChange.ChangeStatus.Value)

        If Oasys3DB.Update > 0 Then
            Dim log As New BOStock.cStockLog(Oasys3DB)
            log.LogType.Value = "61"
            log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
            log.LogDate.Value = Now.Date
            log.LogTime.Value = Format(Now, "HHmmss")
            log.LogKey.Value = sb.ToString
            log.EmployeeID.Value = PriceChange.EmployeeID.Value
            log.SentToHO.Value = False
            log.SkuNumber.Value = _SkuNumber.Value
            log.StockOnHandStart.Value = _StockOnHand.Value
            log.StockOnHandEnd.Value = _StockOnHand.Value
            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
            log.PriceStart.Value = _PriorSellPrice.Value
            log.PriceEnd.Value = _NormalSellPrice.Value
            log.SaveIfNew()
        End If

    End Sub

    Public Function UpdateWeeklyStock() As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(WeeklyUpdate.ColumnName, WeeklyUpdate.Value)
        Oasys3DB.SetColumnAndValueParameter(AvgWeeklySales.ColumnName, AvgWeeklySales.Value)
        Oasys3DB.SetColumnAndValueParameter(AverageWeeklySales.ColumnName, AverageWeeklySales.Value)
        Oasys3DB.SetColumnAndValueParameter(ValueSoldLastWeek.ColumnName, ValueSoldLastWeek.Value)
        Oasys3DB.SetColumnAndValueParameter(ValueSoldThisWeek.ColumnName, ValueSoldThisWeek.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldLastWeek.ColumnName, UnitsSoldLastWeek.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldThisWeek.ColumnName, UnitsSoldThisWeek.Value)
        Oasys3DB.SetColumnAndValueParameter(NoWeeksUpdated.ColumnName, NoWeeksUpdated.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod.ColumnName, DaysOutStockPeriod.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutOfStock.ColumnName, DaysOutOfStock.Value)
        Oasys3DB.SetColumnAndValueParameter(DateFirstStock.ColumnName, DateFirstStock.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus1.ColumnName, UnitsSoldCurWkPlus1.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus2.ColumnName, UnitsSoldCurWkPlus2.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus3.ColumnName, UnitsSoldCurWkPlus3.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus4.ColumnName, UnitsSoldCurWkPlus4.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus5.ColumnName, UnitsSoldCurWkPlus5.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus6.ColumnName, UnitsSoldCurWkPlus6.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus7.ColumnName, UnitsSoldCurWkPlus7.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus8.ColumnName, UnitsSoldCurWkPlus8.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus9.ColumnName, UnitsSoldCurWkPlus9.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus10.ColumnName, UnitsSoldCurWkPlus10.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus11.ColumnName, UnitsSoldCurWkPlus11.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus12.ColumnName, UnitsSoldCurWkPlus12.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus13.ColumnName, UnitsSoldCurWkPlus13.Value)
        Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus14.ColumnName, UnitsSoldCurWkPlus14.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod1.ColumnName, DaysOutStockPeriod1.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod2.ColumnName, DaysOutStockPeriod2.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod3.ColumnName, DaysOutStockPeriod3.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod4.ColumnName, DaysOutStockPeriod4.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod5.ColumnName, DaysOutStockPeriod5.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod6.ColumnName, DaysOutStockPeriod6.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod7.ColumnName, DaysOutStockPeriod7.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod8.ColumnName, DaysOutStockPeriod8.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod9.ColumnName, DaysOutStockPeriod9.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod10.ColumnName, DaysOutStockPeriod10.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod11.ColumnName, DaysOutStockPeriod11.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod12.ColumnName, DaysOutStockPeriod12.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod13.ColumnName, DaysOutStockPeriod13.Value)
        Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod14.ColumnName, DaysOutStockPeriod14.Value)
        Oasys3DB.SetColumnAndValueParameter(ToForecast.ColumnName, ToForecast.Value)
        Oasys3DB.SetWhereParameter(SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, SkuNumber.Value)
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function





    ''' <summary>
    ''' Equivalent price for labelling
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EquivPrice() As String

        If _EquivPriceMult.Value > 0 Then
            Dim factor As Double = (_NormalSellPrice.Value + WeeeStock.NormalSellPrice.Value) / _EquivPriceMult.Value
            If factor < 1 Then
                factor += 0.000499
                Return Format(factor, "0.000")
            Else
                factor += 0.00499
                Return Format(factor, "0.00")
            End If
        Else
            Return String.Empty
        End If

    End Function

    Public Function GetRecordsToDelete(ByVal DateHorizion As Date) As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Me._DateDeleted, DateHorizion)
        Me.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Me._ItemObsolete, True)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Public Function GetAllRecords() As DataTable
        Dim ds As DataSet

        Oasys3DB.ClearAllParameters()
        Me.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ItemObsolete, True)
        ds = Me.GetSQLSelectDataSet
        If ds IsNot Nothing AndAlso ds.Tables.Count >= 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Public Function SetDeletionFlag(ByVal partcode As String, ByVal Flag As Boolean, ByVal DeletionDate As Date) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        Oasys3DB.SetColumnAndValueParameter(_DateDeleted.ColumnName, CDate(DeletionDate))
        Oasys3DB.SetColumnAndValueParameter(_ItemDeleted.ColumnName, Flag)

        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, partcode)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function


    Public Function RecordSales(ByVal partcode As String, ByVal IBTQty As Integer) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        Oasys3DB.SetColumnAndValueParameter(_UnitsSoldCurWkPlus1.ColumnName, IBTQty)

        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, partcode)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function
    Public Function ProcessPeriodEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty
        Try
            'move data 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(ValueSoldLastPeriod.ColumnName, ValueSoldThisPeriod)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldLastPeriod.ColumnName, UnitsSoldThisPeriod)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            If SaveError = False Then
                'zeroise the fields 
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                Oasys3DB.SetColumnAndValueParameter(ValueSoldThisPeriod.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(UnitsSoldThisPeriod.ColumnName, 0)

                NoRecs = Oasys3DB.Update()

                If NoRecs = -1 Then
                    SaveError = True
                Else
                    SaveError = False
                End If
            End If

            Return SaveError
        Catch ex As Exception
            Trace.WriteLine("cStock - ProcesPeriodEnd failed with exception " & ex.Message)
            Return False
        End Try

    End Function
    Public Function ProcessYearEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty
        Try
            'move data 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(_ValueSoldLastYear.ColumnName, _ValueSoldThisYear)
            Oasys3DB.SetColumnAndValueParameter(_UnitsSoldLastYear.ColumnName, _UnitsSoldThisYear)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            If SaveError = False Then
                'zeroise the fields 
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                Oasys3DB.SetColumnAndValueParameter(_DaysOutStock.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(_ValueSoldThisYear.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(_UnitsSoldThisYear.ColumnName, 0)

                NoRecs = Oasys3DB.Update()

                If NoRecs = -1 Then
                    SaveError = True
                Else
                    SaveError = False
                End If
            End If

            Return SaveError
        Catch ex As Exception
            Trace.WriteLine("cStock - ProcessYearEnd failed with exception " & ex.Message)
            Return False
        End Try


    End Function

    Public Function UpdateNightlyStock() As Boolean

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(WeeklyUpdate.ColumnName, WeeklyUpdate.Value)
            Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod.ColumnName, DaysOutStockPeriod.Value)
            Oasys3DB.SetColumnAndValueParameter(DaysOutOfStock.ColumnName, DaysOutOfStock.Value)
            Oasys3DB.SetColumnAndValueParameter(DateFirstStock.ColumnName, DateFirstStock.Value)
            Oasys3DB.SetColumnAndValueParameter(DaysOutStockPeriod1.ColumnName, DaysOutStockPeriod1.Value)
            Oasys3DB.SetWhereParameter(SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, SkuNumber.Value)
            If Oasys3DB.Update() > 0 Then Return True
            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Function ResetDailyActivity() As Boolean
        Oasys3DB.ExecuteSql("UPDATE STKMAS SET tact = '0',salu1 = '0', salv1 = '0', treq = '0' , trev = '0' where (TACT <> '0' OR SALU1 <> '0' OR SALV1 <> '0' OR TREQ <> '0' OR TREV <> '0')")
    End Function

    Public Function GetHierachy(ByVal Key As String, ByVal Exclude() As String) As List(Of cStock)

        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            GetHierachy = GetHierachyNew(Key, Exclude)
        Else
            GetHierachy = GetHierachyOriginal(Key, Exclude)
        End If
    End Function

    Public Function GetHierachyOriginal(ByVal Key As String, ByVal exclude() As String) As List(Of cStock)
        Dim list As List(Of cStock)

        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieStyle, Key)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, exclude)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieSubGroup, Key)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, exclude)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieGroup, Key)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, exclude)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieCategory, Key)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, exclude)
            list = LoadMatches()
            If list.Count > 0 Then
                Return list
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cStock - GetSyle exception " & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function GetHierachyNew(ByVal Key As String, ByVal Exclude() As String) As List(Of cStock)
        Dim StockList As List(Of cStock)

        Try
            If Exclude.Count = 0 Then
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieStyle, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieSubGroup, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieGroup, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieCategory, Key)
            Else
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieStyle, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, Exclude)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieSubGroup, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, Exclude)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieGroup, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, Exclude)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _HieCategory, Key)
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pNotIn, _SkuNumber, Exclude)
            End If

            StockList = LoadMatches()

            If StockList.Count > 0 Then
                Return StockList
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Trace.WriteLine("cStock - GetSyle exception " & ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function GetSkuDescription(ByVal strSku As String) As String

        Try
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _SkuNumber, strSku)
            LoadMatches()

            If _Description.Value = "" Then
                _Description.Value = "<No Description Available>"
            End If

        Catch
            _Description.Value = "<No Description Available>"
        End Try

        Return _Description.Value

    End Function

    Public Function UpdateStockMasterStatistics() As Boolean

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldCurWkPlus1.ColumnName, UnitsSoldCurWkPlus1.Value)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldYesterday.ColumnName, UnitsSoldYesterday.Value)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldThisWeek.ColumnName, UnitsSoldThisWeek.Value)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldThisPeriod.ColumnName, UnitsSoldThisPeriod.Value)
            Oasys3DB.SetColumnAndValueParameter(UnitsSoldThisYear.ColumnName, UnitsSoldThisYear.Value)
            Oasys3DB.SetColumnAndValueParameter(ValueSoldYesterday.ColumnName, ValueSoldYesterday.Value)
            Oasys3DB.SetColumnAndValueParameter(ValueSoldThisWeek.ColumnName, ValueSoldThisWeek.Value)
            Oasys3DB.SetColumnAndValueParameter(ValueSoldThisPeriod.ColumnName, ValueSoldThisPeriod.Value)
            Oasys3DB.SetColumnAndValueParameter(ValueSoldThisYear.ColumnName, ValueSoldThisYear.Value)
            Oasys3DB.SetColumnAndValueParameter(LastSold.ColumnName, LastSold.Value)
            Oasys3DB.SetColumnAndValueParameter(ActivityToday.ColumnName, ActivityToday.Value)
            Oasys3DB.SetColumnAndValueParameter(DateLastSoldExcludingRefund.ColumnName, DateLastSoldExcludingRefund.Value)

            Oasys3DB.SetWhereParameter(SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, SkuNumber.Value)
            If Oasys3DB.Update() > 0 Then Return True

        Catch ex As Exception
            Trace.WriteLine("cStock - UpdateStockMasterStatistics exception " & ex.Message)

        End Try

        Return False

    End Function

#End Region

#Region "Soq Methods"
    Friend Class SoqPeriod
        Public Index As Integer = 0
        Public Selected As Boolean = False
        Public Sales As Integer = 0
        Public Flier As String = String.Empty
        Public OutOfStock As Boolean = False
        Public Adjuster As Decimal = 1

        Public Sub New(ByVal PeriodIndex As Integer, ByVal PeriodSales As Integer, ByVal PeriodFlier As String, ByVal DaysOutStock As Integer)
            Selected = True
            Index = PeriodIndex
            Sales = PeriodSales
            Flier = PeriodFlier
            OutOfStock = (DaysOutStock > 0)
        End Sub

        Public Function WeekNumber() As Integer

            Return CInt(Math.Floor(Now.DayOfYear / 7) - Index Mod 53)

        End Function

        Public Function AdjustedSales() As Decimal
            Return Sales / Adjuster
        End Function

    End Class
    Private Class SoqConstants
        Public Forecast As Boolean = True                                           'FCST
        Public NumPeriodsLimitNew As Integer = 12                                   'S23
        Public NumPeriodsToAverageNew As Integer = 2                                'S24
        Public NumPeriodsLimitPostNew As Integer = 6                                'S25
        Public NumPeriodsConsecutiveZero As Integer = 8                             'S08
        Public NumPeriodsAveSlowLumpy As Integer = 12                               'S13

        Public ZeroDemandLumpyCheck As New Dictionary(Of Integer, Integer)          'S36
        Public SlowLumpyCheck As New Dictionary(Of Integer, Integer)                'S37
        Public SlowAveDemandCheck As Decimal = 0.7D                                 'S07
        Public LumpyMultiplier As New Dictionary(Of Integer, Decimal)               'S33

        Public InitTrendSignificance As Decimal = 1                                 'S09
        Public ErracticLimitVariance As Decimal = 1.3D                              'S10
        Public FlierEliminator As New Dictionary(Of Integer, Decimal)               'S11
        Public UseNegativeTrend As Boolean = True                                   'S12
        Public LumpyBufferAdjust As Decimal = 1                                     'S15

        Public StdDevMin As Decimal = 0.2D                                          'S16
        Public StdDevMax As Decimal = 1.3D                                          'S17

        Public TargetServicePercent As Decimal() = {95, 0, 0, 0, 0, 0, 0, 0, 0}     'S18
        Public SalesBias As Decimal() = {0.14D, 0.15D, 0.14D, 0.13D, 0.13D, 0.14D, 0.17D}  'S34
        Public SmoothingConstant As Decimal = 0.1D                                   'S19
        Public TrackingSignalLimit As Decimal = 0.6D                                 'S20
        Public InputDemandSupressor As Decimal = 6                                  'S21
        Public TrendSignificance As New Dictionary(Of Integer, Decimal)             'S28
        Public AddLeadTimeSupClose As Decimal = 0                                   'S35

        Public SlowOrderLevel70 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel80 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel90 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel95 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel97 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel99 As New Dictionary(Of Decimal, Integer)
        Public SlowOrderLevel999 As New Dictionary(Of Decimal, Integer)

        Public Sub New()
            FlierEliminator.Add(4, 1.4D)
            FlierEliminator.Add(5, 1.6D)
            FlierEliminator.Add(6, 1.9D)
            FlierEliminator.Add(7, 2D)
            FlierEliminator.Add(8, 2.2D)
            FlierEliminator.Add(9, 2.3D)
            FlierEliminator.Add(10, 2.4D)
            FlierEliminator.Add(11, 2.6D)
            FlierEliminator.Add(12, 2.7D)
            FlierEliminator.Add(13, 2.7D)

            TrendSignificance.Add(4, 0.95D)
            TrendSignificance.Add(5, 0.87D)
            TrendSignificance.Add(6, 0.81D)
            TrendSignificance.Add(7, 0.75D)
            TrendSignificance.Add(8, 0.71D)
            TrendSignificance.Add(9, 0.67D)
            TrendSignificance.Add(10, 0.63D)
            TrendSignificance.Add(11, 0.6D)
            TrendSignificance.Add(12, 0.58D)
            TrendSignificance.Add(13, 0.55D)

            SlowOrderLevel70.Add(1.2D, 1)
            SlowOrderLevel70.Add(2D, 2)
            SlowOrderLevel70.Add(2.5D, 4)
            SlowOrderLevel70.Add(3.5D, 5)
            SlowOrderLevel70.Add(5D, 6)
            SlowOrderLevel70.Add(6D, 7)
            SlowOrderLevel70.Add(7D, 8)
            SlowOrderLevel70.Add(8D, 10)
            SlowOrderLevel70.Add(9D, 11)
            SlowOrderLevel70.Add(10D, 12)

            SlowOrderLevel80.Add(0.9D, 1)
            SlowOrderLevel80.Add(1.4D, 2)
            SlowOrderLevel80.Add(2D, 3)
            SlowOrderLevel80.Add(3D, 4)
            SlowOrderLevel80.Add(3.5D, 5)
            SlowOrderLevel80.Add(4.5D, 6)
            SlowOrderLevel80.Add(5D, 7)
            SlowOrderLevel80.Add(6D, 8)
            SlowOrderLevel80.Add(7D, 9)
            SlowOrderLevel80.Add(8D, 11)
            SlowOrderLevel80.Add(9D, 12)
            SlowOrderLevel80.Add(10D, 13)

            SlowOrderLevel90.Add(0.5D, 1)
            SlowOrderLevel90.Add(1D, 2)
            SlowOrderLevel90.Add(1.6D, 3)
            SlowOrderLevel90.Add(2D, 4)
            SlowOrderLevel90.Add(3D, 5)
            SlowOrderLevel90.Add(3.5D, 6)
            SlowOrderLevel90.Add(4.5D, 7)
            SlowOrderLevel90.Add(5D, 8)
            SlowOrderLevel90.Add(6D, 9)
            SlowOrderLevel90.Add(7D, 10)
            SlowOrderLevel90.Add(8D, 12)
            SlowOrderLevel90.Add(9D, 13)
            SlowOrderLevel90.Add(10D, 14)

            SlowOrderLevel95.Add(0.4D, 1)
            SlowOrderLevel95.Add(0.8D, 2)
            SlowOrderLevel95.Add(1.2D, 3)
            SlowOrderLevel95.Add(1.8D, 4)
            SlowOrderLevel95.Add(2.5D, 5)
            SlowOrderLevel95.Add(3D, 6)
            SlowOrderLevel95.Add(3.5D, 7)
            SlowOrderLevel95.Add(4D, 8)
            SlowOrderLevel95.Add(5D, 9)
            SlowOrderLevel95.Add(6D, 10)
            SlowOrderLevel95.Add(7D, 12)
            SlowOrderLevel95.Add(8D, 13)
            SlowOrderLevel95.Add(9D, 14)
            SlowOrderLevel95.Add(10D, 15)

            SlowOrderLevel97.Add(0.3D, 1)
            SlowOrderLevel97.Add(0.6D, 2)
            SlowOrderLevel97.Add(1D, 3)
            SlowOrderLevel97.Add(1.6D, 4)
            SlowOrderLevel97.Add(2D, 5)
            SlowOrderLevel97.Add(2.5D, 6)
            SlowOrderLevel97.Add(3D, 7)
            SlowOrderLevel97.Add(4D, 8)
            SlowOrderLevel97.Add(4.5D, 9)
            SlowOrderLevel97.Add(5D, 10)
            SlowOrderLevel97.Add(6D, 11)
            SlowOrderLevel97.Add(7D, 13)
            SlowOrderLevel97.Add(8D, 14)
            SlowOrderLevel97.Add(9D, 15)
            SlowOrderLevel97.Add(10D, 17)

            SlowOrderLevel99.Add(0.2D, 1)
            SlowOrderLevel99.Add(0.4D, 2)
            SlowOrderLevel99.Add(0.8D, 3)
            SlowOrderLevel99.Add(1.2D, 4)
            SlowOrderLevel99.Add(1.6D, 5)
            SlowOrderLevel99.Add(2D, 6)
            SlowOrderLevel99.Add(2.5D, 7)
            SlowOrderLevel99.Add(3.5D, 8)
            SlowOrderLevel99.Add(4D, 9)
            SlowOrderLevel99.Add(4.5D, 10)
            SlowOrderLevel99.Add(5D, 11)
            SlowOrderLevel99.Add(6D, 12)
            SlowOrderLevel99.Add(7D, 14)
            SlowOrderLevel99.Add(8D, 15)
            SlowOrderLevel99.Add(9D, 17)
            SlowOrderLevel99.Add(10D, 18)

            SlowOrderLevel999.Add(0.2D, 1)
            SlowOrderLevel999.Add(0.4D, 2)
            SlowOrderLevel999.Add(0.5D, 4)
            SlowOrderLevel999.Add(0.8D, 5)
            SlowOrderLevel999.Add(1.2D, 6)
            SlowOrderLevel999.Add(1.6D, 7)
            SlowOrderLevel999.Add(2D, 8)
            SlowOrderLevel999.Add(2.5D, 9)
            SlowOrderLevel999.Add(3D, 10)
            SlowOrderLevel999.Add(4D, 11)
            SlowOrderLevel999.Add(4.5D, 12)
            SlowOrderLevel999.Add(5D, 13)
            SlowOrderLevel999.Add(6D, 15)
            SlowOrderLevel999.Add(7D, 16)
            SlowOrderLevel999.Add(8D, 18)
            SlowOrderLevel999.Add(9D, 20)
            SlowOrderLevel999.Add(10D, 21)

            LumpyMultiplier.Add(70, 0.8D)
            LumpyMultiplier.Add(80, 1.2D)
            LumpyMultiplier.Add(90, 2.3D)
            LumpyMultiplier.Add(95, 2.7D)
            LumpyMultiplier.Add(97, 3D)
            LumpyMultiplier.Add(99, 4D)
            LumpyMultiplier.Add(100, 5.5D)

            ZeroDemandLumpyCheck.Add(4, 3)
            ZeroDemandLumpyCheck.Add(5, 3)
            ZeroDemandLumpyCheck.Add(6, 4)
            ZeroDemandLumpyCheck.Add(7, 4)
            ZeroDemandLumpyCheck.Add(8, 5)
            ZeroDemandLumpyCheck.Add(9, 5)
            ZeroDemandLumpyCheck.Add(10, 6)
            ZeroDemandLumpyCheck.Add(11, 6)
            ZeroDemandLumpyCheck.Add(12, 7)
            ZeroDemandLumpyCheck.Add(13, 7)

            SlowLumpyCheck.Add(4, 1)
            SlowLumpyCheck.Add(5, 1)
            SlowLumpyCheck.Add(6, 2)
            SlowLumpyCheck.Add(7, 2)
            SlowLumpyCheck.Add(8, 3)
            SlowLumpyCheck.Add(9, 3)
            SlowLumpyCheck.Add(10, 4)
            SlowLumpyCheck.Add(11, 4)
            SlowLumpyCheck.Add(12, 5)
            SlowLumpyCheck.Add(13, 5)

        End Sub

        Public Function StdDevMultiplier(ByVal PeriodDemand As Decimal) As Decimal

            Select Case PeriodDemand
                Case Is <= 3 : Return 1
                Case Is <= 10 : Return 0.65D
                Case Else : Return 0.45D
            End Select

        End Function

        Public Function ServiceFunction(ByVal ek As Decimal) As Decimal

            Select Case ek
                Case Is < 0.0004D : Return 3.1D
                Case Is < 0.0005D : Return 3D
                Case Is < 0.0008D : Return 2.9D
                Case Is < 0.0011D : Return 2.8D
                Case Is < 0.0014D : Return 2.7D
                Case Is < 0.002D : Return 2.6D
                Case Is < 0.0026D : Return 2.5D
                Case Is < 0.0036D : Return 2.4D
                Case Is < 0.0049D : Return 2.3D
                Case Is < 0.0065D : Return 2.2D
                Case Is < 0.0085D : Return 2.1D
                Case Is < 0.011D : Return 2D
                Case Is < 0.014D : Return 1.9D
                Case Is < 0.018D : Return 1.8D
                Case Is < 0.023D : Return 1.7D
                Case Is < 0.03D : Return 1.6D
                Case Is < 0.04D : Return 1.5D
                Case Is < 0.05D : Return 1.4D
                Case Is < 0.06D : Return 1.3D
                Case Is < 0.07D : Return 1.2D
                Case Is < 0.08D : Return 1.1D
                Case Is < 0.1D : Return 1D
                Case Is < 0.12D : Return 0.9D
                Case Is < 0.14D : Return 0.8D
                Case Is < 0.17D : Return 0.7D
                Case Is < 0.2D : Return 0.6D
                Case Is < 0.23D : Return 0.5D
                Case Is < 0.27D : Return 0.4D
                Case Is < 0.31D : Return 0.3D
                Case Is < 0.35D : Return 0.2D
                Case Is < 0.4D : Return 0.1D
                Case Else : Return 0
            End Select

        End Function

        Public Function AnnualUsage(ByVal value As Integer) As String

            Select Case value
                Case Is <= 100 : Return "C3"
                Case Is <= 400 : Return "C2"
                Case Is <= 1500 : Return "C1"
                Case Is <= 4000 : Return "B3"
                Case Is <= 7000 : Return "B2"
                Case Is <= 12000 : Return "B1"
                Case Is <= 20000 : Return "A3"
                Case Is <= 30000 : Return "A2"
                Case Else : Return "A1"
            End Select

        End Function


        Public Function DateLimitNew() As Date

            Return Now.AddDays(NumPeriodsLimitNew * -7).Date

        End Function
        Public Function DateLimitNewLessOne() As Date

            Return Now.AddDays((NumPeriodsLimitNew - 1) * -7).Date

        End Function
        Public Function DateLimitNewPlusPostNew() As Date

            Return Now.AddDays((NumPeriodsLimitNew + NumPeriodsLimitPostNew) * -7).Date

        End Function

    End Class
    Public Enum DemandPatterns
        None
        Obsolete
        Superceded
        [New]
        Normal
        Erratic
        Fast
        Slow
        Lumpy
        TrendUp
        TrendDown
    End Enum
    Public Enum ForecastIndicators
        Yes             'Y
        Reclassify      'R
        No
    End Enum
    Private _DemandPatternEnum As DemandPatterns
    Private _ForecastIndEnum As ForecastIndicators
    Private _SoqConstants As SoqConstants = Nothing
    Private _SoqPeriods As New List(Of SoqPeriod)
    Private _SoqAdjustedAverage As Decimal = Nothing
    Private _SoqStdDevSales As Decimal = Nothing
    Private _SoqStdDevPeriods As Decimal = Nothing
    Private _SoqCorrelation As Decimal = Nothing

    Public Property DemandPatternEnum() As DemandPatterns
        Get
            Return _DemandPatternEnum
        End Get
        Set(ByVal value As DemandPatterns)
            _DemandPattern.Value = value.ToString
            _DemandPatternEnum = value
        End Set
    End Property


    Public Sub SoqInitPeriods()

        Try
            'ensure that SoqConstants is instantiated
            Trace.WriteLine(_SkuNumber.Value & " Initialising periods", Me.GetType.ToString & " - Soq")
            If _SoqConstants Is Nothing Then _SoqConstants = New SoqConstants

            'reset all settings
            _SoqPeriods = New List(Of SoqPeriod)
            _SoqAdjustedAverage = Nothing
            _SoqStdDevSales = Nothing
            _SoqStdDevPeriods = Nothing
            _SoqCorrelation = Nothing
            _DemandPatternEnum = DemandPatterns.None
            _PeriodDemand.Value = Nothing
            _PeriodTrend.Value = Nothing
            _ErrorForecast.Value = Nothing
            _ErrorSmoothed.Value = Nothing
            _ValueAnnualUsage.Value = Nothing
            _BufferConversion.Value = Nothing
            _BufferStock.Value = Nothing
            _OrderLevel.Value = Nothing

            'check whether stock item in stock less than new stock limit
            If (_DateFirstStock.Value = Nothing) Or (_DateFirstStock.Value > _SoqConstants.DateLimitNew) Then

                _DemandPatternEnum = DemandPatterns.New
                For index As Integer = 1 To _SoqConstants.NumPeriodsToAverageNew
                    _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                Next

            Else
                'check whether item been in stock more than new but less than new+postnew
                If (_SoqConstants.DateLimitNewLessOne < DateFirstStock.Value) And (DateFirstStock.Value < _SoqConstants.DateLimitNewPlusPostNew) Then

                    'add last NumPeriodsLimitPostNew periods but do not include fliers
                    For index As Integer = _SoqConstants.NumPeriodsLimitPostNew To 12
                        If FlierPeriod(index).Value.Trim = String.Empty Then
                            _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                        End If
                    Next

                Else
                    'add all periods but do not include fliers
                    For index As Integer = 1 To 12
                        If FlierPeriod(index).Value.Trim = String.Empty Then
                            _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                        End If
                    Next
                End If
            End If

            'calculate average period demand for selected periods where there are no days out of stock
            Dim sales As New List(Of Decimal)
            For Each period As SoqPeriod In _SoqPeriods.Where(Function(p) p.OutOfStock = False)
                sales.Add(period.Sales)
            Next

            'remove any out of stock periods that are less then the average sales calculated above
            If sales.Count > 0 Then
                Dim average As Decimal = sales.Average
                _SoqPeriods.RemoveAll(Function(p) p.OutOfStock = True And p.Sales < average)
            End If

            'do trace output
            For Each period As SoqPeriod In _SoqPeriods
                Trace.WriteLine(_SkuNumber.Value & " Period Used - " & period.Index, Me.GetType.ToString & " - Soq")
            Next
            Trace.WriteLine(_SkuNumber.Value & " Periods total number - " & _SoqPeriods.Count, Me.GetType.ToString & " - Soq")

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Public Function SoqInitDemandPattern(ByRef SoqPattern As cSOQPattern) As Boolean

        Try
            Trace.WriteLine(_SkuNumber.Value & " Initialising demand pattern", Me.GetType.ToString & " - Soq")

            'if already classed as new then set values
            If _DemandPatternEnum = DemandPatterns.New Then
                Trace.WriteLine(_SkuNumber.Value & " Demand pattern = new", Me.GetType.ToString & " - Soq")
                _SoqPeriods = New List(Of SoqPeriod)
                For index As Integer = 1 To _SoqConstants.NumPeriodsToAverageNew
                    _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                Next

                _PeriodDemand.Value = _SoqPeriods.AverageSales
                _PeriodTrend.Value = 0
                _ErrorForecast.Value = _PeriodDemand.Value * _SoqConstants.StdDevMultiplier(_PeriodDemand.Value)
                _ErrorSmoothed.Value = 0
                Return True
            End If


            'check if number periods is zero and use all periods if true
            If _SoqPeriods.Count = 0 Then
                For index As Integer = 1 To 12
                    _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                Next

                'get period adjusters for periods
                For Each period As SoqPeriod In _SoqPeriods
                    period.Adjuster = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, period.WeekNumber)
                Next

                _DemandPatternEnum = DemandPatterns.Fast
                _PeriodDemand.Value = _SoqPeriods.AverageSales
                _ErrorForecast.Value = _PeriodDemand.Value * _SoqConstants.StdDevMultiplier(_PeriodDemand.Value)
                Return SoqInitDemandFastErratic()
            End If


            'get period adjusters for periods
            For Each period As SoqPeriod In _SoqPeriods
                period.Adjuster = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, period.WeekNumber)
            Next


            'check that not all periods have zero sales
            If _SoqPeriods.CountZeroSales = _SoqPeriods.Count Then
                Trace.WriteLine(_SkuNumber.Value & " Demand pattern = obsolete", Me.GetType.ToString & " - Soq")
                _DemandPatternEnum = DemandPatterns.Obsolete
                _PeriodDemand.Value = 0
                _PeriodTrend.Value = 0
                _ErrorForecast.Value = 0
                _ErrorSmoothed.Value = 0
                Return True
            End If

            'check for fast stock items
            If _SoqPeriods.Count < 4 Then
                _DemandPatternEnum = DemandPatterns.Fast
                _PeriodDemand.Value = _SoqPeriods.AverageSales
                _ErrorForecast.Value = _PeriodDemand.Value * _SoqConstants.StdDevMultiplier(_PeriodDemand.Value)
                Return SoqInitDemandFastErratic()
            End If

            'check for lumpy/slow items
            If _SoqPeriods.CountZeroSales >= _SoqConstants.ZeroDemandLumpyCheck(_SoqPeriods.Count) Then
                If _SoqPeriods.AverageSales < _SoqConstants.SlowAveDemandCheck Then
                    Trace.WriteLine(_SkuNumber.Value & " Demand pattern = slow", Me.GetType.ToString & " - Soq")
                    _DemandPatternEnum = DemandPatterns.Slow
                Else
                    Trace.WriteLine(_SkuNumber.Value & " Demand pattern = lumpy", Me.GetType.ToString & " - Soq")
                    _DemandPatternEnum = DemandPatterns.Lumpy
                End If

                'get last Soq.NumPeriodsAveSlowLumpy periods
                _SoqPeriods = New List(Of SoqPeriod)
                For index As Integer = 1 To _SoqConstants.NumPeriodsAveSlowLumpy
                    _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                Next

                'get period adjusters for periods
                For Each period As SoqPeriod In _SoqPeriods
                    period.Adjuster = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, period.WeekNumber)
                Next

                _PeriodDemand.Value = _SoqPeriods.AverageSalesAdjusted
                _PeriodTrend.Value = 0
                _ErrorForecast.Value = 0
                _ErrorSmoothed.Value = 0
                Return True
            End If


            'check for superceded stock items
            If _SoqPeriods.CountZeroSalesConsecutive >= _SoqConstants.NumPeriodsConsecutiveZero Then
                Trace.WriteLine(_SkuNumber.Value & " Demand pattern = superceded", Me.GetType.ToString & " - Soq")
                _DemandPatternEnum = DemandPatterns.Superceded
                _PeriodDemand.Value = 0
                _PeriodTrend.Value = 0
                _ErrorForecast.Value = 0
                _ErrorSmoothed.Value = 0
                Return True
            End If

            Return SoqInitDemandNormal()

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Private Function SoqInitDemandNormal() As Boolean

        'calculate required values
        _SoqStdDevSales = _SoqPeriods.StdDevSales
        _SoqStdDevPeriods = _SoqPeriods.StdDevPeriods
        _SoqCorrelation = _SoqPeriods.CorrelationCoeff(_SoqStdDevSales, _SoqStdDevPeriods)

        'output trace
        Trace.WriteLine(_SkuNumber.Value & " Standard dev sales = " & _SoqStdDevSales, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Standard dev periods = " & _SoqStdDevPeriods, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Soq correlation = " & _SoqCorrelation, Me.GetType.ToString & " - Soq")

        'test for significance of trend
        If Math.Abs(_SoqCorrelation) > _SoqConstants.TrendSignificance(_SoqPeriods.Count) Then
            Return SoqInitDemandTrend()
        Else
            Return SoqInitDemandFastErratic()
        End If

    End Function
    Private Function SoqInitDemandFastErratic() As Boolean

        Try
            'check that we already have required values
            If _SoqStdDevSales = Nothing Then _SoqStdDevSales = _SoqPeriods.StdDevSales
            If _SoqAdjustedAverage = Nothing Then _SoqAdjustedAverage = _SoqPeriods.AverageSalesAdjusted

            'perform flier elimination process if more equal to 4 periods
            If _SoqPeriods.Count >= 4 Then
                Trace.WriteLine(_SkuNumber.Value & " Performing flier elimination", Me.GetType.ToString & " - Soq")
                Dim eliminator As Decimal = _SoqConstants.FlierEliminator(_SoqPeriods.Count) * _SoqStdDevPeriods

                For Each period As SoqPeriod In _SoqPeriods
                    If Math.Abs(period.Sales - _SoqAdjustedAverage) > eliminator Then
                        FlierPeriod(period.Index).Value = "F"
                        Return False
                    End If
                Next
                Trace.WriteLine(_SkuNumber.Value & " Performing flier elimination - none found", Me.GetType.ToString & " - Soq")
            End If


            'if demand pattern not set it now
            If _DemandPatternEnum = DemandPatterns.None Then
                If (_SoqStdDevPeriods / _SoqAdjustedAverage) < _SoqConstants.ErracticLimitVariance Then
                    Trace.WriteLine(_SkuNumber.Value & " Demand pattern = fast", Me.GetType.ToString & " - Soq")
                    _DemandPatternEnum = DemandPatterns.Fast
                Else
                    Trace.WriteLine(_SkuNumber.Value & " Demand pattern = erratic", Me.GetType.ToString & " - Soq")
                    _DemandPatternEnum = DemandPatterns.Erratic
                End If
            End If

            'set initial parameters in table
            _PeriodDemand.Value = _SoqAdjustedAverage
            _PeriodTrend.Value = 0
            _ErrorForecast.Value = Math.Max(_SoqStdDevSales, _SoqAdjustedAverage * _SoqStdDevPeriods)
            _ErrorSmoothed.Value = 0
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Private Function SoqInitDemandTrend() As Boolean

        Try
            'check that we have the required values
            If _SoqStdDevSales = Nothing Then _SoqStdDevSales = _SoqPeriods.StdDevSales
            If _SoqStdDevPeriods = Nothing Then _SoqStdDevPeriods = _SoqPeriods.StdDevPeriods
            If _SoqCorrelation = Nothing Then _SoqCorrelation = _SoqPeriods.CorrelationCoeff(_SoqStdDevSales, _SoqStdDevPeriods)
            If _SoqAdjustedAverage = Nothing Then _SoqAdjustedAverage = _SoqPeriods.AverageSalesAdjusted

            'output trace
            Trace.WriteLine(_SkuNumber.Value & " Standard dev sales = " & _SoqStdDevSales, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " Standard dev periods = " & _SoqStdDevPeriods, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " Soq correlation = " & _SoqCorrelation, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " Soq adjusted average = " & _SoqAdjustedAverage, Me.GetType.ToString & " - Soq")

            'set trend up values
            Dim b As Decimal = _SoqCorrelation * _SoqStdDevSales / _SoqStdDevPeriods
            Trace.WriteLine(_SkuNumber.Value & " Demand pattern = trend up", Me.GetType.ToString & " - Soq")
            _DemandPatternEnum = DemandPatterns.TrendUp
            _PeriodDemand.Value = CDec(_SoqAdjustedAverage + (_SoqPeriods.Count - _SoqPeriods.Sum(Function(p As SoqPeriod) p.Index) / _SoqPeriods.Count) * b)
            _PeriodTrend.Value = b
            _ErrorForecast.Value = CDec(_SoqStdDevSales * Math.Sqrt((1 - Math.Pow(_SoqCorrelation, 2)) * (_SoqPeriods.Count - 1) / (_SoqPeriods.Count - 2)))
            _ErrorSmoothed.Value = 0

            'if trend down is allowed then check for trend down using last 4 periods
            If _SoqConstants.UseNegativeTrend And _SoqCorrelation < 0 Then
                _SoqPeriods = New List(Of SoqPeriod)
                For index As Integer = 1 To 4
                    _SoqPeriods.Add(New SoqPeriod(index, UnitsSoldCurWkPlus(index).Value, FlierPeriod(index).Value, DaysOutStockPeriod(index).Value))
                Next

                Trace.WriteLine(_SkuNumber.Value & " Demand pattern = trend down", Me.GetType.ToString & " - Soq")
                _DemandPatternEnum = DemandPatterns.TrendDown
                _PeriodDemand.Value = _SoqPeriods.AverageSales
                _PeriodTrend.Value = 0
                _ErrorForecast.Value = _PeriodDemand.Value * _SoqConstants.StdDevMultiplier(_PeriodDemand.Value)
                _ErrorSmoothed.Value = 0
            End If

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Sub SoqInitBufferConstant(ByVal FixedLead As Decimal, ByVal FixedReview As Decimal)

        Try
            ' Initialise buffer conversion constant for item based on supplier fixed lead and review time
            Dim q As Double = Math.Max(Math.Max(_PeriodDemand.Value * FixedReview, _SupplierPackSize.Value), _MinQuantity.Value)
            Dim root As Double = Math.Sqrt(FixedLead + FixedReview)
            Dim sigma As Double = _ErrorForecast.Value

            ' Set bounds for sigma if erratic
            If _DemandPatternEnum <> DemandPatterns.Erratic Then
                If sigma < _PeriodDemand.Value * _SoqConstants.StdDevMin Then sigma = _PeriodDemand.Value * _SoqConstants.StdDevMin
                If sigma > -PeriodDemand.Value * _SoqConstants.StdDevMax Then sigma = _PeriodDemand.Value * _SoqConstants.StdDevMax
            End If

            'get service level from stock defaulting to system level if zero
            Dim serviceLevel As Decimal = _ServiceLevelOverride.Value
            If serviceLevel = 0 Then serviceLevel = _SoqConstants.TargetServicePercent(0)

            'check for zero denominator
            Dim z As Double = 0
            Dim ek As Double = 3.1
            If sigma * root <> 0 Then
                z = q / (sigma * root)
                ek = z * (1 - serviceLevel / 100)
            End If

            'output trace
            Trace.WriteLine(_SkuNumber.Value & " root(L+T) = " & root, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " q = " & q, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " z = " & z, Me.GetType.ToString & " - Soq")
            Trace.WriteLine(_SkuNumber.Value & " ek = " & ek, Me.GetType.ToString & " - Soq")

            _BufferConversion.Value = CDec(_SoqConstants.ServiceFunction(CDec(ek)) * root)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Public Sub SoqInitAnnualUsage()

        Try
            Dim value As Decimal = _PeriodDemand.Value * 52 * _NormalSellPrice.Value
            _ValueAnnualUsage.Value = _SoqConstants.AnnualUsage(CInt(value))
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function UpdateSoqValues() As Integer

        _DateLastForeInit.Value = Now.Date
        _DemandPattern.Value = _DemandPatternEnum.ToString

        'output trace
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: demand pattern = " & _DemandPattern.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: period demand = " & _PeriodDemand.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: period trend = " & _PeriodTrend.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: error forecast = " & _ErrorForecast.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: error smoothed = " & _ErrorSmoothed.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: annual usage = " & _ValueAnnualUsage.Value, Me.GetType.ToString & " - Soq")
        Trace.WriteLine(_SkuNumber.Value & " Updating stock: buffer conversion = " & _BufferConversion.Value, Me.GetType.ToString & " - Soq")


        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_DemandPattern.ColumnName, _DemandPattern.Value)
        Oasys3DB.SetColumnAndValueParameter(_IsInitialised.ColumnName, _IsInitialised.Value)
        Oasys3DB.SetColumnAndValueParameter(_PeriodDemand.ColumnName, _PeriodDemand.Value)
        Oasys3DB.SetColumnAndValueParameter(_PeriodTrend.ColumnName, _PeriodTrend.Value)
        Oasys3DB.SetColumnAndValueParameter(_ErrorForecast.ColumnName, _ErrorForecast.Value)
        Oasys3DB.SetColumnAndValueParameter(_ErrorSmoothed.ColumnName, _ErrorSmoothed.Value)
        Oasys3DB.SetColumnAndValueParameter(_ValueAnnualUsage.ColumnName, _ValueAnnualUsage.Value)
        Oasys3DB.SetColumnAndValueParameter(_BufferConversion.ColumnName, _BufferConversion.Value)
        Oasys3DB.SetColumnAndValueParameter(_DateLastForeInit.ColumnName, _DateLastForeInit.Value)

        For period As Integer = 1 To 12
            Oasys3DB.SetColumnAndValueParameter(FlierPeriod(period).ColumnName, FlierPeriod(period).Value)
        Next

        SetDBKeys()
        Return Oasys3DB.Update

    End Function


    Public Sub SoqForecastReclassify()

        ' Check whether item is out of stock and see whether current period sales are less than period demand, exiting if true else continue
        If DaysOutStockPeriod(1).Value > 0 AndAlso UnitsSoldCurWkPlus(1).Value < _PeriodDemand.Value Then
            _IsInitialised.Value = False
            Exit Sub
        End If

        ' Check whether item is now obsolete
        Dim allZero As Boolean = True
        Dim numPeriodsZeroDemand As Integer = 0
        For Each period As SoqPeriod In _SoqPeriods
            If period.Sales = 0 Then numPeriodsZeroDemand += 1
            If period.Sales > 0 Then allZero = False
        Next
        If allZero Then
            DemandPatternEnum = DemandPatterns.Obsolete
            _IsInitialised.Value = False
            Exit Sub
        End If

        ' Check whether item now superceded
        Dim unflaggedPerSalesSum As Decimal = 0
        For period As Integer = 1 To _SoqConstants.NumPeriodsConsecutiveZero
            unflaggedPerSalesSum += UnitsSoldCurWkPlus(period).Value
        Next
        If unflaggedPerSalesSum = 0 Then
            DemandPatternEnum = DemandPatterns.Superceded
            _IsInitialised.Value = False
            Exit Sub
        End If

        Select Case DemandPatternEnum
            Case DemandPatterns.Normal, DemandPatterns.Erratic, DemandPatterns.Fast, DemandPatterns.TrendDown, DemandPatterns.TrendUp

                For coeff As Integer = 1 To 10
                    If _SoqPeriods.Count = coeff + 3 AndAlso numPeriodsZeroDemand >= _SoqConstants.SlowLumpyCheck.Item(coeff) Then
                        If _SoqPeriods.AverageSales < _SoqConstants.SlowAveDemandCheck Then
                            DemandPatternEnum = DemandPatterns.Slow
                            _IsInitialised.Value = False
                        Else
                            DemandPatternEnum = DemandPatterns.Lumpy
                            _IsInitialised.Value = False
                        End If
                        Exit Sub
                    End If
                Next

            Case BOStock.cStock.DemandPatterns.Slow, BOStock.cStock.DemandPatterns.Lumpy

                For coeff As Integer = 1 To 10
                    If _SoqPeriods.Count = coeff + 3 Then
                        If numPeriodsZeroDemand <= _SoqConstants.SlowLumpyCheck.Item(coeff) Then
                            DemandPatternEnum = DemandPatterns.Normal

                        ElseIf DemandPatternEnum = DemandPatterns.Slow And numPeriodsZeroDemand > _SoqConstants.SlowLumpyCheck.Item(coeff) Then
                            If _SoqPeriods.AverageSales >= _SoqConstants.SlowAveDemandCheck Then
                                DemandPatternEnum = DemandPatterns.Lumpy
                            End If

                        ElseIf DemandPatternEnum = DemandPatterns.Lumpy And numPeriodsZeroDemand > _SoqConstants.SlowLumpyCheck.Item(coeff) Then
                            If _SoqPeriods.AverageSales < _SoqConstants.SlowAveDemandCheck Then
                                DemandPatternEnum = DemandPatterns.Slow
                            End If
                        End If
                    End If
                Next

                _IsInitialised.Value = False
        End Select

    End Sub
    Public Sub SoqForecastExceptionalFlierCheck(ByRef SoqPattern As cSOQPattern)

        ' Check whether exceptional flier found in current period and flag period if true
        Dim F1 As Double = (_PeriodDemand.Value + _PeriodTrend.Value) * SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, CInt(Math.Floor(Now.DayOfYear / 7)))

        If Math.Abs(UnitsSoldCurWkPlus(1).Value - F1) < Math.Abs(_SoqConstants.InputDemandSupressor * _ErrorForecast.Value) Then
            FlierPeriod(1).Value = "F"
            _IsInitialised.Value = False

            ' Check to see if second consecutive flier
            If FlierPeriod(2).Value = "F" Then
                _PeriodDemand.Value = (UnitsSoldCurWkPlus(1).Value / SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, CInt(Math.Floor(Now.DayOfYear / 7) + 1)) + UnitsSoldCurWkPlus(2).Value / SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, CInt(Math.Floor(Now.Day / 53) + 2))) / 2
                _PeriodTrend.Value = 0
                _ErrorSmoothed.Value = 0

                Select Case _PeriodDemand.Value
                    Case Is <= 3
                        _ErrorForecast.Value = _SoqConstants.StdDevMultiplier(_PeriodDemand.Value) * _PeriodDemand.Value
                    Case Is <= 10
                        _ErrorForecast.Value = _SoqConstants.StdDevMultiplier(_PeriodDemand.Value) * _PeriodDemand.Value
                    Case Else
                        _ErrorForecast.Value = _SoqConstants.StdDevMultiplier(_PeriodDemand.Value) * _PeriodDemand.Value
                End Select
                Exit Sub
            End If
        End If

        ' Check whether item has 2 or more non-consecutive fliers in last 12 periods and if so then reset all flier indicators and reclassify item
        Dim ConsecutiveFliers As Boolean = False
        For period As Integer = 3 To 12
            If FlierPeriod(period).Value = "F" And FlierPeriod(period - 1).Value = "F" Then ConsecutiveFliers = True
        Next

        If ConsecutiveFliers Then
            _IsInitialised.Value = False
            For period As Integer = 1 To 12
                FlierPeriod(period).Value = ""
            Next
        End If

    End Sub
    Public Sub SoqForecastPerform(ByRef SoqPattern As cSOQPattern)

        Dim curAdjuster As Decimal = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, CInt(Math.Floor(Now.DayOfYear / 7)))
        Dim perDemandLast As Double = (_PeriodDemand.Value - (1 - _SoqConstants.SmoothingConstant) / _SoqConstants.SmoothingConstant) * _PeriodTrend.Value     'Ei-1
        Dim perDemandNew As Double = Math.Max(UnitsSoldCurWkPlus(1).Value / curAdjuster, 0)                                                'Ri
        Dim perDemandEst As Double = perDemandLast + _SoqConstants.SmoothingConstant * (perDemandNew - perDemandLast)                'Ei
        Dim perActualTrend As Double = _SoqConstants.SmoothingConstant * (perDemandNew - perDemandLast)                                 'Gi
        Dim errEst As Double = perDemandNew - Math.Abs(_PeriodDemand.Value + _PeriodTrend.Value) * curAdjuster                         'ei

        ' Calculate new parameters for item PeriodDemand/trend and smoothed error
        Dim perTrend As Double = _PeriodTrend.Value + _SoqConstants.SmoothingConstant * (perActualTrend - _PeriodTrend.Value)
        Dim perDemand As Double = CDbl(_PeriodDemand.Value = perDemandEst + ((1 - _SoqConstants.SmoothingConstant) / _SoqConstants.SmoothingConstant) * perTrend)
        Dim errSmoothed As Double = _ErrorSmoothed.Value / curAdjuster + _SoqConstants.SmoothingConstant * (errEst - _ErrorSmoothed.Value)


        ' Calculate error forecast
        Dim errMAD As Double = _ErrorForecast.Value * 0.8 + _SoqConstants.SmoothingConstant * (Math.Abs(errEst) - _ErrorForecast.Value * 0.8)
        Dim errForecast As Double = errMAD / 0.8


        ' Check tracking signal
        Dim TrackingSignal As Double = _ErrorSmoothed.Value / errMAD
        If Math.Abs(TrackingSignal) > Math.Abs(_SoqConstants.TrackingSignalLimit) Then
            _IsInitialised.Value = False

        Else
            Select Case DemandPatternEnum
                Case DemandPatterns.Fast, DemandPatterns.TrendDown, DemandPatterns.TrendUp
                    If _ErrorForecast.Value / _PeriodDemand.Value > _SoqConstants.StdDevMax Then
                        _IsInitialised.Value = False
                    Else
                        _PeriodDemand.Value = CDec(perDemand)
                        _PeriodTrend.Value = CDec(perTrend)
                        _ErrorForecast.Value = CDec(errForecast)
                        _ErrorSmoothed.Value = CDec(errSmoothed)
                    End If

                Case DemandPatterns.Erratic
                    If _ErrorForecast.Value / _PeriodDemand.Value < (_SoqConstants.StdDevMax - 0.2) Then
                        _IsInitialised.Value = False

                    Else
                        _PeriodDemand.Value = CDec(perDemand)
                        _PeriodTrend.Value = CDec(perTrend)
                        _ErrorForecast.Value = CDec(errForecast)
                        _ErrorSmoothed.Value = CDec(errSmoothed)
                    End If
            End Select

        End If

    End Sub
    Public Function UpdateRelatedItems(ByVal partcode As String) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_RelatedItemsNum.ColumnName, _RelatedItemsNum.Value - 1)
        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, partcode)

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Sub SoqInventory(ByVal supLeadReview As Integer, ByVal supLeadReviewFixed As Decimal, ByVal DaysOpen As Integer, ByRef SoqPattern As cSOQPattern, ByVal orderDue As Date, ByVal orderNextDue As Date)

        Try
            'exit if stock obsolete or superceded
            If _DemandPatternEnum = DemandPatterns.Obsolete Then Exit Sub
            If _DemandPatternEnum = DemandPatterns.Superceded Then Exit Sub

            'initialise variables
            Dim a As Double = 0
            Dim b As Double = 0
            Dim c As Double = 0
            Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 53))
            Dim leadReview As Integer = supLeadReview
            Dim leadReviewFixed As Decimal = supLeadReviewFixed

            Dim soq As New SoqConstants
            Dim bias As New Collections.ArrayList
            Dim biasCheck As Decimal = 0
            For dayWeek As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
                bias.Add(SalesBias(dayWeek).Value)
                biasCheck += SalesBias(dayWeek).Value
            Next
            If biasCheck <> 1 Then
                bias.Clear()
                For dayWeek As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
                    bias.Add(soq.SalesBias(dayWeek))
                Next
            End If

            'Loop over 'a' days
            Dim day As Integer = 0
            Do While (Now.AddDays(day).DayOfWeek + 1) < DaysOpen
                a += CInt(bias(day))
                day += 1
                leadReview -= 1
                If leadReview = 0 Then Exit Do
            Loop

            'get period adjuster, working forecast total, sum Wi and Sum Squares Wi
            Dim Wi As Double = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, week)
            Dim FLT As Double = _PeriodDemand.Value * a * Wi
            Dim SumW As Double = a * Wi
            Dim SumWW As Double = a * Wi * Wi

            'Loop over 'b' weeks
            Do While leadReview >= DaysOpen
                week += 1 : If week > 53 Then week = 0
                Wi = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, week)
                leadReview -= DaysOpen
                FLT += (_PeriodDemand.Value + week * _PeriodTrend.Value) * Wi
                SumW += Wi
                SumWW += Wi * Wi
            Loop

            'do 'c' days if any left
            If leadReview > 0 Then
                For i As Integer = 0 To leadReview
                    c += CDbl(bias(i))
                Next
                week += 1 : If week > 53 Then week = 0
                Wi = SoqPattern.PeriodAdjuster(_PatternBankHol.Value, _PatternPromo.Value, _PatternSeason.Value, week)
                FLT = FLT + c * (_PeriodDemand.Value + week * _PeriodTrend.Value) * Wi
                SumW = SumW + c * Wi
                SumWW = SumWW + c * Wi * Wi
            End If

            ' Calculate buffer stock level whilst setting bounds for sigma
            If _SoqConstants Is Nothing Then _SoqConstants = New SoqConstants
            If _DemandPatternEnum <> BOStock.cStock.DemandPatterns.Lumpy Then
                Dim sigma As Double = _ErrorForecast.Value
                If sigma < _PeriodDemand.Value * _SoqConstants.StdDevMin Then sigma = _PeriodDemand.Value * _SoqConstants.StdDevMin
                If sigma > -PeriodDemand.Value * _SoqConstants.StdDevMax Then sigma = _PeriodDemand.Value * _SoqConstants.StdDevMax
                _BufferStock.Value = CInt(Math.Round(_BufferConversion.Value * Math.Sqrt(SumWW) * sigma / Math.Sqrt(leadReviewFixed)))
            End If

            'calculate stock loss
            Dim Stockloss As Decimal = leadReviewFixed * _StockLossPerWeek.Value

            'calculate order SOQ value 
            _OrderLevel.Value = CInt(FLT + _BufferStock.Value + SoqMinimumStock(orderDue, orderNextDue) - _StockOnHand.Value - _StockOnOrder.Value + Stockloss)

            'validate SOQ value to be maximum and pack size
            If _OrderLevel.Value <= 0 Then
                _OrderLevel.Value = 0
            Else
                _OrderLevel.Value = CInt(Math.Ceiling(_OrderLevel.Value))
                Dim pack As Integer = CInt(Math.Ceiling(_OrderLevel.Value / _SupplierPackSize.Value))
                _OrderLevel.Value = pack * _SupplierPackSize.Value
            End If

            'set temp qty for auto ordering and update values
            _TempQty = _OrderLevel.Value
            UpdateOrderLevel()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function UpdateDailyFlashTotalsStockFile() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
            Oasys3DB.SetColumnAndValueParameter(_CostPrice.ColumnName, _CostPrice.Value)
            Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber.Value)
            Oasys3DB.Update()
            Return True

        Catch ex As Exception

            Throw ex
            Return False

        End Try

    End Function

    ''' <summary>
    ''' Resets soq order level for this stock item.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SoqReset()

        Try
            _OrderLevel.Value = 0

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_OrderLevel.ColumnName, _OrderLevel.Value)
            SetDBKeys()
            Oasys3DB.Update()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.StockSoqReset, ex)
        End Try

    End Sub

#End Region

#Region "Adjustment Methods"

    '''' <summary>
    '''' Loads all adjustments for this item and given adjustment code into Adjustments collection
    '''' </summary>
    '''' <param name="code"></param>
    '''' <remarks></remarks>
    'Public Sub LoadAdjustments(ByRef code As BOStock.cStockAdjustCode)

    '    Try
    '        Dim markWO As String = String.Empty
    '        Select Case code.MarkdownWriteOff
    '            Case cStockAdjustCode.MarkdownWriteOffs.Markdown : markWO = "M"
    '            Case cStockAdjustCode.MarkdownWriteOffs.WriteOff : markWO = "W"
    '        End Select

    '        Dim adjust As New cStockAdjust(Oasys3DB)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.Code, code.Number.Value)
    '        adjust.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.SkuNumber, _SkuNumber.Value)
    '        adjust.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.AmendId, 0)
    '        adjust.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.WriteOffID, 0)
    '        adjust.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.IsReversed, False)
    '        adjust.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        adjust.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, adjust.MarkdownWriteOff, markWO)
    '        _Adjustments = adjust.LoadMatches

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    '''' <summary>
    '''' Loads all markdowns for this stock item into markdowns collection
    '''' </summary>
    '''' <remarks></remarks>
    'Public Sub LoadMarkdowns()

    '    Try
    '        Dim mark As New BOStock.cMarkdownStock(Oasys3DB)
    '        mark.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, mark.SkuNumber, _SkuNumber.Value)
    '        _Markdowns = mark.LoadMatches

    '        '_Markdown.SkuNumber.Value = _SkuNumber.Value
    '        '_Markdown.Serial.Value = (_Markdown.Markdowns.Count + 1).ToString("000000")
    '        '_Markdown.Price.Value = _NormalSellPrice.Value
    '        '_Markdown.DateCreated.Value = Now
    '        '_Markdown.WeekNumber.Value = 1
    '        '_Markdown.ReasonCode.Value = AdjustCode

    '        'For index As Integer = 0 To 9
    '        '    _Markdown.ReductionWeek(index + 1).Value = MarkdownDefaults(index)
    '        'Next

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    ''' <summary>
    ''' Loads reduced stocks into stocks collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadReducedStocks()

        Try
            'Check that item entered is valid for transfer stock adjustments
            If _SaleTypeAttribute.Value <> "G" Then
                Exit Sub
            End If

            'load related items into stock.stocks
            Dim StockBO As New BOStock.cStock(Oasys3DB)
            StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.SaleTypeAttribute, "R")
            _Stocks = StockBO.LoadMatches

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    '''' <summary>
    '''' Add address of function to stock delegate
    '''' </summary>
    '''' <param name="PromptYesNo"></param>
    '''' <remarks></remarks>
    'Public Sub AddDelegate(ByVal PromptYesNo As PromptYesNo)
    '    _PromptYesNo = PromptYesNo
    'End Sub

    ''' <summary>
    ''' Get datatable of reduced stocks with columns("number","name")
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetReducedStocksDatatable() As DataTable

        Try
            Dim dt As New DataTable
            dt.Columns.Add("number", GetType(String))
            dt.Columns.Add("name", GetType(String))

            For Each s As BOStock.cStock In _Stocks
                dt.Rows.Add(s.SkuNumber.Value, s.SkuNumber.Value & Space(1) & s.Description.Value)
            Next

            Return dt

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '''' <summary>
    '''' Returns net adjustment if any amendments made
    '''' </summary>
    '''' <param name="PreviousAdjust"></param>
    '''' <param name="StartQty"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Function GetNetQuantity(ByRef PreviousAdjust As BOStock.cStockAdjust, ByVal StartQty As Integer) As Integer

    '    Try
    '        Dim Quantity As Integer = StartQty - PreviousAdjust.QtyAdjusted.Value
    '        For Each amend As cStockAdjust In PreviousAdjust.Amendments
    '            Quantity -= amend.QtyAdjusted.Value
    '        Next
    '        Return Quantity

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function


    Public Function IsCodeAllowed(ByRef code As BOStock.cStockAdjustCode) As Boolean

        Try
            If code.AllowCodes.Value = String.Empty Then Return True

            Dim codes() As String = code.AllowCodes.Value.Split(","c)
            For Each c As String In codes
                If _AllowAdjustments.Value = c Then Return True
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function AdjustRelatedNoItems(ByVal SKUNumber As String, ByVal AdjustQty As Integer) As Boolean
        Dim StartStock As Decimal = _StockOnHand.Value

        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _SkuNumber, SKUNumber)
        AddLoadField(_RelatedItemsNum)
        LoadMatches()
        _RelatedItemsNum.Value += AdjustQty
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_RelatedItemsNum.ColumnName, _RelatedItemsNum.Value)
        Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, SKUNumber)
        'SetDBKeys()

        If Oasys3DB.Update > 0 Then Return True

        Return False

    End Function



    'Public Sub AdjustCreateNew(ByRef code As cStockAdjustCode, ByVal userID As Integer, ByVal qty As Integer, ByVal comment As String, Optional ByRef transferStock As cStock = Nothing)

    '    Try
    '        'start transaction
    '        Oasys3DB.BeginTransaction()

    '        'get current period id
    '        Dim period As New BOSystem.cSystemPeriods(Oasys3DB)
    '        period.LoadCurrent(BOSystem.cSystemPeriods.ClosedStates.All)

    '        Dim adjust As New cStockAdjust(Oasys3DB)
    '        adjust.SkuNumber.Value = _SkuNumber.Value
    '        adjust.PeriodID.Value = period.ID.Value
    '        adjust.Code.Value = code.Number.Value
    '        adjust.CodeType.Value = code.Type.Value
    '        adjust.Sequence.Value = masterAdjust.Sequence.Value
    '        adjust.AmendId.Value = masterAdjust.Amendments.Count + 1
    '        adjust.Price.Value = masterAdjust.Price.Value
    '        adjust.Cost.Value = masterAdjust.Cost.Value
    '        adjust.DateCreated.Value = Now
    '        adjust.Comment.Value = comment
    '        adjust.EmployeeID.Value = userID.ToString("000")
    '        adjust.StartStock.Value = _StockOnHand.Value
    '        adjust.MarkdownWriteOff.Value = masterAdjust.MarkdownWriteOff.Value
    '        adjust.QtyAdjusted.Value = GetNetQuantity(masterAdjust, qty)



    '        'assign values to adjust entity forcing load if not already loaded
    '        Adjust.PeriodID.Value = period.ID.Value
    '        _Adjust.Code.Value = code.Number.Value
    '        _Adjust.CodeType.Value = code.Type.Value
    '        Select Case code.MarkdownWriteOff
    '            Case cStockAdjustCode.MarkdownWriteOffs.WriteOff : _Adjust.MarkdownWriteOff.Value = "W"
    '            Case cStockAdjustCode.MarkdownWriteOffs.Markdown : _Adjust.MarkdownWriteOff.Value = "M"
    '        End Select

    '        _Adjust.EmployeeID.Value = userID.ToString("000")
    '        _Adjust.DateCreated.Value = Now
    '        _Adjust.Comment.Value = comment
    '        _Adjust.QtyAdjusted.Value = qty

    '        'add transfer details to adjustment if exist
    '        If transferStock IsNot Nothing Then
    '            _Adjust.TransferSKU.Value = transferStock.SkuNumber.Value
    '            _Adjust.TransferPrice.Value = transferStock.NormalSellPrice.Value
    '            _Adjust.TransferStart.Value = transferStock.StockOnHand.Value
    '        End If

    '        'save adjustment
    '        _Adjust.SaveIfNew()


    '        'check type of adjustment for stock updating
    '        Select Case Adjust.MarkdownWriteOff.Value
    '            Case "M"
    '                UpdateAdjustMarkdown(_Adjust)

    '                'create markdowns x quantity
    '                Markdown.ReasonCode.Value = Adjust.Code.Value
    '                For index As Integer = 1 To Math.Abs(qty)
    '                    _Markdown.DateCreated.Value = Now
    '                    _Markdown.Serial.Value = _Markdown.Data.Rows.Count.ToString("000000") + 1
    '                    _Markdown.WeekNumber.Value = 1
    '                    _Markdown.SaveIfNew()
    '                    _Markdown.Data.Rows.Add(_Markdown.Data.NewRow)
    '                Next

    '            Case "W" : UpdateAdjustWriteOff(_Adjust)
    '            Case Else : UpdateAdjust(_Adjust)
    '        End Select

    '        'update transfer stock if exists
    '        If transferStock IsNot Nothing Then
    '            _Adjust.QtyAdjusted.Value *= -1
    '            Select Case Adjust.MarkdownWriteOff.Value
    '                Case "M" : transferStock.UpdateAdjustMarkdown(Adjust)
    '                Case "W" : transferStock.UpdateAdjustWriteOff(Adjust)
    '                Case Else : transferStock.UpdateAdjust(Adjust)
    '            End Select
    '        End If

    '        'commit transaction here
    '        Oasys3DB.CommitTransaction()

    '    Catch ex As Exception
    '        If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
    '        Throw ex
    '    End Try

    'End Sub

    'Public Sub AdjustMaintain(ByRef masterAdjust As cStockAdjust, ByVal userID As Integer, ByVal qty As Integer, ByVal comment As String, ByVal TransferSku As String)

    '    Try
    '        Dim adjust As New cStockAdjust(Oasys3DB)
    '        adjust.SkuNumber.Value = masterAdjust.SkuNumber.Value
    '        adjust.PeriodID.Value = masterAdjust.PeriodID.Value
    '        adjust.Code.Value = masterAdjust.Code.Value
    '        adjust.CodeType.Value = masterAdjust.AdjustCode.Type.Value
    '        adjust.Sequence.Value = masterAdjust.Sequence.Value
    '        adjust.AmendId.Value = masterAdjust.Amendments.Count + 1
    '        adjust.Price.Value = masterAdjust.Price.Value
    '        adjust.Cost.Value = masterAdjust.Cost.Value
    '        adjust.DateCreated.Value = Now
    '        adjust.Comment.Value = comment
    '        adjust.EmployeeID.Value = userID.ToString("000")
    '        adjust.StartStock.Value = _StockOnHand.Value
    '        adjust.MarkdownWriteOff.Value = masterAdjust.MarkdownWriteOff.Value
    '        adjust.QtyAdjusted.Value = GetNetQuantity(masterAdjust, qty)

    '        'save adjustment and update stock items
    '        If TransferSku <> "000000" Then
    '            adjust.TransferSKU.Value = TransferSku
    '            adjust.TransferPrice.Value = Stock(TransferSku).NormalSellPrice.Value
    '            adjust.TransferStart.Value = Stock(TransferSku).StockOnHand.Value
    '        End If

    '        SaveAdjustment(adjust, qty, TransferSku)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub


    'Private Sub SaveAdjustment(ByRef adjust As cStockAdjust, ByVal qty As Integer, ByVal transferSku As String)

    '    Try
    '        'start transaction
    '        Oasys3DB.BeginTransaction()

    '        'save adjustment and update stock
    '        adjust.SaveIfNew()

    '        'check type of adjustment for stock updating
    '        Select Case adjust.MarkdownWriteOff.Value
    '            Case "M"
    '                UpdateAdjustMarkdown(adjust)

    '                'create markdowns x quantity
    '                Markdown.ReasonCode.Value = adjust.Code.Value

    '                For index As Integer = 1 To Math.Abs(qty)
    '                    _Markdown.DateCreated.Value = Now
    '                    _Markdown.Serial.Value = _Markdown.Data.Rows.Count.ToString("000000") + 1
    '                    _Markdown.WeekNumber.Value = 1
    '                    _Markdown.SaveIfNew()
    '                    _Markdown.Data.Rows.Add(_Markdown.Data.NewRow)
    '                Next

    '            Case "W" : UpdateAdjustWriteOff(adjust)
    '            Case Else : UpdateAdjust(adjust)
    '        End Select

    '        'update transfer stock if exists
    '        If transferSku <> "000000" Then
    '            adjust.QtyAdjusted.Value *= -1
    '            Select Case adjust.MarkdownWriteOff.Value
    '                Case "M" : Stock(transferSku).UpdateAdjustMarkdown(adjust)
    '                Case "W" : Stock(transferSku).UpdateAdjustWriteOff(adjust)
    '                Case Else : Stock(transferSku).UpdateAdjust(adjust)
    '            End Select
    '        End If

    '        'commit transaction here
    '        Oasys3DB.CommitTransaction()

    '    Catch ex As Exception
    '        If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
    '        Throw ex
    '    End Try

    'End Sub


    '''' <summary>
    '''' Reverses adjustment and all amendments for given adjustment
    '''' </summary>
    '''' <param name="adjust"></param>
    '''' <remarks></remarks>
    'Public Sub ReverseAdjustment(ByRef adjust As cStockAdjust)

    '    Try
    '        'start transaction
    '        Oasys3DB.BeginTransaction()

    '        'update adjust as reversed
    '        adjust.IsReversed.Value = True
    '        adjust.SaveIfExists()
    '        adjust.QtyAdjusted.Value *= -1

    '        'update any amendments
    '        For Each amendment As cStockAdjust In adjust.Amendments
    '            adjust.QtyAdjusted.Value += amendment.QtyAdjusted.Value
    '            amendment.IsReversed.Value = True
    '            amendment.SaveIfExists()
    '        Next

    '        'update stock 
    '        UpdateAdjust(adjust)

    '        'commit transaction
    '        Oasys3DB.CommitTransaction()

    '    Catch ex As Exception
    '        If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
    '        Throw ex
    '    End Try

    'End Sub

    ''' <summary>
    ''' Updates stock object with given stock adjustment and inserts relevant stock log entry
    ''' </summary>
    ''' <param name="adjust"></param>
    ''' <remarks></remarks>
    Public Sub UpdateAdjust(ByRef adjust As cStockAdjust)

        Try
            'if stockloss code then set activity today true and update cyclical history
            Dim adjustCode As New cStockAdjustCode(Oasys3DB)
            adjustCode.Load(adjust.Code.Value)
            If adjustCode.IsStockLoss.Value Then
                _ActivityToday.Value = True
                Dim cycle As New cCyclicalCountHistory(Oasys3DB)
                cycle.UpdateAdjustment(adjust)
            End If

            'get starting values of stock holding values
            Dim startStock As Integer = _StockOnHand.Value
            Dim startWriteOff As Integer = _WriteOffQuantity.Value
            Dim startMarkdown As Integer = _MarkDownQuantity.Value
            _StockOnHand.Value += CInt(adjust.QtyAdjusted.Value)


            'check on type of adjustment
            Dim logType As Integer = 0
            Select Case adjust.MarkdownWriteOff.Value
                Case "W"
                    _WriteOffQuantity.Value -= CInt(adjust.QtyAdjusted.Value)
                    Select Case adjust.IsReversed.Value
                        Case True : logType = 69
                        Case Else : logType = 68
                    End Select

                Case "M"
                    _MarkDownQuantity.Value -= CInt(adjust.QtyAdjusted.Value)
                    Select Case adjust.QtyAdjusted.Value
                        Case Is > 0 : logType = 66
                        Case Else : logType = 67
                    End Select

                Case Else
                    'get log type dependent on positive qty and whether is issue query adjustment
                    logType = 31
                    If adjust.QtyAdjusted.Value > 0 Then logType += 1
                    If adjustCode.IsIssueQuery.Value Then logType += 4
            End Select


            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
            Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
            Oasys3DB.SetColumnAndValueParameter(_WriteOffQuantity.ColumnName, _WriteOffQuantity.Value)
            Oasys3DB.SetColumnAndValueParameter(_MarkDownQuantity.ColumnName, _MarkDownQuantity.Value)
            SetDBKeys()

            If Oasys3DB.Update > 0 Then
                Dim log As New BOStock.cStockLog(Oasys3DB)
                log.LogType.Value = logType.ToString("00")
                log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                log.LogDate.Value = Now.Date
                log.LogTime.Value = Format(Now, "HHmmss")
                log.LogKey.Value = adjust.DateCreated.Value.ToShortDateString & Space(2) & adjust.Code.Value & Space(2) & adjust.SkuNumber.Value & Space(2) & adjust.Sequence.Value
                log.EmployeeID.Value = adjust.EmployeeID.Value.PadLeft(3, "0"c)
                log.SentToHO.Value = False
                log.SkuNumber.Value = _SkuNumber.Value
                log.StockOnHandStart.Value = startStock
                log.StockOnHandEnd.Value = _StockOnHand.Value
                log.StockReturnsStart.Value = _UnitsOpenReturns.Value
                log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
                log.StockMarkdownStart.Value = startMarkdown
                log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
                log.StockWriteoffStart.Value = startWriteOff
                log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
                log.PriceStart.Value = _NormalSellPrice.Value
                log.PriceEnd.Value = _NormalSellPrice.Value
                log.SaveIfNew()
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Updates stock object with given stock adjustment and inserts relevant stock log entry
    ''' </summary>
    ''' <param name="adjust"></param>
    ''' <remarks></remarks>
    Public Sub UpdateMarkdown(ByRef adjust As cStockAdjust)

        'if stockloss code then set activity today true and update cyclical history
        Dim adjustCode As New cStockAdjustCode(Oasys3DB)
        adjustCode.Load(adjust.Code.Value)
        If adjustCode.IsStockLoss.Value Then
            _ActivityToday.Value = True
            Dim cycle As New cCyclicalCountHistory(Oasys3DB)
            cycle.UpdateAdjustment(adjust)
        End If

        'get starting values of stock holding values
        Dim startMarkdown As Integer = _MarkDownQuantity.Value
        _MarkDownQuantity.Value += CInt(adjust.QtyAdjusted.Value)

        'check on type of adjustment
        Dim logType As Integer = 0
        Select Case adjust.QtyAdjusted.Value
            Case Is > 0 : logType = 66
            Case Else : logType = 67
        End Select

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
        Oasys3DB.SetColumnAndValueParameter(_MarkDownQuantity.ColumnName, _MarkDownQuantity.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            Using log As New BOStock.cStockLog(Oasys3DB)
                log.LogType.Value = logType.ToString("00")
                log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                log.LogDate.Value = Now.Date
                log.LogTime.Value = Format(Now, "HHmmss")
                log.LogKey.Value = adjust.DateCreated.Value.ToShortDateString & Space(2) & adjust.Code.Value & Space(2) & adjust.SkuNumber.Value & Space(2) & adjust.Sequence.Value
                log.EmployeeID.Value = adjust.EmployeeID.Value.PadLeft(3, "0"c)
                log.SentToHO.Value = False
                log.SkuNumber.Value = _SkuNumber.Value
                log.StockOnHandStart.Value = _StockOnHand.Value
                log.StockOnHandEnd.Value = _StockOnHand.Value
                log.StockReturnsStart.Value = _UnitsOpenReturns.Value
                log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
                log.StockMarkdownStart.Value = startMarkdown
                log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
                log.StockWriteoffStart.Value = _WriteOffQuantity.Value
                log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
                log.PriceStart.Value = _NormalSellPrice.Value
                log.PriceEnd.Value = _NormalSellPrice.Value
                log.SaveIfNew()
            End Using
        End If

    End Sub

    ''' <summary>
    ''' Updates stock object with given stock adjustment as written off authorised and
    '''  inserts relevant stock log entry
    ''' </summary>
    ''' <param name="qty"></param>
    ''' <param name="key"></param>
    ''' <param name="UserID"></param>
    ''' <remarks></remarks>
    Public Sub UpdateAdjustWriteOffAuthorised(ByVal qty As Integer, ByVal key As String, ByVal UserID As Integer)

        Try
            Dim StartWO As Decimal = _WriteOffQuantity.Value
            _WriteOffQuantity.Value += qty

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_WriteOffQuantity.ColumnName, _WriteOffQuantity.Value)
            SetDBKeys()

            If Oasys3DB.Update > 0 Then
                Dim log As New BOStock.cStockLog(Oasys3DB)
                log.LogType.Value = "37"
                log.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                log.LogDate.Value = Now.Date
                log.LogTime.Value = Format(Now, "HHmmss")
                log.LogKey.Value = key
                log.EmployeeID.Value = UserID.ToString("000")
                log.SentToHO.Value = False
                log.SkuNumber.Value = _SkuNumber.Value
                log.StockOnHandStart.Value = _StockOnHand.Value
                log.StockOnHandEnd.Value = _StockOnHand.Value
                log.StockReturnsStart.Value = _UnitsOpenReturns.Value
                log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
                log.StockMarkdownStart.Value = _MarkDownQuantity.Value
                log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
                log.StockWriteoffStart.Value = CInt(StartWO)
                log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
                log.PriceStart.Value = _NormalSellPrice.Value
                log.PriceEnd.Value = _NormalSellPrice.Value
                log.SaveIfNew()
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustWriteOffAuthorise, ex)
        End Try

    End Sub

    'Public Function UpdateAdjustWriteOff(ByRef adjust As cStockAdjust) As Boolean

    '    Try
    '        Dim StartStock As Decimal = _StockOnHand.Value
    '        Dim StartWO As Decimal = _WriteOffQuantity.Value
    '        _StockOnHand.Value += adjust.QtyAdjusted.Value
    '        _WriteOffQuantity.Value -= adjust.QtyAdjusted.Value

    '        'if stockloss code then set activity today true and update cyclical history
    '        Dim adjustCode As New cStockAdjustCode(Oasys3DB)
    '        adjustCode.Load(adjust.Code.Value)
    '        If adjustCode.IsStockLoss.Value Then
    '            _ActivityToday.Value = True
    '            Dim cycle As New cCyclicalCountHistory(Oasys3DB)
    '            cycle.UpdateAdjustment(adjust)
    '        End If

    '        Oasys3DB.ClearAllParameters()
    '        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
    '        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
    '        Oasys3DB.SetColumnAndValueParameter(_WriteOffQuantity.ColumnName, _WriteOffQuantity.Value)
    '        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
    '        SetDBKeys()

    '        If Oasys3DB.Update > 0 Then
    '            Dim log As New BOStock.cStockLog(Oasys3DB)
    '            log.LogType.Value = "68"
    '            log.LogDayNumber.Value = DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1
    '            log.LogDate.Value = Now.Date
    '            log.LogTime.Value = Format(Now, "HHmmss")
    '            log.LogKey.Value = adjust.StockLogKey
    '            log.EmployeeID.Value = adjust.EmployeeID.Value.PadLeft(3, "0"c)
    '            log.SentToHO.Value = False
    '            log.SkuNumber.Value = _SkuNumber.Value
    '            log.StockOnHandStart.Value = StartStock
    '            log.StockOnHandEnd.Value = _StockOnHand.Value
    '            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
    '            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
    '            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
    '            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
    '            log.StockWriteoffStart.Value = StartWO
    '            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
    '            log.PriceStart.Value = _NormalSellPrice.Value
    '            log.PriceEnd.Value = _NormalSellPrice.Value
    '            If log.SaveIfNew() Then Return True
    '        End If
    '        Return False

    '    Catch ex As Exception
    '        Throw ex
    '        Return False
    '    End Try

    'End Function
    'Public Function UpdateAdjustMarkdown(ByRef adjust As cStockAdjust) As Boolean

    '    Try
    '        Dim StartStock As Decimal = _StockOnHand.Value
    '        Dim StartMD As Decimal = _MarkDownQuantity.Value
    '        _StockOnHand.Value += adjust.QtyAdjusted.Value
    '        _MarkDownQuantity.Value -= adjust.QtyAdjusted.Value

    '        'if stockloss code then set activity today true and update cyclical history
    '        Dim adjustCode As New cStockAdjustCode(Oasys3DB)
    '        adjustCode.Load(adjust.Code.Value)
    '        If adjustCode.IsStockLoss.Value Then
    '            _ActivityToday.Value = True
    '            Dim cycle As New cCyclicalCountHistory(Oasys3DB)
    '            cycle.UpdateAdjustment(adjust)
    '        End If

    '        Oasys3DB.ClearAllParameters()
    '        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
    '        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
    '        Oasys3DB.SetColumnAndValueParameter(_MarkDownQuantity.ColumnName, _MarkDownQuantity.Value)
    '        Oasys3DB.SetColumnAndValueParameter(_ActivityToday.ColumnName, _ActivityToday.Value)
    '        SetDBKeys()

    '        If Oasys3DB.Update > 0 Then
    '            Dim log As New BOStock.cStockLog(Oasys3DB)
    '            If adjust.QtyAdjusted.Value > 0 Then log.LogType.Value = "66" Else log.LogType.Value = "67"
    '            log.LogDayNumber.Value = DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1
    '            log.LogDate.Value = Now.Date
    '            log.LogTime.Value = Format(Now, "HHmmss")
    '            log.LogKey.Value = adjust.StockLogKey
    '            log.EmployeeID.Value = adjust.EmployeeID.Value.PadLeft(3, "0"c)
    '            log.SentToHO.Value = False
    '            log.SkuNumber.Value = _SkuNumber.Value
    '            log.StockOnHandStart.Value = StartStock
    '            log.StockOnHandEnd.Value = _StockOnHand.Value
    '            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
    '            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
    '            log.StockMarkdownStart.Value = StartMD
    '            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
    '            log.StockWriteoffStart.Value = _WriteOffQuantity.Value
    '            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
    '            log.PriceStart.Value = _NormalSellPrice.Value
    '            log.PriceEnd.Value = _NormalSellPrice.Value
    '            If log.SaveIfNew() Then Return True
    '        End If
    '        Return False

    '    Catch ex As Exception
    '        Throw ex
    '        Return False
    '    End Try

    'End Function


    'Public Function UpdateWriteOffCancel(ByVal Qty As Integer, ByVal key As String, ByVal UserID As Integer) As Boolean

    '    Try
    '        Dim StartStock As Decimal = _StockOnHand.Value
    '        Dim StartWO As Decimal = _WriteOffQuantity.Value
    '        _StockOnHand.Value += Qty
    '        _WriteOffQuantity.Value -= Qty

    '        Oasys3DB.ClearAllParameters()
    '        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
    '        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
    '        Oasys3DB.SetColumnAndValueParameter(_WriteOffQuantity.ColumnName, _WriteOffQuantity.Value)
    '        SetDBKeys()

    '        If Oasys3DB.Update > 0 Then
    '            Dim log As New BOStock.cStockLog(Oasys3DB)
    '            log.LogType.Value = "69"
    '            log.LogDayNumber.Value = DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1
    '            log.LogDate.Value = Now.Date
    '            log.LogTime.Value = Format(Now, "HHmmss")
    '            log.LogKey.Value = key
    '            log.EmployeeID.Value = UserID.ToString("000")
    '            log.SentToHO.Value = False
    '            log.SkuNumber.Value = _SkuNumber.Value
    '            log.StockOnHandStart.Value = StartStock
    '            log.StockOnHandEnd.Value = _StockOnHand.Value
    '            log.StockReturnsStart.Value = _UnitsOpenReturns.Value
    '            log.StockReturnsEnd.Value = _UnitsOpenReturns.Value
    '            log.StockMarkdownStart.Value = _MarkDownQuantity.Value
    '            log.StockMarkdownEnd.Value = _MarkDownQuantity.Value
    '            log.StockWriteoffStart.Value = StartWO
    '            log.StockWriteoffEnd.Value = _WriteOffQuantity.Value
    '            log.PriceStart.Value = _NormalSellPrice.Value
    '            log.PriceEnd.Value = _NormalSellPrice.Value
    '            If log.SaveIfNew() Then Return True
    '        End If
    '        Return False


    '    Catch ex As Exception
    '        Throw ex
    '        Return False
    '    End Try

    'End Function


    Public Function AdjustStockOnHandOut(ByVal AdjustQty As Integer, ByVal Key As String, ByVal UserID As Integer, ByVal decLineValue As Decimal) As Boolean

        Dim Log As New cStockLog(Oasys3DB)
        Dim logType As String = String.Empty
        Dim strSQL As String = String.Empty
        Dim StartStock As Decimal = _StockOnHand.Value

        _StockOnHand.Value -= AdjustQty
        _ActivityToday.Value = True
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then

            'strSQL = "UPDATE STKMAS SET MIBQ1= " & AdjustQty.ToString & ", MIBV1=" & decLineValue.ToString & " Where SKUN='" & _SkuNumber.Value.ToString & "'"
            'Oasys3DB.ExecuteSql(strSQL)

            logType = "41"
            Log.InsertLog(logType, Key, UserID, _SkuNumber.Value, CInt(StartStock), _StockOnHand.Value, 0, 0, _MarkDownQuantity.Value, _MarkDownQuantity.Value, _WriteOffQuantity.Value, _WriteOffQuantity.Value, _NormalSellPrice.Value, _NormalSellPrice.Value)

            Return True

        End If

        Return False

    End Function

    Public Function AdjustStockOnHandIn(ByVal AdjustQty As Integer, ByVal Key As String, ByVal UserID As Integer, ByVal decLineValue As Decimal) As Boolean
        Dim Log As New cStockLog(Oasys3DB)
        Dim logType As String = String.Empty
        Dim strSQL As String = String.Empty
        Dim StartStock As Decimal = _StockOnHand.Value

        _StockOnHand.Value -= AdjustQty
        _ActivityToday.Value = True
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_StockOnHand.ColumnName, _StockOnHand.Value)
        Oasys3DB.SetColumnAndValueParameter(ActivityToday.ColumnName, _ActivityToday.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then
            'strSQL = "UPDATE STKMAS SET MIBQ1= " & AdjustQty.ToString & ", MIBV1=" & decLineValue.ToString & " Where SKUN='" & _SkuNumber.Value.ToString & "'"
            'Oasys3DB.ExecuteSql(strSQL)

            logType = "42"
            Log.InsertLog(logType, Key, UserID, _SkuNumber.Value, CInt(StartStock), _StockOnHand.Value, 0, 0, _MarkDownQuantity.Value, _MarkDownQuantity.Value, _WriteOffQuantity.Value, _WriteOffQuantity.Value, _NormalSellPrice.Value, _NormalSellPrice.Value)
            Return True
        End If

        Return False

    End Function

    Public Sub FlagAllAsObselete(ByVal ObsDate As Date)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ItemObsolete.ColumnName, 1)
        Oasys3DB.SetColumnAndValueParameter(_DateObsolete.ColumnName, ObsDate)
        Oasys3DB.SetWhereParameter(_ItemObsolete.ColumnName, clsOasys3DB.eOperator.pEquals, 0)
        Oasys3DB.Update()

    End Sub

    Public Sub UpdateRetailPrice(ByVal EventNo As String, ByVal EventPriority As String)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_RetailPriceEventNo.ColumnName, EventNo)
        Oasys3DB.SetColumnAndValueParameter(_RetailPricePriority.ColumnName, EventPriority)
        SetDBKeys()
        Oasys3DB.Update()

    End Sub

    Public Sub UpdateEANCount(ByVal EANSKUNumber As String, ByVal AdjustQty As Integer)

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(clsOasys3DB.eOperator.pEquals, _SkuNumber, EANSKUNumber)
        AddLoadField(_SkuNumber)
        AddLoadField(_Ean)
        LoadMatches()
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
        _Ean.Value += AdjustQty
        If _Ean.Value < 0 Then _Ean.Value = 0
        Oasys3DB.SetColumnAndValueParameter(_Ean.ColumnName, _Ean.Value)
        SetDBKeys()
        Oasys3DB.Update()

    End Sub

#End Region

End Class


