<Serializable()> Public Class cStockAdjust
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STKADJ"
        BOFields.Add(_PeriodID)
        BOFields.Add(_Code)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Sequence)
        BOFields.Add(_AmendId)
        BOFields.Add(_ParentSeq)
        BOFields.Add(_TransferSKU)
        BOFields.Add(_TransferStart)
        BOFields.Add(_TransferPrice)
        BOFields.Add(_DateCreated)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_StartStock)
        BOFields.Add(_QtyAdjusted)
        BOFields.Add(_Price)
        BOFields.Add(_TransferValue)
        BOFields.Add(_Cost)
        BOFields.Add(_Comment)
        BOFields.Add(_CodeType)
        BOFields.Add(_DrlNumber)
        BOFields.Add(_IsReversed)
        BOFields.Add(_MarkdownWriteOff)
        BOFields.Add(_WriteOffID)
        BOFields.Add(_WriteOffDate)
        BOFields.Add(_RTI)
        BOFields.Add(_CommedToHeadOffice)
        BOConstraints.Add(New Object() {_DateCreated, _Code, _SkuNumber, _Sequence, _AmendId})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStockAdjust)

        LoadBORecords(count)

        Dim col As New List(Of cStockAdjust)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cStockAdjust))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStockAdjust(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", False, False)
    Private _DateCreated As New ColField(Of Date)("DATE1", Nothing, "Date Created", True, False)
    Private _Code As New ColField(Of String)("CODE", "00", "Code", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "Sku Number", True, False)
    Private _Sequence As New ColField(Of String)("SEQN", "00", "Sequence", True, False)
    Private _AmendId As New ColField(Of Integer)("AmendId", 0, "Amendment ID", True, False)
    Private _ParentSeq As New ColField(Of String)("ParentSeq", "00", "Parent Sequence", False, False)
    Private _TransferSKU As New ColField(Of String)("TSKU", "000000", "Transfer SKU", False, False)
    Private _TransferStart As New ColField(Of Decimal)("TransferStart", 0, "Transfer Start", False, False)
    Private _TransferPrice As New ColField(Of Decimal)("TransferPrice", 0, "Transfer Price", False, False, 2)
    Private _EmployeeID As New ColField(Of String)("INIT", "", "Employee ID", False, False)
    Private _StartStock As New ColField(Of Decimal)("SSTK", 0, "Start Stock", False, False)
    Private _QtyAdjusted As New ColField(Of Decimal)("QUAN", 0, "Qty Adjusted", False, False)
    Private _Price As New ColField(Of Decimal)("PRIC", 0, "Price", False, False)
    Private _Cost As New ColField(Of Decimal)("COST", 0, "Cost", False, False)
    Private _Comment As New ColField(Of String)("INFO", "", "Comment", False, False)
    Private _DrlNumber As New ColField(Of String)("DRLN", "000000", "Drl Number", False, False)
    Private _IsReversed As New ColField(Of Boolean)("IsReversed", False, "Is Reversed", False, False)
    Private _MarkdownWriteOff As New ColField(Of String)("MOWT", "", "Markdown Write Off", False, False)
    Private _CodeType As New ColField(Of String)("TYPE", "", "Code Type", False, False)
    Private _WriteOffID As New ColField(Of Integer)("WAUT", 0, "Write Off ID", False, False)
    Private _WriteOffDate As New ColField(Of Date)("DAUT", Nothing, "Write Off Date", False, False)
    Private _RTI As New ColField(Of String)("RTI", "N", "RTI", False, False)
    Private _CommedToHeadOffice As New ColField(Of Boolean)("COMM", False, "Commed to Head Office", False, False)
    Private _TransferValue As New ColField(Of Decimal)("TVAL", 0, "Transfer Value", False, False)

#End Region

#Region "Field Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            Return _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property Code() As ColField(Of String)
        Get
            Code = _Code
        End Get
        Set(ByVal value As ColField(Of String))
            _Code = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Sequence() As ColField(Of String)
        Get
            Return _Sequence
        End Get
        Set(ByVal value As ColField(Of String))
            _Sequence = value
        End Set
    End Property
    Public Property AmendId() As ColField(Of Integer)
        Get
            Return _AmendId
        End Get
        Set(ByVal value As ColField(Of Integer))
            _AmendId = value
        End Set
    End Property
    Public Property ParentSequence() As ColField(Of String)
        Get
            Return _ParentSeq
        End Get
        Set(ByVal value As ColField(Of String))
            _ParentSeq = value
        End Set
    End Property
    Public Property TransferSKU() As ColField(Of String)
        Get
            Return _TransferSKU
        End Get
        Set(ByVal value As ColField(Of String))
            _TransferSKU = value
        End Set
    End Property
    Public Property TransferStart() As ColField(Of Decimal)
        Get
            Return _TransferStart
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TransferStart = value
        End Set
    End Property
    Public Property TransferPrice() As ColField(Of Decimal)
        Get
            Return _TransferPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TransferPrice = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)
        Get
            EmployeeID = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property StartStock() As ColField(Of Decimal)
        Get
            StartStock = _StartStock
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _StartStock = value
        End Set
    End Property
    Public Property QtyAdjusted() As ColField(Of Decimal)
        Get
            QtyAdjusted = _QtyAdjusted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyAdjusted = value
        End Set
    End Property
    Public Property Price() As ColField(Of Decimal)
        Get
            Price = _Price
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Price = value
        End Set
    End Property
    Public Property TransferValue() As ColField(Of Decimal)
        Get
            TransferValue = _TransferValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TransferValue = value
        End Set
    End Property
    Public Property Cost() As ColField(Of Decimal)
        Get
            Cost = _Cost
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Cost = value
        End Set
    End Property
    Public Property Comment() As ColField(Of String)
        Get
            Comment = _Comment
        End Get
        Set(ByVal value As ColField(Of String))
            _Comment = value
        End Set
    End Property
    Public Property DrlNumber() As ColField(Of String)
        Get
            DrlNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property IsReversed() As ColField(Of Boolean)
        Get
            IsReversed = _IsReversed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsReversed = value
        End Set
    End Property
    Public Property MarkdownWriteOff() As ColField(Of String)
        Get
            Return _MarkdownWriteOff
        End Get
        Set(ByVal value As ColField(Of String))
            _MarkdownWriteOff = value
        End Set
    End Property
    Public Property CodeType() As ColField(Of String)
        Get
            Return _CodeType
        End Get
        Set(ByVal value As ColField(Of String))
            _CodeType = value
        End Set
    End Property
    Public Property WriteOffID() As ColField(Of Integer)
        Get
            WriteOffID = _WriteOffID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _WriteOffID = value
        End Set
    End Property
    Public Property WriteOffDate() As ColField(Of Date)
        Get
            WriteOffDate = _WriteOffDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _WriteOffDate = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property
    Public Property CommedToHeadOffice() As ColField(Of Boolean)
        Get
            CommedToHeadOffice = _CommedToHeadOffice
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommedToHeadOffice = value
        End Set
    End Property

#End Region

#Region "Data Sets"
    Public Enum ColumnNames
        Selected
        EndStock
        SkuDescription
        CategoryNumber
        CategoryDescription
        GroupNumber
        GroupDescription
        SubgroupNumber
        SubgroupDescription
        StyleNumber
        StyleDescription
        CodeSecurity
        CodeNumber
        CodeDescription
        QtyAdjust
        QtyMarkdown
        QtyWrittenOff
        Value
    End Enum
    Public Function Column(ByVal name As ColumnNames) As String
        Return name.ToString
    End Function

    Private _ds As New DataSet
    Private _dtCodeSummary As New DataTable
    Private _dtCodeDetail As New DataTable
    Public ReadOnly Property DataSet() As DataSet
        Get
            Return _ds
        End Get
    End Property
    Public ReadOnly Property TableAdjusts() As DataTable
        Get
            Return _ds.Tables(0)
        End Get
    End Property
    Public ReadOnly Property TableAmends() As DataTable
        Get
            Return _ds.Tables(1)
        End Get
    End Property
    Public ReadOnly Property TableCodeSummary() As DataTable
        Get
            Return _dtCodeSummary
        End Get
    End Property
    Public ReadOnly Property TableCodeDetail() As DataTable
        Get
            Return _dtCodeDetail
        End Get
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Resets values for this stock adjustment initialising with given stock values
    ''' </summary>
    ''' <param name="stock"></param>
    ''' <remarks></remarks>
    Public Overloads Sub ResetValues(ByRef stock As cStock)
        MyBase.ResetValues()
        _SkuNumber.Value = stock.SkuNumber.Value
        _StartStock.Value = stock.StockOnHand.Value
        _Price.Value = stock.NormalSellPrice.Value
        _Cost.Value = stock.CostPrice.Value
    End Sub

    Public Overloads Sub ResetMarkdown(ByRef stock As cStock)
        MyBase.ResetValues()
        _SkuNumber.Value = stock.SkuNumber.Value
        _StartStock.Value = stock.MarkDownQuantity.Value
        _Price.Value = stock.NormalSellPrice.Value
        _Cost.Value = stock.CostPrice.Value
    End Sub

    ''' <summary>
    ''' Creates new stock adjustment or amendment for given parameters and for this instance if stock values specified.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="code"></param>
    ''' <param name="userID"></param>
    ''' <param name="comment"></param>
    ''' <param name="qty"></param>
    ''' <param name="stock"></param>
    ''' <param name="transferStock"></param>
    ''' <remarks></remarks>
    Public Sub CreateNew(ByRef code As cStockAdjustCode, ByVal userID As Integer, ByVal comment As String, ByVal qty As Integer, ByRef stock As cStock, Optional ByRef transferStock As cStock = Nothing, Optional ByVal drlNumber As String = "")

        Try
            'check if adjustment been set up for stock item and set up if not
            If _SkuNumber.Value = _SkuNumber.DefaultValue Then
                ResetValues(stock)
            End If

            'load adjustment data if not already done so
            If _ds.Tables.Count = 0 Then AdjustmentsLoadSkuAndCode(_SkuNumber.Value, code.Number.Value)

            If _QtyAdjusted.Value <> _QtyAdjusted.DefaultValue Then
                'is an amendment
                _StartStock.Value = stock.StockOnHand.Value
                _QtyAdjusted.Value = qty
                _AmendId.Value = 0
                ' Referral 532 - Set up the relationship
                _ParentSeq.Value = stock.Adjust.Sequence.Value

                'get next amend id number and qty to amend
                For Each rowAdjust As DataRow In TableAdjusts.Rows
                    If CDate(rowAdjust(_DateCreated.ColumnName)) = _DateCreated.Value _
                    AndAlso CStr(rowAdjust(_SkuNumber.ColumnName)) = _SkuNumber.Value _
                    AndAlso CStr(rowAdjust(_Code.ColumnName)) = _Code.Value _
                    AndAlso CStr(rowAdjust(_Sequence.ColumnName)) = _ParentSeq.Value Then
                        ' Referral 532 - as always incrementing SEQN, need another field to define parent/child relationship for amendments
                        ' so use new one called _ParentSeq
                        _AmendId.Value += 1
                        _QtyAdjusted.Value -= CDec(rowAdjust(_QtyAdjusted.ColumnName))

                        For Each rowAmend As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                            _AmendId.Value += 1
                            _QtyAdjusted.Value -= CDec(rowAmend(_QtyAdjusted.ColumnName))
                        Next
                    End If
                Next

                ' Referral 532 - Make sure Seq no is always different, even for amendments
                'get next sequence number
                Dim seq As Integer = 1
                For Each dr As DataRow In TableAdjusts.Rows
                    If CDate(dr(_DateCreated.ColumnName)) = _DateCreated.Value Then
                        seq += 1
                    End If
                Next
                _Sequence.Value = seq.ToString("00")

            Else
                'is a new adjustment so reset all values to stock info
                ResetValues(stock)

                'get current period id and date
                Dim period As New BOSystem.cSystemPeriods(Oasys3DB)
                period.LoadCurrent(BOSystem.cSystemPeriods.ClosedStates.All)
                _PeriodID.Value = period.ID.Value
                _DateCreated.Value = Now.Date

                'assign values to adjust entity forcing load if not already loaded
                _Code.Value = code.Number.Value
                _CodeType.Value = code.Type.Value
                Select Case code.MarkdownWriteOff
                    Case cStockAdjustCode.MarkdownWriteOffs.WriteOff : _MarkdownWriteOff.Value = "W"
                    Case cStockAdjustCode.MarkdownWriteOffs.Markdown : _MarkdownWriteOff.Value = "M"
                End Select

                'get next sequence number
                Dim seq As Integer = 1
                For Each dr As DataRow In TableAdjusts.Rows
                    If CDate(dr(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CInt(dr(_AmendId.ColumnName)) = 0 Then
                        seq += 1
                    End If
                Next
                _Sequence.Value = seq.ToString("00")
                _QtyAdjusted.Value = qty

                'add transfer details to adjustment if exist
                If transferStock IsNot Nothing Then
                    _TransferSKU.Value = transferStock.SkuNumber.Value
                    _TransferPrice.Value = transferStock.NormalSellPrice.Value
                    _TransferStart.Value = transferStock.StockOnHand.Value
                End If
            End If

            'set common values
            _EmployeeID.Value = userID.ToString("000")
            _Comment.Value = comment
            _DrlNumber.Value = drlNumber.PadLeft(6, "0"c)

            'start transaction
            Oasys3DB.BeginTransaction()

            'insert adjustment
            SaveIfNew()

            'check type of adjustment for stock updating
            stock.UpdateAdjust(Me)

            'create markdowns x quantity (only where adjustment qty is -ve and so markdown qty +ve (only if scheduling used)
            If _MarkdownWriteOff.Value = "M" AndAlso qty < 0 Then
                Using param As New BOSystem.cParameter(Oasys3DB)
                    If param.GetParameterBoolean(4112) Then
                        stock.Markdown.ReasonCode.Value = _Code.Value
                        For index As Integer = 1 To (qty * -1)
                            stock.Markdown.DateCreated.Value = Now
                            stock.Markdown.Serial.Value = (stock.Markdown.ActiveTable.Rows.Count + index).ToString.PadLeft(6, "0"c)
                            stock.Markdown.WeekNumber.Value = 1
                            stock.Markdown.SaveIfNew()
                        Next
                    End If
                End Using
            End If

            'update transfer stock if exists
            If transferStock IsNot Nothing Then
                _QtyAdjusted.Value *= -1
                transferStock.UpdateAdjust(Me)
            End If

            'commit transaction here
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Creates new stock adjustment or amendment for given parameters and for this instance if stock values specified.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="code"></param>
    ''' <param name="userID"></param>
    ''' <param name="comment"></param>
    ''' <param name="qty"></param>
    ''' <param name="stock"></param>
    ''' <param name="transferStock"></param>
    ''' <remarks></remarks>
    Public Sub CreateNew(ByRef code As cStockAdjustCode, ByVal userID As Integer, ByVal comment As String, ByVal qty As Integer, ByRef stock As cStock, ByVal adjustDate As Date, Optional ByRef transferStock As cStock = Nothing, Optional ByVal drlNumber As String = "")

        Try
            'check if adjustment been set up for stock item and set up if not
            If _SkuNumber.Value = _SkuNumber.DefaultValue Then
                ResetValues(stock)
            End If

            'load adjustment data if not already done so
            If _ds.Tables.Count = 0 Then AdjustmentsLoad(_SkuNumber.Value, code.Number.Value, adjustDate)

            If _QtyAdjusted.Value <> _QtyAdjusted.DefaultValue Then
                'is an amendment
                _StartStock.Value = stock.StockOnHand.Value
                _QtyAdjusted.Value = qty
                _AmendId.Value = 0

                'get next amend id number and qty to amend
                For Each rowAdjust As DataRow In TableAdjusts.Rows
                    If CDate(rowAdjust(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CStr(rowAdjust(_SkuNumber.ColumnName)) = _SkuNumber.Value AndAlso CStr(rowAdjust(_Code.ColumnName)) = _Code.Value AndAlso CStr(rowAdjust(_Sequence.ColumnName)) = _Sequence.Value Then
                        _AmendId.Value += 1
                        _QtyAdjusted.Value -= CDec(rowAdjust(_QtyAdjusted.ColumnName))

                        For Each rowAmend As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                            _AmendId.Value += 1
                            _QtyAdjusted.Value -= CDec(rowAmend(_QtyAdjusted.ColumnName))
                        Next
                    End If
                Next

            Else
                'is a new adjustment so reset all values to stock info
                ResetValues(stock)

                'get current period id and date
                Dim period As New BOSystem.cSystemPeriods(Oasys3DB)
                period.LoadCurrent(BOSystem.cSystemPeriods.ClosedStates.All)
                _PeriodID.Value = period.ID.Value
                _DateCreated.Value = Now.Date

                'assign values to adjust entity forcing load if not already loaded
                _Code.Value = code.Number.Value
                _CodeType.Value = code.Type.Value
                Select Case code.MarkdownWriteOff
                    Case cStockAdjustCode.MarkdownWriteOffs.WriteOff : _MarkdownWriteOff.Value = "W"
                    Case cStockAdjustCode.MarkdownWriteOffs.Markdown : _MarkdownWriteOff.Value = "M"
                End Select

                'get next sequence number
                Dim seq As Integer = 1
                For Each dr As DataRow In TableAdjusts.Rows
                    If CDate(dr(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CInt(dr(_AmendId.ColumnName)) = 0 Then
                        seq += 1
                    End If
                Next
                _Sequence.Value = seq.ToString("00")
                _QtyAdjusted.Value = qty

                'add transfer details to adjustment if exist
                If transferStock IsNot Nothing Then
                    _TransferSKU.Value = transferStock.SkuNumber.Value
                    _TransferPrice.Value = transferStock.NormalSellPrice.Value
                    _TransferStart.Value = transferStock.StockOnHand.Value
                End If
            End If

            'set common values
            _EmployeeID.Value = userID.ToString("000")
            _Comment.Value = comment
            _DrlNumber.Value = drlNumber.PadLeft(6, "0"c)

            'start transaction
            Oasys3DB.BeginTransaction()

            'insert adjustment
            SaveIfNew()

            'check type of adjustment for stock updating
            stock.UpdateAdjust(Me)

            'create markdowns x quantity (only where adjustment qty is -ve and so markdown qty +ve (only if scheduling used)
            If _MarkdownWriteOff.Value = "M" AndAlso qty < 0 Then
                Using param As New BOSystem.cParameter(Oasys3DB)
                    If param.GetParameterBoolean(4112) Then
                        stock.Markdown.ReasonCode.Value = _Code.Value
                        For index As Integer = 1 To (qty * -1)
                            stock.Markdown.DateCreated.Value = Now
                            stock.Markdown.Serial.Value = (stock.Markdown.ActiveTable.Rows.Count + index).ToString.PadLeft(6, "0"c)
                            stock.Markdown.WeekNumber.Value = 1
                            stock.Markdown.SaveIfNew()
                        Next
                    End If
                End Using
            End If

            'update transfer stock if exists
            If transferStock IsNot Nothing Then
                _QtyAdjusted.Value *= -1
                transferStock.UpdateAdjust(Me)
            End If

            'commit transaction here
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Creates new stock adjustment or amendment for given parameters and for this instance if stock values specified.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="code"></param>
    ''' <param name="userID"></param>
    ''' <param name="comment"></param>
    ''' <param name="qty"></param>
    ''' <param name="stock"></param>
    ''' <param name="transferStock"></param>
    ''' <remarks></remarks>
    Public Sub CreateNewMarkdown(ByRef code As cStockAdjustCode, ByVal userID As Integer, ByVal comment As String, ByVal qty As Integer, ByRef stock As cStock, ByVal adjustDate As Date, Optional ByRef transferStock As cStock = Nothing, Optional ByVal drlNumber As String = "")

        Try
            'check if adjustment been set up for stock item and set up if not
            If _SkuNumber.Value <> _SkuNumber.DefaultValue Then
                ResetMarkdown(stock)
            End If

            'load adjustment data if not already done so
            _ds = New DataSet
            _dtCodeDetail = New DataTable
            _dtCodeSummary = New DataTable
            If _ds.Tables.Count = 0 Then AdjustmentsLoad(_SkuNumber.Value, code.Number.Value, adjustDate)

            If _QtyAdjusted.Value <> _QtyAdjusted.DefaultValue Then
                'is an amendment
                _StartStock.Value = stock.StockOnHand.Value
                _QtyAdjusted.Value = qty
                _AmendId.Value = 0

                'get next amend id number and qty to amend
                For Each rowAdjust As DataRow In TableAdjusts.Rows
                    If CDate(rowAdjust(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CStr(rowAdjust(_SkuNumber.ColumnName)) = _SkuNumber.Value AndAlso CStr(rowAdjust(_Code.ColumnName)) = _Code.Value AndAlso CStr(rowAdjust(_Sequence.ColumnName)) = _Sequence.Value Then
                        _AmendId.Value += 1
                        _QtyAdjusted.Value -= CDec(rowAdjust(_QtyAdjusted.ColumnName))

                        For Each rowAmend As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                            _AmendId.Value += 1
                            _QtyAdjusted.Value -= CDec(rowAmend(_QtyAdjusted.ColumnName))
                        Next
                    End If
                Next

            Else
                'is a new adjustment so reset all values to stock info
                ResetMarkdown(stock)

                'get current period id and date
                Dim period As New BOSystem.cSystemPeriods(Oasys3DB)
                period.LoadCurrent(BOSystem.cSystemPeriods.ClosedStates.All)
                _PeriodID.Value = period.ID.Value
                _DateCreated.Value = Now.Date

                'assign values to adjust entity forcing load if not already loaded
                _Code.Value = code.Number.Value
                _CodeType.Value = code.Type.Value
                Select Case code.MarkdownWriteOff
                    Case cStockAdjustCode.MarkdownWriteOffs.WriteOff : _MarkdownWriteOff.Value = "W"
                    Case cStockAdjustCode.MarkdownWriteOffs.Markdown : _MarkdownWriteOff.Value = "M"
                End Select

                'get next sequence number
                Dim seq As Integer = 1
                For Each dr As DataRow In TableAdjusts.Rows
                    If CDate(dr(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CInt(dr(_AmendId.ColumnName)) = 0 Then
                        seq += 1
                    End If
                Next
                _Sequence.Value = seq.ToString("00")
                _QtyAdjusted.Value = qty

                'add transfer details to adjustment if exist
                If transferStock IsNot Nothing Then
                    _TransferSKU.Value = transferStock.SkuNumber.Value
                    _TransferPrice.Value = transferStock.NormalSellPrice.Value
                    _TransferStart.Value = transferStock.StockOnHand.Value
                End If
            End If

            'set common values
            _EmployeeID.Value = userID.ToString("000")
            _Comment.Value = comment
            _DrlNumber.Value = drlNumber.PadLeft(6, "0"c)

            'start transaction
            Oasys3DB.BeginTransaction()

            'insert adjustment
            SaveIfNew()

            'check type of adjustment for stock updating
            stock.UpdateMarkdown(Me)

            'create markdowns x quantity (only where adjustment qty is -ve and so markdown qty +ve (only if scheduling used)
            If _MarkdownWriteOff.Value = "M" AndAlso qty < 0 Then
                Using param As New BOSystem.cParameter(Oasys3DB)
                    If param.GetParameterBoolean(4112) Then
                        stock.Markdown.ReasonCode.Value = _Code.Value
                        For index As Integer = 1 To (qty * -1)
                            stock.Markdown.DateCreated.Value = Now
                            stock.Markdown.Serial.Value = (stock.Markdown.ActiveTable.Rows.Count + index).ToString.PadLeft(6, "0"c)
                            stock.Markdown.WeekNumber.Value = 1
                            stock.Markdown.SaveIfNew()
                        Next
                    End If
                End Using
            End If

            'update transfer stock if exists
            If transferStock IsNot Nothing Then
                _QtyAdjusted.Value *= -1
                transferStock.UpdateMarkdown(Me)
            End If

            'commit transaction here
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustSave, ex)
        End Try

    End Sub


    ''' <summary>
    ''' Reverses adjustment and all amendments for given adjustment
    ''' </summary>
    ''' <param name="stock"></param>
    ''' <remarks></remarks>
    Public Sub ReverseAdjustment(ByRef rowAdjust As DataRow, ByVal userId As Integer, ByRef stock As cStock)

        Try
            'load data if nothing there
            If TableAdjusts.Columns.Count = 0 Then AdjustmentsLoadSkuAndCode(_SkuNumber.Value, _Code.Value)

            'load datarow into this instance
            Me.LoadFromRow(rowAdjust)

            'get next amend id number and qty to amend
            _AmendId.Value = 1
            For Each dr As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                _AmendId.Value += 1
                _QtyAdjusted.Value += CDec(dr(_QtyAdjusted.ColumnName))
            Next

            _StartStock.Value = stock.StockOnHand.Value
            _QtyAdjusted.Value *= -1
            _EmployeeID.Value = userId.ToString("000")

            'start transaction
            Oasys3DB.BeginTransaction()

            'insert reverse adjustment
            SaveIfNew()

            'check type of adjustment for stock updating
            stock.UpdateAdjust(Me)

            'update transfer stock if exists
            If _TransferSKU.Value <> "000000" Then
                Dim transferStock As New cStock(Oasys3DB)
                transferStock.LoadStockItem(_TransferSKU.Value)
                _QtyAdjusted.Value *= -1
                transferStock.UpdateAdjust(Me)
            End If

            'update all adjustments as reversed
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IsReversed.ColumnName, True)
            Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, _SkuNumber.Value)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, _DateCreated.Value.Date)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_Code.ColumnName, clsOasys3DB.eOperator.pEquals, Code.Value)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_Sequence.ColumnName, clsOasys3DB.eOperator.pEquals, _Sequence.Value)
            Oasys3DB.Update()

            'commit transaction here
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustReverse, ex)
        End Try

    End Sub


    ''' <summary>
    ''' Returns datatable of adjustments greater than given date for given codes.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="codes"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAdjustmentsTable(ByVal startDate As Date, ByVal codes As ArrayList) As DataTable

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pGreaterThanOrEquals, startDate)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_Code.ColumnName, clsOasys3DB.eOperator.pIn, codes)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Return dt

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Function


    ''' <summary>
    ''' Loads adjustments into 'Data' dataset for given parameters and for stock item if business object
    '''  is linked to stock item else for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AdjustmentsLoad(ByVal skuNumber As String, ByVal codeNumber As String, ByVal adjustDate As Date)

        Try
            'create sql statement
            Dim sb As New StringBuilder()
            sb.Append("EXEC AdjustsSelectForSkuAndCode ")
            sb.Append("'" & skuNumber & "', ")
            sb.Append("'" & codeNumber & "'")
            sb.Append("'" & adjustDate.ToString("yyyy-MM-dd") & "'")

            _ds = Oasys3DB.SqlServer.GetDataset(sb.ToString)
            _ds.Tables(0).DefaultView.RowFilter = _IsReversed.ColumnName & "=0"

            Dim parentCols As DataColumn() = New DataColumn() {TableAdjusts.Columns(_DateCreated.ColumnName), TableAdjusts.Columns(_SkuNumber.ColumnName), TableAdjusts.Columns(_Code.ColumnName), TableAdjusts.Columns(_Sequence.ColumnName)}
            Dim childCols As DataColumn() = New DataColumn() {TableAmends.Columns(_DateCreated.ColumnName), TableAmends.Columns(_SkuNumber.ColumnName), TableAmends.Columns(_Code.ColumnName), TableAmends.Columns(_ParentSeq.ColumnName)}
            ' Referral 532 - As need Sequence to always increment, need another field on which to base the relationship
            'Dim childCols As DataColumn() = New DataColumn() {TableAmends.Columns(_DateCreated.ColumnName), TableAmends.Columns(_SkuNumber.ColumnName), TableAmends.Columns(_Code.ColumnName), TableAmends.Columns(_Sequence.ColumnName)}

            _ds.Relations.Add("Amendments", parentCols, childCols)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads adjustments into 'Data' dataset for given parameters and for stock item if business object
    '''  is linked to stock item else for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AdjustmentsLoadSkuAndCode(ByVal skuNumber As String, ByVal codeNumber As String)

        Try
            'create sql statement
            Dim sb As New StringBuilder()
            sb.Append("EXEC AdjustsSelectForSkuAndCode ")
            sb.Append("'" & skuNumber & "', ")
            sb.Append("'" & codeNumber & "'")

            _ds = Oasys3DB.SqlServer.GetDataset(sb.ToString)
            _ds.Tables(0).DefaultView.RowFilter = _IsReversed.ColumnName & "=0"

            Dim parentCols As DataColumn() = New DataColumn() {TableAdjusts.Columns(_DateCreated.ColumnName), TableAdjusts.Columns(_SkuNumber.ColumnName), TableAdjusts.Columns(_Code.ColumnName), TableAdjusts.Columns(_Sequence.ColumnName)}
            Dim childCols As DataColumn() = New DataColumn() {TableAmends.Columns(_DateCreated.ColumnName), TableAmends.Columns(_SkuNumber.ColumnName), TableAmends.Columns(_Code.ColumnName), TableAmends.Columns(_ParentSeq.ColumnName)}
            ' Referral 532 - As need Sequence to always increment, need another field on which to base the relationship
            'Dim childCols As DataColumn() = New DataColumn() {TableAmends.Columns(_DateCreated.ColumnName), TableAmends.Columns(_SkuNumber.ColumnName), TableAmends.Columns(_Code.ColumnName), TableAmends.Columns(_Sequence.ColumnName)}

            _ds.Relations.Add("Amendments", parentCols, childCols)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads code summary for adjustments in given date range into TableCodeSummary
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AdjustmentsCodeSummaryLoad(ByVal startDate As Date, ByVal endDate As Date)

        Try
            'create sql statement
            Dim sb As New StringBuilder("EXEC AdjustsSelectCodeSummaryDateRange ")
            sb.Append("'" & startDate.ToString("yyyy-MM-dd") & "', ")
            sb.Append("'" & endDate.ToString("yyyy-MM-dd") & "'")
            _dtCodeSummary = Oasys3DB.SqlServer.GetDataset(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads code details for adjustments in given date range into TableCodeDetail
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AdjustmentsCodeDetailLoad(ByVal startDate As Date, ByVal endDate As Date)

        Try
            'create sql statement
            Dim sb As New StringBuilder("EXEC AdjustsSelectCodeDetailDateRange ")
            sb.Append("'" & startDate.ToString("yyyy-MM-dd") & "', ")
            sb.Append("'" & endDate.ToString("yyyy-MM-dd") & "'")
            _dtCodeDetail = Oasys3DB.SqlServer.GetDataset(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns number of adjustments selected in TableAdjusts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AdjustmentsSelected() As Integer

        Dim total As Integer = 0
        For Each dr As DataRow In TableAdjusts.Rows
            If CBool(dr(Column(ColumnNames.Selected))) = True Then
                total += 1
            End If
        Next

        Return total

    End Function



    ''' <summary>
    ''' Loads open write off adjustments into 'Data' dataset all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub WriteOffsLoad()

        Try
            'create sql statement
            _ds = Oasys3DB.SqlServer.GetDataset("EXEC AdjustsSelectWriteOffs")

            Dim col As New DataColumn("Selected", GetType(Boolean))
            col.DefaultValue = False
            col.Caption = "Selected"
            _ds.Tables(0).Columns.Add(col)
            col.SetOrdinal(0)

            Dim col2 As New DataColumn("Selected", GetType(Boolean))
            col2.DefaultValue = False
            col2.Caption = "Selected"
            _ds.Tables(1).Columns.Add(col2)
            col2.SetOrdinal(0)

            Dim parentCols As DataColumn() = New DataColumn() {TableAdjusts.Columns(_DateCreated.ColumnName), TableAdjusts.Columns(_SkuNumber.ColumnName), TableAdjusts.Columns(_Code.ColumnName), TableAdjusts.Columns(_Sequence.ColumnName)}
            Dim childCols As DataColumn() = New DataColumn() {TableAmends.Columns(_DateCreated.ColumnName), TableAmends.Columns(_SkuNumber.ColumnName), TableAmends.Columns(_Code.ColumnName), TableAmends.Columns(_Sequence.ColumnName)}

            _ds.Relations.Add("Amendments", parentCols, childCols)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Authorises selected write off adjustments and associated stock objects
    ''' </summary>
    ''' <param name="UserID"></param>
    ''' <remarks></remarks>
    Public Sub WriteOffsAuthorise(ByVal userID As Integer)

        Try
            'loop over rows of data table and see if selected for write off
            For Each rowAdjust As DataRow In TableAdjusts.Rows
                If CBool(rowAdjust(Column(ColumnNames.Selected))) = True Then

                    'start transaction
                    Oasys3DB.BeginTransaction()

                    'update adjustments (including amendments) as written off
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
                    Oasys3DB.SetColumnAndValueParameter(_WriteOffDate.ColumnName, Now.Date)
                    Oasys3DB.SetColumnAndValueParameter(_WriteOffID.ColumnName, userID)
                    Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, rowAdjust(_SkuNumber.ColumnName))
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, rowAdjust(_DateCreated.ColumnName))
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_Code.ColumnName, clsOasys3DB.eOperator.pEquals, rowAdjust(_Code.ColumnName))
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_Sequence.ColumnName, clsOasys3DB.eOperator.pEquals, rowAdjust(_Sequence.ColumnName))
                    Oasys3DB.Update()

                    'get net adjustment
                    Dim qty As Integer = CInt(rowAdjust(_QtyAdjusted.ColumnName))
                    For Each dr As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                        qty += CInt(dr(_QtyAdjusted.ColumnName))
                    Next

                    'update stock as written off
                    Dim stock As New cStock(Oasys3DB)
                    stock.LoadStockItem(CStr(rowAdjust(_SkuNumber.ColumnName)))
                    stock.UpdateAdjustWriteOffAuthorised(qty, CDate(rowAdjust(_DateCreated.ColumnName)) & Space(2) & CStr(rowAdjust(_Code.ColumnName)) & Space(2) & CStr(rowAdjust(_SkuNumber.ColumnName)) & Space(2) & CStr(rowAdjust(_Sequence.ColumnName)), userID)

                    'commit transaction
                    Oasys3DB.CommitTransaction()

                    'set row to be deleted
                    rowAdjust.Delete()
                End If
            Next

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustWriteOffAuthorise, ex)
        Finally
            'apply deletions
            TableAdjusts.AcceptChanges()
        End Try

    End Sub

    ''' <summary>
    ''' Cancels selected write off adjustments and updates associated stock objects
    ''' </summary>
    ''' <param name="UserID"></param>
    ''' <remarks></remarks>
    Public Sub WriteOffsCancel(ByVal UserID As Integer)

        Try
            'loop over rows of data table and see if selected for write off
            For Each rowAdjust As DataRow In TableAdjusts.Rows
                If CBool(rowAdjust(Column(ColumnNames.Selected))) = True Then

                    'create new adjustment to cancel write off
                    Me.LoadFromRow(rowAdjust)
                    _AmendId.Value = 1
                    _IsReversed.Value = True

                    'get net adjustment and next amend id number
                    For Each dr As DataRow In rowAdjust.GetChildRows(_ds.Relations(0).RelationName)
                        _AmendId.Value += 1
                        _QtyAdjusted.Value += CInt(dr(_QtyAdjusted.ColumnName))
                    Next

                    'set values
                    _QtyAdjusted.Value *= -1
                    _EmployeeID.Value = UserID.ToString("000")

                    'start transaction
                    Oasys3DB.BeginTransaction()

                    'save new adjustment
                    SaveIfNew()

                    'reverse adjustments (including amendments) to cancel
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
                    Oasys3DB.SetColumnAndValueParameter(_IsReversed.ColumnName, True)
                    Oasys3DB.SetWhereParameter(_SkuNumber.ColumnName, clsOasys3DB.eOperator.pEquals, _SkuNumber.Value)
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_DateCreated.ColumnName, clsOasys3DB.eOperator.pEquals, _DateCreated.Value)
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_Code.ColumnName, clsOasys3DB.eOperator.pEquals, _Code.Value)
                    Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                    Oasys3DB.SetWhereParameter(_Sequence.ColumnName, clsOasys3DB.eOperator.pEquals, _Sequence.Value)
                    Oasys3DB.Update()

                    'update stock as cancelled
                    Dim stock As New cStock(Oasys3DB)
                    stock.LoadStockItem(_SkuNumber.Value)
                    stock.UpdateAdjust(Me)

                    'commit transaction
                    Oasys3DB.CommitTransaction()

                    'set row to be deleted
                    rowAdjust.Delete()
                End If
            Next

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.AdjustWriteOffCancel, ex)
        Finally
            'apply deletions
            TableAdjusts.AcceptChanges()
        End Try

    End Sub




    Public Function UpdateDailyFlashTotalsAdjustment() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
            Oasys3DB.SetColumnAndValueParameter(_SkuNumber.ColumnName, _SkuNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_DateCreated.ColumnName, _DateCreated.Value)
            Oasys3DB.SetColumnAndValueParameter(_Code.ColumnName, _Code.Value)
            Oasys3DB.SetColumnAndValueParameter(_Sequence.ColumnName, _Sequence.Value)
            Oasys3DB.SetColumnAndValueParameter(_EmployeeID.ColumnName, _EmployeeID.Value)
            Oasys3DB.SetColumnAndValueParameter(_StartStock.ColumnName, _StartStock.Value)
            Oasys3DB.SetColumnAndValueParameter(_QtyAdjusted.ColumnName, _QtyAdjusted.Value)
            Oasys3DB.SetColumnAndValueParameter(_Price.ColumnName, _Price.Value)
            Oasys3DB.SetColumnAndValueParameter(_Cost.ColumnName, _Cost.Value)
            Oasys3DB.SetColumnAndValueParameter(_CodeType.ColumnName, _CodeType.Value)
            Oasys3DB.SetColumnAndValueParameter(_Comment.ColumnName, _Comment.Value)
            Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, "000000")
            Oasys3DB.SetColumnAndValueParameter(_PeriodID.ColumnName, _PeriodID.Value)
            Oasys3DB.SetColumnAndValueParameter(_AmendId.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(_TransferSKU.ColumnName, "000000")
            Oasys3DB.SetColumnAndValueParameter(_TransferStart.ColumnName, "0")
            Oasys3DB.SetColumnAndValueParameter(_TransferPrice.ColumnName, 0.0)
            Oasys3DB.SetColumnAndValueParameter(_WriteOffID.ColumnName, "000")
            Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, _RTI.Value)
            Oasys3DB.Insert()

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function ISISInsertStockAdjust(ByVal listSKU As List(Of String), ByVal listQuantitySKU As List(Of Integer), ByVal strAdjustCode As String, ByVal strUserID As String) As Boolean

        Dim BOStock As New BOStock.cStock(Oasys3DB)
        Dim intCount As Integer = 0

        Try
            Dim BOStockAdjustCode As New BOStock.cStockAdjustCode(Oasys3DB)
            BOStockAdjustCode.Load(strAdjustCode)

            Do Until intCount = listSKU.Count
                BOStock.LoadStockItem(listSKU.Item(intCount).ToString)
                BOStock.Adjust.CreateNew(BOStockAdjustCode, CInt(strUserID), "HHT Adjust", listQuantitySKU.Item(intCount), BOStock)

                intCount += 1
            Loop

            Return True

        Catch ex As Exception
            Trace.WriteLine("StockAdjust: The insert of SKU " & listSKU.Item(intCount) & " was a failure" & vbNewLine & "Error: " & ex.Message)
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Returns net quantity amendment for this stock adjustment.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="startQty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAmendmentNetQty(ByVal startQty As Integer) As Integer

        Try
            'load data if nothing there
            If TableAdjusts.Columns.Count = 0 Then AdjustmentsLoadSkuAndCode(_SkuNumber.Value, _Code.Value)

            Dim qty As Integer = startQty
            For Each dr As DataRow In TableAdjusts.Rows
                If CDate(dr(_DateCreated.ColumnName)) = _DateCreated.Value AndAlso CStr(dr(_Sequence.ColumnName)) = _Sequence.Value Then
                    qty -= CInt(dr(_QtyAdjusted.ColumnName))
                End If
            Next

            Return qty

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AdjustNetAmendment, ex)
        End Try

    End Function

    '''' <summary>
    '''' Returns stock log key for this adjustment
    '''' </summary>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Function StockLogKey() As String

    '    Return _DateCreated.Value.ToShortDateString & Space(2) & _Code.Value & Space(2) & _SkuNumber.Value & Space(2) & _Sequence.Value

    'End Function

#End Region

End Class