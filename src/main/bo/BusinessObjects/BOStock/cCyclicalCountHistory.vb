﻿<Serializable()> Public Class cCyclicalCountHistory
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CYHMAS"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_EndofCurrCycle)
        BOFields.Add(_EndofPrevCycle)
        BOFields.Add(_EndofCycles2Ago)
        BOFields.Add(_EndofCycles3Ago)
        BOFields.Add(_AdjustQtyCurrCycle)
        BOFields.Add(_AdjustQtyPrevCycle)
        BOFields.Add(_AdjustQtyCycle2Ago)
        BOFields.Add(_AdjustQtyCycle3Ago)
        BOFields.Add(_AdjustValCurrCycle)
        BOFields.Add(_AdjustValPrevCycle)
        BOFields.Add(_AdjustValCycle2Ago)
        BOFields.Add(_AdjustValCycle3Ago)
        BOFields.Add(_YTDSalesCurrCycle)
        BOFields.Add(_YTDSalesPrevCycle)
        BOFields.Add(_YTDSalesCycle2Ago)
        BOFields.Add(_YTDSalesCycle3Ago)
        BOFields.Add(_PYrSalesCurCycle)
        BOFields.Add(_PYrSalesPrevCycle)
        BOFields.Add(_PYrSalesCycle2Ago)
        BOFields.Add(_PYrSalesCycle3Ago)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCyclicalCountHistory)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cCyclicalCountHistory)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cCyclicalCountHistory))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCyclicalCountHistory(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "SKU Number")
    Private _EndofCurrCycle As New ColField(Of Date)("ADAT1", Nothing, False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "End Of Curr Cycle")
    Private _EndofPrevCycle As New ColField(Of Date)("ADAT2", Nothing, False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "End Of Prev Cycle")
    Private _EndofCycles2Ago As New ColField(Of Date)("ADAT3", Nothing, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "End Of Cycles 2 Ago")
    Private _EndofCycles3Ago As New ColField(Of Date)("ADAT4", Nothing, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "End Of Cycles 3 Ago")
    Private _AdjustQtyCurrCycle As New ColField(Of Decimal)("AQ021", 0, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Adjust Qty Curr Cycle")
    Private _AdjustQtyPrevCycle As New ColField(Of Decimal)("AQ022", 0, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Adjust Qty Prev Cycle")
    Private _AdjustQtyCycle2Ago As New ColField(Of Decimal)("AQ023", 0, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Adjust Qty Cycle 2 Ago")
    Private _AdjustQtyCycle3Ago As New ColField(Of Decimal)("AQ024", 0, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Adjust Qty Cycle 3 Ago")
    Private _AdjustValCurrCycle As New ColField(Of Decimal)("AV021", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Adjust Val Curr Cycle")
    Private _AdjustValPrevCycle As New ColField(Of Decimal)("AV022", 0, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Adjust Val Prev Cycle")
    Private _AdjustValCycle2Ago As New ColField(Of Decimal)("AV023", 0, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Adjust Val Cycle 2 Ago")
    Private _AdjustValCycle3Ago As New ColField(Of Decimal)("AV024", 0, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Adjust Val Cycle 3 Ago")
    Private _YTDSalesCurrCycle As New ColField(Of Decimal)("YTDS1", 0, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "YTD Sales Curr Cycle")
    Private _YTDSalesPrevCycle As New ColField(Of Decimal)("YTDS2", 0, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "YTD Sales Prev Cycle")
    Private _YTDSalesCycle2Ago As New ColField(Of Decimal)("YTDS3", 0, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "YTD Sales Cycle 2 Ago")
    Private _YTDSalesCycle3Ago As New ColField(Of Decimal)("YTDS4", 0, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "YTD Sales Cycle 3 Ago")
    Private _PYrSalesCurCycle As New ColField(Of Decimal)("PYRS1", 0, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "PYr Sales Curr Cycle")
    Private _PYrSalesPrevCycle As New ColField(Of Decimal)("PYRS2", 0, False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "PYr Sales Prev Cycle")
    Private _PYrSalesCycle2Ago As New ColField(Of Decimal)("PYRS3", 0, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "PYr Sales Cycle 2 Ago")
    Private _PYrSalesCycle3Ago As New ColField(Of Decimal)("PYRS4", 0, False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "PYr Sales Cycle 3 Ago")

#End Region

#Region "Field Properties"

    Public Property SKUNumber() As ColField(Of String)
        Get
            SKUNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property EndofCurrCycle() As ColField(Of Date)
        Get
            EndofCurrCycle = _EndofCurrCycle
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndofCurrCycle = value
        End Set
    End Property
    Public Property EndofPrevCycle() As ColField(Of Date)
        Get
            EndofPrevCycle = _EndofPrevCycle
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndofPrevCycle = value
        End Set
    End Property
    Public Property EndofCycles2Ago() As ColField(Of Date)
        Get
            EndofCycles2Ago = _EndofCycles2Ago
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndofCycles2Ago = value
        End Set
    End Property
    Public Property EndofCycles3Ago() As ColField(Of Date)
        Get
            EndofCycles3Ago = _EndofCycles3Ago
        End Get
        Set(ByVal value As ColField(Of Date))
            _EndofCycles3Ago = value
        End Set
    End Property
    Public Property AdjustQtyCurrCycle() As ColField(Of Decimal)
        Get
            AdjustQtyCurrCycle = _AdjustQtyCurrCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustQtyCurrCycle = value
        End Set
    End Property
    Public Property AdjustQtyPrevCycle() As ColField(Of Decimal)
        Get
            AdjustQtyPrevCycle = _AdjustQtyPrevCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustQtyPrevCycle = value
        End Set
    End Property
    Public Property AdjustQtyCycle2Ago() As ColField(Of Decimal)
        Get
            AdjustQtyCycle2Ago = _AdjustQtyCycle2Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustQtyCycle2Ago = value
        End Set
    End Property
    Public Property AdjustQtyCycle3Ago() As ColField(Of Decimal)
        Get
            AdjustQtyCycle3Ago = _AdjustQtyCycle3Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustQtyCycle3Ago = value
        End Set
    End Property
    Public Property AdjustValCurrCycle() As ColField(Of Decimal)
        Get
            AdjustValCurrCycle = _AdjustValCurrCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustValCurrCycle = value
        End Set
    End Property
    Public Property AdjustValPrevCycle() As ColField(Of Decimal)
        Get
            AdjustValPrevCycle = _AdjustValPrevCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustValPrevCycle = value
        End Set
    End Property
    Public Property AdjustValCycle2Ago() As ColField(Of Decimal)
        Get
            AdjustValCycle2Ago = _AdjustValCycle2Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustValCycle2Ago = value
        End Set
    End Property
    Public Property AdjustValCycle3Ago() As ColField(Of Decimal)
        Get
            AdjustValCycle3Ago = _AdjustValCycle3Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _AdjustValCycle3Ago = value
        End Set
    End Property
    Public Property YTDSalesCurrCycle() As ColField(Of Decimal)
        Get
            YTDSalesCurrCycle = _YTDSalesCurrCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _YTDSalesCurrCycle = value
        End Set
    End Property
    Public Property YTDSalesPrevCycle() As ColField(Of Decimal)
        Get
            YTDSalesPrevCycle = _YTDSalesPrevCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _YTDSalesPrevCycle = value
        End Set
    End Property
    Public Property YTDSalesCycle2Ago() As ColField(Of Decimal)
        Get
            YTDSalesCycle2Ago = _YTDSalesCycle2Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _YTDSalesCycle2Ago = value
        End Set
    End Property
    Public Property YTDSalesCycle3Ago() As ColField(Of Decimal)
        Get
            YTDSalesCycle3Ago = _YTDSalesCycle3Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _YTDSalesCycle3Ago = value
        End Set
    End Property
    Public Property PYrSalesCurCycle() As ColField(Of Decimal)
        Get
            PYrSalesCurCycle = _PYrSalesCurCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PYrSalesCurCycle = value
        End Set
    End Property
    Public Property PYrSalesPrevCycle() As ColField(Of Decimal)
        Get
            PYrSalesPrevCycle = _PYrSalesPrevCycle
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PYrSalesPrevCycle = value
        End Set
    End Property
    Public Property PYrSalesCycle2Ago() As ColField(Of Decimal)
        Get
            PYrSalesCycle2Ago = _PYrSalesCycle2Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PYrSalesCycle2Ago = value
        End Set
    End Property
    Public Property PYrSalesCycle3Ago() As ColField(Of Decimal)
        Get
            PYrSalesCycle3Ago = _PYrSalesCycle3Ago
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PYrSalesCycle3Ago = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function Load(ByVal SKUNumber As String) As Boolean
        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, SKUNumber)
        LoadMatches()
        Return True

    End Function

    Public Sub UpdateAdjustment(ByRef adjust As cStockAdjust)

        Try
            If _SkuNumber.Value = "000000" Then
                _SkuNumber.Value = adjust.SkuNumber.Value
                ClearLists()
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, _SkuNumber.Value)
                LoadMatches()
            End If

            _EndofCurrCycle.Value = adjust.DateCreated.Value
            If adjust.IsReversed.Value Then
                _AdjustQtyCurrCycle.Value -= Math.Abs(adjust.QtyAdjusted.Value)
                _AdjustValCurrCycle.Value -= Math.Abs(adjust.QtyAdjusted.Value * adjust.Price.Value)
            Else
                _AdjustQtyCurrCycle.Value += Math.Abs(adjust.QtyAdjusted.Value)
                _AdjustValCurrCycle.Value += Math.Abs(adjust.QtyAdjusted.Value * adjust.Price.Value)
            End If

            If Not SaveIfExists() Then SaveIfNew()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function AveragePreviousAdjustQty() As Decimal

        Return (_AdjustQtyPrevCycle.Value + _AdjustQtyCycle2Ago.Value + _AdjustQtyCycle3Ago.Value) / 3

    End Function

#End Region

End Class
