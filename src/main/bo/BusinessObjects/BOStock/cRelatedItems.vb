﻿<Serializable()> Public Class cRelatedItems
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RELITM"
        BOFields.Add(_SingleItem)
        BOFields.Add(_BulkOrPackageItem)
        BOFields.Add(_Deleted)
        BOFields.Add(_NoSinglesPerPack)
        BOFields.Add(_SinglesSold)
        BOFields.Add(_SinglesSoldPTD)
        BOFields.Add(_SinglesSoldYTD)
        BOFields.Add(_SinglesSoldPYr)
        BOFields.Add(_TransForMUPThisWeek)
        BOFields.Add(_TransForMUPPriorWeek)
        BOFields.Add(_MarkUpThisWeek)
        BOFields.Add(_MarkUpPriorWeek)
        BOFields.Add(_MarkUpPeriodTD)
        BOFields.Add(_MarkUpPriorPeriod)
        BOFields.Add(_MarkUpYTD)
        BOFields.Add(_MarkUPPriorYR)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cRelatedItems)

        LoadBORecords(count)

        Dim col As New List(Of cRelatedItems)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cRelatedItems))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cRelatedItems(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SingleItem As New ColField(Of String)("SPOS", "", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Single Item")
    Private _BulkOrPackageItem As New ColField(Of String)("PPOS", "", False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Bulk Or Package Item")
    Private _Deleted As New ColField(Of Boolean)("DELC", False, False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Deleted")
    Private _NoSinglesPerPack As New ColField(Of Decimal)("NSPP", 0, False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "No Singles Per Pack")
    Private _SinglesSold As New ColField(Of Decimal)("QTYS", 0, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Singles Sold")
    Private _SinglesSoldPTD As New ColField(Of Decimal)("PTDQ", 0, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Singles Sold PTD")
    Private _SinglesSoldYTD As New ColField(Of Decimal)("YTDQ", 0, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Singles Sold YTD")
    Private _SinglesSoldPYr As New ColField(Of Decimal)("PYRQ", 0, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Singles Sold PYr")
    Private _TransForMUPThisWeek As New ColField(Of Decimal)("WTDS", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Trans For MUP This Week")
    Private _TransForMUPPriorWeek As New ColField(Of Decimal)("PWKS", 0, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Trans For MUP Prior Week")
    Private _MarkUpThisWeek As New ColField(Of Decimal)("WTDM", 0, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Mark Up This Week")
    Private _MarkUpPriorWeek As New ColField(Of Decimal)("PWKM", 0, False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Mark Up Prior Week")
    Private _MarkUpPeriodTD As New ColField(Of Decimal)("PTDM", 0, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Mark Up Period TD")
    Private _MarkUpPriorPeriod As New ColField(Of Decimal)("PPDM", 0, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Mark Up Prior Period")
    Private _MarkUpYTD As New ColField(Of Decimal)("YTDM", 0, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Mark Up YTD")
    Private _MarkUPPriorYR As New ColField(Of Decimal)("PYRM", 0, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Mark Up YR")

#End Region

#Region "Field Properties"

    Public Property SingleItem() As ColField(Of String)
        Get
            SingleItem = _SingleItem
        End Get
        Set(ByVal value As ColField(Of String))
            _SingleItem = value
        End Set
    End Property

    Public Property BulkOrPackageItem() As ColField(Of String)
        Get
            BulkOrPackageItem = _BulkOrPackageItem
        End Get
        Set(ByVal value As ColField(Of String))
            _BulkOrPackageItem = value
        End Set
    End Property

    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Deleted = value
        End Set
    End Property

    Public Property NoSinglesPerPack() As ColField(Of Decimal)
        Get
            NoSinglesPerPack = _NoSinglesPerPack
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NoSinglesPerPack = value
        End Set
    End Property

    Public Property SinglesSold() As ColField(Of Decimal)
        Get
            SinglesSold = _SinglesSold
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SinglesSold = value
        End Set
    End Property

    Public Property SinglesSoldPTD() As ColField(Of Decimal)
        Get
            SinglesSoldPTD = _SinglesSoldPTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SinglesSoldPTD = value
        End Set
    End Property

    Public Property SinglesSoldYTD() As ColField(Of Decimal)
        Get
            SinglesSoldYTD = _SinglesSoldYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SinglesSoldYTD = value
        End Set
    End Property

    Public Property SinglesSoldPYr() As ColField(Of Decimal)
        Get
            SinglesSoldPYr = _SinglesSoldPYr
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SinglesSoldPYr = value
        End Set
    End Property

    Public Property TransForMUPThisWeek() As ColField(Of Decimal)
        Get
            TransForMUPThisWeek = _TransForMUPThisWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TransForMUPThisWeek = value
        End Set
    End Property

    Public Property TransForMUPPriorWeek() As ColField(Of Decimal)
        Get
            TransForMUPPriorWeek = _TransForMUPPriorWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TransForMUPPriorWeek = value
        End Set
    End Property

    Public Property MarkUpThisWeek() As ColField(Of Decimal)
        Get
            MarkUpThisWeek = _MarkUpThisWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpThisWeek = value
        End Set
    End Property

    Public Property MarkUpPriorWeek() As ColField(Of Decimal)
        Get
            MarkUpPriorWeek = _MarkUpPriorWeek
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpPriorWeek = value
        End Set
    End Property

    Public Property MarkUpPeriodTD() As ColField(Of Decimal)
        Get
            MarkUpPeriodTD = _MarkUpPeriodTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpPeriodTD = value
        End Set
    End Property

    Public Property MarkUpPriorPeriod() As ColField(Of Decimal)
        Get
            MarkUpPriorPeriod = _MarkUpPriorPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpPriorPeriod = value
        End Set
    End Property

    Public Property MarkUpYTD() As ColField(Of Decimal)
        Get
            MarkUpYTD = _MarkUpYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpYTD = value
        End Set
    End Property

    Public Property MarkUPPriorYR() As ColField(Of Decimal)
        Get
            MarkUPPriorYR = _MarkUPPriorYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUPPriorYR = value
        End Set
    End Property

#End Region

#Region "Methods"

    Private _Items As List(Of cRelatedItems) = Nothing

    Public Property Items() As List(Of cRelatedItems)
        Get
            Return _Items
        End Get
        Set(ByVal value As List(Of cRelatedItems))
            _Items = value
        End Set
    End Property

    ''' <summary>
    ''' Loads all related items into Items collection.
    ''' </summary>
    ''' <param name="skuNumber">Sku number to use as selection criteria</param>
    ''' <param name="singleSku">Set true if passing single sku number or false for bulk sku number</param>
    ''' <remarks></remarks>
    Public Sub LoadRelatedItems(ByVal skuNumber As String, Optional ByVal singleSku As Boolean = True)

        Dim relItem As New cRelatedItems(Oasys3DB)

        'check whether to get bulk item sku number
        If singleSku Then
            relItem.AddLoadFilter(clsOasys3DB.eOperator.pEquals, relItem.SingleItem, skuNumber)
            relItem.LoadMatches()
            skuNumber = relItem.BulkOrPackageItem.Value
        End If

        'get all single related items for this bulk sku number
        relItem.ClearLists()
        relItem.AddLoadFilter(clsOasys3DB.eOperator.pEquals, relItem.BulkOrPackageItem, skuNumber)
        _Items = relItem.LoadMatches

    End Sub

    ''' <summary>
    ''' Loads all related items into Items collection.
    ''' </summary>
    ''' <param name="stock"></param>
    ''' <remarks></remarks>
    Public Sub LoadRelatedItems(ByRef stock As cStock)

        Dim relItem As New cRelatedItems(Oasys3DB)
        Dim sku As String = String.Empty

        'check whether to get bulk item sku number
        Select Case True

            Case stock.RelatedItemSingle.Value
                relItem.AddLoadFilter(clsOasys3DB.eOperator.pEquals, relItem.SingleItem, sku)
                relItem.LoadMatches()
                sku = relItem.BulkOrPackageItem.Value

            Case stock.RelatedNoItems.Value > 0
                sku = stock.SkuNumber.Value
        End Select

        'get all single related items for this bulk sku number
        If sku <> String.Empty Then
            relItem.ClearLists()
            relItem.AddLoadFilter(clsOasys3DB.eOperator.pEquals, relItem.BulkOrPackageItem, sku)
            _Items = relItem.LoadMatches
        End If

    End Sub

    Public Function GetSingleSkuNumbers(ByVal bulkSkuNumber As String) As ArrayList

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnParameter(_SingleItem.ColumnName)
        Oasys3DB.SetWhereParameter(_BulkOrPackageItem.ColumnName, clsOasys3DB.eOperator.pEquals, bulkSkuNumber)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)
        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(0)) Then
                Dim skus As New ArrayList
                For Each dr As DataRow In dt.Rows
                    skus.Add(dr.Item(0))
                Next

                Return skus
            End If
        End If

        Return Nothing

    End Function

    Public Function GetBulkSkuNumber(ByVal singleSkuNumber As String) As String

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnParameter(_BulkOrPackageItem.ColumnName)
        Oasys3DB.SetWhereParameter(_SingleItem.ColumnName, clsOasys3DB.eOperator.pEquals, singleSkuNumber)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)
        Dim relItemsImplementation As IRelatedItems = (New RelatedItemsFactory).GetImplementation

        If relItemsImplementation.GetBulkSkuNumber_Datatable_HasData(dt) Then
            Return dt.Rows(0)(0).ToString
        End If

        Return Nothing
    End Function

    Public Sub GetRelatedItems(ByRef stocks As List(Of cStock))

        For index As Integer = 0 To stocks.Count - 1
            For Each relatedStock As cStock In GetRelatedItems(stocks(index))
                Dim Sku As String = relatedStock.SkuNumber.Value
                If Not stocks.Any(Function(x As cStock)
                                      Return x.SkuNumber.Value = Sku
                                  End Function) Then
                    stocks.Add(relatedStock)
                End If
            Next
        Next

    End Sub

    Public Function GetRelatedItems(ByRef stock As cStock) As List(Of cStock)

        'get any related items
        Dim relatedItems As New List(Of cStock)
        Dim singleSkus As ArrayList = Nothing
        If stock.RelatedItemSingle.Value Then
            Dim bulkSku As String = GetBulkSkuNumber(stock.SkuNumber.Value)
            Dim relItemsImplementation As IRelatedItems = (New RelatedItemsFactory).GetImplementation

            singleSkus = relItemsImplementation.GetRelatedSkuList_FromBulkSkuNumber(Me, bulkSku)
        End If

        If stock.RelatedNoItems.Value > 0 Then
            singleSkus = GetSingleSkuNumbers(stock.SkuNumber.Value)
        End If

        If singleSkus IsNot Nothing AndAlso singleSkus.Count > 0 Then
            For Each sku As String In singleSkus
                If sku <> stock.SkuNumber.Value Then
                    'load stock item and add to details
                    Dim newStock As New BOStock.cStock(Oasys3DB)
                    newStock.LoadStockItem(sku, False)
                    If newStock.BORecords.Any() Then
                        relatedItems.Add(newStock)
                    End If
                End If
            Next
        End If

        Return relatedItems

    End Function

    Public Function GetRelatedItem(ByVal strPartCode As String) As List(Of cRelatedItems)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_SingleItem.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, strPartCode)
        Return LoadMatches()

    End Function

    Public Function GetRelatedSingleItem(ByVal strPartCode As String) As List(Of cRelatedItems)

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SingleItem, strPartCode)
        Return LoadMatches()

    End Function

    Public Function ProcessPeriodEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        Oasys3DB.SetColumnAndValueParameter(MarkUpPriorPeriod.ColumnName, MarkUpPeriodTD) 'move values

        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        If SaveError = False Then
            'zeroise the fields 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(MarkUpPeriodTD.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(SinglesSoldPTD.ColumnName, 0)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If
        End If

        Return SaveError

    End Function

    Public Function ProcessYearEnd() As Boolean

        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        Oasys3DB.SetColumnAndValueParameter(MarkUPPriorYR.ColumnName, MarkUpYTD) 'move values
        Oasys3DB.SetColumnAndValueParameter(SinglesSoldPYr.ColumnName, SinglesSoldYTD) 'move values
        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        If SaveError = False Then
            'zeroise the fields 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(MarkUpYTD.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(SinglesSoldYTD.ColumnName, 0)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If
        End If

        Return SaveError

    End Function

    Public Function GetActiveItems() As List(Of cRelatedItems)
        Dim lstRelatedItems As List(Of cRelatedItems)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Deleted, 0)
        lstRelatedItems = LoadMatches()
        Return lstRelatedItems
    End Function

    Public Function RemoveItem(ByVal spos As String, ByVal ppos As String) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(SingleItem.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, spos)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(BulkOrPackageItem.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, ppos)

        NoRecs = Oasys3DB.Delete()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function UpdateItemsSold(ByVal strSPOS As String, ByVal strPPOS As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetColumnAndValueParameter(SinglesSold.ColumnName, SinglesSold.Value)
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetWhereParameter(BulkOrPackageItem.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, strSPOS)
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(BulkOrPackageItem.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, strPPOS)
        Return CBool(Oasys3DB.Update())

    End Function
#End Region

End Class
