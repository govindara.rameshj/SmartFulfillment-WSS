﻿Public Class RelatedItemsNew_For_GetRelatedItems
    Implements IRelatedItems

    Public Function GetBulkSkuNumber_Datatable_HasData(ByRef GetBulkSkuNumberDatatable As System.Data.DataTable) As Boolean Implements IRelatedItems.GetBulkSkuNumber_Datatable_HasData

        If GetBulkSkuNumberDatatable IsNot Nothing Then
            With GetBulkSkuNumberDatatable
                If .Rows.Count > 0 AndAlso Not IsDBNull(.Rows(0)(0)) Then
                    Return True
                End If
            End With
        End If
    End Function

    Public Function GetRelatedSkuList_FromBulkSkuNumber(ByRef relItems As cRelatedItems, ByVal BulkSkuNumber As String) As System.Collections.ArrayList Implements IRelatedItems.GetRelatedSkuList_FromBulkSkuNumber
        Dim skuList As ArrayList

        If relItems IsNot Nothing And Not String.IsNullOrEmpty(BulkSkuNumber) Then
            skuList = relItems.GetSingleSkuNumbers(BulkSkuNumber)
            If skuList Is Nothing Then
                skuList = New ArrayList
            End If
            skuList.Add(BulkSkuNumber)
        Else
            skuList = New ArrayList
        End If

        Return skuList
    End Function
End Class
