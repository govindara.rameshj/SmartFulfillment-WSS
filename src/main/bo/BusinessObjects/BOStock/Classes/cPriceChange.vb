﻿<Serializable()> Public Class cPriceChange
    Inherits cBaseClass
    Implements IPriceChange

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start() Implements IPriceChange.Start

        TableName = "PRCCHG"
        BOFields.Add(_SkuNumber)
        BOFields.Add(_EffectiveDate)
        BOFields.Add(_NewPrice)
        BOFields.Add(_ChangeStatus)
        BOFields.Add(_ShelfLabelPrinted)
        BOFields.Add(_AutoApplyDate)
        BOFields.Add(_AutoAppliedDate)
        BOFields.Add(_MarkUpChange)
        BOFields.Add(_CommedToHo)
        BOFields.Add(_SmallLabel)
        BOFields.Add(_MediumLabel)
        BOFields.Add(_LargeLabel)
        BOFields.Add(_EventNumber)
        BOFields.Add(_EventPriority)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_ManagerID)

    End Sub

    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPriceChange) Implements IPriceChange.LoadMatches

        LoadBORecords(count)

        Dim col As New List(Of cPriceChange)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cPriceChange))
        Next

        Return col

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1) Implements IPriceChange.LoadBORecords

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.ResetValues()
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPriceChange(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "Sku Number", True, False)
    Private _EffectiveDate As New ColField(Of Date)("PDAT", Nothing, "Effective Date", True, False)
    Private _NewPrice As New ColField(Of Decimal)("PRIC", 0, "New Price", False, False)
    Private _ChangeStatus As New ColField(Of String)("PSTA", "", "Change Status", False, False)
    Private _ShelfLabelPrinted As New ColField(Of Boolean)("SHEL", False, "Shelf Label Printed", False, False)
    Private _AutoApplyDate As New ColField(Of Date)("AUDT", Nothing, "Auto Apply Date", False, False)
    Private _AutoAppliedDate As New ColField(Of Date)("AUAP", Nothing, "Auto Applied Date", False, False)
    Private _MarkUpChange As New ColField(Of Decimal)("MARK", 0, "Mark Up Change", False, False)
    Private _CommedToHo As New ColField(Of Boolean)("MCOM", False, "Commed To HO", False, False)
    Private _SmallLabel As New ColField(Of Boolean)("LABS", False, "Peg Label", False, False)
    Private _MediumLabel As New ColField(Of Boolean)("LABM", False, "Small Label", False, False)
    Private _LargeLabel As New ColField(Of Boolean)("LABL", False, "Medium Label", False, False)
    Private _EventNumber As New ColField(Of String)("EVNT", "", "Event Number", False, False)
    Private _EventPriority As New ColField(Of String)("PRIO", "", "Event Priority", False, False)
    Private _EmployeeID As New ColField(Of String)("EEID", "000", "Employee ID", False, False)
    Private _ManagerID As New ColField(Of String)("MEID", "000", "Manager ID", False, False)

#End Region

#Region "Public Properties"

    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property EffectiveDate() As ColField(Of Date)
        Get
            EffectiveDate = _EffectiveDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EffectiveDate = value
        End Set
    End Property
    Public Property NewPrice() As ColField(Of Decimal)
        Get
            NewPrice = _NewPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NewPrice = value
        End Set
    End Property
    Public Property ChangeStatus() As ColField(Of String)
        Get
            ChangeStatus = _ChangeStatus
        End Get
        Set(ByVal value As ColField(Of String))
            _ChangeStatus = value
        End Set
    End Property
    Public Property ShelfLablePrinted() As ColField(Of Boolean)
        Get
            ShelfLablePrinted = _ShelfLabelPrinted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ShelfLabelPrinted = value
        End Set
    End Property
    Public Property AutoApplyDate() As ColField(Of Date)
        Get
            AutoApplyDate = _AutoApplyDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AutoApplyDate = value
        End Set
    End Property
    Public Property AutoAppliedDate() As ColField(Of Date)
        Get
            AutoAppliedDate = _AutoAppliedDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AutoAppliedDate = value
        End Set
    End Property
    Public Property MarkUpChange() As ColField(Of Decimal)
        Get
            MarkUpChange = _MarkUpChange
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MarkUpChange = value
        End Set
    End Property
    Public Property CommedToHo() As ColField(Of Boolean)
        Get
            CommedToHo = _CommedToHo
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommedToHo = value
        End Set
    End Property
    Public Property SmallLabel() As ColField(Of Boolean)
        Get
            SmallLabel = _SmallLabel
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SmallLabel = value
        End Set
    End Property
    Public Property MediumLabel() As ColField(Of Boolean)
        Get
            MediumLabel = _MediumLabel
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _MediumLabel = value
        End Set
    End Property
    Public Property LargeLabel() As ColField(Of Boolean)
        Get
            LargeLabel = _LargeLabel
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LargeLabel = value
        End Set
    End Property
    Public Property EventNo() As ColField(Of String)
        Get
            EventNo = _EventNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventNumber = value
        End Set
    End Property
    Public Property EventPriority() As ColField(Of String)
        Get
            EventPriority = _EventPriority
        End Get
        Set(ByVal value As ColField(Of String))
            _EventPriority = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)
        Get
            EmployeeID = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property ManagerID() As ColField(Of String)
        Get
            ManagerID = _ManagerID
        End Get
        Set(ByVal value As ColField(Of String))
            _ManagerID = value
        End Set
    End Property

#End Region

#Region "Entities"

    'Private _PriceChanges As New List(Of cPriceChange)
    Protected PriceChangesField As New List(Of cPriceChange) 'child class PriceChange requires access
    Private _Stock As cStock = Nothing

    Public Property PriceChanges() As List(Of cPriceChange) Implements IPriceChange.PriceChanges
        Get
            Return PriceChangesField
        End Get
        Set(ByVal value As List(Of cPriceChange))
            PriceChangesField = value
        End Set
    End Property

    Public Property Stock() As cStock Implements IPriceChange.Stock
        Get
            If _Stock Is Nothing Then
                _Stock = New cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As cStock)
            _Stock = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all effective price changes for given parameter into PriceChanges collection of this instance.
    ''' </summary>
    ''' <param name="applyDate"></param>
    ''' <remarks></remarks>
    Public Overridable Sub Load(ByVal applyDate As Date) Implements IPriceChange.Load

        Dim Repository As IPriceChangeRepository
        Dim DT As DataTable
        Dim Row As cPriceChange

        'get data
        Repository = PriceChangeRepositoryFactory.FactoryGet
        DT = Repository.Load(applyDate)

        'load business object
        PriceChangesField = New List(Of cPriceChange)

        For Each DR As DataRow In DT.Rows

            Row = New cPriceChange(Oasys3DB)
            Row.LoadFromRow(DR)
            PriceChangesField.Add(Row)
        Next

    End Sub

    ''' <summary>
    ''' Assigns given stock items to price change objects in PriceChanges collection of this instance.
    ''' </summary>
    ''' <param name="stocks"></param>
    ''' <remarks></remarks>
    Public Sub AssignStocks(ByVal stocks As List(Of cStock)) Implements IPriceChange.AssignStocks

        If PriceChangesField Is Nothing Then Exit Sub

        'assign stocks to price change records
        For Each price As BOStock.cPriceChange In PriceChangesField
            For Each stock As BOStock.cStock In stocks
                If price.SkuNumber.Value = stock.SkuNumber.Value Then
                    price.Stock = stock
                    Exit For
                End If
            Next
        Next

    End Sub

    ''' <summary>
    ''' Applies price change to this instance.
    ''' </summary>
    ''' <param name="Tomorrow"></param>
    ''' <param name="Today"></param>
    ''' <param name="UserID"></param>
    ''' <param name="AutoChangePriceUpdate"></param>
    ''' <remarks></remarks>
    Public Sub ApplyPriceChange(ByVal tomorrow As Date, ByVal today As Date, ByVal userID As Integer, ByVal autoChangePriceUpdate As Boolean) Implements IPriceChange.ApplyPriceChange

        Trace.WriteLine("Starting ApplyPriceChange Routine.")

        'check stock exists
        If _Stock Is Nothing Then
            Trace.WriteLine("Stock item does not exist for sku: " & _SkuNumber.Value, Me.GetType.ToString)
            Exit Sub
        End If

        Dim onHand As Integer = Stock.StockOnHand.Value
        Dim markdowns As Integer = Stock.MarkDownQuantity.Value
        Dim newPrice As Decimal = _NewPrice.Value
        Dim oldPrice As Decimal = Stock.NormalSellPrice.Value

        Trace.WriteLine("We are about to Apply thge price change.")

        _AutoAppliedDate.Value = today
        Trace.WriteLine("AutoAppliedDate = " & today.ToString)

        _ChangeStatus.Value = "S"
        Trace.WriteLine("ChangeStatus = 'S' ")

        _EmployeeID.Value = userID.ToString("000")
        _ManagerID.Value = userID.ToString("000")

        'check if stock qty > 0
        Dim qty As Integer = onHand + markdowns + Stock.WriteOffQuantity.Value
        If qty > 0 Then _MarkUpChange.Value = qty * (newPrice - oldPrice)

        Try
            'start transaction
            Oasys3DB.BeginTransaction()

            'save price change record
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_AutoAppliedDate.ColumnName, _AutoAppliedDate.Value)
            Oasys3DB.SetColumnAndValueParameter(_ChangeStatus.ColumnName, _ChangeStatus.Value)
            Oasys3DB.SetColumnAndValueParameter(_EmployeeID.ColumnName, _EmployeeID.Value)
            Oasys3DB.SetColumnAndValueParameter(_ManagerID.ColumnName, _ManagerID.Value)
            Oasys3DB.SetColumnAndValueParameter(_MarkUpChange.ColumnName, _MarkUpChange.Value)
            Oasys3DB.SetColumnAndValueParameter(_SmallLabel.ColumnName, _SmallLabel.Value)
            Oasys3DB.SetColumnAndValueParameter(_MediumLabel.ColumnName, _MediumLabel.Value)
            Oasys3DB.SetColumnAndValueParameter(_LargeLabel.ColumnName, _LargeLabel.Value)
            SetDBKeys()
            Oasys3DB.Update()

            'update stock item and inserts stock log entry
            Stock.UpdatePriceChanges(Me)

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Removes multiple price changes from PriceChanges collection.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub RemoveMultiples() Implements IPriceChange.RemoveMultiples

        If PriceChangesField Is Nothing Then Exit Sub
        Dim multiples As New List(Of cPriceChange)

        'get any multiples
        For Each price As cPriceChange In PriceChangesField
            If price.Stock.AutoApplyPriceChgs.Value Then Continue For
            If price.Stock.ItemObsolete.Value AndAlso (price.Stock.StockOnHand.Value > 0 Or price.Stock.MarkDownQuantity.Value > 0) Then Continue For
            If price.Stock.NonStockItem.Value AndAlso (price.Stock.StockOnHand.Value > 0 Or price.Stock.MarkDownQuantity.Value > 0) Then Continue For

            For Each checkprice As cPriceChange In PriceChangesField
                If checkprice Is price Then Continue For
                If checkprice.SkuNumber.Value = price.SkuNumber.Value Then
                    multiples.Add(checkprice)
                End If
            Next
        Next

        'remove multiples from priceChanges
        For Each price As cPriceChange In multiples
            Dim priceloop As cPriceChange = price
            PriceChangesField.RemoveAll(Function(p As cPriceChange) p Is priceloop)
        Next

    End Sub

    Public Function MarkUpRec() As List(Of cPriceChange) Implements IPriceChange.MarkUpRec

        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CommedToHo, "0")
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, _ChangeStatus, "U")
        MarkUpRec = LoadMatches()

    End Function

    Public Function DeleteUnapplied() As Boolean Implements IPriceChange.DeleteUnapplied

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.SetWhereParameter(_ChangeStatus.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, "U")
        Return (Oasys3DB.Delete() <> -1)

    End Function

    Public Function DeleteUnappliedBySku(ByVal Sku As String) As Boolean Implements IPriceChange.DeleteUnappliedBySku

        Oasys3DB.ExecuteSql("DELETE FROM PRCCHG WHERE SKUN='" & Sku & "' AND PSTA='U'")
        Return True

    End Function

    Public Function AddRecord(ByVal Sku As String, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean Implements IPriceChange.AddRecord

        Dim applyDate As Date = DateAdd(DateInterval.Day, daysafter, startDate)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(PartCode.ColumnName, Sku)
        Oasys3DB.SetColumnAndValueParameter(EffectiveDate.ColumnName, startDate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, False)
        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Return (Oasys3DB.Insert() <> -1)

    End Function

    Public Function AddRecord(ByVal stockItem As cStock, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean Implements IPriceChange.AddRecord

        Dim applyDate As Date = DateAdd(DateInterval.Day, daysafter, startDate)

        'check if can update first
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Oasys3DB.SetWhereParameter(PartCode.ColumnName, clsOasys3DB.eOperator.pEquals, stockItem.SkuNumber.Value)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(EffectiveDate.ColumnName, clsOasys3DB.eOperator.pEquals, startDate)
        If Oasys3DB.Update() > 0 Then Return True

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(PartCode.ColumnName, stockItem.SkuNumber.Value)
        Oasys3DB.SetColumnAndValueParameter(EffectiveDate.ColumnName, startDate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Return (Oasys3DB.Insert() <> -1)

    End Function

    Public Function AddRecordUpdPriceChange(ByVal stockItem As cStock, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean Implements IPriceChange.AddRecordUpdPriceChange

        Dim applyDate As Date = DateAdd(DateInterval.Day, daysafter, startDate)

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, stockItem.SkuNumber.Value)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EffectiveDate, startDate)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, _EventNumber, eventNum)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ChangeStatus, "U")
        Dim PriceChg As List(Of cPriceChange)
        PriceChg = LoadMatches()
        If PriceChg.Count > 0 Then Return False

        Oasys3DB.ClearAllParameters()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, stockItem.SkuNumber.Value)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EffectiveDate, startDate)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, _EventNumber, eventNum)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ChangeStatus, "U")
        PriceChg = LoadMatches()

        If PriceChg.Count > 0 Then
            'DELETE
            Dim strSQL As String = "DELETE FROM PRCCHG WHERE SKUN='" + stockItem.SkuNumber.Value.ToString + "' AND PDAT='" + startDate.ToString("yyyy-MM-dd") + "'"
            Oasys3DB.ExecuteSql(strSQL)
        End If

        'check if can update first
        Oasys3DB.ClearAllParameters()
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SkuNumber, stockItem.SkuNumber.Value)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, _EffectiveDate, startDate)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _EventNumber, eventNum)
        JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, _ChangeStatus, "U")
        PriceChg = LoadMatches()
        If PriceChg.Count > 0 Then Return False

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        If (stockItem.NonStockItem.Value = True Or stockItem.AutoApplyPriceChgs.Value = True Or stockItem.ItemObsolete.Value = True) AndAlso ((stockItem.StockOnHand.Value + stockItem.MarkDownQuantity.Value) = 0) Then
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(ShelfLablePrinted.ColumnName, 1)
        Else
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        End If
        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Oasys3DB.SetWhereParameter(PartCode.ColumnName, clsOasys3DB.eOperator.pEquals, stockItem.SkuNumber.Value)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(EffectiveDate.ColumnName, clsOasys3DB.eOperator.pEquals, startDate)
        If Oasys3DB.Update() > 0 Then
            Dim strSQL As String = "DELETE FROM PRCCHG WHERE SKUN='" + stockItem.SkuNumber.Value.ToString + "' AND PDAT='" + startDate.ToString("yyyy-MM-dd") + "'"
            Oasys3DB.ExecuteSql(strSQL)
        End If


        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(PartCode.ColumnName, stockItem.SkuNumber.Value)
        Oasys3DB.SetColumnAndValueParameter(EffectiveDate.ColumnName, startDate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        If (stockItem.AutoApplyPriceChgs.Value = True Or stockItem.ItemObsolete.Value = True) AndAlso stockItem.StockOnHand.Value = 0 Then
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(ShelfLablePrinted.ColumnName, 1)
        Else
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        End If

        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Return (Oasys3DB.Insert() <> -1)

    End Function

#End Region

End Class