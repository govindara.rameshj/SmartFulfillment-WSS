﻿Public Class RelatedItemsExisting_For_GetRelatedItems
    Implements IRelatedItems

    Public Function GetBulkSkuNumber_Datatable_HasData(ByRef GetBulkSkuNumberDatatable As System.Data.DataTable) As Boolean Implements IRelatedItems.GetBulkSkuNumber_Datatable_HasData

        If GetBulkSkuNumberDatatable IsNot Nothing Then
            With GetBulkSkuNumberDatatable
                If .Rows.Count > 0 AndAlso IsDBNull(.Rows(0)(0)) Then
                    Return True
                End If
            End With
        End If
    End Function

    Public Function GetRelatedSkuList_FromBulkSkuNumber(ByRef relItems As cRelatedItems, ByVal BulkSkuNumber As String) As System.Collections.ArrayList Implements IRelatedItems.GetRelatedSkuList_FromBulkSkuNumber

        If relItems IsNot Nothing Then
            Return relItems.GetSingleSkuNumbers(BulkSkuNumber)
        Else
            Return Nothing
        End If
    End Function
End Class
