﻿Public Class PriceChange
    Inherits cPriceChange
    Implements IPriceChange

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
    End Sub

    Public Overloads Function AddRecordUpdPriceChange(ByVal stockItem As cStock, ByVal startDate As Date, ByVal priority As Integer, ByVal eventNum As String, ByVal price As Decimal, ByVal daysafter As Integer) As Boolean Implements IPriceChange.AddRecordUpdPriceChange

        Dim applyDate As Date = DateAdd(DateInterval.Day, daysafter, startDate)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        If (stockItem.NonStockItem.Value = True Or stockItem.AutoApplyPriceChgs.Value = True Or stockItem.ItemObsolete.Value = True) AndAlso ((stockItem.StockOnHand.Value + stockItem.MarkDownQuantity.Value) <= 0) Then
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(ShelfLablePrinted.ColumnName, 1)
        Else
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        End If
        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Oasys3DB.SetWhereParameter(PartCode.ColumnName, clsOasys3DB.eOperator.pEquals, stockItem.SkuNumber.Value)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(EffectiveDate.ColumnName, clsOasys3DB.eOperator.pEquals, startDate)
        If Oasys3DB.Update() > 0 Then
            Dim strSql As String = "DELETE FROM PRCCHG WHERE SKUN='" + stockItem.SkuNumber.Value.ToString + "' AND PDAT='" + startDate.ToString("yyyy-MM-dd") + "'"
            Oasys3DB.ExecuteSql(strSql)
        End If

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(PartCode.ColumnName, stockItem.SkuNumber.Value)
        Oasys3DB.SetColumnAndValueParameter(EffectiveDate.ColumnName, startDate)
        Oasys3DB.SetColumnAndValueParameter(EventPriority.ColumnName, priority)
        Oasys3DB.SetColumnAndValueParameter(EventNo.ColumnName, eventNum)
        Oasys3DB.SetColumnAndValueParameter(NewPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(AutoApplyDate.ColumnName, applyDate)
        If (stockItem.NonStockItem.Value = True Or stockItem.AutoApplyPriceChgs.Value = True Or stockItem.ItemObsolete.Value = True) AndAlso ((stockItem.StockOnHand.Value + stockItem.MarkDownQuantity.Value) <= 0) Then
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, 0)
            Oasys3DB.SetColumnAndValueParameter(ShelfLablePrinted.ColumnName, 1)
        Else
            Oasys3DB.SetColumnAndValueParameter(SmallLabel.ColumnName, stockItem.NoOfSmallLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(MediumLabel.ColumnName, stockItem.NoOfMediumLabels.Value)
            Oasys3DB.SetColumnAndValueParameter(LargeLabel.ColumnName, stockItem.NoOfLargeLabels.Value)
        End If

        Oasys3DB.SetColumnAndValueParameter(ChangeStatus.ColumnName, "U")
        Return (Oasys3DB.Insert() <> -1)

    End Function


End Class