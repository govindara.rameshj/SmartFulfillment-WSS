﻿<Serializable()> Public Class cHalfHourlyTrades
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "HTRSUM"
        BOFields.Add(_TransactionDate)
        BOFields.Add(_StartTime)
        BOFields.Add(_EndTime)
        BOFields.Add(_NoOfSales)
        BOFields.Add(_ValueOfSales)
        BOFields.Add(_NoOfRefund)
        BOFields.Add(_ValueOfRefund)
        BOFields.Add(_TimeSlotID)
        BOFields.Add(_CommedToHO)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cHalfHourlyTrades)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cHalfHourlyTrades)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cHalfHourlyTrades))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cHalfHourlyTrades(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("DATE1") : _TransactionDate.Value = CDate(drRow(dcColumn))
                    Case ("STIM") : _StartTime.Value = CStr(drRow(dcColumn))
                    Case ("ETIM") : _EndTime.Value = CStr(drRow(dcColumn))
                    Case ("NSAL") : _NoOfSales.Value = CInt(drRow(dcColumn))
                    Case ("NSAL") : _ValueOfSales.Value = CDec(drRow(dcColumn))
                    Case ("NREF") : _NoOfRefund.Value = CInt(drRow(dcColumn))
                    Case ("VREF") : _ValueOfRefund.Value = CDec(drRow(dcColumn))
                    Case ("SLOT") : _TimeSlotID.Value = CInt(drRow(dcColumn))
                    Case ("COMM") : _CommedToHO.Value = CBool(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _TransactionDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _StartTime As New ColField(Of String)("STIM", "000000", "Starting Time", True, False)
    Private _EndTime As New ColField(Of String)("ETIM", "000000", "Ending Time", False, False)
    Private _NoOfSales As New ColField(Of Integer)("NSAL", 0, "No of Sales", False, False)
    Private _ValueOfSales As New ColField(Of Decimal)("VSAL", 0, "Value of Sales", False, False)
    Private _NoOfRefund As New ColField(Of Integer)("NREF", 0, "No of Refunds", False, False)
    Private _ValueOfRefund As New ColField(Of Decimal)("VREF", 0, "Value of Refunds", False, False)
    Private _TimeSlotID As New ColField(Of Integer)("SLOT", 0, "Time Slot", False, False)
    Private _CommedToHO As New ColField(Of Boolean)("COMM", false, "Commed To HO", False, False)

#End Region

#Region "Field Properties"

    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = _TransactionDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TransactionDate = value
        End Set
    End Property
    Public Property StartTime() As ColField(Of String)
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal value As ColField(Of String))
            _StartTime = value
        End Set
    End Property
    Public Property EndTime() As ColField(Of String)
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal value As ColField(Of String))
            _EndTime = value
        End Set
    End Property
    Public Property NoOfSales() As ColField(Of Integer)
        Get
            NoOfSales = _NoOfSales
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NoOfSales = value
        End Set
    End Property
    Public Property ValueOfSales() As ColField(Of Decimal)
        Get
            ValueOfSales = _ValueOfSales
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueOfSales = value
        End Set
    End Property
    Public Property NoOfRefund() As ColField(Of Integer)
        Get
            NoOfRefund = _NoOfRefund
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NoOfRefund = value
        End Set
    End Property
    Public Property ValueOfRefund() As ColField(Of Decimal)
        Get
            ValueOfRefund = _ValueOfRefund
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ValueOfRefund = value
        End Set
    End Property
    Public Property TimeSlotID() As ColField(Of Integer)
        Get
            TimeSlotID = _TimeSlotID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TimeSlotID = value
        End Set
    End Property
    Public Property CommedToHO() As ColField(Of Boolean)
        Get
            CommedToHO = _CommedToHO
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommedToHO = value
        End Set
    End Property

#End Region

#Region "Entities"

#End Region

#Region "Methods"

    Public Function GetTodaysUnprocessedRecords(ByRef dteLiveDate As Date) As List(Of cHalfHourlyTrades)

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CommedToHO, False)
            Return LoadMatches()

        Catch ex As Exception

            Throw ex
            Return Nothing

        End Try

    End Function

    Public Function UpdateCommedFlag() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_CommedToHO.ColumnName, True)
            Oasys3DB.SetWhereParameter(_StartTime.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _StartTime.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_TransactionDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TransactionDate.Value)
            If CBool(Oasys3DB.Update()) Then Return True
            Return False

        Catch ex As Exception
            Return False
        End Try

    End Function

#End Region

End Class
