﻿<Serializable()> Public Class cSalesPaid
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLPAID"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_TenderType)
        BOFields.Add(_TenderAmount)
        BOFields.Add(_CreditCardNo)
        BOFields.Add(_CreditCardExpiryDate)
        BOFields.Add(_CouponNumber)
        BOFields.Add(_AuthorisationCode)
        BOFields.Add(_CCNumberKeyed)
        BOFields.Add(_SupervisorCode)
        BOFields.Add(_CouponPostCode)
        BOFields.Add(_ChequeAccountNo)
        BOFields.Add(_ChequeSortCode)
        BOFields.Add(_ChequeNumber)
        BOFields.Add(_DBRProcessed)
        BOFields.Add(_EftposVoucherNo)
        BOFields.Add(_IssueNumber)
        BOFields.Add(_AutorisationType)
        BOFields.Add(_EftposMerchantNumber)
        BOFields.Add(_CashBackAmount)
        BOFields.Add(_DigitCount)
        BOFields.Add(_EftposCommsPrep)
        BOFields.Add(_CardDescription)
        BOFields.Add(_EftposID)
        BOFields.Add(_EftposCollected)
        BOFields.Add(_EftposSTDT)
        BOFields.Add(_EftposAuthDesc)
        BOFields.Add(_EftposSecurityCode)
        BOFields.Add(_RTI)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_ForeignTendered)
        BOFields.Add(_ExchangeRate)
        BOFields.Add(_ExchangeFactor)
        BOConstraints.Add(New Object() {_TranDate, _TillID, _TranNumber, _SequenceNumber})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesPaid)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesPaid)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesPaid))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesPaid(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("DATE1") : _TranDate.Value = CDate(drRow(dcColumn))
                    Case ("TILL") : _TillID.Value = CStr(drRow(dcColumn))
                    Case ("TRAN") : _TranNumber.Value = CStr(drRow(dcColumn))
                    Case ("NUMB") : _SequenceNumber.Value = CInt(drRow(dcColumn))
                    Case ("TYPE") : _TenderType.Value = CDec(drRow(dcColumn))
                    Case ("AMNT") : _TenderAmount.Value = CDec(drRow(dcColumn))
                    Case ("CARD") : _CreditCardNo.Value = CStr(drRow(dcColumn))
                    Case ("EXDT") : _CreditCardExpiryDate.Value = CStr(drRow(dcColumn))
                    Case ("COPN") : _CouponNumber.Value = CStr(drRow(dcColumn))
                    Case ("AUTH") : _AuthorisationCode.Value = CStr(drRow(dcColumn))
                    Case ("CKEY") : _CCNumberKeyed.Value = CBool(drRow(dcColumn))
                    Case ("SUPV") : _SupervisorCode.Value = CStr(drRow(dcColumn))
                    Case ("POST") : _CouponPostCode.Value = CStr(drRow(dcColumn))
                    Case ("CKAC") : _ChequeAccountNo.Value = CStr(drRow(dcColumn))
                    Case ("CKSC") : _ChequeSortCode.Value = CStr(drRow(dcColumn))
                    Case ("CKNO") : _ChequeNumber.Value = CStr(drRow(dcColumn))
                    Case ("DBRF") : _DBRProcessed.Value = CBool(drRow(dcColumn))
                    Case ("SEQN") : _EftposVoucherNo.Value = CStr(drRow(dcColumn))
                    Case ("ISSU") : _IssueNumber.Value = CStr(drRow(dcColumn))
                    Case ("QTYP") : _AutorisationType.Value = CStr(drRow(dcColumn))
                    Case ("MERC") : _EftposMerchantNumber.Value = CStr(drRow(dcColumn))
                    Case ("CBAM") : _CashBackAmount.Value = CDec(drRow(dcColumn))
                    Case ("DIGC") : _DigitCount.Value = CInt(drRow(dcColumn))
                    Case ("ECOM") : _EftposCommsPrep.Value = CBool(drRow(dcColumn))
                    Case ("CTYP") : _CardDescription.Value = CStr(drRow(dcColumn))
                    Case ("EFID") : _EftposID.Value = CStr(drRow(dcColumn))
                    Case ("EFTC") : _EftposCollected.Value = CStr(drRow(dcColumn))
                    Case ("STDT") : _EftposSTDT.Value = CStr(drRow(dcColumn))
                    Case ("AUTHDESC") : _EftposAuthDesc.Value = CStr(drRow(dcColumn))
                    Case ("SECCODE") : _EftposSecurityCode.Value = CStr(drRow(dcColumn))
                    Case ("RTI") : _RTI.Value = CStr(drRow(dcColumn))
                    Case ("TENC") : _CurrencyID.Value = CStr(drRow(dcColumn))
                    Case ("TENV") : _ForeignTendered.Value = CDec(drRow(dcColumn))
                    Case ("MRAT") : _ExchangeRate.Value = CDec(drRow(dcColumn))
                    Case ("MPOW") : _ExchangeFactor.Value = CDec(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _SequenceNumber As New ColField(Of Integer)("NUMB", 0, "Sequence Number", True, False)
    Private _TenderType As New ColField(Of Decimal)("TYPE", 0, "Tender Type", False, False)
    Private _TenderAmount As New ColField(Of Decimal)("AMNT", 0, "Tender Amount", False, False, 2)
    Private _CreditCardNo As New ColField(Of String)("CARD", "", "Credit Card Number", False, False)
    Private _CreditCardExpiryDate As New ColField(Of String)("EXDT", "", "Credit Card Expiry", False, False)
    Private _CouponNumber As New ColField(Of String)("COPN", "", "Coupon Number", False, False)
    Private _AuthorisationCode As New ColField(Of String)("AUTH", "", "Authorisation Code", False, False)
    Private _CCNumberKeyed As New ColField(Of Boolean)("CKEY", False, "Credit Card Keyed", False, False)
    Private _SupervisorCode As New ColField(Of String)("SUPV", "", "Supervisor Code", False, False)
    Private _CouponPostCode As New ColField(Of String)("POST", "", "Coupon Postcode", False, False)
    Private _ChequeAccountNo As New ColField(Of String)("CKAC", "", "Cheque Account Number", False, False)
    Private _ChequeSortCode As New ColField(Of String)("CKSC", "", "Cheque Sort Code", False, False)
    Private _ChequeNumber As New ColField(Of String)("CKNO", "", "Cheque Number", False, False)
    Private _DBRProcessed As New ColField(Of Boolean)("DBRF", False, "DBR Processed", False, False)
    Private _EftposVoucherNo As New ColField(Of String)("SEQN", "", "EFTPOS Voucher Number", False, False)
    Private _IssueNumber As New ColField(Of String)("ISSU", "", "Issue Number", False, False)
    Private _AutorisationType As New ColField(Of String)("ATYP", "", "Authorisation Type", False, False)
    Private _EftposMerchantNumber As New ColField(Of String)("MERC", "", "EFTPOS Merchant Number", False, False)
    Private _CashBackAmount As New ColField(Of Decimal)("CBAM", 0, "Cash Back Amount", False, False)
    Private _DigitCount As New ColField(Of Integer)("DIGC", 0, "Digit Count", False, False)
    Private _EftposCommsPrep As New ColField(Of Boolean)("ECOM", False, "EFTPOS Comms Prepared", False, False)
    Private _CardDescription As New ColField(Of String)("CTYP", "", "Card Description", False, False)
    Private _EftposID As New ColField(Of String)("EFID", "", "EFTPOS ID", False, False)
    Private _EftposCollected As New ColField(Of String)("EFTC", "", "EFTPOS Collected", False, False)
    Private _EftposSTDT As New ColField(Of String)("STDT", "", "EFTPOS STDT", False, False)
    Private _EftposAuthDesc As New ColField(Of String)("AUTHDESC", "", "EFTPOS Authorisation Description", False, False)
    Private _EftposSecurityCode As New ColField(Of String)("SECCODE", "", "EFTPOS Security Code", False, False)
    Private _RTI As New ColField(Of String)("RTI", "S", "RTI", False, False)
    Private _CurrencyID As New ColField(Of String)("TENC", "", "Currency ID", False, False)
    Private _ForeignTendered As New ColField(Of Decimal)("TENV", 0, "Foreign Currency Tendered", False, False, 2)
    Private _ExchangeRate As New ColField(Of Decimal)("MRAT", 0, "Exchange Rate", False, False, 4)
    Private _ExchangeFactor As New ColField(Of Decimal)("MPOW", 0, "Exchange Power", False, False)

#End Region

#Region "Field Properties"

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransNo() As ColField(Of String)
        Get
            TransNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of Integer)
        Get
            SequenceNo = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SequenceNumber = value
        End Set
    End Property
    Public Property TenderType() As ColField(Of Decimal)
        Get
            TenderType = _TenderType
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType = value
        End Set
    End Property
    Public Property TenderAmount() As ColField(Of Decimal)
        Get
            TenderAmount = _TenderAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderAmount = value
        End Set
    End Property
    Public Property CreditCardNo() As ColField(Of String)
        Get
            CreditCardNo = _CreditCardNo
        End Get
        Set(ByVal value As ColField(Of String))
            _CreditCardNo = value
        End Set
    End Property
    Public Property CreditCardExpiryDate() As ColField(Of String)
        Get
            CreditCardExpiryDate = _CreditCardExpiryDate
        End Get
        Set(ByVal value As ColField(Of String))
            _CreditCardExpiryDate = value
        End Set
    End Property
    Public Property CouponNo() As ColField(Of String)
        Get
            CouponNo = _CouponNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CouponNumber = value
        End Set
    End Property
    Public Property AuthorisationCode() As ColField(Of String)
        Get
            AuthorisationCode = _AuthorisationCode
        End Get
        Set(ByVal value As ColField(Of String))
            _AuthorisationCode = value
        End Set
    End Property
    Public Property CCNumberKeyed() As ColField(Of Boolean)
        Get
            CCNumberKeyed = _CCNumberKeyed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CCNumberKeyed = value
        End Set
    End Property
    Public Property SupervisorCode() As ColField(Of String)
        Get
            SupervisorCode = _SupervisorCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SupervisorCode = value
        End Set
    End Property
    Public Property CouponPostCode() As ColField(Of String)
        Get
            CouponPostCode = _CouponPostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CouponPostCode = value
        End Set
    End Property
    Public Property ChequeAccountNo() As ColField(Of String)
        Get
            ChequeAccountNo = _ChequeAccountNo
        End Get
        Set(ByVal value As ColField(Of String))
            _ChequeAccountNo = value
        End Set
    End Property
    Public Property ChequeSortCode() As ColField(Of String)
        Get
            ChequeSortCode = _ChequeSortCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ChequeSortCode = value
        End Set
    End Property
    Public Property ChequeNo() As ColField(Of String)
        Get
            ChequeNo = _ChequeNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ChequeNumber = value
        End Set
    End Property
    Public Property DBRProcessed() As ColField(Of Boolean)
        Get
            DBRProcessed = _DBRProcessed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DBRProcessed = value
        End Set
    End Property
    Public Property EftposVoucherNo() As ColField(Of String)
        Get
            EftposVoucherNo = _EftposVoucherNo
        End Get
        Set(ByVal value As ColField(Of String))
            _EftposVoucherNo = value
        End Set
    End Property
    Public Property IssueNo() As ColField(Of String)
        Get
            IssueNo = _IssueNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _IssueNumber = value
        End Set
    End Property
    Public Property AutorisationType() As ColField(Of String)
        Get
            AutorisationType = _AutorisationType
        End Get
        Set(ByVal value As ColField(Of String))
            _AutorisationType = value
        End Set
    End Property
    Public Property EftposMerchantNo() As ColField(Of String)
        Get
            EftposMerchantNo = _EftposMerchantNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EftposMerchantNumber = value
        End Set
    End Property
    Public Property CashBackAmount() As ColField(Of Decimal)
        Get
            CashBackAmount = _CashBackAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CashBackAmount = value
        End Set
    End Property
    Public Property DigitCount() As ColField(Of Integer)
        Get
            DigitCount = _DigitCount
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DigitCount = value
        End Set
    End Property
    Public Property EftposCommsPrep() As ColField(Of Boolean)
        Get
            EftposCommsPrep = _EftposCommsPrep
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _EftposCommsPrep = value
        End Set
    End Property
    Public Property CardDescription() As ColField(Of String)
        Get
            CardDescription = _CardDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _CardDescription = value
        End Set
    End Property
    Public Property EftposID() As ColField(Of String)
        Get
            Return _EftposID
        End Get
        Set(ByVal value As ColField(Of String))
            _EftposID = value
        End Set
    End Property
    Public Property EftposAuthDesc() As ColField(Of String)
        Get
            Return _EftposAuthDesc
        End Get
        Set(ByVal value As ColField(Of String))
            _EftposAuthDesc = value
        End Set
    End Property
    Public Property EftposSecurityCode() As ColField(Of String)
        Get
            Return _EftposSecurityCode
        End Get
        Set(ByVal value As ColField(Of String))
            _EftposSecurityCode = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property ForeignTendered() As ColField(Of Decimal)
        Get
            Return _ForeignTendered
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ForeignTendered = value
        End Set
    End Property
    Public Property ExchangeRate() As ColField(Of Decimal)
        Get
            Return _ExchangeRate
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExchangeRate = value
        End Set
    End Property
    Public Property ExchangeFactor() As ColField(Of Decimal)
        Get
            Return _ExchangeFactor
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExchangeFactor = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Payments As List(Of cSalesPaid) = Nothing

    Public Property Payments() As List(Of cSalesPaid)
        Get
            Return _Payments
        End Get
        Set(ByVal value As List(Of cSalesPaid))
            _Payments = value
        End Set
    End Property

#End Region

End Class
