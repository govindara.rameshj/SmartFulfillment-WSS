﻿<Serializable()> Public Class cSalesLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLLINE"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_DepartmentNumber)
        BOFields.Add(_BarCodeEntry)
        BOFields.Add(_SupervisorNumber)
        BOFields.Add(_QuantitySold)
        BOFields.Add(_LookUpPrice)
        BOFields.Add(_ActualSellPrice)
        BOFields.Add(_ActualPriceExVat)
        BOFields.Add(_ExtendedValue)
        BOFields.Add(_ExtendedCost)
        BOFields.Add(_RelatedItems)
        BOFields.Add(_PriceOverrideReason)
        BOFields.Add(_ItemTagged)
        BOFields.Add(_CatchAllItem)
        BOFields.Add(_VatCode)
        BOFields.Add(_TempPriceMarginAmt)
        BOFields.Add(_TempPriceMarginCode)
        BOFields.Add(_PriceOverrideAmount)
        BOFields.Add(_OverrideMarginCode)
        BOFields.Add(_QtyBreakMarginAmount)
        BOFields.Add(_QtyBreakMarginCode)
        BOFields.Add(_DealGroupMarginAmt)
        BOFields.Add(_DealGroupMarginCode)
        BOFields.Add(_MultiBuyMarginAmount)
        BOFields.Add(_MultiBuyMarginCode)
        BOFields.Add(_HierarchyMarginAmt)
        BOFields.Add(_HierarchyMarginCode)
        BOFields.Add(_EmpSalePriMarginAmt)
        BOFields.Add(_EmpSalePriMarginCode)
        BOFields.Add(_LineReversed)
        BOFields.Add(_LastEventSequenceNo)
        BOFields.Add(_HierCategory)
        BOFields.Add(_HierGroup)
        BOFields.Add(_HierSubGroup)
        BOFields.Add(_HierStyle)
        BOFields.Add(_QuarantineSupervisor)
        BOFields.Add(_EmpSaleSecMarginAmt)
        BOFields.Add(_MarkDownStock)
        BOFields.Add(_SaleType)
        BOFields.Add(_VatCodeNumber)
        BOFields.Add(_VatAmount)
        BOFields.Add(_RTI)
        BOConstraints.Add(New Object() {_TranDate, _TillID, _TranNumber, _SequenceNumber})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("DATE1") : _TranDate.Value = CDate(drRow(dcColumn))
                    Case ("TILL") : _TillID.Value = CStr(drRow(dcColumn))
                    Case ("TRAN") : _TranNumber.Value = CStr(drRow(dcColumn))
                    Case ("NUMB") : _SequenceNumber.Value = CInt(drRow(dcColumn))
                    Case ("SKUN") : _SkuNumber.Value = CStr(drRow(dcColumn))
                    Case ("DEPT") : _DepartmentNumber.Value = CStr(drRow(dcColumn))
                    Case ("IBAR") : _BarCodeEntry.Value = CBool(drRow(dcColumn))
                    Case ("SUPV") : _SupervisorNumber.Value = CStr(drRow(dcColumn))
                    Case ("QUAN") : _QuantitySold.Value = CDec(drRow(dcColumn))
                    Case ("SPRI") : _LookUpPrice.Value = CDec(drRow(dcColumn))
                    Case ("PRIC") : _ActualSellPrice.Value = CDec(drRow(dcColumn))
                    Case ("PRVE") : _ActualPriceExVat.Value = CDec(drRow(dcColumn))
                    Case ("EXTP") : _ExtendedValue.Value = CDec(drRow(dcColumn))
                    Case ("EXTC") : _ExtendedCost.Value = CDec(drRow(dcColumn))
                    Case ("RITM") : _RelatedItems.Value = CBool(drRow(dcColumn))
                    Case ("PORC") : _PriceOverrideReason.Value = CInt(drRow(dcColumn))
                    Case ("ITAG") : _ItemTagged.Value = CBool(drRow(dcColumn))
                    Case ("CATA") : _CatchAllItem.Value = CBool(drRow(dcColumn))
                    Case ("VSYM") : _VatCode.Value = CStr(drRow(dcColumn))
                    Case ("TPPD") : _TempPriceMarginAmt.Value = CDec(drRow(dcColumn))
                    Case ("TPME") : _TempPriceMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("POPD") : _PriceOverrideAmount.Value = CDec(drRow(dcColumn))
                    Case ("POME") : _OverrideMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("QBPD") : _QtyBreakMarginAmount.Value = CDec(drRow(dcColumn))
                    Case ("QBME") : _QtyBreakMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("DGPD") : _DealGroupMarginAmt.Value = CDec(drRow(dcColumn))
                    Case ("DGME") : _DealGroupMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("MBPD") : _MultiBuyMarginAmount.Value = CDec(drRow(dcColumn))
                    Case ("MBME") : _MultiBuyMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("HSPD") : _HierarchyMarginAmt.Value = CDec(drRow(dcColumn))
                    Case ("HSME") : _HierarchyMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("ESPD") : _EmpSalePriMarginAmt.Value = CDec(drRow(dcColumn))
                    Case ("ESME") : _EmpSalePriMarginCode.Value = CStr(drRow(dcColumn))
                    Case ("LREV") : _LineReversed.Value = CBool(drRow(dcColumn))
                    Case ("ESEQ") : _LastEventSequenceNo.Value = CStr(drRow(dcColumn))
                    Case ("CTGY") : _HierCategory.Value = CStr(drRow(dcColumn))
                    Case ("GRUP") : _HierGroup.Value = CStr(drRow(dcColumn))
                    Case ("SGRP") : _HierSubGroup.Value = CStr(drRow(dcColumn))
                    Case ("STYL") : _HierStyle.Value = CStr(drRow(dcColumn))
                    Case ("QSUP") : _QuarantineSupervisor.Value = CStr(drRow(dcColumn))
                    Case ("ESEV") : _EmpSaleSecMarginAmt.Value = CDec(drRow(dcColumn))
                    Case ("IMDN") : _MarkDownStock.Value = CBool(drRow(dcColumn))
                    Case ("SALT") : _SaleType.Value = CStr(drRow(dcColumn))
                    Case ("VATN") : _VatCodeNumber.Value = CDec(drRow(dcColumn))
                    Case ("VATV") : _VatAmount.Value = CDec(drRow(dcColumn))
                    Case ("BDCO") : _BackDoorCollect.Value = CStr(drRow(dcColumn))
                    Case ("BDCOInd") : _BackDoorCollected.Value = CBool(drRow(dcColumn))
                    Case ("RCOD") : _LineReverseCode.Value = CStr(drRow(dcColumn))
                    Case ("RTI") : _RTI.Value = CStr(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "", "Transaction Number", True, False)
    Private _SequenceNumber As New ColField(Of Integer)("NUMB", 0, "Sequence Number", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU Number", False, False)
    Private _DepartmentNumber As New ColField(Of String)("DEPT", "", "Department", False, False)
    Private _BarCodeEntry As New ColField(Of Boolean)("IBAR", False, "Bar Code Entry", False, False)
    Private _SupervisorNumber As New ColField(Of String)("SUPV", "", "Supervisor Number", False, False)

    Private _QuantitySold As New ColField(Of Decimal)("QUAN", 0, "Quantity Sold", False, False)
    Private _LookUpPrice As New ColField(Of Decimal)("SPRI", 0, "Lookup Price", False, False, 2)
    Private _ActualSellPrice As New ColField(Of Decimal)("PRIC", 0, "Actual Sell Price", False, False, 2)
    Private _ActualPriceExVat As New ColField(Of Decimal)("PRVE", 0, "Actual Price ex VAT", False, False, 2)
    Private _ExtendedValue As New ColField(Of Decimal)("EXTP", 0, "Extended Value", False, False, 2)
    Private _ExtendedCost As New ColField(Of Decimal)("EXTC", 0, "Extended Cost", False, False, 2)
    Private _RelatedItems As New ColField(Of Boolean)("RITM", False, "Related Items", False, False)
    Private _PriceOverrideReason As New ColField(Of Integer)("PORC", 0, "Price Override Reason", False, False)
    Private _ItemTagged As New ColField(Of Boolean)("ITAG", False, "Item Tagged", False, False)
    Private _CatchAllItem As New ColField(Of Boolean)("CATA", False, "Catch All Item", False, False)
    Private _VatCode As New ColField(Of String)("VSYM", "", "VAT Code", False, False)

    Private _TempPriceMarginAmt As New ColField(Of Decimal)("TPPD", 0, "Temp Price Margin Amount", False, False, 2)
    Private _TempPriceMarginCode As New ColField(Of String)("TPME", "", "Temp Price Margin Code", False, False)
    Private _PriceOverrideAmount As New ColField(Of Decimal)("POPD", 0, "Price Override Amount", False, False, 2)
    Private _OverrideMarginCode As New ColField(Of String)("POME", "", "Override Margin Code", False, False)
    Private _QtyBreakMarginAmount As New ColField(Of Decimal)("QBPD", 0, "Quantity Break Margin Amount", False, False, 2)
    Private _QtyBreakMarginCode As New ColField(Of String)("QBME", "", "Quantity Break Margin Code", False, False)
    Private _DealGroupMarginAmt As New ColField(Of Decimal)("DGPD", 0, "Deal Group Margin Amount", False, False, 2)
    Private _DealGroupMarginCode As New ColField(Of String)("DGME", "", "Deal Group Margin Code", False, False)
    Private _MultiBuyMarginAmount As New ColField(Of Decimal)("MBPD", 0, "Multi Buy Margin Amount", False, False, 2)
    Private _MultiBuyMarginCode As New ColField(Of String)("MBME", "", "Multi Buy Margin Code", False, False)
    Private _HierarchyMarginAmt As New ColField(Of Decimal)("HSPD", 0, "Hierarchy Margin Amount", False, False, 2)
    Private _HierarchyMarginCode As New ColField(Of String)("HSME", "", "Hierarchy Margin Code", False, False)
    Private _EmpSalePriMarginAmt As New ColField(Of Decimal)("ESPD", 0, "Emp Sale Price Margin Amount", False, False, 2)
    Private _EmpSalePriMarginCode As New ColField(Of String)("ESME", "", "Emp Sale Price Margin Code", False, False)
    Private _LineReversed As New ColField(Of Boolean)("LREV", False, "Line Reversed", False, False)
    Private _LastEventSequenceNo As New ColField(Of String)("ESEQ", "", "Last Event Sequence Number", False, False)
    Private _HierCategory As New ColField(Of String)("CTGY", "000000", "Hierarchy Category", False, False)
    Private _HierGroup As New ColField(Of String)("GRUP", "000000", "Hierarchy Group", False, False)
    Private _HierSubGroup As New ColField(Of String)("SGRP", "000000", "Hierarachy Subgroup", False, False)
    Private _HierStyle As New ColField(Of String)("STYL", "000000", "Hierarchy Style", False, False)
    Private _QuarantineSupervisor As New ColField(Of String)("QSUP", "", "Quarantine Supervisor", False, False)
    Private _EmpSaleSecMarginAmt As New ColField(Of Decimal)("ESEV", 0, "Emp Sale Sec Margin Amount", False, False, 2)
    Private _MarkDownStock As New ColField(Of Boolean)("IMDN", False, "Markdown Stock", False, False)
    Private _SaleType As New ColField(Of String)("SALT", "", "Sale Type", False, False)
    Private _VatCodeNumber As New ColField(Of Decimal)("VATN", 0, "VAT Code Number", False, False)
    Private _VatAmount As New ColField(Of Decimal)("VATV", 0, "VAT Amount", False, False, 2)
    Private _BackDoorCollect As New ColField(Of String)("BDCO", "N", "Back Door Collection", False, False, 1)
    Private _BackDoorCollected As New ColField(Of Boolean)("BDCOInd", False, "Back Door Collected", False, False)
    Private _LineReverseCode As New ColField(Of String)("RCOD", "00", "Reversal Code", False, False, 2)
    Private _RTI As New ColField(Of String)("RTI", "S", "RTI", False, False)

#End Region

#Region "Field Properties"

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransNo() As ColField(Of String)
        Get
            TransNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of Integer)
        Get
            SequenceNo = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SequenceNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property DepartmentNo() As ColField(Of String)
        Get
            DepartmentNo = _DepartmentNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DepartmentNumber = value
        End Set
    End Property
    Public Property BarCodeEntry() As ColField(Of Boolean)
        Get
            BarCodeEntry = _BarCodeEntry
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _BarCodeEntry = value
        End Set
    End Property
    Public Property SupervisorNo() As ColField(Of String)
        Get
            SupervisorNo = _SupervisorNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupervisorNumber = value
        End Set
    End Property
    Public Property QuantitySold() As ColField(Of Decimal)
        Get
            QuantitySold = _QuantitySold
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QuantitySold = value
        End Set
    End Property
    Public Property LoopUpPrice() As ColField(Of Decimal)
        Get
            LoopUpPrice = _LookUpPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _LookUpPrice = value
        End Set
    End Property
    Public Property ActualSellPrice() As ColField(Of Decimal)
        Get
            ActualSellPrice = _ActualSellPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ActualSellPrice = value
        End Set
    End Property
    Public Property ActualPriceExVat() As ColField(Of Decimal)
        Get
            ActualPriceExVat = _ActualPriceExVat
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ActualPriceExVat = value
        End Set
    End Property
    Public Property ExtendedValue() As ColField(Of Decimal)
        Get
            ExtendedValue = _ExtendedValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExtendedValue = value
        End Set
    End Property
    Public Property ExtendedCost() As ColField(Of Decimal)
        Get
            ExtendedCost = _ExtendedCost
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExtendedCost = value
        End Set
    End Property
    Public Property RelatedItems() As ColField(Of Boolean)
        Get
            RelatedItems = _RelatedItems
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RelatedItems = value
        End Set
    End Property
    Public Property PriceOverrideReason() As ColField(Of Integer)
        Get
            PriceOverrideReason = _PriceOverrideReason
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PriceOverrideReason = value
        End Set
    End Property
    Public Property ItemTagged() As ColField(Of Boolean)
        Get
            ItemTagged = _ItemTagged
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ItemTagged = value
        End Set
    End Property
    Public Property CatchAllItem() As ColField(Of Boolean)
        Get
            CatchAllItem = _CatchAllItem
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CatchAllItem = value
        End Set
    End Property
    Public Property VatCode() As ColField(Of String)
        Get
            VatCode = _VatCode
        End Get
        Set(ByVal value As ColField(Of String))
            _VatCode = value
        End Set
    End Property
    Public Property TempPriceMarginAmt() As ColField(Of Decimal)
        Get
            TempPriceMarginAmt = _TempPriceMarginAmt
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TempPriceMarginAmt = value
        End Set
    End Property
    Public Property TempPriceMarginCode() As ColField(Of String)
        Get
            TempPriceMarginCode = _TempPriceMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _TempPriceMarginCode = value
        End Set
    End Property
    Public Property PriceOverrideAmount() As ColField(Of Decimal)
        Get
            PriceOverrideAmount = _PriceOverrideAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceOverrideAmount = value
        End Set
    End Property
    Public Property OverrideMarginCode() As ColField(Of String)
        Get
            OverrideMarginCode = _OverrideMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _OverrideMarginCode = value
        End Set
    End Property
    Public Property QtyBreakMarginAmount() As ColField(Of Decimal)
        Get
            QtyBreakMarginAmount = _QtyBreakMarginAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyBreakMarginAmount = value
        End Set
    End Property
    Public Property QtyBreakMarginCode() As ColField(Of String)
        Get
            QtyBreakMarginCode = _QtyBreakMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _QtyBreakMarginCode = value
        End Set
    End Property
    Public Property DealGroupMarginAmt() As ColField(Of Decimal)
        Get
            DealGroupMarginAmt = _DealGroupMarginAmt
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DealGroupMarginAmt = value
        End Set
    End Property
    Public Property DealGroupMarginCode() As ColField(Of String)
        Get
            DealGroupMarginCode = _DealGroupMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _DealGroupMarginCode = value
        End Set
    End Property
    Public Property MultiBuyMarginAmount() As ColField(Of Decimal)
        Get
            MultiBuyMarginAmount = _MultiBuyMarginAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MultiBuyMarginAmount = value
        End Set
    End Property
    Public Property MultiBuyMarginCode() As ColField(Of String)
        Get
            MultiBuyMarginCode = _MultiBuyMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _MultiBuyMarginCode = value
        End Set
    End Property
    Public Property HierarchyMarginAmt() As ColField(Of Decimal)
        Get
            HierarchyMarginAmt = _HierarchyMarginAmt
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _HierarchyMarginAmt = value
        End Set
    End Property
    Public Property HierarchyMarginCode() As ColField(Of String)
        Get
            HierarchyMarginCode = _HierarchyMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _HierarchyMarginCode = value
        End Set
    End Property
    Public Property EmpSalePriMarginAmt() As ColField(Of Decimal)
        Get
            EmpSalePriMarginAmt = _EmpSalePriMarginAmt
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EmpSalePriMarginAmt = value
        End Set
    End Property
    Public Property EmpSalePriMarginCode() As ColField(Of String)
        Get
            EmpSalePriMarginCode = _EmpSalePriMarginCode
        End Get
        Set(ByVal value As ColField(Of String))
            _EmpSalePriMarginCode = value
        End Set
    End Property
    Public Property LineReversed() As ColField(Of Boolean)
        Get
            LineReversed = _LineReversed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LineReversed = value
        End Set
    End Property
    Public Property LastEventSequenceNo() As ColField(Of String)
        Get
            LastEventSequenceNo = _LastEventSequenceNo
        End Get
        Set(ByVal value As ColField(Of String))
            _LastEventSequenceNo = value
        End Set
    End Property
    Public Property HierCategory() As ColField(Of String)
        Get
            HierCategory = _HierCategory
        End Get
        Set(ByVal value As ColField(Of String))
            _HierCategory = value
        End Set
    End Property
    Public Property HierGroup() As ColField(Of String)
        Get
            HierGroup = _HierGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HierGroup = value
        End Set
    End Property
    Public Property HierSubGroup() As ColField(Of String)
        Get
            HierSubGroup = _HierSubGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HierSubGroup = value
        End Set
    End Property
    Public Property HierStyle() As ColField(Of String)
        Get
            HierStyle = _HierStyle
        End Get
        Set(ByVal value As ColField(Of String))
            _HierStyle = value
        End Set
    End Property
    Public Property QuarantineSupervisor() As ColField(Of String)
        Get
            QuarantineSupervisor = _QuarantineSupervisor
        End Get
        Set(ByVal value As ColField(Of String))
            _QuarantineSupervisor = value
        End Set
    End Property
    Public Property EmpSaleSecMarginAmt() As ColField(Of Decimal)
        Get
            EmpSaleSecMarginAmt = _EmpSaleSecMarginAmt
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _EmpSaleSecMarginAmt = value
        End Set
    End Property
    Public Property MarkDownStock() As ColField(Of Boolean)
        Get
            MarkDownStock = _MarkDownStock
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _MarkDownStock = value
        End Set
    End Property
    Public Property SaleType() As ColField(Of String)
        Get
            SaleType = _SaleType
        End Get
        Set(ByVal value As ColField(Of String))
            _SaleType = value
        End Set
    End Property
    Public Property VatCodeNo() As ColField(Of Decimal)
        Get
            VatCodeNo = _VatCodeNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatCodeNumber = value
        End Set
    End Property
    Public Property VatAmount() As ColField(Of Decimal)
        Get
            VatAmount = _VatAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatAmount = value
        End Set
    End Property
    Public Property BackDoorCollect() As ColField(Of String)
        Get
            Return _BackDoorCollect
        End Get
        Set(ByVal value As ColField(Of String))
            _BackDoorCollect = value
        End Set
    End Property

    Public Property BackDoorCollected() As ColField(Of Boolean)
        Get
            Return _BackDoorCollected
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _BackDoorCollected = value
        End Set
    End Property

    Public Property LineReverseCode() As ColField(Of String)
        Get
            Return _LineReverseCode
        End Get
        Set(ByVal value As ColField(Of String))
            _LineReverseCode = value
        End Set
    End Property

    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _Lines As List(Of cSalesLine) = Nothing

    Public Property Lines() As List(Of cSalesLine)
        Get
            Return _Lines
        End Get
        Set(ByVal value As List(Of cSalesLine))
            _Lines = value
        End Set
    End Property

    Public Function GetLinesForTransaction(ByVal strTransactionId As String) As List(Of cSalesLine)

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TranNumber, strTransactionId)
            Return LoadMatches()

        Catch ex As Exception

            Throw ex
            Return Nothing

        End Try

    End Function 'GetLinesForTransaction

    Public Function CalcPaidTotal() As Decimal
        Return ExtendedValue.Value - QtyBreakMarginAmount.Value - DealGroupMarginAmt.Value - MultiBuyMarginAmount.Value - HierarchyMarginAmt.Value - EmpSaleSecMarginAmt.Value
    End Function

#End Region

End Class
