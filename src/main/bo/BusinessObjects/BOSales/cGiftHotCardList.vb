﻿<Serializable()> Public Class cGiftHotCardList
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "GIFHOT"
        BOFields.Add(_SerialNumber)
        BOFields.Add(_RedeemedText)
        BOFields.Add(_AdditionalInfo)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cGiftHotCardList)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cGiftHotCardList)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cGiftHotCardList))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPromiseCustomer(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _SerialNumber As New ColField(Of String)("SERI", "", "Serial No", True, False)
    Private _RedeemedText As New ColField(Of String)("TEXT", "", "Redeemed Text", False, False)
    Private _AdditionalInfo As New ColField(Of String)("HOTT", "", "Additional Info", False, False)

#End Region

#Region "Field Properties"

    Public Property SerialNumber() As ColField(Of String)
        Get
            SerialNumber = _SerialNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SerialNumber = value
        End Set
    End Property
    Public Property RedeemedText() As ColField(Of String)
        Get
            RedeemedText = _RedeemedText
        End Get
        Set(ByVal value As ColField(Of String))
            _RedeemedText = value
        End Set
    End Property
    Public Property AdditionalInfo() As ColField(Of String)
        Get
            AdditionalInfo = _AdditionalInfo
        End Get
        Set(ByVal value As ColField(Of String))
            _AdditionalInfo = value
        End Set
    End Property
    Public Sub DeleteAllRecords()
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.Delete()
    End Sub

#End Region

End Class
