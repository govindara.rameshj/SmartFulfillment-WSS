﻿<Serializable()> Public Class cSalesBarcodeCheck
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLEANCHK"
        BOFields.Add(_DIGKey)
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_Time)
        BOFields.Add(_Entry)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_ReasonText)
        BOFields.Add(_KeyScanBarCode)
        BOFields.Add(_Type)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_FoundInBarCodeTab)
        BOFields.Add(_SkuInBarCode)
        BOFields.Add(_SkuFound)
        BOFields.Add(_SkuEntered)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesBarcodeCheck)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesBarcodeCheck)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesBarcodeCheck))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesBarcodeCheck(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _DIGKey As New ColField(Of Integer)("TKEY", 0, "DIG Key", False, False)
    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _SequenceNumber As New ColField(Of Decimal)("SEQN", 0, "Sequence Number", True, False)
    Private _Time As New ColField(Of String)("TIME", "000000", "Time", False, False)
    Private _Entry As New ColField(Of String)("ENTRY", "", "Entry", False, False)
    Private _ReasonCode As New ColField(Of String)("REASONCODE", "00", "Reason Code", False, False)
    Private _ReasonText As New ColField(Of String)("KREASON", "", "Reason Text", False, False)
    Private _KeyScanBarCode As New ColField(Of String)("ITEM", "", "Key Scan Bar Code", False, False)
    Private _Type As New ColField(Of String)("TYPE", "", "Type", False, False)
    Private _EmployeeID As New ColField(Of String)("EEID", "", "Employee ID", False, False)
    Private _FoundInBarCodeTab As New ColField(Of Boolean)("EAFOUND", False, "Found in Bar Code Tab", False, False)
    Private _SkuInBarCode As New ColField(Of String)("EASKUN", "", "SKU in Bar Code", False, False)
    Private _SkuFound As New ColField(Of Boolean)("EASKUFOUND", False, "SKU Found", False, False)
    Private _SkuEntered As New ColField(Of String)("SKUN", "", "SKU Entered", False, False)

#End Region

#Region "Field Properties"

    Public Property DIGKey() As ColField(Of Integer)
        Get
            DIGKey = _DIGKey
        End Get
        Set(ByVal value As ColField(Of Integer))
            _DIGKey = value
        End Set
    End Property
    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of Decimal)
        Get
            SequenceNo = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SequenceNumber = value
        End Set
    End Property
    Public Property Time() As ColField(Of String)
        Get
            Time = _Time
        End Get
        Set(ByVal value As ColField(Of String))
            _Time = value
        End Set
    End Property
    Public Property Entry() As ColField(Of String)
        Get
            Entry = _Entry
        End Get
        Set(ByVal value As ColField(Of String))
            _Entry = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of String)
        Get
            ReasonCode = _ReasonCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ReasonCode = value
        End Set
    End Property
    Public Property ReasonText() As ColField(Of String)
        Get
            ReasonText = _ReasonText
        End Get
        Set(ByVal value As ColField(Of String))
            _ReasonText = value
        End Set
    End Property
    Public Property KeyScanBarCode() As ColField(Of String)
        Get
            KeyScanBarCode = _KeyScanBarCode
        End Get
        Set(ByVal value As ColField(Of String))
            _KeyScanBarCode = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property EmployeeNo() As ColField(Of String)
        Get
            EmployeeNo = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property FoundInBarCodeTab() As ColField(Of Boolean)
        Get
            FoundInBarCodeTab = _FoundInBarCodeTab
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _FoundInBarCodeTab = value
        End Set
    End Property
    Public Property PartCodeInBarCode() As ColField(Of String)
        Get
            PartCodeInBarCode = _SkuInBarCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuInBarCode = value
        End Set
    End Property
    Public Property PartCodeFound() As ColField(Of Boolean)
        Get
            PartCodeFound = _SkuFound
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SkuFound = value
        End Set
    End Property
    Public Property PartCodeEntered() As ColField(Of String)
        Get
            PartCodeEntered = _SkuEntered
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuEntered = value
        End Set
    End Property

#End Region

End Class