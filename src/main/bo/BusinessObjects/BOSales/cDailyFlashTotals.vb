﻿<Serializable()> Public Class cDailyFlashTotals
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RSFLAS"
        BOFields.Add(_ID)
        BOFields.Add(_FLOT)
        BOFields.Add(_PICK)
        BOFields.Add(_NormalSalesCount)
        BOFields.Add(_SalesCorrectionsCount)
        BOFields.Add(_RefundsCount)
        BOFields.Add(_RefundsCorrectionsCount)
        BOFields.Add(_CashierSignOnCount)
        BOFields.Add(_CashierSignOffCount)
        BOFields.Add(_MiscIncomeCount)
        BOFields.Add(_PaidOutCount)
        BOFields.Add(_OpenDrawerCount)
        BOFields.Add(_ReprintLogoCount)
        BOFields.Add(_ViodCount)
        BOFields.Add(_PriceViolationsCount)
        BOFields.Add(_UnknownTypeCount)
        BOFields.Add(_NormalSalesTime)
        BOFields.Add(_SalesCorrectionsTime)
        BOFields.Add(_RefundsTime)
        BOFields.Add(_RefundsCorrectionsTime)
        BOFields.Add(_CashierSignOnTime)
        BOFields.Add(_CashierSignOffTime)
        BOFields.Add(_MiscIncomeTime)
        BOFields.Add(_PaidOutTime)
        BOFields.Add(_OpenDrawerTime)
        BOFields.Add(_ReprintLogoTime)
        BOFields.Add(_ViodTime)
        BOFields.Add(_PriceViolationsTime)
        BOFields.Add(_UnknownTypeTime)
        BOFields.Add(_NormalSalesAmount)
        BOFields.Add(_SalesCorrectionsAmount)
        BOFields.Add(_RefundsAmount)
        BOFields.Add(_RefundsCorrectionsAmount)
        BOFields.Add(_CashierSignOnAmount)
        BOFields.Add(_CashierSignOffAmount)
        BOFields.Add(_MiscIncomeAmount)
        BOFields.Add(_PaidOutAmount)
        BOFields.Add(_OpenDrawerAmount)
        BOFields.Add(_ReprintLogoAmount)
        BOFields.Add(_ViodAmount)
        BOFields.Add(_PriceViolationsAmount)
        BOFields.Add(_UnknownTypeAmount)
        BOFields.Add(_TenderType1Count)
        BOFields.Add(_TenderType2Count)
        BOFields.Add(_TenderType3Count)
        BOFields.Add(_TenderType4Count)
        BOFields.Add(_TenderType5Count)
        BOFields.Add(_TenderType6Count)
        BOFields.Add(_TenderType7Count)
        BOFields.Add(_TenderType8Count)
        BOFields.Add(_TenderType9Count)
        BOFields.Add(_TenderType10Count)
        BOFields.Add(_TenderType11Count)
        BOFields.Add(_TenderType12Count)
        BOFields.Add(_TenderType13Count)
        BOFields.Add(_TenderType14Count)
        BOFields.Add(_TenderType15Count)
        BOFields.Add(_TenderType16Count)
        BOFields.Add(_TenderType17Count)
        BOFields.Add(_TenderType18Count)
        BOFields.Add(_TenderType19Count)
        BOFields.Add(_TenderType20Count)
        BOFields.Add(_TenderType1Amount)
        BOFields.Add(_TenderType2Amount)
        BOFields.Add(_TenderType3Amount)
        BOFields.Add(_TenderType4Amount)
        BOFields.Add(_TenderType5Amount)
        BOFields.Add(_TenderType6Amount)
        BOFields.Add(_TenderType7Amount)
        BOFields.Add(_TenderType8Amount)
        BOFields.Add(_TenderType9Amount)
        BOFields.Add(_TenderType10Amount)
        BOFields.Add(_TenderType11Amount)
        BOFields.Add(_TenderType12Amount)
        BOFields.Add(_TenderType13Amount)
        BOFields.Add(_TenderType14Amount)
        BOFields.Add(_TenderType15Amount)
        BOFields.Add(_TenderType16Amount)
        BOFields.Add(_TenderType17Amount)
        BOFields.Add(_TenderType18Amount)
        BOFields.Add(_TenderType19Amount)
        BOFields.Add(_TenderType20Amount)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cDailyFlashTotals)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cDailyFlashTotals)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cDailyFlashTotals))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cDailyFlashTotals(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("FKEY") : _ID.Value = CInt(drRow(dcColumn))
                    Case ("FLOT") : _FLOT.Value = CDec(drRow(dcColumn))
                    Case ("PICK") : _PICK.Value = CDec(drRow(dcColumn))
                    Case ("NUMB1") : _NormalSalesCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB2") : _SalesCorrectionsCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB3") : _RefundsCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB4") : _RefundsCorrectionsCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB5") : _CashierSignOnCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB6") : _CashierSignOffCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB7") : _MiscIncomeCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB8") : _PaidOutCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB9") : _OpenDrawerCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB10") : _ReprintLogoCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB11") : _ViodCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB12") : _PriceViolationsCount.Value = CDec(drRow(dcColumn))
                    Case ("NUMB13") : _UnknownTypeCount.Value = CDec(drRow(dcColumn))
                    Case ("TIME1") : _NormalSalesTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME2") : _SalesCorrectionsTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME3") : _RefundsTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME4") : _RefundsCorrectionsTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME5") : _CashierSignOnTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME6") : _CashierSignOffTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME7") : _MiscIncomeTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME8") : _PaidOutTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME9") : _OpenDrawerTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME10") : _ReprintLogoTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME11") : _ViodTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME12") : _PriceViolationsTime.Value = CStr(drRow(dcColumn))
                    Case ("TIME13") : _UnknownTypeTime.Value = CStr(drRow(dcColumn))
                    Case ("AMNT1") : _NormalSalesAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT2") : _SalesCorrectionsAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT3") : _RefundsAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT4") : _RefundsCorrectionsAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT5") : _CashierSignOnAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT6") : _CashierSignOffAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT7") : _MiscIncomeAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT8") : _PaidOutAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT9") : _OpenDrawerAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT10") : _ReprintLogoAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT11") : _ViodAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT12") : _PriceViolationsAmount.Value = CDec(drRow(dcColumn))
                    Case ("AMNT13") : _UnknownTypeAmount.Value = CDec(drRow(dcColumn))
                    Case ("TTCO1") : _TenderType1Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO2") : _TenderType2Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO3") : _TenderType3Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO4") : _TenderType4Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO5") : _TenderType5Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO6") : _TenderType6Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO7") : _TenderType7Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO8") : _TenderType8Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO9") : _TenderType9Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO10") : _TenderType10Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO11") : _TenderType11Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO12") : _TenderType12Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO13") : _TenderType13Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO14") : _TenderType14Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO15") : _TenderType15Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO16") : _TenderType16Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO17") : _TenderType17Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO18") : _TenderType18Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO19") : _TenderType19Count.Value = CDec(drRow(dcColumn))
                    Case ("TTCO20") : _TenderType20Count.Value = CDec(drRow(dcColumn))
                    Case ("TTAM1") : _TenderType1Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM2") : _TenderType2Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM3") : _TenderType3Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM4") : _TenderType4Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM5") : _TenderType5Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM6") : _TenderType6Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM7") : _TenderType7Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM8") : _TenderType8Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM9") : _TenderType9Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM10") : _TenderType10Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM11") : _TenderType11Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM12") : _TenderType12Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM13") : _TenderType13Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM14") : _TenderType14Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM15") : _TenderType15Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM16") : _TenderType16Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM17") : _TenderType17Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM18") : _TenderType18Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM19") : _TenderType19Amount.Value = CDec(drRow(dcColumn))
                    Case ("TTAM20") : _TenderType20Amount.Value = CDec(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"
    Private _ID As New ColField(Of Integer)("FKEY", 1, "ID", True, True)
    Private _FLOT As New ColField(Of Decimal)("FLOT", 0, "Float", False, False)
    Private _PICK As New ColField(Of Decimal)("PICK", 0, "Pick", False, False)
    Private _NormalSalesCount As New ColField(Of Decimal)("NUMB1", 0, "Normal Sales Count", False, False)
    Private _SalesCorrectionsCount As New ColField(Of Decimal)("NUMB2", 0, "Sales Corrections Count", False, False)
    Private _RefundsCount As New ColField(Of Decimal)("NUMB3", 0, "Sales Corrections Count", False, False)
    Private _RefundsCorrectionsCount As New ColField(Of Decimal)("NUMB4", 0, "Refunds Corrections Count", False, False)
    Private _CashierSignOnCount As New ColField(Of Decimal)("NUMB5", 0, "Cashier Sign On Count", False, False)
    Private _CashierSignOffCount As New ColField(Of Decimal)("NUMB6", 0, "Cashier Sign Off Count", False, False)
    Private _MiscIncomeCount As New ColField(Of Decimal)("NUMB7", 0, "Misc Income Count", False, False)
    Private _PaidOutCount As New ColField(Of Decimal)("NUMB8", 0, "Paid Out Count", False, False)
    Private _OpenDrawerCount As New ColField(Of Decimal)("NUMB9", 0, "Open Draw Count", False, False)
    Private _ReprintLogoCount As New ColField(Of Decimal)("NUMB10", 0, "Reprint Logo Count", False, False)
    Private _ViodCount As New ColField(Of Decimal)("NUMB11", 0, "Void Count", False, False)
    Private _PriceViolationsCount As New ColField(Of Decimal)("NUMB12", 0, "Price Violations Count", False, False)
    Private _UnknownTypeCount As New ColField(Of Decimal)("NUMB13", 0, "Unknown Type Count", False, False)
    Private _NormalSalesTime As New ColField(Of String)("TIME1", "", "Time", False, False)
    Private _SalesCorrectionsTime As New ColField(Of String)("TIME2", "", "Time", False, False)
    Private _RefundsTime As New ColField(Of String)("TIME3", "", "Time", False, False)
    Private _RefundsCorrectionsTime As New ColField(Of String)("TIME4", "", "Time", False, False)
    Private _CashierSignOnTime As New ColField(Of String)("TIME5", "", "Time", False, False)
    Private _CashierSignOffTime As New ColField(Of String)("TIME6", "", "Time", False, False)
    Private _MiscIncomeTime As New ColField(Of String)("TIME7", "", "Time", False, False)
    Private _PaidOutTime As New ColField(Of String)("TIME8", "", "Time", False, False)
    Private _OpenDrawerTime As New ColField(Of String)("TIME9", "", "Time", False, False)
    Private _ReprintLogoTime As New ColField(Of String)("TIME10", "", "Time", False, False)
    Private _ViodTime As New ColField(Of String)("TIME11", "", "Time", False, False)
    Private _PriceViolationsTime As New ColField(Of String)("TIME12", "", "Time", False, False)
    Private _UnknownTypeTime As New ColField(Of String)("TIME13", "", "Time", False, False)
    Private _NormalSalesAmount As New ColField(Of Decimal)("AMNT1", 0, "Normal Sales Amount", False, False)
    Private _SalesCorrectionsAmount As New ColField(Of Decimal)("AMNT2", 0, "Sales Corrections Amount", False, False)
    Private _RefundsAmount As New ColField(Of Decimal)("AMNT3", 0, "Sales Corrections Amount", False, False)
    Private _RefundsCorrectionsAmount As New ColField(Of Decimal)("AMNT4", 0, "Refunds Corrections Amount", False, False)
    Private _CashierSignOnAmount As New ColField(Of Decimal)("AMNT5", 0, "Cashier Sign On Amount", False, False)
    Private _CashierSignOffAmount As New ColField(Of Decimal)("AMNT6", 0, "Cashier Sign Off Count", False, False)
    Private _MiscIncomeAmount As New ColField(Of Decimal)("AMNT7", 0, "Misc Income Amount", False, False)
    Private _PaidOutAmount As New ColField(Of Decimal)("AMNT8", 0, "Paid Out Amount", False, False)
    Private _OpenDrawerAmount As New ColField(Of Decimal)("AMNT9", 0, "Open Draw Amount", False, False)
    Private _ReprintLogoAmount As New ColField(Of Decimal)("AMNT10", 0, "Reprint Logo Amount", False, False)
    Private _ViodAmount As New ColField(Of Decimal)("AMNT11", 0, "Void Amount", False, False)
    Private _PriceViolationsAmount As New ColField(Of Decimal)("AMNT12", 0, "Price Violations Amount", False, False)
    Private _UnknownTypeAmount As New ColField(Of Decimal)("AMNT13", 0, "Unknown Type Amount", False, False)
    Private _TenderType1Count As New ColField(Of Decimal)("TTCO1", 0, "Tender Type 1 Count", False, False)
    Private _TenderType2Count As New ColField(Of Decimal)("TTCO2", 0, "Tender Type 2 Count", False, False)
    Private _TenderType3Count As New ColField(Of Decimal)("TTCO3", 0, "Tender Type 3 Count", False, False)
    Private _TenderType4Count As New ColField(Of Decimal)("TTCO4", 0, "Tender Type 4 Count", False, False)
    Private _TenderType5Count As New ColField(Of Decimal)("TTCO5", 0, "Tender Type 5 Count", False, False)
    Private _TenderType6Count As New ColField(Of Decimal)("TTCO6", 0, "Tender Type 6 Count", False, False)
    Private _TenderType7Count As New ColField(Of Decimal)("TTCO7", 0, "Tender Type 7 Count", False, False)
    Private _TenderType8Count As New ColField(Of Decimal)("TTCO8", 0, "Tender Type 8 Count", False, False)
    Private _TenderType9Count As New ColField(Of Decimal)("TTCO9", 0, "Tender Type 9 Count", False, False)
    Private _TenderType10Count As New ColField(Of Decimal)("TTCO10", 0, "Tender Type 10 Count", False, False)
    Private _TenderType11Count As New ColField(Of Decimal)("TTCO11", 0, "Tender Type 11 Count", False, False)
    Private _TenderType12Count As New ColField(Of Decimal)("TTCO12", 0, "Tender Type 12 Count", False, False)
    Private _TenderType13Count As New ColField(Of Decimal)("TTCO13", 0, "Tender Type 13 Count", False, False)
    Private _TenderType14Count As New ColField(Of Decimal)("TTCO14", 0, "Tender Type 14 Count", False, False)
    Private _TenderType15Count As New ColField(Of Decimal)("TTCO15", 0, "Tender Type 15 Count", False, False)
    Private _TenderType16Count As New ColField(Of Decimal)("TTCO16", 0, "Tender Type 16 Count", False, False)
    Private _TenderType17Count As New ColField(Of Decimal)("TTCO17", 0, "Tender Type 17 Count", False, False)
    Private _TenderType18Count As New ColField(Of Decimal)("TTCO18", 0, "Tender Type 18 Count", False, False)
    Private _TenderType19Count As New ColField(Of Decimal)("TTCO19", 0, "Tender Type 19 Count", False, False)
    Private _TenderType20Count As New ColField(Of Decimal)("TTCO20", 0, "Tender Type 20 Count", False, False)
    Private _TenderType1Amount As New ColField(Of Decimal)("TTAM1", 0, "Tender Type 1 Amount", False, False)
    Private _TenderType2Amount As New ColField(Of Decimal)("TTAM2", 0, "Tender Type 2 Amount", False, False)
    Private _TenderType3Amount As New ColField(Of Decimal)("TTAM3", 0, "Tender Type 3 Amount", False, False)
    Private _TenderType4Amount As New ColField(Of Decimal)("TTAM4", 0, "Tender Type 4 Amount", False, False)
    Private _TenderType5Amount As New ColField(Of Decimal)("TTAM5", 0, "Tender Type 5 Amount", False, False)
    Private _TenderType6Amount As New ColField(Of Decimal)("TTAM6", 0, "Tender Type 6 Amount", False, False)
    Private _TenderType7Amount As New ColField(Of Decimal)("TTAM7", 0, "Tender Type 7 Amount", False, False)
    Private _TenderType8Amount As New ColField(Of Decimal)("TTAM8", 0, "Tender Type 8 Amount", False, False)
    Private _TenderType9Amount As New ColField(Of Decimal)("TTAM9", 0, "Tender Type 9 Amount", False, False)
    Private _TenderType10Amount As New ColField(Of Decimal)("TTAM10", 0, "Tender Type 10 Amount", False, False)
    Private _TenderType11Amount As New ColField(Of Decimal)("TTAM11", 0, "Tender Type 11 Amount", False, False)
    Private _TenderType12Amount As New ColField(Of Decimal)("TTAM12", 0, "Tender Type 12 Amount", False, False)
    Private _TenderType13Amount As New ColField(Of Decimal)("TTAM13", 0, "Tender Type 13 Amount", False, False)
    Private _TenderType14Amount As New ColField(Of Decimal)("TTAM14", 0, "Tender Type 14 Amount", False, False)
    Private _TenderType15Amount As New ColField(Of Decimal)("TTAM15", 0, "Tender Type 15 Amount", False, False)
    Private _TenderType16Amount As New ColField(Of Decimal)("TTAM16", 0, "Tender Type 16 Amount", False, False)
    Private _TenderType17Amount As New ColField(Of Decimal)("TTAM17", 0, "Tender Type 17 Amount", False, False)
    Private _TenderType18Amount As New ColField(Of Decimal)("TTAM18", 0, "Tender Type 18 Amount", False, False)
    Private _TenderType19Amount As New ColField(Of Decimal)("TTAM19", 0, "Tender Type 19 Amount", False, False)
    Private _TenderType20Amount As New ColField(Of Decimal)("TTAM20", 0, "Tender Type 20 Amount", False, False)

#End Region

#Region "Field Properties"

    Public Property Id() As ColField(Of Integer)
        Get
            Id = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Float() As ColField(Of Decimal)
        Get
            Float = _FLOT
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _FLOT = value
        End Set
    End Property
    Public Property Pick() As ColField(Of Decimal)
        Get
            Pick = _PICK
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PICK = value
        End Set
    End Property
    Public Property NormalSalesCount() As ColField(Of Decimal)
        Get
            NormalSalesCount = _NormalSalesCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NormalSalesCount = value
        End Set
    End Property
    Public Property SalesCorrectionsCount() As ColField(Of Decimal)
        Get
            SalesCorrectionsCount = _SalesCorrectionsCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesCorrectionsCount = value
        End Set
    End Property
    Public Property RefundsCount() As ColField(Of Decimal)
        Get
            RefundsCount = _RefundsCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundsCount = value
        End Set
    End Property
    Public Property RefundsCorrectionsCount() As ColField(Of Decimal)
        Get
            RefundsCorrectionsCount = _RefundsCorrectionsCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundsCorrectionsCount = value
        End Set
    End Property
    Public Property CashierSignOnCount() As ColField(Of Decimal)
        Get
            CashierSignOnCount = _CashierSignOnCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CashierSignOnCount = value
        End Set
    End Property
    Public Property CashierSignOffCount() As ColField(Of Decimal)
        Get
            CashierSignOffCount = _CashierSignOffCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CashierSignOffCount = value
        End Set
    End Property
    Public Property MiscIncomeCount() As ColField(Of Decimal)
        Get
            MiscIncomeCount = _MiscIncomeCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount = value
        End Set
    End Property
    Public Property PaidOutCount() As ColField(Of Decimal)
        Get
            PaidOutCount = _PaidOutCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PaidOutCount = value
        End Set
    End Property
    Public Property OpenDrawerCount() As ColField(Of Decimal)
        Get
            OpenDrawerCount = _OpenDrawerCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OpenDrawerCount = value
        End Set
    End Property
    Public Property ReprintLogoCount() As ColField(Of Decimal)
        Get
            ReprintLogoCount = _ReprintLogoCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReprintLogoCount = value
        End Set
    End Property
    Public Property ViodCount() As ColField(Of Decimal)
        Get
            ViodCount = _ViodCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ViodCount = value
        End Set
    End Property
    Public Property PriceViolationsCount() As ColField(Of Decimal)
        Get
            PriceViolationsCount = _PriceViolationsCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceViolationsCount = value
        End Set
    End Property
    Public Property UnknownTypeCount() As ColField(Of Decimal)
        Get
            UnknownTypeCount = _UnknownTypeCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnknownTypeCount = value
        End Set
    End Property
    Public Property NormalSalesTime() As ColField(Of String)
        Get
            NormalSalesTime = _NormalSalesTime
        End Get
        Set(ByVal value As ColField(Of String))
            _NormalSalesTime = value
        End Set
    End Property
    Public Property SalesCorrectionsTime() As ColField(Of String)
        Get
            SalesCorrectionsTime = _SalesCorrectionsTime
        End Get
        Set(ByVal value As ColField(Of String))
            _SalesCorrectionsTime = value
        End Set
    End Property
    Public Property RefundsTime() As ColField(Of String)
        Get
            RefundsTime = _RefundsTime
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundsTime = value
        End Set
    End Property
    Public Property RefundsCorrectionsTime() As ColField(Of String)
        Get
            RefundsCorrectionsTime = _RefundsCorrectionsTime
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundsCorrectionsTime = value
        End Set
    End Property
    Public Property CashierSignOnTime() As ColField(Of String)
        Get
            CashierSignOnTime = _CashierSignOnTime
        End Get
        Set(ByVal value As ColField(Of String))
            _CashierSignOnTime = value
        End Set
    End Property
    Public Property CashierSignOffTime() As ColField(Of String)
        Get
            CashierSignOffTime = _CashierSignOffTime
        End Get
        Set(ByVal value As ColField(Of String))
            _CashierSignOffTime = value
        End Set
    End Property
    Public Property MiscIncomeTime() As ColField(Of String)
        Get
            MiscIncomeTime = _MiscIncomeTime
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscIncomeTime = value
        End Set
    End Property
    Public Property PaidOutTime() As ColField(Of String)
        Get
            PaidOutTime = _PaidOutTime
        End Get
        Set(ByVal value As ColField(Of String))
            _PaidOutTime = value
        End Set
    End Property
    Public Property OpenDrawerTime() As ColField(Of String)
        Get
            OpenDrawerTime = _OpenDrawerTime
        End Get
        Set(ByVal value As ColField(Of String))
            _OpenDrawerTime = value
        End Set
    End Property
    Public Property ReprintLogoTime() As ColField(Of String)
        Get
            ReprintLogoTime = _ReprintLogoTime
        End Get
        Set(ByVal value As ColField(Of String))
            _ReprintLogoTime = value
        End Set
    End Property
    Public Property ViodTime() As ColField(Of String)
        Get
            ViodTime = _ViodTime
        End Get
        Set(ByVal value As ColField(Of String))
            _ViodTime = value
        End Set
    End Property
    Public Property PriceViolationsTime() As ColField(Of String)
        Get
            PriceViolationsTime = _PriceViolationsTime
        End Get
        Set(ByVal value As ColField(Of String))
            _PriceViolationsTime = value
        End Set
    End Property
    Public Property UnknownTypeTime() As ColField(Of String)
        Get
            UnknownTypeTime = _UnknownTypeTime
        End Get
        Set(ByVal value As ColField(Of String))
            _UnknownTypeTime = value
        End Set
    End Property
    Public Property NormalSalesAmount() As ColField(Of Decimal)
        Get
            NormalSalesAmount = _NormalSalesAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NormalSalesAmount = value
        End Set
    End Property
    Public Property SalesCorrectionsAmount() As ColField(Of Decimal)
        Get
            SalesCorrectionsAmount = _SalesCorrectionsAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesCorrectionsAmount = value
        End Set
    End Property
    Public Property RefundsAmount() As ColField(Of Decimal)
        Get
            RefundsAmount = _RefundsAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundsAmount = value
        End Set
    End Property
    Public Property RefundsCorrectionsAmount() As ColField(Of Decimal)
        Get
            RefundsCorrectionsAmount = _RefundsCorrectionsAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundsCorrectionsAmount = value
        End Set
    End Property
    Public Property CashierSignOnAmount() As ColField(Of Decimal)
        Get
            CashierSignOnAmount = _CashierSignOnAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CashierSignOnAmount = value
        End Set
    End Property
    Public Property CashierSignOffAmount() As ColField(Of Decimal)
        Get
            CashierSignOffAmount = _CashierSignOffAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CashierSignOffAmount = value
        End Set
    End Property
    Public Property MiscIncomeAmount() As ColField(Of Decimal)
        Get
            MiscIncomeAmount = _MiscIncomeAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeAmount = value
        End Set
    End Property
    Public Property PaidOutAmount() As ColField(Of Decimal)
        Get
            PaidOutAmount = _PaidOutAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PaidOutAmount = value
        End Set
    End Property
    Public Property OpenDrawerAmount() As ColField(Of Decimal)
        Get
            OpenDrawerAmount = _OpenDrawerAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OpenDrawerAmount = value
        End Set
    End Property
    Public Property ReprintLogoAmount() As ColField(Of Decimal)
        Get
            ReprintLogoAmount = _ReprintLogoAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReprintLogoAmount = value
        End Set
    End Property
    Public Property ViodAmount() As ColField(Of Decimal)
        Get
            ViodAmount = _ViodAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ViodAmount = value
        End Set
    End Property
    Public Property PriceViolationsAmount() As ColField(Of Decimal)
        Get
            PriceViolationsAmount = _PriceViolationsAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PriceViolationsAmount = value
        End Set
    End Property
    Public Property UnknownTypeAmount() As ColField(Of Decimal)
        Get
            UnknownTypeAmount = _UnknownTypeAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _UnknownTypeAmount = value
        End Set
    End Property
    Public Property TenderType1Count() As ColField(Of Decimal)
        Get
            TenderType1Count = _TenderType1Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType1Count = value
        End Set
    End Property
    Public Property TenderType2Count() As ColField(Of Decimal)
        Get
            TenderType2Count = _TenderType2Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType2Count = value
        End Set
    End Property
    Public Property TenderType3Count() As ColField(Of Decimal)
        Get
            TenderType3Count = _TenderType3Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType3Count = value
        End Set
    End Property
    Public Property TenderType4Count() As ColField(Of Decimal)
        Get
            TenderType4Count = _TenderType4Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType4Count = value
        End Set
    End Property
    Public Property TenderType5Count() As ColField(Of Decimal)
        Get
            TenderType5Count = _TenderType5Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType5Count = value
        End Set
    End Property
    Public Property TenderType6Count() As ColField(Of Decimal)
        Get
            TenderType6Count = _TenderType6Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType6Count = value
        End Set
    End Property
    Public Property TenderType7Count() As ColField(Of Decimal)
        Get
            TenderType7Count = _TenderType7Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType7Count = value
        End Set
    End Property
    Public Property TenderType8Count() As ColField(Of Decimal)
        Get
            TenderType8Count = _TenderType8Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType8Count = value
        End Set
    End Property
    Public Property TenderType9Count() As ColField(Of Decimal)
        Get
            TenderType9Count = _TenderType9Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType9Count = value
        End Set
    End Property
    Public Property TenderType10Count() As ColField(Of Decimal)
        Get
            TenderType10Count = _TenderType10Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType10Count = value
        End Set
    End Property
    Public Property TenderType11Count() As ColField(Of Decimal)
        Get
            TenderType11Count = _TenderType11Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType11Count = value
        End Set
    End Property
    Public Property TenderType12Count() As ColField(Of Decimal)
        Get
            TenderType12Count = _TenderType12Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType12Count = value
        End Set
    End Property
    Public Property TenderType13Count() As ColField(Of Decimal)
        Get
            TenderType13Count = _TenderType13Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType13Count = value
        End Set
    End Property
    Public Property TenderType14Count() As ColField(Of Decimal)
        Get
            TenderType14Count = _TenderType14Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType14Count = value
        End Set
    End Property
    Public Property TenderType15Count() As ColField(Of Decimal)
        Get
            TenderType15Count = _TenderType15Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType15Count = value
        End Set
    End Property
    Public Property TenderType16Count() As ColField(Of Decimal)
        Get
            TenderType16Count = _TenderType16Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType16Count = value
        End Set
    End Property
    Public Property TenderType17Count() As ColField(Of Decimal)
        Get
            TenderType17Count = _TenderType17Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType17Count = value
        End Set
    End Property
    Public Property TenderType18Count() As ColField(Of Decimal)
        Get
            TenderType18Count = _TenderType18Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType18Count = value
        End Set
    End Property
    Public Property TenderType19Count() As ColField(Of Decimal)
        Get
            TenderType19Count = _TenderType19Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType19Count = value
        End Set
    End Property
    Public Property TenderType20Count() As ColField(Of Decimal)
        Get
            TenderType20Count = _TenderType20Count
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType20Count = value
        End Set
    End Property
    Public Property TenderType1Amount() As ColField(Of Decimal)
        Get
            TenderType1Amount = _TenderType1Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType1Amount = value
        End Set
    End Property
    Public Property TenderType2Amount() As ColField(Of Decimal)
        Get
            TenderType2Amount = _TenderType2Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType2Amount = value
        End Set
    End Property
    Public Property TenderType3Amount() As ColField(Of Decimal)
        Get
            TenderType3Amount = _TenderType3Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType3Amount = value
        End Set
    End Property
    Public Property TenderType4Amount() As ColField(Of Decimal)
        Get
            TenderType4Amount = _TenderType4Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType4Amount = value
        End Set
    End Property
    Public Property TenderType5Amount() As ColField(Of Decimal)
        Get
            TenderType5Amount = _TenderType5Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType5Amount = value
        End Set
    End Property
    Public Property TenderType6Amount() As ColField(Of Decimal)
        Get
            TenderType6Amount = _TenderType6Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType6Amount = value
        End Set
    End Property
    Public Property TenderType7Amount() As ColField(Of Decimal)
        Get
            TenderType7Amount = _TenderType7Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType7Amount = value
        End Set
    End Property
    Public Property TenderType8Amount() As ColField(Of Decimal)
        Get
            TenderType8Amount = _TenderType8Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType8Amount = value
        End Set
    End Property
    Public Property TenderType9Amount() As ColField(Of Decimal)
        Get
            TenderType9Amount = _TenderType9Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType9Amount = value
        End Set
    End Property
    Public Property TenderType10Amount() As ColField(Of Decimal)
        Get
            TenderType10Amount = _TenderType10Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType10Amount = value
        End Set
    End Property
    Public Property TenderType11Amount() As ColField(Of Decimal)
        Get
            TenderType11Amount = _TenderType11Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType11Amount = value
        End Set
    End Property
    Public Property TenderType12Amount() As ColField(Of Decimal)
        Get
            TenderType12Amount = _TenderType12Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType12Amount = value
        End Set
    End Property
    Public Property TenderType13Amount() As ColField(Of Decimal)
        Get
            TenderType13Amount = _TenderType13Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType13Amount = value
        End Set
    End Property
    Public Property TenderType14Amount() As ColField(Of Decimal)
        Get
            TenderType14Amount = _TenderType14Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType14Amount = value
        End Set
    End Property
    Public Property TenderType15Amount() As ColField(Of Decimal)
        Get
            TenderType15Amount = _TenderType15Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType15Amount = value
        End Set
    End Property
    Public Property TenderType16Amount() As ColField(Of Decimal)
        Get
            TenderType16Amount = _TenderType16Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType16Amount = value
        End Set
    End Property
    Public Property TenderType17Amount() As ColField(Of Decimal)
        Get
            TenderType17Amount = _TenderType17Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType17Amount = value
        End Set
    End Property
    Public Property TenderType18Amount() As ColField(Of Decimal)
        Get
            TenderType18Amount = _TenderType18Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType18Amount = value
        End Set
    End Property
    Public Property TenderType19Amount() As ColField(Of Decimal)
        Get
            TenderType19Amount = _TenderType19Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType19Amount = value
        End Set
    End Property
    Public Property TenderType20Amount() As ColField(Of Decimal)
        Get
            TenderType20Amount = _TenderType20Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TenderType20Amount = value
        End Set
    End Property

#End Region

End Class
