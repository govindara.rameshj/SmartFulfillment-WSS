﻿Imports System.Data
Imports System.Data.SqlClient

<Serializable()> Public Class cSalesHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLTOTS"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_CashierID)
        BOFields.Add(_TranTime)
        BOFields.Add(_SupervisiorID)
        BOFields.Add(_TranCode)
        BOFields.Add(_OpenDrawCode)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_Description)
        BOFields.Add(_OrderNumber)
        BOFields.Add(_AccountSale)
        BOFields.Add(_Voided)
        BOFields.Add(_VoidSupervisor)
        BOFields.Add(_TrainingMode)
        BOFields.Add(_DailyUpdateProc)
        BOFields.Add(_ExternalDocNumber)
        BOFields.Add(_SupervisorUsed)
        BOFields.Add(_StoreNumber)
        BOFields.Add(_MerchandiseAmount)
        BOFields.Add(_NonMerchandiseAmount)
        BOFields.Add(_TaxAmount)
        BOFields.Add(_DicountAmount)
        BOFields.Add(_DiscountSupervisor)
        BOFields.Add(_TotalSalesAmount)
        BOFields.Add(_CustomerAcctNumber)
        BOFields.Add(_CustomerAcctCardNumber)
        BOFields.Add(_AccountUpdComplete)
        BOFields.Add(_FromDCOOrders)
        BOFields.Add(_TransactionComplete)
        BOFields.Add(_EmployeeDiscountOnly)
        BOFields.Add(_RefundCashier)
        BOFields.Add(_RefundSupervisor)
        BOFields.Add(_VatRate1)
        BOFields.Add(_VatRate2)
        BOFields.Add(_VatRate3)
        BOFields.Add(_VatRate4)
        BOFields.Add(_VatRate5)
        BOFields.Add(_VatRate6)
        BOFields.Add(_VatRate7)
        BOFields.Add(_VatRate8)
        BOFields.Add(_VatRate9)
        BOFields.Add(_VatSymbol1)
        BOFields.Add(_VatSymbol2)
        BOFields.Add(_VatSymbol3)
        BOFields.Add(_VatSymbol4)
        BOFields.Add(_VatSymbol5)
        BOFields.Add(_VatSymbol6)
        BOFields.Add(_VatSymbol7)
        BOFields.Add(_VatSymbol8)
        BOFields.Add(_VatSymbol9)
        BOFields.Add(_ExVATValue1)
        BOFields.Add(_ExVATValue2)
        BOFields.Add(_ExVATValue3)
        BOFields.Add(_ExVATValue4)
        BOFields.Add(_ExVATValue5)
        BOFields.Add(_ExVATValue6)
        BOFields.Add(_ExVATValue7)
        BOFields.Add(_ExVATValue8)
        BOFields.Add(_ExVATValue9)
        BOFields.Add(_VATValue1)
        BOFields.Add(_VATValue2)
        BOFields.Add(_VATValue3)
        BOFields.Add(_VATValue4)
        BOFields.Add(_VATValue5)
        BOFields.Add(_VATValue6)
        BOFields.Add(_VATValue7)
        BOFields.Add(_VATValue8)
        BOFields.Add(_VATValue9)
        BOFields.Add(_TransParked)
        BOFields.Add(_RefundManager)
        BOFields.Add(_TenderOverideCode)
        BOFields.Add(_RecoveredFromParked)
        BOFields.Add(_Offline)
        BOFields.Add(_GiftTokensPrinted)
        BOFields.Add(_ColleagueCardNumber)
        BOFields.Add(_RTI)
        BOFields.Add(_SaveStatus)
        BOFields.Add(_SaveSequenceNo)
        BOFields.Add(_CashBalUpdated)
        BOFields.Add(_CardNumber)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("DATE1") : _TranDate.Value = CDate(drRow(dcColumn))
                    Case ("TILL") : _TillID.Value = CStr(drRow(dcColumn))
                    Case ("TRAN") : _TranNumber.Value = CStr(drRow(dcColumn))
                    Case ("CASH") : _CashierID.Value = CStr(drRow(dcColumn))
                    Case ("TIME") : _TranTime.Value = CStr(drRow(dcColumn))
                    Case ("SUPV") : _SupervisiorID.Value = CStr(drRow(dcColumn))
                    Case ("TCOD") : _TranCode.Value = CStr(drRow(dcColumn))
                    Case ("OPEN") : _OpenDrawCode.Value = CInt(drRow(dcColumn))
                    Case ("MISC") : _ReasonCode.Value = CInt(drRow(dcColumn))
                    Case ("DESCR") : _Description.Value = CStr(drRow(dcColumn))
                    Case ("ORDN") : _OrderNumber.Value = CStr(drRow(dcColumn))
                    Case ("ACCT") : _AccountSale.Value = CBool(drRow(dcColumn))
                    Case ("VOID") : _Voided.Value = CBool(drRow(dcColumn))
                    Case ("VSUP") : _VoidSupervisor.Value = CStr(drRow(dcColumn))
                    Case ("TMOD") : _TrainingMode.Value = CBool(drRow(dcColumn))
                    Case ("PROC") : _DailyUpdateProc.Value = CBool(drRow(dcColumn))
                    Case ("DOCN") : _ExternalDocNumber.Value = CStr(drRow(dcColumn))
                    Case ("SUSE") : _SupervisorUsed.Value = CBool(drRow(dcColumn))
                    Case ("STOR") : _StoreNumber.Value = CStr(drRow(dcColumn))
                    Case ("MERC") : _MerchandiseAmount.Value = CDec(drRow(dcColumn))
                    Case ("NMER") : _NonMerchandiseAmount.Value = CDec(drRow(dcColumn))
                    Case ("TAXA") : _TaxAmount.Value = CDec(drRow(dcColumn))
                    Case ("DISC") : _DicountAmount.Value = CDec(drRow(dcColumn))
                    Case ("DSUP") : _DiscountSupervisor.Value = CStr(drRow(dcColumn))
                    Case ("TOTL") : _TotalSalesAmount.Value = CDec(drRow(dcColumn))
                    Case ("ACCN") : _CustomerAcctNumber.Value = CStr(drRow(dcColumn))
                    Case ("CARD") : _CustomerAcctCardNumber.Value = CStr(drRow(dcColumn))
                    Case ("AUPD") : _AccountUpdComplete.Value = CBool(drRow(dcColumn))
                    Case ("BACK") : _FromDCOOrders.Value = CBool(drRow(dcColumn))
                    Case ("ICOM") : _TransactionComplete.Value = CBool(drRow(dcColumn))
                    Case ("IEMP") : _EmployeeDiscountOnly.Value = CBool(drRow(dcColumn))
                    Case ("RCAS") : _RefundCashier.Value = CStr(drRow(dcColumn))
                    Case ("RSUP") : _RefundSupervisor.Value = CStr(drRow(dcColumn))
                    Case ("VATR1") : _VatRate1.Value = CDec(drRow(dcColumn))
                    Case ("VATR2") : _VatRate2.Value = CDec(drRow(dcColumn))
                    Case ("VATR3") : _VatRate3.Value = CDec(drRow(dcColumn))
                    Case ("VATR4") : _VatRate4.Value = CDec(drRow(dcColumn))
                    Case ("VATR5") : _VatRate5.Value = CDec(drRow(dcColumn))
                    Case ("VATR6") : _VatRate6.Value = CDec(drRow(dcColumn))
                    Case ("VATR7") : _VatRate7.Value = CDec(drRow(dcColumn))
                    Case ("VATR8") : _VatRate8.Value = CDec(drRow(dcColumn))
                    Case ("VATR9") : _VatRate9.Value = CDec(drRow(dcColumn))
                    Case ("VSYM1") : _VatSymbol1.Value = CStr(drRow(dcColumn))
                    Case ("VSYM2") : _VatSymbol2.Value = CStr(drRow(dcColumn))
                    Case ("VSYM3") : _VatSymbol3.Value = CStr(drRow(dcColumn))
                    Case ("VSYM4") : _VatSymbol4.Value = CStr(drRow(dcColumn))
                    Case ("VSYM5") : _VatSymbol5.Value = CStr(drRow(dcColumn))
                    Case ("VSYM6") : _VatSymbol6.Value = CStr(drRow(dcColumn))
                    Case ("VSYM7") : _VatSymbol7.Value = CStr(drRow(dcColumn))
                    Case ("VSYM8") : _VatSymbol8.Value = CStr(drRow(dcColumn))
                    Case ("VSYM9") : _VatSymbol9.Value = CStr(drRow(dcColumn))
                    Case ("XVAT1") : _ExVATValue1.Value = CDec(drRow(dcColumn))
                    Case ("XVAT2") : _ExVATValue2.Value = CDec(drRow(dcColumn))
                    Case ("XVAT3") : _ExVATValue3.Value = CDec(drRow(dcColumn))
                    Case ("XVAT4") : _ExVATValue4.Value = CDec(drRow(dcColumn))
                    Case ("XVAT5") : _ExVATValue5.Value = CDec(drRow(dcColumn))
                    Case ("XVAT6") : _ExVATValue6.Value = CDec(drRow(dcColumn))
                    Case ("XVAT7") : _ExVATValue7.Value = CDec(drRow(dcColumn))
                    Case ("XVAT8") : _ExVATValue8.Value = CDec(drRow(dcColumn))
                    Case ("XVAT9") : _ExVATValue9.Value = CDec(drRow(dcColumn))
                    Case ("VATV1") : _VATValue1.Value = CDec(drRow(dcColumn))
                    Case ("VATV2") : _VATValue2.Value = CDec(drRow(dcColumn))
                    Case ("VATV3") : _VATValue3.Value = CDec(drRow(dcColumn))
                    Case ("VATV4") : _VATValue4.Value = CDec(drRow(dcColumn))
                    Case ("VATV5") : _VATValue5.Value = CDec(drRow(dcColumn))
                    Case ("VATV6") : _VATValue6.Value = CDec(drRow(dcColumn))
                    Case ("VATV7") : _VATValue7.Value = CDec(drRow(dcColumn))
                    Case ("VATV8") : _VATValue8.Value = CDec(drRow(dcColumn))
                    Case ("VATV9") : _VATValue9.Value = CDec(drRow(dcColumn))
                    Case ("PARK") : _TransParked.Value = CBool(drRow(dcColumn))
                    Case ("RMAN") : _RefundManager.Value = CStr(drRow(dcColumn))
                    Case ("TOCD") : _TenderOverideCode.Value = CStr(drRow(dcColumn))
                    Case ("PKRC") : _RecoveredFromParked.Value = CBool(drRow(dcColumn))
                    Case ("REMO") : _Offline.Value = CBool(drRow(dcColumn))
                    Case ("GTPN") : _GiftTokensPrinted.Value = CDec(drRow(dcColumn))
                    Case ("CCRD") : _ColleagueCardNumber.Value = CStr(drRow(dcColumn))
                    Case ("RTI") : _RTI.Value = CStr(drRow(dcColumn))
                    Case ("SSTA") : _SaveStatus.Value = CStr(drRow(dcColumn))
                    Case ("SSEQ") : _SaveSequenceNo.Value = CStr(drRow(dcColumn))
                    Case ("CBBU") : _CashBalUpdated.Value = CStr(drRow(dcColumn))
                    Case ("CARD_NO") : _CardNumber.Value = CStr(drRow(dcColumn))
                    Case "RTICOUNT"
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _CashierID As New ColField(Of String)("CASH", "000", "Cashier ID", False, False)
    Private _TranTime As New ColField(Of String)("TIME", "000000", "Transaction Time", False, False)
    Private _SupervisiorID As New ColField(Of String)("SUPV", "000", "Supervisor ID", False, False)
    Private _TranCode As New ColField(Of String)("TCOD", "00", "Transaction Code", False, False)

    Private _OpenDrawCode As New ColField(Of Integer)("OPEN", 0, "Open Drawer Code", False, False)
    Private _ReasonCode As New ColField(Of Integer)("MISC", 0, "Reason Code", False, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _OrderNumber As New ColField(Of String)("ORDN", "000000", "Order Number", False, False)
    Private _AccountSale As New ColField(Of Boolean)("ACCT", False, "Account Sale", False, False)
    Private _Voided As New ColField(Of Boolean)("VOID", False, "Voided", False, False)
    Private _VoidSupervisor As New ColField(Of String)("VSUP", "000", "Voided Supervisor", False, False)
    Private _TrainingMode As New ColField(Of Boolean)("TMOD", False, "Training Mode", False, False)
    Private _DailyUpdateProc As New ColField(Of Boolean)("PROC", False, "Daily Updated Proc", False, False)
    Private _ExternalDocNumber As New ColField(Of String)("DOCN", "", "External Document Number", False, False)
    Private _SupervisorUsed As New ColField(Of Boolean)("SUSE", False, "Supervisor Used", False, False)

    Private _StoreNumber As New ColField(Of String)("STOR", "000", "Store Number", False, False)
    Private _MerchandiseAmount As New ColField(Of Decimal)("MERC", 0, "Merchandise Amount", False, False, 2)
    Private _NonMerchandiseAmount As New ColField(Of Decimal)("NMER", 0, "Non Merchandise Amount", False, False, 2)
    Private _TaxAmount As New ColField(Of Decimal)("TAXA", 0, "Tax Amount", False, False, 2)
    Private _DicountAmount As New ColField(Of Decimal)("DISC", 0, "Discount Amount", False, False, 2)
    Private _DiscountSupervisor As New ColField(Of String)("DSUP", "000", "Discount Supervisor", False, False)
    Private _TotalSalesAmount As New ColField(Of Decimal)("TOTL", 0, "Total Sales Amount", False, False, 2)
    Private _CustomerAcctNumber As New ColField(Of String)("ACCN", "000000", "Customer Account Number", False, False)
    Private _CustomerAcctCardNumber As New ColField(Of String)("CARD", "00", "Customer Account Card Number", False, False)
    Private _AccountUpdComplete As New ColField(Of Boolean)("AUPD", False, "Account Update Complete", False, False)
    Private _FromDCOOrders As New ColField(Of Boolean)("BACK", False, "From DC Orders", False, False)
    Private _TransactionComplete As New ColField(Of Boolean)("ICOM", False, "Transaction Complete", False, False)
    Private _EmployeeDiscountOnly As New ColField(Of Boolean)("IEMP", False, "Employee Discount Only", False, False)
    Private _RefundCashier As New ColField(Of String)("RCAS", "000", "Refund Cashier", False, False)
    Private _RefundSupervisor As New ColField(Of String)("RSUP", "000", "Refund Supervisor", False, False)

    Private _VatRate1 As New ColField(Of Decimal)("VATR1", 0, "VAT Rate 1", False, False, 2)
    Private _VatRate2 As New ColField(Of Decimal)("VATR2", 0, "VAT Rate 2", False, False, 2)
    Private _VatRate3 As New ColField(Of Decimal)("VATR3", 0, "VAT Rate 3", False, False, 2)
    Private _VatRate4 As New ColField(Of Decimal)("VATR4", 0, "VAT Rate 4", False, False, 2)
    Private _VatRate5 As New ColField(Of Decimal)("VATR5", 0, "VAT Rate 5", False, False, 2)
    Private _VatRate6 As New ColField(Of Decimal)("VATR6", 0, "VAT Rate 6", False, False, 2)
    Private _VatRate7 As New ColField(Of Decimal)("VATR7", 0, "VAT Rate 7", False, False, 2)
    Private _VatRate8 As New ColField(Of Decimal)("VATR8", 0, "VAT Rate 8", False, False, 2)
    Private _VatRate9 As New ColField(Of Decimal)("VATR9", 0, "VAT Rate 9", False, False, 2)

    Private _VatSymbol1 As New ColField(Of String)("VSYM1", "", "VAT Symbol 1", False, False)
    Private _VatSymbol2 As New ColField(Of String)("VSYM2", "", "VAT Symbol 2", False, False)
    Private _VatSymbol3 As New ColField(Of String)("VSYM3", "", "VAT Symbol 3", False, False)
    Private _VatSymbol4 As New ColField(Of String)("VSYM4", "", "VAT Symbol 4", False, False)
    Private _VatSymbol5 As New ColField(Of String)("VSYM5", "", "VAT Symbol 5", False, False)
    Private _VatSymbol6 As New ColField(Of String)("VSYM6", "", "VAT Symbol 6", False, False)
    Private _VatSymbol7 As New ColField(Of String)("VSYM7", "", "VAT Symbol 7", False, False)
    Private _VatSymbol8 As New ColField(Of String)("VSYM8", "", "VAT Symbol 8", False, False)
    Private _VatSymbol9 As New ColField(Of String)("VSYM9", "", "VAT Symbol 9", False, False)

    Private _ExVATValue1 As New ColField(Of Decimal)("XVAT1", 0, "Ex VAT Value 1", False, False, 2)
    Private _ExVATValue2 As New ColField(Of Decimal)("XVAT2", 0, "Ex VAT Value 2", False, False, 2)
    Private _ExVATValue3 As New ColField(Of Decimal)("XVAT3", 0, "Ex VAT Value 3", False, False, 2)
    Private _ExVATValue4 As New ColField(Of Decimal)("XVAT4", 0, "Ex VAT Value 4", False, False, 2)
    Private _ExVATValue5 As New ColField(Of Decimal)("XVAT5", 0, "Ex VAT Value 5", False, False, 2)
    Private _ExVATValue6 As New ColField(Of Decimal)("XVAT6", 0, "Ex VAT Value 6", False, False, 2)
    Private _ExVATValue7 As New ColField(Of Decimal)("XVAT7", 0, "Ex VAT Value 7", False, False, 2)
    Private _ExVATValue8 As New ColField(Of Decimal)("XVAT8", 0, "Ex VAT Value 8", False, False, 2)
    Private _ExVATValue9 As New ColField(Of Decimal)("XVAT9", 0, "Ex VAT Value 9", False, False, 2)

    Private _VATValue1 As New ColField(Of Decimal)("VATV1", 0, "VAT Value 1", False, False, 2)
    Private _VATValue2 As New ColField(Of Decimal)("VATV2", 0, "VAT Value 2", False, False, 2)
    Private _VATValue3 As New ColField(Of Decimal)("VATV3", 0, "VAT Value 3", False, False, 2)
    Private _VATValue4 As New ColField(Of Decimal)("VATV4", 0, "VAT Value 4", False, False, 2)
    Private _VATValue5 As New ColField(Of Decimal)("VATV5", 0, "VAT Value 5", False, False, 2)
    Private _VATValue6 As New ColField(Of Decimal)("VATV6", 0, "VAT Value 6", False, False, 2)
    Private _VATValue7 As New ColField(Of Decimal)("VATV7", 0, "VAT Value 7", False, False, 2)
    Private _VATValue8 As New ColField(Of Decimal)("VATV8", 0, "VAT Value 8", False, False, 2)
    Private _VATValue9 As New ColField(Of Decimal)("VATV9", 0, "VAT Value 9", False, False, 2)

    Private _TransParked As New ColField(Of Boolean)("PARK", False, "Transaction Parked", False, False)
    Private _RefundManager As New ColField(Of String)("RMAN", "000", "Refund Manager", False, False)
    Private _TenderOverideCode As New ColField(Of String)("TOCD", "00", "Tender Override Code", False, False)
    Private _RecoveredFromParked As New ColField(Of Boolean)("PKRC", False, "Recovered from Parked", False, False)
    Private _Offline As New ColField(Of Boolean)("REMO", False, "Offline", False, False)
    Private _GiftTokensPrinted As New ColField(Of Decimal)("GTPN", 0, "Gift Tokens Printed", False, False)
    Private _ColleagueCardNumber As New ColField(Of String)("CCRD", "", "Colleague Card Number", False, False)
    Private _RTI As New ColField(Of String)("RTI", "", "RTI", False, False)
    Private _SaveStatus As New ColField(Of String)("SSTA", "", "Save Status", False, False)
    Private _SaveSequenceNo As New ColField(Of String)("SSEQ", "", "Save Sequence", False, False)
    Private _CashBalUpdated As New ColField(Of String)("CBBU", "", "Cash Bal Updated", False, False)
    Private _CardNumber As New ColField(Of String)("CARD_NO", "", "Card Number", False, False)

#End Region

#Region "Field Properties"

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property CashierID() As ColField(Of String)
        Get
            CashierID = _CashierID
        End Get
        Set(ByVal value As ColField(Of String))
            _CashierID = value
        End Set
    End Property
    Public Property TransactionTime() As ColField(Of String)
        Get
            TransactionTime = _TranTime
        End Get
        Set(ByVal value As ColField(Of String))
            _TranTime = value
        End Set
    End Property
    Public Property SupervisiorNo() As ColField(Of String)
        Get
            SupervisiorNo = _SupervisiorID
        End Get
        Set(ByVal value As ColField(Of String))
            _SupervisiorID = value
        End Set
    End Property
    Public Property TransactionCode() As ColField(Of String)
        Get
            TransactionCode = _TranCode
        End Get
        Set(ByVal value As ColField(Of String))
            _TranCode = value
        End Set
    End Property
    Public Property OpenDrawCode() As ColField(Of Integer)
        Get
            OpenDrawCode = _OpenDrawCode
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OpenDrawCode = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of Integer)
        Get
            ReasonCode = _ReasonCode
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReasonCode = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property
    Public Property AccountSale() As ColField(Of Boolean)
        Get
            AccountSale = _AccountSale
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AccountSale = value
        End Set
    End Property
    Public Property Voided() As ColField(Of Boolean)
        Get
            Voided = _Voided
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Voided = value
        End Set
    End Property
    Public Property VoidSupervisor() As ColField(Of String)
        Get
            VoidSupervisor = _VoidSupervisor
        End Get
        Set(ByVal value As ColField(Of String))
            _VoidSupervisor = value
        End Set
    End Property
    Public Property TrainingMode() As ColField(Of Boolean)
        Get
            TrainingMode = _TrainingMode
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _TrainingMode = value
        End Set
    End Property
    Public Property DailyUpdateProc() As ColField(Of Boolean)
        Get
            DailyUpdateProc = _DailyUpdateProc
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DailyUpdateProc = value
        End Set
    End Property
    Public Property ExternalDocumentNo() As ColField(Of String)
        Get
            ExternalDocumentNo = _ExternalDocNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ExternalDocNumber = value
        End Set
    End Property
    Public Property SupervisiorUsed() As ColField(Of Boolean)
        Get
            SupervisiorUsed = _SupervisorUsed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SupervisorUsed = value
        End Set
    End Property
    Public Property StoreNo() As ColField(Of String)
        Get
            StoreNo = _StoreNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreNumber = value
        End Set
    End Property
    Public Property MerchandiseAmount() As ColField(Of Decimal)
        Get
            MerchandiseAmount = _MerchandiseAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MerchandiseAmount = value
        End Set
    End Property
    Public Property NonMerchandiseAmount() As ColField(Of Decimal)
        Get
            NonMerchandiseAmount = _NonMerchandiseAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _NonMerchandiseAmount = value
        End Set
    End Property
    Public Property TaxAmount() As ColField(Of Decimal)
        Get
            TaxAmount = _TaxAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TaxAmount = value
        End Set
    End Property
    Public Property DicountAmount() As ColField(Of Decimal)
        Get
            DicountAmount = _DicountAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DicountAmount = value
        End Set
    End Property
    Public Property DiscountSupervisor() As ColField(Of String)
        Get
            DiscountSupervisor = _DiscountSupervisor
        End Get
        Set(ByVal value As ColField(Of String))
            _DiscountSupervisor = value
        End Set
    End Property
    Public Property TotalSalesAmount() As ColField(Of Decimal)
        Get
            TotalSalesAmount = _TotalSalesAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TotalSalesAmount = value
        End Set
    End Property
    Public Property CustomerAcctNo() As ColField(Of String)
        Get
            CustomerAcctNo = _CustomerAcctNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAcctNumber = value
        End Set
    End Property
    Public Property CustomerAcctCardNo() As ColField(Of String)
        Get
            CustomerAcctCardNo = _CustomerAcctCardNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerAcctCardNumber = value
        End Set
    End Property
    Public Property AccountUpdComplete() As ColField(Of Boolean)
        Get
            AccountUpdComplete = _AccountUpdComplete
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AccountUpdComplete = value
        End Set
    End Property
    Public Property FromDCOOrders() As ColField(Of Boolean)
        Get
            FromDCOOrders = _FromDCOOrders
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _FromDCOOrders = value
        End Set
    End Property
    Public Property TransactionComplete() As ColField(Of Boolean)
        Get
            TransactionComplete = _TransactionComplete
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _TransactionComplete = value
        End Set
    End Property
    Public Property EmployeeDiscountOnly() As ColField(Of Boolean)
        Get
            EmployeeDiscountOnly = _EmployeeDiscountOnly
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _EmployeeDiscountOnly = value
        End Set
    End Property
    Public Property RefundCashier() As ColField(Of String)
        Get
            RefundCashier = _RefundCashier
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundCashier = value
        End Set
    End Property
    Public Property RefundSupervisor() As ColField(Of String)
        Get
            RefundSupervisor = _RefundSupervisor
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundSupervisor = value
        End Set
    End Property
    Public Property VatRate1() As ColField(Of Decimal)
        Get
            VatRate1 = _VatRate1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate1 = value
        End Set
    End Property
    Public Property VatRate2() As ColField(Of Decimal)
        Get
            VatRate2 = _VatRate2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate2 = value
        End Set
    End Property
    Public Property VatRate3() As ColField(Of Decimal)
        Get
            VatRate3 = _VatRate3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate3 = value
        End Set
    End Property
    Public Property VatRate4() As ColField(Of Decimal)
        Get
            VatRate4 = _VatRate4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate4 = value
        End Set
    End Property
    Public Property VatRate5() As ColField(Of Decimal)
        Get
            VatRate5 = _VatRate5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate5 = value
        End Set
    End Property
    Public Property VatRate6() As ColField(Of Decimal)
        Get
            VatRate6 = _VatRate6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate6 = value
        End Set
    End Property
    Public Property VatRate7() As ColField(Of Decimal)
        Get
            VatRate7 = _VatRate7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate7 = value
        End Set
    End Property
    Public Property VatRate8() As ColField(Of Decimal)
        Get
            VatRate8 = _VatRate8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate8 = value
        End Set
    End Property
    Public Property VatRate9() As ColField(Of Decimal)
        Get
            VatRate9 = _VatRate9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VatRate9 = value
        End Set
    End Property
    Public Property VatSymbol1() As ColField(Of String)
        Get
            VatSymbol1 = _VatSymbol1
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol1 = value
        End Set
    End Property
    Public Property VatSymbol2() As ColField(Of String)
        Get
            VatSymbol2 = _VatSymbol2
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol2 = value
        End Set
    End Property
    Public Property VatSymbol3() As ColField(Of String)
        Get
            VatSymbol3 = _VatSymbol3
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol3 = value
        End Set
    End Property
    Public Property VatSymbol4() As ColField(Of String)
        Get
            VatSymbol4 = _VatSymbol4
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol4 = value
        End Set
    End Property
    Public Property VatSymbol5() As ColField(Of String)
        Get
            VatSymbol5 = _VatSymbol5
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol5 = value
        End Set
    End Property
    Public Property VatSymbol6() As ColField(Of String)
        Get
            VatSymbol6 = _VatSymbol6
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol6 = value
        End Set
    End Property
    Public Property VatSymbol7() As ColField(Of String)
        Get
            VatSymbol7 = _VatSymbol7
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol7 = value
        End Set
    End Property
    Public Property VatSymbol8() As ColField(Of String)
        Get
            VatSymbol8 = _VatSymbol8
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol8 = value
        End Set
    End Property
    Public Property VatSymbol9() As ColField(Of String)
        Get
            VatSymbol9 = _VatSymbol9
        End Get
        Set(ByVal value As ColField(Of String))
            _VatSymbol9 = value
        End Set
    End Property
    Public Property ExVATValue1() As ColField(Of Decimal)
        Get
            ExVATValue1 = _ExVATValue1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue1 = value
        End Set
    End Property
    Public Property ExVATValue2() As ColField(Of Decimal)
        Get
            ExVATValue2 = _ExVATValue2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue2 = value
        End Set
    End Property
    Public Property ExVATValue3() As ColField(Of Decimal)
        Get
            ExVATValue3 = _ExVATValue3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue3 = value
        End Set
    End Property
    Public Property ExVATValue4() As ColField(Of Decimal)
        Get
            ExVATValue4 = _ExVATValue4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue4 = value
        End Set
    End Property
    Public Property ExVATValue5() As ColField(Of Decimal)
        Get
            ExVATValue5 = _ExVATValue5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue5 = value
        End Set
    End Property
    Public Property ExVATValue6() As ColField(Of Decimal)
        Get
            ExVATValue6 = _ExVATValue6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue6 = value
        End Set
    End Property
    Public Property ExVATValue7() As ColField(Of Decimal)
        Get
            ExVATValue7 = _ExVATValue7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue7 = value
        End Set
    End Property
    Public Property ExVATValue8() As ColField(Of Decimal)
        Get
            ExVATValue8 = _ExVATValue8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue8 = value
        End Set
    End Property
    Public Property ExVATValue9() As ColField(Of Decimal)
        Get
            ExVATValue9 = _ExVATValue9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExVATValue9 = value
        End Set
    End Property
    Public Property VATValue1() As ColField(Of Decimal)
        Get
            VATValue1 = _VATValue1
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue1 = value
        End Set
    End Property
    Public Property VATValue2() As ColField(Of Decimal)
        Get
            VATValue2 = _VATValue2
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue2 = value
        End Set
    End Property
    Public Property VATValue3() As ColField(Of Decimal)
        Get
            VATValue3 = _VATValue3
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue3 = value
        End Set
    End Property
    Public Property VATValue4() As ColField(Of Decimal)
        Get
            VATValue4 = _VATValue4
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue4 = value
        End Set
    End Property
    Public Property VATValue5() As ColField(Of Decimal)
        Get
            VATValue5 = _VATValue5
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue5 = value
        End Set
    End Property
    Public Property VATValue6() As ColField(Of Decimal)
        Get
            VATValue6 = _VATValue6
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue6 = value
        End Set
    End Property
    Public Property VATValue7() As ColField(Of Decimal)
        Get
            VATValue7 = _VATValue7
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue7 = value
        End Set
    End Property
    Public Property VATValue8() As ColField(Of Decimal)
        Get
            VATValue8 = _VATValue8
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue8 = value
        End Set
    End Property
    Public Property VATValue9() As ColField(Of Decimal)
        Get
            VATValue9 = _VATValue9
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VATValue9 = value
        End Set
    End Property
    Public Property TransParked() As ColField(Of Boolean)
        Get
            TransParked = _TransParked
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _TransParked = value
        End Set
    End Property
    Public Property RefundManager() As ColField(Of String)
        Get
            RefundManager = _RefundManager
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundManager = value
        End Set
    End Property
    Public Property TenderOverideCode() As ColField(Of String)
        Get
            TenderOverideCode = _TenderOverideCode
        End Get
        Set(ByVal value As ColField(Of String))
            _TenderOverideCode = value
        End Set
    End Property
    Public Property RecoveredFromParked() As ColField(Of Boolean)
        Get
            RecoveredFromParked = _RecoveredFromParked
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _RecoveredFromParked = value
        End Set
    End Property
    Public Property Offline() As ColField(Of Boolean)
        Get
            Offline = _Offline
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Offline = value
        End Set
    End Property
    Public Property GiftTokensPrinted() As ColField(Of Decimal)
        Get
            GiftTokensPrinted = _GiftTokensPrinted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _GiftTokensPrinted = value
        End Set
    End Property
    Public Property ColleagueCardNo() As ColField(Of String)
        Get
            ColleagueCardNo = _ColleagueCardNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ColleagueCardNumber = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property
    Public Property SaveStatus() As ColField(Of String)
        Get
            SaveStatus = _SaveStatus
        End Get
        Set(ByVal value As ColField(Of String))
            _SaveStatus = value
        End Set
    End Property
    Public Property SaveSequenceNo() As ColField(Of String)
        Get
            SaveSequenceNo = _SaveSequenceNo
        End Get
        Set(ByVal value As ColField(Of String))
            _SaveSequenceNo = value
        End Set
    End Property
    Public Property CashBalUpdated() As ColField(Of String)
        Get
            CashBalUpdated = _CashBalUpdated
        End Get
        Set(ByVal value As ColField(Of String))
            _CashBalUpdated = value
        End Set
    End Property
    Public Property CardNumber() As ColField(Of String)
        Get
            CardNumber = _CardNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CardNumber = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Headers As List(Of cSalesHeader) = Nothing
    Private _Lines As List(Of cSalesLine) = Nothing
    Private _Paids As List(Of cSalesPaid) = Nothing

    Public Property Headers() As List(Of cSalesHeader)
        Get
            Return _Headers
        End Get
        Set(ByVal value As List(Of cSalesHeader))
            _Headers = value
        End Set
    End Property
    Public Property Lines() As List(Of cSalesLine)
        Get
            If _Lines Is Nothing Then
                Dim lin As New cSalesLine(Oasys3DB)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TransDate, _TranDate.Value)
                lin.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TillID, _TillID.Value)
                lin.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TransNo, _TranNumber.Value)
                _Lines = lin.LoadMatches
            End If
            Return _Lines
        End Get
        Set(ByVal value As List(Of cSalesLine))
            _Lines = value
        End Set
    End Property

    Public Property Paids() As List(Of cSalesPaid)

        Get
            If _Paids Is Nothing Then
                Dim p As New cSalesPaid(Oasys3DB)

                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TransDate, _TranDate.Value)
                p.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TillID, _TillID.Value)
                p.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TransNo, _TranNumber.Value)
                _Paids = p.LoadMatches
            End If
            Return _Paids
        End Get
        Set(ByVal value As List(Of cSalesPaid))
            _Paids = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetTodaysUnprocessedRecords(ByRef dteLiveDate As Date) As List(Of cSalesHeader)

        Try

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, DailyUpdateProc, False)
            JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, TransDate, dteLiveDate)
            Return LoadMatches()

        Catch ex As Exception

            Throw ex
            Return Nothing

        End Try

    End Function

    Public Function UpdateProcessedFlag(ByVal blnFlag As Boolean) As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_DailyUpdateProc.ColumnName, blnFlag)
            Oasys3DB.SetWhereParameter(_TillID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TillID.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_TranDate.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TranDate.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_TranNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TranNumber.Value)
            If CBool(Oasys3DB.Update()) Then Return True
            Return False

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub LoadValidTransactions(ByVal SaleDate As Date, Optional ByVal WithLinesPayments As Boolean = False)

        Using saleHeader As New cSalesHeader(Oasys3DB)
            saleHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleHeader.TransDate, SaleDate)
            saleHeader.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            saleHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleHeader.Voided, False)
            saleHeader.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            saleHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, saleHeader.TrainingMode, False)
            _Headers = saleHeader.LoadMatches
        End Using

        If WithLinesPayments Then
            Dim lines As New List(Of cSalesLine)
            Using saleLine As New cSalesLine(Oasys3DB)
                saleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleLine.TransDate, SaleDate)
                saleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                saleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleLine.LineReversed, False)
                lines = saleLine.LoadMatches
            End Using

            Dim paids As New List(Of cSalesPaid)
            Using salePaid As New cSalesPaid(Oasys3DB)
                salePaid.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, salePaid.TransDate, SaleDate)
                paids = salePaid.LoadMatches
            End Using

            'match lines and payments to headers
            For Each h As cSalesHeader In _Headers
                Dim tran As String = h.TransactionNo.Value
                Dim till As String = h.TillID.Value
                h.Lines = lines.FindAll(Function(l As cSalesLine) l.TransNo.Value = tran AndAlso l.TillID.Value = till)
                h.Paids = paids.FindAll(Function(p As cSalesPaid) p.TransNo.Value = tran AndAlso p.TillID.Value = till)
            Next
        End If

    End Sub

    ' Select all transactions for period, including lines and paids.
    ' For WebOrder it also selects the orders which are passed into WSS after last Nightly Routine run
    Public Sub LoadTransactionsForFeedPeriod(periodID As Integer)

        Dim cmd As New SqlCommand("dbo.GetTransactionsForPeriod")
        cmd.Parameters.Add("@periodID", SqlDbType.Int).Value = periodID
        Dim dataset As DataSet = Oasys3DB.ExecuteCMD(cmd)


        Dim saleLineRows = dataset.Tables(1).AsEnumerable()
        Dim allLines = saleLineRows.Select(Function(l)
                                               Dim line = New cSalesLine()
                                               line.LoadFromRow(l)
                                               Return line
                                           End Function).ToList()

        Dim salePaidRows = dataset.Tables(2).AsEnumerable()
        Dim allPaids = salePaidRows.Select(Function(paidRow)
                                               Dim paid = New cSalesPaid()
                                               paid.LoadFromRow(paidRow)
                                               Return paid
                                           End Function).ToList()

        Dim saleHeaderRows = dataset.Tables(0).AsEnumerable()

        _Headers = saleHeaderRows.Select(Function(r)
                                             Dim header = New cSalesHeader(Oasys3DB)
                                             header.LoadFromRow(r)
                                             header.Lines = allLines.Where(Function(l) l.TransNo.Value = header.TransactionNo.Value AndAlso l.TransDate.Value = header.TransDate.Value _
                                                                               AndAlso l.TillID.Value = header.TillID.Value).ToList()
                                             header.Paids = allPaids.Where(Function(p) p.TransNo.Value = header.TransactionNo.Value AndAlso p.TransDate.Value = header.TransDate.Value _
                                                                               AndAlso p.TillID.Value = header.TillID.Value).ToList()
                                             Return header
                                         End Function).ToList()
    End Sub

#End Region

End Class
