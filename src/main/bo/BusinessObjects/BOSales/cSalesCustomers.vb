﻿<Serializable()> Public Class cSalesCustomers
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLRCUS"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_CustomerName)
        BOFields.Add(_PostCode)
        BOFields.Add(_OrigTransStore)
        BOFields.Add(_OrigTransDate)
        BOFields.Add(_OrigTranTill)
        BOFields.Add(_OrigTranNumb)
        BOFields.Add(_HouseName)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_PhoneNumber)
        BOFields.Add(_LineNumber)
        BOFields.Add(_OrigTranValidated)
        BOFields.Add(_OrigTenderType)
        BOFields.Add(_RefundReason)
        BOFields.Add(_LabelsRequired)
        BOFields.Add(_MobileNumber)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesCustomers)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesCustomers)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesCustomers))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesCustomers(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _CustomerName As New ColField(Of String)("NAME", "", "Customer Name", False, False)
    Private _PostCode As New ColField(Of String)("POST", "", "Post Code", False, False)
    Private _OrigTransStore As New ColField(Of String)("OSTR", "", "Original Store", False, False)
    Private _OrigTransDate As New ColField(Of Date)("ODAT", Nothing, "Original Date", False, False)
    Private _OrigTranTill As New ColField(Of String)("OTIL", "00", "Original Till ID", False, False)
    Private _OrigTranNumb As New ColField(Of String)("OTRN", "0000", "Original Transaction Number", False, False)
    Private _HouseName As New ColField(Of String)("HNAM", "", "House Name", False, False)
    Private _AddressLine1 As New ColField(Of String)("HAD1", "", "Address 1", False, False)
    Private _AddressLine2 As New ColField(Of String)("HAD2", "", "Address 2", False, False)
    Private _AddressLine3 As New ColField(Of String)("HAD3", "", "Address 3", False, False)
    Private _PhoneNumber As New ColField(Of String)("PHON", "", "Phone Number", False, False)
    Private _LineNumber As New ColField(Of Integer)("NUMB", 0, "Line Number", False, False)
    Private _OrigTranValidated As New ColField(Of Boolean)("OVAL", False, "Original Transaction Validated", False, False)
    Private _OrigTenderType As New ColField(Of Decimal)("OTEN", 0, "Original Tender Type", False, False)
    Private _RefundReason As New ColField(Of String)("RCOD", "00", "Refund Reason", False, False)
    Private _LabelsRequired As New ColField(Of Boolean)("LABL", False, "Labels Required", False, False)
    Private _MobileNumber As New ColField(Of String)("MOBP", "", "Mobile Number", False, False)

#End Region

#Region "Field Properties"

    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property CustomerName() As ColField(Of String)
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerName = value
        End Set
    End Property
    Public Property PostCode() As ColField(Of String)
        Get
            PostCode = _PostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _PostCode = value
        End Set
    End Property
    Public Property OrigTransStore() As ColField(Of String)
        Get
            OrigTransStore = _OrigTransStore
        End Get
        Set(ByVal value As ColField(Of String))
            _OrigTransStore = value
        End Set
    End Property
    Public Property OrigTransDate() As ColField(Of Date)
        Get
            OrigTransDate = _OrigTransDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _OrigTransDate = value
        End Set
    End Property
    Public Property OrigTranTil() As ColField(Of String)
        Get
            OrigTranTil = _OrigTranTill
        End Get
        Set(ByVal value As ColField(Of String))
            _OrigTranTill = value
        End Set
    End Property
    Public Property OrigTranNumb() As ColField(Of String)
        Get
            OrigTranNumb = _OrigTranNumb
        End Get
        Set(ByVal value As ColField(Of String))
            _OrigTranNumb = value
        End Set
    End Property
    Public Property HouseName() As ColField(Of String)
        Get
            HouseName = _HouseName
        End Get
        Set(ByVal value As ColField(Of String))
            _HouseName = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property PhoneNo() As ColField(Of String)
        Get
            PhoneNo = _PhoneNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _PhoneNumber = value
        End Set
    End Property
    Public Property LineNo() As ColField(Of Integer)
        Get
            LineNo = _LineNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LineNumber = value
        End Set
    End Property
    Public Property OrigTranValidated() As ColField(Of Boolean)
        Get
            OrigTranValidated = _OrigTranValidated
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OrigTranValidated = value
        End Set
    End Property
    Public Property OrigTenType() As ColField(Of Decimal)
        Get
            OrigTenType = _OrigTenderType
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrigTenderType = value
        End Set
    End Property
    Public Property RefundReason() As ColField(Of String)
        Get
            RefundReason = _RefundReason
        End Get
        Set(ByVal value As ColField(Of String))
            _RefundReason = value
        End Set
    End Property
    Public Property LabelsRequired() As ColField(Of Boolean)
        Get
            LabelsRequired = _LabelsRequired
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _LabelsRequired = value
        End Set
    End Property
    Public Property MobilePhoneNo() As ColField(Of String)
        Get
            MobilePhoneNo = _MobileNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _MobileNumber = value
        End Set
    End Property

#End Region

End Class
