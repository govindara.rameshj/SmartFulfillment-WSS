﻿<Serializable()> Public Class cPromiseLineItem
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLOLIN"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_CompetitorName)
        BOFields.Add(_CompetitorAddressLine1)
        BOFields.Add(_CompetitorAddressLine2)
        BOFields.Add(_CompetitorAddressLine3)
        BOFields.Add(_CompetitorAddressLine4)
        BOFields.Add(_CompetitorPostCode)
        BOFields.Add(_CompetitorPhoneNo)
        BOFields.Add(_CompetitorFaxNo)
        BOFields.Add(_CompetitorPrice)
        BOFields.Add(_VatInclusivePrices)
        BOFields.Add(_CompetitorPriceConverted)
        BOFields.Add(_PreviousPurchase)
        BOFields.Add(_OriginalTransStore)
        BOFields.Add(_OriginalTranTillID)
        BOFields.Add(_OriginalTranNumb)
        BOFields.Add(_Filler)
        BOFields.Add(_OriginalTransDate)
        BOFields.Add(_OriginalSellingPrice)
        BOConstraints.Add(New Object() {_TranDate, _TillID, _TranNumber, _SequenceNumber})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPromiseLineItem)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPromiseLineItem)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPromiseLineItem))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPromiseLineItem(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _SequenceNumber As New ColField(Of Integer)("NUMB", 0, "Sequence Number", True, False)
    Private _CompetitorName As New ColField(Of String)("CNAM", "", "Competitor Name", False, False)
    Private _CompetitorAddressLine1 As New ColField(Of String)("CAD1", "", "Competitor Address 1", False, False)
    Private _CompetitorAddressLine2 As New ColField(Of String)("CAD2", "", "Competitor Address 2", False, False)
    Private _CompetitorAddressLine3 As New ColField(Of String)("CAD3", "", "Competitor Address 3", False, False)
    Private _CompetitorAddressLine4 As New ColField(Of String)("CAD4", "", "Competitor Address 4", False, False)
    Private _CompetitorPostCode As New ColField(Of String)("CPST", "", "Competitor Post Code", False, False)
    Private _CompetitorPhoneNo As New ColField(Of String)("CPHN", "", "Competitor Phone Number", False, False)
    Private _CompetitorFaxNo As New ColField(Of String)("CFAX", "", "Competitor Fax", False, False)
    Private _CompetitorPrice As New ColField(Of Decimal)("EPRI", 0, "Competitor Price", False, False)
    Private _VatInclusivePrices As New ColField(Of Boolean)("EVAT", False, "VAT Inclusive Prices", False, False)
    Private _CompetitorPriceConverted As New ColField(Of Decimal)("CPRI", 0, "Competitor Price Converted", False, False, 2)
    Private _PreviousPurchase As New ColField(Of Boolean)("PREV", False, "Previous Purchase", False, False)
    Private _OriginalTransStore As New ColField(Of String)("OSTR", "", "Original Store", False, False)
    Private _OriginalTranTillID As New ColField(Of String)("OTIL", "00", "Original Till ID", False, False)
    Private _OriginalTranNumb As New ColField(Of String)("OTRN", "0000", "Original Transaction Number", False, False)
    Private _OriginalTransDate As New ColField(Of Date)("ODAT", Nothing, "Original Transaction Date", False, False)
    Private _OriginalSellingPrice As New ColField(Of Decimal)("OPRI", 0, "Original Selling Price", False, False, 2)
    Private _Filler As New ColField(Of Boolean)("FILL", False, "Filler", False, False)

#End Region

#Region "Field Properties"

    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of Integer)
        Get
            SequenceNo = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SequenceNumber = value
        End Set
    End Property
    Public Property CompetitorName() As ColField(Of String)
        Get
            CompetitorName = _CompetitorName
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorName = value
        End Set
    End Property
    Public Property CompetitorAddressLine1() As ColField(Of String)
        Get
            CompetitorAddressLine1 = _CompetitorAddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorAddressLine1 = value
        End Set
    End Property
    Public Property CompetitorAddressLine2() As ColField(Of String)
        Get
            CompetitorAddressLine2 = _CompetitorAddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorAddressLine2 = value
        End Set
    End Property
    Public Property CompetitorAddressLine3() As ColField(Of String)
        Get
            CompetitorAddressLine3 = _CompetitorAddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorAddressLine3 = value
        End Set
    End Property
    Public Property CompetitorAddressLine4() As ColField(Of String)
        Get
            Return _CompetitorAddressLine4
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorAddressLine4 = value
        End Set
    End Property
    Public Property CompetitorPostCode() As ColField(Of String)
        Get
            CompetitorPostCode = _CompetitorPostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorPostCode = value
        End Set
    End Property
    Public Property CompetitorPhoneNo() As ColField(Of String)
        Get
            CompetitorPhoneNo = _CompetitorPhoneNo
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorPhoneNo = value
        End Set
    End Property
    Public Property CompetitorFaxNo() As ColField(Of String)
        Get
            CompetitorFaxNo = _CompetitorFaxNo
        End Get
        Set(ByVal value As ColField(Of String))
            _CompetitorFaxNo = value
        End Set
    End Property
    Public Property CompetitorPrice() As ColField(Of Decimal)
        Get
            CompetitorPrice = _CompetitorPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CompetitorPrice = value
        End Set
    End Property
    Public Property VatInclusivePrices() As ColField(Of Boolean)
        Get
            VatInclusivePrices = _VatInclusivePrices
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _VatInclusivePrices = value
        End Set
    End Property
    Public Property CompetitorPriceConverted() As ColField(Of Decimal)
        Get
            CompetitorPriceConverted = _CompetitorPriceConverted
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _CompetitorPriceConverted = value
        End Set
    End Property
    Public Property PreviousPurchase() As ColField(Of Boolean)
        Get
            PreviousPurchase = _PreviousPurchase
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PreviousPurchase = value
        End Set
    End Property
    Public Property OriginalTransStore() As ColField(Of String)
        Get
            OriginalTransStore = _OriginalTransStore
        End Get
        Set(ByVal value As ColField(Of String))
            _OriginalTransStore = value
        End Set
    End Property
    Public Property OriginalTranTil() As ColField(Of String)
        Get
            OriginalTranTil = _OriginalTranTillID
        End Get
        Set(ByVal value As ColField(Of String))
            _OriginalTranTillID = value
        End Set
    End Property
    Public Property OriginalTranNumb() As ColField(Of String)
        Get
            OriginalTranNumb = _OriginalTranNumb
        End Get
        Set(ByVal value As ColField(Of String))
            _OriginalTranNumb = value
        End Set
    End Property
    Public Property Filler() As ColField(Of Boolean)
        Get
            Return _Filler
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Filler = value
        End Set
    End Property
    Public Property OriginalTransDate() As ColField(Of Date)
        Get
            OriginalTransDate = _OriginalTransDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _OriginalTransDate = value
        End Set
    End Property
    Public Property OriginalSellingPrice() As ColField(Of Decimal)
        Get
            OriginalSellingPrice = _OriginalSellingPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OriginalSellingPrice = value
        End Set
    End Property

#End Region

End Class
