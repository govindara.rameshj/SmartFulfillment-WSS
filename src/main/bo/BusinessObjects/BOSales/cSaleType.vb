<Serializable()> Public Class cSaleType
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "TOSOPT"
        BOFields.Add(_FKEY)
        BOFields.Add(_TOSD)
        BOFields.Add(_TEND)
        BOFields.Add(_SUPV)
        BOFields.Add(_DOCN)
        BOFields.Add(_SPRT)
        BOFields.Add(_OPEN)
        BOFields.Add(_SIGN)
        BOFields.Add(_SPEC)
        BOFields.Add(_SASA)
        BOFields.Add(_AREA)
        BOFields.Add(_SVLR)
        BOFields.Add(_MIPT)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSaleType)

        LoadBORecords(count)

        Dim col As New List(Of cSaleType)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cSaleType))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSaleType(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _FKEY As New ColField(Of String)("FKEY", "", "ID", True, False)
    Private _TOSD As New ColField(Of String)("TOSD", "", "Description", False, False)
    Private _TEND As New ColField(Of Boolean)("TEND", False, "Is Valid", False, False)
    Private _SUPV As New ColField(Of Boolean)("SUPV", False, "Supervisor Required", False, False)
    Private _DOCN As New ColField(Of Boolean)("DOCN", False, "Ask External Document", False, False)
    Private _SPRT As New ColField(Of Boolean)("SPRT", False, "Special Print", False, False)
    Private _OPEN As New ColField(Of Boolean)("OPEN", False, "Open Drawer", False, False)
    Private _SIGN As New ColField(Of Boolean)("SIGN", False, "Is Positive", False, False)
    Private _SPEC As New ColField(Of Boolean)("SPEC", False, "Is Special Tender Allowed", False, False)
    Private _SASA As New ColField(Of Boolean)("SASA", False, "Is Special Supervisor Required", False, False)
    Private _AREA As New ColField(Of Boolean)("AREA", False, "Request area code", False, False)
    Private _SVLR As New ColField(Of Boolean)("SVLR", False, "Supervisor required for line reversals", False, False)
    Private _MIPT As New ColField(Of Boolean)("MIPT", False, "Tender valid for account payment transactions", False, False)

#End Region

#Region "Field Properties"

    Public Property FKEY() As ColField(Of String)
        Get
            Return _FKEY
        End Get
        Set(ByVal value As ColField(Of String))
            _FKEY = value
        End Set
    End Property
    Public Property TOSD() As ColField(Of String)
        Get
            Return _TOSD
        End Get
        Set(ByVal value As ColField(Of String))
            _TOSD = value
        End Set
    End Property
    Public Property TEND() As ColField(Of Boolean)
        Get
            Return _TEND
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _TEND = value
        End Set
    End Property
    Public Property DOCN() As ColField(Of Boolean)
        Get
            Return _DOCN
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DOCN = value
        End Set
    End Property
    Public Property SUPV() As ColField(Of Boolean)
        Get
            Return _SUPV
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SUPV = value
        End Set
    End Property
    Public Property SPRT() As ColField(Of Boolean)
        Get
            Return _SPRT
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SPRT = value
        End Set
    End Property
    Public Property OPEN() As ColField(Of Boolean)
        Get
            Return _OPEN
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _OPEN = value
        End Set
    End Property
    Public Property SIGN() As ColField(Of Boolean)
        Get
            Return _SIGN
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SIGN = value
        End Set
    End Property
    Public Property SPEC() As ColField(Of Boolean)
        Get
            Return _SPEC
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SPEC = value
        End Set
    End Property
    Public Property SASA() As ColField(Of Boolean)
        Get
            Return _SASA
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SASA = value
        End Set
    End Property
    Public Property AREA() As ColField(Of Boolean)
        Get
            Return _AREA
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AREA = value
        End Set
    End Property
    Public Property SVLR() As ColField(Of Boolean)
        Get
            Return _SVLR
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SVLR = value
        End Set
    End Property
    Public Property MIPT() As ColField(Of Boolean)
        Get
            Return _MIPT
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _MIPT = value
        End Set
    End Property
#End Region

End Class
