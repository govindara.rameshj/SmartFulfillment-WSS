﻿Public Module mExtensions

    ''' <summary>
    ''' Get category sales in dataview ordered by category name (columns are number,name,value)
    ''' </summary>
    ''' <param name="Headers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function CategorySales(ByVal Headers As List(Of cSalesHeader)) As DataView

        Try
            Dim categories As New Dictionary(Of String, Decimal)

            'generate dictionary with category and value
            For Each h As cSalesHeader In Headers
                For Each l As cSalesLine In h.Lines
                    If l.LineReversed.Value Then Continue For
                    Dim cat As String = l.HierCategory.Value
                    Dim sales As Decimal = l.CalcPaidTotal
                    Dim value As Decimal

                    If categories.TryGetValue(cat, value) Then
                        categories.Item(cat) = value + sales
                    Else
                        categories.Add(cat, sales)
                    End If
                Next
            Next

            'get category names from hierarchy table
            If categories.Count = 0 Then Return Nothing
            Dim hie As New BOHierarchy.cHierachyCategory(Headers(0).Oasys3DB)
            hie.Load(categories.Keys.ToArray)

            'create datatable from dictionary and add category name
            Dim dt As New DataTable
            dt.Columns.Add("number", GetType(String))
            dt.Columns.Add("name", GetType(String))
            dt.Columns.Add("value", GetType(Decimal))

            For Each kvp As KeyValuePair(Of String, Decimal) In categories
                dt.Rows.Add(kvp.Key, hie.Hierarchy(kvp.Key).Description.Value, kvp.Value)
            Next

            'create dataview to sort table
            Dim dv As DataView = New DataView(dt, "", "name", DataViewRowState.CurrentRows)
            Return dv

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Returns total of foreign tender sales converted to default currency
    ''' </summary>
    ''' <param name="Paids"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function ForeignPayments(ByVal Paids As List(Of cSalesPaid)) As Decimal

        Try
            Dim total As Decimal = 0
            For Each payment As BOSales.cSalesPaid In Paids
                If payment.ForeignTendered.Value <> 0 Then
                    total += CDec(payment.ForeignTendered.Value * payment.ExchangeRate.Value * Math.Pow(10, payment.ExchangeFactor.Value))
                End If
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Get total of Lines grouped by SKU, for Extended Price Less Discounts in dataview ordered by SKU Number (columns are CategoryNumber,TotalValue)
    ''' </summary>
    ''' <param name="Headers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function LineGroupTotals(ByVal Headers As List(Of cSalesHeader)) As DataView

        Try
            Dim SKUGroups As New Dictionary(Of String, Decimal)

            'generate dictionary with category and value
            For Each hdr As cSalesHeader In Headers
                For Each sLine As cSalesLine In hdr.Lines
                    Dim SKUGroup As String = sLine.SkuNumber.Value.Substring(0, 2)
                    Dim sales As Decimal = sLine.CalcPaidTotal
                    Dim value As Decimal

                    If SKUGroups.TryGetValue(SKUGroup, value) Then
                        SKUGroups.Item(SKUGroup) = value + sales
                    Else
                        SKUGroups.Add(SKUGroup, sales)
                    End If
                Next
            Next

            If SKUGroups.Count = 0 Then Return Nothing

            'create datatable from dictionary and add category name
            Dim dt As New DataTable
            dt.Columns.Add("CategoryNumber", GetType(String))
            dt.Columns.Add("TotalValue", GetType(Decimal))

            For Each kvp As KeyValuePair(Of String, Decimal) In SKUGroups
                dt.Rows.Add(kvp.Key, kvp.Value)
            Next

            'create dataview to sort table
            Dim dv As DataView = New DataView(dt, "", "", DataViewRowState.CurrentRows)
            dv.Sort = "CategoryNumber"
            Return dv

        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Module
