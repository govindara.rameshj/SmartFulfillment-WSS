﻿<Serializable()> Public Class cSalesGiftTokenLines
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLGIFT"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_SerialNumber)
        BOFields.Add(_CashierID)
        BOFields.Add(_TranCode)
        BOFields.Add(_VoucherAmount)
        BOFields.Add(_CommedToHO)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesGiftTokenLines)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesGiftTokenLines)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesGiftTokenLines))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesGiftTokenLines(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _SequenceNumber As New ColField(Of Integer)("LINE", 0, "Sequence Number", True, False)
    Private _SerialNumber As New ColField(Of String)("SERI", "", "Serial Number", True, False)
    Private _CashierID As New ColField(Of String)("EEID", "", "Cashier ID", False, False)
    Private _TranCode As New ColField(Of String)("TYPE", "", "Transaction Code", False, False)
    Private _VoucherAmount As New ColField(Of Decimal)("AMNT", 0, "Voucher Amount", False, False, 2)
    Private _CommedToHO As New ColField(Of Boolean)("COMM", False, "Commed to HO", False, False)

#End Region

#Region "Field Properties"

    Public Property TranDate() As ColField(Of Date)
        Get
            TranDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of Integer)
        Get
            SequenceNo = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SequenceNumber = value
        End Set
    End Property
    Public Property SerialNo() As ColField(Of String)
        Get
            SerialNo = _SerialNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SerialNumber = value
        End Set
    End Property
    Public Property CashierNo() As ColField(Of String)
        Get
            CashierNo = _CashierID
        End Get
        Set(ByVal value As ColField(Of String))
            _CashierID = value
        End Set
    End Property
    Public Property TransactionCode() As ColField(Of String)
        Get
            TransactionCode = _TranCode
        End Get
        Set(ByVal value As ColField(Of String))
            _TranCode = value
        End Set
    End Property
    Public Property VoucherAmount() As ColField(Of Decimal)
        Get
            VoucherAmount = _VoucherAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _VoucherAmount = value
        End Set
    End Property
    Public Property CommedToHO() As ColField(Of Boolean)
        Get
            CommedToHO = _CommedToHO
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommedToHO = value
        End Set
    End Property

#End Region

End Class
