﻿<Serializable()> Public Class cSalesAnasNotRecognised
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLANAS"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_TranLineNumber)
        BOFields.Add(_LineSequenceNumber)
        BOFields.Add(_EanNumberScanned)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesAnasNotRecognised)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesAnasNotRecognised)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesAnasNotRecognised))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesAnasNotRecognised(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _TranLineNumber As New ColField(Of Integer)("NUMB", 0, "Transaction Line Number", True, False)
    Private _LineSequenceNumber As New ColField(Of String)("SEQN", "", "Line Sequence Number", True, False)
    Private _EanNumberScanned As New ColField(Of String)("EANN", "", "EAN Number Scanned", False, False)

#End Region

#Region "Field Properties"

    Public Property TranDate() As ColField(Of Date)
        Get
            TranDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property TransactionLineNo() As ColField(Of Integer)
        Get
            TransactionLineNo = _TranLineNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TranLineNumber = value
        End Set
    End Property
    Public Property LineSequenceNo() As ColField(Of String)
        Get
            LineSequenceNo = _LineSequenceNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _LineSequenceNumber = value
        End Set
    End Property
    Public Property EanNumberScanned() As ColField(Of String)
        Get
            EanNumberScanned = _EanNumberScanned
        End Get
        Set(ByVal value As ColField(Of String))
            _EanNumberScanned = value
        End Set
    End Property

#End Region

End Class
