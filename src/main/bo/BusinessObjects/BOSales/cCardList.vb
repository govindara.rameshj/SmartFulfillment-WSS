<Serializable()> Public Class cCardList
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CARD_LIST"
        BOFields.Add(_CardNumber)
        BOFields.Add(_CustomerName)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_AddressLine4)
        BOFields.Add(_PostCode)
        BOFields.Add(_HideAddress)
        BOFields.Add(_IsDeleted)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPromiseCustomer)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPromiseCustomer)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPromiseCustomer))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPromiseCustomer(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _CardNumber As New ColField(Of String)("CARD_NO", "", "Card Number", True, False)
    Private _CustomerName As New ColField(Of String)("CUST_NAME", "", "Customer Name", False, False)
    Private _AddressLine1 As New ColField(Of String)("CUST_ADD1", "", "Address 1", False, False)
    Private _AddressLine2 As New ColField(Of String)("CUST_ADD2", "", "Address 2", False, False)
    Private _AddressLine3 As New ColField(Of String)("CUST_ADD3", "", "Address 3", False, False)
    Private _AddressLine4 As New ColField(Of String)("CUST_ADD4", "", "Address 4", False, False)
    Private _PostCode As New ColField(Of String)("CUST_POST", "", "Post Code", False, False)
    Private _HideAddress As New ColField(Of String)("HIDE_ADDR", "", "Hide Address", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELETED", False, "Deleted", False, False)

#End Region

#Region "Field Properties"

    Public Property CardNumber() As ColField(Of String)
        Get
            CardNumber = _CardNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _CardNumber = value
        End Set
    End Property
    Public Property CustomerName() As ColField(Of String)
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As ColField(Of String))
            _CustomerName = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property AddressLine4() As ColField(Of String)
        Get
            AddressLine4 = _AddressLine4
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine4 = value
        End Set
    End Property
    Public Property PostCode() As ColField(Of String)
        Get
            PostCode = _PostCode
        End Get
        Set(ByVal value As ColField(Of String))
            _PostCode = value
        End Set
    End Property
    Public Property HideAddress() As ColField(Of String)
        Get
            HideAddress = _HideAddress
        End Get
        Set(ByVal value As ColField(Of String))
            _HideAddress = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property

    Public Sub DeleteAllRecords()
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
        Oasys3DB.Delete()
    End Sub

#End Region

End Class
