﻿<Serializable()> Public Class cSalesTransactionEvent
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DLEVNT"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_TranLineNumber)
        BOFields.Add(_EventSequenceNumber)
        BOFields.Add(_EventType)
        BOFields.Add(_DiscountAmount)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSalesTransactionEvent)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSalesTransactionEvent)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSalesTransactionEvent))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSalesTransactionEvent(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of Date)("DATE1", Nothing, "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "00", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "0000", "Transaction Number", True, False)
    Private _TranLineNumber As New ColField(Of Integer)("NUMB", 0, "Transaction Line Number", True, False)
    Private _EventSequenceNumber As New ColField(Of String)("ESEQ", "", "Event Sequence", True, False)
    Private _EventType As New ColField(Of String)("TYPE", "", "Event Type", False, False)
    Private _DiscountAmount As New ColField(Of Decimal)("AMNT", 0, "Discount Amount", False, False, 2)

#End Region

#Region "Field Properties"

    Public Property TranDate() As ColField(Of Date)
        Get
            TranDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property TransactionLineNo() As ColField(Of Integer)
        Get
            TransactionLineNo = _TranLineNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TranLineNumber = value
        End Set
    End Property
    Public Property EventSeqNo() As ColField(Of String)
        Get
            EventSeqNo = _EventSequenceNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _EventSequenceNumber = value
        End Set
    End Property
    Public Property EventType() As ColField(Of String)
        Get
            EventType = _EventType
        End Get
        Set(ByVal value As ColField(Of String))
            _EventType = value
        End Set
    End Property
    Public Property DiscountAmount() As ColField(Of Decimal)
        Get
            DiscountAmount = _DiscountAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DiscountAmount = value
        End Set
    End Property

#End Region

End Class

