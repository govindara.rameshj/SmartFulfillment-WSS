﻿<Serializable()> Public Class cSupplierDetail
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SUPDET"
        BOFields.Add(_Number)
        BOFields.Add(_IsTradanet)
        BOFields.Add(_BbcCode)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_DepotType)
        BOFields.Add(_DepotNumber)
        BOFields.Add(_DepotNotes)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_AddressLine4)
        BOFields.Add(_AddressLine5)
        BOFields.Add(_AddressPostcode)
        BOFields.Add(_PhoneNumber1)
        BOFields.Add(_PhoneNumber2)
        BOFields.Add(_FaxNumber1)
        BOFields.Add(_FaxNumber2)
        BOFields.Add(_ContactName1)
        BOFields.Add(_ContactName2)
        BOFields.Add(_LeadTime1)
        BOFields.Add(_LeadTime2)
        BOFields.Add(_LeadTime3)
        BOFields.Add(_LeadTime4)
        BOFields.Add(_LeadTime5)
        BOFields.Add(_LeadTime6)
        BOFields.Add(_LeadTime7)
        BOFields.Add(_LeadTimeFixed)
        BOFields.Add(_ReviewDay1)
        BOFields.Add(_ReviewDay2)
        BOFields.Add(_ReviewDay3)
        BOFields.Add(_ReviewDay4)
        BOFields.Add(_ReviewDay5)
        BOFields.Add(_ReviewDay6)
        BOFields.Add(_ReviewDay0)
        BOFields.Add(_SoqFrequency)
        BOFields.Add(_DeliveryCheckMethod)
        BOFields.Add(_DateOrderCloseStart)
        BOFields.Add(_DateOrderCloseEnd)
        BOFields.Add(_DateDelCloseStart)
        BOFields.Add(_DateDelCloseEnd)
        BOFields.Add(_ReturnsPolicyCode)
        BOFields.Add(_ReturnsMessageRef)
        BOFields.Add(_OrderMinType)
        BOFields.Add(_OrderMinValue)
        BOFields.Add(_OrderMinUnits)
        BOFields.Add(_OrderMinWeight)
        BOFields.Add(_TruckCapWeight)
        BOFields.Add(_TruckCapVolume)
        BOFields.Add(_TruckCapPallets)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSupplierDetail)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSupplierDetail)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSupplierDetail))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSupplierDetail(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("SUPN") : _Number.Value = drRow(dcColumn).ToString.Trim
                    Case ("TNET") : _IsTradanet.Value = CBool(drRow(dcColumn))
                    Case ("BBCC") : _BbcCode.Value = drRow(dcColumn).ToString.Trim
                    Case ("DELC") : _IsDeleted.Value = CBool(drRow(dcColumn))
                    Case ("TYPE") : _DepotType.Value = drRow(dcColumn).ToString.Trim
                    Case ("DEPO") : _DepotNumber.Value = drRow(dcColumn).ToString.Trim
                    Case ("NOTE") : _DepotNotes.Value = drRow(dcColumn).ToString.Trim
                    Case ("ADD1") : _AddressLine1.Value = drRow(dcColumn).ToString.Trim
                    Case ("ADD2") : _AddressLine2.Value = drRow(dcColumn).ToString.Trim
                    Case ("ADD3") : _AddressLine3.Value = drRow(dcColumn).ToString.Trim
                    Case ("ADD4") : _AddressLine4.Value = drRow(dcColumn).ToString.Trim
                    Case ("ADD5") : _AddressLine5.Value = drRow(dcColumn).ToString.Trim
                    Case ("POST") : _AddressPostcode.Value = drRow(dcColumn).ToString.Trim
                    Case ("PHO1") : _PhoneNumber1.Value = drRow(dcColumn).ToString.Trim
                    Case ("PHO2") : _PhoneNumber2.Value = drRow(dcColumn).ToString.Trim
                    Case ("FAX1") : _FaxNumber1.Value = drRow(dcColumn).ToString.Trim
                    Case ("FAX2") : _FaxNumber2.Value = drRow(dcColumn).ToString.Trim
                    Case ("CON1") : _ContactName1.Value = drRow(dcColumn).ToString.Trim
                    Case ("CON2") : _ContactName2.Value = drRow(dcColumn).ToString.Trim
                    Case ("VLED1") : _LeadTime1.Value = CInt(drRow(dcColumn))
                    Case ("VLED2") : _LeadTime2.Value = CInt(drRow(dcColumn))
                    Case ("VLED3") : _LeadTime3.Value = CInt(drRow(dcColumn))
                    Case ("VLED4") : _LeadTime4.Value = CInt(drRow(dcColumn))
                    Case ("VLED5") : _LeadTime5.Value = CInt(drRow(dcColumn))
                    Case ("VLED6") : _LeadTime6.Value = CInt(drRow(dcColumn))
                    Case ("VLED7") : _LeadTime7.Value = CInt(drRow(dcColumn))
                    Case ("LEAD") : _LeadTimeFixed.Value = CInt(drRow(dcColumn))
                    Case ("ReviewDay1") : _ReviewDay1.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay2") : _ReviewDay2.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay3") : _ReviewDay3.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay4") : _ReviewDay4.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay5") : _ReviewDay5.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay6") : _ReviewDay6.Value = CBool(drRow(dcColumn))
                    Case ("ReviewDay0") : _ReviewDay0.Value = CBool(drRow(dcColumn))
                    Case ("SOQF") : _SoqFrequency.Value = CInt(drRow(dcColumn))
                    Case ("CHKM") : _DeliveryCheckMethod.Value = CStr(drRow(dcColumn))
                    Case ("OCSD") : _DateOrderCloseStart.Value = CDate(drRow(dcColumn))
                    Case ("OCED") : _DateOrderCloseEnd.Value = CDate(drRow(dcColumn))
                    Case ("DCSD") : _DateDelCloseStart.Value = CDate(drRow(dcColumn))
                    Case ("DCED") : _DateDelCloseEnd.Value = CDate(drRow(dcColumn))
                    Case ("RPNO") : _ReturnsPolicyCode.Value = CStr(drRow(dcColumn))
                    Case ("RMES") : _ReturnsMessageRef.Value = CInt(drRow(dcColumn))
                    Case ("MCPT") : _OrderMinType.Value = CStr(drRow(dcColumn))
                    Case ("MCPV") : _OrderMinValue.Value = CInt(drRow(dcColumn))
                    Case ("MCPC") : _OrderMinUnits.Value = CInt(drRow(dcColumn))
                    Case ("MCPW") : _OrderMinWeight.Value = CInt(drRow(dcColumn))
                    Case ("TCPW") : _TruckCapWeight.Value = CInt(drRow(dcColumn))
                    Case ("TCPV") : _TruckCapVolume.Value = CInt(drRow(dcColumn))
                    Case ("TCPP") : _TruckCapPallets.Value = CInt(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("SUPN", "000000", "Supplier Number", True, False)
    Private _IsTradanet As New ColField(Of Boolean)("TNET", False, "Is Tradanet", False, False)
    Private _BbcCode As New ColField(Of String)("BBCC", "", "BBC Code", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELC", False, "Is Deleted", False, False)
    Private _DepotType As New ColField(Of String)("TYPE", "", "Depot Type", True, False)
    Private _DepotNumber As New ColField(Of String)("DEPO", "", "Depot Number", True, False)
    Private _DepotNotes As New ColField(Of String)("NOTE", "", "Depot Notes", False, False)
    Private _AddressLine1 As New ColField(Of String)("ADD1", "", "Address Line 1", False, False)
    Private _AddressLine2 As New ColField(Of String)("ADD2", "", "Address Line 2", False, False)
    Private _AddressLine3 As New ColField(Of String)("ADD3", "", "Address Line 3", False, False)
    Private _AddressLine4 As New ColField(Of String)("ADD4", "", "Address Line 4", False, False)
    Private _AddressLine5 As New ColField(Of String)("ADD5", "", "Address Line 5", False, False)
    Private _AddressPostcode As New ColField(Of String)("POST", "", "Address Postcode", False, False)
    Private _PhoneNumber1 As New ColField(Of String)("PHO1", "", "Phone Number 1", False, False)
    Private _PhoneNumber2 As New ColField(Of String)("PHO2", "", "Phone Number 2", False, False)
    Private _FaxNumber1 As New ColField(Of String)("FAX1", "", "Fax Number 1", False, False)
    Private _FaxNumber2 As New ColField(Of String)("FAX2", "", "Fax Number 2", False, False)
    Private _ContactName1 As New ColField(Of String)("CON1", "", "Contact Name 1", False, False)
    Private _ContactName2 As New ColField(Of String)("CON2", "", "Contact Name 2", False, False)

    Private _LeadTime7 As New ColField(Of Integer)("VLED7", 0, "Lead Time 7", False, False)
    Private _LeadTime1 As New ColField(Of Integer)("VLED1", 0, "Lead Time 1", False, False)
    Private _LeadTime2 As New ColField(Of Integer)("VLED2", 0, "Lead Time 2", False, False)
    Private _LeadTime3 As New ColField(Of Integer)("VLED3", 0, "Lead Time 3", False, False)
    Private _LeadTime4 As New ColField(Of Integer)("VLED4", 0, "Lead Time 4", False, False)
    Private _LeadTime5 As New ColField(Of Integer)("VLED5", 0, "Lead Time 5", False, False)
    Private _LeadTime6 As New ColField(Of Integer)("VLED6", 0, "Lead Time 6", False, False)
    Private _LeadTimeFixed As New ColField(Of Integer)("LEAD", 0, "Lead Time Fixed", False, False)

    Private _ReviewDay0 As New ColField(Of Boolean)("ReviewDay0", False, "Review Day 0", False, False)
    Private _ReviewDay1 As New ColField(Of Boolean)("ReviewDay1", False, "Review Day 1", False, False)
    Private _ReviewDay2 As New ColField(Of Boolean)("ReviewDay2", False, "Review Day 2", False, False)
    Private _ReviewDay3 As New ColField(Of Boolean)("ReviewDay3", False, "Review Day 3", False, False)
    Private _ReviewDay4 As New ColField(Of Boolean)("ReviewDay4", False, "Review Day 4", False, False)
    Private _ReviewDay5 As New ColField(Of Boolean)("ReviewDay5", False, "Review Day 5", False, False)
    Private _ReviewDay6 As New ColField(Of Boolean)("ReviewDay6", False, "Review Day 6", False, False)

    Private _SoqFrequency As New ColField(Of Integer)("SOQF", 0, "SOQ Frequency", False, False)
    Private _DeliveryCheckMethod As New ColField(Of String)("CHKM", "", "Delivery Check Method", False, False)
    Private _DateOrderCloseStart As New ColField(Of Date)("OCSD", Nothing, "Date Order Close Start", False, False)
    Private _DateOrderCloseEnd As New ColField(Of Date)("OCED", Nothing, "Date Order Close End", False, False)
    Private _DateDelCloseStart As New ColField(Of Date)("DCSD", Nothing, "Date Del Close Start", False, False)
    Private _DateDelCloseEnd As New ColField(Of Date)("DCED", Nothing, "Date Del Close End", False, False)
    Private _ReturnsPolicyCode As New ColField(Of String)("RPNO", "", "Returns Policy Code", False, False)
    Private _ReturnsMessageRef As New ColField(Of Integer)("RMES", 0, "Returns Message Reference", False, False)

    Private _OrderMinType As New ColField(Of String)("MCPT", "", "Order Min Type", False, False)
    Private _OrderMinValue As New ColField(Of Integer)("MCPV", 0, "Order Min Value", False, False)
    Private _OrderMinUnits As New ColField(Of Integer)("MCPC", 0, "Order Min Units", False, False)
    Private _OrderMinWeight As New ColField(Of Integer)("MCPW", 0, "Order Min Weight", False, False)

    Private _TruckCapWeight As New ColField(Of Integer)("TCPW", 0, "Truck Cap Weight", False, False)
    Private _TruckCapVolume As New ColField(Of Integer)("TCPV", 0, "Truck Cap Volume", False, False)
    Private _TruckCapPallets As New ColField(Of Integer)("TCPP", 0, "Truck Cap Pallets", False, False)

#End Region

#Region "Field Properties"

    Public Property SupplierNumber() As ColField(Of String)
        Get
            SupplierNumber = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Tradanet() As ColField(Of Boolean)
        Get
            Tradanet = _IsTradanet
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsTradanet = value
        End Set
    End Property
    Public Property BBC() As ColField(Of String)
        Get
            BBC = _BbcCode
        End Get
        Set(ByVal value As ColField(Of String))
            _BbcCode = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property DepotType() As ColField(Of String)
        Get
            DepotType = _DepotType
        End Get
        Set(ByVal value As ColField(Of String))
            _DepotType = value
        End Set
    End Property
    Public Property DepotNumber() As ColField(Of String)
        Get
            DepotNumber = _DepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DepotNumber = value
        End Set
    End Property
    Public Property DepotNotes() As ColField(Of String)
        Get
            DepotNotes = _DepotNotes
        End Get
        Set(ByVal value As ColField(Of String))
            _DepotNotes = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property AddressLine4() As ColField(Of String)
        Get
            AddressLine4 = _AddressLine4
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine4 = value
        End Set
    End Property
    Public Property AddressLine5() As ColField(Of String)
        Get
            AddressLine5 = _AddressLine5
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine5 = value
        End Set
    End Property
    Public Property AddressPostcode() As ColField(Of String)
        Get
            AddressPostcode = _AddressPostcode
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressPostcode = value
        End Set
    End Property
    Public Property PhoneNumber1() As ColField(Of String)
        Get
            PhoneNumber1 = _PhoneNumber1
        End Get
        Set(ByVal value As ColField(Of String))
            _PhoneNumber1 = value
        End Set
    End Property
    Public Property PhoneNumber2() As ColField(Of String)
        Get
            PhoneNumber2 = _PhoneNumber2
        End Get
        Set(ByVal value As ColField(Of String))
            _PhoneNumber2 = value
        End Set
    End Property
    Public Property FaxNumber1() As ColField(Of String)
        Get
            FaxNumber1 = _FaxNumber1
        End Get
        Set(ByVal value As ColField(Of String))
            _FaxNumber1 = value
        End Set
    End Property
    Public Property FaxNumber2() As ColField(Of String)
        Get
            FaxNumber2 = _FaxNumber2
        End Get
        Set(ByVal value As ColField(Of String))
            _FaxNumber2 = value
        End Set
    End Property
    Public Property ContactName1() As ColField(Of String)
        Get
            ContactName1 = _ContactName1
        End Get
        Set(ByVal value As ColField(Of String))
            _ContactName1 = value
        End Set
    End Property
    Public Property ContactName2() As ColField(Of String)
        Get
            ContactName2 = _ContactName2
        End Get
        Set(ByVal value As ColField(Of String))
            _ContactName2 = value
        End Set
    End Property

    Public Property LeadTime(ByVal Index As String) As ColField(Of Integer)
        Get

            If CDbl(Index) = 0 Then Index = "1"
            Dim ColName As String = LeadTime0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Integer))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Integer))

            If Index = "0" Then Index = "1"
            Dim ColName As String = LeadTime0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Integer)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property LeadTime0() As ColField(Of Integer)
        Get
            LeadTime0 = _LeadTime1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime1 = value
        End Set
    End Property
    Public Property LeadTime1() As ColField(Of Integer)
        Get
            LeadTime1 = _LeadTime2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime2 = value
        End Set
    End Property
    Public Property LeadTime2() As ColField(Of Integer)
        Get
            LeadTime2 = _LeadTime3
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime3 = value
        End Set
    End Property
    Public Property LeadTime3() As ColField(Of Integer)
        Get
            LeadTime3 = _LeadTime4
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime4 = value
        End Set
    End Property
    Public Property LeadTime4() As ColField(Of Integer)
        Get
            LeadTime4 = _LeadTime5
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime5 = value
        End Set
    End Property
    Public Property LeadTime5() As ColField(Of Integer)
        Get
            LeadTime5 = _LeadTime6
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime6 = value
        End Set
    End Property
    Public Property LeadTime6() As ColField(Of Integer)
        Get
            LeadTime6 = _LeadTime7
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTime7 = value
        End Set
    End Property
    Public Property LeadTimeFixed() As ColField(Of Integer)
        Get
            LeadTimeFixed = _LeadTimeFixed
        End Get
        Set(ByVal value As ColField(Of Integer))
            _LeadTimeFixed = value
        End Set
    End Property

    Public Property ReviewDay(ByVal Index As String) As ColField(Of Boolean)
        Get
            'Extract Column Name
            Dim ColName As String = ReviewDay0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Boolean))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Boolean))
            Dim ColName As String = ReviewDay0.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Boolean)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property ReviewDay0() As ColField(Of Boolean)
        Get
            Return _ReviewDay0
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay0 = value
        End Set
    End Property
    Public Property ReviewDay1() As ColField(Of Boolean)
        Get
            Return _ReviewDay1
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay1 = value
        End Set
    End Property
    Public Property ReviewDay2() As ColField(Of Boolean)
        Get
            Return _ReviewDay2
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay2 = value
        End Set
    End Property
    Public Property ReviewDay3() As ColField(Of Boolean)
        Get
            Return _ReviewDay3
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay3 = value
        End Set
    End Property
    Public Property ReviewDay4() As ColField(Of Boolean)
        Get
            Return _ReviewDay4
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay4 = value
        End Set
    End Property
    Public Property ReviewDay5() As ColField(Of Boolean)
        Get
            Return _ReviewDay5
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay5 = value
        End Set
    End Property
    Public Property ReviewDay6() As ColField(Of Boolean)
        Get
            Return _ReviewDay6
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReviewDay6 = value
        End Set
    End Property


    Public Property SOQFrequency() As ColField(Of Integer)
        Get
            SOQFrequency = _SoqFrequency
        End Get
        Set(ByVal value As ColField(Of Integer))
            _SoqFrequency = value
        End Set
    End Property
    Public Property DeliveryCheckMethod() As ColField(Of String)
        Get
            DeliveryCheckMethod = _DeliveryCheckMethod
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryCheckMethod = value
        End Set
    End Property
    Public Property DateOrderCloseStart() As ColField(Of Date)
        Get
            DateOrderCloseStart = _DateOrderCloseStart
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateOrderCloseStart = value
        End Set
    End Property
    Public Property DateOrderCloseEnd() As ColField(Of Date)
        Get
            DateOrderCloseEnd = _DateOrderCloseEnd
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateOrderCloseEnd = value
        End Set
    End Property
    Public Property DateDelCloseStart() As ColField(Of Date)
        Get
            DateDelCloseStart = _DateDelCloseStart
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateDelCloseStart = value
        End Set
    End Property
    Public Property DateDelCloseEnd() As ColField(Of Date)
        Get
            DateDelCloseEnd = _DateDelCloseEnd
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateDelCloseEnd = value
        End Set
    End Property
    Public Property ReturnsPolicyCode() As ColField(Of String)
        Get
            ReturnsPolicyCode = _ReturnsPolicyCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ReturnsPolicyCode = value
        End Set
    End Property
    Public Property ReturnsMessageRef() As ColField(Of Integer)
        Get
            ReturnsMessageRef = _ReturnsMessageRef
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReturnsMessageRef = value
        End Set
    End Property
    Public Property OrderMinType() As ColField(Of String)
        Get
            OrderMinType = _OrderMinType
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderMinType = value
        End Set
    End Property
    Public Property OrderMinValue() As ColField(Of Integer)
        Get
            OrderMinValue = _OrderMinValue
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderMinValue = value
        End Set
    End Property
    Public Property OrderMinUnits() As ColField(Of Integer)
        Get
            OrderMinUnits = _OrderMinUnits
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderMinUnits = value
        End Set
    End Property
    Public Property OrderMinWeight() As ColField(Of Integer)
        Get
            OrderMinWeight = _OrderMinWeight
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderMinWeight = value
        End Set
    End Property
    Public Property TruckCapWeight() As ColField(Of Integer)
        Get
            TruckCapWeight = _TruckCapWeight
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TruckCapWeight = value
        End Set
    End Property
    Public Property TruckCapVolume() As ColField(Of Integer)
        Get
            TruckCapVolume = _TruckCapVolume
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TruckCapVolume = value
        End Set
    End Property
    Public Property TruckCapPallets() As ColField(Of Integer)
        Get
            TruckCapPallets = _TruckCapPallets
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TruckCapPallets = value
        End Set
    End Property


#End Region

#Region "Methods"
    Private _Details As List(Of cSupplierDetail) = Nothing

    Public Property Details() As List(Of cSupplierDetail)
        Get
            Return _Details
        End Get
        Set(ByVal value As List(Of cSupplierDetail))
            _Details = value
        End Set
    End Property

    Public Function FlagAllAsDeleted() As Boolean
        Oasys3DB.ExecuteSql("UPDATE SUPDET SET DELC = '1' where delc = '0'")
    End Function

    Public Function GetDetailRecords(ByVal Supno As String) As List(Of cSupplierDetail)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, Supno)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotType, "S")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotNumber, "000")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, 0)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, Supno)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotType, "O")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotNumber, "000")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, 0)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pOr)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, Supno)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotType, "S")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DepotNumber, "999")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, 0)
        Return LoadMatches()
    End Function

#End Region

End Class
