﻿Imports System.Data.SqlClient

Partial Class Purchasing
    Shared _connectionString As String = String.Empty

    Public Property ConnectionString() As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property

    Public Sub New(ByVal connectionString As String)
        Me.New()
        _connectionString = connectionString
    End Sub



    Partial Class SupplierDataTable

        ''' <summary>
        ''' Loads suppliers into datatable
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub LoadSuppliers()

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.SupplierTableAdapter
                da.Connection = con
                da.Fill(Me)
            End Using

        End Sub

        ''' <summary>
        ''' Loads suppliers into datatable for given supplier numbers
        ''' </summary>
        ''' <param name="supplierNumbers"></param>
        ''' <remarks></remarks>
        Public Sub LoadSuppliers(ByVal supplierNumbers As ArrayList)

            If supplierNumbers Is Nothing OrElse supplierNumbers.Count = 0 Then
                LoadSuppliers()
            Else
                Using con As New SqlConnection(_connectionString)
                    Dim da As New PurchasingTableAdapters.SupplierTableAdapter
                    da.Connection = con
                    da.FillByNumber(Me, supplierNumbers.ToStringCsv)
                End Using
            End If

        End Sub

        ''' <summary>
        ''' Returns all supplier numbers in an arraylist
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetAllSupplierNumbers() As ArrayList

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.SupplierTableAdapter
                da.Connection = con
                da.Fill(Me)

                Dim al As New ArrayList
                For Each dr As SupplierRow In Me.Rows
                    al.Add(dr.Number)
                Next

                Return al
            End Using

        End Function

    End Class

    Partial Class SupplierRow

        Public Overrides Function ToString() As String
            Return Me.Number & Space(1) & Me.NAME
        End Function

    End Class




    Partial Class SupplierDetailDataTable

        ''' <summary>
        ''' Loads supplier details into datatable for given parameters
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub LoadDetails(ByVal supplierNumber As String, ByVal depotType As String, ByVal depotNumber As String)

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.SupplierDetailTableAdapter
                da.Connection = con
                da.FillByNumberDepot(Me, supplierNumber, depotType, depotNumber)
            End Using

        End Sub

    End Class

    Partial Class SupplierDetailRow

        Public Function DaysLeadFixed() As Decimal

            Return CDec(Me.LeadTimeFixed / 7)

        End Function

        Public Function DaysReviewFixed() As Decimal

            Dim reviewFixed As Decimal = 0
            If Me.ReviewDay0 Then reviewFixed += 1
            If Me.ReviewDay1 Then reviewFixed += 1
            If Me.ReviewDay2 Then reviewFixed += 1
            If Me.ReviewDay3 Then reviewFixed += 1
            If Me.ReviewDay4 Then reviewFixed += 1
            If Me.ReviewDay5 Then reviewFixed += 1
            If Me.ReviewDay6 Then reviewFixed += 1

            If reviewFixed = 0 Then reviewFixed = 1 Else reviewFixed = 1 / reviewFixed
            Return reviewFixed

        End Function

    End Class




    Partial Class StockDataTable

        ''' <summary>
        ''' Loads stocks into datatable
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub LoadStocks()

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.StockTableAdapter
                da.Connection = con
                da.Fill(Me)
            End Using

        End Sub

        ''' <summary>
        ''' Loads stocks into datatable for given supplier numbers
        ''' </summary>
        ''' <param name="supplierNumbers"></param>
        ''' <remarks></remarks>
        Public Sub LoadOrderStocks(ByVal supplierNumbers As ArrayList)

            If supplierNumbers Is Nothing OrElse supplierNumbers.Count = 0 Then
                LoadStocks()
            Else
                Using con As New SqlConnection(_connectionString)
                    Dim da As New PurchasingTableAdapters.StockTableAdapter
                    da.Connection = con
                    da.FillBySupplierNumber(Me, supplierNumbers.ToStringCsv)
                End Using
            End If

        End Sub

        ''' <summary>
        ''' Performs all updates
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PerformChanges()

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.StockTableAdapter
                da.Connection = con

                'only updates to do here
                Dim dt As StockDataTable = CType(Me.GetChanges(DataRowState.Modified), StockDataTable)
                If dt IsNot Nothing Then
                    da.Update(dt)
                End If

            End Using

        End Sub

    End Class

    Partial Class StockRow

        Public Overrides Function ToString() As String
            Return Me.Number & Space(1) & Me.Description.Trim
        End Function

        Public Property UnitSalesPeriod(ByVal period As Integer) As Integer
            Get
                Select Case period
                    Case 1 : Return Me.UnitSalesPeriod1
                    Case 2 : Return Me.UnitSalesPeriod2
                    Case 3 : Return Me.UnitSalesPeriod3
                    Case 4 : Return Me.UnitSalesPeriod4
                    Case 5 : Return Me.UnitSalesPeriod5
                    Case 6 : Return Me.UnitSalesPeriod6
                    Case 7 : Return Me.UnitSalesPeriod7
                    Case 8 : Return Me.UnitSalesPeriod8
                    Case 9 : Return Me.UnitSalesPeriod9
                    Case 10 : Return Me.UnitSalesPeriod10
                    Case 11 : Return Me.UnitSalesPeriod11
                    Case 12 : Return Me.UnitSalesPeriod12
                    Case 13 : Return Me.UnitSalesPeriod13
                    Case 14 : Return Me.UnitSalesPeriod14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Integer)
                Select Case period
                    Case 1 : Me.UnitSalesPeriod1 = value
                    Case 2 : Me.UnitSalesPeriod2 = value
                    Case 3 : Me.UnitSalesPeriod3 = value
                    Case 4 : Me.UnitSalesPeriod4 = value
                    Case 5 : Me.UnitSalesPeriod5 = value
                    Case 6 : Me.UnitSalesPeriod6 = value
                    Case 7 : Me.UnitSalesPeriod7 = value
                    Case 8 : Me.UnitSalesPeriod8 = value
                    Case 9 : Me.UnitSalesPeriod9 = value
                    Case 10 : Me.UnitSalesPeriod10 = value
                    Case 11 : Me.UnitSalesPeriod11 = value
                    Case 12 : Me.UnitSalesPeriod12 = value
                    Case 13 : Me.UnitSalesPeriod13 = value
                    Case 14 : Me.UnitSalesPeriod14 = value
                End Select
            End Set
        End Property

        Public Property DaysOutStockPeriod(ByVal period As Integer) As Short
            Get
                Select Case period
                    Case 1 : Return Me.DaysOutStockPeriod1
                    Case 2 : Return Me.DaysOutStockPeriod2
                    Case 3 : Return Me.DaysOutStockPeriod3
                    Case 4 : Return Me.DaysOutStockPeriod4
                    Case 5 : Return Me.DaysOutStockPeriod5
                    Case 6 : Return Me.DaysOutStockPeriod6
                    Case 7 : Return Me.DaysOutStockPeriod7
                    Case 8 : Return Me.DaysOutStockPeriod8
                    Case 9 : Return Me.DaysOutStockPeriod9
                    Case 10 : Return Me.DaysOutStockPeriod10
                    Case 11 : Return Me.DaysOutStockPeriod11
                    Case 12 : Return Me.DaysOutStockPeriod12
                    Case 13 : Return Me.DaysOutStockPeriod13
                    Case 14 : Return Me.DaysOutStockPeriod14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Short)
                Select Case period
                    Case 1 : Me.DaysOutStockPeriod1 = value
                    Case 2 : Me.DaysOutStockPeriod2 = value
                    Case 3 : Me.DaysOutStockPeriod3 = value
                    Case 4 : Me.DaysOutStockPeriod4 = value
                    Case 5 : Me.DaysOutStockPeriod5 = value
                    Case 6 : Me.DaysOutStockPeriod6 = value
                    Case 7 : Me.DaysOutStockPeriod7 = value
                    Case 8 : Me.DaysOutStockPeriod8 = value
                    Case 9 : Me.DaysOutStockPeriod9 = value
                    Case 10 : Me.DaysOutStockPeriod10 = value
                    Case 11 : Me.DaysOutStockPeriod11 = value
                    Case 12 : Me.DaysOutStockPeriod12 = value
                    Case 13 : Me.DaysOutStockPeriod13 = value
                    Case 14 : Me.DaysOutStockPeriod14 = value
                End Select
            End Set
        End Property

        Public Property FlierPeriod(ByVal period As Integer) As String
            Get
                Select Case period
                    Case 1 : Return Me.FlierPeriod1
                    Case 2 : Return Me.FlierPeriod2
                    Case 3 : Return Me.FlierPeriod3
                    Case 4 : Return Me.FlierPeriod4
                    Case 5 : Return Me.FlierPeriod5
                    Case 6 : Return Me.FlierPeriod6
                    Case 7 : Return Me.FlierPeriod7
                    Case 8 : Return Me.FlierPeriod8
                    Case 9 : Return Me.FlierPeriod9
                    Case 10 : Return Me.FlierPeriod10
                    Case 11 : Return Me.FlierPeriod11
                    Case 12 : Return Me.FlierPeriod12
                    Case Else : Return String.Empty
                End Select
            End Get
            Set(ByVal value As String)
                Select Case period
                    Case 1 : Me.FlierPeriod1 = value
                    Case 2 : Me.FlierPeriod2 = value
                    Case 3 : Me.FlierPeriod3 = value
                    Case 4 : Me.FlierPeriod4 = value
                    Case 5 : Me.FlierPeriod5 = value
                    Case 6 : Me.FlierPeriod6 = value
                    Case 7 : Me.FlierPeriod7 = value
                    Case 8 : Me.FlierPeriod8 = value
                    Case 9 : Me.FlierPeriod9 = value
                    Case 10 : Me.FlierPeriod10 = value
                    Case 11 : Me.FlierPeriod11 = value
                    Case 12 : Me.FlierPeriod12 = value
                End Select
            End Set
        End Property

    End Class




    Partial Class SaleWeightDataTable

        ''' <summary>
        ''' Loads sale weights into datatable
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub LoadSaleWeights()

            Using con As New SqlConnection(_connectionString)
                Dim da As New PurchasingTableAdapters.SaleWeightTableAdapter
                da.Connection = con
                da.Fill(Me)
            End Using

        End Sub

        ''' <summary>
        ''' Returns period sales weighting for given keys
        ''' </summary>
        ''' <param name="Bank"></param>
        ''' <param name="Promo"></param>
        ''' <param name="Season"></param>
        ''' <param name="week"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PeriodAdjuster(ByVal Bank As String, ByVal Promo As String, ByVal Season As String, ByVal week As Integer) As Decimal

            'retrieve rows
            Dim b As SaleWeightRow = Me.First(Function(f As SaleWeightRow) f.PATT = Bank)
            Dim p As SaleWeightRow = Me.First(Function(f As SaleWeightRow) f.PATT = Promo)
            Dim s As SaleWeightRow = Me.First(Function(f As SaleWeightRow) f.PATT = Season)

            Dim Wi As Double = b.Pattern(week) * p.Pattern(week) * s.Pattern(week)
            If Wi = 0 Then Wi = 1
            Return CDec(Wi)

        End Function

    End Class

    Partial Class SaleWeightRow

        Public ReadOnly Property Pattern(ByVal week As Integer) As Double
            Get
                For Each c As DataColumn In Me.Table.Columns
                    If c.ColumnName = "WPAR" & week Then
                        Return CDbl(Me.Item(c.ColumnName))
                    End If
                Next
            End Get
        End Property

    End Class

End Class