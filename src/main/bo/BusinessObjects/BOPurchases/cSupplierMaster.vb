﻿<Serializable()> Public Class cSupplierMaster
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SUPMAS"
        BOFields.Add(_Number)
        BOFields.Add(_Alpha)
        BOFields.Add(_Name)
        BOFields.Add(_Type)
        BOFields.Add(_DeletedByHO)
        BOFields.Add(_HelpLineNumber)
        BOFields.Add(_PrimaryMerchant)
        BOFields.Add(_VendorPrimaryCode)
        BOFields.Add(_OrderDepotNumber)
        BOFields.Add(_ReturnsDepotNumber)
        BOFields.Add(_BBCSiteNumber)
        BOFields.Add(_SoqNumber)
        BOFields.Add(_SoqOrdered)
        BOFields.Add(_SoqDate)
        BOFields.Add(_DateLastOrdered)
        BOFields.Add(_DateLastReceived)
        BOFields.Add(_PONumOutstanding)
        BOFields.Add(_POValueOutstanding)
        BOFields.Add(mQtyOrdPeriod)
        BOFields.Add(mQtyOrdLast)
        BOFields.Add(mQtyOrdPrior)
        BOFields.Add(mQtyOrdYTD)
        BOFields.Add(mQtyOrdPYR)
        BOFields.Add(mQtyRecPeriod)
        BOFields.Add(mQtyRecLast)
        BOFields.Add(mQtyRecPrior)
        BOFields.Add(mQtyRecYTD)
        BOFields.Add(mQtyRecPYR)
        BOFields.Add(mPOLinesRecPeriod)
        BOFields.Add(mPOLinesRecLast)
        BOFields.Add(mPOLinesRecPrior)
        BOFields.Add(mPOLinesRecYTD)
        BOFields.Add(mPOLinesRecPYR)
        BOFields.Add(mPOLinesNotRecPeriod)
        BOFields.Add(mPOLinesNotRecLast)
        BOFields.Add(mPOLinesNotRecPrior)
        BOFields.Add(mPOLinesNotRecYTD)
        BOFields.Add(mPOLinesNotRecPYR)
        BOFields.Add(mPOLinesOversPeriod)
        BOFields.Add(mPOLinesOversLast)
        BOFields.Add(mPOLinesOversPrior)
        BOFields.Add(mPOLinesOversYTD)
        BOFields.Add(mPOLinesOversPYR)
        BOFields.Add(mPOLinesUndersPeriod)
        BOFields.Add(mPOLinesUndersLast)
        BOFields.Add(mPOLinesUndersPrior)
        BOFields.Add(mPOLinesUndersYTD)
        BOFields.Add(mPOLinesUndersPYR)
        BOFields.Add(mPORecNumPeriod)
        BOFields.Add(mPORecNumLast)
        BOFields.Add(mPORecNumPrior)
        BOFields.Add(mPORecNumYTD)
        BOFields.Add(mPORecNumPYR)
        BOFields.Add(mPORecValuePeriod)
        BOFields.Add(mPORecValueLast)
        BOFields.Add(mPORecValuePrior)
        BOFields.Add(mPORecValueYTD)
        BOFields.Add(mPORecValuePYR)
        BOFields.Add(mDaysToPORecPeriod)
        BOFields.Add(mDaysToPORecLast)
        BOFields.Add(mDaysToPORecPrior)
        BOFields.Add(mDaysToPORecYTD)
        BOFields.Add(mDaysToPORecPYR)
        BOFields.Add(mDaysShortestPeriod)
        BOFields.Add(mDaysShortestLast)
        BOFields.Add(mDaysShortestPrior)
        BOFields.Add(mDaysShortestYTD)
        BOFields.Add(mDaysShortestPYR)
        BOFields.Add(mDaysLongestPeriod)
        BOFields.Add(mDaysLongestLast)
        BOFields.Add(mDaysLongestPrior)
        BOFields.Add(mDaysLongestYTD)
        BOFields.Add(mDaysLongestPYR)
        BOFields.Add(mPalletCheck)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSupplierMaster)

        LoadBORecords(count)

        Dim col As New List(Of cSupplierMaster)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cSupplierMaster))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSupplierMaster(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("SUPN") : _Number.Value = CStr(drRow(dcColumn))
                    Case ("ALPH") : _Alpha.Value = drRow(dcColumn).ToString.Trim
                    Case ("NAME") : _Name.Value = drRow(dcColumn).ToString.Trim
                    Case ("TYPE") : _Type.Value = CStr(drRow(dcColumn))
                    Case ("DELC") : _DeletedByHO.Value = CBool(drRow(dcColumn))
                    Case ("HLIN") : _HelpLineNumber.Value = CStr(drRow(dcColumn))
                    Case ("MERC") : _PrimaryMerchant.Value = CStr(drRow(dcColumn))
                    Case ("VPCC") : _VendorPrimaryCode.Value = CStr(drRow(dcColumn))
                    Case ("ODNO") : _OrderDepotNumber.Value = CStr(drRow(dcColumn))
                    Case ("RDNO") : _ReturnsDepotNumber.Value = CStr(drRow(dcColumn))
                    Case ("BBCN") : _BBCSiteNumber.Value = CStr(drRow(dcColumn))
                    Case ("QCTL") : _SoqNumber.Value = CStr(drRow(dcColumn))
                    Case ("QFLG") : _SoqOrdered.Value = CBool(drRow(dcColumn))
                    Case ("SOQDate") : _SoqDate.Value = CDate(drRow(dcColumn))
                    Case ("DLPO") : _DateLastOrdered.Value = CDate(drRow(dcColumn))
                    Case ("DLRE") : _DateLastReceived.Value = CDate(drRow(dcColumn))
                    Case ("OPON") : _PONumOutstanding.Value = CDec(drRow(dcColumn))
                    Case ("OPOV") : _POValueOutstanding.Value = CDec(drRow(dcColumn))
                    Case ("QTYO1") : mQtyOrdPeriod.Value = CDec(drRow(dcColumn))
                    Case ("QTYO2") : mQtyOrdLast.Value = CDec(drRow(dcColumn))
                    Case ("QTYO3") : mQtyOrdPrior.Value = CDec(drRow(dcColumn))
                    Case ("QTYO4") : mQtyOrdYTD.Value = CDec(drRow(dcColumn))
                    Case ("QTYO5") : mQtyOrdPYR.Value = CDec(drRow(dcColumn))
                    Case ("QTYR1") : mQtyRecPeriod.Value = CDec(drRow(dcColumn))
                    Case ("QTYR2") : mQtyRecLast.Value = CDec(drRow(dcColumn))
                    Case ("QTYR3") : mQtyRecPrior.Value = CDec(drRow(dcColumn))
                    Case ("QTYR4") : mQtyRecYTD.Value = CDec(drRow(dcColumn))
                    Case ("QTYR5") : mQtyRecPYR.Value = CDec(drRow(dcColumn))
                    Case ("LREC1") : mPOLinesRecPeriod.Value = CDec(drRow(dcColumn))
                    Case ("LREC2") : mPOLinesRecLast.Value = CDec(drRow(dcColumn))
                    Case ("LREC3") : mPOLinesRecPrior.Value = CDec(drRow(dcColumn))
                    Case ("LREC4") : mPOLinesRecYTD.Value = CDec(drRow(dcColumn))
                    Case ("LREC5") : mPOLinesRecPYR.Value = CDec(drRow(dcColumn))
                    Case ("LNRE1") : mPOLinesNotRecPeriod.Value = CDec(drRow(dcColumn))
                    Case ("LNRE2") : mPOLinesNotRecLast.Value = CDec(drRow(dcColumn))
                    Case ("LNRE3") : mPOLinesNotRecPrior.Value = CDec(drRow(dcColumn))
                    Case ("LNRE4") : mPOLinesNotRecYTD.Value = CDec(drRow(dcColumn))
                    Case ("LNRE5") : mPOLinesNotRecPYR.Value = CDec(drRow(dcColumn))
                    Case ("LOVR1") : mPOLinesOversPeriod.Value = CDec(drRow(dcColumn))
                    Case ("LOVR2") : mPOLinesOversLast.Value = CDec(drRow(dcColumn))
                    Case ("LOVR3") : mPOLinesOversPrior.Value = CDec(drRow(dcColumn))
                    Case ("LOVR4") : mPOLinesOversYTD.Value = CDec(drRow(dcColumn))
                    Case ("LOVR5") : mPOLinesOversPYR.Value = CDec(drRow(dcColumn))
                    Case ("LUND1") : mPOLinesUndersPeriod.Value = CDec(drRow(dcColumn))
                    Case ("LUND2") : mPOLinesUndersLast.Value = CDec(drRow(dcColumn))
                    Case ("LUND3") : mPOLinesUndersPrior.Value = CDec(drRow(dcColumn))
                    Case ("LUND4") : mPOLinesUndersYTD.Value = CDec(drRow(dcColumn))
                    Case ("LUND5") : mPOLinesUndersPYR.Value = CDec(drRow(dcColumn))
                    Case ("PREC1") : mPORecNumPeriod.Value = CDec(drRow(dcColumn))
                    Case ("PREC2") : mPORecNumLast.Value = CDec(drRow(dcColumn))
                    Case ("PREC3") : mPORecNumPrior.Value = CDec(drRow(dcColumn))
                    Case ("PREC4") : mPORecNumYTD.Value = CDec(drRow(dcColumn))
                    Case ("PREC5") : mPORecNumPYR.Value = CDec(drRow(dcColumn))
                    Case ("VREC1") : mPORecValuePeriod.Value = CDec(drRow(dcColumn))
                    Case ("VREC2") : mPORecValueLast.Value = CDec(drRow(dcColumn))
                    Case ("VREC3") : mPORecValuePrior.Value = CDec(drRow(dcColumn))
                    Case ("VREC4") : mPORecValueYTD.Value = CDec(drRow(dcColumn))
                    Case ("VREC5") : mPORecValuePYR.Value = CDec(drRow(dcColumn))
                    Case ("DYTR1") : mDaysToPORecPeriod.Value = CDec(drRow(dcColumn))
                    Case ("DYTR2") : mDaysToPORecLast.Value = CDec(drRow(dcColumn))
                    Case ("DYTR3") : mDaysToPORecPrior.Value = CDec(drRow(dcColumn))
                    Case ("DYTR4") : mDaysToPORecYTD.Value = CDec(drRow(dcColumn))
                    Case ("DYTR5") : mDaysToPORecPYR.Value = CDec(drRow(dcColumn))
                    Case ("DYSH1") : mDaysShortestPeriod.Value = CDec(drRow(dcColumn))
                    Case ("DYSH2") : mDaysShortestLast.Value = CDec(drRow(dcColumn))
                    Case ("DYSH3") : mDaysShortestPrior.Value = CDec(drRow(dcColumn))
                    Case ("DYSH4") : mDaysShortestYTD.Value = CDec(drRow(dcColumn))
                    Case ("DYSH5") : mDaysShortestPYR.Value = CDec(drRow(dcColumn))
                    Case ("DYLO1") : mDaysLongestPeriod.Value = CDec(drRow(dcColumn))
                    Case ("DYLO2") : mDaysLongestLast.Value = CDec(drRow(dcColumn))
                    Case ("DYLO3") : mDaysLongestPrior.Value = CDec(drRow(dcColumn))
                    Case ("DYLO4") : mDaysLongestYTD.Value = CDec(drRow(dcColumn))
                    Case ("DYLO5") : mDaysLongestPYR.Value = CDec(drRow(dcColumn))
                    Case ("PalletCheck") : mPalletCheck.Value = CBool(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("SUPN", "0", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Number")
    Private _Alpha As New ColField(Of String)("ALPH", "", False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Alpha")
    Private _Name As New ColField(Of String)("NAME", "", False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Name")
    Private _Type As New ColField(Of String)("TYPE", "", False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Type")
    Private _DeletedByHO As New ColField(Of Boolean)("DELC", False, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Deleted By HO")
    Private _HelpLineNumber As New ColField(Of String)("HLIN", "", False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Help Line Number")
    Private _PrimaryMerchant As New ColField(Of String)("MERC", "", False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Primary Merchant")
    Private _VendorPrimaryCode As New ColField(Of String)("VPCC", "00", False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Vendor Primary Code")
    Private _OrderDepotNumber As New ColField(Of String)("ODNO", "", False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Order Depot Number")
    Private _ReturnsDepotNumber As New ColField(Of String)("RDNO", "", False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Returns Depot Number")
    Private _BBCSiteNumber As New ColField(Of String)("BBCN", "", False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "BBC Site Number")

    Private _SoqNumber As New ColField(Of String)("QCTL", "000000", False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "SOQ Control Number")
    Private _SoqOrdered As New ColField(Of Boolean)("QFLG", False, False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "SOQ Ready To Process")
    Private _SoqDate As New ColField(Of Date)("SOQDate", Nothing, False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "SOQ Date")

    Private _DateLastOrdered As New ColField(Of Date)("DLPO", Nothing, False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Date Last Ordered")
    Private _DateLastReceived As New ColField(Of Date)("DLRE", Nothing, False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Date Last Received")
    Private _PONumOutstanding As New ColField(Of Decimal)("OPON", 0, False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "PO Number Outstanding")
    Private _POValueOutstanding As New ColField(Of Decimal)("OPOV", 0, False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "PO Value Outstanding")

    Private mQtyOrdPeriod As New ColField(Of Decimal)("QTYO1", 0, False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "Qty Ord Period")
    Private mQtyOrdLast As New ColField(Of Decimal)("QTYO2", 0, False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "Qty Ord Last")
    Private mQtyOrdPrior As New ColField(Of Decimal)("QTYO3", 0, False, False, 1, 0, True, True, 21, 0, 0, 0, "", 0, "Qty Ord Prior")
    Private mQtyOrdYTD As New ColField(Of Decimal)("QTYO4", 0, False, False, 1, 0, True, True, 22, 0, 0, 0, "", 0, "Qty Ord YTD")
    Private mQtyOrdPYR As New ColField(Of Decimal)("QTYO5", 0, False, False, 1, 0, True, True, 23, 0, 0, 0, "", 0, "Qty Ord PYR")
    Private mQtyRecPeriod As New ColField(Of Decimal)("QTYR1", 0, False, False, 1, 0, True, True, 24, 0, 0, 0, "", 0, "QTY Rec Period")
    Private mQtyRecLast As New ColField(Of Decimal)("QTYR2", 0, False, False, 1, 0, True, True, 25, 0, 0, 0, "", 0, "QTY Rec Last")
    Private mQtyRecPrior As New ColField(Of Decimal)("QTYR3", 0, False, False, 1, 0, True, True, 26, 0, 0, 0, "", 0, "QTY Rec Prior")
    Private mQtyRecYTD As New ColField(Of Decimal)("QTYR4", 0, False, False, 1, 0, True, True, 27, 0, 0, 0, "", 0, "QTY Rec YTD")
    Private mQtyRecPYR As New ColField(Of Decimal)("QTYR5", 0, False, False, 1, 0, True, True, 28, 0, 0, 0, "", 0, "QTY Rec PYR")
    Private mPOLinesRecPeriod As New ColField(Of Decimal)("LREC1", 0, False, False, 1, 0, True, True, 29, 0, 0, 0, "", 0, "PO Lines Rec Period")
    Private mPOLinesRecLast As New ColField(Of Decimal)("LREC2", 0, False, False, 1, 0, True, True, 30, 0, 0, 0, "", 0, "PO Lines Rec Last")
    Private mPOLinesRecPrior As New ColField(Of Decimal)("LREC3", 0, False, False, 1, 0, True, True, 31, 0, 0, 0, "", 0, "PO Lines Rec Prior")
    Private mPOLinesRecYTD As New ColField(Of Decimal)("LREC4", 0, False, False, 1, 0, True, True, 32, 0, 0, 0, "", 0, "PO Lines Rec YTD")
    Private mPOLinesRecPYR As New ColField(Of Decimal)("LREC5", 0, False, False, 1, 0, True, True, 33, 0, 0, 0, "", 0, "PO Lines Rec PYR")
    Private mPOLinesNotRecPeriod As New ColField(Of Decimal)("LNRE1", 0, False, False, 1, 0, True, True, 34, 0, 0, 0, "", 0, "PO Lines Not Rec Period")
    Private mPOLinesNotRecLast As New ColField(Of Decimal)("LNRE2", 0, False, False, 1, 0, True, True, 35, 0, 0, 0, "", 0, "PO Lines Not Rec Last")
    Private mPOLinesNotRecPrior As New ColField(Of Decimal)("LNRE3", 0, False, False, 1, 0, True, True, 36, 0, 0, 0, "", 0, "PO Lines Not Rec Prior")
    Private mPOLinesNotRecYTD As New ColField(Of Decimal)("LNRE4", 0, False, False, 1, 0, True, True, 37, 0, 0, 0, "", 0, "PO Lines Not Rec YTD")
    Private mPOLinesNotRecPYR As New ColField(Of Decimal)("LNRE5", 0, False, False, 1, 0, True, True, 38, 0, 0, 0, "", 0, "PO Lines Not Rec PYR")
    Private mPOLinesOversPeriod As New ColField(Of Decimal)("LOVR1", 0, False, False, 1, 0, True, True, 39, 0, 0, 0, "", 0, "PO Lines Overs Period")
    Private mPOLinesOversLast As New ColField(Of Decimal)("LOVR2", 0, False, False, 1, 0, True, True, 40, 0, 0, 0, "", 0, "PO Lines Overs Last")
    Private mPOLinesOversPrior As New ColField(Of Decimal)("LOVR3", 0, False, False, 1, 0, True, True, 41, 0, 0, 0, "", 0, "PO Lines Overs Prior")
    Private mPOLinesOversYTD As New ColField(Of Decimal)("LOVR4", 0, False, False, 1, 0, True, True, 42, 0, 0, 0, "", 0, "PO Lines Overs YTD")
    Private mPOLinesOversPYR As New ColField(Of Decimal)("LOVR5", 0, False, False, 1, 0, True, True, 43, 0, 0, 0, "", 0, "PO Lines Overs PYR")
    Private mPOLinesUndersPeriod As New ColField(Of Decimal)("LUND1", 0, False, False, 1, 0, True, True, 44, 0, 0, 0, "", 0, "PO Lines Unders Period")
    Private mPOLinesUndersLast As New ColField(Of Decimal)("LUND2", 0, False, False, 1, 0, True, True, 45, 0, 0, 0, "", 0, "PO Lines Unders Last")
    Private mPOLinesUndersPrior As New ColField(Of Decimal)("LUND3", 0, False, False, 1, 0, True, True, 46, 0, 0, 0, "", 0, "PO Lines Unders Prior")
    Private mPOLinesUndersYTD As New ColField(Of Decimal)("LUND4", 0, False, False, 1, 0, True, True, 47, 0, 0, 0, "", 0, "PO Lines Unders YTD")
    Private mPOLinesUndersPYR As New ColField(Of Decimal)("LUND5", 0, False, False, 1, 0, True, True, 48, 0, 0, 0, "", 0, "PO Lines Unders PYR")
    Private mPORecNumPeriod As New ColField(Of Decimal)("PREC1", 0, False, False, 1, 0, True, True, 49, 0, 0, 0, "", 0, "Po Rec Num Period")
    Private mPORecNumLast As New ColField(Of Decimal)("PREC2", 0, False, False, 1, 0, True, True, 50, 0, 0, 0, "", 0, "PO Rec Num Last")
    Private mPORecNumPrior As New ColField(Of Decimal)("PREC3", 0, False, False, 1, 0, True, True, 51, 0, 0, 0, "", 0, "PO Rec Num Prior")
    Private mPORecNumYTD As New ColField(Of Decimal)("PREC4", 0, False, False, 1, 0, True, True, 52, 0, 0, 0, "", 0, "PO Rec Num YTD")
    Private mPORecNumPYR As New ColField(Of Decimal)("PREC5", 0, False, False, 1, 0, True, True, 53, 0, 0, 0, "", 0, "PO Rec Num PYR")
    Private mPORecValuePeriod As New ColField(Of Decimal)("VREC1", 0, False, False, 1, 0, True, True, 54, 0, 0, 0, "", 0, "PO Rec Value Period")
    Private mPORecValueLast As New ColField(Of Decimal)("VREC2", 0, False, False, 1, 0, True, True, 55, 0, 0, 0, "", 0, "PO Rec Value Last")
    Private mPORecValuePrior As New ColField(Of Decimal)("VREC3", 0, False, False, 1, 0, True, True, 56, 0, 0, 0, "", 0, "PO Rec Value Prior")
    Private mPORecValueYTD As New ColField(Of Decimal)("VREC4", 0, False, False, 1, 0, True, True, 57, 0, 0, 0, "", 0, "PO Rec Value YTD")
    Private mPORecValuePYR As New ColField(Of Decimal)("VREC5", 0, False, False, 1, 0, True, True, 58, 0, 0, 0, "", 0, "PO Rec Value PYR")
    Private mDaysToPORecPeriod As New ColField(Of Decimal)("DYTR1", 0, False, False, 1, 0, True, True, 59, 0, 0, 0, "", 0, "Days To PO Rec Period")
    Private mDaysToPORecLast As New ColField(Of Decimal)("DYTR2", 0, False, False, 1, 0, True, True, 60, 0, 0, 0, "", 0, "Days To PO Rec Last")
    Private mDaysToPORecPrior As New ColField(Of Decimal)("DYTR3", 0, False, False, 1, 0, True, True, 61, 0, 0, 0, "", 0, "Days To PO Rec Prior")
    Private mDaysToPORecYTD As New ColField(Of Decimal)("DYTR4", 0, False, False, 1, 0, True, True, 62, 0, 0, 0, "", 0, "Days To PO Rec YTD")
    Private mDaysToPORecPYR As New ColField(Of Decimal)("DYTR5", 0, False, False, 1, 0, True, True, 63, 0, 0, 0, "", 0, "Days To PO Rec PYR")
    Private mDaysShortestPeriod As New ColField(Of Decimal)("DYSH1", 0, False, False, 1, 0, True, True, 64, 0, 0, 0, "", 0, "Days Shortest Period")
    Private mDaysShortestLast As New ColField(Of Decimal)("DYSH2", 0, False, False, 1, 0, True, True, 65, 0, 0, 0, "", 0, "Days Shortest Last")
    Private mDaysShortestPrior As New ColField(Of Decimal)("DYSH3", 0, False, False, 1, 0, True, True, 66, 0, 0, 0, "", 0, "Days Shortest Prior")
    Private mDaysShortestYTD As New ColField(Of Decimal)("DYSH4", 0, False, False, 1, 0, True, True, 67, 0, 0, 0, "", 0, "Days Shortest YTD")
    Private mDaysShortestPYR As New ColField(Of Decimal)("DYSH5", 0, False, False, 1, 0, True, True, 68, 0, 0, 0, "", 0, "Days Shortest PYR")
    Private mDaysLongestPeriod As New ColField(Of Decimal)("DYLO1", 0, False, False, 1, 0, True, True, 69, 0, 0, 0, "", 0, "Days Longest Period")
    Private mDaysLongestLast As New ColField(Of Decimal)("DYLO2", 0, False, False, 1, 0, True, True, 70, 0, 0, 0, "", 0, "Days Longest Last")
    Private mDaysLongestPrior As New ColField(Of Decimal)("DYLO3", 0, False, False, 1, 0, True, True, 71, 0, 0, 0, "", 0, "Days Longest Prior")
    Private mDaysLongestYTD As New ColField(Of Decimal)("DYLO4", 0, False, False, 1, 0, True, True, 72, 0, 0, 0, "", 0, "Days Longest YTD")
    Private mDaysLongestPYR As New ColField(Of Decimal)("DYLO5", 0, False, False, 1, 0, True, True, 73, 0, 0, 0, "", 0, "Days Longest PYR")
    Private mPalletCheck As New ColField(Of Boolean)("PalletCheck", False, False, False, 1, 0, True, True, 74, 0, 0, 0, "", 0, "Pallet Check")
#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Alpha() As ColField(Of String)
        Get
            Alpha = _Alpha
        End Get
        Set(ByVal value As ColField(Of String))
            _Alpha = value
        End Set
    End Property
    Public Property Name() As ColField(Of String)
        Get
            Name = _Name
        End Get
        Set(ByVal value As ColField(Of String))
            _Name = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property DeletedByHO() As ColField(Of Boolean)
        Get
            DeletedByHO = _DeletedByHO
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DeletedByHO = value
        End Set
    End Property
    Public Property HelpLineNumber() As ColField(Of String)
        Get
            HelpLineNumber = _HelpLineNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _HelpLineNumber = value
        End Set
    End Property
    Public Property PrimaryMerchant() As ColField(Of String)
        Get
            PrimaryMerchant = _PrimaryMerchant
        End Get
        Set(ByVal value As ColField(Of String))
            _PrimaryMerchant = value
        End Set
    End Property
    Public Property VendorPrimaryCode() As ColField(Of String)
        Get
            VendorPrimaryCode = _VendorPrimaryCode
        End Get
        Set(ByVal value As ColField(Of String))
            _VendorPrimaryCode = value
        End Set
    End Property
    Public Property OrderDepotNumber() As ColField(Of String)
        Get
            OrderDepotNumber = _OrderDepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderDepotNumber = value
        End Set
    End Property
    Public Property ReturnsDepotNumber() As ColField(Of String)
        Get
            ReturnsDepotNumber = _ReturnsDepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ReturnsDepotNumber = value
        End Set
    End Property
    Public Property BBCSiteNumber() As ColField(Of String)
        Get
            BBCSiteNumber = _BBCSiteNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _BBCSiteNumber = value
        End Set
    End Property

    Public Property SoqNumber() As ColField(Of String)
        Get
            SoqNumber = _SoqNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SoqNumber = value
        End Set
    End Property
    Public Property SoqOrdered() As ColField(Of Boolean)
        Get
            SoqOrdered = _SoqOrdered
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SoqOrdered = value
        End Set
    End Property
    Public Property SoqDate() As ColField(Of Date)
        Get
            SoqDate = _SoqDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _SoqDate = value
        End Set
    End Property

    Public Property DateLastOrdered() As ColField(Of Date)
        Get
            DateLastOrdered = _DateLastOrdered
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastOrdered = value
        End Set
    End Property
    Public Property DateLastReceived() As ColField(Of Date)
        Get
            DateLastReceived = _DateLastReceived
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastReceived = value
        End Set
    End Property
    Public Property PONumOutstanding() As ColField(Of Decimal)
        Get
            PONumOutstanding = _PONumOutstanding
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PONumOutstanding = value
        End Set
    End Property
    Public Property POValueOutstanding() As ColField(Of Decimal)
        Get
            POValueOutstanding = _POValueOutstanding
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _POValueOutstanding = value
        End Set
    End Property
    Public Property QtyOrdPeriod() As ColField(Of Decimal)
        Get
            QtyOrdPeriod = mQtyOrdPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyOrdPeriod = value
        End Set
    End Property
    Public Property QtyOrdLast() As ColField(Of Decimal)
        Get
            QtyOrdLast = mQtyOrdLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyOrdLast = value
        End Set
    End Property
    Public Property QtyOrdPrior() As ColField(Of Decimal)
        Get
            QtyOrdPrior = mQtyOrdPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyOrdPrior = value
        End Set
    End Property
    Public Property QtyOrdYTD() As ColField(Of Decimal)
        Get
            QtyOrdYTD = mQtyOrdYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyOrdYTD = value
        End Set
    End Property
    Public Property QtyOrdPYR() As ColField(Of Decimal)
        Get
            QtyOrdPYR = mQtyOrdPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyOrdPYR = value
        End Set
    End Property
    Public Property QtyRecPeriod() As ColField(Of Decimal)
        Get
            QtyRecPeriod = mQtyRecPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyRecPeriod = value
        End Set
    End Property
    Public Property QtyRecLast() As ColField(Of Decimal)
        Get
            QtyRecLast = mQtyRecLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyRecLast = value
        End Set
    End Property
    Public Property QtyRecPrior() As ColField(Of Decimal)
        Get
            QtyRecPrior = mQtyRecPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyRecPrior = value
        End Set
    End Property
    Public Property QtyRecYTD() As ColField(Of Decimal)
        Get
            QtyRecYTD = mQtyRecYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyRecYTD = value
        End Set
    End Property
    Public Property QtyRecPYR() As ColField(Of Decimal)
        Get
            QtyRecPYR = mQtyRecPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mQtyRecPYR = value
        End Set
    End Property
    Public Property POLinesRecPeriod() As ColField(Of Decimal)
        Get
            POLinesRecPeriod = mPOLinesRecPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesRecPeriod = value
        End Set
    End Property
    Public Property POLinesRecLast() As ColField(Of Decimal)
        Get
            POLinesRecLast = mPOLinesRecLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesRecLast = value
        End Set
    End Property
    Public Property POLinesRecPrior() As ColField(Of Decimal)
        Get
            POLinesRecPrior = mPOLinesRecPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesRecPrior = value
        End Set
    End Property
    Public Property POLinesRecYTD() As ColField(Of Decimal)
        Get
            POLinesRecYTD = mPOLinesRecYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesRecYTD = value
        End Set
    End Property
    Public Property POLinesRecPYR() As ColField(Of Decimal)
        Get
            POLinesRecPYR = mPOLinesRecPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesRecPYR = value
        End Set
    End Property
    Public Property POLinesNotRecPeriod() As ColField(Of Decimal)
        Get
            POLinesNotRecPeriod = mPOLinesNotRecPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesNotRecPeriod = value
        End Set
    End Property
    Public Property POLinesNotRecLast() As ColField(Of Decimal)
        Get
            POLinesNotRecLast = mPOLinesNotRecLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesNotRecLast = value
        End Set
    End Property
    Public Property POLinesNotRecPrior() As ColField(Of Decimal)
        Get
            POLinesNotRecPrior = mPOLinesNotRecPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesNotRecPrior = value
        End Set
    End Property
    Public Property POLinesNotRecYTD() As ColField(Of Decimal)
        Get
            POLinesNotRecYTD = mPOLinesNotRecYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesNotRecYTD = value
        End Set
    End Property
    Public Property POLinesNotRecPYR() As ColField(Of Decimal)
        Get
            POLinesNotRecPYR = mPOLinesNotRecPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesNotRecPYR = value
        End Set
    End Property
    Public Property POLinesOversPeriod() As ColField(Of Decimal)
        Get
            POLinesOversPeriod = mPOLinesOversPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesOversPeriod = value
        End Set
    End Property
    Public Property POLinesOversLast() As ColField(Of Decimal)
        Get
            POLinesOversLast = mPOLinesOversLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesOversLast = value
        End Set
    End Property
    Public Property POLinesOversPrior() As ColField(Of Decimal)
        Get
            POLinesOversPrior = mPOLinesOversPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesOversPrior = value
        End Set
    End Property
    Public Property POLinesOversYTD() As ColField(Of Decimal)
        Get
            POLinesOversYTD = mPOLinesOversYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesOversYTD = value
        End Set
    End Property
    Public Property POLinesOversPYR() As ColField(Of Decimal)
        Get
            POLinesOversPYR = mPOLinesOversPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesOversPYR = value
        End Set
    End Property
    Public Property POLinesUndersPeriod() As ColField(Of Decimal)
        Get
            POLinesUndersPeriod = mPOLinesUndersPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesUndersPeriod = value
        End Set
    End Property
    Public Property POLinesUndersLast() As ColField(Of Decimal)
        Get
            POLinesUndersLast = mPOLinesUndersLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesUndersLast = value
        End Set
    End Property
    Public Property POLinesUndersPrior() As ColField(Of Decimal)
        Get
            POLinesUndersPrior = mPOLinesUndersPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesUndersPrior = value
        End Set
    End Property
    Public Property POLinesUndersYTD() As ColField(Of Decimal)
        Get
            POLinesUndersYTD = mPOLinesUndersYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesUndersYTD = value
        End Set
    End Property
    Public Property POLinesUndersPYR() As ColField(Of Decimal)
        Get
            POLinesUndersPYR = mPOLinesUndersPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPOLinesUndersPYR = value
        End Set
    End Property
    Public Property PORecNumPeriod() As ColField(Of Decimal)
        Get
            PORecNumPeriod = mPORecNumPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecNumPeriod = value
        End Set
    End Property
    Public Property PORecNumLast() As ColField(Of Decimal)
        Get
            PORecNumLast = mPORecNumLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecNumLast = value
        End Set
    End Property
    Public Property PORecNumPrior() As ColField(Of Decimal)
        Get
            PORecNumPrior = mPORecNumPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecNumPrior = value
        End Set
    End Property
    Public Property PORecNumYTD() As ColField(Of Decimal)
        Get
            PORecNumYTD = mPORecNumYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecNumYTD = value
        End Set
    End Property
    Public Property PORecNumPYR() As ColField(Of Decimal)
        Get
            PORecNumPYR = mPORecNumPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecNumPYR = value
        End Set
    End Property
    Public Property PORecValuePeriod() As ColField(Of Decimal)
        Get
            PORecValuePeriod = mPORecValuePeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecValuePeriod = value
        End Set
    End Property
    Public Property PORecValueLast() As ColField(Of Decimal)
        Get
            PORecValueLast = mPORecValueLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecValueLast = value
        End Set
    End Property
    Public Property PORecValuePrior() As ColField(Of Decimal)
        Get
            PORecValuePrior = mPORecValuePrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecValuePrior = value
        End Set
    End Property
    Public Property PORecValueYTD() As ColField(Of Decimal)
        Get
            PORecValueYTD = mPORecValueYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecValueYTD = value
        End Set
    End Property
    Public Property PORecValuePYR() As ColField(Of Decimal)
        Get
            PORecValuePYR = mPORecValuePYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mPORecValuePYR = value
        End Set
    End Property
    Public Property DaysToPORecPeriod() As ColField(Of Decimal)
        Get
            DaysToPORecPeriod = mDaysToPORecPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysToPORecPeriod = value
        End Set
    End Property
    Public Property DaysToPORecLast() As ColField(Of Decimal)
        Get
            DaysToPORecLast = mDaysToPORecLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysToPORecLast = value
        End Set
    End Property
    Public Property DaysToPORecPrior() As ColField(Of Decimal)
        Get
            DaysToPORecPrior = mDaysToPORecPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysToPORecPrior = value
        End Set
    End Property
    Public Property DaysToPORecYTD() As ColField(Of Decimal)
        Get
            DaysToPORecYTD = mDaysToPORecYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysToPORecYTD = value
        End Set
    End Property
    Public Property DaysToPORecPYR() As ColField(Of Decimal)
        Get
            DaysToPORecPYR = mDaysToPORecPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysToPORecPYR = value
        End Set
    End Property
    Public Property DaysShortestPeriod() As ColField(Of Decimal)
        Get
            DaysShortestPeriod = mDaysShortestPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysShortestPeriod = value
        End Set
    End Property
    Public Property DaysShortestLast() As ColField(Of Decimal)
        Get
            DaysShortestLast = mDaysShortestLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysShortestLast = value
        End Set
    End Property
    Public Property DaysShortestPrior() As ColField(Of Decimal)
        Get
            DaysShortestPrior = mDaysShortestPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysShortestPrior = value
        End Set
    End Property
    Public Property DaysShortestYTD() As ColField(Of Decimal)
        Get
            DaysShortestYTD = mDaysShortestYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysShortestYTD = value
        End Set
    End Property
    Public Property DaysShortestPYR() As ColField(Of Decimal)
        Get
            DaysShortestPYR = mDaysShortestPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysShortestPYR = value
        End Set
    End Property
    Public Property DaysLongestPeriod() As ColField(Of Decimal)
        Get
            DaysLongestPeriod = mDaysLongestPeriod
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysLongestPeriod = value
        End Set
    End Property
    Public Property DaysLongestLast() As ColField(Of Decimal)
        Get
            DaysLongestLast = mDaysLongestLast
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysLongestLast = value
        End Set
    End Property
    Public Property DaysLongestPrior() As ColField(Of Decimal)
        Get
            DaysLongestPrior = mDaysLongestPrior
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysLongestPrior = value
        End Set
    End Property
    Public Property DaysLongestYTD() As ColField(Of Decimal)
        Get
            DaysLongestYTD = mDaysLongestYTD
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysLongestYTD = value
        End Set
    End Property
    Public Property DaysLongestPYR() As ColField(Of Decimal)
        Get
            DaysLongestPYR = mDaysLongestPYR
        End Get
        Set(ByVal value As ColField(Of Decimal))
            mDaysLongestPYR = value
        End Set
    End Property
    Public Property PalletCheck() As ColField(Of Boolean)
        Get
            Return mPalletCheck
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mPalletCheck = value
        End Set
    End Property

#End Region

#Region "Enumerations"
    Public Enum SoqStates
        None
        Expired
        No
        Yes
        Ordered
    End Enum
    Public Enum SupplierTypes
        All
        Direct
        Warehouse
    End Enum

    Public Function SupplierType() As SupplierTypes

        If OrderDetails.BBC.Value.Trim.Length = 0 Then
            Return SupplierTypes.Direct
        Else
            Return SupplierTypes.Warehouse
        End If

    End Function
    Public Function SoqState() As SoqStates

        If _SoqOrdered.Value Then Return SoqStates.Ordered
        If _SoqNumber.Value = "000000" Then
            If _SoqDate.Value.Date = Now.Date Then Return SoqStates.No
            Return SoqStates.None
        Else
            If _SoqDate.Value.Date = Now.Date Then Return SoqStates.Yes
            Return SoqStates.Expired
        End If

    End Function

#End Region

#Region "Entities"
    Private _Suppliers As List(Of cSupplierMaster) = Nothing
    Private _SoqPattern As BOStock.cSOQPattern = Nothing
    Private _OrderDetails As cSupplierDetail = Nothing
    Private _OrderStocks As List(Of BOStock.cStock) = Nothing
    Private _ReturnDetails As cSupplierDetail = Nothing
    Private _ReturnStocks As List(Of BOStock.cStock) = Nothing
    Private _ReturnNotes As List(Of cSupplierNote) = Nothing

    Public Property Supplier(ByVal SupplierNumber As String) As cSupplierMaster
        Get
            For Each s As cSupplierMaster In _Suppliers
                If s.Number.Value = SupplierNumber Then Return s
            Next
            Return Nothing
        End Get
        Set(ByVal value As cSupplierMaster)
            For Each s As cSupplierMaster In _Suppliers
                If s.Number.Value = SupplierNumber Then
                    s = value
                End If
            Next
        End Set
    End Property
    Public Property Suppliers() As List(Of cSupplierMaster)
        Get
            Return _Suppliers
        End Get
        Set(ByVal value As List(Of cSupplierMaster))
            _Suppliers = value
        End Set
    End Property
    Public Property SoqPattern() As BOStock.cSOQPattern
        Get
            If _SoqPattern Is Nothing Then
                _SoqPattern = New BOStock.cSOQPattern(Oasys3DB)
                _SoqPattern.Patterns = _SoqPattern.LoadMatches
            End If
            Return _SoqPattern
        End Get
        Set(ByVal value As BOStock.cSOQPattern)
            _SoqPattern = value
        End Set
    End Property

    Public Property OrderDetails() As cSupplierDetail
        Get
            If _OrderDetails Is Nothing Then 'load return details
                _OrderDetails = New cSupplierDetail(Oasys3DB)
                _OrderDetails.AddLoadField(_OrderDetails.SupplierNumber)
                _OrderDetails.AddLoadField(_OrderDetails.AddressLine1)
                _OrderDetails.AddLoadField(_OrderDetails.AddressLine2)
                _OrderDetails.AddLoadField(_OrderDetails.AddressLine3)
                _OrderDetails.AddLoadField(_OrderDetails.AddressLine4)
                _OrderDetails.AddLoadField(_OrderDetails.AddressLine5)
                _OrderDetails.AddLoadField(_OrderDetails.AddressPostcode)
                _OrderDetails.AddLoadField(_OrderDetails.ReturnsMessageRef)
                _OrderDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _OrderDetails.SupplierNumber, _Number.Value)
                _OrderDetails.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                _OrderDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _OrderDetails.DepotType, "O")
                _OrderDetails.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                _OrderDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _OrderDetails.DepotNumber, _ReturnsDepotNumber.Value)
                _OrderDetails.LoadMatches()
            End If
            Return _OrderDetails
        End Get
        Set(ByVal value As cSupplierDetail)
            _OrderDetails = value
        End Set
    End Property
    Public Property OrderStocks() As List(Of BOStock.cStock)
        Get
            If _OrderStocks Is Nothing Then 'load stocks from database
                Dim StockBO As New BOStock.cStock(Oasys3DB)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.AlternateSupplier, _Number.Value)
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.ItemDeleted, False)
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.ItemObsolete, False)
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.RelatedItemSingle, False)
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.DoNotOrder, False)
                StockBO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(clsOasys3DB.eOperator.pLessThanOrEquals, StockBO.InitialOrderDate, Now.Date)
                StockBO.SortBy(StockBO.HierCategory.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                StockBO.SortBy(StockBO.HierGroup.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                StockBO.SortBy(StockBO.HierSubGroup.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                StockBO.SortBy(StockBO.HierStyle.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                StockBO.SortBy(StockBO.SkuNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)

                _OrderStocks = StockBO.LoadMatches
            End If
            Return _OrderStocks
        End Get
        Set(ByVal value As List(Of BOStock.cStock))
            _OrderStocks = value
        End Set
    End Property
    Public Property OrderStock(ByVal SkuNumber As String) As BOStock.cStock
        Get
            For Each stock As BOStock.cStock In OrderStocks
                If stock.SkuNumber.Value = SkuNumber Then Return stock
            Next
            Return Nothing
        End Get
        Set(ByVal value As BOStock.cStock)
            If _OrderStocks IsNot Nothing Then
                For Each stock As BOStock.cStock In _OrderStocks
                    If stock.SkuNumber.Value = SkuNumber Then
                        stock = value
                    End If
                Next
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns stock item in ReturnStocks collection given sku number. Returns nothing if none found
    ''' </summary>
    ''' <param name="SkuNumber"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ReturnStock(ByVal SkuNumber As String) As BOStock.cStock
        Get
            For Each s As BOStock.cStock In ReturnStocks
                If s.SkuNumber.Value = SkuNumber Then Return s
            Next
            Return Nothing
        End Get
        Set(ByVal value As BOStock.cStock)
            For Each s As BOStock.cStock In ReturnStocks
                If s.SkuNumber.Value = SkuNumber Then s = value
            Next
        End Set
    End Property
    Public Property ReturnStocks() As List(Of BOStock.cStock)
        Get
            If _ReturnStocks Is Nothing Then
                LoadReturnStocks()
            End If
            Return _ReturnStocks
        End Get
        Set(ByVal value As List(Of BOStock.cStock))
            _ReturnStocks = value
        End Set
    End Property
    Public Property ReturnDetails() As cSupplierDetail
        Get
            If _ReturnDetails Is Nothing Then 'load return details
                Try
                    _ReturnDetails = New cSupplierDetail(Oasys3DB)
                    _ReturnDetails.AddLoadField(_ReturnDetails.SupplierNumber)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressLine1)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressLine2)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressLine3)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressLine4)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressLine5)
                    _ReturnDetails.AddLoadField(_ReturnDetails.AddressPostcode)
                    _ReturnDetails.AddLoadField(_ReturnDetails.ReturnsMessageRef)
                    _ReturnDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ReturnDetails.SupplierNumber, _Number.Value)
                    _ReturnDetails.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    _ReturnDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ReturnDetails.DepotType, "R")
                    _ReturnDetails.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    _ReturnDetails.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _ReturnDetails.DepotNumber, _ReturnsDepotNumber.Value)
                    _ReturnDetails.LoadMatches()
                Catch ex As Exception
                    Throw ex
                    Exit Property
                End Try
            End If
            Return _ReturnDetails
        End Get
        Set(ByVal value As cSupplierDetail)
            _ReturnDetails = value
        End Set
    End Property
    Public Property ReturnNotes() As List(Of cSupplierNote)
        Get
            If _ReturnNotes Is Nothing Then
                Dim note As New cSupplierNote(Oasys3DB)
                note.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, note.SupplierID, _Number.Value)
                note.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                note.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, note.Type, "001")
                note.SortBy(note.SeqNo.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                _ReturnNotes = note.LoadMatches
            End If
            Return _ReturnNotes
        End Get
        Set(ByVal value As List(Of cSupplierNote))
            _ReturnNotes = value
        End Set
    End Property

#End Region

#Region "Events"
    Public Event NotifyProgress(ByVal PercentComplete As Integer)

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads supplier for given number into this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="SupplierNumber"></param>
    ''' <remarks></remarks>
    Public Sub LoadSupplier(ByVal SupplierNumber As String)

        Try
            ClearLists()
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _Number, SupplierNumber)
            LoadMatches()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SupplierLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Get dataview of suppliers with columns (number,name,display).  Uses existing collection if possible
    ''' else will return list of all suppliers. Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNumberNamesDataview() As DataView

        Try
            Dim dt As New DataTable

            If _Suppliers IsNot Nothing Then
                dt.Columns.Add(New DataColumn("number", GetType(String)))
                dt.Columns.Add(New DataColumn("name", GetType(String)))
                dt.Columns.Add("display", GetType(String), "number + ' ' + TRIM(name)")

                For Each sup As cSupplierMaster In _Suppliers
                    dt.Rows.Add(sup.Number.Value, sup.Name.Value)
                Next
            Else
                Oasys3DB.ClearAllParameters()
                AddLoadField(_Number)
                AddLoadField(_Name)
                dt = GetSQLSelectDataSet().Tables(0)

                dt.Columns(0).ColumnName = "number"
                dt.Columns(1).ColumnName = "name"
                dt.Columns.Add("display", GetType(String), "number + ' ' + TRIM(name)")
            End If

            dt.PrimaryKey = New DataColumn() {dt.Columns(0)}
            Return New DataView(dt, "", "name", DataViewRowState.CurrentRows)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SupplierLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns supplier return address on separate lines
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReturnNumberNameAddress() As String

        Try
            Dim sb As New StringBuilder(_Number.Value & Space(1) & _Name.Value & Environment.NewLine)
            If ReturnDetails.AddressLine1.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressLine1.Value & Environment.NewLine)
            If _ReturnDetails.AddressLine2.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressLine2.Value & Environment.NewLine)
            If _ReturnDetails.AddressLine3.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressLine3.Value & Environment.NewLine)
            If _ReturnDetails.AddressLine4.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressLine4.Value & Environment.NewLine)
            If _ReturnDetails.AddressLine5.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressLine5.Value & Environment.NewLine)
            If _ReturnDetails.AddressPostcode.Value <> String.Empty Then sb.Append(_ReturnDetails.AddressPostcode.Value & Environment.NewLine)

            sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length)
            Return sb.ToString

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SupplierGeneral, ex)
        End Try

    End Function





    ''' <summary>
    ''' Performs increase of purchase order until the minimum order specification is reached
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PerformMcp()

        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 53))
        Dim value As Decimal = 0
        Dim units As Integer = 0
        Dim weight As Decimal = 0

        'set initial values for mcp conditions
        For Each stock As BOStock.cStock In _OrderStocks
            value += stock.TempQty * stock.NormalSellPrice.Value
            units += stock.TempQty
            weight += stock.TempQty * stock.Weight.Value
        Next

        Do
            'test for exit conditions
            Select Case _OrderDetails.OrderMinType.Value
                Case "M" : If value > _OrderDetails.OrderMinValue.Value Then Exit Do
                Case "U", "C" : If units > _OrderDetails.OrderMinUnits.Value Then Exit Do
                Case "T" : If weight > _OrderDetails.OrderMinWeight.Value Then Exit Do
                Case "E" : If (value > _OrderDetails.OrderMinValue.Value) Or (units > _OrderDetails.OrderMinUnits.Value) Then Exit Do
                Case Else : Exit Sub
            End Select

            Dim stockToIncrease As BOStock.cStock = Nothing
            Dim LowestCover As Decimal = Nothing
            Dim LowestPrice As Decimal = Nothing

            'check whether demand period is greater then zero in order to include in mcp
            For Each stock As BOStock.cStock In _OrderStocks
                If stock.PeriodDemand.Value > 0 Then
                    Dim cover As Decimal = Math.Abs(stock.TempQty - stock.OrderLevel.Value) / (stock.PeriodDemand.Value * _SoqPattern.PeriodAdjuster(stock.PatternBankHol.Value, stock.PatternPromo.Value, stock.PatternSeason.Value, week))

                    If (stockToIncrease Is Nothing) Then
                        stockToIncrease = stock
                        LowestPrice = stock.NormalSellPrice.Value
                        LowestCover = cover

                    ElseIf (cover = LowestCover) And (stock.NormalSellPrice.Value < LowestPrice) Then
                        stockToIncrease = stock
                        LowestPrice = stock.NormalSellPrice.Value
                        LowestCover = cover

                    ElseIf (cover < LowestCover) Then
                        stockToIncrease = stock
                        LowestPrice = stock.NormalSellPrice.Value
                        LowestCover = cover
                    End If
                End If
            Next


            If stockToIncrease Is Nothing Then
                Exit Sub
            Else
                stockToIncrease.TempQty += stockToIncrease.SupplierPackSize.Value
                value += stockToIncrease.SupplierPackSize.Value * stockToIncrease.NormalSellPrice.Value
                units += stockToIncrease.SupplierPackSize.Value
                weight += stockToIncrease.SupplierPackSize.Value * stockToIncrease.Weight.Value
            End If
        Loop

    End Sub


    'Public Function GetSupplierName(ByVal SupplierNumber As String) As String
    '    ClearLoadField()
    '    ClearLoadFilter()
    '    AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, SupplierNumber)
    '    LoadMatches()

    '    Return _Name.Value

    'End Function

    Public Function GetSupplierPalletCheck(ByVal SupplierNumber As String) As Boolean
        ClearLoadField()
        ClearLoadFilter()
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Number, SupplierNumber)
        LoadMatches()

        Return mPalletCheck.Value

    End Function


    Public Function ProcessPeriodEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty

        Try
            'move data last period to prior period
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(QtyOrdPrior.ColumnName, QtyOrdLast)
            Oasys3DB.SetColumnAndValueParameter(QtyRecPrior.ColumnName, QtyRecLast)
            Oasys3DB.SetColumnAndValueParameter(POLinesRecPrior.ColumnName, POLinesRecLast)
            Oasys3DB.SetColumnAndValueParameter(POLinesNotRecPrior.ColumnName, POLinesNotRecLast)
            Oasys3DB.SetColumnAndValueParameter(POLinesOversPrior.ColumnName, POLinesOversLast)
            Oasys3DB.SetColumnAndValueParameter(POLinesUndersPrior.ColumnName, POLinesUndersLast)
            Oasys3DB.SetColumnAndValueParameter(PORecNumPrior.ColumnName, PORecNumLast)
            Oasys3DB.SetColumnAndValueParameter(PORecValuePrior.ColumnName, PORecValueLast)
            Oasys3DB.SetColumnAndValueParameter(DaysToPORecPrior.ColumnName, DaysToPORecLast)
            Oasys3DB.SetColumnAndValueParameter(DaysShortestPrior.ColumnName, DaysShortestLast)
            Oasys3DB.SetColumnAndValueParameter(DaysLongestPrior.ColumnName, DaysLongestLast)

            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            If SaveError = False Then
                'move data PERIOD  to LAST
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                Oasys3DB.SetColumnAndValueParameter(QtyOrdLast.ColumnName, QtyOrdPeriod)
                Oasys3DB.SetColumnAndValueParameter(QtyRecLast.ColumnName, QtyRecPeriod)
                Oasys3DB.SetColumnAndValueParameter(POLinesRecLast.ColumnName, POLinesRecPeriod)
                Oasys3DB.SetColumnAndValueParameter(POLinesNotRecLast.ColumnName, POLinesNotRecPeriod)
                Oasys3DB.SetColumnAndValueParameter(POLinesOversLast.ColumnName, POLinesOversPeriod)
                Oasys3DB.SetColumnAndValueParameter(POLinesUndersLast.ColumnName, POLinesUndersPeriod)
                Oasys3DB.SetColumnAndValueParameter(PORecNumLast.ColumnName, PORecNumPeriod)
                Oasys3DB.SetColumnAndValueParameter(PORecValueLast.ColumnName, PORecValuePeriod)
                Oasys3DB.SetColumnAndValueParameter(DaysToPORecLast.ColumnName, DaysToPORecPeriod)
                Oasys3DB.SetColumnAndValueParameter(DaysShortestLast.ColumnName, DaysShortestPeriod)
                Oasys3DB.SetColumnAndValueParameter(DaysLongestLast.ColumnName, DaysLongestPeriod)

                NoRecs = Oasys3DB.Update()

                If NoRecs = -1 Then
                    SaveError = True
                Else
                    SaveError = False
                End If

                If SaveError = False Then
                    'zeroise the data 
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                    Oasys3DB.SetColumnAndValueParameter(QtyOrdPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(QtyRecPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(POLinesRecPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(POLinesNotRecPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(POLinesOversPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(POLinesUndersPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(PORecNumPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(PORecValuePeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(DaysToPORecPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(DaysShortestPeriod.ColumnName, 0)
                    Oasys3DB.SetColumnAndValueParameter(DaysLongestPeriod.ColumnName, 0)

                    NoRecs = Oasys3DB.Update()

                    If NoRecs = -1 Then
                        SaveError = True
                    Else
                        SaveError = False
                    End If
                End If
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cSupplierMaster - ProcessPeriodEnd failed with exception " & ex.Message)
            Return False
        End Try

    End Function
    Public Function ProcessYearEnd() As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim RawSql As String = String.Empty
        Try
            'move data 
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

            Oasys3DB.SetColumnAndValueParameter(QtyOrdPYR.ColumnName, QtyOrdYTD)
            Oasys3DB.SetColumnAndValueParameter(QtyRecPYR.ColumnName, QtyRecYTD)
            Oasys3DB.SetColumnAndValueParameter(POLinesRecPYR.ColumnName, POLinesRecYTD)
            Oasys3DB.SetColumnAndValueParameter(POLinesNotRecPYR.ColumnName, POLinesNotRecYTD)
            Oasys3DB.SetColumnAndValueParameter(POLinesOversPYR.ColumnName, POLinesOversYTD)
            Oasys3DB.SetColumnAndValueParameter(POLinesUndersPYR.ColumnName, POLinesUndersYTD)
            Oasys3DB.SetColumnAndValueParameter(PORecNumPYR.ColumnName, PORecNumYTD)
            Oasys3DB.SetColumnAndValueParameter(PORecValuePYR.ColumnName, PORecValueYTD)
            Oasys3DB.SetColumnAndValueParameter(DaysToPORecPYR.ColumnName, DaysToPORecYTD)
            Oasys3DB.SetColumnAndValueParameter(DaysShortestPYR.ColumnName, DaysShortestYTD)
            Oasys3DB.SetColumnAndValueParameter(DaysLongestPYR.ColumnName, DaysLongestYTD)


            NoRecs = Oasys3DB.Update()

            If NoRecs = -1 Then
                SaveError = True
            Else
                SaveError = False
            End If

            If SaveError = False Then
                'zeroise the fields 
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

                Oasys3DB.SetColumnAndValueParameter(QtyOrdYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(QtyRecYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(POLinesRecYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(POLinesNotRecYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(POLinesOversYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(POLinesUndersYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(PORecNumYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(PORecValueYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(DaysToPORecYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(DaysShortestYTD.ColumnName, 0)
                Oasys3DB.SetColumnAndValueParameter(DaysLongestYTD.ColumnName, 0)

                NoRecs = Oasys3DB.Update()

                If NoRecs = -1 Then
                    SaveError = True
                Else
                    SaveError = False
                End If
            End If

            Return SaveError
        Catch ex As Exception
            Trace.WriteLine("cSupplierMaster - ProcessYearEnd failed with exception " & ex.Message)
            Return False
        End Try


    End Function
    Public Function FlagAllAsDeleted() As Boolean
        Oasys3DB.ExecuteSql("UPDATE SUPMAS SET DELC = '1' where delc = '0'")
    End Function
    Public Function GetActiveRecords() As List(Of cSupplierMaster)
        Dim lstSupplierMaster As List(Of cSupplierMaster)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _DeletedByHO, 0)
        lstSupplierMaster = LoadMatches()
        Return lstSupplierMaster
    End Function

#End Region

#Region "Soq Methods"

    ''' <summary>
    ''' Resets all soq information for this supplier and associated stock items.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SoqReset()

        Try
            'start transaction
            Oasys3DB.BeginTransaction()

            'assign object values and persist to database
            _SoqDate.Value = Nothing
            _SoqNumber.Value = "000000"
            _SoqOrdered.Value = False

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_SoqNumber.ColumnName, _SoqNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_SoqOrdered.ColumnName, _SoqOrdered.Value)
            Oasys3DB.SetColumnAndValueParameter(_SoqDate.ColumnName, _SoqDate.Value)
            SetDBKeys()
            Oasys3DB.Update()

            'update all stock items
            For Each stock As BOStock.cStock In OrderStocks
                stock.SoqReset()
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As OasysDbException
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.SupplierSoqReset, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Perform Soq inventory for all valid stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SoqInventory(ByVal daysOpen As Integer)

        Try
            'check if any stocks to perform soq
            Dim count As Integer = OrderStocks.Count
            If count = 0 Then
                RaiseEvent NotifyProgress(-1)
                Exit Sub
            End If

            'start transaction
            Oasys3DB.BeginTransaction()

            Dim soqSuggested As Boolean = False
            Dim supLeadReview As Integer = DaysLeadVariable()
            Dim supLeadReviewFixed As Decimal = DaysLeadReviewFixed()
            Dim orderDue As Date = OrderDueDate(daysOpen)
            Dim orderNextDue As Date = OrderNextDueDate(daysOpen)

            Dim index As Integer = 0
            For Each stock As BOStock.cStock In _OrderStocks
                index += 1
                stock.SoqInventory(supLeadReview, supLeadReviewFixed, daysOpen, _SoqPattern, orderDue, orderNextDue)
                If stock.OrderLevel.Value > 0 Then soqSuggested = True
                RaiseEvent NotifyProgress(CInt(index / count * 100))
            Next

            'If soq suggested then get next soq number
            If soqSuggested Then
                Dim sysNumbers As New BOSystem.cSystemNumbers(Oasys3DB)
                _SoqNumber.Value = sysNumbers.GetSOQNumber.ToString("000000")
            Else
                _SoqNumber.Value = "000000"
            End If

            _SoqDate.Value = Now.Date
            _SoqOrdered.Value = False

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_SoqNumber.ColumnName, _SoqNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_SoqOrdered.ColumnName, _SoqOrdered.Value)
            Oasys3DB.SetColumnAndValueParameter(_SoqDate.ColumnName, _SoqDate.Value)
            SetDBKeys()
            Oasys3DB.Update()

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Updates Soq information in supplier master table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UpdateSOQ()

        _SoqDate.Value = Now.Date
        _SoqOrdered.Value = False

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_SoqNumber.ColumnName, _SoqNumber.Value)
        Oasys3DB.SetColumnAndValueParameter(_SoqOrdered.ColumnName, _SoqOrdered.Value)
        Oasys3DB.SetColumnAndValueParameter(_SoqDate.ColumnName, _SoqDate.Value)
        SetDBKeys()
        Oasys3DB.Update()

    End Sub


    Public Function DaysReviewFixed() As Decimal

        Dim reviewFixed As Decimal = 0

        For dayWeek As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
            If OrderDetails.ReviewDay(CStr(dayWeek)).Value Then reviewFixed += 1
        Next

        If reviewFixed = 0 Then reviewFixed = 1 Else reviewFixed = 1 / reviewFixed
        Return reviewFixed

    End Function
    Public Function DaysReviewVariable() As Integer

        Dim reviewVariable As Integer = 0

        Dim lead As Integer = 0
        If OrderDetails.BBC.Value = "" Then lead = 1

        'get number of days till next review day (limited to one week)
        Do Until OrderDetails.ReviewDay(CStr(Now.AddDays(reviewVariable + lead).DayOfWeek)).Value Or reviewVariable = 7
            reviewVariable += 1
        Loop


        ' Check whether supplier has closedown dates and get new review date if true and determine if
        ' value falls within this period and calculate next review day after closeEnd
        If (OrderDetails.DateOrderCloseStart.Value <> Nothing) And (OrderDetails.DateOrderCloseEnd.Value <> Nothing) Then

            If (Now.AddDays(reviewVariable) > OrderDetails.DateOrderCloseStart.Value) And (Now.AddDays(reviewVariable) < OrderDetails.DateOrderCloseEnd.Value) Then

                Dim check As Integer = 0
                reviewVariable = CInt(DateDiff(DateInterval.Day, Now.AddDays(lead), OrderDetails.DateOrderCloseEnd.Value))
                Do Until OrderDetails.ReviewDay(CStr(Now.AddDays(reviewVariable + lead).DayOfWeek)).Value Or check = 7
                    reviewVariable += 1
                    check += 1
                Loop

            End If
        End If

        Return reviewVariable

    End Function
    Public Function DaysLeadVariable() As Integer

        Dim lead As Integer = 0
        If OrderDetails.BBC.Value = "" Then lead = 1

        DaysLeadVariable = OrderDetails.LeadTime(CStr(Now.AddDays(lead).DayOfWeek)).Value
        If DaysLeadVariable = 0 Then DaysLeadVariable = OrderDetails.LeadTimeFixed.Value

    End Function
    Public Function DaysLeadFixed() As Decimal

        Return CDec(OrderDetails.LeadTimeFixed.Value / 7)

    End Function
    Public Function DaysLeadReviewVariable() As Integer

        Return DaysLeadVariable() + DaysReviewVariable()

    End Function
    Public Function DaysLeadReviewFixed() As Decimal

        Return DaysLeadFixed() + DaysReviewFixed()

    End Function

#End Region

#Region "Ordering Methods"

    ''' <summary>
    ''' Loads suppliers available for ordering into the Suppliers property
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadOrderSuppliers(ByVal Type As SupplierTypes, Optional ByVal SupplierNumbers As ArrayList = Nothing)

        'get list of suppliers from database view with valid stock items
        Dim proc As String = String.Empty
        Select Case Type
            Case SupplierTypes.All : proc = My.Resources.Procs.SuppliersOrderingGetAll
            Case SupplierTypes.Direct : proc = My.Resources.Procs.SuppliersOrderingGetDirect
            Case SupplierTypes.Warehouse : proc = My.Resources.Procs.SuppliersOrderingGetWarehouse
        End Select

        Dim dt As New DataTable
        dt = Oasys3DB.ExecuteSql("EXEC " & proc).Tables(0)

        'load supplier master and details from rows returned
        _Suppliers = New List(Of cSupplierMaster)
        For Each dr As DataRow In dt.Rows
            Dim sup As New cSupplierMaster(Oasys3DB)
            Dim det As New cSupplierDetail(Oasys3DB)
            sup.LoadFromRow(dr)
            det.LoadFromRow(dr)
            sup.OrderDetails = det
            _Suppliers.Add(sup)
        Next

    End Sub

    ''' <summary>
    ''' Returns string representation of supplier order type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OrderTypeString() As String

        Dim sb As New Text.StringBuilder
        Select Case OrderDetails.BBC.Value.Trim
            Case "D" : sb.Append("IW Discrete")
            Case "C" : sb.Append("IW Consolidated")
            Case "W" : sb.Append("IW")
            Case "A" : sb.Append("Alternative Supplier")
            Case Else : sb.Append("Direct")
        End Select
        If OrderDetails.Tradanet.Value Then sb.Append(" Tradanet")

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Returns order due date for supplier taking days store open into account. 
    '''  Throws exception on error
    ''' </summary>
    ''' <param name="daysOpen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OrderDueDate(ByVal daysOpen As Integer, Optional ByVal fromDate As Date = Nothing) As Date

        If fromDate = Nothing Then
            fromDate = Now.Date
        End If

        Dim lead As Integer = 0
        Dim dueDate As Date

        Select Case OrderDetails.BBC.Value
            Case "D", "C", "W", "A" : dueDate = fromDate.AddDays(OrderDetails.LeadTime(CStr(fromDate.AddDays(1).DayOfWeek)).Value - 1)
            Case Else : dueDate = fromDate.AddDays(OrderDetails.LeadTime(CStr(fromDate.DayOfWeek)).Value)
        End Select

        'check that duedate is on an open day
        Select Case dueDate.DayOfWeek
            Case DayOfWeek.Sunday : If daysOpen < 7 Then dueDate = dueDate.AddDays(1)
            Case DayOfWeek.Saturday : If daysOpen < 6 Then dueDate = dueDate.AddDays(2)
            Case DayOfWeek.Friday : If daysOpen < 5 Then dueDate = dueDate.AddDays(3)
            Case DayOfWeek.Thursday : If daysOpen < 4 Then dueDate = dueDate.AddDays(4)
            Case DayOfWeek.Wednesday : If daysOpen < 3 Then dueDate = dueDate.AddDays(5)
            Case DayOfWeek.Tuesday : If daysOpen < 2 Then dueDate = dueDate.AddDays(6)
        End Select

        Return dueDate

    End Function

    ''' <summary>
    ''' Returns next but one order due date taking days store open into account.
    '''  Throws exception on error
    ''' </summary>
    ''' <param name="daysOpen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OrderNextDueDate(ByVal daysOpen As Integer) As Date

        Return OrderDueDate(daysOpen, OrderDueDate(daysOpen))

    End Function

    Public Function OrderMinimumString() As String

        Select Case OrderDetails.OrderMinType.Value
            Case "M" : Return OrderDetails.OrderMinValue.Value & " Monetary Value"
            Case "U", "C" : Return OrderDetails.OrderMinUnits.Value & " Cartons"
            Case "T" : Return OrderDetails.OrderMinWeight.Value & " Weight - Capacity (Kg)"
            Case "E" : Return OrderDetails.OrderMinValue.Value & " Monetary Value OR " & OrderDetails.OrderMinUnits.Value & " Cartons"
            Case Else : Return "No Constraint"
        End Select

    End Function

    Public Function UpdatePOOrdered(ByVal OrderValue As Decimal, ByVal OrderQty As Integer) As Boolean

        _PONumOutstanding.Value += 1
        _POValueOutstanding.Value += OrderValue
        mQtyOrdPeriod.Value += OrderQty
        _DateLastOrdered.Value = Now.Date
        _SoqOrdered.Value = True
        _SoqNumber.Value = "000000"


        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_PONumOutstanding.ColumnName, _PONumOutstanding.Value)
        Oasys3DB.SetColumnAndValueParameter(_POValueOutstanding.ColumnName, _POValueOutstanding.Value)
        Oasys3DB.SetColumnAndValueParameter(mQtyOrdPeriod.ColumnName, mQtyOrdPeriod.Value)
        Oasys3DB.SetColumnAndValueParameter(_DateLastOrdered.ColumnName, _DateLastOrdered.Value)
        Oasys3DB.SetColumnAndValueParameter(_SoqOrdered.ColumnName, _SoqOrdered.Value)
        Oasys3DB.SetColumnAndValueParameter(_SoqNumber.ColumnName, _SoqNumber.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then Return True
        Return False

    End Function

    Public Function UpdatePoOutstanding(ByVal Value As Decimal, ByVal Qty As Integer) As Boolean

        _PONumOutstanding.Value += Qty
        _POValueOutstanding.Value += Value

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_PONumOutstanding.ColumnName, _PONumOutstanding.Value)
        Oasys3DB.SetColumnAndValueParameter(_POValueOutstanding.ColumnName, _POValueOutstanding.Value)
        SetDBKeys()
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function

#End Region

#Region "Returns Methods"

    ''' <summary>
    ''' Loads all returns suppliers into suppliers collection of this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadReturnSuppliers()

        Try
            Dim sup As New cSupplierMaster(Oasys3DB)
            sup.AddLoadField(sup.Number)
            sup.AddLoadField(sup.Name)
            sup.AddLoadField(sup.ReturnsDepotNumber)
            _Suppliers = sup.LoadMatches

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnSuppliersLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all return stocks for this supplier into the ReturnStocks collection. Throws an Oasys exception if error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadReturnStocks()

        Try
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.AddLoadField(stocks.SkuNumber)
            stocks.AddLoadField(stocks.SupplierNo)
            stocks.AddLoadField(stocks.Description)
            stocks.AddLoadField(stocks.NormalSellPrice)
            stocks.AddLoadField(stocks.CostPrice)
            stocks.AddLoadField(stocks.StockOnHand)
            stocks.AddLoadField(stocks.UnitsReceivedToday)
            stocks.AddLoadField(stocks.ValueReceivedToday)
            stocks.AddLoadField(stocks.UnitsInOpenReturns)
            stocks.AddLoadField(stocks.ValueInOpenReturns)
            stocks.AddLoadField(stocks.MarkDownQuantity)
            stocks.AddLoadField(stocks.WriteOffQuantity)
            stocks.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, stocks.SupplierNo, _Number.Value)
            _ReturnStocks = stocks.LoadMatches

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnStocksLoad, ex)
        End Try

    End Sub

    Public Function UpdateSupplierReceived(ByVal ReceivedValue As Decimal, ByVal ReceivedQty As Integer, ByVal ReceivedDate As Date) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_PONumOutstanding.ColumnName, _PONumOutstanding.Value - 1)
        Oasys3DB.SetColumnAndValueParameter(_POValueOutstanding.ColumnName, _POValueOutstanding.Value - ReceivedValue)
        Oasys3DB.SetColumnAndValueParameter(mQtyOrdPeriod.ColumnName, mQtyOrdPeriod.Value - ReceivedQty)
        Oasys3DB.SetColumnAndValueParameter(_DateLastReceived.ColumnName, ReceivedDate)
        Oasys3DB.SetColumnAndValueParameter(_SoqOrdered.ColumnName, True)
        SetDBKeys()
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function

#End Region

End Class
