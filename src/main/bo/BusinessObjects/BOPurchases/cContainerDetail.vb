﻿<Serializable()> Public Class cContainerDetail
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CONDET"
        BOFields.Add(_AssemblyDepotNumber)
        BOFields.Add(_ContainerNumber)
        BOFields.Add(_StorePoNumber)
        BOFields.Add(_StorePoLineNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Quantity)
        BOFields.Add(_Price)
        BOFields.Add(_RejectedReason)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cContainerDetail)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cContainerDetail)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cContainerDetail))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cContainerDetail(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _AssemblyDepotNumber As New ColField(Of String)("ADEP", "", "Assembly Depot Number", True, False)
    Private _ContainerNumber As New ColField(Of String)("CNUM", "", "Container Number", True, False)
    Private _StorePoNumber As New ColField(Of String)("PNUM", "", "Store PO Number", True, False)
    Private _StorePoLineNumber As New ColField(Of String)("LINE", "", "Store PO Line Number", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "SKU Number", False, False)
    Private _Quantity As New ColField(Of Decimal)("QUAN", 0, "Quantity", False, False)
    Private _Price As New ColField(Of Decimal)("PRIC", 0, "Price", False, False, 2)
    Private _RejectedReason As New ColField(Of String)("REJI", "", "Rejected Reason", False, False)

#End Region

#Region "Field Properties"

    Public Property AssemblyDepotNumber() As ColField(Of String)
        Get
            AssemblyDepotNumber = _AssemblyDepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _AssemblyDepotNumber = value
        End Set
    End Property
    Public Property ContainerNumber() As ColField(Of String)
        Get
            ContainerNumber = _ContainerNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ContainerNumber = value
        End Set
    End Property
    Public Property StorePoNumber() As ColField(Of String)
        Get
            StorePoNumber = _StorePoNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StorePoNumber = value
        End Set
    End Property
    Public Property StorePoLineNumber() As ColField(Of String)
        Get
            StorePoLineNumber = _StorePoLineNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StorePoLineNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Decimal)
        Get
            Quantity = _Quantity
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Quantity = value
        End Set
    End Property
    Public Property Price() As ColField(Of Decimal)
        Get
            Price = _Price
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Price = value
        End Set
    End Property
    Public Property RejectedReason() As ColField(Of String)
        Get
            RejectedReason = _RejectedReason
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectedReason = value
        End Set
    End Property


#End Region

#Region "Methods"
    Private _Stock As BOStock.cStock = Nothing

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property

#End Region

End Class
