﻿Imports System.Threading

Public Class Soq
    Private _connectionString As String = String.Empty
    Private _doForecasting As Boolean
    Public Event SoqStarted(ByVal supplier As Purchasing.SupplierRow)
    Public Event SoqProgress(ByVal supplier As Purchasing.SupplierRow, ByVal progress As Integer)
    Public Event SoqCompleted(ByVal supplier As Purchasing.SupplierRow, ByVal status As CompletedStatus)

    Public Sub New(ByVal connectionString As String)
        _connectionString = connectionString
        _doForecasting = Oasys.System.Parameter.GetBoolean(207)
    End Sub


    Public Sub PerformInitialisation(ByVal supplierNumber As String)

        Dim al As New ArrayList
        al.Add(supplierNumber)
        PerformInitialisation(al)

    End Sub

    Public Sub PerformInitialisation(ByVal supplierNumbers As ArrayList)

        'instantiate typed dataset class and load required suppliers and stocks and sales weighting
        Dim ds As New Purchasing(_connectionString)
        ds.Supplier.LoadSuppliers(supplierNumbers)
        ds.Stock.LoadOrderStocks(supplierNumbers)
        ds.SaleWeight.LoadSaleWeights()

        'enumerate over suppliers loaded and perform soq
        For Each sup As Purchasing.SupplierRow In ds.Supplier.Rows
            TraceDebug(sup.ToString, "Performing SOQ")
            Dim stockcount As Integer = sup.GetStockRows.Count
            RaiseEvent SoqStarted(sup)

            'check if deleted
            If sup.IsDeleted Then
                RaiseEvent SoqCompleted(sup, CompletedStatus.Deleted)
                Continue For
            End If

            'check has any stock items
            If stockcount = 0 Then
                RaiseEvent SoqCompleted(sup, CompletedStatus.NoStocks)
                Continue For
            End If


            'perform initialisation for each stock item
            For Each stock As Purchasing.StockRow In sup.GetStockRows
                RaiseEvent SoqProgress(sup, CInt(Array.IndexOf(sup.GetStockRows, stock) / stockcount * 100))

                'check if need to do forecasting
                If _doForecasting Then
                    'roll on summary fields
                    For index As Integer = 12 To 2 Step -1
                        stock.FlierPeriod(index) = stock.FlierPeriod(index - 1)
                    Next
                    stock.FlierPeriod1 = String.Empty

                    For index As Integer = 14 To 2 Step -1
                        stock.UnitSalesPeriod(index) = stock.UnitSalesPeriod(index - 1)
                        stock.DaysOutStockPeriod(index) = stock.DaysOutStockPeriod(index - 1)
                    Next
                    stock.UnitSalesPeriod1 = 0
                    stock.DaysOutStockPeriod1 = 0


                    'check if item is out of stock and whether current period sales are less than period demand
                    If stock.DaysOutStockPeriod2 > 0 AndAlso stock.UnitSalesPeriod2 < stock.PeriodDemand Then
                        TraceDebug(stock.ToString, "Out of stock and current sales < period demand")
                        stock.IsInitialised = False
                        Continue For
                    End If

                    'get initialisation periods
                    Dim soqPeriods As List(Of SoqPeriod) = GetInitialisationPeriods(stock)

                    If Not DemandPatternReclassified(stock, soqPeriods) AndAlso stock.IsInitialised Then
                        'get sale weighting adjusters
                        Dim b As String = stock.PatternBankHol
                        Dim p As String = stock.PatternPromo
                        Dim s As String = stock.PatternSeason
                        Dim bank As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = b)
                        Dim promo As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = p)
                        Dim season As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = s)

                        If Not ExceptionalFliersFound(stock, bank, promo, season) Then
                            ForecastParameters(stock, bank, promo, season)

                            Select Case stock.DemandPattern
                                Case DemandPattern.[New], DemandPattern.Fast, DemandPattern.Erratic, DemandPattern.TrendDown, DemandPattern.TrendUp
                                    'load supplier details for lead and review times
                                    ds.SupplierDetail.LoadDetails(sup.Number, "O", sup.OrderDepotNumber)
                                    Dim det As Purchasing.SupplierDetailRow = sup.GetSupplierDetailRows(0)

                                    ForecastBufferConstant(stock, det.DaysLeadFixed, det.DaysReviewFixed)
                            End Select

                        End If
                    End If
                End If

                'perform initialisation routine
                If stock.IsInitialised = False Then
                    stock.DemandPattern = stock.DemandPattern.Trim
                    stock.PeriodDemand = 0
                    stock.PeriodTrend = 0
                    stock.ErrorForecast = 0
                    stock.ErrorSmoothed = 0
                    stock.ValueAnnualUsage = String.Empty
                    stock.BufferConversion = 0
                    stock.BufferStock = 0
                    stock.OrderLevel = 0

                    'get sale weighting adjusters
                    Dim b As String = stock.PatternBankHol
                    Dim p As String = stock.PatternPromo
                    Dim s As String = stock.PatternSeason
                    Dim bank As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = b)
                    Dim promo As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = p)
                    Dim season As Purchasing.SaleWeightRow = ds.SaleWeight.First(Function(f As Purchasing.SaleWeightRow) f.PATT = s)

                    'get initialisation periods
                    Dim soqPeriods As New List(Of SoqPeriod)
                    Do
                        soqPeriods = GetInitialisationPeriods(stock)
                    Loop While Not DemandPatternInitialised(stock, bank, promo, season, soqPeriods)

                    'initialise value annual usage
                    Dim value As Decimal = stock.PeriodDemand * 52 * stock.SalePrice
                    stock.ValueAnnualUsage = SoqConstants.AnnualUsage(CInt(value))
                    stock.DateLastForeInit = Now.Date
                    stock.IsInitialised = True
                End If
            Next

            'update stocks 
            ds.Stock.PerformChanges()
            RaiseEvent SoqCompleted(sup, CompletedStatus.Successful)
        Next

    End Sub


    Private Function GetInitialisationPeriods(ByRef stock As Purchasing.StockRow) As List(Of SoqPeriod)

        Dim soqPeriods As New List(Of SoqPeriod)
        TraceDebug(stock.ToString, "Initialising periods")

        'get number of weeks in stock
        'if there is a date first stocked then set values
        Dim days As Integer = 0
        Dim weeks As Integer = 0
        If stock.DateFirstStocked <> Nothing Then
            days = CInt(DateDiff(DateInterval.Day, Now.Date, stock.DateFirstStocked.Date))
            weeks = CInt(Math.Abs(Math.Ceiling(days / 7)))
        End If

        Select Case True
            Case weeks < SoqConstants.InitNewLimit
                'use last soq.InitNewPeriods periods
                TraceDebug(stock.ToString, "In stock less than new stock limit")
                stock.DemandPattern = DemandPattern.[New]
                Dim start As Integer = Math.Max(0, weeks - SoqConstants.InitNewPeriods)
                For index As Integer = start To weeks
                    soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index - 1), stock.DaysOutStockPeriod(index)))
                Next

            Case (SoqConstants.InitNewLimit <= weeks) AndAlso (weeks < SoqConstants.InitNewLimit + SoqConstants.InitPostLimit)
                'add all but first NumPeriodsLimitPostNew periods but do not include fliers
                TraceDebug(stock.ToString, "In stock between lower and upper limits")
                For index As Integer = SoqConstants.InitPostLimit + 1 To weeks
                    If stock.FlierPeriod(index).ToString.Trim.Length = 0 Then
                        soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index - 1), stock.DaysOutStockPeriod(index)))
                    End If
                Next

            Case Else
                'add all periods but do not include fliers
                TraceDebug(stock.ToString, "In stock above upper limit")
                For index As Integer = 2 To 13
                    If stock.FlierPeriod(index).ToString.Trim.Length = 0 Then
                        soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index - 1), stock.DaysOutStockPeriod(index)))
                    End If
                Next
        End Select

        'calculate average period demand for selected periods where there are no days out of stock
        Dim sales As New List(Of Decimal)
        For Each period As SoqPeriod In soqPeriods.Where(Function(p) p.OutOfStock = False)
            sales.Add(period.Sales)
        Next

        'remove any out of stock periods that are less then the average sales calculated above
        If sales.Count > 0 Then
            Dim average As Decimal = sales.Average
            TraceDebug(stock.ToString, "Average sales over initialised periods: " & average)
            soqPeriods.RemoveAll(Function(p) p.OutOfStock = True AndAlso p.Sales < average)
        End If

        'do trace output
        For Each period As SoqPeriod In soqPeriods
            TraceDebug(stock.ToString, "Period Used: " & period.Index)
        Next

        Return soqPeriods

    End Function

    Private Function DemandPatternReclassified(ByRef stock As Purchasing.StockRow, ByRef soqPeriods As List(Of SoqPeriod)) As Boolean

        'perform demand pattern classification checks
        Select Case True
            Case soqPeriods.CountZeroSales = soqPeriods.Count
                TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to obsolete")
                stock.DemandPattern = DemandPattern.Obsolete
                stock.IsInitialised = False

            Case soqPeriods.CountZeroSalesConsecutive = SoqConstants.NumPeriodsConsecutiveZero
                TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to superceded")
                stock.DemandPattern = DemandPattern.Superceded
                stock.IsInitialised = False

            Case Else
                Select Case stock.DemandPattern
                    Case DemandPattern.Normal, DemandPattern.Erratic, DemandPattern.Fast, DemandPattern.TrendDown, DemandPattern.TrendUp

                        If soqPeriods.CountZeroSales > SoqConstants.ZeroDemandLumpyCheck(soqPeriods.Count) Then
                            stock.IsInitialised = False

                            If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                                TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to slow")
                                stock.DemandPattern = DemandPattern.Slow
                            Else
                                TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to lumpy")
                                stock.DemandPattern = DemandPattern.Lumpy
                            End If
                        End If

                    Case DemandPattern.Slow, DemandPattern.Lumpy
                        stock.IsInitialised = False

                        Select Case soqPeriods.CountZeroSales
                            Case Is <= SoqConstants.SlowLumpyCheck(soqPeriods.Count)
                                TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to normal")
                                stock.DemandPattern = DemandPattern.Normal

                            Case Else
                                Select Case stock.DemandPattern
                                    Case DemandPattern.Slow
                                        If soqPeriods.AverageSales >= SoqConstants.SlowAveDemandCheck Then
                                            TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to lumpy")
                                            stock.DemandPattern = DemandPattern.Lumpy
                                        End If

                                    Case DemandPattern.Lumpy
                                        If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                                            TraceDebug(stock.ToString, "Reclassified " & stock.DemandPattern & " to slow")
                                            stock.DemandPattern = DemandPattern.Slow
                                        End If
                                End Select
                        End Select
                End Select
        End Select

        Return (stock.IsInitialised = False)

    End Function

    Private Function ExceptionalFliersFound(ByRef stock As Purchasing.StockRow, ByRef bank As Purchasing.SaleWeightRow, ByRef promo As Purchasing.SaleWeightRow, ByRef season As Purchasing.SaleWeightRow) As Boolean

        TraceDebug(stock.ToString, "Performing exceptional flier check")

        ' Check whether exceptional flier found in current period and flag period if true
        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 7))
        Dim adjuster As Decimal = CDec(bank.Pattern(week) * promo.Pattern(week) * season.Pattern(week))
        Dim F1 As Double = (stock.PeriodDemand + stock.PeriodTrend) * adjuster
        Dim periodCheck As Double = stock.UnitSalesPeriod1 - F1
        Dim absLimit As Double = Math.Abs(SoqConstants.InputDemandSupressor * stock.ErrorForecast)

        TraceDebug(stock.ToString, "Current week = " & week)
        TraceDebug(stock.ToString, "Week adjuster = " & adjuster)
        TraceDebug(stock.ToString, "Period(1) (demand+trend)*adjuster = " & F1)
        TraceDebug(stock.ToString, "Period(1) absolute period check = " & periodCheck)
        TraceDebug(stock.ToString, "Absolute flier limit = " & absLimit)


        If periodCheck < absLimit Then
            TraceDebug(stock.ToString, "Flier found for period 1")
            stock.FlierPeriod1 = "F"
            stock.IsInitialised = False

            ' Check to see if second consecutive flier
            If stock.FlierPeriod2 = "F" Then
                TraceDebug(stock.ToString, "Flier found for period 2")
                Dim average1 As Double = stock.UnitSalesPeriod1 / (bank.Pattern(week + 1) * promo.Pattern(week + 1) * season.Pattern(week + 1))
                Dim average2 As Double = stock.UnitSalesPeriod2 / (bank.Pattern(week + 2) * promo.Pattern(week + 2) * season.Pattern(week + 2))
                stock.PeriodDemand = CDec((average1 + average2) / 2)
                stock.PeriodTrend = 0
                stock.ErrorSmoothed = 0

                Select Case stock.PeriodDemand
                    Case Is <= 3 : stock.ErrorForecast = SoqConstants.StdDevMultiplier(stock.PeriodDemand) * stock.PeriodDemand
                    Case Is <= 10 : stock.ErrorForecast = SoqConstants.StdDevMultiplier(stock.PeriodDemand) * stock.PeriodDemand
                    Case Else : stock.ErrorForecast = SoqConstants.StdDevMultiplier(stock.PeriodDemand) * stock.PeriodDemand
                End Select
            End If

            TraceDebug(stock.ToString, "Stock is not to be forecasted")
            Return True
        End If

        ' Check whether item has 2 or more non-consecutive fliers in last 12 periods and if so then reset all flier indicators and reclassify item
        Dim numberNonConsecutiveFliers As Integer = 0
        For period As Integer = 3 To 12
            If stock.FlierPeriod(period) = "F" And stock.FlierPeriod(period - 1) <> "F" Then numberNonConsecutiveFliers += 1
        Next

        If numberNonConsecutiveFliers >= 2 Then
            TraceDebug(stock.ToString, numberNonConsecutiveFliers & " non consecutive fliers found")
            TraceDebug(stock.ToString, "Stock is to be reclassified")
            stock.IsInitialised = False
            For period As Integer = 1 To 12
                stock.FlierPeriod(period) = ""
            Next
            Return True
        End If

        Return False

    End Function

    Private Sub ForecastParameters(ByRef stock As Purchasing.StockRow, ByRef bank As Purchasing.SaleWeightRow, ByRef promo As Purchasing.SaleWeightRow, ByRef season As Purchasing.SaleWeightRow)

        TraceDebug(stock.ToString, "Performing forecasting")

        'check if flier for this period
        If stock.FlierPeriod1 = "F" Then
            TraceDebug(stock.ToString, "Flier found thus forecasting stopped")
            Exit Sub
        End If

        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 7))
        Dim weekAdjuster As Decimal = CDec(bank.Pattern(week) * promo.Pattern(week) * season.Pattern(week))

        Dim lastDemand As Double = stock.PeriodDemand - (1 - SoqConstants.SmoothingConstant) / SoqConstants.SmoothingConstant * stock.PeriodTrend     'Ei-1
        Dim adjustedDemand As Double = Math.Max(CType(stock.UnitSalesPeriod1, Double) / weekAdjuster, 0)                                                'Ri
        Dim estimateDemand As Double = lastDemand + SoqConstants.SmoothingConstant * (adjustedDemand - lastDemand)                'Ei
        Dim actualTrend As Double = SoqConstants.SmoothingConstant * (adjustedDemand - lastDemand)                                 'Gi
        Dim estimateError As Double = stock.UnitSalesPeriod1 / weekAdjuster - Math.Abs(stock.PeriodDemand + stock.PeriodTrend)

        ' Calculate new parameters for item PeriodDemand/trend and smoothed error
        Dim newTrend As Double = stock.PeriodTrend + SoqConstants.SmoothingConstant * (actualTrend - stock.PeriodTrend)
        Dim newDemand As Double = estimateDemand + (1 - SoqConstants.SmoothingConstant) / SoqConstants.SmoothingConstant * newTrend
        Dim newErrorSmoothed As Double = stock.ErrorSmoothed + SoqConstants.SmoothingConstant * (estimateError - stock.ErrorSmoothed)

        ' Calculate error forecast
        Dim errMAD As Double = stock.ErrorForecast * 0.8 + SoqConstants.SmoothingConstant * (Math.Abs(estimateError) - stock.ErrorForecast * 0.8)
        Dim newErrorForecast As Double = errMAD / 0.8
        Dim TrackingSignal As Double = newErrorSmoothed / errMAD


        'output calculated values
        TraceDebug(stock.ToString, "lastDemand       = " & lastDemand)
        TraceDebug(stock.ToString, "adjustedDemand   = " & adjustedDemand)
        TraceDebug(stock.ToString, "estimateDemand   = " & estimateDemand)
        TraceDebug(stock.ToString, "actualTrend      = " & actualTrend)
        TraceDebug(stock.ToString, "estimateError    = " & estimateError)
        TraceDebug(stock.ToString, "newTrend         = " & newTrend)
        TraceDebug(stock.ToString, "newDemand        = " & newDemand)
        TraceDebug(stock.ToString, "newErrorSmoothed = " & newErrorSmoothed)
        TraceDebug(stock.ToString, "errorMad         = " & errMAD)
        TraceDebug(stock.ToString, "newErrorForecast = " & newErrorForecast)
        TraceDebug(stock.ToString, "signal           = " & TrackingSignal)

        ' Check tracking signal
        If Math.Abs(TrackingSignal) > Math.Abs(SoqConstants.TrackingSignalLimit) Then
            stock.IsInitialised = False
            TraceDebug(stock.ToString, "Reclassify item")
        Else
            Select Case stock.DemandPattern
                Case DemandPattern.Fast, DemandPattern.TrendDown, DemandPattern.TrendUp
                    If newErrorForecast / newDemand > SoqConstants.StdDevMax Then
                        stock.IsInitialised = False
                        TraceDebug(stock.ToString, "Reclassify item")
                    Else
                        TraceDebug(stock.ToString, "Setting values")
                        stock.PeriodDemand = CDec(newDemand)
                        stock.PeriodTrend = CDec(newTrend)
                        stock.ErrorForecast = CDec(newErrorForecast)
                        stock.ErrorSmoothed = CDec(newErrorSmoothed)
                    End If

                Case DemandPattern.Erratic
                    If newErrorForecast / newDemand < (SoqConstants.StdDevMax - 0.2) Then
                        stock.IsInitialised = False
                        TraceDebug(stock.ToString, "Reclassify item")
                    Else
                        TraceDebug(stock.ToString, "Setting values")
                        stock.PeriodDemand = CDec(newDemand)
                        stock.PeriodTrend = CDec(newTrend)
                        stock.ErrorForecast = CDec(newErrorForecast)
                        stock.ErrorSmoothed = CDec(newErrorSmoothed)
                    End If
            End Select

        End If

    End Sub

    Private Sub ForecastBufferConstant(ByRef stock As Purchasing.StockRow, ByVal FixedLead As Decimal, ByVal FixedReview As Decimal)

        'initialise buffer conversion constant for item based on supplier fixed lead and review time
        TraceDebug(stock.ToString, "Forecasting Buffer")
        Dim q As Double = Math.Max(Math.Max(stock.PeriodDemand * FixedReview, stock.PACK), stock.MinimumQty)
        Dim root As Double = Math.Sqrt(FixedLead + FixedReview)
        Dim sigma As Double = stock.ErrorForecast

        'output trace
        TraceDebug(stock.ToString, "q = " & q)
        TraceDebug(stock.ToString, "root = " & root)
        TraceDebug(stock.ToString, "sigma = " & sigma)

        ' Set bounds for sigma if erratic
        If stock.DemandPattern <> DemandPattern.Erratic Then
            If sigma < stock.PeriodDemand * SoqConstants.StdDevMin Then sigma = stock.PeriodDemand * SoqConstants.StdDevMin
            If sigma > stock.PeriodDemand * SoqConstants.StdDevMax Then sigma = stock.PeriodDemand * SoqConstants.StdDevMax
            TraceDebug(stock.ToString, "sigma (bounded) = " & sigma)
        End If

        'get service level from stock defaulting to system level if zero
        Dim serviceLevel As Decimal = stock.ServiceLevelOverride
        If serviceLevel = 0 Then serviceLevel = SoqConstants.TargetServicePercent(0)

        'check for zero denominator
        Dim z As Double = 0
        Dim ek As Double = 3.1
        If sigma * root <> 0 Then
            z = q / (sigma * root)
            ek = z * (1 - serviceLevel / 100)
        End If

        'set buffer
        stock.BufferConversion = CDec(SoqConstants.ServiceFunction(CDec(ek)) * root)

        'output trace
        TraceDebug(stock.ToString, "z = " & z)
        TraceDebug(stock.ToString, "ek = " & ek)
        TraceDebug(stock.ToString, "buffer = " & stock.BufferConversion)

    End Sub



    Private Function DemandPatternInitialised(ByRef stock As Purchasing.StockRow, ByRef bank As Purchasing.SaleWeightRow, ByRef promo As Purchasing.SaleWeightRow, ByRef season As Purchasing.SaleWeightRow, ByRef soqPeriods As List(Of SoqPeriod)) As Boolean

        TraceDebug(stock.ToString, "Initialising demand pattern")

        'if already classed as new then set values
        If stock.DemandPattern = DemandPattern.[New] Then
            TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            soqPeriods = New List(Of SoqPeriod)

            For index As Integer = 1 To SoqConstants.InitNewPeriods
                soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index), stock.DaysOutStockPeriod(index)))
                TraceDebug(stock.ToString, "Period Used: " & index)
            Next

            stock.PeriodDemand = soqPeriods.AverageSales
            stock.PeriodTrend = 0
            stock.ErrorForecast = stock.PeriodDemand * SoqConstants.StdDevMultiplier(stock.PeriodDemand)
            stock.ErrorSmoothed = 0
            Return True
        End If

        'check if number periods is zero and use all periods if true
        If soqPeriods.Count = 0 Then
            'get rbar as avreage of all periods
            Dim rbar As Decimal = 0
            For index As Integer = 2 To 13
                rbar += stock.UnitSalesPeriod(index)
            Next
            rbar /= 12

            stock.DemandPattern = DemandPattern.Fast
            stock.PeriodDemand = rbar
            TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            Return DemandPatternInitialiseFastErratic(stock, soqPeriods)
        End If


        'get period adjusters for periods
        For Each period As SoqPeriod In soqPeriods
            period.Adjuster = CDec(bank.Pattern(period.WeekNumber) * promo.Pattern(period.WeekNumber) * season.Pattern(period.WeekNumber))
        Next


        'check that not all periods have zero sales
        If soqPeriods.CountZeroSales = soqPeriods.Count Then
            stock.DemandPattern = DemandPattern.Obsolete
            TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            stock.PeriodDemand = 0
            stock.PeriodTrend = 0
            stock.ErrorForecast = 0
            stock.ErrorSmoothed = 0
            Return True
        End If

        'check for fast stock items
        If soqPeriods.Count < 4 Then
            stock.DemandPattern = DemandPattern.Fast
            stock.PeriodDemand = soqPeriods.AverageSales
            TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            Return DemandPatternInitialiseFastErratic(stock, soqPeriods)
        End If

        'check for superceded stock items
        If soqPeriods.CountZeroSalesConsecutive >= SoqConstants.NumPeriodsConsecutiveZero Then
            stock.DemandPattern = DemandPattern.Superceded
            TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            stock.PeriodDemand = 0
            stock.PeriodTrend = 0
            stock.ErrorForecast = 0
            stock.ErrorSmoothed = 0
            Return True
        End If

        'check for lumpy/slow items
        If soqPeriods.CountZeroSales >= SoqConstants.ZeroDemandLumpyCheck(soqPeriods.Count) Then
            If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                stock.DemandPattern = DemandPattern.Slow
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            Else
                stock.DemandPattern = DemandPattern.Lumpy
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            End If

            'get last Soq.NumPeriodsAveSlowLumpy periods
            soqPeriods = New List(Of SoqPeriod)
            For index As Integer = 1 To SoqConstants.NumPeriodsAveSlowLumpy
                soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index), stock.DaysOutStockPeriod(index)))
                TraceDebug(stock.ToString, "Period Used: " & index)
            Next

            'get period adjusters for periods
            For Each period As SoqPeriod In soqPeriods
                period.Adjuster = CDec(bank.Pattern(period.WeekNumber) * promo.Pattern(period.WeekNumber) * season.Pattern(period.WeekNumber))
            Next

            stock.PeriodDemand = soqPeriods.AverageSalesAdjusted
            stock.PeriodTrend = 0
            stock.ErrorForecast = 0
            stock.ErrorSmoothed = 0
            Return True
        End If

        Return DemandPatternInitialiseNormal(stock, soqPeriods)

    End Function

    Private Function DemandPatternInitialiseNormal(ByRef stock As Purchasing.StockRow, ByRef soqPeriods As List(Of SoqPeriod)) As Boolean

        'calculate required values
        Dim sdSales As Decimal = soqPeriods.StdDevSales
        Dim sdPeriods As Decimal = soqPeriods.StdDevPeriods
        Dim correlation As Decimal = soqPeriods.CorrelationCoeff(sdSales, sdPeriods)
        Dim trendSig As Decimal = SoqConstants.TrendSignificance(soqPeriods.Count)

        'output trace
        TraceDebug(stock.ToString, "Initialising demand pattern - normal")
        TraceDebug(stock.ToString, "Standard dev sales (sigmaR) = " & sdSales)
        TraceDebug(stock.ToString, "Standard dev periods (sigmaP) = " & sdPeriods)
        TraceDebug(stock.ToString, "Correlation (r) = " & correlation)
        TraceDebug(stock.ToString, "Soq.TrendSignificance(" & soqPeriods.Count & ") = " & trendSig)

        'test for significance of trend
        If Math.Abs(correlation) > trendSig Then
            Return DemandPatternInitialiseTrend(stock, soqPeriods)
        Else
            Return DemandPatternInitialiseFastErratic(stock, soqPeriods)
        End If

    End Function

    Private Function DemandPatternInitialiseFastErratic(ByRef stock As Purchasing.StockRow, ByRef soqPeriods As List(Of SoqPeriod)) As Boolean

        Dim adjustedAverage As Decimal = 0
        Dim sdSales As Decimal = 0
        Dim sdPeriods As Decimal = 0

        Select Case soqPeriods.Count
            Case Is < 4
                TraceDebug(stock.ToString, "Initialising demand pattern - already set to fast")
                adjustedAverage = stock.PeriodDemand
                sdSales = stock.PeriodDemand * SoqConstants.StdDevMultiplier(stock.PeriodDemand)

            Case Else
                TraceDebug(stock.ToString, "Initialising demand pattern - fast erratic")
                adjustedAverage = soqPeriods.AverageSalesAdjusted
                sdSales = soqPeriods.StdDevSales
                sdPeriods = soqPeriods.StdDevPeriods
        End Select

        'output trace
        TraceDebug(stock.ToString, "Soq adjusted average (Rbar) = " & adjustedAverage)
        TraceDebug(stock.ToString, "Standard dev sales (sigmaR) = " & sdSales)
        TraceDebug(stock.ToString, "Standard dev periods (sigmaP) = " & sdPeriods)


        'perform flier elimination process if more equal to 4 periods
        If soqPeriods.Count >= 4 Then
            Dim lowLimit As Decimal = adjustedAverage - (SoqConstants.FlierEliminator(soqPeriods.Count) * sdSales)
            Dim highLimit As Decimal = adjustedAverage + (SoqConstants.FlierEliminator(soqPeriods.Count) * sdSales)
            TraceDebug(stock.ToString, "Performing flier elimination")
            TraceDebug(stock.ToString, "Flier eliminator low limit = " & lowLimit)
            TraceDebug(stock.ToString, "Flier eliminator high limit = " & highLimit)

            For Each period As SoqPeriod In soqPeriods
                If period.Sales < lowLimit OrElse period.Sales > highLimit Then
                    TraceDebug(stock.ToString, "Flier found at period " & period.Index)
                    stock.FlierPeriod(period.Index) = "F"
                    Return False
                End If
            Next

            TraceDebug(stock.ToString, "Flier elimination - none found")

            If adjustedAverage <> 0 AndAlso (sdSales / adjustedAverage) < SoqConstants.ErracticLimitVariance Then
                stock.DemandPattern = DemandPattern.Fast
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            Else
                stock.DemandPattern = DemandPattern.Erratic
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            End If
        End If

        'if demand pattern not set it now
        If stock.DemandPattern = DemandPattern.None Then

        End If

        'set initial parameters in table
        stock.PeriodDemand = adjustedAverage
        stock.PeriodTrend = 0
        stock.ErrorForecast = Math.Max(sdSales, adjustedAverage * SoqConstants.StdDevMin)
        stock.ErrorSmoothed = 0
        Return True

    End Function

    Private Function DemandPatternInitialiseTrend(ByRef stock As Purchasing.StockRow, ByRef soqPeriods As List(Of SoqPeriod)) As Boolean

        'calculate required values
        Dim sdSales As Decimal = soqPeriods.StdDevSales
        Dim sdPeriods As Decimal = soqPeriods.StdDevPeriods
        Dim correlation As Decimal = soqPeriods.CorrelationCoeff(sdSales, sdPeriods)
        Dim adjustedAverage As Decimal = soqPeriods.AverageSalesAdjusted

        'output trace
        TraceDebug(stock.ToString, "Initialising demand pattern - trend")
        TraceDebug(stock.ToString, "Standard dev sales (sigmaR)= " & sdSales)
        TraceDebug(stock.ToString, "Standard dev periods (sigmaP)= " & sdPeriods)
        TraceDebug(stock.ToString, "Soq correlation (r)= " & correlation)
        TraceDebug(stock.ToString, "Soq adjusted average (Rbar)= " & adjustedAverage)

        'set trend up values
        Dim b As Decimal = correlation * sdSales / sdPeriods
        stock.DemandPattern = DemandPattern.TrendUp
        stock.PeriodDemand = CDec(adjustedAverage + (soqPeriods.Count - soqPeriods.Sum(Function(p As SoqPeriod) p.Index) / soqPeriods.Count) * b)
        stock.PeriodTrend = b
        stock.ErrorForecast = CDec(sdSales * Math.Sqrt((1 - Math.Pow(correlation, 2)) * (soqPeriods.Count - 1) / (soqPeriods.Count - 2)))
        stock.ErrorSmoothed = 0

        'limit error forecast if too low
        If stock.ErrorForecast < (adjustedAverage * SoqConstants.StdDevMin) Then
            stock.ErrorForecast = adjustedAverage * SoqConstants.StdDevMin
        End If

        'if negative then set trend down
        If correlation > 0 Then
            stock.DemandPattern = DemandPattern.TrendDown

            'if trend down is allowed then check for trend down using last 4 periods
            If SoqConstants.UseNegativeTrend Then
                soqPeriods = New List(Of SoqPeriod)
                For index As Integer = 2 To 5
                    soqPeriods.Add(New SoqPeriod(index, stock.UnitSalesPeriod(index), stock.FlierPeriod(index), stock.DaysOutStockPeriod(index)))
                Next

                stock.PeriodDemand = soqPeriods.AverageSales
                stock.PeriodTrend = 0
                stock.ErrorForecast = stock.PeriodDemand * SoqConstants.StdDevMultiplier(stock.PeriodDemand)
                stock.ErrorSmoothed = 0
            End If
        End If

        TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
        Return True

    End Function




    Private Sub TraceDebug(ByVal section1 As String, Optional ByVal section2 As String = "")

        Dim sb As New StringBuilder(section1)
        If section2.Length > 0 Then sb.Append(" - " & section2)
        Trace.WriteLine(sb.ToString, Me.GetType.ToString)

    End Sub

    Public Enum CompletedStatus
        Successful
        Cancelled
        NoStocks
        [Error]
        Deleted
    End Enum

    Public Structure DemandPattern
        Private _value As String
        Public Shared ReadOnly None As String = ""
        Public Shared ReadOnly Obsolete As String = "Obsolete"
        Public Shared ReadOnly Superceded As String = "Superceded"
        Public Shared ReadOnly [New] As String = "New"
        Public Shared ReadOnly Normal As String = "Normal"
        Public Shared ReadOnly Erratic As String = "Erratic"
        Public Shared ReadOnly Fast As String = "Fast"
        Public Shared ReadOnly Slow As String = "Slow"
        Public Shared ReadOnly Lumpy As String = "Lumpy"
        Public Shared ReadOnly TrendUp As String = "TrendUp"
        Public Shared ReadOnly TrendDown As String = "TrendDown"
    End Structure

    Public Structure ForecastIndicator
        Private _value As String
        Public Shared ReadOnly Yes As Char = "Y"c
        Public Shared ReadOnly No As Char = "N"c
        Public Shared ReadOnly Reclassify As Char = "R"c
    End Structure

End Class