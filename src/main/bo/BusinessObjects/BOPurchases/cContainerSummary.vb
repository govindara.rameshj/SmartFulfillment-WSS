﻿<Serializable()> Public Class cContainerSummary
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CONSUM"
        BOFields.Add(_AssemblyDepotNumber)
        BOFields.Add(_Number)
        BOFields.Add(_DespatchDate)
        BOFields.Add(_Description)
        BOFields.Add(_DespatchDepotNumber)
        BOFields.Add(_VehicleLoadReference)
        BOFields.Add(_TotalLines)
        BOFields.Add(_ParentNumber)
        BOFields.Add(_Received)
        BOFields.Add(_PrintRequired)
        BOFields.Add(_DeliveryDate)
        BOFields.Add(_Value)
        BOFields.Add(_AutomaticallyPrinted)
        BOFields.Add(_RejectedReason)
        BOFields.Add(_IsLastProcessed)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cContainerSummary)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cContainerSummary)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cContainerSummary))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cContainerSummary(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _AssemblyDepotNumber As New ColField(Of String)("ADEP", "", "Assembly Depot Number", True, False)
    Private _Number As New ColField(Of String)("CNUM", "", "Number", True, False)
    Private _DespatchDate As New ColField(Of Date)("DDAT", Nothing, "Despatch Date", False, False)
    Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    Private _DespatchDepotNumber As New ColField(Of String)("DDEP", "", "Despatch Depot Number", False, False)
    Private _VehicleLoadReference As New ColField(Of String)("VREF", "", "Vehicle Load Reference", False, False)
    Private _TotalLines As New ColField(Of Decimal)("TOTL", 0, "Total Lines", False, False)
    Private _ParentNumber As New ColField(Of String)("PNUM", "", "Parent Number", False, False)
    Private _Received As New ColField(Of Boolean)("IREC", False, "Received", False, False)
    Private _PrintRequired As New ColField(Of Boolean)("IPRT", False, "Print Required", False, False)
    Private _DeliveryDate As New ColField(Of Date)("DELD", Nothing, "Delivery Date", False, False)
    Private _Value As New ColField(Of Decimal)("VALU", 0, "Value", False, False, 2)
    Private _AutomaticallyPrinted As New ColField(Of Boolean)("APRT", False, "Automatically Printed", False, False)
    Private _RejectedReason As New ColField(Of String)("REJI", "", "Rejected Reason", False, False)
    Private _IsLastProcessed As New ColField(Of Boolean)("IsLastProcessed", False, "Is Last Processed", False, False)

#End Region

#Region "Field Properties"

    Public Property AssemblyDepotNumber() As ColField(Of String)
        Get
            AssemblyDepotNumber = _AssemblyDepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _AssemblyDepotNumber = value
        End Set
    End Property
    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property DespatchDate() As ColField(Of Date)
        Get
            DespatchDate = _DespatchDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DespatchDate = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property DespatchDepotNumber() As ColField(Of String)
        Get
            DespatchDepotNumber = _DespatchDepotNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DespatchDepotNumber = value
        End Set
    End Property
    Public Property VehicleLoadReference() As ColField(Of String)
        Get
            VehicleLoadReference = _VehicleLoadReference
        End Get
        Set(ByVal value As ColField(Of String))
            _VehicleLoadReference = value
        End Set
    End Property
    Public Property TotalLines() As ColField(Of Decimal)
        Get
            TotalLines = _TotalLines
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _TotalLines = value
        End Set
    End Property
    Public Property ParentNumber() As ColField(Of String)
        Get
            ParentNumber = _ParentNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ParentNumber = value
        End Set
    End Property
    Public Property Received() As ColField(Of Boolean)
        Get
            Received = _Received
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Received = value
        End Set
    End Property
    Public Property PrintRequired() As ColField(Of Boolean)
        Get
            PrintRequired = _PrintRequired
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintRequired = value
        End Set
    End Property
    Public Property DeliveryDate() As ColField(Of Date)
        Get
            DeliveryDate = _DeliveryDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DeliveryDate = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Value = _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property AutomaticallyPrinted() As ColField(Of Boolean)
        Get
            AutomaticallyPrinted = _AutomaticallyPrinted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AutomaticallyPrinted = value
        End Set
    End Property
    Public Property RejectedReason() As ColField(Of String)
        Get
            RejectedReason = _RejectedReason
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectedReason = value
        End Set
    End Property
    Public Property IsLastProcessed() As ColField(Of Boolean)
        Get
            IsLastProcessed = _IsLastProcessed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsLastProcessed = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Headers As List(Of cContainerSummary) = Nothing
    Private _Details As List(Of cContainerDetail) = Nothing

    Public Property Headers() As List(Of cContainerSummary)
        Get
            If _Headers Is Nothing Then
                _Headers = LoadMatches()
            End If
            Return _Headers
        End Get
        Set(ByVal value As List(Of cContainerSummary))
            _Headers = value
        End Set
    End Property
    Public Property Header(ByVal AssemblyNumber As String, ByVal Number As String) As cContainerSummary
        Get
            For Each head As cContainerSummary In Headers
                If head.AssemblyDepotNumber.Value = AssemblyNumber And head.Number.Value = Number Then Return head
            Next
            Return Nothing
        End Get
        Set(ByVal value As cContainerSummary)
            For Each head As cContainerSummary In Headers
                If head.AssemblyDepotNumber.Value = AssemblyNumber And head.Number.Value = Number Then head = value
            Next
        End Set
    End Property

    Public Property Details(Optional ByVal LoadStocks As Boolean = False) As List(Of cContainerDetail)
        Get
            If _Details Is Nothing Then
                Dim det As New cContainerDetail(Oasys3DB)
                det.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, det.AssemblyDepotNumber, _AssemblyDepotNumber.Value)
                det.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                det.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, det.ContainerNumber, _Number.Value)
                _Details = det.LoadMatches
            End If

            If _Details.Count > 0 And LoadStocks Then
                'get stocks for details and load into stock object
                Dim skuNumbers As New ArrayList
                For Each det As cContainerDetail In _Details
                    If Not skuNumbers.Contains(det.SkuNumber.Value) Then skuNumbers.Add(det.SkuNumber.Value)
                Next

                Dim stocks As New BOStock.cStock(Oasys3DB)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
                stocks.Stocks = stocks.LoadMatches

                For Each det As cContainerDetail In _Details
                    For Each stock As BOStock.cStock In stocks.Stocks
                        If det.SkuNumber.Value = stock.SkuNumber.Value Then
                            det.Stock = stock
                            Exit For
                        End If
                    Next
                Next
            End If

            Return _Details
        End Get
        Set(ByVal value As List(Of cContainerDetail))
            _Details = value
        End Set
    End Property
    Public Property Detail(ByVal PoNumber As String, ByVal LineNumber As String) As cContainerDetail
        Get
            For Each det As cContainerDetail In Details
                If det.StorePoNumber.Value = PoNumber And det.StorePoLineNumber.Value = LineNumber Then Return det
            Next
            Return Nothing
        End Get
        Set(ByVal value As cContainerDetail)
            For Each det As cContainerDetail In Details
                If det.StorePoNumber.Value = PoNumber And det.StorePoLineNumber.Value = LineNumber Then det = value
            Next
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function RejectionString() As String

        Select Case _RejectedReason.Value.Trim
            Case "A" : Return "A - Invalid Purchase Order record"
            Case "B" : Return "B - Purchase Order record not on file"
            Case "C" : Return "C - Purchase Order already received"
            Case "D" : Return "D - Purchase Order deleted"
            Case "E" : Return "E - SOQ number does not match"
            Case "F" : Return "F - Invalid Container record"
            Case "G" : Return "G - Container record not on file"
            Case "H" : Return "H  -New issue number less than previous"
            Case "I" : Return "I - Item not on file"
            Case "J" : Return "J - Item not in Purchase Line"
            Case "K" : Return "K - Purchase Line record deleted"
            Case "L" : Return "L - Issue quantity not equal to Purchase quantity"
            Case "M" : Return "M - Order qty left not equal to follow"
            Case "N" : Return "N - Supplier record not on file"
                ' "O" is actually used
            Case "P" : Return "P - SKU not set up on system"
            Case Else : Return String.Empty
        End Select

    End Function

#End Region

End Class
