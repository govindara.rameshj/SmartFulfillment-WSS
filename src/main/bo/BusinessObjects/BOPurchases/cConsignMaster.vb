﻿<Serializable()> Public Class cConsignMaster
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CONMAS"
        BOFields.Add(_Number)
        BOFields.Add(_PONumber)
        BOFields.Add(_POReleaseNumber)
        BOFields.Add(_SupplierNumber)
        BOFields.Add(_DateConsigned)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_IBTIndicator)
        BOFields.Add(_IBTStoreNumber)
        BOFields.Add(_BBCIssueNumber)
        BOFields.Add(_Done)
        BOFields.Add(_DeliveryNote1)
        BOFields.Add(_DeliveryNote2)
        BOFields.Add(_DeliveryNote3)
        BOFields.Add(_DeliveryNote4)
        BOFields.Add(_DeliveryNote5)
        BOFields.Add(_DeliveryNote6)
        BOFields.Add(_DeliveryNote7)
        BOFields.Add(_DeliveryNote8)
        BOFields.Add(_DeliveryNote9)
        BOFields.Add(_PalletsReceived)
        BOFields.Add(_PalletsReturned)
        BOFields.Add(_PrintCopies)
        BOFields.Add(_PrintCopyInit1)
        BOFields.Add(_PrintCopyInit2)
        BOFields.Add(_PrintCopyInit3)
        BOFields.Add(_PrintCopyInit4)
        BOFields.Add(_PrintCopyInit5)
        BOFields.Add(_PrintCopyInit6)
        BOFields.Add(_PrintCopyTime1)
        BOFields.Add(_PrintCopyTime2)
        BOFields.Add(_PrintCopyTime3)
        BOFields.Add(_PrintCopyTime4)
        BOFields.Add(_PrintCopyTime5)
        BOFields.Add(_PrintCopyTime6)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cConsignMaster)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cConsignMaster)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cConsignMaster))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cConsignMaster(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Function Save(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType) As Boolean

        Try
            Select Case eSave
                Case clsOasys3DB.eSqlQueryType.pInsert
                    'get next consigment number
                    Dim SysNumbers As New BOSystem.cSystemNumbers(Oasys3DB)
                    _Number.Value = SysNumbers.GetConsignmentNumber.ToString.PadLeft(6, "0"c)
                    _PrintCopyInit1.Value = "FIRST"

                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, eSave)
                    SetDBFieldValues(clsOasys3DB.eSqlQueryType.pInsert)

                    If Oasys3DB.Insert <= 0 Then Return False
                    GetDBIdentity()

                Case clsOasys3DB.eSqlQueryType.pUpdate
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, eSave)
                    SetDBFieldValues(clsOasys3DB.eSqlQueryType.pUpdate)
                    SetDBKeys()

                    If Oasys3DB.Update <= 0 Then Return False

                Case Else : Return False
            End Select

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

#Region "Fields"

    Private _Number As New ColField(Of String)("NUMB", "000000", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Number")
    Private _PONumber As New ColField(Of String)("PONO", "000000", False, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "PO Number")
    Private _POReleaseNumber As New ColField(Of Decimal)("PREL", 0, False, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Po Release Number")
    Private _SupplierNumber As New ColField(Of String)("SUPP", "00000", False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Supplier Number")
    Private _DateConsigned As New ColField(Of Date)("EDAT", CDate("1900/01/01"), False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Date Consigned")
    Private _EmployeeID As New ColField(Of String)("EEID", "000", False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Employee ID")
    Private _IBTIndicator As New ColField(Of Boolean)("IBTI", False, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "IBT Indicator")
    Private _IBTStoreNumber As New ColField(Of String)("IBTS", "000", False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "IBT Store Number")
    Private _BBCIssueNumber As New ColField(Of String)("BBCI", "000000", False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "BBC Issue Number")
    Private _Done As New ColField(Of Boolean)("DONE", False, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Done")
    Private _DeliveryNote1 As New ColField(Of String)("DNOT1", "", False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Delivery Number 1")
    Private _DeliveryNote2 As New ColField(Of String)("DNOT2", "", False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "Delivery Number 2")
    Private _DeliveryNote3 As New ColField(Of String)("DNOT3", "", False, False, 1, 0, True, True, 13, 0, 0, 0, "", 0, "Delivery Number 3")
    Private _DeliveryNote4 As New ColField(Of String)("DNOT4", "", False, False, 1, 0, True, True, 14, 0, 0, 0, "", 0, "Delivery Number 4")
    Private _DeliveryNote5 As New ColField(Of String)("DNOT5", "", False, False, 1, 0, True, True, 15, 0, 0, 0, "", 0, "Delivery Number 5")
    Private _DeliveryNote6 As New ColField(Of String)("DNOT6", "", False, False, 1, 0, True, True, 16, 0, 0, 0, "", 0, "Delivery Number 6")
    Private _DeliveryNote7 As New ColField(Of String)("DNOT7", "", False, False, 1, 0, True, True, 17, 0, 0, 0, "", 0, "Delivery Number 7")
    Private _DeliveryNote8 As New ColField(Of String)("DNOT8", "", False, False, 1, 0, True, True, 18, 0, 0, 0, "", 0, "Delivery Number 8")
    Private _DeliveryNote9 As New ColField(Of String)("DNOT9", "", False, False, 1, 0, True, True, 19, 0, 0, 0, "", 0, "Delivery Number 9")
    Private _PalletsReceived As New ColField(Of Decimal)("RCVD", 0, False, False, 1, 0, True, True, 20, 0, 0, 0, "", 0, "Pallets Received")
    Private _PalletsReturned As New ColField(Of Decimal)("RTND", 0, False, False, 1, 0, True, True, 21, 0, 0, 0, "", 0, "Pallets Received")
    Private _PrintCopies As New ColField(Of Decimal)("CNUM", 0, False, False, 1, 0, True, True, 22, 0, 0, 0, "", 0, "Print Copies")
    Private _PrintCopyInit1 As New ColField(Of String)("INIT1", "", False, False, 1, 0, True, True, 23, 0, 0, 0, "", 0, "Print Copy Init 1")
    Private _PrintCopyInit2 As New ColField(Of String)("INIT2", "", False, False, 1, 0, True, True, 24, 0, 0, 0, "", 0, "Print Copy Init 2")
    Private _PrintCopyInit3 As New ColField(Of String)("INIT3", "", False, False, 1, 0, True, True, 25, 0, 0, 0, "", 0, "Print Copy Init 3")
    Private _PrintCopyInit4 As New ColField(Of String)("INIT4", "", False, False, 1, 0, True, True, 26, 0, 0, 0, "", 0, "Print Copy Init 4")
    Private _PrintCopyInit5 As New ColField(Of String)("INIT5", "", False, False, 1, 0, True, True, 27, 0, 0, 0, "", 0, "Print Copy Init 5")
    Private _PrintCopyInit6 As New ColField(Of String)("INIT6", "", False, False, 1, 0, True, True, 28, 0, 0, 0, "", 0, "Print Copy Init 6")
    Private _PrintCopyTime1 As New ColField(Of String)("TIME1", "0000", False, False, 1, 0, True, True, 29, 0, 0, 0, "", 0, "Print Copy Time 1")
    Private _PrintCopyTime2 As New ColField(Of String)("TIME2", "0000", False, False, 1, 0, True, True, 30, 0, 0, 0, "", 0, "Print Copy Time 2")
    Private _PrintCopyTime3 As New ColField(Of String)("TIME3", "0000", False, False, 1, 0, True, True, 31, 0, 0, 0, "", 0, "Print Copy Time 3")
    Private _PrintCopyTime4 As New ColField(Of String)("TIME4", "0000", False, False, 1, 0, True, True, 32, 0, 0, 0, "", 0, "Print Copy Time 4")
    Private _PrintCopyTime5 As New ColField(Of String)("TIME5", "0000", False, False, 1, 0, True, True, 33, 0, 0, 0, "", 0, "Print Copy Time 5")
    Private _PrintCopyTime6 As New ColField(Of String)("TIME6", "0000", False, False, 1, 0, True, True, 34, 0, 0, 0, "", 0, "Print Copy Time 6")


#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property PONumber() As ColField(Of String)
        Get
            PONumber = _PONumber
        End Get
        Set(ByVal value As ColField(Of String))
            _PONumber = value
        End Set
    End Property
    Public Property POReleaseNumber() As ColField(Of Decimal)
        Get
            POReleaseNumber = _POReleaseNumber
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _POReleaseNumber = value
        End Set
    End Property
    Public Property SupplierNumber() As ColField(Of String)
        Get
            SupplierNumber = _SupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierNumber = value
        End Set
    End Property
    Public Property DateConsigned() As ColField(Of Date)
        Get
            DateConsigned = _DateConsigned
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateConsigned = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)
        Get
            EmployeeID = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property IBTIndicator() As ColField(Of Boolean)
        Get
            IBTIndicator = _IBTIndicator
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IBTIndicator = value
        End Set
    End Property
    Public Property IBTStoreNumber() As ColField(Of String)
        Get
            IBTStoreNumber = _IBTStoreNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _IBTStoreNumber = value
        End Set
    End Property
    Public Property BBCIssueNumber() As ColField(Of String)
        Get
            BBCIssueNumber = _BBCIssueNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _BBCIssueNumber = value
        End Set
    End Property
    Public Property Done() As ColField(Of Boolean)
        Get
            Done = _Done
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Done = value
        End Set
    End Property

    Public Property DeliveryNote(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = DeliveryNote1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = DeliveryNote1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property DeliveryNote1() As ColField(Of String)
        Get
            DeliveryNote1 = _DeliveryNote1
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote1 = value
        End Set
    End Property
    Public Property DeliveryNote2() As ColField(Of String)
        Get
            DeliveryNote2 = _DeliveryNote2
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote2 = value
        End Set
    End Property
    Public Property DeliveryNote3() As ColField(Of String)
        Get
            DeliveryNote3 = _DeliveryNote3
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote3 = value
        End Set
    End Property
    Public Property DeliveryNote4() As ColField(Of String)
        Get
            DeliveryNote4 = _DeliveryNote4
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote4 = value
        End Set
    End Property
    Public Property DeliveryNote5() As ColField(Of String)
        Get
            DeliveryNote5 = _DeliveryNote5
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote5 = value
        End Set
    End Property
    Public Property DeliveryNote6() As ColField(Of String)
        Get
            DeliveryNote6 = _DeliveryNote6
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote6 = value
        End Set
    End Property
    Public Property DeliveryNote7() As ColField(Of String)
        Get
            DeliveryNote7 = _DeliveryNote7
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote7 = value
        End Set
    End Property
    Public Property DeliveryNote8() As ColField(Of String)
        Get
            DeliveryNote8 = _DeliveryNote8
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote8 = value
        End Set
    End Property
    Public Property DeliveryNote9() As ColField(Of String)
        Get
            DeliveryNote9 = _DeliveryNote9
        End Get
        Set(ByVal value As ColField(Of String))
            _DeliveryNote9 = value
        End Set
    End Property
    Public Property PalletsReceived() As ColField(Of Decimal)
        Get
            PalletsReceived = _PalletsReceived
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PalletsReceived = value
        End Set
    End Property
    Public Property PalletsReturned() As ColField(Of Decimal)
        Get
            PalletsReturned = _PalletsReturned
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PalletsReturned = value
        End Set
    End Property
    Public Property PrintCopies() As ColField(Of Decimal)
        Get
            PrintCopies = _PrintCopies
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PrintCopies = value
        End Set
    End Property

    Public Property PrintCopyInit(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = PrintCopyInit1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = PrintCopyInit1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property PrintCopyInit1() As ColField(Of String)
        Get
            PrintCopyInit1 = _PrintCopyInit1
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit1 = value
        End Set
    End Property
    Public Property PrintCopyInit2() As ColField(Of String)
        Get
            PrintCopyInit2 = _PrintCopyInit2
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit2 = value
        End Set
    End Property
    Public Property PrintCopyInit3() As ColField(Of String)
        Get
            PrintCopyInit3 = _PrintCopyInit3
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit3 = value
        End Set
    End Property
    Public Property PrintCopyInit4() As ColField(Of String)
        Get
            PrintCopyInit4 = _PrintCopyInit4
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit4 = value
        End Set
    End Property
    Public Property PrintCopyInit5() As ColField(Of String)
        Get
            PrintCopyInit5 = _PrintCopyInit5
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit5 = value
        End Set
    End Property
    Public Property PrintCopyInit6() As ColField(Of String)
        Get
            PrintCopyInit6 = _PrintCopyInit6
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyInit6 = value
        End Set
    End Property

    Public Property PrintCopyTime(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = PrintCopyTime1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = PrintCopyTime1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString
            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property PrintCopyTime1() As ColField(Of String)
        Get
            PrintCopyTime1 = _PrintCopyTime1
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime1 = value
        End Set
    End Property
    Public Property PrintCopyTime2() As ColField(Of String)
        Get
            PrintCopyTime2 = _PrintCopyTime2
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime2 = value
        End Set
    End Property
    Public Property PrintCopyTime3() As ColField(Of String)
        Get
            PrintCopyTime3 = _PrintCopyTime3
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime3 = value
        End Set
    End Property
    Public Property PrintCopyTime4() As ColField(Of String)
        Get
            PrintCopyTime4 = _PrintCopyTime4
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime4 = value
        End Set
    End Property
    Public Property PrintCopyTime5() As ColField(Of String)
        Get
            PrintCopyTime5 = _PrintCopyTime5
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime5 = value
        End Set
    End Property
    Public Property PrintCopyTime6() As ColField(Of String)
        Get
            PrintCopyTime6 = _PrintCopyTime6
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintCopyTime6 = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function UpdateDone() As Boolean

        Try
            _Done.Value = True

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_Done.ColumnName, _Done.Value)
            SetDBKeys()
            Oasys3DB.Update()
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function setDone(ByVal consignmentno As String, ByVal Selectedno As Integer) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_Done.ColumnName, True)
        Oasys3DB.SetWhereParameter(_PONumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, Selectedno.ToString("#####0").PadLeft(6, "0"c))
        Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Number.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, consignmentno)
        NoRecs = Oasys3DB.Update()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

    End Function

#End Region


End Class
