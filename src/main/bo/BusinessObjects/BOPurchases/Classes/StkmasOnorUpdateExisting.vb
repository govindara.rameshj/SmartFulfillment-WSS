﻿Public Class StkmasOnorUpdateExisting
    Implements IStkmasOnorUpdate

    Public Function GetOnOrderValue(ByRef conn As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal isulin As cIssueLine) As Decimal Implements IStkmasOnorUpdate.GetOnOrderValue
        Return isulin.QtyOrdered.Value
    End Function
End Class
