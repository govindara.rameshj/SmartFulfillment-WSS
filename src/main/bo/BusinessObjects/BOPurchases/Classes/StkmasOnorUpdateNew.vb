﻿Public Class StkmasOnorUpdateNew
    Implements IStkmasOnorUpdate

    Private repositoryInstance As New StkmasOnorRepository

    Public Overridable Function GetOnOrderValue(ByRef conn As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal isulin As cIssueLine) As Decimal Implements IStkmasOnorUpdate.GetOnOrderValue
        Dim dt As DataTable
        dt = repositoryInstance.GetPurlinRecord(conn, isulin.SkuNumber.Value, isulin.IssueNumber.Value)
        If Not dt Is Nothing Then
            Return CDec(dt.Rows(0)(0))
        End If
        Return Nothing
    End Function
End Class
