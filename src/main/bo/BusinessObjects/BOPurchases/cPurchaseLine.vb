﻿<Serializable()> Public Class cPurchaseLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PURLIN"
        BOFields.Add(_Id)
        BOFields.Add(_HeaderId)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_KeyedDescription)
        BOFields.Add(_SkuProductCode)
        BOFields.Add(_OrderQty)
        BOFields.Add(_OrderPrice)
        BOFields.Add(_OrderCost)
        BOFields.Add(_DateLastOrder)
        BOFields.Add(_ReceivedQty)
        BOFields.Add(_DateLastReceipt)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_ReceiptQty)
        BOFields.Add(_BackOrderQty)
        BOFields.Add(_ShipperNumber)
        BOFields.Add(_Confirmation)
        BOFields.Add(_ReasonCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPurchaseLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPurchaseLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPurchaseLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPurchaseLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _Id As New ColField(Of Integer)("TKEY", 0, "ID", True, True)
    Private _HeaderId As New ColField(Of Integer)("HKEY", 0, "Header ID", False, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "SKU Number", False, False)
    Private _KeyedDescription As New ColField(Of String)("KDES", "", "Keyed Description", False, False)
    Private _SkuProductCode As New ColField(Of String)("PROD", "", "SKU Product Code", False, False)
    Private _OrderQty As New ColField(Of Integer)("QTYO", 0, "Order Qty", False, False)
    Private _OrderPrice As New ColField(Of Decimal)("PRIC", 0, "Order Price", False, False, 2)
    Private _OrderCost As New ColField(Of Decimal)("COST", 0, "Order Cost", False, False, 2)
    Private _DateLastOrder As New ColField(Of Date)("DLOR", Nothing, "Date Last Order", False, False)
    Private _ReceivedQty As New ColField(Of Integer)("RQTY", 0, "Received Qty", False, False)
    Private _DateLastReceipt As New ColField(Of Date)("RDAT", Nothing, "Date Last Receipt", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELE", False, "Is Deleted", False, False)
    Private _ReceiptQty As New ColField(Of Integer)("WRQT", 0, "Receipt Qty", False, False)
    Private _BackOrderQty As New ColField(Of Integer)("WQTB", 0, "Back Order Qty", False, False)
    Private _ShipperNumber As New ColField(Of String)("WSHI", "", "Shipper Number", False, False)
    Private _Confirmation As New ColField(Of String)("CONF", "", "Confirmation", False, False)
    Private _ReasonCode As New ColField(Of Integer)("REAS", 0, "Reason Code", False, False)

#End Region

#Region "Field Properties"

    Public Property PurchaseLineID() As ColField(Of Integer)
        Get
            PurchaseLineID = _Id
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Id = value
        End Set
    End Property
    Public Property HeaderIdentity() As ColField(Of Integer)
        Get
            HeaderIdentity = _HeaderId
        End Get
        Set(ByVal value As ColField(Of Integer))
            _HeaderId = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property KeyedDescription() As ColField(Of String)
        Get
            KeyedDescription = _KeyedDescription
        End Get
        Set(ByVal value As ColField(Of String))
            _KeyedDescription = value
        End Set
    End Property
    Public Property SKUProductCode() As ColField(Of String)
        Get
            SKUProductCode = _SkuProductCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuProductCode = value
        End Set
    End Property
    Public Property OrderQty() As ColField(Of Integer)
        Get
            OrderQty = _OrderQty
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderQty = value
        End Set
    End Property
    Public Property OrderPrice() As ColField(Of Decimal)
        Get
            OrderPrice = _OrderPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderPrice = value
        End Set
    End Property
    Public Property OrderCost() As ColField(Of Decimal)
        Get
            OrderCost = _OrderCost
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderCost = value
        End Set
    End Property
    Public Property DateLastOrder() As ColField(Of Date)
        Get
            DateLastOrder = _DateLastOrder
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastOrder = value
        End Set
    End Property
    Public Property ReceivedQty() As ColField(Of Integer)
        Get
            ReceivedQty = _ReceivedQty
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReceivedQty = value
        End Set
    End Property
    Public Property DateLastReceipt() As ColField(Of Date)
        Get
            DateLastReceipt = _DateLastReceipt
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateLastReceipt = value
        End Set
    End Property
    Public Property DeleteFlag() As ColField(Of Boolean)
        Get
            DeleteFlag = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property ReceiptQty() As ColField(Of Integer)
        Get
            ReceiptQty = _ReceiptQty
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReceiptQty = value
        End Set
    End Property
    Public Property BackOrderQty() As ColField(Of Integer)
        Get
            BackOrderQty = _BackOrderQty
        End Get
        Set(ByVal value As ColField(Of Integer))
            _BackOrderQty = value
        End Set
    End Property
    Public Property ShipperNumber() As ColField(Of String)
        Get
            ShipperNumber = _ShipperNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ShipperNumber = value
        End Set
    End Property
    Public Property Confirmation() As ColField(Of String)
        Get
            Confirmation = _Confirmation
        End Get
        Set(ByVal value As ColField(Of String))
            _Confirmation = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of Integer)
        Get
            ReasonCode = _ReasonCode
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReasonCode = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _Stock As BOStock.cStock = Nothing
    Private _StockLogKey As String = String.Empty

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property

    Public Function UpdateReceived(ByVal UserID As Integer) As Boolean

        Try
            _DateLastReceipt.Value = Now
            _IsDeleted.Value = True
            _Confirmation.Value = "C"

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_ReceivedQty.ColumnName, _ReceivedQty.Value)
            Oasys3DB.SetColumnAndValueParameter(_Confirmation.ColumnName, _Confirmation.Value)
            Oasys3DB.SetColumnAndValueParameter(_DateLastReceipt.ColumnName, _DateLastReceipt.Value)
            Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, _IsDeleted.Value)
            SetDBKeys()
            Oasys3DB.Update()

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

#End Region

End Class
