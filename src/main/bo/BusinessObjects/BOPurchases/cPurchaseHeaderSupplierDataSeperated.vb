﻿Public Class cPurchaseHeaderSupplierDataSeperated
    Inherits cPurchaseHeader

    Private _SupplierCode As New ColField(Of String)("SupplierCode", "", "Supplier No", False, False)
    Private _SupplierName As New ColField(Of String)("SupplierName", "", "Supplier Name", False, False)

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)

        MyBase.New(oasys3DB)

        BOFields.Add(_SupplierCode)
        BOFields.Add(_SupplierName)

    End Sub

    Friend Overrides Function GetData(ByVal count As Integer) As System.Data.DataSet

        Dim SB As New StringBuilder
        Dim DS As DataSet

        With SB
            If count = -1 Then
                .AppendLine("select a.*,                            ")
            Else
                .AppendLine("select top " & count.ToString & " a.*, ")
            End If

            .AppendLine("       SupplierCode = b.SUPN,              ")
            .AppendLine("       SupplierName = b.Name               ")
            .AppendLine("from PURHDR a                              ")
            .AppendLine("inner join SUPMAS b                        ")
            .AppendLine("      on b.SUPN = a.SUPP                   ")

            If Oasys3DB.WhereColumns.Count > 0 Then

                .AppendLine("where ")

                For Index As Integer = 0 To Oasys3DB.WhereColumns.Count - 1
                    If Index > 0 Then
                        Select Case Oasys3DB.WhereJoins(Index - 1)
                            Case clsOasys3DB.eOperator.pAnd
                                .AppendLine("and ")

                            Case Else
                                'this will be a issue

                        End Select
                    End If

                    .AppendLine(CStr(IIf(Oasys3DB.WhereColumns(Index).ToString. _
                                         Contains("SupplierName"), "SUPP", Oasys3DB.WhereColumns(Index).ToString)) & Space(1))

                    Select Case Oasys3DB.WhereComparison(Index)
                        Case clsOasys3DB.eOperator.pIn
                            .AppendLine("in ( ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")
                            .AppendLine(") ")

                        Case clsOasys3DB.eOperator.pEquals
                            .AppendLine(" = ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case clsOasys3DB.eOperator.pNotEquals
                            .AppendLine(" <> ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case clsOasys3DB.eOperator.pLike
                            .AppendLine(" like ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "%' ")

                        Case clsOasys3DB.eOperator.pGreaterThan
                            .AppendLine(" > ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                            .AppendLine(" >= ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case clsOasys3DB.eOperator.pLessThan
                            .AppendLine(" < ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case clsOasys3DB.eOperator.pLessThanOrEquals
                            .AppendLine(" <= ")
                            .AppendLine("'" & Oasys3DB.WhereValues(Index).ToString & "' ")

                        Case Else
                            'this will be a issue

                    End Select
                Next
            End If

            If Oasys3DB.WhereColumns.Count = 0 Then
                .AppendLine("where ")
            Else
                .AppendLine("and ")
            End If

            .AppendLine("DDAT is not null  ")
            .AppendLine("order by ODAT desc")   'sort on purchase order date

        End With

        With Oasys3DB
            .ClearAllParameters()
            DS = .ExecuteSql(SB.ToString)
        End With

        Return DS

    End Function

    Friend Overrides Function GetNewInstance() As cPurchaseHeader

        Return New cPurchaseHeaderSupplierDataSeperated(Oasys3DB)

    End Function

    Public Overrides Sub LoadFromRow(ByVal drRow As DataRow)

        MyBase.LoadFromRow(drRow)

        For Each Col As DataColumn In drRow.Table.Columns

            If Not IsDBNull(drRow(Col)) Then

                Select Case (Col.ColumnName)

                    Case ("SupplierCode") : _SupplierCode.Value = drRow(Col).ToString
                    Case ("SupplierName") : _SupplierName.Value = drRow(Col).ToString

                End Select

            End If

        Next

    End Sub

End Class