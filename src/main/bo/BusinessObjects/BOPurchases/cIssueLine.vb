﻿<Serializable()> Public Class cIssueLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ISULIN"
        BOFields.Add(_IssueNumber)
        BOFields.Add(_LineNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_QtyOrdered)
        BOFields.Add(_QtyIssued)
        BOFields.Add(_QtyToFollow)
        BOFields.Add(_SupplierReqToFollow)
        BOFields.Add(_AddByMaintenance)
        BOFields.Add(_RejectionCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cIssueLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cIssueLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cIssueLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cIssueLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _IssueNumber As New ColField(Of String)("NUMB", "000000", "Issue Number", True, False)
    Private _LineNumber As New ColField(Of String)("LINE", "0000", "Line Number", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "000000", "SKU Number", False, False)
    Private _QtyOrdered As New ColField(Of Decimal)("QTYO", 0, "Quantity Ordered", False, False)
    Private _QtyIssued As New ColField(Of Decimal)("QTYI", 0, "Quantity Issued", False, False)
    Private _QtyToFollow As New ColField(Of Decimal)("QTYF", 0, "Quantity to Follow", False, False)
    Private _SupplierReqToFollow As New ColField(Of Boolean)("SRTF", False, "Supplier Required to Follow", False, False)
    Private _AddByMaintenance As New ColField(Of Boolean)("ADDM", False, "Add by Maintenance", False, False)
    Private _RejectionCode As New ColField(Of String)("REJI", "", "Rejection Code", False, False)

#End Region

#Region "Field Properties"

    Public Property IssueNumber() As ColField(Of String)
        Get
            IssueNumber = _IssueNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _IssueNumber = value
        End Set
    End Property
    Public Property LineNumber() As ColField(Of String)
        Get
            LineNumber = _LineNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _LineNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property QtyOrdered() As ColField(Of Decimal)
        Get
            QtyOrdered = _QtyOrdered
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyOrdered = value
        End Set
    End Property
    Public Property QtyIssued() As ColField(Of Decimal)
        Get
            QtyIssued = _QtyIssued
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyIssued = value
        End Set
    End Property
    Public Property QtyTofollow() As ColField(Of Decimal)
        Get
            QtyTofollow = _QtyToFollow
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyToFollow = value
        End Set
    End Property
    Public Property SupplierRecToFollow() As ColField(Of Boolean)
        Get
            SupplierRecToFollow = _SupplierReqToFollow
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SupplierReqToFollow = value
        End Set
    End Property
    Public Property AddByMaintenance() As ColField(Of Boolean)
        Get
            AddByMaintenance = _AddByMaintenance
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _AddByMaintenance = value
        End Set
    End Property
    Public Property RejectionCode() As ColField(Of String)
        Get
            RejectionCode = _RejectionCode
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectionCode = value
        End Set
    End Property


#End Region

#Region "Methods"
    Private _Stock As BOStock.cStock = Nothing

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property
    Public Function UpdateRejectCode(ByVal issueNum As String, ByVal lineno As String, ByVal RejectCode As String) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_RejectionCode.ColumnName, RejectCode)
            Oasys3DB.SetWhereParameter(_IssueNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, issueNum)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_LineNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, lineno)

            NoRecs = Oasys3DB.Update()
            If NoRecs > 0 Then
                SaveError = False
            Else
                SaveError = True
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cIssueLine - UpdateRecjectCode exception " & ex.Message)
            Return True
        End Try

    End Function

#End Region

End Class

