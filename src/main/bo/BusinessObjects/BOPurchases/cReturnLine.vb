﻿<Serializable()> Public Class cReturnLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RETLIN"
        BOFields.Add(_ID)
        BOFields.Add(_HeaderID)
        BOFields.Add(_SKUNumber)
        BOFields.Add(_QtyReturned)
        BOFields.Add(_Price)
        BOFields.Add(_Cost)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_RTI)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cReturnLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cReturnLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cReturnLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cReturnLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("TKEY", 0, True, True, 1, 0, True, True, 1, 0, 0, 0, "", 0, "ID")
    Private _HeaderID As New ColField(Of Integer)("HKEY", 0, False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Header ID")
    Private _SKUNumber As New ColField(Of String)("SKUN", "000000", False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "SKU Number")
    Private _QtyReturned As New ColField(Of Decimal)("QUAN", 0, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Qty Returned")
    Private _Price As New ColField(Of Decimal)("PRIC", 0, False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "Price")
    Private _Cost As New ColField(Of Decimal)("COST", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Cost")
    Private _ReasonCode As New ColField(Of String)("REAS", "", False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Reason Code")
    Private _RTI As New ColField(Of String)("RTI", "S", False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "RTI")

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property HeaderID() As ColField(Of Integer)
        Get
            Return _HeaderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _HeaderID = value
        End Set
    End Property
    Public Property SKUNumber() As ColField(Of String)
        Get
            SKUNumber = _SKUNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SKUNumber = value
        End Set
    End Property
    Public Property QtyReturned() As ColField(Of Decimal)
        Get
            QtyReturned = _QtyReturned
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _QtyReturned = value
        End Set
    End Property
    Public Property Price() As ColField(Of Decimal)
        Get
            Return _Price
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Price = value
        End Set
    End Property
    Public Property Cost() As ColField(Of Decimal)
        Get
            Cost = _Cost
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Cost = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of String)
        Get
            Return _ReasonCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ReasonCode = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _table As New DataTable
    Private _Stock As BOStock.cStock = Nothing

    Public ReadOnly Property Table() As DataTable
        Get
            Return _table
        End Get
    End Property
    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SKUNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property
#End Region

#Region "Enums"
    Public Enum TableCol
        Id
        HeaderId
        SkuNumber
        SkuDescription
        Quantity
        Price
        Cost
        Value
        ReasonCode
        ReasonDescription
        Rti
    End Enum
#End Region

#Region "Methods"

    Public Sub DeleteLines(ByVal HeaderID As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(_HeaderID.ColumnName, clsOasys3DB.eOperator.pEquals, HeaderID)
            Oasys3DB.Delete()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Loads view table into Table property of this instance.
    '''  Throws exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub TableLoadDetails(ByVal headerId As Integer)

        Try
            Dim sb As New StringBuilder("EXEC " & My.Resources.Procs.ReturnsSelectDetailsForId & Space(1) & headerId)
            _table = Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

End Class
