﻿Public Class SoqConstants
    Public Shared ReadOnly Forecast As Boolean = True                                           'FCST
    Public Shared ReadOnly InitNewLimit As Integer = 12                                         'S23
    Public Shared ReadOnly InitNewPeriods As Integer = 2                                        'S24
    Public Shared ReadOnly InitPostLimit As Integer = 6                                         'S25
    Public Shared ReadOnly NumPeriodsConsecutiveZero As Integer = 8                             'S08
    Public Shared ReadOnly NumPeriodsAveSlowLumpy As Integer = 12                               'S13
    Public Shared ReadOnly SlowAveDemandCheck As Decimal = 0.7D                                 'S07

    Public Shared ReadOnly InitTrendSignificance As Decimal = 1                                 'S09
    Public Shared ReadOnly ErracticLimitVariance As Decimal = 1.3D                              'S10
    Public Shared ReadOnly UseNegativeTrend As Boolean = True                                   'S12
    Public Shared ReadOnly LumpyBufferAdjust As Decimal = 1                                     'S15

    Public Shared ReadOnly StdDevMin As Decimal = 0.2D                                          'S16
    Public Shared ReadOnly StdDevMax As Decimal = 1.3D                                          'S17

    Public Shared ReadOnly TargetServicePercent As Decimal() = {95, 0, 0, 0, 0, 0, 0, 0, 0}     'S18
    Public Shared ReadOnly SalesBias As Decimal() = {0.14D, 0.15D, 0.14D, 0.13D, 0.13D, 0.14D, 0.17D}  'S34
    Public Shared ReadOnly SmoothingConstant As Decimal = 0.1D                                  'S19
    Public Shared ReadOnly TrackingSignalLimit As Decimal = 0.6D                                'S20
    Public Shared ReadOnly InputDemandSupressor As Decimal = 6                                  'S21
    Public Shared ReadOnly AddLeadTimeSupClose As Decimal = 0                                   'S35

    Public Shared ReadOnly Property LumpyMultiplier() As Dictionary(Of Integer, Decimal)               'S33
        Get
            LumpyMultiplier = New Dictionary(Of Integer, Decimal)
            LumpyMultiplier.Add(70, 0.8D)
            LumpyMultiplier.Add(80, 1.2D)
            LumpyMultiplier.Add(90, 2.3D)
            LumpyMultiplier.Add(95, 2.7D)
            LumpyMultiplier.Add(97, 3D)
            LumpyMultiplier.Add(99, 4D)
            LumpyMultiplier.Add(100, 5.5D)
        End Get
    End Property

    Public Shared ReadOnly Property FlierEliminator() As Dictionary(Of Integer, Decimal)        'S11
        Get
            FlierEliminator = New Dictionary(Of Integer, Decimal)
            FlierEliminator.Add(4, 1.4D)
            FlierEliminator.Add(5, 1.6D)
            FlierEliminator.Add(6, 1.9D)
            FlierEliminator.Add(7, 2D)
            FlierEliminator.Add(8, 2.2D)
            FlierEliminator.Add(9, 2.3D)
            FlierEliminator.Add(10, 2.4D)
            FlierEliminator.Add(11, 2.6D)
            FlierEliminator.Add(12, 2.7D)
            FlierEliminator.Add(13, 2.7D)
        End Get
    End Property

    Public Shared ReadOnly Property ZeroDemandLumpyCheck() As Dictionary(Of Integer, Integer)   'S36
        Get
            ZeroDemandLumpyCheck = New Dictionary(Of Integer, Integer)
            ZeroDemandLumpyCheck.Add(4, 3)
            ZeroDemandLumpyCheck.Add(5, 3)
            ZeroDemandLumpyCheck.Add(6, 4)
            ZeroDemandLumpyCheck.Add(7, 4)
            ZeroDemandLumpyCheck.Add(8, 5)
            ZeroDemandLumpyCheck.Add(9, 5)
            ZeroDemandLumpyCheck.Add(10, 6)
            ZeroDemandLumpyCheck.Add(11, 6)
            ZeroDemandLumpyCheck.Add(12, 7)
            ZeroDemandLumpyCheck.Add(13, 7)
        End Get
    End Property

    Public Shared ReadOnly Property SlowLumpyCheck() As Dictionary(Of Integer, Integer)           'S37
        Get
            SlowLumpyCheck = New Dictionary(Of Integer, Integer)
            SlowLumpyCheck.Add(4, 1)
            SlowLumpyCheck.Add(5, 1)
            SlowLumpyCheck.Add(6, 2)
            SlowLumpyCheck.Add(7, 2)
            SlowLumpyCheck.Add(8, 3)
            SlowLumpyCheck.Add(9, 3)
            SlowLumpyCheck.Add(10, 4)
            SlowLumpyCheck.Add(11, 4)
            SlowLumpyCheck.Add(12, 5)
            SlowLumpyCheck.Add(13, 5)
        End Get
    End Property

    Public Shared ReadOnly Property TrendSignificance() As Dictionary(Of Integer, Decimal)             'S28
        Get
            TrendSignificance = New Dictionary(Of Integer, Decimal)
            TrendSignificance.Add(4, 0.95D)
            TrendSignificance.Add(5, 0.87D)
            TrendSignificance.Add(6, 0.81D)
            TrendSignificance.Add(7, 0.75D)
            TrendSignificance.Add(8, 0.71D)
            TrendSignificance.Add(9, 0.67D)
            TrendSignificance.Add(10, 0.63D)
            TrendSignificance.Add(11, 0.6D)
            TrendSignificance.Add(12, 0.58D)
            TrendSignificance.Add(13, 0.55D)
        End Get
    End Property



    Public Shared ReadOnly Property SlowOrderLevel70() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel70 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel70.Add(1.2D, 1)
            SlowOrderLevel70.Add(2D, 2)
            SlowOrderLevel70.Add(2.5D, 4)
            SlowOrderLevel70.Add(3.5D, 5)
            SlowOrderLevel70.Add(5D, 6)
            SlowOrderLevel70.Add(6D, 7)
            SlowOrderLevel70.Add(7D, 8)
            SlowOrderLevel70.Add(8D, 10)
            SlowOrderLevel70.Add(9D, 11)
            SlowOrderLevel70.Add(10D, 12)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel80() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel80 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel80.Add(0.9D, 1)
            SlowOrderLevel80.Add(1.4D, 2)
            SlowOrderLevel80.Add(2D, 3)
            SlowOrderLevel80.Add(3D, 4)
            SlowOrderLevel80.Add(3.5D, 5)
            SlowOrderLevel80.Add(4.5D, 6)
            SlowOrderLevel80.Add(5D, 7)
            SlowOrderLevel80.Add(6D, 8)
            SlowOrderLevel80.Add(7D, 9)
            SlowOrderLevel80.Add(8D, 11)
            SlowOrderLevel80.Add(9D, 12)
            SlowOrderLevel80.Add(10D, 13)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel90() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel90 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel90.Add(0.5D, 1)
            SlowOrderLevel90.Add(1D, 2)
            SlowOrderLevel90.Add(1.6D, 3)
            SlowOrderLevel90.Add(2D, 4)
            SlowOrderLevel90.Add(3D, 5)
            SlowOrderLevel90.Add(3.5D, 6)
            SlowOrderLevel90.Add(4.5D, 7)
            SlowOrderLevel90.Add(5D, 8)
            SlowOrderLevel90.Add(6D, 9)
            SlowOrderLevel90.Add(7D, 10)
            SlowOrderLevel90.Add(8D, 12)
            SlowOrderLevel90.Add(9D, 13)
            SlowOrderLevel90.Add(10D, 14)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel95() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel95 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel95.Add(0.4D, 1)
            SlowOrderLevel95.Add(0.8D, 2)
            SlowOrderLevel95.Add(1.2D, 3)
            SlowOrderLevel95.Add(1.8D, 4)
            SlowOrderLevel95.Add(2.5D, 5)
            SlowOrderLevel95.Add(3D, 6)
            SlowOrderLevel95.Add(3.5D, 7)
            SlowOrderLevel95.Add(4D, 8)
            SlowOrderLevel95.Add(5D, 9)
            SlowOrderLevel95.Add(6D, 10)
            SlowOrderLevel95.Add(7D, 12)
            SlowOrderLevel95.Add(8D, 13)
            SlowOrderLevel95.Add(9D, 14)
            SlowOrderLevel95.Add(10D, 15)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel97() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel97 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel97.Add(0.3D, 1)
            SlowOrderLevel97.Add(0.6D, 2)
            SlowOrderLevel97.Add(1D, 3)
            SlowOrderLevel97.Add(1.6D, 4)
            SlowOrderLevel97.Add(2D, 5)
            SlowOrderLevel97.Add(2.5D, 6)
            SlowOrderLevel97.Add(3D, 7)
            SlowOrderLevel97.Add(4D, 8)
            SlowOrderLevel97.Add(4.5D, 9)
            SlowOrderLevel97.Add(5D, 10)
            SlowOrderLevel97.Add(6D, 11)
            SlowOrderLevel97.Add(7D, 13)
            SlowOrderLevel97.Add(8D, 14)
            SlowOrderLevel97.Add(9D, 15)
            SlowOrderLevel97.Add(10D, 17)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel99() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel99 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel99.Add(0.2D, 1)
            SlowOrderLevel99.Add(0.4D, 2)
            SlowOrderLevel99.Add(0.8D, 3)
            SlowOrderLevel99.Add(1.2D, 4)
            SlowOrderLevel99.Add(1.6D, 5)
            SlowOrderLevel99.Add(2D, 6)
            SlowOrderLevel99.Add(2.5D, 7)
            SlowOrderLevel99.Add(3.5D, 8)
            SlowOrderLevel99.Add(4D, 9)
            SlowOrderLevel99.Add(4.5D, 10)
            SlowOrderLevel99.Add(5D, 11)
            SlowOrderLevel99.Add(6D, 12)
            SlowOrderLevel99.Add(7D, 14)
            SlowOrderLevel99.Add(8D, 15)
            SlowOrderLevel99.Add(9D, 17)
            SlowOrderLevel99.Add(10D, 18)
        End Get
    End Property

    Public Shared ReadOnly Property SlowOrderLevel999() As Dictionary(Of Decimal, Integer)
        Get
            SlowOrderLevel999 = New Dictionary(Of Decimal, Integer)
            SlowOrderLevel999.Add(0.2D, 1)
            SlowOrderLevel999.Add(0.4D, 2)
            SlowOrderLevel999.Add(0.5D, 4)
            SlowOrderLevel999.Add(0.8D, 5)
            SlowOrderLevel999.Add(1.2D, 6)
            SlowOrderLevel999.Add(1.6D, 7)
            SlowOrderLevel999.Add(2D, 8)
            SlowOrderLevel999.Add(2.5D, 9)
            SlowOrderLevel999.Add(3D, 10)
            SlowOrderLevel999.Add(4D, 11)
            SlowOrderLevel999.Add(4.5D, 12)
            SlowOrderLevel999.Add(5D, 13)
            SlowOrderLevel999.Add(6D, 15)
            SlowOrderLevel999.Add(7D, 16)
            SlowOrderLevel999.Add(8D, 18)
            SlowOrderLevel999.Add(9D, 20)
            SlowOrderLevel999.Add(10D, 21)
        End Get
    End Property



    Public Shared Function StdDevMultiplier(ByVal PeriodDemand As Decimal) As Decimal

        Select Case PeriodDemand
            Case Is <= 3 : Return 1
            Case Is <= 10 : Return 0.65D
            Case Else : Return 0.45D
        End Select

    End Function

    Public Shared Function ServiceFunction(ByVal ek As Decimal) As Decimal

        Select Case ek
            Case Is < 0.0004D : Return 3.1D
            Case Is < 0.0005D : Return 3
            Case Is < 0.0008D : Return 2.9D
            Case Is < 0.0011D : Return 2.8D
            Case Is < 0.0014D : Return 2.7D
            Case Is < 0.002D : Return 2.6D
            Case Is < 0.0026D : Return 2.5D
            Case Is < 0.0036D : Return 2.4D
            Case Is < 0.0049D : Return 2.3D
            Case Is < 0.0065D : Return 2.2D
            Case Is < 0.0085D : Return 2.1D
            Case Is < 0.011D : Return 2
            Case Is < 0.014D : Return 1.9D
            Case Is < 0.018D : Return 1.8D
            Case Is < 0.023D : Return 1.7D
            Case Is < 0.03D : Return 1.6D
            Case Is < 0.04D : Return 1.5D
            Case Is < 0.05D : Return 1.4D
            Case Is < 0.06D : Return 1.3D
            Case Is < 0.07D : Return 1.2D
            Case Is < 0.08D : Return 1.1D
            Case Is < 0.1D : Return 1
            Case Is < 0.12D : Return 0.9D
            Case Is < 0.14D : Return 0.8D
            Case Is < 0.17D : Return 0.7D
            Case Is < 0.2D : Return 0.6D
            Case Is < 0.23D : Return 0.5D
            Case Is < 0.27D : Return 0.4D
            Case Is < 0.31D : Return 0.3D
            Case Is < 0.35D : Return 0.2D
            Case Is < 0.4D : Return 0.1D
            Case Else : Return 0
        End Select

    End Function

    Public Shared Function AnnualUsage(ByVal value As Integer) As String

        Select Case value
            Case Is <= 100 : Return "C3"
            Case Is <= 400 : Return "C2"
            Case Is <= 1500 : Return "C1"
            Case Is <= 4000 : Return "B3"
            Case Is <= 7000 : Return "B2"
            Case Is <= 12000 : Return "B1"
            Case Is <= 20000 : Return "A3"
            Case Is <= 30000 : Return "A2"
            Case Else : Return "A1"
        End Select

    End Function


    Public Shared Function DateLimitNew() As Date

        Return Now.AddDays(InitNewLimit * -7).Date

    End Function

    Public Shared Function DateLimitNewPost() As Date

        Return Now.AddDays((InitNewLimit + InitPostLimit) * -7).Date

    End Function

End Class