﻿<Serializable()> Public Class cIssueHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "ISUHDR"
        BOFields.Add(_IssueNumber)
        BOFields.Add(_IssueDate)
        BOFields.Add(_SupplierNumber)
        BOFields.Add(_StorePoNumber)
        BOFields.Add(_StoreSoqNumber)
        BOFields.Add(_SupplierBbcCode)
        BOFields.Add(_IssueValue)
        BOFields.Add(_PricingDoc)
        BOFields.Add(_ImportedIssue)
        BOFields.Add(_ReceivedIssue)
        BOFields.Add(_DeleteIssue)
        BOFields.Add(_DrlNumber)
        BOFields.Add(_RejectionCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cIssueHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cIssueHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cIssueHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cIssueHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _IssueNumber As New ColField(Of String)("NUMB", "", "Issue Number", True, False)
    Private _IssueDate As New ColField(Of Date)("IDAT", Nothing, "Issue Date", False, False)
    Private _SupplierNumber As New ColField(Of String)("SUPP", "00000", "Supplier Number", False, False)
    Private _StorePoNumber As New ColField(Of String)("SPON", "000000", "Store PO Number", False, False)
    Private _StoreSoqNumber As New ColField(Of String)("SOQN", "000000", "Store SOQ Number", False, False)
    Private _SupplierBbcCode As New ColField(Of String)("BBCC", "", "Supplier BBC Code", False, False)
    Private _IssueValue As New ColField(Of Decimal)("VALU", 0, "Issue Value", False, False, 2)
    Private _PricingDoc As New ColField(Of String)("PNUM", "000000", "Pricing Document", False, False)
    Private _ImportedIssue As New ColField(Of Boolean)("IMPI", False, "Imported Issue", False, False)
    Private _ReceivedIssue As New ColField(Of Boolean)("RECI", False, "Received Issue", False, False)
    Private _DeleteIssue As New ColField(Of Boolean)("DELI", False, "Delete Issue", False, False)
    Private _DrlNumber As New ColField(Of String)("DRLN", "000000", "DRL Number", False, False)
    Private _RejectionCode As New ColField(Of String)("REJI", "", "Rejection Code", False, False)

#End Region

#Region "Field Properties"

    Public Property IssueNumber() As ColField(Of String)
        Get
            IssueNumber = _IssueNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _IssueNumber = value
        End Set
    End Property
    Public Property IssueDate() As ColField(Of Date)
        Get
            IssueDate = _IssueDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _IssueDate = value
        End Set
    End Property
    Public Property SupplierNumber() As ColField(Of String)
        Get
            SupplierNumber = _SupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierNumber = value
        End Set
    End Property
    Public Property SupplierBbcCode() As ColField(Of String)
        Get
            SupplierBbcCode = _SupplierBbcCode
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierBbcCode = value
        End Set
    End Property
    Public Property StorePoNumber() As ColField(Of String)
        Get
            StorePoNumber = _StorePoNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StorePoNumber = value
        End Set
    End Property
    Public Property StoreSoqNumber() As ColField(Of String)
        Get
            StoreSoqNumber = _StoreSoqNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreSoqNumber = value
        End Set
    End Property
    Public Property IssueValue() As ColField(Of Decimal)
        Get
            IssueValue = _IssueValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _IssueValue = value
        End Set
    End Property
    Public Property PricingDoc() As ColField(Of String)
        Get
            PricingDoc = _PricingDoc
        End Get
        Set(ByVal value As ColField(Of String))
            _PricingDoc = value
        End Set
    End Property
    Public Property ImportedIssue() As ColField(Of Boolean)
        Get
            ImportedIssue = _ImportedIssue
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ImportedIssue = value
        End Set
    End Property
    Public Property ReceivedIssue() As ColField(Of Boolean)
        Get
            ReceivedIssue = _ReceivedIssue
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReceivedIssue = value
        End Set
    End Property
    Public Property DeleteIssue() As ColField(Of Boolean)
        Get
            DeleteIssue = _DeleteIssue
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _DeleteIssue = value
        End Set
    End Property
    Public Property DrlNumber() As ColField(Of String)
        Get
            DrlNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property RejectionCode() As ColField(Of String)
        Get
            RejectionCode = _RejectionCode
        End Get
        Set(ByVal value As ColField(Of String))
            _RejectionCode = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Headers As List(Of cIssueHeader) = Nothing
    Private _Lines As List(Of cIssueLine) = Nothing
    Private _Supplier As cSupplierMaster = Nothing
    Private _Order As cPurchaseHeader = Nothing

    Public Property Headers() As List(Of cIssueHeader)
        Get
            If _Headers Is Nothing Then
                _Headers = LoadMatches()
            End If
            Return _Headers
        End Get
        Set(ByVal value As List(Of cIssueHeader))
            _Headers = value
        End Set
    End Property
    Public Property Header(ByVal number As String) As cIssueHeader
        Get
            For Each head As cIssueHeader In Headers
                If head.IssueNumber.Value = number Then Return head
            Next
            Return Nothing
        End Get
        Set(ByVal value As cIssueHeader)
            For Each head As cIssueHeader In Headers
                If head.IssueNumber.Value = number Then head = value
            Next
        End Set
    End Property

    Public Property Lines() As List(Of cIssueLine)
        Get
            If _Lines Is Nothing Then
                Dim lin As New cIssueLine(Oasys3DB)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.IssueNumber, _IssueNumber.Value)
                _Lines = lin.LoadMatches
            End If
            Return _Lines
        End Get
        Set(ByVal value As List(Of cIssueLine))
            _Lines = value
        End Set
    End Property
    Public Property Line(ByVal number As Integer) As cIssueLine
        Get
            For Each lin As cIssueLine In Lines
                If lin.LineNumber.Value = number.ToString("0000") Then Return lin
            Next

            Dim newLine As New cIssueLine(Oasys3DB)
            newLine.IssueNumber.Value = _IssueNumber.Value
            newLine.LineNumber.Value = (_Lines.Count + 1).ToString("0000")
            _Lines.Add(newLine)
            Return newLine
        End Get
        Set(ByVal value As cIssueLine)
            For Each lin As cIssueLine In Lines
                If lin.LineNumber.Value = number.ToString("0000") Then lin = value
            Next
        End Set
    End Property

    Public Property Supplier() As cSupplierMaster
        Get
            If _Supplier Is Nothing Then
                _Supplier = New cSupplierMaster(Oasys3DB)
                _Supplier.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Supplier.Number, _SupplierNumber.Value)
                _Supplier.LoadMatches()
            End If
            Return _Supplier
        End Get
        Set(ByVal value As cSupplierMaster)
            _Supplier = value
        End Set
    End Property
    Public Property Order() As cPurchaseHeader
        Get
            If _Order Is Nothing Then
                _Order = New cPurchaseHeader(Oasys3DB)
                _Order.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Order.PONumber, _StorePoNumber.Value)
                _Order.LoadMatches()
            End If
            Return _Order
        End Get
        Set(ByVal value As cPurchaseHeader)
            _Order = value
        End Set
    End Property


#End Region

#Region "Methods"

    Public Function UpdateIssueReceived(ByVal UserID As Integer) As Boolean

        Try
            Dim stkmasOnorFactoryInstance As IStkmasOnorUpdate = New StkmasOnorUpdateFactory().GetImplementation()
            'first check that if not imported that purchase order exists and satisfies conditions
            If Not _ImportedIssue.Value Then
                If Order.PurchaseHeaderID.Value = 0 Then
                    _RejectionCode.Value = "B"
                    SaveIfExists()
                    Return False
                End If

                If _Order.ReceivedComplete.Value Then
                    _RejectionCode.Value = "C"
                    SaveIfExists()
                    Return False
                End If

                If _Order.Deleted.Value Then
                    _RejectionCode.Value = "D"
                    SaveIfExists()
                    Return False
                End If
            End If

            'check that there are any lines to receive
            If Lines.Count = 0 Then
                _ReceivedIssue.Value = True
                SaveIfExists()
                Return True
            End If


            'create drl header for this issue
            Dim drlHeader As New cDrlHeader(Oasys3DB)
            drlHeader.EmployeeID.Value = UserID.ToString("000")
            drlHeader.Comment.Value = "Auto Issue Receipt"
            drlHeader.POSupplierBBC.Value = True
            drlHeader.POSupplierNumber.Value = _SupplierNumber.Value
            drlHeader.PONumber.Value = _StorePoNumber.Value
            drlHeader.POSOQNumber.Value = _StoreSoqNumber.Value
            drlHeader.PODeliveryNote1.Value = _IssueNumber.Value
            drlHeader.PODeliveryNote2.Value = "Y"
            drlHeader.Details = New List(Of cDrlDetail)

            If Not _ImportedIssue.Value Then
                drlHeader.POReturnID.Value = _Order.PurchaseHeaderID.Value
                drlHeader.PODate.Value = _Order.DateOrderCreated.Value
            Else
                drlHeader.PODeliveryNote3.Value = "IMPORT"
            End If


            'start transaction
            Oasys3DB.BeginTransaction()


            'get stocks for issue lines
            Dim skuNumbers As New ArrayList
            For Each lin As cIssueLine In _Lines
                skuNumbers.Add(lin.SkuNumber.Value)
            Next
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
            stocks.Stocks = stocks.LoadMatches

            If stocks.Stocks.Count <> skuNumbers.Count Then
                _RejectionCode.Value = "P"
                SaveIfExists()

                For Each lin As cIssueLine In Lines
                    Dim l As cIssueLine = lin
                    Dim b As BOStock.cStock
                    b = stocks.Stocks.Find(Function(x)
                                               Return x.SkuNumber.Value = l.SkuNumber.Value
                                           End Function)
                    If b Is Nothing Then
                        lin.RejectionCode.Value = "P"
                        lin.SaveIfExists()
                    End If
                Next

                Oasys3DB.CommitTransaction()
                Return False
            End If

            'loop through issue lines
            For Each lin As cIssueLine In Lines
                'If lin.QtyIssued.Value <= 0 Then Continue For

                Dim stockFound As Boolean = False
                For Each stock As BOStock.cStock In stocks.Stocks
                    If lin.SkuNumber.Value = stock.SkuNumber.Value Then
                        lin.Stock = stock
                        stockFound = True
                        Exit For
                    End If
                Next

                'check that a sku exists for this
                If Not stockFound Then
                    lin.RejectionCode.Value = "I"
                    lin.SaveIfExists()
                    Continue For
                End If

                'create drldet record and update purlin if exists
                drlHeader.Value.Value += (lin.QtyIssued.Value * lin.Stock.NormalSellPrice.Value)
                drlHeader.Detail(lin).OrderPrice.Value = lin.Stock.NormalSellPrice.Value
                drlHeader.Detail(lin).SkuPrice.Value = lin.Stock.NormalSellPrice.Value

                If Not _ImportedIssue.Value Then
                    drlHeader.Detail(lin).OrderPrice.Value = _Order.Line(lin.SkuNumber.Value).OrderPrice.Value
                    _Order.Line(lin.SkuNumber.Value).ReceivedQty.Value = CInt(lin.QtyIssued.Value)
                    _Order.Line(lin.SkuNumber.Value).UpdateReceived(UserID)
                End If

                If lin.RejectionCode.Value <> "" Then
                    lin.RejectionCode.Value = ""
                    lin.SaveIfExists()
                End If
            Next

            'insert drlheader and details
            drlHeader.SaveIfNew()

            'loop through issue lines and update stock
            For Each lin As cIssueLine In _Lines
                Dim key As String = _IssueNumber.Value & Space(1) & lin.LineNumber.Value & Space(1) & drlHeader.Number.Value & Space(1) & drlHeader.Detail(lin.SkuNumber.Value).SequenceNumber.Value
                lin.Stock.UpdateReceivedIssue(CInt(stkmasOnorFactoryInstance.GetOnOrderValue(Oasys3DB, lin)), CInt(lin.QtyIssued.Value), key, UserID)
            Next


            'update issue header record as received
            _ReceivedIssue.Value = True
            _DrlNumber.Value = drlHeader.Number.Value
            _RejectionCode.Value = ""
            SaveIfExists()

            'if purchase order exists then 
            If Not _ImportedIssue.Value Then
                _Order.UpdateReceivedIssue(_DrlNumber.Value)
            End If

            'commit changes
            Oasys3DB.CommitTransaction()
            Return True

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw ex
            Return False
        End Try

    End Function

    Public Function UpdateIssueImported() As String

        Try
            'check that purchase order does not exist and return false if it does
            If _StorePoNumber.Value <> "000000" Then
                If Order.PurchaseHeaderID.Value <> 0 Then Return "Purchase order exists for this issue. Cannot import"
            End If

            _ImportedIssue.Value = True

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_ImportedIssue.ColumnName, _ImportedIssue.Value)
            SetDBKeys()
            Oasys3DB.Update()

            Return String.Empty

        Catch ex As Exception
            Throw
            Return "Problem importing issue"
        End Try

    End Function

    Public Function RejectionString() As String

        Select Case _RejectionCode.Value.Trim
            Case "A" : Return "A - Invalid Purchase Order record"
            Case "B" : Return "B - Purchase Order record not on file"
            Case "C" : Return "C - Purchase Order already received"
            Case "D" : Return "D - Purchase Order deleted"
            Case "E" : Return "E - SOQ number does not match"
            Case "F" : Return "F - Invalid Container record"
            Case "G" : Return "G - Container record not on file"
            Case "H" : Return "H  -New issue number less than previous"
            Case "I" : Return "I - Item not on file"
            Case "J" : Return "J - Item not in Purchase Line"
            Case "K" : Return "K - Purchase Line record deleted"
            Case "L" : Return "L - Issue quantity not equal to Purchase quantity"
            Case "M" : Return "M - Order qty left not equal to follow"
            Case "N" : Return "N - Supplier record not on file"
                ' "O" is actually used
            Case "P" : Return "P - SKU not set up on system"
            Case Else : Return String.Empty
        End Select

    End Function

    Public Function PoTypeString() As String

        Select Case _SupplierBbcCode.Value
            Case "C" : Return "BBC Consolidated"
            Case "D" : Return "BBC Discreet"
            Case "W" : Return "Warehouse"
            Case "A" : Return "Alternative Sup"
            Case Else : Return String.Empty
        End Select

    End Function


    Public Function Import() As Boolean

        'check if purchase order does exists and if so then if is deleted or received
        If _StorePoNumber.Value <> "000000" Then
            Dim okToImport As Boolean = True
            If Order.PurchaseHeaderID.Value <> 0 Then
                okToImport = False
                If Order.Deleted.Value Then okToImport = True
                If Order.ReceivedComplete.Value Then okToImport = True
            End If

            If okToImport Then
                _ImportedIssue.Value = True

                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
                Oasys3DB.SetColumnAndValueParameter(_ImportedIssue.ColumnName, _ImportedIssue.Value)
                SetDBKeys()
                Oasys3DB.Update()
                Return True
            End If

        End If
        Return False

    End Function


    Public Function checkIssueHeader() As List(Of cIssueHeader)
        Dim LstRecordsReturned As List(Of cIssueHeader)

        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, DeleteIssue, "0")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, ReceivedIssue, "0")
        LstRecordsReturned = LoadMatches()
        Return LstRecordsReturned
    End Function

    Public Function UpdateRejectCode(ByVal issueNum As String, ByVal RejectCode As String) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_RejectionCode.ColumnName, RejectCode)
            Oasys3DB.SetWhereParameter(_IssueNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, issueNum)

            NoRecs = Oasys3DB.Update()
            If NoRecs > 0 Then
                SaveError = False
            Else
                SaveError = True
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cIssueHeader - UpdateRecjectCode exception " & ex.Message)
            Return True
        End Try

    End Function

    Public Function UpdateSupplierNumber(ByVal issueNum As String, ByVal SupplierNum As String) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_SupplierNumber.ColumnName, SupplierNum)
            Oasys3DB.SetWhereParameter(_IssueNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, issueNum)

            NoRecs = Oasys3DB.Update()
            If NoRecs > 0 Then
                SaveError = False
            Else
                SaveError = True
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cIssueHeader - UpdateSupplierNumber exception " & ex.Message)
            Return True
        End Try

    End Function

    Public Function UpdateIssueValue(ByVal issueNum As String, ByVal value As Decimal) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IssueValue.ColumnName, value)
            Oasys3DB.SetWhereParameter(_IssueNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, issueNum)

            NoRecs = Oasys3DB.Update()
            If NoRecs > 0 Then
                SaveError = False
            Else
                SaveError = True
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cIssueHeader - UpdateIssueValue exception " & ex.Message)
            Return True
        End Try

    End Function


    Public Function QtyOrdered() As Integer

        Dim qty As Integer = 0
        For Each l As cIssueLine In Lines
            qty += CInt(l.QtyOrdered.Value)
        Next
        Return qty

    End Function

    Public Function QtyIssued() As Integer

        Dim qty As Integer = 0
        For Each l As cIssueLine In Lines
            qty += CInt(l.QtyIssued.Value)
        Next
        Return qty

    End Function

    Public Function QtyFollow() As Integer

        Dim qty As Integer = 0
        For Each l As cIssueLine In Lines
            qty += CInt(l.QtyTofollow.Value)
        Next
        Return qty

    End Function

#End Region

End Class

