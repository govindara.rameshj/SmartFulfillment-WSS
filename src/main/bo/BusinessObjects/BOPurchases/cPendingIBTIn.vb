<Serializable()> Public Class cPendingIbtIn
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "PendingIbtIn"
        BOFields.Add(_ID)
        BOFields.Add(_BatchID)
        BOFields.Add(_StoreNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Quantity)
        BOFields.Add(_DateCreated)
        BOFields.Add(_UserID)
        BOFields.Add(_DrlNumber)
        BOFields.Add(_IsProcessed)
        BOFields.Add(_IbtNumber)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPendingIbtIn)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPendingIbtIn)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPendingIbtIn))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPendingIbtIn(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _BatchID As New ColField(Of Integer)("BatchID", 0, "Batch ID", False, False)
    Private _StoreNumber As New ColField(Of String)("StoreNumber", "", "Store Number", False, False)
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "000000", "SKU Number", False, False)
    Private _Quantity As New ColField(Of Integer)("Quantity", 0, "Quantity", False, False)
    Private _DateCreated As New ColField(Of Date)("DateCreated", Nothing, "Date Created", False, False)
    Private _UserID As New ColField(Of Integer)("UserID", 0, "User ID", False, False)
    Private _DrlNumber As New ColField(Of String)("DrlNumber", "", "Drl Number", False, False)
    Private _IsProcessed As New ColField(Of Boolean)("IsProcessed", False, "Is Processed", False, False)
    Private _IbtNumber As New ColField(Of String)("IbtNumber", "", "Ibt Note Number", False, False)
#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property BatchID() As ColField(Of Integer)
        Get
            BatchID = _BatchID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _BatchID = value
        End Set
    End Property
    Public Property StoreNumber() As ColField(Of String)
        Get
            StoreNumber = _StoreNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Integer)
        Get
            Quantity = _Quantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Quantity = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property UserID() As ColField(Of Integer)
        Get
            UserID = _UserID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UserID = value
        End Set
    End Property
    Public Property DrlNumber() As ColField(Of String)
        Get
            DrlNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property IsProcessed() As ColField(Of Boolean)
        Get
            Return _IsProcessed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsProcessed = value
        End Set
    End Property
    Public Property IbtNumber() As ColField(Of String)
        Get
            Return _IbtNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _IbtNumber = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Function GetUnprocessedIBTIns() As List(Of cPendingIbtIn)

        Try
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsProcessed, False)
            SortBy(StoreNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            SortBy(SkuNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Return LoadMatches()

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function GetUnprocessedIBTInsbyStore(ByVal strStore As String, ByVal strCountryCode As String, Optional ByVal strIBTNumber As String = "") As List(Of cPendingIbtIn)

        Try
            Dim strStoreNumbers As New ArrayList
            strStoreNumbers.Add(strStore)
            strStoreNumbers.Add(CStr(IIf(strCountryCode.ToLower = "uk", "8", "5")) & strStore)

            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsProcessed, False)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, _StoreNumber, strStoreNumbers)
            If strIBTNumber <> "" Then
                JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, _IbtNumber, strIBTNumber)
            End If
            SortBy(SkuNumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Return LoadMatches()

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function UpdateProcessedIBTInsByStore(ByVal strStore As String, ByVal strDrlNumber As String, ByVal blnSetting As Boolean, ByVal strCountryCode As String, Optional ByVal strIBTNumber As String = "") As Boolean

        Try
            Dim strStoreNumbers As New ArrayList
            strStoreNumbers.Add(strStore)
            strStoreNumbers.Add(CStr(IIf(strCountryCode.ToLower = "uk", "8", "5")) & strStore)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IsProcessed.ColumnName, blnSetting)
            Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, strDrlNumber)
            Oasys3DB.SetWhereParameter(_StoreNumber.ColumnName, clsOasys3DB.eOperator.pIn, strStoreNumbers)
            'Oasys3DB.SetWhereParameter(_StoreNumber.ColumnName, clsOasys3DB.eOperator.pEquals, strStore)
            'Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pOr)
            'Oasys3DB.SetWhereParameter(_StoreNumber.ColumnName, clsOasys3DB.eOperator.pEquals, IIf(strCountryCode.ToLower = "uk", "8", "5") & strStore)
            If strIBTNumber <> "" Then
                Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                Oasys3DB.SetWhereParameter(_IbtNumber.ColumnName, clsOasys3DB.eOperator.pEquals, strIBTNumber)
            End If
            'SetDBKeys(Oasys3DB)
            Oasys3DB.Update()
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Function ISISInsertIBTIn(ByVal listSKU As List(Of String), ByVal listSKUQuantity As List(Of String), _
                                    ByVal strUserID As String, ByVal strDate As String, ByVal strStoreID As String, ByVal strIBTno As String) As Boolean

        Dim intBatchID As Integer = 0
        Dim intCount As Integer = 0
        Dim BOPendingIBTIn As New BOPurchases.cPendingIbtIn(Oasys3DB)
        Dim BOConsign As New BOPurchases.cConsignMaster(Oasys3DB)
        Dim BOUser As New BOSecurityProfile.cSystemUsers(Oasys3DB)
        Dim BOSystem As New BOSystem.cParameter(Oasys3DB)

        Const METHOD_NAME As String = "ISISInsertIBTOut"

        Try

            If BOSystem.GetParameterInteger(147) = 3 Then
                strStoreID = strStoreID.Substring(1, 3)
                Trace.WriteLine(METHOD_NAME & " StoreID = " & strStoreID)
            End If

            BOConsign.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOConsign.PONumber, strIBTno.Substring(4))
            BOConsign.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            BOConsign.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BOConsign.IBTStoreNumber, strStoreID)

            If BOConsign.LoadMatches.Count <> 0 Then
                Trace.WriteLine(METHOD_NAME & " DRLNumber is already in use, DRLNumber= " & strIBTno.Substring(4))
                Return False
            End If

            BOPendingIBTIn.AddAggregateField(clsOasys3DB.eAggregates.pAggMax, BOPendingIBTIn.BatchID, "intBatchID")

            If BOPendingIBTIn.GetAggregateField IsNot Nothing Then
                intBatchID = CInt(BOPendingIBTIn.GetAggregateField)
            Else
                intBatchID = 0
            End If

            'inserts a new record for each SKU within the message
            Do Until intCount = listSKU.Count

                BOPendingIBTIn.SkuNumber.Value = listSKU.Item(intCount)
                BOPendingIBTIn.IbtNumber.Value = strIBTno
                BOPendingIBTIn.BatchID.Value = intBatchID + 1
                BOPendingIBTIn.Quantity.Value = CInt(listSKUQuantity.Item(intCount))
                BOPendingIBTIn.StoreNumber.Value = strStoreID
                BOPendingIBTIn.UserID.Value = CInt(strUserID)
                BOPendingIBTIn.DateCreated.Value = CDate(strDate)
                If BOPendingIBTIn.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                    Trace.WriteLine(METHOD_NAME & " SKU " & listSKU.Item(intCount) & "was not saved")
                    Return False
                Else
                    BOConsign.PONumber.Value = strIBTno.Substring(4)
                    BOConsign.POReleaseNumber.Value = 0
                    BOConsign.EmployeeID.Value = BOUser.ISISGetUserEmpoyeeID(strUserID)
                    BOConsign.DateConsigned.Value = Date.Today
                    BOConsign.SupplierNumber.Value = "00000"
                    BOConsign.IBTIndicator.Value = True
                    BOConsign.IBTStoreNumber.Value = strStoreID

                    If BOConsign.Save(clsOasys3DB.eSqlQueryType.pInsert) = False Then
                        Trace.WriteLine(METHOD_NAME & " SKU " & listSKU.Item(intCount) & "was not saved")
                        Return False
                    End If
                End If
                intCount += 1
            Loop 'intCount = intQuantity
            Return True
        Catch ex As Exception
            Trace.WriteLine(METHOD_NAME & " Error:" & ex.Message)
            Return False
        End Try
    End Function


#End Region

End Class
