﻿Public Class vwSuppliersWithStockItems
    Public Enum col
        Number
        Name
        Display
    End Enum
    Private _oasys3DB As Oasys3.DB.clsOasys3DB
    Private _view As String = "vwSuppliersWithStockItems"
    Private _table As DataTable = Nothing

    Public ReadOnly Property Table() As DataTable
        Get
            Return _table
        End Get
    End Property


    Public Sub New()
        _oasys3DB = New Oasys3.DB.clsOasys3DB("Default", 0)
    End Sub
    Public Sub New(ByVal oasys3DB As Oasys3.DB.clsOasys3DB)
        _oasys3DB = oasys3DB
    End Sub


    ''' <summary>
    ''' Loads view table into Table property of this instance.
    '''  Throws exception on error
    ''' </summary>
    ''' <param name="withSorting"></param>
    ''' <remarks></remarks>
    Public Sub LoadView(Optional ByVal withSorting As Boolean = False)

        Try
            Dim sb As New StringBuilder("SELECT * FROM " & _view)
            If withSorting Then sb.Append(" ORDER BY " & col.Name)

            _table = _oasys3DB.ExecuteSql(sb.ToString).Tables(0)

            'remove any null rows
            For index As Integer = _table.Rows.Count - 1 To 0 Step -1
                Dim dr As DataRow = _table.Rows(index)
                If dr(0).ToString.Length = 0 Then _table.Rows.Remove(dr)
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Function GetName(ByVal number As String) As String

        Try
            If _table Is Nothing Then LoadView()
            For Each dr As DataRow In _table.Rows
                If CStr(dr(col.Number)) = number Then
                    Return dr(col.Name).ToString.Trim
                End If
            Next

            Return My.Resources.Errors.WarnNoSupplierName

        Catch ex As Exception
            Throw ex
        End Try

    End Function


End Class
