﻿Imports TpWickes.Library

Public Class StkmasOnorUpdateFactory
    Inherits RequirementSwitchFactory(Of IStkmasOnorUpdate)

    Private Const _RequirementId As Integer = -946

    Public Overrides Function ImplementationA() As IStkmasOnorUpdate
        Return New StkmasOnorUpdateNew
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementSwitch As IRequirementRepository

        RequirementSwitch = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(_RequirementId)
    End Function

    Public Overrides Function ImplementationB() As IStkmasOnorUpdate
        Return New StkmasOnorUpdateExisting
    End Function

End Class
