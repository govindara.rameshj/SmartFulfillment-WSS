﻿<Serializable()> Public Class cReturnHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "RETHDR"
        BOFields.Add(_ID)
        BOFields.Add(_Number)
        BOFields.Add(_SupplierNumber)
        BOFields.Add(_DateCreated)
        BOFields.Add(_DateExpectCollect)
        BOFields.Add(_DrlNumber)
        BOFields.Add(_Value)
        BOFields.Add(_PrintNeededEntry)
        BOFields.Add(_PrintNeededRelease)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_RTI)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cReturnHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cReturnHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cReturnHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cReturnHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("TKEY") : _ID.Value = CInt(drRow(dcColumn))
                    Case ("NUMB") : _Number.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("SUPP") : _SupplierNumber.Value = CStr(drRow(dcColumn))
                    Case ("EDAT") : _DateCreated.Value = CDate(drRow(dcColumn))
                    Case ("RDAT") : _DateExpectCollect.Value = CDate(drRow(dcColumn))
                    Case ("DRLN") : _DrlNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("VALU") : _Value.Value = CDec(drRow(dcColumn))
                    Case ("EPRT") : _PrintNeededEntry.Value = CBool(drRow(dcColumn))
                    Case ("RPRT") : _PrintNeededRelease.Value = CBool(drRow(dcColumn))
                    Case ("IsDeleted") : _IsDeleted.Value = CBool(drRow(dcColumn))
                    Case ("RTI") : _RTI.Value = CStr(drRow(dcColumn))
                End Select
            End If
        Next
        ' Referral 346A
        ' Rather than get the Lines every time from the database (as the previous stab
        ' at fixing this referral did), which affected other processes...
        ' Reset Lines, to force Lines property to fetch lines from database.
        ' If don't do this will retain existing lines which maybe for a different header
        ' that therefore do not match this header
        Lines = Nothing
    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("TKEY", 0, True, True, 1, 0, True, True, 1, 0, 0, 0, "", 0, "ID")
    Private _Number As New ColField(Of String)("NUMB", "000000", False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Number")
    Private _SupplierNumber As New ColField(Of String)("SUPP", "00000", False, False, 1, 0, True, True, 5, 0, 0, 0, "", 0, "Supplier Number")
    Private _DateCreated As New ColField(Of Date)("EDAT", Nothing, False, False, 1, 0, True, True, 6, 0, 0, 0, "", 0, "Date Created")
    Private _DateExpectCollect As New ColField(Of Date)("RDAT", Nothing, False, False, 1, 0, True, True, 7, 0, 0, 0, "", 0, "Date Expert Collect")
    Private _DrlNumber As New ColField(Of String)("DRLN", "000000", False, False, 1, 0, True, True, 8, 0, 0, 0, "", 0, "DRL Number")
    Private _Value As New ColField(Of Decimal)("VALU", 0, False, False, 1, 0, True, True, 9, 0, 0, 0, "", 0, "Value")
    Private _PrintNeededEntry As New ColField(Of Boolean)("EPRT", False, False, False, 1, 0, True, True, 10, 0, 0, 0, "", 0, "Print Needed Entry")
    Private _PrintNeededRelease As New ColField(Of Boolean)("RPRT", False, False, False, 1, 0, True, True, 11, 0, 0, 0, "", 0, "Print Needed Release")
    Private _IsDeleted As New ColField(Of Boolean)("IsDeleted", False, "Is Deleted", False, False)
    Private _RTI As New ColField(Of String)("RTI", "S", False, False, 1, 0, True, True, 12, 0, 0, 0, "", 0, "RTI")

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property SupplierNumber() As ColField(Of String)
        Get
            SupplierNumber = _SupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierNumber = value
        End Set
    End Property
    Public Property DateCreated() As ColField(Of Date)
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateCreated = value
        End Set
    End Property
    Public Property DateExpectCollect() As ColField(Of Date)
        Get
            DateExpectCollect = _DateExpectCollect
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateExpectCollect = value
        End Set
    End Property
    Public Property DrlNumber() As ColField(Of String)
        Get
            DrlNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Value = _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property PrintNeededEntry() As ColField(Of Boolean)
        Get
            PrintNeededEntry = _PrintNeededEntry
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintNeededEntry = value
        End Set
    End Property
    Public Property PrintNeededRelease() As ColField(Of Boolean)
        Get
            PrintNeededRelease = _PrintNeededRelease
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _PrintNeededRelease = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            Return _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _table As New DataTable
    Private _Supplier As cSupplierMaster = Nothing
    Private _Headers As List(Of cReturnHeader) = Nothing
    Private _Lines As List(Of cReturnLine) = Nothing

    Public ReadOnly Property Table() As DataTable
        Get
            Return _table
        End Get
    End Property
    Public Property Supplier() As cSupplierMaster
        Get
            If _Supplier Is Nothing Then
                _Supplier = New cSupplierMaster(Oasys3DB)
                _Supplier.AddLoadFilter(clsOasys3DB.eOperator.pEquals, _Supplier.Number, _SupplierNumber.Value)
                _Supplier.LoadMatches()
            End If
            Return _Supplier
        End Get
        Set(ByVal value As cSupplierMaster)
            _Supplier = value
        End Set
    End Property
    Public Property Header(ByVal HeaderID As Integer) As cReturnHeader
        Get
            For Each h As cReturnHeader In _Headers
                If h.ID.Value = HeaderID Then Return h
            Next
            Return New cReturnHeader(Oasys3DB)
        End Get
        Set(ByVal value As cReturnHeader)
            For Each h As cReturnHeader In _Headers
                If h.ID.Value = HeaderID Then
                    h = value
                End If
            Next
        End Set
    End Property
    Public Property Header(ByVal ReturnNumber As String) As cReturnHeader
        Get
            For Each h As cReturnHeader In _Headers
                If h.Number.Value = ReturnNumber Then Return h
            Next
            Return New cReturnHeader(Oasys3DB)
        End Get
        Set(ByVal value As cReturnHeader)
            For Each h As cReturnHeader In _Headers
                If h.Number.Value = ReturnNumber Then
                    h = value
                End If
            Next
        End Set
    End Property
    Public Property Headers() As List(Of cReturnHeader)
        Get
            Return _Headers
        End Get
        Set(ByVal value As List(Of cReturnHeader))
            _Headers = value
        End Set
    End Property

    Public Property Line(ByVal LineID As Integer) As cReturnLine
        Get
            For Each l As cReturnLine In Lines
                If l.ID.Value = LineID Then Return l
            Next

            Dim newLine As New cReturnLine(Oasys3DB) 'create new line
            newLine.HeaderID.Value = _ID.Value
            newLine.ID.Value = LineID
            _Lines.Add(newLine)
            Return newLine
        End Get
        Set(ByVal value As cReturnLine)
            If _Lines IsNot Nothing Then
                For Each l As cReturnLine In _Lines
                    If l.ID.Value = LineID Then
                        l = value
                    End If
                Next
            End If
        End Set
    End Property
    Private Property Lines() As List(Of cReturnLine)
        Get
            ' Referral 346A
            ' Rather than get the Lines every time from the database (as the previous stab
            ' at fixing this referral did), which affected other processes...
            ' Removed code that was getting lines from database regardless
            ' as this prevented use of lines stored before saving.  Reinstated previous
            ' code.
            If _Lines Is Nothing Then
                _Lines = New List(Of cReturnLine)
                Dim l As New cReturnLine(Oasys3DB)
                l.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, l.HeaderID, _ID.Value)
                _Lines = l.LoadMatches
            End If

            Return _Lines
        End Get
        Set(ByVal value As List(Of cReturnLine))
            ' Referral 346A
            ' Allow _Lines to be set to 'Nothing'.
            _Lines = value
        End Set
    End Property


#End Region

#Region "Enums"

    Public Enum TableCol
        Id
        Number
        SupplierNumber
        SupplierName
        DateCreated
        DateExpectCollect
        PoNumber
        DrlNumber
        Value
        IsPrintedEntry
        IsPrintedRelease
        IsDeleted
        Rti
    End Enum

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all non released returns into Headers collection of this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadNonReleasedReturns()

        Try
            Dim retHeader As New BOPurchases.cReturnHeader(Oasys3DB)
            retHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, retHeader.DrlNumber, "000000")
            retHeader.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            retHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, retHeader.IsDeleted, False)
            _Headers = retHeader.LoadMatches

            'LoadSuppliers()

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all released returns into Headers collection of this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadReleasedReturns()

        Try
            Dim retHeader As New BOPurchases.cReturnHeader(Oasys3DB)
            retHeader.AddLoadFilter(clsOasys3DB.eOperator.pNotEquals, retHeader.DrlNumber, "000000")
            retHeader.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            retHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, retHeader.IsDeleted, False)
            _Headers = retHeader.LoadMatches

            'LoadSuppliers()

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all lines and stock into the line Stock object for this instance. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadLinesAndStock()

        Try
            Dim stocks As New BOStock.cStock(Oasys3DB)
            stocks.LoadStockItems(Lines.SkuNumbers)

            For Each lin As cReturnLine In _Lines
                For Each stock As BOStock.cStock In stocks.Stocks
                    If lin.SKUNumber.Value = stock.SkuNumber.Value Then lin.Stock = stock
                Next
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads open returns into Table object of this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub TableLoadOpenReturns()

        Try
            Dim sb As New StringBuilder("EXEC " & My.Resources.Procs.ReturnsSelectHeadersOpen)
            _table = Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnLoad, ex)
        End Try

    End Sub




    ''' <summary>
    ''' Sets this instance as a new return for given values. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="SupplierNumber"></param>
    ''' <param name="DateCollection"></param>
    ''' <remarks></remarks>
    Public Sub CreateReturn(ByVal SupplierNumber As String, ByVal DateCollection As Date)

        Try
            ClearLists()
            _ID.Value = 0
            _Number.Value = "000000"
            _SupplierNumber.Value = SupplierNumber
            _DateCreated.Value = Now.Date
            _DateExpectCollect.Value = DateCollection
            _DrlNumber.Value = "000000"
            _Value.Value = 0
            _PrintNeededEntry.Value = True
            _PrintNeededRelease.Value = True
            _Lines = New List(Of cReturnLine)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnCreate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Adds new line or updates existing line for this header. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="Stock"></param>
    ''' <param name="ReasonCode"></param>
    ''' <param name="Quantity"></param>
    ''' <remarks></remarks>
    Public Sub AddLine(ByVal Stock As BOStock.cStock, ByVal ReasonCode As String, ByVal Quantity As Integer)

        Try
            For Each l As cReturnLine In Lines
                If l.SKUNumber.Value = Stock.SkuNumber.Value And l.ReasonCode.Value = ReasonCode Then
                    l.QtyReturned.Value += Quantity
                    Exit Sub
                End If
            Next

            'create new line
            Dim lin As New cReturnLine(Oasys3DB)
            lin.ReasonCode.Value = ReasonCode
            lin.SKUNumber.Value = Stock.SkuNumber.Value
            lin.Cost.Value = Stock.CostPrice.Value
            lin.Price.Value = Stock.NormalSellPrice.Value
            lin.QtyReturned.Value = Quantity
            _Lines.Add(lin)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnCreate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Adds new line or updates existing line for this header. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <param name="cost"></param>
    ''' <param name="Price"></param>
    ''' <param name="qty"></param>
    ''' <param name="ReasonCode"></param>
    ''' <remarks></remarks>
    Public Sub AddLine(ByVal skuNumber As String, ByVal cost As Decimal, ByVal Price As Decimal, ByVal qty As Integer, ByVal reasonCode As String, Optional ByVal headerId As Integer = 0)

        Try
            For Each l As cReturnLine In Lines
                If l.SKUNumber.Value = skuNumber And l.ReasonCode.Value = reasonCode Then
                    l.QtyReturned.Value += qty
                    Exit Sub
                End If
            Next

            'create new line
            Dim lin As New cReturnLine(Oasys3DB)

            lin.HeaderID.Value = headerId
            lin.ReasonCode.Value = reasonCode
            lin.SKUNumber.Value = skuNumber
            lin.Cost.Value = cost
            lin.Price.Value = Price
            lin.QtyReturned.Value = qty

            _Lines.Add(lin)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnCreate, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Checks if return value is below given limit. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="LowLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsOverLowLimit(ByVal LowLevel As Decimal) As Boolean

        Try
            'get value of lines in collection
            _Value.Value = 0
            For Each l As cReturnLine In Lines
                _Value.Value += (l.QtyReturned.Value * l.Price.Value)
            Next

            If _Value.Value >= LowLevel Then Return True
            Return False

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnParams, ex)
        End Try

    End Function

    ''' <summary>
    ''' Inserts return and lines to database and updates stock items. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Insert(ByVal UserID As Integer)

        'check if already inserted
        If _ID.Value > 0 Then
            Throw New OasysDbException(My.Resources.Errors.ReturnAlreadyExists)
        End If


        Try
            'get value of lines in collection
            _Value.Value = 0
            For Each l As cReturnLine In Lines
                _Value.Value += (l.QtyReturned.Value * l.Price.Value)
            Next

            'start transaction
            Oasys3DB.BeginTransaction()

            'get next system number in required format
            Dim sysNumber As New BOSystem.cSystemNumbers(Oasys3DB)
            _Number.Value = CStr(sysNumber.GetReturnNumber)

            'insert header and all details
            SaveIfNew()

            'insert lines and update stock
            For Each lin As cReturnLine In _Lines
                lin.HeaderID.Value = _ID.Value
                lin.SaveIfNew()
                Dim key As String = _Number.Value & Space(6) & _ID.Value.ToString("000") & Space(5) & lin.ID.Value
                lin.Stock.UpdateReturnCreate(lin.QtyReturned.Value, key, UserID)
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.ReturnSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Update return as deleted. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UpdateDeleted(ByVal UserID As Integer)

        Try
            _IsDeleted.Value = True

            'start transaction
            Oasys3DB.BeginTransaction()

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, _IsDeleted.Value)
            SetDBKeys()
            Oasys3DB.Update()

            For Each lin As BOPurchases.cReturnLine In Lines
                Dim key As String = _Number.Value & Space(6) & _ID.Value.ToString("000") & Space(5) & lin.ID.Value
                lin.Stock.UpdateReturnDelete(lin.QtyReturned.Value, key, UserID)
            Next

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.ReturnDelete, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Releases return, creating DRL and updating return and stock items. 
    ''' Throws Oasys exception on error
    ''' </summary>
    ''' <param name="UserID"></param>
    ''' <param name="Comment"></param>
    ''' <remarks></remarks>
    Public Sub ReleaseReturn(ByVal UserID As Integer, ByVal Comment As String)

        Try
            'create DRL header and lines
            Dim drlHeader As New cDrlHeader(Oasys3DB)
            drlHeader.Type.Value = "3"
            drlHeader.Value.Value = _Value.Value
            drlHeader.AssignedDate.Value = Now
            drlHeader.POReturnID.Value = _ID.Value
            drlHeader.EmployeeID.Value = UserID.ToString("000")
            drlHeader.Comment.Value = Comment
            drlHeader.RetSupplierNumber.Value = _SupplierNumber.Value
            drlHeader.RetDate.Value = _DateCreated.Value
            drlHeader.RetNumber.Value = _Number.Value
            For Each lin As cReturnLine In Lines
                drlHeader.AddDetail(lin)
            Next

            'start transaction
            Oasys3DB.BeginTransaction()

            'save drl header and details
            If drlHeader.SaveIfNew Then
                'update return header
                _DrlNumber.Value = drlHeader.Number.Value
                _DateExpectCollect.Value = Now
                _PrintNeededRelease.Value = True

                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
                Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, _DrlNumber.Value)
                Oasys3DB.SetColumnAndValueParameter(_DateExpectCollect.ColumnName, _DateExpectCollect.Value)
                Oasys3DB.SetColumnAndValueParameter(_PrintNeededRelease.ColumnName, _PrintNeededRelease.Value)
                SetDBKeys()
                Oasys3DB.Update()

                'update stock records
                For Each lin As BOPurchases.cReturnLine In _Lines
                    Dim key As String = _Number.Value & Space(6) & _ID.Value.ToString("000") & Space(5) & lin.ID.Value
                    lin.Stock.UpdateReturnRelease(lin.QtyReturned.Value, key, UserID)
                Next
            End If

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.ReturnRelease, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Updates the print entry flag as given. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="Printed"></param>
    ''' <remarks></remarks>
    Public Sub UpdatePrintedEntry(ByVal Printed As Boolean)

        Try
            _PrintNeededEntry.Value = Printed

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_PrintNeededEntry.ColumnName, _PrintNeededEntry.Value)
            SetDBKeys()
            Oasys3DB.Update()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Updates the print released flag as given. Throws Oasys exception on error
    ''' </summary>
    ''' <param name="Printed"></param>
    ''' <remarks></remarks>
    Public Sub UpdatePrintedRelease(ByVal Printed As Boolean)

        Try
            _PrintNeededRelease.Value = Printed

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_PrintNeededRelease.ColumnName, _PrintNeededRelease.Value)
            SetDBKeys()
            Oasys3DB.Update()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.ReturnSave, ex)
        End Try

    End Sub

    Public Sub Update(ByVal UserID As Integer, HeaderID As Integer, Value As Decimal)

        If _ID.Value = Nothing Then
            Throw New OasysDbException(My.Resources.Errors.ReturnSave)
        End If

        Try
            Oasys3DB.BeginTransaction()

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_Value.ColumnName, _Value.Value)
            SetDBKeys()
            Oasys3DB.Update()

            Dim LinesToDelete As New List(Of cReturnLine)
            Dim l As New cReturnLine(Oasys3DB)
            l.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, l.HeaderID, _ID.Value)
            LinesToDelete = l.LoadMatches
            For Each lin As BOPurchases.cReturnLine In LinesToDelete
                Dim key As String = _Number.Value & Space(6) & _ID.Value.ToString("000") & Space(5) & lin.ID.Value
                lin.Stock.UpdateReturnDelete(lin.QtyReturned.Value, key, UserID)
            Next

            Dim allLines As New cReturnLine(Oasys3DB)
            allLines.DeleteLines(HeaderID)

            SaveIfExists()
            For Each lin As cReturnLine In _Lines
                lin.SaveIfNew()
                Dim key As String = _Number.Value & Space(6) & _ID.Value.ToString("000") & Space(5) & lin.ID.Value
                lin.Stock.UpdateReturnCreate(lin.QtyReturned.Value, key, UserID)
            Next

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.ReturnSave, ex)
        End Try

    End Sub

    Public Sub ClearLines()
        _Lines = New List(Of cReturnLine)
    End Sub

#End Region

End Class
