﻿Imports System.Data

Public Class StkmasOnorRepository

    Public Overridable Function GetPurlinRecord(ByRef conn As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal skuNumber As String, ByVal purchaseOrderNumber As String) As DataTable
        Dim DR As Data.Odbc.OdbcDataReader
        Dim DT As New DataTable

        Using com As New Data.Odbc.OdbcCommand()
            com.Connection = conn.Connection
            com.CommandText = "{CALL usp_GetPurLineOnOrderValue( ?, ? )}"
            com.CommandType = CommandType.StoredProcedure
            com.Parameters.Add("skuNumber", Odbc.OdbcType.Char, 6).Direction = ParameterDirection.Input
            com.Parameters.Add("poNumber", Odbc.OdbcType.Char, 6).Direction = ParameterDirection.Input
            com.Parameters(0).Value = skuNumber
            com.Parameters(1).Value = purchaseOrderNumber
            com.Transaction = conn.OdbcTransaction
            DR = com.ExecuteReader()
            If DR.HasRows Then
                DT.Load(DR)
                DR.Close()
                Return DT
            End If
        End Using
        Return Nothing
    End Function
End Class
