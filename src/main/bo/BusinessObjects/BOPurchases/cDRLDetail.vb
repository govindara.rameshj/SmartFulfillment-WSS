﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("SaveIBTOut_UnitTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
"9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
"3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
"97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
"54e0a4a4")> 

<Serializable()> Public Class cDrlDetail
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DRLDET"
        BOFields.Add(_DrlNumber)
        BOFields.Add(_SequenceNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_OrderQty)
        BOFields.Add(_ReceivedQty)
        BOFields.Add(_OrderPrice)
        BOFields.Add(_BulkSkuDesc)
        BOFields.Add(_BulkSkuProdCode)
        BOFields.Add(_SkuPrice)
        BOFields.Add(_IbtInOutQty)
        BOFields.Add(_ReturnQty)
        BOFields.Add(_POLineNumber)
        BOFields.Add(_BackOrdQty)
        BOFields.Add(_RTI)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cDrlDetail)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cDrlDetail)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cDrlDetail))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cDrlDetail(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _DrlNumber As New ColField(Of String)("NUMB", "", "DRL Number", True, False)
    Private _SequenceNumber As New ColField(Of String)("SEQN", "", "Sequence Number", True, False)
    Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU Number", False, False)
    Private _ReasonCode As New ColField(Of String)("ReasonCode", "00", "Reason Code", False, False)
    Private _OrderQty As New ColField(Of Decimal)("ORDQ", 0, "Order Qty", False, False)
    Private _ReceivedQty As New ColField(Of Decimal)("RECQ", 0, "Received Qty", False, False)
    Private _OrderPrice As New ColField(Of Decimal)("ORDP", 0, "Order Price", False, False, 2)
    Private _BulkSkuDesc As New ColField(Of String)("BDES", "", "Bulk SKU Desc", False, False)
    Private _BulkSkuProdCode As New ColField(Of String)("BSPC", "", "Bulk SKU Prod Code", False, False)
    Private _SkuPrice As New ColField(Of Decimal)("PRIC", 0, "SKU Price", False, False, 2)
    Private _IbtInOutQty As New ColField(Of Decimal)("IBTQ", 0, "IBT In/Out", False, False, 2)
    Private _ReturnQty As New ColField(Of Decimal)("RETQ", 0, "Return Qty", False, False)
    Private _POLineNumber As New ColField(Of Integer)("POLN", 0, "PO Line Number", False, False)
    Private _BackOrdQty As New ColField(Of Decimal)("FOLQ", 0, "Back Ord Qty", False, False)
    Private _RTI As New ColField(Of String)("RTI", "N", "RTI", False, False)

#End Region

#Region "Field Properties"

    Public Property DrlNumber() As ColField(Of String)
        Get
            DrlNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property SequenceNumber() As ColField(Of String)
        Get
            SequenceNumber = _SequenceNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SequenceNumber = value
        End Set
    End Property
    Public Property SkuNumber() As ColField(Of String)
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of String)
        Get
            Return _ReasonCode
        End Get
        Set(ByVal value As ColField(Of String))
            _ReasonCode = value
        End Set
    End Property
    Public Property OrderQty() As ColField(Of Decimal)
        Get
            OrderQty = _OrderQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderQty = value
        End Set
    End Property
    Public Property ReceivedQty() As ColField(Of Decimal)
        Get
            ReceivedQty = _ReceivedQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReceivedQty = value
        End Set
    End Property
    Public Property OrderPrice() As ColField(Of Decimal)
        Get
            OrderPrice = _OrderPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderPrice = value
        End Set
    End Property
    Public Property BulkSkuDesc() As ColField(Of String)
        Get
            BulkSkuDesc = _BulkSkuDesc
        End Get
        Set(ByVal value As ColField(Of String))
            _BulkSkuDesc = value
        End Set
    End Property
    Public Property BulkSkuProdCode() As ColField(Of String)
        Get
            BulkSkuProdCode = _BulkSkuProdCode
        End Get
        Set(ByVal value As ColField(Of String))
            _BulkSkuProdCode = value
        End Set
    End Property
    Public Property SkuPrice() As ColField(Of Decimal)
        Get
            SkuPrice = _SkuPrice
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SkuPrice = value
        End Set
    End Property
    Public Property IbtInOutQty() As ColField(Of Decimal)
        Get
            IbtInOutQty = _IbtInOutQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _IbtInOutQty = value
        End Set
    End Property
    Public Property ReturnQty() As ColField(Of Decimal)
        Get
            ReturnQty = _ReturnQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ReturnQty = value
        End Set
    End Property
    Public Property POLineNumber() As ColField(Of Integer)
        Get
            POLineNumber = _POLineNumber
        End Get
        Set(ByVal value As ColField(Of Integer))
            _POLineNumber = value
        End Set
    End Property
    Public Property BackOrdQty() As ColField(Of Decimal)
        Get
            BackOrdQty = _BackOrdQty
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _BackOrdQty = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _Stock As BOStock.cStock = Nothing
    Private _OriginalQty As Integer = 0

    Public Property Stock() As BOStock.cStock
        Get
            If _Stock Is Nothing Then
                _Stock = New BOStock.cStock(Oasys3DB)
                _Stock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Stock.SkuNumber, _SkuNumber.Value)
                _Stock.LoadMatches()
            End If
            Return _Stock
        End Get
        Set(ByVal value As BOStock.cStock)
            _Stock = value
        End Set
    End Property

    Public ReadOnly Property OriginalQty() As Integer
        Get
            Return _OriginalQty
        End Get
    End Property

    Public Function SaveRecord(ByVal nextDrlno As Integer, ByVal partno As String, ByVal qty As Integer, ByVal sequence As String, ByVal price As Decimal) As Boolean

        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, nextDrlno.ToString("######0").PadLeft(6, "0"c))
        Oasys3DB.SetColumnAndValueParameter(_SkuNumber.ColumnName, partno)
        Oasys3DB.SetColumnAndValueParameter(_SequenceNumber.ColumnName, sequence)
        Oasys3DB.SetColumnAndValueParameter(_SkuPrice.ColumnName, price)
        Oasys3DB.SetColumnAndValueParameter(_IbtInOutQty.ColumnName, qty)
        Oasys3DB.SetColumnAndValueParameter(_OrderQty.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_ReceivedQty.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_OrderPrice.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_ReturnQty.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_POLineNumber.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_BackOrdQty.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, "N")

        NoRecs = Oasys3DB.Insert()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError

    End Function

    Public Function UpdateMaintained() As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ReceivedQty.ColumnName, _ReceivedQty.Value)
        SetDBKeys()

        If Oasys3DB.Update > 0 Then Return True
        Return False

    End Function

    Public Function UpdateRTI() As Boolean

        _RTI.Value = "S"
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, _RTI.Value)
        SetDBKeys()
        If Oasys3DB.Update() > 0 Then Return True
        Return False

    End Function


#End Region

End Class
