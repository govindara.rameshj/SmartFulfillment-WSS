﻿<Serializable()> Public Class cPurchaseHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub
    Public Sub New(ByRef Supplier As cSupplierMaster)
        MyBase.New(Supplier.Oasys3DB)
        Start()
        _Supplier = Supplier

        'get purchase order number and insert as deleted
        Dim sysNumbers As New BOSystem.cSystemNumbers(Oasys3DB)
        _PONumber.Value = sysNumbers.GetPONumber.ToString.PadLeft(6, "0"c)
        _SupplierNumber.Value = _Supplier.Number.Value
        _SupplierBBC.Value = _Supplier.OrderDetails.BBC.Value
        _SupplierTradanet.Value = _Supplier.OrderDetails.Tradanet.Value
        _SoqNumber.Value = _Supplier.SoqNumber.Value
        _SourceEntry.Value = IIf(_Supplier.SoqNumber.Value = "000000", "E", "S").ToString
        _DateOrderCreated.Value = Now.Date
        _IsDeleted.Value = True

        'insert header and get identity
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pInsert)
        SetDBFieldValues(clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.Insert()
        GetDBIdentity()

        'create new lines using supplier order stock temp qty
        For Each stock As BOStock.cStock In Supplier.OrderStocks
            If stock.TempQty > 0 Then Line(stock).OrderQty.Value = stock.TempQty
        Next

    End Sub

    Public Overrides Sub Start()

        TableName = "PURHDR"
        BOFields.Add(_PurchaseHeaderID)
        BOFields.Add(_PONumber)
        BOFields.Add(_HONumber)
        BOFields.Add(_SoqNumber)
        BOFields.Add(_SupplierNumber)
        BOFields.Add(_SupplierTradanet)
        BOFields.Add(_SupplierBBC)
        BOFields.Add(_DateOrderCreated)
        BOFields.Add(_DateOrderDue)
        BOFields.Add(_ReleaseNumber)
        BOFields.Add(_RaiserInitials)
        BOFields.Add(_SourceEntry)
        BOFields.Add(_OrderQty)
        BOFields.Add(_OrderQtyCartons)
        BOFields.Add(_OrderValue)
        BOFields.Add(_PrintFlag)
        BOFields.Add(_PrintReprints)
        BOFields.Add(_DrlNumber)
        BOFields.Add(_ConsignNumber)
        BOFields.Add(_ReceivedComplete)
        BOFields.Add(_ReceivedPartial)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_CommBBCPrepped)
        BOFields.Add(_Confirmation)
        BOFields.Add(_ReasonCode)
        BOFields.Add(_Message1)
        BOFields.Add(_Message2)
        BOFields.Add(_IssueFileValidated)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPurchaseHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPurchaseHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPurchaseHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()

            'Dim ds As DataSet = Oasys3DB.Query(count)
            Dim ds As DataSet = GetData(count)

            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1

                        'Dim BO As New cPurchaseHeader(Oasys3DB)
                        Dim BO As cPurchaseHeader = GetNewInstance()

                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)

                    Next

            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("TKEY") : _PurchaseHeaderID.Value = CInt(drRow(dcColumn))
                    Case ("NUMB") : _PONumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("HONO") : _HONumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("SOQN") : _SoqNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("RNUM") : _DrlNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("PNUM") : _ConsignNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("SUPP") : _SupplierNumber.Value = CStr(drRow(dcColumn))
                    Case ("TNET") : _SupplierTradanet.Value = CBool(drRow(dcColumn))
                    Case ("BBCC") : _SupplierBBC.Value = CStr(drRow(dcColumn))
                    Case ("ODAT") : _DateOrderCreated.Value = CDate(drRow(dcColumn))

                    Case ("DDAT") : _DateOrderDue.Value = CDate(drRow(dcColumn))
                    Case ("RELN") : _ReleaseNumber.Value = drRow(dcColumn).ToString.PadLeft(2, "0"c)
                    Case ("RAIS") : _RaiserInitials.Value = CStr(drRow(dcColumn))
                    Case ("SRCE") : _SourceEntry.Value = CStr(drRow(dcColumn))
                    Case ("QTYO") : _OrderQty.Value = CInt(drRow(dcColumn))
                    Case ("NOCR") : _OrderQtyCartons.Value = CInt(drRow(dcColumn))
                    Case ("VALU") : _OrderValue.Value = CDec(drRow(dcColumn))
                    Case ("PRFL") : _PrintFlag.Value = CStr(drRow(dcColumn))
                    Case ("NORE") : _PrintReprints.Value = CInt(drRow(dcColumn))
                    Case ("RCOM") : _ReceivedComplete.Value = CBool(drRow(dcColumn))

                    Case ("RPAR") : _ReceivedPartial.Value = CBool(drRow(dcColumn))
                    Case ("DELM") : _IsDeleted.Value = CBool(drRow(dcColumn))
                    Case ("COMM") : _CommBBCPrepped.Value = CBool(drRow(dcColumn))
                    Case ("CONF") : _Confirmation.Value = CStr(drRow(dcColumn))
                    Case ("REAS") : _ReasonCode.Value = CInt(drRow(dcColumn))
                    Case ("MES1") : _Message1.Value = CStr(drRow(dcColumn))
                    Case ("MES2") : _Message2.Value = CStr(drRow(dcColumn))
                    Case ("OKIS") : _IssueFileValidated.Value = CBool(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

    Friend Overridable Function GetData(ByVal count As Integer) As DataSet

        Dim ds As DataSet = Oasys3DB.Query(count)
        Return ds

    End Function

    Friend Overridable Function GetNewInstance() As cPurchaseHeader

        Return New cPurchaseHeader(Oasys3DB)

    End Function

#Region "Fields"

    Private _PurchaseHeaderID As New ColField(Of Integer)("TKEY", 0, "ID", True, True)
    Private _PONumber As New ColField(Of String)("NUMB", "000000", "PO Number", False, False)
    Private _HONumber As New ColField(Of String)("HONO", "000000", "HO Number", False, False)
    Private _SoqNumber As New ColField(Of String)("SOQN", "000000", "SOQ Number", False, False)
    Private _DrlNumber As New ColField(Of String)("RNUM", "000000", "DRL Number", False, False)
    Private _ConsignNumber As New ColField(Of String)("PNUM", "000000", "Consignment Number", False, False)
    Private _SupplierNumber As New ColField(Of String)("SUPP", "", "Supplier Number", False, False)
    Private _SupplierTradanet As New ColField(Of Boolean)("TNET", False, "Is Supplier Tradanet", False, False)
    Private _SupplierBBC As New ColField(Of String)("BBCC", "", "Supplier BBC Code", False, False)
    Private _DateOrderCreated As New ColField(Of Date)("ODAT", Nothing, "Date Order Created", False, False)
    Private _DateOrderDue As New ColField(Of Date)("DDAT", Nothing, "Date Order Due", False, False)
    Private _ReleaseNumber As New ColField(Of String)("RELN", "00", "Release Number", False, False)
    Private _RaiserInitials As New ColField(Of String)("RAIS", "", "Raiser Initials", False, False)
    Private _SourceEntry As New ColField(Of String)("SRCE", "", "Source Entry", False, False)
    Private _OrderQty As New ColField(Of Integer)("QTYO", 0, "Order Qty", False, False)
    Private _OrderQtyCartons As New ColField(Of Integer)("NOCR", 0, "Order Qty Cartons", False, False)
    Private _OrderValue As New ColField(Of Decimal)("VALU", 0, "Order Value", False, False, 2)
    Private _PrintFlag As New ColField(Of String)("PRFL", "N", "Print Flag", False, False)
    Private _PrintReprints As New ColField(Of Integer)("NORE", 0, "Print Reprints", False, False)
    Private _ReceivedComplete As New ColField(Of Boolean)("RCOM", False, "Received Complete", False, False)
    Private _ReceivedPartial As New ColField(Of Boolean)("RPAR", False, "Received Partial", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELM", False, "Is Deleted", False, False)
    Private _CommBBCPrepped As New ColField(Of Boolean)("COMM", False, "Comm BBC Prepped", False, False)
    Private _Confirmation As New ColField(Of String)("CONF", "", "Confirmation", False, False)
    Private _ReasonCode As New ColField(Of Integer)("REAS", 0, "Reason Code", False, False)
    Private _Message1 As New ColField(Of String)("MES1", "", "Message 1", False, False)
    Private _Message2 As New ColField(Of String)("MES2", "", "Message 2", False, False)
    Private _IssueFileValidated As New ColField(Of Boolean)("OKIS", False, "Issue File Validated", False, False)

#End Region

#Region "Public Properties"
    Public Property PurchaseHeaderID() As ColField(Of Integer)
        Get
            PurchaseHeaderID = _PurchaseHeaderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PurchaseHeaderID = value
        End Set
    End Property
    Public Property PONumber() As ColField(Of String)
        Get
            PONumber = _PONumber
        End Get
        Set(ByVal value As ColField(Of String))
            _PONumber = value
        End Set
    End Property
    Public Property HONumber() As ColField(Of String)
        Get
            HONumber = _HONumber
        End Get
        Set(ByVal value As ColField(Of String))
            _HONumber = value
        End Set
    End Property
    Public Property SOQNumber() As ColField(Of String)
        Get
            SOQNumber = _SoqNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SoqNumber = value
        End Set
    End Property
    Public Property DRLNumber() As ColField(Of String)
        Get
            DRLNumber = _DrlNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _DrlNumber = value
        End Set
    End Property
    Public Property ConsignNumber() As ColField(Of String)
        Get
            ConsignNumber = _ConsignNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ConsignNumber = value
        End Set
    End Property
    Public Property SupplierNumber() As ColField(Of String)
        Get
            SupplierNumber = _SupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierNumber = value
        End Set
    End Property
    Public Property SupplierTradanet() As ColField(Of Boolean)
        Get
            SupplierTradanet = _SupplierTradanet
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _SupplierTradanet = value
        End Set
    End Property
    Public Property SupplierBBC() As ColField(Of String)
        Get
            SupplierBBC = _SupplierBBC
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierBBC = value
        End Set
    End Property
    Public Property DateOrderCreated() As ColField(Of Date)
        Get
            DateOrderCreated = _DateOrderCreated
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateOrderCreated = value
        End Set
    End Property
    Public Property DateOrderDue() As ColField(Of Date)
        Get
            DateOrderDue = _DateOrderDue
        End Get
        Set(ByVal value As ColField(Of Date))
            _DateOrderDue = value
        End Set
    End Property
    Public Property ReleaseNumber() As ColField(Of String)
        Get
            ReleaseNumber = _ReleaseNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ReleaseNumber = value
        End Set
    End Property
    Public Property RaiserInitials() As ColField(Of String)
        Get
            RaiserInitials = _RaiserInitials
        End Get
        Set(ByVal value As ColField(Of String))
            _RaiserInitials = value
        End Set
    End Property
    Public Property SourceEntry() As ColField(Of String)
        Get
            SourceEntry = _SourceEntry
        End Get
        Set(ByVal value As ColField(Of String))
            _SourceEntry = value
        End Set
    End Property
    Public Property OrderQty() As ColField(Of Integer)
        Get
            OrderQty = _OrderQty
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderQty = value
        End Set
    End Property
    Public Property OrderQtyCartons() As ColField(Of Integer)
        Get
            OrderQtyCartons = _OrderQtyCartons
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OrderQtyCartons = value
        End Set
    End Property
    Public Property OrderValue() As ColField(Of Decimal)
        Get
            OrderValue = _OrderValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _OrderValue = value
        End Set
    End Property
    Public Property PrintFlag() As ColField(Of String)
        Get
            PrintFlag = _PrintFlag
        End Get
        Set(ByVal value As ColField(Of String))
            _PrintFlag = value
        End Set
    End Property
    Public Property PrintReprints() As ColField(Of Integer)
        Get
            PrintReprints = _PrintReprints
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PrintReprints = value
        End Set
    End Property
    Public Property ReceivedComplete() As ColField(Of Boolean)
        Get
            ReceivedComplete = _ReceivedComplete
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReceivedComplete = value
        End Set
    End Property
    Public Property ReceivedPartial() As ColField(Of Boolean)
        Get
            ReceivedPartial = _ReceivedPartial
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ReceivedPartial = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property CommBBCPrepped() As ColField(Of Boolean)
        Get
            CommBBCPrepped = _CommBBCPrepped
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommBBCPrepped = value
        End Set
    End Property
    Public Property Confirmation() As ColField(Of String)
        Get
            Confirmation = _Confirmation
        End Get
        Set(ByVal value As ColField(Of String))
            _Confirmation = value
        End Set
    End Property
    Public Property ReasonCode() As ColField(Of Integer)
        Get
            ReasonCode = _ReasonCode
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ReasonCode = value
        End Set
    End Property
    Public Property Message1() As ColField(Of String)
        Get
            Message1 = _Message1
        End Get
        Set(ByVal value As ColField(Of String))
            _Message1 = value
        End Set
    End Property
    Public Property Message2() As ColField(Of String)
        Get
            Message2 = _Message2
        End Get
        Set(ByVal value As ColField(Of String))
            _Message2 = value
        End Set
    End Property
    Public Property IssueFileValidated() As ColField(Of Boolean)
        Get
            IssueFileValidated = _IssueFileValidated
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IssueFileValidated = value
        End Set
    End Property
#End Region

#Region "Entities"
    Private _Orders As List(Of cPurchaseHeader) = Nothing
    Private _Lines As List(Of cPurchaseLine) = Nothing
    Private _Supplier As cSupplierMaster = Nothing
    Private _Consignment As cConsignMaster = Nothing
    Private _DrlHeaders As List(Of cDrlHeader) = Nothing

    Public Property Orders() As List(Of cPurchaseHeader)
        Get
            If _Orders Is Nothing Then _Orders = New List(Of cPurchaseHeader)
            Return _Orders
        End Get
        Set(ByVal value As List(Of cPurchaseHeader))
            _Orders = value
        End Set
    End Property
    Public Property Order(ByVal HeaderID As Integer) As cPurchaseHeader
        Get
            If _Orders Is Nothing Then Return Nothing
            For Each o As cPurchaseHeader In _Orders
                If o.PurchaseHeaderID.Value = HeaderID Then Return o
            Next
            Return Nothing
        End Get
        Set(ByVal value As cPurchaseHeader)
            If _Orders IsNot Nothing Then
                For Each o As cPurchaseHeader In _Orders
                    If o.PurchaseHeaderID.Value = HeaderID Then
                        o = value
                    End If
                Next
            End If
        End Set
    End Property

    Public Property Lines(Optional ByVal LoadStocks As Boolean = False) As List(Of cPurchaseLine)
        Get
            If _Lines Is Nothing Then
                Dim lin As New cPurchaseLine(Oasys3DB)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.HeaderIdentity, _PurchaseHeaderID.Value)
                _Lines = lin.LoadMatches
            End If

            If _Lines.Count > 0 And LoadStocks Then
                'get stocks for details and load into stock object
                Dim skuNumbers As New ArrayList
                For Each lin As cPurchaseLine In _Lines
                    If Not skuNumbers.Contains(lin.SkuNumber.Value) Then skuNumbers.Add(lin.SkuNumber.Value)
                Next

                Dim stocks As New BOStock.cStock(Oasys3DB)
                stocks.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, stocks.SkuNumber, skuNumbers)
                stocks.Stocks = stocks.LoadMatches

                For Each lin As cPurchaseLine In _Lines
                    For Each stock As BOStock.cStock In stocks.Stocks
                        If lin.SkuNumber.Value = stock.SkuNumber.Value Then
                            lin.Stock = stock
                            Exit For
                        End If
                    Next
                Next
            End If
            Return _Lines
        End Get
        Set(ByVal value As List(Of cPurchaseLine))
            _Lines = value
        End Set
    End Property
    Public Property Line(ByVal SKUNumber As String) As cPurchaseLine
        Get
            For Each lin As cPurchaseLine In Lines
                If lin.SkuNumber.Value = SKUNumber Then Return lin
            Next

            Dim newlin As New cPurchaseLine(Oasys3DB)
            newlin.HeaderIdentity.Value = _PurchaseHeaderID.Value
            newlin.SkuNumber.Value = SKUNumber
            _Lines.Add(newlin)
            Return newlin
        End Get
        Set(ByVal value As cPurchaseLine)
            If _Lines IsNot Nothing Then
                For Each lin As cPurchaseLine In _Lines
                    If lin.SkuNumber.Value = SKUNumber Then
                        lin = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Line(ByVal Stock As BOStock.cStock) As cPurchaseLine
        Get
            For Each lin As cPurchaseLine In Lines
                If lin.SkuNumber.Value = Stock.SkuNumber.Value Then Return lin
            Next

            Dim newlin As New cPurchaseLine(Oasys3DB)
            newlin.HeaderIdentity.Value = _PurchaseHeaderID.Value
            newlin.SkuNumber.Value = Stock.SkuNumber.Value
            newlin.SKUProductCode.Value = Stock.SupplierPartCode.Value
            newlin.OrderPrice.Value = Stock.NormalSellPrice.Value
            newlin.OrderCost.Value = Stock.CostPrice.Value
            newlin.DateLastOrder.Value = Stock.LastOrdered.Value
            newlin.DateLastReceipt.Value = Stock.LastReceived.Value
            newlin.Stock = Stock
            _Lines.Add(newlin)
            Return newlin
        End Get
        Set(ByVal value As cPurchaseLine)
            If _Lines IsNot Nothing Then
                For Each lin As cPurchaseLine In _Lines
                    If lin.SkuNumber.Value = Stock.SkuNumber.Value Then
                        lin = value
                    End If
                Next
            End If
        End Set
    End Property

    Public Property Supplier() As cSupplierMaster
        Get
            If _Supplier Is Nothing Then
                _Supplier = New cSupplierMaster(Oasys3DB)
                _Supplier.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Supplier.Number, _SupplierNumber.Value)
                _Supplier.LoadMatches()
            End If
            Return _Supplier
        End Get
        Set(ByVal value As cSupplierMaster)
            _Supplier = value
        End Set
    End Property
    Public Property Consignment() As cConsignMaster
        Get
            If _Consignment Is Nothing Then
                'Check whether need to create new consignment
                If _ConsignNumber.Value = "000000" Then
                    _Consignment = New BOPurchases.cConsignMaster(Oasys3DB)
                    _Consignment.PONumber.Value = _PONumber.Value
                    _Consignment.POReleaseNumber.Value = CDec(_ReleaseNumber.Value)
                    _Consignment.SupplierNumber.Value = _SupplierNumber.Value
                    _Consignment.DateConsigned.Value = Now.Date
                    Return _Consignment
                End If

                'load existing consignment
                _Consignment = New BOPurchases.cConsignMaster(Oasys3DB)
                _Consignment.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Consignment.Number, _ConsignNumber.Value)
                _Consignment.LoadMatches()

            End If
            Return _Consignment
        End Get
        Set(ByVal value As cConsignMaster)
            _Consignment = value
        End Set
    End Property
    Public Property DrlHeaders() As List(Of cDrlHeader)
        Get
            If _DrlHeaders Is Nothing Then
                Dim drl As New cDrlHeader(Oasys3DB)
                drl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, drl.PONumber, _PONumber.Value)
                _DrlHeaders = drl.LoadMatches
            End If
            Return _DrlHeaders
        End Get
        Set(ByVal value As List(Of cDrlHeader))
            _DrlHeaders = value
        End Set
    End Property
    Public Property DrlHeader(ByVal number As String) As cDrlHeader
        Get
            For Each drl As cDrlHeader In DrlHeaders
                If drl.Number.Value = number Then Return drl
            Next
            Return Nothing
        End Get
        Set(ByVal value As cDrlHeader)
            For Each drl As cDrlHeader In DrlHeaders
                If drl.Number.Value = number Then drl = value
            Next
        End Set
    End Property


#End Region

#Region "Methods"

    Public Sub UpdateOrdered()

        Try
            'start transaction
            Oasys3DB.BeginTransaction()

            'set header values
            _IsDeleted.Value = False
            _OrderQty.Value = 0
            _OrderQtyCartons.Value = 0
            _OrderValue.Value = 0

            For Each lin As cPurchaseLine In _Lines
                'update header values
                _OrderQty.Value += lin.OrderQty.Value
                _OrderQtyCartons.Value = CInt(_OrderQtyCartons.Value + (lin.OrderQty.Value / lin.Stock.SupplierPackSize.Value))
                _OrderValue.Value += (lin.OrderQty.Value * lin.OrderPrice.Value)

                'insert header identity
                lin.HeaderIdentity.Value = _PurchaseHeaderID.Value
                lin.SaveIfNew()

                'update stock associated with line item (held in line.stock property)
                lin.Stock.UpdateOrdered(lin.OrderQty.Value)
            Next

            'update header with total values
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IsDeleted.ColumnName, _IsDeleted.Value)
            Oasys3DB.SetColumnAndValueParameter(_OrderQty.ColumnName, _OrderQty.Value)
            Oasys3DB.SetColumnAndValueParameter(_OrderQtyCartons.ColumnName, _OrderQtyCartons.Value)
            Oasys3DB.SetColumnAndValueParameter(_OrderValue.ColumnName, _OrderValue.Value)
            Oasys3DB.SetColumnAndValueParameter(_DateOrderDue.ColumnName, _DateOrderDue.Value)
            SetDBKeys()
            Oasys3DB.Update()

            'update supplier as ordered and zero order level of stocks
            Supplier.UpdatePOOrdered(_OrderValue.Value, _OrderQty.Value)
            Dim stock As New BOStock.cStock(Oasys3DB)
            stock.SupplierNo.Value = Supplier.Number.Value
            stock.UpdateSupplierOrderLevelZero()

            'commit transaction
            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        End Try

    End Sub

    Public Function UpdatePOConsigned() As Boolean

        'insert consignment
        If _Consignment Is Nothing Then Return False
        _Consignment.SaveIfNew()
        _ConsignNumber.Value = _Consignment.Number.Value

        'update purchase header with consignment number
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ConsignNumber.ColumnName, _ConsignNumber.Value)
        SetDBKeys()
        Oasys3DB.Update()

        Return True

    End Function

    Public Function UpdateReceived(ByVal UserID As Integer, ByVal Comment As String, ByVal DeliveryNotes As String) As Boolean

        Try
            'create drl header for this issue
            Dim drl As New cDrlHeader(Oasys3DB)
            drl.EmployeeID.Value = UserID.ToString("000")
            drl.Comment.Value = Comment
            drl.POConsignNumber.Value = _ConsignNumber.Value
            drl.POSupplierBBC.Value = True
            drl.POSupplierNumber.Value = _SupplierNumber.Value
            drl.PONumber.Value = _PONumber.Value
            drl.POSOQNumber.Value = _SoqNumber.Value
            drl.POReturnID.Value = _PurchaseHeaderID.Value
            drl.Details = New List(Of cDrlDetail)

            Select Case _SupplierBBC.Value
                Case "W", "A", "C" : drl.POSupplierBBC.Value = True
                Case Else : drl.POSupplierBBC.Value = False
            End Select

            'set delivery notes
            Dim note() As String = DeliveryNotes.Split(","c)
            For index As Integer = 0 To 8
                If index <= note.GetUpperBound(0) Then
                    drl.PODeliveryNote(index + 1).Value = note(index).Trim
                End If
            Next

            'loop through lines and add details to drl if greater than zero amount received
            For Each lin As cPurchaseLine In Lines
                If lin.ReceivedQty.Value > 0 Then
                    drl.Value.Value += (drl.Detail(lin).ReceivedQty.Value * drl.Detail(lin).OrderPrice.Value)
                End If
            Next

            'save drl header and details
            drl.SaveIfNew()


            'increment release number
            Dim release As Integer = CInt(_ReleaseNumber.Value) + 1
            _ReleaseNumber.Value = release.ToString("00")
            _DrlNumber.Value = drl.Number.Value
            _ReceivedPartial.Value = True
            _ReceivedComplete.Value = True


            'update database
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_ReleaseNumber.ColumnName, _ReleaseNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, _DrlNumber.Value)
            Oasys3DB.SetColumnAndValueParameter(_ReceivedPartial.ColumnName, _ReceivedPartial.Value)
            Oasys3DB.SetColumnAndValueParameter(_ReceivedComplete.ColumnName, _ReceivedComplete.Value)
            SetDBKeys()
            Oasys3DB.Update()

            'update consigment as done if exists
            If _ConsignNumber.Value <> "000000" Then
                Consignment.UpdateDone()
            End If

            'update purchase order lines and associated stock items
            For Each lin As cPurchaseLine In _Lines
                If lin.ReceivedQty.Value > 0 Then
                    lin.UpdateReceived(UserID)
                    Dim key As String = _PONumber.Value & Space(1) & lin.PurchaseLineID.Value & Space(1) & drl.Number.Value & Space(1) & drl.Detail(lin.SkuNumber.Value).SequenceNumber.Value
                    lin.Stock.UpdateReceivedIssue(lin.OrderQty.Value, lin.ReceivedQty.Value, key, UserID)
                End If
            Next

            'commit transaction
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function UpdateReceivedIssue(ByVal IssueDrlNumber As String) As Boolean

        'increment release number
        Dim release As Integer = CInt(_ReleaseNumber.Value) + 1
        _ReleaseNumber.Value = release.ToString("00")
        _DrlNumber.Value = IssueDrlNumber
        '_ReceivedPartial.Value = True
        _ReceivedComplete.Value = True

        ''loop through lines and see if all lines have been received
        'For Each lin As cPurchaseLine In Lines
        '    If Not lin.DeleteFlag.Value Then
        '        _ReceivedComplete.Value = False
        '        Exit For
        '    End If
        'Next

        'update database
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_ReleaseNumber.ColumnName, _ReleaseNumber.Value)
        Oasys3DB.SetColumnAndValueParameter(_DrlNumber.ColumnName, _DrlNumber.Value)
        'Oasys3DB.SetColumnAndValueParameter(_ReceivedPartial.ColumnName, _ReceivedPartial.Value)
        Oasys3DB.SetColumnAndValueParameter(_ReceivedComplete.ColumnName, _ReceivedComplete.Value)
        SetDBKeys()
        Return (Oasys3DB.Update() > 0)

    End Function

    Public Function DeleteOldPos() As Boolean

        Try
            'get records in question
            ClearLists()
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pIn, _SupplierBBC, New String() {"W", "A", "C"})
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, _DateOrderDue, Now.Date)
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _IsDeleted, False)
            JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _ReceivedComplete, False)
            _Orders = LoadMatches()

            For Each po As cPurchaseHeader In _Orders
                If po.ReceivedPartial.Value Then Continue For

                Oasys3DB.BeginTransaction()

                po.Deleted.Value = True
                po.ReceivedComplete.Value = True
                po.SaveIfExists()

                For Each lin As cPurchaseLine In po.Lines(True)
                    lin.Stock.UpdateOrderQty(lin.OrderQty.Value * -1)
                    lin.DeleteFlag.Value = True
                    lin.SaveIfExists()
                Next

                po.Supplier.UpdatePoOutstanding(po.OrderValue.Value * -1, -1)

                Oasys3DB.CommitTransaction()
            Next

            Return True

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw ex
            Return False
        End Try

    End Function

    Public Function ReceiptCheck(ByVal PONumber As String, ByVal ReceiptNo As String) As Boolean

        Dim dsLines As DataSet
        Me.Oasys3DB.ClearAllParameters()
        Me.Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)

        Me.Oasys3DB.SetWhereParameter(_PONumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, PONumber)
        Me.Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Me.Oasys3DB.SetWhereParameter(_DrlNumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, ReceiptNo)

        dsLines = Oasys3DB.Query()

        If dsLines.Tables(0).Rows.Count > 0 Then
            ReceiptCheck = True
        Else
            ReceiptCheck = False
        End If



    End Function

    Public Function UpdateIssueFileUpdated(ByVal PONo As String) As Boolean
        Dim NoRecs As Integer = -1
        Dim SaveError As Boolean = False
        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_IssueFileValidated.ColumnName, True)
            Oasys3DB.SetWhereParameter(_PONumber.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, PONo)

            NoRecs = Oasys3DB.Update()
            If NoRecs > 0 Then
                SaveError = False
            Else
                SaveError = True
            End If

            Return SaveError

        Catch ex As Exception
            Trace.WriteLine("cIssueHeader - UpdateRecjectCode exception " & ex.Message)
            Return True
        End Try


    End Function

#End Region

End Class