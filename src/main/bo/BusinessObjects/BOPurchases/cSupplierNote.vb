﻿<Serializable()> Public Class cSupplierNote
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SUPNOT"
        BOFields.Add(_SupplierID)
        BOFields.Add(_Type)
        BOFields.Add(_Sequence)
        BOFields.Add(_Text)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSupplierNote)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSupplierNote)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSupplierNote))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSupplierNote(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"
    Private _SupplierID As New ColField(Of String)("SUPN", "", True, False, 1, 0, True, True, 1, 0, 0, 0, "", 0, "Supplier ID")
    Private _Type As New ColField(Of String)("TYPE", "", True, False, 1, 0, True, True, 2, 0, 0, 0, "", 0, "Type")
    Private _Sequence As New ColField(Of String)("SEQN", "", True, False, 1, 0, True, True, 3, 0, 0, 0, "", 0, "Sequence")
    Private _Text As New ColField(Of String)("TEXT", "", False, False, 1, 0, True, True, 4, 0, 0, 0, "", 0, "Text")
#End Region

#Region "Field Properties"

    Public Property SupplierID() As ColField(Of String)
        Get
            SupplierID = _SupplierID
        End Get
        Set(ByVal value As ColField(Of String))
            _SupplierID = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property SeqNo() As ColField(Of String)
        Get
            SeqNo = _Sequence
        End Get
        Set(ByVal value As ColField(Of String))
            _Sequence = value
        End Set
    End Property
    Public Property Text() As ColField(Of String)
        Get
            Text = _Text
        End Get
        Set(ByVal value As ColField(Of String))
            _Text = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _table As New DataTable

    Public Property Table() As DataTable
        Get
            Return _table
        End Get
        Set(ByVal value As DataTable)
            _table = value
        End Set
    End Property
#End Region

#Region "Enums"
    Public Enum TableCol
        SupplierNumber
        Type
        Sequence
        Text
    End Enum

#End Region

#Region "Methods"

    Public Function DeleteAll() As Boolean
        Oasys3DB.ExecuteSql("DELETE FROM SUPNOT WHERE SUPN > '     '")
    End Function

    Public Function GetSuppliersType999(ByVal Supno As String) As List(Of cSupplierNote)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _SupplierID, Supno)
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Type, "999")
        JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, _Sequence, "000")

        Return LoadMatches()

    End Function

    ''' <summary>
    ''' Loads view table into Table property of this instance.
    '''  Throws exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub TableLoadReturnPolicy(ByVal supplierNumber As Integer)

        Try
            Dim sb As New StringBuilder("EXEC " & My.Resources.Procs.SuppliersSelectReturnPolicy & Space(1) & supplierNumber)
            _table = Oasys3DB.ExecuteSql(sb.ToString).Tables(0)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

End Class
