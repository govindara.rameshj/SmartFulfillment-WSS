﻿Public Module mExtensions

    ''' <summary>
    ''' Returns arraylist of supplier numbers in collection
    ''' </summary>
    ''' <param name="Details"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Function GetNumbers(ByRef Details As List(Of cSupplierDetail)) As ArrayList

        Try
            Dim al As New ArrayList
            For Each det As cSupplierDetail In Details
                If Not al.Contains(det.SupplierNumber.Value) Then al.Add(det.SupplierNumber.Value)
            Next
            Return al

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Returns a datatable of 3 columns ('Number', 'Name', 'Display')
    ''' </summary>
    ''' <param name="Suppliers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Function GetNameNumberTable(ByRef Suppliers As List(Of cSupplierMaster)) As DataTable

        Try
            Dim dt As New DataTable
            dt.Columns.Add("Number", GetType(String))
            dt.Columns.Add("Name", GetType(String))
            dt.Columns.Add("Display", GetType(String))

            For Each supplier As cSupplierMaster In Suppliers
                dt.Rows.Add(supplier.Number.Value, supplier.Name.Value, supplier.Number.Value & Space(1) & supplier.Name.Value)
            Next

            Return dt

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Returns arraylist of sku numbers in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function SkuNumbers(ByRef lines As List(Of cReturnLine)) As ArrayList

        If lines Is Nothing Then Return Nothing
        Dim skus As New ArrayList
        For Each lin As cReturnLine In lines
            If Not skus.Contains(lin.SKUNumber.Value) Then skus.Add(lin.SKUNumber.Value)
        Next

        Return skus

    End Function


    <System.Runtime.CompilerServices.Extension()> Friend Function AverageSales(ByRef Periods As List(Of SoqPeriod)) As Decimal

        If Periods.Count = 0 Then Return 0
        Return CDec(Periods.Average(Function(p As SoqPeriod) p.Sales))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function AverageSalesAdjusted(ByRef Periods As List(Of SoqPeriod)) As Decimal

        If Periods.Count = 0 Then Return 0
        Return Periods.Average(Function(p As SoqPeriod) p.AdjustedSales)

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CountZeroSales(ByRef Periods As List(Of SoqPeriod)) As Integer

        Dim count As Integer = 0
        For Each period As SoqPeriod In Periods.Where(Function(p) p.Sales = 0)
            count += 1
        Next
        Return count

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CountZeroSalesConsecutive(ByRef Periods As List(Of SoqPeriod)) As Integer

        Dim count As Integer = 0
        For Each period As SoqPeriod In Periods
            If period.Sales = 0 Then
                count += 1
            Else
                Exit For
            End If
        Next
        Return count

    End Function


    <System.Runtime.CompilerServices.Extension()> Friend Function StdDevSales(ByRef Periods As List(Of SoqPeriod)) As Decimal

        If Periods.Count <= 1 Then Return 0
        Dim sumR As Double = 0
        Dim sumRR As Double = 0

        For Each period As SoqPeriod In Periods
            sumR += period.AdjustedSales
            sumRR += Math.Pow(period.AdjustedSales, 2)
        Next

        sumR = Math.Pow(sumR, 2)
        sumR /= Periods.Count

        Dim stdDev As Decimal = CDec(sumRR - sumR)
        stdDev /= (Periods.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function StdDevPeriods(ByRef Periods As List(Of SoqPeriod)) As Decimal

        If Periods.Count <= 1 Then Return 0
        Dim sumP As Double = 0
        Dim sumPP As Double = 0

        For Each period As SoqPeriod In Periods
            sumP += period.Index
            sumPP += Math.Pow(period.Index, 2)
        Next

        Dim stdDev As Decimal = CDec(sumPP - (Math.Pow(sumP, 2) / Periods.Count))
        stdDev /= (Periods.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function
    <System.Runtime.CompilerServices.Extension()> Friend Function CorrelationCoeff(ByRef Periods As List(Of SoqPeriod), ByVal StdDevPeriodSales As Decimal, ByVal StdDevPeriodPeriods As Decimal) As Decimal

        If Periods.Count <= 1 Then Return 0
        If StdDevPeriodSales * StdDevPeriodPeriods * (Periods.Count - 1) = 0 Then Return 0

        Dim sumR As Double = 0
        Dim sumP As Double = 0
        Dim sumRP As Double = 0

        For Each period As SoqPeriod In Periods
            sumR += period.AdjustedSales
            sumP += period.Index
            sumRP += (period.Index * period.Sales)
        Next

        Dim stdDev As Decimal = CDec(sumRP - (sumR * sumP / Periods.Count))
        stdDev /= (StdDevPeriodSales * StdDevPeriodPeriods * (Periods.Count - 1))
        Return stdDev

    End Function


    <System.Runtime.CompilerServices.Extension()> Friend Function ToStringCsv(ByRef al As ArrayList) As String

        Dim sb As New StringBuilder
        For Each value As Object In al
            If sb.Length > 0 Then sb.Append(",")
            sb.Append(value.ToString)
        Next

        Return sb.ToString

    End Function


End Module
