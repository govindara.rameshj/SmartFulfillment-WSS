﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("SaveIBTOut_UnitTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
"9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
"3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
"97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
"54e0a4a4")> 


<Serializable()> Public Class cDrlHeader
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DRLSUM"
        BOFields.Add(_Number)
        BOFields.Add(_Type)
        BOFields.Add(_Value)
        BOFields.Add(_AssignedDate)
        BOFields.Add(_CommedToHO)
        BOFields.Add(_POReturnID)
        BOFields.Add(_EmployeeID)
        BOFields.Add(_Comment)
        BOFields.Add(_POSupplierBBC)
        BOFields.Add(_POSupplierNumber)
        BOFields.Add(_POConsignNumber)
        BOFields.Add(_PONumber)
        BOFields.Add(_PODate)
        BOFields.Add(_POReleaseNumber)
        BOFields.Add(_POSoqNumber)
        BOFields.Add(_PODeliveryNote1)
        BOFields.Add(_PODeliveryNote2)
        BOFields.Add(_PODeliveryNote3)
        BOFields.Add(_PODeliveryNote4)
        BOFields.Add(_PODeliveryNote5)
        BOFields.Add(_PODeliveryNote6)
        BOFields.Add(_PODeliveryNote7)
        BOFields.Add(_PODeliveryNote8)
        BOFields.Add(_PODeliveryNote9)
        BOFields.Add(_ISTStoreNumber)
        BOFields.Add(_ISTKeyedIn)
        BOFields.Add(_ISTOutputReq)
        BOFields.Add(_RetSupplierNumber)
        BOFields.Add(_RetDate)
        BOFields.Add(_RetNumber)
        BOFields.Add(_ISTConsignNumber)
        BOFields.Add(_IsProjectSalesOrder)
        BOFields.Add(_RTI)
        BOFields.Add(_EmployeeNumb)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cDrlHeader)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cDrlHeader)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cDrlHeader))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cDrlHeader(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Function Save(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType) As Boolean

        Try
            Select Case eSave
                Case clsOasys3DB.eSqlQueryType.pInsert
                    'get next drlnumber from system numbers
                    Dim SysNumbers As New BOSystem.cSystemNumbers(Oasys3DB)
                    SysNumbers.LoadDrlNumber()
                    _Number.Value = SysNumbers.GetNumberString
                    SysNumbers.IncrementNumber()

                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, eSave)
                    SetDBFieldValues(clsOasys3DB.eSqlQueryType.pInsert)

                    If Oasys3DB.Insert <= 0 Then Return False
                    GetDBIdentity()

                    'insert all details
                    If _Details IsNot Nothing Then
                        For Each det As cDrlDetail In _Details
                            det.DrlNumber.Value = _Number.Value
                            det.SaveIfNew()
                        Next
                    End If

                Case clsOasys3DB.eSqlQueryType.pUpdate
                    Oasys3DB.ClearAllParameters()
                    Oasys3DB.SetTableParameter(TableName, eSave)
                    SetDBFieldValues(clsOasys3DB.eSqlQueryType.pUpdate)
                    SetDBKeys()

                    If Oasys3DB.Update <= 0 Then Return False

                Case Else : Return False
            End Select

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("NUMB") : _Number.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("TYPE") : _Type.Value = CStr(drRow(dcColumn))
                    Case ("VALU") : _Value.Value = CDec(drRow(dcColumn))
                    Case ("DATE1") : _AssignedDate.Value = CDate(drRow(dcColumn))
                    Case ("CLAS") : _ClassNumber.Value = CStr(drRow(dcColumn))
                    Case ("COMM") : _CommedToHO.Value = CBool(drRow(dcColumn))
                    Case ("TKEY") : _POReturnID.Value = CInt(drRow(dcColumn))
                    Case ("INIT") : _EmployeeID.Value = CStr(drRow(dcColumn))
                    Case ("INFO") : _Comment.Value = CStr(drRow(dcColumn))

                    Case ("0BBC") : _POSupplierBBC.Value = CBool(drRow(dcColumn))
                    Case ("0SUP") : _POSupplierNumber.Value = CStr(drRow(dcColumn))
                    Case ("0CON") : _POConsignNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("0PON") : _PONumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("0DAT") : _PODate.Value = CDate(drRow(dcColumn))
                    Case ("0REL") : _POReleaseNumber.Value = drRow(dcColumn).ToString.PadLeft(2, "0"c)
                    Case ("0SOQ") : _POSoqNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)

                    Case ("0DL1") : _PODeliveryNote1.Value = CStr(drRow(dcColumn))
                    Case ("0DL2") : _PODeliveryNote2.Value = CStr(drRow(dcColumn))
                    Case ("0DL3") : _PODeliveryNote3.Value = CStr(drRow(dcColumn))
                    Case ("0DL4") : _PODeliveryNote4.Value = CStr(drRow(dcColumn))
                    Case ("0DL5") : _PODeliveryNote5.Value = CStr(drRow(dcColumn))
                    Case ("0DL6") : _PODeliveryNote6.Value = CStr(drRow(dcColumn))
                    Case ("0DL7") : _PODeliveryNote7.Value = CStr(drRow(dcColumn))
                    Case ("0DL8") : _PODeliveryNote8.Value = CStr(drRow(dcColumn))
                    Case ("0DL9") : _PODeliveryNote9.Value = CStr(drRow(dcColumn))

                    Case ("1STR") : _ISTStoreNumber.Value = drRow(dcColumn).ToString.PadLeft(3, "0"c)
                    Case ("1IBT") : _ISTKeyedIn.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("1PRT") : _ISTOutputReq.Value = CBool(drRow(dcColumn))
                    Case ("1CON") : _ISTConsignNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)

                    Case ("3SUP") : _RetSupplierNumber.Value = CStr(drRow(dcColumn))
                    Case ("3DAT") : _RetDate.Value = CDate(drRow(dcColumn))
                    Case ("3PON") : _RetNumber.Value = drRow(dcColumn).ToString.PadLeft(6, "0"c)
                    Case ("IPSO") : _IsProjectSalesOrder.Value = CBool(drRow(dcColumn))
                    Case ("RTI") : _RTI.Value = CStr(drRow(dcColumn))
                    Case ("EmployeeId") : _EmployeeNumb.Value = CStr(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _Number As New ColField(Of String)("NUMB", "000000", "Number", True, False)
    Private _Type As New ColField(Of String)("TYPE", "0", "Type", False, False)
    Private _Value As New ColField(Of Decimal)("VALU", 0, "Value", False, False, 2)
    Private _AssignedDate As New ColField(Of Date)("DATE1", Now.Date, "Assigned Date", False, False)
    Private _ClassNumber As New ColField(Of String)("CLAS", "00", False, False)
    Private _CommedToHO As New ColField(Of Boolean)("COMM", False, "Commed To HO", False, False)
    Private _POReturnID As New ColField(Of Integer)("TKEY", 0, "PO Return ID", False, False)
    Private _EmployeeID As New ColField(Of String)("INIT", "", "Employee Initials", False, False)
    Private _Comment As New ColField(Of String)("INFO", "", "Comment", False, False)
    Private _POSupplierBBC As New ColField(Of Boolean)("0BBC", False, "PO Supplier BBC", False, False)
    Private _POSupplierNumber As New ColField(Of String)("0SUP", "00000", "PO Supplier Number", False, False)
    Private _POConsignNumber As New ColField(Of String)("0CON", "000000", "PO Consign Number", False, False)
    Private _PONumber As New ColField(Of String)("0PON", "000000", "PO Number", False, False)
    Private _PODate As New ColField(Of Date)("0DAT", Nothing, "PO Date", False, False)
    Private _POReleaseNumber As New ColField(Of String)("0REL", "00", "PO Release Number", False, False)
    Private _POSoqNumber As New ColField(Of String)("0SOQ", "000000", "POSOQ Number", False, False)
    Private _PODeliveryNote1 As New ColField(Of String)("0DL1", "", "PO Delivery Note 1", False, False)
    Private _PODeliveryNote2 As New ColField(Of String)("0DL2", "", "PO Delivery Note 2", False, False)
    Private _PODeliveryNote3 As New ColField(Of String)("0DL3", "", "PO Delivery Note 3", False, False)
    Private _PODeliveryNote4 As New ColField(Of String)("0DL4", "", "PO Delivery Note 4", False, False)
    Private _PODeliveryNote5 As New ColField(Of String)("0DL5", "", "PO Delivery Note 5", False, False)
    Private _PODeliveryNote6 As New ColField(Of String)("0DL6", "", "PO Delivery Note 6", False, False)
    Private _PODeliveryNote7 As New ColField(Of String)("0DL7", "", "PO Delivery Note 7", False, False)
    Private _PODeliveryNote8 As New ColField(Of String)("0DL8", "", "PO Delivery Note 8", False, False)
    Private _PODeliveryNote9 As New ColField(Of String)("0DL9", "", "PO Delivery Note 9", False, False)
    Private _ISTStoreNumber As New ColField(Of String)("1STR", "000", "IST Store Number", False, False)
    Private _ISTKeyedIn As New ColField(Of String)("1IBT", "000000", "IST Keyed In", False, False)
    Private _ISTOutputReq As New ColField(Of Boolean)("1PRT", False, "IST Output Req", False, False)
    Private _ISTConsignNumber As New ColField(Of String)("1CON", "000000", "IST Consign Number", False, False)
    Private _RetSupplierNumber As New ColField(Of String)("3SUP", "00000", "Return Supplier Number", False, False)
    Private _RetDate As New ColField(Of Date)("3DAT", Nothing, "Return Date", False, False)
    Private _RetNumber As New ColField(Of String)("3PON", "000000", "Return Number", False, False)
    Private _IsProjectSalesOrder As New ColField(Of Boolean)("IPSO", False, "Is Project Sales Order", False, False)
    Private _RTI As New ColField(Of String)("RTI", "N", "RTI", False, False)
    Private _EmployeeNumb As New ColField(Of String)("EmployeeId", "", "Employee ID", False, False)


#End Region

#Region "Field Properties"

    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Type = _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Value = _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property ClassNumber() As ColField(Of String)
        Get
            ClassNumber = _ClassNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ClassNumber = value
        End Set
    End Property
    Public Property AssignedDate() As ColField(Of Date)
        Get
            AssignedDate = _AssignedDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _AssignedDate = value
        End Set
    End Property
    Public Property CommedToHO() As ColField(Of Boolean)
        Get
            CommedToHO = _CommedToHO
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _CommedToHO = value
        End Set
    End Property
    Public Property POReturnID() As ColField(Of Integer)
        Get
            POReturnID = _POReturnID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _POReturnID = value
        End Set
    End Property
    Public Property EmployeeID() As ColField(Of String)
        Get
            EmployeeID = _EmployeeID
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeID = value
        End Set
    End Property
    Public Property Comment() As ColField(Of String)
        Get
            Comment = _Comment
        End Get
        Set(ByVal value As ColField(Of String))
            _Comment = value
        End Set
    End Property
    Public Property POSupplierBBC() As ColField(Of Boolean)
        Get
            POSupplierBBC = _POSupplierBBC
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _POSupplierBBC = value
        End Set
    End Property
    Public Property POSupplierNumber() As ColField(Of String)
        Get
            POSupplierNumber = _POSupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _POSupplierNumber = value
        End Set
    End Property
    Public Property POConsignNumber() As ColField(Of String)
        Get
            POConsignNumber = _POConsignNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _POConsignNumber = value
        End Set
    End Property
    Public Property PONumber() As ColField(Of String)
        Get
            PONumber = _PONumber
        End Get
        Set(ByVal value As ColField(Of String))
            _PONumber = value
        End Set
    End Property
    Public Property PODate() As ColField(Of Date)
        Get
            PODate = _PODate
        End Get
        Set(ByVal value As ColField(Of Date))
            _PODate = value
        End Set
    End Property
    Public Property POReleaseNumber() As ColField(Of String)
        Get
            POReleaseNumber = _POReleaseNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _POReleaseNumber = value
        End Set
    End Property
    Public Property POSOQNumber() As ColField(Of String)
        Get
            POSOQNumber = _POSoqNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _POSoqNumber = value
        End Set
    End Property
    Public Property PODeliveryNote(ByVal Index As Integer) As ColField(Of String)
        Get
            'Extract Column Name
            Dim ColName As String = PODeliveryNote1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of String))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of String))
            Dim ColName As String = PODeliveryNote1.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 1) & Index.ToString

            For Each field In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of String)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property PODeliveryNote1() As ColField(Of String)
        Get
            PODeliveryNote1 = _PODeliveryNote1
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote1 = value
        End Set
    End Property
    Public Property PODeliveryNote2() As ColField(Of String)
        Get
            PODeliveryNote2 = _PODeliveryNote2
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote2 = value
        End Set
    End Property
    Public Property PODeliveryNote3() As ColField(Of String)
        Get
            PODeliveryNote3 = _PODeliveryNote3
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote3 = value
        End Set
    End Property
    Public Property PODeliveryNote4() As ColField(Of String)
        Get
            PODeliveryNote4 = _PODeliveryNote4
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote4 = value
        End Set
    End Property
    Public Property PODeliveryNote5() As ColField(Of String)
        Get
            PODeliveryNote5 = _PODeliveryNote5
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote5 = value
        End Set
    End Property
    Public Property PODeliveryNote6() As ColField(Of String)
        Get
            PODeliveryNote6 = _PODeliveryNote6
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote6 = value
        End Set
    End Property
    Public Property PODeliveryNote7() As ColField(Of String)
        Get
            PODeliveryNote7 = _PODeliveryNote7
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote7 = value
        End Set
    End Property
    Public Property PODeliveryNote8() As ColField(Of String)
        Get
            PODeliveryNote8 = _PODeliveryNote8
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote8 = value
        End Set
    End Property
    Public Property PODeliveryNote9() As ColField(Of String)
        Get
            PODeliveryNote9 = _PODeliveryNote9
        End Get
        Set(ByVal value As ColField(Of String))
            _PODeliveryNote9 = value
        End Set
    End Property
    Public Property ISTStoreNumber() As ColField(Of String)
        Get
            ISTStoreNumber = _ISTStoreNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ISTStoreNumber = value
        End Set
    End Property
    Public Property ISTKeyedIn() As ColField(Of String)
        Get
            ISTKeyedIn = _ISTKeyedIn
        End Get
        Set(ByVal value As ColField(Of String))
            _ISTKeyedIn = value
        End Set
    End Property
    Public Property ISTOutputReq() As ColField(Of Boolean)
        Get
            ISTOutputReq = _ISTOutputReq
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _ISTOutputReq = value
        End Set
    End Property
    Public Property ISTConsignNumber() As ColField(Of String)
        Get
            ISTConsignNumber = _ISTConsignNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _ISTConsignNumber = value
        End Set
    End Property
    Public Property RetSupplierNumber() As ColField(Of String)
        Get
            RetSupplierNumber = _RetSupplierNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _RetSupplierNumber = value
        End Set
    End Property
    Public Property RetDate() As ColField(Of Date)
        Get
            RetDate = _RetDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _RetDate = value
        End Set
    End Property
    Public Property RetNumber() As ColField(Of String)
        Get
            RetNumber = _RetNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _RetNumber = value
        End Set
    End Property
    Public Property IsProjectSalesOrder() As ColField(Of Boolean)
        Get
            IsProjectSalesOrder = _IsProjectSalesOrder
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsProjectSalesOrder = value
        End Set
    End Property
    Public Property RTI() As ColField(Of String)
        Get
            Return _RTI
        End Get
        Set(ByVal value As ColField(Of String))
            _RTI = value
        End Set
    End Property
    Public Property EmployeeNumb() As ColField(Of String)
        Get
            EmployeeNumb = _EmployeeNumb
        End Get
        Set(ByVal value As ColField(Of String))
            _EmployeeNumb = value
        End Set
    End Property
#End Region

#Region "Entities"
    Private _Details As List(Of cDrlDetail) = Nothing
    Private _Headers As List(Of cDrlHeader) = Nothing
    Public colItem As New List(Of cDrlDetail)

    Public Property Headers() As List(Of cDrlHeader)
        Get
            Return _Headers
        End Get
        Set(ByVal value As List(Of cDrlHeader))
            _Headers = value
        End Set
    End Property
    Public Property Header(ByVal DRLNumber As String) As cDrlHeader
        Get
            If _Headers Is Nothing Then _Headers = New List(Of cDrlHeader)
            For Each h As cDrlHeader In _Headers
                If h.Number.Value = DRLNumber Then Return h
            Next
            Return New cDrlHeader(Oasys3DB)
        End Get
        Set(ByVal value As cDrlHeader)
            If _Headers IsNot Nothing Then
                For Each h As cDrlHeader In _Headers
                    If h.Number.Value = DRLNumber Then
                        h = value
                    End If
                Next
            End If
        End Set
    End Property

    Public Property Details() As List(Of cDrlDetail)
        Get
            If _Details Is Nothing Then 'load existing details
                Dim det As New cDrlDetail(Oasys3DB)
                det.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, det.DrlNumber, _Number.Value)
                _Details = det.LoadMatches
            End If
            Return _Details
        End Get
        Set(ByVal value As List(Of cDrlDetail))
            If _Details Is Nothing Then _Details = New List(Of cDrlDetail)
            _Details = value
        End Set
    End Property
    Public Property Detail(ByVal line As cPurchaseLine) As cDrlDetail
        Get
            For Each det As cDrlDetail In Details
                If det.POLineNumber.Value = line.PurchaseLineID.Value Then Return det
            Next

            Dim newDet As New cDrlDetail(Oasys3DB)
            newDet.DrlNumber.Value = _Number.Value
            newDet.SkuNumber.Value = line.SkuNumber.Value
            newDet.SequenceNumber.Value = (Details.Count + 1).ToString("0000")
            newDet.OrderQty.Value = line.OrderQty.Value
            newDet.ReceivedQty.Value = line.ReceivedQty.Value
            newDet.OrderPrice.Value = line.OrderPrice.Value
            newDet.SkuPrice.Value = line.Stock.NormalSellPrice.Value
            newDet.POLineNumber.Value = line.PurchaseLineID.Value
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cDrlDetail)
            If _Details IsNot Nothing Then
                For Each d As cDrlDetail In _Details
                    If d.POLineNumber.Value = line.PurchaseLineID.Value Then
                        d = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Detail(ByVal line As cIssueLine) As cDrlDetail
        Get
            For Each det As cDrlDetail In Details
                If det.POLineNumber.Value = CDbl(line.LineNumber.Value) Then Return det
            Next

            Dim newDet As New cDrlDetail(Oasys3DB)
            newDet.DrlNumber.Value = _Number.Value
            newDet.SkuNumber.Value = line.SkuNumber.Value
            newDet.SequenceNumber.Value = (Details.Count + 1).ToString("0000")
            newDet.OrderQty.Value = line.QtyOrdered.Value
            newDet.ReceivedQty.Value = line.QtyIssued.Value
            newDet.POLineNumber.Value = CInt(line.LineNumber.Value)
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cDrlDetail)
            If _Details IsNot Nothing Then
                For Each d As cDrlDetail In _Details
                    If d.POLineNumber.Value = CDbl(line.LineNumber.Value) Then
                        d = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Detail(ByVal Stock As BOStock.cStock, Optional ByVal ReasonCode As String = "00") As cDrlDetail
        Get
            For Each det As cDrlDetail In Details
                If det.SkuNumber.Value = Stock.SkuNumber.Value And det.ReasonCode.Value = ReasonCode Then Return det
            Next

            Dim newDet As New cDrlDetail(Oasys3DB)
            newDet.DrlNumber.Value = _Number.Value
            newDet.SkuNumber.Value = Stock.SkuNumber.Value
            newDet.SequenceNumber.Value = (Details.Count + 1).ToString("0000")
            newDet.OrderPrice.Value = Stock.NormalSellPrice.Value
            newDet.SkuPrice.Value = Stock.NormalSellPrice.Value
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cDrlDetail)
            If _Details IsNot Nothing Then
                For Each det As cDrlDetail In _Details
                    If det.SkuNumber.Value = Stock.SkuNumber.Value And det.ReasonCode.Value = ReasonCode Then det = value
                Next
            End If
        End Set
    End Property
    Public Property Detail(ByVal SkuNumber As String, Optional ByVal ReasonCode As String = "00") As cDrlDetail
        Get
            For Each det As cDrlDetail In Details
                If det.SkuNumber.Value = SkuNumber And det.ReasonCode.Value = ReasonCode Then Return det
            Next

            Dim newDet As New cDrlDetail(Oasys3DB)
            newDet.DrlNumber.Value = _Number.Value
            newDet.SkuNumber.Value = SkuNumber
            newDet.ReasonCode.Value = ReasonCode
            newDet.SequenceNumber.Value = (Details.Count + 1).ToString("0000")
            _Details.Add(newDet)
            Return newDet
        End Get
        Set(ByVal value As cDrlDetail)
            If _Details IsNot Nothing Then
                For Each det As cDrlDetail In Details
                    If det.SkuNumber.Value = SkuNumber And det.ReasonCode.Value = ReasonCode Then
                        det = value
                    End If
                Next
            End If
        End Set
    End Property

#End Region

#Region "Events"
    Public Event PercentageDone(ByVal intpercentageDone As Integer)
#End Region

#Region "Methods"

    Public Sub AddDetail(ByVal line As cReturnLine)

        If _Details Is Nothing Then _Details = New List(Of cDrlDetail)

        Dim det As New cDrlDetail(Oasys3DB)
        det.DrlNumber.Value = _Number.Value
        det.SequenceNumber.Value = (_Details.Count + 1).ToString("0000")
        det.ReasonCode.Value = line.ReasonCode.Value
        det.SkuNumber.Value = line.SKUNumber.Value
        det.SkuPrice.Value = line.Price.Value
        det.ReturnQty.Value = line.QtyReturned.Value
        _Details.Add(det)

    End Sub

    Public Function UpdateCommed() As Boolean

        _CommedToHO.Value = True
        _RTI.Value = "S"

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Oasys3DB.SetColumnAndValueParameter(_CommedToHO.ColumnName, _CommedToHO.Value)
        Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, _RTI.Value)
        SetDBKeys()

        If Oasys3DB.Update() > 0 Then
            For Each detail As BOPurchases.cDrlDetail In Details
                detail.RTI.Value = "S"
                detail.UpdateRTI()
            Next
            Return True
        End If

        Return False

    End Function

    Public Sub UpdateMaintained(ByVal UserID As Integer)

        Try
            Oasys3DB.BeginTransaction()
            Dim value As Decimal = 0

            'loop through details and either insert or update
            For Each det As cDrlDetail In Details
                If det.OriginalQty = 0 Then
                    det.SaveIfNew()
                    det.Stock.UpdateReceived(0, CInt(det.ReceivedQty.Value), _Number.Value & Space(1) & det.SequenceNumber.Value, UserID)

                ElseIf det.ReceivedQty.Value <> det.OriginalQty Then
                    det.SaveIfExists()
                    det.Stock.UpdateReceivedMaintained(CInt((det.ReceivedQty.Value - det.OriginalQty)), _Number.Value & Space(1) & det.SequenceNumber.Value, UserID)

                End If
                value += (det.ReceivedQty.Value * det.OrderPrice.Value)
            Next

            _RTI.Value = "N"
            _Value.Value = value
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_Comment.ColumnName, _Comment.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote1.ColumnName, _PODeliveryNote1.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote2.ColumnName, _PODeliveryNote2.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote3.ColumnName, _PODeliveryNote3.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote4.ColumnName, _PODeliveryNote4.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote5.ColumnName, _PODeliveryNote5.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote6.ColumnName, _PODeliveryNote6.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote7.ColumnName, _PODeliveryNote7.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote8.ColumnName, _PODeliveryNote8.Value)
            Oasys3DB.SetColumnAndValueParameter(_PODeliveryNote9.ColumnName, _PODeliveryNote9.Value)
            Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, _RTI.Value)
            Oasys3DB.SetColumnAndValueParameter(_Value.ColumnName, _Value.Value)
            SetDBKeys()
            Oasys3DB.Update()

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw
        End Try

    End Sub

    Public Sub addItemToCollection(ByVal partno As String, ByVal sequence As String, ByVal qty As Integer, ByVal price As Decimal)
        Dim item As New BOPurchases.cDrlDetail(Oasys3DB)
        item.SkuNumber.Value = partno
        item.SequenceNumber.Value = sequence
        item.SkuPrice.Value = price
        item.IbtInOutQty.Value = qty
        colItem.Add(item)
    End Sub

    Public Sub ClearItemCollection()
        If colItem IsNot Nothing Then
            colItem.Clear()
        End If
    End Sub

    Public Function SaveIBTOut(ByVal comment As String, ByVal fromstore As String, ByVal totalvalue As Decimal, ByVal userid As Integer, ByVal drlno As String, ByVal IncludeInSales As Boolean) As Boolean

        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim initials As String
        Dim itemerr As Boolean
        Dim NextDrlNo As String
        Dim intCount As Integer = 0
        Dim intLastPercent As Integer = 0

        Dim BOsysno As New BOSystem.cSystemNumbers(Oasys3DB)
        Dim BOstk As New BOStock.cStock(Oasys3DB)
        Dim BOstkhis As New BOStock.cStockHistory(Oasys3DB)
        Dim BOUser As New BOSecurityProfile.cSystemUsers(Oasys3DB)

        BOUser.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOUser.ID, userid)
        If BOUser.LoadMatches.Count < 1 Then
            initials = String.Empty
        Else
            initials = BOUser.Initials.Value
        End If

        If drlno = String.Empty Then
            NextDrlNo = CStr(BOsysno.GetDRLNumber)
        Else
            NextDrlNo = drlno
        End If

        _Number.Value = NextDrlNo

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(_Number.ColumnName, NextDrlNo)
        Oasys3DB.SetColumnAndValueParameter(_Type.ColumnName, "2")
        Oasys3DB.SetColumnAndValueParameter(_AssignedDate.ColumnName, Date.Today)
        Oasys3DB.SetColumnAndValueParameter(_Comment.ColumnName, comment)
        Oasys3DB.SetColumnAndValueParameter(_ISTStoreNumber.ColumnName, fromstore)
        Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, "N")
        Oasys3DB.SetColumnAndValueParameter(_Value.ColumnName, totalvalue)
        Oasys3DB.SetColumnAndValueParameter(_EmployeeID.ColumnName, initials)
        Oasys3DB.SetColumnAndValueParameter(_ISTOutputReq.ColumnName, True)
        Oasys3DB.SetColumnAndValueParameter(_ClassNumber.ColumnName, "00")
        Oasys3DB.SetColumnAndValueParameter(_POReturnID.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_POSupplierNumber.ColumnName, "00000")
        Oasys3DB.SetColumnAndValueParameter(_POConsignNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_PONumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_POReleaseNumber.ColumnName, "00")
        Oasys3DB.SetColumnAndValueParameter(_POSoqNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_ISTKeyedIn.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_RetSupplierNumber.ColumnName, "00000")
        Oasys3DB.SetColumnAndValueParameter(_RetNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_ISTConsignNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_EmployeeNumb.ColumnName, userid)
        NoRecs = Oasys3DB.Insert()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
            For Each item As BOPurchases.cDrlDetail In colItem
                BOstk.AddLoadField(BOstk.SkuNumber)
                BOstk.AddLoadField(BOstk.MarkDownQuantity)
                BOstk.AddLoadField(BOstk.WriteOffQuantity)
                BOstk.AddLoadField(BOstk.StockOnHand)
                BOstk.AddLoadField(BOstk.UnitsSoldCurWkPlus1)
                BOstk.AddLoadField(BOstk.NormalSellPrice)
                BOstk.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOstk.SkuNumber, item.SkuNumber.Value)
                BOstk.LoadMatches()
                If IncludeInSales = True Then
                    BOstk.RecordSales(item.SkuNumber.Value, CInt(BOstk.UnitsSoldCurWkPlus1.Value + item.IbtInOutQty.Value))
                End If
                BOstk.AdjustStockOnHandOut(CInt(item.IbtInOutQty.Value), NextDrlNo & Space(1) & item.SequenceNumber.Value, userid, (item.IbtInOutQty.Value * item.SkuPrice.Value))
                itemerr = item.SaveRecord(CInt(NextDrlNo), item.SkuNumber.Value, CInt(item.IbtInOutQty.Value), item.SequenceNumber.Value, item.SkuPrice.Value)
                intCount = intCount + 1
                If intCount / colItem.Count <> intLastPercent Then
                    intLastPercent = CInt((intCount / colItem.Count) * 100)
                    RaiseEvent PercentageDone(intLastPercent)
                End If
                If itemerr = True Then
                    SaveError = True
                End If
            Next
        End If

        Return SaveError

    End Function

    Public Function SaveIBTin(ByVal comment As String, ByVal store As String, ByVal userid As Integer, ByVal consignmentno As String, ByVal selectedNo As Integer, ByVal totalvalue As Decimal) As Boolean

        Dim NoRecs As Integer
        Dim SaveError As Boolean
        Dim initials As String
        Dim itemerr As Boolean
        Dim NextDrlNo As Integer
        Dim intCount As Integer = 0
        Dim intLastPercent As Integer = 0

        Dim BOsysno As New BOSystem.cSystemNumbers(Oasys3DB)
        Dim BOstk As New BOStock.cStock(Oasys3DB)
        Dim BOUser As New BOSecurityProfile.cSystemUsers(Oasys3DB)
        Dim BOConmas As New cConsignMaster(Oasys3DB)

        BOUser.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOUser.ID, userid)
        If BOUser.LoadMatches.Count < 1 Then
            initials = String.Empty
        Else
            initials = BOUser.Initials.Value
        End If

        BOConmas.setDone(consignmentno, selectedNo)

        NextDrlNo = BOsysno.GetDRLNumber

        _Number.Value = CStr(NextDrlNo)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        Oasys3DB.SetColumnAndValueParameter(_Number.ColumnName, NextDrlNo.ToString("######0").PadLeft(6, "0"c))
        Oasys3DB.SetColumnAndValueParameter(_Type.ColumnName, "1")
        Oasys3DB.SetColumnAndValueParameter(_AssignedDate.ColumnName, Date.Today)
        Oasys3DB.SetColumnAndValueParameter(_Comment.ColumnName, comment)
        Oasys3DB.SetColumnAndValueParameter(_ISTStoreNumber.ColumnName, store)
        Oasys3DB.SetColumnAndValueParameter(_ISTKeyedIn.ColumnName, selectedNo.ToString("#####0").PadLeft(6, "0"c))
        Oasys3DB.SetColumnAndValueParameter(_ISTConsignNumber.ColumnName, consignmentno)
        Oasys3DB.SetColumnAndValueParameter(_Value.ColumnName, totalvalue)
        Oasys3DB.SetColumnAndValueParameter(_EmployeeID.ColumnName, initials)
        Oasys3DB.SetColumnAndValueParameter(_ClassNumber.ColumnName, "00")
        Oasys3DB.SetColumnAndValueParameter(_POReturnID.ColumnName, 0)
        Oasys3DB.SetColumnAndValueParameter(_POSupplierNumber.ColumnName, "00000")
        Oasys3DB.SetColumnAndValueParameter(_POConsignNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_PONumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_POReleaseNumber.ColumnName, "00")
        Oasys3DB.SetColumnAndValueParameter(_POSoqNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_RetSupplierNumber.ColumnName, "00000")
        Oasys3DB.SetColumnAndValueParameter(_RetNumber.ColumnName, "000000")
        Oasys3DB.SetColumnAndValueParameter(_RTI.ColumnName, "N")
        Oasys3DB.SetColumnAndValueParameter(_EmployeeNumb.ColumnName, userid)
        NoRecs = Oasys3DB.Insert()

        If NoRecs = -1 Then

            SaveError = True

        Else

            SaveError = False
            For Each item As cDrlDetail In colItem

                BOstk.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOstk.SkuNumber, item.SkuNumber.Value)
                BOstk.LoadMatches()
                BOstk.AdjustStockOnHandIn(CInt(item.IbtInOutQty.Value), NextDrlNo.ToString("#####0").PadLeft(6, "0"c) & Space(1) & item.SequenceNumber.Value, userid, ((item.IbtInOutQty.Value * -1) * item.SkuPrice.Value))

                itemerr = item.SaveRecord(NextDrlNo, item.SkuNumber.Value, CInt(item.IbtInOutQty.Value * -1), item.SequenceNumber.Value, item.SkuPrice.Value)
                intCount = intCount + 1
                If intCount / colItem.Count <> intLastPercent Then
                    intLastPercent = CInt((intCount / colItem.Count) * 100)
                    RaiseEvent PercentageDone(intLastPercent)
                End If

                If itemerr = True Then
                    SaveError = True
                End If

            Next item

        End If

        Return SaveError

    End Function

    Public Function DRLN0Printed(ByVal drlno As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

        'this flag really means needs to be printed therefore set to false 
        Oasys3DB.SetColumnAndValueParameter(_ISTOutputReq.ColumnName, False)
        Oasys3DB.SetWhereParameter(_Number.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, drlno)
        Return (Oasys3DB.Update() <> -1)

    End Function

#End Region

End Class
