﻿Imports System.Data.SqlClient

Partial Class ReportDataSet
    Private _connectionString As String = String.Empty

    Public Sub New(ByVal connectionString As String)
        Me.New()
        _connectionString = connectionString
    End Sub

    ''' <summary>
    ''' Loads report, parameters and columns into dataset for given report id
    ''' </summary>
    ''' <param name="reportId"></param>
    ''' <remarks></remarks>
    Public Sub LoadReport(ByVal reportId As Integer)

        Using connection As New SqlConnection(_connectionString)
            Dim daReport As New ReportDataSetTableAdapters.ReportTableAdapter
            daReport.Connection = connection
            daReport.FillById(Me.Report, reportId)

            Dim daParams As New ReportDataSetTableAdapters.ReportParameterTableAdapter
            daParams.Connection = connection
            daParams.FillByReportId(Me.ReportParameter, reportId)

            Dim daColumns As New ReportDataSetTableAdapters.ReportColumnTableAdapter
            daColumns.Connection = connection
            daColumns.FillByReportId(Me.ReportColumn, reportId)
        End Using

    End Sub

End Class
