﻿Imports System.Data.SqlClient

Public Class Report
    Private _connectionString As String = String.Empty
    Private _reportId As Integer
    Private _text As String = String.Empty
    Private _title As String = String.Empty
    Private _isEditable As Boolean
    Private _parameters As New List(Of ReportParameter)
    Private _columns As New List(Of ReportColumn)
    Private _dataset As New DataSet

    Public ReadOnly Property Parameters() As List(Of ReportParameter)
        Get
            Return _parameters
        End Get
    End Property
    Public ReadOnly Property Columns() As List(Of ReportColumn)
        Get
            Return _columns
        End Get
    End Property
    Public ReadOnly Property DataSet() As DataSet
        Get
            Return _dataset
        End Get
    End Property
    Public ReadOnly Property Title() As String
        Get
            Return _title
        End Get
    End Property
    Public ReadOnly Property IsEditable() As Boolean
        Get
            Return _isEditable
        End Get
    End Property

    Public Sub New(ByVal connectionString As String, ByVal reportId As Integer)
        _connectionString = connectionString
        _reportId = reportId

        'get report to load from table
        Dim dsReports As New ReportDataSet(_connectionString)
        dsReports.LoadReport(_reportId)

        'check the type of report for report
        Dim rep As ReportDataSet.ReportRow = dsReports.Report.First
        If rep IsNot Nothing Then

            'set properties
            _text = rep.Text
            _title = rep.Title
            _isEditable = rep.IsEditable
            _parameters = New List(Of ReportParameter)
            _columns = New List(Of ReportColumn)

            'get parameters
            For Each parameter As ReportDataSet.ReportParameterRow In rep.GetReportParametersRows
                Dim p As New ReportParameter(parameter)
                _parameters.Add(p)
            Next

            'get report columns
            For Each column As ReportDataSet.ReportColumnRow In rep.GetReportColumnsRows
                Dim col As New ReportColumn(column)
                _columns.Add(col)
            Next

        End If

    End Sub

    ''' <summary>
    ''' Loads report data into dataset
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadDataSet()

        'load dataset from stored procedure using parameters
        Using connection As New SqlConnection(_connectionString)
            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = _text

            For Each param As ReportParameter In _parameters
                Dim parameter As New SqlParameter
                parameter.ParameterName = param.Name
                parameter.SqlDbType = param.SqlDbType
                parameter.Size = param.Size
                parameter.Direction = param.Direction
                parameter.Value = param.Value
                command.Parameters.Add(parameter)
            Next

            _dataset = New DataSet
            Dim da As New SqlDataAdapter(command)
            da.Fill(_dataset)
        End Using


    End Sub


End Class