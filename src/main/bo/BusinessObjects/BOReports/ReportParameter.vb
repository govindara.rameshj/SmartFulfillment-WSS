﻿Public Class ReportParameter
    Private _name As String = String.Empty
    Private _prompt As String = String.Empty
    Private _dbType As SqlDbType
    Private _size As Integer
    Private _direction As ParameterDirection
    Private _value As Object = Nothing

    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property
    Public ReadOnly Property Prompt() As String
        Get
            Return _prompt
        End Get
    End Property
    Public ReadOnly Property SqlDbType() As SqlDbType
        Get
            Return _dbType
        End Get
    End Property
    Public ReadOnly Property Size() As Integer
        Get
            Return _size
        End Get
    End Property
    Public ReadOnly Property Direction() As ParameterDirection
        Get
            Return _direction
        End Get
    End Property
    Public Property Value() As Object
        Get
            Return _value
        End Get
        Set(ByVal value As Object)
            _value = value
        End Set
    End Property

    Public Sub New(ByVal parameter As ReportDataSet.ReportParameterRow)

        _name = parameter.Name
        _prompt = parameter.Prompt
        _dbType = CType(parameter.DataType, Data.SqlDbType)
        _direction = CType(parameter.Direction, ParameterDirection)
        _size = parameter.Size

        'check for default value depedending on datatype
        If parameter.DefaultValue.Trim.Length > 0 Then
            Select Case _dbType
                Case SqlDbType.Date, SqlDbType.DateTime, SqlDbType.DateTime2, SqlDbType.DateTimeOffset, SqlDbType.SmallDateTime, SqlDbType.Time, SqlDbType.Timestamp
                    Select Case parameter.DefaultValue.ToLower
                        Case "now" : _value = Now.Date
                        Case Else : _value = CType(parameter.DefaultValue, DateTime)
                    End Select

                Case SqlDbType.Bit
                    Select Case parameter.DefaultValue.ToLower
                        Case "1", "true" : _value = 1
                        Case Else : _value = 0
                    End Select
            End Select
        End If

    End Sub

End Class