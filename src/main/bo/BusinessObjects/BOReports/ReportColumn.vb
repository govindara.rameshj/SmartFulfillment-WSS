﻿Public Class ReportColumn
    Private _level As Integer
    Private _name As String = String.Empty
    Private _display As String = String.Empty
    Private _minWidth As Integer
    Private _maxWidth As Integer
    Private _isVisible As Boolean
    Private _sumType As Int16

    Public ReadOnly Property Level() As Integer
        Get
            Return _level
        End Get
    End Property
    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property
    Public ReadOnly Property Display() As String
        Get
            Return _display
        End Get
    End Property
    Public ReadOnly Property MinWidth() As Integer
        Get
            Return _minWidth
        End Get
    End Property
    Public ReadOnly Property MaxWidth() As Integer
        Get
            Return _maxWidth
        End Get
    End Property
    Public ReadOnly Property IsVisible() As Boolean
        Get
            Return _isVisible
        End Get
    End Property
    Public ReadOnly Property SummaryType() As Int16
        Get
            Return _sumType
        End Get
    End Property

    Public Sub New(ByVal reportColumn As ReportDataSet.ReportColumnRow)
        _level = reportColumn.TableLevel
        _name = reportColumn.Name.Trim
        _display = reportColumn.DisplayName.Trim
        _minWidth = reportColumn.MinWidth
        _maxWidth = reportColumn.MaxWidth
        _isVisible = reportColumn.IsVisible
        If _display.Length = 0 Then _display = _name
        _sumType = CShort(reportColumn.SummaryType)
    End Sub

End Class