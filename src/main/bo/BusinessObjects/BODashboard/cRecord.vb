<Serializable()> Public Class cRecord
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DashboardData"
        BOFields.Add(_ID)
        BOFields.Add(_ActivityID)
        BOFields.Add(_EnteredDate)
        BOFields.Add(_EnteredTime)
        BOFields.Add(_Quantity)
        BOFields.Add(_Value)
        BOFields.Add(_DataDate)
        BOFields.Add(_DataTime)
        BOFields.Add(_Active)
        BOFields.Add(_IsInitialisation)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cRecord)

        LoadBORecords(count)

        Dim col As New List(Of cRecord)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cRecord))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cRecord(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Oasys3.DB.OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _ActivityID As New ColField(Of Integer)("ActivityID", 0, "Activity ID", False, False)
    Private _EnteredDate As New ColField(Of Date)("EnteredDate", Nothing, "Date Entered", False, False)
    Private _EnteredTime As New ColField(Of Date)("EnteredTime", Nothing, "Time Entered", False, False)
    Private _Quantity As New ColField(Of Integer)("Quantity", 0, "Quantity", False, False)
    Private _Value As New ColField(Of Decimal)("Value", 0, "Value", False, False, 2)
    Private _DataDate As New ColField(Of Date)("DataDate", Nothing, "Date Data", False, False)
    Private _DataTime As New ColField(Of Date)("DataTime", Nothing, "Data Time", False, False)
    Private _Active As New ColField(Of Boolean)("Active", True, "Active", False, False)
    Private _IsInitialisation As New ColField(Of Boolean)("IsInitialisation", False, "Is Initialisation", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property ActivityID() As ColField(Of Integer)
        Get
            Return _ActivityID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ActivityID = value
        End Set
    End Property
    Public Property EnteredDate() As ColField(Of Date)
        Get
            Return _EnteredDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _EnteredDate = value
        End Set
    End Property
    Public Property EnteredTime() As ColField(Of Date)
        Get
            Return _EnteredTime
        End Get
        Set(ByVal value As ColField(Of Date))
            _EnteredTime = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Integer)
        Get
            Return _Quantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Quantity = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Return _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property DataDate() As ColField(Of Date)
        Get
            Return _DataDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _DataDate = value
        End Set
    End Property
    Public Property DataTime() As ColField(Of Date)
        Get
            Return _DataTime
        End Get
        Set(ByVal value As ColField(Of Date))
            _DataTime = value
        End Set
    End Property
    Public Property Active() As ColField(Of Boolean)
        Get
            Return _Active
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _Active = value
        End Set
    End Property
    Public Property IsInitialisation() As ColField(Of Boolean)
        Get
            Return _IsInitialisation
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsInitialisation = value
        End Set
    End Property

#End Region

End Class