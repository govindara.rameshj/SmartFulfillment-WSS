﻿Public Module Extensions

    ''' <summary>
    ''' Returns total quantity of records for today
    ''' </summary>
    ''' <param name="Records"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function QuantityToday(ByVal Records As List(Of cRecord)) As Integer

        Dim total As Decimal = 0
        For Each rec As cRecord In Records
            If rec.DataDate.Value >= Now.Date Then total += rec.Quantity.Value
        Next
        Return CInt(total)

    End Function

    ''' <summary>
    ''' Returns total quantity of all records (default is week to date)
    ''' </summary>
    ''' <param name="Records"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function Quantity(ByVal Records As List(Of cRecord)) As Integer

        Return Records.Sum(Function(r As cRecord) r.Quantity.Value)

    End Function

    ''' <summary>
    ''' Returns total value of records for today
    ''' </summary>
    ''' <param name="Records"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function ValueToday(ByVal Records As List(Of cRecord)) As Decimal

        Dim total As Decimal = 0
        For Each rec As cRecord In Records
            If rec.DataDate.Value >= Now.Date Then total += rec.Value.Value
        Next
        Return total

    End Function

    ''' <summary>
    ''' Returns total value of all records (default is week to date)
    ''' </summary>
    ''' <param name="Records"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function Value(ByVal Records As List(Of cRecord)) As Decimal

        Return Records.Sum(Function(r As cRecord) r.Value.Value)

    End Function

    ''' <summary>
    ''' Returns last date
    ''' </summary>
    ''' <param name="Records"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function LastDate(ByVal Records As List(Of cRecord)) As Date

        If Records.Count = 0 Then Return Nothing

        Dim rec As cRecord = Records(0)
        For Each r As cRecord In Records
            If r.DataDate.Value >= rec.DataDate.Value Then
                rec = r
            End If
        Next
        Return rec.DataDate.Value

    End Function

    ''' <summary>
    ''' Loads records for each activity in list
    ''' </summary>
    ''' <param name="Activities"></param>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Sub LoadRecords(ByVal Activities As List(Of cActivity), ByVal endDate As Date)

        For Each act As cActivity In Activities
            act.LoadRecords(endDate)
        Next

    End Sub



End Module
