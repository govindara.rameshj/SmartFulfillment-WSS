<Serializable()> Public Class cGroup
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DashboardGroup"
        BOFields.Add(_ID)
        BOFields.Add(_Description)
        BOConstraints.Add(New Object() {_ID})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cGroup)

        LoadBORecords(count)

        Dim col As New List(Of cGroup)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cGroup))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cGroup(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Oasys3.DB.OasysDbException
            Throw
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Groups As List(Of cGroup) = Nothing
    Private _Activities As List(Of cActivity) = Nothing

    Public Property Groups() As List(Of cGroup)
        Get
            If _Groups Is Nothing Then
                Load()
            End If
            Return _Groups
        End Get
        Set(ByVal value As List(Of cGroup))
            _Groups = value
        End Set
    End Property
    Public Property Activities() As List(Of cActivity)
        Get
            If _Activities Is Nothing Then
                Dim act As New cActivity(Oasys3DB)
                act.AddLoadFilter(clsOasys3DB.eOperator.pEquals, act.GroupID, _ID.Value)
                act.SortBy(act.GroupSequence.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                _Activities = act.LoadMatches
            End If
            Return _Activities
        End Get
        Set(ByVal value As List(Of cActivity))
            _Activities = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all groups into the Groups collection for this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load()

        ClearLists()
        SortBy(_ID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        _Groups = LoadMatches()

    End Sub

    ''' <summary>
    ''' Returns whether any activity for this group has the quantity field visible
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QuantityVisible() As Boolean

        For Each act As cActivity In Activities
            If act.IsQuantity.Value Then Return True
        Next
        Return False

    End Function

    ''' <summary>
    ''' Returns whether any activity for this group has the value field visible
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TotalVisible() As Boolean

        For Each act As cActivity In Activities
            If act.IsValue.Value Then Return True
        Next
        Return False

    End Function

    ''' <summary>
    ''' Returns whether any activity for this group has the last date field visible
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LastDateVisible() As Boolean

        For Each act As cActivity In Activities
            If act.IsLastDate.Value Then Return True
        Next
        Return False

    End Function

    ''' <summary>
    ''' Initialises all activities for this group
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Initialise()

        For Each act As cActivity In Activities
            act.Initialise()
        Next

    End Sub

#End Region

End Class