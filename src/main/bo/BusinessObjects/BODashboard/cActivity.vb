<Serializable()> Public Class cActivity
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "DashboardActivity"
        BOFields.Add(_ID)
        BOFields.Add(_IDLink)
        BOFields.Add(_Description)
        BOFields.Add(_GroupID)
        BOFields.Add(_GroupSequence)
        BOFields.Add(_Formula)
        BOFields.Add(_FormatQuantity)
        BOFields.Add(_FormatValue)
        BOFields.Add(_HyperLink)
        BOFields.Add(_IsQuantity)
        BOFields.Add(_IsValue)
        BOFields.Add(_IsLastDate)
        BOFields.Add(_IsVisible)
        BOFields.Add(_IsWeekToDate)
        BOFields.Add(_InitialisingProcedure)

        BOConstraints.Add(New Object() {_ID})
        BOConstraints.Add(New Object() {_GroupID, _GroupSequence})

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cActivity)

        LoadBORecords(count)

        Dim col As New List(Of cActivity)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cActivity))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cActivity(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Oasys3.DB.OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(Me.GetType.ToString, ex)
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _IDLink As New ColField(Of Integer)("IDLink", 0, "ID Link", False, False)
    Private _Description As New ColField(Of String)("Description", "", "Description", False, False)
    Private _GroupID As New ColField(Of Integer)("GroupID", 0, "Group ID", False, False)
    Private _GroupSequence As New ColField(Of Integer)("GroupSequence", 0, "Group Sequence", False, False)
    Private _Formula As New ColField(Of String)("Formula", "", "Formula", False, False)
    Private _FormatQuantity As New ColField(Of String)("FormatQuantity", "", "Format Quantity", False, False)
    Private _FormatValue As New ColField(Of String)("FormatValue", "", "Format Value", False, False)
    Private _HyperLink As New ColField(Of String)("HyperLink", "", "HyperLink", False, False)
    Private _IsQuantity As New ColField(Of Boolean)("IsQuantity", True, "Is Quantity", False, False)
    Private _IsValue As New ColField(Of Boolean)("IsValue", True, "Is Total", False, False)
    Private _IsLastDate As New ColField(Of Boolean)("IsLastDate", False, "Is Last Date", False, False)
    Private _IsVisible As New ColField(Of Boolean)("IsVisible", True, "Is Visible", False, False)
    Private _IsWeekToDate As New ColField(Of Boolean)("IsWeekToDate", False, "Is Week to Date", False, False)
    Private _InitialisingProcedure As New ColField(Of String)("InitialisingProcedure", "", "Initialising Procedure", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            ID = _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property IDLink() As ColField(Of Integer)
        Get
            Return _IDLink
        End Get
        Set(ByVal value As ColField(Of Integer))
            _IDLink = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Return _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property GroupID() As ColField(Of Integer)
        Get
            Return _GroupID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _GroupID = value
        End Set
    End Property
    Public Property GroupSequence() As ColField(Of Integer)
        Get
            Return _GroupSequence
        End Get
        Set(ByVal value As ColField(Of Integer))
            _GroupSequence = value
        End Set
    End Property
    Public Property Formula() As ColField(Of String)
        Get
            Return _Formula
        End Get
        Set(ByVal value As ColField(Of String))
            _Formula = value
        End Set
    End Property
    Public Property FormatQuantity() As ColField(Of String)
        Get
            Return _FormatQuantity
        End Get
        Set(ByVal value As ColField(Of String))
            _FormatQuantity = value
        End Set
    End Property
    Public Property FormatValue() As ColField(Of String)
        Get
            Return _FormatValue
        End Get
        Set(ByVal value As ColField(Of String))
            _FormatValue = value
        End Set
    End Property
    Public Property HyperLink() As ColField(Of String)
        Get
            Return _HyperLink
        End Get
        Set(ByVal value As ColField(Of String))
            _HyperLink = value
        End Set
    End Property
    Public Property IsQuantity() As ColField(Of Boolean)
        Get
            Return _IsQuantity
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsQuantity = value
        End Set
    End Property
    Public Property IsValue() As ColField(Of Boolean)
        Get
            Return _IsValue
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsValue = value
        End Set
    End Property
    Public Property IsLastDate() As ColField(Of Boolean)
        Get
            Return _IsLastDate
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsLastDate = value
        End Set
    End Property
    Public Property IsVisible() As ColField(Of Boolean)
        Get
            Return _IsVisible
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsVisible = value
        End Set
    End Property
    Public Property IsWeekToDate() As ColField(Of Boolean)
        Get
            Return _IsWeekToDate
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsWeekToDate = value
        End Set
    End Property
    Public Property InitialisingProcedure() As ColField(Of String)
        Get
            Return _InitialisingProcedure
        End Get
        Set(ByVal value As ColField(Of String))
            _InitialisingProcedure = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Records As List(Of cRecord) = Nothing
    Public Property Records() As List(Of cRecord)
        Get
            Try
                If _Records Is Nothing Then
                    LoadRecords(Now.Date)
                End If
                Return _Records

            Catch ex As Exception
                Trace.WriteLine(ex.Message, Me.GetType.ToString)
                Return Nothing
            End Try

        End Get
        Set(ByVal value As List(Of cRecord))
            _Records = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads records for this activity into Records collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadRecords(ByVal endDate As Date)

        Dim rec As New cRecord(Oasys3DB)

        'add ID filter
        If _IDLink.Value <> 0 Then
            rec.AddLoadFilter(clsOasys3DB.eOperator.pEquals, rec.ActivityID, _IDLink.Value)
        Else
            rec.AddLoadFilter(clsOasys3DB.eOperator.pEquals, rec.ActivityID, _ID.Value)
        End If

        'check if is last date
        If _IsLastDate.Value Then
            rec.AddAggregateField(clsOasys3DB.eAggregates.pAggMax, rec.DataDate, rec.DataDate.ColumnName)
        Else
            'get last sunday for start date
            Dim startDate As Date = Now.Date
            Do While startDate.DayOfWeek <> DayOfWeek.Sunday
                startDate = startDate.AddDays(-1)
            Loop

            rec.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            rec.AddLoadFilter(clsOasys3DB.eOperator.pGreaterThanOrEquals, rec.DataDate, startDate)
            rec.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            rec.AddLoadFilter(clsOasys3DB.eOperator.pLessThanOrEquals, rec.DataDate, endDate)
        End If

        _Records = rec.LoadMatches

    End Sub

    ''' <summary>
    ''' Returns total quantity of activity (default is for week to date)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Quantity() As Integer

        Return Records.Quantity

    End Function

    ''' <summary>
    ''' Returns total quantity of activity for today
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QuantityToday() As Integer

        Return Records.QuantityToday

    End Function

    ''' <summary>
    ''' Returns total value of activity (default is for week to date)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Value() As Decimal

        Return Records.Value

    End Function

    ''' <summary>
    ''' Returns total value of activity for today
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValueToday() As Decimal

        Return Records.ValueToday

    End Function

    ''' <summary>
    ''' Returns last date of activity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LastDate() As Date

        Return Records.LastDate

    End Function

    ''' <summary>
    ''' Initialises this activity by executing relevant stored procedure
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Initialise()

        If _InitialisingProcedure.Value <> String.Empty Then
            Oasys3DB.ExecuteSql("EXEC " & _InitialisingProcedure.Value)
        End If

    End Sub


#End Region

End Class