﻿''' <summary>
''' 
''' </summary>
''' <history>
''' Author  : Partha Dutta
''' Date    : 02/09/2010
''' Referral: 354
''' Notes   : Used existing code and modified it to point to the "vision" tables
'''           Clean sheet solution would have used stored procedures rather than dynamic sequel
''' 
'''           Changed the "date" field to a proper DATE
'''           Removed unnecessary fields "_DateUpdated"
'''           Removed unnecessary function "InsertVisionRecords"
''' 
''' </history>
''' <remarks></remarks>
<Serializable()> Public Class cPVTotals
    Inherits cBaseClass

    Dim _Oasys3DB As Oasys3.DB.clsOasys3DB = Nothing

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        _Oasys3DB = oasys3DB
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "VisionTotal"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_CashierID)
        BOFields.Add(_TranTime)
        BOFields.Add(_TranType)
        BOFields.Add(_MiscReasonCode)
        BOFields.Add(_Description)
        BOFields.Add(_OrderNumber)
        BOFields.Add(_MerchandisingAmount)
        BOFields.Add(_NonMerchandisingAmount)
        BOFields.Add(_TotalDiscountAmount)
        BOFields.Add(_TotalTaxAmount)
        BOFields.Add(_TotalSaleAmount)
        BOFields.Add(_ColleagueDiscount)
        BOFields.Add(_CollegueDiscAmount)
        BOFields.Add(_Pivotal)

        'not being used
        'BOFields.Add(_DateUpdated)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPVTotals)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPVTotals)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPVTotals))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPVTotals(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

    Public Sub LoadFromBOOld(ByRef LoadFrom As cPvTotalsOld)

        Try
            Dim oldStringField As ColField(Of String)
            Dim convertDate As Date

            For Each oldField As Object In LoadFrom.BOFields
                If TypeOf (oldField) Is ColField(Of String) Then
                    oldStringField = CType(oldField, ColField(Of String))
                    With oldStringField
                        If .Value IsNot Nothing Then
                            Select Case .ColumnName
                                Case LoadFrom.TransDate.ColumnName
                                    ' This field actually stored as a date now
                                    Me.TransDate.Value = CDate(IIf(Date.TryParse(.Value, convertDate), convertDate, Nothing))
                                Case LoadFrom.TillID.ColumnName
                                    Me.TillID.Value = .Value
                                Case LoadFrom.TransactionNo.ColumnName
                                    Me.TransactionNo.Value = .Value
                                Case LoadFrom.CashierNo.ColumnName
                                    Me.CashierNo.Value = .Value
                                Case LoadFrom.MiscReasonCode.ColumnName
                                    Me.MiscReasonCode.Value = .Value
                                Case LoadFrom.Description.ColumnName
                                    Me.Description.Value = .Value
                                Case LoadFrom.OrderNo.ColumnName
                                    Me.OrderNo.Value = .Value
                                Case LoadFrom.MerchanisingAmt.ColumnName
                                    Me.MerchanisingAmt.Value = .Value
                                Case LoadFrom.NonMerchanisingAmt.ColumnName
                                    Me.NonMerchanisingAmt.Value = .Value
                                Case LoadFrom.TotalTaxAmt.ColumnName
                                    Me.TotalTaxAmt.Value = .Value
                                Case LoadFrom.TotalDiscountAmt.ColumnName
                                    Me.TotalDiscountAmt.Value = .Value
                                Case LoadFrom.TotalSaleAmt.ColumnName
                                    Me.TotalSaleAmt.Value = .Value
                                Case LoadFrom.ColleagueDiscount.ColumnName
                                    Me.ColleagueDiscount.Value = .Value
                                Case LoadFrom.CollegueDiscAmt.ColumnName
                                    Me.CollegueDiscAmt.Value = .Value
                                Case LoadFrom.Pivotal.ColumnName
                                    Me.Pivotal.Value = .Value
                            End Select
                        End If
                    End With
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Fields"

    'moved to vision tables, date field in now a proper date
    'Private _TranDate As New ColField(Of Date)("TranDate", "", "Transation Date", True, False)
    Private _TranDate As New ColField(Of Date)("TranDate", DateTime.MinValue, "Transation Date", True, False)

    Private _TillID As New ColField(Of String)("TillId", "", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TranNumber", "", "Transaction Number", True, False)
    'non key fields
    Private _CashierID As New ColField(Of String)("CashierId", "", "Cashier ID", False, False)
    Private _TranTime As New ColField(Of String)("TranDateTime", "", "Transation Time", False, False)
    Private _TranType As New ColField(Of String)("Type", "", "Transaction Type", False, False)
    Private _MiscReasonCode As New ColField(Of String)("ReasonCode", "", "Reason Code", False, False)
    Private _Description As New ColField(Of String)("ReasonDescription", "", "Reason Description", False, False)
    Private _OrderNumber As New ColField(Of String)("OrderNumber", "", "Order Number", False, False)
    Private _MerchandisingAmount As New ColField(Of String)("ValueMerchandising", "", "Merchandising Amount", False, False)
    Private _NonMerchandisingAmount As New ColField(Of String)("ValueNonMerchandising", "", "Non Merchandising Amount", False, False)
    Private _TotalTaxAmount As New ColField(Of String)("ValueTax", "", "Total Tax Amount", False, False)
    Private _TotalDiscountAmount As New ColField(Of String)("ValueDiscount", "", "Total Discount Amount", False, False)
    Private _TotalSaleAmount As New ColField(Of String)("Value", "", "Total Sale Amount", False, False)
    Private _ColleagueDiscount As New ColField(Of String)("IsColleagueDiscount", "", "Colleague Discount", False, False)
    Private _CollegueDiscAmount As New ColField(Of String)("ValueColleagueDiscount", "", "Colleague Discount Amount", False, False)
    Private _Pivotal As New ColField(Of String)("IsPivotal", "", "IsPivotal", False, False)
    'not being used
    'Private _DateUpdated As New ColField(Of String)("CBBU", "", "Date Updated", False, False)

    'have listed exiting  code to show mapping between PVTOTS and VisionTotal tables

    'Private _TranDate As New ColField(Of String)("DATE1", "", "Transation Date", True, False)
    'Private _TillID As New ColField(Of String)("TILL", "", "Till ID", True, False)
    'Private _TranNumber As New ColField(Of String)("TRAN", "", "Transaction Number", True, False)

    'Private _CashierID As New ColField(Of String)("CASH", "", "Cashier ID", False, False)
    'Private _TranTime As New ColField(Of String)("TIME", "", "Transation Time", False, False)
    'Private _TranType As New ColField(Of String)("TCOD", "", "Transaction Type", False, False)
    'Private _MiscReasonCode As New ColField(Of String)("MISC", "", "Miscellaneous Reason Code", False, False)
    'Private _Description As New ColField(Of String)("DESCR", "", "Description", False, False)
    'Private _OrderNumber As New ColField(Of String)("ORDN", "", "Order Number", False, False)
    'Private _MerchandisingAmount As New ColField(Of String)("MERC", "", "Merchandising Amount", False, False)
    'Private _NonMerchandisingAmount As New ColField(Of String)("NMER", "", "Non Merchandising Amount", False, False)
    'Private _TotalTaxAmount As New ColField(Of String)("TAXA", "", "Total Tax Amount", False, False)
    'Private _TotalDiscountAmount As New ColField(Of String)("DISC", "", "Total Discount Amount", False, False)
    'Private _TotalSaleAmount As New ColField(Of String)("TOTL", "", "Total Sale Amount", False, False)
    'Private _ColleagueDiscount As New ColField(Of String)("IEMP", "", "Colleague Discount", False, False)
    'Private _CollegueDiscAmount As New ColField(Of String)("PVEM", "", "Colleague Discount Amount", False, False)
    'Private _Pivotal As New ColField(Of String)("PIVT", "", "Pivotal", False, False)

    'Private _DateUpdated As New ColField(Of String)("CBBU", "", "Date Updated", False, False)

#End Region

#Region "Field Properties"

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property CashierNo() As ColField(Of String)
        Get
            CashierNo = _CashierID
        End Get
        Set(ByVal value As ColField(Of String))
            _CashierID = value
        End Set
    End Property
    Public Property TransactionTime() As ColField(Of String)
        Get
            TransactionTime = _TranTime
        End Get
        Set(ByVal value As ColField(Of String))
            _TranTime = value
        End Set
    End Property
    Public Property TransactionType() As ColField(Of String)
        Get
            TransactionType = _TranType
        End Get
        Set(ByVal value As ColField(Of String))
            _TranType = value
        End Set
    End Property
    Public Property MiscReasonCode() As ColField(Of String)
        Get
            MiscReasonCode = _MiscReasonCode
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscReasonCode = value
        End Set
    End Property
    Public Property Description() As ColField(Of String)
        Get
            Description = _Description
        End Get
        Set(ByVal value As ColField(Of String))
            _Description = value
        End Set
    End Property
    Public Property OrderNo() As ColField(Of String)
        Get
            OrderNo = _OrderNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _OrderNumber = value
        End Set
    End Property
    Public Property MerchanisingAmt() As ColField(Of String)
        Get
            MerchanisingAmt = _MerchandisingAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _MerchandisingAmount = value
        End Set
    End Property
    Public Property NonMerchanisingAmt() As ColField(Of String)
        Get
            NonMerchanisingAmt = _NonMerchandisingAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _NonMerchandisingAmount = value
        End Set
    End Property
    Public Property TotalTaxAmt() As ColField(Of String)
        Get
            TotalTaxAmt = _TotalTaxAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _TotalTaxAmount = value
        End Set
    End Property
    Public Property TotalDiscountAmt() As ColField(Of String)
        Get
            TotalDiscountAmt = _TotalDiscountAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _TotalDiscountAmount = value
        End Set
    End Property
    Public Property TotalSaleAmt() As ColField(Of String)
        Get
            TotalSaleAmt = _TotalSaleAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _TotalSaleAmount = value
        End Set
    End Property
    Public Property ColleagueDiscount() As ColField(Of String)
        Get
            ColleagueDiscount = _ColleagueDiscount
        End Get
        Set(ByVal value As ColField(Of String))
            _ColleagueDiscount = value
        End Set
    End Property
    Public Property CollegueDiscAmt() As ColField(Of String)
        Get
            CollegueDiscAmt = _CollegueDiscAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _CollegueDiscAmount = value
        End Set
    End Property
    Public Property Pivotal() As ColField(Of String)
        Get
            Pivotal = _Pivotal
        End Get
        Set(ByVal value As ColField(Of String))
            _Pivotal = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Headers As List(Of cPVTotals) = Nothing
    Private _Lines As List(Of cPVLine) = Nothing
    Private _Paids As List(Of cPVPaid) = Nothing

    Public Property Headers() As List(Of cPVTotals)
        Get
            Return _Headers
        End Get
        Set(ByVal value As List(Of cPVTotals))
            _Headers = value
        End Set
    End Property
    Public Property Lines() As List(Of cPVLine)
        Get
            If _Lines Is Nothing Then
                Dim lin As New cPVLine(Oasys3DB)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TransDate, _TranDate.Value)
                lin.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TillID, _TillID.Value)
                lin.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                lin.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, lin.TransNo, _TranNumber.Value)
                _Lines = lin.LoadMatches
            End If
            Return _Lines
        End Get
        Set(ByVal value As List(Of cPVLine))
            _Lines = value
        End Set
    End Property
    Public Property Paids() As List(Of cPVPaid)
        Get
            If _Paids Is Nothing Then
                Dim p As New cPVPaid(Oasys3DB)
                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TransactionDate, _TranDate.Value)
                p.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TillID, _TillID.Value)
                p.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                p.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, p.TransactionNo, _TranNumber.Value)
                _Paids = p.LoadMatches
            End If
            Return _Paids
        End Get
        Set(ByVal value As List(Of cPVPaid))
            _Paids = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub LoadValidTransactions(ByVal SaleDate As Date, Optional ByVal WithLinesPayments As Boolean = False)

        Using PVHeader As New cPVTotals(Oasys3DB)
            'now a proper date
            'PVHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, PVHeader.TransDate, SaleDate.ToString("dd/MM/yy"))
            PVHeader.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, PVHeader.TransDate, SaleDate)

            _Headers = PVHeader.LoadMatches
        End Using

        If WithLinesPayments Then
            Dim clines As New List(Of cPVLine)
            Using saleLine As New cPVLine(Oasys3DB)
                'now a proper date
                'saleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleLine.TransDate, SaleDate.ToString("dd/MM/yy"))
                saleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, saleLine.TransDate, SaleDate)

                clines = saleLine.LoadMatches
            End Using

            Dim cPaids As New List(Of cPVPaid)
            Using salePaid As New cPVPaid(Oasys3DB)
                'now a proper date
                'salePaid.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, salePaid.TransactionDate, SaleDate.ToString("dd/MM/yy"))
                salePaid.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, salePaid.TransactionDate, SaleDate)

                cPaids = salePaid.LoadMatches
            End Using

            'match lines and payments to headers
            For Each h As cPVTotals In _Headers
                Dim tran As String = h.TransactionNo.Value
                Dim till As String = h.TillID.Value
                h.Lines = clines.FindAll(Function(l As cPVLine) l.TransNo.Value = tran AndAlso l.TillID.Value = till)
                h.Paids = cPaids.FindAll(Function(p As cPVPaid) p.TransactionNo.Value = tran AndAlso p.TillID.Value = till)
            Next
        End If

    End Sub

    Public Function LineGroupTotals() As DataView

        Try
            Dim SKUGroups As New Dictionary(Of String, Decimal)

            'generate dictionary with category and value
            For Each hdr As cPVTotals In Headers
                For Each sLine As cPVLine In hdr.Lines
                    Dim SKUGroup As String = sLine.PartCode.Value.Substring(0, 2)
                    Dim sales As Decimal = CDec(sLine.ExtendedValue.Value)
                    Dim value As Decimal

                    If SKUGroups.TryGetValue(SKUGroup, value) Then
                        SKUGroups.Item(SKUGroup) = value + sales
                    Else
                        SKUGroups.Add(SKUGroup, sales)
                    End If
                Next
            Next

            If SKUGroups.Count = 0 Then Return Nothing

            'create datatable from dictionary and add category name
            Dim dt As New DataTable
            dt.Columns.Add("CategoryNumber", GetType(String))
            dt.Columns.Add("TotalValue", GetType(Decimal))

            For Each kvp As KeyValuePair(Of String, Decimal) In SKUGroups
                dt.Rows.Add(kvp.Key, kvp.Value)
            Next

            'create dataview to sort table
            Dim dv As DataView = New DataView(dt, "", "", DataViewRowState.CurrentRows)
            dv.Sort = "CategoryNumber"
            Return dv

        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function CategorySales(Optional ByRef Results As DataView = Nothing) As DataView

        Try
            Dim categories As New Dictionary(Of String, Decimal)

            'If previous results, then extract values into Dictionary to append values to
            If ((Results Is Nothing) = False) Then
                For Each dr As DataRow In Results.Table.Rows
                    categories.Add(dr(0).ToString, CDec(dr(2).ToString))
                Next
            End If

            'generate dictionary with category and value
            For Each h As cPVTotals In Headers
                For Each l As cPVLine In h.Lines
                    Dim cat As String = l.HierCategory.Value
                    Dim sales As Decimal = CDec(l.ExtendedValue.Value)
                    Dim value As Decimal

                    If categories.TryGetValue(cat, value) Then
                        categories.Item(cat) = value + sales
                    Else
                        categories.Add(cat, sales)
                    End If
                Next
            Next

            'get category names from hierarchy table
            If categories.Count = 0 Then Return Nothing
            Dim hie As New BOHierarchy.cHierachyCategory(_Oasys3DB)
            hie.Load(categories.Keys.ToArray)

            'create datatable from dictionary and add category name
            Dim dt As New DataTable
            dt.Columns.Add("number", GetType(String))
            dt.Columns.Add("name", GetType(String))
            dt.Columns.Add("value", GetType(Decimal))

            For Each kvp As KeyValuePair(Of String, Decimal) In categories
                dt.Rows.Add(kvp.Key, hie.Hierarchy(kvp.Key).Description.Value, kvp.Value)
            Next

            'create dataview to sort table
            Dim dv As DataView = New DataView(dt, "", "name", DataViewRowState.CurrentRows)
            Return dv

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

End Class

'function not being used
'Public Function InsertVisionRecords() As Boolean

'    Try
'        Oasys3DB.ClearAllParameters()
'        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
'        Oasys3DB.SetColumnAndValueParameter(TransDate.ColumnName, TransDate.Value)
'        Oasys3DB.SetColumnAndValueParameter(TillID.ColumnName, TillID.Value)
'        Oasys3DB.SetColumnAndValueParameter(TransactionNo.ColumnName, TransactionNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(CashierNo.ColumnName, CashierNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(TransactionTime.ColumnName, TransactionTime.Value)
'        Oasys3DB.SetColumnAndValueParameter(TransactionType.ColumnName, TransactionType.Value)
'        Oasys3DB.SetColumnAndValueParameter(MiscReasonCode.ColumnName, MiscReasonCode.Value)
'        Oasys3DB.SetColumnAndValueParameter(Description.ColumnName, Description.Value)
'        Oasys3DB.SetColumnAndValueParameter(OrderNo.ColumnName, OrderNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(MerchanisingAmt.ColumnName, MerchanisingAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(NonMerchanisingAmt.ColumnName, NonMerchanisingAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(TotalTaxAmt.ColumnName, TotalTaxAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(TotalDiscountAmt.ColumnName, TotalDiscountAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(TotalSaleAmt.ColumnName, TotalSaleAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(ColleagueDiscount.ColumnName, ColleagueDiscount.Value)
'        Oasys3DB.SetColumnAndValueParameter(CollegueDiscAmt.ColumnName, CollegueDiscAmt.Value)
'        Oasys3DB.SetColumnAndValueParameter(Pivotal.ColumnName, Pivotal.Value)
'        If Oasys3DB.Insert > 0 Then Return True
'        Return False

'    Catch ex As Exception

'        Return False

'    End Try

'End Function