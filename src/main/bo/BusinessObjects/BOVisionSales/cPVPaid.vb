﻿''' <summary>
''' 
''' </summary>
''' <history>
''' Author  : Partha Dutta
''' Date    : 02/09/2010
''' Referral: 354
''' Notes   : Used existing code and modified it to point to the "vision" tables
'''           Clean sheet solution would have used stored procedures rather than dynamic sequel
''' 
'''           Changed the "date" field to a proper DATE
'''           Removed unnecessary fields "_ExchangeRate" and "_ExchangePower"
'''           Removed unnecessary function "InsertVisionRecords"
''' 
''' </history>
''' <remarks></remarks>
<Serializable()> Public Class cPVPaid
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "VisionPayment"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_Number)
        BOFields.Add(_TenderType)
        BOFields.Add(_TenderAmount)
        BOFields.Add(_FromPivotal)

        'not being used
        'BOFields.Add(_ExchangeRate)
        'BOFields.Add(_ExchangePower)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPVPaid)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPVPaid)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPVPaid))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPVPaid(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

    Public Sub LoadFromBOOld(ByRef LoadFrom As cPVPaidOld)

        Try
            Dim oldStringField As ColField(Of String)
            Dim convertDate As Date

            For Each oldField As Object In LoadFrom.BOFields
                If TypeOf (oldField) Is ColField(Of String) Then
                    oldStringField = CType(oldField, ColField(Of String))
                    With oldStringField
                        If .Value IsNot Nothing Then
                            Select Case .ColumnName
                                Case LoadFrom.TransactionDate.ColumnName
                                    ' This field actually stored as a date now
                                    Me.TransactionDate.Value = CDate(IIf(Date.TryParse(.Value, convertDate), convertDate, Nothing))
                                Case LoadFrom.TillID.ColumnName
                                    Me.TillID.Value = .Value
                                Case LoadFrom.TransactionNo.ColumnName
                                    Me.TransactionNo.Value = .Value
                                Case LoadFrom.SequenceNo.ColumnName
                                    Me.SequenceNo.Value = .Value
                                Case LoadFrom.TenderType.ColumnName
                                    Me.TenderType.Value = .Value
                                Case LoadFrom.TenderAmount.ColumnName
                                    Me.TenderAmount.Value = .Value
                                Case LoadFrom.FromPivotal.ColumnName
                                    Me.FromPivotal.Value = .Value
                            End Select
                        End If
                    End With
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Fields"

    'moved to vision tables, date field in now a proper date
    'Private _TranDate As New ColField(Of String)("TranDate", "", "Transaction Date", True, False)
    Private _TranDate As New ColField(Of Date)("TranDate", DateTime.MinValue, "Transation Date", True, False)

    Private _TillID As New ColField(Of String)("TillId", "", "Transaction ID", True, False)
    Private _TranNumber As New ColField(Of String)("TranNumber", "", "Transaction Number", True, False)
    Private _Number As New ColField(Of String)("Number", "", "Number", True, False)
    'non key fields
    Private _TenderType As New ColField(Of String)("TenderTypeId", "", "Tender Type", False, False)
    Private _TenderAmount As New ColField(Of String)("ValueTender", "", "Tender Amount", False, False)
    Private _FromPivotal As New ColField(Of String)("IsPivotal", "", "From Pivotal", False, False)
    'not being used
    'Private _ExchangeRate As New ColField(Of String)("MRAT", "", "Exchange Rate", False, False)
    'Private _ExchangePower As New ColField(Of String)("MPOW", "", "Exchange Power", False, False)

    'have listed exiting  code to show mapping between PVPAID and VisionPayment tables
    'Private _TranDate As New ColField(Of String)("DATE1", "", "Transaction Date", True, False)
    'Private _TillID As New ColField(Of String)("TILL", "", "Transaction ID", True, False)
    'Private _TranNumber As New ColField(Of String)("TRAN", "", "Transaction Number", True, False)
    'Private _Number As New ColField(Of String)("NUMB", "", "Number", False, False)

    'Private _TenderType As New ColField(Of String)("TYPE", "", "Tender Type", False, False)
    'Private _TenderAmount As New ColField(Of String)("AMNT", "", "Tender Amount", False, False)
    'Private _FromPivotal As New ColField(Of String)("PIVT", "", "From Pivotal", False, False)

    'Private _ExchangeRate As New ColField(Of String)("MRAT", "", "Exchange Rate", False, False)
    'Private _ExchangePower As New ColField(Of String)("MPOW", "", "Exchange Power", False, False)

#End Region

#Region "Field Properties"

    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property SequenceNo() As ColField(Of String)
        Get
            SequenceNo = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property TenderType() As ColField(Of String)
        Get
            TenderType = _TenderType
        End Get
        Set(ByVal value As ColField(Of String))
            _TenderType = value
        End Set
    End Property
    Public Property TenderAmount() As ColField(Of String)
        Get
            TenderAmount = _TenderAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _TenderAmount = value
        End Set
    End Property
    Public Property FromPivotal() As ColField(Of String)
        Get
            FromPivotal = _FromPivotal
        End Get
        Set(ByVal value As ColField(Of String))
            _FromPivotal = value
        End Set
    End Property

    'not being used

    'Public Property ExchangeRate() As ColField(Of String)
    '    Get
    '        Return _ExchangeRate
    '    End Get
    '    Set(ByVal value As ColField(Of String))
    '        _ExchangeRate = value
    '    End Set
    'End Property
    'Public Property ExchangePower() As ColField(Of String)
    '    Get
    '        Return _ExchangePower
    '    End Get
    '    Set(ByVal value As ColField(Of String))
    '        _ExchangePower = value
    '    End Set
    'End Property

#End Region

#Region "Methods"

    Private _Payments As List(Of cPVPaid) = Nothing

    Public Property Payments() As List(Of cPVPaid)
        Get
            Return _Payments
        End Get
        Set(ByVal value As List(Of cPVPaid))
            _Payments = value
        End Set
    End Property

#End Region

End Class

'function not being used
'Public Function InsertVisionRecords() As Boolean

'    Try
'        Oasys3DB.ClearAllParameters()
'        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
'        Oasys3DB.SetColumnAndValueParameter(TransactionDate.ColumnName, TransactionDate.Value)
'        Oasys3DB.SetColumnAndValueParameter(TillID.ColumnName, TillID.Value)
'        Oasys3DB.SetColumnAndValueParameter(TransactionNo.ColumnName, TransactionNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(SequenceNo.ColumnName, SequenceNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(TenderType.ColumnName, TenderType.Value)
'        Oasys3DB.SetColumnAndValueParameter(TenderAmount.ColumnName, TenderAmount.Value)
'        Oasys3DB.SetColumnAndValueParameter(FromPivotal.ColumnName, FromPivotal.Value)
'        If Oasys3DB.Insert > 0 Then Return True
'        Return False

'    Catch ex As Exception

'        Return False

'    End Try

'End Function

