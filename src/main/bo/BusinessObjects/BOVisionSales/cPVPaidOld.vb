﻿<Serializable()> Public Class cPVPaidOld
    Inherits cBaseClass

#Region "Constructors"

    Public Sub New()

        MyBase.New()
        Start()
    End Sub

    Public Sub New(ByVal strConnection As String)

        MyBase.New(strConnection)
        Start()
    End Sub

    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)

        MyBase.New(oasys3DB)
        Start()
    End Sub
#End Region

    Public Overrides Sub Start()

        TableName = "PVPAID"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_Number)
        BOFields.Add(_TenderType)
        BOFields.Add(_TenderAmount)
        BOFields.Add(_FromPivotal)
        BOFields.Add(_ExchangeRate)
        BOFields.Add(_ExchangePower)
    End Sub

    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPVPaidOld)
        Dim col As New List(Of cPVPaidOld)

        Try
            LoadBORecords(count)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPVPaidOld))
            Next
        Catch ex As Exception
            Throw ex
        End Try
        Return col
    End Function

    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)
            GetSQLBase()

            Dim ds As DataSet = Oasys3DB.Query(count)

            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Dim BO As cPVPaidOld

                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)
                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        BO = New cPVPaidOld(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select
        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try
    End Sub

#Region "Fields"

    Private _TranDate As New ColField(Of String)("DATE1", "", "Transaction Date", True, False)
    Private _TillID As New ColField(Of String)("TILL", "", "Transaction ID", True, False)
    Private _TranNumber As New ColField(Of String)("TRAN", "", "Transaction Number", True, False)
    Private _Number As New ColField(Of String)("NUMB", "", "Number", False, False)
    Private _TenderType As New ColField(Of String)("TYPE", "", "Tender Type", False, False)
    Private _TenderAmount As New ColField(Of String)("AMNT", "", "Tender Amount", False, False)
    Private _FromPivotal As New ColField(Of String)("PIVT", "", "From Pivotal", False, False)
    Private _ExchangeRate As New ColField(Of String)("MRAT", "", "Exchange Rate", False, False)
    Private _ExchangePower As New ColField(Of String)("MPOW", "", "Exchange Power", False, False)
#End Region

#Region "Field Properties"

    Public Property TransactionDate() As ColField(Of String)
        Get
            TransactionDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of String))
            _TranDate = value
        End Set
    End Property

    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property

    Public Property TransactionNo() As ColField(Of String)
        Get
            TransactionNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property

    Public Property SequenceNo() As ColField(Of String)
        Get
            SequenceNo = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property

    Public Property TenderType() As ColField(Of String)
        Get
            TenderType = _TenderType
        End Get
        Set(ByVal value As ColField(Of String))
            _TenderType = value
        End Set
    End Property

    Public Property TenderAmount() As ColField(Of String)
        Get
            TenderAmount = _TenderAmount
        End Get
        Set(ByVal value As ColField(Of String))
            _TenderAmount = value
        End Set
    End Property

    Public Property FromPivotal() As ColField(Of String)
        Get
            FromPivotal = _FromPivotal
        End Get
        Set(ByVal value As ColField(Of String))
            _FromPivotal = value
        End Set
    End Property

    Public Property ExchangeRate() As ColField(Of String)
        Get
            Return _ExchangeRate
        End Get
        Set(ByVal value As ColField(Of String))
            _ExchangeRate = value
        End Set
    End Property

    Public Property ExchangePower() As ColField(Of String)
        Get
            Return _ExchangePower
        End Get
        Set(ByVal value As ColField(Of String))
            _ExchangePower = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Function InsertVisionRecords() As Boolean

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
            Oasys3DB.SetColumnAndValueParameter(TransactionDate.ColumnName, TransactionDate.Value)
            Oasys3DB.SetColumnAndValueParameter(TillID.ColumnName, TillID.Value)
            Oasys3DB.SetColumnAndValueParameter(TransactionNo.ColumnName, TransactionNo.Value)
            Oasys3DB.SetColumnAndValueParameter(SequenceNo.ColumnName, SequenceNo.Value)
            Oasys3DB.SetColumnAndValueParameter(TenderType.ColumnName, TenderType.Value)
            Oasys3DB.SetColumnAndValueParameter(TenderAmount.ColumnName, TenderAmount.Value)
            Oasys3DB.SetColumnAndValueParameter(FromPivotal.ColumnName, FromPivotal.Value)
            If Oasys3DB.Insert > 0 Then
                InsertVisionRecords = True
            End If
        Catch ex As Exception
        End Try
    End Function

    Private _Payments As List(Of cPVPaidOld) = Nothing

    Public Property Payments() As List(Of cPVPaidOld)
        Get
            Return _Payments
        End Get
        Set(ByVal value As List(Of cPVPaidOld))
            _Payments = value
        End Set
    End Property
#End Region
End Class
