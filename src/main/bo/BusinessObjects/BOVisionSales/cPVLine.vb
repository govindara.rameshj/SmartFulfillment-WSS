﻿''' <summary>
''' 
''' </summary>
''' <history>
''' Author  : Partha Dutta
''' Date    : 02/09/2010
''' Referral: 354
''' Notes   : Used existing code and modified it to point to the "vision" tables
'''           Clean sheet solution would have used stored procedures rather than dynamic sequel
''' 
'''           Changed the "date" field to a proper DATE
'''           Removed unnecessary fields "_WeeeType" and "_WeeeSequence" and "_WeeeValue"
'''           Removed unnecessary function "InsertLineRecord"
''' 
''' </history>
''' <remarks></remarks>
<Serializable()> Public Class cPVLine
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "VisionLine"
        BOFields.Add(_TranDate)
        BOFields.Add(_TillID)
        BOFields.Add(_TranNumber)
        BOFields.Add(_SkuNumber)
        BOFields.Add(_Number)
        BOFields.Add(_QuantitySold)
        BOFields.Add(_LookUpPrice)
        BOFields.Add(_ActualSellPrice)
        BOFields.Add(_ActualPriceExVat)
        BOFields.Add(_ExtendedValue)
        BOFields.Add(_RelatedItems)
        BOFields.Add(_VatCode)
        BOFields.Add(_HierCategory)
        BOFields.Add(_HierGroup)
        BOFields.Add(_HierSubGroup)
        BOFields.Add(_HierStyle)
        BOFields.Add(_SaleType)
        BOFields.Add(_InStoreStockItem)
        BOFields.Add(_MovementType)
        BOFields.Add(_FromPivotal)

        'not being used
        'BOFields.Add(_WeeeType)
        'BOFields.Add(_WeeeSequence)
        'BOFields.Add(_WeeeValue)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cPVLine)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cPVLine)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cPVLine))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cPVLine(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

    Public Sub LoadFromBOOld(ByRef LoadFrom As cPVLineOld)

        Try
            Dim oldStringField As ColField(Of String)
            Dim convertDate As Date

            For Each oldField As Object In LoadFrom.BOFields
                If TypeOf (oldField) Is ColField(Of String) Then
                    oldStringField = CType(oldField, ColField(Of String))
                    With oldStringField
                        If .Value IsNot Nothing Then
                            Select Case .ColumnName
                                Case LoadFrom.TransDate.ColumnName
                                    ' This field actually stored as a date now
                                    Me.TransDate.Value = CDate(IIf(Date.TryParse(.Value, convertDate), convertDate, Nothing))
                                Case LoadFrom.TillID.ColumnName
                                    Me.TillID.Value = .Value
                                Case LoadFrom.TransNo.ColumnName
                                    Me.TransNo.Value = .Value
                                Case LoadFrom.PartCode.ColumnName
                                    Me.PartCode.Value = .Value
                                Case LoadFrom.Number.ColumnName
                                    Me.Number.Value = .Value
                                Case LoadFrom.QuantitySold.ColumnName
                                    Me.QuantitySold.Value = .Value
                                Case LoadFrom.LoopUpPrice.ColumnName
                                    Me.LoopUpPrice.Value = .Value
                                Case LoadFrom.ActualSellPrice.ColumnName
                                    Me.ActualSellPrice.Value = .Value
                                Case LoadFrom.ActualPriceExVat.ColumnName
                                    Me.ActualPriceExVat.Value = .Value
                                Case LoadFrom.ExtendedValue.ColumnName
                                    Me.ExtendedValue.Value = .Value
                                Case LoadFrom.RelatedItems.ColumnName
                                    Me.RelatedItems.Value = .Value
                                Case LoadFrom.VatCode.ColumnName
                                    Me.VatCode.Value = .Value
                                Case LoadFrom.HierCategory.ColumnName
                                    Me.HierCategory.Value = .Value
                                Case LoadFrom.HierGroup.ColumnName
                                    Me.HierGroup.Value = .Value
                                Case LoadFrom.HierSubGroup.ColumnName
                                    Me.HierSubGroup.Value = .Value
                                Case LoadFrom.HierStyle.ColumnName
                                    Me.HierStyle.Value = .Value
                                Case LoadFrom.SaleType.ColumnName
                                    Me.SaleType.Value = .Value
                                Case LoadFrom.InStoreStockItem.ColumnName
                                    Me.InStoreStockItem.Value = .Value
                                Case LoadFrom.MovementType.ColumnName
                                    Me.MovementType.Value = .Value
                                Case LoadFrom.FromPivotal.ColumnName
                                    Me.FromPivotal.Value = .Value
                            End Select
                        End If
                    End With
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Fields"

    'moved to vision tables, date field in now a proper date
    'Private _TranDate As New ColField(Of String)("TranDate", "", "Transaction Date", True, False)
    Private _TranDate As New ColField(Of Date)("TranDate", DateTime.MinValue, "Transation Date", True, False)

    Private _TillID As New ColField(Of String)("TillId", "", "Till ID", True, False)
    Private _TranNumber As New ColField(Of String)("TranNumber", "", "Transaction Number", True, False)
    Private _Number As New ColField(Of String)("Number", "", "Number", True, False)
    'non key fields
    Private _SkuNumber As New ColField(Of String)("SkuNumber", "", "SKU Number", False, False)
    Private _QuantitySold As New ColField(Of String)("Quantity", "", "Quantity Sold", False, False)
    Private _LookUpPrice As New ColField(Of String)("PriceLookup", "", "look up Price", False, False)
    Private _ActualSellPrice As New ColField(Of String)("Price", "", "Actual Sell Price", False, False)
    Private _ActualPriceExVat As New ColField(Of String)("PriceExVat", "", "Actual Price ex VAT", False, False)
    Private _ExtendedValue As New ColField(Of String)("PriceExtended", "", "Extended Value", False, False)
    Private _RelatedItems As New ColField(Of String)("IsRelatedItemSingle", "", "Related Items", False, False)
    Private _VatCode As New ColField(Of String)("VatSymbol", "", "VAT Code", False, False)
    Private _HierCategory As New ColField(Of String)("HieCategory", "", "Hierarachy Category", False, False)
    Private _HierGroup As New ColField(Of String)("HieGroup", "", "Hierarchy Group", False, False)
    Private _HierSubGroup As New ColField(Of String)("HieSubgroup", "", "Hierarchy Subgroup", False, False)
    Private _HierStyle As New ColField(Of String)("HieStyle", "", "Hierarchy Style", False, False)
    Private _SaleType As New ColField(Of String)("SaleType", "", "Sale Type", False, False)
    Private _InStoreStockItem As New ColField(Of String)("IsInStoreStockItem", "", "In Store Stock Item", False, False)
    Private _MovementType As New ColField(Of String)("MovementTypeCode", "", "Movement Type", False, False)
    Private _FromPivotal As New ColField(Of String)("IsPivotal", "", "From Pivotal", False, False)
    'not being used
    'Private _WeeeType As New ColField(Of String)("WTYP", "", "Weee Type", False, False)
    'Private _WeeeSequence As New ColField(Of String)("WSEQ", "", "Weee Sequence", False, False)
    'Private _WeeeValue As New ColField(Of String)("WCOS", "", "Weee Value", False, False)

    'have listed exiting  code to show mapping between PVLINE and VisionLine tables
    'Private _TranDate As New ColField(Of String)("DATE1", "", "Transaction Date", True, False)
    'Private _TillID As New ColField(Of String)("TILL", "", "Till ID", True, False)
    'Private _TranNumber As New ColField(Of String)("TRAN", "", "Transaction Number", True, False)
    'Private _Number As New ColField(Of String)("NUMB", "", "Number", False, False)

    'Private _SkuNumber As New ColField(Of String)("SKUN", "", "SKU Number", False, False)
    'Private _QuantitySold As New ColField(Of String)("QUAN", "", "Quantity Sold", False, False)
    'Private _LookUpPrice As New ColField(Of String)("SPRI", "", "look up Price", False, False)
    'Private _ActualSellPrice As New ColField(Of String)("PRIC", "", "Actual Sell Price", False, False)
    'Private _ActualPriceExVat As New ColField(Of String)("PRVE", "", "Actual Price ex VAT", False, False)
    'Private _ExtendedValue As New ColField(Of String)("EXTP", "", "Extended Value", False, False)
    'Private _RelatedItems As New ColField(Of String)("RITM", "", "Related Items", False, False)
    'Private _VatCode As New ColField(Of String)("VSYM", "", "VAT Code", False, False)
    'Private _HierCategory As New ColField(Of String)("CTGY", "", "Hierarachy Category", False, False)
    'Private _HierGroup As New ColField(Of String)("GRUP", "", "Hierarchy Group", False, False)
    'Private _HierSubGroup As New ColField(Of String)("SGRP", "", "Hierarchy Subgroup", False, False)
    'Private _HierStyle As New ColField(Of String)("STYL", "", "Hierarchy Style", False, False)
    'Private _SaleType As New ColField(Of String)("SALT", "", "Sale Type", False, False)
    'Private _InStoreStockItem As New ColField(Of String)("PIVI", "", "In Store Stock Item", False, False)
    'Private _MovementType As New ColField(Of String)("PIVM", "", "Movement Type", False, False)
    'Private _FromPivotal As New ColField(Of String)("PIVT", "", "From Pivotal", False, False)

    'Private _WeeeType As New ColField(Of String)("WTYP", "", "Weee Type", False, False)
    'Private _WeeeSequence As New ColField(Of String)("WSEQ", "", "Weee Sequence", False, False)
    'Private _WeeeValue As New ColField(Of String)("WCOS", "", "Weee Value", False, False)

#End Region

#Region "Field Properties"

    Public Property TransDate() As ColField(Of Date)
        Get
            TransDate = _TranDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _TranDate = value
        End Set
    End Property
    Public Property TillID() As ColField(Of String)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of String))
            _TillID = value
        End Set
    End Property
    Public Property TransNo() As ColField(Of String)
        Get
            TransNo = _TranNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _TranNumber = value
        End Set
    End Property
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = _SkuNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SkuNumber = value
        End Set
    End Property
    Public Property Number() As ColField(Of String)
        Get
            Number = _Number
        End Get
        Set(ByVal value As ColField(Of String))
            _Number = value
        End Set
    End Property
    Public Property QuantitySold() As ColField(Of String)
        Get
            QuantitySold = _QuantitySold
        End Get
        Set(ByVal value As ColField(Of String))
            _QuantitySold = value
        End Set
    End Property
    Public Property LoopUpPrice() As ColField(Of String)
        Get
            LoopUpPrice = _LookUpPrice
        End Get
        Set(ByVal value As ColField(Of String))
            _LookUpPrice = value
        End Set
    End Property
    Public Property ActualSellPrice() As ColField(Of String)
        Get
            ActualSellPrice = _ActualSellPrice
        End Get
        Set(ByVal value As ColField(Of String))
            _ActualSellPrice = value
        End Set
    End Property
    Public Property ActualPriceExVat() As ColField(Of String)
        Get
            ActualPriceExVat = _ActualPriceExVat
        End Get
        Set(ByVal value As ColField(Of String))
            _ActualPriceExVat = value
        End Set
    End Property
    Public Property ExtendedValue() As ColField(Of String)
        Get
            ExtendedValue = _ExtendedValue
        End Get
        Set(ByVal value As ColField(Of String))
            _ExtendedValue = value
        End Set
    End Property
    Public Property RelatedItems() As ColField(Of String)
        Get
            RelatedItems = _RelatedItems
        End Get
        Set(ByVal value As ColField(Of String))
            _RelatedItems = value
        End Set
    End Property
    Public Property VatCode() As ColField(Of String)
        Get
            VatCode = _VatCode
        End Get
        Set(ByVal value As ColField(Of String))
            _VatCode = value
        End Set
    End Property
    Public Property HierCategory() As ColField(Of String)
        Get
            HierCategory = _HierCategory
        End Get
        Set(ByVal value As ColField(Of String))
            _HierCategory = value
        End Set
    End Property
    Public Property HierGroup() As ColField(Of String)
        Get
            HierGroup = _HierGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HierGroup = value
        End Set
    End Property
    Public Property HierSubGroup() As ColField(Of String)
        Get
            HierSubGroup = _HierSubGroup
        End Get
        Set(ByVal value As ColField(Of String))
            _HierSubGroup = value
        End Set
    End Property
    Public Property HierStyle() As ColField(Of String)
        Get
            HierStyle = _HierStyle
        End Get
        Set(ByVal value As ColField(Of String))
            _HierStyle = value
        End Set
    End Property
    Public Property SaleType() As ColField(Of String)
        Get
            SaleType = _SaleType
        End Get
        Set(ByVal value As ColField(Of String))
            _SaleType = value
        End Set
    End Property
    Public Property InStoreStockItem() As ColField(Of String)
        Get
            InStoreStockItem = _InStoreStockItem
        End Get
        Set(ByVal value As ColField(Of String))
            _InStoreStockItem = value
        End Set
    End Property
    Public Property MovementType() As ColField(Of String)
        Get
            MovementType = _MovementType
        End Get
        Set(ByVal value As ColField(Of String))
            _MovementType = value
        End Set
    End Property
    Public Property FromPivotal() As ColField(Of String)
        Get
            FromPivotal = _FromPivotal
        End Get
        Set(ByVal value As ColField(Of String))
            _FromPivotal = value
        End Set
    End Property

#End Region

#Region "Methods"
    Private _Lines As List(Of cPVLine) = Nothing

    Public Property Lines() As List(Of cPVLine)
        Get
            Return _Lines
        End Get
        Set(ByVal value As List(Of cPVLine))
            _Lines = value
        End Set
    End Property

#End Region

End Class

'function not being used
'Public Function InsertLineRecord() As Boolean

'    Try
'        Oasys3DB.ClearAllParameters()
'        Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
'        Oasys3DB.SetColumnAndValueParameter(TransDate.ColumnName, TransDate.Value)
'        Oasys3DB.SetColumnAndValueParameter(TillID.ColumnName, TillID.Value)
'        Oasys3DB.SetColumnAndValueParameter(TransNo.ColumnName, TransNo.Value)
'        Oasys3DB.SetColumnAndValueParameter(Number.ColumnName, Number.Value)
'        Oasys3DB.SetColumnAndValueParameter(PartCode.ColumnName, PartCode.Value)
'        Oasys3DB.SetColumnAndValueParameter(QuantitySold.ColumnName, QuantitySold.Value)
'        Oasys3DB.SetColumnAndValueParameter(LoopUpPrice.ColumnName, LoopUpPrice.Value)
'        Oasys3DB.SetColumnAndValueParameter(ActualSellPrice.ColumnName, ActualSellPrice.Value)
'        Oasys3DB.SetColumnAndValueParameter(ActualPriceExVat.ColumnName, ActualPriceExVat.Value)
'        Oasys3DB.SetColumnAndValueParameter(ExtendedValue.ColumnName, ExtendedValue.Value)
'        Oasys3DB.SetColumnAndValueParameter(RelatedItems.ColumnName, RelatedItems.Value)
'        Oasys3DB.SetColumnAndValueParameter(VatCode.ColumnName, VatCode.Value)
'        Oasys3DB.SetColumnAndValueParameter(InStoreStockItem.ColumnName, InStoreStockItem.Value)
'        Oasys3DB.SetColumnAndValueParameter(MovementType.ColumnName, MovementType.Value)
'        Oasys3DB.SetColumnAndValueParameter(HierCategory.ColumnName, HierCategory.Value)
'        Oasys3DB.SetColumnAndValueParameter(HierGroup.ColumnName, HierGroup.Value)
'        Oasys3DB.SetColumnAndValueParameter(HierSubGroup.ColumnName, HierSubGroup.Value)
'        Oasys3DB.SetColumnAndValueParameter(HierStyle.ColumnName, HierStyle.Value)
'        Oasys3DB.SetColumnAndValueParameter(SaleType.ColumnName, SaleType.Value)
'        Oasys3DB.SetColumnAndValueParameter(FromPivotal.ColumnName, FromPivotal.Value)
'        If Oasys3DB.Insert > 0 Then Return True
'        Return False

'    Catch ex As Exception
'        Return False

'    End Try

'End Function