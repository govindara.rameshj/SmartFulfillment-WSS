﻿Public Module mExtensions

    ''' <summary>
    ''' Get category sales in dataview ordered by category name (columns are number,name,value)
    ''' </summary>
    ''' <param name="Headers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function CategorySales(ByVal Headers As List(Of cPVTotals)) As DataView

        Dim categories As New Dictionary(Of String, Decimal)

        'generate dictionary with category and value
        For Each h As cPVTotals In Headers
            For Each l As cPVLine In h.Lines
                Dim cat As String = l.HierCategory.Value
                Dim sales As Decimal = CDec(l.ExtendedValue.Value)
                Dim value As Decimal

                If categories.TryGetValue(cat, value) Then
                    categories.Item(cat) = value + sales
                Else
                    categories.Add(cat, sales)
                End If
            Next
        Next

        'get category names from hierarchy table
        If categories.Count = 0 Then Return Nothing
        Dim hie As New BOHierarchy.cHierachyCategory(Headers(0).Oasys3DB)
        hie.Load(categories.Keys.ToArray)

        'create datatable from dictionary and add category name
        Dim dt As New DataTable
        dt.Columns.Add("number", GetType(String))
        dt.Columns.Add("name", GetType(String))
        dt.Columns.Add("value", GetType(Decimal))

        For Each kvp As KeyValuePair(Of String, Decimal) In categories
            dt.Rows.Add(kvp.Key, hie.Hierarchy(kvp.Key).Description.Value, kvp.Value)
        Next

        'create dataview to sort table
        Dim dv As DataView = New DataView(dt, "", "name", DataViewRowState.CurrentRows)
        Return dv

    End Function


    ''' <summary>
    ''' Get total of Lines grouped by SKU, for Extended Price Less Discounts in dataview ordered by SKU Number (columns are CategoryNumber,TotalValue)
    ''' </summary>
    ''' <param name="Headers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function LineGroupTotals(ByVal Headers As List(Of cPVTotals)) As DataView

        Dim SKUGroups As New Dictionary(Of String, Decimal)

        'generate dictionary with category and value
        For Each hdr As cPVTotals In Headers
            For Each sLine As cPVLine In hdr.Lines
                Dim SKUGroup As String = sLine.PartCode.Value.Substring(0, 2)
                Dim sales As Decimal = CDec(sLine.ExtendedValue.Value)
                Dim value As Decimal

                If SKUGroups.TryGetValue(SKUGroup, value) Then
                    SKUGroups.Item(SKUGroup) = value + sales
                Else
                    SKUGroups.Add(SKUGroup, sales)
                End If
            Next
        Next

        If SKUGroups.Count = 0 Then Return Nothing

        'create datatable from dictionary and add category name
        Dim dt As New DataTable
        dt.Columns.Add("CategoryNumber", GetType(String))
        dt.Columns.Add("TotalValue", GetType(Decimal))

        For Each kvp As KeyValuePair(Of String, Decimal) In SKUGroups
            dt.Rows.Add(kvp.Key, kvp.Value)
        Next

        'create dataview to sort table
        Dim dv As DataView = New DataView(dt, "", "", DataViewRowState.CurrentRows)
        dv.Sort = "CategoryNumber"
        Return dv

    End Function

End Module
