<Serializable()> Public Class cStore
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "STRMAS"
        BOFields.Add(_StoreID)
        BOFields.Add(_AddressLine1)
        BOFields.Add(_AddressLine2)
        BOFields.Add(_AddressLine3)
        BOFields.Add(_AddressLine4)
        BOFields.Add(_StoreNameOnReceipt)
        BOFields.Add(_Telephone)
        BOFields.Add(_Fax)
        BOFields.Add(_ManagerName)
        BOFields.Add(_RegionCode)
        BOFields.Add(_IsDeleted)
        BOFields.Add(_CountryCode)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cStore)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cStore)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cStore))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cStore(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _StoreID As New ColField(Of String)("NUMB", "", "Store ID", True, False)
    Private _AddressLine1 As New ColField(Of String)("ADD1", "", "Address Line 1", False, False)
    Private _AddressLine2 As New ColField(Of String)("ADD2", "", "Address Line 2", False, False)
    Private _AddressLine3 As New ColField(Of String)("ADD3", "", "Address Line 3", False, False)
    Private _AddressLine4 As New ColField(Of String)("ADD4", "", "Address Line 4", False, False)
    Private _StoreNameOnReceipt As New ColField(Of String)("TILD", "", "Store Name", False, False)
    Private _Telephone As New ColField(Of String)("PHON", "", "Telephone No.", False, False)
    Private _Fax As New ColField(Of String)("SFAX", "", "Fax No.", False, False)
    Private _ManagerName As New ColField(Of String)("MANG", "", "Managers Name", False, False)
    Private _RegionCode As New ColField(Of String)("REGC", "", "Region Code", False, False)
    Private _IsDeleted As New ColField(Of Boolean)("DELC", False, "Deleted", False, False)
    Private _CountryCode As New ColField(Of String)("CountryCode", "", "Country Code", False, False)

#End Region

#Region "Field Properties"

    Public Property StoreID() As ColField(Of String)
        Get
            StoreID = _StoreID
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreID = value
        End Set
    End Property
    Public Property AddressLine1() As ColField(Of String)
        Get
            AddressLine1 = _AddressLine1
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine1 = value
        End Set
    End Property
    Public Property AddressLine2() As ColField(Of String)
        Get
            AddressLine2 = _AddressLine2
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine2 = value
        End Set
    End Property
    Public Property AddressLine3() As ColField(Of String)
        Get
            AddressLine3 = _AddressLine3
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine3 = value
        End Set
    End Property
    Public Property AddressLine4() As ColField(Of String)
        Get
            AddressLine4 = _AddressLine4
        End Get
        Set(ByVal value As ColField(Of String))
            _AddressLine4 = value
        End Set
    End Property
    Public Property StoreNameOnReceipt() As ColField(Of String)
        Get
            StoreNameOnReceipt = _StoreNameOnReceipt
        End Get
        Set(ByVal value As ColField(Of String))
            _StoreNameOnReceipt = value
        End Set
    End Property
    Public Property Telephone() As ColField(Of String)
        Get
            Telephone = _Telephone
        End Get
        Set(ByVal value As ColField(Of String))
            _Telephone = value
        End Set
    End Property
    Public Property Fax() As ColField(Of String)
        Get
            Fax = _Fax
        End Get
        Set(ByVal value As ColField(Of String))
            _Fax = value
        End Set
    End Property
    Public Property ManagerName() As ColField(Of String)
        Get
            ManagerName = _ManagerName
        End Get
        Set(ByVal value As ColField(Of String))
            _ManagerName = value
        End Set
    End Property
    Public Property RegionCode() As ColField(Of String)
        Get
            RegionCode = _RegionCode
        End Get
        Set(ByVal value As ColField(Of String))
            _RegionCode = value
        End Set
    End Property
    Public Property IsDeleted() As ColField(Of Boolean)
        Get
            IsDeleted = _IsDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsDeleted = value
        End Set
    End Property
    Public Property CountryCode() As ColField(Of String)
        Get
            CountryCode = _CountryCode
        End Get
        Set(ByVal value As ColField(Of String))
            _CountryCode = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function FlagAllAsDeleted() As Boolean
        Oasys3DB.ExecuteSql("UPDATE STRMAS SET DELC = '1' where delc = '0'")
    End Function

#End Region

End Class



