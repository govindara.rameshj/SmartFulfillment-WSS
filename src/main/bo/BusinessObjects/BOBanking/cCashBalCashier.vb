﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("BOBanking.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                     "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                     "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                     "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                     "54e0a4a4")> 
<Serializable()> Public Class cCashBalCashier
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CashBalCashier"
        BOFields.Add(_PeriodID)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_CashierID)
        BOFields.Add(_GrossSalesAmount)
        BOFields.Add(_DiscountAmount)
        BOFields.Add(_SalesCount)
        BOFields.Add(_SalesAmount)
        BOFields.Add(_SalesCorrectCount)
        BOFields.Add(_SalesCorrectAmount)
        BOFields.Add(_RefundCount)
        BOFields.Add(_RefundAmount)
        BOFields.Add(_RefundCorrectCount)
        BOFields.Add(_RefundCorrectAmount)
        BOFields.Add(_FloatIssued)
        BOFields.Add(_FloatReturned)
        BOFields.Add(_MiscIncomeCount01)
        BOFields.Add(_MiscIncomeCount02)
        BOFields.Add(_MiscIncomeCount03)
        BOFields.Add(_MiscIncomeCount04)
        BOFields.Add(_MiscIncomeCount05)
        BOFields.Add(_MiscIncomeCount06)
        BOFields.Add(_MiscIncomeCount07)
        BOFields.Add(_MiscIncomeCount08)
        BOFields.Add(_MiscIncomeCount09)
        BOFields.Add(_MiscIncomeCount10)
        BOFields.Add(_MiscIncomeCount11)
        BOFields.Add(_MiscIncomeCount12)
        BOFields.Add(_MiscIncomeCount13)
        BOFields.Add(_MiscIncomeCount14)
        BOFields.Add(_MiscIncomeCount15)
        BOFields.Add(_MiscIncomeCount16)
        BOFields.Add(_MiscIncomeCount17)
        BOFields.Add(_MiscIncomeCount18)
        BOFields.Add(_MiscIncomeCount19)
        BOFields.Add(_MiscIncomeCount20)
        BOFields.Add(_MiscIncomeValue01)
        BOFields.Add(_MiscIncomeValue02)
        BOFields.Add(_MiscIncomeValue03)
        BOFields.Add(_MiscIncomeValue04)
        BOFields.Add(_MiscIncomeValue05)
        BOFields.Add(_MiscIncomeValue06)
        BOFields.Add(_MiscIncomeValue07)
        BOFields.Add(_MiscIncomeValue08)
        BOFields.Add(_MiscIncomeValue09)
        BOFields.Add(_MiscIncomeValue10)
        BOFields.Add(_MiscIncomeValue11)
        BOFields.Add(_MiscIncomeValue12)
        BOFields.Add(_MiscIncomeValue13)
        BOFields.Add(_MiscIncomeValue14)
        BOFields.Add(_MiscIncomeValue15)
        BOFields.Add(_MiscIncomeValue16)
        BOFields.Add(_MiscIncomeValue17)
        BOFields.Add(_MiscIncomeValue18)
        BOFields.Add(_MiscIncomeValue19)
        BOFields.Add(_MiscIncomeValue20)
        BOFields.Add(_MisOutCount01)
        BOFields.Add(_MisOutCount02)
        BOFields.Add(_MisOutCount03)
        BOFields.Add(_MisOutCount04)
        BOFields.Add(_MisOutCount05)
        BOFields.Add(_MisOutCount06)
        BOFields.Add(_MisOutCount07)
        BOFields.Add(_MisOutCount08)
        BOFields.Add(_MisOutCount09)
        BOFields.Add(_MisOutCount10)
        BOFields.Add(_MisOutCount11)
        BOFields.Add(_MisOutCount12)
        BOFields.Add(_MisOutCount13)
        BOFields.Add(_MisOutCount14)
        BOFields.Add(_MisOutCount15)
        BOFields.Add(_MisOutCount16)
        BOFields.Add(_MisOutCount17)
        BOFields.Add(_MisOutCount18)
        BOFields.Add(_MisOutCount19)
        BOFields.Add(_MisOutCount20)
        BOFields.Add(_MisOutValue01)
        BOFields.Add(_MisOutValue02)
        BOFields.Add(_MisOutValue03)
        BOFields.Add(_MisOutValue04)
        BOFields.Add(_MisOutValue05)
        BOFields.Add(_MisOutValue06)
        BOFields.Add(_MisOutValue07)
        BOFields.Add(_MisOutValue08)
        BOFields.Add(_MisOutValue09)
        BOFields.Add(_MisOutValue10)
        BOFields.Add(_MisOutValue11)
        BOFields.Add(_MisOutValue12)
        BOFields.Add(_MisOutValue13)
        BOFields.Add(_MisOutValue14)
        BOFields.Add(_MisOutValue15)
        BOFields.Add(_MisOutValue16)
        BOFields.Add(_MisOutValue17)
        BOFields.Add(_MisOutValue18)
        BOFields.Add(_MisOutValue19)
        BOFields.Add(_MisOutValue20)
        BOFields.Add(_Comment)
        BOFields.Add(_NumTransactions)
        BOFields.Add(_NumCorrections)
        BOFields.Add(_NumOpenDrawer)
        BOFields.Add(_NumLinesReversed)
        BOFields.Add(_NumLinesSold)
        BOFields.Add(_NumLinesScanned)
        BOFields.Add(_NumVoids)
        BOFields.Add(_ExchangeRate)
        BOFields.Add(_ExchangePower)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCashBalCashier)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cCashBalCashier)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cCashBalCashier))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCashBalCashier(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub
    Public Overrides Sub LoadFromRow(ByVal drRow As System.Data.DataRow)

        For Each dcColumn As DataColumn In drRow.Table.Columns
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("PeriodID") : _PeriodID.Value = CInt(drRow(dcColumn))
                    Case ("CurrencyID") : _CurrencyID.Value = CStr(drRow(dcColumn))
                    Case ("CashierID") : _CashierID.Value = CInt(drRow(dcColumn))
                    Case ("GrossSalesAmount") : _GrossSalesAmount.Value = CDec(drRow(dcColumn))
                    Case ("DiscountAmount") : _DiscountAmount.Value = CDec(drRow(dcColumn))
                    Case ("SalesCount") : _SalesCount.Value = CDec(drRow(dcColumn))
                    Case ("SalesAmount") : _SalesAmount.Value = CDec(drRow(dcColumn))
                    Case ("SalesCorrectCount") : _SalesCorrectCount.Value = CDec(drRow(dcColumn))
                    Case ("SalesCorrectAmount") : _SalesCorrectAmount.Value = CDec(drRow(dcColumn))
                    Case ("RefundCount") : _RefundCount.Value = CDec(drRow(dcColumn))
                    Case ("RefundAmount") : _RefundAmount.Value = CDec(drRow(dcColumn))
                    Case ("RefundCorrectCount") : _RefundCorrectCount.Value = CDec(drRow(dcColumn))
                    Case ("RefundCorrectAmount") : _RefundCorrectAmount.Value = CDec(drRow(dcColumn))
                    Case ("FloatIssued") : _FloatIssued.Value = CDec(drRow(dcColumn))
                    Case ("FloatReturned") : _FloatReturned.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount01") : _MiscIncomeCount01.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount02") : _MiscIncomeCount02.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount03") : _MiscIncomeCount03.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount04") : _MiscIncomeCount04.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount05") : _MiscIncomeCount05.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount06") : _MiscIncomeCount06.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount07") : _MiscIncomeCount07.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount08") : _MiscIncomeCount08.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount09") : _MiscIncomeCount09.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount10") : _MiscIncomeCount10.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount11") : _MiscIncomeCount11.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount12") : _MiscIncomeCount12.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount13") : _MiscIncomeCount13.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount14") : _MiscIncomeCount14.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount15") : _MiscIncomeCount15.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount16") : _MiscIncomeCount16.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount17") : _MiscIncomeCount17.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount18") : _MiscIncomeCount18.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount19") : _MiscIncomeCount19.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeCount20") : _MiscIncomeCount20.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue01") : _MiscIncomeValue01.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue02") : _MiscIncomeValue02.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue03") : _MiscIncomeValue03.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue04") : _MiscIncomeValue04.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue05") : _MiscIncomeValue05.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue06") : _MiscIncomeValue06.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue07") : _MiscIncomeValue07.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue08") : _MiscIncomeValue08.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue09") : _MiscIncomeValue09.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue10") : _MiscIncomeValue10.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue11") : _MiscIncomeValue11.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue12") : _MiscIncomeValue12.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue13") : _MiscIncomeValue13.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue14") : _MiscIncomeValue14.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue15") : _MiscIncomeValue15.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue16") : _MiscIncomeValue16.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue17") : _MiscIncomeValue17.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue18") : _MiscIncomeValue18.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue19") : _MiscIncomeValue19.Value = CDec(drRow(dcColumn))
                    Case ("MiscIncomeValue20") : _MiscIncomeValue20.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount01") : _MisOutCount01.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount02") : _MisOutCount02.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount03") : _MisOutCount03.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount04") : _MisOutCount04.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount05") : _MisOutCount05.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount06") : _MisOutCount06.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount07") : _MisOutCount07.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount08") : _MisOutCount08.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount09") : _MisOutCount09.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount10") : _MisOutCount10.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount11") : _MisOutCount11.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount12") : _MisOutCount12.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount13") : _MisOutCount13.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount14") : _MisOutCount14.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount15") : _MisOutCount15.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount16") : _MisOutCount16.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount17") : _MisOutCount17.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount18") : _MisOutCount18.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount19") : _MisOutCount19.Value = CDec(drRow(dcColumn))
                    Case ("MisOutCount20") : _MisOutCount20.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue01") : _MisOutValue01.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue02") : _MisOutValue02.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue03") : _MisOutValue03.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue04") : _MisOutValue04.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue05") : _MisOutValue05.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue06") : _MisOutValue06.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue07") : _MisOutValue07.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue08") : _MisOutValue08.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue09") : _MisOutValue09.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue10") : _MisOutValue10.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue11") : _MisOutValue11.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue12") : _MisOutValue12.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue13") : _MisOutValue13.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue14") : _MisOutValue14.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue15") : _MisOutValue15.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue16") : _MisOutValue16.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue17") : _MisOutValue17.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue18") : _MisOutValue18.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue19") : _MisOutValue19.Value = CDec(drRow(dcColumn))
                    Case ("MisOutValue20") : _MisOutValue20.Value = CDec(drRow(dcColumn))
                    Case ("Comment") : _Comment.Value = CStr(drRow(dcColumn))
                    Case ("NumTransactions") : _NumTransactions.Value = CInt(drRow(dcColumn))
                    Case ("NumCorrections") : _NumCorrections.Value = CInt(drRow(dcColumn))
                    Case ("NumVoids") : _NumVoids.Value = CInt(drRow(dcColumn))
                    Case ("NumLinesReversed") : _NumLinesReversed.Value = CInt(drRow(dcColumn))
                    Case ("NumLinesSold") : _NumLinesSold.Value = CInt(drRow(dcColumn))
                    Case ("NumLinesScanned") : _NumLinesScanned.Value = CInt(drRow(dcColumn))
                    Case ("NumOpenDrawer") : _NumOpenDrawer.Value = CInt(drRow(dcColumn))
                    Case ("ExchangeRate") : _ExchangeRate.Value = CDec(drRow(dcColumn))
                    Case ("ExchangePower") : _ExchangePower.Value = CDec(drRow(dcColumn))
                End Select
            End If
        Next

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", True, False)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _CashierID As New ColField(Of Integer)("CashierID", 0, "Cashier ID", True, False)
    Private _GrossSalesAmount As New ColField(Of Decimal)("GrossSalesAmount", 0, "Sales Amount", False, False, 2)
    Private _DiscountAmount As New ColField(Of Decimal)("DiscountAmount", 0, "Discount Amount", False, False, 2)
    Private _SalesCount As New ColField(Of Decimal)("SalesCount", 0, "Sales Count", False, False)
    Private _SalesAmount As New ColField(Of Decimal)("SalesAmount", 0, "Sales Amount", False, False, 2)
    Private _SalesCorrectCount As New ColField(Of Decimal)("SalesCorrectCount", 0, "Sales Correct Count", False, False)
    Private _SalesCorrectAmount As New ColField(Of Decimal)("SalesCorrectAmount", 0, "Sales Correct Amount", False, False, 2)
    Private _RefundCount As New ColField(Of Decimal)("RefundCount", 0, "Refund Count", False, False)
    Private _RefundAmount As New ColField(Of Decimal)("RefundAmount", 0, "Refund Amount", False, False, 2)
    Private _RefundCorrectCount As New ColField(Of Decimal)("RefundCorrectCount", 0, "Refund Correct Count", False, False)
    Private _RefundCorrectAmount As New ColField(Of Decimal)("RefundCorrectAmount", 0, "Refund Correct Amount", False, False, 2)
    Private _FloatIssued As New ColField(Of Decimal)("FloatIssued", 0, "Float Issued", False, False, 2)
    Private _FloatReturned As New ColField(Of Decimal)("FloatReturned", 0, "Float Returned", False, False, 2)

    Private _MiscIncomeCount01 As New ColField(Of Decimal)("MiscIncomeCount01", 0, "Misc Income Count 01", False, False)
    Private _MiscIncomeCount02 As New ColField(Of Decimal)("MiscIncomeCount02", 0, "Misc Income Count 02", False, False)
    Private _MiscIncomeCount03 As New ColField(Of Decimal)("MiscIncomeCount03", 0, "Misc Income Count 03", False, False)
    Private _MiscIncomeCount04 As New ColField(Of Decimal)("MiscIncomeCount04", 0, "Misc Income Count 04", False, False)
    Private _MiscIncomeCount05 As New ColField(Of Decimal)("MiscIncomeCount05", 0, "Misc Income Count 05", False, False)
    Private _MiscIncomeCount06 As New ColField(Of Decimal)("MiscIncomeCount06", 0, "Misc Income Count 06", False, False)
    Private _MiscIncomeCount07 As New ColField(Of Decimal)("MiscIncomeCount07", 0, "Misc Income Count 07", False, False)
    Private _MiscIncomeCount08 As New ColField(Of Decimal)("MiscIncomeCount08", 0, "Misc Income Count 08", False, False)
    Private _MiscIncomeCount09 As New ColField(Of Decimal)("MiscIncomeCount09", 0, "Misc Income Count 09", False, False)
    Private _MiscIncomeCount10 As New ColField(Of Decimal)("MiscIncomeCount10", 0, "Misc Income Count 10", False, False)
    Private _MiscIncomeCount11 As New ColField(Of Decimal)("MiscIncomeCount11", 0, "Misc Income Count 11", False, False)
    Private _MiscIncomeCount12 As New ColField(Of Decimal)("MiscIncomeCount12", 0, "Misc Income Count 12", False, False)
    Private _MiscIncomeCount13 As New ColField(Of Decimal)("MiscIncomeCount13", 0, "Misc Income Count 13", False, False)
    Private _MiscIncomeCount14 As New ColField(Of Decimal)("MiscIncomeCount14", 0, "Misc Income Count 14", False, False)
    Private _MiscIncomeCount15 As New ColField(Of Decimal)("MiscIncomeCount15", 0, "Misc Income Count 15", False, False)
    Private _MiscIncomeCount16 As New ColField(Of Decimal)("MiscIncomeCount16", 0, "Misc Income Count 16", False, False)
    Private _MiscIncomeCount17 As New ColField(Of Decimal)("MiscIncomeCount17", 0, "Misc Income Count 17", False, False)
    Private _MiscIncomeCount18 As New ColField(Of Decimal)("MiscIncomeCount18", 0, "Misc Income Count 18", False, False)
    Private _MiscIncomeCount19 As New ColField(Of Decimal)("MiscIncomeCount19", 0, "Misc Income Count 19", False, False)
    Private _MiscIncomeCount20 As New ColField(Of Decimal)("MiscIncomeCount20", 0, "Misc Income Count 20", False, False)
    Private _MiscIncomeValue01 As New ColField(Of Decimal)("MiscIncomeValue01", 0, "Misc Income Value 01", False, False, 2)
    Private _MiscIncomeValue02 As New ColField(Of Decimal)("MiscIncomeValue02", 0, "Misc Income Value 02", False, False, 2)
    Private _MiscIncomeValue03 As New ColField(Of Decimal)("MiscIncomeValue03", 0, "Misc Income Value 03", False, False, 2)
    Private _MiscIncomeValue04 As New ColField(Of Decimal)("MiscIncomeValue04", 0, "Misc Income Value 04", False, False, 2)
    Private _MiscIncomeValue05 As New ColField(Of Decimal)("MiscIncomeValue05", 0, "Misc Income Value 05", False, False, 2)
    Private _MiscIncomeValue06 As New ColField(Of Decimal)("MiscIncomeValue06", 0, "Misc Income Value 06", False, False, 2)
    Private _MiscIncomeValue07 As New ColField(Of Decimal)("MiscIncomeValue07", 0, "Misc Income Value 07", False, False, 2)
    Private _MiscIncomeValue08 As New ColField(Of Decimal)("MiscIncomeValue08", 0, "Misc Income Value 08", False, False, 2)
    Private _MiscIncomeValue09 As New ColField(Of Decimal)("MiscIncomeValue09", 0, "Misc Income Value 09", False, False, 2)
    Private _MiscIncomeValue10 As New ColField(Of Decimal)("MiscIncomeValue10", 0, "Misc Income Value 10", False, False, 2)
    Private _MiscIncomeValue11 As New ColField(Of Decimal)("MiscIncomeValue11", 0, "Misc Income Value 11", False, False, 2)
    Private _MiscIncomeValue12 As New ColField(Of Decimal)("MiscIncomeValue12", 0, "Misc Income Value 12", False, False, 2)
    Private _MiscIncomeValue13 As New ColField(Of Decimal)("MiscIncomeValue13", 0, "Misc Income Value 13", False, False, 2)
    Private _MiscIncomeValue14 As New ColField(Of Decimal)("MiscIncomeValue14", 0, "Misc Income Value 14", False, False, 2)
    Private _MiscIncomeValue15 As New ColField(Of Decimal)("MiscIncomeValue15", 0, "Misc Income Value 15", False, False, 2)
    Private _MiscIncomeValue16 As New ColField(Of Decimal)("MiscIncomeValue16", 0, "Misc Income Value 16", False, False, 2)
    Private _MiscIncomeValue17 As New ColField(Of Decimal)("MiscIncomeValue17", 0, "Misc Income Value 17", False, False, 2)
    Private _MiscIncomeValue18 As New ColField(Of Decimal)("MiscIncomeValue18", 0, "Misc Income Value 18", False, False, 2)
    Private _MiscIncomeValue19 As New ColField(Of Decimal)("MiscIncomeValue19", 0, "Misc Income Value 19", False, False, 2)
    Private _MiscIncomeValue20 As New ColField(Of Decimal)("MiscIncomeValue20", 0, "Misc Income Value 20", False, False, 2)

    Private _MisOutCount01 As New ColField(Of Decimal)("MisOutCount01", 0, "Misc Out Count 01", False, False)
    Private _MisOutCount02 As New ColField(Of Decimal)("MisOutCount02", 0, "Misc Out Count 02", False, False)
    Private _MisOutCount03 As New ColField(Of Decimal)("MisOutCount03", 0, "Misc Out Count 03", False, False)
    Private _MisOutCount04 As New ColField(Of Decimal)("MisOutCount04", 0, "Misc Out Count 04", False, False)
    Private _MisOutCount05 As New ColField(Of Decimal)("MisOutCount05", 0, "Misc Out Count 05", False, False)
    Private _MisOutCount06 As New ColField(Of Decimal)("MisOutCount06", 0, "Misc Out Count 06", False, False)
    Private _MisOutCount07 As New ColField(Of Decimal)("MisOutCount07", 0, "Misc Out Count 07", False, False)
    Private _MisOutCount08 As New ColField(Of Decimal)("MisOutCount08", 0, "Misc Out Count 08", False, False)
    Private _MisOutCount09 As New ColField(Of Decimal)("MisOutCount09", 0, "Misc Out Count 09", False, False)
    Private _MisOutCount10 As New ColField(Of Decimal)("MisOutCount10", 0, "Misc Out Count 10", False, False)
    Private _MisOutCount11 As New ColField(Of Decimal)("MisOutCount11", 0, "Misc Out Count 11", False, False)
    Private _MisOutCount12 As New ColField(Of Decimal)("MisOutCount12", 0, "Misc Out Count 12", False, False)
    Private _MisOutCount13 As New ColField(Of Decimal)("MisOutCount13", 0, "Misc Out Count 13", False, False)
    Private _MisOutCount14 As New ColField(Of Decimal)("MisOutCount14", 0, "Misc Out Count 14", False, False)
    Private _MisOutCount15 As New ColField(Of Decimal)("MisOutCount15", 0, "Misc Out Count 15", False, False)
    Private _MisOutCount16 As New ColField(Of Decimal)("MisOutCount16", 0, "Misc Out Count 16", False, False)
    Private _MisOutCount17 As New ColField(Of Decimal)("MisOutCount17", 0, "Misc Out Count 17", False, False)
    Private _MisOutCount18 As New ColField(Of Decimal)("MisOutCount18", 0, "Misc Out Count 18", False, False)
    Private _MisOutCount19 As New ColField(Of Decimal)("MisOutCount19", 0, "Misc Out Count 19", False, False)
    Private _MisOutCount20 As New ColField(Of Decimal)("MisOutCount20", 0, "Misc Out Count 20", False, False)
    Private _MisOutValue01 As New ColField(Of Decimal)("MisOutValue01", 0, "Misc Out Value 01", False, False, 2)
    Private _MisOutValue02 As New ColField(Of Decimal)("MisOutValue02", 0, "Misc Out Value 02", False, False, 2)
    Private _MisOutValue03 As New ColField(Of Decimal)("MisOutValue03", 0, "Misc Out Value 03", False, False, 2)
    Private _MisOutValue04 As New ColField(Of Decimal)("MisOutValue04", 0, "Misc Out Value 04", False, False, 2)
    Private _MisOutValue05 As New ColField(Of Decimal)("MisOutValue05", 0, "Misc Out Value 05", False, False, 2)
    Private _MisOutValue06 As New ColField(Of Decimal)("MisOutValue06", 0, "Misc Out Value 06", False, False, 2)
    Private _MisOutValue07 As New ColField(Of Decimal)("MisOutValue07", 0, "Misc Out Value 07", False, False, 2)
    Private _MisOutValue08 As New ColField(Of Decimal)("MisOutValue08", 0, "Misc Out Value 08", False, False, 2)
    Private _MisOutValue09 As New ColField(Of Decimal)("MisOutValue09", 0, "Misc Out Value 09", False, False, 2)
    Private _MisOutValue10 As New ColField(Of Decimal)("MisOutValue10", 0, "Misc Out Value 10", False, False, 2)
    Private _MisOutValue11 As New ColField(Of Decimal)("MisOutValue11", 0, "Misc Out Value 11", False, False, 2)
    Private _MisOutValue12 As New ColField(Of Decimal)("MisOutValue12", 0, "Misc Out Value 12", False, False, 2)
    Private _MisOutValue13 As New ColField(Of Decimal)("MisOutValue13", 0, "Misc Out Value 13", False, False, 2)
    Private _MisOutValue14 As New ColField(Of Decimal)("MisOutValue14", 0, "Misc Out Value 14", False, False, 2)
    Private _MisOutValue15 As New ColField(Of Decimal)("MisOutValue15", 0, "Misc Out Value 15", False, False, 2)
    Private _MisOutValue16 As New ColField(Of Decimal)("MisOutValue16", 0, "Misc Out Value 16", False, False, 2)
    Private _MisOutValue17 As New ColField(Of Decimal)("MisOutValue17", 0, "Misc Out Value 17", False, False, 2)
    Private _MisOutValue18 As New ColField(Of Decimal)("MisOutValue18", 0, "Misc Out Value 18", False, False, 2)
    Private _MisOutValue19 As New ColField(Of Decimal)("MisOutValue19", 0, "Misc Out Value 19", False, False, 2)
    Private _MisOutValue20 As New ColField(Of Decimal)("MisOutValue20", 0, "Misc Out Value 20", False, False, 2)

    Private _ExchangeRate As New ColField(Of Decimal)("ExchangeRate", 0, "Exchange Rate", False, False, 4)
    Private _ExchangePower As New ColField(Of Decimal)("ExchangePower", 0, "Exchange Power", False, False)
    Private _Comment As New ColField(Of String)("Comment", "")
    Private _NumTransactions As New ColField(Of Integer)("NumTransactions", 0, "Number Transactions", False, False)
    Private _NumCorrections As New ColField(Of Integer)("NumCorrections", 0, "Number Corrections", False, False)
    Private _NumVoids As New ColField(Of Integer)("NumVoids", 0, "Number Voids", False, False)
    Private _NumOpenDrawer As New ColField(Of Integer)("NumOpenDrawer", 0, "Number Open Drawers", False, False)
    Private _NumLinesReversed As New ColField(Of Integer)("NumLinesReversed", 0, "Number Lines Reversed", False, False)
    Private _NumLinesSold As New ColField(Of Integer)("NumLinesSold", 0, "Number Lines Sold", False, False)
    Private _NumLinesScanned As New ColField(Of Integer)("NumLinesScanned", 0, "Number Lines Scanned", False, False)

#End Region

#Region "Field Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property CashierID() As ColField(Of Integer)
        Get
            CashierID = _CashierID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _CashierID = value
        End Set
    End Property
    Public Property GrossSalesAmount() As ColField(Of Decimal)
        Get
            GrossSalesAmount = _GrossSalesAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _GrossSalesAmount = value
        End Set
    End Property
    Public Property DiscountAmount() As ColField(Of Decimal)
        Get
            DiscountAmount = _DiscountAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _DiscountAmount = value
        End Set
    End Property
    Public Property SalesCount() As ColField(Of Decimal)
        Get
            SalesCount = _SalesCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesCount = value
        End Set
    End Property
    Public Property SalesAmount() As ColField(Of Decimal)
        Get
            SalesAmount = _SalesAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesAmount = value
        End Set
    End Property
    Public Property SalesCorrectCount() As ColField(Of Decimal)
        Get
            SalesCorrectCount = _SalesCorrectCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesCorrectCount = value
        End Set
    End Property
    Public Property SalesCorrectAmount() As ColField(Of Decimal)
        Get
            SalesCorrectAmount = _SalesCorrectAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SalesCorrectAmount = value
        End Set
    End Property
    Public Property RefundCount() As ColField(Of Decimal)
        Get
            RefundCount = _RefundCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundCount = value
        End Set
    End Property
    Public Property RefundAmount() As ColField(Of Decimal)
        Get
            RefundAmount = _RefundAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundAmount = value
        End Set
    End Property
    Public Property RefundCorrectCount() As ColField(Of Decimal)
        Get
            RefundCorrectCount = _RefundCorrectCount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundCorrectCount = value
        End Set
    End Property
    Public Property RefundCorrectAmount() As ColField(Of Decimal)
        Get
            RefundCorrectAmount = _RefundCorrectAmount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _RefundCorrectAmount = value
        End Set
    End Property
    Public Property FloatIssued() As ColField(Of Decimal)
        Get
            Return _FloatIssued
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _FloatIssued = value
        End Set
    End Property
    Public Property FloatReturned() As ColField(Of Decimal)
        Get
            Return _FloatReturned
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _FloatReturned = value
        End Set
    End Property

    Public Property MiscIncomeCount(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            Dim ColName As String = MiscIncomeCount01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (CStr(field.ColumnName) = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))

            If Index = 0 Then Index = 7
            Dim ColName As String = MiscIncomeCount01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MiscIncomeCount01() As ColField(Of Decimal)
        Get
            MiscIncomeCount01 = _MiscIncomeCount01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount01 = value
        End Set
    End Property
    Public Property MiscIncomeCount02() As ColField(Of Decimal)
        Get
            MiscIncomeCount02 = _MiscIncomeCount02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount02 = value
        End Set
    End Property
    Public Property MiscIncomeCount03() As ColField(Of Decimal)
        Get
            MiscIncomeCount03 = _MiscIncomeCount03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount03 = value
        End Set
    End Property
    Public Property MiscIncomeCount04() As ColField(Of Decimal)
        Get
            MiscIncomeCount04 = _MiscIncomeCount04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount04 = value
        End Set
    End Property
    Public Property MiscIncomeCount05() As ColField(Of Decimal)
        Get
            MiscIncomeCount05 = _MiscIncomeCount05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount05 = value
        End Set
    End Property
    Public Property MiscIncomeCount06() As ColField(Of Decimal)
        Get
            MiscIncomeCount06 = _MiscIncomeCount06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount06 = value
        End Set
    End Property
    Public Property MiscIncomeCount07() As ColField(Of Decimal)
        Get
            MiscIncomeCount07 = _MiscIncomeCount07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount07 = value
        End Set
    End Property
    Public Property MiscIncomeCount08() As ColField(Of Decimal)
        Get
            MiscIncomeCount08 = _MiscIncomeCount08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount08 = value
        End Set
    End Property
    Public Property MiscIncomeCount09() As ColField(Of Decimal)
        Get
            MiscIncomeCount09 = _MiscIncomeCount09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount09 = value
        End Set
    End Property
    Public Property MiscIncomeCount10() As ColField(Of Decimal)
        Get
            MiscIncomeCount10 = _MiscIncomeCount10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount10 = value
        End Set
    End Property
    Public Property MiscIncomeCount11() As ColField(Of Decimal)
        Get
            MiscIncomeCount11 = _MiscIncomeCount11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount11 = value
        End Set
    End Property
    Public Property MiscIncomeCount12() As ColField(Of Decimal)
        Get
            MiscIncomeCount12 = _MiscIncomeCount12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount12 = value
        End Set
    End Property
    Public Property MiscIncomeCount13() As ColField(Of Decimal)
        Get
            MiscIncomeCount13 = _MiscIncomeCount13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount13 = value
        End Set
    End Property
    Public Property MiscIncomeCount14() As ColField(Of Decimal)
        Get
            MiscIncomeCount14 = _MiscIncomeCount14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount14 = value
        End Set
    End Property
    Public Property MiscIncomeCount15() As ColField(Of Decimal)
        Get
            MiscIncomeCount15 = _MiscIncomeCount15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount15 = value
        End Set
    End Property
    Public Property MiscIncomeCount16() As ColField(Of Decimal)
        Get
            MiscIncomeCount16 = _MiscIncomeCount16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount16 = value
        End Set
    End Property
    Public Property MiscIncomeCount17() As ColField(Of Decimal)
        Get
            MiscIncomeCount17 = _MiscIncomeCount17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount17 = value
        End Set
    End Property
    Public Property MiscIncomeCount18() As ColField(Of Decimal)
        Get
            MiscIncomeCount18 = _MiscIncomeCount18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount18 = value
        End Set
    End Property
    Public Property MiscIncomeCount19() As ColField(Of Decimal)
        Get
            MiscIncomeCount19 = _MiscIncomeCount19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount19 = value
        End Set
    End Property
    Public Property MiscIncomeCount20() As ColField(Of Decimal)
        Get
            MiscIncomeCount20 = _MiscIncomeCount20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeCount20 = value
        End Set
    End Property

    Public Property MiscIncomeValue(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            Dim ColName As String = MiscIncomeValue01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))

            If Index = 0 Then Index = 7
            Dim ColName As String = MiscIncomeValue01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MiscIncomeValue01() As ColField(Of Decimal)
        Get
            MiscIncomeValue01 = _MiscIncomeValue01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue01 = value
        End Set
    End Property
    Public Property MiscIncomeValue02() As ColField(Of Decimal)
        Get
            MiscIncomeValue02 = _MiscIncomeValue02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue02 = value
        End Set
    End Property
    Public Property MiscIncomeValue03() As ColField(Of Decimal)
        Get
            MiscIncomeValue03 = _MiscIncomeValue03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue03 = value
        End Set
    End Property
    Public Property MiscIncomeValue04() As ColField(Of Decimal)
        Get
            MiscIncomeValue04 = _MiscIncomeValue04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue04 = value
        End Set
    End Property
    Public Property MiscIncomeValue05() As ColField(Of Decimal)
        Get
            MiscIncomeValue05 = _MiscIncomeValue05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue05 = value
        End Set
    End Property
    Public Property MiscIncomeValue06() As ColField(Of Decimal)
        Get
            MiscIncomeValue06 = _MiscIncomeValue06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue06 = value
        End Set
    End Property
    Public Property MiscIncomeValue07() As ColField(Of Decimal)
        Get
            MiscIncomeValue07 = _MiscIncomeValue07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue07 = value
        End Set
    End Property
    Public Property MiscIncomeValue08() As ColField(Of Decimal)
        Get
            MiscIncomeValue08 = _MiscIncomeValue08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue08 = value
        End Set
    End Property
    Public Property MiscIncomeValue09() As ColField(Of Decimal)
        Get
            MiscIncomeValue09 = _MiscIncomeValue09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue09 = value
        End Set
    End Property
    Public Property MiscIncomeValue10() As ColField(Of Decimal)
        Get
            MiscIncomeValue10 = _MiscIncomeValue10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue10 = value
        End Set
    End Property
    Public Property MiscIncomeValue11() As ColField(Of Decimal)
        Get
            MiscIncomeValue11 = _MiscIncomeValue11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue11 = value
        End Set
    End Property
    Public Property MiscIncomeValue12() As ColField(Of Decimal)
        Get
            MiscIncomeValue12 = _MiscIncomeValue12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue12 = value
        End Set
    End Property
    Public Property MiscIncomeValue13() As ColField(Of Decimal)
        Get
            MiscIncomeValue13 = _MiscIncomeValue13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue13 = value
        End Set
    End Property
    Public Property MiscIncomeValue14() As ColField(Of Decimal)
        Get
            MiscIncomeValue14 = _MiscIncomeValue14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue14 = value
        End Set
    End Property
    Public Property MiscIncomeValue15() As ColField(Of Decimal)
        Get
            MiscIncomeValue15 = _MiscIncomeValue15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue15 = value
        End Set
    End Property
    Public Property MiscIncomeValue16() As ColField(Of Decimal)
        Get
            MiscIncomeValue16 = _MiscIncomeValue16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue16 = value
        End Set
    End Property
    Public Property MiscIncomeValue17() As ColField(Of Decimal)
        Get
            MiscIncomeValue17 = _MiscIncomeValue17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue17 = value
        End Set
    End Property
    Public Property MiscIncomeValue18() As ColField(Of Decimal)
        Get
            MiscIncomeValue18 = _MiscIncomeValue18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue18 = value
        End Set
    End Property
    Public Property MiscIncomeValue19() As ColField(Of Decimal)
        Get
            MiscIncomeValue19 = _MiscIncomeValue19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue19 = value
        End Set
    End Property
    Public Property MiscIncomeValue20() As ColField(Of Decimal)
        Get
            MiscIncomeValue20 = _MiscIncomeValue20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MiscIncomeValue20 = value
        End Set
    End Property

    Public Property MisOutCount(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            Dim ColName As String = MisOutCount01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))

            If Index = 0 Then Index = 7
            Dim ColName As String = MisOutCount01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MisOutCount01() As ColField(Of Decimal)
        Get
            MisOutCount01 = _MisOutCount01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount01 = value
        End Set
    End Property
    Public Property MisOutCount02() As ColField(Of Decimal)
        Get
            MisOutCount02 = _MisOutCount02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount02 = value
        End Set
    End Property
    Public Property MisOutCount03() As ColField(Of Decimal)
        Get
            MisOutCount03 = _MisOutCount03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount03 = value
        End Set
    End Property
    Public Property MisOutCount04() As ColField(Of Decimal)
        Get
            MisOutCount04 = _MisOutCount04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount04 = value
        End Set
    End Property
    Public Property MisOutCount05() As ColField(Of Decimal)
        Get
            MisOutCount05 = _MisOutCount05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount05 = value
        End Set
    End Property
    Public Property MisOutCount06() As ColField(Of Decimal)
        Get
            MisOutCount06 = _MisOutCount06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount06 = value
        End Set
    End Property
    Public Property MisOutCount07() As ColField(Of Decimal)
        Get
            MisOutCount07 = _MisOutCount07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount07 = value
        End Set
    End Property
    Public Property MisOutCount08() As ColField(Of Decimal)
        Get
            MisOutCount08 = _MisOutCount08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount08 = value
        End Set
    End Property
    Public Property MisOutCount09() As ColField(Of Decimal)
        Get
            MisOutCount09 = _MisOutCount09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount09 = value
        End Set
    End Property
    Public Property MisOutCount10() As ColField(Of Decimal)
        Get
            MisOutCount10 = _MisOutCount10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount10 = value
        End Set
    End Property
    Public Property MisOutCount11() As ColField(Of Decimal)
        Get
            MisOutCount11 = _MisOutCount11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount11 = value
        End Set
    End Property
    Public Property MisOutCount12() As ColField(Of Decimal)
        Get
            MisOutCount12 = _MisOutCount12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount12 = value
        End Set
    End Property
    Public Property MisOutCount13() As ColField(Of Decimal)
        Get
            MisOutCount13 = _MisOutCount13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount13 = value
        End Set
    End Property
    Public Property MisOutCount14() As ColField(Of Decimal)
        Get
            MisOutCount14 = _MisOutCount14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount14 = value
        End Set
    End Property
    Public Property MisOutCount15() As ColField(Of Decimal)
        Get
            MisOutCount15 = _MisOutCount15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount15 = value
        End Set
    End Property
    Public Property MisOutCount16() As ColField(Of Decimal)
        Get
            MisOutCount16 = _MisOutCount16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount16 = value
        End Set
    End Property
    Public Property MisOutCount17() As ColField(Of Decimal)
        Get
            MisOutCount17 = _MisOutCount17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount17 = value
        End Set
    End Property
    Public Property MisOutCount18() As ColField(Of Decimal)
        Get
            MisOutCount18 = _MisOutCount18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount18 = value
        End Set
    End Property
    Public Property MisOutCount19() As ColField(Of Decimal)
        Get
            MisOutCount19 = _MisOutCount19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount19 = value
        End Set
    End Property
    Public Property MisOutCount20() As ColField(Of Decimal)
        Get
            MisOutCount20 = _MisOutCount20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutCount20 = value
        End Set
    End Property

    Public Property MisOutValue(ByVal Index As Integer) As ColField(Of Decimal)
        Get
            Dim ColName As String = MisOutValue01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    Return CType(field, Global.OasysDBBO.ColField(Of Decimal))
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As ColField(Of Decimal))

            If Index = 0 Then Index = 7
            Dim ColName As String = MisOutValue01.ColumnName
            ColName = ColName.Substring(0, ColName.Length - 2) & Index.ToString.PadLeft(2, "0"c)

            For Each field As IColField In BOFields
                If (field.ColumnName = ColName) Then
                    CType(field, Global.OasysDBBO.ColField(Of Decimal)).Value = value.Value
                    Exit For
                End If
            Next
        End Set
    End Property
    Public Property MisOutValue01() As ColField(Of Decimal)
        Get
            MisOutValue01 = _MisOutValue01
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue01 = value
        End Set
    End Property
    Public Property MisOutValue02() As ColField(Of Decimal)
        Get
            MisOutValue02 = _MisOutValue02
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue02 = value
        End Set
    End Property
    Public Property MisOutValue03() As ColField(Of Decimal)
        Get
            MisOutValue03 = _MisOutValue03
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue03 = value
        End Set
    End Property
    Public Property MisOutValue04() As ColField(Of Decimal)
        Get
            MisOutValue04 = _MisOutValue04
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue04 = value
        End Set
    End Property
    Public Property MisOutValue05() As ColField(Of Decimal)
        Get
            MisOutValue05 = _MisOutValue05
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue05 = value
        End Set
    End Property
    Public Property MisOutValue06() As ColField(Of Decimal)
        Get
            MisOutValue06 = _MisOutValue06
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue06 = value
        End Set
    End Property
    Public Property MisOutValue07() As ColField(Of Decimal)
        Get
            MisOutValue07 = _MisOutValue07
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue07 = value
        End Set
    End Property
    Public Property MisOutValue08() As ColField(Of Decimal)
        Get
            MisOutValue08 = _MisOutValue08
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue08 = value
        End Set
    End Property
    Public Property MisOutValue09() As ColField(Of Decimal)
        Get
            MisOutValue09 = _MisOutValue09
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue09 = value
        End Set
    End Property
    Public Property MisOutValue10() As ColField(Of Decimal)
        Get
            MisOutValue10 = _MisOutValue10
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue10 = value
        End Set
    End Property
    Public Property MisOutValue11() As ColField(Of Decimal)
        Get
            MisOutValue11 = _MisOutValue11
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue11 = value
        End Set
    End Property
    Public Property MisOutValue12() As ColField(Of Decimal)
        Get
            MisOutValue12 = _MisOutValue12
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue12 = value
        End Set
    End Property
    Public Property MisOutValue13() As ColField(Of Decimal)
        Get
            MisOutValue13 = _MisOutValue13
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue13 = value
        End Set
    End Property
    Public Property MisOutValue14() As ColField(Of Decimal)
        Get
            MisOutValue14 = _MisOutValue14
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue14 = value
        End Set
    End Property
    Public Property MisOutValue15() As ColField(Of Decimal)
        Get
            MisOutValue15 = _MisOutValue15
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue15 = value
        End Set
    End Property
    Public Property MisOutValue16() As ColField(Of Decimal)
        Get
            MisOutValue16 = _MisOutValue16
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue16 = value
        End Set
    End Property
    Public Property MisOutValue17() As ColField(Of Decimal)
        Get
            MisOutValue17 = _MisOutValue17
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue17 = value
        End Set
    End Property
    Public Property MisOutValue18() As ColField(Of Decimal)
        Get
            MisOutValue18 = _MisOutValue18
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue18 = value
        End Set
    End Property
    Public Property MisOutValue19() As ColField(Of Decimal)
        Get
            MisOutValue19 = _MisOutValue19
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue19 = value
        End Set
    End Property
    Public Property MisOutValue20() As ColField(Of Decimal)
        Get
            MisOutValue20 = _MisOutValue20
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _MisOutValue20 = value
        End Set
    End Property
    Public Property ExchangeRate() As ColField(Of Decimal)
        Get
            Return _ExchangeRate
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExchangeRate = value
        End Set
    End Property
    Public Property ExchangePower() As ColField(Of Decimal)
        Get
            Return _ExchangePower
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ExchangePower = value
        End Set
    End Property
    Public Property Comment() As ColField(Of String)
        Get
            Comment = _Comment
        End Get
        Set(ByVal value As ColField(Of String))
            _Comment = value
        End Set
    End Property

    Public Property NumTransactions() As ColField(Of Integer)
        Get
            Return _NumTransactions
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumTransactions = value
        End Set
    End Property
    Public Property NumCorrections() As ColField(Of Integer)
        Get
            Return _NumCorrections
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumCorrections = value
        End Set
    End Property
    Public Property NumVoids() As ColField(Of Integer)
        Get
            Return _NumVoids
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumVoids = value
        End Set
    End Property
    Public Property NumLinesReversed() As ColField(Of Integer)
        Get
            Return _NumLinesReversed
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumLinesReversed = value
        End Set
    End Property
    Public Property NumLinesSold() As ColField(Of Integer)
        Get
            Return _NumLinesSold
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumLinesSold = value
        End Set
    End Property
    Public Property NumLinesScanned() As ColField(Of Integer)
        Get
            Return _NumLinesScanned
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumLinesScanned = value
        End Set
    End Property
    Public Property NumOpenDrawer() As ColField(Of Integer)
        Get
            Return _NumOpenDrawer
        End Get
        Set(ByVal value As ColField(Of Integer))
            _NumOpenDrawer = value
        End Set
    End Property

#End Region

#Region "Dataset"
    Private _ds As New DataSet
    Public ReadOnly Property CashierDataset() As DataSet
        Get
            Return _ds
        End Get
    End Property

#End Region

#Region "Entities"
    Private _Cashiers As List(Of cCashBalCashier) = Nothing
    Private _TenderTypes As List(Of cCashBalCashierTen) = Nothing
    Private _tenderVariances As List(Of cCashBalCashierTenVar) = Nothing
    Private _tenderRelatedVariances As List(Of cCashBalCashierTenVar) = Nothing

    Public Property Cashier(ByVal PeriodID As Integer, ByVal CurrencyID As String, ByVal CashierID As Integer) As cCashBalCashier
        Get
            If _Cashiers Is Nothing Then 'Load cashiers from database.
                Dim cash As New cCashBalCashier(Oasys3DB)
                cash.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, cash.PeriodID, PeriodID)
                cash.SortBy(cash.CashierID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                _Cashiers = cash.LoadMatches
            End If

            For Each cash As cCashBalCashier In _Cashiers
                If (cash.CurrencyID.Value = CurrencyID) And (cash.CashierID.Value = CashierID) Then Return cash
            Next

            'If here then cashier header does not exist so create new one and insert into database.
            Dim newCash As New cCashBalCashier(Oasys3DB)
            newCash.PeriodID.Value = PeriodID
            newCash.CurrencyID.Value = CurrencyID
            newCash.CashierID.Value = CashierID
            newCash.SaveIfNew()
            _Cashiers.Add(newCash)
            Return newCash
        End Get
        Set(ByVal value As cCashBalCashier)
            For Each c As cCashBalCashier In _Cashiers
                If (c.CurrencyID.Value = CurrencyID) And (c.CashierID.Value = CashierID) Then
                    c = value
                End If
            Next
        End Set
    End Property
    Public Property Cashiers() As List(Of cCashBalCashier)
        Get
            Return _Cashiers
        End Get
        Set(ByVal value As List(Of cCashBalCashier))
            _Cashiers = value
        End Set
    End Property

    Public Property TenderType(ByVal TenderTypeID As Integer) As cCashBalCashierTen
        Get
            For Each ttype As cCashBalCashierTen In TenderTypes
                If (ttype.ID.Value = TenderTypeID) Then Return ttype
            Next

            Dim newttype As New cCashBalCashierTen(Oasys3DB)
            newttype.PeriodID.Value = _PeriodID.Value
            newttype.CashierID.Value = _CashierID.Value
            newttype.CurrencyID.Value = _CurrencyID.Value
            newttype.ID.Value = TenderTypeID
            _TenderTypes.Add(newttype)
            Return newttype
        End Get
        Set(ByVal value As cCashBalCashierTen)
            For Each ten As cCashBalCashierTen In _TenderTypes
                If (ten.ID.Value = TenderTypeID) Then
                    ten = value
                End If
            Next
        End Set
    End Property
    Public Property TenderTypes() As List(Of cCashBalCashierTen)
        Get
            If _TenderTypes Is Nothing Then
                Dim ten As New cCashBalCashierTen(Oasys3DB)
                ten.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, ten.PeriodID, _PeriodID.Value)
                ten.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                ten.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, ten.CashierID, _CashierID.Value)
                ten.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                ten.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, ten.CurrencyID, _CurrencyID.Value)
                _TenderTypes = ten.LoadMatches
            End If
            Return _TenderTypes
        End Get
        Set(ByVal value As List(Of cCashBalCashierTen))
            _TenderTypes = value
        End Set
    End Property

    Public ReadOnly Property TenderVariances() As List(Of cCashBalCashierTenVar)
        Get
            If _tenderVariances Is Nothing Then LoadTenderVariances()
            Return _tenderVariances
        End Get
    End Property
    Public ReadOnly Property TenderRelatedVariances() As List(Of cCashBalCashierTenVar)
        Get
            If _tenderRelatedVariances Is Nothing Then LoadTenderRelatedVariances()
            Return _tenderRelatedVariances
        End Get
    End Property
#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads all cashiers and tenders for given period into CashierDataset
    ''' </summary>
    ''' <param name="periodId"></param>
    ''' <remarks></remarks>
    Public Sub CashierLoad(ByVal periodId As Integer)

        Try
            'get cashiers
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
            Oasys3DB.SetOrderByParameter(_CashierID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            _ds = Oasys3DB.Query
            _ds.Tables(0).TableName = "Cashiers"

            'get denominations
            Dim tenders As New cCashBalCashierTen(Oasys3DB)
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(tenders.TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(tenders.PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0).Copy
            dt.TableName = "Tenders"
            _ds.Tables.Add(dt)

            'set up relation
            Dim parentCols As DataColumn() = New DataColumn() {_ds.Tables(0).Columns(_PeriodID.ColumnName), _ds.Tables(0).Columns(_CurrencyID.ColumnName), _ds.Tables(0).Columns(_CashierID.ColumnName)}
            Dim childCols As DataColumn() = New DataColumn() {_ds.Tables(1).Columns(tenders.PeriodID.ColumnName), _ds.Tables(1).Columns(tenders.CurrencyID.ColumnName), _ds.Tables(1).Columns(tenders.CashierID.ColumnName)}

            _ds.Relations.Add("Tenders", parentCols, childCols)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all cashiers available for given period ID into cashiers collection of this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <remarks></remarks>
    Public Sub LoadCashiers(ByVal periodID As Integer)

        Try
            _Cashiers = New List(Of cCashBalCashier)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
            Oasys3DB.SetOrderByParameter(_CashierID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'add cashiers found to collection
            For Each dr As DataRow In dt.Rows
                Dim c As New cCashBalCashier(Oasys3DB)
                c.LoadFromRow(dr)
                _Cashiers.Add(c)
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all cashiers available for given period ID into cashiers collection of this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="StartPeriodID"></param>
    ''' <param name="EndPeriodID"></param>
    ''' <remarks></remarks>
    Public Sub LoadCashiers(ByVal StartPeriodID As Integer, ByVal EndPeriodID As Integer)

        Try
            Dim c As New BOBanking.cCashBalCashier(Oasys3DB)
            c.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, c.PeriodID, StartPeriodID)
            c.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            c.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, c.PeriodID, EndPeriodID)
            c.SortBy(c.CashierID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            _Cashiers = c.LoadMatches

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads cashier for given parameters into this instance or creates new record if one not found.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="CashierID"></param>
    ''' <param name="PeriodID"></param>
    ''' <param name="CurrencyID"></param>
    ''' <remarks></remarks>
    Public Sub LoadCashier(ByVal CashierID As Integer, ByVal PeriodID As Integer, ByVal CurrencyID As String)

        Try
            ClearLists()
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _CashierID, CashierID)
            JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _PeriodID, PeriodID)
            JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(clsOasys3DB.eOperator.pEquals, _CurrencyID, CurrencyID)
            If LoadMatches.Count = 0 Then
                _CashierID.Value = CashierID
                _PeriodID.Value = PeriodID
                _CurrencyID.Value = CurrencyID
                SaveIfNew()
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads cashier records for all currencies for given parameters into Cashiers collection of this instance.
    '''  Throws Oasys exception on error or no cashier found
    ''' </summary>
    ''' <param name="PeriodID"></param>
    ''' <param name="cashierID"></param>
    ''' <remarks></remarks>
    Public Sub LoadCashier(ByVal periodID As Integer, ByVal cashierID As Integer)

        Try
            _Cashiers = New List(Of cCashBalCashier)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_CashierID.ColumnName, clsOasys3DB.eOperator.pEquals, cashierID)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'add cashiers found to collection
            For Each dr As DataRow In dt.Rows
                Dim c As New cCashBalCashier(Oasys3DB)
                c.LoadFromRow(dr)
                _Cashiers.Add(c)
            Next

            'check for any cashiers found
            If _Cashiers.Count = 0 Then
                Throw New OasysDbException(My.Resources.Errors.CashierNotFound)
            End If

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Returns list of cashiers for given period
    ''' </summary>
    ''' <param name="periodId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCashiers(ByVal periodId As Integer) As List(Of cCashBalCashier)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'add cashiers found to collection
            Dim cashiers As New List(Of cCashBalCashier)
            For Each dr As DataRow In dt.Rows
                Dim c As New cCashBalCashier(Oasys3DB)
                c.LoadFromRow(dr)
                cashiers.Add(c)
            Next

            Return cashiers

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns list of cashiers for given period and cashier ids
    ''' </summary>
    ''' <param name="periodId"></param>
    ''' <param name="cashierId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCashiers(ByVal periodId As Integer, ByVal cashierId As Integer) As List(Of cCashBalCashier)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_CashierID.ColumnName, clsOasys3DB.eOperator.pEquals, cashierId)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            'add cashiers found to collection
            Dim cashiers As New List(Of cCashBalCashier)
            For Each dr As DataRow In dt.Rows
                Dim c As New cCashBalCashier(Oasys3DB)
                c.LoadFromRow(dr)
                cashiers.Add(c)
            Next

            Return cashiers

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns list of cashiers for given period and cashier ids generating new cashier records if none found for id
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <remarks></remarks>
    Public Function GetCashiers(ByVal periodId As Integer, ByVal cashierIds As ArrayList) As List(Of cCashBalCashier)

        Try
            Dim cashiers As New List(Of cCashBalCashier)

            If cashierIds.Count > 0 Then
                Oasys3DB.ClearAllParameters()
                Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
                Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
                Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
                Oasys3DB.SetWhereParameter(_CashierID.ColumnName, clsOasys3DB.eOperator.pIn, cashierIds)
                Oasys3DB.SetOrderByParameter(_CashierID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
                Dim dt As DataTable = Oasys3DB.Query.Tables(0)

                'add cashiers found to collection
                For Each dr As DataRow In dt.Rows
                    Dim c As New cCashBalCashier(Oasys3DB)
                    c.LoadFromRow(dr)
                    cashiers.Add(c)
                Next

                'get default currency id
                Dim defaultCurrencyId As String = String.Empty
                Using sysCurrency As New BOSystemCurrency.cSystemCurrency(Oasys3DB)
                    defaultCurrencyId = sysCurrency.GetDefaultCurrencyID
                End Using

                'check that a cashier record exists for each id and default currency else create and persist
                For Each cashierId As Integer In cashierIds
                    Dim id As Integer = cashierId
                    Dim cashier As cCashBalCashier = cashiers.Find(Function(f) f.CashierID.Value = id AndAlso f.CurrencyID.Value = defaultCurrencyId)
                    If cashier Is Nothing Then
                        cashier = New cCashBalCashier(Oasys3DB)
                        cashier.CashierID.Value = id
                        cashier.PeriodID.Value = periodId
                        cashier.CurrencyID.Value = defaultCurrencyId
                        cashier.SaveIfNew()
                        cashiers.Add(cashier)
                    End If
                Next
            End If

            Return cashiers

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierLoad, ex)
        End Try

    End Function



    Private Sub LoadTenderVariances()

        Dim tenVar As New cCashBalCashierTenVar(Oasys3DB)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.PeriodID, _PeriodID.Value)
        tenVar.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.CashierID, _CashierID.Value)
        tenVar.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.CurrencyID, _CurrencyID.Value)
        _tenderVariances = tenVar.LoadMatches

    End Sub

    Private Sub LoadTenderRelatedVariances()

        Dim tenVar As New cCashBalCashierTenVar(Oasys3DB)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.TradingPeriodID, _PeriodID.Value)
        tenVar.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.CashierID, _CashierID.Value)
        tenVar.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        tenVar.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, tenVar.CurrencyID, _CurrencyID.Value)
        _tenderRelatedVariances = tenVar.LoadMatches

    End Sub



    Public Sub DeleteTenders()

        Try
            Dim cashierTender As New cCashBalCashierTen(Oasys3DB)

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(cashierTender.TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(cashierTender.PeriodID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _PeriodID.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(cashierTender.CashierID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CashierID.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(cashierTender.CurrencyID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CurrencyID.Value)
            Oasys3DB.Delete()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.CashierTenderDelete, ex)
        End Try

    End Sub

    Public Function UpdatePickup() As Boolean

        Try
            'check that there are any tenders
            If TenderTypes.Count > 0 Then
                'Delete all ttypes from database first
                _TenderTypes(0).DeleteAllCurrencyTenders()

                'Insert tender from collection if non-zero values.
                For Each tender As cCashBalCashierTen In _TenderTypes
                    If (tender.Amount.Value <> 0) Or (tender.PickUp.Value <> 0) Then tender.SaveIfNew()
                Next
            End If

            'Update header record with todays exchange rate
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pUpdate)
            Oasys3DB.SetColumnAndValueParameter(_ExchangeRate.ColumnName, _ExchangeRate.Value)
            Oasys3DB.SetColumnAndValueParameter(_ExchangePower.ColumnName, _ExchangePower.Value)
            Oasys3DB.SetColumnAndValueParameter(_MiscIncomeValue20.ColumnName, _MiscIncomeValue20.Value)
            Oasys3DB.SetColumnAndValueParameter(_MisOutValue20.ColumnName, _MisOutValue20.Value)
            SetDBKeys()
            If Oasys3DB.Update > 0 Then Return True
            Return False

        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

    Public Function GetCashierPerformanceRecord(ByVal strCashierNum As String, ByVal intPeriodId As Integer) As List(Of cCashBalCashier)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, CashierID, CInt(strCashierNum))
            JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, PeriodID, intPeriodId)
            Return LoadMatches()

        Catch ex As Exception
            Throw ex
            Return Nothing

        End Try

    End Function

    ''' <summary>
    ''' Updates cashier with float issued amount
    ''' </summary>
    ''' <param name="Value"></param>
    ''' <remarks></remarks>
    Public Sub FloatUpdateIssued(ByVal Value As Decimal)

        Try
            _FloatIssued.Value += Value
            SaveIfExists()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Returns value of total floats is use across all periods
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFloatsInUse() As Decimal

        Try
            'get default currency
            Dim defaultCurrencyId As String = String.Empty
            Using sysCurrency As New BOSystemCurrency.cSystemCurrency(Oasys3DB)
                defaultCurrencyId = sysCurrency.GetDefaultCurrencyID
            End Using

            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggSum, _FloatIssued.ColumnName, _FloatIssued.ColumnName)
            Oasys3DB.SetColumnAggregate(clsOasys3DB.eAggregates.pAggSum, _FloatReturned.ColumnName, _FloatReturned.ColumnName)
            Oasys3DB.SetWhereParameter(_CurrencyID.ColumnName, clsOasys3DB.eOperator.pEquals, defaultCurrencyId)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)




            'deals with null floats

            'existing
            'Dim issued As Decimal = dt.Rows(0)(0)
            'Dim returned As Decimal = dt.Rows(0)(1)

            'refactored
            Return FloatIssuedMinusReturned(dt)

        Catch ex As Exception
            Throw New OasysDbException("Getting floats", ex)
        End Try

    End Function

#End Region

#Region "Private, Friend Procedures And Functions"

    Friend Function FloatIssuedMinusReturned(ByRef DT As DataTable) As Decimal
        Dim Float As IBankingFloat
        Dim issued As Decimal
        Dim returned As Decimal

        Float = (New BankingFloatFactory).GetImplementation

        issued = Float.FloatIssues(DT).Value
        returned = Float.FloatReturned(DT).Value

        Return issued - returned

    End Function

#End Region

End Class