﻿<Serializable()> Public Class cSafeBagsDenoms
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SafeBagsDenoms"
        BOFields.Add(_BagID)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_TenderID)
        BOFields.Add(_ID)
        BOFields.Add(_Value)
        BOFields.Add(_SlipNumber)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSafeBagsDenoms)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSafeBagsDenoms)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSafeBagsDenoms))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSafeBagsDenoms(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _BagID As New ColField(Of Integer)("BagID", 0, "Bag ID", True, False)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _TenderID As New ColField(Of Integer)("TenderID", 1, "Tender ID", True, False)
    Private _ID As New ColField(Of Decimal)("ID", 0.01D, "ID", True, False)
    Private _Value As New ColField(Of Decimal)("Value", 0, "Value", False, False)
    Private _SlipNumber As New ColField(Of String)("SlipNumber", "", "Slip Number", False, False)

#End Region

#Region "Fields Properties"

    Public Property BagID() As ColField(Of Integer)
        Get
            Return _BagID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _BagID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property TenderID() As ColField(Of Integer)
        Get
            Return _TenderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TenderID = value
        End Set
    End Property
    Public Property ID() As ColField(Of Decimal)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ID = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Return _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property SlipNumber() As ColField(Of String)
        Get
            Return _SlipNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SlipNumber = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns all denominations for given bag ID.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="bagID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDenominations(ByVal bagID As Integer) As List(Of cSafeBagsDenoms)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_BagID.ColumnName, clsOasys3DB.eOperator.pEquals, bagID)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim denoms As New List(Of cSafeBagsDenoms)
            For Each dr As DataRow In dt.Rows
                Dim denom As New cSafeBagsDenoms(Oasys3DB)
                denom.LoadFromRow(dr)
                denoms.Add(denom)
            Next

            Return denoms

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.BagDenomsLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Deletes all denominations for given bag ID.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="bagID"></param>
    ''' <remarks></remarks>
    Public Overloads Sub Delete(ByVal bagID As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(_BagID.ColumnName, clsOasys3DB.eOperator.pEquals, bagID)
            Oasys3DB.Delete()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.BagDenomsDelete, ex)
        End Try

    End Sub

#End Region

End Class
