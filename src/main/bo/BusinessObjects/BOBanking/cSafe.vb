﻿<Serializable()> Public Class cSafe
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "Safe"
        BOFields.Add(_PeriodID)
        BOFields.Add(_PeriodDate)
        BOFields.Add(_UserID1)
        BOFields.Add(_UserID2)
        BOFields.Add(_LastAmended)
        BOFields.Add(_IsClosed)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSafe)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSafe)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSafe))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSafe(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", True, False)
    Private _PeriodDate As New ColField(Of Date)("PeriodDate", Nothing, "Period Date", False, False)
    Private _UserID1 As New ColField(Of Integer)("UserID1", 0, "UserID 1", False, False)
    Private _UserID2 As New ColField(Of Integer)("UserID2", 0, "UserID 2", False, False)
    Private _LastAmended As New ColField(Of Date)("LastAmended", Now, "Last Amended", False, False)
    Private _IsClosed As New ColField(Of Boolean)("IsClosed", False, "Is Closed", False, False)

#End Region

#Region "Fields Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property PeriodDate() As ColField(Of Date)
        Get
            Return _PeriodDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _PeriodDate = value
        End Set
    End Property
    Public Property UserID1() As ColField(Of Integer)
        Get
            Return _UserID1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UserID1 = value
        End Set
    End Property
    Public Property UserID2() As ColField(Of Integer)
        Get
            Return _UserID2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _UserID2 = value
        End Set
    End Property
    Public Property LastAmended() As ColField(Of Date)
        Get
            Return _LastAmended
        End Get
        Set(ByVal value As ColField(Of Date))
            _LastAmended = value
        End Set
    End Property
    Public Property IsClosed() As ColField(Of Boolean)
        Get
            Return _IsClosed
        End Get
        Set(ByVal value As ColField(Of Boolean))
            _IsClosed = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Safes As List(Of cSafe) = Nothing
    Private _SysCurrency As BOSystemCurrency.cSystemCurrency = Nothing
    Private _Denoms As List(Of cSafeDenoms) = Nothing
    Private _Bags As List(Of cSafeBags) = Nothing

    Public Property Safes() As List(Of cSafe)
        Get
            If _Safes Is Nothing Then _Safes = New List(Of cSafe)
            Return _Safes
        End Get
        Set(ByVal value As List(Of cSafe))
            _Safes = value
        End Set
    End Property

    Public Property SysCurrency() As BOSystemCurrency.cSystemCurrency
        Get
            If _SysCurrency Is Nothing Then
                _SysCurrency = New BOSystemCurrency.cSystemCurrency(Oasys3DB)
                _SysCurrency.LoadDefaultCurrency(True)
            End If
            Return _SysCurrency
        End Get
        Set(ByVal value As BOSystemCurrency.cSystemCurrency)
            _SysCurrency = value
        End Set
    End Property

    Public Property Denoms() As List(Of cSafeDenoms)
        Get
            If _Denoms Is Nothing Then
                LoadDenominations()
            End If
            Return _Denoms
        End Get
        Set(ByVal value As List(Of cSafeDenoms))
            _Denoms = value
        End Set
    End Property
    Public Property Denom(ByVal sysDen As BOSystemCurrency.cSystemCurrencyDen) As cSafeDenoms
        Get
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = sysDen.CurrencyID.Value) AndAlso (den.ID.Value = sysDen.ID.Value) AndAlso (den.TenderID.Value = sysDen.TenderID.Value) Then
                    Return den
                End If
            Next

            Dim newDen As New cSafeDenoms(Oasys3DB)
            newDen.PeriodID.Value = _PeriodID.Value
            newDen.CurrencyID.Value = sysDen.CurrencyID.Value
            newDen.ID.Value = sysDen.ID.Value
            newDen.TenderID.Value = sysDen.TenderID.Value
            _Denoms.Add(newDen)
            Return newDen
        End Get
        Set(ByVal value As cSafeDenoms)
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = sysDen.CurrencyID.Value) AndAlso (den.ID.Value = sysDen.ID.Value) AndAlso (den.TenderID.Value = sysDen.TenderID.Value) Then
                    den = value
                End If
            Next
        End Set
    End Property
    Public Property Denom(ByVal bagDen As cSafeBagsDenoms) As cSafeDenoms
        Get
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = bagDen.CurrencyID.Value) AndAlso (den.ID.Value = bagDen.ID.Value) AndAlso (den.TenderID.Value = bagDen.TenderID.Value) Then
                    Return den
                End If
            Next

            Dim newDen As New cSafeDenoms(Oasys3DB)
            newDen.PeriodID.Value = _PeriodID.Value
            newDen.CurrencyID.Value = bagDen.CurrencyID.Value
            newDen.ID.Value = bagDen.ID.Value
            newDen.TenderID.Value = bagDen.TenderID.Value
            _Denoms.Add(newDen)
            Return newDen
        End Get
        Set(ByVal value As cSafeDenoms)
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = bagDen.CurrencyID.Value) AndAlso (den.ID.Value = bagDen.ID.Value) AndAlso (den.TenderID.Value = bagDen.TenderID.Value) Then
                    den = value
                End If
            Next
        End Set
    End Property
    Public Property Denom(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As cSafeDenoms
        Get
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = currencyId) AndAlso (den.ID.Value = denominationId) AndAlso (den.TenderID.Value = tenderId) Then
                    Return den
                End If
            Next

            Dim newDen As New cSafeDenoms(Oasys3DB)
            newDen.PeriodID.Value = _PeriodID.Value
            newDen.CurrencyID.Value = currencyId
            newDen.ID.Value = denominationId
            newDen.TenderID.Value = tenderId
            _Denoms.Add(newDen)
            Return newDen
        End Get
        Set(ByVal value As cSafeDenoms)
            For Each den As cSafeDenoms In Denoms
                If (den.CurrencyID.Value = currencyId) AndAlso (den.ID.Value = denominationId) AndAlso (den.TenderID.Value = tenderId) Then
                    den = value
                End If
            Next
        End Set
    End Property

    Public Property Bags() As List(Of cSafeBags)
        Get
            If _Bags Is Nothing Then
                LoadBags()
            End If
            Return _Bags
        End Get
        Set(ByVal value As List(Of cSafeBags))
            _Bags = value
        End Set
    End Property
    Public Property Bag(ByVal headerID As Integer) As cSafeBags
        Get
            For Each b As cSafeBags In Bags
                If b.ID.Value = headerID Then Return b
            Next
            Return Nothing
        End Get
        Set(ByVal value As cSafeBags)
            For Each b As cSafeBags In Bags
                If b.ID.Value = headerID Then
                    b = value
                End If
            Next
        End Set
    End Property

#End Region

#Region "Data Set"
    Private _dtSafe As New DataTable

    Public ReadOnly Property SafeTable() As DataTable
        Get
            Return _dtSafe
        End Get
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads safe for given period ID into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load(ByVal periodID As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If dt.Rows.Count > 0 Then
                Me.LoadFromRow(dt.Rows(0))
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads current safe record into this instance for given period ID or creates new safe record from previous days
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load(ByVal periodID As Integer, ByVal userID As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, periodID)
            Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Descending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Select Case True
                Case dt.Rows.Count > 0 AndAlso CInt(dt.Rows(0)(_PeriodID.ColumnName)) = periodID
                    'safe bag exists so load into instance and exit
                    LoadFromRow(dt.Rows(0))
                    Exit Sub

                Case dt.Rows.Count > 0
                    'check if a previous bag exists to get old denoms
                    Dim den As New cSafeDenoms(Oasys3DB)
                    Dim previousPeriodID As Integer = CInt(dt.Rows(0)(_PeriodID.ColumnName))
                    _Denoms = den.GetDenominations(previousPeriodID)

                Case Else
                    'there are no bags in the table so create default sysvalue from old banking tables
                    dt = Oasys3DB.ExecuteSql("SELECT BANK FROM CBSHDR").Tables(0)
                    Dim den As New cSafeDenoms(Oasys3DB)
                    den.PeriodID.Value = periodID
                    den.CurrencyID.Value = SysCurrency.ID.Value
                    den.SystemValue.Value = CDec(dt.Rows(0)(0))
                    _Denoms = New List(Of cSafeDenoms)
                    _Denoms.Add(den)
            End Select

            'set new safe bag values and save to database
            _PeriodID.Value = periodID
            _PeriodDate.Value = Now.Date
            _UserID1.Value = userID
            _UserID2.Value = userID
            _LastAmended.Value = Now

            SaveIfNew()
            UpdateDenominations()

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeLoad, ex)
        End Try

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha
    ' Date     : 020/06/2011
    ' Referral : 828
    ' Notes    : Load safe record
    '            Above version of the Load function does not create the safe correctly in certain scenerios
    '            This version remedies that
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub Load(ByVal PeriodID As Integer, ByVal UserID As Integer, ByVal PeriodDate As Date)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, PeriodID)
            Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Descending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Select Case True
                Case dt.Rows.Count > 0 AndAlso CInt(dt.Rows(0)(_PeriodID.ColumnName)) = PeriodID
                    'safe bag exists so load into instance and exit
                    LoadFromRow(dt.Rows(0))
                    Exit Sub

                Case dt.Rows.Count > 0
                    'check if a previous bag exists to get old denoms
                    Dim den As New cSafeDenoms(Oasys3DB)
                    Dim previousPeriodID As Integer = CInt(dt.Rows(0)(_PeriodID.ColumnName))
                    _Denoms = den.GetDenominations(previousPeriodID)

                Case Else
                    'there are no bags in the table so create default sysvalue from old banking tables
                    dt = Oasys3DB.ExecuteSql("SELECT BANK FROM CBSHDR").Tables(0)
                    Dim den As New cSafeDenoms(Oasys3DB)
                    den.PeriodID.Value = PeriodID
                    den.CurrencyID.Value = SysCurrency.ID.Value
                    den.SystemValue.Value = CDec(dt.Rows(0)(0))
                    _Denoms = New List(Of cSafeDenoms)
                    _Denoms.Add(den)
            End Select

            'set new safe bag values and save to database
            _PeriodID.Value = PeriodID
            '_PeriodDate.Value = Now.Date       this code is NOT correct
            _PeriodDate.Value = PeriodDate
            _UserID1.Value = UserID
            _UserID2.Value = UserID
            _LastAmended.Value = Now

            SaveIfNew()
            UpdateDenominations()

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads banking bags into Bags collection of this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadBags()

        Try
            Dim bankingBag As New cSafeBags(Oasys3DB)
            _Bags = bankingBag.GetBags(_PeriodID.Value)

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.BagsGet, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Loads all safe denominations into the Denoms collection.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadDenominations()

        Try
            Dim den As New cSafeDenoms(Oasys3DB)
            _Denoms = den.GetDenominations(_PeriodID.Value)

        Catch ex As OasysDbException
            Throw ex
        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeDenomsLoad, ex)
        End Try

    End Sub

    Public Function BagsUsingPickupPeriod() As List(Of cSafeBags)
        Dim bankingBag As New cSafeBags(Oasys3DB)
        Return bankingBag.GetBagsForPickupPeriod(_PeriodID.Value)
    End Function

    Public Function GetNonClosedSafeRecords() As DataTable

        'get current trading period
        Dim currentPeriod As Integer = 0
        Using sysPeriod As New BOSystem.cSystemPeriods(Oasys3DB)
            currentPeriod = sysPeriod.GetCurrentPeriodID()
        End Using

        'get todays record
        'get past closed periods from database
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, currentPeriod)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'get past closed periods from database
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_IsClosed.ColumnName, clsOasys3DB.eOperator.pEquals, False)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pLessThan, currentPeriod)
        Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Descending)
        Dim dt2 As DataTable = Oasys3DB.Query.Tables(0)

        dt.Merge(dt2, True)
        Return dt

    End Function


    ''' <summary>
    ''' Loads all non-closed safes into SafeTable for periods equal or less than given period ID
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Sub SafeLoadNonClosed(ByVal periodId As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_IsClosed.ColumnName, clsOasys3DB.eOperator.pEquals, False)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, periodId)
            Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Descending)

            _dtSafe = Oasys3DB.Query.Tables(0)
            _dtSafe.Columns.Add("Display", GetType(String))
            For Each dr As DataRow In _dtSafe.Rows
                dr("Display") = String.Format("{0} - {1}", dr(_PeriodID.ColumnName), Format(CType(dr(_PeriodDate.ColumnName), Date), "dd/MM/yyyy"))
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeLoadNonClosed, ex)
        End Try

    End Sub

    '''' <summary>
    '''' Returns count of bags for given type in Bags collection.
    ''''  Throws Oasys exception on error
    '''' </summary>
    '''' <param name="type"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Function BagsCount(ByVal type As String) As Integer

    '    Try
    '        Dim total As Decimal = 0
    '        For Each b As cSafeBags In Bags
    '            If b.Type.Value.Equals(type) Then total += 1
    '        Next

    '        Return total

    '    Catch ex As Exception
    '        Throw New OasysDbException(My.Resources.Errors.BagsCount, ex)
    '    End Try

    'End Function

    Public Function GetAllRecords() As List(Of cSafe)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Descending)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        Dim safes As New List(Of cSafe)
        For Each dr As DataRow In dt.Rows
            Dim s As New cSafe(Oasys3DB)
            s.LoadFromRow(dr)
            safes.Add(s)
        Next

        Return safes

    End Function



    ''' <summary>
    ''' Adds contents of given bag denominations back to the safe and persists
    ''' </summary>
    ''' <param name="bag"></param>
    ''' <remarks></remarks>
    Public Sub AddBackToSafe(ByVal bag As cSafeBags)

        For Each den As cSafeBagsDenoms In bag.Denoms
            Dim safeDen As cSafeDenoms = Denom(den)
            safeDen.SystemValue.Value += den.Value.Value
            safeDen.SafeValue.Value += den.Value.Value
        Next

        _LastAmended.Value = Now
        SaveIfExists()
        UpdateDenominations()

    End Sub

    ''' <summary>
    ''' Removes given bag values from safe and persists
    ''' </summary>
    ''' <param name="bag"></param>
    ''' <remarks></remarks>
    Public Sub RemoveFromSafe(ByVal bag As cSafeBags)

        For Each den As cSafeBagsDenoms In bag.Denoms
            Dim safeDen As cSafeDenoms = Denom(den)
            safeDen.SystemValue.Value -= den.Value.Value
            safeDen.SafeValue.Value -= den.Value.Value
        Next

        _LastAmended.Value = Now
        SaveIfExists()
        UpdateDenominations()

    End Sub





    ''' <summary>
    ''' Persists bag and all denominations to database.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update()

        Try
            Oasys3DB.BeginTransaction()

            'update bag
            _LastAmended.Value = Now
            SaveIfExists()

            UpdateDenominations()

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.BagUpdate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Persists bag and all denominations to database.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update(ByVal userID1 As Integer, ByVal userID2 As Integer)

        Try
            Oasys3DB.BeginTransaction()

            'update bag
            _UserID1.Value = userID1
            _UserID2.Value = userID2
            _LastAmended.Value = Now
            SaveIfExists()

            UpdateDenominations()

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.BagUpdate, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Persists denoms for this safe to database where there is a safe, change or system value.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UpdateDenominations()

        Try
            DeleteDenominations()

            'insert denominations where there is a safe, change or system value
            For Each den As cSafeDenoms In Denoms
                If den.SafeValue.Value = 0 AndAlso den.ChangeValue.Value = 0 AndAlso den.SystemValue.Value = 0 Then
                    Continue For
                End If

                den.PeriodID.Value = _PeriodID.Value
                den.SuggestedValue.Value = 0

                'update suggested value
                Dim sysDen As BOSystemCurrency.cSystemCurrencyDen = SysCurrency.Currency(den.CurrencyID.Value).Denom(den.ID.Value, den.TenderID.Value)
                Dim minimum As Decimal = den.SafeValue.Value + den.ChangeValue.Value - sysDen.SafeMinimum.Value
                Dim bullion As Decimal = sysDen.BullionMultiple.Value
                If bullion >= den.ID.Value AndAlso minimum >= bullion Then
                    Dim multiple As Integer = CInt(Math.Floor(minimum / bullion))
                    den.SuggestedValue.Value = bullion * multiple
                End If

                den.SaveIfNew()
            Next

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeDenomsSave, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Deletes all safe denominations from database.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DeleteDenominations()

        Try
            Dim den As New cSafeDenoms(Oasys3DB)
            den.Delete(_PeriodID.Value)

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeDenomsDelete, ex)
        End Try

    End Sub



    ''' <summary>
    ''' Closed this period.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClosePeriod()

        Try
            _IsClosed.Value = True
            _LastAmended.Value = Now
            SaveIfExists()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeClosingPeriod, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns string of any outstanding non closed periods. Returns empty string if none outstanding
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOutstandingPeriods() As String

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pLessThan, _PeriodID.Value)
            Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_IsClosed.ColumnName, clsOasys3DB.eOperator.pEquals, False)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim sb As New StringBuilder
            For Each dr As DataRow In dt.Rows
                sb.Append(My.Resources.CommsNotPrepareFor & CDate(dr(_PeriodDate.ColumnName)).ToShortDateString & Environment.NewLine)
            Next

            Return sb.ToString

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeGetOutstandingPeriods, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns list of closed periods
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetClosedPeriods() As ArrayList

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_IsClosed.ColumnName, clsOasys3DB.eOperator.pEquals, True)
            Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim list As New ArrayList
            For Each dr As DataRow In dt.Rows
                list.Add(dr(_PeriodID.ColumnName))
            Next

            Return list

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeGetClosedPeriods, ex)
        End Try

    End Function

    ''' <summary>
    ''' New Banking - Update Safe and set the SafeChecked field
    ''' </summary>
    ''' <param name="intBankingPeriodID"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub NewBankingSafeMaintenance(ByVal intBankingPeriodID As Integer)
        Try
            Oasys3DB.BeginTransaction()

            _LastAmended.Value = Now
            SaveIfExists()
            UpdateDenominations()

            'set SafeBags : SafeChecked to True for banking period safe NOT current safe
            Oasys3DB.NewBankingNonQueryExecuteSql("exec NewBankingSafeChecked @PeriodID = " & intBankingPeriodID.ToString)

            Oasys3DB.CommitTransaction()

        Catch ex As Exception
            If Oasys3DB.Transaction Then Oasys3DB.RollBackTransaction()
            Throw New OasysDbException(My.Resources.Errors.BagUpdate, ex)
        End Try
    End Sub

    Public Sub NewBankingSafeMaintenanceTransactionSafe(ByVal intBankingPeriodID As Integer)

        _LastAmended.Value = Now
        SaveIfExists()
        UpdateDenominations()

        'set SafeBags : SafeChecked to True for banking period safe NOT current safe
        Oasys3DB.NewBankingNonQueryExecuteSql("exec NewBankingSafeChecked @PeriodID = " & intBankingPeriodID.ToString)

    End Sub

#End Region

End Class
