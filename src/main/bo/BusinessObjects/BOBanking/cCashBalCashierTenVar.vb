﻿<Serializable()> Public Class cCashBalCashierTenVar
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CashBalCashierTenVar"
        BOFields.Add(_PeriodID)
        BOFields.Add(_TradingPeriodID)
        BOFields.Add(_CashierID)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_ID)
        BOFields.Add(_Quantity)
        BOFields.Add(_Amount)
        BOFields.Add(_PickUp)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCashBalCashierTenVar)

        LoadBORecords(count)

        Dim col As New List(Of cCashBalCashierTenVar)
        For Each record As cBaseClass In BORecords
            col.Add(CType(record, cCashBalCashierTenVar))
        Next

        Return col

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCashBalCashierTenVar(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", True, False)
    Private _TradingPeriodID As New ColField(Of Integer)("TradingPeriodID", 0, "Trading Period ID", True, False)
    Private _CashierID As New ColField(Of Integer)("CashierID", 0, "Cashier ID", True, False)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Quantity As New ColField(Of Integer)("Quantity", 0, "Quantity", False, False)
    Private _Amount As New ColField(Of Decimal)("Amount", 0, "Amount", False, False, 2)
    Private _PickUp As New ColField(Of Decimal)("PickUp", 0, "Pickup", False, False, 2)

#End Region

#Region "Field Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property TradingPeriodID() As ColField(Of Integer)
        Get
            Return _TradingPeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TradingPeriodID = value
        End Set
    End Property
    Public Property CashierID() As ColField(Of Integer)
        Get
            CashierID = _CashierID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _CashierID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Integer)
        Get
            Quantity = _Quantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Quantity = value
        End Set
    End Property
    Public Property Amount() As ColField(Of Decimal)
        Get
            Amount = _Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Amount = value
        End Set
    End Property
    Public Property PickUp() As ColField(Of Decimal)
        Get
            PickUp = _PickUp
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PickUp = value
        End Set
    End Property

#End Region

    Public Enum ColumnNames
        PeriodID
        TradingPeriodID
        CashierID
        CurrencyID
        ID
        Quantity
        Amount
        PickUp
    End Enum

    Public Function GetVariances(ByVal periodId As Integer) As List(Of cCashBalCashierTenVar)

        Dim variances As New List(Of cCashBalCashierTenVar)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_TradingPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
        Oasys3DB.SetOrderByParameter(_PeriodID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        For Each dr As DataRow In dt.Rows
            Dim newVar As New cCashBalCashierTenVar(Oasys3DB)
            newVar.LoadFromRow(dr)
            variances.Add(newVar)
        Next

        Return variances

    End Function

End Class
