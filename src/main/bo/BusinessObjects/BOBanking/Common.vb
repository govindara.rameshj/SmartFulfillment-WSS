﻿<HideModuleName()> Public Module Common

    ''' <summary>
    ''' Returns tender value of given tender for this collection
    ''' </summary>
    ''' <param name="Tenders"></param>
    ''' <param name="TenderID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Function Tender(ByRef Tenders As List(Of cCashBalCashierTen), ByVal CurrencyID As String, ByVal TenderID As Integer) As Decimal

        For Each t As cCashBalCashierTen In Tenders.Where(Function(c As cCashBalCashierTen) c.CurrencyID.Value = CurrencyID)
            If t.ID.Value = TenderID Then Return t.Amount.Value
        Next
        Return 0

    End Function
    <System.Runtime.CompilerServices.Extension()> Function Tender(ByRef Tenders As List(Of cCashBalTillTen), ByVal CurrencyID As String, ByVal TenderID As Integer) As Decimal

        For Each t As cCashBalTillTen In Tenders.Where(Function(c As cCashBalTillTen) c.CurrencyID.Value = CurrencyID)
            If t.ID.Value = TenderID Then Return t.Amount.Value
        Next
        Return 0

    End Function
    <System.Runtime.CompilerServices.Extension()> Function Tender(ByRef Cashiers As List(Of cCashBalCashier), ByVal CurrencyID As String, ByVal TenderID As Integer) As Decimal

        Dim total As Decimal = 0
        For Each c As cCashBalCashier In Cashiers
            total += c.TenderTypes.Tender(CurrencyID, TenderID)
        Next
        Return total

    End Function
    <System.Runtime.CompilerServices.Extension()> Function Tender(ByRef Tills As List(Of cCashBalTill), ByVal CurrencyID As String, ByVal TenderID As Integer) As Decimal

        Dim total As Decimal = 0
        For Each t As cCashBalTill In Tills
            total += t.TenderTypes.Tender(CurrencyID, TenderID)
        Next
        Return total

    End Function

    ''' <summary>
    ''' Returns cash taken less change for this collection
    ''' </summary>
    ''' <param name="Tenders"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Function TenderCash(ByRef Tenders As List(Of cCashBalCashierTen), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each t As cCashBalCashierTen In Tenders.Where(Function(c As cCashBalCashierTen) c.CurrencyID.Value = CurrencyID)
            If t.ID.Value = 1 Then total += t.Amount.Value
            If t.ID.Value = 99 Then total += t.Amount.Value
        Next
        Return total

    End Function
    <System.Runtime.CompilerServices.Extension()> Function TenderCash(ByRef Tenders As List(Of cCashBalTillTen), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each t As cCashBalTillTen In Tenders.Where(Function(c As cCashBalTillTen) c.CurrencyID.Value = CurrencyID)
            If t.ID.Value = 1 Then total += t.Amount.Value
            If t.ID.Value = 99 Then total += t.Amount.Value
        Next
        Return total

    End Function
    <System.Runtime.CompilerServices.Extension()> Function TenderCash(ByRef Cashiers As List(Of cCashBalCashier), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each c As cCashBalCashier In Cashiers
            total += c.TenderTypes.TenderCash(CurrencyID)
        Next
        Return total

    End Function
    <System.Runtime.CompilerServices.Extension()> Function TenderCash(ByRef Tills As List(Of cCashBalTill), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each t As cCashBalTill In Tills
            total += t.TenderTypes.TenderCash(CurrencyID)
        Next
        Return total

    End Function


    <System.Runtime.CompilerServices.Extension()> Function FloatInUse(ByRef Cashiers As List(Of cCashBalCashier), ByVal CurrencyID As String) As Decimal

        For Each cashier As cCashBalCashier In Cashiers
            If cashier.CurrencyID.Value = CurrencyID Then
                Return cashier.FloatIssued.Value - cashier.FloatReturned.Value
            End If
        Next
        Return 0

    End Function
    <System.Runtime.CompilerServices.Extension()> Function FloatInUse(ByRef Tills As List(Of cCashBalTill), ByVal CurrencyID As String) As Decimal

        For Each till As cCashBalTill In Tills
            If till.CurrencyID.Value = CurrencyID Then
                Return till.FloatIssued.Value - till.FloatReturned.Value
            End If
        Next
        Return 0

    End Function
    <System.Runtime.CompilerServices.Extension()> Function FloatIssued(ByRef Cashiers As List(Of cCashBalCashier), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each cashier As cCashBalCashier In Cashiers
            If cashier.CurrencyID.Value = CurrencyID Then
                total += cashier.FloatIssued.Value
            End If
        Next
        Return total

    End Function
    <System.Runtime.CompilerServices.Extension()> Function FloatIssued(ByRef Tills As List(Of cCashBalTill), ByVal CurrencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each till As cCashBalTill In Tills
            If till.CurrencyID.Value = CurrencyID Then
                total += till.FloatIssued.Value
            End If
        Next
        Return total

    End Function


    <System.Runtime.CompilerServices.Extension()> Public Function MiscOuts(ByVal Cashiers As List(Of cCashBalCashier)) As Decimal

        Try
            Dim total As Decimal = 0

            For Each cashier As BOBanking.cCashBalCashier In Cashiers
                For index As Integer = 1 To 20
                    total += cashier.MisOutValue(index).Value
                Next
            Next

            Return total

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    <System.Runtime.CompilerServices.Extension()> Public Function MiscOuts(ByVal Tills As List(Of cCashBalTill)) As Decimal

        Try
            Dim total As Decimal = 0

            For Each till As BOBanking.cCashBalTill In Tills
                For index As Integer = 1 To 20
                    total += till.MisOutValue(index).Value
                Next
            Next

            Return total

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    <System.Runtime.CompilerServices.Extension()> Public Function MiscIns(ByVal Cashiers As List(Of cCashBalCashier)) As Decimal

        Try
            Dim total As Decimal = 0

            For Each cashier As BOBanking.cCashBalCashier In Cashiers
                For index As Integer = 1 To 20
                    total += cashier.MiscIncomeValue(index).Value
                Next
            Next

            Return total

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    <System.Runtime.CompilerServices.Extension()> Public Function MiscIns(ByVal Tills As List(Of cCashBalTill)) As Decimal

        Try
            Dim total As Decimal = 0

            For Each till As BOBanking.cCashBalTill In Tills
                For index As Integer = 1 To 20
                    total += till.MiscIncomeValue(index).Value
                Next
            Next

            Return total

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    <System.Runtime.CompilerServices.Extension()> Sub Update(ByRef Tenders As List(Of cCashBalCashierTen))

        Try
            'check that there are any tenders to save
            If Tenders.Count = 0 Then Exit Sub

            'delete all tender types from table first
            Tenders(0).DeleteAllCurrencyTenders()

            'loop through tenders and insert if any values
            For Each tender As cCashBalCashierTen In Tenders
                If (tender.Amount.Value <> 0) Or (tender.PickUp.Value <> 0) Then tender.SaveIfNew()
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    <System.Runtime.CompilerServices.Extension()> Sub Update(ByRef Tenders As List(Of cCashBalTillTen))

        Try
            'check that there are any tenders to save
            If Tenders.Count = 0 Then Exit Sub

            'delete all tender types from table first
            Tenders(0).DeleteAllCurrencyTenders()

            'loop through tenders and insert if any values
            For Each tender As cCashBalTillTen In Tenders
                If (tender.Amount.Value <> 0) Or (tender.PickUp.Value <> 0) Then tender.SaveIfNew()
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    '''' <summary>
    '''' Returns cashtill tender given values. If not found then one will be created and added to collection
    '''' </summary>
    '''' <param name="cashTills"></param>
    '''' <param name="periodId"></param>
    '''' <param name="currencyId"></param>
    '''' <param name="accountId"></param>
    '''' <param name="tenderId"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    '<System.Runtime.CompilerServices.Extension()> Function GetTender(ByRef cashTills As List(Of CashierTill), ByVal periodId As Integer, ByVal currencyId As String, ByVal accountId As Integer, ByVal tenderId As Decimal) As CashierTillTender

    '    'get cash till record for this currency or create one if none exist
    '    Dim cashTill As CashierTill = cashTills.Find(Function(f As CashierTill) f.CurrencyId = currencyId)
    '    If cashTill Is Nothing Then
    '        cashTill = New CashierTill(periodId, currencyId, accountId)
    '        cashTills.Add(cashTill)
    '    End If

    '    'get cash till tender record for this denom else create one if none exist
    '    Dim cashTillTender As CashierTillTender = cashTill.Tenders.Find(Function(f As CashierTillTender) f.TenderId = tenderId)
    '    If cashTillTender Is Nothing Then
    '        cashTillTender = New CashierTillTender(tenderId)
    '        cashTill.Tenders.Add(cashTillTender)
    '    End If

    '    Return cashTillTender

    'End Function

    'Public Function BagsAreIdentical(ByVal bag1 As cSafeBags, ByVal bag2 As cSafeBags) As Boolean

    '    'check bag2 against bag1
    '    For Each denom1 As cSafeBagsDenoms In bag1.Denoms
    '        If denom1.Value.Value = 0 Then Continue For
    '        Dim id As Decimal = denom1.ID.Value
    '        Dim denom2 As cSafeBagsDenoms = bag2.Denoms.Find(Function(f) f.ID.Value = id)
    '        If denom2 Is Nothing Then
    '            Return False
    '        End If

    '        If denom1.Value.Value <> denom2.Value.Value Then
    '            Return False
    '        End If
    '    Next

    '    'check bag1 against bag2
    '    For Each denom2 As cSafeBagsDenoms In bag2.Denoms
    '        If denom2.Value.Value = 0 Then Continue For
    '        Dim id As Decimal = denom2.ID.Value
    '        Dim denom1 As cSafeBagsDenoms = bag1.Denoms.Find(Function(f) f.ID.Value = id)
    '        If denom1 Is Nothing Then
    '            Return False
    '        End If

    '        If denom2.Value.Value <> denom1.Value.Value Then
    '            Return False
    '        End If
    '    Next

    '    'if here then bags are identical
    '    Return True

    'End Function

End Module

Public Structure BagTypes
    Private _value As String
    Public Shared Float As String = "F"
    Public Shared Pickup As String = "P"
    Public Shared Banking As String = "B"
    Public Shared [Property] As String = "L"
    Public Shared GiftToken As String = "G"
End Structure

Public Structure BagStates
    Private _value As String
    Public Shared Released As String = "R"
    Public Shared Cancelled As String = "C"
    Public Shared Sealed As String = "S"
    Public Shared BackToSafe As String = "B"
    Public Shared PreparedNotReleased As String = "P"
    Public Shared ReleasedNotPrepared As String = "Q"

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Partha Dutta
    ' Date        : 04/10/2010
    ' Referral No : 387 & 430
    ' Notes       : Allow banking bags to be outputted to STHOA file
    '               Bank accounts 8211, 8212, 8213, 8214
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Shared ManagerChecked As String = "M"

End Structure

Public Structure AccountabilityTypes
    Private _value As String
    Public Shared Cashier As String = "C"
    Public Shared Till As String = "T"
End Structure