﻿<Serializable()> Public Class cCashBalTillTen
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "CashBalTillTen"
        BOFields.Add(_PeriodID)
        BOFields.Add(_TillID)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_ID)
        BOFields.Add(_Quantity)
        BOFields.Add(_Amount)
        BOFields.Add(_PickUp)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cCashBalTillTen)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cCashBalTillTen)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cCashBalTillTen))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cCashBalTillTen(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", True, False)
    Private _TillID As New ColField(Of Integer)("TillID", 0, "Till ID", True, False)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, False)
    Private _Quantity As New ColField(Of Integer)("Quantity", 0, "Count", False, False)
    Private _Amount As New ColField(Of Decimal)("Amount", 0, "Value", False, False, 2)
    Private _PickUp As New ColField(Of Decimal)("PickUp", 0, "Pickup", False, False, 2)

#End Region

#Region "Field Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property TillID() As ColField(Of Integer)
        Get
            TillID = _TillID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TillID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property Quantity() As ColField(Of Integer)
        Get
            Quantity = _Quantity
        End Get
        Set(ByVal value As ColField(Of Integer))
            _Quantity = value
        End Set
    End Property
    Public Property Amount() As ColField(Of Decimal)
        Get
            Amount = _Amount
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Amount = value
        End Set
    End Property
    Public Property PickUp() As ColField(Of Decimal)
        Get
            PickUp = _PickUp
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _PickUp = value
        End Set
    End Property

#End Region

#Region "Enums"

    Public Enum ColumnNames
        PeriodID
        TillID
        CurrencyID
        ID
        Quantity
        Amount
        PickUp
    End Enum

#End Region

#Region "Methods"

    Public Sub DeleteAllCurrencyTenders()

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _PeriodID.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_TillID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _TillID.Value)
            Oasys3DB.SetWhereJoinParameter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Oasys3DB.SetWhereParameter(_CurrencyID.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, _CurrencyID.Value)
            Oasys3DB.Delete()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region


End Class
