﻿<Serializable()> Public Class cSafeBags
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SafeBags"
        BOFields.Add(_ID)
        BOFields.Add(_SealNumber)
        BOFields.Add(_InPeriodID)
        BOFields.Add(_InDate)
        BOFields.Add(_InUserID1)
        BOFields.Add(_InUserID2)
        BOFields.Add(_OutPeriodID)
        BOFields.Add(_OutDate)
        BOFields.Add(_OutUserID1)
        BOFields.Add(_OutUserID2)
        BOFields.Add(_AccountabilityType)
        BOFields.Add(_AccountabilityID)
        BOFields.Add(_Type)
        BOFields.Add(_State)
        BOFields.Add(_Value)
        BOFields.Add(_FloatValue)
        BOFields.Add(_RelatedBagId)
        BOFields.Add(_PickupPeriodID)
        BOFields.Add(_Comments)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSafeBags)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSafeBags)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSafeBags))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSafeBags(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _SealNumber As New ColField(Of String)("SealNumber", "", "Seal Number", False, False)
    Private _Type As New ColField(Of String)("Type", "", "Bag Type", False, False)
    Private _State As New ColField(Of String)("State", "", "Bag State", False, False)
    Private _Value As New ColField(Of Decimal)("Value", 0, "Bag Value", False, False, 2)
    Private _InPeriodID As New ColField(Of Integer)("InPeriodID", 0, "In Period ID", False, False)
    Private _InDate As New ColField(Of Date)("InDate", Nothing, "In Date", False, False)
    Private _InUserID1 As New ColField(Of Integer)("InUserID1", 0, "In UserID 1", False, False)
    Private _InUserID2 As New ColField(Of Integer)("InUserID2", 0, "In UserID 2", False, False)
    Private _OutPeriodID As New ColField(Of Integer)("OutPeriodID", Nothing, "Out Period ID", False, False)
    Private _OutDate As New ColField(Of Date)("OutDate", Nothing, "Out Date", False, False)
    Private _OutUserID1 As New ColField(Of Integer)("OutUserID1", Nothing, "Out UserID 1", False, False)
    Private _OutUserID2 As New ColField(Of Integer)("OutUserID2", Nothing, "Out UserID 2", False, False)
    Private _AccountabilityType As New ColField(Of String)("AccountabilityType", "", "Accountability", False, False)
    Private _AccountabilityID As New ColField(Of Integer)("AccountabilityID", Nothing, "Accountability ID", False, False)
    Private _FloatValue As New ColField(Of Decimal)("FloatValue", 0, "Float Value", False, False)
    Private _PickupPeriodID As New ColField(Of Integer)("PickupPeriodID", 0, "Pickup Period ID", False, False)
    Private _RelatedBagId As New ColField(Of Integer)("RelatedBagId", 0, "Related Bag ID", False, False)
    Private _Comments As New ColField(Of String)("Comments", "", "Comments", False, False)

#End Region

#Region "Fields Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property SealNumber() As ColField(Of String)
        Get
            SealNumber = _SealNumber
        End Get
        Set(ByVal value As ColField(Of String))
            _SealNumber = value
        End Set
    End Property
    Public Property InPeriodID() As ColField(Of Integer)
        Get
            Return _InPeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _InPeriodID = value
        End Set
    End Property
    Public Property InDate() As ColField(Of Date)
        Get
            Return _InDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _InDate = value
        End Set
    End Property
    Public Property InUserID1() As ColField(Of Integer)
        Get
            InUserID1 = _InUserID1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _InUserID1 = value
        End Set
    End Property
    Public Property InUserID2() As ColField(Of Integer)
        Get
            InUserID2 = _InUserID2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _InUserID2 = value
        End Set
    End Property
    Public Property OutPeriodID() As ColField(Of Integer)
        Get
            Return _OutPeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OutPeriodID = value
        End Set
    End Property
    Public Property OutDate() As ColField(Of Date)
        Get
            Return _OutDate
        End Get
        Set(ByVal value As ColField(Of Date))
            _OutDate = value
        End Set
    End Property
    Public Property OutUserID1() As ColField(Of Integer)
        Get
            Return _OutUserID1
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OutUserID1 = value
        End Set
    End Property
    Public Property OutUserID2() As ColField(Of Integer)
        Get
            Return _OutUserID2
        End Get
        Set(ByVal value As ColField(Of Integer))
            _OutUserID2 = value
        End Set
    End Property
    Public Property AccountabilityType() As ColField(Of String)
        Get
            Return _AccountabilityType
        End Get
        Set(ByVal value As ColField(Of String))
            _AccountabilityType = value
        End Set
    End Property
    Public Property AccountabilityID() As ColField(Of Integer)
        Get
            Return _AccountabilityID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _AccountabilityID = value
        End Set
    End Property
    Public Property Type() As ColField(Of String)
        Get
            Return _Type
        End Get
        Set(ByVal value As ColField(Of String))
            _Type = value
        End Set
    End Property
    Public Property State() As ColField(Of String)
        Get
            Return _State
        End Get
        Set(ByVal value As ColField(Of String))
            _State = value
        End Set
    End Property
    Public Property Value() As ColField(Of Decimal)
        Get
            Return _Value
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _Value = value
        End Set
    End Property
    Public Property FloatValue() As ColField(Of Decimal)
        Get
            Return _FloatValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _FloatValue = value
        End Set
    End Property
    Public Property RelatedBagId() As ColField(Of Integer)
        Get
            Return _RelatedBagId
        End Get
        Set(ByVal value As ColField(Of Integer))
            _RelatedBagId = value
        End Set
    End Property
    Public Property PickupPeriodID() As ColField(Of Integer)
        Get
            Return _PickupPeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PickupPeriodID = value
        End Set
    End Property
    Public Property Comments() As ColField(Of String)
        Get
            Comments = _Comments
        End Get
        Set(ByVal value As ColField(Of String))
            _Comments = value
        End Set
    End Property

#End Region

#Region "Entities"
    Private _Denoms As List(Of cSafeBagsDenoms) = Nothing
    Private _RelatedBag As cSafeBags = Nothing

    Public Property Denoms() As List(Of cSafeBagsDenoms)
        Get
            If _Denoms Is Nothing Then
                Dim den As New cSafeBagsDenoms(Oasys3DB)
                _Denoms = den.GetDenominations(_ID.Value)
            End If
            Return _Denoms
        End Get
        Set(ByVal value As List(Of cSafeBagsDenoms))
            _Denoms = value
        End Set
    End Property
    Public Property Denom(ByVal CurrencyID As String, ByVal DenID As Decimal, ByVal TenderID As Integer) As cSafeBagsDenoms
        Get
            For Each den As cSafeBagsDenoms In Denoms
                If (den.CurrencyID.Value = CurrencyID) And (den.ID.Value = DenID) And (den.TenderID.Value = TenderID) Then Return den
            Next

            Dim newDen As New cSafeBagsDenoms(Oasys3DB)
            newDen.BagID.Value = _ID.Value
            newDen.CurrencyID.Value = CurrencyID
            newDen.ID.Value = DenID
            newDen.TenderID.Value = TenderID

            _Denoms.Add(newDen)
            Return newDen
        End Get
        Set(ByVal value As cSafeBagsDenoms)
            If _Denoms IsNot Nothing Then
                For Each den As cSafeBagsDenoms In _Denoms
                    If (den.CurrencyID.Value = CurrencyID) And (den.ID.Value = DenID) And (den.TenderID.Value = TenderID) Then
                        den = value
                    End If
                Next
            End If
        End Set
    End Property
    Public Property Denom(ByVal sysDen As BOSystemCurrency.cSystemCurrencyDen) As cSafeBagsDenoms
        Get
            For Each den As cSafeBagsDenoms In Denoms
                If (den.CurrencyID.Value = sysDen.CurrencyID.Value) And (den.ID.Value = sysDen.ID.Value) And (den.TenderID.Value = sysDen.TenderID.Value) Then Return den
            Next

            Dim newDen As New cSafeBagsDenoms(Oasys3DB)
            newDen.BagID.Value = _ID.Value
            newDen.CurrencyID.Value = sysDen.CurrencyID.Value
            newDen.ID.Value = sysDen.ID.Value
            newDen.TenderID.Value = sysDen.TenderID.Value

            _Denoms.Add(newDen)
            Return newDen
        End Get
        Set(ByVal value As cSafeBagsDenoms)
            If _Denoms IsNot Nothing Then
                For Each den As cSafeBagsDenoms In _Denoms
                    If (den.CurrencyID.Value = sysDen.CurrencyID.Value) And (den.ID.Value = sysDen.ID.Value) And (den.TenderID.Value = sysDen.TenderID.Value) Then
                        den = value
                    End If
                Next
            End If
        End Set
    End Property
    'Public Property Denom(ByVal bagDen As cCashBagHeaderDen) As cCashBagHeaderDen
    '    Get
    '        For Each den As cCashBagHeaderDen In Denoms
    '            If (den.CurrencyID.Value = bagDen.CurrencyID.Value) And (den.ID.Value = bagDen.ID.Value) And (den.TenderID.Value = bagDen.TenderID.Value) Then Return den
    '        Next

    '        Dim newDen As New cCashBagHeaderDen(Oasys3DB)
    '        newDen.CashBagHeaderID.Value = _ID.Value
    '        newDen.CurrencyID.Value = bagDen.CurrencyID.Value
    '        newDen.ID.Value = bagDen.ID.Value
    '        newDen.TenderID.Value = bagDen.TenderID.Value

    '        _Denoms.Add(newDen)
    '        Return newDen
    '    End Get
    '    Set(ByVal value As cCashBagHeaderDen)
    '        If _Denoms IsNot Nothing Then
    '            For Each den As cCashBagHeaderDen In _Denoms
    '                If (den.CurrencyID.Value = bagDen.CurrencyID.Value) And (den.ID.Value = bagDen.ID.Value) And (den.TenderID.Value = bagDen.TenderID.Value) Then
    '                    den = value
    '                End If
    '            Next
    '        End If
    '    End Set
    'End Property
    Public ReadOnly Property RelatedBag() As cSafeBags
        Get
            If _RelatedBagId.Value > 0 Then LoadRelatedBag()
            Return _RelatedBag
        End Get
    End Property

#End Region

#Region "Methods"

    Private Sub LoadRelatedBag()

        _RelatedBag = New cSafeBags(Oasys3DB)
        _RelatedBag.AddLoadFilter(clsOasys3DB.eOperator.pEquals, _RelatedBag.ID, _RelatedBagId.Value)
        _RelatedBag.LoadMatches()

    End Sub


    Public Sub Load(ByVal id As Integer)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_ID.ColumnName, clsOasys3DB.eOperator.pEquals, id)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Me.LoadFromRow(dt.Rows(0))
        End If

    End Sub

    ''' <summary>
    ''' Returns all banking bags for given period ID
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPickupBags(ByVal periodId As Integer) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_PickupPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, BagTypes.Pickup)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function

    ''' <summary>
    ''' Returns all banking bags for given period ID
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBags(ByVal periodID As Integer) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_InPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
        Dim dt1 As DataTable = Oasys3DB.Query.Tables(0)

        Oasys3DB.ClearWhereParameters()
        Oasys3DB.ClearWhereJoinParameters()
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_InPeriodID.ColumnName, clsOasys3DB.eOperator.pLessThan, periodID)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_OutPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, 0)
        Dim dt2 As DataTable = Oasys3DB.Query.Tables(0)

        Oasys3DB.ClearWhereParameters()
        Oasys3DB.ClearWhereJoinParameters()
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_InPeriodID.ColumnName, clsOasys3DB.eOperator.pLessThan, periodID)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_OutPeriodID.ColumnName, clsOasys3DB.eOperator.pGreaterThanOrEquals, periodID)
        Dim dt3 As DataTable = Oasys3DB.Query.Tables(0)

        Dim bags As New List(Of cSafeBags)

        For Each dr As DataRow In dt1.Rows
            'check that bag is not already in list
            Dim bagID As Integer = CInt(dr(_ID.ColumnName))
            If Not bags.Exists(Function(b As cSafeBags) b.ID.Value = bagID) Then
                Dim bag As New cSafeBags(Oasys3DB)
                bag.LoadFromRow(dr)
                bags.Add(bag)
            End If
        Next

        For Each dr As DataRow In dt2.Rows
            'check that bag is not already in list
            Dim bagID As Integer = CInt(dr(_ID.ColumnName))
            If Not bags.Exists(Function(b As cSafeBags) b.ID.Value = bagID) Then
                Dim bag As New cSafeBags(Oasys3DB)
                bag.LoadFromRow(dr)
                bags.Add(bag)
            End If
        Next

        For Each dr As DataRow In dt3.Rows
            'check that bag is not already in list
            Dim bagID As Integer = CInt(dr(_ID.ColumnName))
            If Not bags.Exists(Function(b As cSafeBags) b.ID.Value = bagID) Then
                Dim bag As New cSafeBags(Oasys3DB)
                bag.LoadFromRow(dr)
                bags.Add(bag)
            End If
        Next

        Return bags

    End Function

    Public Function GetBagsForPickupPeriod(ByVal periodID As Integer) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_PickupPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
        Dim dt1 As DataTable = Oasys3DB.Query.Tables(0)

        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt1.Rows
            'check that bag is not already in list
            Dim bagID As Integer = CInt(dr(_ID.ColumnName))
            If Not bags.Exists(Function(b As cSafeBags) b.ID.Value = bagID) Then
                Dim bag As New cSafeBags(Oasys3DB)
                bag.LoadFromRow(dr)
                bags.Add(bag)
            End If
        Next

        Return bags

    End Function

    ''' <summary>
    ''' Returns collection of bags created in given period for given type
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <param name="type">Use BagTypes structure for required string value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBagsForPeriod(ByVal periodID As Integer, ByVal type As String) As List(Of cSafeBags)

        'get bags
        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_PickupPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, "C")
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, type)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'assign rows returned to bags and return
        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function

    ''' <summary>
    ''' Returns collection of bags created in given period for given type.
    '''  Throws Oasys 
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <param name="accountId"></param>
    ''' <param name="type">Use BagTypes structure for required string value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBagsForPeriod(ByVal periodID As Integer, ByVal accountId As Integer, ByVal type As String) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_PickupPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_AccountabilityID.ColumnName, clsOasys3DB.eOperator.pEquals, accountId)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pNotEquals, BagStates.Cancelled)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, type)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'assign rows returned to bags and return
        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function


    ''' <summary>
    ''' Returns collection of bags released in given period for given type
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <param name="type">Use BagTypes structure for required string value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBagsReleasedForPeriod(ByVal periodId As Integer, ByVal type As String) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_OutPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pEquals, BagStates.Released)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, type)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'assign rows returned to bags and return
        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function

    ''' <summary>
    ''' Returns collection of banking bags released and not commed in given period
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankingBagsForPeriod(ByVal periodId As Integer) As List(Of cSafeBags)

        Dim al As New ArrayList
        al.Add(BagStates.ReleasedNotPrepared)
        al.Add(BagStates.Sealed)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 04/10/2010
        ' Referral No : 387 & 430
        ' Notes       : Allow banking bags to be outputted to STHOA file
        '               Bank accounts 8211, 8212, 8213, 8214
        '               SafeBags -> State is ("Q", "S", "M")
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        al.Add(BagStates.ManagerChecked) 'State = "M"

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_PickupPeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodId)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pIn, al)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, BagTypes.Banking)

        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'assign rows returned to bags and return
        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function

    ''' <summary>
    ''' Returns collection of sealed bags for given period and type
    ''' </summary>
    ''' <param name="periodId"></param>
    ''' <param name="type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBagsSealedForPeriod(ByVal periodId As Integer, ByVal type As String) As List(Of cSafeBags)

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetWhereParameter(_InPeriodID.ColumnName, clsOasys3DB.eOperator.pLessThanOrEquals, periodId)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_State.ColumnName, clsOasys3DB.eOperator.pEquals, BagStates.Sealed)
        Oasys3DB.SetWhereJoinParameter(clsOasys3DB.eOperator.pAnd)
        Oasys3DB.SetWhereParameter(_Type.ColumnName, clsOasys3DB.eOperator.pEquals, type)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        'assign rows returned to bags and return
        Dim bags As New List(Of cSafeBags)
        For Each dr As DataRow In dt.Rows
            Dim bag As New cSafeBags(Oasys3DB)
            bag.LoadFromRow(dr)
            bags.Add(bag)
        Next

        Return bags

    End Function


    ''' <summary>
    ''' Returns whether seal number has been used before
    ''' </summary>
    ''' <param name="number"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsSealNumberValid(ByVal number As String) As Boolean

        Oasys3DB.ClearAllParameters()
        Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
        Oasys3DB.SetColumnParameter(_SealNumber.ColumnName)
        Oasys3DB.SetWhereParameter(_SealNumber.ColumnName, clsOasys3DB.eOperator.pEquals, number)
        Dim dt As DataTable = Oasys3DB.Query.Tables(0)

        If dt.Rows.Count = 0 Then Return True
        Return False

    End Function

    ''' <summary>
    ''' Saves current bag as cancelled and resets 'in' values with 'out' values
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CancelAndReset(ByVal comments As String)

        'save bag as cancelled
        _Comments.Value = comments
        _State.Value = BagStates.Cancelled
        SaveIfExists()

        'reset bag values
        _ID.Value = 0
        _InDate.Value = _OutDate.Value
        _InPeriodID.Value = _OutPeriodID.Value
        _InUserID1.Value = _OutUserID1.Value
        _InUserID2.Value = _OutUserID2.Value

    End Sub

    ''' <summary>
    ''' Persists bag and all denominations to database
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update()

        'check that there are any denominations and get value
        _Value.Value = 0
        For Each den As cSafeBagsDenoms In Denoms
            _Value.Value += den.Value.Value
        Next

        'inser or update bag
        If _ID.Value = 0 Then
            SaveIfNew()
        Else
            SaveIfExists()
        End If

        'delete any existing denominations for this header
        Dim headerDen As New cSafeBagsDenoms(Oasys3DB)
        headerDen.Delete(_ID.Value)

        'insert denominations
        For Each den As cSafeBagsDenoms In _Denoms
            If den.Value.Value <> 0 Then
                den.BagID.Value = _ID.Value
                den.SaveIfNew()
            End If
        Next

    End Sub


#End Region

End Class

