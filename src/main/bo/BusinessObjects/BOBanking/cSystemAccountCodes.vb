﻿<Serializable()> Public Class cSystemAccountCodes
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SystemAccountCodes"
        BOFields.Add(_ID)
        BOFields.Add(_MiscInAccount01)
        BOFields.Add(_MiscInAccount02)
        BOFields.Add(_MiscInAccount03)
        BOFields.Add(_MiscInAccount04)
        BOFields.Add(_MiscInAccount05)
        BOFields.Add(_MiscInAccount06)
        BOFields.Add(_MiscInAccount07)
        BOFields.Add(_MiscInAccount08)
        BOFields.Add(_MiscInAccount09)
        BOFields.Add(_MiscInAccount10)
        BOFields.Add(_MiscInAccount11)
        BOFields.Add(_MiscInAccount12)
        BOFields.Add(_MiscInAccount13)
        BOFields.Add(_MiscInAccount14)
        BOFields.Add(_MiscInAccount15)
        BOFields.Add(_MiscInAccount16)
        BOFields.Add(_MiscInAccount17)
        BOFields.Add(_MiscInAccount18)
        BOFields.Add(_MiscInAccount19)
        BOFields.Add(_MiscInAccount20)
        BOFields.Add(_MiscInAccountCode01)
        BOFields.Add(_MiscInAccountCode02)
        BOFields.Add(_MiscInAccountCode03)
        BOFields.Add(_MiscInAccountCode04)
        BOFields.Add(_MiscInAccountCode05)
        BOFields.Add(_MiscInAccountCode06)
        BOFields.Add(_MiscInAccountCode07)
        BOFields.Add(_MiscInAccountCode08)
        BOFields.Add(_MiscInAccountCode09)
        BOFields.Add(_MiscInAccountCode10)
        BOFields.Add(_MiscInAccountCode11)
        BOFields.Add(_MiscInAccountCode12)
        BOFields.Add(_MiscInAccountCode13)
        BOFields.Add(_MiscInAccountCode14)
        BOFields.Add(_MiscInAccountCode15)
        BOFields.Add(_MiscInAccountCode16)
        BOFields.Add(_MiscInAccountCode17)
        BOFields.Add(_MiscInAccountCode18)
        BOFields.Add(_MiscInAccountCode19)
        BOFields.Add(_MiscInAccountCode20)
        BOFields.Add(_MiscOutAccount01)
        BOFields.Add(_MiscOutAccount02)
        BOFields.Add(_MiscOutAccount03)
        BOFields.Add(_MiscOutAccount04)
        BOFields.Add(_MiscOutAccount05)
        BOFields.Add(_MiscOutAccount06)
        BOFields.Add(_MiscOutAccount07)
        BOFields.Add(_MiscOutAccount08)
        BOFields.Add(_MiscOutAccount09)
        BOFields.Add(_MiscOutAccount10)
        BOFields.Add(_MiscOutAccount11)
        BOFields.Add(_MiscOutAccount12)
        BOFields.Add(_MiscOutAccount13)
        BOFields.Add(_MiscOutAccount14)
        BOFields.Add(_MiscOutAccount15)
        BOFields.Add(_MiscOutAccount16)
        BOFields.Add(_MiscOutAccount17)
        BOFields.Add(_MiscOutAccount18)
        BOFields.Add(_MiscOutAccount19)
        BOFields.Add(_MiscOutAccount20)
        BOFields.Add(_MiscOutAccountCode01)
        BOFields.Add(_MiscOutAccountCode02)
        BOFields.Add(_MiscOutAccountCode03)
        BOFields.Add(_MiscOutAccountCode04)
        BOFields.Add(_MiscOutAccountCode05)
        BOFields.Add(_MiscOutAccountCode06)
        BOFields.Add(_MiscOutAccountCode07)
        BOFields.Add(_MiscOutAccountCode08)
        BOFields.Add(_MiscOutAccountCode09)
        BOFields.Add(_MiscOutAccountCode10)
        BOFields.Add(_MiscOutAccountCode11)
        BOFields.Add(_MiscOutAccountCode12)
        BOFields.Add(_MiscOutAccountCode13)
        BOFields.Add(_MiscOutAccountCode14)
        BOFields.Add(_MiscOutAccountCode15)
        BOFields.Add(_MiscOutAccountCode16)
        BOFields.Add(_MiscOutAccountCode17)
        BOFields.Add(_MiscOutAccountCode18)
        BOFields.Add(_MiscOutAccountCode19)
        BOFields.Add(_MiscOutAccountCode20)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSystemAccountCodes)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSystemAccountCodes)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSystemAccountCodes))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            BORecords = New List(Of cBaseClass)

            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSystemAccountCodes(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _ID As New ColField(Of Integer)("ID", 0, "ID", True, True)
    Private _MiscInAccount01 As New ColField(Of String)("MiscInAccount01", "", "Misc In Account 01", False, False)
    Private _MiscInAccount02 As New ColField(Of String)("MiscInAccount02", "", "Misc In Account 02", False, False)
    Private _MiscInAccount03 As New ColField(Of String)("MiscInAccount03", "", "Misc In Account 03", False, False)
    Private _MiscInAccount04 As New ColField(Of String)("MiscInAccount04", "", "Misc In Account 04", False, False)
    Private _MiscInAccount05 As New ColField(Of String)("MiscInAccount05", "", "Misc In Account 05", False, False)
    Private _MiscInAccount06 As New ColField(Of String)("MiscInAccount06", "", "Misc In Account 06", False, False)
    Private _MiscInAccount07 As New ColField(Of String)("MiscInAccount07", "", "Misc In Account 07", False, False)
    Private _MiscInAccount08 As New ColField(Of String)("MiscInAccount08", "", "Misc In Account 08", False, False)
    Private _MiscInAccount09 As New ColField(Of String)("MiscInAccount09", "", "Misc In Account 09", False, False)
    Private _MiscInAccount10 As New ColField(Of String)("MiscInAccount10", "", "Misc In Account 10", False, False)
    Private _MiscInAccount11 As New ColField(Of String)("MiscInAccount11", "", "Misc In Account 11", False, False)
    Private _MiscInAccount12 As New ColField(Of String)("MiscInAccount12", "", "Misc In Account 12", False, False)
    Private _MiscInAccount13 As New ColField(Of String)("MiscInAccount13", "", "Misc In Account 13", False, False)
    Private _MiscInAccount14 As New ColField(Of String)("MiscInAccount14", "", "Misc In Account 14", False, False)
    Private _MiscInAccount15 As New ColField(Of String)("MiscInAccount15", "", "Misc In Account 15", False, False)
    Private _MiscInAccount16 As New ColField(Of String)("MiscInAccount16", "", "Misc In Account 16", False, False)
    Private _MiscInAccount17 As New ColField(Of String)("MiscInAccount17", "", "Misc In Account 17", False, False)
    Private _MiscInAccount18 As New ColField(Of String)("MiscInAccount18", "", "Misc In Account 18", False, False)
    Private _MiscInAccount19 As New ColField(Of String)("MiscInAccount19", "", "Misc In Account 19", False, False)
    Private _MiscInAccount20 As New ColField(Of String)("MiscInAccount20", "", "Misc In Account 20", False, False)
    Private _MiscInAccountCode01 As New ColField(Of String)("MiscInAccountCode01", "", "Misc In AccountCode 01", False, False)
    Private _MiscInAccountCode02 As New ColField(Of String)("MiscInAccountCode02", "", "Misc In AccountCode 02", False, False)
    Private _MiscInAccountCode03 As New ColField(Of String)("MiscInAccountCode03", "", "Misc In AccountCode 03", False, False)
    Private _MiscInAccountCode04 As New ColField(Of String)("MiscInAccountCode04", "", "Misc In AccountCode 04", False, False)
    Private _MiscInAccountCode05 As New ColField(Of String)("MiscInAccountCode05", "", "Misc In AccountCode 05", False, False)
    Private _MiscInAccountCode06 As New ColField(Of String)("MiscInAccountCode06", "", "Misc In AccountCode 06", False, False)
    Private _MiscInAccountCode07 As New ColField(Of String)("MiscInAccountCode07", "", "Misc In AccountCode 07", False, False)
    Private _MiscInAccountCode08 As New ColField(Of String)("MiscInAccountCode08", "", "Misc In AccountCode 08", False, False)
    Private _MiscInAccountCode09 As New ColField(Of String)("MiscInAccountCode09", "", "Misc In AccountCode 09", False, False)
    Private _MiscInAccountCode10 As New ColField(Of String)("MiscInAccountCode10", "", "Misc In AccountCode 10", False, False)
    Private _MiscInAccountCode11 As New ColField(Of String)("MiscInAccountCode11", "", "Misc In AccountCode 11", False, False)
    Private _MiscInAccountCode12 As New ColField(Of String)("MiscInAccountCode12", "", "Misc In AccountCode 12", False, False)
    Private _MiscInAccountCode13 As New ColField(Of String)("MiscInAccountCode13", "", "Misc In AccountCode 13", False, False)
    Private _MiscInAccountCode14 As New ColField(Of String)("MiscInAccountCode14", "", "Misc In AccountCode 14", False, False)
    Private _MiscInAccountCode15 As New ColField(Of String)("MiscInAccountCode15", "", "Misc In AccountCode 15", False, False)
    Private _MiscInAccountCode16 As New ColField(Of String)("MiscInAccountCode16", "", "Misc In AccountCode 16", False, False)
    Private _MiscInAccountCode17 As New ColField(Of String)("MiscInAccountCode17", "", "Misc In AccountCode 17", False, False)
    Private _MiscInAccountCode18 As New ColField(Of String)("MiscInAccountCode18", "", "Misc In AccountCode 18", False, False)
    Private _MiscInAccountCode19 As New ColField(Of String)("MiscInAccountCode19", "", "Misc In AccountCode 19", False, False)
    Private _MiscInAccountCode20 As New ColField(Of String)("MiscInAccountCode20", "", "Misc In AccountCode 20", False, False)
    Private _MiscOutAccount01 As New ColField(Of String)("MiscOutAccount01", "", "Misc In Account 01", False, False)
    Private _MiscOutAccount02 As New ColField(Of String)("MiscOutAccount02", "", "Misc In Account 02", False, False)
    Private _MiscOutAccount03 As New ColField(Of String)("MiscOutAccount03", "", "Misc In Account 03", False, False)
    Private _MiscOutAccount04 As New ColField(Of String)("MiscOutAccount04", "", "Misc In Account 04", False, False)
    Private _MiscOutAccount05 As New ColField(Of String)("MiscOutAccount05", "", "Misc In Account 05", False, False)
    Private _MiscOutAccount06 As New ColField(Of String)("MiscOutAccount06", "", "Misc In Account 06", False, False)
    Private _MiscOutAccount07 As New ColField(Of String)("MiscOutAccount07", "", "Misc In Account 07", False, False)
    Private _MiscOutAccount08 As New ColField(Of String)("MiscOutAccount08", "", "Misc In Account 08", False, False)
    Private _MiscOutAccount09 As New ColField(Of String)("MiscOutAccount09", "", "Misc In Account 09", False, False)
    Private _MiscOutAccount10 As New ColField(Of String)("MiscOutAccount10", "", "Misc In Account 10", False, False)
    Private _MiscOutAccount11 As New ColField(Of String)("MiscOutAccount11", "", "Misc In Account 11", False, False)
    Private _MiscOutAccount12 As New ColField(Of String)("MiscOutAccount12", "", "Misc In Account 12", False, False)
    Private _MiscOutAccount13 As New ColField(Of String)("MiscOutAccount13", "", "Misc In Account 13", False, False)
    Private _MiscOutAccount14 As New ColField(Of String)("MiscOutAccount14", "", "Misc In Account 14", False, False)
    Private _MiscOutAccount15 As New ColField(Of String)("MiscOutAccount15", "", "Misc In Account 15", False, False)
    Private _MiscOutAccount16 As New ColField(Of String)("MiscOutAccount16", "", "Misc In Account 16", False, False)
    Private _MiscOutAccount17 As New ColField(Of String)("MiscOutAccount17", "", "Misc In Account 17", False, False)
    Private _MiscOutAccount18 As New ColField(Of String)("MiscOutAccount18", "", "Misc In Account 18", False, False)
    Private _MiscOutAccount19 As New ColField(Of String)("MiscOutAccount19", "", "Misc In Account 19", False, False)
    Private _MiscOutAccount20 As New ColField(Of String)("MiscOutAccount20", "", "Misc In Account 20", False, False)
    Private _MiscOutAccountCode01 As New ColField(Of String)("MiscOutAccountCode01", "", "Misc In AccountCode 01", False, False)
    Private _MiscOutAccountCode02 As New ColField(Of String)("MiscOutAccountCode02", "", "Misc In AccountCode 02", False, False)
    Private _MiscOutAccountCode03 As New ColField(Of String)("MiscOutAccountCode03", "", "Misc In AccountCode 03", False, False)
    Private _MiscOutAccountCode04 As New ColField(Of String)("MiscOutAccountCode04", "", "Misc In AccountCode 04", False, False)
    Private _MiscOutAccountCode05 As New ColField(Of String)("MiscOutAccountCode05", "", "Misc In AccountCode 05", False, False)
    Private _MiscOutAccountCode06 As New ColField(Of String)("MiscOutAccountCode06", "", "Misc In AccountCode 06", False, False)
    Private _MiscOutAccountCode07 As New ColField(Of String)("MiscOutAccountCode07", "", "Misc In AccountCode 07", False, False)
    Private _MiscOutAccountCode08 As New ColField(Of String)("MiscOutAccountCode08", "", "Misc In AccountCode 08", False, False)
    Private _MiscOutAccountCode09 As New ColField(Of String)("MiscOutAccountCode09", "", "Misc In AccountCode 09", False, False)
    Private _MiscOutAccountCode10 As New ColField(Of String)("MiscOutAccountCode10", "", "Misc In AccountCode 10", False, False)
    Private _MiscOutAccountCode11 As New ColField(Of String)("MiscOutAccountCode11", "", "Misc In AccountCode 11", False, False)
    Private _MiscOutAccountCode12 As New ColField(Of String)("MiscOutAccountCode12", "", "Misc In AccountCode 12", False, False)
    Private _MiscOutAccountCode13 As New ColField(Of String)("MiscOutAccountCode13", "", "Misc In AccountCode 13", False, False)
    Private _MiscOutAccountCode14 As New ColField(Of String)("MiscOutAccountCode14", "", "Misc In AccountCode 14", False, False)
    Private _MiscOutAccountCode15 As New ColField(Of String)("MiscOutAccountCode15", "", "Misc In AccountCode 15", False, False)
    Private _MiscOutAccountCode16 As New ColField(Of String)("MiscOutAccountCode16", "", "Misc In AccountCode 16", False, False)
    Private _MiscOutAccountCode17 As New ColField(Of String)("MiscOutAccountCode17", "", "Misc In AccountCode 17", False, False)
    Private _MiscOutAccountCode18 As New ColField(Of String)("MiscOutAccountCode18", "", "Misc In AccountCode 18", False, False)
    Private _MiscOutAccountCode19 As New ColField(Of String)("MiscOutAccountCode19", "", "Misc In AccountCode 19", False, False)
    Private _MiscOutAccountCode20 As New ColField(Of String)("MiscOutAccountCode20", "", "Misc In AccountCode 20", False, False)

#End Region

#Region "Field Properties"

    Public Property ID() As ColField(Of Integer)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _ID = value
        End Set
    End Property
    Public Property MiscInAccount01() As ColField(Of String)
        Get
            Return _MiscInAccount01
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount01 = value
        End Set
    End Property
    Public Property MiscInAccount02() As ColField(Of String)
        Get
            Return _MiscInAccount02
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount02 = value
        End Set
    End Property
    Public Property MiscInAccount03() As ColField(Of String)
        Get
            Return _MiscInAccount03
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount03 = value
        End Set
    End Property
    Public Property MiscInAccount04() As ColField(Of String)
        Get
            Return _MiscInAccount04
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount04 = value
        End Set
    End Property
    Public Property MiscInAccount05() As ColField(Of String)
        Get
            Return _MiscInAccount05
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount05 = value
        End Set
    End Property
    Public Property MiscInAccount06() As ColField(Of String)
        Get
            Return _MiscInAccount06
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount06 = value
        End Set
    End Property
    Public Property MiscInAccount07() As ColField(Of String)
        Get
            Return _MiscInAccount07
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount07 = value
        End Set
    End Property
    Public Property MiscInAccount08() As ColField(Of String)
        Get
            Return _MiscInAccount08
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount08 = value
        End Set
    End Property
    Public Property MiscInAccount09() As ColField(Of String)
        Get
            Return _MiscInAccount09
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount09 = value
        End Set
    End Property
    Public Property MiscInAccount10() As ColField(Of String)
        Get
            Return _MiscInAccount10
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount10 = value
        End Set
    End Property
    Public Property MiscInAccount11() As ColField(Of String)
        Get
            Return _MiscInAccount11
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount11 = value
        End Set
    End Property
    Public Property MiscInAccount12() As ColField(Of String)
        Get
            Return _MiscInAccount12
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount12 = value
        End Set
    End Property
    Public Property MiscInAccount13() As ColField(Of String)
        Get
            Return _MiscInAccount13
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount13 = value
        End Set
    End Property
    Public Property MiscInAccount14() As ColField(Of String)
        Get
            Return _MiscInAccount14
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount14 = value
        End Set
    End Property
    Public Property MiscInAccount15() As ColField(Of String)
        Get
            Return _MiscInAccount15
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount01 = value
        End Set
    End Property
    Public Property MiscInAccount16() As ColField(Of String)
        Get
            Return _MiscInAccount16
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount16 = value
        End Set
    End Property
    Public Property MiscInAccount17() As ColField(Of String)
        Get
            Return _MiscInAccount17
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount17 = value
        End Set
    End Property
    Public Property MiscInAccount18() As ColField(Of String)
        Get
            Return _MiscInAccount18
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount18 = value
        End Set
    End Property
    Public Property MiscInAccount19() As ColField(Of String)
        Get
            Return _MiscInAccount19
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount19 = value
        End Set
    End Property
    Public Property MiscInAccount20() As ColField(Of String)
        Get
            Return _MiscInAccount20
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccount20 = value
        End Set
    End Property
    Public Property MiscInAccountCode01() As ColField(Of String)
        Get
            Return _MiscInAccountCode01
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode01 = value
        End Set
    End Property
    Public Property MiscInAccountCode02() As ColField(Of String)
        Get
            Return _MiscInAccountCode02
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode02 = value
        End Set
    End Property
    Public Property MiscInAccountCode03() As ColField(Of String)
        Get
            Return _MiscInAccountCode03
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode03 = value
        End Set
    End Property
    Public Property MiscInAccountCode04() As ColField(Of String)
        Get
            Return _MiscInAccountCode04
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode04 = value
        End Set
    End Property
    Public Property MiscInAccountCode05() As ColField(Of String)
        Get
            Return _MiscInAccountCode05
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode05 = value
        End Set
    End Property
    Public Property MiscInAccountCode06() As ColField(Of String)
        Get
            Return _MiscInAccountCode06
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode06 = value
        End Set
    End Property
    Public Property MiscInAccountCode07() As ColField(Of String)
        Get
            Return _MiscInAccountCode07
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode07 = value
        End Set
    End Property
    Public Property MiscInAccountCode08() As ColField(Of String)
        Get
            Return _MiscInAccountCode08
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode08 = value
        End Set
    End Property
    Public Property MiscInAccountCode09() As ColField(Of String)
        Get
            Return _MiscInAccountCode09
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode09 = value
        End Set
    End Property
    Public Property MiscInAccountCode10() As ColField(Of String)
        Get
            Return _MiscInAccountCode10
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode10 = value
        End Set
    End Property
    Public Property MiscInAccountCode11() As ColField(Of String)
        Get
            Return _MiscInAccountCode11
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode11 = value
        End Set
    End Property
    Public Property MiscInAccountCode12() As ColField(Of String)
        Get
            Return _MiscInAccountCode12
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode12 = value
        End Set
    End Property
    Public Property MiscInAccountCode13() As ColField(Of String)
        Get
            Return _MiscInAccountCode13
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode13 = value
        End Set
    End Property
    Public Property MiscInAccountCode14() As ColField(Of String)
        Get
            Return _MiscInAccountCode14
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode14 = value
        End Set
    End Property
    Public Property MiscInAccountCode15() As ColField(Of String)
        Get
            Return _MiscInAccountCode15
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode01 = value
        End Set
    End Property
    Public Property MiscInAccountCode16() As ColField(Of String)
        Get
            Return _MiscInAccountCode16
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode16 = value
        End Set
    End Property
    Public Property MiscInAccountCode17() As ColField(Of String)
        Get
            Return _MiscInAccountCode17
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode17 = value
        End Set
    End Property
    Public Property MiscInAccountCode18() As ColField(Of String)
        Get
            Return _MiscInAccountCode18
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode18 = value
        End Set
    End Property
    Public Property MiscInAccountCode19() As ColField(Of String)
        Get
            Return _MiscInAccountCode19
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode19 = value
        End Set
    End Property
    Public Property MiscInAccountCode20() As ColField(Of String)
        Get
            Return _MiscInAccountCode20
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscInAccountCode20 = value
        End Set
    End Property
    Public Property MiscOutAccount01() As ColField(Of String)
        Get
            Return _MiscOutAccount01
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount01 = value
        End Set
    End Property
    Public Property MiscOutAccount02() As ColField(Of String)
        Get
            Return _MiscOutAccount02
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount02 = value
        End Set
    End Property
    Public Property MiscOutAccount03() As ColField(Of String)
        Get
            Return _MiscOutAccount03
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount03 = value
        End Set
    End Property
    Public Property MiscOutAccount04() As ColField(Of String)
        Get
            Return _MiscOutAccount04
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount04 = value
        End Set
    End Property
    Public Property MiscOutAccount05() As ColField(Of String)
        Get
            Return _MiscOutAccount05
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount05 = value
        End Set
    End Property
    Public Property MiscOutAccount06() As ColField(Of String)
        Get
            Return _MiscOutAccount06
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount06 = value
        End Set
    End Property
    Public Property MiscOutAccount07() As ColField(Of String)
        Get
            Return _MiscOutAccount07
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount07 = value
        End Set
    End Property
    Public Property MiscOutAccount08() As ColField(Of String)
        Get
            Return _MiscOutAccount08
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount08 = value
        End Set
    End Property
    Public Property MiscOutAccount09() As ColField(Of String)
        Get
            Return _MiscOutAccount09
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount09 = value
        End Set
    End Property
    Public Property MiscOutAccount10() As ColField(Of String)
        Get
            Return _MiscOutAccount10
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount10 = value
        End Set
    End Property
    Public Property MiscOutAccount11() As ColField(Of String)
        Get
            Return _MiscOutAccount11
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount11 = value
        End Set
    End Property
    Public Property MiscOutAccount12() As ColField(Of String)
        Get
            Return _MiscOutAccount12
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount12 = value
        End Set
    End Property
    Public Property MiscOutAccount13() As ColField(Of String)
        Get
            Return _MiscOutAccount13
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount13 = value
        End Set
    End Property
    Public Property MiscOutAccount14() As ColField(Of String)
        Get
            Return _MiscOutAccount14
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount14 = value
        End Set
    End Property
    Public Property MiscOutAccount15() As ColField(Of String)
        Get
            Return _MiscOutAccount15
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount01 = value
        End Set
    End Property
    Public Property MiscOutAccount16() As ColField(Of String)
        Get
            Return _MiscOutAccount16
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount16 = value
        End Set
    End Property
    Public Property MiscOutAccount17() As ColField(Of String)
        Get
            Return _MiscOutAccount17
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount17 = value
        End Set
    End Property
    Public Property MiscOutAccount18() As ColField(Of String)
        Get
            Return _MiscOutAccount18
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount18 = value
        End Set
    End Property
    Public Property MiscOutAccount19() As ColField(Of String)
        Get
            Return _MiscOutAccount19
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount19 = value
        End Set
    End Property
    Public Property MiscOutAccount20() As ColField(Of String)
        Get
            Return _MiscOutAccount20
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccount20 = value
        End Set
    End Property
    Public Property MiscOutAccountCode01() As ColField(Of String)
        Get
            Return _MiscOutAccountCode01
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode01 = value
        End Set
    End Property
    Public Property MiscOutAccountCode02() As ColField(Of String)
        Get
            Return _MiscOutAccountCode02
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode02 = value
        End Set
    End Property
    Public Property MiscOutAccountCode03() As ColField(Of String)
        Get
            Return _MiscOutAccountCode03
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode03 = value
        End Set
    End Property
    Public Property MiscOutAccountCode04() As ColField(Of String)
        Get
            Return _MiscOutAccountCode04
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode04 = value
        End Set
    End Property
    Public Property MiscOutAccountCode05() As ColField(Of String)
        Get
            Return _MiscOutAccountCode05
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode05 = value
        End Set
    End Property
    Public Property MiscOutAccountCode06() As ColField(Of String)
        Get
            Return _MiscOutAccountCode06
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode06 = value
        End Set
    End Property
    Public Property MiscOutAccountCode07() As ColField(Of String)
        Get
            Return _MiscOutAccountCode07
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode07 = value
        End Set
    End Property
    Public Property MiscOutAccountCode08() As ColField(Of String)
        Get
            Return _MiscOutAccountCode08
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode08 = value
        End Set
    End Property
    Public Property MiscOutAccountCode09() As ColField(Of String)
        Get
            Return _MiscOutAccountCode09
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode09 = value
        End Set
    End Property
    Public Property MiscOutAccountCode10() As ColField(Of String)
        Get
            Return _MiscOutAccountCode10
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode10 = value
        End Set
    End Property
    Public Property MiscOutAccountCode11() As ColField(Of String)
        Get
            Return _MiscOutAccountCode11
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode11 = value
        End Set
    End Property
    Public Property MiscOutAccountCode12() As ColField(Of String)
        Get
            Return _MiscOutAccountCode12
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode12 = value
        End Set
    End Property
    Public Property MiscOutAccountCode13() As ColField(Of String)
        Get
            Return _MiscOutAccountCode13
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode13 = value
        End Set
    End Property
    Public Property MiscOutAccountCode14() As ColField(Of String)
        Get
            Return _MiscOutAccountCode14
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode14 = value
        End Set
    End Property
    Public Property MiscOutAccountCode15() As ColField(Of String)
        Get
            Return _MiscOutAccountCode15
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode01 = value
        End Set
    End Property
    Public Property MiscOutAccountCode16() As ColField(Of String)
        Get
            Return _MiscOutAccountCode16
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode16 = value
        End Set
    End Property
    Public Property MiscOutAccountCode17() As ColField(Of String)
        Get
            Return _MiscOutAccountCode17
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode17 = value
        End Set
    End Property
    Public Property MiscOutAccountCode18() As ColField(Of String)
        Get
            Return _MiscOutAccountCode18
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode18 = value
        End Set
    End Property
    Public Property MiscOutAccountCode19() As ColField(Of String)
        Get
            Return _MiscOutAccountCode19
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode19 = value
        End Set
    End Property
    Public Property MiscOutAccountCode20() As ColField(Of String)
        Get
            Return _MiscOutAccountCode20
        End Get
        Set(ByVal value As ColField(Of String))
            _MiscOutAccountCode20 = value
        End Set
    End Property


#End Region

#Region "Methods"

    ''' <summary>
    ''' Loads first record into this instance.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Load()

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            If dt.Rows.Count > 0 Then
                Me.LoadFromRow(dt.Rows(0))
            End If

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AccountCodesLoad, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Returns array of miscellaneous income codes.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMiscInCodes() As String()

        Try
            Dim codes(19) As String
            codes(0) = _MiscInAccountCode01.Value
            codes(1) = _MiscInAccountCode02.Value
            codes(2) = _MiscInAccountCode03.Value
            codes(3) = _MiscInAccountCode04.Value
            codes(4) = _MiscInAccountCode05.Value
            codes(5) = _MiscInAccountCode06.Value
            codes(6) = _MiscInAccountCode07.Value
            codes(7) = _MiscInAccountCode08.Value
            codes(8) = _MiscInAccountCode09.Value
            codes(9) = _MiscInAccountCode10.Value
            codes(10) = _MiscInAccountCode11.Value
            codes(11) = _MiscInAccountCode12.Value
            codes(12) = _MiscInAccountCode13.Value
            codes(13) = _MiscInAccountCode14.Value
            codes(14) = _MiscInAccountCode15.Value
            codes(15) = _MiscInAccountCode16.Value
            codes(16) = _MiscInAccountCode17.Value
            codes(17) = _MiscInAccountCode18.Value
            codes(18) = _MiscInAccountCode19.Value
            codes(19) = _MiscInAccountCode20.Value

            Return codes

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AccountCodesGetMiscIn, ex)
        End Try

    End Function

    ''' <summary>
    ''' Returns array of miscellaneous outgoing codes.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMiscOutCodes() As String()

        Try
            Dim codes(19) As String
            codes(0) = _MiscOutAccountCode01.Value
            codes(1) = _MiscOutAccountCode02.Value
            codes(2) = _MiscOutAccountCode03.Value
            codes(3) = _MiscOutAccountCode04.Value
            codes(4) = _MiscOutAccountCode05.Value
            codes(5) = _MiscOutAccountCode06.Value
            codes(6) = _MiscOutAccountCode07.Value
            codes(7) = _MiscOutAccountCode08.Value
            codes(8) = _MiscOutAccountCode09.Value
            codes(9) = _MiscOutAccountCode10.Value
            codes(10) = _MiscOutAccountCode11.Value
            codes(11) = _MiscOutAccountCode12.Value
            codes(12) = _MiscOutAccountCode13.Value
            codes(13) = _MiscOutAccountCode14.Value
            codes(14) = _MiscOutAccountCode15.Value
            codes(15) = _MiscOutAccountCode16.Value
            codes(16) = _MiscOutAccountCode17.Value
            codes(17) = _MiscOutAccountCode18.Value
            codes(18) = _MiscOutAccountCode19.Value
            codes(19) = _MiscOutAccountCode20.Value

            Return codes

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.AccountCodesGetMiscIn, ex)
        End Try

    End Function

#End Region

End Class
