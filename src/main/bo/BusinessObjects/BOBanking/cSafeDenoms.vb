﻿<Serializable()> Public Class cSafeDenoms
    Inherits cBaseClass

    Public Sub New()
        MyBase.New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        MyBase.New(strConnection)
        Start()
    End Sub
    Public Sub New(ByRef oasys3DB As Oasys3.DB.clsOasys3DB)
        MyBase.New(oasys3DB)
        Start()
    End Sub

    Public Overrides Sub Start()

        TableName = "SafeDenoms"
        BOFields.Add(_PeriodID)
        BOFields.Add(_CurrencyID)
        BOFields.Add(_TenderID)
        BOFields.Add(_ID)
        BOFields.Add(_SafeValue)
        BOFields.Add(_ChangeValue)
        BOFields.Add(_SystemValue)
        BOFields.Add(_SuggestedValue)

    End Sub
    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of cSafeDenoms)

        Try
            LoadBORecords(count)

            Dim col As New List(Of cSafeDenoms)
            For Each record As cBaseClass In BORecords
                col.Add(CType(record, cSafeDenoms))
            Next

            Return col

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Overrides Sub LoadBORecords(Optional ByVal count As Integer = -1)

        Try
            GetSQLBase()
            Dim ds As DataSet = Oasys3DB.Query(count)

            BORecords = New List(Of cBaseClass)
            Select Case ds.Tables(0).Rows.Count
                Case 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                Case Is > 1
                    Me.LoadFromRow(ds.Tables(0).Rows(0))
                    BORecords.Add(Me)

                    For rowIndex As Integer = 1 To ds.Tables(0).Rows.Count - 1
                        Dim BO As New cSafeDenoms(Oasys3DB)
                        BO.LoadFromRow(ds.Tables(0).Rows(rowIndex))
                        BORecords.Add(BO)
                    Next
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            ClearLists()
        End Try

    End Sub

#Region "Fields"

    Private _PeriodID As New ColField(Of Integer)("PeriodID", 0, "Period ID", True, False)
    Private _CurrencyID As New ColField(Of String)("CurrencyID", "", "Currency ID", True, False)
    Private _TenderID As New ColField(Of Integer)("TenderID", 1, "Tender ID", True, False)
    Private _ID As New ColField(Of Decimal)("ID", 0.01D, "ID", True, False)
    Private _SafeValue As New ColField(Of Decimal)("SafeValue", 0, "Safe Value", False, False)
    Private _ChangeValue As New ColField(Of Decimal)("ChangeValue", 0, "Change Value", False, False)
    Private _SystemValue As New ColField(Of Decimal)("SystemValue", 0, "System Value", False, False)
    Private _SuggestedValue As New ColField(Of Decimal)("SuggestedValue", 0, "Suggested Value", False, False)

#End Region

#Region "Fields Properties"

    Public Property PeriodID() As ColField(Of Integer)
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _PeriodID = value
        End Set
    End Property
    Public Property CurrencyID() As ColField(Of String)
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As ColField(Of String))
            _CurrencyID = value
        End Set
    End Property
    Public Property TenderID() As ColField(Of Integer)
        Get
            Return _TenderID
        End Get
        Set(ByVal value As ColField(Of Integer))
            _TenderID = value
        End Set
    End Property
    Public Property ID() As ColField(Of Decimal)
        Get
            Return _ID
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ID = value
        End Set
    End Property
    Public Property SafeValue() As ColField(Of Decimal)
        Get
            Return _SafeValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SafeValue = value
        End Set
    End Property
    Public Property ChangeValue() As ColField(Of Decimal)
        Get
            Return _ChangeValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _ChangeValue = value
        End Set
    End Property
    Public Property SystemValue() As ColField(Of Decimal)
        Get
            Return _SystemValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SystemValue = value
        End Set
    End Property
    Public Property SuggestedValue() As ColField(Of Decimal)
        Get
            Return _SuggestedValue
        End Get
        Set(ByVal value As ColField(Of Decimal))
            _SuggestedValue = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>
    ''' Returns safe denominations for given period.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDenominations(ByVal periodID As Integer) As List(Of cSafeDenoms)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pSelect)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
            Dim dt As DataTable = Oasys3DB.Query.Tables(0)

            Dim denoms As New List(Of cSafeDenoms)
            For Each dr As DataRow In dt.Rows
                Dim denom As New cSafeDenoms(Oasys3DB)
                denom.LoadFromRow(dr)
                denoms.Add(denom)
            Next

            Return denoms

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeDenomsLoad, ex)
        End Try

    End Function

    ''' <summary>
    ''' Deletes safe denominations for given period ID.
    '''  Throws Oasys exception on error
    ''' </summary>
    ''' <param name="periodID"></param>
    ''' <remarks></remarks>
    Public Overloads Sub Delete(ByVal periodID As Integer)

        Try
            Oasys3DB.ClearAllParameters()
            Oasys3DB.SetTableParameter(TableName, clsOasys3DB.eSqlQueryType.pDelete)
            Oasys3DB.SetWhereParameter(_PeriodID.ColumnName, clsOasys3DB.eOperator.pEquals, periodID)
            Oasys3DB.Delete()

        Catch ex As Exception
            Throw New OasysDbException(My.Resources.Errors.SafeDenomsDelete, ex)
        End Try

    End Sub

#End Region

End Class
