
About this solution

This is the current WIXSTEP.NLM source code, version 3.08 (or so), imported into a .NET
solution so that the intellisense and IDE can be used to work on the code. This project
does not compile through the IDE.

Watcom version 11.0 and the Novell NLM SDK are required to build the NLM. Their default
locations are C:\SDKCD15 and C:\WATCOM.

To make the .NLM, open a DOS window, CD to this directory, run AUTOEXEC.BAT to setup the
paths and env variables, and then run NMAKE, a whatcom utility which will use the files
MAKEINIT and MAKEFILE to build WIXSTEP.NLM. Errors and warnings can be seen in the
command line window.

