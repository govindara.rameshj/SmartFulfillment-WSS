


HWND GetMainWindowHandle(void);



// Logging

#define ProcessLogNone		0
#define ProcessLogConsole 1
#define ProcessLogFile		2

void ProcessLogSetFilename(char *FileName);
int ProcessLogInitialize(void);
int ProcessLogDevice(void);
FILE *ProcessLogHandle(void);
void ProcessLogFlushBuffer(void);
void ProcessLogStartToFile(int append);
void ProcessLogStartToConsole(void);
void ProcessLogStop(void);
int ProcessLogWriteTimeStamp(char *msg);
int ProcessLogWrite(char *msg);
int ProcessLogWriteLong(long li);
int ProcessLogWriteLn(char *msg);
BOOL ProcessLogWriteToScreenStale(void);
void SetLogToScreen(BOOL on);
BOOL GetLogToScreen(void);
void ProcessLogWriteToScreen(void);

void DisplayServerLog(void);
