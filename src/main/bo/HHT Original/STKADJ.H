//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio 
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Header:StkAdj.h - STKADJ table definition
//---------------------------------------------------------------------------
/*     Wickes : STKADJ Stock Adjustment File definition					*/
/*     W06 - 05.07.00 - JR - WIX941 (Based on UX version W06 )		    */
/*----------------------------------------------------------------------*/

typedef struct
   {
	  //BTI_CHAR date[ 2 ];	/* D	Date of adjustment 		(key0)		*/
	  DBLongDate date;		/* D	Date of adjustment 		(key0)		*/
	  //BTI_CHAR code[ 1 ];	/* C2	Stock Adjustment code	(key0)		*/
	  BTI_CHAR code[2];		/* char 2	Stock Adjustment code	(key0)		*/
	  //BTI_CHAR skun[ 3 ];	/* C6 	SKU Number adjusted		(key0)		*/		
	  BTI_CHAR skun[6];		/* char 6 	SKU Number adjusted		(key0)		*/		
	  //BTI_CHAR seqn[ 1 ];	/* C2 	Always 0 unless transfer type		*/		
	  BTI_CHAR seqn[2];		/* char 2 	Always 0 unless transfer type		*/		
							/*    	then 0->99              (key0)  	*/		
	  BTI_CHAR init[ 5 ];	/* A5 	Initials of who did adjustment		*/	
	  //BTI_CHAR dept[ 1 ];	/* C2 	from Dept number ** not used W06**	*/	
	  BTI_CHAR dept[2];		/* char 2 	from Dept number ** not used W06**	*/	
	  //BTI_CHAR sstk[ 3 ];	/* N6 	Starting stock qty - before adj.	*/	
	  BTI_CHAR sstk[4];		/* 7.0 	Starting stock qty - before adj.	*/	
	  //BTI_CHAR quan[ 3 ];	/* N6 	Adjustment qty						*/	
	  BTI_CHAR quan[4];		/* 7.0 	Adjustment qty						*/	
	  //BTI_CHAR pric[ 4 ];	/* N8.2	Price at time of adj.				*/	
	  BTI_CHAR pric[5];		/* 9.2	Price at time of adj.				*/	
	  //BTI_CHAR cost[ 5 ];	/* N10.4 Cost at time of adj.				*/	
	  BTI_CHAR cost[6];		/* 11.4 Cost at time of adj.				*/	
	  BTI_CHAR comm[ 1 ];	/* I    ON = has been comm'ed to head office*/	
	  BTI_CHAR type[ 1 ];	/* A1   Adjustment type from SACODE file	*/	
	  //BTI_CHAR tsku[ 3 ];	/* C6   Transfer TO item ** not used **  	*/	
	  BTI_CHAR tsku[6];		/* char 6  Transfer TO item ** not used **  	*/	
	  //BTI_CHAR tval[ 4 ];	/* N8.2 Transfer MD value ** not used **  	*/	
	  BTI_CHAR tval[5];		/* 9.2  Transfer MD value ** not used **  	*/	
	  BTI_CHAR info[ 20 ];	/* T20  Adjustment comment(code 10/20)		*/
	  //BTI_CHAR drln[ 3 ];	/* C6   Drl Number (code 04 ->H/O DRL adj)	*/	
	  BTI_CHAR drln[6];		/* char 6  Drl Number (code 04 ->H/O DRL adj)	*/	
	  BTI_CHAR rcod[ 1 ];	/* A1   Reversal Ind                        */	
							/*		R = Reversed Adj. (applies to orig) */
							/*		A = Reversal Adjustment				*/
	  BTI_CHAR mowt[ 1 ];	/* A1	Mark-down(M) or Write Off Type(W)	*/
	  //BTI_CHAR waut[ 2 ];	/* C3	ID of person authorising Write off	*/
	  BTI_CHAR waut[3];		/* char 3 ID of person authorising Write off	*/
							/*		for MOWT = W only.      			*/
	  BTI_CHAR daut[4];
	  BTI_CHAR spare[ 94 ];	/* A96  filler						      	*/
} STKADJ;
