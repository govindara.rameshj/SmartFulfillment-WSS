//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WIXSTEP.rc
//
#define IDM_MAIN_MENU                   102
#define IDM_VIEW_LOGTOSCREEN            40004
#define IDM_ABOUT                       40009
#define IDM_FILE_INITIALIZE_SERVER      40013
#define IDM_FILE_BEGIN_LOGGING          40016
#define IDM_FILE_SHUTDOWN_SERVER        40020
#define IDM_FILE_END_LOGGING            40021
#define IDM_FILE_EXIT_SHUTDOWN_SERVER   40022
#define ID_FILE_SQLSTATEMENT            40023

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40024
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
