//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio 
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// About this solution: This is the current WIXSTEP.NLM source code imported 
// into a .NET solution so that the intellisense and IDE can be used to work 
// on the code. This project does not compile through the IDE. To make the .NLM, 
// open a DOS window, CD to this directory, run AUTOEXEC.BAT to setup the paths 
// and env variables, and then run NMAKE, a WATCOM utility which will use the 
// files MAKEINIT and MAKEFILE to build WIXSTEP.NLM. Errors and warnings can be 
// seen in the command line window. The WATCOM and Novell NLM SDK locations are 
// referenced in AUTOEXEC. Current default locations are C:\SDKCD15\ and
// C:\WHATCOM
// --------------------------------------------------------------------------
// Module:WixStep.c - Main for the HHT Server with NLM Console, init, socket 
//										listener, and cleanup.
//	Main() initializes everything and then enters a loop that checks keyboard
//	input (F8 exits the NLM) and calls iMainProgramLogic(), which is the socket 
//	listener. When a socket has activity, iMainProgramLogic() handles connects,
//	disconnects, and packets (by calling lApp()).
//-----------------------------------------------------------------------------
// Old documentation:
// * A WinSock Step emulation program for Windows NT/95
// * 6/1/98 by Mark Heath & David Clavey, Lowther Consultants
// * Main Program and Socket Start
// * Win32-Console mode and 16bit DOS Versions
// * Converted to NLM in June 98 (JG)
//-----------------------------------------------------------------------------
// Revision History
//-----------------------------------------------------------------------------
// W3.08b GS - 15/06/05 - Verbal - Fixed bug with STKLOG Lookup- SEEK [STKLOG]
// W3.08a GS - 19/04/05 - Verbal - Fixed bug with sold today in primary count
// W3.08	GS - 11/01/05 - AL1129 - Changes to Support .dta files
// A3.07e14.10.04 JR  AL1129  Changes to Support .dta files
// A3.07c26.11.01 JR  2001-041 Added Calculation for Price change Markup/Down
// W3.07b20.04.01 JR  a.Ref: Changes to resolve Overnight "Sold Today" problem
//                    b.Verbal Request to Close Resources via UNLOAD NLM 
// W3.07 19.12.00 JR  WIX979 : Changes to WIXSTEP.TXT for menu changes
//                    and identify non orderable items (for Gap Walk).
// W3.06 17.11.00 JR  WIX729 : Added Label request Functionality 
// W3.05 08.11.00 JR  Verbal Req: Menu Changes for Main Menu GAP WALK option
// W3.04 25.10.00 JR  WIX925 : Added Gap Walk & further Menu Changes
// W3.03 10.10.00 JR  WIX933 : Added Price Change Functionality
// W3.02 20.09.00 JR  WIX950 : Added Menu & Security Functionality
// W3.01 19.09.00 JR  WIX732 : MultiUser Changes 
// W2.08 25.08.00 JR  WIX732 : MultiUser Changes 
// W2.07 25.07.00 JR  WIX947 : Remove from SOH and display MOH (MD On-Hand)
//                    and rearrange input fields
// W2.06 05.07.00 JR  WIX941 : Added fuctionality to support MarkDowns
//                    and Write Off Stock Adjustnments
// W2.05 09.02.00 JR  CTS869 : Changes to "Sold Today" to include Markdown
//                    stock movement type "04"
// W2.04 07.02.00 JR  CTS869 : Changes to Database.c to support entered
//										scanned SKU's less than 6 digits in length.
// W2.02 17.01.00 JR  CTS869 : Changes to Fields.C and Database.c to support
//                    stock take of MarkDown Stock. 
// W2.01 13.12.99 JR  CTS051 : Rename TCPSTEP(1.08) as WIXSTEP.NLM(2.01)
//-----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_ 
#include "wixstep.h"

/* Function prototypes */
int iMainProgramLogic(void);
void vCloseRoutine( void ) ;								/* W3.07b*/ 

extern int Make_New (SOCKET s);
extern void vCloseSocket (char *, int, int);
extern int getInUseIdx (SOCKET s);
extern void setInUseOctet (SOCKET s, unsigned int octet);
extern void vStartMenuDatabase( void );					/*	W3.02	*/  
extern void vStartPriceChanges( void );					/*	W3.03	*/  
extern int iInitialise(SOCKET *);
extern int iInitialiseUsers(void);
extern long lApp(long, SOCKET, int);
extern BOOL fLogonAttempt(SOCKET);
extern long lMenuLogoff(SOCKET, int);						/*	W3.02	*/
 

void DisplayConsoleHelp(BOOL ClearScreen);

char pcSpaces[SCREEN_COLS + 1];
fd_set readfds;
struct timeval  sSelTime;

INUSE_TYPE sInUse[MAX_SOCKETS];
SVUSE_TYPE sSvUse[MAX_SOCKETS];				/* W.05d Saved Socket Detail*/
USER_TYPE sUser[MAX_SOCKETS];
COUNTED_SKUS pcCount[MAX_SOCKETS];			/*Counted SKUs/User  W2.08	*/
GAPWALK_SKUS pcGapTable[MAX_SOCKETS];		/*Gap Walk SKUs/User W3.05	*/
MENU_OPTIONS pcMenuTable[MAX_SOCKETS];		/*Menu Options/User  W3.06	*/

FILE *hCommandFile;

/* Global Socket definition */
SOCKET sSocket;

long lTimeOutValue  = 10;	/* Menu Timeout In Seconds*/			/*W3.02*/
/***************************************************************************
Function:	 main
Description: Main program flow showing how TSR should be implemented
Passed:		 none
Return:		 none
****************************************************************************/

void Debug()
{
/*
	char dateStr[12];
	char dateStr2[12];
	long loop,jDate;
	ProcessLogStartToFile(FALSE);
	strcpy(dateStr,"01/01/2003");
	loop=DateStrToDayNumber(dateStr);
	while (loop<39000) {
		DayNumberToDateStr(dateStr,loop);
		jDate=DateStrToDayNumber(dateStr);
		DayNumberToDateStr(dateStr2,jDate);
		fprintf(ProcessLogHandle(),"%7ld=>%11s=>%7ld=>%11s\n",loop, dateStr,jDate,dateStr2);
		strcpy(dateStr,dateStr2);
		loop++;
	}
	*/
}

void main(void /*int argc, char **argv*/)
{
	int iLoop=1;
	char c;
	ProcessLogInitialize();
	ProcessLogStartToConsole();

	/* Set up Exit Routine */
  atexit(vCloseRoutine);								
	printf ("WIXSTEP Server Initialising, version %s\n",APP_VERSION);
	if (iStartUpDatabase() != 0) {
		printf ("\nServer Database failed to start, stopping Server.\n");
		return;
	}
	printf("Database ready\n");
	
	if (iInitialise(&sSocket)<0) {
		printf ("\nSocket failed to start, stopping Server.\n");
		return;
	}
	printf("Socket services ready\n");

	if(iInitialiseUsers() < 0) {
		printf("\nInitialise failed, stopping Server.\n");
		return;
	}
	printf("Local workspace and scripting file ready\n");

	DisplayConsoleHelp(FALSE);
	
	Debug();

	printf(">");
	while(iLoop) {
		if (kbhit()) {									// Keyboard monitor, f8 key exits code 
			if ((c=getch()) == 0)	{				// Fn key ignore char "0" 
				c=getch();									// get next character
				printf("F%d\n",c-58);
				switch (c) {
					case 62:									// F4 = log to screen
						ProcessLogStartToConsole();
						break;
					case 63:									// F5 = Log to file (rewrite)
						ProcessLogStartToFile(FALSE);
						DisplayLogFileWarning();
						break;
					case 64:									// F6 = Log to file (append)
						ProcessLogStartToFile(TRUE);
						DisplayLogFileWarning();
						break;
					case 65:									// F7 = Stop Logging
						ProcessLogStop();
						break;
					case 66:									// 66=F8 Exits 
						iLoop=0;
						break;
					default:
						DisplayConsoleHelp(TRUE);
						break;
				}
			} else {
				DisplayConsoleHelp(TRUE);
			}
		printf(">");	
		} else {	/* Call Main Program Logic with TSR timer */
			iLoop = iMainProgramLogic();
		}
	}
}

void DisplayConsoleHelp(BOOL ClearScreen)
{
	if (ClearScreen) {
		clrscr();
	}
	printf(	"\n"
					"WIXSTEP HHT Server version %s\n"
					"Database path is %s\n"
					"Support file path is %s\n",
					APP_VERSION,DATABASE_PATH,BASE_PATH);
	if (ProcessLogDevice()==ProcessLogFile) {
		printf("Logging to file %s\\WIZSTEP.LOG\n",BASE_PATH);
	} else 	if (ProcessLogDevice()==ProcessLogConsole) {
		printf("Logging to console\n");
	} else {
		printf("Logging is Off\n");
	}
	printf("  \n"
				 "  F4 Log to screen only\n"
				 "  F5 Log to file WIXSTEP.LOG (rewrite)\n"
				 "  F6 Log to file WIXSTEP.LOG (append)\n"
				 "  F7 Stop Logging\n"
				 "  F8 Shutdown HHT server\n"
				 "  Press any other key for this menu\n\n");


}

void DisplayLogFileWarning()
{
	printf ("\n\t ---------------------------------------------------------\n");
	printf (  "\t         Please REMEMBER to Switch logging OFF            \n");
	printf (  "\t              (via pressing the F7 key)                   \n");
	printf (  "\t    when logging has completed, then review the Log.      \n");
	printf (  "\t ---------------------------------------------------------\n\n");
}


/***************************************************************************
Function:    Start up database
Description: Start up the Betrieve Database
Passed:      none
Return:      0 = Success, else Btrieve error
****************************************************************************/
int iStartUpDatabase( void )
{
	#define VERSION_OFFSET     0
  #define REVISION_OFFSET    2
  #define PLATFORM_ID_OFFSET 4
	BTI_CHAR RetBuf[15];
	BTI_WORD length=15;
	BTI_SINT iDataBaseStatus;
  
	/* GET BTRIEVE VERSION /W BTRV FOR PLATFORMS THAT DO NOT SUPPORT BTRVID */
  iDataBaseStatus = BTRVID( B_VERSION, NULL, RetBuf, &length, NULL, NULL,&dbUserID);
  if (iDataBaseStatus == B_NO_ERROR) {
    printf( "Btrieve Database Version Is %d.%d %d\n",
         (*(BTI_WORD *)RetBuf+VERSION_OFFSET),
         (*(BTI_WORD *)RetBuf+REVISION_OFFSET),
         (int)(*(BTI_CHAR *)RetBuf+PLATFORM_ID_OFFSET));
		DBInitTable(&dbtStkMas,"stkmas",&stkmas,sizeof(stkmas));
		DBInitTable(&dbtEanMas,"eanmas",&eanmas,sizeof(eanmas));
		DBInitTable(&dbtHhtHdr,"hhthdr",&hhthdr,sizeof(hhthdr));
		DBInitTable(&dbtHhtDet,"hhtdet",&hhtdet,sizeof(hhtdet));
		DBInitTable(&dbtStkLog,"stklog",&stklog,sizeof(stklog));
		DBInitTable(&dbtStkAdj,"stkadj",&stkadj,sizeof(stkadj));
		DBInitTable(&dbtSaCode,"sacode",&sacode,sizeof(sacode));
		DBInitTable(&dbtSysDat,"sysdat",&sysdat,sizeof(sysdat));

		DBInitTable(&dbtAfgCtl,"afgctl",&afgctl,sizeof(afgctl));
		DBInitTable(&dbtAfpCtl,"afpctl",&afpctl,sizeof(afpctl));
		DBInitTable(&dbtAfdCtl,"afdctl",&afdctl,sizeof(afdctl));
		DBInitTable(&dbtActLog,"actlog",&actlog,sizeof(actlog));
		DBInitTable(&dbtSysCod,"syscod",&syscod,sizeof(syscod));
		DBInitTable(&dbtSysPas,"syspas",&syspas,sizeof(syspas));

		DBInitTable(&dbtPrcChg,"prcchg",&prcchg,sizeof(prcchg));
		DBInitTable(&dbtTmpFil,"tmpfil",&tmpfil,sizeof(tmpfil));
	} else {
    printf( "Btrieve B_VERSION Status = %d\n", iDataBaseStatus );
	}
	return (iDataBaseStatus);
}
/***************************************************************************
Function:	 Main Program Logic
Description: Main program logic scanning sockets application function
Passed:		 none
Return:		 1 = Success, 0 = Failed
****************************************************************************/

int near UserID=0;

int GetUserID()
{
	return (UserID);
}


int iMainProgramLogic(void)
{
	long li;
	int i;
	FD_ZERO (&readfds);
	sSelTime.tv_sec = 1; 
	sSelTime.tv_usec = 0;
	/* Always check the main socket for activity */
	FD_SET (sSocket, &readfds);
	/* Check all sInUse sockets for activity. */
	for (i = 0; i < MAX_SOCKETS; i++) {
		if(sInUse[i].uiEndOctet) {
			FD_SET(sInUse[i].sSocketId, &readfds);
			/* Allow for the exceptional condition*/
			li=GetCurrentSeconds();
			if ((sUser[i].lTimeOut > 86400) && (li < lTimeOutValue))
				li += 86400 ;
			if (li > sUser[i].lTimeOut) {
				sUser[i].lCurrentPos = lMenuLogoff(sInUse[i].sSocketId,i);
			}
		}
	}
	/* if there's not any activity, go back to the top of the for loop */
	if(!select(MAX_SOCKETS,&readfds, (fd_set *) 0, (fd_set *) 0,&sSelTime)) {
		return(1);
	}
	/* If you get here, there's some data on a socket to process. */
	/* look for new incomming connections. */
	for(i = 0; i < MAX_SOCKETS; i++) {
		if(FD_ISSET(i,&readfds)) {
			if(i == sSocket) {
				 Make_New(sSocket);
			}
		}
	}
	/* loop through all the sockets with activity and do a read_echo */
	for(i = 0; i < MAX_SOCKETS; i++) {
		/* if the socket id is not zero (i.e. if it in use) */
		/*   AND it has activity. */
		UserID=i;
		if(sInUse[i].sSocketId && FD_ISSET(sInUse[i].sSocketId, &readfds)) {
			/* read & echo to the socket iff it is not the main socket. */
			if(sInUse[i].sSocketId != sSocket) {
				if(sUser[i].fLoggedOn == TRUE) {
					sUser[i].lCurrentPos = lApp(sUser[i].lCurrentPos,sInUse[i].sSocketId,i);
				} else {
					sUser[i].fLoggedOn = fLogonAttempt(sInUse[i].sSocketId);
				}
			}
		}
	}
	return(1);
}
/***************************************************************************
Function:	 Close Routine 									
Description: Close Routine called at End of program
Passed:		 none
Return:		 none
****************************************************************************/
void vCloseRoutine( void ) {									/*W3.07b*/
	
	int i;
	// Close sockets
	for(i = 0; i < MAX_SOCKETS; i++) {
		if(sInUse[i].sSocketId) {
			t_close(sInUse[i].sSocketId);						/*	W1.07	*/
		}
		if(FD_ISSET(sInUse[i].sSocketId, &readfds)) {
			t_close(sInUse[i].sSocketId);						/*	W1.07	*/
		}
	}
	t_close(sSocket);											/* W1.07    */
	fclose(hCommandFile);
	ProcessLogWriteLn("Sockets closed");	
  // Close DB Tables
	DBCloseTable(&dbtStkMas);
	DBCloseTable(&dbtEanMas);
	DBCloseTable(&dbtHhtHdr);
	DBCloseTable(&dbtHhtDet);
	DBCloseTable(&dbtStkLog);
	DBCloseTable(&dbtStkAdj);
	DBCloseTable(&dbtSaCode);
	DBCloseTable(&dbtSysDat);

	DBCloseTable(&dbtAfgCtl);
	DBCloseTable(&dbtAfpCtl);
	DBCloseTable(&dbtAfdCtl);
	DBCloseTable(&dbtActLog);
	DBCloseTable(&dbtSysCod);
	DBCloseTable(&dbtSysPas);

	DBCloseTable(&dbtPrcChg);
	DBCloseTable(&dbtTmpFil);
	
	ProcessLogWriteLn("DB Tables closed");	

	/* STOP BTRIEVE */
  BTRVID(B_STOP,NULL,NULL,NULL,NULL,NULL,&dbUserID);
	ProcessLogWriteLn("DB stopped");	
}
