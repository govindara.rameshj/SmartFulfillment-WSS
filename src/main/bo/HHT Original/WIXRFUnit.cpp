//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "WIXRFUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BTAccess"
#pragma resource "*.dfm"

#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <process.h>
#include <string.h>
#include <stdlib.h>

TfrmWicksRF *frmWicksRF;
//---------------------------------------------------------------------------

__fastcall TfrmWicksRF::TfrmWicksRF(TComponent* Owner)
	: TForm(Owner)
{
}

//---------------------------------------------------------------------------

long GetDBDecFieldToLong(long *RetVar, void *DBField, int DBFieldLength)
{
	int i,precision=DBFieldLength*2-1;
	char *p;
	*RetVar=0;
	p=(char *)(DBField);
	for (i=0 ; i<precision ; i++) {
		*RetVar+=((*p>>((i%2)*4)) & 0x0f)*(10^i);
		if (i%2==1) {	
			p+=1;		// next byte
		}
	}
	if (((*p>>4) & 0x0f)==0x0d) {
		*RetVar*=-1L;
	}
	return (*RetVar);
}

char *GetDBDecFieldToStr(char *RetVar, void *DBField, int DBFieldLength, int Scale)
{
	long value,decimalPt=0;
	int precision=DBFieldLength*2-1;
	char fmtStr[21];

	if (Scale>6) {
		Scale=6;
	}
	if (Scale>0) {
		decimalPt=10L^Scale;
	}

	GetDBDecFieldToLong(&value,DBField,DBFieldLength);
	strcpy(fmtStr,"%");
	itoa(precision,fmtStr+1,10);
	strcat(fmtStr,"ld");
	if (decimalPt) {
		strcat(fmtStr,".%0");
		itoa(Scale,fmtStr+strlen(fmtStr),10);
		strcat(fmtStr,"ld");
		sprintf(RetVar,fmtStr,value/decimalPt,value%decimalPt);
	} else {
		sprintf(RetVar,fmtStr,value);
	}
	return (RetVar);
}

//---------------------------------------------------------------------------

void __fastcall TfrmWicksRF::btnEnterClick(TObject *Sender)
{
	txtResult->Text="";
}
//---------------------------------------------------------------------------

void __fastcall TfrmWicksRF::FormCreate(TObject *Sender)
{

	if (DbStkMas->Open()) {
		ShowMessage("Error Opening File");
	} else {
		ShowMessage("Opened File");
	}


}
//---------------------------------------------------------------------------

