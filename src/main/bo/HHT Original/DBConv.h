//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio 
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Header:DBConv.h - Database conversion routines and other functions added
//									 during the database field conversion
//-----------------------------------------------------------------------------
// W3.08 GS - 11.01.05   - AL1129  Changes to Support .dta files

// Date Fields

typedef struct DBLongDate
	{
		char day;
		char month;
		unsigned short int year;
	} DBLongDate;

// DBCONV.C Global Function Prototypes
void *DefaultRetValue(void *StackRetParam, int bufferSize);
DBLongDate *GetCurrentDateInDBLongDateFormat(DBLongDate *RetVar);
char *GetCurrentDateTimeMsg(char *RetVar);
char *GetCurrentDateInDDMMYY(char *RetVar);
char *GetCurrentDateInDDMMYYYY(char *RetVar);
char *GetCurrentTimeStr(char *RetVar);
void GetDBLongDateToDBField(void *DBField, DBLongDate *date);
void GetDBFieldToDBLongDate(DBLongDate *date, void *DBField);
char *GetDBLongDateFieldToStr(char *RetVar, DBLongDate *DBDate);
int DaysInMonth(int month, int year);
DBLongDate *GetStrToDBLongDateField(DBLongDate *retDate, char *Value);
int DBLongDateCompare(DBLongDate *d1, DBLongDate *d2);
unsigned long GetCurrentSeconds(void) 	;
unsigned long DBLongDateToDayNumber(DBLongDate *date);
unsigned long DateStrToDayNumber(char *DateStr);
char DayNumberToDBLongDate(DBLongDate *Date, long DayNumber);
char DayNumberToDateStr(char *RetVar, long DayNumber);

/*
char *GetDBCharFieldToStr(char *RetVar, void *DBField, int DBFieldLength);
char *GetStrToDBCharField(void *DBFieldRet, char *Str, int DBFieldLength);
*/

void StripSpaces(char *RetVar, char *Field);
char *ZeroPadField(char *RetVar, char *Field, int length);
/*
char *DBstrncpy(char *Dest, char *Source, int SizeOfDest);
char *GetDBLogicalFieldToStr(char *RetVar, void *DBField);
void SetDBLogicalField(char *DBField, char Value);
void GetDBIdentityField(void *RetVar, void *DBField);
long GetDBDecFieldToLong(char *DBField, short DBFieldLength);
void *GetLongToDBDecField(void *DBFieldRet, signed long Value, int DBFieldLength);
void *GetStrToDBDecField(unsigned char *DBFieldRet, char *Value, short DBFieldLength, char Decimal);
char *GetDBDecFieldToStr(char *RetVar, unsigned char *Data, short Size, char Decimal);
*/

// Character Fields

// Character field to String
//char *GetDBCharFieldToStr(char *RetVar, void *DBField, int DBFieldLength);
// String to Char field
//char *GetStrToDBCharField(void *DBFieldRet, char *Str, int DBFieldLength);
// Copies field to RetVar right justified and left padded with zeros
char *ZeroPadField(char *RetVar, char *Field, int length);
/*
// Copies ASCIIZ strings safely, pads with spaces and null terminates
char *DBstrncpy(char *Dest, char *Source, int SizeOfDest);

// Logical Fields

// Logical field to string
char *GetDBLogicalFieldToStr(char *RetVar, void *DBField);

// Perv ID 
void GetDBIdentityField(void *RetVar, void *DBField);

// Decimal/Numeric Fields

// BCD field to Long
long GetDBDecFieldToLong(char *DBField, short DBFieldLength);
// Long to BCD field
void *GetLongToDBDecField(void *DBFieldRet, signed long Value, int DBFieldLength);
// String to BCD field
void *GetStrToDBDecField(unsigned char *DBFieldRet, char *Value, short DBFieldLength, char Decimal);
// BCD field to String
char *GetDBDecFieldToStr(char *RetVar, unsigned char *Data, short Size, char Decimal);
*/
// Logging

#define ProcessLogNone		0
#define ProcessLogConsole 1
#define ProcessLogFile		2

int ProcessLogInitialize(void);
int ProcessLogDevice(void);
FILE *ProcessLogHandle(void);
void ProcessLogFlushBuffer(void);
void ProcessLogStartToFile(int append);
void ProcessLogStartToConsole(void);
void ProcessLogStop(void);
int ProcessLogWriteTimeStamp(char *msg);
int ProcessLogWrite(char *msg);
int ProcessLogWriteLong(long li);
int ProcessLogWriteLn(char *msg);
int ProcessLogDumpBuffer(char *msg, void *buffer, int length);
/*
char *DBBufferToPrintableStr(char *RetVar, void *buffer, int length, int toHex);
char *DBBufferToHexStr(char *RetVar, void *buffer, int length);
char *DBBufferAddressToString(char *RetVar, void *buffer);
*/

// Database Routines

/*
typedef struct DataBaseTable {

	char					Name[64];				// Table name
	BTI_BYTE			Params[128];		// Btrieve work area
	int						Opened;					// Count of how many times the table has been opened
	int						OpenStatus;			// Btrieve last result code
	BTI_WORD			DataLength;			// Length of table record
	BTI_VOID_PTR	DataBuffer;			// Table record

} DataBaseTable;

BTI_SINT DBInitTable(DataBaseTable *TableInfo, char *TableName, BTI_VOID_PTR DataBuffer, int DataBufferLength);
BTI_SINT DBOpenTable(DataBaseTable *TableInfo, int UserIndex);
void DBClearAccessKeyBuffer(void);
void *DBGetAccessKeyBufferPtr(void);
BTI_SINT DBAccessTableData(DataBaseTable *TableInfo, BTI_SINT BtrieveCmd, BTI_VOID_PTR keyBuf, BTI_SINT keyNum);
BTI_SINT DBCloseTable(DataBaseTable *TableInfo);
*/
