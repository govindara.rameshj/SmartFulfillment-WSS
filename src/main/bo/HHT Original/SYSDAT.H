//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio 
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Header:SysDat.h - SYSDAT table definition
//-----------------------------------------------------------------------------
typedef struct
   {
	  //BTI_CHAR fkey[ 1 ]; /* C2   Record Number  (01)                 */
	  BTI_CHAR fkey[2];		/* char 2 Record Number  (01)                 */
	  //BTI_CHAR days[ 1 ]; /* N1   Number of days store is Open        */
	  BTI_CHAR days[1];		/* Dec 1.0 Number of days store is Open        */
	  //BTI_CHAR wend[ 1 ];	/* N1   Week Ending Day of Week 1-7         */
	  BTI_CHAR wend[1];		/* Dec 1.0 Week Ending Day of Week 1-7         */
	  //BTI_CHAR open[ 14 ];/* N2(O=14)   Days Open Last 14 periods     */
	  BTI_CHAR open[2*14];	/* Dec 3.0*14   Days Open Last 14 periods     */
	  //BTI_CHAR todt[ 2 ]; /* D    Today's date              		    */
	  BTI_CHAR todt[4];		/* Date    Today's date              		    */
	  //BTI_CHAR todw[ 1 ]; /* N1   Today's day of Week Code 1-7	    */
	  BTI_CHAR todw[1];		/* Dec 1.0 Today's day of Week Code 1-7	    */
	  //BTI_CHAR tmdt[ 2 ]; /* D    Tomorrows Date              	    */
	  BTI_CHAR tmdt[4];		/* Date  Tomorrows Date              	    */
	  //BTI_CHAR tmdw[ 1 ]; /* N1   Tomorrows day of Week Code 1-7	    */
	  BTI_CHAR tmdw[1];		/* Dec 1.0  Tomorrows day of Week Code 1-7	    */
	  BTI_CHAR wkdt[4];		/* D    One Week From Today           	    */
	  //BTI_CHAR wk13[ 26 ];/* D(O=13) Last 13 Week End Dates  		    */
	  BTI_CHAR wk13[4*13];	/* D(O=13) Last 13 Week End Dates  		    */
	  //BTI_CHAR live[ 2 ]; /* D    Store Live Date                     */
	  BTI_CHAR live[4];		/* D    Store Live Date                     */
/*    Period End Dates      											*/
	  //BTI_CHAR pset[ 1 ];	/* N1   1-> Use Set 1 ; 2->Use Set 2        */
	  BTI_CHAR pset[1];		/* Dec 1.0 1-> Use Set 1 ; 2->Use Set 2        */
	  //BTI_CHAR s1dt[ 28 ];/* D(O=14) Set 1 (14 Period End Dates)      */
	  BTI_CHAR s1dt[4*14];	/* D(O=14) Set 1 (14 Period End Dates)      */
	  //BTI_CHAR s1yr[ 14 ];/* I(O=14) On->Year End, Else Period End    */
	  BTI_CHAR s1yr[14]; /* I(O=14) On->Year End, Else Period End    */
	  //BTI_CHAR s1pf[ 14 ];/* I(O=14) On->Processed, Else Due for Proc */
	  BTI_CHAR s1pf[14]; /* I(O=14) On->Processed, Else Due for Proc */
	  //BTI_CHAR s2dt[ 28 ];/* D(O=14) Set 2 (14 Period End Dates)      */
	  BTI_CHAR s2dt[4*14];	/* D(O=14) Set 2 (14 Period End Dates)      */
	  //BTI_CHAR s2yr[ 14 ];/* I(O=14) On->Year End, Else Period End    */
	  BTI_CHAR s2yr[14]; /* I(O=14) On->Year End, Else Period End    */
	  //BTI_CHAR s2pf[ 14 ];/* I(O=14) On->Processed, Else Due for Proc */
	  BTI_CHAR s2pf[ 14 ];/* I(O=14) On->Processed, Else Due for Proc */
/*    Current Period / Week Flags										*/
	  //BTI_CHAR tidt[ 2 ]; /* D This Current Period Year End date      */
	  BTI_CHAR tidt[4];		/* D This Current Period Year End date      */
	  //BTI_CHAR ladt[ 2 ]; /* D Prior Period Year End Date             */
	  BTI_CHAR ladt[4];		/* D Prior Period Year End Date             */
	  BTI_CHAR tiyr[1];		/* I On = Current End is a Year end         */
	  BTI_CHAR pper[ 1 ]; /* I On = Period End Detected Not Processed */
	  BTI_CHAR pwek[ 1 ]; /* I On = Week End Detected Not Processed   */
/*    JSNITE and retry Control Area										*/
	  BTI_CHAR nset[ 1 ]; /* C1 Net Set Number                        */
	  //BTI_CHAR nite[ 2 ]; /* C3 JSNITE Step Number 000  Completed OK  */
	  BTI_CHAR nite[3];		/* C3 JSNITE Step Number 000  Completed OK  */
	  BTI_CHAR rety[ 1 ]; /* I  On = retry mode                       */
/*    Supplier Cycle count Fields  										*/
	  //BTI_CHAR cyno[ 1 ]; /* N2 Number of Weeks in the cycle          */
	  BTI_CHAR cyno[2];		/* N2 Number of Weeks in the cycle          */
	  //BTI_CHAR cycw[ 1 ];	/* N2 Current Week number in the cycle      */
	  BTI_CHAR cycw[2];		/* N2 Current Week number in the cycle      */
	  //BTI_CHAR audt[ 2 ]; /* D  Audit date                            */
	  BTI_CHAR audt[4];		/* D  Audit date                            */
/*    Fields for WIX632 (JDA) promotions								*/
	  //BTI_CHAR dpca[ 1 ]; /* N2 Days Prior to Price Change Effective */
	  BTI_CHAR dpca[2];		/* N2 Days Prior to Price Change Effective */
							/*    Date when Price change can be applied.*/
/*    Fields for WIX813 (NITLOG)        								*/
	  //BTI_CHAR ldat[ 2 ]; /* D  Date Upon which the last nightly close*/
	  BTI_CHAR ldat[4];		/* D  Date Upon which the last nightly close*/
							/*    was updated                           */
/*    filler					           								*/
	  BTI_CHAR spare[ 96 ];	/* A96 			                        */
   } SYSDAT;
