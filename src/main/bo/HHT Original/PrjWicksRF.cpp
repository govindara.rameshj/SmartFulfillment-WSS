//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("PrjWicksRF.res");
USEFORM("WIXRFUnit.cpp", frmWicksRF);
USEUNIT("BTAccess.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TfrmWicksRF), &frmWicksRF);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	return 0;
}
//---------------------------------------------------------------------------
