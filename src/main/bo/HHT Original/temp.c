void *GetLongToDBDecField(void *DBFieldRet, signed long Value, int DBFieldLength)
{
	int index=0;
	long rem;
	char *p;
	p=DBConvTempStr+DBFieldLength-1;
	rem=Value;
	printf("\n *p=");
	while (index<DBFieldLength) {
		*p=rem%10;
		rem/=10;
		*p+=(rem%10)<<4;
		rem/=10;
		index++;
		printf("%02X ",*p);
		p--;
  }
	printf("\n");
	p++;
	*p=*p & 0x0f;
	if (Value<0) {
		*p+=0xd0;
	} else {
		*p+=0xf0;
	}
	if (DBFieldRet) {
		memcpy(DBFieldRet,DBConvTempStr,DBFieldLength);
	}
	return (DBConvTempStr);
}
