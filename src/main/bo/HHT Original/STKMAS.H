//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio 
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Header:StkMas.h - STKMAS table definition
//-----------------------------------------------------------------------------
//     W17 - 25.7.00 - JR - WIX941 (Based on STKMAS version W17 )       
//     W05 - 14.5.98 - JR - WIX651 (Based on STKMAS version W05 )       

   typedef struct
   {
    BTI_CHAR skun[6];				// char 6	SKU Number              
	  BTI_CHAR plud[ 20 ];		// A20   	Short Desc              
	  BTI_CHAR desc[ 40 ];		// A40		Long Desc				
	  BTI_CHAR fill[ 10 ];		// A10		Alpha descr				
	  BTI_CHAR dept[2];				// char 2	Department				
	  BTI_CHAR grou[3];				// char 3	Group					
	  BTI_CHAR vatc[ 1  ];		// A1 		VAT Code				
	  BTI_CHAR buyu[ 4  ];		// A4 		Buying UOM Code			
//    Supplier information  											
	  BTI_CHAR supp[5];				// char 5	Supplier       			
	  BTI_CHAR prod[ 10 ];		// A10 		Product Code   			
	  BTI_CHAR pack[3];				// dbl 5.0	items/Pack     			
//    Prices/costs														
	  BTI_CHAR pric[5];				// dbl 9.2	Price          			
	  BTI_CHAR ppri[5];				// dbl 9.2	Prior Price    			
	  BTI_CHAR cost[6];				// dbl 11.4	Cost           			
//    Date Information													
	  DBLongDate dsol;				// D		Last sale      			
	  DBLongDate drec;				// D		Last receipt   			
	  DBLongDate dord;				// D		Last order     			
	  DBLongDate dprc;				// D		Last price change		
	  DBLongDate dset;				// D		Last Setup     			
	  DBLongDate dobs;				// D		Obsolete date  			
	  DBLongDate ddel;				// D		Deleted  date  			
//    Quantities            											
	  BTI_CHAR onha[4];				// dbl 7.0	Stock on Hand 			
	  BTI_CHAR onor[4];				// dbl 7.0	On Order       			
	  BTI_CHAR mini[4];				// dbl 7.0	On Order       			
	  BTI_CHAR maxi[4];				// dbl 7.0	On Order       			
	  BTI_CHAR sold[4];				// dbl 7.0	Sold Today     			
//    Status codes / indicators											
	  BTI_CHAR ista[1];				// A1		Item Status code		
	  BTI_CHAR iris[1];				// Logical	Related item single		
	  BTI_CHAR irib[1];				// Dec 1.0	No of times Related Bulk
	  BTI_CHAR imdn[1];				// Logical	Markdown Item           
	  BTI_CHAR icat[1];				// Logical	Catch all               
	  BTI_CHAR iobs[1];				// Logical	Obsolete                
	  BTI_CHAR idel[1];				// Logical	Deleted                 
	  
	  BTI_CHAR iloc[1];				// Dec 1.0	No of alt locations     
	  BTI_CHAR iean[1];				// Dec 1.0	No of EAN records       
	  BTI_CHAR ippc[1];				// Dec 1.0	No of price changes     
	  
	  BTI_CHAR itag[1];				// Logical	tagged item   		
	  BTI_CHAR inon[1];				// Logical	Non stocked             
//    Sales Stats                 										
	  BTI_CHAR salv[5*7];			// Dec 9.2*7 Sales Value 
	  BTI_CHAR salu[4*7];			// Dec 7.0*7 Sales Units 
//    Days Out of stock		     									
	  BTI_CHAR cpdo[2];				// Dec 3.0	Current Period         	
	  BTI_CHAR cydo[2];				// Dec 3.0	Current Year            		
//    Anticipated Weekly sales											
	  BTI_CHAR awsf[5];				// Dec 9.2	4 Weeks calc		
	  BTI_CHAR aw13[5];				// Dec 9.2	13 weeks calc		
//    13 weeks sales data											
	  BTI_CHAR dats[4];				// D					
	  BTI_CHAR uwek[2];				// Dec 3.0				
	  BTI_CHAR flag[1];				// Logical				
	  BTI_CHAR cflg[1];				// Logical				
	  BTI_CHAR us00[4*14];		// Dec 7.0*14 
	  BTI_CHAR do00[2*14];		// USmallInt*14 
//    SOQ fields  				           								
	  BTI_CHAR sq04[4];				// Dec 7.0           
	  BTI_CHAR sq13[4];				// Dec 7.0           
	  BTI_CHAR sq01[4];				// Dec 7.0           
	  BTI_CHAR sqor[1];				// Logical           
//    daily activity fields		           								
	  BTI_CHAR treq[4];				// Dec 7.0                          
	  BTI_CHAR trev[5];				// Dec 9.2	                        
	  BTI_CHAR tact[1];				// Logical                          
//    Supplier returns			           								
	  BTI_CHAR retq[4];				// Dec 7.0                          
	  BTI_CHAR retv[5];				// Dec 9.2                          
//    Stock movements			           								
	  BTI_CHAR mspr[5*4];			// Dec 9.2*4    
	  BTI_CHAR mstq[4*4];			// Dec 7.0*4    
	  BTI_CHAR mreq[4*4];			// Dec 7.0*4    
	  BTI_CHAR mrev[5*4];			// Dec 9.2*4    
	  BTI_CHAR madq[4*4];			// Dec 7.0*4    
	  BTI_CHAR madv[5*4];			// Dec 9.2*4    
	  BTI_CHAR mpvv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mibq[4*4];			// Dec 7.0*4     
	  BTI_CHAR mibv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mrtq[4*4];			// Dec 7.0*4     
	  BTI_CHAR mrtv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mccv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mdrv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mbsq[4*4];			// Dec 7.0*4     
	  BTI_CHAR mbsv[5*4];			// Dec 9.2*4     
	  BTI_CHAR mstp[4*4];			// Dec 7.0*4     
	  BTI_CHAR fil1[5*4];			// Dec 9.2*4     
//    Client specific fields		     					    
	  BTI_CHAR chkd[1];				// char 1                           
	  BTI_CHAR labn[2];				// Dec 3.0    
	  BTI_CHAR labs[1];				// Char 1                           
	  BTI_CHAR ston[6];				// char 6                           
	  BTI_CHAR stoq[4];				// Dec 7.0                          
	  BTI_CHAR sod1[1];				// char 1                           
	  BTI_CHAR sod2[1];				// char 1                           
	  BTI_CHAR sod3[1];				// char 1                           
	  BTI_CHAR labm[2];				// Dec 3.0    
	  BTI_CHAR labl[2];				// Dec 3.0    
//    SOQ Carriage details		          				    
	  BTI_CHAR wght[4];				// Dec 7.2    
	  BTI_CHAR volu[4];				// Dec 7.2    
	  BTI_CHAR noor[1];				// Logical                          
//    PSS System				           								
	  BTI_CHAR sup1[5];				// char 5                           
	  BTI_CHAR iodt[4];				// Date                             
	  BTI_CHAR fodt[4];				// Date                                
	  BTI_CHAR pmin[3];				// Dec 5.0                          
	  BTI_CHAR pmsd[4];				// Date                                
	  BTI_CHAR pmed[4];				// Date                                
	  BTI_CHAR pmcp[1];				// char 1                           
	  BTI_CHAR sman[3];				// Dec 5.0                          
	  BTI_CHAR dflc[4];				// D                                
	  BTI_CHAR imcp[1];				// Logical                          
	  BTI_CHAR folt[3];				// Dec 5.0                          
	  BTI_CHAR idea[3];				// Dec 5.0                          
	  BTI_CHAR qadj[3];				// Dec 5.2                          
	  BTI_CHAR tagf[ 1 ];			// A1                               
	  BTI_CHAR alph[ 20];			// A20                              
//    Hierarchy Fields      		           				    
	  BTI_CHAR ctgy[6];				// char 6 Hierarchy Category Level 5   		
	  BTI_CHAR grup[6];				// char 6 Hierarchy Category Level 4   		
	  BTI_CHAR sgrp[6];				// char 6 Hierarchy Category Level 3   		
	  BTI_CHAR styl[6];				// char 6 Hierarchy Category Level 2   		
	  BTI_CHAR equv[ 40 ];			// T40 Product Equivalent Desc.     		
	  BTI_CHAR hfil[12];			// char 12 Hierarchy Cat. & Group       		
//    Retail Price fields   		           				    
	  BTI_CHAR revt[6];				// char 6 Retail Price Event Number    		
	  BTI_CHAR rpri[2];				// char 2 Retail Price Priority        		
	  BTI_CHAR iwar[1];				// Logical  Warranty Item                		
	  BTI_CHAR ioff[2];				// Dec 3.0  Weapon Age Restriction  
	  BTI_CHAR isol[2];				// Dec 3.0  Solvent Age Restriction      		
	  BTI_CHAR quar[1];				// char 1 Quarantine Flag              		
	                        		//     Q-> Quarantine B->Batch Qua. 		
	  BTI_CHAR iprd[1];				// Logical Y-> Price discrepancy        		
	  BTI_CHAR eqpm[6];				// Dec 11.6  Equivalent Price Mult.    		
	  BTI_CHAR eqpu[ 7 ];			// A7     Equivalent Price Unit.    		
	  BTI_CHAR mdnq[4];				// Dec 7.0 Mark Down Quantity           		
	  BTI_CHAR wtfq[4];				// Dec 7.0  Write Off Stock              		
	  BTI_CHAR salt[1];				// A1  Sale Type Attribute          		
	  BTI_CHAR modt[1];				// A1  Model Type Attribute         		
	  BTI_CHAR aapc[1];				// Logical Y-> Auto Apply Price Changes 
	  BTI_CHAR ipsk[1];				// Logical  Y-> Palleted SKU             
	  BTI_CHAR aadj[1];				// A1  " " = Don't Allow Type 58/59 Adjs.	
                     				//     "1" = Allow Type58 Adjustments		
                     				//     "2" = Allow Type59 Adjustments		
                     				//	   "3" = Allow Type58/59 Adjustments	
	  BTI_CHAR sup2[5];
	  BTI_CHAR spare[58];
   } STKMAS;

