//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <memory>
#include <stdio.h>

#include "BTAccess.h"
#include "BTAccessEdit.h"
#include "BtrAPI.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//



static inline void ValidCtrCheck(TBTAccess *)
{
		new TBTAccess(NULL);
}
//---------------------------------------------------------------------------
__fastcall TBTAccess::TBTAccess(TComponent* Owner)
		: TComponent(Owner)
{
	int Status;
	AnsiString StatMsg;
	unsigned char TempID[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,'J','G',0,1 };
	ClientID = TempID;
	try {
		Status = WBRQSHELLINIT( "PCI, Inc." );
	} catch (Exception &E) {
		ShowMessage(E.Message);
		return;
	}
	Fields = NULL;
	Indexes = NULL;
	DataBlock = NULL;
	FileOpen = false;
	OKToSet = false;
	FLocale = "US";
	if (Status == 0) {
		BTInitialized = true;
	}	else {
		if (Status == B_ALREADY_INITIALIZED) {
			BTInitialized = true;
		} else {
			StatMsg = "Btrieve Initialization Failure: ";
			StatMsg += Status;
			ShowMessage(StatMsg);
			return;
		}
	}
}
//---------------------------------------------------------------------------
namespace Btaccess
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TBTAccess)};
		RegisterComponents("JMG", classes, 0);
		RegisterComponentEditor(classes[0], __classid(TBTAccessEditor));
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetDDFPath(AnsiString NewPath)
{
	if (NewPath == "") {
		FDDFPath = "";
		FFileName = "";
		ClearTables();
		return;
	}
	if (!FileExists(NewPath + "\\file.ddf")) {
		ShowMessage(AnsiString("Invalid DDF Path assigned to ") + Name + " - Missing File.DDF");
		FFileName = "";
		ClearTables();
		return;
	}

	if (!FileExists(NewPath + "\\field.ddf")) {
		ShowMessage(AnsiString("Invalid DDF Path assigned to ") + Name + " - Missing Field.DDF");
		FFileName = "";
		ClearTables();
		return;
	}
	if (!FileExists(NewPath + "\\index.ddf")) {
		ShowMessage(AnsiString("Invalid DDF Path assigned to ") + Name + " - Missing Index.DDF");
		FFileName = "";
		ClearTables();
		return;
	}
	if (FDDFPath == NewPath) {
		return;
	}

	FDDFPath = NewPath;

	FFileID = 0;
	if (FFileName != "") {
		SetFileInformation();
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFileName(AnsiString NewName)
{
	if (NewName == "") {
		FFileName = "";
		free(Fields);
		free(Indexes);
		Fields = NULL;
		Indexes = NULL;
	} else {
		if (FFileName.Trim().LowerCase() != NewName.Trim().LowerCase()) {
			FFileName = NewName;
			SetFileInformation();
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFileInformation()
{
	int Status;
	AnsiString StatMsg;
	unsigned long DL;
	char KeyBuf[255];
	char WkFileName[21];
	AnsiString WkName;
	DDFFileLayout DataBuf;

	if (!(OKToSet)) {
		return;
	}

	DL = 0;
	Status = BTRCALLID(B_OPEN, PosBlock, &DataBuf, &DL, (FDDFPath + "\\File.ddf").c_str(), (unsigned char)255, 1, ClientID);
	if (Status) {
		StatMsg = "Btrieve Open Failure: ";
		StatMsg += Status;
		StatMsg += " - File.ddf";
		StatMsg += FDDFPath;
		StatMsg +=  "\\File.ddf";
		ShowMessage(StatMsg);
		return;
	}
	WkFileName[20] = '\0';

	DL = sizeof(DataBuf);
	memset(KeyBuf, 0, MAX_KEY_SIZE);
	strncpy(KeyBuf, FFileName.c_str(), 20);
	Status = BTRCALLID(B_GET_GE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 1, ClientID);
	if (Status == 0) {
		memcpy(WkFileName, DataBuf.Name, 20);
		if (AnsiString(WkFileName).Trim().LowerCase() == FFileName.Trim().LowerCase()) {
			FFileID = DataBuf.ID;
			memcpy(FileLocation, DataBuf.Location, 64);
			for (int Count = 63; Count >= 0; Count--) {
				if (FileLocation[Count] == ' ') {
					FileLocation[Count] = '\0';
					continue;
				}
				break;
			}
		} else {
			StatMsg = "Invalid file name specified: ";
			StatMsg += FFileName;
			ShowMessage(StatMsg);
			FFileName = "";
		}
	}
	BTRCALLID(B_CLOSE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 0, ClientID);
	if (FFileID != 0) {
		SetFields();
		SetIndexes();
	} else {
		if (FFileName != "") {
			StatMsg = "Invalid file name specified: ";
			StatMsg += FFileName;
			ShowMessage(StatMsg);
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFields()
{
	int Status;
	AnsiString StatMsg;
	unsigned long DL;
	char KeyBuf[255];
	AnsiString WkName;
	FieldLayout DataBuf;

	FieldCount = 0;
	if (!(OKToSet)) {
		return;
	}

	if (Fields != NULL) {
		free(Fields);
		Fields = NULL;
	}
	DL = 0;
	Status = BTRCALLID(B_OPEN, PosBlock, &DataBuf, &DL, (FDDFPath + "\\Field.ddf").c_str(), (unsigned char)255, 1, ClientID);

	if (Status) {
		StatMsg = "Btrieve Open Failure: ";
		StatMsg += Status;
		ShowMessage(StatMsg);
		return;
	}
	DL = sizeof(DataBuf);

	*(short int *)KeyBuf = FFileID;
	Status = BTRCALLID(B_GET_EQUAL, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 1, ClientID);
	if (Status == 0) {
		do {
			FieldCount++;
			Fields = (FieldLayout *)std::realloc(Fields, DL * FieldCount);
			if (Fields == NULL) {
				ShowMessage("realloc for Fields failed!");
				BTRCALLID(B_CLOSE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 1, ClientID);
				return;
			}
			if (FieldCount == 1) {
				FieldOffset = DataBuf.ID;
			}
			for (int Count = 19; Count >= 0; Count--) {
				if (DataBuf.Name[Count] == ' ') {
					DataBuf.Name[Count] = '\0';
					continue;
				}
				break;
			}
			DataBuf.ID -= FieldOffset;
			Fields[FieldCount - 1] = DataBuf;
			Application->ProcessMessages();
			Status = BTRCALLID(B_GET_NEXT, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 1, ClientID);
		} while((Status == 0) && (*(short int *)KeyBuf == FFileID));
	}
	BTRCALLID(B_CLOSE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 1, ClientID);

	DataLength = 0;
	for (int Count = 0; Count < FieldCount; Count++) {
		DataLength += Fields[Count].Size;
	}
	DataBlock = (unsigned char *)std::realloc(DataBlock, DataLength);
	if (DataBlock == NULL) {
		throw Exception(AnsiString("Memory allocation for ") + Name + " failed!");
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetIndexes()
{
	int Status;
	AnsiString StatMsg;
	unsigned long DL;
	char KeyBuf[255];
	AnsiString WkName;
	IndexLayout DataBuf;

	IndexCount = 0;
	if (!(OKToSet)) {
		return;
	}

	if (Indexes != NULL) {
		free(Indexes);
		Indexes = NULL;
	}
	DL = 0;
	Status = BTRCALLID(B_OPEN, PosBlock, &DataBuf, &DL, (FDDFPath + "\\Index.ddf").c_str(), (unsigned char)255, 2, ClientID);

	if (Status) {
		StatMsg = "Btrieve Open Failure: ";
		StatMsg += Status;
		ShowMessage(StatMsg);
		return;
	}
	DL = sizeof(DataBuf);

//	memset(KeyBuf, 0, 255);
	*(short int *)KeyBuf = FFileID;		// File ID
	*(short int *)(KeyBuf + 2) = 0;		// Index Number
	*(short int *)(KeyBuf + 4) = 0;		// Index Segment
	Status = BTRCALLID(B_GET_GE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 2, ClientID);
	if ((Status == 0) && (*(short int *)KeyBuf == FFileID)) {
		do {
			IndexCount++;
			Indexes = (IndexLayout *)std::realloc(Indexes, DL * IndexCount);
			if (Indexes == NULL) {
				ShowMessage("realloc for Indexes failed!");
				BTRCALLID(B_CLOSE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 2, ClientID);
				return;
			}
			DataBuf.Field -= FieldOffset;
			Indexes[IndexCount - 1] = DataBuf;
			Application->ProcessMessages();
			Status = BTRCALLID(B_GET_NEXT, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 2, ClientID);
		} while((Status == 0) && (*(short int *)KeyBuf == FFileID));
	}
	BTRCALLID(B_CLOSE, PosBlock, &DataBuf, &DL, KeyBuf, (unsigned char)255, 2, ClientID);
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetLocale(AnsiString NewLocale)
{
	if ((NewLocale != "US") &&
			(NewLocale != "UK")) {
		throw Exception(AnsiString("Locale - ") + NewLocale + " - is neither US or UK!");
	} else {
		FLocale = NewLocale;
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::Loaded()
{
	TComponent::Loaded();
	OKToSet = true;
	if (FDDFPath == "") {
		FFileName = "";
	}
	if (FFileName != "") {
		SetFileInformation();
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::ClearTables()
{
	if (Fields != NULL) {
		free(Fields);
		Fields = NULL;
	}
	if (Indexes != NULL) {
		free(Indexes);
		Indexes = NULL;
	}
	if (DataBlock != NULL) {
		free(DataBlock);
		DataBlock = NULL;
	}
	if (FileOpen) {
		Close();
	}
}
//---------------------------------------------------------------------------
__fastcall TBTAccess::~TBTAccess(void)
{
	ClearTables();
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::Clear(void)
{
	memset(DataBlock, 0, DataLength);
	for (int Count = 0; Count < FieldCount; Count++) {
		if (Fields[Count].Type == DECIMAL_TYPE) {
			FieldValue[Count] = 0;
		}
	}
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::Open(void)
{
	unsigned long DL;
	char WkPath[255];

	if ((FDDFPath == "") ||
			(FFileName == "")) {
		return B_FILE_NOT_FOUND;
	}
	strcpy(WkPath, FDDFPath.c_str());
	strcat(WkPath, "\\");
	DL = strlen(WkPath);
	strncpy(WkPath + DL, FileLocation, 64);
	WkPath[64 + DL] = '\0';
	FLastError = BTRCALLID(B_OPEN, PosBlock, NULL, 0, WkPath, MAX_KEY_SIZE, FIndex, ClientID);
	if (FLastError == 0) {
		FileOpen = true;
	} else {
		ShowMessage(AnsiString("Error opening ") + WkPath + " - " + FLastError);
	}
	return FLastError;
}
//---------------------------------------------------------------------------
short int __fastcall TBTAccess::Close(void)
{
	FileOpen = false;
	FLastError = BTRCALLID(B_CLOSE, PosBlock, NULL, 0, 0, 0, 0, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short int __fastcall TBTAccess::GetFirst(unsigned short LockBias)
{
	FLastError = BTRCALLID((unsigned short)(B_GET_FIRST + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetNext(unsigned short LockBias)
{
	FLastError = BTRCALLID((unsigned short)(B_GET_NEXT + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::Insert(void)
{
	FLastError = BTRCALLID(B_INSERT, PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::Update(void)
{
	FLastError = BTRCALLID(B_UPDATE, PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::Delete(void)
{
	FLastError = BTRCALLID(B_DELETE, PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetPosition(void)
{
	unsigned long WkLength = 4;
	FLastError = BTRCALLID(B_GET_POSITION, PosBlock, &FPosition, &WkLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::Unlock(short LockType)
{
	if (LockType != 0) {
		throw Exception(AnsiString("Only single record unlock supported - ") + Name + " - " + LockType);
	}
	FLastError = BTRCALLID(B_UNLOCK, PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetLast(unsigned short LockBias)
{
	FLastError = BTRCALLID((unsigned short)(B_GET_LAST + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetPrevious(unsigned short LockBias)
{
	FLastError = BTRCALLID((unsigned short)(B_GET_PREVIOUS + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetEqual(unsigned short LockBias)
{
	SetupIndex();
	FLastError = BTRCALLID((unsigned short)(B_GET_EQUAL + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetGreater(unsigned short LockBias)
{
	SetupIndex();
	FLastError = BTRCALLID((unsigned short)(B_GET_GT + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetGreaterOrEqual(unsigned short LockBias)
{
	SetupIndex();
	FLastError = BTRCALLID((unsigned short)(B_GET_GE + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetLess(unsigned short LockBias)
{
	SetupIndex();
	FLastError = BTRCALLID((unsigned short)(B_GET_LT + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetLessOrEqual(unsigned short LockBias)
{
	SetupIndex();
	FLastError = BTRCALLID((unsigned short)(B_GET_LE + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::GetDirect(unsigned short LockBias)
{
	*(int *)DataBlock = FPosition;
	FLastError = BTRCALLID((unsigned short)(B_GET_DIRECT + LockBias), PosBlock, DataBlock, &DataLength, KeyBuffer, MAX_KEY_SIZE, FIndex, ClientID);
	return FLastError;
}
//---------------------------------------------------------------------------
short __fastcall TBTAccess::RetrieveIndex(const Variant Index)
{
	AnsiString WkIndex;
	switch (Index.Type() & varTypeMask) {
		case varEmpty    :  throw Exception(AnsiString("No field specified - ") + Name + " - " + Index);
		case varNull     :  throw Exception(AnsiString("Null field specified - ") + Name + " - " + Index);
		case varSmallint :  return Index;
		case varInteger  :	return Index;
		case varSingle   :  throw Exception(AnsiString("SP float field index not supported - ") + Name + " - " + Index);
		case varDouble   :  throw Exception(AnsiString("DP float field index not supported - ") + Name + " - " + Index);
		case varCurrency :  throw Exception(AnsiString("Currency field index not supported - ") + Name + " - " + Index);
		case varDate     :  throw Exception(AnsiString("Date field index not supported - ") + Name + " - " + Index);
		case varOleStr   :  throw Exception(AnsiString("Ole Str field index not supported - ") + Name + " - " + Index);
		case varDispatch :  throw Exception(AnsiString("Dispatch field index not supported - ") + Name + " - " + Index);
		case varError    :  throw Exception(AnsiString("Error field index not supported - ") + Name + " - " + Index);
		case varBoolean  :  throw Exception(AnsiString("Boolean field index not supported - ") + Name + " - " + Index);
		case varVariant  :  throw Exception(AnsiString("Variant field index not supported - ") + Name + " - " + Index);
		case varUnknown  :  throw Exception(AnsiString("Unknown field index specifier - ") + Name + " - " + Index);
		case varByte     :  throw Exception(AnsiString("Byte field index not supported - ") + Name + " - " + Index);
		case varString   :  {
													short Count;
													WkIndex = Index;
													for (Count = 0; Count < FieldCount; Count++) {
														if (WkIndex.LowerCase() == AnsiString(Fields[Count].Name).LowerCase()) return Count;
													}
													throw Exception(AnsiString("Field name not found - ") + Name + " - " + Index);
												}
												//break;
	}
	return -1;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TBTAccess::GetFieldValue(const Variant Index)
{
	AnsiString RetValue;
	FieldIndex = RetrieveIndex(Index);

	switch (Fields[FFieldIndex].Type) {
		case STRING_TYPE : return ExtractString(DataBlock + Fields[FFieldIndex].Offset, Fields[FFieldIndex].Size);

		case INTEGER_TYPE : throw Exception(AnsiString("INTEGER_TYPE field type not supported - ") + Name + " - " + Index);
		case IEEE_TYPE : throw Exception(AnsiString("IEEE_TYPE field type not supported - ") + Name + " - " + Index);

		case DATE_TYPE : return ExtractDate(DataBlock + Fields[FFieldIndex].Offset);

		case TIME_TYPE : throw Exception(AnsiString("TIME_TYPE field type not supported - ") + Name + " - " + Index);

		case DECIMAL_TYPE : return ExtractDecimal(DataBlock + Fields[FFieldIndex].Offset, Fields[FFieldIndex].Size, Fields[FFieldIndex].Decimal);

		case MONEY_TYPE : throw Exception(AnsiString("MONEY_TYPE field type not supported - ") + Name + " - " + Index);

		case LOGICAL_TYPE : return AnsiString((DataBlock[Fields[FFieldIndex].Offset] == '\0') ? "False" : "True");

		case NUMERIC_TYPE : throw Exception(AnsiString("NUMERIC_TYPE field type not supported - ") + Name + " - " + Index);
		case BFLOAT_TYPE : throw Exception(AnsiString("BFLOAT_TYPE field type not supported - ") + Name + " - " + Index);
		case LSTRING_TYPE : throw Exception(AnsiString("LSTRING_TYPE field type not supported - ") + Name + " - " + Index);
		case ZSTRING_TYPE : throw Exception(AnsiString("ZSTRING_TYPE field type not supported - ") + Name + " - " + Index);
		case UNSIGNED_BINARY_TYPE : return (Fields[FFieldIndex].Size == 2) ? AnsiString(*((unsigned short *)(DataBlock + Fields[FFieldIndex].Offset))) : AnsiString(*((unsigned *)(DataBlock + Fields[FFieldIndex].Offset)));

		case AUTOINCREMENT_TYPE : return (Fields[FFieldIndex].Size == 2) ? AnsiString(*((unsigned short *)(DataBlock + Fields[FFieldIndex].Offset))) : AnsiString(*((unsigned *)(DataBlock + Fields[FFieldIndex].Offset)));

		case STS : throw Exception(AnsiString("STS field type not supported - ") + Name + " - " + Index);
		case NUMERIC_SA : throw Exception(AnsiString("NUMERIC_SA field type not supported - ") + Name + " - " + Index);

	}
	return RetValue;
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFieldValue(const Variant Index, AnsiString Value)
{
	FieldIndex = RetrieveIndex(Index);

	switch (Fields[FFieldIndex].Type) {
		case STRING_TYPE : EncodeString(DataBlock + Fields[FFieldIndex].Offset, Fields[FFieldIndex].Size, Value);
											 break;

		case INTEGER_TYPE : throw Exception(AnsiString("INTEGER_TYPE field type not supported - ") + Name + " - " + Index);
		case IEEE_TYPE : throw Exception(AnsiString("IEEE_TYPE field type not supported - ") + Name + " - " + Index);

		case DATE_TYPE : EncodeDate(DataBlock + Fields[FFieldIndex].Offset, Value);
										 break;

		case TIME_TYPE : throw Exception(AnsiString("TIME_TYPE field type not supported - ") + Name + " - " + Index);

		case DECIMAL_TYPE : EncodeDecimal(DataBlock + Fields[FFieldIndex].Offset, Fields[FFieldIndex].Size, Fields[FFieldIndex].Decimal, Value);
												break;

		case MONEY_TYPE : throw Exception(AnsiString("MONEY_TYPE field type not supported - ") + Name + " - " + Index);

		case LOGICAL_TYPE : DataBlock[Fields[FFieldIndex].Offset] = (unsigned char)(Value.LowerCase() == "false" ? 0 : 1);
												break;

		case NUMERIC_TYPE : throw Exception(AnsiString("NUMERIC_TYPE field type not supported - ") + Name + " - " + Index);
		case BFLOAT_TYPE : throw Exception(AnsiString("BFLOAT_TYPE field type not supported - ") + Name + " - " + Index);
		case LSTRING_TYPE : throw Exception(AnsiString("LSTRING_TYPE field type not supported - ") + Name + " - " + Index);
		case ZSTRING_TYPE : throw Exception(AnsiString("ZSTRING_TYPE field type not supported - ") + Name + " - " + Index);
		case UNSIGNED_BINARY_TYPE : if (Fields[FFieldIndex].Size == 2) {
																*((unsigned short *)(DataBlock + Fields[FFieldIndex].Offset)) = (unsigned short)atoi(Value.c_str());
															} else {
																*((unsigned *)(DataBlock + Fields[FFieldIndex].Offset)) = (unsigned)atoi(Value.c_str());
															}
															break;

		case AUTOINCREMENT_TYPE : if (Fields[FFieldIndex].Size == 2) {
																*((unsigned short *)(DataBlock + Fields[FFieldIndex].Offset)) = (unsigned short)atoi(Value.c_str());
															} else {
																*((unsigned *)(DataBlock + Fields[FFieldIndex].Offset)) = (unsigned)atoi(Value.c_str());
															}
															break;

		case STS : throw Exception(AnsiString("STS field type not supported - ") + Name + " - " + Index);
		case NUMERIC_SA : throw Exception(AnsiString("NUMERIC_SA field type not supported - ") + Name + " - " + Index);

	}
}
//---------------------------------------------------------------------------
AnsiString __fastcall TBTAccess::ExtractDate(unsigned char *Data)
{
	char WkRet[11];
	short int WkYear;

	if (FLocale == "US") {
		WkRet[0] = char((Data[1] < 10) ? '0' : (Data[1] / 10) + '0'); // US Version
		WkRet[1] = char((Data[1] % 10) + '0');
		WkRet[2] = '/';
		WkRet[3] = char((Data[0] < 10) ? '0' : (Data[0] / 10) + '0');
		WkRet[4] = char((Data[0] % 10) + '0');
		WkRet[5] = '/';
	}

	if (FLocale == "UK") {
		WkRet[0] = char((Data[0] < 10) ? '0' : (Data[0] / 10) + '0'); // UK Version
		WkRet[1] = char((Data[0] % 10) + '0');
		WkRet[2] = '/';
		WkRet[3] = char((Data[1] < 10) ? '0' : (Data[1] / 10) + '0');
		WkRet[4] = char((Data[1] % 10) + '0');
		WkRet[5] = '/';
	}

	WkYear = *((short int *)(Data + 2));
//	WkYear = char(WkYear % 100);
	WkRet[6] = char(((WkYear / 100) < 10) ? '0' : ((WkYear / 100) / 10) + '0');
	WkRet[7] = char(((WkYear / 100) % 10) + '0');
	WkRet[8] = char(((WkYear % 100) < 10) ? '0' : ((WkYear % 100) / 10) + '0');
	WkRet[9] = char(((WkYear % 100) % 10) + '0');
	WkRet[10] = '\0';
	return AnsiString(WkRet);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TBTAccess::ExtractString(unsigned char *Data, short Size)
{
	char WkRet[256];
	memcpy(WkRet, Data, Size);
	WkRet[Size] = '\0';
	return AnsiString(WkRet);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TBTAccess::ExtractDecimal(unsigned char *Data, short Size, char Decimal)
{
	char WkRet[28];
	int RetPos = 0;
	bool RetStarted = false;
	short StopPoint = short(Size - 1);
	short DecimalPoint = short(Size - (1 + (Decimal >> 1)));
	if (Decimal == 0) DecimalPoint = Size;
	bool DecimalTop = (Decimal & 0x01);
	bool Negative = false;

	for (int Count = 0; Count <= StopPoint; Count++) {
//		See if Decimal goes here
		if (Count == DecimalPoint) {
			if (DecimalTop) {
				if (RetPos == 0) {
					WkRet[RetPos++] = '0';
				}
				WkRet[RetPos++] = '.';
				RetStarted = true;
			}
		}
		if (((Data[Count] >> 4) != 0) || (RetStarted)) {
			WkRet[RetPos++] = char((Data[Count] >> 4) + '0');
			RetStarted = true;
		}
//		See if Decimal goes here
		if (Count == DecimalPoint) {
			if (!DecimalTop) {
				if (RetPos == 0) {
					WkRet[RetPos++] = '0';
				}
				WkRet[RetPos++] = '.';
				RetStarted = true;
			}
		}
		if (Count == StopPoint) {
			if ((Data[Count] & 0x0F) == 0x0D) {
				Negative = true;
//				WkRet[RetPos++] = '-';
			}
		} else {
			if (((Data[Count] & 0x0F) != 0) || (RetStarted)) {
				WkRet[RetPos++] = char((Data[Count] & 0x0F) + '0');
				RetStarted = true;
			}
		}
	}
	if (RetPos == 0) WkRet[RetPos++] = '0';
	WkRet[RetPos] = '\0';
	return (Negative ? AnsiString("-") : AnsiString("")) + AnsiString(WkRet);
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::EncodeDate(unsigned char *Data, AnsiString& Value)
{
	int WkMonth;
	int WkDay;
	int WkYear;

	if (FLocale == "US") {
		sscanf(Value.c_str(), "%d/%d/%d", &WkMonth, &WkDay, &WkYear); // US version
	}

	if (FLocale == "UK") {
		sscanf(Value.c_str(), "%d/%d/%d", &WkDay, &WkMonth, &WkYear); // UK version
	}

	if ((WkMonth != 0) ||
			(WkDay != 0) ||
			(WkYear != 0)) {
		if (WkYear < 100) {
			if (WkYear <= 50) {
				WkYear += 2000;
			} else {
				WkYear += 1900;
			}
		}

		if ((WkMonth < 1) || (WkMonth > 12) || (WkDay < 1)) {
			throw Exception("Invalid Date");
		}
		if ((WkMonth == 4) ||
				(WkMonth == 6) ||
				(WkMonth == 9) ||
				(WkMonth == 11)) {
			if (WkDay > 30) {
				throw Exception("Invalid Date");
			}
		} else if (WkMonth == 2) {
			if (IsLeapYear(WORD(WkYear))) {
				if (WkDay > 29) {
					throw Exception("Invalid Date");
				}
			} else {
				if (WkDay > 28) {
					throw Exception("Invalid Date");
				}
			}
		} else {
			if (WkDay > 31) {
				throw Exception("Invalid Date");
			}
		}
/* Old version.
		if (FLocale == "US") {
			Value = AnsiString(WkMonth) + '/' + WkDay + '/' + WkYear; // US version
		}

		if (FLocale == "UK") {
			Value = AnsiString(WkDay) + '/' + WkMonth + '/' + WkYear; // US version
		}
		TDateTime Test(Value);
*/
	}
//	Test.DecodeDate(&WkYear, &WkMonth, &WkDay); // does not work - 00 is considered 1900

	Data[0] = (unsigned char)WkDay;
	Data[1] = (unsigned char)WkMonth;
	*(short *)(Data + 2) = (short)WkYear;
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::EncodeString(unsigned char *Data, short Size, AnsiString& Value)
{
	strncpy((char *)Data, Value.c_str(), Size);
	for (int Count = Value.Length(); Count < Size; Count++) {
		Data[Count] = ' ';
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::EncodeDecimal(unsigned char *Data, short Size, char Decimal, AnsiString& Value)
{
	char WkRet[28];
	AnsiString FormatString;
	long double TempVal;
	int RetPos = 0;
	int StopPoint;
	bool Negative = false;

	if ((((Size - 1) * 2) + 1) == Decimal) {
		FormatString = "0";
	} else {
		FormatString = AnsiString::StringOfChar('0', ((Size - 1) * 2) + 1 - Decimal);
	}
	if (Decimal != 0) {
		FormatString += '.';
		FormatString += AnsiString::StringOfChar('0', Decimal);
	}
	sscanf(Value.c_str(), "%Lf", &TempVal);
	strcpy(WkRet, FormatFloat(FormatString, TempVal).c_str());

	if (WkRet[0] == '-') {
		Negative = true;
		RetPos = 1;
	}
	StopPoint = Size - 1;
	for (int Count = 0; Count < Size; Count++) {
		if (WkRet[RetPos] == '.') {
			RetPos++;
		}
		Data[Count] = char((WkRet[RetPos++] - '0') << 4);
		if (WkRet[RetPos] == '.') {
			RetPos++;
		}
		if (Count == StopPoint) {
			if (Negative) {
				Data[Count] |= 0x0D;
			} else {
				Data[Count] |= 0x0F;
			}
		} else {
			Data[Count] |= char(WkRet[RetPos++] - '0');
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFieldIndex(short int Index)
{
	if ((Index < 0) || (Index > (FieldCount - 1))) {
		throw Exception(AnsiString("Index out of range - ") + Name + " - " + Index);
	}
	FFieldIndex = Index;
	FFieldName = AnsiString(Fields[Index].Name);
	FFieldSize = Fields[Index].Size;
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetFieldName(AnsiString& Index)
{
	short Count;
	for (Count = 0; Count < FieldCount; Count++) {
		if (Index == Fields[Count].Name) break;
	}
	if (Count == FieldCount) {
		throw Exception(AnsiString("Field name not found - ") + Name + " - " + Index);
	}
	FFieldIndex = Count;
	FFieldName = AnsiString(Fields[Count].Name);
	FFieldSize = Fields[Count].Size;
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetupIndex(void)
{
	int Count;
	for (Count = 0; Count < IndexCount; Count++) {
		if (Indexes[Count].IdxNumb == FIndex) {
			break;
		}
	}
	if (Count == IndexCount) {
		throw Exception(AnsiString("Index out of range - ") + Name + " - " + FIndex);
	}
	memset(KeyBuffer, 0, MAX_KEY_SIZE);
	int KeyPos = 0;
	for (; Count < IndexCount; Count++) {
		if ((Indexes[Count].IdxNumb != FIndex) || (Indexes[Count].File != FFileID)) {
			break;
		}
		if ((KeyPos + Fields[Indexes[Count].Field].Size) > MAX_KEY_SIZE) {
			throw Exception(AnsiString("Index size limit exceeded - ") + Name + " - " + FIndex);
		}
		memcpy(KeyBuffer + KeyPos, DataBlock + Fields[Indexes[Count].Field].Offset, Fields[Indexes[Count].Field].Size);
		KeyPos += Fields[Indexes[Count].Field].Size;
	}
}
//---------------------------------------------------------------------------
void __fastcall TBTAccess::SetIndex(short int WkIndex)
{
	int Count;
	if (!OKToSet) { // Thrown in to prevent load errors (Indexes not initialized yet)
		FIndex = WkIndex;
		return;
	}
	for (Count = 0; Count < IndexCount; Count++) {
		if (Indexes[Count].IdxNumb == WkIndex) {
			break;
		}
	}
	if (Count == IndexCount) {
		throw Exception(AnsiString("Index out of range - ") + Name + " - " + WkIndex);
	}
	FIndex = WkIndex;
}
//---------------------------------------------------------------------------