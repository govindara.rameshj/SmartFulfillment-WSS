//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Header:WixStep.h - Main header for NLM.
//---------------------------------------------------------------------------
/****************************************************************
* A WinSock Step emulation program for Windows NT/95
* 6/1/98 by Mark Heath & David Clavey, Lowther Consultants
* Main Header File
* Win32-Console mode and 16bit DOS Versions
* Converted to NLM in June 98 (JG)
*****************************************************************/

#define APP_VERSION "4.07"
#define BASE_PATH "C:\\"


#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <afx.h>
#include <process.h>
#include <time.h>
#include <conio.h>
#include <winsock.h>
#include <errno.h>
#include <sql.h>
#include <sqlext.h>
//#include <afxdb.h>
#include "processLog.h"
#include "windisplay.h"
#include "resource.h"

#define ID_DO_SOCKET_LISTEN	104

#define MAIN_PORT   6500      // the main STEP port
#define MAX_SOCKETS 21        // support 16 users
#define BUFFER_SIZE 1024*5    // upto 5120 bytes
#define DisplaySocketStatusWidth	42	// Width in chars of the client status window
#define MAX_STRING_LENGTH 161					// Used for temp strings

#define MAX_INPUT    64       // Maximum input size
#define SCREEN_ROWS  8
#define SCREEN_COLS  20

#define KEY_MAX_LENGTH	41
#define SQLSUCCESS(rc) ((rc==SQL_SUCCESS)||(rc==SQL_SUCCESS_WITH_INFO))

/***************************************************************************
   Constants
****************************************************************************/
   #define EXIT_WITH_ERROR    1
   #define TRUE               1
   #define FALSE              0
   #define VERSION_OFFSET     0
   #define REVISION_OFFSET    2
   #define PLATFORM_ID_OFFSET 4

// Fields.h
void vGetField ( char *pcData, size_t pcDataSize, int i );
void vSetField ( char *pcData, char *pcSet, int i );
void vAddField ( char *pcData, int i );
void vGetEquivPrice(char *pcPrice, int sessionNumber);
//void vReadEanMas(EANMAS *eanmas, int i);
void vUpdateCountField( int i );
void vTestHighField( int i );
void vSetReasonLine( int iReasonLine, int i);

// app.c
long lApp(long lCurrentPos, SOCKET sSocket, int i);
BOOL fLogonAttempt(SOCKET sSocket);
int iFindLabel(char *pcLabel);
void vInitReasonDescs(int i);
long lMenuLogoff(SOCKET sSocket, int i);

// Utils.c
int initialiseSockets(HWND hWnd, SOCKET *pSocket);
int initialiseUsers(HWND hWnd);
void vCloseSocket( char * msg, SOCKET socket, int code);
int getInUseIdx(SOCKET s);
unsigned int getInUseOctet(SOCKET s);
int getInUseOidx(unsigned int uiOctet);
void setInUseOctet(SOCKET sSocket, unsigned int uiOctet);
void clearOldConnects(SOCKET sSocket, unsigned int uiOctet);
int Make_New(SOCKET s);
void vClearSvUser( SOCKET s);

// SQLAccess.c

class SQLConnection
{
	//HWND hWnd;
  SQLHENV henv;         // Environment
  HDBC hdbc;         // Connection handle
	SQLHSTMT hstmt;    // Statement handle
	bool connected;

public:
	RETCODE rc;        // ODBC return code
  SQLConnection();           // Constructor
	bool Connect(void);
	void DisplaySQLError(char *cmd);
	bool Disconnect(void);
	bool DoSQLCommand(char *cmdstr);
	bool DoSQLCommand(char *cmdstr, int cursorType);
	bool DoSQLFetch(void);
	bool DoSQLFetch(int fetchType);
	bool SQLGetColumn(int columnNumber, int C_columnType, void *buffer, int length);
	char *SQLGetCurrentDateString(char *s, size_t sSize);
	char *SQLGetCurrentTimeString(char *s, size_t sSize);
	char *SQLQueryAddVarChar(char *cmd, rsize_t cmdSizeInBytes, char *varChar);
};

/*
int SQLLastReturnCode(void);
char *SQLGetCurrentDateString(char *s);
char *SQLGetCurrentTimeString(char *s);
char *SQLQueryAddVarChar(char *cmd, char *varChar);
bool InitializeSQLDataBase(HWND hWnd, char *connectString);
bool DoSQLCommandRecordSet(int sessionNumber, char *cmd);
bool DoSQLCommand(char *cmd);
bool DoSQLFetchRecordSet(int sessionNumber, int fetchType);
bool DoSQLFetch(void);
bool FreeSQLCommand(SQLHSTMT hstmt);
bool FreeSQLCommand(void);
bool SQLGetColumn(SQLHSTMT hstmt, int columnNumber, int columnType, void *buffer, int length);
bool SQLGetColumn(int columnNumber, int columnType, void *buffer, int length);
bool CloseDownSQLDataBase(void);
*/

// Database.h
int iReadSysDatDatabase( int i );

//void vReadSaCode(SACODE *sacode, int i);
//int iFirstSaCodeDatabase(int i);
//int iReadSaCodeDatabase(int i);
int iPriorSaCodeDatabase(int i);
int iSeekSaCodeDatabase(int i, int iOption);
int iGetNextSaCodeDatabase(int i);

//void vReadStkMas(STKMAS *stkmas, int i);
int iReadStkMasDatabase(int i);
int iSeekStkMasDatabase(int i, int iOption);

bool iReadEanMasDatabase(int i);
int iSeekEanMasDatabase(int i);

//void vReadStkAdj(STKADJ *stkadj, int i);
int iSeekStkAdjDatabase(int i);
int iGetNextStkAdjDatabase(int i);
bool bInsertStkAdjDatabase(int i);

//void vReadStkLog(STKLOG *stklog, int i);
long GetStkLogSoldToday(char *skun);
int iReadStkLogDatabase(int i);
int iPriorStkLogDatabase(int i);
int iInsertStkLogDatabase(int i, int iOption);
int iInsertStkLogAdjust91(int i);

int iUpdateStkMasDatabase( int i, int iOption );

//void vReadHhtDet(HHTDET *hhtdet, int i);
int iSeekHhtDetDatabase( int i, int iOption );
int ReadNextHhtDetRecord( int i );
int iUpdateHhtDetDatabase( int i );

//void vReadHhtHdr(HHTHDR *hhthdr, int i);
int iUpdateHhtHdrDatabase( int i );
int SeekLastHhtHdrDatabase( int i );
int ReadNextHhtHdrRecord(int i);
int iUnLockStkMasDatabase( int i );

//Prccbase.c
void vReadPrcChg(int i);
int iSeekPrcChgDatabase( int i );
int iGetNextPrcChgDatabase( int i );
int iUpdatePrcChgDatabase( int i );
int UpdateTmpfilLabelTotals( int i );
int InsertTmpfileLabels( int i);
int InsertTmpfileLabelTotals( int i);
int SeekGapWalkTmpfilRecord(int i);
int UpdateGapWalkTmpfilRecord(int i);
int iInsertGapWalkTmpfilRecord( int i );

//Menubase.c
bool ReadAfgCtlTable(int sessionNumber);
bool ReadAfpCtlTable(int sessionNumber);
bool ReadAfdCtlTable(int sessionNumber);
bool ReadSysPasTable(int sessionNumber);
bool iSeekSysCodDatabase(int sessionNumber, int iOption);
int iGetNextSysCodDatabase( int i );
int iPriorSysCodDatabase( int i );
bool PutInsertActivityLog( int i );
bool iUpdateActLogDatabase( int i );

// DBConv.c

typedef struct DBLongDate
	{
		char day;
		char month;
		unsigned short int year;
	} DBLongDate;

#define ProcessLogNone		0
#define ProcessLogConsole 1
#define ProcessLogFile		2


void *DefaultRetValue(void *StackRetParam, size_t bufferSize);
char *DefaultRetValue(char *StackRetParam, size_t bufferSize);
DBLongDate *GetCurrentDateInDBLongDateFormat(DBLongDate *RetVar);
char *GetCurrentDateTimeMsg(char *RetVar);
char *GetCurrentDateInDDMMYY(char *RetVar);
char *GetCurrentDateInDDMMYYYY(char *RetVar);
char *GetCurrentDateForSql(char *RetVar);
char *GetCurrentTimeStr(char *RetVar);
void GetDBLongDateToDBField(void *DBField, DBLongDate *date);
void GetDBFieldToDBLongDate(DBLongDate *date, void *DBField);
char *GetDBLongDateFieldToStr(char *RetVar, DBLongDate *DBDate);
int DaysInMonth(int month, int year);
DBLongDate *GetStrToDBLongDateField(DBLongDate *retDate, char *strDate);
int DBLongDateCompare(DBLongDate *d1, DBLongDate *d2);
unsigned long GetCurrentSeconds(void) 	;
unsigned long DBLongDateToDayNumber(DBLongDate *date);
unsigned long DateStrToDayNumber(char *DateStr);
BOOL DayNumberToDBLongDate(DBLongDate *Date, long DayNumber);
BOOL DayNumberToDateStr(char *RetVar, long DayNumber);
char *GetDBCharFieldToStr(char *RetVar, void *DBField, int DBFieldLength);
char *GetStrToDBCharField(void *DBFieldRet, char *Str, int DBFieldLength);
void StripSpaces(char *retVar, size_t retVarSize, char *Field);
char *ZeroPadField(char *RetVar, char *Field, int length);
char *ZeroPadField(char *Field, int length);
char *ZeroPadField(char *RetVar, int Field, int length);
char *DBstrncpy(char *Dest, char *Source, int SizeOfDest);
char *GetDBLogicalFieldToStr(char *RetVar, void *DBField);
void SetDBLogicalField(char *DBField, char Value);
void GetDBIdentityField(void *RetVar, void *DBField);
long GetDBDecFieldToLong(char *DBField, short DBFieldLength);
void *GetLongToDBDecField(void *DBFieldRet, signed long Value, int DBFieldLength);
void *GetStrToDBDecField(unsigned char *DBFieldRet, char *Value, short DBFieldLength, char Decimal);
char *GetDBDecFieldToStr(char *RetVar, unsigned char *Data, short Size, char Decimal);

// Logging

#define ProcessLogNone		0
#define ProcessLogConsole 1
#define ProcessLogFile		2

void ProcessLogSetFilename(char *FileName);
int ProcessLogInitialize(void);
int ProcessLogDevice(void);
FILE *ProcessLogHandle(void);
void ProcessLogFlushBuffer(void);
void ProcessLogStartToFile(int append);
void ProcessLogStartToConsole(void);
void ProcessLogStop(void);
int ProcessLogWriteTimeStamp(char *msg);
int ProcessLogWrite(char *msg);
int ProcessLogWriteLong(long li);
int ProcessLogWriteLn(char *msg);
BOOL ProcessLogWriteToScreenStale(void);
void SetLogToScreen(BOOL on);
BOOL GetLogToScreen(void);
void ProcessLogWriteToScreen(void);

void DisplayServerLog(void);

int ProcessLogDumpBuffer(char *msg, void *buffer, int length);
char *DBBufferToPrintableStr(char *RetVar, void *buffer, int length, int toHex);
char *DBBufferToHexStr(char *RetVar, void *buffer, int length);
char *DBBufferAddressToString(char *RetVar, void *buffer);

//int DBInitTable(DataBaseTable *TableInfo, char *TableName, BTI_VOID_PTR DataBuffer, int DataBufferLength);
//int DBOpenTable(DataBaseTable *TableInfo, int UserIndex);
void DBClearAccessKeyBuffer(void);
void *DBGetAccessKeyBufferPtr(void);
//int DBAccessTableData(DataBaseTable *TableInfo, int BtrieveCmd, void *keyBuf, int keyNum);

typedef struct {
  SOCKET sSocketId;             /*   Socket identification        */
  unsigned int uiEndOctet;      /*   TCP type D address           */
  int  iTimeOut;	  						/*   TimeOut Count 				  */
} INUSE_TYPE;

/* W2.08  Save the User detail */
typedef struct {
   long lCurrentPos;            /*   Current line of macro file   */
} SVUSER_TYPE;


typedef struct {
   long lCurrentPos;            /*   Current line of macro file   */
   BOOL fLoggedOn;              /*   Logged on flag               */
   int  iToggle;                /*   Toggle for buttons           */
   int  iGoto;                  /*   Goto test Flag               */
   int  iFlag;                  /*   Update Flag                  */
   char pcLastInput[MAX_INPUT]; /*   Last inputted string         */
   char pcSave[7];              /*   Save Data                    */
   //int  iDateDet;   /* date[2]  D    Date of count                */
   DBLongDate dDateDet;			// W 3.08 GS
   //int  iDateHdr;   /* date[2]  D    Date of count                */
   DBLongDate dDateHdr;			// W 3.08 GS
   char pcSkun[7];  /* skun[3]  C6   SKU Number                   */
   char pcDesc[41]; /* desc[40] A40  Long Description             */
   char pcPric[13]; /* pric[4]  N8.2 Price                        */
   char pcSupp[7];  /* supp[3]  C6   Supplier                     */
   char pcProd[11]; /* prod[10] A10  Product Code                 */
   char pcOnha[9];  /* onha[3]  N6   Stock on Hand (HHTDET)       */
   char pcSold[9];  /* sold[3]  N6   Sold Today                   */
   char pcMini[9];  /* mini[3]  N6   On Order (mini)              */
   char pcMaxi[9];  /* maxi[3]  N6   On Order (maxi)              */
   char pcIsta[2];  /* ista[1]  A1   Item Status code             */
   char pcIobs[2];  /* iobs[1]  I    Obsolete flag                */
   char pcIdel[2];  /* idel[1]  I    Deleted flag                 */
   char pcLabn[7];  /* labn[1]  N2   Number of Small Labels       */
   char pcLabm[7];  /* labm[1]  N2   Number of Medium Labels      */
   char pcLabl[7];  /* labl[1]  N2   Number of Large Labels       */
   char pcTscn[9];  /* tscn[3]  N6   Total shop floor count       */
   char pcTwcn[9];  /* twcn[3]  N6   Total Warehouse count        */
   char pcTpre[9];  /* tpre[3]  N6   Total Presold stock          */
   char pcTcnt[9];  /* tcnt[3]  N6   Total count                  */
   char pcDate[12]; /* date[2]  D    Date of count                */
   char pcNumb[17]; /* numb[8]  C16  EAN Number (EANMAS)          */
                    /* numb[2]  N4   Total items to count (HHTHDR)*/
   char pcDone[9];  /* done[2]  N4   Number of items counted      */
   char pcIadj[3];  /* iadj[1]  I    Y= Today's adjustmts applied */
   char pcIcnt[3];  /* icnt[1]  I    Entry has been made for SKU  */
   char pcInon[3];  /* inon[1]  I    Y= Non stocked               */
   char pcToDo[9];  /* numb[2]  N4   minus done[2] N4 total       */
   char pcAuth[9];  /* auth[2]  C3   Employee ID authorise manager*/
   //char pcSonh[9];  /* onha[3]  N6   Stock on Hand (STKMAS)  W1.03*/
   char pcLsku[9];  /* skun[3]  C6   STKLOG SKU Number       W1.04*/
   char pcLdat[12]; /* date[2]  D    STKLOG date             W1.04*/
   char pcType[3];  /* type[1]  C2   STKLOG Movement Type    W1.04*/
   char pcSstk[9];  /* sstk[3]  N6   STKLOG Start of stock   W1.04*/
   char pcEstk[9];  /* estk[3]  N6   STKLOG End of stock 	 W1.04*/
   char pcSmov[9];  /* smov[3]  N6   Daily Stock Movement 	 W1.04*/
   char pcDayn[11];  /* dayn[4]  N8   Day Number of Movement	 W1.04*/
   //char cToDayn[11]; /* dayn[4]  N8   Current Day Number  	 W1.05*/
   //BOOL bGotToDay;	/*          C1   Got Todays Day Number 	 W3.07b*/
   char pcTmdc[9];  /* tmdc[3]  N6   Total Markdown Count    W2.02*/
   char pcMdnq[9];  /* mdnq[3]  N6   MarkDown Stock          W2.02*/
   char pcSmdn[9];  /* smdn[3]  N6   MarkDown Stock Movement W2.05*/
/* STKADJ record key */
   char pcAaDate[12];/* date[2] D   STKADJ Date key 	   		W2.06*/
   char pcAaCode[3]; /* code[1] C2  STKADJ Code     	   		W2.06*/
   char pcAaSkun[7]; /* Skun[3] C6  STKADJ Sku      	   		W2.06*/
   char pcAaSeqn[3]; /* Seqn[1] C2  STKADJ Seqn     	   		W2.06*/
/* SACODE record Next key */
   char pcSaStart[3];/* numb[1]     SACODE Adj Start key   	   	W2.06*/
/* SACODE record Scrolling*/
   char pcSaNumb[3]; /* numb[1]      SACODE Adj No.        	   	W2.06*/
   char pcSaDesc[26]; /* desc[25]    SACODE description        	W2.06*/
   char pcSaType[2]; /* type[1] A1   SACODE Type of Adj.       	W2.06*/
   char pcSaSign[2]; /* sign[1] A1   SACODE Sign of Adj.       	W2.06*/
   char pcSaSecl[2]; /* secl[1] A1   SACODE Security Level     	W2.06*/
   int pcSaImdn;		/* imdn[1] I    SACODE MarkDown Adj.      	W2.06*/
   int pcSaIwtf;		/* iwtf[1] I    SACODE Write Off Adj.     	W2.06*/
/* SACODE Reason Codes to be displayed */
   char pcxxxx[10];  /* filler     						       	W2.06*/
   char pcReas1[3];  /* Stk Adj Number Line 1			       	W2.06*/
   char pcSel1[21];  /* Desc Reason Line 1				       	W2.06*/
   char pcImdn1;	 /* IMDN Reason Line 1				       	W2.06*/
   char pcIwtf1;	 /* IWTF Reason Line 1				       	W2.06*/
   char pcType1[2];	 /* Type Reason Line 1				       	W2.06*/
   char pcReas2[3];  /* Stk Adj Number Line 2			       	W2.06*/
   char pcSel2[21];  /* Desc Reason Line 2				       	W2.06*/
   char pcImdn2;	 /* IMDN Reason Line 2				       	W2.06*/
   char pcIwtf2;	 /* IWTF Reason Line 2				       	W2.06*/
   char pcType2[2];	 /* Type Reason Line 2				       	W2.06*/
   char pcReas3[3];  /* Stk Adj Number Line 3			       	W2.06*/
   char pcSel3[21];  /* Desc Reason Line 3				       	W2.06*/
   char pcImdn3;	 /* IMDN Reason Line 3				       	W2.06*/
   char pcIwtf3;	 /* IWTF Reason Line 3				       	W2.06*/
   char pcType3[2];	 /* Type Reason Line 3				       	W2.06*/
   char pcReas4[3];  /* Stk Adj Number Line 4			       	W2.06*/
   char pcSel4[21];  /* Desc Reason Line 4				       	W2.06*/
   char pcImdn4;	 /* IMDN Reason Line 4				       	W2.06*/
   char pcIwtf4;	 /* IWTF Reason Line 4				       	W2.06*/
   char pcType4[2];	 /* Type Reason Line 4				       	W2.06*/
   char pcReas5[3];  /* Stk Adj Number Line 5			       	W2.06*/
   char pcSel5[21];  /* Desc Reason Line 5			    	   	W2.06*/
   char pcImdn5;		 /* IMDN Reason Line 5				       	W2.06*/
   char pcIwtf5;		 /* IWTF Reason Line 5				       	W2.06*/
   char pcType5[2];	 /* Type Reason Line 5				       	W2.06*/
/* SACODE Reason Codes to be displayed */
   char pcSelReas[3];	 /* Selected Stk Adj Reason Number     W2.06*/
   char pcSelDesc[20];   /* Selected Description		       W2.06*/
   char pcSelImdn;	 /* Selected IMDN 				       W2.06*/
   char pcSelIwtf;	 /* Selected IWTF 				       W2.06*/
   char pcSelType[2];	 /* Selected Type 				       W2.06*/
/* Markdown Reason Display Line Parameters    */
   int  iReasonLine;	 /* Reason Display Line No.			   W2.06*/
   char pcOption[2]; 	 /* Option Selected   				   W2.06 */
/* Adjustment Parameters                      */
   BOOL bAdjExists;	 	 /* Adjustments Exist				   W2.06 */
   char pcSaSeqn[3]; 	 /* Stk Adj Record Sequence			   W2.06 */
   char pcAdjQty[9]; 	 /* Adjustment Quantity				   W2.06 */
   char pcLastSeqn[3]; 	 /* Last Adjustment sequence on file   W2.06 */
   char pcCost[13]; 	 /* STKMAS Cost unedited 			   W2.06 */
/* Screen handling                            */
   BOOL bScreenTop; 	 /* Screen Top						   W2.06 */
   BOOL bScreenBot; 	 /* Screen Bottom					   W2.06 */
/* SACODE record Selected key */
   char pcSaKey[3];	 /*             SACODE key             	   	W2.06*/
/* STKMAS MarkDown and Write off fields */
   //char ImMadq[9];	 /*   Movement Adj Quantity            	   	W2.06*/
   //char ImMadv[10];	 /*   Movement Adj Value            	   	W2.06*/
/* STKMAS movement fields */
   char imOnha[9];	 /*   ONHA Stock	   			    	   	W2.06*/
   char imRetq[9];	 /*   Return Quantity				   	   	W2.06*/
   char imPric[11];	 /*   Price                 		   	   	W2.06*/
   char imMdnq[9];	 /*   MarkDown Quantity                	   	W2.06*/
   char imWtfq[9];	 /*   Write Off Quantity               	   	W2.06*/
/* STKLOG movement fields */
   char slSstk[9];	 /*   Starting Stock   			    	   	W2.06*/
   char slSmdn[9];	 /*   Starting MarkDown SOH			   	   	W2.06*/
   char slSwtf[9];	 /*   Starting Write Off SOH		   	   	W2.06*/
   char slSret[9];	 /*   Starting Open Returns            	   	W2.06*/
   char slSpri[13];	 /*   Starting Price                   	   	W2.06*/
/* STKLOG movement fields */
   char slEstk[9];	 /*   Ending Stock   			    	   	W2.06*/
   char slEmdn[9];	 /*   Ending MarkDown SOH			   	   	W2.06*/
   char slEwtf[9];	 /*   Ending Write Off SOH		   	   		W2.06*/
   char slEret[9];	 /*   Ending Open Returns            	   	W2.06*/
   char slEpri[13];	 /*   Ending Price                   	   	W2.06*/
   BOOL noPreSlRec;  /*	  No previous StkLog record exists		W2.06 */
/* SYSDAT fields */
   //char sdFkey[3];   /*  fkey C2  Record key            		W2.06 */
   //char sdTodw[2];   /*  todw N1 Today's Day of Week			W2.06 */
   //char sdTmdw[2];   /*  tmdw N1 Tomorrow's Day of Week			W2.06 */
/* HHTDET fields */
   char hdSkun[9];   /*  skun[3]  C6  HHTDET SKU Number         W2.08 */
/* MENU system User fields */
   char pcLang[5];   /*  Entered Language Code					W3.02 */
   char pcWsid[3];   /*  WSID Number		   					W3.03 */
   char pcAfgn[3];   /*  Entered AFG Number   					W3.02 */
   char pcAfpn[3];   /*  Entered AFP Number   					W3.02 */
   char pcAdes[21];  /*  AFGCTL Description to be displayed 	W3.02 */
   char pcMdes[21];  /*  Menu Description to be displayed  		W3.02 */
   //char pcEeid[5];   /*  Entered Employee ID  					W3.02 */
   char pcEeid[4];   /*  Entered Employee ID  					W3.02 */
   char pcEnam[20];  /*  Employee Name to be displayed			W3.02 */
   char pcPass[6];   /*  Entered Employee Password				W3.02 */
   char pcSecl[3];	 /*  Employee Security Level				W3.02 */
   //char pcIafg[3];	 /*  Employee Access to AFG 				W3.02 */
/* SYSPAS fields */
   char spEeid[4];   /*  eeid C3  Employee ID  					W3.02 */
   char spName[36];  /*  name T35 Employee Name					W3.02 */
   char spPass[6];   /*  pass T5  Employee Password				W3.02 */
   //char spSecl[199]; /*  secl C1(O=99) Security Level 			W3.02 */
/* AFGCTL fields */
   char gcLang[4];   /*  lang C3  Language Code					W3.02 */
   char gcNumb[3];   /*  numb C2  AFG Number       				W3.02 */
   char gcDscl[3];   /*  Dscl C1  Security Level for AFG 		W3.02 */
   char gcDesc[36];  /*  Desc T35 Menu Description				W3.02 */
/* AFPCTL fields */
   char fpLang[4];   /*  lang C3  Language Code					W3.02 */
   char fpAfgn[3];   /*  afgn C2  AFG Number       				W3.02 */
   char fpNumb[3];   /*  Numb C2  Menu Number					W3.02 */
   char fpDesc[36];  /*  Desc T35 Menu Description				W3.02 */
   char fpSecl[3];	 /*  Secl C1  Security Level for AFP		W3.02 */
   //char fpIafg[3];	 /*  Iafg I   Access allowed         		W3.02 */
/* AFDCTL fields */
   char dcLang[4];   /*  lang C3  Language Code					W3.04 */
   char dcAfgn[3];   /*  afgn C2  AFG Number       				W3.04 */
   char dcAfgp[3];   /*  afgp C2  Menu Number					W3.04 */
   //char dcText[525]; /*  Text T35.15 (15)Sub Menu Descriptions  W3.04 */
   //char dcSecl[15];	 /*  Secl C1(O=15) Security per Menu Desc.	W3.04 */
/* Table of Menu Options  */
   char pcMenuOpts[11]; /*  Allow upto 10 Menu Options			W3.02 */
   //char pcMend[201];/*  Allow upto 10/10(*20)Menu Descriptions W3.03 */
/* Table of Sub Menu Options  */
   char pcSubMenu[5];   /*  Enter Sub Menu YES/NO ?				W3.04 */
   char pcSubOpts[10];  /*  Allow upto 10 Menu Options     		W3.04 */
   //char pcSubMend[201]; /*  Allow upto 10/10(*20)Menu Descriptions W3.03 */
   char pcAdl1[20];	 /*  Sub Menu line 1 Description   			W3.04 */
   char pcAds1[3];	 /*  Sub Menu line 1 Security Level			W3.04 */
   char pcAdl2[20];	 /*  Sub Menu line 2 Description   			W3.04 */
   char pcAds2[3];	 /*  Sub Menu line 2 Security Level			W3.04 */
   char pcAdl3[20];	 /*  Sub Menu line 3 Description   			W3.04 */
   char pcAds3[3];	 /*  Sub Menu line 3 Security Level			W3.04 */
/* Activity Log Fields  */
   char pcLogin[5];	 /*  User Login/Logout id (IN/OUT)     		W3.02 */
   char pcSystemOut[5]; /*  System Logout id  (YES/NO)			W3.02 */
   char pcMenuStart[9]; /*  Menu Start Time			  			W3.02 */
   char pcStartTime[9]; /*  Menu Option Start Time    			W3.02 */
   char pcEndTime[9];	/*  Menu Option End Time    			W3.02 */
/* Parameters for Menu Timeout */
   long lTimeOut;		/*  User Menu Timeout        			W3.02 */
   BOOL pcLogOut;		/*  Log Out Flag 						W3.02 */
/* PRICE CHANGES : STKLOG Database Date for access to PRCCHG */
   //char slDdat[3];	/*  Database Date  			    	   	W3.03*/
   DBLongDate dslDdat;	// W3.08 GS
/* PRICE CHANGES : Database Date for update to PRCCHG */
   //char ipDpdt[3];		/*  Current PrcChg Database Date   	   	W3.03*/
   DBLongDate dipDpdt;	// W3.08 GS
/* PRICE CHANGES : PRCCHG Database Date */
   char ipSkun[7]; 		/*  skun[3]  C6  HHTDET SKU Number      W3.03*/
   char ipPdat[12];		/*  date[2]  D   Price effective Date  	W3.03*/
   char ipPric[12];		// pric[4]  N8  Price
   char ipPsta[2];		/*  psta[1]  A1  Price Change Status  	W3.03*/
   bool ipLabs;				/*  labs[1]  I   Small Label Print    	W3.03*/
   bool ipLabm;				/*  labm[1]  I   Medium Label Print    	W3.03*/
   bool ipLabl;				/*  labl[1]  I   large Label Print    	W3.03*/
   char space[10];
   char ipEvnt[7]; 		/*  evnt[6]  C6  Event Number           W3.05*/
   char ipPrio[3]; 		/*  prio[2]  C2  Event Priority         W3.05*/
   char ipMark[12];		/*  mark[4]  N9.2  Markup Price         W3.07c*/
/* PRICE CHANGES : PRCCHG User Fields   */
   char pcIpri[12];		/*  pric[4]  N8.2 Price                	W3.03*/
/* PRICE CHANGES : STKMAS Database Fields   */
   char imEqpm[11];		/*  eqpm[5]  N10.6 Equiv.Price Mult  	W3.03*/
   char imEqpu[8];		/*  eqpu[7]  A7   Equiv. Price Units   	W3.03*/
   char imAapc[3];				/*  aapc[1]  I    Auto.apply price chg.	W3.03*/
/* PRICE CHANGES : User Fields   */
   char pcDoingPriceChange;/*  Price Change being processed 	W3.03 */
   char pcEqpr[11];		/*  ip:pric/im:eqpm N8.2 Equiv.Price   	W3.03*/
   char pcEdes[20];		/*  User Equiv. Price Units description	W3.03*/
   char pcSlab[3];		/*  Request for Small Labels			W3.03*/
   char pcMlab[3];		/*  Request for Medium Labels			W3.03*/
   char pcLlab[3];		/*  Request for Large  Labels			W3.03*/
   //char pcTots[7];		/*  Total for Small Labels				W3.03*/
   //char pcTotm[7];		/*  Total for Medium Labels				W3.03*/
   //char pcTotl[7];		/*  Total for Large  Labels				W3.03*/
   char pcThisLabel[5];	/*  This Label Type being Updated		W3.03*/
/* PRICE CHANGES : Tmpfil Fields   */
   //char pcFkey[41]; 	       /*   Fkey Setup field       		W3.03*/
   //char pcTmpFkey[41]; 	       /*   Fkey Read field      		W3.03*/
   //char pcTmpTkey[5]; 	       /*   Tkey Read field      		W3.03*/
   //unsigned long ipPldt; 	/* PDAT Numeric date 				W3.03*/
   DATE_STRUCT dipPldt; 	// W3.08 GS
/* PRICE CHANGES : Actlog Fields   */
   unsigned long int alTkey;       /*   Actlog Tkey Read field(Main Menu)	W3.03*/
   unsigned long int alSubTkey;    /*   Actlog Tkey Read field(Sub Menu)	W3.04*/
/* WALKABOUT : User Fields   */
   char pcWalkAbout[5];		   /*  WalkAbout Selected = YES/NO	W3.04*/
   char pcFirstWalk[5];		   /*  First SKU per Walk = YES/NO	W3.04*/
/* GAP WALK  : Stkmas Fields/User Fields   */
   char imOnor[9];		 /*   On order Quantity			   	   	W3.04*/
   char pcImStatus[21];	 /*   Items Status (IOBS;IDEL;INON)	   	W3.04*/
   char pcReqQty[9];	 /*   Quantity Requested           	   	W3.04*/
   BOOL bNewGapSect;	 /*   New Gap Section           	   	W3.05a*/
   char imNoor[3];		 /*   noor[1] I Non Orderable Item	   	W3.07*/
/* MENU Options : */
   char pcMenuLine1[21];	 /*   Menu MenuLine1          	   	W3.06*/
   char pcMenuLine2[21];	 /*   Menu MenuLine2          	   	W3.06*/
   char pcMenuLine3[21];	 /*   Menu MenuLine3          	   	W3.06*/
   char pcMenuLine4[21];	 /*   Menu MenuLine4          	   	W3.06*/
   char pcMenuLine5[21];	 /*   Menu MenuLine5          	   	W3.06*/
   int  iThisLine;			 /*   This(Top) Menu Line      	   	W3.06*/
   int  iBotLine;			 /*   Last Menu Line 	     	   	W3.06*/
/* PIC & Shop Floor Label checking : */
   char pcPIC[4];		 	 /*  HHT entered PIC 				W3.06 */
   char pcShopFloor[4];	 	 /*  HHT entered PIC Shop Floor 	W3.06 */
   char pcPicLabelOk[4];	 /*  Label Requested YES/NO			W3.06 */
   char pcLabelMenu[4];		 /*  Label Menu required			W3.06 */
   char pcLabelCheck[4];	 /*  HHT entered Label Check		W3.06 */
   char pcPicSkuNof[4];		 /*  PIC SKU is NOF  YES/NO			W3.07 */
/* SYSCOD details                  : */
   char syType[3];	 		 /*  SYSCOD Type        			W3.06 */
   char syCode[3];	 		 /*  SYSCOD Code        			W3.06 */
   char syDesc[36];	 		 /*  SYSCOD Description    			W3.06 */
   char syAttr[2];	 		 /*  SYSCOD Attr        			W3.06 */
/* Label Reason Type              : */
   char pcLrType[3];		 /*  Entered Type "LR"     			W3.06 */
   char pcLrAttr[2];		 /*  Entered Attr "P"Price Change	W3.06 */
/* SYSCOD record Next key */
   char pcSyStart[3];		 /*  Code[2] SYSCOD Code Start key 	W3.06*/
   char pcPcOption[4];		 /*  Price Change Option           	W3.06*/
   int  iEnquiryLine;		 /*  Enquiry Line No.				W3.06*/
} USER_TYPE;

/* W2.08  Table of SKU's being Counted */
typedef struct {
   char pcSkun[7];            /*   Counted SKU   */
} COUNTED_SKUS;

/* W3.05  Table of GAP WALK SKU's */
typedef struct {
   char pcSkun[7];            /*   Gap Walk In Progress SKU   */
} GAPWALK_SKUS;

/* W3.06  Table of Menu Options */
typedef struct {
   char pcAfpn[21];		          /*   Menu Table of (upto) 10 AFPNs */
   char pcOption[201];            /*   Menu Table of (upto) 10 AFPN Descs*/
} MENU_OPTIONS;

#ifdef _THIS_SOURCE_HAS_THE_DATA_
   #define EXT
#else
   #define EXT extern
#endif

EXT int iCurrentState[MAX_SOCKETS];

EXT fd_set readfds;
//EXT struct timeval  sSelTime;
EXT WSADATA WsaData;

EXT char pcSpaces[SCREEN_COLS + 1];

EXT struct timeval sSelTime;

EXT INUSE_TYPE sInUse[MAX_SOCKETS];
EXT USER_TYPE sUser[MAX_SOCKETS];
EXT SVUSER_TYPE svUser[MAX_SOCKETS];					/*W2.08*/

EXT COUNTED_SKUS pcCount[MAX_SOCKETS];				/*W2.08*/
EXT GAPWALK_SKUS pcGapTable[MAX_SOCKETS];			/*W3.05*/
EXT MENU_OPTIONS pcMenuTable[MAX_SOCKETS];			/*W3.06*/

EXT FILE *hCommandFile;

EXT	char buffer[30];
EXT struct tm pNow;
EXT time_t MyTime;
EXT void  AlertUser(HWND,LPSTR);
EXT long lTimeOutValue;		 /* Menu Timeout Value(secs)W3.02	*/
EXT SQLConnection cn;
EXT SQLConnection cnRS[MAX_SOCKETS];		// For persistant connections between HHT packets


