
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <winerror.h>
#include <winGDI.h>
#include "WinDisplay.h"

#include "ProcessLog.h"


char ProcessLogFileName[161];
int near ProcessLogDeviceType=ProcessLogNone;	// Where the process log goes
FILE *hProcessLogHandle;											// Either stdout, or a valid open 
																							//	or closed handle
#define PROCESS_LOG_COLS 81
#define PROCESS_LOG_ROWS 51

char _processLogScreenLine[PROCESS_LOG_ROWS][PROCESS_LOG_COLS+1];
int _processLogCol=0;
int _processLogRow=0;
BOOL _logScreenUpdateStatus=FALSE;
BOOL _logToScreen=TRUE;


//----------------------------------------------------------------------------
// Process log routines (for debugging)
//----------------------------------------------------------------------------


void ProcessLogSetFilename(char *fileName)
{
strcpy(ProcessLogFileName,fileName);
}

int ProcessLogInitialize(void)		// Also used by process log stop
{
	if ((hProcessLogHandle=fopen(ProcessLogFileName,"a+"))==NULL) {	// Open log to get valid handle
		hProcessLogHandle=stdout;
	} else {			
		fclose(hProcessLogHandle);														// Close it
	}
	ProcessLogDeviceType=ProcessLogNone;										// Init sets to no log
	return(TRUE);
}

int ProcessLogDevice()
{
return (ProcessLogDeviceType);
}

FILE *ProcessLogHandle(void)
{
	if (hProcessLogHandle==NULL) {
		return(stdout);
	}
	ProcessLogFlushBuffer();
	return(hProcessLogHandle);
}

void ProcessLogCloseFile()
{
	if ((hProcessLogHandle!=NULL) && (hProcessLogHandle!=stdout)) {
		
		fclose(hProcessLogHandle);
	}
}

void ProcessLogFlushBuffer(void)
{
	if (ProcessLogDeviceType==ProcessLogFile) {
		ProcessLogCloseFile();
		hProcessLogHandle=fopen(ProcessLogFileName,"a+");
	}
}

void ProcessLogStartToFile(int append)
{
	char msg[161];
	
	strcpy(msg,"w+");
	if (append) {
		strcpy(msg,"a+");
	}
	ProcessLogCloseFile();
	hProcessLogHandle=fopen(ProcessLogFileName,msg);
	ProcessLogDeviceType=ProcessLogFile;
	sprintf(msg,"Process Log started - %s",ProcessLogFileName);	
	printf(msg);
	if (append) {
		printf("Adding to log file\n");		// to the console to let user know
	} else {
		printf("Rewriting log file\n");
	}
	ProcessLogWriteLn(msg);
}

void ProcessLogStartToConsole(void)
{
	ProcessLogCloseFile();
	hProcessLogHandle=stdout;
	ProcessLogDeviceType=ProcessLogConsole;
	fprintf(hProcessLogHandle,"Process Log started - Console only");
}

void ProcessLogStop(void)
{
	ProcessLogWriteLn("Process log stopped");
	ProcessLogCloseFile();
	ProcessLogInitialize();
}

int ProcessLogWrite(char *msg)
{
	if (hProcessLogHandle!=NULL) {
		//fprintf(hProcessLogHandle,msg);
	}
	ProcessLogFlushBuffer();
		{	// For screen logging
		int i;
		for (i=0 ; i<(int)strlen(msg) ; i++) {
			if ((msg[i]=='\n') || (_processLogCol>=PROCESS_LOG_COLS-1)) {
				_processLogScreenLine[_processLogRow][_processLogCol]=0;
				_processLogCol=0;
				if (++_processLogRow>=PROCESS_LOG_ROWS) {
					memcpy(&_processLogScreenLine[0][0],&_processLogScreenLine[1][0],PROCESS_LOG_COLS*PROCESS_LOG_ROWS-1);
					_processLogRow=PROCESS_LOG_ROWS-1;
				}
			}
			if (msg[i]!='\n') {
				_processLogScreenLine[_processLogRow][_processLogCol++]=msg[i];
			}
			_processLogScreenLine[_processLogRow][_processLogCol]=0;
		}
		_processLogScreenLine[_processLogRow][_processLogCol]=0;
	}
	_logScreenUpdateStatus=TRUE;
	return (1);
}

int ProcessLogWriteLn(char *msg)
{

	ProcessLogWrite(msg);
	return(ProcessLogWrite("\n"));

}

BOOL ProcessLogWriteToScreenStale()
{
	return(_logScreenUpdateStatus);
}

void SetLogToScreen(BOOL on)
{
	_logToScreen=on;
}

BOOL GetLogToScreen()
{
	return(_logToScreen);
}

void ProcessLogWriteToScreen()
{
	int i,lineIndex;
	char s[12];
	WinDisplayFrame Wf;
	
	if (!GetLogToScreen()) {
		return;
	}

	DrawSetupClientWindow(&Wf);
	lineIndex=Wf.rows-1;
	if (lineIndex<_processLogRow) {
		lineIndex=_processLogRow;
	}
	for (i=Wf.rows-1 ; i>=0 ; i--, lineIndex--) {
		sprintf(s,"%d %d",i,lineIndex);
		//WinDisplay_DisplayStr(&Wf,0,i,s);
		//WinDisplay_EraseLine(&Wf,i);
		if (lineIndex<=_processLogRow) {
			WinDisplay_EraseLine(&Wf,i);
			WinDisplay_DisplayLeftJustified(&Wf,0,i,_processLogScreenLine[lineIndex]);
		}
	}
	_logScreenUpdateStatus=FALSE;

}
