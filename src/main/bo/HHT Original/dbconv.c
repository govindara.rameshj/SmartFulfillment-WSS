//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:dbconv.c - Routines for database field conversions, debug logging,
//									 and low level database access.
// This source module contains procedures and functions that were added
// while updating Wixstep to used the new (11/12/2004) database formats.
// These routines are used for date and time conversions, database access,
// process logging, and conversion between DB formats and Wixstep internal
// formats (mostly string).
//----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"
#include <math.h>
//----------------------------------------------------------------------------
// Module level variables
//----------------------------------------------------------------------------

char near DBConvTempStr[41];	// To allow return values with NULL input params

int near ProcessLogDeviceType=ProcessLogNone;	// Where the process log goes
FILE *hProcessLogHandle=stdout;								// Either stdout, or a valid open
																							//	or closed handle

// This buffer is used to pass keys to Btrieve and to store key information between successive calls like
//		GET_NEXT. This buffer is always passed by DBAccessTableData. If the keyBuf parameter to
//		DBAccessTableData is not NULL, it is copied to AccessKeyBuffer before the call.

#define AccessKeyBufferSize 512
char AccessKeyBuffer[AccessKeyBufferSize];

// Called by functions that return a pointer and have an optional return
//   parameter. If the return parameter is not NULL, this function will
//   update the return parameter and return its location. If the return
//   parameter is NULL, the global variable DBConvTempStr had the return
//   information and its address is returned.

char *DefaultRetValue(char *StackRetParam, size_t bufferSize)
{
	if (StackRetParam!=NULL) {
		memcpy(StackRetParam,DBConvTempStr,bufferSize);
		((char *)StackRetParam)[bufferSize] = '\0';
		return((char *)(StackRetParam));
	}
	return ((char *)(DBConvTempStr));
}

void *DefaultRetValue(void *StackRetParam, size_t bufferSize)
{
	if (StackRetParam!=NULL) {
		memcpy(StackRetParam,DBConvTempStr,bufferSize);
		//((char *)StackRetParam)[bufferSize] = '\0';
		return((char *)(StackRetParam));
	}
	return ((char *)(DBConvTempStr));
}

//----------------------------------------------------------------------------
// Date and time routines
//----------------------------------------------------------------------------

DBLongDate *GetCurrentDateInDBLongDateFormat(DBLongDate *RetVar)
{
	DBLongDate *date;
	struct tm pNow;
	time_t MyTime;

	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	date=(DBLongDate *)(DBConvTempStr);
	date->day=pNow.tm_mday;
	date->month=pNow.tm_mon+1;
	date->year=pNow.tm_year+1900;
	return((DBLongDate *)(DefaultRetValue(RetVar,sizeof(DBLongDate))));
}

char *GetCurrentDateTimeMsg(char *RetVar)
{

	struct tm pNow;
	time_t MyTime;
  MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
  strftime(DBConvTempStr,sizeof(DBConvTempStr),"%H:%M:%S on %d/%m/%y ",&pNow);
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));

}

char *GetCurrentDateInDDMMYY(char *RetVar)
{

	struct tm pNow;
	time_t MyTime;

	MyTime=time(NULL);
	localtime_s(&pNow, &MyTime);

	strftime(DBConvTempStr,sizeof(DBConvTempStr),"%d/%m/%y",&pNow);
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));

}
char *GetCurrentDateInDDMMYYYY(char *RetVar)
{

	struct tm pNow;
	time_t MyTime;

	MyTime=time(NULL);
	localtime_s(&pNow, &MyTime);

	strftime(DBConvTempStr,sizeof(DBConvTempStr),"%d/%m/%Y",&pNow);
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));

}

char *GetCurrentTimeStr(char *RetVar)
{

	struct tm pNow;
	time_t MyTime;

	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	strftime(DBConvTempStr,sizeof(DBConvTempStr),"%H%M%S",&pNow);
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));

}

void GetDBLongDateToDBField(void *DBField, DBLongDate *date)
{

	memcpy(DBField, date, sizeof(DBLongDate));

}
void GetDBFieldToDBLongDate(DBLongDate *date, void *DBField)
{

	memcpy(date, DBField, sizeof(DBLongDate));

}

char *GetDBLongDateFieldToStr(char *RetVar, DBLongDate *DBDate)
{

	//fprintf(ProcessLogHandle(),"(DBLongDate->Str=%s - ",DBBufferToHexStr(DBConvTempStr,DBDate,sizeof(DBLongDate)));
	sprintf_s(DBConvTempStr, sizeof(DBConvTempStr),"%02i/%02i/%04i", (int)(DBDate->day),(int)(DBDate->month), (int)(DBDate->year));
	//fprintf(ProcessLogHandle(),"%s)\n",DBConvTempStr);
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));

}

// Returns DBLongDate pointer

#define IsLeapYear(_yr) ((!((_yr) % 400) || ((_yr) % 100) && !((_yr) % 4)) ? TRUE : FALSE)

int DaysInMonth(int month, int year)
{
	int months[]= {	31,28,31,	30,31,30,	31,31,30,	31,30,31};
	if ((month>=1) && (month<=12)) {
		if ((month==2) && (IsLeapYear(year))) {
			return(29);
		}
		return(months[month-1]);
	}
	return(0);
}

DBLongDate *GetStrToDBLongDateField(DBLongDate *retDate, char *strDate)
{
	DBLongDate *date;
	int WkMonth;
	int WkDay;
	int WkYear;

	memset(DBConvTempStr,0,sizeof(DBLongDate));	// For Error, return 0 date

	//printf("GetStrToDBLongDateField String = '%s'\n",strDate);

	if (strDate[0]==0) {
		goto LERR;
	}
	sscanf_s(strDate,"%d/%d/%d", &WkDay, &WkMonth, &WkYear); // UK version

	//printf("%d/%d/%d\n ", WkDay, WkMonth, WkYear);

	if ((WkMonth != 0) ||	(WkDay != 0) ||	(WkYear != 0)) {
		if (WkYear < 100) {
			if (WkYear <= 50) {
				WkYear += 2000;
			} else {
				WkYear += 1900;
			}
		}

		if ((WkMonth<1) || (WkMonth>12) || (WkDay<1)) {
			goto LERR;	// Bad Date
		}
		if ((WkMonth == 4) || (WkMonth == 6) || (WkMonth == 9) ||	(WkMonth == 11)) {
			if (WkDay > 30) {
				goto LERR;	// Bad Date
			}
		} else if (WkMonth == 2) {
			if (IsLeapYear(WkYear)) {
				if (WkDay > 29) {
					goto LERR;	// Bad Date
				}
			} else {
				if (WkDay > 28) {
					goto LERR;	// Bad Date
				}
			}
		} else {
			if (WkDay > 31) {
					goto LERR;	// Bad Date
			}
		}
		date=(DBLongDate*)(DBConvTempStr);
		date->day=(unsigned char)WkDay;
		date->month=(unsigned char)WkMonth;
		date->year=(short)WkYear;
	}
LERR:
	return((DBLongDate *)DefaultRetValue(retDate,sizeof(DBLongDate)));
}

int DBLongDateCompare(DBLongDate *d1, DBLongDate *d2)
{
	//if (memcmp(d1,d2,sizeof(DBLongDate))==0) {
	//	return(0);	// Same
	//}
	if (d1->year < d2->year) return(-1);
	if (d1->year > d2->year) return(1);
	if (d1->month < d2->month) return(-1);
	if (d1->month > d2->month) return(1);
	if (d1->day < d2->day) return(-1);
	if (d1->day > d2->day) return(1);
	return(0);		// Same date
}

unsigned long GetCurrentSeconds(void)
{

	struct tm pNow;
	time_t MyTime;

 	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	return (pNow.tm_hour * 60L * 60L + pNow.tm_min * 60L + pNow.tm_sec);

}

// Returns number of days since 01/01/1900

unsigned long DBLongDateToDayNumber(DBLongDate *date)
{
	int i;
	unsigned long dayNumber=0;

	if ((!date->day) || (!date->month) || (!date->year)) {
		return(0);
	}
	// Get days in months before this one
	for (i=1 ; i<date->month ; i++) {
		dayNumber+=(unsigned long)DaysInMonth(i,date->year);
	}
	// Add days in this month
  dayNumber+=(unsigned long)date->day;
	// Add days in years before this one
	dayNumber+=(unsigned long)((date->year-1900)*365.25);
	//printf("%d, %d, %d = %d",date->day,date->month,date->year,dayNumber);
	return(dayNumber);
}

unsigned long DateStrToDayNumber(char *DateStr)
{
	DBLongDate date;
	//printf("Date=%s\n",DateStr);
	GetStrToDBLongDateField(&date,DateStr);
	return (DBLongDateToDayNumber(&date));
}

BOOL DayNumberToDBLongDate(DBLongDate *Date, long DayNumber)
{

	int day, month,year;

	year=(int)floor((double)DayNumber/365.25);
	day=DayNumber-(int)floor(((double)year*365.25))+1;
	year+=1900;

	month=1;
	while ((day>DaysInMonth(month,year)) && (month<=12)) {
		day-=DaysInMonth(month++,year);
	}
	//day++;
	Date->day=(char)day;
	Date->month=(char)month;
	Date->year=year;
	return(1);

}

BOOL DayNumberToDateStr(char *RetVar, long DayNumber)
{

	DBLongDate date;
	DayNumberToDBLongDate(&date,DayNumber);
	GetDBLongDateFieldToStr(RetVar,&date);
	return(1);

}
//----------------------------------------------------------------------------
// Character conversion routines
//----------------------------------------------------------------------------

char *GetDBCharFieldToStr(char *RetVar, void *DBField, int DBFieldLength)
{
	memcpy(RetVar,DBField,DBFieldLength);
	RetVar[DBFieldLength]=0;

	//fprintf(ProcessLogHandle(),"(DBChar->Str='%s')\n",RetVar);
	return (RetVar);

}
char *GetStrToDBCharField(void *DBFieldRet, char *Str, size_t DBFieldLength)
{

	memset(DBFieldRet,' ',DBFieldLength);
	if (DBFieldLength>strlen(Str)) {	// just copy chars to DB field
		DBFieldLength=strlen(Str);
	}
	memcpy(DBFieldRet,Str,DBFieldLength);
	//fprintf(ProcessLogHandle(),"(Str->DBChar='%s')\n",Str);
	return ((char *)(DBFieldRet));
}

void StripSpaces(char *retVar, size_t retVarSize, char *Field)
{
	int i;
	strcpy_s(retVar, retVarSize,Field);
	// Spaces at the end
	i=strlen(retVar)-1;
	while ((i>=0) && (retVar[i]==32)) {
		retVar[i--]=0;
	}
	// Spaces at the beginning
	while ((strlen(retVar)>0) && (retVar[0]==32)) {
		strcpy_s(retVar, retVarSize, retVar+1);
	}
}

char *ZeroPadField(char *RetVar, char *Field, int length)
{
	char s[64];
	int fLength;
	if (strlen(Field)>63) {
		Field[63]=0;
	}
	if (length>63) {
		length=63;
	}
	StripSpaces(s, sizeof(s), Field);
	fLength=strlen(s);
	if (fLength>length) {
		fLength=length;
	}
	memset(DBConvTempStr,'0',length);
	if (fLength>0) {
		memcpy(DBConvTempStr+(length-fLength),s,fLength);
	}
	DBConvTempStr[length]=0;
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));
}

char *ZeroPadField(char *Field, int length)
{
	char s[64];
	int fLength;
	if (strlen(Field)>63) {
		Field[63]=0;
	}
	if (length>63) {
		length=63;
	}
	StripSpaces(s, sizeof(s), Field);
	fLength=strlen(s);
	if (fLength>length) {
		fLength=length;
	}
	memset(DBConvTempStr,'0',length);
	if (fLength>0) {
		memcpy(DBConvTempStr+(length-fLength),s,fLength);
	}
	DBConvTempStr[length]=0;
	return(DBConvTempStr);
}

char *ZeroPadField(char *RetVar, int Field, int length)
{
	char s[21];
	_itoa_s(Field, s, sizeof(s), 10);
	return(ZeroPadField(RetVar, s, length));
}

char *DBstrncpy(char *Dest, char *Source, int SizeOfDest)
{

	//printf("**'%s', '%s', %d -- ",Dest,Source,SizeOfDest);
	if (SizeOfDest>1) {
		strncpy_s(Dest, SizeOfDest,Source,SizeOfDest-1);	// will pad short strings
		Dest[SizeOfDest-1]=0;
	} else {
		Dest[0]=0;
	}
	//printf("%s', '%s', %d\n",Dest,Source,SizeOfDest);

	return(Dest);
}

//----------------------------------------------------------------------------
// Logical Field conversion routines
//----------------------------------------------------------------------------

char *GetDBLogicalFieldToStr(char *RetVar, void *DBField)
{
	char *p=(char *)(DBField);

	RetVar[0]=0;				// Default to false
	if (*p) {
		RetVar[0]='Y';		// Make it true with non-zero, use 'Y' so it displays
	}
	RetVar[1]=0;				// Terminate the string
	return (RetVar);
}

void SetDBLogicalField(char *DBField, char Value)
{
	if (Value) {
		*DBField=32;
	} else {
		*DBField=0;
	}
	//fprintf(ProcessLogHandle(),"(DBLogical->'%d'\n",*DBField);
}

//----------------------------------------------------------------------------
// Identity fields conversion routines
//----------------------------------------------------------------------------

void GetDBIdentityField(void *RetVar, void *DBField)
{
	memcpy(RetVar,DBField,4);
}

//----------------------------------------------------------------------------
// Decimal/Numeric Field conversion routines (BCD to and from long and string)
//----------------------------------------------------------------------------

long GetDBDecFieldToLong(char *DBField, short DBFieldLength)
{
/*
long li;
	li=atol(GetDBDecFieldToStr(NULL,DBField,DBFieldLength,0));
	//fprintf(ProcessLogHandle(),"(DBDec->Long='%d')\n",li);
	return(li);
	*/
	return(0);
}

void *GetLongToDBDecField(void *DBFieldRet, signed long Value, int DBFieldLength)
{
	int index;
	char s[32];
	char *sp;
	char *dp;
	sprintf_s(s, sizeof(s),"%*ld",DBFieldLength*2-1,labs(Value));
	sp=s;
	dp=DBConvTempStr;
	for (index=0 ; index<DBFieldLength ; index++, dp++) {
		*dp=0;
		if (*sp>='0') {
			*dp=(*sp-'0')<<4;
		}
		sp++;
		if (index==DBFieldLength-1) {	// Time for sign
			if (Value<0) {
				*dp+=0x0d;
			} else {
				*dp+=0x0f;
			}
		} else {
			if (*sp>='0') {
				*dp+=(*sp-'0');
			}
		}
		sp++;
	}
	//fprintf(ProcessLogHandle(),"(Long->DBDec='%ld' -> '",Value);
	//fprintf(ProcessLogHandle(),"')\n");

	return(DefaultRetValue(DBFieldRet,DBFieldLength));
}

void *GetStrToDBDecField(unsigned char *DBFieldRet, char *Value, short DBFieldLength, char Decimal)
{

	char *endStr;
	double dValue,mult;
	long lValue;
	dValue=strtod(Value,&endStr);
	mult=1;//pow(10,(int)(Decimal));
	lValue=(long)(dValue*mult);
	return(GetLongToDBDecField(DBFieldRet,lValue,DBFieldLength));

}
char *GetDBDecFieldToStr(char *RetVar, unsigned char *Data, short Size, char Decimal)
{
	char WkRet[28];
	int RetPos = 0;
	char RetStarted = FALSE;
	short StopPoint = (Size - 1);
	short DecimalPoint;
	int Count;
	char DecimalTop = (Decimal & 0x01);
	char Negative = FALSE;

	DecimalPoint = (Size - (1 + (Decimal >> 1)));
	if (Decimal == 0) DecimalPoint = Size;

	for (Count = 0; Count <= StopPoint; Count++) {
//		See if Decimal goes here
		if (Count == DecimalPoint) {
			if (DecimalTop) {
				if (RetPos == 0) {
					WkRet[RetPos++] = '0';
				}
				WkRet[RetPos++] = '.';
				RetStarted = TRUE;
			}
		}
		if (((Data[Count] >> 4) != 0) || (RetStarted)) {
			WkRet[RetPos++] = ((Data[Count] >> 4) + '0');
			RetStarted = TRUE;
		}
//		See if Decimal goes here
		if (Count == DecimalPoint) {
			if (!DecimalTop) {
				if (RetPos == 0) {
					WkRet[RetPos++] = '0';
				}
				WkRet[RetPos++] = '.';
				RetStarted = TRUE;
			}
		}
		if (Count == StopPoint) {
			if ((Data[Count] & 0x0F) == 0x0D) {
				Negative = TRUE;
//				WkRet[RetPos++] = '-';
			}
		} else {
			if (((Data[Count] & 0x0F) != 0) || (RetStarted)) {
				WkRet[RetPos++] = ((Data[Count] & 0x0F) + '0');
				RetStarted = TRUE;
			}
		}
	}
	if (RetPos == 0) {
		//RetPos=Size*2-2;		// Data is zero so left pad '0'
		//if (RetPos>0) {
		//	memset(WkRet,' ',RetPos);
		//}
		WkRet[RetPos++]='0';
	}
	WkRet[RetPos] = '\0';
	if (Negative==TRUE) {
		DBConvTempStr[0]='-';
		strcpy_s(DBConvTempStr+1, sizeof(DBConvTempStr)-1,WkRet);
	} else {
		strcpy_s(DBConvTempStr, sizeof(DBConvTempStr),WkRet);
	}
	if (RetVar) {
		strcpy_s(RetVar, sizeof(RetVar),DBConvTempStr);
	}

	//fprintf(ProcessLogHandle(),"(DBDec->Str='");
	//ProcessLogDumpBuffer("",Data,Size);
	//fprintf(ProcessLogHandle(),"' -> '%s')\n",DBConvTempStr);

	/*
	{
		char s[sizeof(USER_TYPE)];
		ProcessLogWriteLn(DBBufferToPrintableStr(s,&sUser[GetUserID()],sizeof(USER_TYPE)));
	}
	*/
	return(DefaultRetValue(RetVar,strlen(DBConvTempStr)));
}


//----------------------------------------------------------------------------
// Process log routines (for debugging)
//----------------------------------------------------------------------------

char ProcessLogFileName[161];

void ProcessLogSetFilename(char *fileName)
{
	strcpy_s(ProcessLogFileName, sizeof(ProcessLogFileName),fileName);
}

int ProcessLogInitialize(void)		// Also used by process log stop
{
	if (fopen_s(&hProcessLogHandle, ProcessLogFileName, "a+") !=0 ) {	// Open log to get valid handle
		hProcessLogHandle=stdout;
	} else {
		fclose(hProcessLogHandle);														// Close it
	}
	ProcessLogDeviceType=ProcessLogNone;										// Init sets to no log
	return(TRUE);
}

int ProcessLogDevice()
{
return (ProcessLogDeviceType);
}

FILE *ProcessLogHandle(void)
{
	if (hProcessLogHandle==NULL) {
		return(stdout);
	}
	ProcessLogFlushBuffer();
	return(hProcessLogHandle);
}

void ProcessLogCloseFile()
{
	if ((hProcessLogHandle!=NULL) && (hProcessLogHandle!=stdout)) {

		fclose(hProcessLogHandle);
	}
}

void ProcessLogFlushBuffer(void)
{
	if (ProcessLogDeviceType==ProcessLogFile) {
		ProcessLogCloseFile();
		fopen_s(&hProcessLogHandle, ProcessLogFileName, "a+");
	}
}

void ProcessLogStartToFile(int append)
{
	char msg[161];

	strcpy_s(msg, sizeof(msg),"w+");
	if (append) {
		strcpy_s(msg, sizeof(msg),"a+");
	}
	ProcessLogCloseFile();
	fopen_s(&hProcessLogHandle, ProcessLogFileName, msg);
	ProcessLogDeviceType=ProcessLogFile;
	sprintf_s(msg, sizeof(msg),"Process Log started - %s @ %s",ProcessLogFileName,GetCurrentDateTimeMsg(NULL));
	printf(msg);
	if (append) {
		printf("Adding to log file\n");		// to the console to let user know
	} else {
		printf("Rewriting log file\n");
	}
	ProcessLogWriteLn(msg);
}

void ProcessLogStartToConsole(void)
{
	ProcessLogCloseFile();
	hProcessLogHandle=stdout;
	ProcessLogDeviceType=ProcessLogConsole;
	fprintf(hProcessLogHandle,"Process Log started - Console only");
}

void ProcessLogStop(void)
{
	char msg[161];
	sprintf_s(msg, sizeof(msg),"Process log stopped @ %s",GetCurrentDateTimeMsg(NULL));
	ProcessLogWriteLn(msg);
	ProcessLogCloseFile();
	ProcessLogInitialize();
	printf("Logging Stopped\n");
}

int ProcessLogWriteTimeStamp(char *msg)
{
	int ret;
	ret=ProcessLogWrite(GetCurrentDateTimeMsg(NULL));
	if (msg) {
		ret=ProcessLogWrite(msg);
	}
	return(ret);
}

#define PROCESS_LOG_COLS 81
#define PROCESS_LOG_ROWS 51

char _processLogScreenLine[PROCESS_LOG_ROWS][PROCESS_LOG_COLS+1];
int _processLogCol=0;
int _processLogRow=0;
BOOL _logScreenUpdateStatus=FALSE;
BOOL _logToScreen=TRUE;

int ProcessLogWrite(char *msg)
{
	if (hProcessLogHandle!=NULL) {
		fprintf(hProcessLogHandle,msg);
	}
	ProcessLogFlushBuffer();
		{	// For screen logging
		int i;
		for (i=0 ; i<(int)strlen(msg) ; i++) {
			if ((msg[i]=='\n') || (_processLogCol>=PROCESS_LOG_COLS-1)) {
				_processLogScreenLine[_processLogRow][_processLogCol]=0;
				_processLogCol=0;
				if (++_processLogRow>=PROCESS_LOG_ROWS) {
					memcpy(&_processLogScreenLine[0][0],&_processLogScreenLine[1][0],PROCESS_LOG_COLS*PROCESS_LOG_ROWS-1);
					_processLogRow=PROCESS_LOG_ROWS-1;
				}
			}
			if (msg[i]!='\n') {
				_processLogScreenLine[_processLogRow][_processLogCol++]=msg[i];
			}
			_processLogScreenLine[_processLogRow][_processLogCol]=0;
		}
		_processLogScreenLine[_processLogRow][_processLogCol]=0;
	}
	_logScreenUpdateStatus=TRUE;
	return (1);
}

int ProcessLogWriteLn(char *msg)
{

	ProcessLogWrite(msg);
	return(ProcessLogWrite("\n"));

}

BOOL ProcessLogWriteToScreenStale()
{
	return(_logScreenUpdateStatus);
}

void SetLogToScreen(BOOL on)
{
	_logToScreen=on;
}

BOOL GetLogToScreen()
{
	return(_logToScreen);
}

void ProcessLogWriteToScreen()
{
	int i,lineIndex;
	char s[12];
  RECT rect;
	WinDisplayFrame Wf;
	if (!GetLogToScreen()) {
		return;
	}
	GetClientRect(GetMainWindowHandle(), &rect);
	WinDisplay_CreateFrame(&Wf,GetMainWindowHandle(),&rect);
	//rect.top=Wf.charHeight*3;
	rect.right-=DisplaySocketStatusWidth*Wf.charWidth;
	rect.bottom-=Wf.charHeight*3;
	WinDisplay_ResizeFrame(&Wf,&rect);
	WinDisplay_EraseFrame(&Wf);
	WinDisplay_DrawFrame(&Wf);
	lineIndex=Wf.rows-1;
	if (lineIndex<_processLogRow) {
		lineIndex=_processLogRow;
	}
	for (i=Wf.rows-1 ; i>=0 ; i--, lineIndex--) {
		sprintf_s(s, sizeof(s),"%d %d",i,lineIndex);
		//WinDisplay_EraseLine(&Wf,i);
		if (lineIndex<=_processLogRow) {
			//WinDisplay_ChangeCurrentFontSizeMultiplier(&Wf,.10+(double)(i)*0.10);
			WinDisplay_DisplayLeftJustified(&Wf,0,i,_processLogScreenLine[lineIndex]);
			//WinDisplay_ResetCurrentFontSizeMultiplier(&Wf);
		}
	}
	_logScreenUpdateStatus=FALSE;

}

int ProcessLogDumpBuffer(char *msg, void *buffer, int length)
{

	#define MAX_PD_BUFFER_SIZE	1024
	char s[MAX_PD_BUFFER_SIZE];

	if (length>=MAX_PD_BUFFER_SIZE) {
		length=MAX_PD_BUFFER_SIZE-1;
	}
	ProcessLogWrite(msg);
	DBBufferToPrintableStr(s,buffer,length,FALSE);
	return(ProcessLogWrite(s));

}

char *DBBufferToPrintableStr(char *RetVar, void *buffer, int length, int toHex)
{
	int i;
	char *p;

	p=(char *)(buffer);
	for (i=0 ; i<length ; i++, p++) {
		if ((*p>31) && (*p<128)) {
			RetVar[i]=*p;
		} else  {
			if (toHex) {
				sprintf_s(RetVar+i, sizeof(RetVar) - 1, "\\%02X",*p);	// do the hex value
				i+=2;
			}
			else {
				RetVar[i]='.';									// just do a dot
			}
		}
	}
	RetVar[length-1]=0;
	return(RetVar);

}

char *DBBufferToHexStr(char *RetVar, void *buffer, int length)
{
	int i;
	char *p;

	p=(char *)buffer;
	for (i=0 ; i<length ; i++, p++) {
		sprintf_s(RetVar+i*2, sizeof(RetVar) - 1*2,"%02X",*p);
	}
	return(RetVar);
}

char *DBBufferAddressToString(char *RetVar, void *buffer)
{
	sprintf_s(RetVar, sizeof(RetVar),"%08X",buffer);
	return(RetVar);
}
/*
//----------------------------------------------------------------------------
// Process log routines (for debugging)
//----------------------------------------------------------------------------

int ProcessLogInitialize()		// Also used by process log stop
{
	hProcessLogHandle=fopen(BASE_PATH "WIXSTEP.LOG","a+");	// Open log to get valid handle
	fclose(hProcessLogHandle);															// Close it
	ProcessLogDeviceType=ProcessLogNone;										// Init sets to no log
	return(TRUE);
}

int ProcessLogDevice()
{
return (ProcessLogDeviceType);
}

FILE *ProcessLogHandle(void)
{
	if (hProcessLogHandle==NULL) {
		return(stdout);
	}
	ProcessLogFlushBuffer();
	return(hProcessLogHandle);
}

void ProcessLogCloseFile()
{
	if ((hProcessLogHandle!=NULL) && (hProcessLogHandle!=stdout)) {
		fclose(hProcessLogHandle);
	}
}

void ProcessLogFlushBuffer(void)
{
	if (ProcessLogDeviceType==ProcessLogFile) {
		ProcessLogCloseFile();
		hProcessLogHandle=fopen(BASE_PATH "WIXSTEP.LOG","a+");
	}
}

void ProcessLogStartToFile(int append)
{
	char msg[161];

	strcpy(msg,"w+");
	if (append) {
		strcpy(msg,"a+");
	}
	ProcessLogCloseFile();
	hProcessLogHandle=fopen(BASE_PATH "WIXSTEP.LOG",msg);
	ProcessLogDeviceType=ProcessLogFile;
	sprintf_s(msg,"\nProcess Log started - %sWIXSTEP.LOG @ %s\n",BASE_PATH,GetCurrentDateTimeMsg(NULL));
	printf(msg);
	if (append) {
		printf("Adding to log file\n");		// to the console to let user know
	} else {
		printf("Rewriting log file\n");
	}
	ProcessLogWriteLn(msg);
}

void ProcessLogStartToConsole(void)
{
	ProcessLogCloseFile();
	hProcessLogHandle=stdout;
	ProcessLogDeviceType=ProcessLogConsole;
	fprintf(hProcessLogHandle,"\nProcess Log started - Console only\n");
}

void ProcessLogStop(void)
{
	char msg[161];
	sprintf_s(msg,"Process log stopped @ %s\n",GetCurrentDateTimeMsg(NULL));
	ProcessLogWriteLn(msg);
	ProcessLogCloseFile();
	ProcessLogInitialize();
	printf("Logging Stopped\n");
}

int ProcessLogWriteTimeStamp(char *msg)
{
	int ret;
	ret=ProcessLogWrite(GetCurrentDateTimeMsg(NULL));
	if (msg) {
		ret=ProcessLogWrite(msg);
	}
	return(ret);
}

int ProcessLogWrite(char *msg)
{
	fprintf(hProcessLogHandle,msg);
	ProcessLogFlushBuffer();
	return (1);
}

int ProcessLogWriteLong(long li)
{
	char s[32];
	ltoa(li,s,10);
	return(ProcessLogWrite(s));
}

int ProcessLogWriteLn(char *msg)
{

	ProcessLogWrite(msg);
	return(ProcessLogWrite("\n"));

}

int ProcessLogDumpBuffer(char *msg, void *buffer, int length)
{

	#define MAX_PD_BUFFER_SIZE	1024
	char s[MAX_PD_BUFFER_SIZE];

	if (length>=MAX_PD_BUFFER_SIZE) {
		length=MAX_PD_BUFFER_SIZE-1;
	}
	ProcessLogWrite(msg);
	DBBufferToPrintableStr(s,buffer,length,FALSE);
	return(ProcessLogWrite(s));

}

char *DBBufferToPrintableStr(char *RetVar, void *buffer, int length, int toHex)
{
	int i;
	char *p;

	p=(char *)(buffer);
	for (i=0 ; i<length ; i++, p++) {
		if ((*p>31) && (*p<128)) {
			RetVar[i]=*p;
		} else  {
			if (toHex) {
				sprintf_s(RetVar+i,"\\%02X",*p);	// do the hex value
				i+=2;
			}
			else {
				RetVar[i]='.';									// just do a dot
			}
		}
	}
	RetVar[length-1]=0;
	return(RetVar);

}

char *DBBufferToHexStr(char *RetVar, void *buffer, int length)
{
	int i;
	char *p;

	p=(char *)(buffer);
	for (i=0 ; i<length ; i++, p++) {
		sprintf_s(RetVar+i*2,"%02X",*p);
	}
	return(RetVar);
}

char *DBBufferAddressToString(char *RetVar, void *buffer)
{
	sprintf_s(RetVar,"%08X",buffer);
	return(RetVar);

}
*/
//----------------------------------------------------------------------------
// Database access routines
//----------------------------------------------------------------------------

/*
BTI_SINT DBInitTable(DataBaseTable *TableInfo, char *TableName, BTI_VOID_PTR DataBuffer, int DataBufferLength)
{

	//char msg[128];

	memset(TableInfo,0,sizeof(DataBaseTable));
	strcpy(TableInfo->Name,TableName);
	TableInfo->OpenStatus=B_NO_ERROR;
	TableInfo->Opened=0;
	TableInfo->DataLength=DataBufferLength;
	TableInfo->DataBuffer=DataBuffer;
	//sprintf_s(msg,"INIT %s, Data Length=%d, Buffer=%08X",TableInfo->Name,TableInfo->DataLength,TableInfo->DataBuffer);
	//ProcessLogWriteLn(msg);

	return (TableInfo->OpenStatus);
}
*/

/*
BTI_SINT DBOpenTable(DataBaseTable *TableInfo, int UserIndex)
{

	char fName[128];
	char msg[128];
	char OwnerName[2];
	BTI_WORD OwnerNameLength=0;

	if (TableInfo->Opened==0) {
		OwnerName[0]=0;
    strcpy(fName,DATABASE_PATH);
		strcat(fName,TableInfo->Name);
		OwnerNameLength=0;
    TableInfo->OpenStatus=BTRVID(B_OPEN,TableInfo->Params,OwnerName,&OwnerNameLength,fName,0,&dbUserID);
    if (TableInfo->OpenStatus!=B_NO_ERROR) {
			strcpy(fName, DATABASE_PATH);
			strcat(fName,TableInfo->Name);
			strcat(fName,".dta");
			OwnerNameLength=0;
			TableInfo->OpenStatus=BTRVID(B_OPEN,TableInfo->Params,OwnerName,&OwnerNameLength,fName,0,&dbUserID);
		}
	}
  if (TableInfo->OpenStatus == B_NO_ERROR) {
		TableInfo->Opened+=1;
    strcpy(sUser[UserIndex].pcLastInput, "1");
	} else {
    strcpy(sUser[UserIndex].pcLastInput, "0");
	}
	sprintf_s(msg,"B_OPEN %s, Data Length=%d, Status=%d, Opened=%d",
							fName,TableInfo->DataLength,TableInfo->OpenStatus,TableInfo->Opened);
	ProcessLogWriteLn(msg);
  ThreadSwitch();
  return (TableInfo->OpenStatus);

}
*/

void DBClearAccessKeyBuffer()
{
	memset(AccessKeyBuffer,0,AccessKeyBufferSize);
}

void *DBGetAccessKeyBufferPtr(void) // Used by others to get access to the accessKeyBuffer
{
	return (AccessKeyBuffer);
}
/*
int DBAccessTableData(DataBaseTable *TableInfo, int BtrieveCmd, void* keyBuf, int keyNum)
{
	char msg[128];
	BTI_SINT result;
	// KeyBuf may not necessary for input so see if it was passed
	if ((keyBuf) && (keyBuf!=AccessKeyBuffer)) {
		memcpy(AccessKeyBuffer,keyBuf,AccessKeyBufferSize);
	}
	//ProcessLogWriteLn("DB Access -> ");
	//ProcessLogDumpBuffer("Key=",AccessKeyBuffer,21);
	//ProcessLogWriteLn("");
	result=BTRVID((BTI_WORD)(BtrieveCmd),
								(BTI_VOID_PTR)(TableInfo->Params),
								(BTI_VOID_PTR)(TableInfo->DataBuffer),
								(BTI_WORD_PTR)(&TableInfo->DataLength),
								(BTI_VOID_PTR)(AccessKeyBuffer),
								(BTI_SINT)(keyNum),
								(&dbUserID));
	sprintf_s(msg,"BTRVID=%s, cmd=%d, Data Length=%d, KeyNum=%d, Result=%d",
							TableInfo->Name,BtrieveCmd,TableInfo->DataLength,keyNum,result);
	ProcessLogWriteLn(msg);
	//ProcessLogDumpBuffer("Key=",AccessKeyBuffer,21);
	//ProcessLogWriteLn("");
	//if (result==0) {
	//	ProcessLogDumpBuffer("Rec=",TableInfo->DataBuffer,TableInfo->DataLength);
	//	ProcessLogWriteLn("");
	//}
	//ThreadSwitch();
	return(result);
}
*/
/*
BTI_SINT DBCloseTable(DataBaseTable *TableInfo)
{
	//char msg[128];

  if (TableInfo->OpenStatus==B_NO_ERROR) {
    if (TableInfo->Opened==1) {
      TableInfo->OpenStatus=BTRVID(B_CLOSE,TableInfo->Params,NULL,NULL,NULL,NULL,&dbUserID);
	    ThreadSwitch();
	  }
  }
  if (TableInfo->OpenStatus==B_NO_ERROR) {
		if (TableInfo->Opened>0)
			TableInfo->Opened-=1;
	}
	//sprintf_s(msg,"B_CLOSE %s, Data Length=%d, Status=%d, Opened=%d",
	//						TableInfo->Name,TableInfo->DataLength,TableInfo->OpenStatus,TableInfo->Opened);
	//ProcessLogWriteLn(msg);

	return (TableInfo->OpenStatus);
}
*/

