#ifndef _BTRAPI_H_INCLUDED
/*************************************************************************
**
**  Copyright 1982-1994 Btrieve Technologies, Inc. All Rights Reserved
**
*************************************************************************/
/***************************************************************************
 BTRAPI.H
    This file contains prototypes for the DOS, Extended DOS, MS Windows,
    OS2, and NLM Btrieve call.   The calling application may be C or C++.

    Except for DOS or Extended DOS, the Btrieve application developer will
    also need either an import library or linker import statements to
    identify the imported Btrieve functions to the linker.  The imported
    functions are:

      MS Windows
      ----------
      WBTRCALL.LIB - WBTRVINIT, WBRQSHELLINIT, WBTRVSTOP, BTRCALL,
                     BTRCALLID, BTRCALLBACK
      OS2
      ---
      BTRCALLS.LIB - BTRVINIT,  BRQSHELLINIT,  BTRVSTOP,  BTRCALL

      NLM
      ---
      Use linker import statements - btrv, btrvID

    You will need to compile and link 'btrapi.c' if you call any of
    the following functions from the indicated platforms:
          BTRV .......... MS Windows, OS2, DOS
          BTRVID ........ MS Windows, OS2, DOS
          RQSHELLINIT ... MS Windows

    The interface modules, btrapi.h and btrapi.c, do not support
    32-bit Windows applications.  If you are developing a 32-bit
    Windows application, you must write your own routines to call the
    necessary compiler-specific functions before and after calling
    a function in the Btrieve 16-bit DLL.

    You must define one of the following to your compiler in order to
    compile for a particular platform:

        BTI_DOS     - DOS            ( 16-bit Applications )
        BTI_WIN     - MS WINDOWS     ( 16-bit Applications )
        BTI_OS2     - OS2            ( 16-bit Applications )
        BTI_NLM     - NetWare NLM    ( 32-bit Applications )
        BTI_DOS_32R - Extended DOS with Rational 32-bit + BSTUB.EXE

    If you do not specify one of the above switches to your compiler,
    an error directive in btitypes.h will halt the compilation.

    Extended DOS support is currently limited as described in the prologue
    to the BTI_DOS_32R section in btrapi.c.

    This module is current as of 06/10/94.
***************************************************************************/
#include "btitypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
   PLATFORM-INDEPENDENT FUNCTIONS
     These APIs are the same on all platforms for which they have
     an implementation.  We recommend that you use only these two
     functions with Btrieve 6.x client components, and then issue the
     B_STOP operation prior to exiting your application.
****************************************************************************/
#if defined( BTI_WIN ) || defined( BTI_OS2 ) || defined( BTI_DOS ) || \
    defined( BTI_DOS_32R )
BTI_API BTRV(
           BTI_WORD     operation,
           BTI_VOID_PTR posBlock,
           BTI_VOID_PTR dataBuffer,
           BTI_WORD_PTR dataLength,
           BTI_VOID_PTR keyBuffer,
           BTI_SINT     keyNumber );
#endif

#if defined( BTI_WIN ) || defined( BTI_DOS ) || defined( BTI_NLM ) || \
    defined( BTI_DOS_32R )
BTI_API BTRVID(
           BTI_WORD       operation,
           BTI_VOID_PTR   posBlock,
           BTI_VOID_PTR   dataBuffer,
           BTI_WORD_PTR   dataLength,
           BTI_VOID_PTR   keyBuffer,
           BTI_SINT       keyNumber,
           BTI_BUFFER_PTR clientID );
#endif

/***************************************************************************
   PLATFORM-SPECIFIC FUNCTIONS
      These APIs are specific to the indicated platform.  With the
      exception of BTRCALLBACK, we recommend that you use either
      BTRV or BTRVID, shown above.  Slight performance gains can be
      achieved by using BTRCALL or BTRCALLID.
****************************************************************************/
#if defined( BTI_NLM )
#define BTRV( a, b, c, d, e, f )  btrv( a, b, c, d, e, f )
#define BTRVID( a, b, c, d, e, f, g )  btrvID( a, b, c, d, e, f, g )
#endif

#if defined( BTI_WIN ) || defined( BTI_OS2 )
BTI_API BTRCALL(
           BTI_WORD     operation,
           BTI_VOID_PTR posBlock,
           BTI_VOID_PTR dataBuffer,
           BTI_WORD_PTR dataLength,
           BTI_VOID_PTR keyBuffer,
           BTI_BYTE     keyLength,
           BTI_CHAR     ckeynum );

BTI_API BTRCALLID(
           BTI_WORD       operation,
           BTI_VOID_PTR   posBlock,
           BTI_VOID_PTR   dataBuffer,
           BTI_WORD_PTR   dataLength,
           BTI_VOID_PTR   keyBuffer,
           BTI_BYTE       keyLength,
           BTI_CHAR       ckeynum,
           BTI_BUFFER_PTR clientID );
#endif

#if defined( BTI_WIN )
BTI_API BTRCallback(
           BTI_WORD                   iAction,
           BTI_WORD                   iOption,
           BTI_CB_FUNC_PTR_T          fCallBackFunction,
           BTI_CB_FUNC_PTR_T BTI_FAR *fPreviousCallBackFunction,
           BTI_VOID_PTR               bUserData,
           BTI_VOID_PTR BTI_FAR      *bPreviousUserData,
           BTI_BUFFER_PTR             bClientID );
#endif


/***************************************************************************
   HISTORICAL FUNCTIONS
      These APIs were needed prior to Btrieve 6.x client
      components.  Older applications may still call these functions,
      and the Btrieve 6.x client component will do the appropriate
      thing, depending on the platform.  New applications using the
      6.x client components do NOT have to call these functions.
****************************************************************************/
#if defined( BTI_WIN )
#define BTRVINIT WBTRVINIT
#define BTRVSTOP WBTRVSTOP
BTI_API WBTRVINIT( BTI_CHAR_PTR option );
BTI_API WBRQSHELLINIT( BTI_CHAR_PTR option );
BTI_API WBTRVSTOP( BTI_VOID );
BTI_API RQSHELLINIT( BTI_CHAR_PTR options );
#endif

#if defined( BTI_OS2 )
BTI_API BTRVINIT( BTI_CHAR_PTR options );
BTI_API BTRVSTOP( BTI_VOID );
BTI_API BRQSHELLINIT( BTI_VOID );
#endif

#ifdef __cplusplus
}
#endif

#define _BTRAPI_H_INCLUDED
#endif
