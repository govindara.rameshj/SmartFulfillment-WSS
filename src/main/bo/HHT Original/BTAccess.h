//---------------------------------------------------------------------------
#ifndef BTAccessH
#define BTAccessH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <Dsgnintf.hpp>
#include "BtrConst.h"
//#include "BTAccessEdit.h"
#pragma option -a1
struct DDFFileLayout {
	short int ID;
	char Name[20];
	char Location[64];
	char Attrib;
	char reserved[10];
};
struct FieldLayout {
	short int ID;
	short int DDFID;
	char Name[20];
	char Type;
	short int Offset;
	short int Size;
	short int Decimal;
	char Spare;
};
struct IndexLayout {
	short int File;
	short int Field;
	short int IdxNumb;
	short int IdxSeg;
	short int Flags;
};
#pragma option -a4
//---------------------------------------------------------------------------
class PACKAGE TBTAccess : public TComponent
{
private:
	short __fastcall RetrieveIndex(const Variant Index);
	AnsiString __fastcall ExtractDate(unsigned char *WkData);
	AnsiString __fastcall ExtractString(unsigned char *WkData, short Size);
	AnsiString __fastcall ExtractDecimal(unsigned char *WkData, short Size, short Decimal);

	void __fastcall EncodeDate(unsigned char *WkData, AnsiString& Value);
	void __fastcall EncodeString(unsigned char *WkData, short Size, AnsiString& Value);
	void __fastcall EncodeDecimal(unsigned char *WkData, short Size, short Decimal, AnsiString& Value);

//		AnsiString __fastcall GetFileName() {return BTFileName;};
	void __fastcall SetDDFPath(AnsiString NewPath);
	void __fastcall SetFileName(AnsiString NewName);
	void __fastcall SetLocale(AnsiString NewLocale);
	unsigned char * __fastcall GetClientID() { return FClientID; };
	void __fastcall SetClientID(const unsigned char *Buffer) { memcpy(FClientID, Buffer, 16); };
	void __fastcall ClearTables(void);
	void __fastcall SetFileInformation(void);
	void __fastcall SetFields(void);
	void __fastcall SetIndexes(void);
	void __fastcall SetFieldIndex(short int Index);
	void __fastcall SetFieldName(AnsiString& Index);
	void __fastcall SetIndex(short int WkIndex);
	void __fastcall SetupIndex(void);

	AnsiString __fastcall GetFieldValue(const Variant Index);
	void __fastcall SetFieldValue(const Variant Index, AnsiString Value);

	__property short int FileID = { read=FFileID, write=FFileID, stored=true };

	bool OKToSet;

	Boolean FBTInitialized;

	FieldLayout   *Fields;
	IndexLayout		*Indexes;

	AnsiString FDDFPath;
	AnsiString FFileName;
	AnsiString FLocale;

	short int  FIndex;
	char FileLocation[64];

	short int FFileID;

	int FieldCount;
	int IndexCount;

	short FieldOffset;
	int FPosition;

	short int FLastError;
	short int FFieldIndex;
	AnsiString FFieldName;
	short int FFieldSize;

	bool FileOpen;

	unsigned char FClientID[16];

	unsigned char *DataBlock;
	unsigned long DataLength;

	unsigned  char PosBlock[POS_BLOCK_SIZE];
	unsigned  char KeyBuffer[MAX_KEY_SIZE];

__published:
	__property AnsiString DDFPath = { read=FDDFPath, write=SetDDFPath, stored=true };
	__property AnsiString FileName = { read=FFileName, write=SetFileName, stored=true };
	__property short int Index = { read=FIndex, write=SetIndex, stored=true };
	__property AnsiString Locale = { read=FLocale, write=SetLocale, stored=true };

protected:
public:
	virtual void __fastcall Loaded();

	void __fastcall Clear(void);
	short __fastcall Open(void);
	short __fastcall Close(void);
	short __fastcall Insert(void);
	short __fastcall Update(void);
	short __fastcall Delete(void);
	short __fastcall GetPosition(void);
	short __fastcall Unlock(short LockType);
	short __fastcall GetFirst(unsigned short LockBias);
	short __fastcall GetNext(unsigned short LockBias);
	short __fastcall GetLast(unsigned short LockBias);
	short __fastcall GetPrevious(unsigned short LockBias);
	short __fastcall GetEqual(unsigned short LockBias);
	short __fastcall GetGreater(unsigned short LockBias);
	short __fastcall GetGreaterOrEqual(unsigned short LockBias);
	short __fastcall GetLess(unsigned short LockBias);
	short __fastcall GetLessOrEqual(unsigned short LockBias);
	short __fastcall GetDirect(unsigned short LockBias);

	__property AnsiString FieldValue[const Variant Index] = { read=GetFieldValue, write=SetFieldValue };
	__property short int FieldIndex = { read=FFieldIndex, write=SetFieldIndex, stored=false };
	__property AnsiString FieldName = { read=FFieldName, write=SetFieldName, stored=false };
	__property short int FieldSize = { read=FFieldSize, write=FFieldSize, stored=false };

	__property short int LastError = { read=FLastError, write=FLastError, stored=false };

	__property int Position = { read=FPosition, write=FPosition, stored=false };

	__property bool BTInitialized = { read=FBTInitialized, write=FBTInitialized, stored=false, default=0 };
	__property unsigned char *ClientID = { read=GetClientID, write=SetClientID, stored=false };

	__fastcall virtual TBTAccess(TComponent* Owner);
	__fastcall virtual ~TBTAccess(void);
};
//---------------------------------------------------------------------------

#endif
