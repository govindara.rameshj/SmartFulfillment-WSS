//---------------------------------------------------------------------------
#ifndef WIXRFUnitH
#define WIXRFUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BTAccess.h"
//---------------------------------------------------------------------------
class TfrmWicksRF : public TForm
{
__published:	// IDE-managed Components
	TButton *btnEnter;
	TLabel *Label1;
	TLabel *Label2;
	TEdit *txtTest;
	TEdit *txtResult;
	TBTAccess *DbStkMas;
	void __fastcall btnEnterClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfrmWicksRF(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmWicksRF *frmWicksRF;
//---------------------------------------------------------------------------
#endif
