//---------------------------------------------------------------------------
// Application: Price Checker Server and Client for Windows Socket Services
//							Developed for Atlantic Homecare BSD ATL702.
// Project: PriceCheckerServer and Client- Developed with MS Visual Studio
//					2003, C++
// --------------------------------------------------------------------------
// HEader: WinDisplay.h - Display routines.
//						Manages rectangular areas on the client window. Provides border,
//						Erase, and text resizing and display
// --------------------------------------------------------------------------
#ifndef WinDisplay_h
#define WinDisplay_h
// --------------------------------------------------------------------------
// Global Data Structures
// --------------------------------------------------------------------------
typedef struct WinDisplayFrame {
	// Holds context for a user defined area on the window
	HWND hWnd;
	HDC hdc;
	RECT frameArea;
	RECT borderArea;
	RECT displayArea;
	double fontSizeMultiplier;
	int beginDrawCounter;
	int width;
	int height;
	int charWidth;
	int charHeight;
	int cols;
	int rows;

} WinDisplayFrame;

#define MAX_STRING_LENGTH 161		// Used for temp strings

#define EuroCharacter			0x04	// For inserting Euro symbol into 1 byte strings
// --------------------------------------------------------------------------
// Global Functions
// --------------------------------------------------------------------------
void WinDisplay_SetFontSizeMultiplier(double value);
void WinDisplay_CreateFrame(WinDisplayFrame *retWf,HWND hWnd, RECT *areaSize);
void WinDisplay_ResizeFrame(WinDisplayFrame *Wf, RECT *newArea);
void WinDisplay_EraseFrame(WinDisplayFrame *Wf);
void WinDisplay_DrawFrame(WinDisplayFrame *Wf);
void WinDisplay_DrawFrame(WinDisplayFrame *Wf);
void WinDisplay_ChangeCurrentFontSizeMultiplier(WinDisplayFrame *Wf, double MultiplyBy);
void WinDisplay_ResetCurrentFontSizeMultiplier(WinDisplayFrame *Wf);
int WinDisplay_GetStrPelWidth(WinDisplayFrame *Wf, char *str);
void near WinDisplay_DisplayAt(int x, int y, char *str);
void near WinDisplay_DisplayStr(int charCol, int charRow, char *str);
void WinDisplay_EraseLine(WinDisplayFrame *Wf, int y);
void WinDisplay_DisplayCentered(WinDisplayFrame *Wf, int x, int y, char *str);
void WinDisplay_DisplayRowCentered(WinDisplayFrame *Wf, int y, char *str);
void WinDisplay_DisplayLeftJustified(WinDisplayFrame *Wf, int x, int y, char *str);
void WinDisplay_DisplayRightJustified(WinDisplayFrame *Wf, int x, int y, char *str);
void WinDisplay_DisplayInt(WinDisplayFrame *Wf, int x, int y, int i);
unsigned short *WinDisplay_StrTowStrPtr(char *str);
void WinDisplay_wStrToStr(unsigned short *wStr,int wMaxLength, char *str, int maxLength);

#endif
