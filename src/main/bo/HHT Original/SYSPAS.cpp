//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server 
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed and compiled with MS Visual 
//					Studio 2003 C++ 
// --------------------------------------------------------------------------

#undef _THIS_SOURCE_HAS_THE_DATA_ 
#include "wixstep.h"

SYSPAS_SQL syspas;

bool ReadSysPasTable( int sessionNumber)
{
	extern SQLHSTMT hstmt;       // Statement handle
	char SECLxx[12];
	char cmd[255];
	// build the column name for the SECL
	strcpy(SECLxx, "SECL");
	itoa(atoi(sUser[sessionNumber].pcAfgn),&SECLxx[4],10);
	strcpy(cmd, "select EEID, NAME, PASS, DELC, ");
	strcat(cmd, SECLxx);
	strcat(cmd, " from SYSPAS WHERE EEID = '");
	strcat(cmd, sUser[sessionNumber].pcEeid);
	strcat(cmd, "'");
	if (DoSQLCommand(cmd)) {
		if SQLSUCCESS(SQLFetch(hstmt)) {
			SQLGetColumn(1, SQL_C_CHAR, syspas.EEID, sizeof(syspas.EEID));
			SQLGetData(hstmt, 1, SQL_C_CHAR,&syspas.EEID,sizeof(syspas.EEID),NULL);
			SQLGetData(hstmt, 2, SQL_C_CHAR,&syspas.NAME,sizeof(syspas.NAME),NULL);
			SQLGetData(hstmt, 3, SQL_C_CHAR,&syspas.PASS,sizeof(syspas.PASS),NULL);
			SQLGetData(hstmt, 4, SQL_C_BIT,&syspas.DELC,sizeof(syspas.DELC),NULL);
			SQLGetData(hstmt, 5, SQL_C_CHAR,&syspas.SECL,sizeof(syspas.SECL),NULL);
			sUser[sessionNumber].pcSecl[0] = syspas.SECL[0];
			sUser[sessionNumber].pcSecl[1] = 0;
			strcpy(sUser[sessionNumber].pcLastInput, "1");
			if(syspas.DELC) { 
				strcpy(sUser[sessionNumber].pcLastInput, "2");					
			}
			return true; 
		}
	}
  strcpy(sUser[sessionNumber].pcLastInput, "0");
  return false;
}
