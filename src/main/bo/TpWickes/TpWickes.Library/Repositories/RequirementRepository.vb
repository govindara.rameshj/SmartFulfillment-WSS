﻿Public Class RequirementRepository
    Implements IRequirementRepository

    Public Function RequirementEnabled(ByVal RequirementID As Integer) As Boolean? Implements IRequirementRepository.RequirementEnabled

        Return Cts.Oasys.Core.System.Parameter.GetBoolean(RequirementID)

    End Function

    Public Function IsSwitchPresentAndEnabled(ByVal RequirementID As Integer) As Boolean Implements IRequirementRepository.IsSwitchPresentAndEnabled
        Dim IsRequirementEnabled As Boolean?

        IsRequirementEnabled = RequirementEnabled(RequirementID)
        If IsRequirementEnabled.HasValue Then
            Return IsRequirementEnabled.Value
        End If
    End Function
End Class