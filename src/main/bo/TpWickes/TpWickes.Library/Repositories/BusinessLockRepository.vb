﻿Public Class BusinessLockRepository
    Implements IBusinessLockRepository

    Private _Connection As IConnection
    Private _Command As ICommand
    Private _DT As DataTable

    Private _WorkStationID As Integer
    Private _AssemblyName As String
    Private _ParameterValues As String

    Private _RecordID As Integer
    Private _ReturnedRecordID As SqlParameter

#Region "Interface"

    Public Function GetLock(ByRef Con As Connection) As DataTable Implements IBusinessLockRepository.GetLock

        Reset()

        SetConnection(Con)

        ExecuteGetLock()

        Return _DT

    End Function

    Public Function AddLock(ByRef Con As Connection, _
                            ByVal WorkStationID As Integer, _
                            ByVal AssemblyName As String, _
                            ByVal ParameterValues As String) As Integer Implements IBusinessLockRepository.AddLock

        Reset()

        SetConnection(Con)
        SetWorkStationID(WorkStationID)
        SetAssemblyName(AssemblyName)
        SetParameterValues(ParameterValues)

        ExecuteAddLock()

        Return ReturnedRecordID()

    End Function

    Public Sub DeleteLock(ByRef Con As Connection, ByVal RecordID As Integer) Implements IBusinessLockRepository.DeleteLock

        Reset()

        SetConnection(Con)
        SetRecordID(RecordID)

        ExecuteDeleteLock()

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DT = Nothing

        _WorkStationID = Nothing
        _AssemblyName = Nothing
        _ParameterValues = Nothing

        _RecordID = Nothing
        _ReturnedRecordID = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then

            _Connection = New Connection

        Else

            _Connection = Con

        End If

    End Sub

    Friend Overridable Sub SetWorkStationID(ByVal Value As Integer)

        _WorkStationID = Value

    End Sub

    Friend Overridable Sub SetAssemblyName(ByVal Value As String)

        _AssemblyName = Value

    End Sub

    Friend Overridable Sub SetParameterValues(ByVal Value As String)

        _ParameterValues = Value

    End Sub

    Friend Overridable Sub SetRecordID(ByVal Value As Integer)

        _RecordID = Value

    End Sub

#Region "Get Lock"

    Friend Overridable Sub ExecuteGetLock()

        ConfigureGetLockCommand()
        ExecuteGetLockCommand()

    End Sub

    Friend Overridable Sub ConfigureGetLockCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = "usp_BusinessLockSelect"

    End Sub

    Friend Overridable Sub ExecuteGetLockCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteDataTable()

    End Sub

#End Region

#Region "Add Lock"

    Friend Overridable Sub ExecuteAddLock()

        ConfigureAddLockCommand()
        ExecuteAddLockCommand()

    End Sub

    Friend Overridable Sub ConfigureAddLockCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = "usp_BusinessLockInsert"

        _Command.AddParameter("@WorkStationID", _WorkStationID)
        _Command.AddParameter("@AssemblyName", _AssemblyName)
        _Command.AddParameter("@Parameters", _ParameterValues)
        _ReturnedRecordID = _Command.AddOutputParameter("@RecordID", SqlDbType.Int)

    End Sub

    Friend Overridable Sub ExecuteAddLockCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

    Friend Overridable Function ReturnedRecordID() As Integer

        Return CType(_ReturnedRecordID.Value, Integer)

    End Function

#End Region

#Region "Delete Lock"

    Friend Overridable Sub ExecuteDeleteLock()

        ConfigureDeleteLockCommand()
        ExecuteDeleteLockCommand()

    End Sub

    Friend Overridable Sub ConfigureDeleteLockCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = "usp_BusinessLockDelete"

        _Command.AddParameter("@ID", _RecordID)

    End Sub

    Friend Overridable Sub ExecuteDeleteLockCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()


    End Sub

#End Region

#Region "Command Execution"

    Friend Overridable Sub ExecuteDataTable()

        _DT = _Command.ExecuteDataTable()

    End Sub

    Friend Overridable Sub ExecuteNonQuery()

        _Command.ExecuteDataTable()

    End Sub

#End Region

#Region "Test Conditions"

    Friend Function ConnectionIsNothing() As Boolean

        If _Connection Is Nothing Then Return True

    End Function

    Friend Function CommandIsNothing() As Boolean

        If _Command Is Nothing Then Return True

    End Function

#End Region

#End Region

End Class