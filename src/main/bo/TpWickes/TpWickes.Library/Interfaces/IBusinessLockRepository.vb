﻿Public Interface IBusinessLockRepository

    Function GetLock(ByRef Con As Connection) As DataTable

    Function AddLock(ByRef Con As Connection, _
                     ByVal WorkStationID As Integer, _
                     ByVal AssemblyName As String, _
                     ByVal ParameterValues As String) As Integer

    Sub DeleteLock(ByRef Con As Connection, ByVal RecordID As Integer)

End Interface