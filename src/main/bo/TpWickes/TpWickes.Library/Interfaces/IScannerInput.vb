﻿Public Interface IScannerInput
    Property ScannerInput() As String
    Function InitialiseScanner() As Boolean
    Event InputReceived(ByVal eventType As Short, ByVal strInput As String)
    Function CloseScanner() As Boolean
    Function IsScannerEventDriven() As Boolean
End Interface
