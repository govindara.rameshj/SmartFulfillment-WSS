﻿Public Interface IRequirementRepository

    Function RequirementEnabled(ByVal RequirementID As Integer) As Nullable(Of Boolean)
    Function IsSwitchPresentAndEnabled(ByVal RequirementID As Integer) As Boolean
End Interface