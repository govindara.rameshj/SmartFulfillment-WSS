﻿Public Interface IBankingFloat

    Function FloatIssues(ByRef DT As DataTable) As System.Nullable(Of Decimal)
    Function FloatReturned(ByRef DT As DataTable) As System.Nullable(Of Decimal)
    Function FloatVariance(ByRef DT As DataSet) As System.Nullable(Of Decimal)

End Interface