﻿Public MustInherit Class BaseFactory(Of T)
    Implements IBaseFactory(Of T)

    Public Overridable Function GetImplementation() As T Implements IBaseFactory(Of T).GetImplementation

        Return Implementation()
    End Function

    MustOverride Function Implementation() As T Implements IBaseFactory(Of T).Implementation
End Class
