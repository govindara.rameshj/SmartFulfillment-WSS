﻿Public Class BusinessLockRepositoryFactory
    Inherits BaseFactory(Of IBusinessLockRepository)

    Public Overrides Function Implementation() As IBusinessLockRepository

        Return New BusinessLockRepository

    End Function

End Class