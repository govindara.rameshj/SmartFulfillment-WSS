﻿Public Class RunTimeEnvironmentFactory
    Inherits BaseFactory(Of IRunTimeEnvironment)

    Public Overrides Function Implementation() As IRunTimeEnvironment

        Return New RunTimeEnvironment

    End Function

End Class
