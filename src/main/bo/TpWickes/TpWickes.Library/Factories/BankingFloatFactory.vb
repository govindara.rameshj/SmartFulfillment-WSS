﻿Public Class BankingFloatFactory
    Inherits BaseFactory(Of IBankingFloat)

    Public Overrides Function Implementation() As IBankingFloat

        Return New BankingFloatRevised()

    End Function

End Class