﻿Public MustInherit Class RequirementSwitchFactory(Of T)
    Implements IRequirementSwitchFactory(Of T)

    MustOverride Function ImplementationA() As T Implements IRequirementSwitchFactory(Of T).ImplementationA
    MustOverride Function ImplementationB() As T Implements IRequirementSwitchFactory(Of T).ImplementationB
    MustOverride Function ImplementationA_IsActive() As Boolean Implements IRequirementSwitchFactory(Of T).ImplementationA_IsActive

    Private _requirementSwitchId As Integer

    Protected Sub SetSwitchParameterID(ByVal value As Integer)
        _requirementSwitchId = value
    End Sub

    Public ReadOnly Property SwitchParameterID() As Integer
        Get
            Return _requirementSwitchId
        End Get
    End Property

    Overridable Function GetImplementation() As T Implements IRequirementSwitchFactory(Of T).GetImplementation

        If ImplementationA_IsActive() Then
            Return ImplementationA()
        Else
            Return ImplementationB()
        End If
    End Function
End Class
