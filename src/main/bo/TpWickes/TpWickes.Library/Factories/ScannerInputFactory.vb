﻿Public Class ScannerInputFactory

    Private parameterID As Integer = -22012
    Private SecondParameterID As Integer = -2201201

    Friend Overridable Function IsEnabled() As Boolean
        Return Cts.Oasys.Core.System.Parameter.GetBoolean(parameterID) And _
               Cts.Oasys.Core.System.Parameter.GetBoolean(SecondParameterID)
    End Function

    Public Function FactoryGet() As IScannerInput
        If IsEnabled() = True Then
            Return New ScannerInput
        Else
            Return New ScannerInputDoesNothing
        End If
    End Function

End Class

