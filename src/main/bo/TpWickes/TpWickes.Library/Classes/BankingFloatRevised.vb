﻿Public Class BankingFloatRevised
    Implements IBankingFloat

    Public Function FloatIssues(ByRef DT As System.Data.DataTable) As Decimal? Implements IBankingFloat.FloatIssues

        Dim Value As System.Nullable(Of Decimal)

        If IsDBNull(DT.Rows(0)(0)) = False Then Value = CType(CType(DT.Rows(0)(0), String), System.Nullable(Of Decimal))

        Return HandleNullValue(Value)

    End Function

    Public Function FloatReturned(ByRef DT As System.Data.DataTable) As Decimal? Implements IBankingFloat.FloatReturned

        Dim Value As System.Nullable(Of Decimal)

        If IsDBNull(DT.Rows(0)(1)) = False Then Value = CType(CType(DT.Rows(0)(1), String), System.Nullable(Of Decimal))

        Return HandleNullValue(Value)

    End Function

    Public Function FloatVariance(ByRef DT As System.Data.DataSet) As Decimal? Implements IBankingFloat.FloatVariance

        Dim Value As System.Nullable(Of Decimal)

        If IsDBNull(DT.Tables(0).Rows(0)("FloatVariance")) = False Then Value = CType(CType(DT.Tables(0).Rows(0)("FloatVariance"), String), System.Nullable(Of Decimal))

        Return HandleNullValue(Value)

    End Function

#Region "Private & Friend Procedures And Functions"

    Friend Function HandleNullValue(ByVal Value As System.Nullable(Of Decimal)) As Decimal

        If Value.HasValue = False Then Return 0

        Return Value.Value

    End Function

#End Region

End Class