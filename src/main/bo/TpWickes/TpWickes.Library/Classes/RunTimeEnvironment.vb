﻿Public Class RunTimeEnvironment
    Implements IRunTimeEnvironment

    Public Function CurrentDate() As Date Implements IRunTimeEnvironment.CurrentDate

        Return Now.Date

    End Function

End Class