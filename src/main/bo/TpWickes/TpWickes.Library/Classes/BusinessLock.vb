﻿<Assembly: InternalsVisibleTo("TpWickes.Library.IntegrationTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                          "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                          "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                          "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                          "54e0a4a4")> 
Public Class BusinessLock
    Implements IBusinessLock


    Private _DT As DataTable
    Private _DR As DataRow

    Private _WorkStationID As Integer
    Private _CurrentDateTime As DateTime
    Private _AssemblyName As String
    Private _ParameterValues As String
    Private _LockDuration As System.Nullable(Of Integer)

#Region "Interface"

    Public Function CreateLock(ByVal WorkStationID As Integer, ByVal CurrentDateAndTime As Date, ByVal AssemblyName As String, ByVal ParameterValues As String) As Integer Implements IBusinessLock.CreateLock

        If ExecuteLockExist(WorkStationID, CurrentDateAndTime, AssemblyName, ParameterValues) = True Then Return -1

        Return ExecuteAddLock(WorkStationID, AssemblyName, ParameterValues)

    End Function

    Public Sub RemoveLock(ByVal RecordID As Integer) Implements IBusinessLock.RemoveLock

        ExecuteRemoveLock(RecordID)

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Friend Overridable Function ExecuteLockExist(ByVal WorkStationID As Integer, _
                                                 ByVal CurrentDateAndTime As DateTime, _
                                                 ByVal AssemblyName As String, _
                                                 ByVal ParameterValues As String) As Boolean

        SetWorkStationID(WorkStationID)
        SetCurrentDateAndTime(CurrentDateAndTime)
        SetAssemblyName(AssemblyName)
        SetParameterValues(ParameterValues)

        _LockDuration = GetLockDuration()
        _DT = GetLockData()

        If DataEmpty() = True Then Return False

        Return ProcessData()

    End Function

    Friend Overridable Function ExecuteAddLock(ByVal WorkStationID As Integer, _
                                               ByVal AssemblyName As String, _
                                               ByVal ParameterValues As String) As Integer

        Dim Repository As IBusinessLockRepository

        Repository = (New BusinessLockRepositoryFactory).Implementation

        Return Repository.AddLock(Nothing, WorkStationID, AssemblyName, ParameterValues)

    End Function

    Friend Overridable Sub ExecuteRemoveLock(ByVal RecordID As Integer)

        Dim Repository As IBusinessLockRepository

        Repository = (New BusinessLockRepositoryFactory).Implementation

        Repository.DeleteLock(Nothing, RecordID)

    End Sub

#Region "Properties"

    Private Sub SetWorkStationID(ByVal Value As Integer)
        _WorkStationID = Value
    End Sub

    Private Sub SetCurrentDateAndTime(ByVal Value As DateTime)
        _CurrentDateTime = Value
    End Sub

    Private Sub SetAssemblyName(ByVal Value As String)
        _AssemblyName = Value
    End Sub

    Private Sub SetParameterValues(ByVal Value As String)
        _ParameterValues = Value
    End Sub

#End Region

#Region "Get Data"

    Friend Overridable Function GetLockDuration() As System.Nullable(Of Integer)

        Return Parameter.GetInteger(2)

    End Function

    Friend Overridable Function GetLockData() As DataTable

        Dim Repository As IBusinessLockRepository

        Repository = (New BusinessLockRepositoryFactory).Implementation

        Return Repository.GetLock(Nothing)

    End Function

#End Region

#Region "Process Data"

    Private Function DataEmpty() As Boolean

        Return _DT Is Nothing OrElse _DT.Rows.Count = 0

    End Function

    Private Function ProcessData() As Boolean

        Dim LockExist As Boolean

        LockExist = False

        For Each Row As DataRow In _DT.Rows

            _DR = Row

            If ProcessRecord() = True Then LockExist = True

        Next

        Return LockExist

    End Function

    Private Function ProcessRecord() As Boolean

        If ProcessNameMatch() = False Then Return False
        If LockDurationValid() = True Then Return False
        If WorkStationMatch() = True Then Return False

        Return True

    End Function

    Private Function ProcessNameMatch() As Boolean

        Dim DatabaseAssemblyName As String = String.Empty
        Dim DatabaseParameterValues As String = String.Empty

        If _DR.Item("AssemblyName") IsNot System.DBNull.Value Then DatabaseAssemblyName = CType(_DR.Item("AssemblyName"), String)
        If _DR.Item("Parameters") IsNot System.DBNull.Value Then DatabaseParameterValues = CType(_DR.Item("Parameters"), String)

        If _AssemblyName Is Nothing OrElse _AssemblyName <> DatabaseAssemblyName Then Return False
        If _ParameterValues <> DatabaseParameterValues Then Return False

        Return True

    End Function

    Private Function LockDurationValid() As Boolean

        Dim DatabaseDateTimeStamp As DateTime

        DatabaseDateTimeStamp = CType(_DR.Item("DateTimeStamp"), DateTime)

        If DatabaseDateTimeStamp = DateTime.MinValue Then Return False

        If _LockDuration.HasValue = False Then _LockDuration = 0

        'Return DatabaseDateTimeStamp.AddSeconds(_LockDuration.Value) < _CurrentDateTime
        Return DateTime.Compare(DatabaseDateTimeStamp.AddSeconds(_LockDuration.Value), _CurrentDateTime) < 0

    End Function

    Private Function WorkStationMatch() As Boolean

        Dim DatabaseWorkStationID As Integer

        DatabaseWorkStationID = CType(_DR.Item("WorkStationID"), Integer)

        If DatabaseWorkStationID = _WorkStationID Then Return True

    End Function

#End Region

#End Region

End Class