﻿Public Interface IVisionSaleStockProcessing

    Function RemoveStockNodeFromXmlDocument(ByVal XmlString As String) As String

    Function SaveStock(ByVal StockList As Vision.StockCollection) As Boolean

    Function StockPersist(ByRef Con As Connection, ByVal StockList As Vision.StockCollection, ByVal EmployeeID As Integer) As Boolean

End Interface