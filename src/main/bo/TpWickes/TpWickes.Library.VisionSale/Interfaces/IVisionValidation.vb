﻿Public Interface IVisionValidation

    Function Valid(ByVal TransactionTotal As Decimal, ByVal TenderCount As Integer) As Boolean

End Interface