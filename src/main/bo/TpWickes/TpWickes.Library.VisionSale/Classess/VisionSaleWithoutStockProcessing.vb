﻿Public Class VisionSaleWithoutStockProcessing
    Implements IVisionSaleStockProcessing

    Public Function RemoveStockNodeFromXmlDocument(ByVal XmlString As String) As String Implements IVisionSaleStockProcessing.RemoveStockNodeFromXmlDocument

        Dim TempDocument As New XDocument

        TempDocument = XDocument.Parse(XmlString)

        For Each LineNode As XElement In TempDocument.Descendants.Elements

            If LineNode.Name = "STOCK" Then LineNode.ReplaceWith(String.Empty)

        Next

        Return TempDocument.ToString

    End Function

    Public Function SaveStock(ByVal StockList As Vision.StockCollection) As Boolean Implements IVisionSaleStockProcessing.SaveStock

        Return False

    End Function

    Public Function StockPersist(ByRef Con As Connection, ByVal StockList As Vision.StockCollection, ByVal EmployeeID As Integer) As Boolean Implements IVisionSaleStockProcessing.StockPersist

        Return True

    End Function

End Class