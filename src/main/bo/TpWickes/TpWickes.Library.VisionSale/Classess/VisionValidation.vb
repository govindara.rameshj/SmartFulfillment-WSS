﻿Public Class VisionValidation
    Implements IVisionValidation

    Public Function Valid(ByVal TransactionTotal As Decimal, ByVal TenderCount As Integer) As Boolean Implements IVisionValidation.Valid

        If TransactionTotal = 0 And TenderCount = 0 Then Return False

        If TransactionTotal = 0 And TenderCount > 0 Then Return True

        Return True

    End Function

End Class