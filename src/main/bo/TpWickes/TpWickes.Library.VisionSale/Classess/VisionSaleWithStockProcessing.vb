﻿Public Class VisionSaleWithStockProcessing
    Implements IVisionSaleStockProcessing

    Private _SecurityLevel As String

    Public Function RemoveStockNodeFromXmlDocument(ByVal XmlString As String) As String Implements IVisionSaleStockProcessing.RemoveStockNodeFromXmlDocument

        Return XmlString

    End Function

    Public Function SaveStock(ByVal StockList As Vision.StockCollection) As Boolean Implements IVisionSaleStockProcessing.SaveStock

        If StockList Is Nothing Then Return False

        If StockList.Count = 0 Then Return False

        Return True

    End Function

    Public Function StockPersist(ByRef Con As Connection, _
                                 ByVal StockList As Vision.StockCollection, ByVal EmployeeID As Integer) As Boolean Implements IVisionSaleStockProcessing.StockPersist

        Dim AdjustmentCode As ISaCode
        Dim AdjustmentManager As IStockAdjustmentManager
        Dim SpecificAdjustment As IAdjustment

        Dim TomorrowDate As Date

        AdjustmentCode = SACodeFactory.FactoryGet
        AdjustmentCode.Load(100, "30")

        SpecificAdjustment = AdjustmentFactory.FactoryGet
        TomorrowDate = SpecificAdjustment.ReadAdjustmentDate

        StockPersist = True
        For Each Stock As Vision.Stock In StockList

            AdjustmentManager = StockAdjustmentManagerFactory.FactoryGet
            SpecificAdjustment = AdjustmentFactory.FactoryGet

            If AdjustmentManager.NormalAdjustment(Con, _
                                                  AdjustmentCode, _
                                                  SpecificAdjustment, _
                                                  Stock.StockCode, _
                                                  TomorrowDate, _
                                                  String.Empty, _
                                                  Stock.Quantity, _
                                                  "Vision Refund", _
                                                  EmployeeID) = False Then

                StockPersist = False
                Exit For

            End If

        Next

    End Function

End Class