﻿Namespace Vision

#Region "NonKitchenAndBathroom"

    <XmlRoot("CTSINTEGRATION")> Public Class NonKitchenAndBathroom

#Region "Properties"

        Private _Total As TotalNonKB
        Private _LineCollection As LineCollectionNonKB
        Private _PaymentCollection As PaymentCollectionNonKB
        Private _StockCollection As TpWickes.Library.VisionSale.Vision.StockCollection

        <XmlElement("PVTOTS")> Public Property Total() As TotalNonKB
            Get
                Return _Total
            End Get
            Set(ByVal value As TotalNonKB)
                _Total = value
            End Set
        End Property

        <XmlArray("PVLINE"), XmlArrayItem("LINE")> Public Property Lines() As LineCollectionNonKB
            Get
                Return _LineCollection
            End Get
            Set(ByVal value As LineCollectionNonKB)
                _LineCollection = value
            End Set
        End Property

        <XmlArray("PVPAID"), XmlArrayItem("TENDER")> Public Property Payments() As PaymentCollectionNonKB
            Get
                Return _PaymentCollection
            End Get
            Set(ByVal value As PaymentCollectionNonKB)
                _PaymentCollection = value
            End Set
        End Property

        <XmlArray("STOCK"), XmlArrayItem("LINE")> Public Property Stocks() As TpWickes.Library.VisionSale.Vision.StockCollection
            Get
                Return _StockCollection
            End Get
            Set(ByVal value As TpWickes.Library.VisionSale.Vision.StockCollection)
                _StockCollection = value
            End Set
        End Property

#End Region

#Region "Public Methods"

        Public Shared Function Deserialise(ByVal xmlString As String) As NonKitchenAndBathroom

            Dim VisionSale As IVisionSaleStockProcessing

            VisionSale = (New VisionSaleStockProcessingFactory).GetImplementation()
            xmlString = VisionSale.RemoveStockNodeFromXmlDocument(xmlString)



            Dim response As NonKitchenAndBathroom = Nothing
            Dim serializer As New XmlSerializer(GetType(NonKitchenAndBathroom))

            Using reader As TextReader = New StringReader(xmlString)
                response = CType(serializer.Deserialize(reader), NonKitchenAndBathroom)
            End Using
            Return response

        End Function

        Public Function PersistAndUpdateFlashTotals() As Boolean
            Return PersistFlash(Me)
        End Function

#End Region

#Region "Private Methods"

        Private Function PersistFlash(ByVal wrapper As NonKitchenAndBathroom) As Boolean

            Dim VisionSale As IVisionSaleStockProcessing = (New VisionSaleStockProcessingFactory).GetImplementation()

            Using con As New Connection
                Try
                    Dim linesAffected As Integer = 0
                    con.StartTransaction()

                    linesAffected += TotalPersistFlash(wrapper.Total, con)
                    linesAffected += LinesPersist(wrapper.Lines, con)
                    linesAffected += PaymentsPersistFlash(wrapper.Payments, con)

                    If VisionSale.SaveStock(wrapper.Stocks) = True Then

                        If VisionSale.StockPersist(con, wrapper.Stocks, CType(wrapper.Total.CashierId, Integer)) = False Then

                            con.RollbackTransaction()
                            Return False

                        End If

                    End If

                    If linesAffected = (1 + wrapper.Lines.Count + wrapper.Payments.Count) Then
                        con.CommitTransaction()
                        Return True
                    Else
                        con.RollbackTransaction()
                        Return False
                    End If

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Function

        Private Function TotalPersistFlash(ByVal tot As TotalNonKB, ByRef con As Connection) As Integer
            If tot.Value <> 0 Then
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionTotalPersistFlash)
                    com.AddParameter(My.Resources.Parameters.TranDate, tot.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, tot.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, tot.TranNumber)
                    com.AddParameter(My.Resources.Parameters.CashierId, tot.CashierId)
                    com.AddParameter(My.Resources.Parameters.TranDateTime, tot.TranDateTime)
                    com.AddParameter(My.Resources.Parameters.Type, tot.Type)
                    com.AddParameter(My.Resources.Parameters.ReasonCode, tot.ReasonCode)
                    com.AddParameter(My.Resources.Parameters.ReasonDescription, tot.ReasonDescription)
                    com.AddParameter(My.Resources.Parameters.OrderNumber, tot.OrderNumber)
                    com.AddParameter(My.Resources.Parameters.Value, tot.Value)
                    com.AddParameter(My.Resources.Parameters.ValueMerchandising, tot.ValueMerchandising)
                    com.AddParameter(My.Resources.Parameters.ValueNonMerchandising, tot.ValueNonMerchandising)
                    com.AddParameter(My.Resources.Parameters.ValueTax, tot.ValueTax)
                    com.AddParameter(My.Resources.Parameters.ValueDiscount, tot.ValueDiscount)
                    com.AddParameter(My.Resources.Parameters.ValueColleagueDiscount, tot.ValueColleagueDiscount)
                    com.AddParameter(My.Resources.Parameters.IsColleagueDiscount, tot.IsColleagueDiscount)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, tot.IsPivotal)
                    Return com.ExecuteNonQuery
                End Using
            End If

        End Function

        Private Function LinesPersist(ByVal lines As LineCollectionNonKB, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As LineNonKB In lines
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionLinePersist)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.SkuNumber, l.SkuNumber)
                    com.AddParameter(My.Resources.Parameters.Quantity, l.Quantity)
                    com.AddParameter(My.Resources.Parameters.PriceLookup, l.PriceLookup)
                    com.AddParameter(My.Resources.Parameters.Price, l.Price)
                    com.AddParameter(My.Resources.Parameters.PriceExVat, l.PriceExVat)
                    com.AddParameter(My.Resources.Parameters.PriceExtended, l.PriceExtended)
                    com.AddParameter(My.Resources.Parameters.IsRelatedItemSingle, l.IsRelatedItemSingle)
                    com.AddParameter(My.Resources.Parameters.VatSymbol, l.VatSymbol)
                    com.AddParameter(My.Resources.Parameters.HieCategory, l.HieCategory)
                    com.AddParameter(My.Resources.Parameters.HieGroup, l.HieGroup)
                    com.AddParameter(My.Resources.Parameters.HieSubgroup, l.HieSubgroup)
                    com.AddParameter(My.Resources.Parameters.HieStyle, l.HieStyle)
                    com.AddParameter(My.Resources.Parameters.SaleType, l.SaleType)
                    com.AddParameter(My.Resources.Parameters.IsInStoreStockItem, l.IsInStoreStockItem)
                    com.AddParameter(My.Resources.Parameters.MovementTypeCode, l.MovementTypeCode)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, l.IsPivotal)
                    linesAffected += com.ExecuteNonQuery
                End Using
            Next
            Return linesAffected

        End Function

        Private Function PaymentsPersistFlash(ByVal payments As PaymentCollectionNonKB, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As PaymentNonKB In payments
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionPaymentPersistFlash)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.TenderTypeId, l.TenderTypeId)
                    com.AddParameter(My.Resources.Parameters.ValueTender, l.ValueTender)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, l.IsPivotal)
                    linesAffected += com.ExecuteNonQuery
                End Using
            Next
            Return linesAffected

        End Function

#End Region

    End Class

    Public Class TotalNonKB
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _cashierId As Integer
        Private _tranDateTime As String
        Private _type As String
        Private _reasonCode As String
        Private _reasonDescription As String
        Private _orderNumber As String
        Private _valueMerchandising As Decimal
        Private _valueNonMerchandising As Decimal
        Private _valueTax As Decimal
        Private _valueDiscount As Decimal
        Private _value As Decimal
        Private _isColleagueDiscount As Boolean
        Private _valueColleagueDiscount As Decimal
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("CashierId")> Public Property CashierId() As Integer
            Get
                Return _cashierId
            End Get
            Set(ByVal value As Integer)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("TranDateTime")> Public Property TranDateTime() As String
            Get
                Return _tranDateTime
            End Get
            Set(ByVal value As String)
                _tranDateTime = value
            End Set
        End Property
        <ColumnMapping("Type")> Public Property Type() As String
            Get
                Return _type
            End Get
            Set(ByVal value As String)
                _type = value
            End Set
        End Property
        <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As String)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("ReasonDescription")> Public Property ReasonDescription() As String
            Get
                Return _reasonDescription
            End Get
            Set(ByVal value As String)
                _reasonDescription = value
            End Set
        End Property
        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("ValueMerchandising")> Public Property ValueMerchandising() As Decimal
            Get
                Return _valueMerchandising
            End Get
            Set(ByVal value As Decimal)
                _valueMerchandising = value
            End Set
        End Property
        <ColumnMapping("ValueNonMerchandising")> Public Property ValueNonMerchandising() As Decimal
            Get
                Return _valueNonMerchandising
            End Get
            Set(ByVal value As Decimal)
                _valueNonMerchandising = value
            End Set
        End Property
        <ColumnMapping("ValueTax")> Public Property ValueTax() As Decimal
            Get
                Return _valueTax
            End Get
            Set(ByVal value As Decimal)
                _valueTax = value
            End Set
        End Property
        <ColumnMapping("ValueDiscount")> Public Property ValueDiscount() As Decimal
            Get
                Return _valueDiscount
            End Get
            Set(ByVal value As Decimal)
                _valueDiscount = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("IsColleagueDiscount")> Public Property IsColleagueDiscount() As Boolean
            Get
                Return _isColleagueDiscount
            End Get
            Set(ByVal value As Boolean)
                _isColleagueDiscount = value
            End Set
        End Property
        <ColumnMapping("ValueColleagueDiscount")> Public Property ValueColleagueDiscount() As Decimal
            Get
                Return _valueColleagueDiscount
            End Get
            Set(ByVal value As Decimal)
                _valueColleagueDiscount = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CType(reader.ReadString, Integer)
                    Case "TRAN" : Me.TranNumber = CType(reader.ReadString, Integer)
                    Case "CASH" : Me.CashierId = CType(reader.ReadString, Integer)
                    Case "TIME"
                        Dim t As String = reader.ReadString.PadLeft(6, CType("0", Char))

                        Me.TranDateTime = t.Substring(0, 2) + ":"
                        Me.TranDateTime += t.Substring(2, 2) + ":"
                        Me.TranDateTime += t.Substring(4, 2)

                    Case "TCOD" : Me.Type = reader.ReadString
                    Case "MISC" : Me.ReasonCode = reader.ReadString
                    Case "DESC" : Me.ReasonDescription = reader.ReadString
                    Case "ORDN" : Me.OrderNumber = reader.ReadString
                    Case "MERC" : Me.ValueMerchandising = CType(reader.ReadString, Decimal)
                    Case "NMER" : Me.ValueNonMerchandising = CType(reader.ReadString, Decimal)
                    Case "TAXA" : Me.ValueTax = CType(reader.ReadString, Decimal)
                    Case "DISC" : Me.ValueDiscount = CType(reader.ReadString, Decimal)
                    Case "TOTL" : Me.Value = CType(reader.ReadString, Decimal)
                    Case "IEMP" : Me.IsColleagueDiscount = (reader.ReadString = "Y")
                    Case "PVEM" : Me.ValueColleagueDiscount = CType(reader.ReadString, Decimal)
                    Case "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select

            End While

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
            'writer.WriteStartElement("PVTOTS")
            'writer.WriteAttributeString("DATE", Me.TranDate.ToString("dd/MM/yyyy"))
            'writer.WriteAttributeString("TILL", Me.TillId.ToString)
            'writer.WriteAttributeString("TRAN", Me.TranNumber.ToString)
            'writer.WriteAttributeString("CASH", Me.CashierId.ToString)
            'writer.WriteAttributeString("TIME", Me.TranDateTime)
            'writer.WriteAttributeString("TYPE", Me.Type)
            'writer.WriteAttributeString("MISC", Me.ReasonCode)
            'writer.WriteAttributeString("DESC", Me.ReasonDescription)
            'writer.WriteAttributeString("ORDN", Me.OrderNumber)
            'writer.WriteAttributeString("MERC", Me.ValueMerchandising.ToString)
            'writer.WriteAttributeString("NMER", Me.ValueNonMerchandising.ToString)
            'writer.WriteAttributeString("TAXA", Me.ValueTax.ToString)
            'writer.WriteAttributeString("DISC", Me.ValueDiscount.ToString)
            'writer.WriteAttributeString("TOTL", Me.Value.ToString)
            'writer.WriteAttributeString("IEMP", Me.IsColleagueDiscount.ToString)
            'writer.WriteAttributeString("PVEM", Me.ValueColleagueDiscount.ToString)
            'writer.WriteAttributeString("PIVT", Me.IsPivotal.ToString)
            'writer.WriteEndElement()
        End Sub

#End Region

    End Class

    Public Class LineNonKB
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer
        Private _skuNumber As String
        Private _quantity As Integer
        Private _priceLookup As Decimal
        Private _price As Decimal
        Private _priceExVat As Decimal
        Private _priceExtended As Decimal
        Private _isRelatedItemSingle As Boolean
        Private _vatSymbol As String
        Private _isInStoreStockItem As Boolean
        Private _movementTypeCode As String
        Private _hieCategory As String
        Private _hieGroup As String
        Private _hieSubgroup As String
        Private _hieStyle As String
        Private _saleType As String
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("Quantity")> Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property
        <ColumnMapping("PriceLookup")> Public Property PriceLookup() As Decimal
            Get
                Return _priceLookup
            End Get
            Set(ByVal value As Decimal)
                _priceLookup = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("PriceExVat")> Public Property PriceExVat() As Decimal
            Get
                Return _priceExVat
            End Get
            Set(ByVal value As Decimal)
                _priceExVat = value
            End Set
        End Property
        <ColumnMapping("PriceExtended")> Public Property PriceExtended() As Decimal
            Get
                Return _priceExtended
            End Get
            Set(ByVal value As Decimal)
                _priceExtended = value
            End Set
        End Property
        <ColumnMapping("IsRelatedItemSingle")> Public Property IsRelatedItemSingle() As Boolean
            Get
                Return _isRelatedItemSingle
            End Get
            Set(ByVal value As Boolean)
                _isRelatedItemSingle = value
            End Set
        End Property
        <ColumnMapping("VatSymbol")> Public Property VatSymbol() As String
            Get
                Return _vatSymbol
            End Get
            Set(ByVal value As String)
                _vatSymbol = value
            End Set
        End Property
        <ColumnMapping("IsInStoreStockItem")> Public Property IsInStoreStockItem() As Boolean
            Get
                Return _isInStoreStockItem
            End Get
            Set(ByVal value As Boolean)
                _isInStoreStockItem = value
            End Set
        End Property
        <ColumnMapping("MovementTypeCode")> Public Property MovementTypeCode() As String
            Get
                Return _movementTypeCode
            End Get
            Set(ByVal value As String)
                _movementTypeCode = value
            End Set
        End Property
        <ColumnMapping("HieCategory")> Public Property HieCategory() As String
            Get
                Return _hieCategory
            End Get
            Set(ByVal value As String)
                _hieCategory = value
            End Set
        End Property
        <ColumnMapping("HieGroup")> Public Property HieGroup() As String
            Get
                Return _hieGroup
            End Get
            Set(ByVal value As String)
                _hieGroup = value
            End Set
        End Property
        <ColumnMapping("HieSubgroup")> Public Property HieSubgroup() As String
            Get
                Return _hieSubgroup
            End Get
            Set(ByVal value As String)
                _hieSubgroup = value
            End Set
        End Property
        <ColumnMapping("HieStyle")> Public Property HieStyle() As String
            Get
                Return _hieStyle
            End Get
            Set(ByVal value As String)
                _hieStyle = value
            End Set
        End Property
        <ColumnMapping("SaleType")> Public Property SaleType() As String
            Get
                Return _saleType
            End Get
            Set(ByVal value As String)
                _saleType = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CType(reader.ReadString, Integer)
                    Case "TRAN" : Me.TranNumber = CType(reader.ReadString, Integer)
                    Case "NUMB" : Me.Number = CType(reader.ReadString, Integer)
                    Case "SKUN" : Me.SkuNumber = reader.ReadString
                    Case "QUAN" : Me.Quantity = CType(reader.ReadString, Integer)
                    Case "SPRI" : Me.PriceLookup = CType(reader.ReadString, Decimal)
                    Case "PRIC" : Me.Price = CType(reader.ReadString, Decimal)
                    Case "PRVE" : Me.PriceExVat = CType(reader.ReadString, Decimal)
                    Case "EXTP" : Me.PriceExtended = CType(reader.ReadString, Decimal)
                    Case "RITM" : Me.IsRelatedItemSingle = (reader.ReadString = "Y")
                    Case "VSYM" : Me.VatSymbol = reader.ReadString
                    Case "PIVI" : Me.IsInStoreStockItem = (reader.ReadString = "Y")
                    Case "PIVM" : Me.MovementTypeCode = reader.ReadString
                    Case "CTGY" : Me.HieCategory = reader.ReadString
                    Case "GRUP" : Me.HieGroup = reader.ReadString
                    Case "SGRP" : Me.HieSubgroup = reader.ReadString
                    Case "STYL" : Me.HieStyle = reader.ReadString
                    Case "SALT" : Me.SaleType = reader.ReadString
                    Case Is = "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
            'writer.WriteStartElement("LINE")
            'writer.WriteAttributeString("DATE", Me.TranDate.ToString("dd/MM/yyyy"))
            'writer.WriteAttributeString("TILL", Me.TillId.ToString)
            'writer.WriteAttributeString("TRAN", Me.TranNumber.ToString)
            'writer.WriteAttributeString("NUMB", Me.Number.ToString)
            'writer.WriteAttributeString("SKUN", Me.SkuNumber)
            'writer.WriteAttributeString("QUAN", Me.Quantity.ToString)
            'writer.WriteAttributeString("SPRI", Me.PriceLookup.ToString)
            'writer.WriteAttributeString("PRIC", Me.Price.ToString)
            'writer.WriteAttributeString("PRVE", Me.PriceExVat.ToString)
            'writer.WriteAttributeString("EXTP", Me.PriceExtended.ToString)
            'writer.WriteAttributeString("RITM", Me.IsRelatedItemSingle.ToString)
            'writer.WriteAttributeString("VSYM", Me.VatSymbol)
            'writer.WriteAttributeString("PIVI", Me.IsInStoreStockItem.ToString)
            'writer.WriteAttributeString("PIVM", Me.MovementTypeCode)
            'writer.WriteAttributeString("CTGY", Me.HieCategory)
            'writer.WriteAttributeString("GRUP", Me.HieGroup)
            'writer.WriteAttributeString("SGRP", Me.HieSubgroup)
            'writer.WriteAttributeString("STYL", Me.HieStyle)
            'writer.WriteAttributeString("SALT", Me.SaleType)
            'writer.WriteAttributeString("PIVT", Me.IsPivotal.ToString)
            'writer.WriteEndElement()
        End Sub

#End Region

    End Class

    Public Class LineCollectionNonKB
        Inherits BaseCollection(Of LineNonKB)
    End Class

    Public Class PaymentNonKB
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer
        Private _tenderTypeId As Integer
        Private _valueTender As Decimal
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("TenderTypeId")> Public Property TenderTypeId() As Integer
            Get
                Return _tenderTypeId
            End Get
            Set(ByVal value As Integer)
                _tenderTypeId = value
            End Set
        End Property
        <ColumnMapping("ValueTender")> Public Property ValueTender() As Decimal
            Get
                Return _valueTender
            End Get
            Set(ByVal value As Decimal)
                _valueTender = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CType(reader.ReadString, Integer)
                    Case "TRAN" : Me.TranNumber = CType(reader.ReadString, Integer)
                    Case "NUMB" : Me.Number = CType(reader.ReadString, Integer)
                    Case "TYPE" : Me.TenderTypeId = CType(reader.ReadString, Integer)
                    Case "AMNT" : Me.ValueTender = CType(reader.ReadString, Decimal)
                    Case "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case "CARD", "EXDT", "AUTH", "TRID", "MSGN", "CASH" : reader.ReadString()
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
            'writer.WriteStartElement("TENDER")
            'writer.WriteAttributeString("DATE", Me.TranDate.ToString("dd/MM/yyyy"))
            'writer.WriteAttributeString("TILL", Me.TillId.ToString)
            'writer.WriteAttributeString("TRAN", Me.TranNumber.ToString)
            'writer.WriteAttributeString("NUMB", Me.Number.ToString)
            'writer.WriteAttributeString("TYPE", Me.TenderTypeId.ToString)
            'writer.WriteAttributeString("AMNT", Me.ValueTender.ToString)
            'writer.WriteAttributeString("PIVT", Me.IsPivotal.ToString)
            'writer.WriteEndElement()
        End Sub

#End Region

    End Class

    Public Class PaymentCollectionNonKB
        Inherits BaseCollection(Of PaymentNonKB)
    End Class

#End Region

#Region "KitchenAndBathroom"

    <XmlRoot("CTSVISIONDEPOSIT")> Public Class KitchenAndBathroom

#Region "Properties"

        Private _Total As TotalKB
        Private _PaymentCollection As PaymentCollectionKB
        Private _StockCollection As TpWickes.Library.VisionSale.Vision.StockCollection

        <XmlElement("PVTOTS")> Public Property Total() As TotalKB
            Get
                Return _Total
            End Get
            Set(ByVal value As TotalKB)
                _Total = value
            End Set
        End Property

        <XmlArray("PVPAID"), XmlArrayItem("TENDER")> Public Property Payments() As PaymentCollectionKB
            Get
                Return _PaymentCollection
            End Get
            Set(ByVal value As PaymentCollectionKB)
                _PaymentCollection = value
            End Set
        End Property

        <XmlArray("STOCK"), XmlArrayItem("LINE")> Public Property Stocks() As TpWickes.Library.VisionSale.Vision.StockCollection
            Get
                Return _StockCollection
            End Get
            Set(ByVal value As TpWickes.Library.VisionSale.Vision.StockCollection)
                _StockCollection = value
            End Set
        End Property

#End Region

#Region "Public Methods"

        Public Shared Function Deserialise(ByVal xmlString As String) As KitchenAndBathroom

            Dim VisionSale As IVisionSaleStockProcessing

            VisionSale = (New VisionSaleStockProcessingFactory).GetImplementation()
            xmlString = VisionSale.RemoveStockNodeFromXmlDocument(xmlString)



            Dim response As KitchenAndBathroom = Nothing
            Dim serializer As New XmlSerializer(GetType(KitchenAndBathroom))

            Using reader As TextReader = New StringReader(xmlString)
                response = CType(serializer.Deserialize(reader), KitchenAndBathroom)
            End Using
            Return response

        End Function

        Public Function Persist() As Boolean

            Return Persist(Me)

        End Function

#End Region

#Region "Private Methods"

        Private Function Persist(ByVal wrapper As KitchenAndBathroom) As Boolean

            Dim VisionSale As IVisionSaleStockProcessing = (New VisionSaleStockProcessingFactory).GetImplementation()

            Using con As New Connection

                Try

                    Dim Validation As IVisionValidation

                    Validation = VisionValidationFactory.FactoryGet
                    If Validation.Valid(wrapper.Total.Total, wrapper.Payments.Count) = True Then

                        con.StartTransaction()

                        TotalVision(wrapper.Total, con)
                        PaymentsVision(wrapper.Payments, con)

                        If VisionSale.SaveStock(wrapper.Stocks) = True Then

                            If VisionSale.StockPersist(con, wrapper.Stocks, CType(wrapper.Total.CashierId, Integer)) = False Then

                                con.RollbackTransaction()
                                Return False

                            End If

                        End If

                        con.CommitTransaction()
                        Return True

                    Else

                        Return False

                    End If

                Catch ex As Exception

                    con.RollbackTransaction()
                    Throw

                End Try

            End Using

        End Function

        Private Function TotalVision(ByVal tot As TotalKB, ByRef con As Connection) As Integer

            Using com As Command = con.NewCommand(My.Resources.Procedures.VisionSaleTotal)

                com.AddParameter(My.Resources.Parameters.TranDate, tot.TranDate)
                com.AddParameter(My.Resources.Parameters.TillId, tot.TillId)
                com.AddParameter(My.Resources.Parameters.TranNumber, tot.TranNumber)
                com.AddParameter(My.Resources.Parameters.CashierId, tot.CashierId)
                com.AddParameter(My.Resources.Parameters.TranDateTime, tot.TranDateTime)
                com.AddParameter(My.Resources.Parameters.TransactionCode, tot.TransactionCode)
                com.AddParameter(My.Resources.Parameters.OrderNumber, tot.OrderNumber)
                com.AddParameter(My.Resources.Parameters.ExternalDocumentNumber, tot.ExternalDocumentNumber)
                com.AddParameter(My.Resources.Parameters.MerchandiseAmount, tot.MerchandiseAmount)
                com.AddParameter(My.Resources.Parameters.NonMerchandising, tot.NonMerchandising)
                com.AddParameter(My.Resources.Parameters.TaxAmount, tot.TaxAmount)
                com.AddParameter(My.Resources.Parameters.Discount, tot.Discount)
                com.AddParameter(My.Resources.Parameters.Total, tot.Total)
                com.AddParameter(My.Resources.Parameters.EmployeeDiscount, tot.EmployeeDiscount)
                com.AddParameter(My.Resources.Parameters.RefundCshrNumber, tot.RefundCshrNumber)
                Return com.ExecuteNonQuery

            End Using

        End Function

        Private Function PaymentsVision(ByVal payments As PaymentCollectionKB, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As PaymentKB In payments
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionSalePayment)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.TenderTypeId, l.TenderTypeId)
                    com.AddParameter(My.Resources.Parameters.ValueTender, l.ValueTender)
                    com.AddParameter(My.Resources.Parameters.CreditNumber, l.CreditNumber)
                    com.AddParameter(My.Resources.Parameters.CreditExpireDate, l.CreditExpireDate)
                    linesAffected += com.ExecuteNonQuery
                End Using

                If l.TenderTypeId = 13 Then
                    Using com As Command = con.NewCommand("usp_SaveDataInDLGiftCard")
                        com.AddParameter("@Date1", l.TranDate)
                        com.AddParameter("@Till", l.TillId)
                        com.AddParameter("@Tran", l.TranNumber)
                        com.AddParameter("@CardNum", l.CreditNumber)
                        com.AddParameter("@EEID", l.CashierId)
                        com.AddParameter("@TYPE", "TS")
                        com.AddParameter("@AMNT", -l.ValueTender)
                        com.AddParameter("@AUTH", l.AuthorizationCode)
                        com.AddParameter("@MSGNUM", l.MessageNumber)
                        com.AddParameter("@TRANID", l.TransactionId)
                        com.ExecuteNonQuery()
                    End Using
                End If
            Next
            Return linesAffected

        End Function

#End Region

    End Class

    Public Class TotalKB
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _tranDate As Date
        Private _tillId As String
        Private _tranNumber As String
        Private _cashierId As String
        Private _tranDateTime As String
        Private _suprvCashierNo As String
        Private _transactionCode As String
        Private _miscReasonCodeOccurance As String
        Private _descriptionNo As String
        Private _orderNumber As String
        Private _externalDocumentNo As String
        Private _merchandiseAmount As Decimal
        Private _nonMerchandising As Decimal
        Private _taxAmount As Decimal
        Private _discountAmount As Decimal
        Private _totalSaleAmount As Decimal
        Private _employeeDiscount As Boolean
        Private _indicateUpdted As String
        Private _previous As String
        Private _isPivotal As Boolean

        <ColumnMapping("DATE1")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillId() As String
            Get
                Return _tillId
            End Get
            Set(ByVal value As String)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TranNumber() As String
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As String)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("CASH")> Public Property CashierId() As String
            Get
                Return _cashierId
            End Get
            Set(ByVal value As String)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("TIME")> Public Property TranDateTime() As String
            Get
                Return _tranDateTime
            End Get
            Set(ByVal value As String)
                _tranDateTime = value
            End Set
        End Property
        <ColumnMapping("SUPV")> Public Property Supervisor() As String
            Get
                Return _suprvCashierNo
            End Get
            Set(ByVal value As String)
                _suprvCashierNo = value
            End Set
        End Property
        <ColumnMapping("TCOD")> Public Property TransactionCode() As String
            Get
                Return _transactionCode
            End Get
            Set(ByVal value As String)
                _transactionCode = value
            End Set
        End Property

        <ColumnMapping("MISC")> Public Property MiscReason() As String
            Get
                Return _miscReasonCodeOccurance
            End Get
            Set(ByVal value As String)
                _miscReasonCodeOccurance = value
            End Set
        End Property
        <ColumnMapping("DESCR")> Public Property DescriptionNumber() As String
            Get
                Return _descriptionNo
            End Get
            Set(ByVal value As String)
                _descriptionNo = value
            End Set
        End Property
        <ColumnMapping("ORDN")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property

        <ColumnMapping("DOCN")> Public Property ExternalDocumentNumber() As String
            Get
                Return _externalDocumentNo
            End Get
            Set(ByVal value As String)
                _externalDocumentNo = value
            End Set
        End Property

        <ColumnMapping("MERC")> Public Property MerchandiseAmount() As Decimal
            Get
                Return _merchandiseAmount
            End Get
            Set(ByVal value As Decimal)
                _merchandiseAmount = value
            End Set
        End Property
        <ColumnMapping("NMER")> Public Property NonMerchandising() As Decimal
            Get
                Return _nonMerchandising
            End Get
            Set(ByVal value As Decimal)
                _nonMerchandising = value
            End Set
        End Property
        <ColumnMapping("TAXA")> Public Property TaxAmount() As Decimal
            Get
                Return _taxAmount
            End Get
            Set(ByVal value As Decimal)
                _taxAmount = value
            End Set
        End Property
        <ColumnMapping("DISC")> Public Property Discount() As Decimal
            Get
                Return _discountAmount
            End Get
            Set(ByVal value As Decimal)
                _discountAmount = value
            End Set
        End Property
        <ColumnMapping("TOTL")> Public Property Total() As Decimal
            Get
                Return _totalSaleAmount
            End Get
            Set(ByVal value As Decimal)
                _totalSaleAmount = value
            End Set
        End Property

        <ColumnMapping("IEMP")> Public Property EmployeeDiscount() As Boolean
            Get
                Return _employeeDiscount
            End Get
            Set(ByVal value As Boolean)
                _employeeDiscount = value
            End Set
        End Property

        <ColumnMapping("PVEM")> Public Property Previous() As String
            Get
                Return _previous
            End Get
            Set(ByVal value As String)
                _previous = value
            End Set
        End Property

        <ColumnMapping("PIVT")> Public Property Pivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property

        <ColumnMapping("CBBU")> Public Property RefundCshrNumber() As String
            Get
                Return _indicateUpdted
            End Get
            Set(ByVal value As String)
                _indicateUpdted = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            Try

                While (reader.Read())

                    Select Case reader.LocalName
                        Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                        Case "TILL" : Me.TillId = reader.ReadString
                        Case "TRAN" : Me.TranNumber = reader.ReadString
                        Case "CASH" : Me.CashierId = reader.ReadString
                        Case "TIME" : Me.TranDateTime = reader.ReadString
                        Case "TCOD" : Me.TransactionCode = reader.ReadString
                        Case "MISC"
                            If (reader.ReadString.Length > 0) Then
                                Me.MiscReason = (reader.ReadString)
                            Else
                                Me.MiscReason = String.Empty
                            End If
                        Case "DESC"
                            If (reader.ReadString.Length > 0) Then
                                Me.DescriptionNumber = reader.ReadString
                            Else
                                Me.DescriptionNumber = String.Empty
                            End If
                        Case "ORDN" : Me.OrderNumber = reader.ReadString
                        Case "MERC" : Me.MerchandiseAmount = CType(reader.ReadString, Decimal)
                        Case "NMER" : Me.NonMerchandising = CType(reader.ReadString, Decimal)
                        Case "TAXA" : Me.TaxAmount = CType(reader.ReadString, Decimal)
                        Case "DISC" : Me.Discount = CType(reader.ReadString, Decimal)
                        Case "TOTL" : Me.Total = CType(reader.ReadString, Decimal)
                        Case "IEMP" : Me.EmployeeDiscount = (reader.ReadString = "Y")
                        Case "PVEM"
                            If (reader.ReadString.Length > 0) Then
                                Me.Previous = (reader.ReadString)
                            Else
                                Me.Previous = String.Empty
                            End If
                        Case "PIVT"
                            If (reader.ReadString.Length > 0) Then
                                Me.Pivotal = (reader.ReadString = "Y")
                            Else
                                Me.Pivotal = False
                            End If
                        Case Else
                            reader.ReadEndElement()
                            Exit While
                    End Select
                End While
                If System.DateTime.Equals(Me.TranDate, DateTime.Now.Date) Then
                    Me.RefundCshrNumber = Space(8)

                Else
                    Dim t As String = DateTime.Now.Date.ToString()
                    Me.RefundCshrNumber = t.Substring(0, 2) + "/"
                    Me.RefundCshrNumber += t.Substring(3, 2) + "/"
                    Me.RefundCshrNumber += t.Substring(8, 2)
                End If
                Me.ExternalDocumentNumber = Me.OrderNumber


            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
            'writer.WriteStartElement("PVTOTS")
            'writer.WriteAttributeString("DATE", Me.TranDate.ToString("dd/MM/yyyy"))
            'writer.WriteAttributeString("TILL", Me.TillId)
            'writer.WriteAttributeString("TRAN", Me.TranNumber)
            'writer.WriteAttributeString("CASH", Me.CashierId)
            'writer.WriteAttributeString("TIME", Me.TranDateTime)
            'writer.WriteAttributeString("TCOD", Me.TransactionCode)
            'writer.WriteAttributeString("MISC", Me.MiscReason)
            'writer.WriteAttributeString("DESC", Me.DescriptionNumber)
            'writer.WriteAttributeString("ORDN", Me.OrderNumber)
            'writer.WriteAttributeString("DOCN", Me.ExternalDocumentNumber)
            'writer.WriteAttributeString("MERC", Me.MerchandiseAmount.ToString)
            'writer.WriteAttributeString("NMER", Me.NonMerchandising.ToString)
            'writer.WriteAttributeString("TAXA", Me.TaxAmount.ToString)
            'writer.WriteAttributeString("DISC", Me.Discount.ToString)
            'writer.WriteAttributeString("TOTL", Me.Total.ToString)
            'writer.WriteAttributeString("IEMP", Me.EmployeeDiscount.ToString)
            'writer.WriteAttributeString("CBBU", Me.RefundCshrNumber)
            'writer.WriteEndElement()
        End Sub

#End Region

    End Class

    Public Class PaymentKB
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _tranDate As Date
        Private _tillId As String
        Private _tranNumber As String
        Private _number As Integer
        Private _tenderTypeId As Decimal
        Private _valueTender As Decimal
        Private _creditCardNumber As String
        Private _creditCardExpireDate As String
        Private _isPivotal As Boolean
        Private _authorizationCode As String
        Private _cashierId As String
        Private _messageNumber As String
        Private _transactionId As Decimal

        <ColumnMapping("DATE1")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillId() As String
            Get
                Return _tillId
            End Get
            Set(ByVal value As String)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TranNumber() As String
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As String)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("NUMB")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("TYPE")> Public Property TenderTypeId() As Decimal
            Get
                Return _tenderTypeId
            End Get
            Set(ByVal value As Decimal)
                _tenderTypeId = value
            End Set
        End Property
        <ColumnMapping("AMNT")> Public Property ValueTender() As Decimal
            Get
                Return _valueTender
            End Get
            Set(ByVal value As Decimal)
                _valueTender = value
            End Set
        End Property
        <ColumnMapping("CARD")> Public Property CreditNumber() As String
            Get
                Return _creditCardNumber
            End Get
            Set(ByVal value As String)
                _creditCardNumber = value
            End Set
        End Property
        <ColumnMapping("EXDT")> Public Property CreditExpireDate() As String
            Get
                Return _creditCardExpireDate
            End Get
            Set(ByVal value As String)
                _creditCardExpireDate = value
            End Set
        End Property
        <ColumnMapping("PIVT")> Public Property Pivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property
        <ColumnMapping("AUTH")> Public Property AuthorizationCode() As String
            Get
                Return _authorizationCode
            End Get
            Set(ByVal value As String)
                _authorizationCode = value
            End Set
        End Property
        <ColumnMapping("CASH")> Public Property CashierId() As String
            Get
                Return _cashierId
            End Get
            Set(ByVal value As String)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("MSGN")> Public Property MessageNumber() As String
            Get
                Return _messageNumber
            End Get
            Set(ByVal value As String)
                _messageNumber = value
            End Set
        End Property
        <ColumnMapping("TRID")> Public Property TransactionId() As Decimal
            Get
                Return _transactionId
            End Get
            Set(ByVal value As Decimal)
                _transactionId = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = reader.ReadString
                    Case "TRAN" : Me.TranNumber = reader.ReadString
                    Case "NUMB" : Me.Number = CType(reader.ReadString, Integer)
                    Case "TYPE" : Me.TenderTypeId = CType(reader.ReadString, Decimal)
                    Case "AMNT" : Me.ValueTender = CType(reader.ReadString, Decimal)
                    Case "CARD" : Me.CreditNumber = reader.ReadString
                    Case "EXDT" : Me.CreditExpireDate = reader.ReadString
                    Case "PIVT"
                        If (reader.ReadString.Length > 0) Then
                            Me.Pivotal = (reader.ReadString = "Y")
                        Else
                            Me.Pivotal = False
                        End If
                    Case "AUTH" : Me.AuthorizationCode = reader.ReadString
                    Case "CASH" : Me.CashierId = reader.ReadString
                    Case "MSGN" : Me.MessageNumber = reader.ReadString
                    Case "TRID" : Me.TransactionId = CType(reader.ReadString, Decimal)
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
            'writer.WriteStartElement("TENDER")
            'writer.WriteAttributeString("DATE", Me.TranDate.ToString("dd/MM/yyyy"))
            'writer.WriteAttributeString("TILL", Me.TillId)
            'writer.WriteAttributeString("TRAN", Me.TranNumber)
            'writer.WriteAttributeString("NUMB", Me.Number.ToString)
            'writer.WriteAttributeString("TYPE", Me.TenderTypeId.ToString)
            'writer.WriteAttributeString("AMNT", Me.ValueTender.ToString)
            'writer.WriteAttributeString("CARD", Me.CreditNumber)
            'writer.WriteAttributeString("EXDT", Me.CreditExpireDate)
            'writer.WriteEndElement()
        End Sub

#End Region

    End Class

    Public Class PaymentCollectionKB
        Inherits BaseCollection(Of PaymentKB)
    End Class

#End Region

#Region "Common - Stock"

    Public Class Stock
        Inherits Base
        Implements IXmlSerializable

#Region "Properties"

        Private _Skun As String
        Private _Quantity As Integer

        <ColumnMapping("SKUN")> Public Property StockCode() As String
            Get
                Return _Skun
            End Get
            Set(ByVal value As String)
                _Skun = value
            End Set
        End Property

        <ColumnMapping("QUAN")> Public Property Quantity() As Integer
            Get
                Return _Quantity
            End Get
            Set(ByVal value As Integer)
                _Quantity = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema

            Return Nothing

        End Function

        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "SKUN" : Me.StockCode = reader.ReadString
                    Case "QUAN" : Me.Quantity = CType(reader.ReadString, Integer)
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml

            'writer.WriteStartElement("LINE")
            'writer.WriteAttributeString("SKUN", Me.StockCode)
            'writer.WriteAttributeString("QUAN", Me.Quantity.ToString)
            'writer.WriteEndElement()

        End Sub

#End Region

    End Class

    Public Class StockCollection
        Inherits BaseCollection(Of Stock)
    End Class

#End Region

End Namespace