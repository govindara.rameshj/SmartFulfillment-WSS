﻿'Namespace Vision

'    Public Class Stock
'        Inherits Base
'        Implements IXmlSerializable

'#Region "Properties"

'        Private _Skun As String
'        Private _Quantity As Integer

'        <ColumnMapping("SKUN")> Public Property StockCode() As String
'            Get
'                Return _Skun
'            End Get
'            Set(ByVal value As String)
'                _Skun = value
'            End Set
'        End Property

'        <ColumnMapping("QUAN")> Public Property Quantity() As Integer
'            Get
'                Return _Quantity
'            End Get
'            Set(ByVal value As Integer)
'                _Quantity = value
'            End Set
'        End Property

'#End Region

'#Region "Methods"

'        Public Sub New()
'            MyBase.New()
'        End Sub

'        Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema

'            Return Nothing

'        End Function

'        Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

'            While (reader.Read())
'                Select Case reader.LocalName
'                    Case "SKUN" : Me.StockCode = reader.ReadString
'                    Case "QUAN" : Me.Quantity = CType(reader.ReadString, Integer)
'                    Case Else
'                        reader.ReadEndElement()
'                        Exit While
'                End Select
'            End While

'        End Sub

'        Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml

'            'writer.WriteStartElement("LINE")
'            'writer.WriteAttributeString("SKUN", Me.StockCode)
'            'writer.WriteAttributeString("QUAN", Me.Quantity.ToString)
'            'writer.WriteEndElement()

'        End Sub

'#End Region

'    End Class

'    Public Class StockCollection
'        Inherits BaseCollection(Of Stock)
'    End Class

'End Namespace