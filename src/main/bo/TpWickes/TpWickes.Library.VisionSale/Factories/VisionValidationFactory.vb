﻿Public Class VisionValidationFactory

    Private Shared _mFactoryMember As IVisionValidation

    Public Shared Function FactoryGet() As IVisionValidation

        If _mFactoryMember Is Nothing Then

            Return New VisionValidation

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IVisionValidation)

        _mFactoryMember = obj

    End Sub

End Class