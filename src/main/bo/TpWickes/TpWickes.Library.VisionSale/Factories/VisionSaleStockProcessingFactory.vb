﻿Public Class VisionSaleStockProcessingFactory
    Inherits RequirementSwitchFactory(Of IVisionSaleStockProcessing)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IVisionSaleStockProcessing

        Return New VisionSaleWithStockProcessing

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IVisionSaleStockProcessing

        Return New VisionSaleWithoutStockProcessing

    End Function

End Class