﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                              "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                              "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                              "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                              "6da046ba")> 
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.IntegrationTest, " & _
                                                       "PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                       "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                       "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                       "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                       "6da046ba" _
                                                       )> 
Public Class GetTextCommunicationRepository
    Implements IGetTextCommunicationRepository

    Public Function GetTextCommunications(ByVal FunctionalAreaID As Integer, _
                                          ByVal TypeID As Integer) As DataTable Implements [Interface].IGetTextCommunicationRepository.GetTextCommunications

        Return GetTextComms(FunctionalAreaID, TypeID)
    End Function

    Public Function GetTextCommunications(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, _
                                          ByVal SequenceNumber As Integer) As DataTable Implements IGetTextCommunicationRepository.GetTextCommunications

        Return GetTextComms(FunctionalAreaID, TypeID, SequenceNumber)
    End Function

#Region "Private Procedures & Functions"

    Friend Function GetTextComms(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer) As DataTable
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As ICommand = New Command(con)
                AddStoredProcedure(com)
                AddParameters(com, FunctionalAreaID, TypeID)

                Return com.ExecuteDataTable()
            End Using
        End Using
    End Function

    Friend Function GetTextComms(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, ByVal SequenceNumber As Integer) As DataTable
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As ICommand = New Command(con)
                AddStoredProcedure(com)
                AddParameters(com, FunctionalAreaID, TypeID, SequenceNumber)

                Return com.ExecuteDataTable()
            End Using
        End Using
    End Function

    Friend Sub AddStoredProcedure(ByRef X As Cts.Oasys.Data.ICommand)

        X.StoredProcedureName = My.Resources.Procedures.TextCommunicationsGet
    End Sub

    Friend Sub AddParameters(ByRef X As Cts.Oasys.Data.ICommand, ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer)

        X.AddParameter(My.Resources.Parameters.FunctionalAreaId, FunctionalAreaID)
        X.AddParameter(My.Resources.Parameters.TypeId, TypeID)
    End Sub

    Friend Sub AddParameters(ByRef X As Cts.Oasys.Data.ICommand, _
                             ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, ByVal SequenceNumber As Integer)

        X.AddParameter(My.Resources.Parameters.FunctionalAreaId, FunctionalAreaID)
        X.AddParameter(My.Resources.Parameters.TypeId, TypeID)
        X.AddParameter(My.Resources.Parameters.Sequence, SequenceNumber)
    End Sub
#End Region
End Class