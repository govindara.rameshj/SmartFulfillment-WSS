﻿Public Class OfflineModeRepository
    Implements IOfflineModeRepository

    Public Sub SetOfflineMode(ByVal InOfflineMode As Boolean) Implements [Interface].IOfflineModeRepository.SetOfflineMode

        Try
            ' Always set on the local (offline) database
            Using con As New OfflineModeConnection()
                Using Com As ICommand = CommandFactory.FactoryGet(con)

                    AddSetStoredProcedure(Com)
                    AddInOfflineModeParameter(Com, InOfflineMode)
                    Com.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception("Failed to Set OfflineMode to '" & InOfflineMode.ToString & "'")
        End Try
    End Sub

    Public Function IsInOfflineMode() As Boolean Implements [Interface].IOfflineModeRepository.IsInOfflineMode

        Try
            If RequirementRepositoryFactory.FactoryGet.RequirementEnabled(RequirementSwitchID) Then
                ' Always use the flag in the local (offline) database
                Using con As New OfflineModeConnection()
                    Using Com As ICommand = New Command(con)
                        Dim dt As DataTable

                        AddGetStoredProcedure(Com)
                        dt = Com.ExecuteDataTable
                        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                            Return CBool(dt.Rows(0).Item("BooleanValue"))
                        Else
                            Return False
                        End If
                    End Using
                End Using
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Friend Sub AddSetStoredProcedure(ByRef X As ICommand)

        X.StoredProcedureName = My.Resources.Procedures.OfflineModeSet
    End Sub

    Friend Sub AddGetStoredProcedure(ByRef X As ICommand)

        X.StoredProcedureName = My.Resources.Procedures.OfflineModeGet
    End Sub

    Friend Sub AddInOfflineModeParameter(ByRef X As ICommand, ByVal InOfflineMode As Boolean)

        X.AddParameter(My.Resources.Parameters.InOfflineMode, IIf(InOfflineMode, 1, 0))
    End Sub

    Friend ReadOnly Property RequirementSwitchID() As Integer Implements [Interface].IOfflineModeRepository.RequirementSwitchId
        Get
            RequirementSwitchID = 984151
        End Get
    End Property
End Class
