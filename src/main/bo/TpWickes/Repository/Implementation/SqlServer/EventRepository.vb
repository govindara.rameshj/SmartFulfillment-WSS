﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.UnitTest, " & _
                                                       "PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                       "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                       "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                       "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                       "6da046ba" _
                                                       )> 
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.IntegrationTest, " & _
                                                       "PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                       "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                       "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                       "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                       "6da046ba" _
                                                       )> 
Public Class EventRepository
    Implements IEventRepository

#Region "Single Sku Price Override - Get"

    Public Function GetExistingTemporaryPriceChangeForSkuHeader(ByVal SKU As String, _
                                                                ByVal StartDate As Date, ByVal EndDate As Date, _
                                                                ByVal Deleted As Boolean) As DataTable Implements IEventRepository.GetExistingTemporaryPriceChangeForSkuHeader
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                SingleSkuPriceOverrideGetExisting(com, My.Resources.Procedures.GetExistingTemporaryPriceChangeForSkuHeader, "TS", SKU, StartDate, EndDate, Deleted)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetExistingTemporaryPriceChangeForSkuDetail(ByVal SKU As String, _
                                                                ByVal StartDate As Date, ByVal EndDate As Date, _
                                                                ByVal Deleted As Boolean) As DataTable Implements IEventRepository.GetExistingTemporaryPriceChangeForSkuDetail
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                SingleSkuPriceOverrideGetExisting(com, My.Resources.Procedures.GetExistingTemporaryPriceChangeForSkuDetail, "TS", SKU, StartDate, EndDate, Deleted)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetOverrideTemporaryPriceChangeForSkuHeader(ByVal SKU As String, _
                                                                ByVal StartDate As Date, ByVal EndDate As Date) As DataTable Implements IEventRepository.GetOverrideTemporaryPriceChangeForSkuHeader
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                SingleSkuPriceOverrideGetOverride(com, My.Resources.Procedures.GetOverrideTemporaryPriceChangeForSkuHeader, EventTypeEnumerator.TemporaryPriceOverrideSKU, SKU, StartDate, EndDate)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetOverrideTemporaryPriceChangeForSkuDetail(ByVal SKU As String, _
                                                                ByVal StartDate As Date, ByVal EndDate As Date) As DataTable Implements IEventRepository.GetOverrideTemporaryPriceChangeForSkuDetail
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                SingleSkuPriceOverrideGetOverride(com, My.Resources.Procedures.GetOverrideTemporaryPriceChangeForSkuDetail, EventTypeEnumerator.TemporaryPriceOverrideSKU, SKU, StartDate, EndDate)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

#End Region

#Region "Multiple Sku Price Override - Get"

    Public Function GetExistingDealGroup(ByVal SkuList As String) As DataTable Implements IEventRepository.GetExistingDealGroup
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                MultipleSkuPriceOverrideGetExistingDealGroup(com, My.Resources.Procedures.MultipleSkuPriceOverrideGetExistingDealGroup, SkuList)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetOverrideDealGroup(ByVal SelectedDate As Date, ByVal SkuList As String) As System.Data.DataTable Implements IEventRepository.GetOverrideDealGroup
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                MultipleSkuPriceOverrideGetOverrideDealGroup(com, My.Resources.Procedures.MultipleSkuPriceOverrideGetOverrideDealGroup, SelectedDate, SkuList)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetEventMaster(ByVal SelectedDate As Date, ByVal SkuList As String) As DataTable Implements IEventRepository.GetEventMaster
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                MultipleSkuPriceOverrideGetEventMaster(com, My.Resources.Procedures.MultipleSkuPriceOverrideGetEventMaster, SelectedDate, SkuList)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Function GetValidEvent(ByVal EventNumber As String, ByVal Priority As String) As DataTable Implements IEventRepository.GetValidEvent
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                MultipleSkuPriceOverrideGetValidEvent(com, My.Resources.Procedures.MultipleSkuPriceOverrideGetValidEvent, EventNumber, Priority)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function
#End Region

#Region "Single & Multiple Sku Price Override - Set"

    Public Function SetOverrideTemporaryPriceChange(ByVal EventType As EventTypeEnumerator, _
                                                    ByVal Description As String, ByVal StartDate As Date, ByVal EndDate As Date, _
                                                    ByVal SkuList As String, ByVal QuantityList As String, _
                                                    ByVal Price As Decimal, ByVal CashierSecurityID As Integer, _
                                                    ByVal AuthorisorSecurityID As Integer, ByRef EventHeaderOerrideID As Integer) As Boolean Implements IEventRepository.SetOverrideTemporaryPriceChange
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)

                Dim EventHeaderIdentity As SqlClient.SqlParameter = Nothing
                Dim Success As SqlClient.SqlParameter = Nothing

                TemporaryPriceChangeSet(com, EventHeaderIdentity, Success, My.Resources.Procedures.SetTemporaryPriceChanges, _
                                        EventType, Description, StartDate, EndDate, _
                                        SkuList, QuantityList, Price, CashierSecurityID, AuthorisorSecurityID)
                com.ExecuteNonQuery()
                EventHeaderOerrideID = CType(EventHeaderIdentity.Value, Integer)

                Return CType(Success.Value, Boolean)
            End Using
        End Using
    End Function
#End Region

#Region "Private Procedures & Functions"

#Region "Single SKU Price Override - Get"

    Friend Sub SingleSkuPriceOverrideGetExisting(ByRef X As Cts.Oasys.Data.Command, _
                                                 ByVal StoredProcedureName As String, _
                                                 ByVal EventType As String, ByVal SKU As String, _
                                                 ByVal StartDate As Date, ByVal EndDate As Date, ByVal Deleted As Boolean)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.EventTypeID, EventType, SqlDbType.Char, 2)
        X.AddParameter(My.Resources.Parameters.SkuNumber, SKU, SqlDbType.Char, 6)
        X.AddParameter(My.Resources.Parameters.StartDate, StartDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.EndDate, EndDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.Deleted, Deleted, SqlDbType.Bit)
    End Sub

    Friend Sub SingleSkuPriceOverrideGetOverride(ByRef X As Cts.Oasys.Data.Command, _
                                                 ByVal StoredProcedureName As String, _
                                                 ByVal EventType As EventTypeEnumerator, ByVal SKU As String, _
                                                 ByVal StartDate As Date, ByVal EndDate As Date)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.EventTypeID, EventType, SqlDbType.Int)
        X.AddParameter(My.Resources.Parameters.SkuNumber, SKU, SqlDbType.Char, 6)
        X.AddParameter(My.Resources.Parameters.StartDate, StartDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.EndDate, EndDate, SqlDbType.Date)
    End Sub

#End Region

#Region "Multiple SKU Price Override - Get"

    Friend Sub MultipleSkuPriceOverrideGetExistingDealGroup(ByRef X As Cts.Oasys.Data.Command, _
                                                            ByVal StoredProcedureName As String, ByVal SkuList As String)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.SkuNumbers, SkuList, SqlDbType.Char)
    End Sub

    Friend Sub MultipleSkuPriceOverrideGetOverrideDealGroup(ByRef X As Cts.Oasys.Data.Command, _
                                                            ByVal StoredProcedureName As String, ByVal SelectedDate As Date, ByVal SkuList As String)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.SelectedDate, SelectedDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.SkuNumbers, SkuList, SqlDbType.VarChar)
    End Sub

    Friend Sub MultipleSkuPriceOverrideGetEventMaster(ByRef X As Cts.Oasys.Data.Command, _
                                                      ByVal StoredProcedureName As String, ByVal SelectedDate As Date, ByVal SkuList As String)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.SelectedDate, SelectedDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.SkuNumbers, SkuList, SqlDbType.VarChar)
    End Sub

    Friend Sub MultipleSkuPriceOverrideGetValidEvent(ByRef X As Cts.Oasys.Data.Command, _
                                                     ByVal StoredProcedureName As String, ByVal EventNumber As String, ByVal Priority As String)

        AddStoredProcedure(X, StoredProcedureName)
        X.AddParameter(My.Resources.Parameters.EventNumber, EventNumber, SqlDbType.Char)
        X.AddParameter(My.Resources.Parameters.Priority, Priority, SqlDbType.Char)
    End Sub

#End Region

#Region "Single & Multiple Sku Price Override - Set"

    Friend Sub TemporaryPriceChangeSet(ByRef X As Cts.Oasys.Data.Command, _
                                       ByRef EventHeaderIdentity As SqlClient.SqlParameter, _
                                       ByRef Success As SqlClient.SqlParameter, _
                                       ByVal StoredProcedureName As String, _
                                       ByVal EventType As EventTypeEnumerator, ByVal Description As String, _
                                       ByVal StartDate As Date, ByVal EndDate As Date, ByVal SkuList As String, _
                                       ByVal QuantityList As String, ByVal Price As Decimal, _
                                       ByVal CashierSecurityID As Integer, ByVal AuthorisorSecurityID As Integer)

        AddStoredProcedure(X, StoredProcedureName)

        X.AddParameter(My.Resources.Parameters.EventTypeID, EventType, SqlDbType.Int)
        X.AddParameter(My.Resources.Parameters.Description, Description, SqlDbType.NVarChar, 1000)
        X.AddParameter(My.Resources.Parameters.StartDate, StartDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.EndDate, EndDate, SqlDbType.Date)
        X.AddParameter(My.Resources.Parameters.SkuNumbers, SkuList, SqlDbType.VarChar)
        X.AddParameter(My.Resources.Parameters.Quantities, QuantityList, SqlDbType.VarChar)
        X.AddParameter(My.Resources.Parameters.Price, Price, SqlDbType.Decimal)
        X.AddParameter(My.Resources.Parameters.CashierSecurityID, CashierSecurityID, SqlDbType.Int)
        X.AddParameter(My.Resources.Parameters.AuthorisorSecurityID, AuthorisorSecurityID, SqlDbType.Int)

        EventHeaderIdentity = X.AddOutputParameter(My.Resources.Parameters.EventHeaderID, SqlDbType.Int)
        Success = X.AddOutputParameter(My.Resources.Parameters.Success, SqlDbType.Bit)
    End Sub
#End Region

#Region "Single & Multiple Sku Price Override - Common"

    Private Sub AddStoredProcedure(ByRef X As Cts.Oasys.Data.Command, ByVal StoredProcedureName As String)

        X.StoredProcedureName = StoredProcedureName
    End Sub
#End Region
#End Region
End Class