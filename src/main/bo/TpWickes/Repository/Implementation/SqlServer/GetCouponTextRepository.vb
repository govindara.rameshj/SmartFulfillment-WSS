﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                              "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                              "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                              "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                              "6da046ba")> 
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.IntegrationTest, " & _
                                                       "PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                       "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                       "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                       "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                       "6da046ba" _
                                                       )> 
Public Class GetCouponTextRepository
    Implements IGetCouponTextRepository

    Public Function GetCouponTextIncludeDeletedTrue(ByVal CouponID As String) As DataTable Implements IGetCouponTextRepository.GetCouponTextIncludeDeletedTrue

        Return GetCouponText(CouponID, True)
    End Function

    Public Function GetCouponTextIncludeDeletedFalse(ByVal CouponID As String) As DataTable Implements IGetCouponTextRepository.GetCouponTextIncludeDeletedFalse

        Return GetCouponText(CouponID, False)
    End Function

    Public Function GetCouponTextIncludeDeletedTrue(ByVal CouponID As String, ByVal SequenceNumber As String) As DataTable Implements IGetCouponTextRepository.GetCouponTextIncludeDeletedTrue

        Return GetCouponText(CouponID, SequenceNumber, True)
    End Function

    Public Function GetCouponTextIncludeDeletedFalse(ByVal CouponID As String, ByVal SequenceNumber As String) As DataTable Implements IGetCouponTextRepository.GetCouponTextIncludeDeletedFalse

        Return GetCouponText(CouponID, SequenceNumber, False)
    End Function

#Region "Private Procedures & Functions"

    Friend Function GetCouponText(ByVal CouponID As String, ByVal Deleted As Boolean) As System.Data.DataTable
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                AddStoredProcedure(com)
                AddParameters(com, CouponID, Deleted)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Friend Function GetCouponText(ByVal CouponID As String, ByVal SequenceNumber As String, ByVal Deleted As Boolean) As System.Data.DataTable
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                AddStoredProcedure(com)
                AddParameters(com, CouponID, SequenceNumber, Deleted)

                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Friend Sub AddStoredProcedure(ByRef X As Cts.Oasys.Data.Command)

        X.StoredProcedureName = My.Resources.Procedures.CouponTextGet
    End Sub

    Friend Sub AddParameters(ByRef X As Cts.Oasys.Data.Command, ByVal CouponID As String, ByVal Deleted As Boolean)

        X.AddParameter(My.Resources.Parameters.CouponId, CouponID)
        If Deleted = True Then
            AddDeletedParameter(X)
        End If
    End Sub

    Friend Sub AddParameters(ByRef X As Cts.Oasys.Data.Command, ByVal CouponID As String, ByVal SequenceNumber As String, ByVal Deleted As Boolean)

        X.AddParameter(My.Resources.Parameters.CouponId, CouponID)
        X.AddParameter(My.Resources.Parameters.SequenceNumber, SequenceNumber)
        If Deleted = True Then
            AddDeletedParameter(X)
        End If
    End Sub

    Friend Sub AddDeletedParameter(ByRef X As Cts.Oasys.Data.Command)

        X.AddParameter(My.Resources.Parameters.IncludeDeleted, 1)
    End Sub
#End Region
End Class