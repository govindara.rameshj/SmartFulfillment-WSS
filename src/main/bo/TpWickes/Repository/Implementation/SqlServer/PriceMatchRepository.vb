﻿Public Class PriceMatchRepository
    Implements IPriceMatchRepository

    Public Function GetDiscountPercentage(ByVal requirementId As Integer) As Double Implements [Interface].IPriceMatchRepository.GetDiscountPercentage
        Return CDbl(GetDiscount(requirementId))
    End Function

    Public Overridable Function GetCompetitorName() As System.Data.DataTable Implements [Interface].IPriceMatchRepository.GetCompetitorName
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                GetName(com, My.Resources.Procedures.CompetitorNameGet)
                Return com.ExecuteDataTable
            End Using
        End Using
    End Function

    Public Overridable Function SaveLocalCompetitor(ByVal competitorName As String) As Boolean Implements [Interface].IPriceMatchRepository.SaveLocalCompetitor
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                Dim Success As SqlClient.SqlParameter = Nothing
                LocalCompetitorSet(com, competitorName, Success, My.Resources.Procedures.CompetitorNameSet)
                com.ExecuteNonQuery()
                Return CType(Success.Value, Boolean)
            End Using
        End Using
    End Function

    Public Overridable Function CheckCompetitorGroup(ByVal competitorName As String) As Boolean Implements [Interface].IPriceMatchRepository.CheckCompetitorGroup
        Dim OfflineModeRep As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

        Using con As IConnection = ConnectionFactory.FactoryGet(OfflineModeRep.RequirementIsEnabled AndAlso OfflineModeRep.IsInOfflineMode)
            Using com As New Command(con)
                Dim Success As SqlClient.SqlParameter = Nothing
                CheckIfTpGroup(com, competitorName, Success, My.Resources.Procedures.CheckCompetitorGroup)
                com.ExecuteNonQuery()
                Return CType(Success.Value, Boolean)
            End Using
        End Using
    End Function

    Friend Sub GetDiscount(ByRef command As Cts.Oasys.Data.Command, _
                                                 ByVal StoredProcedureName As String)
        AddStoredProcedure(command, StoredProcedureName)
    End Sub

    Friend Sub AddStoredProcedure(ByRef command As Cts.Oasys.Data.Command, ByVal StoredProcedureName As String)

        command.StoredProcedureName = StoredProcedureName
    End Sub

    Friend Sub GetName(ByRef command As Cts.Oasys.Data.Command, _
                                                ByVal StoredProcedureName As String)
        AddStoredProcedure(command, StoredProcedureName)
    End Sub

    Friend Overridable Sub LocalCompetitorSet(ByRef command As Cts.Oasys.Data.Command, _
                                       ByRef competitorName As String, _
                                       ByRef Success As SqlClient.SqlParameter, _
                                       ByVal StoredProcedureName As String)


        AddStoredProcedure(command, StoredProcedureName)
        command.AddParameter(My.Resources.Parameters.CompetitorName, competitorName, SqlDbType.VarChar)
        Success = command.AddOutputParameter(My.Resources.Parameters.Success, SqlDbType.Bit)
    End Sub

    Friend Overridable Sub CheckIfTpGroup(ByRef command As Cts.Oasys.Data.Command, _
                                      ByRef competitorName As String, _
                                      ByRef Success As SqlClient.SqlParameter, _
                                      ByVal StoredProcedureName As String)


        AddStoredProcedure(command, StoredProcedureName)
        command.AddParameter(My.Resources.Parameters.CompetitorName, competitorName, SqlDbType.VarChar)
        Success = command.AddOutputParameter(My.Resources.Parameters.Success, SqlDbType.Bit)
    End Sub


    Friend Overridable Function GetDiscount(ByVal RequirementID As Integer) As Integer

        Return Cts.Oasys.Core.System.Parameter.GetInteger(RequirementID)

    End Function


End Class
