﻿Imports TpWickes.Library

Public Class PriceMatchRepositoryFactory
    Inherits BaseFactory(Of IPriceMatchRepository)

    Public Overrides Function Implementation() As [Interface].IPriceMatchRepository
        Return New PriceMatchRepository
    End Function
End Class
