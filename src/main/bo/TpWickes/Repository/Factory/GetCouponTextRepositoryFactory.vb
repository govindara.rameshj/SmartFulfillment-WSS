﻿Public Class GetCouponTextRepositoryFactory

    Private Shared m_FactoryMember As IGetCouponTextRepository = Nothing

    Public Shared Function FactoryGet() As IGetCouponTextRepository

        If m_FactoryMember Is Nothing Then

            Return New GetCouponTextRepository

        Else

            Return m_FactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IGetCouponTextRepository)

        m_FactoryMember = obj

    End Sub

End Class