﻿Public Class GetTextCommunicationRepositoryFactory

    Private Shared m_FactoryMember As IGetTextCommunicationRepository = Nothing

    Public Shared Function FactoryGet() As IGetTextCommunicationRepository

        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New GetTextCommunicationRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IGetTextCommunicationRepository)

        m_FactoryMember = obj
    End Sub

End Class
