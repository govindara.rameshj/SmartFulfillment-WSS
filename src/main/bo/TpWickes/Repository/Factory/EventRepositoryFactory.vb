﻿Public Class EventRepositoryFactory

    Private Shared m_FactoryMember As IEventRepository

    Public Shared Function FactoryGet() As IEventRepository

        If m_FactoryMember Is Nothing Then

            Return New EventRepository
        Else

            Return m_FactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventRepository)

        m_FactoryMember = obj

    End Sub

End Class