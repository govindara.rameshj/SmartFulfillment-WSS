﻿Public Class GetSaleCouponRepositoryFactory

    Private Shared m_FactoryMember As IGetSaleCouponRepository = Nothing

    Public Shared Function FactoryGet() As IGetSaleCouponRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New GetSaleCouponRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IGetSaleCouponRepository)
        m_FactoryMember = obj
    End Sub

End Class