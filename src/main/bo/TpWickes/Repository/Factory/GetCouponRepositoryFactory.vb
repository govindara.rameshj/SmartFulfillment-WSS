﻿Public Class GetCouponRepositoryFactory

    Private Shared m_FactoryMember As IGetCouponRepository = Nothing

    Public Shared Function FactoryGet() As IGetCouponRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New GetCouponRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IGetCouponRepository)
        m_FactoryMember = obj
    End Sub

End Class