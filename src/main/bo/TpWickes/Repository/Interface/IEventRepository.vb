﻿Public Interface IEventRepository

    'single sku override - get
    Function GetExistingTemporaryPriceChangeForSkuHeader(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal Deleted As Boolean) As DataTable
    Function GetExistingTemporaryPriceChangeForSkuDetail(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal Deleted As Boolean) As DataTable

    Function GetOverrideTemporaryPriceChangeForSkuHeader(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date) As DataTable
    Function GetOverrideTemporaryPriceChangeForSkuDetail(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date) As DataTable

    'multiple sku override - get
    Function GetExistingDealGroup(ByVal SkuList As String) As DataTable
    Function GetOverrideDealGroup(ByVal SelectedDate As Date, ByVal SkuList As String) As DataTable

    'Function GetExistingEventMaster(ByVal SkuList As String) As DataTable
    'Function GetOverrideEventMaster(ByVal SelectedDate As Date, ByVal SkuList As String) As DataTable

    Function GetEventMaster(ByVal SelectedDate As Date, ByVal SkuList As String) As DataTable
    Function GetValidEvent(ByVal EventNumber As String, ByVal Priority As String) As DataTable

    'single/multiple sku override - set
    Function SetOverrideTemporaryPriceChange(ByVal EventType As EventTypeEnumerator, _
                                             ByVal Description As String, ByVal StartDate As Date, _
                                             ByVal EndDate As Date, ByVal SkuList As String, ByVal QuantityList As String, _
                                             ByVal Price As Decimal, ByVal CashierSecurityID As Integer, _
                                             ByVal AuthorisorSecurityID As Integer, ByRef EventHeaderOerrideID As Integer) As Boolean

End Interface
