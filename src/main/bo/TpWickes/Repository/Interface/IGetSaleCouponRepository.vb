﻿Public Interface IGetSaleCouponRepository

    Function GetSaleCoupon(ByVal TransactionDate As Date, ByVal TransactionTillID As String, ByVal TransactionNumber As String) As DataTable
    Function GetSaleCoupon(ByVal TransactionDate As Date, ByVal TransactionTillID As String, ByVal TransactionNumber As String, ByVal SequenceNumber As String) As DataTable

End Interface