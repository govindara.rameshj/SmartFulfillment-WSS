﻿Public Interface IGetCouponTextRepository

    Function GetCouponTextIncludeDeletedTrue(ByVal CouponId As String) As DataTable
    Function GetCouponTextIncludeDeletedFalse(ByVal CouponId As String) As DataTable

    Function GetCouponTextIncludeDeletedTrue(ByVal CouponId As String, ByVal SequenceNumber As String) As DataTable
    Function GetCouponTextIncludeDeletedFalse(ByVal CouponId As String, ByVal SequenceNumber As String) As DataTable

End Interface
