﻿Public Interface IGetCouponRepository

    Function GetCouponDeletedTrue(ByVal CouponId As String) As DataRow
    Function GetCouponDeletedFalse(ByVal CouponId As String) As DataRow

End Interface