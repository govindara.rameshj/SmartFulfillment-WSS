﻿Public Interface IPriceMatchRepository

    Function GetDiscountPercentage(ByVal requirementId As Integer) As Double
    Function GetCompetitorName() As DataTable
    Function SaveLocalCompetitor(ByVal competitorName As String) As Boolean
    Function CheckCompetitorGroup(ByVal competitorName As String) As Boolean

End Interface
