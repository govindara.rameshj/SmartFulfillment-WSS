﻿Public Interface IGetTextCommunicationRepository

    Function GetTextCommunications(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer) As DataTable
    Function GetTextCommunications(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, ByVal SequenceNumber As Integer) As DataTable

End Interface
