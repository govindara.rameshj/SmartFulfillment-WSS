﻿<ClassInterface(ClassInterfaceType.AutoDual), _
 ComVisible(True), _
 Guid("89491230-0FA7-48AE-803D-FA2CF1702CF2"), _
 ProgId("TPWickes.Interop.Logger.LogManager"), _
 ComSourceInterfaces(GetType(LogManager.ILogManager))> _
Public Class LogManager
    '    Implements IDisposable

    <InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIDispatch), _
    ComVisible(True)> _
    Public Interface ILogManager
        Sub WriteLog(ByVal message As String)
    End Interface

    Public Sub WriteLog(ByVal message As String)
        Cts.Oasys.Core.Logger.LogManager.WriteLog(message)
    End Sub

    '    Sub Dispose() Implements IDisposable.Dispose
    '        Cts.Oasys.Core.Logger.LogManager.DisposeInstance()
    '    End Sub

End Class
