﻿Imports WSS.BO.RemoteServices.Common.Client
Imports WSS.BO.DataLayer.Model.Entities.NonPersistent
Imports slf4net
Imports slf4net.Factories
Imports NLog.Config
Imports NLog.Targets
Imports NLog
Imports slf4net.NLog
Imports WSS.BO.RemoteServices.ProxyService.Client

<ClassInterface(ClassInterfaceType.AutoDual), _
 ComVisible(True), _
 Guid("6F38E9D2-C66B-4fc8-868C-93D01C5B7A1B"), _
 ProgId("TPWickes.Interop.PostCodeResolver.PostCodeResolver"), _
 ComSourceInterfaces(GetType(AddressLookupService.IAddressLookupService))> _
Public Class AddressLookupService

    <InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIDispatch), _
    ComVisible(True)> _
    Public Interface IAddressLookupService
        Function ResolvePostCode(ByVal postcode As String) As AddressLookupResponse
        Function RefinePickListItem(ByVal pickupItemId As String) As AddressLookupResponse
    End Interface

    Private proxyApiFacade As ProxyServiceClient

    Private logger As NLog.Logger

    Public Sub Initialize(apiAddress As String)
        logger = GetLogger()
        proxyApiFacade = New ProxyServiceClient(apiAddress)
    End Sub

    Public Function ResolvePostCode(ByVal postcode As String) As AddressLookupResponse
        Try
            Return (ConvertPostCodeResolvingResponse(proxyApiFacade.ResolvePostCode(Of WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses.FourLineAddress)(postcode)))
        Catch ex As BaseServiceClientException
            logger.Error(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function RefinePickListItem(ByVal pickupItemId As String) As AddressLookupResponse
        Try
            Return ConvertPostCodeResolvingResponse(proxyApiFacade.RefinePickListItem(Of WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses.FourLineAddress)(pickupItemId))
        Catch ex As BaseServiceClientException
            Return Nothing
        End Try
    End Function

    Private Function ConvertPostCodeResolvingResponse(receivedResponse As WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupResponse(Of WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses.FourLineAddress)) As AddressLookupResponse

        Dim convertedPostCodeResolvingResponse = New AddressLookupResponse

        convertedPostCodeResolvingResponse.Status = ConvertStatus(receivedResponse.Status)
        convertedPostCodeResolvingResponse.Address = ConvertAddress(receivedResponse.Address)
        convertedPostCodeResolvingResponse.PickupItems = ConvertPickupItems(receivedResponse.PickupItems)

        Return convertedPostCodeResolvingResponse
    End Function

    Private Function ConvertStatus(status As WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupStatus) As AddressLookupStatus

        Select Case status
            Case WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupStatus.ExactMatchFound
                Return AddressLookupStatus.ExactMatchFound
            Case WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupStatus.NotFound
                Return AddressLookupStatus.NotFound
            Case WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupStatus.SeveralMatchesFound
                Return AddressLookupStatus.SeveralMatchesFound
            Case WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.AddressLookupStatus.TooManyMatches
                Return AddressLookupStatus.TooManyMatches
            Case Else
                Return Nothing
        End Select
    End Function

    Private Function ConvertAddress(address As WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses.FourLineAddress) As FourLineAddress

        Dim convertedAddress = New FourLineAddress

        If address IsNot Nothing Then
            convertedAddress.AddressLine1 = address.AddressLine1
            convertedAddress.AddressLine2 = address.AddressLine2
            convertedAddress.Town = address.Town
            convertedAddress.PostCode = address.PostCode
        End If

        Return convertedAddress
    End Function

    Private Function ConvertPickupItems(pickupItems As List(Of WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.PickupItem)) As Collection

        Dim convertedPickupItems = New Collection

        If (pickupItems IsNot Nothing) Then
            Dim pickupItem As WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.PickupItem
            For Each pickupItem In pickupItems
                convertedPickupItems.Add(New PickupItem() With {.Id = pickupItem.Id, .Description = pickupItem.Description})
            Next
        End If

        Return convertedPickupItems
    End Function

    Private Function GetLogger() As NLog.Logger

        Dim config = New LoggingConfiguration

        Dim outputDebugStringTarget = New OutputDebugStringTarget
        config.AddTarget("odst", outputDebugStringTarget)

        Dim loggerRule = New LoggingRule("*", LogLevel.Debug, outputDebugStringTarget)
        config.LoggingRules.Add(loggerRule)

        NLog.LogManager.Configuration = config

        InitLogging()

        Return NLog.LogManager.GetCurrentClassLogger()
    End Function

    Private Sub InitLogging()
        Dim factory = New slf4net.NLog.NLogLoggerFactory
        Dim resolver = New SimpleFactoryResolver(factory)
        LoggerFactory.SetFactoryResolver(resolver)
    End Sub

End Class

Public Class SimpleFactoryResolver
    Implements IFactoryResolver

    Private _factory As ILoggerFactory

    Public Sub New(factory As ILoggerFactory)
        _factory = factory
    End Sub

    Public Function GetFactory() As slf4net.ILoggerFactory Implements slf4net.IFactoryResolver.GetFactory
        Return _factory
    End Function
End Class