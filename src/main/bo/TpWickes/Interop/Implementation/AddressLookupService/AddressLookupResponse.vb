﻿<ProgId("TPWickes.Interop.AddressLookupService.AddressLookupResponse"), _
 ComVisible(True), _
 Guid("EC4C6577-56C0-4689-A8E4-F0D4F96159B9")>
Public Class AddressLookupResponse
    Public Property Status As AddressLookupStatus
    Public Property Address As FourLineAddress
    Public Property PickupItems As Collection
End Class
