﻿<ClassInterface(ClassInterfaceType.AutoDual), _
 ComVisible(True), _
 Guid("40F80F9C-07A7-45d2-9CA7-6098606BB915"), _
 ProgId("TPWickes.Interop.Coupons.Implementation.RetrieveCoupon.1"), _
 ComSourceInterfaces(GetType(RetrieveCoupon.IRetrieveCouponsEventSink))> _
Public Class RetrieveCoupon
    'Friend oCouponMaster As New Coupon
    Friend oCouponMaster As ICoupon
    Friend CouponID As String 'taken from positions 1-7
    Friend MarketingReference As String 'taken from positions 8-11
    Friend SerialNumber As String 'taken from positions 12-17
    Friend RowNo As Long 'for checking if other exclusive coupon exists
    Friend OtherCouponFound As Boolean
    Friend ExclusiveCouponFound As Boolean
    Friend CouponResp As String
    Friend Address As Object
    Friend MgrID As String
    Friend CouponNumber As String
    Dim couponStatusChanged As Boolean = False
    Private partCodeColumn As Integer
    Private descriptionColumn As Integer
    Private lineTypeColumn As Integer
    Private exclusiveCouponColumn As Integer
    Private couponIdColumn As Integer
    Private couponReusableColumn As Integer
    Private couponSerialNumberColumn As Integer
    Private couponSupervisorColumn As Integer
    Private couponMarketingReferenceColumn As Integer
    Private lineTypeCouponColumn As Integer
    Private couponStatus As Integer
    Private buyCouponIndicator As Boolean
    Private Const messageOk As String = "1"
    Private Const messageCancel As String = "2"

#Region "Event Declarations"

    Public Delegate Sub InvalidCouponEventHandler(ByRef response As String)
    Public Delegate Sub DeletedCouponEventHandler(ByRef response As String)
    Public Delegate Sub ExclusiveCouponEventHandler(ByRef response As String)
    Public Delegate Sub ExclusiveCouponOtherCouponsEventHandler(ByRef response As String)
    Public Delegate Sub ExclusiveCouponAlreadyExistsInTransactionEventHandler(ByRef response As String)
    Public Delegate Sub CustomerDetailsEventHandler()
    Public Delegate Sub CheckOtherExclusiveCouponInSpreadSheetEventHandler(ByRef otherCouponFound As Boolean, ByRef exclusiveCouponFound As Boolean)
    Public Delegate Sub DeleteExclusiveCouponsEventHandler()
    Public Delegate Sub UnableToValidateCouponUsingSoapEventHandler(ByRef response As String)
    Public Delegate Sub ManagersAuthorisationEventHandler(ByRef response As String)
    Public Delegate Sub VerifyCouponUsageEventHandler(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef MgrID As String, ByRef response As Boolean)
    Public Delegate Sub AddtoTransactionEventHandler(ByVal GiveCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal couponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As String, ByVal managerId As String, ByVal marketReference As String)
    Public Event InvalidCouponEvent As InvalidCouponEventHandler
    Public Event DeletedCouponEvent As DeletedCouponEventHandler
    Public Event ExclusiveCouponEvent As ExclusiveCouponEventHandler
    Public Event ExclusiveAlreadyExistsInTransactionEvent As ExclusiveCouponAlreadyExistsInTransactionEventHandler
    Public Event ExclusiveCouponOtherCouponsEvent As ExclusiveCouponOtherCouponsEventHandler
    Public Event CustomerDetailsEvent As CustomerDetailsEventHandler
    Public Event CheckOtherExclusiveCouponInSpreadSheetEvent As CheckOtherExclusiveCouponInSpreadSheetEventHandler
    Public Event DeleteExclusiveCouponsEvent As DeleteExclusiveCouponsEventHandler
    Public Event UnableToValidateCouponUsingSoapEvent As UnableToValidateCouponUsingSoapEventHandler
    Public Event ManagersAuthorisationEvent As ManagersAuthorisationEventHandler
    Public Event VerifyCouponUsageEvent As VerifyCouponUsageEventHandler
    Public Event AddtoTransactionEvent As AddtoTransactionEventHandler

    Private Sub castInstance_InvalidCouponEvent(ByRef response As String)
        RaiseEvent InvalidCouponEvent(response)
    End Sub

    Private Sub castInstance_DeletedCouponEvent(ByRef response As String)
        RaiseEvent DeletedCouponEvent(response)
    End Sub

    Private Sub castInstance_ExclusiveCouponEvent(ByRef response As String)
        RaiseEvent ExclusiveCouponEvent(response)
    End Sub

    Private Sub castInstance_ExclusiveAlreadyExistsInTransactionEvent(ByRef response As String)
        RaiseEvent ExclusiveAlreadyExistsInTransactionEvent(response)
    End Sub

    Private Sub castInstance_ExclusiveCouponOtherCouponsEvent(ByRef response As String)
        RaiseEvent ExclusiveCouponOtherCouponsEvent(response)
    End Sub

    Private Sub castInstance_CustomerDetailsEvent()
        RaiseEvent CustomerDetailsEvent()
    End Sub

    Private Sub castInstance_CheckOtherExclusiveCouponInSpreadSheetEvent(ByRef otherCouponFound As Boolean, ByRef exclusiveCouponFound As Boolean)
        RaiseEvent CheckOtherExclusiveCouponInSpreadSheetEvent(otherCouponFound, exclusiveCouponFound)
    End Sub

    Private Sub castInstance_DeleteExclusiveCouponsEvent()
        RaiseEvent DeleteExclusiveCouponsEvent()
    End Sub

    Private Sub castInstance_UnableToValidateCouponUsingSoapEvent(ByRef response As String)
        RaiseEvent UnableToValidateCouponUsingSoapEvent(response)
    End Sub

    Private Sub castInstance_ManagersAuthorisationEvent(ByRef response As String)
        RaiseEvent ManagersAuthorisationEvent(response)
    End Sub

    Private Sub castInstance_VerifyCouponUsageEvent(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef MgrID As String, ByRef response As Boolean)
        RaiseEvent VerifyCouponUsageEvent(strCouponID, strSerialNo, MgrID, response)
    End Sub

    Private Sub castInstance_AddtoTransactionEvent(ByVal GiveCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal couponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As String, ByVal managerId As String, ByVal marketReference As String)
        RaiseEvent AddtoTransactionEvent(GiveCoupon, couponDescription, exclusiveCoupon, couponId, reusableCoupon, serialNumber, managerId, marketReference)
    End Sub

    Public Sub HookCustomEvents()
        Dim castInstance As New RetrieveCoupon

        AddHandler castInstance.InvalidCouponEvent, AddressOf Me.castInstance_InvalidCouponEvent
        AddHandler castInstance.DeletedCouponEvent, AddressOf Me.castInstance_DeletedCouponEvent
        AddHandler castInstance.ExclusiveCouponEvent, AddressOf Me.castInstance_ExclusiveCouponEvent
        AddHandler castInstance.ExclusiveAlreadyExistsInTransactionEvent, AddressOf Me.castInstance_ExclusiveAlreadyExistsInTransactionEvent
        AddHandler castInstance.ExclusiveCouponOtherCouponsEvent, AddressOf Me.castInstance_ExclusiveCouponOtherCouponsEvent
        AddHandler castInstance.CustomerDetailsEvent, AddressOf Me.castInstance_CustomerDetailsEvent
        AddHandler castInstance.CheckOtherExclusiveCouponInSpreadSheetEvent, AddressOf Me.castInstance_CheckOtherExclusiveCouponInSpreadSheetEvent
        AddHandler castInstance.DeleteExclusiveCouponsEvent, AddressOf Me.castInstance_DeleteExclusiveCouponsEvent
        AddHandler castInstance.UnableToValidateCouponUsingSoapEvent, AddressOf Me.castInstance_UnableToValidateCouponUsingSoapEvent
        AddHandler castInstance.ManagersAuthorisationEvent, AddressOf Me.castInstance_ManagersAuthorisationEvent
        AddHandler castInstance.VerifyCouponUsageEvent, AddressOf Me.castInstance_VerifyCouponUsageEvent
        AddHandler castInstance.AddtoTransactionEvent, AddressOf Me.castInstance_AddtoTransactionEvent
    End Sub

    <InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIDispatch), _
    ComVisible(True)> _
    Public Interface IRetrieveCouponsEventSink

        Sub InvalidCouponEvent(ByRef response As String)
        Sub DeletedCouponEvent(ByRef response As String)
        Sub ExclusiveCouponEvent(ByRef response As String)
        Sub ExclusiveAlreadyExistsInTransactionEvent(ByRef response As String)
        Sub ExclusiveCouponOtherCouponsEvent(ByRef response As String)
        Sub CustomerDetailsEvent()
        Sub CheckOtherExclusiveCouponInSpreadSheetEvent(ByRef otherCouponFound As Boolean, ByRef exclusiveCouponFound As Boolean)
        Sub DeleteExclusiveCouponsEvent()
        Sub UnableToValidateCouponUsingSoapEvent(ByRef response As String)
        Sub ManagersAuthorisationEvent(ByRef response As String)
        Sub VerifyCouponUsageEvent(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef MgrID As String, ByRef response As Boolean)
        Sub AddtoTransactionEvent(ByVal GiveCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal couponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As String, ByVal managerId As String, ByVal marketReference As String)
    End Interface
#End Region


#Region "Public Sub"

    Public Sub RetrieveBuyCoupon(ByVal strCouponNo As String)

        SetBuyCouponIndicator(True)
        ReadCouponData(strCouponNo)
        If CheckForInvalidCoupon() Then
            Exit Sub
        End If
        FindFirstExclusiveCoupon()
        If CheckCouponExclusivity() Then
            Exit Sub
        End If
        HandleFurtherExclusiveCoupons()
        SetCouponNumberBuyCoupon(strCouponNo)
        SetMarketingReference()
        SetSerialNumber()
        If Not CheckSerialNumber() Then
            Exit Sub
        End If
        CheckCustomerInformation()
        AddtoTransaction()
    End Sub

    Public Sub RetrieveGetCoupon(ByVal strCouponNo As String)

        ReadCouponData(strCouponNo)
        If CheckForInvalidCouponDoNotRaiseEvents() Then
            Exit Sub
        End If
        'printing Coupon so set Market Ref and Serial No
        SetCouponNumber(strCouponNo)
        SetMarketingReference()
        SetSerialNumber()
        AddtoTransaction()
    End Sub
#End Region

#Region "Private Methods and Sub"

    Private Sub ReadCouponData(ByVal couponNumber As String)

        ' TO DO : Call repository
        SetCouponID(couponNumber)
        CouponMaster.Initialise(CouponID, True)
    End Sub

    Private Function CheckForInvalidCoupon() As Boolean

        CheckForInvalidCouponNumber()
        CheckForDeletedCoupon()
        If couponStatusChanged Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckForInvalidCouponDoNotRaiseEvents() As Boolean

        CheckForInvalidCouponNumberDoNotRaiseEvent()
        CheckForDeletedCouponDoNotRaiseEvent()
        If couponStatusChanged Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub SetCouponNumber(ByVal strCouponNo As String)

        CouponNumber = strCouponNo.Substring(0, 7) & "0000" & "000000"
    End Sub

    Private Sub SetCouponNumberBuyCoupon(ByVal strCouponNo As String)

        CouponNumber = strCouponNo
    End Sub

    Private Sub SetBuyCouponIndicator(ByVal buyIndicator As Boolean)

        buyCouponIndicator = buyIndicator
    End Sub

    Private Sub SetMarketingReference()

        MarketingReference = CouponNumber.Substring(7, 4)
    End Sub

    Private Sub SetSerialNumber()

        SerialNumber = CouponNumber.Substring(11, 6)
    End Sub

    Private Sub AddtoTransaction()

        RaiseEvent AddtoTransactionEvent(buyCouponIndicator, CouponMaster.Description, CouponMaster.Exclusive, CouponID, CouponMaster.Reusable, SerialNumber, MgrID, MarketingReference)
    End Sub

    Private Sub SetCouponID(ByVal couponNumber As String)

        CouponID = couponNumber.Substring(0, 7)
    End Sub

    Private Sub CheckForInvalidCouponNumber()
        Dim response As String = String.Empty

        If Not MatchingCouponsFound() Then
            RaiseEvent InvalidCouponEvent(response)
            couponStatusChanged = True
        End If
    End Sub

    Private Sub CheckForDeletedCoupon()
        Dim response As String = String.Empty

        If CouponIsMarkedAsDeleted() Then
            RaiseEvent DeletedCouponEvent(response)
            couponStatusChanged = True
        End If
    End Sub

    Private Sub CheckForInvalidCouponNumberDoNotRaiseEvent()
        Dim response As String = String.Empty

        If Not MatchingCouponsFound() Then
            couponStatusChanged = True
        End If
    End Sub

    Private Sub CheckForDeletedCouponDoNotRaiseEvent()
        Dim response As String = String.Empty

        If CouponIsMarkedAsDeleted() Then
            couponStatusChanged = True
        End If
    End Sub

    Private Function MatchingCouponsFound() As Boolean

        Return CouponMaster.ExistsInDatabase
    End Function

    Private Function CouponIsMarkedAsDeleted() As Boolean

        Return CouponMaster.Deleted
    End Function

    Private Sub FindFirstExclusiveCoupon()

        OtherCouponFound = False
        ExclusiveCouponFound = False
        RaiseEvent CheckOtherExclusiveCouponInSpreadSheetEvent(OtherCouponFound, ExclusiveCouponFound)
    End Sub

    Private Function CouponIsExclusive() As Boolean

        Return CouponMaster.Exclusive
    End Function

    Private Sub SetExclusiveCouponFound()

        ExclusiveCouponFound = True
    End Sub

    Private ReadOnly Property isExclusiveCouponFound() As Boolean
        Get
            Return ExclusiveCouponFound = True
        End Get
    End Property

    Private ReadOnly Property isOtherNonExclusiveCouponFound() As Boolean
        Get
            Return OtherCouponFound = True
        End Get
    End Property

    Private Sub EstablishHowToHandleExclusiveCoupon()

        If isOtherNonExclusiveCouponFound Then
            If OverRideOtherNonExclusiveCoupons() Then
                SetExclusiveCouponFound()
            End If
            'flag to delete all coupons
        Else
            If NotificationOfCouponExclusivityAccepted() Then SetExclusiveCouponFound() 'flag to delete all coupons
        End If
    End Sub

    Private Sub EstablishHowToHandleNonExclusiveCoupon()

        If isExclusiveCouponFound Then
            If OverRideOtherExclusiveCoupon() Then
                SetExclusiveCouponFound() 'flag to delete all coupons
            End If
        End If
    End Sub

    Private Function CheckCouponExclusivity() As Boolean

        'handle if Exclusive Coupon exists
        If CouponIsExclusive() Then
            EstablishHowToHandleExclusiveCoupon()
        Else 'non-exclusive so check if Exclusive exists and accept/reject coupon
            EstablishHowToHandleNonExclusiveCoupon()
        End If

        If Not CouponResp Is Nothing Then
            If CouponResp.Equals(messageCancel) Then 'Reject/Cancel entry
                couponStatusChanged = True
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Private Function OverRideOtherExclusiveCoupon() As Boolean

        CouponResp = String.Empty
        RaiseEvent ExclusiveAlreadyExistsInTransactionEvent(CouponResp)
        If CouponResp.Equals(messageOk) Then
            ExclusiveCouponFound = True
            Return True
        Else
            Return False
        End If
    End Function

    Private Function NotificationOfCouponExclusivityAccepted() As Boolean

        CouponResp = String.Empty
        RaiseEvent ExclusiveCouponEvent(CouponResp)
        If CouponResp.Equals(messageOk) Then
            ExclusiveCouponFound = True
            Return True
        Else
            Return False
        End If
    End Function

    Private Function OverRideOtherNonExclusiveCoupons() As Boolean

        CouponResp = String.Empty
        RaiseEvent ExclusiveCouponOtherCouponsEvent(CouponResp)
        If CouponResp.Equals(messageOk) Then
            ExclusiveCouponFound = True
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub HandleFurtherExclusiveCoupons()

        If Not CouponResp Is Nothing Then
            If CouponResp.Equals(messageOk) And ExclusiveCouponFound Then
                RaiseEvent DeleteExclusiveCouponsEvent()
            End If
        End If
    End Sub

    Private Function CheckSerialNumber() As Boolean
        Dim response As Boolean

        MgrID = String.Empty
        If (CouponMaster.SerialNumber = True) Then
            RaiseEvent VerifyCouponUsageEvent(CouponID, SerialNumber, MgrID, response)
            If Not response Then
                Return False
            End If
        End If 'Coupon is Serial Numbered, so validate at Head Office
        Return True
    End Function

    Private Sub CheckCustomerInformation()

        If (CouponMaster.RequiresCustomerInfo = True) Then
            RaiseEvent CustomerDetailsEvent()
        End If 'address required
    End Sub

    Private ReadOnly Property CouponMaster() As ICoupon
        Get
            If oCouponMaster Is Nothing Then
                oCouponMaster = CouponFactory.FactoryGet
            End If
            Return oCouponMaster
        End Get
    End Property
#End Region
End Class

'Namespace [Coupon]

'    '<ComVisible(True), _
'    ' Guid("40F80F9C-07A7-45d2-9CA7-6098606BB915"), _
'    ' ClassInterface(ClassInterfaceType.None), _
'    ' ProgId("TPWickes.Interop.Coupons.Implementation.RetrieveCoupon.1"), _
'    ' ComDefaultInterface(GetType(IRetrieveCoupon))> _
'    <ClassInterface(ClassInterfaceType.AutoDual), _
'     Guid("40F80F9C-07A7-45d2-9CA7-6098606BB915"), _
'     ProgId("TPWickes.Interop.Coupons.Implementation.RetrieveCoupon.1"), _
'     ComVisible(True), _
'     ComSourceInterfaces(GetType(IRetrieveCouponEventSink))> _
'    Public Class RetrieveCoupon
'        Implements IRetrieveCoupon

'        Private WithEvents RetrieveCouponImplementation As RetrieveCoupon

'        Public Event AddtoTransactionEvent(ByVal GiveCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal couponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As Boolean, ByVal managerId As String, ByVal marketReference As String) Implements [Interface].IRetrieveCoupon.AddtoTransactionEvent
'        Public Event CheckOtherExclusiveCouponInSpreadSheetEvent(ByRef otherCouponFound As Boolean, ByRef exclusiveCouponFound As Boolean) Implements [Interface].IRetrieveCoupon.CheckOtherExclusiveCouponInSpreadSheetEvent
'        Public Event CustomerDetailsEvent() Implements [Interface].IRetrieveCoupon.CustomerDetailsEvent
'        Public Event DeletedCouponEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.DeletedCouponEvent
'        Public Event DeleteExclusiveCouponsEvent() Implements [Interface].IRetrieveCoupon.DeleteExclusiveCouponsEvent
'        Public Event ExclusiveAlreadyExistsInTransactionEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.ExclusiveAlreadyExistsInTransactionEvent
'        Public Event ExclusiveCouponEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.ExclusiveCouponEvent
'        Public Event ExclusiveCouponOtherCouponsEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.ExclusiveCouponOtherCouponsEvent
'        Public Event InvalidCouponEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.InvalidCouponEvent
'        Public Event ManagersAuthorisationEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.ManagersAuthorisationEvent
'        Public Event UnableToValidateCouponUsingSoapEvent(ByRef response As String) Implements [Interface].IRetrieveCoupon.UnableToValidateCouponUsingSoapEvent
'        Public Event VerifyCouponUsageEvent(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef MgrID As String, ByRef response As Boolean) Implements [Interface].IRetrieveCoupon.VerifyCouponUsageEvent

'        Public Sub RetrieveBuyCoupon(ByVal CouponId As String) Implements [Interface].IRetrieveCoupon.RetrieveBuyCoupon

'            RetrieveCouponImplementation.RetrieveBuyCoupon(CouponId)
'        End Sub

'        Public Sub RetrieveGetCoupon(ByVal CouponId As String) Implements [Interface].IRetrieveCoupon.RetrieveGetCoupon

'            RetrieveCouponImplementation.RetrieveGetCoupon(CouponId)
'        End Sub

'        Public Sub New()
'            MyBase.New()

'            RetrieveCouponImplementation = RetrieveCouponFactory.FactoryGet
'        End Sub

'        Private Sub RetrieveCouponImplementation_AddtoTransactionEvent(ByVal GiveCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal couponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As Boolean, ByVal managerId As String, ByVal marketReference As String) Handles RetrieveCouponImplementation.AddtoTransactionEvent

'            RaiseEvent AddtoTransactionEvent(GiveCoupon, couponDescription, exclusiveCoupon, couponId, reusableCoupon, serialNumber, managerId, marketReference)
'        End Sub

'        Private Sub RetrieveCouponImplementation_CheckOtherExclusiveCouponInSpreadSheetEvent(ByRef otherCouponFound As Boolean, ByRef exclusiveCouponFound As Boolean) Handles RetrieveCouponImplementation.CheckOtherExclusiveCouponInSpreadSheetEvent

'            RaiseEvent CheckOtherExclusiveCouponInSpreadSheetEvent(otherCouponFound, exclusiveCouponFound)
'        End Sub

'        Private Sub RetrieveCouponImplementation_CustomerDetailsEvent() Handles RetrieveCouponImplementation.CustomerDetailsEvent

'            RaiseEvent CustomerDetailsEvent()
'        End Sub

'        Private Sub RetrieveCouponImplementation_DeletedCouponEvent(ByRef response As String) Handles RetrieveCouponImplementation.DeletedCouponEvent

'            RaiseEvent DeletedCouponEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_DeleteExclusiveCouponsEvent() Handles RetrieveCouponImplementation.DeleteExclusiveCouponsEvent

'            RaiseEvent DeleteExclusiveCouponsEvent()
'        End Sub

'        Private Sub RetrieveCouponImplementation_ExclusiveAlreadyExistsInTransactionEvent(ByRef response As String) Handles RetrieveCouponImplementation.ExclusiveAlreadyExistsInTransactionEvent

'            RaiseEvent ExclusiveAlreadyExistsInTransactionEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_ExclusiveCouponEvent(ByRef response As String) Handles RetrieveCouponImplementation.ExclusiveCouponEvent

'            RaiseEvent ExclusiveCouponEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_ExclusiveCouponOtherCouponsEvent(ByRef response As String) Handles RetrieveCouponImplementation.ExclusiveCouponOtherCouponsEvent

'            RaiseEvent ExclusiveCouponOtherCouponsEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_InvalidCouponEvent(ByRef response As String) Handles RetrieveCouponImplementation.InvalidCouponEvent

'            RaiseEvent InvalidCouponEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_ManagersAuthorisationEvent(ByRef response As String) Handles RetrieveCouponImplementation.ManagersAuthorisationEvent

'            RaiseEvent ManagersAuthorisationEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_UnableToValidateCouponUsingSoapEvent(ByRef response As String) Handles RetrieveCouponImplementation.UnableToValidateCouponUsingSoapEvent

'            RaiseEvent UnableToValidateCouponUsingSoapEvent(response)
'        End Sub

'        Private Sub RetrieveCouponImplementation_VerifyCouponUsageEvent(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef MgrID As String, ByRef response As Boolean) Handles RetrieveCouponImplementation.VerifyCouponUsageEvent

'            RaiseEvent VerifyCouponUsageEvent(strCouponID, strSerialNo, MgrID, response)
'        End Sub
'    End Class
'End Namespace