﻿Namespace PriceMatch
    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("F37F86B3-7EF8-47A9-925D-A98CB6B29024"), _
     ComVisible(True)> _
    Public Interface IPriceMatch
        Sub CalculateOverridePrice(ByVal SkuNumber As String, ByVal ProductDescription As String, ByVal StoreId As Integer, ByVal TransactionDate As Date, ByVal TillId As Integer _
                              , ByVal TransactionNumber As Integer, ByVal OriginalPrice As Double, ByVal CurrentPrice As Double, ByVal VatRate As Double)
        ReadOnly Property CompetitorName() As String
        ReadOnly Property VATInclusive() As Boolean
        ReadOnly Property CurrentPrice() As Double
        ReadOnly Property CompetitorPrice() As String
        ReadOnly Property Overrideprice() As String
        ReadOnly Property TillId() As Integer
        ReadOnly Property TransactionNumber() As Integer
        ReadOnly Property StoreId() As Integer
        ReadOnly Property TransactionDate() As Date
        ReadOnly Property OriginalPrice() As Double
        ReadOnly Property SkuNumber() As String
        ReadOnly Property VatRate() As Double
        ReadOnly Property ProductDescription() As String
        ReadOnly Property DiscountPercentage() As Double
    End Interface
End Namespace

