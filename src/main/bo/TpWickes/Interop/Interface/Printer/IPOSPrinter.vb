﻿
Namespace Printer
    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("175C18C4-9F61-40a8-9D09-BAD6661D36EA"), _
     ComVisible(True)> _
    Public Interface IPOSPrinter

        Function GetPrintableLogo() As String
        Function GetPrintablePaperCut() As String
        Function GetCurrencySymbol() As String

        Function GetLineStartBig() As String
        Function GetLineStartCompressed() As String
        Function GetLineStartHuge() As String
        Function GetLineStartNormal() As String

        Function GetLineEnd() As String
        Function GetNewLine() As String
        Function GetBlankLine(Optional ByVal NumberOfLines As Integer = 1) As String
        Function GetDivideLine() As String

        Function GetLineLengthBig() As Integer
        Function GetLineLengthCompressed() As Integer
        Function GetLineLengthNormal() As Integer
        Function GetLineLengthHuge() As Integer

        Function GetPrintBarCodeAlignmentCentre() As Integer
        Function GetPrintBarCodeTextPositionBelow() As Integer
        Function GetPrintBarCodeSymbologyCode39() As Integer
        Function GetFiscalPrinterStationReceipt() As Integer
    End Interface
End Namespace