﻿
Namespace [Coupon]

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("1AC7BB82-92BE-4ae7-83DC-9329D55CBF34"), _
     ComVisible(True)> _
    Public Interface IPrintCoupon

        Function GetCouponBody(ByVal SaleCouponId As String) As String
        Function GetCouponFooterNoEnd(ByVal SaleCouponId As String) As String
        Function GetCouponFooterLineFeedAtEnd(ByVal SaleCouponId As String, ByVal NumberOfLinesAtEnd As Integer) As String
        Function GetCouponBarCodeStation() As Integer
        Function GetCouponBarCodeSymbology() As Integer
        Function GetCouponBarCodeHeight() As Integer
        Function GetCouponBarCodeWidth() As Integer
        Function GetCouponBarCodeAlignment() As Integer
        Function GetCouponBarCodeTextPosition() As Integer
    End Interface
End Namespace
