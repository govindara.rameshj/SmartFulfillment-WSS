﻿Namespace [Coupon]

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("9D4E80AC-EB9A-464e-8D89-8E3C3B6469C9"), _
     ComVisible(True)> _
    Public Interface IHeader

        Function CouponWebServiceAvailable() As Boolean
        Function CouponRedeemed(ByVal CouponMasterID As String) As Boolean
    End Interface

End Namespace