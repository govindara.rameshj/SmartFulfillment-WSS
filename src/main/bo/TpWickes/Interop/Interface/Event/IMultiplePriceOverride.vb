﻿Namespace Events

    <InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("189043E0-92D4-46D9-B2F4-1C8B673EA0DA"), ComVisible(True)> _
    Public Interface IMultiplePriceOverride

        Function GetDealGroup(ByVal SelectedDate As Date, ByVal SkuNumbers As String) As ADODB.Recordset
        Function GetEventMaster(ByVal SelectedDate As Date, ByVal SkuNumbers As String) As ADODB.Recordset
        Function GetValidEvent(ByVal EventNumber As String, ByVal Priority As String) As ADODB.Recordset

        Function SetNewPrice(ByVal SkuNumberList As String, ByVal QuantityList As String, _
                             ByVal SelectedDate As Date, ByVal TotalPrice As Double, _
                             ByVal CashierSecurityID As Integer, ByVal AuthorisorSecurityID As Integer) As Boolean

    End Interface

End Namespace