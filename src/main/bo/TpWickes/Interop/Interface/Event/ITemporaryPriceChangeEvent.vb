﻿Namespace Events

    <InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("1B41AC05-284B-4712-BE20-8A9B0F5249AF"), ComVisible(True)> _
    Public Interface ITemporaryPriceChangeEvent

        'VB6 cannot process decimal type, will use double

        Function GetCurrentPrice(ByVal SKU As String, ByVal SelectedDate As Date, ByRef OverrideEvent As Boolean, _
                                 ByRef ExistingEventPrice As Double, ByRef ExistingEventNumber As String) As Double

        Function SetNewPrice(ByVal SKU As String, ByVal SelectedDate As Date, ByVal NewPrice As Double, _
                             ByVal SecurityCheckUserOneID As Integer, ByVal SecurityCheckUserTwoID As Integer) As Boolean

    End Interface

End Namespace