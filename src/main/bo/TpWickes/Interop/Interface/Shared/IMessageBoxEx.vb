﻿Public Enum VbMsgBoxStyle
    vbApplicationModal = 0
    vbDefaultButton1 = 0
    vbOKOnly = 0
    vbOKCancel = 1
    vbAbortRetryIgnore = 2
    vbYesNoCancel = 3
    vbYesNo = 4
    vbRetryCancel = 5
    vbCritical = 16
    vbExclamation = 48
    vbInformation = 64
    vbQuestion = 32
    vbDefaultButton2 = 256
    vbDefaultButton3 = 512
    vbDefaultButton4 = 768
    vbMsgBoxHelpButton = 16384
    vbMsgBoxRight = 524288
    vbMsgBoxRtlReading = 1048576
    vbMsgBoxSetForeground = 65536
    vbSystemModal = 4096
End Enum

Public Enum VbMsgBoxResult
    vbOK = 1
    vbCancel = 2
    vbAbort = 3
    vbRetry = 4
    vbIgnore = 5
    vbYes = 6
    vbNo = 7
End Enum

Public Interface IMessageBoxEx
    Event MessageBoxEx(ByVal strMessage As String, _
                        ByVal vbButtons As VbMsgBoxStyle, _
                        ByVal strTitle As String, _
                        ByVal strButton1Caption As String, _
                        ByVal strButton2Caption As String, _
                        ByVal strButton3Caption As String, _
                        ByVal lngFontSize As Long, _
                        ByVal lngBackColor As Long, _
                        ByVal lngHeightRequested As Long, _
                        ByVal lngWidthRequested As Long, _
                        ByRef Response As VbMsgBoxResult)
End Interface
