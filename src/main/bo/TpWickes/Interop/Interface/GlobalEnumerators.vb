﻿<ComVisible(True), _
 Guid("F6BEEAB7-09B4-4e46-96C9-6ACBFA3BF520")> _
Public Enum CouponStatus

    Active = 0
    NoEventExists = 1
    EventNotActive = 2
    CouponReturnedAsNotUsed = 3
    ReversedAtTill = 7
    UnAppliedAndReturnedToCustomer = 8
    StoreIssuedAsReward = 9
End Enum
