Namespace Sale

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("6CE388FD-31E5-4a0a-8CD3-E4D9E8FDFAA5"), _
     ComVisible(True)> _
    Public Interface ISaleCoupons

        ReadOnly Property ExistsInDatabase() As Boolean

        Function Initialise(ByVal TransactionDate As Date, ByVal TillId As String, ByVal TransactionNumber As String) As Boolean
        Function FirstCoupon() As ISaleCoupon
        Function NextCoupon(ByVal previousSequence As Integer) As ISaleCoupon

    End Interface

End Namespace