Namespace Sale

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("7E38EDEF-326A-4aee-8EE1-C72DF8907244"), _
     ComVisible(True)> _
    Public Interface ISaleCoupon

        Property StoreID() As String                       'DlCoupon -> StoreID
        Property TransactionDate() As Date                 'DlCoupon -> TranDate
        Property TransactionTillID() As String             'DlCoupon -> TranTillID
        Property TransactionNumber() As String             'DlCoupon -> TranNo
        Property SequenceNumber() As String                'DlCoupon -> Sequence
        Property CouponID() As String                      'DlCoupon -> CouponID
        Property Status() As CouponStatus                  'DlCoupon -> Status
        Property Exclusive() As Boolean                    'DlCoupon -> ExcCoupon
        Property MarketReference() As String               'DlCoupon -> MarketRef
        Property Reusable() As Boolean                     'DlCoupon -> Reusable
        Property IssuingStoreNumber() As String            'DlCoupon -> IssueStoreNo
        Property SerialNumber() As String                  'DlCoupon -> SerialNo
        Property ManagerID() As String                     'DlCoupon -> ManagerID
        Property RtiFlag() As String                       'DlCoupon -> RTIFlag

        ReadOnly Property BarData() As String              'Combine DlCoupon -> CouponID, MarketRef & SerialNo

    End Interface

End Namespace