﻿Namespace VolumeMovement

    'VB6 cannot process decimal type, will use double

    <InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("6B97FB82-1F98-41F6-B115-4D62CAB5ADE9"), ComVisible(True)> _
    Public Interface IOrderLine

        Property SellingStoreLineNo() As Integer
        Property ProductCode() As Integer
        Property ProductDescription() As String
        Property TotalOrderQuantity() As Double
        Property QuantityTaken() As Integer
        Property UOM() As String
        Property LineValue() As Double
        Property DeliveryChargeItem() As Boolean
        Property SellingPrice() As Double

    End Interface

End Namespace