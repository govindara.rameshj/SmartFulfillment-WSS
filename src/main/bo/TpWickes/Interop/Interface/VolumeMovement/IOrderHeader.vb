﻿Namespace VolumeMovement

    'VB6 cannot process decimal type, will use double

    <InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("2B750601-FDA5-498C-9B00-55165A3BEC22"), ComVisible(True)> _
    Public Interface IOrderHeader

        Property SellingStoreCode() As Integer
        Property SellingStoreOrderNumber() As Integer
        Property RequiredDeliveryDate() As Date
        Property DeliveryCharge() As Double
        Property TotalOrderValue() As Double
        Property SaleDate() As Date
        Property DeliveryPostcode() As String
        Property ToBeDelivered() As Boolean

        Property Lines() As List(Of IOrderLine)

        Sub AddByValue(ByVal SellingStoreLineNo As Integer, ByVal ProductCode As Integer, _
                       ByVal ProductDescription As String, ByVal TotalOrderQuantity As Double, _
                       ByVal QuantityTaken As Integer, ByVal UOM As String, ByVal LineValue As Double, _
                       ByVal DeliveryChargeItem As Boolean, ByVal SellingPrice As Double)

    End Interface

End Namespace