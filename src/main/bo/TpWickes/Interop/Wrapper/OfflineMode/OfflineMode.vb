﻿Namespace [OfflineMode]

    <ComVisible(True), _
     Guid("0C399495-6A09-49da-B15A-57E06FE04AFC"), _
     ClassInterface(ClassInterfaceType.None), _
     ProgId("TPWickes.Interop.OfflineMode.Wrappers.OfflineMode.1"), _
     ComDefaultInterface(GetType(IWrapperOfflineMode))> _
    Public Class OfflineMode
        Implements IWrapperOfflineMode


        Public Function FactoryGet() As [Interface].OfflineMode.IOfflineMode Implements IWrapperOfflineMode.FactoryGet

            Return OfflineModeFactory.FactoryGet
        End Function
    End Class
End Namespace