﻿Namespace Events

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
    Guid("A63E73B2-1259-447A-BEBD-C071EE38A696"), _
    ComVisible(True)> _
    Public Interface IWrapperMultiplePriceOverride
        Function FactoryGet() As IMultiplePriceOverride
    End Interface

End Namespace
