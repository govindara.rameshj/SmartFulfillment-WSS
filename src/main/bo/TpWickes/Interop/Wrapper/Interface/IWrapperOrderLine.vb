﻿Namespace VolumeMovement

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
    Guid("997E77D0-4D7A-49BB-8C63-0B63378475B8"), _
    ComVisible(True)> _
    Public Interface IWrapperOrderLine
        Function FactoryGet() As [Interface].VolumeMovement.IOrderLine
    End Interface
End Namespace