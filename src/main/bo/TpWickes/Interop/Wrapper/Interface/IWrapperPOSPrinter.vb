﻿<InterfaceType(ComInterfaceType.InterfaceIsDual), _
 Guid("EF24B4F0-9537-4e2d-B1BE-241DF4BF7A0D"), _
 ComVisible(True)> _
Public Interface IWrapperPOSPrinter

    Function FactoryGet() As Printer.IPOSPrinter
End Interface
