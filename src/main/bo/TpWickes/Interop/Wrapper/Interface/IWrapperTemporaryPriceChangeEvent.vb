﻿Namespace Events

    <InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("A8DC1F72-584C-48c6-AE8E-6584F5634A60"), ComVisible(True)> _
    Public Interface IWrapperTemporaryPriceChangeEvent

        Function FactoryGet() As ITemporaryPriceChangeEvent

    End Interface

End Namespace