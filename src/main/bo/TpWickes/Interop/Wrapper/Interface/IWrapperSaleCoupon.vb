﻿Namespace Sale

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("75D62B0A-CE12-4593-BDA0-29558896E069"), _
     ComVisible(True)> _
    Public Interface IWrapperSaleCoupon

        Function FactoryGet() As ISaleCoupon

    End Interface

End Namespace