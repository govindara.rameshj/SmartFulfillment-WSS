﻿Namespace [OfflineMode]

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("605E6C22-6378-4332-A651-B9467CE66A72"), _
     ComVisible(True)> _
    Public Interface IWrapperOfflineMode
        Function FactoryGet() As [Interface].OfflineMode.IOfflineMode
    End Interface
End Namespace