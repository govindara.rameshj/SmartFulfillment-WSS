﻿Namespace VolumeMovement

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
   Guid("C4690E3E-37BF-441E-AE41-FB5254D70BF7"), _
   ComVisible(True)> _
   Public Interface IWrapperOrderHeader
        Function FactoryGet() As [Interface].VolumeMovement.IOrderHeader
    End Interface
End Namespace