﻿Namespace Sale

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("1FE49AE8-12DE-444a-908A-4FB94E451F14"), _
     ComVisible(True)> _
    Public Interface IWrapperSaleCoupons

        Function FactoryGet() As ISaleCoupons
    End Interface
End Namespace