﻿Namespace [Coupon]

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("B07C9721-E6CE-4c3c-BBA6-CBCACF1278E3"), _
     ComVisible(True)> _
    Public Interface IWrapperPrintCoupon

        Function FactoryGet() As IPrintCoupon
    End Interface
End Namespace
