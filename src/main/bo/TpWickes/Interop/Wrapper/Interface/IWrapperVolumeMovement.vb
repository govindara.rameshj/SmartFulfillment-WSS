﻿Namespace VolumeMovement

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
    Guid("0A4C98F9-66D0-45D2-8EDA-F0CE32A23D1F"), _
    ComVisible(True)> _
    Public Interface IWrapperVolumeMovement

        Function FactoryGet() As [Interface].VolumeMovement.IVolumeMovement

    End Interface
End Namespace