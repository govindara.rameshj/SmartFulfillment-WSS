﻿Namespace [Coupon]

    <InterfaceType(ComInterfaceType.InterfaceIsDual), _
     Guid("506E549B-7D40-4b89-95E7-C61967832A84"), _
     ComVisible(True)> _
    Public Interface IWrapperHeader

        Function FactoryGet() As IHeader

    End Interface

End Namespace