﻿Namespace Sale

    <ComVisible(True), _
     Guid("A8780B31-6BCF-42b9-BE81-6527278EFCAF"), _
     ClassInterface(ClassInterfaceType.None), _
     ProgId("TPWickes.Interop.Coupons.Wrapper.SaleCoupon.1"), _
     ComDefaultInterface(GetType(IWrapperSaleCoupon))> _
    Public Class SaleCoupon
        Implements IWrapperSaleCoupon

        Public Function FactoryGet() As [Interface].Sale.ISaleCoupon Implements IWrapperSaleCoupon.FactoryGet

            Return SaleCouponFactory.FactoryGet()

        End Function

    End Class

End Namespace