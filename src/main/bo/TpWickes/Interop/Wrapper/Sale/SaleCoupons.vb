﻿Namespace Sale

    <ComVisible(True), _
     Guid("BCFBC6B9-286A-470f-98AA-6B80A4CBDF41"), _
     ClassInterface(ClassInterfaceType.None), _
     ProgId("TPWickes.Interop.Coupons.Wrapper.SaleCoupons.1"), _
     ComDefaultInterface(GetType(IWrapperSaleCoupons))> _
    Public Class SaleCoupons
        Implements IWrapperSaleCoupons


        Public Function FactoryGet() As [Interface].Sale.ISaleCoupons Implements IWrapperSaleCoupons.FactoryGet

            Return SaleCouponsFactory.FactoryGet
        End Function
    End Class
End Namespace