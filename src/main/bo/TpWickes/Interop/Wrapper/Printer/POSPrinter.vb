﻿
<ComVisible(True), _
 Guid("00517A10-2B4B-4578-A215-0309864B6F63"), _
 ClassInterface(ClassInterfaceType.None), _
 ProgId("TPWickes.Interop.POSPrinter.Wrapper.POSPrinter.1"), _
 ComDefaultInterface(GetType(IWrapperPOSPrinter))> _
Public Class POSPrinter
    Implements IWrapperPOSPrinter

    Public Function FactoryGet() As Printer.IPOSPrinter Implements IWrapperPOSPrinter.FactoryGet

        Return POSPrinterFactory.FactoryGet()
    End Function
End Class