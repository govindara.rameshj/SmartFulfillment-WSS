﻿Namespace PriceMatch
    <ComVisible(True), _
    Guid("49AA6155-477E-4327-B7AF-97BA86EBB6C6"), _
    ClassInterface(ClassInterfaceType.None), _
    ProgId("TPWickes.Interop.PriceMatch.Wrappers.PriceMatch.1"), _
    ComDefaultInterface(GetType(IWrapperPriceMatch))> _
   Public Class PriceMatch
        Implements IWrapperPriceMatch

        Public Function FactoryGet() As [Interface].PriceMatch.IPriceMatch Implements IWrapperPriceMatch.FactoryGet
            Return (New PriceMatchFactory).Implementation
        End Function
    End Class
End Namespace
