﻿Namespace [Coupon]

    <ComVisible(True), _
     Guid("00B7A4DD-B069-4e50-B23E-E1653581B7BA"), _
     ClassInterface(ClassInterfaceType.None), _
     ProgId("TPWickes.Interop.Coupons.Wrappers.Header.1"), _
     ComDefaultInterface(GetType(IWrapperHeader))> _
    Public Class Header
        Implements IWrapperHeader

        Public Function FactoryGet() As [Interface].Coupon.IHeader Implements IWrapperHeader.FactoryGet

            Return HeaderFactory.FactoryGet()

        End Function

    End Class

End Namespace