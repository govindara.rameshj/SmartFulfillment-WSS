﻿Namespace [Coupon]

    <ComVisible(True), _
     Guid("7443FD48-30B1-4087-A901-9BF60D88CE08"), _
     ClassInterface(ClassInterfaceType.None), _
     ProgId("TPWickes.Interop.Coupons.Wrappers.PrintCoupon.1"), _
     ComDefaultInterface(GetType(IWrapperPrintCoupon))> _
    Public Class PrintCoupon
        Implements IWrapperPrintCoupon

        Public Function FactoryGet() As [Interface].Coupon.IPrintCoupon Implements IWrapperPrintCoupon.FactoryGet

            Return PrintCouponFactory.FactoryGet()
        End Function
    End Class
End Namespace