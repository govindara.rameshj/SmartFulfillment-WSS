﻿Namespace VolumeMovement

    <ComVisible(True), _
    Guid("F12FC0DB-017E-4929-8858-8517F8454327"), _
    ClassInterface(ClassInterfaceType.None), _
    ProgId("TPWickes.Interop.VolumeMovement.Wrapper.VolumeMovements.1"), _
    ComDefaultInterface(GetType(IWrapperVolumeMovement))> _
   Public Class VolumeMovements
        Implements IWrapperVolumeMovement

        Public Function FactoryGet() As [Interface].VolumeMovement.IVolumeMovement Implements IWrapperVolumeMovement.FactoryGet
            Return VolumeMovementFactory.FactoryGet
        End Function
    End Class
End Namespace