﻿Namespace VolumeMovement

    <ComVisible(True), _
   Guid("8FFA39B6-5C6E-4B0B-8F96-E461DE964AC3"), _
   ClassInterface(ClassInterfaceType.None), _
   ProgId("TPWickes.Interop.VolumeMovement.Wrapper.OrderLine.1"), _
   ComDefaultInterface(GetType(IWrapperOrderLine))> _
   Public Class OrderLine
        Implements IWrapperOrderLine

        Public Function FactoryGet() As [Interface].VolumeMovement.IOrderLine Implements IWrapperOrderLine.FactoryGet
            Return OrderLineFactory.FactoryGet
        End Function
    End Class
End Namespace