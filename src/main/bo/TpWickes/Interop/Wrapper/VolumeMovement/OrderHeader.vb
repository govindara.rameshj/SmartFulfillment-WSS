﻿Namespace VolumeMovement

    <ComVisible(True), _
  Guid("E440A78F-B9C7-464E-9AD1-ED63B1E1820E"), _
  ClassInterface(ClassInterfaceType.None), _
  ProgId("TPWickes.Interop.VolumeMovement.Wrapper.OrderHeader.1"), _
  ComDefaultInterface(GetType(IWrapperOrderHeader))> _
   Public Class OrderHeader
        Implements IWrapperOrderHeader

        Public Function FactoryGet() As [Interface].VolumeMovement.IOrderHeader Implements IWrapperOrderHeader.FactoryGet
            Return OrderHeaderFactory.FactoryGet
        End Function
    End Class
End Namespace