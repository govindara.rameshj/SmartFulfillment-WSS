﻿Namespace Events

    <ComVisible(True), Guid("D5AC2FD1-6E2A-403e-AAA9-B164F3C03AC8"), _
                       ClassInterface(ClassInterfaceType.None), _
                       ProgId("TPWickes.Interop.Events.Wrappers.Events.1"), _
                       ComDefaultInterface(GetType(IWrapperTemporaryPriceChangeEvent))> _
    Public Class TemporaryPriceChangeEvent
        Implements IWrapperTemporaryPriceChangeEvent

        Public Function FactoryGet() As ITemporaryPriceChangeEvent Implements IWrapperTemporaryPriceChangeEvent.FactoryGet

            Return TemporaryPriceChangeEventFactory.FactoryGet()

        End Function

    End Class

End Namespace