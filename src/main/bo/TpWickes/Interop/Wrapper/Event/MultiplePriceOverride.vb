﻿Namespace Events

    <ComVisible(True), Guid("B41B32EF-DE3F-4E78-8A5D-FC3421666A8C"), _
                      ClassInterface(ClassInterfaceType.None), _
                      ProgId("TPWickes.Interop.Events.Wrappers.MultiplePriceOverride.1"), _
                      ComDefaultInterface(GetType(IWrapperMultiplePriceOverride))> _
   Public Class MultiplePriceOverride
        Implements IWrapperMultiplePriceOverride

        Public Function FactoryGet() As [Interface].Events.IMultiplePriceOverride Implements IWrapperMultiplePriceOverride.FactoryGet
            Return MultiplePriceOverrideFactory.FactoryGet()
        End Function
    End Class
End Namespace
