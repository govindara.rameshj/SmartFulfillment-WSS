﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventOverrideMaster
    Inherits Base
    Implements IEventOverrideMaster

#Region "Table Fields"

    Friend _ID As Integer
    Friend _EventHeaderOverrideID As Integer
    Friend _SkuNumber As String
    Friend _Price As Decimal

    <ColumnMapping("ID")> _
    Public Property ID() As Integer Implements IEventOverrideMaster.ID
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    <ColumnMapping("EventHeaderOverrideID")> _
    Public Property HeaderID() As Integer Implements IEventOverrideMaster.HeaderID
        Get
            Return _EventHeaderOverrideID
        End Get
        Set(ByVal value As Integer)
            _EventHeaderOverrideID = value
        End Set
    End Property

    <ColumnMapping("SkuNumber")> _
    Public Property Sku() As String Implements IEventOverrideMaster.Sku
        Get
            Return _SkuNumber
        End Get
        Set(ByVal value As String)
            _SkuNumber = value
        End Set
    End Property

    <ColumnMapping("Price")> _
    Public Property Price() As Decimal Implements IEventOverrideMaster.Price
        Get
            Return _Price
        End Get
        Set(ByVal value As Decimal)
            _Price = value
        End Set
    End Property

#End Region

End Class