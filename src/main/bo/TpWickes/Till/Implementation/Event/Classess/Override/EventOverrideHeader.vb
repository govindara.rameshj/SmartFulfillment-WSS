﻿Public Class EventOverrideHeader
    Inherits Base
    Implements IEventOverrideHeader

#Region "Table Fields"

    Friend _ID As Integer
    Friend _EventTypeID As Integer
    Friend _Description As String
    Friend _StartDate As Date
    Friend _EndDate As Date
    Friend _Revision As Integer
    Friend _CashierSecurityID As Integer
    Friend _AuthorisorSecurityID As Integer

    <ColumnMapping("ID")> _
    Public Property ID() As Integer Implements IEventOverrideHeader.ID
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    <ColumnMapping("EventTypeID")> _
    Public Property EventTypeID() As Integer Implements IEventOverrideHeader.EventTypeID
        Get
            Return _EventTypeID
        End Get
        Set(ByVal value As Integer)
            _EventTypeID = value
        End Set
    End Property

    <ColumnMapping("Description")> _
    Public Property Description() As String Implements IEventOverrideHeader.Description
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    <ColumnMapping("StartDate")> _
    Public Property StartDate() As Date Implements IEventOverrideHeader.StartDate
        Get
            Return _StartDate
        End Get
        Set(ByVal value As Date)
            _StartDate = value
        End Set
    End Property

    <ColumnMapping("EndDate")> _
    Public Property EndDate() As Date Implements IEventOverrideHeader.EndDate
        Get
            Return _EndDate
        End Get
        Set(ByVal value As Date)
            _EndDate = value
        End Set
    End Property

    <ColumnMapping("Revision")> _
    Public Property RevisionNumber() As Integer Implements IEventOverrideHeader.RevisionNumber
        Get
            Return _Revision
        End Get
        Set(ByVal value As Integer)
            _Revision = value
        End Set
    End Property

    <ColumnMapping("CashierSecurityID")> _
    Public Property CashierSecurityID() As Integer Implements IEventOverrideHeader.CashierSecurityID
        Get
            Return _CashierSecurityID
        End Get
        Set(ByVal value As Integer)
            _CashierSecurityID = value
        End Set
    End Property

    <ColumnMapping("AuthorisorSecurityID")> _
    Public Property AuthorisorSecurityID() As Integer Implements IEventOverrideHeader.AuthorisorSecurityID
        Get
            Return _AuthorisorSecurityID
        End Get
        Set(ByVal value As Integer)
            _AuthorisorSecurityID = value
        End Set
    End Property

#Region "Properties"

    Friend _Lines As IEventOverrideMasters

    Public Property MasterCollection() As IEventOverrideMasters Implements IEventOverrideHeader.MasterCollection
        Get
            Return _Lines
        End Get
        Set(ByVal value As IEventOverrideMasters)
            _Lines = value
        End Set
    End Property

#End Region

#End Region

End Class