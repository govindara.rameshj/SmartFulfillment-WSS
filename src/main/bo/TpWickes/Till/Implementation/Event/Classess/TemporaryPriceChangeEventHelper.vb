﻿Friend Class TemporaryPriceChangeEventHelper
    Implements ITemporaryPriceChangeEventHelper

#Region "Properties"

    Friend _SKU As String
    Friend _SelectedDate As Date

    Friend _ExistsInDB As Boolean
    Friend _EventExistingHeaderCollection As IEventExistingHeaders
    Friend _EventOverrideHeaderCollection As IEventOverrideHeaders

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements ITemporaryPriceChangeEventHelper.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

    Public ReadOnly Property ExistingEventCollection() As IEventExistingHeaders Implements ITemporaryPriceChangeEventHelper.ExistingEventCollection
        Get
            Return _EventExistingHeaderCollection
        End Get
    End Property

    Public ReadOnly Property OverrideEventCollection() As IEventOverrideHeaders Implements ITemporaryPriceChangeEventHelper.OverrideEventCollection
        Get
            Return _EventOverrideHeaderCollection
        End Get
    End Property

#End Region

#Region "Methods"

    Public Function GetEventExistingPrice() As Decimal? Implements ITemporaryPriceChangeEventHelper.GetEventExistingPrice

        If _EventExistingHeaderCollection Is Nothing Then Return Nothing

        'multiple records                                       : if detail "day of week" flag active
        '                                                         then check that correct header "active day" flag is active
        'multiple records                                       : select highest priority number
        'multiple recordsafter highest priority number selected : select highet event no


        Return _EventExistingHeaderCollection.ExistingPrice(_SelectedDate)

    End Function

    Public Function GetEventExistingEventNumber() As String Implements ITemporaryPriceChangeEventHelper.GetEventExistingEventNumber

        If _EventExistingHeaderCollection Is Nothing Then Return Nothing

        'multiple records                                       : if detail "day of week" flag active
        '                                                         then check that correct header "active day" flag is active
        'multiple records                                       : select highest priority number
        'multiple recordsafter highest priority number selected : select highet event no


        Return _EventExistingHeaderCollection.ExistingEventNumber(_SelectedDate)

    End Function

    Public Function GetEventOverridePrice() As Decimal? Implements ITemporaryPriceChangeEventHelper.GetEventOverridePrice

        If _EventOverrideHeaderCollection Is Nothing Then Return Nothing

        'multiple records : return price from entry with highest sequence no
        '                   assuming each header will have only one detail, if not last one will win

        Return _EventOverrideHeaderCollection.OverridePrice

    End Function

    Public Function Initialise(ByVal SKU As String, ByVal SelectedDate As Date?) As Boolean Implements ITemporaryPriceChangeEventHelper.Initialise

        If SKU Is Nothing OrElse SKU.Trim.Length = 0 Then Return False
        If SelectedDate.HasValue = False Then Return False

        Load(SKU, SelectedDate.Value, SelectedDate.Value, False)

        _SKU = SKU
        _SelectedDate = SelectedDate.Value

        Return True

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Overloads Sub Load(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal Deleted As Boolean)

        Dim ExistingEventsExist As Boolean
        Dim OverrideEventsExist As Boolean

        ExistingEventsExist = ExistingEventsLoadData(SKU, StartDate, EndDate, Deleted)
        OverrideEventsExist = OverrideEventsLoadData(SKU, StartDate, EndDate)

        If ExistingEventsExist = True Or OverrideEventsExist = True Then _ExistsInDB = True

    End Sub

    Friend Function ExistingEventsLoadData(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal Deleted As Boolean) As Boolean

        Dim Repository As IEventRepository
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        Repository = EventRepositoryFactory.FactoryGet

        HeaderDT = Repository.GetExistingTemporaryPriceChangeForSkuHeader(SKU, StartDate, EndDate, Deleted)
        DetailDT = Repository.GetExistingTemporaryPriceChangeForSkuDetail(SKU, StartDate, EndDate, Deleted)

        If HeaderDT Is Nothing OrElse HeaderDT.Rows.Count = 0 Then Return False
        If DetailDT Is Nothing OrElse DetailDT.Rows.Count = 0 Then Return False

        _EventExistingHeaderCollection = EventExistingHeadersFactory.FactoryGet
        _EventExistingHeaderCollection.Initialise(HeaderDT, DetailDT)

        Return True

    End Function

    Friend Function OverrideEventsLoadData(ByVal SKU As String, ByVal StartDate As Date, ByVal EndDate As Date) As Boolean

        Dim Repository As IEventRepository
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        Repository = EventRepositoryFactory.FactoryGet

        HeaderDT = Repository.GetOverrideTemporaryPriceChangeForSkuHeader(SKU, StartDate, EndDate)
        DetailDT = Repository.GetOverrideTemporaryPriceChangeForSkuDetail(SKU, StartDate, EndDate)

        If HeaderDT Is Nothing OrElse HeaderDT.Rows.Count = 0 Then Return False
        If DetailDT Is Nothing OrElse DetailDT.Rows.Count = 0 Then Return False

        _EventOverrideHeaderCollection = EventOverrideHeadersFactory.FactoryGet
        _EventOverrideHeaderCollection.Initialise(HeaderDT, DetailDT)

        Return True

    End Function

#End Region

End Class