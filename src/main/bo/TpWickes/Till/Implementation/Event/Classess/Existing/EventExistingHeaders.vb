﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventExistingHeaders
    Inherits BaseCollection(Of EventExistingHeader)
    Implements IEventExistingHeaders

#Region "Properties"

    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements IEventExistingHeaders.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

    Public ReadOnly Property CollectionCount() As Integer Implements IEventExistingHeaders.CollectionCount
        Get
            Return Me.Count
        End Get
    End Property

    Public ReadOnly Property ItemObject(ByVal Index As Integer) As IEventExistingHeader Implements IEventExistingHeaders.ItemObject
        Get
            Return Me.Item(Index)
        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub Initialise(ByVal HeaderDT As DataTable, ByVal DetailDT As DataTable) Implements IEventExistingHeaders.Initialise

        Me.Load(HeaderDT)

        _ExistsInDB = True

        PopulateDetail(DetailDT)

    End Sub

    Public Function ExistingPrice(ByVal Today As Date) As Decimal? Implements IEventExistingHeaders.ExistingPrice

        Dim Detail As IEventExistingMaster
        Dim EventID As Integer
        Dim Priority As Integer
        Dim Price As System.Nullable(Of Decimal)

        Priority = 0

        For Each Header As IEventExistingHeader In Me.Items

            'multiple records : select highest priority number; <= was used instead of < so that multiple "event no" secenario could be considered
            If Priority <= CType(Header.Priority, Integer) Then

                Priority = CType(Header.Priority, Integer)

                EventID = 0

                If EventID < CType(Header.EventID, Integer) Then

                    EventID = CType(Header.EventID, Integer)

                    For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1
                        Detail = Header.MasterCollection.ItemObject(DetailIndex)
                        'multiple records : if detail "day of week" flag active then check that correct header "active day" flag is active
                        If Detail.DayOfWeekEvent = True And ActiveDay(Today.DayOfWeek, Header) = False Then Continue For
                        Price = Detail.Price
                    Next

                End If

            End If

        Next

        Return Price

    End Function

    Public Function ExistingEventNumber(ByVal Today As Date) As String Implements IEventExistingHeaders.ExistingEventNumber

        Dim Detail As IEventExistingMaster
        Dim EventID As Integer
        Dim Priority As Integer

        ExistingEventNumber = String.Empty
        Priority = 0

        For Each Header As IEventExistingHeader In Me.Items

            'multiple records : select highest priority number; <= was used instead of < so that multiple "event no" secenario could be considered
            If Priority <= CType(Header.Priority, Integer) Then

                Priority = CType(Header.Priority, Integer)

                EventID = 0

                If EventID < CType(Header.EventID, Integer) Then

                    EventID = CType(Header.EventID, Integer)

                    For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1
                        Detail = Header.MasterCollection.ItemObject(DetailIndex)
                        'multiple records : if detail "day of week" flag active then check that correct header "active day" flag is active
                        If Detail.DayOfWeekEvent = True And ActiveDay(Today.DayOfWeek, Header) = False Then Continue For

                        ExistingEventNumber = Header.EventID

                    Next

                End If

            End If

        Next

    End Function

#End Region

#Region "Private Procedures & Functions"

    Private Sub PopulateDetail(ByRef DetailDT As DataTable)

        Dim DetailCollection As IEventExistingMasters

        For Each X As IEventExistingHeader In Me.Items

            DetailCollection = EventExistingMastersFactory.FactoryGet

            DetailCollection.Initialise(FilteredDataTable(DetailDT, X.EventID))

            X.MasterCollection = DetailCollection

        Next

    End Sub

    Private Function FilteredDataTable(ByVal DetailDT As DataTable, ByVal EventID As String) As DataTable

        Dim DT As DataTable
        Dim DR As DataRow()

        DT = DetailDT.Clone
        DR = DetailDT.Select("NUMB = '" & EventID & "'")

        For Each Row As DataRow In DR

            DT.ImportRow(Row)

        Next

        Return DT

    End Function

    Friend Function ActiveDay(ByVal Day As System.DayOfWeek, ByRef Header As IEventExistingHeader) As Boolean

        Select Case Day

            Case DayOfWeek.Monday
                Return Header.Monday

            Case DayOfWeek.Tuesday
                Return Header.Tuesday

            Case DayOfWeek.Wednesday
                Return Header.Wednesday

            Case DayOfWeek.Thursday
                Return Header.Thursday

            Case DayOfWeek.Friday
                Return Header.Friday

            Case DayOfWeek.Saturday
                Return Header.Saturday

            Case DayOfWeek.Sunday
                Return Header.Sunday

        End Select

        Return False

    End Function

#End Region

End Class