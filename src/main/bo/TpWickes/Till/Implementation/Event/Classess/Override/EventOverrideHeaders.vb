﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventOverrideHeaders
    Inherits BaseCollection(Of EventOverrideHeader)
    Implements IEventOverrideHeaders

#Region "Properties"

    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements IEventOverrideHeaders.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

    Public ReadOnly Property CollectionCount() As Integer Implements IEventOverrideHeaders.CollectionCount
        Get
            Return Me.Count
        End Get
    End Property

    Public ReadOnly Property ItemObject(ByVal Index As Integer) As IEventOverrideHeader Implements IEventOverrideHeaders.ItemObject
        Get
            Return Me.Item(Index)
        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub Initialise(ByVal HeaderDT As DataTable, ByVal DetailDT As DataTable) Implements IEventOverrideHeaders.Initialise

        Me.Load(HeaderDT)

        _ExistsInDB = True

        PopulateDetail(DetailDT)

    End Sub

    Public Function OverridePrice() As Decimal? Implements IEventOverrideHeaders.OverridePrice

        Dim Detail As IEventOverrideMaster
        Dim SequenceNo As Integer
        Dim Price As System.Nullable(Of Decimal)

        SequenceNo = 0

        For Each Header As IEventOverrideHeader In Me.Items

            'multiple records : return price from entry with highest sequence no
            If SequenceNo < Header.RevisionNumber Then

                SequenceNo = Header.RevisionNumber

                'assuming each header will have only one detail, if not last one will win
                Detail = Header.MasterCollection.ItemObject(0)
                Price = Detail.Price

                'For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1
                '    Detail = Header.MasterCollection.ItemObject(DetailIndex)
                '    Price = Detail.Price
                'Next

            End If

        Next

        Return Price

    End Function

    'Public Function OverridePrice() As Decimal? Implements IEventOverrideHeaders.OverridePrice

    '    Dim Detail As IEventOverrideMaster
    '    Dim SequenceNo As Integer
    '    Dim Price As System.Nullable(Of Decimal)

    '    For Each Header As IEventOverrideHeader In Me.Items

    '        For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1

    '            Detail = Header.MasterCollection.ItemObject(DetailIndex)

    '            If SequenceNo < Header.SequenceNumber Then

    '                SequenceNo = Header.SequenceNumber

    '                Price = Detail.Price

    '            End If

    '        Next

    '    Next

    '    Return Price

    'End Function

#End Region

#Region "Private Procedures & Functions"

    Private Sub PopulateDetail(ByRef DetailDT As DataTable)

        Dim DetailCollection As IEventOverrideMasters

        For Each X As IEventOverrideHeader In Me.Items

            DetailCollection = EventOverrideMastersFactory.FactoryGet

            DetailCollection.Initialise(FilteredDataTable(DetailDT, X.ID))

            X.MasterCollection = DetailCollection

        Next

    End Sub

    Private Function FilteredDataTable(ByVal DetailDT As DataTable, ByVal ID As Integer) As DataTable

        Dim DT As DataTable
        Dim DR As DataRow()

        DT = DetailDT.Clone
        DR = DetailDT.Select("EventHeaderOverrideID = " & ID.ToString)

        For Each Row As DataRow In DR

            DT.ImportRow(Row)

        Next

        Return DT

    End Function

#End Region

End Class