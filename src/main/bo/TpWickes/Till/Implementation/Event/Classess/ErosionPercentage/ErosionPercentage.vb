﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class ErosionPercentage
    Implements IErosionPercentage

    Friend _Sku As String
    Friend _Quantity As System.Nullable(Of Integer)
    Friend _SellingPrice As System.Nullable(Of Double)
    Friend _ErosionPercentage As System.Nullable(Of Double)

    Public Property Sku() As String Implements IErosionPercentage.Sku
        Get
            Return _Sku
        End Get
        Set(ByVal value As String)
            _Sku = value
        End Set
    End Property

    Public Property Quantity() As System.Nullable(Of Integer) Implements IErosionPercentage.Quantity
        Get
            Return _Quantity
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _Quantity = value
        End Set
    End Property

    Public Property SellingPrice() As System.Nullable(Of Double) Implements IErosionPercentage.SellingPrice
        Get
            Return _SellingPrice
        End Get
        Set(ByVal value As System.Nullable(Of Double))
            _SellingPrice = value
        End Set
    End Property

    Public Property ErosionPercentage() As System.Nullable(Of Double) Implements IErosionPercentage.ErosionPercentage
        Get
            Return _ErosionPercentage
        End Get
        Set(ByVal value As System.Nullable(Of Double))
            _ErosionPercentage = value
        End Set
    End Property

End Class