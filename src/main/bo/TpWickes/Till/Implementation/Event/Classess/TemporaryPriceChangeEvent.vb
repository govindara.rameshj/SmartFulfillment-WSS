﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.IntegrationTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                               "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                               "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                               "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                               "6da046ba")> 
Public Class TemporaryPriceChangeEvent
    Implements ITemporaryPriceChangeEvent

    Friend _EventHeaderOverideID As Integer

    Public Function GetCurrentPrice(ByVal SKU As String, ByVal SelectedDate As Date, ByRef OverrideEvent As Boolean, _
                                    ByRef ExistingEventPrice As Double, ByRef ExistingEventNumber As String) As Double Implements ITemporaryPriceChangeEvent.GetCurrentPrice

        Dim X As ITemporaryPriceChangeEventHelper
        Dim TempPrice As System.Nullable(Of Decimal)
        Dim TempEventNumber As String

        X = TemporaryPriceChangeEventHelperFactory.FactoryGet

        X.Initialise(SKU, SelectedDate)

        'return existing TS event price if exist
        ExistingEventPrice = 0
        ExistingEventNumber = String.Empty

        TempPrice = X.GetEventExistingPrice()
        TempEventNumber = X.GetEventExistingEventNumber()

        If TempPrice.HasValue = True Then ExistingEventPrice = TempPrice.Value
        If TempEventNumber IsNot Nothing Then ExistingEventNumber = TempEventNumber

        'events - override
        TempPrice = X.GetEventOverridePrice()
        OverrideEvent = True
        If TempPrice.HasValue = True Then Return TempPrice.Value

        'event - existing
        TempPrice = X.GetEventExistingPrice()
        OverrideEvent = False
        If TempPrice.HasValue = True Then Return TempPrice.Value

        Return 0

    End Function

    Public Function SetNewPrice(ByVal SKU As String, ByVal SelectedDate As Date, ByVal NewPrice As Double, ByVal SecurityCheckUserOneID As Integer, ByVal SecurityCheckUserTwoID As Integer) As Boolean Implements ITemporaryPriceChangeEvent.SetNewPrice

        Dim Repository As IEventRepository

        Repository = EventRepositoryFactory.FactoryGet

        Return Repository.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideSKU, _
                                                          "Single Day Price Override - " & SKU, SelectedDate, SelectedDate, SKU, "1", _
                                                          CType(NewPrice, Decimal), SecurityCheckUserOneID, SecurityCheckUserTwoID, _EventHeaderOverideID)

    End Function

End Class