﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventExistingHeader
    Inherits Base
    Implements IEventExistingHeader

#Region "Table Fields"

    Friend _EventID As String
    Friend _Priority As String
    Friend _Monday As Boolean
    Friend _Tuesday As Boolean
    Friend _Wednesday As Boolean
    Friend _Thursday As Boolean
    Friend _Friday As Boolean
    Friend _Saturday As Boolean
    Friend _Sunday As Boolean
    Friend _Deleted As Boolean

    <ColumnMapping("NUMB")> _
    Public Property EventID() As String Implements IEventExistingHeader.EventID
        Get
            Return _EventID
        End Get
        Set(ByVal value As String)
            _EventID = value
        End Set
    End Property

    <ColumnMapping("PRIO")> _
    Public Property Priority() As String Implements IEventExistingHeader.Priority
        Get
            Return _Priority
        End Get
        Set(ByVal value As String)
            _Priority = value
        End Set
    End Property

    <ColumnMapping("DACT1")> _
    Public Property Monday() As Boolean Implements IEventExistingHeader.Monday
        Get
            Return _Monday
        End Get
        Set(ByVal value As Boolean)
            _Monday = value
        End Set
    End Property

    <ColumnMapping("DACT2")> _
    Public Property Tuesday() As Boolean Implements IEventExistingHeader.Tuesday
        Get
            Return _Tuesday
        End Get
        Set(ByVal value As Boolean)
            _Tuesday = value
        End Set
    End Property

    <ColumnMapping("DACT3")> _
    Public Property Wednesday() As Boolean Implements IEventExistingHeader.Wednesday
        Get
            Return _Wednesday
        End Get
        Set(ByVal value As Boolean)
            _Wednesday = value
        End Set
    End Property

    <ColumnMapping("DACT4")> _
    Public Property Thursday() As Boolean Implements IEventExistingHeader.Thursday
        Get
            Return _Thursday
        End Get
        Set(ByVal value As Boolean)
            _Thursday = value
        End Set
    End Property

    <ColumnMapping("DACT5")> _
    Public Property Friday() As Boolean Implements IEventExistingHeader.Friday
        Get
            Return _Friday
        End Get
        Set(ByVal value As Boolean)
            _Friday = value
        End Set
    End Property

    <ColumnMapping("DACT6")> _
    Public Property Saturday() As Boolean Implements IEventExistingHeader.Saturday
        Get
            Return _Saturday
        End Get
        Set(ByVal value As Boolean)
            _Saturday = value
        End Set
    End Property

    <ColumnMapping("DACT7")> _
    Public Property Sunday() As Boolean Implements IEventExistingHeader.Sunday
        Get
            Return _Sunday
        End Get
        Set(ByVal value As Boolean)
            _Sunday = value
        End Set
    End Property

    <ColumnMapping("IDEL")> _
    Public Property Deleted() As Boolean Implements IEventExistingHeader.Deleted
        Get
            Return _Deleted
        End Get
        Set(ByVal value As Boolean)
            _Deleted = value
        End Set
    End Property

#Region "Properties"

    Friend _Lines As IEventExistingMasters

    Public Property MasterCollection() As IEventExistingMasters Implements IEventExistingHeader.MasterCollection
        Get
            Return _Lines
        End Get
        Set(ByVal value As IEventExistingMasters)
            _Lines = value
        End Set
    End Property

#End Region

#End Region

End Class