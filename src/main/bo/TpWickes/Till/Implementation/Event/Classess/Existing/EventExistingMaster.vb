﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventExistingMaster
    Inherits Base
    Implements IEventExistingMaster

#Region "Table Fields"

    Friend _EventType As String
    Friend _EventKeyOne As String
    Friend _EventID As String
    Friend _Priority As String
    Friend _Deleted As Boolean
    Friend _DayOfWeekEvent As Boolean
    Friend _StartDate As System.Nullable(Of Date)
    Friend _EndDate As System.Nullable(Of Date)
    Friend _Price As System.Nullable(Of Decimal)

    <ColumnMapping("TYPE")> _
    Public Property EventType() As String Implements IEventExistingMaster.EventType
        Get
            Return _EventType
        End Get
        Set(ByVal value As String)
            _EventType = value
        End Set
    End Property

    <ColumnMapping("KEY1")> _
    Public Property EventKeyOne() As String Implements IEventExistingMaster.EventKeyOne
        Get
            Return _EventKeyOne
        End Get
        Set(ByVal value As String)
            _EventKeyOne = value
        End Set
    End Property

    <ColumnMapping("NUMB")> _
    Public Property EventID() As String Implements IEventExistingMaster.EventID
        Get
            Return _EventID
        End Get
        Set(ByVal value As String)
            _EventID = value
        End Set
    End Property

    <ColumnMapping("PRIO")> _
    Public Property Priority() As String Implements IEventExistingMaster.Priority
        Get
            Return _Priority
        End Get
        Set(ByVal value As String)
            _Priority = value
        End Set
    End Property

    <ColumnMapping("IDEL")> _
    Public Property Deleted() As Boolean Implements IEventExistingMaster.Deleted
        Get
            Return _Deleted
        End Get
        Set(ByVal value As Boolean)
            _Deleted = value
        End Set
    End Property

    <ColumnMapping("IDOW")> _
    Public Property DayOfWeekEvent() As Boolean Implements IEventExistingMaster.DayOfWeekEvent
        Get
            Return _DayOfWeekEvent
        End Get
        Set(ByVal value As Boolean)
            _DayOfWeekEvent = value
        End Set
    End Property

    <ColumnMapping("SDAT")> _
    Public Property StartDate() As Date? Implements IEventExistingMaster.StartDate
        Get
            Return _StartDate
        End Get
        Set(ByVal value As Date?)
            _StartDate = value
        End Set
    End Property

    <ColumnMapping("EDAT")> _
    Public Property EndDate() As Date? Implements IEventExistingMaster.EndDate
        Get
            Return _EndDate
        End Get
        Set(ByVal value As Date?)
            _EndDate = value
        End Set
    End Property

    <ColumnMapping("PRIC")> _
    Public Property Price() As Decimal? Implements IEventExistingMaster.Price
        Get
            Return _Price
        End Get
        Set(ByVal value As Decimal?)
            _Price = value
        End Set
    End Property

#End Region

End Class