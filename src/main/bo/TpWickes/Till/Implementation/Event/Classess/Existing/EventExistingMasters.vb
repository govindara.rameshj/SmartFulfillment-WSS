﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class EventExistingMasters
    Inherits BaseCollection(Of EventExistingMaster)
    Implements IEventExistingMasters

#Region "Properties"

    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements IEventExistingMasters.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

    Public ReadOnly Property CollectionCount() As Integer Implements IEventExistingMasters.CollectionCount
        Get
            Return Me.Count
        End Get
    End Property

    Public ReadOnly Property ItemObject(ByVal Index As Integer) As IEventExistingMaster Implements IEventExistingMasters.ItemObject
        Get
            Return Me.Item(Index)
        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub Initialise(ByVal DetailDT As System.Data.DataTable) Implements IEventExistingMasters.Initialise

        Me.Load(DetailDT)

        _ExistsInDB = True

    End Sub

#End Region

End Class