﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.IntegrationTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                               "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                               "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                               "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                               "6da046ba")> 
Public Class MultiplePriceOverride
    Implements IMultiplePriceOverride

    Friend _EventHeaderOverideID As Integer

#Region "Methods"

    Public Function GetDealGroup(ByVal SelectedDate As Date, ByVal SkuNumbers As String) As Recordset Implements IMultiplePriceOverride.GetDealGroup

        Dim Repository As IEventRepository
        Dim ExistingDT As DataTable
        Dim OverrideDT As DataTable
        Dim RS As Recordset

        'get data
        Repository = EventRepositoryFactory.FactoryGet
        ExistingDT = Repository.GetExistingDealGroup(SkuNumbers)
        OverrideDT = Repository.GetOverrideDealGroup(SelectedDate, SkuNumbers)

        'convert to recordset
        RS = DealGroupCreateStructure()

        With RS
            .CursorType = CursorTypeEnum.adOpenStatic
            .LockType = LockTypeEnum.adLockOptimistic
            .Open()
        End With

        For Each Row As DataRow In ExistingDT.Rows

            DealGroupAddRecord(RS, Row.Item("EventNumber").ToString, _
                                   Row.Item("DealGroupNumber").ToString, _
                                   Row.Item("DealType").ToString, _
                                   Row.Item("ItemKey").ToString, _
                                   CType(Row.Item("Deleted"), Boolean), _
                                   CType(Row.Item("Quantity"), Decimal), _
                                   CType(Row.Item("ErosionValue"), Decimal), _
                                   CType(Row.Item("ErosionPercentage"), Decimal))

        Next

        GenerateErosionPercentage(OverrideDT)

        For Each Row As DataRow In OverrideDT.Rows

            DealGroupAddRecord(RS, Row.Item("EventNumber").ToString, _
                                   Row.Item("DealGroupNumber").ToString, _
                                   Row.Item("DealType").ToString, _
                                   Row.Item("ItemKey").ToString, _
                                   CType(Row.Item("Deleted"), Boolean), _
                                   CType(Row.Item("Quantity"), Decimal), _
                                   CType(Row.Item("ErosionValue"), Decimal), _
                                   CType(Row.Item("ErosionPercentage"), Decimal))

        Next

        Return RS

    End Function

    Public Function GetEventMaster(ByVal SelectedDate As Date, ByVal SkuNumbers As String) As Recordset Implements IMultiplePriceOverride.GetEventMaster

        Dim Repository As IEventRepository
        Dim DT As DataTable
        Dim RS As Recordset

        'get data
        Repository = EventRepositoryFactory.FactoryGet
        DT = Repository.GetEventMaster(SelectedDate, SkuNumbers)

        'convert to recordset
        RS = EventMasterCreateStructure()

        With RS
            .CursorType = CursorTypeEnum.adOpenStatic
            .LockType = LockTypeEnum.adLockOptimistic
            .Open()
        End With

        For Each Row As DataRow In DT.Rows

            EventMasterAddRecord(RS, Row.Item("EventNumber").ToString, _
                                     Row.Item("Priority").ToString, _
                                     Row.Item("EventKey2").ToString, _
                                     CType(Row.Item("TimeOrDayRelated"), Boolean), _
                                     CType(Row.Item("SpecialPrice"), Decimal), _
                                     CType(Row.Item("EDAT"), Date), _
                                     Row.Item("BUYCPN").ToString, _
                                     Row.Item("GETCPN").ToString)

        Next

        Return RS

    End Function

    Public Function GetValidEvent(ByVal EventNumber As String, ByVal Priority As String) As Recordset Implements IMultiplePriceOverride.GetValidEvent

        Dim Repository As IEventRepository
        Dim DT As DataTable
        Dim RS As Recordset

        'get data
        Repository = EventRepositoryFactory.FactoryGet
        DT = Repository.GetValidEvent(EventNumber, Priority)

        'convert to recordset
        RS = ValidEventCreateStructure()

        With RS
            .CursorType = CursorTypeEnum.adOpenStatic
            .LockType = LockTypeEnum.adLockOptimistic
            .Open()
        End With

        For Each Row As DataRow In DT.Rows

            ValidEventAddRecord(RS, CType(Row.Item("StartDate"), Date), _
                                     Row.Item("StartTime").ToString, _
                                     CType(Row.Item("EndDate"), Date), _
                                     Row.Item("Description").ToString, _
                                     CType(Row.Item("ActiveDay1"), Boolean), _
                                     CType(Row.Item("ActiveDay2"), Boolean), _
                                     CType(Row.Item("ActiveDay3"), Boolean), _
                                     CType(Row.Item("ActiveDay4"), Boolean), _
                                     CType(Row.Item("ActiveDay5"), Boolean), _
                                     CType(Row.Item("ActiveDay6"), Boolean), _
                                     CType(Row.Item("ActiveDay7"), Boolean))

        Next

        Return RS

    End Function

    Public Function SetNewPrice(ByVal SkuNumberList As String, ByVal QuantityList As String, ByVal SelectedDate As Date, _
                                ByVal TotalPrice As Double, ByVal CashierSecurityID As Integer, ByVal AuthorisorSecurityID As Integer) As Boolean Implements IMultiplePriceOverride.SetNewPrice

        Dim Repository As IEventRepository

        Repository = EventRepositoryFactory.FactoryGet

        Return Repository.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideDealGroup, _
                                                          "Multiple Day Price Override: SKU List - (" & SkuNumberList & ") Quantity List - (" & QuantityList & ")", _
                                                          SelectedDate, SelectedDate, SkuNumberList, QuantityList, _
                                                          CType(TotalPrice, Decimal), CashierSecurityID, AuthorisorSecurityID, _EventHeaderOverideID)

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Function DealGroupCreateStructure() As Recordset

        Dim RS As New Recordset

        With RS

            .Fields.Append("EventNumber", DataTypeEnum.adChar, 6)
            .Fields.Append("DealGroupNumber", DataTypeEnum.adChar, 6)
            .Fields.Append("DealType", DataTypeEnum.adChar, 1)
            .Fields.Append("ItemKey", DataTypeEnum.adChar, 6)
            .Fields.Append("Deleted", DataTypeEnum.adBoolean)

            .Fields.Append("Quantity", DataTypeEnum.adDecimal, 7, FieldAttributeEnum.adFldIsNullable)
            .Fields("Quantity").NumericScale = 0

            .Fields.Append("ErosionValue", DataTypeEnum.adDecimal, 9, FieldAttributeEnum.adFldIsNullable)
            .Fields("ErosionValue").NumericScale = 2

            .Fields.Append("ErosionPercentage", DataTypeEnum.adDecimal, 7, FieldAttributeEnum.adFldIsNullable)
            .Fields("ErosionPercentage").NumericScale = 3

        End With

        Return RS

    End Function

    Friend Sub DealGroupAddRecord(ByRef RS As Recordset, _
                                  ByVal EventNumber As String, ByVal DealGroupNumber As String, ByVal DealType As String, _
                                  ByVal ItemKey As String, ByVal Deleted As Boolean, ByVal Quantity As Decimal, _
                                  ByVal ErosionValue As Decimal, ByVal ErosionPercentage As Decimal)

        With RS
            .AddNew()
            RS.Fields("EventNumber").Value = EventNumber
            RS.Fields("DealGroupNumber").Value = DealGroupNumber
            RS.Fields("DealType").Value = DealType
            RS.Fields("ItemKey").Value = ItemKey
            RS.Fields("Deleted").Value = Deleted
            RS.Fields("Quantity").Value = Quantity
            RS.Fields("ErosionValue").Value = ErosionValue
            RS.Fields("ErosionPercentage").Value = ErosionPercentage

            .Update()
        End With

    End Sub

    Friend Function EventMasterCreateStructure() As Recordset

        Dim RS As New Recordset

        With RS

            .Fields.Append("EventNumber", DataTypeEnum.adChar, 6)
            .Fields.Append("Priority", DataTypeEnum.adChar, 2, FieldAttributeEnum.adFldIsNullable)
            .Fields.Append("EventKey2", DataTypeEnum.adChar, 8)
            .Fields.Append("TimeOrDayRelated", DataTypeEnum.adBoolean)

            .Fields.Append("SpecialPrice", DataTypeEnum.adDecimal, 9, FieldAttributeEnum.adFldIsNullable)
            .Fields("SpecialPrice").NumericScale = 2

            .Fields.Append("EDAT", DataTypeEnum.adDate, FieldAttributeEnum.adFldIsNullable)
            .Fields.Append("BUYCPN", DataTypeEnum.adChar, 7)
            .Fields.Append("GETCPN", DataTypeEnum.adChar, 7)

        End With

        Return RS

    End Function

    Friend Sub EventMasterAddRecord(ByRef RS As Recordset, _
                                    ByVal EventNumber As String, ByVal Priority As String, ByVal EventKey2 As String, _
                                    ByVal TimeOrDayRelated As Boolean, ByVal SpecialPrice As Decimal, ByVal EDAT As Date, _
                                    ByVal BUYCPN As String, ByVal GETCPN As String)

        With RS
            .AddNew()
            RS.Fields("EventNumber").Value = EventNumber
            RS.Fields("Priority").Value = Priority
            RS.Fields("EventKey2").Value = EventKey2
            RS.Fields("TimeOrDayRelated").Value = TimeOrDayRelated
            RS.Fields("SpecialPrice").Value = SpecialPrice
            RS.Fields("EDAT").Value = EDAT
            RS.Fields("BUYCPN").Value = BUYCPN
            RS.Fields("GETCPN").Value = GETCPN

            .Update()
        End With

    End Sub

    Friend Function ValidEventCreateStructure() As Recordset

        Dim RS As New Recordset

        With RS

            .Fields.Append("StartDate", DataTypeEnum.adDate, FieldAttributeEnum.adFldIsNullable)
            .Fields.Append("StartTime", DataTypeEnum.adChar, 4, FieldAttributeEnum.adFldIsNullable)
            .Fields.Append("EndDate", DataTypeEnum.adDate, FieldAttributeEnum.adFldIsNullable)

            '.Fields.Append("Description", DataTypeEnum.adChar, 40, FieldAttributeEnum.adFldIsNullable)
            .Fields.Append("Description", DataTypeEnum.adVarChar, 1000, FieldAttributeEnum.adFldIsNullable)

            .Fields.Append("ActiveDay1", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay2", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay3", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay4", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay5", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay6", DataTypeEnum.adBoolean)
            .Fields.Append("ActiveDay7", DataTypeEnum.adBoolean)

        End With

        Return RS

    End Function

    Friend Sub ValidEventAddRecord(ByRef RS As Recordset, _
                                   ByVal StartDate As Date, ByVal StartTime As String, ByVal EndDate As Date, ByVal Description As String, _
                                   ByVal ActiveDay1 As Boolean, ByVal ActiveDay2 As Boolean, ByVal ActiveDay3 As Boolean, _
                                   ByVal ActiveDay4 As Boolean, ByVal ActiveDay5 As Boolean, ByVal ActiveDay6 As Boolean, ByVal ActiveDay7 As Boolean)

        With RS
            .AddNew()
            RS.Fields("StartDate").Value = StartDate
            RS.Fields("StartTime").Value = StartTime
            RS.Fields("EndDate").Value = EndDate
            RS.Fields("Description").Value = Description
            RS.Fields("ActiveDay1").Value = ActiveDay1
            RS.Fields("ActiveDay2").Value = ActiveDay2
            RS.Fields("ActiveDay3").Value = ActiveDay3
            RS.Fields("ActiveDay4").Value = ActiveDay4
            RS.Fields("ActiveDay5").Value = ActiveDay5
            RS.Fields("ActiveDay6").Value = ActiveDay6
            RS.Fields("ActiveDay7").Value = ActiveDay7

            .Update()
        End With

    End Sub

    Friend Sub GenerateErosionPercentage(ByRef DT As DataTable)

        Dim PercentageCollection As List(Of IErosionPercentages)
        Dim Percentage As IErosionPercentages
        Dim EventNumber As String

        PercentageCollection = New List(Of IErosionPercentages)
        EventNumber = String.Empty

        'populate
        For Each Row As DataRow In DT.Rows

            If EventNumber <> Row.Item("EventNumber").ToString Then

                EventNumber = Row.Item("EventNumber").ToString

                Percentage = ErosionPercentagesFactory.FactoryGet
                Percentage.EventNumber = EventNumber

                Percentage.Add(Row.Item("ItemKey").ToString, CType(Row.Item("Quantity"), Integer), CType(Row.Item("SellingPrice"), Decimal))
                PercentageCollection.Add(Percentage)

            Else

                Percentage = PercentageCollection.Find(Function(X As IErosionPercentages) X.EventNumber = EventNumber)
                Percentage.Add(Row.Item("ItemKey").ToString, CType(Row.Item("Quantity"), Integer), CType(Row.Item("SellingPrice"), Decimal))

            End If

        Next

        'calculate
        For Each EventNo As IErosionPercentages In PercentageCollection

            EventNo.GenerateErosionPrices()

        Next

        'update
        Dim Rows As DataRow()

        For Each Column As DataColumn In DT.Columns

            Column.ReadOnly = False

        Next

        For Each EventNo As IErosionPercentages In PercentageCollection

            For Each SKU As IErosionPercentage In EventNo.ErosionPercentageList

                Rows = DT.Select("EventNumber = '" & EventNo.EventNumber & "' AND ItemKey = '" & SKU.Sku & "'")

                Rows(0).Item("ErosionPercentage") = SKU.ErosionPercentage

            Next

        Next

    End Sub

#End Region

End Class