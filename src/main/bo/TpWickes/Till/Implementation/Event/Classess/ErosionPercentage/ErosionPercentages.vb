﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class ErosionPercentages
    Implements IErosionPercentages

    Friend _EventNumber As String
    Friend _ErosionPercentageList As New List(Of IErosionPercentage)

#Region "Properties"

    Public Property EventNumber() As String Implements IErosionPercentages.EventNumber

        Get

            Return _EventNumber

        End Get
        Set(ByVal value As String)

            _EventNumber = value

        End Set

    End Property

    Public ReadOnly Property ErosionPercentageList() As List(Of IErosionPercentage) Implements IErosionPercentages.ErosionPercentageList

        Get

            Return _ErosionPercentageList

        End Get

    End Property

#End Region

#Region "Methods"

    Public Sub Add(ByVal Sku As String, _
                   ByVal Quantity As System.Nullable(Of Integer), ByVal SellingPrice As System.Nullable(Of Double)) Implements IErosionPercentages.Add

        Dim X As IErosionPercentage

        X = ErosionPercentageFactory.FactoryGet

        With X
            .Sku = Sku
            .Quantity = Quantity
            .SellingPrice = SellingPrice
        End With

        _ErosionPercentageList.Add(X)

    End Sub

    Public Sub GenerateErosionPrice() Implements IErosionPercentages.GenerateErosionPrices

        If _ErosionPercentageList.Count = 0 Then Exit Sub

        If SkuQuantitiesValid(_ErosionPercentageList) = False Then Exit Sub

        If SkuSellingPricesValid(_ErosionPercentageList) = False Then Exit Sub

        CalculateSkuErosionPercentages(_ErosionPercentageList)

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Friend Function SkuQuantitiesValid(ByRef List As List(Of IErosionPercentage)) As Boolean

        SkuQuantitiesValid = True

        For Each X As IErosionPercentage In List

            If X.Quantity.HasValue = False Then SkuQuantitiesValid = False

        Next

    End Function

    Friend Function SkuSellingPricesValid(ByRef List As List(Of IErosionPercentage)) As Boolean

        SkuSellingPricesValid = True

        For Each X As IErosionPercentage In List

            If X.SellingPrice.HasValue = False Then SkuSellingPricesValid = False

        Next

    End Function

    Friend Function CalculateActualPrice(ByRef List As List(Of IErosionPercentage)) As Double

        Dim ActualPrice As Double

        For Each X As IErosionPercentage In List

            ActualPrice += X.Quantity.Value * X.SellingPrice.Value

        Next

        Return ActualPrice

    End Function

    Friend Sub CalculateSkuErosionPercentages(ByRef List As List(Of IErosionPercentage))

        Dim ActualPrice As Double

        ActualPrice = CalculateActualPrice(List)

        For Each X As IErosionPercentage In List

            X.ErosionPercentage = SkuErosionPercentage(X.Quantity.Value, X.SellingPrice.Value, ActualPrice) * 100

        Next

        '% erosion need to add up to 100%; fudge will be applied to the last item in list
        List.Item(List.Count - 1).ErosionPercentage = RoundValue(List.Item(List.Count - 1).ErosionPercentage.Value + ErosionPercentageDescrepency(List), 3)

    End Sub

    Friend Function ErosionPercentageDescrepency(ByRef List As List(Of IErosionPercentage)) As Double

        Dim Total As Double

        For Each X As IErosionPercentage In List

            Total += X.ErosionPercentage.Value

        Next

        Return RoundValue(100.0 - RoundValue(Total, 3), 3)

    End Function

    Friend Function SkuErosionPercentage(ByVal Quantity As Integer, ByVal SellingPrice As Double, ByVal DealGroupPrice As Double) As Double

        Return RoundValue((Quantity * SellingPrice) / DealGroupPrice, 5)

    End Function

    Friend Function RoundValue(ByVal Value As Double, ByVal Precision As Integer) As Double

        Return Math.Round(Value, Precision, MidpointRounding.AwayFromZero)

    End Function

#End Region

End Class