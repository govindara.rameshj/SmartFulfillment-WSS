﻿Friend Class TemporaryPriceChangeEventHelperFactory

    Private Shared _mFactoryMember As ITemporaryPriceChangeEventHelper

    Public Shared Function FactoryGet() As ITemporaryPriceChangeEventHelper

        If _mFactoryMember Is Nothing Then

            Return New TemporaryPriceChangeEventHelper

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ITemporaryPriceChangeEventHelper)

        _mFactoryMember = obj

    End Sub

End Class
