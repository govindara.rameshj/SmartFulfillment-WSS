﻿Public Class EventExistingHeaderFactory

    Private Shared _mFactoryMember As IEventExistingHeader

    Public Shared Function FactoryGet() As IEventExistingHeader

        If _mFactoryMember Is Nothing Then

            Return New EventExistingHeader

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventExistingHeader)

        _mFactoryMember = obj

    End Sub

End Class