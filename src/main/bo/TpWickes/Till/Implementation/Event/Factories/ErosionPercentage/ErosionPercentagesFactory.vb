﻿Public Class ErosionPercentagesFactory

    Private Shared _mFactoryMember As IErosionPercentages

    Public Shared Function FactoryGet() As IErosionPercentages

        If _mFactoryMember Is Nothing Then

            Return New ErosionPercentages

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IErosionPercentages)

        _mFactoryMember = obj

    End Sub

End Class