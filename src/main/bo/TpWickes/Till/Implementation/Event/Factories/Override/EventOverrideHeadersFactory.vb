﻿Public Class EventOverrideHeadersFactory

    Private Shared _mFactoryMember As IEventOverrideHeaders

    Public Shared Function FactoryGet() As IEventOverrideHeaders

        If _mFactoryMember Is Nothing Then

            Return New EventOverrideHeaders

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventOverrideHeaders)

        _mFactoryMember = obj

    End Sub

End Class