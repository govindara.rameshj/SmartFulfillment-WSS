﻿Public Class EventExistingHeadersFactory

    Private Shared _mFactoryMember As IEventExistingHeaders

    Public Shared Function FactoryGet() As IEventExistingHeaders

        If _mFactoryMember Is Nothing Then

            Return New EventExistingHeaders

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventExistingHeaders)

        _mFactoryMember = obj

    End Sub

End Class