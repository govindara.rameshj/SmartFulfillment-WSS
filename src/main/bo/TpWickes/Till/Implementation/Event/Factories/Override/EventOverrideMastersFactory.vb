﻿Public Class EventOverrideMastersFactory

    Private Shared _mFactoryMember As IEventOverrideMasters

    Public Shared Function FactoryGet() As IEventOverrideMasters

        If _mFactoryMember Is Nothing Then

            Return New EventOverrideMasters

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventOverrideMasters)

        _mFactoryMember = obj

    End Sub

End Class