﻿Public Class ErosionPercentageFactory

    Private Shared _mFactoryMember As ErosionPercentage

    Public Shared Function FactoryGet() As ErosionPercentage

        If _mFactoryMember Is Nothing Then

            Return New ErosionPercentage

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ErosionPercentage)

        _mFactoryMember = obj

    End Sub

End Class