﻿Public Class EventOverrideMasterFactory

    Private Shared _mFactoryMember As IEventOverrideMaster

    Public Shared Function FactoryGet() As IEventOverrideMaster

        If _mFactoryMember Is Nothing Then

            Return New EventOverrideMaster

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventOverrideMaster)

        _mFactoryMember = obj

    End Sub

End Class