﻿Public Class EventExistingMasterFactory

    Private Shared _mFactoryMember As IEventExistingMaster

    Public Shared Function FactoryGet() As IEventExistingMaster

        If _mFactoryMember Is Nothing Then

            Return New EventExistingMaster

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventExistingMaster)

        _mFactoryMember = obj

    End Sub

End Class