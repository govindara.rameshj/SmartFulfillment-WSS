﻿Public Class EventExistingMastersFactory

    Private Shared _mFactoryMember As IEventExistingMasters

    Public Shared Function FactoryGet() As IEventExistingMasters

        If _mFactoryMember Is Nothing Then

            Return New EventExistingMasters

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventExistingMasters)

        _mFactoryMember = obj

    End Sub

End Class