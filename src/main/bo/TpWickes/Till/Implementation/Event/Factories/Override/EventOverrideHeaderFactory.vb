﻿Public Class EventOverrideHeaderFactory

    Private Shared _mFactoryMember As IEventOverrideHeader

    Public Shared Function FactoryGet() As IEventOverrideHeader

        If _mFactoryMember Is Nothing Then

            Return New EventOverrideHeader

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IEventOverrideHeader)

        _mFactoryMember = obj

    End Sub

End Class