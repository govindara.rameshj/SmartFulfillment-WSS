﻿Public Interface ITemporaryPriceChangeEventHelper

    ReadOnly Property ExistsInDatabase() As Boolean
    ReadOnly Property ExistingEventCollection() As IEventExistingHeaders
    ReadOnly Property OverrideEventCollection() As IEventOverrideHeaders

    Function GetEventOverridePrice() As System.Nullable(Of Decimal)
    Function GetEventExistingPrice() As System.Nullable(Of Decimal)
    Function GetEventExistingEventNumber() As String

    Function Initialise(ByVal SKU As String, ByVal SelectedDate As System.Nullable(Of Date)) As Boolean

End Interface
