﻿Imports System.Text

Public Class PrintCoupon
    Implements IPrintCoupon

    Private POSPrinter As IPOSPrinter

#Region "IPrintCoupon Implementation"

    Public Function GetCouponBody(ByVal SaleCouponId As String) As String Implements IPrintCoupon.GetCouponBody
        Dim CouponText As ICouponTextLine
        Dim CouponTextLines As ICouponTextLines = CouponTextLinesFactory.FactoryGet
        Dim CouponBody As New StringBuilder

        GetCouponBody = ""
        If CouponBody IsNot Nothing Then
            If CouponTextLines.Initialise(SaleCouponId, False) Then
                'For Each CouponText In CouponTextLines.List
                For nextLine As Integer = 0 To CouponTextLines.LineCount - 1
                    CouponText = CouponTextLines.Line(nextLine)
                    If CouponText IsNot Nothing Then
                        With CouponText
                            CouponBody.Append(GetFormattedText(Trim$(.PrintText), .PrintSize, .TextAlignment))
                        End With
                    End If
                Next
                GetCouponBody = CouponBody.ToString
            End If
        End If
    End Function

    Public Function GetCouponFooterNoEnd(ByVal CouponId As String) As String Implements IPrintCoupon.GetCouponFooterNoEnd

        GetCouponFooterNoEnd = GetPrintableCouponFooter(CouponId)
    End Function

    Public Function GetCouponFooterLineFeedAtEnd(ByVal CouponId As String, ByVal NumberOfLinesAtEnd As Integer) As String Implements IPrintCoupon.GetCouponFooterLineFeedAtEnd
        Dim CouponFooter As New StringBuilder

        With CouponFooter
            .Append(GetPrintableCouponFooter(CouponId))
            .Append(GetPOSPrinter.GetBlankLine(NumberOfLinesAtEnd))
            .Append(GetPOSPrinter.GetDivideLine)

            GetCouponFooterLineFeedAtEnd = .ToString
        End With
    End Function

    Public Function GetCouponBarCodeStation() As Integer Implements IPrintCoupon.GetCouponBarCodeStation

        GetCouponBarCodeStation = CouponBarCodeStation
    End Function

    Public Function GetCouponBarCodeSymbology() As Integer Implements IPrintCoupon.GetCouponBarCodeSymbology

        GetCouponBarCodeSymbology = CouponBarCodeSymbology
    End Function

    Public Function GetCouponBarCodeHeight() As Integer Implements IPrintCoupon.GetCouponBarCodeHeight

        GetCouponBarCodeHeight = CouponBarCodeHeight
    End Function

    Public Function GetCouponBarCodeWidth() As Integer Implements IPrintCoupon.GetCouponBarCodeWidth

        GetCouponBarCodeWidth = CouponBarCodeWidth
    End Function

    Public Function GetCouponBarCodeAlignment() As Integer Implements IPrintCoupon.GetCouponBarCodeAlignment

        GetCouponBarCodeAlignment = CouponBarCodeAlignment
    End Function

    Public Function GetCouponBarCodeTextPosition() As Integer Implements IPrintCoupon.GetCouponBarCodeTextPosition

        GetCouponBarCodeTextPosition = CouponBarCodeTextPosition
    End Function

    Friend Function GetPrintableCouponFooter(ByVal CouponId As String) As String
        Dim CouponFooter As New StringBuilder
        Dim CouponMaster As TpWickes.Core.Till.Interface.Coupon.ICoupon = CouponFactory.FactoryGet

        GetPrintableCouponFooter = ""
        If CouponFooter IsNot Nothing Then
            With CouponFooter
                .Append(GetPOSPrinter.GetBlankLine(2))
                .Append(GetPrintableCouponTermsAndConditions)
                If CouponMaster IsNot Nothing Then
                    If CouponMaster.Initialise(CouponId, False) Then
                        If CouponMaster.Exclusive Then
                            .Append(GetPOSPrinter.GetBlankLine(2))
                            .Append(GetPrintableCouponExclusiveClause)
                        End If
                        If Not CouponMaster.Reusable Then
                            .Append(GetPOSPrinter.GetBlankLine(2))
                            .Append(GetPrintableCouponNonReusableClause)
                        End If
                    End If
                End If
                .Append(GetPOSPrinter.GetBlankLine(1))
                GetPrintableCouponFooter = CouponFooter.ToString
            End With
        End If
    End Function

    Friend Function GetPrintableCouponTermsAndConditions() As String
        Dim CouponHeader As ICouponHeader = CouponHeaderFactory.FactoryGet

        GetPrintableCouponTermsAndConditions = GetFormattedText(CouponHeader.GetTermsAndConditionsText, CouponTextPrintSize.Small, CouponTextAlignment.Left)
    End Function

    Friend Function GetPrintableCouponExclusiveClause() As String
        Dim CouponHeader As ICouponHeader = CouponHeaderFactory.FactoryGet

        GetPrintableCouponExclusiveClause = GetFormattedText(CouponHeader.GetExclusiveClause, CouponTextPrintSize.Small, CouponTextAlignment.Left)
    End Function

    Friend Function GetPrintableCouponNonReusableClause() As String
        Dim CouponHeader As ICouponHeader = CouponHeaderFactory.FactoryGet

        GetPrintableCouponNonReusableClause = GetFormattedText(CouponHeader.GetNonReusableClause, CouponTextPrintSize.Small, CouponTextAlignment.Left)
    End Function

    Friend Function GetFormattedText(ByVal Text As String, ByVal PrintSize As CouponTextPrintSize, ByVal Alignment As CouponTextAlignment) As String

        GetFormattedText = Text
        If Len(Text) > 0 Then
            Dim maxLineLen As Integer
            Dim remainingWords() As String
            Dim printData As StringBuilder
            Dim printLine As New StringBuilder

            Text = Text.Replace(ChrW(163), GetPOSPrinter.GetCurrencySymbol)
            maxLineLen = GetMaxLineLength(PrintSize)
            ReDim remainingWords(0)
            remainingWords(0) = Nothing
            Do
                printData = GetFirstLineOfWordsFromText(Text, maxLineLen, remainingWords)
                AlignText(printData, Alignment, maxLineLen)
                printLine.Append(GetLineStart(PrintSize))
                printLine.Append(printData.ToString)
                printLine.Append(GetPOSPrinter.GetLineEnd)
                ' Now manage any words from line that did not fit on
                If HaveRemainingWords(remainingWords) Then
                    Text = String.Join(" ", remainingWords).Trim()
                End If
            Loop While HaveRemainingWords(remainingWords)
            GetFormattedText = printLine.ToString()
        End If
    End Function

    Friend Function GetMaxLineLength(ByVal PrintSize As CouponTextPrintSize) As Integer

        Select Case PrintSize
            Case CouponTextPrintSize.Small
                GetMaxLineLength = GetPOSPrinter.GetLineLengthCompressed
            Case CouponTextPrintSize.Large
                GetMaxLineLength = GetPOSPrinter.GetLineLengthBig
            Case CouponTextPrintSize.Huge
                GetMaxLineLength = GetPOSPrinter.GetLineLengthHuge
            Case Else
                GetMaxLineLength = GetPOSPrinter.GetLineLengthNormal
        End Select
    End Function

    Friend Function GetFirstLineOfWordsFromText(ByVal Text As String, ByVal MaxLineLen As Integer, ByRef RemainingWords() As String) As StringBuilder
        Dim words() As String
        Dim wordsAddedToLineSoFar As Integer
        Dim delimiters() As String = {" "}

        ' Need to make New Lines into a separate word for spliting up lines, e.g. 'quisque.vbCrLfOrnaum' would be treated as 1 word if did not do this
        Text = Text.Replace(vbNewLine, " \n/ ")
        GetFirstLineOfWordsFromText = New StringBuilder()
        words = Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
        'printData = New StringBuilder
        wordsAddedToLineSoFar = 0
        ReDim RemainingWords(0)
        RemainingWords(0) = Nothing

        ' If 1st word is already too big, then just go with truncated version of
        ' that and ignore the rest
        If words(0).Length > MaxLineLen Then
            GetFirstLineOfWordsFromText.Append(Text.Substring(0, MaxLineLen))
            GetFirstLineOfWordsFromText.Append(GetPOSPrinter.GetNewLine)
        Else
            ' string the words together up to the line length limit limit and save the rest for next line
            Dim PenultimateLineWasANewLine As Boolean

            For Each word As String In words
                ' New Line indicators (see above) should not be treated as proper words, just readd the new line in
                If String.Compare(word, "\n/") = 0 Then
                    GetFirstLineOfWordsFromText.Append(vbNewLine)
                    PenultimateLineWasANewLine = True
                Else
                    ' Make sure not reached the end of line, or had new lines on previous line(s)
                    If GetFirstLineOfWordsFromText.ToString.Length + (" " & word).Length <= MaxLineLen And Not PenultimateLineWasANewLine Then
                        If GetFirstLineOfWordsFromText.ToString.Trim.Length > 0 Then
                            GetFirstLineOfWordsFromText.Append(" ")
                        End If
                        GetFirstLineOfWordsFromText.Append(word)
                        wordsAddedToLineSoFar += 1
                    Else
                        ' Save any words that not fit on this line or come after a new line indicator, for the next line
                        ReDim RemainingWords(words.GetUpperBound(0) - wordsAddedToLineSoFar)
                        Array.Copy(words, wordsAddedToLineSoFar, RemainingWords, 0, RemainingWords.GetUpperBound(0) + 1)

                        ' Return any new line indicators back to original state
                        Dim NextWord As Integer = 0

                        Do While NextWord <= RemainingWords.GetUpperBound(0)
                            If String.Compare(RemainingWords(NextWord), "\n/") = 0 Then
                                If NextWord = 0 Then
                                    RemainingWords(NextWord + 1) = vbNewLine & RemainingWords(NextWord + 1)
                                Else
                                    RemainingWords(NextWord - 1) = RemainingWords(NextWord - 1) & vbNewLine
                                End If
                                ' Delete '\n' word by moving everything after, up 1 place...
                                For Delete As Integer = NextWord To RemainingWords.GetUpperBound(0) - 1
                                    RemainingWords(Delete) = RemainingWords(Delete + 1)
                                Next
                                '...and remove the duplicate of the end
                                ReDim Preserve RemainingWords(RemainingWords.GetUpperBound(0) - 1)
                            Else
                                NextWord += 1
                            End If
                        Loop
                        Exit For
                    End If
                End If
            Next word
        End If
    End Function

    Friend Function AlignText(ByRef Text As StringBuilder, ByVal Alignment As CouponTextAlignment, ByVal MaxLineLen As Integer) As StringBuilder

        If Text IsNot Nothing Then
            With Text
                Select Case Alignment
                    Case CouponTextAlignment.Centre
                        .Insert(0, New String(" "c, CInt((MaxLineLen - .ToString.Length) / 2)))
                    Case CouponTextAlignment.Right
                        .Insert(0, New String(" "c, (MaxLineLen - .ToString.Length)))
                    Case Else
                        ' Nothing to do, i.e. leave line as is
                End Select
            End With
        End If
        AlignText = Text
    End Function

    Friend Function HaveRemainingWords(ByVal RemainingWords() As String) As Boolean

        If RemainingWords.Length > 0 Then
            For Each word As String In RemainingWords
                If word IsNot Nothing Then
                    If word.Trim.Length > 0 Then
                        HaveRemainingWords = True
                        Exit For
                    End If
                End If
            Next
        Else
            HaveRemainingWords = True
        End If
    End Function

    'Friend Function GetArrayOfWordsThatFitLineLength(ByVal Text As String, ByVal MaxLineLen As Integer) As String()
    '    Dim returnWords() As String
    '    Dim textWords() As String
    '    Dim delimiters() As String = {" ", vbNewLine}

    '    ReDim GetArrayOfWordsThatFitLineLength(0)

    '    Do While Text.Contains("  ")
    '        Text = Text.Replace("  ", " ")
    '    Loop
    '    'textWords = Text.Split(" "c)
    '    textWords = Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
    '    ReDim returnWords(0)
    '    For Each word As String In textWords
    '        ' Split any words
    '        Do While word.Length > MaxLineLen
    '            returnWords(returnWords.GetUpperBound(0)) = word.Substring(0, MaxLineLen - 1) & "-"
    '            ReDim Preserve returnWords(returnWords.GetUpperBound(0) + 1)
    '            word = word.Substring(MaxLineLen - 1)
    '        Loop
    '        returnWords(returnWords.GetUpperBound(0)) = word
    '        ReDim Preserve returnWords(returnWords.GetUpperBound(0) + 1)
    '    Next
    '    ReDim GetArrayOfWordsThatFitLineLength(returnWords.GetUpperBound(0))
    '    returnWords.CopyTo(GetArrayOfWordsThatFitLineLength, 0)
    '    ' remove the last unfilled entry
    '    ReDim Preserve GetArrayOfWordsThatFitLineLength(returnWords.GetUpperBound(0) - 1)
    'End Function

    Friend Function GetLineStart(ByVal PrintSize As CouponTextPrintSize) As String
        Dim lineStart As New StringBuilder

        Select Case PrintSize
            Case CouponTextPrintSize.Small
                GetLineStart = GetPOSPrinter.GetLineStartCompressed
            Case CouponTextPrintSize.Large
                GetLineStart = GetPOSPrinter.GetLineStartBig
            Case CouponTextPrintSize.Huge
                GetLineStart = GetPOSPrinter.GetLineStartHuge
            Case Else
                GetLineStart = GetPOSPrinter.GetLineStartNormal
        End Select
    End Function

#End Region

    Friend ReadOnly Property CouponBarCodeAlignment() As Integer
        Get
            CouponBarCodeAlignment = GetPOSPrinter.GetPrintBarCodeAlignmentCentre
        End Get
    End Property

    Friend ReadOnly Property CouponBarCodeHeight() As Integer
        Get
            CouponBarCodeHeight = 90
        End Get
    End Property

    Friend ReadOnly Property CouponBarCodeStation() As Integer
        Get
            CouponBarCodeStation = GetPOSPrinter.GetFiscalPrinterStationReceipt
        End Get
    End Property

    Friend ReadOnly Property CouponBarCodeSymbology() As Integer
        Get
            CouponBarCodeSymbology = GetPOSPrinter.GetPrintBarCodeSymbologyCode39
        End Get
    End Property

    Friend ReadOnly Property CouponBarCodeTextPosition() As Integer
        Get
            CouponBarCodeTextPosition = GetPOSPrinter.GetPrintBarCodeTextPositionBelow
        End Get
    End Property

    Friend ReadOnly Property CouponBarCodeWidth() As Integer
        Get
            CouponBarCodeWidth = 162
        End Get
    End Property

    Friend Function GetPOSPrinter() As IPOSPrinter

        If POSPrinter Is Nothing Then
            POSPrinter = POSPrinterFactory.POSPrinterFactory.FactoryGet
        End If
        GetPOSPrinter = POSPrinter
    End Function
End Class
