﻿Public Class Header
    Implements IHeader

#Region "IHeader Implementation"

    Public Function CouponRedeemed(ByVal CouponMasterID As String) As Boolean Implements IHeader.CouponRedeemed

        CouponRedeemed = False              ' Webservice is not currently available, so default to not redeemed

    End Function

    Public Function CouponWebServiceAvailable() As Boolean Implements IHeader.CouponWebServiceAvailable

        CouponWebServiceAvailable = False   ' Webservice is not currently available

    End Function

#End Region

End Class