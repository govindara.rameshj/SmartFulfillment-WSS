﻿Public Interface IVolumeMovementResponseMessage

    Property SuccessFlag() As Boolean
    Property EstoreFulfiller() As String
    Property HubFulfiller() As String

    Function ResponseMessage() As String

End Interface
