﻿'VB6 cannot process decimal type, will use double

Public Class OrderHeader
    Implements IOrderHeader

    Friend _sellingStoreCode As Integer
    Friend _sellingStoreOrderNumber As Integer
    Friend _requiredDeliveryDate As Date
    Friend _deliveryCharge As Double
    Friend _totalOrderValue As Double
    Friend _saleDate As Date
    Friend _deliveryPostcode As String
    Friend _toBeDelivered As Boolean
    Friend _lines As New List(Of IOrderLine)

    Public Property SellingStoreCode() As Integer Implements IOrderHeader.SellingStoreCode
        Get
            Return (_sellingStoreCode)

        End Get
        Set(ByVal value As Integer)
            _sellingStoreCode = value
        End Set
    End Property

    Public Property SellingStoreOrderNumber() As Integer Implements IOrderHeader.SellingStoreOrderNumber
        Get
            Return (_sellingStoreOrderNumber)

        End Get
        Set(ByVal value As Integer)
            _sellingStoreOrderNumber = value
        End Set
    End Property

    Public Property RequiredDeliveryDate() As Date Implements IOrderHeader.RequiredDeliveryDate
        Get
            Return (_requiredDeliveryDate)

        End Get
        Set(ByVal value As Date)
            _requiredDeliveryDate = value
        End Set
    End Property

    Public Property DeliveryCharge() As Double Implements IOrderHeader.DeliveryCharge
        Get
            Return (_deliveryCharge)

        End Get
        Set(ByVal value As Double)
            _deliveryCharge = value
        End Set
    End Property

    Public Property TotalOrderValue() As Double Implements IOrderHeader.TotalOrderValue
        Get
            Return (_totalOrderValue)

        End Get
        Set(ByVal value As Double)
            _totalOrderValue = value
        End Set
    End Property

    Public Property SaleDate() As Date Implements IOrderHeader.SaleDate
        Get
            Return (_saleDate)

        End Get
        Set(ByVal value As Date)
            _saleDate = value
        End Set
    End Property

    Public Property DeliveryPostcode() As String Implements IOrderHeader.DeliveryPostcode
        Get
            Return (_deliveryPostcode)

        End Get
        Set(ByVal value As String)
            _deliveryPostcode = value
        End Set
    End Property

    Public Property ToBeDelivered() As Boolean Implements IOrderHeader.ToBeDelivered
        Get
            Return (_toBeDelivered)

        End Get
        Set(ByVal value As Boolean)
            _toBeDelivered = value
        End Set
    End Property

    Public Property Lines() As List(Of IOrderLine) Implements IOrderHeader.Lines

        Get

            Return _lines

        End Get
        Set(ByVal value As List(Of IOrderLine))

            _lines = value

        End Set

    End Property

    Public Sub AddByValue(ByVal SellingStoreLineNo As Integer, ByVal ProductCode As Integer, ByVal ProductDescription As String, _
                          ByVal TotalOrderQuantity As Double, ByVal QuantityTaken As Integer, ByVal UOM As String, _
                          ByVal LineValue As Double, ByVal DeliveryChargeItem As Boolean, ByVal SellingPrice As Double) Implements IOrderHeader.AddByValue

        Dim OrderLine As IOrderLine

        'OrderLine = New OrderLine
        OrderLine = OrderLineFactoryDirectlyImplemented()

        With OrderLine
            .SellingStoreLineNo = SellingStoreLineNo
            .ProductCode = ProductCode
            .ProductDescription = ProductDescription
            .TotalOrderQuantity = TotalOrderQuantity
            .QuantityTaken = QuantityTaken
            .UOM = UOM
            .LineValue = LineValue
            .DeliveryChargeItem = DeliveryChargeItem
            .SellingPrice = SellingPrice
        End With

        _lines.Add(OrderLine)

    End Sub

    Friend Function OrderLineFactoryDirectlyImplemented(Optional ByVal Line As IOrderLine = Nothing) As IOrderLine

        If Line IsNot Nothing Then

            Return Line

        Else

            Return New OrderLine

        End If

    End Function

End Class

Public Class OrderLine
    Implements IOrderLine

    Friend _sellingStoreLineNo As Integer
    Friend _productCode As Integer
    Friend _productDescription As String
    Friend _totalOrderQuantity As Double
    Friend _quantityTaken As Integer
    Friend _uom As String
    Friend _lineValue As Double
    Friend _deliveryChargeItem As Boolean
    Friend _sellingPrice As Double

    Public Property SellingStoreLineNo() As Integer Implements IOrderLine.SellingStoreLineNo
        Get
            Return _sellingStoreLineNo
        End Get
        Set(ByVal value As Integer)
            _sellingStoreLineNo = value
        End Set
    End Property

    Public Property ProductCode() As Integer Implements IOrderLine.ProductCode
        Get
            Return _productCode
        End Get
        Set(ByVal value As Integer)
            _productCode = value
        End Set
    End Property

    Public Property ProductDescription() As String Implements IOrderLine.ProductDescription
        Get
            Return _productDescription
        End Get
        Set(ByVal value As String)
            _productDescription = value
        End Set
    End Property

    Public Property TotalOrderQuantity() As Double Implements IOrderLine.TotalOrderQuantity
        Get
            Return _totalOrderQuantity
        End Get
        Set(ByVal value As Double)
            _totalOrderQuantity = value
        End Set
    End Property

    Public Property QuantityTaken() As Integer Implements IOrderLine.QuantityTaken
        Get
            Return _quantityTaken
        End Get
        Set(ByVal value As Integer)
            _quantityTaken = value
        End Set
    End Property

    Public Property UOM() As String Implements IOrderLine.UOM
        Get
            Return _uom
        End Get
        Set(ByVal value As String)
            _uom = value
        End Set
    End Property

    Public Property LineValue() As Double Implements IOrderLine.LineValue
        Get
            Return _lineValue
        End Get
        Set(ByVal value As Double)
            _lineValue = value
        End Set
    End Property

    Public Property DeliveryChargeItem() As Boolean Implements IOrderLine.DeliveryChargeItem
        Get
            Return _deliveryChargeItem
        End Get
        Set(ByVal value As Boolean)
            _deliveryChargeItem = value
        End Set
    End Property

    Public Property SellingPrice() As Double Implements IOrderLine.SellingPrice
        Get
            Return _sellingPrice
        End Get
        Set(ByVal value As Double)
            _sellingPrice = value
        End Set
    End Property

End Class