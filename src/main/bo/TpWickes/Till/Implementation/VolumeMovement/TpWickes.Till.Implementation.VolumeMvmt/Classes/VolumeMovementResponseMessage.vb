﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class VolumeMovementResponseMessage
    Implements IVolumeMovementResponseMessage

#Region "Properties"

    Friend _EstoreFulfiller As String
    Friend _HubFulfiller As String
    Friend _SuccessFlag As Boolean

    Public Property EstoreFulfiller() As String Implements IVolumeMovementResponseMessage.EstoreFulfiller

        Get

            Return _EstoreFulfiller

        End Get
        Set(ByVal value As String)

            _EstoreFulfiller = value

        End Set

    End Property

    Public Property HubFulfiller() As String Implements IVolumeMovementResponseMessage.HubFulfiller

        Get

            Return _HubFulfiller

        End Get
        Set(ByVal value As String)

            _HubFulfiller = value

        End Set

    End Property

    Public Property SuccessFlag() As Boolean Implements IVolumeMovementResponseMessage.SuccessFlag

        Get

            Return _SuccessFlag

        End Get
        Set(ByVal value As Boolean)

            _SuccessFlag = value

        End Set

    End Property

#End Region

#Region "Methods"

    Public Sub New()

        _EstoreFulfiller = String.Empty
        _HubFulfiller = String.Empty
        _SuccessFlag = False

    End Sub

    Public Function ResponseMessage() As String Implements IVolumeMovementResponseMessage.ResponseMessage

        Dim X As ITextCommunicationLines

        Dim ResponseType As TextCommunicationVolumeMovementTypes
        Dim ResponseString As String

        X = TextCommunicationLinesFactory.FactoryGet
        ResponseType = WorkOutResponseType()
        X.Initialise(TextCommunicationFunctionalAreas.Volume, WorkOutResponseType())
        ResponseString = X.GetFullText

        Return FormatResponseString(ResponseType, ResponseString)

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Function WorkOutResponseType() As TextCommunicationVolumeMovementTypes

        If _SuccessFlag = False Then Return TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure

        If _EstoreFulfiller = String.Empty And _HubFulfiller = String.Empty Then Return TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure

        If _EstoreFulfiller <> String.Empty And _HubFulfiller <> String.Empty Then Return TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageSplit

        If _HubFulfiller <> String.Empty Then Return TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageHub

        If _EstoreFulfiller <> String.Empty Then Return TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageEstore

    End Function

    Friend Function FormatResponseString(ByVal X As TextCommunicationVolumeMovementTypes, ByVal ReponseString As String) As String

        Select Case X

            Case TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageEstore, TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure

                Return ReponseString

            Case TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageHub, TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageSplit

                Return ReponseString.Replace("<Store>", _HubFulfiller)

            Case Else

                Return String.Empty

        End Select

    End Function

#End Region

End Class