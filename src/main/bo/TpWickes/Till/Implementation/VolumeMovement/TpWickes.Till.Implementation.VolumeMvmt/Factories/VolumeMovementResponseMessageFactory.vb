﻿Friend Class VolumeMovementResponseMessageFactory

    Private Shared _mFactoryMember As IVolumeMovementResponseMessage

    Public Shared Function FactoryGet() As IVolumeMovementResponseMessage

        If _mFactoryMember Is Nothing Then

            Return New VolumeMovementResponseMessage

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IVolumeMovementResponseMessage)

        _mFactoryMember = obj

    End Sub

End Class
