﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class WebService
    Implements IVolumeMovement

#Region "Interface"

    Public Function OMCheckFulfilment(ByVal Order As IOrderHeader) As String Implements IVolumeMovement.OMCheckFulfilment

        Dim OrderManager As IBaseService
        Dim ValidateRequest As IValidateXml
        Dim ValidateResponse As IValidateXml

        Dim RequestBO As OMCheckFulfilmentRequest
        Dim ResponseBO As OMCheckFulfilmentResponse

        Dim RequestXml As String
        Dim ResponseXml As String

        Dim ResponseMessage As IVolumeMovementResponseMessage

        Trace.WriteLine("Fulfillment Information: Start")

        ValidateRequest = ValidateXmlFactory.FactoryGet
        ValidateResponse = ValidateXmlFactory.FactoryGet
        ResponseMessage = VolumeMovementResponseMessageFactory.FactoryGet

        Try

            If Order IsNot Nothing Then Order.SellingStoreCode = StoreCode8000Formating(Order.SellingStoreCode)

            'configure
            OrderManager = CreateOrderManager()
            RequestBO = CreateAndConfigureRequest(OrderManager, Order)
            RequestXml = RequestBO.Serialise

            'request validation
            Trace.WriteLine("Fulfillment Information: Validating Request Against XSD - Start")
            If ValidateRequest.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Input, RequestXml, Nothing) = False Then

                Trace.WriteLine("Fulfillment Information: Validating Request Against XSD - Failed " & RequestXml)
                Exit Try

            End If
            Trace.WriteLine("Fulfillment Information: Validating Request Against XSD - End")

            'ask & get response order manager

            Trace.WriteLine("Fulfillment Information: Sending Request - " & Now.ToLocalTime)
            ResponseXml = AskOrderManager(OrderManager, RequestBO.Serialise)
            Trace.WriteLine("Fulfillment Information: Received Response - " & Now.ToLocalTime)

            Trace.WriteLine("Fulfillment Information: Response XML - " & ResponseXml)
            If ResponseXml IsNot Nothing And ResponseXml.Length > 0 Then

                'response validation
                Trace.WriteLine("Fulfillment Information: Validating Response Against XSD - Start")
                If ValidateResponse.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Ouptut, ResponseXml, Nothing) = False Then

                    Trace.WriteLine("Fulfillment Information: Validating Response Against XSD - Failed")
                    Exit Try

                End If
                Trace.WriteLine("Fulfillment Information: Validating Response Against XSD - End")

                'generate return message
                Trace.WriteLine("Fulfillment Information: Generate Return Message - Start")

                ResponseBO = ConvertResponseXmlIntoBusinessObject(ResponseXml)

                ConfigureResponseMessage(ResponseMessage, _
                                         ResponseBO.EstoreFulfiller, _
                                         ResponseBO.HubFulfiller, ResponseBO.SuccessFlag)

                Trace.WriteLine("Fulfillment Information: Generate Return Message - End")

            End If

        Catch ex As Exception

            Trace.WriteLine("Fulfillment Information: Error - " & ex.Message)

        End Try

        OMCheckFulfilment = ResponseMessage.ResponseMessage
        Trace.WriteLine("Fulfillment Information: Return Message - " & ResponseMessage.ResponseMessage)
        Trace.WriteLine("Fulfillment Information: End")

    End Function

#End Region

#Region "Private Procedures & Functions"

    Private Function CreateOrderManager() As IBaseService

        Return BaseServiceFactory.FactoryGet

    End Function

    Private Function CreateAndConfigureRequest(ByRef OrderManager As IBaseService, ByRef Order As IOrderHeader) As OMCheckFulfilmentRequest

        Dim Request As OMCheckFulfilmentRequest

        Request = CType(OrderManager.GetRequest, OMCheckFulfilmentRequest)
        Request.SetFields(Order)

        Return Request

    End Function

    Private Function AskOrderManager(ByRef OrderManager As IBaseService, ByVal XmlDocument As String) As String

        Return OrderManager.GetResponseXml(XmlDocument)

    End Function

    'Private Function OrderManagerRequest(ByVal Order As IOrderHeader) As String

    '    Dim OrderManager As IBaseService
    '    Dim Request As OMCheckFulfilmentRequest

    '    OrderManager = BaseServiceFactory.FactoryGet

    '    Request = CType(OrderManager.GetRequest, OMCheckFulfilmentRequest)
    '    Request.SetFields(Order)

    '    OrderManagerRequest = OrderManager.GetResponseXml(Request.Serialise)

    'End Function

    Private Function ConvertResponseXmlIntoBusinessObject(ByVal ResponseXML As String) As OMCheckFulfilmentResponse

        Dim OrderManager As BaseService
        Dim Response As OMCheckFulfilmentResponse

        OrderManager = New CheckFulfimentService
        Response = CType(OrderManager.GetResponse, OMCheckFulfilmentResponse)
        Response = CType(Response.Deserialise(ResponseXML), OMCheckFulfilmentResponse)

        ConvertResponseXmlIntoBusinessObject = Response

    End Function

    Private Sub ConfigureResponseMessage(ByRef ResponseMessageToCaller As IVolumeMovementResponseMessage, ByVal EstoreFulfiller As String, ByVal HubFulfiller As String, ByVal SuccessFlag As Boolean)

        ResponseMessageToCaller.EstoreFulfiller = EstoreFulfiller
        ResponseMessageToCaller.HubFulfiller = HubFulfiller
        ResponseMessageToCaller.SuccessFlag = SuccessFlag

    End Sub

    Friend Function StoreCode8000Formating(ByVal StoreCode As Integer) As Integer

        If StoreCode < 8000 Then

            Return 8000 + StoreCode

        Else

            Return StoreCode

        End If

    End Function

#End Region

End Class