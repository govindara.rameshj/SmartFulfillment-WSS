﻿Public Class OfflineMode
    Implements IOfflineMode


    Public Sub SetOfflineMode(ByVal InOfflineMode As Boolean) Implements InterOp.Interface.OfflineMode.IOfflineMode.SetOfflineMode

        Try
            Dim Repository As IOfflineModeRepository = OfflineModeRepositoryFactory.FactoryGet

            Repository.SetOfflineMode(InOfflineMode)
        Catch ex As Exception

        End Try
    End Sub
End Class
