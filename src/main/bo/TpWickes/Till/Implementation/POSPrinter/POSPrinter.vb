﻿Imports System.Text

Public Class POSPrinter
    Implements IPOSPrinter

#Region "Constructors"

    Public Sub New()

    End Sub
#End Region

    Public Function GetBlankLine(Optional ByVal NumberOfLines As Integer = 1) As String Implements IPOSPrinter.GetBlankLine
        Dim BlankLine As New StringBuilder

        With BlankLine
            For line As Integer = 1 To NumberOfLines
                .Append(_blankLine)
            Next
            GetBlankLine = .ToString
        End With
    End Function

    Public Function GetDivideLine() As String Implements IPOSPrinter.GetDivideLine

        GetDivideLine = DivideLine
    End Function

    Public Function GetFiscalPrinterStationReceipt() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetFiscalPrinterStationReceipt

        GetFiscalPrinterStationReceipt = FptrPsFiscalReceipt
    End Function

    Public Function GetLineEnd() As String Implements IPOSPrinter.GetLineEnd
        Dim lineEnd As New StringBuilder

        With lineEnd
            .Append(NewLine)
            .Append(EscapePrefix)
            .Append(EscapePrintSizeNormal)
            GetLineEnd = .ToString
        End With
    End Function

    Public Function GetLineLengthBig() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetLineLengthBig

        GetLineLengthBig = 22
    End Function

    Public Function GetLineLengthCompressed() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetLineLengthCompressed

        GetLineLengthCompressed = 56
    End Function

    Public Function GetLineLengthHuge() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetLineLengthHuge

        GetLineLengthHuge = 20
    End Function

    Public Function GetLineLengthNormal() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetLineLengthNormal

        GetLineLengthNormal = 44
    End Function

    Public Function GetLineStartBig() As String Implements IPOSPrinter.GetLineStartBig
        Dim lineStart As New StringBuilder

        With lineStart
            .Append(EscapePrefix)
            .Append(EscapePrintSizeBig)
            GetLineStartBig = .ToString()
        End With
    End Function

    Public Function GetLineStartCompressed() As String Implements IPOSPrinter.GetLineStartCompressed
        Dim lineStart As New StringBuilder

        With lineStart
            .Append(EscapePrefix)
            .Append(EscapePrintSizeCompressed)
            GetLineStartCompressed = .ToString()
        End With
    End Function

    Public Function GetLineStartHuge() As String Implements IPOSPrinter.GetLineStartHuge
        Dim lineStart As New StringBuilder

        With lineStart
            .Append(EscapePrefix)
            .Append(EscapePrintSizeHuge)
            GetLineStartHuge = .ToString()
        End With
    End Function

    Public Function GetLineStartNormal() As String Implements IPOSPrinter.GetLineStartNormal
        Dim lineStart As New StringBuilder

        With lineStart
            .Append(EscapePrefix)
            .Append(EscapePrintSizeNormal)
            GetLineStartNormal = .ToString()
        End With
    End Function

    Public Function GetNewLine() As String Implements InterOp.Interface.Printer.IPOSPrinter.GetNewLine

        GetNewLine = NewLine
    End Function

    Public Function GetPrintablePaperCut() As String Implements IPOSPrinter.GetPrintablePaperCut

        GetPrintablePaperCut = GetPaperCut()
    End Function

    Public Function GetPrintableLogo() As String Implements IPOSPrinter.GetPrintableLogo

        GetPrintableLogo = GetPrintLogo()
    End Function

    Public Function GetCurrencySymbol() As String Implements IPOSPrinter.GetCurrencySymbol

        GetCurrencySymbol = Chr(156)
    End Function

    Public Function GetPrintBarCodeAlignmentCentre() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetPrintBarCodeAlignmentCentre

        GetPrintBarCodeAlignmentCentre = PtrBcCenter
    End Function

    Public Function GetPrintBarCodeSymbologyCode39() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetPrintBarCodeSymbologyCode39

        GetPrintBarCodeSymbologyCode39 = PtrBcsCode39
    End Function

    Public Function GetPrintBarCodeTextPositionBelow() As Integer Implements InterOp.Interface.Printer.IPOSPrinter.GetPrintBarCodeTextPositionBelow

        GetPrintBarCodeTextPositionBelow = PtrBcTextBelow
    End Function

#Region "POS Printer Methods"


    Friend Function GetLineFeed(Optional ByVal NumberOfLines As Integer = 1) As String
        Dim LineFeed As New StringBuilder

        With LineFeed
            .Append(EscapePrefix)
            .Append(EscapeLineFeed(NumberOfLines))
            GetLineFeed = .ToString
        End With
    End Function

    Friend Function GetPaperCut() As String
        Dim PaperCut As New StringBuilder

        With PaperCut
            .Append(EscapePrefix)
            .Append(EscapePaperCut)
            GetPaperCut = .ToString
        End With
    End Function

    Friend Function GetPrintLogo() As String
        Dim PrintLogo As New StringBuilder

        With PrintLogo
            .Append(EscapePrefix)
            .Append(EscapePrintLogo)
            GetPrintLogo = .ToString
        End With
    End Function
#End Region

#Region "POS Printer Properties"
    Private _blankLine As String = " " & vbNewLine
    Private _divideLine As String = New String(Chr(205), 40)
    Private _escapeLineFeed As String = "lF"
    Private _escapePaperCut As String = Chr(105).ToString
    Private _escapePrefix As String = Chr(27).ToString
    Private _escapePrintBold As String = "|bC"
    Private _escapePrintLogo As String = "|1B"
    Private _escapePrintSizeBig As String = "|2C"
    Private _escapePrintSizeCompressed As String = "!" & Chr(1)
    Private _escapePrintSizeHuge As String = "|4C"
    Private _escapePrintSizeNormal As String = "|N"
    Private _escapePrintUnderline As String = "|uC"
    Private _lineLengthBig As Integer = 22
    Private _lineLengthCompressed As Integer = 56
    Private _lineLengthHuge As Integer = 20
    Private _lineLengthNormal As Integer = 44
    Private _newLine As String = vbNewLine

    Friend Property DivideLine() As String
        Get
            DivideLine = _divideLine
        End Get
        Set(ByVal value As String)
            _divideLine = value
        End Set
    End Property

    Friend Property EscapeLineFeed(Optional ByVal NumberOfLines As Integer = 1) As String
        Get
            EscapeLineFeed = "|" & NumberOfLines.ToString.Trim() & _escapeLineFeed
        End Get
        Set(ByVal value As String)
            _escapeLineFeed = value
        End Set
    End Property

    Friend Property EscapePaperCut() As String
        Get
            EscapePaperCut = _escapePaperCut
        End Get
        Set(ByVal value As String)
            _escapePaperCut = value
        End Set
    End Property

    Friend Property EscapePrefix() As String
        Get
            EscapePrefix = _escapePrefix
        End Get
        Set(ByVal value As String)
            _escapePrefix = value
        End Set
    End Property

    Friend Property EscapePrintBold() As String
        Get
            EscapePrintBold = _escapePrintBold
        End Get
        Set(ByVal value As String)
            _escapePrintBold = value
        End Set
    End Property

    Friend Property EscapePrintLogo() As String
        Get
            EscapePrintLogo = _escapePrintLogo
        End Get
        Set(ByVal value As String)
            _escapePrintLogo = value
        End Set
    End Property

    Friend Property EscapePrintSizeBig() As String
        Get
            EscapePrintSizeBig = _escapePrintSizeBig
        End Get
        Set(ByVal value As String)
            _escapePrintSizeBig = value
        End Set
    End Property

    Friend Property EscapePrintSizeCompressed() As String
        Get
            EscapePrintSizeCompressed = _escapePrintSizeCompressed
        End Get
        Set(ByVal value As String)
            _escapePrintSizeCompressed = value
        End Set
    End Property

    Friend Property EscapePrintSizeHuge() As String
        Get
            EscapePrintSizeHuge = _escapePrintSizeHuge
        End Get
        Set(ByVal value As String)
            _escapePrintSizeHuge = value
        End Set
    End Property

    Friend Property EscapePrintSizeNormal() As String
        Get
            EscapePrintSizeNormal = _escapePrintSizeNormal
        End Get
        Set(ByVal value As String)
            _escapePrintSizeNormal = value
        End Set
    End Property

    Friend Property EscapePrintUnderline() As String
        Get
            EscapePrintUnderline = _escapePrintUnderline
        End Get
        Set(ByVal value As String)
            _escapePrintUnderline = value
        End Set
    End Property

    Friend Property LineLengthBig() As Integer
        Get
            LineLengthBig = _lineLengthBig
        End Get
        Set(ByVal value As Integer)
            _lineLengthBig = value
        End Set
    End Property

    Friend Property LineLengthCompressed() As Integer
        Get
            LineLengthCompressed = _lineLengthCompressed
        End Get
        Set(ByVal value As Integer)
            _lineLengthCompressed = value
        End Set
    End Property

    Friend Property LineLengthHuge() As Integer
        Get
            LineLengthHuge = _lineLengthHuge
        End Get
        Set(ByVal value As Integer)
            _lineLengthHuge = value
        End Set
    End Property

    Friend Property LineLengthNormal() As Integer
        Get
            LineLengthNormal = _lineLengthNormal
        End Get
        Set(ByVal value As Integer)
            _lineLengthNormal = value
        End Set
    End Property

    Friend Property NewLine() As String
        Get
            NewLine = _newLine
        End Get
        Set(ByVal value As String)
            _newLine = value
        End Set
    End Property
#End Region
End Class
