﻿Module POSPrinterConstants
    REM *///////////////////////////////////////////////////////////////////
    REM *///////////////////////////////////////////////////////////////////
    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OPOSALL.BAS
    REM *
    REM *   Includes all OPOS Device Classes.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 96-03-18 OPOS Release 1.01                                    CRM
    REM * 96-04-22 OPOS Release 1.1                                     CRM
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM * 98-03-06 OPOS Release 1.3                                     CRM
    REM * 99-06-18 OPOS Release 1.4 and 1.5                             CRM
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM * 01-07-15 OPOS Release 1.6                                 THH/BKS
    REM *
    REM *///////////////////////////////////////////////////////////////////
    REM *///////////////////////////////////////////////////////////////////
    REM *///////////////////////////////////////////////////////////////////



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * Opos.h
    REM *
    REM *   General header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 97-06-04 OPOS Release 1.2               MtD                      CRM
    REM *   Add OPOS_FOREVER.
    REM *   Add BinaryConversion values.
    REM * 98-03-06 OPOS Release 1.3                                     CRM
    REM *   Add CapPowerReporting, PowerState, and PowerNotify values.
    REM *   Add power reporting values for StatusUpdateEvent.
    REM * 00-09-24 OPOS Release 1.5                                     CRM
    REM *   Add OpenResult status values.
    REM * 00-07-18 OPOS Release 1.5                                     BKS
    REM *   Add many values and Point Card Reader Writer and
    REM *   POS Power sections.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * OPOS "State" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposSClosed As Integer = 1
    Public Const OposSIdle As Integer = 2
    Public Const OposSBusy As Integer = 3
    Public Const OposSError As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * OPOS "ResultCode" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposSuccess As Integer = 0
    Public Const OposEClosed As Integer = 101
    Public Const OposEClaimed As Integer = 102
    Public Const OposENotclaimed As Integer = 103
    Public Const OposENoservice As Integer = 104
    Public Const OposEDisabled As Integer = 105
    Public Const OposEIllegal As Integer = 106
    Public Const OposENohardware As Integer = 107
    Public Const OposEOffline As Integer = 108
    Public Const OposENoexist As Integer = 109
    Public Const OposEExists As Integer = 110
    Public Const OposEFailure As Integer = 111
    Public Const OposETimeout As Integer = 112
    Public Const OposEBusy As Integer = 113
    Public Const OposEExtended As Integer = 114


    REM *///////////////////////////////////////////////////////////////////
    REM * OPOS "OpenResult" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const Oposopenerr As Integer = 300

    Public Const OposOrAlreadyopen As Integer = 301
    Public Const OposOrRegbadname As Integer = 302
    Public Const OposOrRegprogid As Integer = 303
    Public Const OposOrCreate As Integer = 304
    Public Const OposOrBadif As Integer = 305
    Public Const OposOrFailedopen As Integer = 306
    Public Const OposOrBadversion As Integer = 307

    Public Const Oposopenerrso As Integer = 400

    Public Const OposOrsNoport = 401
    Public Const OposOrsNotsupported = 402
    Public Const OposOrsConfig = 403
    Public Const OposOrsSpecific = 450


    REM *///////////////////////////////////////////////////////////////////
    REM * OPOS "BinaryConversion" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposBcNone As Integer = 0
    Public Const OposBcNibble As Integer = 1
    Public Const OposBcDecimal As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "CheckHealth" Method: "Level" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposChInternal As Integer = 1
    Public Const OposChExternal As Integer = 2
    Public Const OposChInteractive As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * OPOS "CapPowerReporting", "PowerState", "PowerNotify" Property
    REM *   Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposPrNone As Integer = 0
    Public Const OposPrStandard As Integer = 1
    Public Const OposPrAdvanced As Integer = 2

    Public Const OposPnDisabled As Integer = 0
    Public Const OposPnEnabled As Integer = 1

    Public Const OposPsUnknown As Integer = 2000
    Public Const OposPsOnline As Integer = 2001
    Public Const OposPsOff As Integer = 2002
    Public Const OposPsOffline As Integer = 2003
    Public Const OposPsOffOffline As Integer = 2004


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorEvent" Event: "ErrorLocus" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposElOutput As Integer = 1
    Public Const OposElInput As Integer = 2
    Public Const OposElInputData As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorEvent" Event: "ErrorResponse" Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposErRetry As Integer = 11
    Public Const OposErClear As Integer = 12
    Public Const OposErContinueinput As Integer = 13


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" Event: Common "Status" Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposSuePowerOnline As Integer = 2001
    Public Const OposSuePowerOff As Integer = 2002
    Public Const OposSuePowerOffline As Integer = 2003
    Public Const OposSuePowerOffOffline As Integer = 2004


    REM *///////////////////////////////////////////////////////////////////
    REM * General Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposForever As Integer = -1



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposBb.h
    REM *
    REM *   Bump Bar header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 98-03-06 OPOS Release 1.3                                     BB
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CurrentUnitID" and "UnitsOnline" Properties
    REM *  and "Units" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const BbUid1 As Integer = &H1
    Public Const BbUid2 As Integer = &H2
    Public Const BbUid3 As Integer = &H4
    Public Const BbUid4 As Integer = &H8
    Public Const BbUid5 As Integer = &H10
    Public Const BbUid6 As Integer = &H20
    Public Const BbUid7 As Integer = &H40
    Public Const BbUid8 As Integer = &H80
    Public Const BbUid9 As Integer = &H100
    Public Const BbUid10 As Integer = &H200
    Public Const BbUid11 As Integer = &H400
    Public Const BbUid12 As Integer = &H800
    Public Const BbUid13 As Integer = &H1000
    Public Const BbUid14 As Integer = &H2000
    Public Const BbUid15 As Integer = &H4000
    Public Const BbUid16 As Integer = &H8000
    Public Const BbUid17 As Integer = &H10000
    Public Const BbUid18 As Integer = &H20000
    Public Const BbUid19 As Integer = &H40000
    Public Const BbUid20 As Integer = &H80000
    Public Const BbUid21 As Integer = &H100000
    Public Const BbUid22 As Integer = &H200000
    Public Const BbUid23 As Integer = &H400000
    Public Const BbUid24 As Integer = &H800000
    Public Const BbUid25 As Integer = &H1000000
    Public Const BbUid26 As Integer = &H2000000
    Public Const BbUid27 As Integer = &H4000000
    Public Const BbUid28 As Integer = &H8000000
    Public Const BbUid29 As Integer = &H10000000
    Public Const BbUid30 As Integer = &H20000000
    Public Const BbUid31 As Integer = &H40000000
    Public Const BbUid32 As Integer = &H80000000


    REM *///////////////////////////////////////////////////////////////////
    REM * "DataEvent" Event: "Status" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const BbDeKey As Integer = &H1



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposCash.h
    REM *
    REM *   Cash Drawer header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 98-03-06 OPOS Release 1.3                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" Event Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CashSueDrawerclosed As Integer = 0
    Public Const CashSueDraweropen As Integer = 1



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposCAT.h
    REM *
    REM *   CAT header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 98-09-23 OPOS Release 1.4                                 OPOS-J
    REM * 00-09-24 OPOS Release 1.5                                    BKS
    REM *   Add CAT_PAYMENT_DEBIT, CAT_MEDIA_UNSPECIFIED,
    REM *   CAT_MEDIA_NONDEFINE, CAT_MEDIA_CREDIT, CAT_MEDIA_DEBIT
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * Payment Condition Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CatPaymentLump As Integer = 10
    Public Const CatPaymentBonus1 As Integer = 21
    Public Const CatPaymentBonus2 As Integer = 22
    Public Const CatPaymentBonus3 As Integer = 23
    Public Const CatPaymentBonus4 As Integer = 24
    Public Const CatPaymentBonus5 As Integer = 25
    Public Const CatPaymentInstallment1 As Integer = 61
    Public Const CatPaymentInstallment2 As Integer = 62
    Public Const CatPaymentInstallment3 As Integer = 63
    Public Const CatPaymentBonusCombination1 As Integer = 31
    Public Const CatPaymentBonusCombination2 As Integer = 32
    Public Const CatPaymentBonusCombination3 As Integer = 33
    Public Const CatPaymentBonusCombination4 As Integer = 34
    Public Const CatPaymentRevolving As Integer = 80
    Public Const CatPaymentDebit As Integer = 110


    REM *///////////////////////////////////////////////////////////////////
    REM * Transaction Type Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CatTransactionSales As Integer = 10
    Public Const CatTransactionVoid As Integer = 20
    Public Const CatTransactionRefund As Integer = 21
    Public Const CatTransactionVoidpresales As Integer = 29
    Public Const CatTransactionCompletion As Integer = 30
    Public Const CatTransactionPresales As Integer = 40
    Public Const CatTransactionCheckcard As Integer = 41


    REM *///////////////////////////////////////////////////////////////////
    REM * "PaymentMedia" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CatMediaUnspecified As Integer = 0
    Public Const CatMediaNondefine As Integer = 0
    Public Const CatMediaCredit As Integer = 1
    Public Const CatMediaDebit As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * ResultCodeExtended Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEcatCentererror As Integer = 1
    Public Const OposEcatCommanderror As Integer = 90
    Public Const OposEcatReset As Integer = 91
    Public Const OposEcatCommunicationerror As Integer = 92
    Public Const OposEcatDailylogoverflow As Integer = 200


    REM *///////////////////////////////////////////////////////////////////
    REM * "Daily Log" Property  & Argument Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CatDlNone As Integer = 0                                 'None of them
    Public Const CatDlReporting As Integer = 1                            'Only Reporting
    Public Const CatDlSettlement As Integer = 2                           'Only Settlement
    Public Const CatDlReportingSettlement As Integer = 3                  'Both of them



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposChan.h
    REM *
    REM *   Cash Changer header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM * 00-09-24 OPOS Release 1.5                                  OPOS-J
    REM *   Add DepositStatus Constants.
    REM *   Add EndDeposit Constants.
    REM *   Add PauseDeposit Constants.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "DeviceStatus" and "FullStatus" Property Constants
    REM * "StatusUpdateEvent" Event Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const ChanStatusOk As Integer = 0                         ' DeviceStatus, FullStatus

    Public Const ChanStatusEmpty As Integer = 11                     ' DeviceStatus, StatusUpdateEvent
    Public Const ChanStatusNearempty As Integer = 12                 ' DeviceStatus, StatusUpdateEvent
    Public Const ChanStatusEmptyok As Integer = 13                   ' StatusUpdateEvent

    Public Const ChanStatusFull As Integer = 21                      ' FullStatus, StatusUpdateEvent
    Public Const ChanStatusNearfull As Integer = 22                  ' FullStatus, StatusUpdateEvent
    Public Const ChanStatusFullok As Integer = 23                    ' StatusUpdateEvent

    Public Const ChanStatusJam As Integer = 31                       ' DeviceStatus, StatusUpdateEvent
    Public Const ChanStatusJamok As Integer = 32                     ' StatusUpdateEvent

    Public Const ChanStatusAsync As Integer = 91                     ' StatusUpdateEvent


    REM *///////////////////////////////////////////////////////////////////
    REM * "DepositStatus" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const ChanStatusDepositStart As Integer = 1
    Public Const ChanStatusDepositEnd As Integer = 2
    Public Const ChanStatusDepositNone As Integer = 3
    Public Const ChanStatusDepositCount As Integer = 4
    Public Const ChanStatusDepositJam As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "EndDeposit" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const ChanDepositChange As Integer = 1
    Public Const ChanDepositNochange As Integer = 2
    Public Const ChanDepositrepay As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "PauseDeposit" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const ChanDepositPause As Integer = 1
    Public Const ChanDepositRestart As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Cash Changer
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEchanOverdispense As Integer = 201



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposCoin.h
    REM *
    REM *   Coin Dispenser header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "DispenserStatus" Property Constants
    REM * "StatusUpdateEvent" Event: "Data" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const CoinStatusOk As Integer = 1
    Public Const CoinStatusEmpty As Integer = 2
    Public Const CoinStatusNearempty As Integer = 3
    Public Const CoinStatusJam As Integer = 4



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposDisp.h
    REM *
    REM *   Line Display header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 96-03-18 OPOS Release 1.01                                    CRM
    REM *   Add DISP_MT_INIT constant and MarqueeFormat constants.
    REM * 96-04-22 OPOS Release 1.1                                     CRM
    REM *   Add CapCharacterSet values for Kana and Kanji.
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *   Add CapCharacterSet and CharacterSet constants for UNICODE
    REM * 01-07-15 OPOS Release 1.6                                     BKS
    REM *   Add CapCursorType, CapCustomGlyph, CapReadBack, CapReverse,
    REM *     CursorType property constants.
    REM *   Add DefineGlyph, DisplayText and DisplayTextAt parameter
    REM *     constants.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapBlink" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCbNoblink As Integer = 0
    Public Const DispCbBlinkall As Integer = 1
    Public Const DispCbBlinkeach As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapCharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCcsNumeric As Integer = 0
    Public Const DispCcsAlpha As Integer = 1
    Public Const DispCcsAscii As Integer = 998
    Public Const DispCcsKana As Integer = 10
    Public Const DispCcsKanji As Integer = 11
    Public Const DispCcsUnicode As Integer = 997


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapCursorType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCctNone As Integer = &H0
    Public Const DispCctFixed As Integer = &H1
    Public Const DispCctBlock As Integer = &H2
    Public Const DispCctHalfblock As Integer = &H4
    Public Const DispCctUnderline As Integer = &H8
    Public Const DispCctReverse As Integer = &H10
    Public Const DispCctOther As Integer = &H20


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapReadBack" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCrbNone As Integer = &H0
    Public Const DispCrbSingle As Integer = &H1


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapReverse" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCrNone As Integer = &H0
    Public Const DispCrReverseall As Integer = &H1
    Public Const DispCrReverseeach As Integer = &H2


    REM *///////////////////////////////////////////////////////////////////
    REM * "CharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCsUnicode As Integer = 997
    Public Const DispCsAscii As Integer = 998
    Public Const DispCsWindows As Integer = 999


    REM *///////////////////////////////////////////////////////////////////
    REM * "CursorType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispCtNone As Integer = 0
    Public Const DispCtFixed As Integer = 1
    Public Const DispCtBlock As Integer = 2
    Public Const DispCtHalfblock As Integer = 3
    Public Const DispCtUnderline As Integer = 4
    Public Const DispCtReverse As Integer = 5
    Public Const DispCtOther As Integer = 6


    REM *///////////////////////////////////////////////////////////////////
    REM * "MarqueeType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispMtNone As Integer = 0
    Public Const DispMtUp As Integer = 1
    Public Const DispMtDown As Integer = 2
    Public Const DispMtLeft As Integer = 3
    Public Const DispMtRight As Integer = 4
    Public Const DispMtInit As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "MarqueeFormat" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispMfWalk As Integer = 0
    Public Const DispMfPlace As Integer = 1


    REM *///////////////////////////////////////////////////////////////////
    REM * "DefineGlyph" Method: "GlyphType" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispGtSingle As Integer = 1


    REM *///////////////////////////////////////////////////////////////////
    REM * "DisplayText" Method: "Attribute" Property Constants
    REM * "DisplayTextAt" Method: "Attribute" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispDtNormal As Integer = 0
    Public Const DispDtBlink As Integer = 1
    Public Const DispDtReverse As Integer = 2
    Public Const DispDtBlinkReverse As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "ScrollText" Method: "Direction" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispStUp As Integer = 1
    Public Const DispStDown As Integer = 2
    Public Const DispStLeft As Integer = 3
    Public Const DispStRight As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "SetDescriptor" Method: "Attribute" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const DispSdOff As Integer = 0
    Public Const DispSdOn As Integer = 1
    Public Const DispSdBlink As Integer = 2



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposFptr.h
    REM *
    REM *   Fiscal Printer header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 98-03-06 OPOS Release 1.3                                     PDU
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *   Change CountryCode constants and add code for Russia
    REM * 01-07-15 OPOS Release 1.6                                     THH
    REM *   Add values for all 1.6 added properties and method
    REM *   parameters
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "ActualCurrency" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrAcBrc As Integer = 1
    Public Const FptrAcBgl As Integer = 2
    Public Const FptrAcEur As Integer = 3
    Public Const FptrAcGrd As Integer = 4
    Public Const FptrAcHuf As Integer = 5
    Public Const FptrAcItl As Integer = 6
    Public Const FptrAcPlz As Integer = 7
    Public Const FptrAcRol As Integer = 8
    Public Const FptrAcRur As Integer = 9
    Public Const FptrAcTrl As Integer = 10


    REM *///////////////////////////////////////////////////////////////////
    REM * "ContractorId" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrCidFirst As Integer = 1
    Public Const FptrCidSecond As Integer = 2
    Public Const FptrCidSingle As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * Fiscal Printer Station Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrSJournal As Integer = 1
    Public Const FptrSReceipt As Integer = 2
    Public Const FptrSSlip As Integer = 4

    Public Const FptrSJournalReceipt As Integer = FptrSJournal Or FptrSReceipt


    REM *///////////////////////////////////////////////////////////////////
    REM * "CountryCode" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrCcBrazil As Integer = 1
    Public Const FptrCcGreece As Integer = 2
    Public Const FptrCcHungary As Integer = 4
    Public Const FptrCcItaly As Integer = 8
    Public Const FptrCcPoland As Integer = 16
    Public Const FptrCcTurkey As Integer = 32
    Public Const FptrCcRussia As Integer = 64
    Public Const FptrCcBulgaria As Integer = 128
    Public Const FptrCcRomania As Integer = 256


    REM *///////////////////////////////////////////////////////////////////
    REM * "DateType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrDtConf As Integer = 1
    Public Const FptrDtEod As Integer = 2
    Public Const FptrDtReset As Integer = 3
    Public Const FptrDtRtc As Integer = 4
    Public Const FptrDtVat As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorLevel" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrElNone As Integer = 1
    Public Const FptrElRecoverable As Integer = 2
    Public Const FptrElFatal As Integer = 3
    Public Const FptrElBlocked As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorState", "PrinterState" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrPsMonitor As Integer = 1
    Public Const FptrPsFiscalReceipt As Integer = 2
    Public Const FptrPsFiscalReceiptTotal As Integer = 3
    Public Const FptrPsFiscalReceiptEnding As Integer = 4
    Public Const FptrPsFiscalDocument As Integer = 5
    Public Const FptrPsFixedOutput As Integer = 6
    Public Const FptrPsItemList As Integer = 7
    Public Const FptrPsLocked As Integer = 8
    Public Const FptrPsNonfiscal As Integer = 9
    Public Const FptrPsReport As Integer = 10


    REM *///////////////////////////////////////////////////////////////////
    REM * "FiscalReceiptStation" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrRsReceipt As Integer = 1
    Public Const FptrRsSlip As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "FiscalReceiptType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrRtCashIn As Integer = 1
    Public Const FptrRtCashOut As Integer = 2
    Public Const FptrRtGeneric As Integer = 3
    Public Const FptrRtSales As Integer = 4
    Public Const FptrRtService As Integer = 5
    Public Const FptrRtSimpleInvoice As Integer = 6


    REM *///////////////////////////////////////////////////////////////////
    REM * "MessageType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrMtAdvance As Integer = 1
    Public Const FptrMtAdvancePaid As Integer = 2
    Public Const FptrMtAmountToBePaid As Integer = 3
    Public Const FptrMtAmountToBePaidBack As Integer = 4
    Public Const FptrMtCard As Integer = 5
    Public Const FptrMtCardNumber As Integer = 6
    Public Const FptrMtCardType As Integer = 7
    Public Const FptrMtCash As Integer = 8
    Public Const FptrMtCashier As Integer = 9
    Public Const FptrMtCashRegisterNumber As Integer = 10
    Public Const FptrMtChange As Integer = 11
    Public Const FptrMtCheque As Integer = 12
    Public Const FptrMtClientNumber As Integer = 13
    Public Const FptrMtClientSignature As Integer = 14
    Public Const FptrMtCounterState As Integer = 15
    Public Const FptrMtCreditCard As Integer = 16
    Public Const FptrMtCurrency As Integer = 17
    Public Const FptrMtCurrencyValue As Integer = 18
    Public Const FptrMtDeposit As Integer = 19
    Public Const FptrMtDepositReturned As Integer = 20
    Public Const FptrMtDotLine As Integer = 21
    Public Const FptrMtDriverNumb As Integer = 22
    Public Const FptrMtEmptyLine As Integer = 23
    Public Const FptrMtFreeText As Integer = 24
    Public Const FptrMtFreeTextWithDayLimit As Integer = 25
    Public Const FptrMtGivenDiscount As Integer = 26
    Public Const FptrMtLocalCredit As Integer = 27
    Public Const FptrMtMileageKm As Integer = 28
    Public Const FptrMtNote As Integer = 29
    Public Const FptrMtPaid As Integer = 30
    Public Const FptrMtPayIn As Integer = 31
    Public Const FptrMtPointGranted As Integer = 32
    Public Const FptrMtPointsBonus As Integer = 33
    Public Const FptrMtPointsReceipt As Integer = 34
    Public Const FptrMtPointsTotal As Integer = 35
    Public Const FptrMtProfited As Integer = 36
    Public Const FptrMtRate As Integer = 37
    Public Const FptrMtRegisterNumb As Integer = 38
    Public Const FptrMtShiftNumber As Integer = 39
    Public Const FptrMtStateOfAnAccount As Integer = 40
    Public Const FptrMtSubscription As Integer = 41
    Public Const FptrMtTable As Integer = 42
    Public Const FptrMtThankYouForLoyalty As Integer = 43
    Public Const FptrMtTransactionNumb As Integer = 44
    Public Const FptrMtValidTo As Integer = 45
    Public Const FptrMtVoucher As Integer = 46
    Public Const FptrMtVoucherPaid As Integer = 47
    Public Const FptrMtVoucherValue As Integer = 48
    Public Const FptrMtWithDiscount As Integer = 49
    Public Const FptrMtWithoutUplift As Integer = 50


    REM *///////////////////////////////////////////////////////////////////
    REM * "SlipSelection" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrSsFullLength As Integer = 1
    Public Const FptrSsValidation As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "TotalizerType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrTtDocument As Integer = 1
    Public Const FptrTtDay As Integer = 2
    Public Const FptrTtReceipt As Integer = 3
    Public Const FptrTtGrand As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "GetData" Method Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrGdCurrentTotal As Integer = 1
    Public Const FptrGdDailyTotal As Integer = 2
    Public Const FptrGdReceiptNumber As Integer = 3
    Public Const FptrGdRefund As Integer = 4
    Public Const FptrGdNotPaid As Integer = 5
    Public Const FptrGdMidVoid As Integer = 6
    Public Const FptrGdZReport As Integer = 7
    Public Const FptrGdGrandTotal As Integer = 8
    Public Const FptrGdPrinterId As Integer = 9
    Public Const FptrGdFirmware As Integer = 10
    Public Const FptrGdRestart As Integer = 11
    Public Const FptrGdRefundVoid As Integer = 12
    Public Const FptrGdNumbConfigBlock As Integer = 13
    Public Const FptrGdNumbCurrencyBlock As Integer = 14
    Public Const FptrGdNumbHdrBlock As Integer = 15
    Public Const FptrGdNumbResetBlock As Integer = 16
    Public Const FptrGdNumbVatBlock As Integer = 17
    Public Const FptrGdFiscalDoc As Integer = 18
    Public Const FptrGdFiscalDocVoid As Integer = 19
    Public Const FptrGdFiscalRec As Integer = 20
    Public Const FptrGdFiscalRecVoid As Integer = 21
    Public Const FptrGdNonfiscalDoc As Integer = 22
    Public Const FptrGdNonfiscalDocVoid As Integer = 23
    Public Const FptrGdNonfiscalRec As Integer = 24
    Public Const FptrGdSimpInvoice As Integer = 25
    Public Const FptrGdTender As Integer = 26
    Public Const FptrGdLinecount As Integer = 27
    Public Const FptrGdDescriptionLength As Integer = 28

    Public Const FptrPdlCash As Integer = 1
    Public Const FptrPdlCheque As Integer = 2
    Public Const FptrPdlChitty As Integer = 3
    Public Const FptrPdlCoupon As Integer = 4
    Public Const FptrPdlCurrency As Integer = 5
    Public Const FptrPdlDrivenOff As Integer = 6
    Public Const FptrPdlEftImprinter As Integer = 7
    Public Const FptrPdlEftTerminal As Integer = 8
    Public Const FptrPdlTerminalImprinter As Integer = 9
    Public Const FptrPdlFreeGift As Integer = 10
    Public Const FptrPdlGiro As Integer = 11
    Public Const FptrPdlHome As Integer = 12
    Public Const FptrPdlImprinterWithIssuer As Integer = 13
    Public Const FptrPdlLocalAccount As Integer = 14
    Public Const FptrPdlLocalAccountCard As Integer = 15
    Public Const FptrPdlPayCard As Integer = 16
    Public Const FptrPdlPayCardManual As Integer = 17
    Public Const FptrPdlPrepay As Integer = 18
    Public Const FptrPdlPumpTest As Integer = 19
    Public Const FptrPdlShortCredit As Integer = 20
    Public Const FptrPdlStaff As Integer = 21
    Public Const FptrPdlVoucher As Integer = 22

    Public Const FptrLcItem As Integer = 1
    Public Const FptrLcItemVoid As Integer = 2
    Public Const FptrLcDiscount As Integer = 3
    Public Const FptrLcDiscountVoid As Integer = 4
    Public Const FptrLcSurcharge As Integer = 5
    Public Const FptrLcSurchargeVoid As Integer = 6
    Public Const FptrLcRefund As Integer = 7
    Public Const FptrLcRefundVoid As Integer = 8
    Public Const FptrLcSubtotalDiscount As Integer = 9
    Public Const FptrLcSubtotalDiscountVoid As Integer = 10
    Public Const FptrLcSubtotalSurcharge As Integer = 11
    Public Const FptrLcSubtotalSurchargeVoid As Integer = 12
    Public Const FptrLcComment As Integer = 13
    Public Const FptrLcSubtotal As Integer = 14
    Public Const FptrLcTotal As Integer = 15

    Public Const FptrDlItem As Integer = 1
    Public Const FptrDlItemAdjustment As Integer = 2
    Public Const FptrDlItemFuel As Integer = 3
    Public Const FptrDlItemFuelVoid As Integer = 4
    Public Const FptrDlNotPaid As Integer = 5
    Public Const FptrDlPackageAdjustment As Integer = 6
    Public Const FptrDlRefund As Integer = 7
    Public Const FptrDlRefundVoid As Integer = 8
    Public Const FptrDlSubtotalAdjustment As Integer = 9
    Public Const FptrDlTotal As Integer = 10
    Public Const FptrDlVoid As Integer = 11
    Public Const FptrDlVoidItem As Integer = 12


    REM *///////////////////////////////////////////////////////////////////
    REM * "GetTotalizer" Method Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrGtGross As Integer = 1
    Public Const FptrGtNet As Integer = 2
    Public Const FptrGtDiscount As Integer = 3
    Public Const FptrGtDiscountVoid As Integer = 4
    Public Const FptrGtItem As Integer = 5
    Public Const FptrGtItemVoid As Integer = 6
    Public Const FptrGtNotPaid As Integer = 7
    Public Const FptrGtRefund As Integer = 8
    Public Const FptrGtRefundVoid As Integer = 9
    Public Const FptrGtSubtotalDiscount As Integer = 10
    Public Const FptrGtSubtotalDiscountVoid As Integer = 11
    Public Const FptrGtSubtotalSurcharges As Integer = 12
    Public Const FptrGtSubtotalSurchargesVoid As Integer = 13
    Public Const FptrGtSurcharges As Integer = 14
    Public Const FptrGtSSurchargesVoid As Integer = 15
    Public Const FptrGtVat As Integer = 16
    Public Const FptrGtVatCategory As Integer = 17


    REM *///////////////////////////////////////////////////////////////////
    REM * "AdjustmentType" arguments in diverse methods
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrAtAmountDiscount As Integer = 1
    Public Const FptrAtAmountSurcharge As Integer = 2
    Public Const FptrAtPercentageDiscount As Integer = 3
    Public Const FptrAtPercentageSurcharge As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ReportType" argument in "PrintReport" method
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrRtOrdinal As Integer = 1
    Public Const FptrRtDate As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "NewCurrency" argument in "SetCurrency" method
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrScEuro As Integer = 1


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" Event: "Data" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const FptrSueCoverOpen As Integer = 11
    Public Const FptrSueCoverOk As Integer = 12

    Public Const FptrSueJrnEmpty As Integer = 21
    Public Const FptrSueJrnNearempty As Integer = 22
    Public Const FptrSueJrnPaperok As Integer = 23

    Public Const FptrSueRecEmpty As Integer = 24
    Public Const FptrSueRecNearempty As Integer = 25
    Public Const FptrSueRecPaperok As Integer = 26

    Public Const FptrSueSlpEmpty As Integer = 27
    Public Const FptrSueSlpNearempty As Integer = 28
    Public Const FptrSueSlpPaperok As Integer = 29

    Public Const FptrSueIdle As Integer = 1001


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Fiscal Printer
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEfptrCoverOpen As Integer = 201                ' (Several)
    Public Const OposEfptrJrnEmpty As Integer = 202                 ' (Several)
    Public Const OposEfptrRecEmpty As Integer = 203                 ' (Several)
    Public Const OposEfptrSlpEmpty As Integer = 204                 ' (Several)
    Public Const OposEfptrSlpForm As Integer = 205                  ' EndRemoval
    Public Const OposEfptrMissingDevices As Integer = 206           ' (Several)
    Public Const OposEfptrWrongState As Integer = 207               ' (Several)
    Public Const OposEfptrTechnicalAssistance As Integer = 208      ' (Several)
    Public Const OposEfptrClockError As Integer = 209               ' (Several)
    Public Const OposEfptrFiscalMemoryFull As Integer = 210         ' (Several)
    Public Const OposEfptrFiscalMemoryDisconnected As Integer = 211 ' (Several)
    Public Const OposEfptrFiscalTotalsError As Integer = 212        ' (Several)
    Public Const OposEfptrBadItemQuantity As Integer = 213          ' (Several)
    Public Const OposEfptrBadItemAmount As Integer = 214            ' (Several)
    Public Const OposEfptrBadItemDescription As Integer = 215       ' (Several)
    Public Const OposEfptrReceiptTotalOverflow As Integer = 216     ' (Several)
    Public Const OposEfptrBadVat As Integer = 217                   ' (Several)
    Public Const OposEfptrBadPrice As Integer = 218                 ' (Several)
    Public Const OposEfptrBadDate As Integer = 219                  ' (Several)
    Public Const OposEfptrNegativeTotal As Integer = 220            ' (Several)
    Public Const OposEfptrWordNotAllowed As Integer = 221           ' (Several)
    Public Const OposEfptrBadLength As Integer = 222                ' (Several)
    Public Const OposEfptrMissingSetCurrency As Integer = 223       ' (Several)




    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposKbd.h
    REM *
    REM *   POS Keyboard header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 96-04-22 OPOS Release 1.1                                     CRM
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM *   Add "EventTypes" and "POSKeyEventType" values.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "EventTypes" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const KbdEtDown As Integer = 1
    Public Const KbdEtDownUp As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "POSKeyEventType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const KbdKetKeydown As Integer = 1
    Public Const KbdKetKeyup As Integer = 2



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposLock.h
    REM *
    REM *   Keylock header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "KeyPosition" Property Constants
    REM * "WaitForKeylockChange" Method: "KeyPosition" Parameter
    REM * "StatusUpdateEvent" Event: "Data" Parameter
    REM *///////////////////////////////////////////////////////////////////

    Public Const LockKpAny As Integer = 0                            ' WaitForKeylockChange Only
    Public Const LockKpLock As Integer = 1
    Public Const LockKpNorm As Integer = 2
    Public Const LockKpSupr As Integer = 3



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposMicr.h
    REM *
    REM *   MICR header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CheckType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const MicrCtPersonal As Integer = 1
    Public Const MicrCtBusiness As Integer = 2
    Public Const MicrCtUnknown As Integer = 99


    REM *///////////////////////////////////////////////////////////////////
    REM * "CountryCode" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const MicrCcUsa As Integer = 1
    Public Const MicrCcCanada As Integer = 2
    Public Const MicrCcMexico As Integer = 3
    Public Const MicrCcUnknown As Integer = 99


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for MICR
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEmicrNocheck As Integer = 201         ' EndInsertion
    Public Const OposEmicrCheck As Integer = 202           ' EndRemoval



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposMsr.h
    REM *
    REM *   Magnetic Stripe Reader header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM *   Add ErrorReportingType values.
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *   Add constants relating to Track 4 Data
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "TracksToRead" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const MsrTr1 As Integer = 1
    Public Const MsrTr2 As Integer = 2
    Public Const MsrTr3 As Integer = 4
    Public Const MsrTr4 As Integer = 8

    Public Const MsrTr12 As Integer = MsrTr1 Or MsrTr2
    Public Const MsrTr13 As Integer = MsrTr1 Or MsrTr3
    Public Const MsrTr14 As Integer = MsrTr1 Or MsrTr4
    Public Const MsrTr23 As Integer = MsrTr2 Or MsrTr3
    Public Const MsrTr24 As Integer = MsrTr2 Or MsrTr4
    Public Const MsrTr34 As Integer = MsrTr3 Or MsrTr4

    Public Const MsrTr123 As Integer = MsrTr1 Or MsrTr2 Or MsrTr3
    Public Const MsrTr124 As Integer = MsrTr1 Or MsrTr2 Or MsrTr4
    Public Const MsrTr134 As Integer = MsrTr1 Or MsrTr3 Or MsrTr4
    Public Const MsrTr234 As Integer = MsrTr2 Or MsrTr3 Or MsrTr4

    Public Const MsrTr1234 As Integer = MsrTr1 Or MsrTr2 Or MsrTr3 Or MsrTr4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorReportingType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const MsrErtCard As Integer = 0
    Public Const MsrErtTrack As Integer = 1


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorEvent" Event: "ResultCodeExtended" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEmsrStart As Integer = 201
    Public Const OposEmsrEnd As Integer = 202
    Public Const OposEmsrParity As Integer = 203
    Public Const OposEmsrLrc As Integer = 204


    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposPcrw.H
    REM *
    REM *   Point Card Reader Writer header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapCharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwCcsAlpha As Integer = 1
    Public Const PcrwCcsAscii As Integer = 998
    Public Const PcrwCcsKana As Integer = 10
    Public Const PcrwCcsKanji As Integer = 11
    Public Const PcrwCcsUnicode As Integer = 997


    REM *///////////////////////////////////////////////////////////////////
    REM * "CardState" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwStateNocard As Integer = 1
    Public Const PcrwStateRemaining As Integer = 2
    Public Const PcrwStateInrw As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * CapTrackToRead and TrackToWrite Property constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwTrack1 As Integer = &H1
    Public Const PcrwTrack2 As Integer = &H2
    Public Const PcrwTrack3 As Integer = &H4
    Public Const PcrwTrack4 As Integer = &H8
    Public Const PcrwTrack5 As Integer = &H10
    Public Const PcrwTrack6 As Integer = &H20


    REM *///////////////////////////////////////////////////////////////////
    REM * "CharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwCsUnicode As Integer = 997
    Public Const PcrwCsAscii As Integer = 998
    Public Const PcrwCsWindows As Integer = 999


    REM *///////////////////////////////////////////////////////////////////
    REM * "MappingMode" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwMmDots As Integer = 1
    Public Const PcrwMmTwips As Integer = 2
    Public Const PcrwMmEnglish As Integer = 3
    Public Const PcrwMmMetric As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for PoinrCardR/W
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEpcrwRead As Integer = 201
    Public Const OposEpcrwWrite As Integer = 202
    Public Const OposEpcrwJam As Integer = 203
    Public Const OposEpcrwMotor As Integer = 204
    Public Const OposEpcrwCover As Integer = 205
    Public Const OposEpcrwPrinter As Integer = 206
    Public Const OposEpcrwRelease As Integer = 207
    Public Const OposEpcrwDisplay As Integer = 208
    Public Const OposEpcrwNocard As Integer = 209


    REM *///////////////////////////////////////////////////////////////////
    REM * Magnetic read/write status Property Constants for PoinrCardR/W
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEpcrwStart As Integer = 211
    Public Const OposEpcrwEnd As Integer = 212
    Public Const OposEpcrwParity As Integer = 213
    Public Const OposEpcrwEncode As Integer = 214
    Public Const OposEpcrwLrc As Integer = 215
    Public Const OposEpcrwVerify As Integer = 216


    REM *///////////////////////////////////////////////////////////////////
    REM * "RotatedPrint" Method: "Rotation" Parameter Constants
    REM * "RotateSpecial" Property Constants (PCRWRPNORMALASYNC not legal)
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwRpNormal As Integer = &H1
    Public Const PcrwRpNormalasync As Integer = &H2

    Public Const PcrwRpRight90 As Integer = &H101
    Public Const PcrwRpLeft90 As Integer = &H102
    Public Const PcrwRpRotate180 As Integer = &H103


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" "Status" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PcrwSueNocard As Integer = 1
    Public Const PcrwSueRemaining As Integer = 2
    Public Const PcrwSueInrw As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposPpad.h
    REM *
    REM *   PIN Pad header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 98-03-06 OPOS Release 1.3                                     JDB
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *   Add PpadDispNone for devices with no display
    REM *   Add OposEppadBadKey extended result code
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapDisplay" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadDispUnrestricted As Integer = 1
    Public Const PpadDispPinrestricted As Integer = 2
    Public Const PpadDispRestrictedList As Integer = 3
    Public Const PpadDispRestrictedOrder As Integer = 4
    Public Const PpadDispNone As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "AvailablePromptsList" and "Prompt" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadMsgEnterpin As Integer = 1
    Public Const PpadMsgPleasewait As Integer = 2
    Public Const PpadMsgEntervalidpin As Integer = 3
    Public Const PpadMsgRetriesexceeded As Integer = 4
    Public Const PpadMsgApproved As Integer = 5
    Public Const PpadMsgDeclined As Integer = 6
    Public Const PpadMsgCanceled As Integer = 7
    Public Const PpadMsgAmountok As Integer = 8
    Public Const PpadMsgNotready As Integer = 9
    Public Const PpadMsgIdle As Integer = 10
    Public Const PpadMsgSlideCard As Integer = 11
    Public Const PpadMsgInsertcard As Integer = 12
    Public Const PpadMsgSelectcardtype As Integer = 13


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapLanguage" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadLangNone As Integer = 1
    Public Const PpadLangOne As Integer = 2
    Public Const PpadLangPinrestricted As Integer = 3
    Public Const PpadLangUnrestricted As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "TransactionType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadTransDebit As Integer = 1
    Public Const PpadTransCredit As Integer = 2
    Public Const PpadTransInq As Integer = 3
    Public Const PpadTransReconcile As Integer = 4
    Public Const PpadTransAdmin As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "EndEFTTransaction" Method Completion Code Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadEftNormal As Integer = 1
    Public Const PpadEftAbnormal As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "DataEvent" Event Status Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PpadSuccess As Integer = 1
    Public Const PpadCancel As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEppadBadKey = 201


    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposPtr.h
    REM *
    REM *   POS Printer header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 96-04-22 OPOS Release 1.1                                     CRM
    REM *   Add CapCharacterSet values.
    REM *   Add ErrorLevel values.
    REM *   Add TransactionPrint Control values.
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM *   Remove PTR_RP_NORMAL_ASYNC.
    REM *   Add more barcode symbologies.
    REM * 98-03-06 OPOS Release 1.3                                     CRM
    REM *   Add more PrintTwoNormal constants.
    REM * 00-09-24 OPOS Release 1.5                                   EPSON
    REM *   Add CapRecMarkFeed values and MarkFeed constants.
    REM *   Add ChangePrintSide constants.
    REM *   Add StatusUpdateEvent constants.
    REM *   Add ResultCodeExtended values.
    REM *   Add CapXxxCartridgeSensor and XxxCartridgeState values.
    REM *   Add CartridgeNotify values.
    REM * 00-09-24 OPOS Release 1.5                                     BKS
    REM *   Add CapCharacterset and CharacterSet values for UNICODE.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * Printer Station Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrSJournal As Integer = 1
    Public Const PtrSReceipt As Integer = 2
    Public Const PtrSSlip As Integer = 4

    Public Const PtrSJournalReceipt As Integer = PtrSJournal Or PtrSReceipt
    Public Const PtrSJournalSlip As Integer = PtrSJournal Or PtrSSlip
    Public Const PtrSReceiptSlip As Integer = PtrSReceipt Or PtrSSlip

    Public Const PtrTwoReceiptJournal As Integer = &H8000& + PtrSJournalReceipt
    Public Const PtrTwoSlipJournal As Integer = &H8000& + PtrSJournalSlip
    Public Const PtrTwoSlipReceipt As Integer = &H8000& + PtrSReceiptSlip


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapCharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrCcsAlpha As Integer = 1
    Public Const PtrCcsAscii As Integer = 998
    Public Const PtrCcsKana As Integer = 10
    Public Const PtrCcsKanji As Integer = 11
    Public Const PtrCcsUnicode As Integer = 997


    REM *///////////////////////////////////////////////////////////////////
    REM * "CharacterSet" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrCsUnicode As Integer = 997
    Public Const PtrCsAscii As Integer = 998
    Public Const PtrCsWindows As Integer = 999


    REM *///////////////////////////////////////////////////////////////////
    REM * "ErrorLevel" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrElNone As Integer = 1
    Public Const PtrElRecoverable As Integer = 2
    Public Const PtrElFatal As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "MapMode" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrMmDots As Integer = 1
    Public Const PtrMmTwips As Integer = 2
    Public Const PtrMmEnglish As Integer = 3
    Public Const PtrMmMetric As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "CapXxxColor" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrColorPrimary = &H1
    Public Const PtrColorCustom1 = &H2
    Public Const PtrColorCustom2 = &H4
    Public Const PtrColorCustom3 = &H8
    Public Const PtrColorCustom4 = &H10
    Public Const PtrColorCustom5 = &H20
    Public Const PtrColorCustom6 = &H40
    Public Const PtrColorCyan = &H100
    Public Const PtrColorMagenta = &H200
    Public Const PtrColorYellow = &H400
    Public Const PtrColorFull = &H80000000

    REM *///////////////////////////////////////////////////////////////////
    REM * "CapXxxCartridgeSensor" and  "XxxCartridgeState" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrCartUnknown = &H10000000
    Public Const PtrCartOk = &H0
    Public Const PtrCartRemoved = &H1
    Public Const PtrCartEmpty = &H2
    Public Const PtrCartNearend = &H4
    Public Const PtrCartCleaning = &H8

    REM *///////////////////////////////////////////////////////////////////
    REM * "CartridgeNotify"  Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrCnDisabled = &H0
    Public Const PtrCnEnabled = &H1


    REM *///////////////////////////////////////////////////////////////////
    REM * "CutPaper" Method Constant
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrCpFullcut As Integer = 100


    REM *///////////////////////////////////////////////////////////////////
    REM * "PrintBarCode" Method Constants:
    REM *///////////////////////////////////////////////////////////////////

    REM *   "Alignment" Parameter
    REM *     Either the distance from the left-most print column to the start
    REM *     of the bar code, or one of the following:

    Public Const PtrBcLeft As Integer = -1
    Public Const PtrBcCenter As Integer = -2
    Public Const PtrBcRight As Integer = -3

    REM *   "TextPosition" Parameter

    Public Const PtrBcTextNone As Integer = -11
    Public Const PtrBcTextAbove As Integer = -12
    Public Const PtrBcTextBelow As Integer = -13

    REM *   "Symbology" Parameter:

    REM *     One dimensional symbologies
    Public Const PtrBcsUpca As Integer = 101                         ' Digits
    Public Const PtrBcsUpce As Integer = 102                         ' Digits
    Public Const PtrBcsJan8 As Integer = 103                         ' = EAN 8
    Public Const PtrBcsEan8 As Integer = 103                         ' = JAN 8 (added in 1.2)
    Public Const PtrBcsJan13 As Integer = 104                        ' = EAN 13
    Public Const PtrBcsEan13 As Integer = 104                        ' = JAN 13 (added in 1.2)
    Public Const PtrBcsTf As Integer = 105                           ' (Discrete 2 of 5) Digits
    Public Const PtrBcsItf As Integer = 106                          ' (Interleaved 2 of 5) Digits
    Public Const PtrBcsCodabar As Integer = 107                      ' Digits, -, $, :, /, ., +;
    '   4 start/stop characters
    '   (a, b, c, d)
    Public Const PtrBcsCode39 As Integer = 108                       ' Alpha, Digits, Space, -, .,
    '   $, /, +, %; start/stop (*)
    ' Also has Full ASCII feature
    Public Const PtrBcsCode93 As Integer = 109                       ' Same characters as Code 39
    Public Const PtrBcsCode128 As Integer = 110                      ' 128 data characters
    REM *        (The following were added in Release 1.2)
    Public Const PtrBcsUpcaS As Integer = 111                        ' UPC-A with supplemental
    '   barcode
    Public Const PtrBcsUpceS As Integer = 112                        ' UPC-E with supplemental
    '   barcode
    Public Const PtrBcsUpcd1 As Integer = 113                        ' UPC-D1
    Public Const PtrBcsUpcd2 As Integer = 114                        ' UPC-D2
    Public Const PtrBcsUpcd3 As Integer = 115                        ' UPC-D3
    Public Const PtrBcsUpcd4 As Integer = 116                        ' UPC-D4
    Public Const PtrBcsUpcd5 As Integer = 117                        ' UPC-D5
    Public Const PtrBcsEan8S As Integer = 118                        ' EAN 8 with supplemental
    '   barcode
    Public Const PtrBcsEan13S As Integer = 119                       ' EAN 13 with supplemental
    '   barcode
    Public Const PtrBcsEan128 As Integer = 120                       ' EAN 128
    Public Const PtrBcsOcra As Integer = 121                         ' OCR "A"
    Public Const PtrBcsOcrb As Integer = 122                         ' OCR "B"


    REM *     Two dimensional symbologies
    Public Const PtrBcsPdf417 As Integer = 201
    Public Const PtrBcsMaxicode As Integer = 202

    REM *     Start of Printer-Specific bar code symbologies
    Public Const PtrBcsOther As Integer = 501


    REM *///////////////////////////////////////////////////////////////////
    REM * "PrintBitmap" Method Constants:
    REM *///////////////////////////////////////////////////////////////////

    REM *   "Width" Parameter
    REM *     Either bitmap width or:

    Public Const PtrBmAsis As Integer = -11                          ' One pixel per printer dot

    REM *   "Alignment" Parameter
    REM *     Either the distance from the left-most print column to the start
    REM *     of the bitmap, or one of the following:

    Public Const PtrBmLeft As Integer = -1
    Public Const PtrBmCenter As Integer = -2
    Public Const PtrBmRight As Integer = -3


    REM *///////////////////////////////////////////////////////////////////
    REM * "RotatePrint" Method: "Rotation" Parameter Constants
    REM * "RotateSpecial" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrRpNormal As Integer = &H1
    Public Const PtrRpRight90 As Integer = &H101
    Public Const PtrRpLeft90 As Integer = &H102
    Public Const PtrRpRotate180 As Integer = &H103


    REM *///////////////////////////////////////////////////////////////////
    REM * "SetLogo" Method: "Location" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrLTop As Integer = 1
    Public Const PtrLBottom As Integer = 2


    REM *///////////////////////////////////////////////////////////////////
    REM * "TransactionPrint" Method: "Control" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrTpTransaction As Integer = 11
    Public Const PtrTpNormal As Integer = 12


    REM *///////////////////////////////////////////////////////////////////
    REM * "MarkFeed" Method: "Type" Parameter Constants
    REM * "CapRecMarkFeed" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrMfToTakeup = 1
    Public Const PtrMfToCutter = 2
    Public Const PtrMfToCurrentTof = 4
    Public Const PtrMfToNextTof = 8

    REM *///////////////////////////////////////////////////////////////////
    REM * "ChangePrintSide" Method: "Side" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrPsUnknown = 0
    Public Const PtrPsSide1 = 1
    Public Const PtrPsSide2 = 2
    Public Const PtrPsOpposite = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" Event: "Data" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PtrSueCoverOpen As Integer = 11
    Public Const PtrSueCoverOk As Integer = 12

    Public Const PtrSueJrnEmpty As Integer = 21
    Public Const PtrSueJrnNearempty As Integer = 22
    Public Const PtrSueJrnPaperok As Integer = 23

    Public Const PtrSueRecEmpty As Integer = 24
    Public Const PtrSueRecNearempty As Integer = 25
    Public Const PtrSueRecPaperok As Integer = 26

    Public Const PtrSueSlpEmpty As Integer = 27
    Public Const PtrSueSlpNearempty As Integer = 28
    Public Const PtrSueSlpPaperok As Integer = 29

    Public Const PtrSueJrnCartridgeEmpty = 41
    Public Const PtrSueJrnCartridgeNearempty = 42
    Public Const PtrSueJrnHeadCleaning = 43
    Public Const PtrSueJrnCartridgeOk = 44

    Public Const PtrSueRecCartridgeEmpty = 45
    Public Const PtrSueRecCartridgeNearempty = 46
    Public Const PtrSueRecHeadCleaning = 47
    Public Const PtrSueRecCartridgeOk = 48

    Public Const PtrSueSlpCartridgeEmpty = 49
    Public Const PtrSueSlpCartridgeNearempty = 50
    Public Const PtrSueSlpHeadCleaning = 51
    Public Const PtrSueSlpCartridgeOk = 52

    Public Const PtrSueIdle As Integer = 1001


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Printer
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEptrCoverOpen As Integer = 201          ' (Several)
    Public Const OposEptrJrnEmpty As Integer = 202           ' (Several)
    Public Const OposEptrRecEmpty As Integer = 203           ' (Several)
    Public Const OposEptrSlpEmpty As Integer = 204           ' (Several)
    Public Const OposEptrSlpForm As Integer = 205            ' EndRemoval
    Public Const OposEptrToobig As Integer = 206             ' PrintBitmap
    Public Const OposEptrBadformat As Integer = 207          ' PrintBitmap
    Public Const OposEptrJrnCartridgeRemoved = 208 ' (Several)
    Public Const OposEptrJrnCartridgeEmpty = 209   ' (Several)
    Public Const OposEptrJrnHeadCleaning = 210     ' (Several)
    Public Const OposEptrRecCartridgeRemoved = 211 ' (Several)
    Public Const OposEptrRecCartridgeEmpty = 212   ' (Several)
    Public Const OposEptrRecHeadCleaning = 213     ' (Several)
    Public Const OposEptrSlpCartridgeRemoved = 214 ' (Several)
    Public Const OposEptrSlpCartridgeEmpty = 215   ' (Several)
    Public Const OposEptrSlpHeadCleaning = 216     ' (Several)



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposPwr.h
    REM *
    REM *   POSPower header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 99-02-22 OPOS Release 1.x                                     AL
    REM * 99-09-13 OPOS Release 1.x                                     TH
    REM *            ACCU -> UPS, FAN_ALARM and HEAT_ALARM added
    REM * 99-12-06 OPOS Release 1.x                                     TH
    REM *            FAN_ALARM and HEAT_ALARM changed to FAN_STOPPED,
    REM *          FAN_RUNNING, TEMPERATURE_HIGH and TEMPERATURE_OK
    REM * 00-09-24 OPOS Release 1.5                                     TH
    REM *          SHUTDOWN added
    REM *
    REM *///////////////////////////////////////////////////////////////////

    REM *///////////////////////////////////////////////////////////////////
    REM * "UPSChargeState" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const PwrUpsFull = 1
    Public Const PwrUpsWarning = 2
    Public Const PwrUpsLow = 4
    Public Const PwrUpsCritical = 8


    REM *///////////////////////////////////////////////////////////////////
    REM * "StatusUpdateEvent" Event: "Status" Parameter
    REM *///////////////////////////////////////////////////////////////////

    Public Const PwrSueUpsFull = 11
    Public Const PwrSueUpsWarning = 12
    Public Const PwrSueUpsLow = 13
    Public Const PwrSueUpsCritical = 14
    Public Const PwrSueFanStopped = 15
    Public Const PwrSueFanRunning = 16
    Public Const PwrSueTemperatureHigh = 17
    Public Const PwrSueTemperatureOk = 18
    Public Const PwrSueShutdown = 19



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposRod.h
    REM *
    REM *   Remote Order Display header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 98-03-06 OPOS Release 1.3                                     BB
    REM * 00-09-24 OPOS Release 1.5                                    BKS
    REM *   Added CharacterSet value for UNICODE.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "CurrentUnitID" and "UnitsOnline" Properties
    REM *  and "Units" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodUid1 As Integer = &H1
    Public Const RodUid2 As Integer = &H2
    Public Const RodUid3 As Integer = &H4
    Public Const RodUid4 As Integer = &H8
    Public Const RodUid5 As Integer = &H10
    Public Const RodUid6 As Integer = &H20
    Public Const RodUid7 As Integer = &H40
    Public Const RodUid8 As Integer = &H80
    Public Const RodUid9 As Integer = &H100
    Public Const RodUid10 As Integer = &H200
    Public Const RodUid11 As Integer = &H400
    Public Const RodUid12 As Integer = &H800
    Public Const RodUid13 As Integer = &H1000
    Public Const RodUid14 As Integer = &H2000
    Public Const RodUid15 As Integer = &H4000
    Public Const RodUid16 As Integer = &H8000
    Public Const RodUid17 As Integer = &H10000
    Public Const RodUid18 As Integer = &H20000
    Public Const RodUid19 As Integer = &H40000
    Public Const RodUid20 As Integer = &H80000
    Public Const RodUid21 As Integer = &H100000
    Public Const RodUid22 As Integer = &H200000
    Public Const RodUid23 As Integer = &H400000
    Public Const RodUid24 As Integer = &H800000
    Public Const RodUid25 As Integer = &H1000000
    Public Const RodUid26 As Integer = &H2000000
    Public Const RodUid27 As Integer = &H4000000
    Public Const RodUid28 As Integer = &H8000000
    Public Const RodUid29 As Integer = &H10000000
    Public Const RodUid30 As Integer = &H20000000
    Public Const RodUid31 As Integer = &H40000000
    Public Const RodUid32 As Integer = &H80000000


    REM *///////////////////////////////////////////////////////////////////
    REM * Broadcast Methods: "Attribute" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodAttrBlink As Integer = &H80

    Public Const RodAttrBgBlack As Integer = &H0
    Public Const RodAttrBgBlue As Integer = &H10
    Public Const RodAttrBgGreen As Integer = &H20
    Public Const RodAttrBgCyan As Integer = &H30
    Public Const RodAttrBgRed As Integer = &H40
    Public Const RodAttrBgMagenta As Integer = &H50
    Public Const RodAttrBgBrown As Integer = &H60
    Public Const RodAttrBgGray As Integer = &H70

    Public Const RodAttrIntensity As Integer = &H8

    Public Const RodAttrFgBlack As Integer = &H0
    Public Const RodAttrFgBlue As Integer = &H1
    Public Const RodAttrFgGreen As Integer = &H2
    Public Const RodAttrFgCyan As Integer = &H3
    Public Const RodAttrFgRed As Integer = &H4
    Public Const RodAttrFgMagenta As Integer = &H5
    Public Const RodAttrFgBrown As Integer = &H6
    Public Const RodAttrFgGray As Integer = &H7


    REM *///////////////////////////////////////////////////////////////////
    REM * "DrawBox" Method: "BorderType" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodBdrSingle As Integer = 1
    Public Const RodBdrDouble As Integer = 2
    Public Const RodBdrSolid As Integer = 3


    REM *///////////////////////////////////////////////////////////////////
    REM * "ControlClock" Method: "Function" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodClkStart As Integer = 1
    Public Const RodClkPause As Integer = 2
    Public Const RodClkResume As Integer = 3
    Public Const RodClkMove As Integer = 4
    Public Const RodClkStop As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "ControlCursor" Method: "Function" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodCrsLine As Integer = 1
    Public Const RodCrsLineBlink As Integer = 2
    Public Const RodCrsBlock As Integer = 3
    Public Const RodCrsBlockBlink As Integer = 4
    Public Const RodCrsOff As Integer = 5


    REM *///////////////////////////////////////////////////////////////////
    REM * "SelectCharacterSet" Method: "CharacterSet" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodCsUnicode As Integer = 997
    Public Const RodCsAscii As Integer = 998
    Public Const RodCsWindows As Integer = 999


    REM *///////////////////////////////////////////////////////////////////
    REM * "TransactionDisplay" Method: "Function" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodTdTransaction As Integer = 11
    Public Const RodTdNormal As Integer = 12


    REM *///////////////////////////////////////////////////////////////////
    REM * "UpdateVideoRegionAttribute" Method: "Function" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodUaSet As Integer = 1
    Public Const RodUaIntensityOn As Integer = 2
    Public Const RodUaIntensityOff As Integer = 3
    Public Const RodUaReverseOn As Integer = 4
    Public Const RodUaReverseOff As Integer = 5
    Public Const RodUaBlinkOn As Integer = 6
    Public Const RodUaBlinkOff As Integer = 7


    REM *///////////////////////////////////////////////////////////////////
    REM * "EventTypes" Property and "DataEvent" Event: "Status" Parameter Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const RodDeTouchUp As Integer = &H1
    Public Const RodDeTouchDown As Integer = &H2
    Public Const RodDeTouchMove As Integer = &H4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Remote Order Display
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposErodBadclk As Integer = 201           ' ControlClock
    Public Const OposErodNoclocks As Integer = 202         ' ControlClock
    Public Const OposErodNoregion As Integer = 203         ' RestoreVideoRegion
    Public Const OposErodNobuffers As Integer = 204        ' SaveVideoRegion
    Public Const OposErodNoroom As Integer = 205           ' SaveVideoRegion



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposScal.h
    REM *
    REM *   Scale header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "WeightUnit" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    Public Const ScalWuGram As Integer = 1
    Public Const ScalWuKilogram As Integer = 2
    Public Const ScalWuOunce As Integer = 3
    Public Const ScalWuPound As Integer = 4


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Scale
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEscalOverweight As Integer = 201      ' ReadWeight



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposScan.h
    REM *
    REM *   Scanner header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM *   Add "ScanDataType" values.
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "ScanDataType" Property Constants
    REM *///////////////////////////////////////////////////////////////////

    REM * One dimensional symbologies
    Public Const ScanSdtUpca As Integer = 101                        ' Digits
    Public Const ScanSdtUpce As Integer = 102                        ' Digits
    Public Const ScanSdtJan8 As Integer = 103                        ' = EAN 8
    Public Const ScanSdtEan8 As Integer = 103                        ' = JAN 8 (added in 1.2)
    Public Const ScanSdtJan13 As Integer = 104                       ' = EAN 13
    Public Const ScanSdtEan13 As Integer = 104                       ' = JAN 13 (added in 1.2)
    Public Const ScanSdtTf As Integer = 105                          ' (Discrete 2 of 5) Digits
    Public Const ScanSdtItf As Integer = 106                         ' (Interleaved 2 of 5) Digits
    Public Const ScanSdtCodabar As Integer = 107                     ' Digits, -, $, :, /, ., +;
    '   4 start/stop characters
    '   (a, b, c, d)
    Public Const ScanSdtCode39 As Integer = 108                      ' Alpha, Digits, Space, -, .,
    '   $, /, +, %; start/stop (*)
    ' Also has Full ASCII feature
    Public Const ScanSdtCode93 As Integer = 109                      ' Same characters as Code 39
    Public Const ScanSdtCode128 As Integer = 110                     ' 128 data characters

    Public Const ScanSdtUpcaS As Integer = 111                       ' UPC-A with supplemental
    '   barcode
    Public Const ScanSdtUpceS As Integer = 112                       ' UPC-E with supplemental
    '   barcode
    Public Const ScanSdtUpcd1 As Integer = 113                       ' UPC-D1
    Public Const ScanSdtUpcd2 As Integer = 114                       ' UPC-D2
    Public Const ScanSdtUpcd3 As Integer = 115                       ' UPC-D3
    Public Const ScanSdtUpcd4 As Integer = 116                       ' UPC-D4
    Public Const ScanSdtUpcd5 As Integer = 117                       ' UPC-D5
    Public Const ScanSdtEan8S As Integer = 118                       ' EAN 8 with supplemental
    '   barcode
    Public Const ScanSdtEan13S As Integer = 119                      ' EAN 13 with supplemental
    '   barcode
    Public Const ScanSdtEan128 As Integer = 120                      ' EAN 128
    Public Const ScanSdtOcra As Integer = 121                        ' OCR "A"
    Public Const ScanSdtOcrb As Integer = 122                        ' OCR "B"

    REM * Two dimensional symbologies
    Public Const ScanSdtPdf417 As Integer = 201
    Public Const ScanSdtMaxicode As Integer = 202

    REM * Special cases
    Public Const ScanSdtOther As Integer = 501                       ' Start of Scanner-Specific bar
    '   code symbologies
    Public Const ScanSdtUnknown As Integer = 0                       ' Cannot determine the barcode
    '   symbology.



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposSig.h
    REM *
    REM *   Signature Capture header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM * No definitions required for this version.



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposTone.h
    REM *
    REM *   Tone Indicator header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 97-06-04 OPOS Release 1.2                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM * No definitions required for this version.



    REM *///////////////////////////////////////////////////////////////////
    REM *
    REM * OposTot.h
    REM *
    REM *   Hard Totals header file for OPOS Applications.
    REM *
    REM * Modification history
    REM * ------------------------------------------------------------------
    REM * 95-12-08 OPOS Release 1.0                                     CRM
    REM *
    REM *///////////////////////////////////////////////////////////////////


    REM *///////////////////////////////////////////////////////////////////
    REM * "ResultCodeExtended" Property Constants for Hard Totals
    REM *///////////////////////////////////////////////////////////////////

    Public Const OposEtotNoroom As Integer = 201           ' Create, Write
    Public Const OposEtotValidation As Integer = 202       ' Read, Write



    REM *End of OPOSALL.BAS*
End Module

