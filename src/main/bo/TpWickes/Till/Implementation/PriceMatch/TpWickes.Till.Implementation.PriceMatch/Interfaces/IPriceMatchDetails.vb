﻿Public Interface IPriceMatchDetails

    Property CurrentPrice() As Double
    Property Price() As String
    Property Overrideprice() As String
    Property TillId() As Integer
    Property TransactionNumber() As Integer
    Property StoreId() As Integer
    Property TransactionDate() As Date
    Property OriginalPrice() As Double
    Property SkuNumber() As String
    Property VatRate() As Double
    Property ProductDescription() As String
    Property VatInclusive() As Boolean
    Property DiscountPercentage() As Double
    Property CompetitorName() As DataTable
    Property SelectedCompetitorName() As String
    Function PriceAfterPercentDiscount(ByVal price As String, ByVal percentage As Double) As Double
    Function Discount(ByVal price As String, ByVal percentage As Double) As Double
End Interface
