﻿Imports System.Windows.Forms
Imports TpWickes.Repository.Interface
Imports TpWickes.Repository.Implementation.SqlServer

Public Class PriceMatchFormDetails
    Implements IPriceMatchDetails

    Private ParameterId As Integer = 6004
    Private _Currentprice As Double
    Private _Price As String
    Private _Overrideprice As String
    Private _TillId As Integer
    Private _TransactionNumber As Integer
    Private _StoreId As Integer
    Private _TransactionDate As Date
    Private _OriginalPrice As Double
    Private _VatRate As Double
    Private _SkuNumber As String
    Private _ProductDescription As String
    Private _VatInclusive As Boolean = True
    Private _DiscountPercentage As Double
    Private _CompetitorName As DataTable
    Private Repository As IPriceMatchRepository
    Private _SelectedCompetitorName As String


    Public Property CurrentPrice() As Double Implements IPriceMatchDetails.CurrentPrice
        Get
            Return _Currentprice

        End Get
        Set(ByVal value As Double)
            _Currentprice = value
        End Set
    End Property

    Public Property Price() As String Implements IPriceMatchDetails.Price
        Get
            Return _Price
        End Get
        Set(ByVal value As String)
            _Price = value
        End Set
    End Property

    Public Property Overrideprice() As String Implements IPriceMatchDetails.Overrideprice
        Get
            Return _Overrideprice

        End Get
        Set(ByVal value As String)
            _Overrideprice = value
        End Set
    End Property

    Public Property TillId() As Integer Implements IPriceMatchDetails.TillId
        Get
            Return _TillId

        End Get
        Set(ByVal value As Integer)
            _TillId = value
        End Set
    End Property
    Public Property TransactionNumber() As Integer Implements IPriceMatchDetails.TransactionNumber
        Get
            Return _TransactionNumber

        End Get
        Set(ByVal value As Integer)
            _TransactionNumber = value
        End Set
    End Property

    Public Property StoreId() As Integer Implements IPriceMatchDetails.StoreId
        Get
            Return _StoreId

        End Get
        Set(ByVal value As Integer)
            _StoreId = value
        End Set
    End Property

    Public Property TransactionDate() As Date Implements IPriceMatchDetails.TransactionDate
        Get
            Return _TransactionDate

        End Get
        Set(ByVal value As Date)
            _TransactionDate = value
        End Set
    End Property

    Public Property OriginalPrice() As Double Implements IPriceMatchDetails.OriginalPrice
        Get
            Return _OriginalPrice

        End Get
        Set(ByVal value As Double)
            _OriginalPrice = value
        End Set
    End Property

    Public Property SkuNumber() As String Implements IPriceMatchDetails.SkuNumber
        Get
            Return _SkuNumber

        End Get
        Set(ByVal value As String)
            _SkuNumber = value
        End Set
    End Property

    Public Property ProductDescription() As String Implements IPriceMatchDetails.ProductDescription
        Get
            Return _ProductDescription

        End Get
        Set(ByVal value As String)
            _ProductDescription = value
        End Set
    End Property

    Public Property VatInclusive() As Boolean Implements IPriceMatchDetails.VatInclusive
        Get
            Return _VatInclusive

        End Get
        Set(ByVal value As Boolean)
            _VatInclusive = value
        End Set
    End Property
    Public Property VatRate() As Double Implements IPriceMatchDetails.VatRate
        Get
            Return _VatRate
        End Get
        Set(ByVal value As Double)
            _VatRate = value
        End Set
    End Property
    Public Property DiscountPercentage() As Double Implements IPriceMatchDetails.DiscountPercentage
        Get
            Return _DiscountPercentage

        End Get
        Set(ByVal value As Double)
            _DiscountPercentage = value
        End Set
    End Property
    Public Property CompetitorName() As DataTable Implements IPriceMatchDetails.CompetitorName
        Get
            Return _CompetitorName

        End Get
        Set(ByVal value As DataTable)
            _CompetitorName = value
        End Set
    End Property

    Public Property SelectedCompetitorName() As String Implements IPriceMatchDetails.SelectedCompetitorName
        Get
            Return _SelectedCompetitorName

        End Get
        Set(ByVal value As String)
            _SelectedCompetitorName = value
        End Set
    End Property

    Public Overridable Function PriceAfterPercentDiscount(ByVal price As String, ByVal percentage As Double) As Double Implements IPriceMatchDetails.PriceAfterPercentDiscount
        If Not price = String.Empty Then
            Return CDbl(price) - Discount(price, percentage)
        Else
            Return 0
        End If

    End Function

    Friend Overridable Function Discount(ByVal price As String, ByVal percentage As Double) As Double Implements IPriceMatchDetails.Discount
        If Not price = String.Empty Then
            Return CDbl(price) * (percentage / 100)
        Else
            Return 0
        End If
    End Function

    Friend Function PriceAfterDiscount(ByVal competitorPrice As String) As Double
        If Not Repository.CheckCompetitorGroup(SelectedCompetitorName) Then
            Return PriceAfterPercentDiscount(CStr(CompetitorPriceIncludingVat(Price)), DiscountPercentage)
        Else
            Return CompetitorPriceIncludingVat(competitorPrice)
        End If
    End Function

    Friend Overridable Function CompetitorPriceIncludingVat(ByVal competitorPrice As String) As Double
        If Not competitorPrice = String.Empty Then
            If VatInclusive Then
                Return CDbl(competitorPrice)
            Else
                Return VatAdjustedCompetitorPrice(competitorPrice)
            End If
        Else
            Return 0
        End If

    End Function
    Friend Overridable Function VatAdjustedCompetitorPrice(ByVal competitorPriceIncudingVat As String) As Double
        If Not competitorPriceIncudingVat = String.Empty Then
            Return CDbl(competitorPriceIncudingVat) + CDbl(competitorPriceIncudingVat) * (VatRate / 100)
        Else
            Return 0
        End If

    End Function

    Friend Overridable Function ValidateData(ByVal dataValue As String) As Boolean
        If Not dataValue = String.Empty Or Nothing Then
            If dataValue = "0" Then
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If

    End Function

    Friend Overridable Function ValidateName(ByVal name As String) As Boolean
        If name = String.Empty Or Nothing Then
            Return True
        Else
            Return False
        End If
    End Function


    Friend Overridable Function ValidateOverrideAndCurrentPrice(ByVal overridePrice As Double, ByVal currentPrice As Double) As Boolean
        If overridePrice > currentPrice Then
            Return True
        Else
            Return False
        End If
    End Function

    Friend Overridable Function ValidateOverrideAndCurrentPriceForSuspiciousPrice(ByVal overridePrice As Double, ByVal currentPrice As Double) As Boolean
        If currentPrice - overridePrice > 999.99 Then
            Return True
        Else
            Return False
        End If
    End Function

    Friend Function ValidateCompetitorPriceAgainstCurrentPrice(ByVal overridePrice As Double, ByVal currentPrice As Double) As Boolean
        If ValidateOverrideAndCurrentPrice(Overrideprice, CurrentPrice) Then
            MessageBoxCreate("Override Price is Greater Than Current Price ", "Invalid Price Entered")
            Return False
        Else
            Return True
        End If

    End Function

    Friend Function Validation() As Boolean

        If ValidateName(SelectedCompetitorName) Then
            MessageBoxCreate("Competitor Name is not Selected ", "Missing Competitor Details")
            Return False
        End If

        If ValidateData(Price) Then
            MessageBoxCreate("Competitor Price Entered is Invalid ", "Missing Competitor Details")
            Return False
        End If

        If ValidateData(Overrideprice) Then
            MessageBoxCreate("Override Price Entered is Invalid ", "Missing Override Price")
            Return False
        End If

        If ValidateOverrideAndCurrentPriceForSuspiciousPrice(CDbl(Overrideprice), CurrentPrice) Then
            MessageBoxCreate("Suspicious Price Entered ", "Invalid Price Entered")
            Return False
        End If
        Return True
    End Function

    Friend Sub MessageBoxCreate(ByVal message As String, ByVal messageStatus As String)
        Dim msgchk As New MessageBoxExtended
        msgchk.BackColor = System.Drawing.Color.Red
        msgchk.btnOk.BackColor = System.Drawing.SystemColors.ButtonFace
        msgchk.Text = messageStatus
        msgchk.lblMsg.Text = message
        msgchk.lblMsg.Anchor = AnchorStyles.Top
        msgchk.lblMsg.Left = CInt(msgchk.Width / 2 - msgchk.lblMsg.Width / 2)
        msgchk.lblMsg.Top = CInt((msgchk.Height - msgchk.lblMsg.Height) / 2)
        msgchk.ShowDialog()
    End Sub

    Friend Sub LoadData()
        Repository = New PriceMatchRepository
        DiscountPercentage = Repository.GetDiscountPercentage(ParameterId)
        CompetitorName = Repository.GetCompetitorName
    End Sub

    Friend Function SaveCompetitor(ByVal competitorName As String) As Boolean
        Return Repository.SaveLocalCompetitor(competitorName)
    End Function
    Friend Function FormatPencePrice(ByVal price As Integer) As String
        Try
            Dim pounds As Integer = price \ 100
            Dim pence As Integer = price Mod 100
            Return CStr(pounds) + "." + Right("00" + CStr(pence), 2)
        Catch ex As Exception

        End Try
        Return String.Empty
    End Function

    Friend Function FormatPrice(ByVal price As String) As String
        Try
            If price = String.Empty Then
                Return String.Empty
            Else
                Return FormatPencePrice(CInt(price.Replace(".", "").Trim))
            End If
        Catch ex As Exception

        End Try
        Return String.Empty
    End Function
End Class
