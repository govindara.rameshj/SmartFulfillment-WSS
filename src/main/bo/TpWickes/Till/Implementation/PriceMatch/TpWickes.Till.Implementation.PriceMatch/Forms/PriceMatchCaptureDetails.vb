﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports System.Text
Imports Cts.Oasys.Core.System

Public Class frmPriceMatchCaptureDetails

    Private priceDetails As New PriceMatchFormDetails
    Private priceOverride As New PriceMatchOverride

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Function CalculateOverridePrice(ByVal SkuNumber As String, ByVal ProductDescription As String, ByVal StoreId As Integer, ByVal TransactionDate As Date, ByVal TillId As Integer _
                               , ByVal TransactionNumber As Integer, ByVal OriginalPrice As Double, ByVal CurrentPrice As Double, ByVal VatRate As Double) As IPriceMatchDetails

        GetDataValues(SkuNumber, ProductDescription, StoreId, TransactionDate, TillId, TransactionNumber, OriginalPrice, CurrentPrice, VatRate)
        Me.ShowDialog()
        Return priceDetails
    End Function

    Private Sub PriceMatchCaptureDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim BackColor As System.Drawing.Color = Drawing.ColorTranslator.FromWin32(Parameter.GetInteger(130))
        Me.BackColor = BackColor
        Me.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAccept.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lblSkuNumber.BackColor = System.Drawing.Color.White
        Me.lblDescription.BackColor = System.Drawing.Color.White
        Me.lblCurrentPriceValue.BackColor = System.Drawing.Color.White
        Me.lblStoreValue.BackColor = System.Drawing.Color.White
        Me.lblTillNoValue.BackColor = System.Drawing.Color.White
        Me.lblTranNoValue.BackColor = System.Drawing.Color.White
        Me.lblOriginalPriceValue.BackColor = System.Drawing.Color.White
        Me.lblDateValue.BackColor = System.Drawing.Color.White
        Me.txtOverrideTo.BackColor = System.Drawing.Color.White
    End Sub


    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        SetFormValues()
        priceDetails.Overrideprice = String.Empty
        txtPrice.Text = String.Empty
        txtOverrideTo.Text = String.Empty
        Me.txtPrice.Focus()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If priceDetails.Validation() Then
            priceDetails.SaveCompetitor(priceDetails.SelectedCompetitorName)
            Me.Close()
        Else
            Me.txtPrice.Focus()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        priceDetails.SelectedCompetitorName = String.Empty
        Me.Close()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Me.btnAdd.Focus()
        With Me.cmbCompetitorName
            .DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            .Text = String.Empty
            .Focus()
            .Refresh()
        End With
    End Sub

    Private Sub frmPriceMatchCaptureDetails_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F3 : btnReset.PerformClick()
            Case Keys.F2 : btnAdd.PerformClick()
            Case Keys.F12 : btnCancel.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Private Sub GetDataValues(ByVal SkuNumber As String, ByVal ProductDescription As String, ByVal StoreId As Integer, ByVal TransactionDate As Date, ByVal TillId As Integer _
                                   , ByVal TransactionNumber As Integer, ByVal OriginalPrice As Double, ByVal CurrentPrice As Double, ByVal VatRate As Double)
        SetDataValues(SkuNumber, ProductDescription, StoreId, TransactionDate, TillId, TransactionNumber, OriginalPrice, CurrentPrice, VatRate)
        SetFormValues()
        SetCompetitorName()

    End Sub
    Private Sub SetDataValues(ByVal SkuNumber As String, ByVal ProductDescription As String, ByVal StoreId As Integer, ByVal TransactionDate As Date, ByVal TillId As Integer _
                                   , ByVal TransactionNumber As Integer, ByVal OriginalPrice As Double, ByVal CurrentPrice As Double, ByVal VatRate As Double)

        priceDetails.SkuNumber = SkuNumber
        priceDetails.StoreId = StoreId
        priceDetails.TransactionDate = TransactionDate
        priceDetails.TillId = TillId
        priceDetails.TransactionNumber = TransactionNumber
        priceDetails.OriginalPrice = OriginalPrice
        priceDetails.CurrentPrice = CurrentPrice
        priceDetails.VatInclusive = True
        priceDetails.ProductDescription = ProductDescription
        priceDetails.VatRate = VatRate
        priceDetails.LoadData()
    End Sub
    Private Sub SetFormValues()
        SetTrasactionDetails()
        lblSkuNumber.Text = priceDetails.SkuNumber
        lblCurrentPriceValue.Text = priceDetails.CurrentPrice.ToString("N2")
        chkVatInclusive.Checked = priceDetails.VatInclusive
        lblDescription.Text = priceDetails.ProductDescription
    End Sub

    Private Sub SetCompetitorName()
        With Me.cmbCompetitorName
            .DataSource = priceDetails.CompetitorName
            .DisplayMember = "Name"
        End With
    End Sub
    Private Sub SetTrasactionDetails()
        If Not (priceDetails.OriginalPrice = 0 Or  priceDetails.OriginalPrice = Nothing) Then
            lblStoreValue.Text = priceDetails.StoreId.ToString.PadLeft(3, "0"c)
            lblDateValue.Text = priceDetails.TransactionDate.ToShortDateString
            lblTillNoValue.Text = priceDetails.TillId.ToString.PadLeft(2, "0"c)
            lblTranNoValue.Text = priceDetails.TransactionNumber.ToString.PadLeft(4, "0"c)
            lblOriginalPriceValue.Text = priceDetails.OriginalPrice.ToString("N2")
        End If
    End Sub

    Private Sub txtPrice_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrice.TextChanged

        Dim cursorposition As Integer = txtPrice.SelectionStart
        Dim originalTextLength As Integer = txtPrice.TextLength
        'txtPrice.Text = priceDetails.FormatPrice(txtPrice.Text)
        txtPrice.Text = priceDetails.FormatPrice(txtPrice.Text)

        If originalTextLength = 1 Then
            txtPrice.SelectionStart = 4
        Else
            If txtPrice.TextLength = originalTextLength Then txtPrice.SelectionStart = cursorposition

            If txtPrice.TextLength < originalTextLength Then txtPrice.SelectionStart = cursorposition - 1

            If txtPrice.TextLength > originalTextLength Then txtPrice.SelectionStart = cursorposition + 1
        End If

        txtPrice.SelectionLength = 0
        If Not txtPrice.Text = String.Empty Then
            priceDetails.Price = FormatNumber(txtPrice.Text, 2)
        End If
    End Sub

    Private Sub txtOverrideTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOverrideTo.TextChanged
        If Not priceDetails.Price = String.Empty Then
            If Not priceDetails.Overrideprice = String.Empty Then
                txtOverrideTo.Text = FormatNumber(priceDetails.Overrideprice, 2)
            End If
        End If
    End Sub

    Private Sub cmbCompetitorName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompetitorName.SelectedIndexChanged
        'priceDetails.SelectedCompetitorName = DirectCast(DirectCast(DirectCast(cmbCompetitorName.SelectedValue, System.Object), System.Data.DataRowView).Row, System.Data.DataRow).ItemArray(0)
        priceDetails.SelectedCompetitorName = CStr(DirectCast(cmbCompetitorName.SelectedItem, System.Data.DataRowView).Item(0))
        If Not priceDetails.Price = String.Empty Then
            priceDetails.Overrideprice = CStr(priceDetails.PriceAfterDiscount(priceDetails.Price))
            Me.txtOverrideTo.Text = FormatNumber(priceDetails.Overrideprice, 2)
        Else
            Me.txtOverrideTo.Text = String.Empty
        End If
    End Sub

    Private Sub chkVatInclusive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVatInclusive.CheckedChanged
        priceDetails.VatInclusive = chkVatInclusive.Checked
        If Not priceDetails.Price = String.Empty Then
            priceDetails.Overrideprice = CStr(priceDetails.PriceAfterDiscount(priceDetails.Price))
            Me.txtOverrideTo.Text = FormatNumber(priceDetails.Overrideprice, 2)
        Else
            Me.txtOverrideTo.Text = String.Empty
        End If
    End Sub

    Private Sub txtPrice_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrice.KeyPress, txtPrice.KeyPress
        If e.Handled Then Exit Sub
        If Not priceDetails.Price = String.Empty Then
            e.Handled = True
            Select Case e.KeyChar
                Case ChrW(Keys.Enter) : CompetitorPricePopulateControls()
                Case Else : e.Handled = False

            End Select
            If ((Not IsNumeric(e.KeyChar)) And Not (e.KeyChar = ChrW(Keys.Back)) And Not (e.KeyChar = ".")) Then
                e.Handled = True
            End If
        Else
            Me.txtOverrideTo.Text = String.Empty
        End If
    End Sub

    Private Sub txtPrice_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrice.Leave
        'CompetitorPricePopulateControls()
    End Sub
    Private Sub cmbCompetitorName_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompetitorName.Leave
        If Me.cmbCompetitorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown Then
            Dim dtNewRow As DataRow
            If Not Me.cmbCompetitorName.Text = "" Then
                priceDetails.SelectedCompetitorName = Me.cmbCompetitorName.Text
            End If
            If Not (Me.cmbCompetitorName.Text = "" Or CheckCompetitorAlreadyExists(Me.cmbCompetitorName.Text)) Then
                dtNewRow = priceDetails.CompetitorName.NewRow()
                dtNewRow.Item("Name") = Me.cmbCompetitorName.Text
                priceDetails.CompetitorName.Rows.Add(dtNewRow)
                'Me.cmbCompetitorName.SelectedValue = dtNewRow.Item("Name")
            End If
            With Me.cmbCompetitorName
                .DataSource = priceDetails.CompetitorName
                .Update()
                .DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
                .Text = priceDetails.SelectedCompetitorName
            End With
            Me.txtPrice.Focus()
        End If
    End Sub

    Private Function CheckCompetitorAlreadyExists(ByVal competitorname As String) As Boolean
        For i = 0 To priceDetails.CompetitorName.Rows.Count - 1
            If priceDetails.CompetitorName.Rows(i).Item(0).ToString.ToLower = Me.cmbCompetitorName.Text.ToLower Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub cmbCompetitorName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbCompetitorName.KeyPress
        e.Handled = True
        Select Case e.KeyChar
            Case ChrW(Keys.Enter) : Me.txtPrice.Focus()
            Case Else : e.Handled = False
        End Select
    End Sub

    Private Sub CompetitorPricePopulateControls()
        If Not txtPrice.Text = String.Empty Then
            priceDetails.Price = FormatNumber(Me.txtPrice.Text, 2)
            priceDetails.Overrideprice = CStr(priceDetails.PriceAfterDiscount(priceDetails.Price))
            If priceDetails.ValidateCompetitorPriceAgainstCurrentPrice(CDbl(priceDetails.Overrideprice), priceDetails.CurrentPrice) = False Then
                priceDetails.Overrideprice = CStr(priceDetails.CurrentPrice)
            End If
            Me.txtOverrideTo.Text = FormatNumber(priceDetails.Overrideprice, 2)
        Else
            priceDetails.Price = String.Empty
            Me.txtOverrideTo.Text = String.Empty
        End If

    End Sub

    Private Sub lblCurrentPriceValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblCurrentPriceValue.Click

    End Sub
End Class