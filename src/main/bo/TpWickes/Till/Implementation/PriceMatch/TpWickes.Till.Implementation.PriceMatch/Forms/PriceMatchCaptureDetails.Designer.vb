﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPriceMatchCaptureDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblOk = New System.Windows.Forms.Label
        Me.grpCompetitor = New System.Windows.Forms.GroupBox
        Me.chkVatInclusive = New System.Windows.Forms.CheckBox
        Me.btnAdd = New System.Windows.Forms.Button
        Me.cmbCompetitorName = New System.Windows.Forms.ComboBox
        Me.txtPrice = New System.Windows.Forms.TextBox
        Me.lblPrice = New System.Windows.Forms.Label
        Me.lblVatInclusive = New System.Windows.Forms.Label
        Me.lblCompetitorName = New System.Windows.Forms.Label
        Me.grpTransactionDetails = New System.Windows.Forms.GroupBox
        Me.lblOriginalPriceValue = New System.Windows.Forms.Label
        Me.lblTranNoValue = New System.Windows.Forms.Label
        Me.lblTillNoValue = New System.Windows.Forms.Label
        Me.lblDateValue = New System.Windows.Forms.Label
        Me.lblStoreValue = New System.Windows.Forms.Label
        Me.lblOriginalPrice = New System.Windows.Forms.Label
        Me.lblTillTranNumber = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblStore = New System.Windows.Forms.Label
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnAccept = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.txtOverrideTo = New System.Windows.Forms.TextBox
        Me.lblOverrideTo = New System.Windows.Forms.Label
        Me.lblItem = New System.Windows.Forms.Label
        Me.lblCurrentPrice = New System.Windows.Forms.Label
        Me.lblSkuNumber = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblCurrentPriceValue = New System.Windows.Forms.Label
        Me.grpCompetitor.SuspendLayout()
        Me.grpTransactionDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOk
        '
        Me.lblOk.AutoSize = True
        Me.lblOk.Location = New System.Drawing.Point(211, 100)
        Me.lblOk.Name = "lblOk"
        Me.lblOk.Size = New System.Drawing.Size(0, 13)
        Me.lblOk.TabIndex = 2
        Me.lblOk.Visible = False
        '
        'grpCompetitor
        '
        Me.grpCompetitor.Controls.Add(Me.chkVatInclusive)
        Me.grpCompetitor.Controls.Add(Me.btnAdd)
        Me.grpCompetitor.Controls.Add(Me.cmbCompetitorName)
        Me.grpCompetitor.Controls.Add(Me.txtPrice)
        Me.grpCompetitor.Controls.Add(Me.lblPrice)
        Me.grpCompetitor.Controls.Add(Me.lblVatInclusive)
        Me.grpCompetitor.Controls.Add(Me.lblCompetitorName)
        Me.grpCompetitor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCompetitor.Location = New System.Drawing.Point(5, 32)
        Me.grpCompetitor.Name = "grpCompetitor"
        Me.grpCompetitor.Size = New System.Drawing.Size(329, 158)
        Me.grpCompetitor.TabIndex = 3
        Me.grpCompetitor.TabStop = False
        Me.grpCompetitor.Text = "Competitor Details"
        '
        'chkVatInclusive
        '
        Me.chkVatInclusive.AutoSize = True
        Me.chkVatInclusive.Location = New System.Drawing.Point(235, 124)
        Me.chkVatInclusive.Name = "chkVatInclusive"
        Me.chkVatInclusive.Size = New System.Drawing.Size(15, 14)
        Me.chkVatInclusive.TabIndex = 4
        Me.chkVatInclusive.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Location = New System.Drawing.Point(248, 26)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "F2-Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'cmbCompetitorName
        '
        Me.cmbCompetitorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompetitorName.FormattingEnabled = True
        Me.cmbCompetitorName.Location = New System.Drawing.Point(51, 26)
        Me.cmbCompetitorName.MaxLength = 30
        Me.cmbCompetitorName.Name = "cmbCompetitorName"
        Me.cmbCompetitorName.Size = New System.Drawing.Size(178, 21)
        Me.cmbCompetitorName.Sorted = True
        Me.cmbCompetitorName.TabIndex = 1
        '
        'txtPrice
        '
        Me.txtPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrice.Location = New System.Drawing.Point(51, 121)
        Me.txtPrice.MaxLength = 12
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtPrice.Size = New System.Drawing.Size(67, 20)
        Me.txtPrice.TabIndex = 3
        Me.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Location = New System.Drawing.Point(6, 124)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(36, 13)
        Me.lblPrice.TabIndex = 3
        Me.lblPrice.Text = "Price"
        '
        'lblVatInclusive
        '
        Me.lblVatInclusive.AutoSize = True
        Me.lblVatInclusive.Location = New System.Drawing.Point(143, 124)
        Me.lblVatInclusive.Name = "lblVatInclusive"
        Me.lblVatInclusive.Size = New System.Drawing.Size(86, 13)
        Me.lblVatInclusive.TabIndex = 2
        Me.lblVatInclusive.Text = "VAT Inclusive"
        '
        'lblCompetitorName
        '
        Me.lblCompetitorName.AutoSize = True
        Me.lblCompetitorName.Location = New System.Drawing.Point(6, 31)
        Me.lblCompetitorName.Name = "lblCompetitorName"
        Me.lblCompetitorName.Size = New System.Drawing.Size(39, 13)
        Me.lblCompetitorName.TabIndex = 1
        Me.lblCompetitorName.Text = "Name"
        '
        'grpTransactionDetails
        '
        Me.grpTransactionDetails.Controls.Add(Me.lblOriginalPriceValue)
        Me.grpTransactionDetails.Controls.Add(Me.lblTranNoValue)
        Me.grpTransactionDetails.Controls.Add(Me.lblTillNoValue)
        Me.grpTransactionDetails.Controls.Add(Me.lblDateValue)
        Me.grpTransactionDetails.Controls.Add(Me.lblStoreValue)
        Me.grpTransactionDetails.Controls.Add(Me.lblOriginalPrice)
        Me.grpTransactionDetails.Controls.Add(Me.lblTillTranNumber)
        Me.grpTransactionDetails.Controls.Add(Me.lblDate)
        Me.grpTransactionDetails.Controls.Add(Me.lblStore)
        Me.grpTransactionDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpTransactionDetails.Location = New System.Drawing.Point(355, 32)
        Me.grpTransactionDetails.Name = "grpTransactionDetails"
        Me.grpTransactionDetails.Size = New System.Drawing.Size(282, 158)
        Me.grpTransactionDetails.TabIndex = 4
        Me.grpTransactionDetails.TabStop = False
        Me.grpTransactionDetails.Text = "Original Transaction Details"
        '
        'lblOriginalPriceValue
        '
        Me.lblOriginalPriceValue.BackColor = System.Drawing.Color.White
        Me.lblOriginalPriceValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOriginalPriceValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOriginalPriceValue.Location = New System.Drawing.Point(91, 125)
        Me.lblOriginalPriceValue.Name = "lblOriginalPriceValue"
        Me.lblOriginalPriceValue.Size = New System.Drawing.Size(100, 21)
        Me.lblOriginalPriceValue.TabIndex = 25
        Me.lblOriginalPriceValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTranNoValue
        '
        Me.lblTranNoValue.BackColor = System.Drawing.Color.White
        Me.lblTranNoValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTranNoValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranNoValue.Location = New System.Drawing.Point(132, 95)
        Me.lblTranNoValue.Name = "lblTranNoValue"
        Me.lblTranNoValue.Size = New System.Drawing.Size(59, 21)
        Me.lblTranNoValue.TabIndex = 24
        Me.lblTranNoValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTillNoValue
        '
        Me.lblTillNoValue.BackColor = System.Drawing.Color.White
        Me.lblTillNoValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTillNoValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTillNoValue.Location = New System.Drawing.Point(91, 95)
        Me.lblTillNoValue.Name = "lblTillNoValue"
        Me.lblTillNoValue.Size = New System.Drawing.Size(35, 21)
        Me.lblTillNoValue.TabIndex = 23
        Me.lblTillNoValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDateValue
        '
        Me.lblDateValue.BackColor = System.Drawing.Color.White
        Me.lblDateValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDateValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateValue.Location = New System.Drawing.Point(91, 63)
        Me.lblDateValue.Name = "lblDateValue"
        Me.lblDateValue.Size = New System.Drawing.Size(100, 21)
        Me.lblDateValue.TabIndex = 22
        Me.lblDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStoreValue
        '
        Me.lblStoreValue.BackColor = System.Drawing.Color.White
        Me.lblStoreValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStoreValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStoreValue.Location = New System.Drawing.Point(91, 31)
        Me.lblStoreValue.Name = "lblStoreValue"
        Me.lblStoreValue.Size = New System.Drawing.Size(100, 21)
        Me.lblStoreValue.TabIndex = 21
        Me.lblStoreValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblOriginalPrice
        '
        Me.lblOriginalPrice.AutoSize = True
        Me.lblOriginalPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOriginalPrice.Location = New System.Drawing.Point(11, 129)
        Me.lblOriginalPrice.Name = "lblOriginalPrice"
        Me.lblOriginalPrice.Size = New System.Drawing.Size(69, 13)
        Me.lblOriginalPrice.TabIndex = 3
        Me.lblOriginalPrice.Text = "Original Price"
        '
        'lblTillTranNumber
        '
        Me.lblTillTranNumber.AutoSize = True
        Me.lblTillTranNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTillTranNumber.Location = New System.Drawing.Point(11, 99)
        Me.lblTillTranNumber.Name = "lblTillTranNumber"
        Me.lblTillTranNumber.Size = New System.Drawing.Size(62, 13)
        Me.lblTillTranNumber.TabIndex = 2
        Me.lblTillTranNumber.Text = "Till-Tran No"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(11, 67)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(30, 13)
        Me.lblDate.TabIndex = 1
        Me.lblDate.Text = "Date"
        '
        'lblStore
        '
        Me.lblStore.AutoSize = True
        Me.lblStore.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStore.Location = New System.Drawing.Point(11, 35)
        Me.lblStore.Name = "lblStore"
        Me.lblStore.Size = New System.Drawing.Size(32, 13)
        Me.lblStore.TabIndex = 0
        Me.lblStore.Text = "Store"
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnReset.Location = New System.Drawing.Point(130, 227)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(83, 34)
        Me.btnReset.TabIndex = 5
        Me.btnReset.Text = "F3-Reset"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnAccept
        '
        Me.btnAccept.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAccept.Location = New System.Drawing.Point(280, 227)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(83, 34)
        Me.btnAccept.TabIndex = 6
        Me.btnAccept.Text = "F5-Accept"
        Me.btnAccept.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnCancel.Location = New System.Drawing.Point(430, 227)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(83, 34)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "F12-Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'txtOverrideTo
        '
        Me.txtOverrideTo.Location = New System.Drawing.Point(446, 192)
        Me.txtOverrideTo.MaxLength = 12
        Me.txtOverrideTo.Name = "txtOverrideTo"
        Me.txtOverrideTo.ReadOnly = True
        Me.txtOverrideTo.Size = New System.Drawing.Size(100, 20)
        Me.txtOverrideTo.TabIndex = 17
        '
        'lblOverrideTo
        '
        Me.lblOverrideTo.AutoSize = True
        Me.lblOverrideTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverrideTo.Location = New System.Drawing.Point(354, 199)
        Me.lblOverrideTo.Name = "lblOverrideTo"
        Me.lblOverrideTo.Size = New System.Drawing.Size(74, 13)
        Me.lblOverrideTo.TabIndex = 9
        Me.lblOverrideTo.Text = "Override To"
        '
        'lblItem
        '
        Me.lblItem.AutoSize = True
        Me.lblItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItem.Location = New System.Drawing.Point(2, 7)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(31, 13)
        Me.lblItem.TabIndex = 13
        Me.lblItem.Text = "Item"
        '
        'lblCurrentPrice
        '
        Me.lblCurrentPrice.AutoSize = True
        Me.lblCurrentPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentPrice.Location = New System.Drawing.Point(366, 7)
        Me.lblCurrentPrice.Name = "lblCurrentPrice"
        Me.lblCurrentPrice.Size = New System.Drawing.Size(81, 13)
        Me.lblCurrentPrice.TabIndex = 14
        Me.lblCurrentPrice.Text = "Current Price"
        '
        'lblSkuNumber
        '
        Me.lblSkuNumber.BackColor = System.Drawing.Color.White
        Me.lblSkuNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSkuNumber.Location = New System.Drawing.Point(35, 3)
        Me.lblSkuNumber.Name = "lblSkuNumber"
        Me.lblSkuNumber.Size = New System.Drawing.Size(54, 21)
        Me.lblSkuNumber.TabIndex = 18
        Me.lblSkuNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.Color.White
        Me.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDescription.Location = New System.Drawing.Point(95, 3)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(252, 21)
        Me.lblDescription.TabIndex = 19
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentPriceValue
        '
        Me.lblCurrentPriceValue.BackColor = System.Drawing.Color.White
        Me.lblCurrentPriceValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCurrentPriceValue.Location = New System.Drawing.Point(446, 3)
        Me.lblCurrentPriceValue.Name = "lblCurrentPriceValue"
        Me.lblCurrentPriceValue.Size = New System.Drawing.Size(100, 21)
        Me.lblCurrentPriceValue.TabIndex = 20
        Me.lblCurrentPriceValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmPriceMatchCaptureDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(642, 267)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblCurrentPriceValue)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblSkuNumber)
        Me.Controls.Add(Me.lblCurrentPrice)
        Me.Controls.Add(Me.lblItem)
        Me.Controls.Add(Me.lblOverrideTo)
        Me.Controls.Add(Me.txtOverrideTo)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.grpTransactionDetails)
        Me.Controls.Add(Me.grpCompetitor)
        Me.Controls.Add(Me.lblOk)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPriceMatchCaptureDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capture Competitor Details"
        Me.grpCompetitor.ResumeLayout(False)
        Me.grpCompetitor.PerformLayout()
        Me.grpTransactionDetails.ResumeLayout(False)
        Me.grpTransactionDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOk As System.Windows.Forms.Label
    Friend WithEvents grpCompetitor As System.Windows.Forms.GroupBox
    Friend WithEvents grpTransactionDetails As System.Windows.Forms.GroupBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtOverrideTo As System.Windows.Forms.TextBox
    Friend WithEvents lblOverrideTo As System.Windows.Forms.Label
    Friend WithEvents lblItem As System.Windows.Forms.Label
    Friend WithEvents lblCurrentPrice As System.Windows.Forms.Label
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblVatInclusive As System.Windows.Forms.Label
    Friend WithEvents lblCompetitorName As System.Windows.Forms.Label
    Friend WithEvents cmbCompetitorName As System.Windows.Forms.ComboBox
    Friend WithEvents lblOriginalPrice As System.Windows.Forms.Label
    Friend WithEvents lblTillTranNumber As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblStore As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents chkVatInclusive As System.Windows.Forms.CheckBox
    Friend WithEvents lblSkuNumber As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblCurrentPriceValue As System.Windows.Forms.Label
    Friend WithEvents lblDateValue As System.Windows.Forms.Label
    Friend WithEvents lblStoreValue As System.Windows.Forms.Label
    Friend WithEvents lblTillNoValue As System.Windows.Forms.Label
    Friend WithEvents lblTranNoValue As System.Windows.Forms.Label
    Friend WithEvents lblOriginalPriceValue As System.Windows.Forms.Label
End Class
