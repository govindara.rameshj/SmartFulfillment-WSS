﻿Imports TpWickes.InterOp.Interface.PriceMatch

Public Class PriceMatchOverride
    Implements IPriceMatch

    Dim priceDetailsInstance As New PriceMatchFormDetails
    Public Sub CalculateOverridePrice(ByVal SkuNumber As String, ByVal ProductDescription As String, ByVal StoreId As Integer, ByVal TransactionDate As Date, ByVal TillId As Integer, ByVal TransactionNumber As Integer, ByVal OriginalPrice As Double, ByVal CurrentPrice As Double, ByVal VatRate As Double) Implements IPriceMatch.CalculateOverridePrice
        Dim priceMatchFormInstance As New PriceMatch.frmPriceMatchCaptureDetails
        priceDetailsInstance = CType(priceMatchFormInstance.CalculateOverridePrice(SkuNumber, ProductDescription, StoreId, TransactionDate, TillId, TransactionNumber, OriginalPrice, CurrentPrice, VatRate), PriceMatchFormDetails)
    End Sub
    Public ReadOnly Property CompetitorName() As String Implements IPriceMatch.CompetitorName
        Get
            Return priceDetailsInstance.SelectedCompetitorName
        End Get
    End Property

    Public ReadOnly Property VATInclusive() As Boolean Implements IPriceMatch.VATInclusive
        Get
            Return priceDetailsInstance.VatInclusive
        End Get
    End Property

    Public ReadOnly Property CurrentPrice() As Double Implements IPriceMatch.CurrentPrice
        Get
            Return priceDetailsInstance.CurrentPrice
        End Get
    End Property

    Public ReadOnly Property DiscountPercentage() As Double Implements IPriceMatch.DiscountPercentage
        Get
            Return priceDetailsInstance.DiscountPercentage
        End Get
    End Property

    Public ReadOnly Property OriginalPrice() As Double Implements IPriceMatch.OriginalPrice
        Get
            Return priceDetailsInstance.OriginalPrice
        End Get
    End Property

    Public ReadOnly Property Overrideprice() As String Implements IPriceMatch.Overrideprice
        Get
            Return priceDetailsInstance.Overrideprice
        End Get
    End Property

    Public ReadOnly Property CompetitorPrice() As String Implements IPriceMatch.CompetitorPrice
        Get
            Return priceDetailsInstance.Price
        End Get
    End Property

    Public ReadOnly Property ProductDescription() As String Implements IPriceMatch.ProductDescription
        Get
            Return priceDetailsInstance.ProductDescription
        End Get
    End Property

    Public ReadOnly Property SkuNumber() As String Implements IPriceMatch.SkuNumber
        Get
            Return priceDetailsInstance.SkuNumber
        End Get
    End Property

    Public ReadOnly Property StoreId() As Integer Implements IPriceMatch.StoreId
        Get
            Return priceDetailsInstance.StoreId
        End Get
    End Property

    Public ReadOnly Property TillId() As Integer Implements IPriceMatch.TillId
        Get
            Return priceDetailsInstance.TillId
        End Get
    End Property

    Public ReadOnly Property TransactionDate() As Date Implements IPriceMatch.TransactionDate
        Get
            Return priceDetailsInstance.TransactionDate
        End Get
    End Property

    Public ReadOnly Property TransactionNumber() As Integer Implements IPriceMatch.TransactionNumber
        Get
            Return priceDetailsInstance.TransactionNumber
        End Get
    End Property

    Public ReadOnly Property VatRate() As Double Implements IPriceMatch.VatRate
        Get
            Return priceDetailsInstance.VatRate
        End Get
    End Property
End Class
