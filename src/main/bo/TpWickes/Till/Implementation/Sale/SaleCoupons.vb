﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class SaleCoupons
    Inherits BaseCollection(Of SaleCoupon)
    Implements ISaleCoupons

#Region "Properties"

    Friend _TransactionDate As Date
    Friend _TillID As String
    Friend _TransactionNo As String
    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements InterOp.Interface.Sale.ISaleCoupons.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

#End Region

#Region "Methods"

    Public Function Initialise(ByVal TransactionDate As Date, ByVal TillId As String, ByVal TransactionNumber As String) As Boolean Implements ISaleCoupons.Initialise

        If TransactionDate <> DateTime.MinValue And _
           TillId IsNot Nothing AndAlso TillId.Trim.Length <> 0 And _
           TransactionNumber IsNot Nothing AndAlso TransactionNumber.Trim.Length <> 0 Then

            _TransactionDate = TransactionDate
            _TillID = TillId
            _TransactionNo = TransactionNumber

            Load(TransactionDate, TillId, TransactionNumber)
            Return True

        End If

        Return False

    End Function

    Public Function FirstCoupon() As ISaleCoupon Implements ISaleCoupons.FirstCoupon
        Dim returnSaleCoupon As SaleCoupon = Nothing

        If Items IsNot Nothing Then
            Dim firstSeq As Integer
            Dim nextSeq As Integer

            ' Get the coupon with the lowest sequence number
            For Each saleCoupon As SaleCoupon In Items
                If Integer.TryParse(saleCoupon.SequenceNumber, nextSeq) Then
                    If returnSaleCoupon Is Nothing Then
                        returnSaleCoupon = saleCoupon
                        firstSeq = nextSeq
                    Else
                        If nextSeq < firstSeq Then
                            returnSaleCoupon = saleCoupon
                            firstSeq = nextSeq
                        End If
                    End If
                End If
            Next
        End If
        FirstCoupon = returnSaleCoupon
    End Function

    Public Function NextCoupon(ByVal previousSequence As Integer) As ISaleCoupon Implements ISaleCoupons.NextCoupon
        Dim returnSaleCoupon As SaleCoupon = Nothing

        If Items IsNot Nothing Then
            Dim firstSeq As Integer
            Dim nextSeq As Integer

            ' Get the coupon with the lowest sequence number that is higher than the previousSequence parameter
            For Each saleCoupon As SaleCoupon In Items
                If Integer.TryParse(saleCoupon.SequenceNumber, nextSeq) Then
                    If nextSeq > previousSequence Then
                        If returnSaleCoupon Is Nothing Then
                            returnSaleCoupon = saleCoupon
                            firstSeq = nextSeq
                        Else
                            If nextSeq < firstSeq Then
                                returnSaleCoupon = saleCoupon
                                firstSeq = nextSeq
                            End If
                        End If
                    End If
                End If
            Next
        End If
        NextCoupon = returnSaleCoupon
    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Overloads Sub Load(ByVal TransactionDate As Date, ByVal TillID As String, ByVal TransactionNumber As String)

        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Repository = GetSaleCouponRepositoryFactory.FactoryGet

        DT = Repository.GetSaleCoupon(TransactionDate, TillID, TransactionNumber)

        If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then

            _ExistsInDB = True
            Me.Load(DT)

        End If

    End Sub

    Friend Overloads Sub Load(ByVal TransactionDate As Date, ByVal TillID As String, ByVal TransactionNumber As String, ByVal SequenceNumber As String)

        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Repository = GetSaleCouponRepositoryFactory.FactoryGet

        DT = Repository.GetSaleCoupon(TransactionDate, TillID, TransactionNumber, SequenceNumber)

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            _ExistsInDB = True
            Me.Load(DT)

        End If

    End Sub

#End Region

End Class