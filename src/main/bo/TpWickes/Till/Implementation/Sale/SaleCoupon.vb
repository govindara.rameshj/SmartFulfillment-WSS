﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class SaleCoupon
    Inherits Base
    Implements ISaleCoupon

#Region "Table Fields"

    Friend _StoreID As String
    Friend _TranDate As Date
    Friend _TranTillID As String
    Friend _TranNo As String
    Friend _Sequence As String
    Friend _CouponID As String
    Friend _Status As String
    Friend _ExcCoupon As Boolean
    Friend _MarketRef As String
    Friend _ReUsable As Boolean
    Friend _IssueStoreNo As String
    Friend _SerialNo As String
    Friend _ManagerID As String
    Friend _RTIFlag As String

    <ColumnMapping("STOREID")> _
    Public Property StoreID() As String Implements ISaleCoupon.StoreID
        Get
            Return _StoreID
        End Get
        Set(ByVal value As String)
            _StoreID = value
        End Set
    End Property

    <ColumnMapping("TRANDATE")> _
    Public Property TransactionDate() As Date Implements ISaleCoupon.TransactionDate
        Get
            Return _TranDate
        End Get
        Set(ByVal value As Date)
            _TranDate = value
        End Set
    End Property

    <ColumnMapping("TRANTILLID")> _
    Public Property TransactionTillID() As String Implements ISaleCoupon.TransactionTillID
        Get
            Return _TranTillID
        End Get
        Set(ByVal value As String)
            _TranTillID = value
        End Set
    End Property

    <ColumnMapping("TRANNO")> _
    Public Property TransactionNumber() As String Implements ISaleCoupon.TransactionNumber
        Get
            Return _TranNo
        End Get
        Set(ByVal value As String)
            _TranNo = value
        End Set
    End Property

    <ColumnMapping("SEQUENCE")> _
    Public Property SequenceNumber() As String Implements ISaleCoupon.SequenceNumber
        Get
            Return _Sequence
        End Get
        Set(ByVal value As String)
            _Sequence = value
        End Set
    End Property

    <ColumnMapping("COUPONID")> _
    Public Property CouponID() As String Implements ISaleCoupon.CouponID
        Get
            Return _CouponID
        End Get
        Set(ByVal value As String)
            _CouponID = value
        End Set
    End Property

    <ColumnMapping("STATUS")> _
    Friend WriteOnly Property PrivateStatus() As String
        Set(ByVal value As String)

            _Status = value

        End Set
    End Property

    Public Property Status() As CouponStatus Implements ISaleCoupon.Status
        Get
            Try
                Dim Temp As CouponStatus

                Temp = CType([Enum].Parse(GetType(CouponStatus), _Status), CouponStatus)

                'valid range
                If [Enum].IsDefined(GetType(CouponStatus), Temp) = True Then

                    Return Temp

                Else

                    Throw New InvalidOperationException("Enum value out of range")

                End If

            Catch ex As ArgumentNullException

                Throw ex       'enum type or value is nothing

            Catch ex As ArgumentException

                Throw ex      'not an enum type or value is empty or blank

            End Try

        End Get
        Set(ByVal value As InterOp.Interface.CouponStatus)

            _Status = value.ToString

        End Set
    End Property

    <ColumnMapping("EXCCOUPON")> _
    Public Property Exclusive() As Boolean Implements ISaleCoupon.Exclusive
        Get
            Return _ExcCoupon
        End Get
        Set(ByVal value As Boolean)
            _ExcCoupon = value
        End Set
    End Property

    <ColumnMapping("MARKETREF")> _
    Public Property MarketReference() As String Implements ISaleCoupon.MarketReference
        Get
            Return _MarketRef
        End Get
        Set(ByVal value As String)
            _MarketRef = value
        End Set
    End Property

    <ColumnMapping("REUSABLE")> _
    Public Property Reusable() As Boolean Implements ISaleCoupon.Reusable
        Get
            Return _ReUsable
        End Get
        Set(ByVal value As Boolean)
            _ReUsable = value
        End Set
    End Property

    <ColumnMapping("ISSUESTORENO")> _
    Public Property IssuingStoreNumber() As String Implements ISaleCoupon.IssuingStoreNumber
        Get
            Return _IssueStoreNo
        End Get
        Set(ByVal value As String)
            _IssueStoreNo = value
        End Set
    End Property

    <ColumnMapping("SERIALNO")> _
    Public Property SerialNumber() As String Implements ISaleCoupon.SerialNumber
        Get
            Return _SerialNo
        End Get
        Set(ByVal value As String)
            _SerialNo = value
        End Set
    End Property

    <ColumnMapping("MANAGERID")> _
    Public Property ManagerID() As String Implements ISaleCoupon.ManagerID
        Get
            Return _ManagerID
        End Get
        Set(ByVal value As String)
            _ManagerID = value
        End Set
    End Property

    <ColumnMapping("RTIFLAG")> _
    Public Property RtiFlag() As String Implements ISaleCoupon.RtiFlag
        Get
            Return _RTIFlag
        End Get
        Set(ByVal value As String)
            _RTIFlag = value
        End Set
    End Property

#End Region

#Region "Properties"

    Public ReadOnly Property BarData() As String Implements ISaleCoupon.BarData
        Get
            BarData = ""
            If _CouponID IsNot Nothing Then
                BarData = _CouponID
            End If
            If _MarketRef IsNot Nothing Then
                BarData &= _MarketRef
            End If
            If _SerialNo IsNot Nothing Then
                BarData &= _SerialNo
            End If
        End Get
    End Property

#End Region

End Class