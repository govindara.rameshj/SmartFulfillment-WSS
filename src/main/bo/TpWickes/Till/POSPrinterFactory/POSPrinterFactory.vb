﻿
Public Class POSPrinterFactory

    Private Shared _mFactoryMember As IPOSPrinter

    Public Shared Function FactoryGet() As IPOSPrinter

        If _mFactoryMember Is Nothing Then
            Return New POSPrinter
        Else
            Return _mFactoryMember   'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IPOSPrinter)

        _mFactoryMember = obj
    End Sub
End Class