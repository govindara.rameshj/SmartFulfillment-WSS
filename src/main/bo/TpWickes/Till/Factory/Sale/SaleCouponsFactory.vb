﻿
Public Class SaleCouponsFactory

    Private Shared _mFactoryMember As ISaleCoupons

    Public Shared Function FactoryGet() As ISaleCoupons

        If _mFactoryMember Is Nothing Then
            Return New SaleCoupons
        Else
            Return _mFactoryMember   'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ISaleCoupons)

        _mFactoryMember = obj
    End Sub
End Class
