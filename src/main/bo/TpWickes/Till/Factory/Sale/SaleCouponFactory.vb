﻿Public Class SaleCouponFactory

    Private Shared _mFactoryMember As ISaleCoupon

    Public Shared Function FactoryGet() As ISaleCoupon

        If _mFactoryMember Is Nothing Then
            Return New SaleCoupon
        Else
            Return _mFactoryMember   'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ISaleCoupon)

        _mFactoryMember = obj

    End Sub

End Class