﻿Public Class VolumeMovementFactory

    Private Shared _mFactoryMember As VolumeMovement.IVolumeMovement

    Public Shared Function FactoryGet() As VolumeMovement.IVolumeMovement

        If _mFactoryMember Is Nothing Then

            Return New WebService

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As VolumeMovement.IVolumeMovement)

        _mFactoryMember = obj

    End Sub

End Class