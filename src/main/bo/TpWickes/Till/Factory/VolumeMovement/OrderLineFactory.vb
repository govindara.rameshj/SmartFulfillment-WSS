﻿Public Class OrderLineFactory

    Private Shared _mFactoryMember As VolumeMovement.IOrderLine

    Public Shared Function FactoryGet() As VolumeMovement.IOrderLine

        If _mFactoryMember Is Nothing Then

            Return New OrderLine

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As VolumeMovement.IOrderLine)

        _mFactoryMember = obj

    End Sub

End Class