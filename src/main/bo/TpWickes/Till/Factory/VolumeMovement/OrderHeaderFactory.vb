﻿Public Class OrderHeaderFactory

    Private Shared _mFactoryMember As VolumeMovement.IOrderHeader

    Public Shared Function FactoryGet() As VolumeMovement.IOrderHeader

        If _mFactoryMember Is Nothing Then

            Return New OrderHeader

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As VolumeMovement.IOrderHeader)

        _mFactoryMember = obj

    End Sub

End Class