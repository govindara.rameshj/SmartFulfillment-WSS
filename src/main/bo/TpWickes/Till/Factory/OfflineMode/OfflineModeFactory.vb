﻿Public Class OfflineModeFactory

    Private Shared _mFactoryMember As IOfflineMode

    Public Shared Function FactoryGet() As IOfflineMode

        If _mFactoryMember Is Nothing Then
            Return New OfflineMode
        Else
            Return _mFactoryMember   'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IOfflineMode)

        _mFactoryMember = obj
    End Sub
End Class
