﻿Imports TpWickes.Library
Imports TpWickes.Till.Implementation.PriceMatch

Public Class PriceMatchFactory
    Inherits BaseFactory(Of PriceMatch.IPriceMatch)

    Public Overrides Function Implementation() As InterOp.Interface.PriceMatch.IPriceMatch
        Return New PriceMatchOverride
    End Function
End Class
