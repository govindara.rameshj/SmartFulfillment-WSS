﻿Public Class TemporaryPriceChangeEventFactory

    Private Shared _mFactoryMember As ITemporaryPriceChangeEvent

    Public Shared Function FactoryGet() As ITemporaryPriceChangeEvent

        If _mFactoryMember Is Nothing Then

            Return New TemporaryPriceChangeEvent

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ITemporaryPriceChangeEvent)

        _mFactoryMember = obj

    End Sub

End Class
