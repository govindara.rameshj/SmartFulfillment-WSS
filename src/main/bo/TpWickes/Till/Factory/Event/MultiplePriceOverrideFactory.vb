﻿Public Class MultiplePriceOverrideFactory
    Private Shared _mFactoryMember As IMultiplePriceOverride

    Public Shared Function FactoryGet() As IMultiplePriceOverride

        If _mFactoryMember Is Nothing Then

            Return New MultiplePriceOverride

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IMultiplePriceOverride)

        _mFactoryMember = obj

    End Sub

End Class
