﻿Public Class PrintCouponFactory

    Private Shared _mFactoryMember As IPrintCoupon

    Public Shared Function FactoryGet() As IPrintCoupon

        If _mFactoryMember Is Nothing Then
            Return New PrintCoupon
        Else
            Return _mFactoryMember   'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IPrintCoupon)

        _mFactoryMember = obj
    End Sub
End Class
