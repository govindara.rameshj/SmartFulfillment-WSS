﻿Public Class HeaderFactory

    Private Shared _mFactoryMember As IHeader

    Public Shared Function FactoryGet() As IHeader

        If _mFactoryMember Is Nothing Then
            Return New Header
        Else
            Return _mFactoryMember   'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IHeader)

        _mFactoryMember = obj

    End Sub

End Class