﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class TextCommunicationLine
    Inherits Base
    Implements ITextCommunicationLine

#Region "Table Fields"

    Friend _Id As Integer
    Friend _FunctionalAreaId As Integer
    Friend _TypeId As Integer
    Friend _Sequence As Integer
    Friend _Description As String
    Friend _SummaryText As String
    Friend _FullText As String
    Friend _LineDelimiter As String
    Friend _ParagraphDelimiter As String
    Friend _FieldStartDelimiter As String
    Friend _FieldEndDelimiter As String
    Friend _Deleted As Boolean

    <ColumnMapping("Id")> _
    Public Property ID() As Integer Implements [Interface].ITextCommunicationLine.ID
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    <ColumnMapping("FunctionalAreaId")> _
    Public Property FunctionalAreaID() As Integer Implements [Interface].ITextCommunicationLine.FunctionalAreaID
        Get
            Return _FunctionalAreaId
        End Get
        Set(ByVal value As Integer)
            _FunctionalAreaId = value
        End Set
    End Property

    <ColumnMapping("TypeId")> _
    Public Property TypeID() As Integer Implements [Interface].ITextCommunicationLine.TypeID
        Get
            Return _TypeId
        End Get
        Set(ByVal value As Integer)
            _TypeId = value
        End Set
    End Property

    <ColumnMapping("Sequence")> _
    Public Property SequenceNumber() As Integer Implements [Interface].ITextCommunicationLine.SequenceNumber
        Get
            Return _Sequence
        End Get
        Set(ByVal value As Integer)
            _Sequence = value
        End Set
    End Property

    <ColumnMapping("Description")> _
    Public Property Description() As String Implements [Interface].ITextCommunicationLine.Description
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    <ColumnMapping("SummaryText")> _
    Public Property SummaryText() As String Implements [Interface].ITextCommunicationLine.SummaryText
        Get
            Return _SummaryText
        End Get
        Set(ByVal value As String)
            _SummaryText = value
        End Set
    End Property

    <ColumnMapping("FullText")> _
    Public Property FullText() As String Implements [Interface].ITextCommunicationLine.FullText
        Get
            Return _FullText
        End Get
        Set(ByVal value As String)
            _FullText = value
        End Set
    End Property

    <ColumnMapping("LineDelimiter")> _
    Public Property LineDelimiter() As String Implements [Interface].ITextCommunicationLine.LineDelimiter
        Get
            Return _LineDelimiter
        End Get
        Set(ByVal value As String)
            _LineDelimiter = value
        End Set
    End Property

    <ColumnMapping("ParagraphDelimiter")> _
    Public Property ParagraphDelimiter() As String Implements [Interface].ITextCommunicationLine.ParagraphDelimiter
        Get
            Return _ParagraphDelimiter
        End Get
        Set(ByVal value As String)
            _ParagraphDelimiter = value
        End Set
    End Property

    <ColumnMapping("FieldStartDelimiter")> _
    Public Property FieldStartDelimiter() As String Implements [Interface].ITextCommunicationLine.FieldStartDelimiter
        Get
            Return _FieldStartDelimiter
        End Get
        Set(ByVal value As String)
            _FieldStartDelimiter = value
        End Set
    End Property

    <ColumnMapping("FieldEndDelimiter")> _
    Public Property FieldEndDelimiter() As String Implements [Interface].ITextCommunicationLine.FieldEndDelimiter
        Get
            Return _FieldEndDelimiter
        End Get
        Set(ByVal value As String)
            _FieldEndDelimiter = value
        End Set
    End Property

    <ColumnMapping("Deleted")> _
    Public Property Deleted() As Boolean Implements [Interface].ITextCommunicationLine.Deleted
        Get
            Return _Deleted
        End Get
        Set(ByVal value As Boolean)
            _Deleted = value
        End Set
    End Property

#End Region

#Region "Properties"

    Public ReadOnly Property GetFullText() As String Implements [Interface].ITextCommunicationLine.GetFullText
        Get
            Dim Temp As String

            'this field in the database is typed as not nullable
            Temp = _FullText.Trim
            If _LineDelimiter IsNot Nothing Then Temp = Temp.Replace(LineDelimiter, "  ")
            If _ParagraphDelimiter IsNot Nothing Then Temp = Temp.Replace(ParagraphDelimiter, vbNewLine)

            Return Temp

            'Return _FullText.Replace(LineDelimiter, "  ").Replace(ParagraphDelimiter, vbNewLine)

        End Get
    End Property

    Public ReadOnly Property GetSummaryText() As String Implements [Interface].ITextCommunicationLine.GetSummaryText
        Get

            Dim Temp As String

            Temp = String.Empty
            If _SummaryText IsNot Nothing Then Temp = _SummaryText.Trim
            If _LineDelimiter IsNot Nothing Then Temp = Temp.Replace(LineDelimiter, "  ")
            If _ParagraphDelimiter IsNot Nothing Then Temp = Temp.Replace(ParagraphDelimiter, vbNewLine)

            Return Temp

            'Return _SummaryText.Replace(LineDelimiter, "  ").Replace(ParagraphDelimiter, vbNewLine)

        End Get
    End Property

#End Region

End Class