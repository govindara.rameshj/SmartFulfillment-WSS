﻿Public Class OfflineMode
    Implements IOfflineMode

    Public Function IsInOfflineMode() As Boolean Implements [Interface].IOfflineMode.IsInOfflineMode
        Dim OfflineModeRep As TpWickes.Core.Till.Repository.Interface.IOfflineModeRepository = TpWickes.Core.Till.Repository.Factory.OfflineModeRepositoryFactory.FactoryGet

        If OfflineModeRep IsNot Nothing Then
            Return OfflineModeRep.IsInOfflineMode
        End If
    End Function
End Class
