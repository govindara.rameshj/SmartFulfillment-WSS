﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class TextCommunicationLines
    Inherits BaseCollection(Of TextCommunicationLine)
    Implements ITextCommunicationLines

#Region "Properties"

    Friend _FunctionalAreaID As Integer
    Friend _TypeID As Integer
    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements ITextCommunicationLines.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

#End Region

#Region "Methods"

    Public Function Initialise(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer) As Boolean Implements ITextCommunicationLines.Initialise

        If FunctionalAreaID <> 0 AndAlso TypeID <> 0 Then

            _FunctionalAreaID = FunctionalAreaID
            _TypeID = TypeID

            Load(FunctionalAreaID, TypeID)
            Return True

        End If

        Return False

    End Function

    Public Function GetFullText() As String Implements ITextCommunicationLines.GetFullText

        GetFullText = ""

        For Each Line As ITextCommunicationLine In Items

            'If GetFullText.Length > 0 Then GetFullText &= vbNewLine & vbNewLine
            If GetFullText.Length > 0 AndAlso Line.GetFullText.Length > 0 Then GetFullText &= vbNewLine & vbNewLine

            GetFullText &= Line.GetFullText()

        Next

    End Function

    Public Function GetSummaryText() As String Implements ITextCommunicationLines.GetSummaryText

        GetSummaryText = ""

        For Each Line As ITextCommunicationLine In Items

            'If GetSummaryText.Length > 0 Then GetSummaryText &= vbNewLine & vbNewLine
            If GetSummaryText.Length > 0 AndAlso Line.GetSummaryText.Length > 0 Then GetSummaryText &= vbNewLine & vbNewLine

            GetSummaryText &= Line.GetSummaryText

        Next

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Overloads Sub Load(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer)

        Dim Repository As IGetTextCommunicationRepository
        Dim DT As DataTable

        Repository = GetTextCommunicationRepositoryFactory.FactoryGet

        DT = Repository.GetTextCommunications(FunctionalAreaID, TypeID)

        If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then

            Me.Load(DT)
            _ExistsInDB = True

        End If

    End Sub

    Friend Overloads Sub Load(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, ByVal SequenceNumber As Integer)

        Dim Repository As IGetTextCommunicationRepository
        Dim DT As DataTable

        Repository = GetTextCommunicationRepositoryFactory.FactoryGet

        DT = Repository.GetTextCommunications(FunctionalAreaID, TypeID, SequenceNumber)

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            Me.Load(DT)
            _ExistsInDB = True

        End If

    End Sub

#End Region

End Class