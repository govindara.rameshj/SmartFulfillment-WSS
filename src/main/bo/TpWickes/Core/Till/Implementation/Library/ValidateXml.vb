﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class ValidateXml
    Implements IValidateXml

    Private _ErrorOccurred As Boolean
    Private _ErrorMessage As String

    Public Sub New()

        _ErrorOccurred = False
        _ErrorMessage = String.Empty

    End Sub

#Region "Interface"

    Public Function ValideXML(ByVal Type As XsdType, ByVal Direction As XsdTypeDirection, _
                              ByVal XmlDocument As String, ByRef XmlError As String) As Boolean Implements IValidateXml.ValideXML

        Return CommonValidateXML(Type, Direction, XDocument.Parse(XmlDocument), XmlError)

    End Function

    Public Function ValideXML(ByVal Type As XsdType, ByVal Direction As XsdTypeDirection, _
                              ByVal XmlDocument As XDocument, ByRef XmlError As String) As Boolean Implements IValidateXml.ValideXML

        Return CommonValidateXML(Type, Direction, XmlDocument, XmlError)

    End Function

#End Region

#Region "Private Procedures And Functions"

    Friend Function CommonValidateXML(ByVal Type As XsdType, ByVal Direction As XsdTypeDirection, _
                                      ByVal XmlDocument As XDocument, ByRef XmlError As String) As Boolean

        Dim XsdPath As String
        Dim XsdName As String

        XsdPath = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText

        XsdName = ConfigXMLDoc.SelectSingleNode(TagType(Type) & TagDirection(Direction)).InnerText

        CommonValidateXML = Not Validate(XsdPath & XsdName, XmlDocument)
        XmlError = _ErrorMessage

    End Function

    Friend Function TagDirection(ByVal Direction As XsdTypeDirection) As String

        Select Case Direction

            Case XsdTypeDirection.Input

                TagDirection = "Input"

            Case XsdTypeDirection.Ouptut

                TagDirection = "Output"

            Case Else

                TagDirection = String.Empty

        End Select

    End Function

    Friend Function TagType(ByVal Type As XsdType) As String

        TagType = "Configuration/XSDFiles/"

        Select Case Type

            Case XsdType.StoreFulfilmentRequest
                TagType &= "CTSFulfilmentRequest/"

            Case XsdType.StoreCreateOrder
                TagType &= "CTSQODCreate/"

            Case XsdType.StoreCreateRefund
                TagType &= "CTSRefundCreate/"

            Case XsdType.StoreStatusNotification
                TagType &= "CTSStatusNotification/"

            Case XsdType.StoreUpdateRefund
                TagType &= "CTSUpdateRefund/"

            Case XsdType.OrderManagerCheckHowOrderIsFulfilled
                TagType &= "OrderManagerCheckFulfilment/"

            Case XsdType.OrderManagerCreateOrder
                TagType &= "OrderManagerCreateOrder/"

            Case XsdType.OrderManagerCreateRefund
                TagType &= "OrderManagerCreateRefund/"

            Case XsdType.OrderManagerOrderStatus
                TagType &= "OrderManagerStatusUpdate/"

            Case Else
                TagType &= String.Empty

        End Select

    End Function

    Friend Function Validate(ByVal XsdPathAndFileName As String, ByVal XmlDocument As XDocument) As Boolean

        Dim SR As StreamReader
        Dim XsdMarkup As XElement
        Dim Schemas As XmlSchemaSet = New XmlSchemaSet()

        SR = New StreamReader(XsdPathAndFileName)
        XsdMarkup = XElement.Parse(SR.ReadToEnd)

        Schemas = New XmlSchemaSet()
        Schemas.Add("", XsdMarkup.CreateReader)

        XmlDocument.Validate(Schemas, AddressOf XsdError)

        SR.Close()
        XsdMarkup = Nothing

        Return _ErrorOccurred

    End Function

    Friend Sub XsdError(ByVal o As Object, ByVal e As ValidationEventArgs)

        _ErrorOccurred = True
        _ErrorMessage = e.Message

    End Sub

#End Region

End Class