﻿Imports TpWickes.Core.Till.Implementation.Library

Public Class CouponHeader
    Implements ICouponHeader

    Private _gotExclusiveClause As Boolean
    Private _exclusiveClause As String
    Private _gotNonReuseClause As Boolean
    Private _nonReuseClause As String
    Private _gotTermsAndConditions As Boolean
    Private _termsAndConditions As String

#Region "ICouponHeader Implementation"

    Public Function GetExclusiveClause() As String Implements ICouponHeader.GetExclusiveClause

        If Not _gotExclusiveClause Then
            _exclusiveClause = ReadExclusiveClause()
            _gotExclusiveClause = True
        End If
        GetExclusiveClause = _exclusiveClause
    End Function

    Public Function GetNonReusableClause() As String Implements ICouponHeader.GetNonReusableClause

        If Not _gotNonReuseClause Then
            _nonReuseClause = ReadNonReuseClause()
            _gotNonReuseClause = True
        End If
        GetNonReusableClause = _nonReuseClause
    End Function

    Public Function GetTermsAndConditionsText() As String Implements ICouponHeader.GetTermsAndConditionsText

        If Not _gotTermsAndConditions Then
            _termsAndConditions = ReadTermsAndConditions()
            _gotTermsAndConditions = True
        End If
        GetTermsAndConditionsText = _termsAndConditions
    End Function
#End Region

    Friend Function ReadExclusiveClause() As String

        ReadExclusiveClause = ReadCouponTextCommunication(TextCommunicationCouponTypes.ExclusiveClause).GetFullText()
    End Function

    Friend Function ReadNonReuseClause() As String

        ReadNonReuseClause = ReadCouponTextCommunication(TextCommunicationCouponTypes.NonReuseClause).GetFullText()
    End Function

    Friend Function ReadTermsAndConditions() As String

        ReadTermsAndConditions = ReadCouponTextCommunication(TextCommunicationCouponTypes.TermsAndConditions).GetFullText()
    End Function

    Friend Function ReadCouponTextCommunication(ByVal CouponType As TextCommunicationCouponTypes) As ITextCommunicationLines

        ReadCouponTextCommunication = New TextCommunicationLines
        ReadCouponTextCommunication.Initialise(TextCommunicationFunctionalAreas.Coupons, CouponType)
    End Function
End Class
