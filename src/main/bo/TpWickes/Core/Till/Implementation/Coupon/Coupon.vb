﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class Coupon
    Inherits Base
    Implements ICoupon

#Region "Table Fields"

    Friend _COUPONID As String
    Friend _DESCRIPTION As String
    Friend _STOREGEN As Boolean
    Friend _SERIALNO As Boolean
    Friend _EXCCOUPON As Boolean
    Friend _REUSABLE As Boolean
    Friend _COLLECTINFO As Boolean
    Friend _DELETED As Boolean

    <ColumnMapping("COUPONID")> _
    Public Property CouponID() As String Implements ICoupon.CouponID
        Get
            CouponID = _COUPONID
        End Get
        Set(ByVal value As String)
            _COUPONID = value
        End Set
    End Property

    <ColumnMapping("DESCRIPTION")> _
    Public Property Description() As String Implements ICoupon.Description
        Get
            Description = _DESCRIPTION
        End Get
        Set(ByVal value As String)
            _DESCRIPTION = value
        End Set
    End Property

    <ColumnMapping("STOREGEN")> _
    Public Property StoreGenerated() As Boolean Implements ICoupon.StoreGenerated
        Get
            StoreGenerated = _STOREGEN
        End Get
        Set(ByVal value As Boolean)
            _STOREGEN = value
        End Set
    End Property

    <ColumnMapping("SERIALNO")> _
    Public Property SerialNumber() As Boolean Implements ICoupon.SerialNumber
        Get
            SerialNumber = _SERIALNO
        End Get
        Set(ByVal value As Boolean)
            _SERIALNO = value
        End Set
    End Property

    <ColumnMapping("EXCCOUPON")> _
    Public Property Exclusive() As Boolean Implements ICoupon.Exclusive
        Get
            Exclusive = _EXCCOUPON
        End Get
        Set(ByVal value As Boolean)
            _EXCCOUPON = value
        End Set
    End Property

    <ColumnMapping("REUSABLE")> _
    Public Property Reusable() As Boolean Implements ICoupon.Reusable
        Get
            Reusable = _REUSABLE
        End Get
        Set(ByVal value As Boolean)
            _REUSABLE = value
        End Set
    End Property

    <ColumnMapping("COLLECTINFO")> _
    Public Property RequiresCustomerInfo() As Boolean Implements ICoupon.RequiresCustomerInfo
        Get
            RequiresCustomerInfo = _COLLECTINFO
        End Get
        Set(ByVal value As Boolean)
            _COLLECTINFO = value
        End Set
    End Property

    <ColumnMapping("DELETED")> _
    Public Property Deleted() As Boolean Implements ICoupon.Deleted
        Get
            Deleted = _DELETED
        End Get
        Set(ByVal value As Boolean)
            _DELETED = value
        End Set
    End Property

#End Region

#Region "Properties"

    Friend _ExistsInDB As Boolean
    Friend _TextLines As ICouponTextLines

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements ICoupon.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

    'Public Property TextLines() As ICouponTextLines Implements ICoupon.TextLines
    '    Get
    '        Return _TextLines
    '    End Get
    '    Set(ByVal value As ICouponTextLines)
    '        _TextLines = value
    '    End Set
    'End Property

#End Region

#Region "Methods"

    Public Function Initialise(ByVal CouponId As String, ByVal Deleted As Boolean) As Boolean Implements ICoupon.Initialise

        If CouponId IsNot Nothing AndAlso CouponId.Trim.Length <> 0 Then

            Load(CouponId, Deleted)
            Return True

        End If

        Return False

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Overloads Sub Load(ByVal CouponID As String, ByVal Deleted As Boolean)

        Dim Repository As IGetCouponRepository
        Dim DR As DataRow

        Repository = GetCouponRepositoryFactory.FactoryGet

        If Deleted = True Then

            DR = Repository.GetCouponDeletedTrue(CouponID)

        Else

            DR = Repository.GetCouponDeletedFalse(CouponID)

        End If

        If DR IsNot Nothing Then

            Me.Load(DR)

            _ExistsInDB = True

        End If

    End Sub

#End Region

End Class