﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class CouponTextLines
    Inherits BaseCollection(Of CouponTextLine)
    Implements ICouponTextLines


#Region "Properties"

    Friend _CouponId As String
    Friend _ExistsInDB As Boolean

    Public ReadOnly Property ExistsInDatabase() As Boolean Implements ICouponTextLines.ExistsInDatabase
        Get
            Return _ExistsInDB
        End Get
    End Property

#End Region

#Region "Methods"

    Public Function Initialise(ByVal CouponId As String, ByVal Deleted As Boolean) As Boolean Implements ICouponTextLines.Initialise

        If CouponId IsNot Nothing AndAlso CouponId.Trim.Length <> 0 Then
            _CouponId = CouponId
            Load(CouponId, Deleted)

            Return True
        End If

        Return False
    End Function

    'Public ReadOnly Property List() As System.Collections.Generic.IList(Of [Interface].ICouponTextLine) Implements [Interface].ICouponTextLines.List
    '    Get
    '        List = New System.Collections.Generic.List(Of CouponTextLine)
    '        For Each Item As CouponTextLine In Items
    '            List.Add(Item)
    '        Next
    '    End Get
    'End Property

    Public ReadOnly Property LineCount() As Integer Implements ICouponTextLines.LineCount
        Get
            Return Me.Count
        End Get
    End Property

    Public ReadOnly Property Line(ByVal Index As Integer) As ICouponTextLine Implements ICouponTextLines.Line
        Get
            Return Me.Item(Index)
        End Get
    End Property
#End Region

#Region "Private Procedures & Functions"

    Friend Overloads Sub Load(ByVal CouponID As String, ByVal Deleted As Boolean)
        Dim Repository As IGetCouponTextRepository
        Dim DT As DataTable

        Repository = GetCouponTextRepositoryFactory.FactoryGet
        If Deleted = True Then
            DT = Repository.GetCouponTextIncludeDeletedTrue(CouponID)
        Else
            DT = Repository.GetCouponTextIncludeDeletedFalse(CouponID)
        End If

        If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
            Me.Load(DT)
            _ExistsInDB = True
        End If
    End Sub

    Friend Overloads Sub Load(ByVal CouponID As String, ByVal SequenceNumber As String, ByVal Deleted As Boolean)
        Dim Repository As IGetCouponTextRepository
        Dim DT As DataTable

        Repository = GetCouponTextRepositoryFactory.FactoryGet
        If Deleted = True Then
            DT = Repository.GetCouponTextIncludeDeletedTrue(CouponID, SequenceNumber)
        Else
            DT = Repository.GetCouponTextIncludeDeletedFalse(CouponID, SequenceNumber)
        End If
        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then
            Me.Load(DT)
            _ExistsInDB = True
        End If
    End Sub
#End Region

End Class
