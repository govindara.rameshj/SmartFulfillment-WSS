﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Till.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                        "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                        "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                        "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                        "6da046ba")> 
Public Class CouponTextLine
    Inherits Base
    Implements ICouponTextLine

#Region "Table Fields"

    Friend _COUPONID As String
    Friend _SEQUENCENO As String
    Friend _PRINTSIZE As String
    Friend _TEXTALIGN As String
    Friend _PRINTTEXT As String
    Friend _DELETED As Boolean

    <ColumnMapping("COUPONID")> _
    Public Property CouponID() As String Implements ICouponTextLine.CouponID
        Get
            Return _COUPONID
        End Get
        Set(ByVal value As String)
            _COUPONID = value
        End Set
    End Property

    <ColumnMapping("SEQUENCENO")> _
    Public Property SequenceNumber() As String Implements ICouponTextLine.SequenceNumber
        Get

            Dim X As Integer

            If Integer.TryParse(_SEQUENCENO, X) = False Then X = -1
            Return X.ToString

        End Get
        Set(ByVal value As String)
            _SEQUENCENO = value
        End Set
    End Property

    <ColumnMapping("PRINTSIZE")> _
    Friend WriteOnly Property PrivatePrintSize() As String

        Set(ByVal value As String)
            _PRINTSIZE = value
        End Set

    End Property

    Public Property PrintSize() As CouponTextPrintSize Implements ICouponTextLine.PrintSize
        Get

            If _PRINTSIZE Is Nothing Then Return CouponTextPrintSize.Normal

            If _PRINTSIZE.Length <> 1 Then Return CouponTextPrintSize.Normal

            If String.Compare(_PRINTSIZE, "s", True) = 0 Then
                PrintSize = CouponTextPrintSize.Small
            ElseIf String.Compare(_PRINTSIZE, "m", True) = 0 Then
                PrintSize = CouponTextPrintSize.Medium
            ElseIf String.Compare(_PRINTSIZE, "l", True) = 0 Then
                PrintSize = CouponTextPrintSize.Large
            ElseIf String.Compare(_PRINTSIZE, "h", True) = 0 Then
                PrintSize = CouponTextPrintSize.Huge
            Else
                PrintSize = CouponTextPrintSize.Normal
            End If

            'Try
            '    Dim Temp As CouponTextPrintSize

            '    Temp = CType([Enum].Parse(GetType(CouponTextPrintSize), _PRINTSIZE), CouponTextPrintSize)

            '    'valid range
            '    If [Enum].IsDefined(GetType(CouponTextPrintSize), Temp) = True Then

            '        Return Temp

            '    Else

            '        Throw New InvalidOperationException("Enum value out of range")

            '    End If

            'Catch ex As ArgumentNullException

            '    Throw ex       'enum type or value is nothing

            'Catch ex As ArgumentException

            '    Throw ex      'not an enum type or value is empty or blank

            'End Try

        End Get
        Set(ByVal value As CouponTextPrintSize)

            Select Case value

                Case CouponTextPrintSize.Small
                    _PRINTSIZE = "S"

                Case CouponTextPrintSize.Medium
                    _PRINTSIZE = "M"

                Case CouponTextPrintSize.Large
                    _PRINTSIZE = "L"

                Case CouponTextPrintSize.Normal
                    _PRINTSIZE = "N"

                Case CouponTextPrintSize.Huge
                    _PRINTSIZE = "H"
            End Select

        End Set
    End Property

    <ColumnMapping("TEXTALIGN")> _
    Friend WriteOnly Property PrivateTextAlign() As String

        Set(ByVal value As String)
            _TEXTALIGN = value
        End Set

    End Property

    Public Property TextAlignment() As CouponTextAlignment Implements ICouponTextLine.TextAlignment
        Get

            If _TEXTALIGN Is Nothing Then Return CouponTextAlignment.Left

            If _TEXTALIGN.Length <> 1 Then Return CouponTextAlignment.Left

            If String.Compare(_TEXTALIGN, "l", True) = 0 Then
                TextAlignment = CouponTextAlignment.Left
            ElseIf String.Compare(_TEXTALIGN, "c", True) = 0 Then
                TextAlignment = CouponTextAlignment.Centre
            ElseIf String.Compare(_TEXTALIGN, "r", True) = 0 Then
                TextAlignment = CouponTextAlignment.Right
            Else
                TextAlignment = CouponTextAlignment.Left
            End If

            'Try
            '    Dim Temp As CouponTextAlignment

            '    Temp = CType([Enum].Parse(GetType(CouponTextAlignment), _TEXTALIGN), CouponTextAlignment)

            '    'valid range
            '    If [Enum].IsDefined(GetType(CouponTextAlignment), Temp) = True Then

            '        Return Temp

            '    Else

            '        Throw New InvalidOperationException("Enum value out of range")

            '    End If

            'Catch ex As ArgumentNullException

            '    Throw ex       'enum type or value is nothing

            'Catch ex As ArgumentException

            '    Throw ex      'not an enum type or value is empty or blank

            'End Try

        End Get
        Set(ByVal value As CouponTextAlignment)

            Select Case value

                Case CouponTextAlignment.Left
                    _TEXTALIGN = "L"

                Case CouponTextAlignment.Centre
                    _TEXTALIGN = "C"

                Case CouponTextAlignment.Right
                    _TEXTALIGN = "R"

            End Select

        End Set
    End Property

    <ColumnMapping("PRINTTEXT")> _
    Public Property PrintText() As String Implements ICouponTextLine.PrintText
        Get
            Return _PRINTTEXT
        End Get
        Set(ByVal value As String)
            _PRINTTEXT = value
        End Set
    End Property

    <ColumnMapping("DELETED")> _
    Public Property Deleted() As Boolean Implements ICouponTextLine.Deleted
        Get
            Return _DELETED
        End Get
        Set(ByVal value As Boolean)
            _DELETED = value
        End Set
    End Property

#End Region

#Region "Properties"

    Public ReadOnly Property PrintTextSubstring(Optional ByVal StringLength As Integer = -1) As String Implements ICouponTextLine.PrintTextSubstring
        Get

            If StringLength <= -1 Then Return _PRINTTEXT

            If StringLength <= _PRINTTEXT.Length Then Return _PRINTTEXT.Substring(0, StringLength)

            Return _PRINTTEXT

        End Get
    End Property

#End Region

End Class