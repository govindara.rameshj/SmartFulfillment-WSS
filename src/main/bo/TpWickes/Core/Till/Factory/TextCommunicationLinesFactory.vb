﻿Public Class TextCommunicationLinesFactory

    Private Shared _mFactoryMember As ITextCommunicationLines

    Public Shared Function FactoryGet() As ITextCommunicationLines

        If _mFactoryMember Is Nothing Then

            Return New TextCommunicationLines

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ITextCommunicationLines)

        _mFactoryMember = obj

    End Sub

End Class
