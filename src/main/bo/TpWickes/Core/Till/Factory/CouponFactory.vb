﻿Public Class CouponFactory

    Private Shared _mFactoryMember As ICoupon

    Public Shared Function FactoryGet() As ICoupon

        If _mFactoryMember Is Nothing Then
            Return New Coupon
        Else
            Return _mFactoryMember   'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ICoupon)

        _mFactoryMember = obj

    End Sub

End Class