﻿Public Class CouponTextLinesFactory

    Private Shared _mFactoryMember As ICouponTextLines

    Public Shared Function FactoryGet() As ICouponTextLines

        If _mFactoryMember Is Nothing Then
            Return New CouponTextLines
        Else
            Return _mFactoryMember   'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ICouponTextLines)

        _mFactoryMember = obj

    End Sub

End Class