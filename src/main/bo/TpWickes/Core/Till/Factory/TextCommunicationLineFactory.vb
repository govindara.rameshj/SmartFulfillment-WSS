﻿Public Class TextCommunicationLineFactory

    Private Shared _mFactoryMember As ITextCommunicationLine

    Public Shared Function FactoryGet() As ITextCommunicationLine

        If _mFactoryMember Is Nothing Then

            Return New TextCommunicationLine

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ITextCommunicationLine)

        _mFactoryMember = obj

    End Sub

End Class
