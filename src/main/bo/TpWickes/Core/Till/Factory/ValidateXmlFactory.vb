﻿Public Class ValidateXmlFactory

    Private Shared _mFactoryMember As IValidateXml

    Public Shared Function FactoryGet() As IValidateXml

        If _mFactoryMember Is Nothing Then

            Return New ValidateXml

        Else

            Return _mFactoryMember

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IValidateXml)

        _mFactoryMember = obj

    End Sub

End Class