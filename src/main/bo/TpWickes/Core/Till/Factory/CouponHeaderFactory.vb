﻿Public Class CouponHeaderFactory

    Private Shared _mFactoryMember As ICouponHeader

    Public Shared Function FactoryGet() As ICouponHeader

        If _mFactoryMember Is Nothing Then
            Return New CouponHeader
        Else
            Return _mFactoryMember   'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ICouponHeader)

        _mFactoryMember = obj
    End Sub
End Class
