﻿Public Class CouponTextLineFactory

    Private Shared _mFactoryMember As ICouponTextLine

    Public Shared Function FactoryGet() As ICouponTextLine

        If _mFactoryMember Is Nothing Then
            Return New CouponTextLine
        Else
            Return _mFactoryMember   'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ICouponTextLine)

        _mFactoryMember = obj

    End Sub

End Class