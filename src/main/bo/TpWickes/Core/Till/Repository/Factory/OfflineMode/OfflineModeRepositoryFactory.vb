﻿Public Class OfflineModeRepositoryFactory
    Private Shared m_FactoryMember As IOfflineModeRepository = Nothing

    Public Shared Function FactoryGet() As IOfflineModeRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New OfflineModeRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IOfflineModeRepository)
        m_FactoryMember = obj
    End Sub

End Class
