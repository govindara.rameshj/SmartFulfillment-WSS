﻿Public Interface IOfflineModeRepository

    Sub SetOfflineMode(ByVal InOfflineMode As Boolean)
    Function IsInOfflineMode() As Boolean
    ReadOnly Property RequirementSwitchId() As Integer
    Property LocalConnection() As OfflineModeConnection
    Property LocalConnectionCommand(ByVal LocalConnection As IConnection) As ICommand
    Function RequirementIsEnabled() As Boolean
    Sub AddGetParameterStoredProcedure(ByRef X As ICommand)
    Sub AddParameterIDParameter(ByRef X As ICommand, ByVal ParameterId As Integer)
    Function LocalSQLExpressServiceExistsAndIsRunning() As Boolean
End Interface
