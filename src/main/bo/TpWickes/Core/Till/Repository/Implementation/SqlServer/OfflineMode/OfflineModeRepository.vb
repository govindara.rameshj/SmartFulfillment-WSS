﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Core.Till.Repository.Implementation.IntegrationTest, " & _
                                                       "PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                       "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                       "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                       "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                       "6da046ba" _
                                                       )> 
Public Class OfflineModeRepository
    Implements IOfflineModeRepository

    Private _localConnection As IConnection
    Private _localConnectionCommand As ICommand

    Public Sub SetOfflineMode(ByVal InOfflineMode As Boolean) Implements [Interface].IOfflineModeRepository.SetOfflineMode

        Try
            If LocalSQLExpressServiceExistsAndIsRunning() Then
                ' Always set on the local (offline) database
                Using con As OfflineModeConnection = LocalConnection()
                    Using Com As ICommand = LocalConnectionCommand(con)

                        AddSetStoredProcedure(Com)
                        AddInOfflineModeParameter(Com, InOfflineMode)
                        Com.ExecuteNonQuery()
                    End Using
                End Using
            Else
                Throw New Exception("Failed to Set OfflineMode to '" & InOfflineMode.ToString & "'.  No instance of SQLExpress running locally.")
            End If
        Catch ex As Exception
            Throw New Exception("Failed to Set OfflineMode to '" & InOfflineMode.ToString & "'")
        End Try
    End Sub

    Public Function IsInOfflineMode() As Boolean Implements [Interface].IOfflineModeRepository.IsInOfflineMode

        Try
            If LocalSQLExpressServiceExistsAndIsRunning() Then
                ' Always use the flag in the local (offline) database
                Using con As OfflineModeConnection = LocalConnection()
                    Using Com As ICommand = LocalConnectionCommand(con)
                        Dim dt As DataTable

                        AddGetStoredProcedure(Com)
                        dt = Com.ExecuteDataTable
                        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                            Return CBool(dt.Rows(0).Item("BooleanValue"))
                        Else
                            Return False
                        End If
                    End Using
                End Using
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function RequirementIsEnabled() As Boolean Implements [Interface].IOfflineModeRepository.RequirementIsEnabled

        Try
            If LocalSQLExpressServiceExistsAndIsRunning() Then
                ' Always use the flag in the local (offline) database
                Using con As OfflineModeConnection = LocalConnection()
                    Using Com As ICommand = LocalConnectionCommand(con)
                        Dim dt As DataTable

                        AddGetParameterStoredProcedure(Com)
                        AddParameterIDParameter(Com, RequirementSwitchID)
                        dt = Com.ExecuteDataTable
                        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                            Return CBool(dt.Rows(0).Item("BooleanValue"))
                        Else
                            Return False
                        End If
                    End Using
                End Using
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Friend Property LocalConnection() As OfflineModeConnection Implements IOfflineModeRepository.LocalConnection
        Get
            If _localConnection Is Nothing Then
                Return New OfflineModeConnection
            Else
                Return CType(_localConnection, OfflineModeConnection)
            End If
        End Get
        Set(ByVal value As OfflineModeConnection)
            _localConnection = value
        End Set
    End Property

    Friend Property LocalConnectionCommand(ByVal LocalConnection As IConnection) As ICommand Implements IOfflineModeRepository.LocalConnectionCommand
        Get
            If _localConnectionCommand Is Nothing Then
                Return CommandFactory.FactoryGet(LocalConnection)
            Else
                Return _localConnectionCommand
            End If
        End Get
        Set(ByVal value As ICommand)
            _localConnectionCommand = value
        End Set
    End Property

    Friend Sub AddSetStoredProcedure(ByRef X As ICommand)

        X.StoredProcedureName = My.Resources.Procedures.OfflineModeSet
    End Sub

    Friend Sub AddGetStoredProcedure(ByRef X As ICommand)

        X.StoredProcedureName = My.Resources.Procedures.OfflineModeGet
    End Sub

    Friend Sub AddInOfflineModeParameter(ByRef X As ICommand, ByVal InOfflineMode As Boolean)

        X.AddParameter(My.Resources.Parameters.InOfflineMode, IIf(InOfflineMode, 1, 0), SqlDbType.Bit)
    End Sub

    Friend ReadOnly Property RequirementSwitchID() As Integer Implements [Interface].IOfflineModeRepository.RequirementSwitchId
        Get
            RequirementSwitchID = 984151
        End Get
    End Property

    Friend Sub AddGetParameterStoredProcedure(ByRef X As ICommand) Implements [Interface].IOfflineModeRepository.AddGetParameterStoredProcedure

        X.StoredProcedureName = My.Resources.Procedures.SystemGetParameter
    End Sub

    Friend Sub AddParameterIDParameter(ByRef X As ICommand, ByVal ParameterId As Integer) Implements [Interface].IOfflineModeRepository.AddParameterIDParameter

        X.AddParameter(My.Resources.Parameters.ParameterId, ParameterId)
    End Sub

    Friend Function LocalSQLExpressServiceExistsAndIsRunning() As Boolean Implements IOfflineModeRepository.LocalSQLExpressServiceExistsAndIsRunning
        Dim sqlServerService As ServiceController = New ServiceController("MSSQL$SQLEXPRESS")

        If sqlServerService IsNot Nothing Then
            Return sqlServerService.Status = ServiceControllerStatus.Running
        End If
    End Function
End Class
