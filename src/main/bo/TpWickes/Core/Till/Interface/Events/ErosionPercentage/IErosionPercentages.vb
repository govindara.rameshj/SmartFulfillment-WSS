﻿Public Interface IErosionPercentages

    Property EventNumber() As String
    ReadOnly Property ErosionPercentageList() As List(Of IErosionPercentage)

    Sub Add(ByVal Sku As String, ByVal Quantity As System.Nullable(Of Integer), ByVal SellingPrice As System.Nullable(Of Double))
    Sub GenerateErosionPrices()

End Interface
