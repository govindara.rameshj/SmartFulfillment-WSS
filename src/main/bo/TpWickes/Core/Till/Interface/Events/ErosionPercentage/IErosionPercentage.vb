﻿Public Interface IErosionPercentage

    Property Sku() As String
    Property Quantity() As System.Nullable(Of Integer)
    Property SellingPrice() As System.Nullable(Of Double)
    Property ErosionPercentage() As System.Nullable(Of Double)

End Interface