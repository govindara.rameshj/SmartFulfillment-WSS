﻿Public Interface IEventOverrideHeaders

    ReadOnly Property ExistsInDatabase() As Boolean
    ReadOnly Property CollectionCount() As Integer
    ReadOnly Property ItemObject(ByVal Index As Integer) As IEventOverrideHeader

    Sub Initialise(ByVal HeaderDT As DataTable, ByVal DetailDT As DataTable)

    Function OverridePrice() As System.Nullable(Of Decimal)

End Interface
