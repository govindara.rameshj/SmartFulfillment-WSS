﻿Public Interface IEventOverrideMasters

    ReadOnly Property ExistsInDatabase() As Boolean
    ReadOnly Property CollectionCount() As Integer
    ReadOnly Property ItemObject(ByVal Index As Integer) As IEventOverrideMaster

    Sub Initialise(ByVal DetailDT As DataTable)

End Interface
