﻿Public Interface IEventExistingMaster

    Property EventType() As String                                  'EVTMAS -> TYPE
    Property EventKeyOne() As String                                'EVTMAS -> KEY1
    'Property EventKeyTwo() As String                                'EVTMAS -> KEY2
    Property EventID() As String                                    'EVTMAS -> NUMB
    Property Priority() As String                                   'EVTMAS -> PRIO
    Property Deleted() As Boolean                                   'EVTMAS -> IDEL
    Property DayOfWeekEvent() As Boolean                            'EVTMAS -> IDOW
    Property StartDate() As System.Nullable(Of Date)                'EVTMAS -> SDAT
    Property EndDate() As System.Nullable(Of Date)                  'EVTMAS -> EDAT
    Property Price() As System.Nullable(Of Decimal)                 'EVTENQ -> PRIC
    'Property BuyQuantity() As System.Nullable(Of Decimal)           'EVTENQ -> BQTY
    'Property GetQuantity() As System.Nullable(Of Decimal)           'EVTENQ -> GQTY
    'Property ValueDiscount() As System.Nullable(Of Decimal)         'EVTENQ -> VDIS
    'Property PercentageDiscount() As System.Nullable(Of Decimal)    'EVTENQ -> PDIS
    'Property BuyCoupon() As String                                  'EVTENQ -> BUYCPN
    'Property GetCoupon() As String                                  'EVTENQ -> GETCPN

End Interface