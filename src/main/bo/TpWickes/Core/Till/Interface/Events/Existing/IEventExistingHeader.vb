﻿Public Interface IEventExistingHeader

    Property EventID() As String                     'EVTENQ -> NUMB
    'Property Description() As String                 'EVTENQ -> DESCR
    Property Priority() As String                    'EVTENQ -> PRIO
    'Property StartDate() As System.Nullable(Of Date) 'EVTENQ -> SDAT
    'Property StartTime() As String                   'EVTENQ -> STIM
    'Property EndDate() As System.Nullable(Of Date)   'EVTENQ -> EDAT
    'Property EndTime() As String                     'EVTENQ -> ETIM
    Property Monday() As Boolean                     'EVTENQ -> DACT1
    Property Tuesday() As Boolean                    'EVTENQ -> DACT2
    Property Wednesday() As Boolean                  'EVTENQ -> DACT3
    Property Thursday() As Boolean                   'EVTENQ -> DACT4
    Property Friday() As Boolean                     'EVTENQ -> DACT5
    Property Saturday() As Boolean                   'EVTENQ -> DACT6
    Property Sunday() As Boolean                     'EVTENQ -> DACT7
    Property Deleted() As Boolean                    'EVTENQ -> IDEL

    Property MasterCollection() As IEventExistingMasters

End Interface
