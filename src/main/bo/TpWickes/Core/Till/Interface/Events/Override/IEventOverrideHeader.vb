﻿Public Interface IEventOverrideHeader

    Property ID() As Integer                               'EventHeaderOverride -> ID
    Property EventTypeID() As Integer                      'EventHeaderOverride -> EventTypeID
    Property Description() As String                       'EventHeaderOverride -> Description
    Property StartDate() As Date                           'EventHeaderOverride -> StartDate
    Property EndDate() As Date                             'EventHeaderOverride -> EndDate
    Property RevisionNumber() As Integer                   'EventHeaderOverride -> Revision
    Property CashierSecurityID() As Integer                'EventHeaderOverride -> CashierSecurityID
    Property AuthorisorSecurityID() As Integer             'EventHeaderOverride -> AuthorisorSecurityID
    'Property DateTimeStamp() As Date                       'EventHeaderOverride -> DateTimeStamp

    Property MasterCollection() As IEventOverrideMasters

End Interface
