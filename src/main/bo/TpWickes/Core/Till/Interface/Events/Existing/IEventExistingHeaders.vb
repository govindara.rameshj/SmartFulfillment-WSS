﻿Public Interface IEventExistingHeaders

    ReadOnly Property ExistsInDatabase() As Boolean
    ReadOnly Property CollectionCount() As Integer
    ReadOnly Property ItemObject(ByVal Index As Integer) As IEventExistingHeader

    Sub Initialise(ByVal HeaderDT As DataTable, ByVal DetailDT As DataTable)

    Function ExistingPrice(ByVal Today As Date) As System.Nullable(Of Decimal)
    Function ExistingEventNumber(ByVal Today As Date) As String

End Interface
