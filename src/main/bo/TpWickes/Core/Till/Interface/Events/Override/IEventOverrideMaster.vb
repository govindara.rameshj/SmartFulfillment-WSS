﻿Public Interface IEventOverrideMaster

    Property ID() As Integer         'EventDetailOverride -> ID
    Property HeaderID() As Integer   'EventDetailOverride -> EventHeaderOverrideID
    Property Sku() As String         'EventDetailOverride -> SkuNumber
    Property Price() As Decimal      'EventHeaderOverride -> Price

End Interface
