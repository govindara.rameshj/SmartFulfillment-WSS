﻿
Public Enum CouponTextPrintSize
    Normal = 0
    Small = 1
    Medium = 2
    Large = 3
    Huge = 4
End Enum

Public Enum CouponTextAlignment
    Left = 0
    Centre = 1
    Right = 2
End Enum

Public Enum TextCommunicationFunctionalAreas
    Coupons = 1
    Volume = 2
End Enum

Public Enum TextCommunicationCouponTypes
    TermsAndConditions = 1
    ExclusiveClause = 2
    NonReuseClause = 3
End Enum

Public Enum TextCommunicationVolumeMovementTypes
    OrderFulfilledByMessageEstore = 4
    OrderFulfilledByMessageHub = 5
    OrderFulfilledByMessageSplit = 6
    OrderFulfilledByMessageFailure = 7
End Enum

Public Enum EventTypeEnumerator
    TemporaryPriceOverrideSKU = 1
    TemporaryPriceOverrideMixAndMatchNotImplemented = 2
    TemporaryPriceOverrideDealGroup = 3
End Enum

Public Enum XsdType

    StoreFulfilmentRequest
    StoreStatusNotification
    StoreUpdateRefund
    StoreCreateOrder
    StoreCreateRefund

    OrderManagerCreateOrder
    OrderManagerCreateRefund
    OrderManagerOrderStatus
    OrderManagerCheckHowOrderIsFulfilled

End Enum

Public Enum XsdTypeDirection
    Input
    Ouptut
End Enum