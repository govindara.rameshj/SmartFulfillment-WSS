﻿'Interface does not need to be exposed to the till, moved from TpWickes.Interop.Interface

Public Interface ICouponTextLine

    Property CouponID() As String                              'CouponText -> COUPONID
    Property SequenceNumber() As String                        'CouponText -> SEQUENCENO
    Property PrintSize() As CouponTextPrintSize                'CouponText -> PRINTSIZE
    Property TextAlignment() As CouponTextAlignment            'CouponText -> TEXTALIGN
    Property PrintText() As String                             'CouponText -> PRINTTEXT
    Property Deleted() As Boolean                              'CouponText -> DELETED

    ReadOnly Property PrintTextSubstring(Optional ByVal StringLength As Integer = -1) As String

End Interface