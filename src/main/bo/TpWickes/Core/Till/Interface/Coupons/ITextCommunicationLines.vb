﻿Public Interface ITextCommunicationLines

    ReadOnly Property ExistsInDatabase() As Boolean

    Function Initialise(ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer) As Boolean
    Function GetFullText() As String
    Function GetSummaryText() As String

End Interface