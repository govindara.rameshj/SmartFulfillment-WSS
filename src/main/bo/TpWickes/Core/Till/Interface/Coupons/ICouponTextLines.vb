﻿'Interface does not need to be exposed to the till, moved from TpWickes.Interop.Interface

Public Interface ICouponTextLines

    ReadOnly Property ExistsInDatabase() As Boolean

    Function Initialise(ByVal CouponId As String, ByVal Deleted As Boolean) As Boolean
    'ReadOnly Property List() As System.Collections.Generic.IList(Of ICouponTextLine)
    ReadOnly Property LineCount() As Integer
    ReadOnly Property Line(ByVal Index As Integer) As ICouponTextLine
End Interface