﻿Namespace Coupon

    Public Interface ICoupon

        Property CouponID() As String                'CouponMaster -> COUPONID
        Property Description() As String             'CouponMaster -> DESCRIPTION
        Property StoreGenerated() As Boolean         'CouponMaster -> STOREGEN
        Property SerialNumber() As Boolean           'CouponMaster -> SERIALNO
        Property Exclusive() As Boolean              'CouponMaster -> EXCCOUPON
        Property Reusable() As Boolean               'CouponMaster -> REUSABLE
        Property RequiresCustomerInfo() As Boolean   'CouponMaster -> COLLECTINFO
        Property Deleted() As Boolean                'CouponMaster -> DELETED

        'Property TextLines() As ICouponTextLines     'CouponText collection

        ReadOnly Property ExistsInDatabase() As Boolean

        Function Initialise(ByVal CouponId As String, ByVal Deleted As Boolean) As Boolean

    End Interface

End Namespace