﻿Public Interface ICouponHeader

    Function GetTermsAndConditionsText() As String
    Function GetExclusiveClause() As String
    Function GetNonReusableClause() As String
End Interface
