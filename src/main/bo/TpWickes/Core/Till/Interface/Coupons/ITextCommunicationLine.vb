﻿Public Interface ITextCommunicationLine

    Property ID() As Integer                         'TextCommunication -> Id
    Property FunctionalAreaID() As Integer           'TextCommunication -> FunctionalAreaId
    Property TypeID() As Integer                     'TextCommunication -> TypeId
    Property SequenceNumber() As Integer             'TextCommunication -> Sequence
    Property Description() As String                 'TextCommunication -> Description
    Property SummaryText() As String                 'TextCommunication -> SummaryText
    Property FullText() As String                    'TextCommunication -> FullText
    Property LineDelimiter() As String               'TextCommunication -> LineDelimiter
    Property ParagraphDelimiter() As String          'TextCommunication -> ParagraphDelimiter
    Property FieldStartDelimiter() As String         'TextCommunication -> FieldStartDelimiter
    Property FieldEndDelimiter() As String           'TextCommunication -> FieldEndDelimiter
    Property Deleted() As Boolean                    'TextCommunication -> Deleted

    ReadOnly Property GetFullText() As String
    ReadOnly Property GetSummaryText() As String

End Interface