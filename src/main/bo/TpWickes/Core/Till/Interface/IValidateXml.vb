﻿Public Interface IValidateXml

    Function ValideXML(ByVal Type As XsdType, ByVal Direction As XsdTypeDirection, _
                              ByVal XmlDocument As String, ByRef XmlError As String) As Boolean

    Function ValideXML(ByVal Type As XsdType, ByVal Direction As XsdTypeDirection, _
                              ByVal XmlDocument As XDocument, ByRef XmlError As String) As Boolean


End Interface
