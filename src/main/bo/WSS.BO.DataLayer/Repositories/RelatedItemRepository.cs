﻿using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class RelatedItemRepository : BaseRepository, IRelatedItemRepository
    {
        public RelatedItem SelectRelatedItem(string packageItemNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.PackageItemNumber == packageItemNumber).FirstOrDefault();
            }
        }
    }
}
