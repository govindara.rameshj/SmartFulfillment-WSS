﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class AlertRepository : BaseRepository, IAlertRepository
    {
        public IList<Alert> GetUnauthorizedAlerts()
        {
            using (var db = GetDb())
            {
                // a.Authorized == false - to force good sql generating
                return db.GetTable<Alert>()
                    .Where(a => a.Authorized == false)
                    .Select(a => a).ToList();
            }
        }

        public int AuthorizeAlerts(IList<Alert> alerts, int userId)
        {
            using (var db = GetDb())
            {
                // a.Authorized == false - to force good sql generating
                return db.GetTable<Alert>()
                    .Where(a => alerts.Contains(a) && a.Authorized == false)
                    .Update(a => new Alert
                    {
                        Authorized = true,
                        AuthUser = userId,
                        AuthDate = DateTime.Now
                    });
            }
        }
    }
}
