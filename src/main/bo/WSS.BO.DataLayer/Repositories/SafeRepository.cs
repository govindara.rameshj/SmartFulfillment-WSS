﻿using System;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class SafeRepository : BaseRepository, ISafeRepository
    {
        private static readonly object LockObject = new object();

        public Safe GetSafe(DateTime currentDate)
        {
            using (var db = GetDb())
            {
                var lastSafeDate = db.GetTable<Safe>()
                    .Select(safe => (DateTime?)safe.PeriodDate)
                    .Max();

                var date = currentDate.Date;

                if (lastSafeDate != date)
                {
                    lock (LockObject)
                    {
                        db.CreateSafeForNonTradingDay(date.AddDays(1));
                    }
                }

                return db.GetTable<Safe>().FirstOrDefault(safe => safe.PeriodDate == date);
            }
        }
    }
}
