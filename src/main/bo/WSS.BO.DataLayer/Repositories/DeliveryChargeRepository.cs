﻿using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class DeliveryChargeRepository : BaseRepository, IDeliveryChargeRepository
    {
        private IList<DeliveryChargeGroup> GetDeliveryChargeGroups()
        {
            using (var db = GetDb())
            {
                return db.GetTable<DeliveryChargeGroup>().ToList();
            }
        }

        private IList<DeliveryChargeGroupValue> GetDeliveryChargeGroupValue()
        {
            using (var db = GetDb())
            {
                return db.GetTable<DeliveryChargeGroupValue>().ToList();
            }
        }

        public DeliveryChargeContainer GetDeliveryChargeContainer()
        {
            return new DeliveryChargeContainer(GetDeliveryChargeGroups(), GetDeliveryChargeGroupValue());
        }
    }
}
