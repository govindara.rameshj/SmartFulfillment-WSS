﻿using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Utility;

namespace WSS.BO.DataLayer.Repositories
{
    public class CashierBalanceRepository : BaseRepository, ICashierBalanceRepository
    {
        #region CashBalCashier

        public bool CashierBalanceExists(int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashier>()
                    .Any(cash => cash.PeriodId == periodId && cash.CashierId == cashierId && cash.CurrencyId == currencyId);
            }
        }

        public void ApplyCashierBalanceUpdate(CashBalCashier update)
        {
            if (!CashierBalanceExists(update.PeriodId, update.CashierId, update.CurrencyId))
            {
                Insert(update);
            }
            else
            {
                using (var db = GetDb())
                {
                    var query = db.GetTable<CashBalCashier>()
                        .Where(c => c.PeriodId == update.PeriodId && c.CurrencyId == update.CurrencyId && c.CashierId == update.CashierId)
                        .SetAdd(c => c.GrossSalesAmount, update.GrossSalesAmount)
                        .SetAdd(c => c.DiscountAmount, update.DiscountAmount)
                        .SetAdd(c => c.SalesCount, update.SalesCount)
                        .SetAdd(c => c.SalesAmount, update.SalesAmount)
                        .SetAdd(c => c.SalesCorrectCount, update.SalesCorrectCount)
                        .SetAdd(c => c.SalesCorrectAmount, update.SalesCorrectAmount)
                        .SetAdd(c => c.RefundCount, update.RefundCount)
                        .SetAdd(c => c.RefundAmount, update.RefundAmount)
                        .SetAdd(c => c.RefundCorrectCount, update.RefundCorrectCount)
                        .SetAdd(c => c.RefundCorrectAmount, update.RefundCorrectAmount)
                        .SetAdd(c => c.NumVoids, update.NumVoids)
                        .SetAdd(c => c.NumOpenDrawer, update.NumOpenDrawer)
                        .SetAdd(c => c.NumLinesReversed, update.NumLinesReversed)
                        .SetAdd(c => c.NumLinesSold, update.NumLinesSold)
                        .SetAdd(c => c.NumLinesScanned, update.NumLinesScanned)
                        .SetAdd(c => c.NumTransactions, update.NumTransactions)
                        .SetAdd(c => c.NumCorrections, update.NumCorrections)
                        .SetAdd(c => c.FloatIssued, update.FloatIssued)
                        .SetAdd(c => c.FloatReturned, update.FloatReturned)
                        .SetAdd(c => c.FloatVariance, update.FloatVariance)
                        .SetAdd(c => c.ExchangeRate, update.ExchangeRate)
                        .SetAdd(c => c.ExchangePower, update.ExchangePower);

                    query = SetAddMiscIncomeCount(query, update);
                    query = SetAddMiscIncomeValue(query, update);
                    query = SetAddMiscOutCount(query, update);
                    query = SetAddMiscOutValue(query, update);

                    query.Update();
                }
            }
        }

        #region Misc accounts helpers

        private QueryableOrUpdatableWrapper<CashBalCashier> SetAddMiscOutValue(QueryableOrUpdatableWrapper<CashBalCashier> query, CashBalCashier update)
        {
            return query
                .SetAdd(c => c.MisOutValue01, update.MisOutValue01)
                .SetAdd(c => c.MisOutValue02, update.MisOutValue02)
                .SetAdd(c => c.MisOutValue03, update.MisOutValue03)
                .SetAdd(c => c.MisOutValue04, update.MisOutValue04)
                .SetAdd(c => c.MisOutValue05, update.MisOutValue05)
                .SetAdd(c => c.MisOutValue06, update.MisOutValue06)
                .SetAdd(c => c.MisOutValue07, update.MisOutValue07)
                .SetAdd(c => c.MisOutValue08, update.MisOutValue08)
                .SetAdd(c => c.MisOutValue09, update.MisOutValue09)
                .SetAdd(c => c.MisOutValue10, update.MisOutValue10)
                .SetAdd(c => c.MisOutValue11, update.MisOutValue11)
                .SetAdd(c => c.MisOutValue12, update.MisOutValue12)
                .SetAdd(c => c.MisOutValue13, update.MisOutValue13)
                .SetAdd(c => c.MisOutValue14, update.MisOutValue14)
                .SetAdd(c => c.MisOutValue15, update.MisOutValue15)
                .SetAdd(c => c.MisOutValue16, update.MisOutValue16)
                .SetAdd(c => c.MisOutValue17, update.MisOutValue17)
                .SetAdd(c => c.MisOutValue18, update.MisOutValue18)
                .SetAdd(c => c.MisOutValue19, update.MisOutValue19)
                .SetAdd(c => c.MisOutValue20, update.MisOutValue20);
        }

        private QueryableOrUpdatableWrapper<CashBalCashier> SetAddMiscOutCount(QueryableOrUpdatableWrapper<CashBalCashier> query, CashBalCashier update)
        {
            return query
                .SetAdd(c => c.MisOutCount01, update.MisOutCount01)
                .SetAdd(c => c.MisOutCount02, update.MisOutCount02)
                .SetAdd(c => c.MisOutCount03, update.MisOutCount03)
                .SetAdd(c => c.MisOutCount04, update.MisOutCount04)
                .SetAdd(c => c.MisOutCount05, update.MisOutCount05)
                .SetAdd(c => c.MisOutCount06, update.MisOutCount06)
                .SetAdd(c => c.MisOutCount07, update.MisOutCount07)
                .SetAdd(c => c.MisOutCount08, update.MisOutCount08)
                .SetAdd(c => c.MisOutCount09, update.MisOutCount09)
                .SetAdd(c => c.MisOutCount10, update.MisOutCount10)
                .SetAdd(c => c.MisOutCount11, update.MisOutCount11)
                .SetAdd(c => c.MisOutCount12, update.MisOutCount12)
                .SetAdd(c => c.MisOutCount13, update.MisOutCount13)
                .SetAdd(c => c.MisOutCount14, update.MisOutCount14)
                .SetAdd(c => c.MisOutCount15, update.MisOutCount15)
                .SetAdd(c => c.MisOutCount16, update.MisOutCount16)
                .SetAdd(c => c.MisOutCount17, update.MisOutCount17)
                .SetAdd(c => c.MisOutCount18, update.MisOutCount18)
                .SetAdd(c => c.MisOutCount19, update.MisOutCount19)
                .SetAdd(c => c.MisOutCount20, update.MisOutCount20);
        }

        private QueryableOrUpdatableWrapper<CashBalCashier> SetAddMiscIncomeValue(QueryableOrUpdatableWrapper<CashBalCashier> query, CashBalCashier update)
        {
            return query
                .SetAdd(c => c.MiscIncomeValue01, update.MiscIncomeValue01)
                .SetAdd(c => c.MiscIncomeValue02, update.MiscIncomeValue02)
                .SetAdd(c => c.MiscIncomeValue03, update.MiscIncomeValue03)
                .SetAdd(c => c.MiscIncomeValue04, update.MiscIncomeValue04)
                .SetAdd(c => c.MiscIncomeValue05, update.MiscIncomeValue05)
                .SetAdd(c => c.MiscIncomeValue06, update.MiscIncomeValue06)
                .SetAdd(c => c.MiscIncomeValue07, update.MiscIncomeValue07)
                .SetAdd(c => c.MiscIncomeValue08, update.MiscIncomeValue08)
                .SetAdd(c => c.MiscIncomeValue09, update.MiscIncomeValue09)
                .SetAdd(c => c.MiscIncomeValue10, update.MiscIncomeValue10)
                .SetAdd(c => c.MiscIncomeValue11, update.MiscIncomeValue11)
                .SetAdd(c => c.MiscIncomeValue12, update.MiscIncomeValue12)
                .SetAdd(c => c.MiscIncomeValue13, update.MiscIncomeValue13)
                .SetAdd(c => c.MiscIncomeValue14, update.MiscIncomeValue14)
                .SetAdd(c => c.MiscIncomeValue15, update.MiscIncomeValue15)
                .SetAdd(c => c.MiscIncomeValue16, update.MiscIncomeValue16)
                .SetAdd(c => c.MiscIncomeValue17, update.MiscIncomeValue17)
                .SetAdd(c => c.MiscIncomeValue18, update.MiscIncomeValue18)
                .SetAdd(c => c.MiscIncomeValue19, update.MiscIncomeValue19)
                .SetAdd(c => c.MiscIncomeValue20, update.MiscIncomeValue20);
        }

        private QueryableOrUpdatableWrapper<CashBalCashier> SetAddMiscIncomeCount(QueryableOrUpdatableWrapper<CashBalCashier> query, CashBalCashier update)
        {
            return query
                .SetAdd(c => c.MiscIncomeCount01, update.MiscIncomeCount01)
                .SetAdd(c => c.MiscIncomeCount02, update.MiscIncomeCount02)
                .SetAdd(c => c.MiscIncomeCount03, update.MiscIncomeCount03)
                .SetAdd(c => c.MiscIncomeCount04, update.MiscIncomeCount04)
                .SetAdd(c => c.MiscIncomeCount05, update.MiscIncomeCount05)
                .SetAdd(c => c.MiscIncomeCount06, update.MiscIncomeCount06)
                .SetAdd(c => c.MiscIncomeCount07, update.MiscIncomeCount07)
                .SetAdd(c => c.MiscIncomeCount08, update.MiscIncomeCount08)
                .SetAdd(c => c.MiscIncomeCount09, update.MiscIncomeCount09)
                .SetAdd(c => c.MiscIncomeCount10, update.MiscIncomeCount10)
                .SetAdd(c => c.MiscIncomeCount11, update.MiscIncomeCount11)
                .SetAdd(c => c.MiscIncomeCount12, update.MiscIncomeCount12)
                .SetAdd(c => c.MiscIncomeCount13, update.MiscIncomeCount13)
                .SetAdd(c => c.MiscIncomeCount14, update.MiscIncomeCount14)
                .SetAdd(c => c.MiscIncomeCount15, update.MiscIncomeCount15)
                .SetAdd(c => c.MiscIncomeCount16, update.MiscIncomeCount16)
                .SetAdd(c => c.MiscIncomeCount17, update.MiscIncomeCount17)
                .SetAdd(c => c.MiscIncomeCount18, update.MiscIncomeCount18)
                .SetAdd(c => c.MiscIncomeCount19, update.MiscIncomeCount19)
                .SetAdd(c => c.MiscIncomeCount20, update.MiscIncomeCount20);
        }

        #endregion

        #endregion

        #region CashBalCashierTen

        public bool CashierBalanceTenderExists(int tenderTypeId, int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return
                    db.GetTable<CashBalCashierTen>()
                        .Any(
                            cash =>
                                cash.PeriodId == periodId && cash.CashierId == cashierId &&
                                cash.CurrencyId == currencyId && cash.TenderTypeId == tenderTypeId);
            }
        }

        public void ApplyCashierBalanceTenderUpdate(CashBalCashierTen update)
        {
            if (!CashierBalanceTenderExists(update.TenderTypeId, update.PeriodId, update.CashierId, update.CurrencyId))
            {
                Insert(update);
            }
            else
            {
                using (var db = GetDb())
                {
                    db.GetTable<CashBalCashierTen>()
                        .Where(x => x.PeriodId == update.PeriodId
                            && x.CurrencyId == update.CurrencyId
                            && x.CashierId == update.CashierId
                            && x.TenderTypeId == update.TenderTypeId)
                        .SetAdd(x => x.Quantity, update.Quantity)
                        .SetAdd(x => x.Amount, update.Amount)
                        .SetAdd(x => x.PickUp, update.PickUp)
                        .Update();
                }
            }
        }

        #endregion

        #region CashBalCashierTenVar

        public bool CashierBalanceTenderVarianceExists(int tenderTypeId, int periodId, int cashierId, string currencyId, int tradingPeriodId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTenVar>()
                    .Any(cash => cash.PeriodId == periodId
                        && cash.CashierId == cashierId
                        && cash.CurrencyId == currencyId
                        && cash.TenderTypeId == tenderTypeId
                        && cash.TradingPeriodId == tradingPeriodId);
            }
        }

        public void ApplyCashierBalanceTenderVarianceUpdate(CashBalCashierTenVar update)
        {
            if (!CashierBalanceTenderVarianceExists(update.TenderTypeId, update.PeriodId, update.CashierId, update.CurrencyId, update.TradingPeriodId))
            {
                Insert(update);
            }
            else
            {
                using (var db = GetDb())
                {
                    db.GetTable<CashBalCashierTenVar>()
                        .Where(x => x.PeriodId == update.PeriodId
                            && x.CurrencyId == update.CurrencyId
                            && x.CashierId == update.CashierId
                            && x.TenderTypeId == update.TenderTypeId
                            && x.TradingPeriodId == update.TradingPeriodId)
                        .SetAdd(x => x.Quantity, update.Quantity)
                        .SetAdd(x => x.Amount, update.Amount)
                        .SetAdd(x => x.PickUp, update.PickUp)
                      .Update();
                }
            }
        }

        #endregion
    }
}
