using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using Cts.Oasys.Core;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class ExternalRequestRepository : BaseRepository, IExternalRequestRepository
    {
        // ReSharper disable once EmptyConstructor
        public ExternalRequestRepository()
        {
        }

        public virtual List<ExternalRequest> GetRequestsForProcessing(int retryPeriodMinutes)
        {
            using (var db = GetDb())
            {
                var query =
                    from e in db.GetTable<ExternalRequest>()
                    where e.Status == ExternalRequestStatus.New ||
                          e.Status == ExternalRequestStatus.Retry &&
                          e.LastRetryDateTime <= DateTime.Now.AddMinutes(-retryPeriodMinutes)
                    select e;

                return query.ToList();
            }
        }

        public virtual Int64 CreateRequest<T>(string type, T details) where T : IExternalRequestDetails
        {
            ExternalRequest externalRequest = new ExternalRequest();
            externalRequest.SetDetails(details);
            var source = details.GenerateSourceDescription(type);

            using (var db = GetDb())
            {
                var value = db.GetTable<ExternalRequest>().Insert(() => new ExternalRequest
                {
                    CreateTime = DateTime.Now,
                    RequestDetailsXml = externalRequest.RequestDetailsXml,
                    Status = ExternalRequestStatus.New,
                    Source = source,
                    Type = type,
                    RetriesCount = 0
                });

                return value;
            }
        }

        public virtual void UpdateRequest(Int64 requestId, int status)
        {
            UpdateRequest(requestId, status, null);
        }

        public virtual void UpdateRequest(Int64 requestId, int status, string errorDescription)
        {
            using (var db = GetDb())
            {
                var query =
                    from e in db.GetTable<ExternalRequest>()
                    where e.Id == requestId
                    select e;

                var request = query.SingleOrDefault();
                if (request == null)
                {
                    return;
                }

                string truncatedErrorDescription = !string.IsNullOrEmpty(errorDescription) && errorDescription.Length > 1000 ? 
                    errorDescription.Substring(0, 1000) : errorDescription;

                db.GetTable<ExternalRequest>()
                    .Where(r => r.Id == requestId)
                    .Set(r => r.Status, status)
                    .Set(r => r.Error, truncatedErrorDescription)
                    .Set(r => r.RetriesCount, request.RetriesCount + (status == ExternalRequestStatus.Processed ? 0 : 1))
                    .Set(r => r.LastRetryDateTime, DateTime.Now)
                    .Update();
            }
        }
    }
}