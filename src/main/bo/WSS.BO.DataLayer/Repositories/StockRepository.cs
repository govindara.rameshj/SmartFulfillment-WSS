using System;
using System.Linq;
using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Utility;
using BLToolkit.Data.Linq;

namespace WSS.BO.DataLayer.Repositories
{
    public class StockRepository : BaseRepository, IStockRepository
    {
        public StockMaster GetStockMaster(string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockMaster>().FirstOrDefault(stkmas => stkmas.SkuNumber == skuNumber);
            }
        }

        public SkuInfo GetSkuInfo(string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<SkuInfo>().FirstOrDefault(skuInfo => skuInfo.SKU == skuNumber);
            }
        }

        public decimal GetStockMasterPrice(string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockMaster>().Where(stkmas => stkmas.SkuNumber == skuNumber).Select(stkmas => stkmas.Price).FirstOrDefault();
            }
        }

        public void ApplyStockUpdate(StockUpdate stockUpdate)
        {
            using (var db = GetDb())
            {
                var originalStockMaster = db.GetTable<StockMaster>().First(sm => sm.SkuNumber == stockUpdate.SkuNumber);
                db.StockLogInsertCheck91(originalStockMaster.SkuNumber, (int)originalStockMaster.StockOnHand, stockUpdate.CashierNumber);
                UpdateStockMaster(db, stockUpdate);
                var stockLog = CreateStockLogForStockUpdate(stockUpdate, originalStockMaster);
                Insert(db, stockLog);
            }
        }

        private static void UpdateStockMaster(OasysDb db, StockUpdate stockUpdate)
        {
            db.GetTable<StockMaster>()
                .Where(sm => sm.SkuNumber == stockUpdate.SkuNumber)
                .SetAdd(sm => sm.StockOnHand, stockUpdate.StockOnHandDelta)
                .SetAdd(sm => sm.MarkdownStock, stockUpdate.MarkdownStockDelta)
                .SetAdd(sm => sm.AdjustmentsValue1, stockUpdate.AdjustmentsValue1Delta)
                .SetAdd(sm => sm.SalesUnits1, stockUpdate.SalesUnits1Delta)
                .SetAdd(sm => sm.SalesUnits2, stockUpdate.SalesUnits2Delta)
                .SetAdd(sm => sm.SalesUnits4, stockUpdate.SalesUnits4Delta)
                .SetAdd(sm => sm.SalesUnits6, stockUpdate.SalesUnits6Delta)
                .SetAdd(sm => sm.SalesValue1, stockUpdate.SalesValue1Delta)
                .SetAdd(sm => sm.SalesValue2, stockUpdate.SalesValue2Delta)
                .SetAdd(sm => sm.SalesValue4, stockUpdate.SalesValue4Delta)
                .SetAdd(sm => sm.SalesValue6, stockUpdate.SalesValue6Delta)
                .Update();
        }

        private static Stklog CreateStockLogForStockUpdate(
            StockUpdate stockUpdate,
            StockMaster originalStock)
        {
            var stockLog = new Stklog
            {
                EndingReturnsQuantity = originalStock.OpenReturnQuantity,
                CashierNumber = stockUpdate.CashierNumber,
                RtiStatus = stockUpdate.RtiFlag,
                FilterForFutureUse = null,
                DateOfMovement = stockUpdate.DateOfMovement,
                DayNumber = stockUpdate.DayNumber,
                EndingMarkdownStockOnHand = originalStock.MarkdownStock + stockUpdate.MarkdownStockDelta,
                EndingPrice = originalStock.Price,
                EndingStockOnHand = originalStock.StockOnHand + stockUpdate.StockOnHandDelta,
                EndingWriteOffStockOnHand = originalStock.WriteOffStock,
                IsSentToHo = false,
                ItemNumber = originalStock.SkuNumber,
                MovementKeys = stockUpdate.MovementKeys,
                OpenReturnsQuantity = originalStock.OpenReturnQuantity,
                StartingMarkdownStockOnHand = originalStock.MarkdownStock,
                StartingPrice = originalStock.Price,
                StartingStockOnHand = originalStock.StockOnHand,
                StartingWriteOffStockOnHand = originalStock.WriteOffStock,
                TimeOfMovement = stockUpdate.TimeOfMovement,
                TypeOfMovement = stockUpdate.TypeOfMovement,
            };
            return stockLog;
        }

        public void UpdateStockMasterStockOnHand(string skuNumber, decimal standartStockUpdateQuantity)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                    .Where(sm => sm.SkuNumber == skuNumber)
                    .SetAdd(sm => sm.StockOnHand, standartStockUpdateQuantity)
                    .Update();
            }
        }

        public void UpdateStockMasterMarkdownStockOnHand(string skuNumber, decimal markdownStockUpdateQuantity)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                    .Where(sm => sm.SkuNumber == skuNumber)
                    .SetAdd(sm => sm.MarkdownStock, markdownStockUpdateQuantity)
                    .Update();
            }
        }

        public string GetStockAdjustmentCodeType(string codeNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockAdjustmentCode>().Single(x => x.CodeNumber == codeNumber).Type;
            }
        }

        public StockAdjustmentCode SelectStockAdjustmentCode(string codeNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockAdjustmentCode>().FirstOrDefault(x => x.CodeNumber == codeNumber);
            }
        }

        public bool CheckStockAdjustmentExists(string skuNumber, string sequenceNumber, string adjustmentCode, DateTime adjustmentDate, bool isStaAuditUpload)
        {
            using (var db = GetDb())
            {

                var stockAdjustment = db.GetTable<StockAdjustmentFile>().FirstOrDefault(x => 
                    x.SkuNumber == skuNumber && 
                    x.SequenceNumber == sequenceNumber && 
                    x.Code == adjustmentCode && 
                    x.Date == adjustmentDate && 
                    x.IsStaAuditUpload == isStaAuditUpload);

                return stockAdjustment != null;
            }
        }

        public void InsertIntoStockAdjustment(StockAdjustmentFile stkadj)
        {
            using (var db = GetDb())
            {
                db.Insert(stkadj);
            }
        }

        public void InsertIntoStockLog(Stklog stocklog)
        {
            using (var db = GetDb())
            {
                db.Insert(stocklog);
            }
        }
    }
}
