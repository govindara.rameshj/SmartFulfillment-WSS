using System;
using System.Collections.Generic;
using System.Linq;
using BLToolkit.Data.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Utility;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Repositories
{
    public class DailyTillRepository : BaseRepository, IDailyTillRepository
    {
        public void InsertEntityIntoDb<T>(T entity) where T : class
        {
            using (var db = GetDb())
            {
                db.Insert(entity);
            }
        }

        public IQueryable<T> SelectEntities<T>(IDailyTillTranKey tran) where T : class, IDailyTillTranKey
        {
            using (var db = GetDb())
            {
                return db.GetTable<T>().WhereRelatedToTran(tran);
            }
        }

        public T SelectEntity<T>(IDailyTillTranKey tran) where T : class, IDailyTillTranKey
        {
            using (var db = GetDb())
            {
                return db.GetTable<T>().WhereRelatedToTran(tran).FirstOrDefault();
            }
        }

        public T SelectExistingEntity<T>(IDailyTillTranKey tran) where T : class, IDailyTillTranKey
        {
            using (var db = GetDb())
            {
                return db.GetTable<T>().WhereRelatedToTran(tran).First();
            }
        }

        public IList<T> SelectListOfEntities<T>(IDailyTillTranKey tran) where T : class, IDailyTillTranKey
        {
            using (var db = GetDb())
            {
                return db.GetTable<T>().WhereRelatedToTran(tran).ToList();
            }
        }

        #region Dltots processing

        public void InsertIntoDltots(Dltots dltots)
        {
            InsertEntityIntoDb(dltots);
        }

        public string SelectFirstDltotsOrderNumber(IDailyTillTranKey tran)
        {
            return SelectEntity<Dltots>(tran).OrderNumber;
        }

        public Dltots SelectDailyTillTran(IDailyTillTranKey tran)
        {
            return SelectEntity<Dltots>(tran);
        }

        public bool IsDltotsExists(String sourceId, String source)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().Any(t => t.SourceTranId == sourceId && t.Source == source);
            }
        }

        public bool IsDltotsExists(IDailyTillTranKey key)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().Any(t => t.TransactionNumber == key.TransactionNumber && t.TillNumber == key.TillNumber && t.CreateDate == key.CreateDate);
            }
        }

        public Dltots SelectDailyTotal(IDailyTillTranKey tran, string transactionCode)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().WhereRelatedToTran(tran).FirstOrDefault(x => x.TransactionCode == transactionCode);
            }
        }

        public Dltots SelectDailyTotal(String sourceId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().FirstOrDefault(t => t.SourceTranId == sourceId);
            }
        }

        public Dltots SelectDailyTotal(string transactionNumber, string tillNumber, DateTime? transactionDate)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().FirstOrDefault(x => x.TransactionNumber == transactionNumber && x.TillNumber == tillNumber && x.CreateDate == transactionDate);
            }
        }

        public string GetOrderNumberByOriginalTransactionId(string refTransactionId, string source)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().Where(x => x.SourceTranId == refTransactionId && x.Source == source).Select(x => x.OrderNumber).FirstOrDefault();
            }
        }

        public Dltots SelectGeneratedDailyTillTran(String sourceTranNumber, String source, string transactionCode)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().FirstOrDefault(t => t.SourceTranNumber == sourceTranNumber && t.Source == source && t.TransactionCode == transactionCode);
            }
        }

        public void UpdateDltots(DltotsUpdate update)
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>().Where(dltots => dltots.SourceTranId == update.SourceTransactionId && dltots.Source == update.Source)
                .Set(dltots => dltots.IsParked, update.IsParked)
                .Set(dltots => dltots.IsRecalled, update.IsRecalled)
                .Set(dltots => dltots.StatusSequence, update.StatusSequence)
                .Set(dltots => dltots.SaveStatus, update.SaveStatus)
                .Update();
            }
        }

        public IDailyTillTranKey GetDailyTillTranKey(string sourceTransactionId, string source)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().Where(x => x.SourceTranId == sourceTransactionId && x.Source == source)
                    .Select(x => new DailyTillTranKey { CreateDate = x.CreateDate, TillNumber = x.TillNumber, TransactionNumber = x.TransactionNumber }).FirstOrDefault();
            }
        }

        public int GetDailyTillTranCount(String sourceId, String source)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().Count(t => t.SourceTranId == sourceId && t.Source == source);
            }
        }

        #endregion

        #region Dlline processing

        public void InsertIntoDlline(DailyTillLine dlline)
        {
            InsertEntityIntoDb(dlline);
        }

        public IList<DailyTillLine> SelectDailyTillLines(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<DailyTillLine>(tran);
        }

        public IList<DailyTillLine> SelectDailyTillLines(IDailyTillTranKey tran, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).Where(x => x.SkuNumber == skuNumber).ToList();
            }
        }

        public DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran)
        {
            return SelectEntity<DailyTillLine>(tran);
        }

        public DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SkuNumber == skuNumber);
            }
        }

        public DailyTillLine SelectDailyTillLine(IDailyTillTranKey tran, int sequenceNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SequenceNumber == sequenceNumber);
            }
        }

        public DailyTillLine SelectDailyTillLineBySourceItemId(IDailyTillTranKey tran, string sourceItemId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SourceItemId == sourceItemId);
            }
        }

        public DailyTillLine SelectDailyLineBySourceItemId(IDailyTillTranKey refundedDailyTillTranKey, string refundedTransactionItemId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(refundedDailyTillTranKey).FirstOrDefault(x => x.SourceItemId == refundedTransactionItemId);
            }
        }

        public string GetSourceItemIdByItemLineNumber(IDailyTillTranKey refundedDailyTillTranKey, int refundedItemLineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(refundedDailyTillTranKey).First(x => x.SequenceNumber == refundedItemLineNumber).SourceItemId;
            }
        }

        public string GetItemSkuByItemLineNumber(IDailyTillTranKey refundedDailyTillTranKey, int refundedItemLineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(refundedDailyTillTranKey).First(x => x.SequenceNumber == refundedItemLineNumber).SkuNumber;
            }
        }

        public int GetItemLineNumberBySku(IDailyTillTranKey dailyTillTranKey, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(dailyTillTranKey).First(x => x.SkuNumber == skuNumber).SequenceNumber;
            }
        }

        public IList<DailyTillLine> SelectReversedDailyTillLines(IDailyTillTranKey tran, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).Where(x => x.SkuNumber == skuNumber && x.IsReversed).ToList();
            }
        }

        public DailyTillLine GetOriginalLineNumber(IDailyTillTranKey tran, string sourceItemId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyTillLine>().WhereRelatedToTran(tran).FirstOrDefault(
                    x => (!string.IsNullOrEmpty(x.SourceItemId) && x.SourceItemId == sourceItemId)
                    || (string.IsNullOrEmpty(x.SourceItemId) && x.SequenceNumber == int.Parse(sourceItemId)));
            }
        }

        #endregion

        #region Dlpaid processing

        public void InsertIntoDlpaid(Dlpaid dlpaid)
        {
            InsertEntityIntoDb(dlpaid);
        }

        public IList<Dlpaid> SelectDlpaids(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlpaid>(tran);
        }

        public Dlpaid SelectDailyPayment(IDailyTillTranKey tran)
        {
            return SelectEntity<Dlpaid>(tran);
        }

        public Dlpaid SelectDailyPayment(IDailyTillTranKey tran, int tenderType)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlpaid>().WhereRelatedToTran(tran).FirstOrDefault(x => x.TenderType == tenderType);
            }
        }

        public Dlpaid SelectDailyPayment(IDailyTillTranKey tran, int tenderType, bool isRefund)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlpaid>().WhereRelatedToTran(tran).FirstOrDefault(x => x.TenderType == tenderType && isRefund ? x.Amount > 0 : x.Amount <= 0);
            }
        }

        #endregion

        #region Dlrcus processing

        public void InsertIntoDlrcus(DailyCustomer dlrcus)
        {
            InsertEntityIntoDb(dlrcus);
        }

        public IList<DailyCustomer> SelectDlrcuses(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<DailyCustomer>(tran);
        }

        public IList<DailyTillTranKey> SelectDlrcusesByOriginalTransactionKey(IDailyTillTranKey tran)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>()
                    .Where(
                        x =>
                            x.OriginalTransactionNumber == tran.TransactionNumber &&
                            x.OriginalTillNumber == tran.TillNumber && x.OriginalSaleDate == tran.CreateDate)
                            .Select(x => new DailyTillTranKey { CreateDate = x.CreateDate, TillNumber = x.TillNumber, TransactionNumber = x.TransactionNumber}).Distinct().ToList();
            }
        }

        public DailyCustomer SelectDailyCustomer(IDailyTillTranKey tran, int lineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>().WhereRelatedToTran(tran).FirstOrDefault(x => x.LineNumber == lineNumber);
            }
        }

        public DailyCustomer SelectDailyCustomerWithNonZeroLineNumber(IDailyTillTranKey tran)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>().WhereRelatedToTran(tran).FirstOrDefault(x => x.LineNumber != 0);
            }
        }

        public DailyCustomer SelectDailyCustomersWithOriginalLineNumber(IDailyTillTranKey tran, int originalLineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>().WhereRelatedToTran(tran).FirstOrDefault(x => x.LineNumber != 0 && x.OriginalLineNumber == originalLineNumber);
            }
        }

        public DailyCustomer SelectDailyCustomerWithFulfilledCustomerName(IDailyTillTranKey tran)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>().WhereRelatedToTran(tran).FirstOrDefault(x => !String.IsNullOrEmpty(x.CustomerName));
            }
        }

        public DailyCustomer SelectDailyCustomer(IDailyTillTranKey tran)
        {
            return SelectEntity<DailyCustomer>(tran);
        }

        public int GetCountOfDailyCustomerWithNonZeroLineNumber(IDailyTillTranKey tran)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyCustomer>().WhereRelatedToTran(tran).Where(x => x.LineNumber != 0).Count();
            }
        }

        #endregion

        #region Dlcomm processing

        public void InsertIntoDlcomm(Dlcomm dlcomm)
        {
            InsertEntityIntoDb(dlcomm);
        }

        public Dlcomm SelectDlcomm(IDailyTillTranKey tran)
        {
            return SelectEntity<Dlcomm>(tran);
        }

        public Dlcomm SelectDlcomm(IDailyTillTranKey tran, int paymentIndex)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlcomm>().WhereRelatedToTran(tran).FirstOrDefault(x => x.PaymentIndex == paymentIndex);
            }
        }

        #endregion

        #region DlGiftCard processing

        public void InsertIntoDlGiftCard(DailyGiftCard giftCard)
        {
            using (var db = GetDb())
            {
                var parameters = new object[]
                {
                    db.Parameter("@DATE1", giftCard.CreateDate),
                    db.Parameter("@TILL", giftCard.TillNumber),
                    db.Parameter("@TRAN", giftCard.TransactionNumber),
                    db.Parameter("@CARDNUM", giftCard.CardNumber),
                    db.Parameter("@EEID", giftCard.CashierNumber),
                    db.Parameter("@TYPE", giftCard.TransactionType),
                    db.Parameter("@AMNT", giftCard.TenderAmount),
                    db.Parameter("@AUTH", giftCard.AuthCode),
                    db.Parameter("@MSGNUM", giftCard.MessageNumber),
                    db.Parameter("@TRANID", giftCard.ExternalTransactionNumber),
                    db.Parameter("@RTIFlag", giftCard.RtiFlag),
                    db.Parameter("@SEQN", giftCard.SequenceNumber)
                };
                db.SetSpCommand("usp_SaveDataInDLGiftCard", parameters).ExecuteNonQuery();
            }
        }

        public DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran)
        {
            return SelectEntity<DailyGiftCard>(tran);
        }

        public DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, string authorisationCode)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyGiftCard>().WhereRelatedToTran(tran).FirstOrDefault(x => x.AuthCode == authorisationCode);
            }
        }

        public DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyGiftCard>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SequenceNumber == sequenceNumber);
            }
        }

        public DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber, decimal tenderAmount)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyGiftCard>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SequenceNumber == sequenceNumber && x.TenderAmount == tenderAmount);
            }
        }

        public DailyGiftCard SelectDailyGiftCard(IDailyTillTranKey tran, int sequenceNumber, decimal tenderAmount, string transactionType)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyGiftCard>().WhereRelatedToTran(tran)
                    .FirstOrDefault(x => x.SequenceNumber == sequenceNumber && x.TenderAmount == tenderAmount && x.TransactionType == transactionType);
            }
        }

        #endregion

        #region Dlevnt processing

        public void InsertIntoDlevnt(Dlevnt dlevnt)
        {
            InsertEntityIntoDb(dlevnt);
        }

        public IList<Dlevnt> SelectDlevnts(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlevnt>(tran);
        }

        public Dlevnt SelectDailyEvent(IDailyTillTranKey tran)
        {
            return SelectEntity<Dlevnt>(tran);
        }

        public Dlevnt SelectDailyEvent(IDailyTillTranKey tran, string erosionType)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlevnt>().WhereRelatedToTran(tran).FirstOrDefault(x => x.ErosionType == erosionType);
            }
        }

        public Dlevnt SelectDailyEventBySequenceNumber(IDailyTillTranKey tran, string sequenceNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlevnt>().WhereRelatedToTran(tran).FirstOrDefault(x => x.EventSequenceNumber == sequenceNumber);
            }
        }

        public IList<Dlevnt> SelectDailyEventByDailyLinesSequenceNumber(IDailyTillTranKey tran, int sequenceNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlevnt>().WhereRelatedToTran(tran).Where(x => x.TransactionLineNumber == sequenceNumber).ToList();
            }
        }

        #endregion

        #region Dlanas processing

        public void InsertIntoDlanas(Dlanas dlanas)
        {
            InsertEntityIntoDb(dlanas);
        }

        public IList<Dlanas> SelectDlanases(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlanas>(tran);
        }

        #endregion

        #region Dlgift processing

        public void InsertIntoDlgift(DailyGiftToken dlgift)
        {
            InsertEntityIntoDb(dlgift);
        }

        public IList<DailyGiftToken> SelectDlgifts(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<DailyGiftToken>(tran);
        }
        #endregion 

        #region Dlocus processing
        
        public void InsertIntoDlocus(Dlocus dlocus)
        {
            InsertEntityIntoDb(dlocus);
        }

        public IList<Dlocus> SelectDlocuses(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlocus>(tran);
        }
        #endregion

        #region Dlolin processing

        public void InsertIntoDlolin(Dlolin dlolin)
        {
            InsertEntityIntoDb(dlolin);
        }
        
        public IList<Dlolin> SelectDlolins(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlolin>(tran);
        }

        public Dlolin SelectDlolin(IDailyTillTranKey tran)
        {
            return SelectEntity<Dlolin>(tran);
        }

        public Dlolin SelectDlolin(IDailyTillTranKey tran, short lineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlolin>().WhereRelatedToTran(tran).FirstOrDefault(x => x.LineNumber == lineNumber);
            }
        }

        #endregion 

        #region Dlreject processing

        public void InsertIntoDlreject(Dlreject dlreject)
        {
            InsertEntityIntoDb(dlreject);
        }

        public IList<Dlreject> SelectDlrejects(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlreject>(tran);
        }

        public Dlreject SelectDlreject(IDailyTillTranKey tran, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dlreject>().WhereRelatedToTran(tran).FirstOrDefault(x => x.SkuNumber == skuNumber);
            }
        }

        #endregion

        #region Dlcoupon processing

        public void InsertIntoDlcoupon(Dlcoupon dlcoupon)
        {
            InsertEntityIntoDb(dlcoupon);
        }

        public IList<Dlcoupon> SelectDlcoupons(IDailyTillTranKey tran)
        {
            return SelectListOfEntities<Dlcoupon>(tran);
        }

        public Dlcoupon SelectDailyCoupon(IDailyTillTranKey tran)
        {
            return SelectEntity<Dlcoupon>(tran);
        }

        #endregion

        #region EventMaster processing

        public void InsertIntoEventMaster(EventMaster eventMaster)
        {
            InsertEntityIntoDb(eventMaster);
        }

        #endregion

        #region Find transaction processing

        public bool TransactionExistsInDb(IDailyTillTranKey key)
        {
            using (var db = GetDb())
            {
                var tran = db.GetTable<Dltots>().FirstOrDefault(x => x.TransactionNumber == key.TransactionNumber && x.TillNumber == key.TillNumber && x.CreateDate == key.CreateDate.Date);
                return tran != null;
            }
        }

        #endregion

        public string GenerateNextTransactionNumber(string tillNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTransactionNumber(tillNumber);
            }
        }

        public string GetNextTillTranNumber()
        {
            using (var db = GetDb())
            {
                return db.GetNextTillTranNumber();
            }
        }

        public void SetOrderNumber(DailyTillTranKey tran, string orderNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>()
                    .WhereRelatedToTran(tran)
                    .Set(x => x.OrderNumber, orderNumber)
                    .Update();
            }
        }

        public bool IsTransactionToCancelForOrder(string refundedTransactionId, string source)
        {
            using (var db = GetDb())
            {
                return
                    db.GetTable<Dltots>()
                        .Any(
                            x =>
                                x.SourceTranId == refundedTransactionId && x.Source == source && x.OrderNumber != Global.NullOrderNumber);
            }
        }
    }
}
