﻿using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class CouponRepository : BaseRepository, ICouponRepository
    {
        public CouponMaster GetCouponMaster(string couponMasterId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CouponMaster>().FirstOrDefault(x => x.CouponMasterId == couponMasterId);
            }
        }

        public EventMaster GetEventByCouponNumber(string couponNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<EventMaster>().FirstOrDefault(x => x.BuyCoupon == couponNumber);
            }
        }
    }
}
