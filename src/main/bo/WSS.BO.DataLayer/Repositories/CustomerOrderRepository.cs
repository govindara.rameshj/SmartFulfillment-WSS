using System;
using System.Collections.Generic;
using System.Linq;
using BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Utility;

namespace WSS.BO.DataLayer.Repositories
{
    public class CustomerOrderRepository: BaseRepository, ICustomerOrderRepository
    {
        #region CORHDR
            
        public void InsertIntoCorhdr(CustomerOrderHeader customerOrderHeader)
        {
            Insert(customerOrderHeader);
        }

        public CustomerOrderHeader SelectCorhdr(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderHeader>().FirstOrDefault(x => x.OrderNumber == orderNumber);
            }
        }

        public string GetNextOrderNumber()
        {
            using (var db = GetDb())
            {
                return db.GetNextOrderNumber();
            }
        }

        public void SetPreviousOrderNumber(string previousOrderNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<Sysnum>().Set(sysnum => sysnum.Next15, previousOrderNumber).Update();
            }
        }

        #endregion

        #region CORHDR4

        public void InsertIntoCorhdr4(CustomerOrderHeader4 customerOrderHeader4)
        {
            Insert(customerOrderHeader4);
        }

        public CustomerOrderHeader4 SelectCorhdr4(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderHeader4>().FirstOrDefault(x => x.OrderNumber == orderNumber);
            }
        }

        #endregion

        #region CORHDR5

        public void InsertIntoCorhdr5(CustomerOrderHeader5 corhdr5)
        {
            Insert(corhdr5);
        }

        public CustomerOrderHeader5 SelectCorhdr5(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderHeader5>().FirstOrDefault(x => x.OrderNumber == orderNumber);
            }
        }

        #endregion

        # region CORLIN

        public void InsertIntoCorlin(CustomerOrderLine customerOrderLin)
        {
            Insert(customerOrderLin);
        }

        public CustomerOrderLine SelectCorlin(string orderNumber, string lineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderLine>().FirstOrDefault(x => x.OrderNumber == orderNumber && x.LineNumber == lineNumber);
            }
        }

        public List<CustomerOrderLine> SelectCorlins(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderLine>().Where(x => x.OrderNumber == orderNumber).ToList();
            }
        }
        #endregion

        #region CORREFUND

        public void InsertIntoCorrefund(CustomerOrderRefund customerOrderRefund)
        {
            Insert(customerOrderRefund);
        }

        public List<CustomerOrderRefund> SelectCorrefunds(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderRefund>().Where(x => x.OrderNumber == orderNumber).ToList();
            }
        }

        public List<CustomerOrderRefund> SelectCorrefunds(string orderNumber, string lineNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderRefund>().Where(x => x.OrderNumber == orderNumber && x.LineNumber == lineNumber).ToList();
            }
        }

        public bool CheckIfTransactionIsCancellation(string orderNumber, IDailyTillTranKey dailyTillTranKey)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderRefund>().Any(
                    x => x.OrderNumber == orderNumber 
                    && x.RefundDate == dailyTillTranKey.CreateDate
                    && x.RefundTill == dailyTillTranKey.TillNumber
                    && x.RefundTransaction == dailyTillTranKey.TransactionNumber);
            }
        }
        #endregion

        #region CORLIN2

        public void InsertIntoCorlin2(CustomerOrderLine2 corlin2)
        {
            Insert(corlin2);
        }

        public List<CustomerOrderLine2> SelectCorlins2(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderLine2>().Where(x => x.OrderNumber == orderNumber).ToList();
            }
        }
        #endregion

        #region CORTXT

        public void InsertIntoCortxt(CustomerOrderText cortxt)
        {
            Insert(cortxt);
        }

        public List<CustomerOrderText> SelectCortxts(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderText>().Where(x => x.OrderNumber == orderNumber).ToList();
            }
        }

        #endregion

        #region Updates

        public void ApplyCorlinUpdate(CustomerOrderLine corlinUpdate)
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderLine>()
                    .Where(x => x.OrderNumber == corlinUpdate.OrderNumber
                        && x.LineNumber == corlinUpdate.LineNumber)
                    .SetAdd(x => x.RefundQuantity, corlinUpdate.RefundQuantity)
                    .SetAdd(x => x.QtyToBeDelivered, corlinUpdate.QtyToBeDelivered)
                    .SetAdd(x => x.QuantityTaken, corlinUpdate.QuantityTaken)
                    .Update();
            }
        }

        public void ApplyCorhdrUpdate(CustomerOrderHeader corhdrUpdate)
        {
            var originalCorhdr = SelectCorhdr(corhdrUpdate.OrderNumber);
            var originalRefundQuantity = originalCorhdr.TotalUnitsRefundedNumber;
            var originalTotalOrderQuantity = originalCorhdr.TotalOrderUnitsNumber;
            corhdrUpdate.CancellationState = Math.Abs(originalRefundQuantity + corhdrUpdate.TotalUnitsRefundedNumber) == originalTotalOrderQuantity
                    ? "1"
                    : "2";

            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderHeader>()
                    .Where(x => x.OrderNumber == corhdrUpdate.OrderNumber)
                    .Set(x => x.RefundDate, corhdrUpdate.RefundDate)
                    .Set(x => x.RefundTillNumber, corhdrUpdate.RefundTillNumber)
                    .Set(x => x.RefundTransactionNumber, corhdrUpdate.RefundTransactionNumber)
                    .Set(x => x.CancellationState, corhdrUpdate.CancellationState)
                    .SetAdd(x => x.TotalUnitsRefundedNumber, corhdrUpdate.TotalUnitsRefundedNumber)
                    .SetAdd(x => x.TotalUnitsTakenNumber, corhdrUpdate.TotalUnitsTakenNumber)
                    .SetAdd(x => x.RevisionNumber, corhdrUpdate.RevisionNumber)
                    .Update();
            }
        }

        public void ApplyCorhdr4Update(CustomerOrderHeader4 corhdr4Update)
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderHeader4>()
                    .Where(x => x.OrderNumber == corhdr4Update.OrderNumber)
                    .Set(x => x.RefundStatus, corhdr4Update.RefundStatus)
                    .Update(); 
            }
        }

        #endregion

        public string GetCorlinLineNumberBySourceLineNumber(int sourceOrderLineNumber, string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderLine2>()
                    .Where(x => x.OrderNumber == orderNumber && x.SourceOrderLineNo == sourceOrderLineNumber)
                    .Select(x => x.LineNumber).First();
            }
        }

        public bool CheckOrderIsDelivered(string orderNumber)
        {
            using (var db = GetDb())
            {
                var isDelivered = db.GetTable<CustomerOrderLine>()
                    .Where(x => x.OrderNumber == orderNumber)
                    .All(x => x.DeliveryStatus == OrderConsignmentStatus.DeliveredStatusOk);
                if (isDelivered)
                {
                    isDelivered = db.GetTable<CustomerOrderHeader4>()
                    .Where(x => x.OrderNumber == orderNumber)
                    .All(x => x.DeliveryStatus == OrderConsignmentStatus.DeliveredStatusOk);
                }
                return isDelivered;
            }
        }

    }
}
