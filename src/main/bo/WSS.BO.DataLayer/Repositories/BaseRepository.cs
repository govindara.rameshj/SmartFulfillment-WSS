using System;
using System.Linq;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using Cts.Oasys.Core.Helpers;
using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    /// <summary>
    /// Methods are made virtual for unit testing purposes.
    /// </summary>
    public class BaseRepository : IRepository
    {
        protected BaseRepository()
        {
        }

        private IOasysDbFactory dbFactory;

        public void SetConnectionFactory(IRepositoryConnectionFactory repositoryConnectionFactory)
        {
            dbFactory = (IOasysDbFactory) repositoryConnectionFactory;
        }

        protected OasysDb GetDb()
        {
            if (dbFactory == null)
            {
                throw new InvalidOperationException("Repository is not initialized, set OasysDbFactory first.");
            }
            return dbFactory.CreateOasysDb();
        }

        protected IQueryable<T> Select<T>()
            where T : class
        {
            using (var db = GetDb())
            {
                return db.GetTable<T>();
            }
        }

        protected void Insert<T>(T obj)
            where T : class
        {
            using (var db = GetDb())
            {
                Insert(db, obj);
            }
        }

        protected void Insert<T>(OasysDb db, T obj)
            where T : class
        {
            db.Insert(obj);
        }
    }
}
