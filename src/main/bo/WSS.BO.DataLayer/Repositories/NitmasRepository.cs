using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLToolkit.Data;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class NitmasRepository : BaseRepository, INitmasRepository
    {
        public bool NitmasStarted(DateTime nitmasPending)
        {
            using (var db = GetDb())
            {
                return db.NitmasStarted(nitmasPending);
            }
        }

        public bool NitmasStopped(DateTime nitmasPending, int taskId)
        {
            using (var db = GetDb())
            {
                return db.NitmasStopped(nitmasPending, taskId);
            }
        }

        public bool WasEodLockExecutedForDate(DateTime dateToCheck)
        {
            using (var db = GetDb())
            {
                return
                    db.GetTable<Nitlog>()
                        .Any(x => x.Date1 == dateToCheck.Date && x.TaskNumber == NitmasTaskId.NitmasEnd.ToString().PadLeft(3, '0'));
            }
        }

        public bool WasPickupExecuted(int virtualCashierNumber)
        {
            using (var db = GetDb())
            {
                var resultParameter = db.Parameter("@pickupProcessEnd", null);
                resultParameter.Direction = ParameterDirection.Output;
                var result = db.SetSpCommand(
                    "CheckPickupProcessEnd", db.Parameter("@ownerID", virtualCashierNumber), resultParameter).ExecuteScalar(ScalarSourceType.OutputParameter, "pickupProcessEnd");
                return (bool)result;
            }
        }

        public bool IsPickupInProgress(int virtualCashierNumber)
        {
            using (var db = GetDb())
            {
                var resultParameter = db.Parameter("@pickupProcessStart", null);
                resultParameter.Direction = ParameterDirection.Output;
                var result = db.SetSpCommand(
                    "CheckPickupProcessStart", db.Parameter("@ownerID", virtualCashierNumber), resultParameter).ExecuteScalar(ScalarSourceType.OutputParameter, "pickupProcessStart");
                return (bool)result;
            }
        }

        public bool WasBankingExecuted(DateTime dateToCheck)
        {
            using (var db = GetDb())
            {
                var bankingFinished = db.Parameter("@bankingFinished", null);
                bankingFinished.Direction = ParameterDirection.Output;
                var result = db.SetSpCommand("CheckBankingFinished", db.Parameter("@bankingDate", dateToCheck), bankingFinished).ExecuteScalar(ScalarSourceType.OutputParameter, "bankingFinished");
                return (bool)result;
            }
        }

        public bool IsBankingInProgress(DateTime dateToCheck)
        {
            using (var db = GetDb())
            {
                var bankingStarted = db.Parameter("@bankingStarted", null);
                bankingStarted.Direction = ParameterDirection.Output;
                var result = db.SetSpCommand("CheckBankingStarted", db.Parameter("@bankingDate", dateToCheck), bankingStarted).ExecuteScalar(ScalarSourceType.OutputParameter, "bankingStarted");
                return (bool)result;
            }
        }

        public void InsertGapWalk(GapWalk gapWalk)
        {
            Insert(gapWalk);
        }

        public List<GapWalk> SelectGapWalks(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetTable<GapWalk>().Where(x => x.DateCreated.Date == date.Date).ToList();
            }
        }

        public List<GapWalkReportLine> CreateGapWalkReport(DateTime reportDate)
        {
            using (var db = GetDb())
            {
                var result = db.SetSpCommand("GapWalkGetItems", db.Parameter("@Date", reportDate)).ExecuteList<GapWalkReportLine>(); 
                return result;
            }
        }
    }
}
