using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Repositories
{
    public class DictionariesRepository : BaseRepository, IDictionariesRepository
    {
        public List<decimal> GetVatRatesListFromRetopt()
        {
            using (var db = GetDb())
            {
                var vatRates = db.GetTable<RetailOptions>().First();

                return new List<decimal>
                {
                    vatRates.VatRate1,
                    vatRates.VatRate2,
                    vatRates.VatRate3,
                    vatRates.VatRate4,
                    vatRates.VatRate5,
                    vatRates.VatRate6,
                    vatRates.VatRate7,
                    vatRates.VatRate8,
                    vatRates.VatRate9
                };
            }
        }

        public string GetStoreId()
        {
            using (var db = GetDb())
            {
                return db.GetTable<RetailOptions>().Select(r => r.StoreId).First();
            }
        }

        public int GetAbsoluteStoreId()
        {
            string storeId = GetStoreId();
            return RetailOptions.GetAbsoluiteStoreId(storeId);
        }

        public string GetDefaultSystemCurrency()
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemCurrency>()
                    .Where(c => c.IsDefault)
                    .Select(c => c.Id).First();
            }
        }

        public virtual Settings GetSettings()
        {
            var settings = new Settings();

            using (var db = GetDb())
            {
                var query =
                    from p in db.GetTable<Parameter>()
                    where p.Id == ParameterId.ApiAddressId ||
                          p.Id == ParameterId.MaxRetriesNumberId ||
                          p.Id == ParameterId.RetryPeriodMinutesId ||
                          p.Id == ParameterId.UserManagmentServiceUrl
                    select p;

                foreach (var parameter in query)
                {
                    switch (parameter.Id)
                    {
                        case ParameterId.ApiAddressId:
                            settings.ApiAddress = parameter.StringValue;
                            break;
                        case ParameterId.MaxRetriesNumberId:
                            settings.MaxRetriesNumber = int.Parse(parameter.StringValue);
                            break;
                        case ParameterId.RetryPeriodMinutesId:
                            settings.RetryPeriodMinutes = int.Parse(parameter.StringValue);
                            break;
                        case ParameterId.UserManagmentServiceUrl:
                            settings.UserManagmentServiceUrl = parameter.StringValue;
                            break;
                        default: 
                            // we have no field to be filled as default
                            break;
                    }
                }
            }

            return settings;
        }

        public string GetSettingStringValue(int parameterId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Parameter>()
                    .Where(p => p.Id == parameterId)
                    .Select(p => p.StringValue)
                    .FirstOrDefault();
            }
        }

        public RetailOptions SelectRetailOptions()
        {
            using (var db = GetDb())
            {
                return db.GetTable<RetailOptions>().First();
            }
        }

        public IDictionary<string, Syscod> SelectSystemCodes(string systemCodeType)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Syscod>().Where(x => x.Type == systemCodeType).ToDictionary(x => x.Code);
            }
        }

        public IDictionary<string, SystemCode> SelectNewSystemCodes(string systemCodeType)
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemCode>().Where(x => x.Type == systemCodeType).ToDictionary(x => x.Id.ToString());
            }
        }

        public int? GetVirtualCashierId(int tillNumber)
        {
            using (var db = GetDb())
            {
                return db.GetVirtualCashierId(tillNumber);
            }
        }

        public List<DeliverySlot> GetDeliverySlots()
        {
            using (var db = GetDb())
            {
                return db.GetDeliverySlots();
            }
        }
    }
}
