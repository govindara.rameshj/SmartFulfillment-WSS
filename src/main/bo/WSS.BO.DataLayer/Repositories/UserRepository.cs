using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Entities;
using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using System.Data.SqlClient;
using System.Linq;


namespace WSS.BO.DataLayer.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public int UserInsert(IUser updateUser)
        {
            using (var db = GetDb())
            {
                return db.UserInsert(updateUser.GetId(), updateUser.GetCode(), updateUser.Name, updateUser.Initials, updateUser.Position, updateUser.PayrollId, updateUser.Outlet,
                                     updateUser.TillReceiptName, updateUser.LanguageCode, updateUser.ProfileId, updateUser.Password, updateUser.PasswordExpires, 
                                     updateUser.SuperPassword, updateUser.SuperPasswordExpires, updateUser.IsManager, updateUser.IsSupervisor);
            }
        }

        public int UserUpdate(IUser updateUser)
        {
            using (var db = GetDb())
            {
                return db.UserUpdate(updateUser.GetId(), updateUser.GetCode(), updateUser.Name, updateUser.Initials, updateUser.Position, updateUser.PayrollId, updateUser.Outlet, 
                                     updateUser.TillReceiptName, updateUser.LanguageCode, updateUser.ProfileId, updateUser.GetIsDeleted(), updateUser.GetDeletedDate(), updateUser.GetDeletedBy(), 
                                     updateUser.GetDeletedWhere(), updateUser.Password, updateUser.PasswordExpires, updateUser.SuperPassword, updateUser.SuperPasswordExpires, 
                                     updateUser.IsManager, updateUser.IsSupervisor);
            }
        }

        public void UpdateSynchronizedWhen(int idUser)
        {
            using (var db = GetDb())
            {
                db.GetTable<SystemUser>()
                    .Where(r => r.Id == idUser)
                    .Set(r => r.SynchronizedWhen, DateTime.Now)
                    .Update();
            }
        }

        public void UpdateSynchronizationFailedWhen(int idUser)
        {
            using (var db = GetDb())
            {
                db.GetTable<SystemUser>()
                    .Where(r => r.Id == idUser)
                    .Set(r => r.SynchronizationFailedWhen, DateTime.Now)
                    .Update();
            }
        }
    }
}
