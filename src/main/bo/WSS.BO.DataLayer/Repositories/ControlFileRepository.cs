using System;
using System.Collections.Generic;
using System.Linq;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Database;
using System.Diagnostics;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.DataLayer.Repositories
{
    public class ControlFileRepository : BaseRepository, IControlFileRepository
    {
        #region IControlFileRepository Members

        public IList<ControlFile> GetControlFile(string fileTypeName, DateTime transactionDate, bool flag)
        {
            using (var db = GetDb())
            {
                return db.GetTable<ControlFile>().Where(
                    x => x.FileName.Equals(fileTypeName) 
                        && x.TransactionDate == transactionDate
                        && x.Flag == flag).ToList();
            }
        }

        public ControlFile GetLatestAvailableVersionRecord(string fileName)
        {
            using (var db = GetDb())
            {
                var record = db.GetTable<ControlFile>().FirstOrDefault(
                    x => x.FileName.Equals(fileName)
                        && x.VersionNumber.Equals(VersionMarker.LastAvailable));
                if (record == null)
                {
                    record = CreateLastAvailableVersionRecord(fileName);
                    Insert(db, record);
                }
                return record;
            }
        }

        public void Delete(string fileTypeName, string version)
        {
            using (var db = GetDb())
            {
                db.GetTable<ControlFile>().Where(
                    x => x.FileName.Equals(fileTypeName)
                        && x.VersionNumber.Equals(version))
                        .Delete();
            }
        }

        public void UpdateLatestAvailableVersionRecord(ControlFile record)
        {
            using (var db = GetDb())
            {
                db.GetTable<ControlFile>()
                    .Where(x => x.FileName.Equals(record.FileName) && x.VersionNumber.Equals(VersionMarker.LastAvailable))
                    .Set(x => x.LastestAvailableVersionNumber, record.VersionNumber)
                    .Set(x => x.PVersionNumber, record.VersionNumber)
                    .Set(x => x.LastestAvailableSequenceNumber, record.SequenceNumber)
                    .Set(x => x.PSequenceNumber, record.SequenceNumber)
                    .Update();
            }
        }

        public void InsertControlFile(ControlFile controlFile)
        {
            using (var db = GetDb())
            {
                Insert(db, controlFile);
            }
        }

        private ControlFile CreateLastAvailableVersionRecord(string fileName)
        {
            var record = new ControlFile();
            record.FileName = fileName;
            record.VersionNumber = VersionMarker.LastAvailable;
            return record;
        }

        #endregion
    }
}
