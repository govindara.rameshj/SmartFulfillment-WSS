﻿
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.BO.DataLayer.Database
{
    interface IOasysDbFactory : IRepositoryConnectionFactory
    {
        OasysDb CreateOasysDb();
    }
}
