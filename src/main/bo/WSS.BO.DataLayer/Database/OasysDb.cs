using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using BLToolkit.Data;
using Cts.Oasys.Core;
using Cts.Oasys.Core.Net4Replacements;
using System.Text;
using BLToolkit.Common;
using WSS.BO.DataLayer.Model.Entities;
using BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Utility.Disposing;
using slf4net;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.DataLayer.Database
{
    public class OasysDb : DbManager, IDisposable
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(OasysDb));
        private static readonly Lazy<String> ConnStr = new Lazy<String>(ConnectionStringHelper.GetSqlConnectionString);
        private readonly int? commandTimeout;

        private readonly IDisposingStrategy disposingStrategy = new SelfControlledDisposingStrategy();

        public OasysDb(SqlConnection connection) : base(connection) 
        { 
        }

        public OasysDb(IOasysDbConfiguration oasysDbConfiguration)
            : this(oasysDbConfiguration, null)
        {
        }

        public OasysDb(IOasysDbConfiguration oasysDbConfiguration, IDisposingStrategy disposingStrategy)
            : this(new SqlConnection(oasysDbConfiguration.ConnectionString))
        {
            if (disposingStrategy != null)
            {
                this.disposingStrategy = disposingStrategy;
            }

            if (oasysDbConfiguration.CommandTimeout != null)
            {
                commandTimeout = oasysDbConfiguration.CommandTimeout;
            }
        }

        public OasysDb()
            : this(new SqlConnection(ConnStr.Value))
        {
        }

        #region Tables

        public Table<Alert> AlertTable { get { return GetTable<Alert>(); } }
        public Table<CardList> CardListTable { get { return GetTable<CardList>(); } }
        public Table<CardScheme> CardSchemesTable { get { return GetTable<CardScheme>(); } }
        public Table<EventChange> EventChangeTable { get { return GetTable<EventChange>(); } }
        public Table<PriceChange> PriceChangeTable { get { return GetTable<PriceChange>(); } }
        public Table<CashierFlashTotal> CashierFlashTotalsTable { get { return GetTable<CashierFlashTotal>(); } }
        public Table<DailyGiftToken> DailyGiftTokensTable { get { return GetTable<DailyGiftToken>(); } }
        public Table<GiftToken> GiftTokensTable { get { return GetTable<GiftToken>(); } }
        public Table<Dlcomm> DlcommTable { get { return GetTable<Dlcomm>(); } }
        public Table<DailyTillLine> DailyTillLinesTable { get { return GetTable<DailyTillLine>(); } }
        public Table<DailyReformattedTillTotals> DailyReformattedTillTotalsTable { get { return GetTable<DailyReformattedTillTotals>(); } }
        public Table<Dltots> DltotsTable { get { return GetTable<Dltots>(); } }
        public Table<SystemUser> SystemUsersTable { get { return GetTable<SystemUser>(); } }
        public Table<SystemOption> SystemOptionsTable { get { return GetTable<SystemOption>(); } }
        public Table<SystemDate> SystemDateTable { get { return GetTable<SystemDate>(); } }
        public Table<SupplierMaster> SupplierMasterTable { get { return GetTable<SupplierMaster>(); } }
        public Table<StockMaster> StockMasterTable { get { return GetTable<StockMaster>(); } }
        public Table<Parameter> ParametersTable { get { return GetTable<Parameter>(); } }
        public Table<NetworkIdSystem> NetworkIdSystemTable { get { return GetTable<NetworkIdSystem>(); } }
        public Table<OrderSourceCode> OrderSourceCodesTable { get { return GetTable<OrderSourceCode>(); } }
        public Table<CustomerOrderHeader> CustomerOrderHeadersTable { get { return GetTable<CustomerOrderHeader>(); } }
        public Table<CustomerOrderHeader4> CustomerOrderHeaders4Table { get { return GetTable<CustomerOrderHeader4>(); } }
        public Table<StockAdjustmentFile> StockAdjustmentFileTable { get { return GetTable<StockAdjustmentFile>(); } }
        public Table<SkuWord> SkuWordTable { get { return GetTable<SkuWord>(); } }
        public Table<Sku> SkuTable { get { return GetTable<Sku>(); } }
        public Table<PicCountHeaderFile> PicCountHeaderFileTable { get { return GetTable<PicCountHeaderFile>(); } }
        public Table<RetailOptions> RetailOptionsTable { get { return GetTable<RetailOptions>(); } }
        public Table<PicCountDetailFile> PicCountDetailFileTable { get { return GetTable<PicCountDetailFile>(); } }
        public Table<NightlyProcedure> NightlyProcedureRecordTable { get { return GetTable<NightlyProcedure>(); } }
        public Table<HierarchyWord> HierarchyWordTable { get { return GetTable<HierarchyWord>(); } }
        public Table<DrlSummary> DrlSummaryTable { get { return GetTable<DrlSummary>(); } }
        public Table<Evtenq> EvtenqTable { get { return GetTable<Evtenq>(); } }
        public Table<EventDealGroup> EventDealGroupsTable { get { return GetTable<EventDealGroup>(); } }
        public Table<DailyGiftCard> DailyGiftCardsTable { get { return GetTable<DailyGiftCard>(); } }
        public Table<EventHeader> EventHeadersTable { get { return GetTable<EventHeader>(); } }
        public Table<EventMaster> EventMastersTable { get { return GetTable<EventMaster>(); } }
        public Table<VisionPayment> VisionPaymentTable { get { return GetTable<VisionPayment>(); } }
        public Table<Report> ReportTable { get { return GetTable<Report>(); } }
        public Table<ReportColumn> ReportColumnTable { get { return GetTable<ReportColumn>(); } }
        public Table<ReportColumns> ReportColumnsTable { get { return GetTable<ReportColumns>(); } }
        public Table<ReportHyperlink> ReportHyperlinkTable { get { return GetTable<ReportHyperlink>(); } }
        public Table<ReportParameters> ReportParametersTable { get { return GetTable<ReportParameters>(); } }
        public Table<ReportTable> ReportTableTable { get { return GetTable<ReportTable>(); } }
        public Table<ReportGrouping> ReportGroupingTable { get { return GetTable<ReportGrouping>(); } }
        public Table<ReportRelation> ReportRelationTable { get { return GetTable<ReportRelation>(); } }
        public Table<ReportSummary> ReportSummaryTable { get { return GetTable<ReportSummary>(); } }
        public Table<MenuConfig> MenuConfigTable { get { return GetTable<MenuConfig>(); } }
        public Table<ProfilemenuAccess> ProfilemenuAccessTable { get { return GetTable<ProfilemenuAccess>(); } }

        #endregion

        #region Stored Procedures

        public void CreateSafeForNonTradingDay(DateTime date)
        {
            SetSpCommand("dbo.CreateSafeForNonTradingDay", Parameter("@PeriodDate", date)).ExecuteNonQuery();
        }

        public string GetNextOrderNumber()
        {
            var result = SetSpCommand("dbo.SystemNumbersGetNext", Parameter("@Id", 10)).ExecuteScalar(ScalarSourceType.ReturnValue);
            return ((int)result).ToString("D6");
        }

        public string OverrideDeadlockPriority(string newDeadlockPriority)
        {
            string cmd = String.Format(@"select deadlock_priority as previousPriority from sys.dm_exec_sessions where session_id=@@spid; set deadlock_priority {0};", newDeadlockPriority);
            return SetCommand(cmd).ExecuteScalar<string>(ScalarSourceType.DataReader, new NameOrIndexParameter("previousPriority"));
        }

        public void RestoreDeadlockPriority(string previousDeadlockPriority)
        {
            string cmd = String.Format("set deadlock_priority {0}", previousDeadlockPriority);
            SetCommand(cmd).ExecuteNonQuery();
        }

        public string GetTransactionNumber(string tillNumber)
        {
            var result = SetSpCommand("dbo.GetTransactionNumberForTill", Parameter("@Id", Int32.Parse(tillNumber))).ExecuteScalar(ScalarSourceType.ReturnValue);
            return ((int) result).ToString("D4");
        }

        public int? GetVirtualCashierId(int tillNumber)
        {
            return SetCommand("select [dbo].[svf_GetCashierIDFromOVC_Mapping_Cashiers](@tillNumber)", Parameter("@tillNumber", tillNumber))
                .ExecuteScalar<int?>(ScalarSourceType.DataReader);
        }

        public List<DeliverySlot> GetDeliverySlots()
        {
            return SetSpCommand("dbo.GetDeliverySlots")
                .ExecuteList<DeliverySlot>();
        }

        public bool NitmasStarted(DateTime nitmasPending)
        {
            var resultParameter = Parameter("@Started", null);
            resultParameter.Direction = ParameterDirection.Output;

            var result = SetSpCommand("dbo.NitmasStarted", Parameter("@NitmasPending", nitmasPending), resultParameter).ExecuteScalar(ScalarSourceType.OutputParameter, "Started");
            return (bool)result;
        }

        public bool NitmasStopped(DateTime nitmasPending, int taskId)
        {
            var resultParameter = Parameter("@Stopped", null);
            resultParameter.Direction = ParameterDirection.Output;
            var result = SetSpCommand("dbo.NitmasStopped", Parameter("@NitmasPending", nitmasPending), Parameter("@TaskId", taskId), resultParameter)
                .ExecuteScalar(ScalarSourceType.OutputParameter, "Stopped");
            return (bool)result;
        }

        public string GetNextTillTranNumber()
        {
            var parameter = Parameter(ParameterDirection.Output, "@TillTranNumber", null);
            return SetSpCommand("dbo.usp_GetNextTillTranNumber", parameter)
                .ExecuteScalar<string>(ScalarSourceType.OutputParameter, "TillTranNumber");
        }

        public DataTable GetBankingReportOnDashBoard(DateTime date)
        {
            return SetSpCommand("dbo.DashBanking", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetBankingReportsReportOnDashBoard(DateTime date)
        {
            return SetSpCommand("dbo.DashActivity2", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetDrlAnalysisReportOnDashBoard(DateTime date)
        {
            return SetSpCommand("dbo.DashDrl", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetPendingShrinkageReportOnDashBoard()
        {
            return SetSpCommand("dbo.DashPendingShrinkage").ExecuteDataTable();
        }

        public DataTable GetPriceChangesReportOnDashBoard(DateTime date)
        {
            return SetSpCommand("dbo.DashPriceChanges2", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetShrinkageReportOnDashBoard(DateTime date)
        {
            return SetSpCommand("dbo.DashShrinkage", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetStoreSalesReportOnDashboard(DateTime date)
        {
            return SetSpCommand("dbo.DashSales2", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetPendingOrdersReportOnDashboard(DateTime date)
        {
            return SetSpCommand("dbo.DashQod", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DataTable GetStockReportsReportOnDashboard(DateTime date)
        {
            return SetSpCommand("dbo.DashStock2", Parameter("@DateEnd", date)).ExecuteDataTable();
        }

        public DateTime GetThisDayLastYear(DateTime date)
        {
            return SetSpCommand("dbo.udf_GetThisDayLastYear", Parameter("@DateIn", date)).ExecuteScalar<DateTime>(ScalarSourceType.ReturnValue);
        }

        public void StockLogInsertCheck91(string skuNumber, int stockStart)
        {
            StockLogInsertCheck91(skuNumber, stockStart, null);
        }

        public void StockLogInsertCheck91(string skuNumber, int stockStart, string userId)
        {
            var parameters = new object[]
                {
                    Parameter("@SkuNumber", skuNumber),
                    Parameter("@StockStart", stockStart),
                    string.IsNullOrEmpty(userId) ? Parameter("@UserId", userId) : Parameter("@UserId", Int32.Parse(userId))
                };

            SetSpCommand("dbo.StockLogInsertCheck91", parameters).ExecuteNonQuery();
        }

        public int UserInsert(int id, string employeeCode, string name, string initials, string position, string payrollId, string outlet, string tillReceiptName, string languageCode, 
                              int profiledId, string password, DateTime passwordExpires, string superPassword = null, DateTime? superPasswordExpires = null, bool isManager = false, 
                              bool isSupervisior = false)
        {
            var parameters = new List<object>
                {
                    Parameter("@Id", id),
                    Parameter("@EmployeeCode", employeeCode),
                    Parameter("@Name", name),
                    Parameter("@Initials", initials),
                    Parameter("@Position", position),
                    Parameter("@PayrollId", payrollId),
                    Parameter("@Outlet", outlet),
                    Parameter("@TillReceiptName", tillReceiptName),
                    Parameter("@LanguageCode", languageCode),
                    Parameter("@ProfileId", profiledId),
                    Parameter("@Password", password),
                    Parameter("@PasswordExpires", passwordExpires),
                    Parameter("@IsManager", isManager),
                    Parameter("@IsSupervisor", isSupervisior)
                };

            if (!String.IsNullOrEmpty(superPassword) && superPassword.Trim().Length > 0) 
            {
                parameters.Add(Parameter("@SuperPassword", superPassword));
                parameters.Add(Parameter("@SuperPasswordExpires", superPasswordExpires));
            }

            return SetSpCommand("dbo.UserInsert", parameters.ToArray()).ExecuteNonQuery();
        }

        public int UserUpdate(int id, string employeeCode, string name, string initials, string position, string payrollId, string outlet, string tillReceiptName, string languageCode,
                              int profiledId, bool isDeleted = false, DateTime? deletedDate = null, string deletedBy = null, string deletedWhere = null, string password = null, 
                              DateTime? passwordExpires = null, string superPassword = null, DateTime? superPasswordExpires = null, bool isManager = false, bool isSupervisior = false)
        {
            var parameters = new List<object>
                {
                    Parameter("@Id", id),
                    Parameter("@EmployeeCode", employeeCode),
                    Parameter("@Name", name),
                    Parameter("@Initials", initials),
                    Parameter("@Position", position),
                    Parameter("@PayrollId", payrollId),
                    Parameter("@Outlet", outlet),
                    Parameter("@TillReceiptName", tillReceiptName),
                    Parameter("@LanguageCode", languageCode),
                    Parameter("@ProfileId", profiledId),
                    Parameter("@IsManager", isManager),
                    Parameter("@IsSupervisor", isSupervisior)
                };

            if (isDeleted)
            {
                parameters.Add(Parameter("@IsDeleted", isDeleted));
                parameters.Add(Parameter("@DeletedDate", deletedDate));
                parameters.Add(Parameter("@DeletedBy", deletedBy));
                parameters.Add(Parameter("@DeletedWhere", deletedWhere));
            }

            if (!String.IsNullOrEmpty(password))
            {
                parameters.Add(Parameter("@Password", password));
                parameters.Add(Parameter("@PasswordExpires", passwordExpires));
            }

            if (!String.IsNullOrEmpty(superPassword) && superPassword.Trim().Length > 0)
            {
                parameters.Add(Parameter("@SuperPassword", superPassword));
                parameters.Add(Parameter("@SuperPasswordExpires", superPasswordExpires));
            }

            return SetSpCommand("dbo.UserUpdate", parameters.ToArray()).ExecuteNonQuery();
        }

        #endregion

        #region Logging

        protected override void OnBeforeOperation(OperationType op)
        {
            if (Log.IsDebugEnabled)
            {
                Log.Debug(BuildOperationLogMessage(op, true));
            }

            base.OnBeforeOperation(op);
        }

        protected override void OnAfterOperation(OperationType op)
        {
            base.OnAfterOperation(op);

            if (Log.IsDebugEnabled)
            {
                Log.Debug(BuildOperationLogMessage(op, false));
            }
        }

        private string BuildOperationLogMessage(OperationType op, bool isBefore)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("OasysDB.On{0}Operation: OperationType = {1}", isBefore ? "Before" : "After", op);

#pragma warning disable 612
            if (op == OperationType.ExecuteReader || op == OperationType.ExecuteScalar || op == OperationType.ExecuteNonQuery)
#pragma warning restore 612
            {
                sb.AppendFormat(", CommandHash={0}", Command.GetHashCode());

                if (isBefore)
                {
                    var generator = new SqlGenerator();
                    string sql = generator.CreateExecutableSqlStatement(Command.CommandText, Command.CommandType, Command.Parameters.Cast<SqlParameter>().ToArray());
                    sb.AppendFormat(", CommandSql:\n{0}", sql);
                }
            }

            return sb.ToString();
        }

        #endregion

        protected override IDbCommand OnInitCommand(IDbCommand command)
        {
            var commandWithTimeout = base.OnInitCommand(command);

            if (commandTimeout != null)
            {
                commandWithTimeout.CommandTimeout = (int)commandTimeout;
            }

            return commandWithTimeout;
        }

        void IDisposable.Dispose()
        {
            if (disposingStrategy.AllowSelfDispose())
            {
                base.Dispose();
            }
        }

        internal void OuterDispose()
        {
            base.Dispose();
        }

        #region Prohibited transaction methods

        private static T ProhibitTransactionMethodsUsage<T>()
        {
            throw new InvalidOperationException(
                "Clients should not use OasysDB transaction methods directly. Use UnitOfWork instead.");
        }

        internal DbManagerTransaction InternalBeginTransaction()
        {
            return base.BeginTransaction();
        }

        internal DbManager InternalCommitTransaction()
        {
            return base.CommitTransaction();
        }

        internal DbManager InternalRollbackTransaction()
        {
            return base.RollbackTransaction();
        }

        [Obsolete]
        public new DbManagerTransaction BeginTransaction(IsolationLevel il)
        {
            return ProhibitTransactionMethodsUsage<DbManagerTransaction>();
        }

        [Obsolete]
        public new DbManagerTransaction BeginTransaction()
        {
            return ProhibitTransactionMethodsUsage<DbManagerTransaction>();
        }

        [Obsolete]
        public new DbManager CommitTransaction()
        {
            return ProhibitTransactionMethodsUsage<DbManager>();
        }

        [Obsolete]
        public new DbManager RollbackTransaction()
        {
            return ProhibitTransactionMethodsUsage<DbManager>();
        }

        #endregion
    }
}
