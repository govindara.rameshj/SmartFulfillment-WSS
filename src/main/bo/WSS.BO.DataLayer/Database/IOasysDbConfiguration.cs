﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Database
{
    public interface IOasysDbConfiguration
    {
        string ConnectionString { get;}
        int? CommandTimeout { get;}
    }
}
