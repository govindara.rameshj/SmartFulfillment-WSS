﻿namespace WSS.BO.DataLayer.Database
{
    class SimpleOasysDbFactory : IOasysDbFactory
    {
        private readonly IOasysDbConfiguration oasysDbConfiguration;

        public SimpleOasysDbFactory(IOasysDbConfiguration oasysDbConfiguration)
        {
            this.oasysDbConfiguration = oasysDbConfiguration;
        }


        public OasysDb CreateOasysDb()
        {
            return new OasysDb(oasysDbConfiguration);
        }
    }
}
