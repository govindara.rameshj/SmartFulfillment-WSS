﻿using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
namespace WSS.BO.DataLayer
{
    public class DataLayerFactory : IDataLayerFactory
    {
        private readonly IRepositoriesFactory outerRepoFactory;
        private readonly IOasysDbConfiguration oasysDbConfiguration;

        static DataLayerFactory()
        {
            BLToolkit.Common.Configuration.NullableValues.String = null;
        }

        public DataLayerFactory(IRepositoriesFactory outerRepoFactory, IOasysDbConfiguration oasysDbConfiguration)
        {
            this.outerRepoFactory = outerRepoFactory;
            this.oasysDbConfiguration = oasysDbConfiguration;
        }

        public T Create<T>() where T : IRepository
        {
            var repo = outerRepoFactory.Create<T>();
            repo.SetConnectionFactory(new SimpleOasysDbFactory(oasysDbConfiguration));
            return repo;
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(outerRepoFactory, oasysDbConfiguration);
        }
    }
}
