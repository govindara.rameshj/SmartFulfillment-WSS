﻿using System;
using slf4net;
using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Utility.Disposing;

namespace WSS.BO.DataLayer
{
    public class UnitOfWork : IUnitOfWork, IOasysDbFactory
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(UnitOfWork));

        private readonly IRepositoriesFactory outerRepoFactory;
        private readonly OasysDb db;
        private bool completed;
        private string deadlockPriorityToRestore;

        public UnitOfWork(IRepositoriesFactory outerRepoFactory, IOasysDbConfiguration oasysDbConfiguration)
        {
            this.outerRepoFactory = outerRepoFactory;

            db = new OasysDb(oasysDbConfiguration, new OwnerControlledDisposingStrategy());
            completed = false;
            db.InternalBeginTransaction();
        }

        #region IUnitOfWork

        public void Commit()
        {
            Complete(true);
        }

        public void Rollback()
        {
            Complete(false);
        }

        public void SetDeadlockPriority(string deadlockPriority)
        {
            deadlockPriorityToRestore = db.OverrideDeadlockPriority(deadlockPriority);
        }

        public void Dispose()
        {
            if (!completed)
            {
                Rollback();
            }
            db.OuterDispose();
        }

        #endregion

        private void Complete(bool doCommit)
        {
            if (completed)
            {
                throw new InvalidOperationException("The unit of work is already completed.");
            }

            if (doCommit)
            {
                db.InternalCommitTransaction();
                Log.Info("UnitOfWork - commited.");
            }
            else
            {
                db.InternalRollbackTransaction();
                Log.Info("UnitOfWork - rolled back.");
            }

            if (deadlockPriorityToRestore != null)
            {
                db.RestoreDeadlockPriority(deadlockPriorityToRestore);
            }

            db.Dispose();
            completed = true;
        }

        public T Create<T>() where T : IRepository
        {
            var repo = outerRepoFactory.Create<T>();
            repo.SetConnectionFactory(this);
            return repo;
        }

        OasysDb IOasysDbFactory.CreateOasysDb()
        {
            return db;
        }
    }
}
