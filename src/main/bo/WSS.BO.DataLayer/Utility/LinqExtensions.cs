﻿using System;
using System.Linq;
using System.Linq.Expressions;
using BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.BO.DataLayer.Utility
{
    public static class LinqExtensions
    {
        public static QueryableOrUpdatableWrapper<TEntityType> SetAdd<TEntityType, TMemberType>(
            this IUpdatable<TEntityType> query,
            Expression<Func<TEntityType, TMemberType>> memberAccessExp, TMemberType addedValue)
        {
            return new QueryableOrUpdatableWrapper<TEntityType>(query).SetAdd(memberAccessExp, addedValue);
        }

        public static QueryableOrUpdatableWrapper<TEntityType> SetAdd<TEntityType, TMemberType>(
            this IQueryable<TEntityType> query,
            Expression<Func<TEntityType, TMemberType>> memberAccessExp, TMemberType addedValue)
        {
            return new QueryableOrUpdatableWrapper<TEntityType>(query).SetAdd(memberAccessExp, addedValue);
        }

        public static IQueryable<T> WhereRelatedToTran<T>(this IQueryable<T> queryable, IDailyTillTranKey tran)
            where T : IDailyTillTranKey
        {
            return
                queryable.Where(x => x.CreateDate == tran.CreateDate.Date
                        && x.TillNumber == tran.TillNumber
                        && x.TransactionNumber == tran.TransactionNumber);

        }
    }
}
