﻿using System;
using System.Linq;
using System.Linq.Expressions;
using BLToolkit.Data.Linq;
using Cts.Oasys.Core.Helpers;

namespace WSS.BO.DataLayer.Utility
{
    public class QueryableOrUpdatableWrapper<T>
    {
        private readonly IQueryable<T> queryable;
        private readonly IUpdatable<T> updatable;

        private QueryableOrUpdatableWrapper(IQueryable<T> queryable, IUpdatable<T> updatable)
        {
            this.queryable = queryable;
            this.updatable = updatable;
        }

        public QueryableOrUpdatableWrapper(IQueryable<T> queryable)
            : this(queryable, null)
        {
        }

        public QueryableOrUpdatableWrapper(IUpdatable<T> updatable)
            : this(null, updatable)
        {
        }

        public QueryableOrUpdatableWrapper<T> SetAdd<TMemberType>(
            Expression<Func<T, TMemberType>> memberAccessExp, TMemberType addedValue)
        {
            QueryableOrUpdatableWrapper<T> result;
            if (addedValue.Equals(default(TMemberType)))
            {
                result = this;
            }
            else
            {
                var addToMemberExpression = PropertyUtils.GetAddToMemberExpression(memberAccessExp, addedValue);
                IUpdatable<T> newQuery;
                if (queryable != null)
                {
                    newQuery = queryable.Set(memberAccessExp, addToMemberExpression);
                }
                else
                {
                    newQuery = updatable.Set(memberAccessExp, addToMemberExpression);
                }
                result = new QueryableOrUpdatableWrapper<T>(newQuery);
            }
            return result;
        }

        public int Update()
        {
            // if Queryable has not been converted to updatable because all SetAdd adds zero, so dont do any updates.
            return updatable != null ? updatable.Update() : 0;
        }
    }
}