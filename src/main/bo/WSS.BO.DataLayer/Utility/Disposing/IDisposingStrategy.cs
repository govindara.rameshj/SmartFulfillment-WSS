﻿namespace WSS.BO.DataLayer.Utility.Disposing
{
    public interface IDisposingStrategy
    {
        bool AllowSelfDispose();
    }
}
