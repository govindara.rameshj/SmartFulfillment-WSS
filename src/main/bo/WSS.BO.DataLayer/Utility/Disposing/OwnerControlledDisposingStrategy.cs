﻿using System;

namespace WSS.BO.DataLayer.Utility.Disposing
{
    public class OwnerControlledDisposingStrategy : IDisposingStrategy
    {
        public bool AllowSelfDispose()
        {
            return false;
        }
    }
}
