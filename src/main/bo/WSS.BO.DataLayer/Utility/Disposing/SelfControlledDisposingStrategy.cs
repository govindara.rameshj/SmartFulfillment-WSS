﻿namespace WSS.BO.DataLayer.Utility.Disposing
{
    class SelfControlledDisposingStrategy : IDisposingStrategy
    {
        public bool AllowSelfDispose()
        {
            return true;
        }
    }
}
