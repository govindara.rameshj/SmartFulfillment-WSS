<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BankingRepSalesAnalysis
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BankingRepSalesAnalysis))
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdReport = New FarPoint.Win.Spread.FpSpread
        Me.lblDate = New System.Windows.Forms.Label
        Me.cmbDate = New System.Windows.Forms.ComboBox
        Me.grpParameters = New System.Windows.Forms.GroupBox
        Me.btnPeriod = New System.Windows.Forms.Button
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.btnSelect = New System.Windows.Forms.Button
        CType(Me.spdReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpParameters.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(534, 408)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(81, 39)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(621, 408)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdReport
        '
        Me.spdReport.About = "3.0.2004.2005"
        Me.spdReport.AccessibleDescription = "SupplierSpread, Direct, Row 0, Column 0, "
        Me.spdReport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdReport.EditModeReplace = True
        Me.spdReport.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdReport.Location = New System.Drawing.Point(3, 71)
        Me.spdReport.Name = "spdReport"
        Me.spdReport.Size = New System.Drawing.Size(694, 331)
        Me.spdReport.TabIndex = 1
        Me.spdReport.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdReport.TextTipAppearance = TipAppearance1
        Me.spdReport.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdReport.VerticalScrollBarWidth = 15
        Me.spdReport.ActiveSheetIndex = -1
        '
        'lblDate
        '
        Me.lblDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblDate.Location = New System.Drawing.Point(6, 17)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(70, 20)
        Me.lblDate.TabIndex = 0
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbDate
        '
        Me.cmbDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDate.FormattingEnabled = True
        Me.cmbDate.Location = New System.Drawing.Point(82, 16)
        Me.cmbDate.Name = "cmbDate"
        Me.cmbDate.Size = New System.Drawing.Size(150, 21)
        Me.cmbDate.TabIndex = 0
        '
        'grpParameters
        '
        Me.grpParameters.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpParameters.Controls.Add(Me.btnPeriod)
        Me.grpParameters.Controls.Add(Me.dtpDate)
        Me.grpParameters.Controls.Add(Me.btnSelect)
        Me.grpParameters.Controls.Add(Me.lblDate)
        Me.grpParameters.Controls.Add(Me.cmbDate)
        Me.grpParameters.Location = New System.Drawing.Point(3, 3)
        Me.grpParameters.Name = "grpParameters"
        Me.grpParameters.Size = New System.Drawing.Size(694, 62)
        Me.grpParameters.TabIndex = 0
        Me.grpParameters.TabStop = False
        Me.grpParameters.Text = "Select Parameters"
        '
        'btnPeriod
        '
        Me.btnPeriod.BackgroundImage = CType(resources.GetObject("btnPeriod.BackgroundImage"), System.Drawing.Image)
        Me.btnPeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPeriod.FlatAppearance.BorderSize = 0
        Me.btnPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPeriod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPeriod.Location = New System.Drawing.Point(237, 17)
        Me.btnPeriod.Name = "btnPeriod"
        Me.btnPeriod.Size = New System.Drawing.Size(20, 20)
        Me.btnPeriod.TabIndex = 32
        Me.btnPeriod.UseVisualStyleBackColor = True
        '
        'dtpDate
        '
        Me.dtpDate.Location = New System.Drawing.Point(263, 16)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(132, 20)
        Me.dtpDate.TabIndex = 1
        Me.dtpDate.Visible = False
        '
        'btnSelect
        '
        Me.btnSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSelect.Location = New System.Drawing.Point(607, 16)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(81, 39)
        Me.btnSelect.TabIndex = 4
        Me.btnSelect.Text = "F5 Select"
        Me.btnSelect.UseVisualStyleBackColor = True
        '
        'BankingRepSalesAnalysis
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Controls.Add(Me.spdReport)
        Me.Controls.Add(Me.grpParameters)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnExit)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(700, 450)
        Me.Name = "BankingRepSalesAnalysis"
        Me.Size = New System.Drawing.Size(700, 450)
        CType(Me.spdReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpParameters.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents spdReport As FarPoint.Win.Spread.FpSpread
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents cmbDate As System.Windows.Forms.ComboBox
    Friend WithEvents grpParameters As System.Windows.Forms.GroupBox
    Friend WithEvents btnSelect As System.Windows.Forms.Button
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPeriod As System.Windows.Forms.Button

End Class
