Imports BOSales
Imports BOVisionSales

Public Class BankingRepSalesAnalysis
    Private Enum col
        CategoryID
        Category
        Tenders
        Pickups
        Float
        Variance
        TotalText
        Total
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _Period As New BOSystem.cSystemPeriods(_Oasys3DB)
    Private _SysCurrency As New BOSystemCurrency.cSystemCurrency(_Oasys3DB)
    Private _HieCategory As New BOHierarchy.cHierachyCategory(_Oasys3DB)
    Private _SaleHeader As BOSales.cSalesHeader
    Private _VisionHeader As BOVisionSales.cPVTotals
    Private _accountType As String = String.Empty
    Private _storeIdName As String = String.Empty
    Private visionData As DataTable



    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnSelect.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else
                e.Handled = False
                MyBase.Form_KeyDown(sender, e)
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Trace.WriteLine(My.Resources.AppStart, Name)
            Cursor = Cursors.WaitCursor

            DisplayProgress(0, My.Resources.GetBankingDays)
            _Period.LoadCurrent(cSystemPeriods.ClosedStates.All, True)
            cmbDate_Initialise()
            spdReport_Initialise()

            DisplayProgress(30, My.Resources.GetSystemCurrencies)
            _SysCurrency.Currencies = _SysCurrency.LoadMatches

            DisplayProgress(60, My.Resources.GetHierarchyCats)
            _HieCategory.Load()

            DisplayProgress(80, My.Resources.GetAccountabilityType)
            Using retOptions As New BOSystem.cRetailOptions(_Oasys3DB)
                retOptions.LoadStoreAndAccountability()
                _accountType = retOptions.AccountabilityType.Value
                _storeIdName = retOptions.StoreIdName
            End Using

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try

    End Sub



    Private Sub cmbDate_Initialise()

        Dim dt As New DataTable
        dt.Columns.Add("id", GetType(Integer))
        dt.Columns.Add("display", GetType(String))
        Dim minDate As Date = Now.Date

        For Each period As BOSystem.cSystemPeriods In _Period.Periods
            dt.Rows.Add(period.ID.Value, period.ID.Value & Space(3) & period.StartDate.Value.ToShortDateString)
            If period.StartDate.Value < minDate Then minDate = period.StartDate.Value
        Next

        cmbDate.ValueMember = "id"
        cmbDate.DisplayMember = "display"
        cmbDate.DataSource = dt

        dtpDate.MinDate = minDate
        dtpDate.MaxDate = Now.Date

    End Sub

    Private Sub btnPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPeriod.Click

        If dtpDate.Visible Then
            dtpDate.Focus()
        Else
            dtpDate.Visible = True
            dtpDate.Focus()
        End If

    End Sub

    Private Sub dtpStartPeriod_CloseUp(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.CloseUp

        'On close of dtpStartPeriod calendar popup section, finds system period relating to selected start date
        'and sets cmbStartPeriod to this system period.
        If dtpDate.Visible Then
            For Each period As BOSystem.cSystemPeriods In _Period.Periods
                If period.ContainsDate(dtpDate.Value) Then
                    cmbDate.SelectedValue = period.ID.Value
                    Exit For
                End If
            Next
            dtpDate.Visible = False
        End If

    End Sub



    Private Sub spdReport_Initialise()

        'Make F keys available in spread via input maps
        Dim im As InputMap = spdReport.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdReport.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)

        Dim sheet As New Spread.SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.ColumnHeaderVisible = False

        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.VerticalGridLine = New GridLine(GridLineType.None)
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.White
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right

        'Print info stuff
        sheet.PrintInfo.Footer = "/lPrinted: " & Now
        sheet.PrintInfo.Footer &= "/cPage /p of /pc"
        sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        sheet.PrintInfo.Margin.Left = 50
        sheet.PrintInfo.Margin.Right = 50
        sheet.PrintInfo.Margin.Bottom = 20
        sheet.PrintInfo.Margin.Top = 20
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.ShowPrintDialog = True
        sheet.PrintInfo.UseSmartPrint = True
        sheet.PrintInfo.Orientation = PrintOrientation.Portrait


        'Column Headers
        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        sheet.ColumnHeader.Cells(0, col.Category).HorizontalAlignment = CellHorizontalAlignment.Left

        'Create column types.
        Dim typeDecimal As New CellType.NumberCellType
        typeDecimal.DecimalPlaces = 2

        'Columns
        sheet.Columns(col.CategoryID).Visible = False
        sheet.Columns(col.Category).Label = "Category"
        sheet.Columns(col.Category).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(col.Tenders).Label = " "
        sheet.Columns(col.Tenders).CellType = typeDecimal
        sheet.Columns(col.Pickups).Label = " "
        sheet.Columns(col.Pickups).CellType = typeDecimal
        sheet.Columns(col.Float).Label = " "
        sheet.Columns(col.Float).CellType = typeDecimal
        sheet.Columns(col.Variance).Label = " "
        sheet.Columns(col.Variance).CellType = typeDecimal
        sheet.Columns(col.TotalText).Label = " "
        sheet.Columns(col.TotalText).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Columns(col.Total).Label = "Gross Sales"
        sheet.Columns(col.Total).CellType = typeDecimal

        spdReport.Sheets.Add(sheet)

    End Sub

    Private Sub spdReport_AddRows()

        'reset sheet
        Dim sheet As SheetView = spdReport.ActiveSheet
        If sheet.RowCount > 0 Then sheet.Rows.Remove(0, sheet.RowCount)

        'get and add category sales to sheet sorted alphabetically
        Dim dv As DataView = _SaleHeader.Headers.CategorySales
        Dim dv2 As DataView = _VisionHeader.Headers.CategorySales
        If (dv Is Nothing AndAlso dv2 Is Nothing) Then
            spdReport_AddRowsVision()

        Else

            Select Case True
                Case dv Is Nothing AndAlso dv2 Is Nothing
                    MessageBox.Show(My.Resources.NoSales)
                    Exit Sub

                Case dv Is Nothing : dv = dv2
                Case dv2 Is Nothing
                Case Else
                    For Each drv2 As DataRowView In dv2
                        Dim found As Boolean = False

                        For Each drv As DataRowView In dv
                            If CStr(drv("number")) = CStr(drv2("number")) Then
                                found = True
                                drv("value") = CDec(drv("value")) + CDec(drv2("value"))
                                Exit For
                            End If
                        Next

                        If Not found Then
                            Dim newdr As DataRowView = dv.AddNew
                            newdr("name") = drv2("name")
                            newdr("number") = drv2("name")
                            newdr("value") = drv2("value")
                            newdr.EndEdit()
                        End If
                    Next
            End Select

            For Each drv As DataRowView In dv
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Cells(sheet.RowCount - 1, col.CategoryID).Value = drv("number")
                sheet.Cells(sheet.RowCount - 1, col.Category).Value = drv("name")
                sheet.Cells(sheet.RowCount - 1, col.Total).Value = drv("value")
            Next

            'Add category totals
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, False)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Total Sales"
            sheet.Cells(sheet.RowCount - 1, col.Total).Formula = "SUM(R1C:R" & sheet.RowCount - 1 & "C)"


            'Create dictionary values for each currency and initialise to zero.
            Dim dicHeaderTenders As New Dictionary(Of String, Decimal)
            Dim dicHeaderPickups As New Dictionary(Of String, Decimal)
            For Each currency As BOSystemCurrency.cSystemCurrency In _SysCurrency.Currencies
                dicHeaderTenders.Add(currency.ID.Value, 0)
                dicHeaderPickups.Add(currency.ID.Value, 0)
            Next

            'Get miscellaneous income/outgoings and deposits from cashier/till headers.
            Dim headerPickup As Decimal = 0
            Dim headerTender As Decimal = 0
            Dim headerTenderForeign As Decimal = 0
            Dim defaultCurId As String = _SysCurrency.GetDefaultCurrencyID

            Dim cashiers As BOBanking.cCashBalCashier = Nothing
            Dim tills As BOBanking.cCashBalTill = Nothing
            Dim periodDate As String = CStr(_Period.Period(CInt(cmbDate.SelectedValue)).StartDate.Value)
            Dim cDateVal As String

            Select Case _accountType
                Case AccountabilityTypes.Cashier
                    cashiers = New BOBanking.cCashBalCashier(_Oasys3DB)
                    cashiers.LoadCashiers(CInt(cmbDate.SelectedValue))

                    For Each cashier As BOBanking.cCashBalCashier In cashiers.Cashiers

                        'Get tenders and pick ups from cashier tender types collection
                        For Each tender As BOBanking.cCashBalCashierTen In cashier.TenderTypes
                            dicHeaderPickups.Item(tender.CurrencyID.Value) += tender.PickUp.Value
                            dicHeaderTenders.Item(tender.CurrencyID.Value) += tender.Amount.Value

                            'Get total values converting into local currency if necessary
                            If tender.CurrencyID.Value = defaultCurId Then
                                headerPickup += tender.PickUp.Value
                                headerTender += tender.Amount.Value
                            Else
                                headerPickup += CDec(tender.PickUp.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                                headerTender += CDec(tender.Amount.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                                headerTenderForeign += CDec(tender.Amount.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                            End If
                        Next
                    Next

                Case AccountabilityTypes.Till
                    tills = New BOBanking.cCashBalTill(_Oasys3DB)
                    tills.LoadTills(CInt(cmbDate.SelectedValue))

                    'Get tenders and pick ups from till tender types collection
                    For Each till As BOBanking.cCashBalTill In tills.Tills
                        For Each tender As BOBanking.cCashBalTillTen In till.TenderTypes
                            dicHeaderPickups.Item(tender.CurrencyID.Value) += tender.PickUp.Value
                            dicHeaderTenders.Item(tender.CurrencyID.Value) += tender.Amount.Value

                            'Get total values converting into local currency if necessary
                            If tender.CurrencyID.Value = defaultCurId Then
                                headerPickup += tender.PickUp.Value
                                headerTender += tender.Amount.Value
                            Else
                                headerPickup += CDec(tender.PickUp.Value * till.ExchangeRate.Value * Math.Pow(10, till.ExchangePower.Value))
                                headerTender += CDec(tender.Amount.Value * till.ExchangeRate.Value * Math.Pow(10, till.ExchangePower.Value))
                            End If
                        Next
                    Next

                Case Else
                    DisplayWarning(My.Resources.WarnNoAccountabiliry)
                    Exit Sub
            End Select


            cDateVal = periodDate.ToString().Substring(6, 4) + "-"
            cDateVal += periodDate.ToString().Substring(3, 2) + "-"
            cDateVal += periodDate.ToString().Substring(0, 2)
            Dim totVision As New StringBuilder()
            Dim totVisionTable As DataTable
            Dim totVal As Decimal = 0
            'Add miscellaneous income
            Dim rowSubs As Integer = sheet.RowCount
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+ Misc Income"
            Select Case _accountType

                Case AccountabilityTypes.Cashier

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = (cashiers.Cashiers.MiscIns + totVal)
                Case AccountabilityTypes.Till

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = (tills.Tills.MiscIns + totVal)
            End Select

            'add paid outs
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "- Paid Out"
            Select Case _accountType
                Case AccountabilityTypes.Cashier
                    totVision = New StringBuilder()
                    totVisionTable = New DataTable()
                    totVal = 0

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = (cashiers.Cashiers.MiscOuts + totVal)
                Case AccountabilityTypes.Till
                    totVision = New StringBuilder()
                    totVisionTable = New DataTable()
                    totVal = 0

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = (tills.Tills.MiscOuts + totVal)
            End Select


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Dhanesh Ramachandran
            '
            ' Date        : 16/09/2010
            '
            ' Change      : Vision Sales
            '
            ' Notes       : Added  + Deposit row to the excel sheet and populating it with 
            '               corresponding vision sale values.
            '
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code Change Starts
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+/- Deposits"
            Select Case _accountType
                Case AccountabilityTypes.Cashier
                    totVision = New StringBuilder()
                    totVisionTable = New DataTable()
                    totVal = 0

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = -(totVal)
                Case AccountabilityTypes.Till
                    totVision = New StringBuilder()
                    totVisionTable = New DataTable()
                    totVal = 0

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = -(totVal)
            End Select
            'Code Change Ends


            'add floats
            Dim floats As Decimal = 0
            sheet.Rows.Add(sheet.RowCount, 1)
            Select Case _accountType
                Case AccountabilityTypes.Cashier : floats = cashiers.Cashiers.FloatIssued(defaultCurId)
                Case AccountabilityTypes.Till : floats = tills.Tills.FloatIssued(defaultCurId)
            End Select
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+ Floats"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = floats

            'add total row
            sheet.AddRows(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Total"
            sheet.Cells(sheet.RowCount - 1, col.Total).Formula = "SUM(R" & rowSubs & "C:R" & sheet.RowCount - 1 & "C)"
            sheet.Cells(sheet.RowCount - 1, col.Total).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            Dim rowTotal As Integer = sheet.RowCount

            'Add tender breakdown headers.
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).CellType = New CellType.TextCellType
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.SetValue(sheet.RowCount - 1, col.Tenders, "Tenders")
            sheet.SetValue(sheet.RowCount - 1, col.Pickups, "Pick ups")
            sheet.SetValue(sheet.RowCount - 1, col.Float, "Float")
            sheet.SetValue(sheet.RowCount - 1, col.Variance, "Variance")


            'Add each currency to sheet for tender breakdown.
            For Each currency As BOSystemCurrency.cSystemCurrency In _SysCurrency.Currencies
                sheet.AddRows(sheet.RowCount, 1)
                sheet.SetValue(sheet.RowCount - 1, col.Category, currency.ID.Value & Space(1) & "(" & currency.Symbol.Value & ")")
                sheet.SetValue(sheet.RowCount - 1, col.Tenders, dicHeaderTenders.Item(currency.ID.Value))
                sheet.SetValue(sheet.RowCount - 1, col.Pickups, dicHeaderPickups.Item(currency.ID.Value))
                sheet.SetValue(sheet.RowCount - 1, col.Float, 0)
                If currency.IsDefault.Value Then
                    sheet.SetValue(sheet.RowCount - 1, col.Float, floats)
                End If
                sheet.SetFormula(sheet.RowCount - 1, col.Variance, "RC[-2]-RC[-3]-RC[-1]")
                sheet.Cells(sheet.RowCount - 1, col.Category).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            Next


            'Add tender breakdown totals.
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.Category).Value = "Total (" & _SysCurrency.Currency(defaultCurId).Symbol.Value & ")"
            sheet.Cells(sheet.RowCount - 1, col.Category).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Tenders).Value = headerTender
            sheet.Cells(sheet.RowCount - 1, col.Pickups).Value = headerPickup
            sheet.Cells(sheet.RowCount - 1, col.Float).Value = floats
            sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-3]-RC[-1]"
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Pickups"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = headerPickup
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)

            'add grand total row
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Variance"
            sheet.Cells(sheet.RowCount - 1, col.Total).Formula = "R[-1]C - R" & rowTotal & "C"
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Currency Variance"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = headerTenderForeign - _SaleHeader.Paids.ForeignPayments

            spdReport.Visible = True
            If sheet.RowCount > 0 Then
                sheet.ColumnHeaderVisible = True
                spd_Resize(spdReport, New EventArgs)
                btnPrint.Visible = True
            Else
                btnPrint.Visible = False
            End If
        End If

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    '
    ' Date        : 01/10/2010
    '
    ' Change      : Vision Sales
    '
    ' Notes       : Added method to show report even if there is no sales
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub spdReport_AddRowsVision()
        'reset sheet
        If (CDbl(visionData.Rows(0).Item(0).ToString()) > 0) Then


            Dim sheet As SheetView = spdReport.ActiveSheet
            If sheet.RowCount > 0 Then sheet.Rows.Remove(0, sheet.RowCount)
            'Add category totals
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, False)
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0.0

            'Create dictionary values for each currency and initialise to zero.
            Dim dicHeaderTenders As New Dictionary(Of String, Decimal)
            Dim dicHeaderPickups As New Dictionary(Of String, Decimal)
            For Each currency As BOSystemCurrency.cSystemCurrency In _SysCurrency.Currencies
                dicHeaderTenders.Add(currency.ID.Value, 0)
                dicHeaderPickups.Add(currency.ID.Value, 0)
            Next


            'Get miscellaneous income/outgoings and deposits from cashier/till headers.
            Dim headerPickup As Decimal = 0
            Dim headerTender As Decimal = 0
            Dim headerTenderForeign As Decimal = 0
            Dim defaultCurId As String = _SysCurrency.GetDefaultCurrencyID
            Dim cDateVal As String
            Dim periodDate As String = CStr(_Period.Period(CInt(cmbDate.SelectedValue)).StartDate.Value)

            Dim cashiers As BOBanking.cCashBalCashier = Nothing
            Dim tills As BOBanking.cCashBalTill = Nothing

            Select Case _accountType
                Case AccountabilityTypes.Cashier
                    cashiers = New BOBanking.cCashBalCashier(_Oasys3DB)
                    cashiers.LoadCashiers(CInt(cmbDate.SelectedValue))

                    For Each cashier As BOBanking.cCashBalCashier In cashiers.Cashiers

                        'Get tenders and pick ups from cashier tender types collection
                        For Each tender As BOBanking.cCashBalCashierTen In cashier.TenderTypes
                            dicHeaderPickups.Item(tender.CurrencyID.Value) += 0
                            dicHeaderTenders.Item(tender.CurrencyID.Value) += 0

                            'Get total values converting into local currency if necessary
                            If tender.CurrencyID.Value = defaultCurId Then
                                headerPickup += 0
                                headerTender += 0
                            Else
                                headerPickup += CDec(tender.PickUp.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                                headerTender += CDec(tender.Amount.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                                headerTenderForeign += CDec(tender.Amount.Value * cashier.ExchangeRate.Value * Math.Pow(10, cashier.ExchangePower.Value))
                            End If
                        Next
                    Next

                Case AccountabilityTypes.Till
                    tills = New BOBanking.cCashBalTill(_Oasys3DB)
                    tills.LoadTills(CInt(cmbDate.SelectedValue))

                    'Get tenders and pick ups from till tender types collection
                    For Each till As BOBanking.cCashBalTill In tills.Tills
                        For Each tender As BOBanking.cCashBalTillTen In till.TenderTypes
                            dicHeaderPickups.Item(tender.CurrencyID.Value) += 0
                            dicHeaderTenders.Item(tender.CurrencyID.Value) += 0
                            'Get total values converting into local currency if necessary
                            If tender.CurrencyID.Value = defaultCurId Then
                                headerPickup += tender.PickUp.Value
                                headerTender += tender.Amount.Value
                            Else
                                headerPickup += CDec(tender.PickUp.Value * till.ExchangeRate.Value * Math.Pow(10, till.ExchangePower.Value))
                                headerTender += CDec(tender.Amount.Value * till.ExchangeRate.Value * Math.Pow(10, till.ExchangePower.Value))
                            End If
                        Next
                    Next

                Case Else
                    DisplayWarning(My.Resources.WarnNoAccountabiliry)
                    Exit Sub
            End Select


            'Add category totals
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, False)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Total Sales"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0.0



            cDateVal = periodDate.ToString().Substring(6, 4) + "-"
            cDateVal += periodDate.ToString().Substring(3, 2) + "-"
            cDateVal += periodDate.ToString().Substring(0, 2)


            'Add miscellaneous income
            Dim rowSubs As Integer = sheet.RowCount
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+ Misc Income"
            Select Case _accountType
                Case AccountabilityTypes.Cashier : sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
                Case AccountabilityTypes.Till : sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
            End Select


            'add paid outs
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "- Paid Out"
            Select Case _accountType
                Case AccountabilityTypes.Cashier : sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
                Case AccountabilityTypes.Till : sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
            End Select

            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+/- Deposits"
            Select Case _accountType
                Case AccountabilityTypes.Cashier
                    Dim totVision As New StringBuilder()
                    Dim totVisionTable As DataTable
                    Dim totVal As Decimal = 0
                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = -(totVal)
                Case AccountabilityTypes.Till
                    Dim totVision As New StringBuilder()
                    Dim totVisionTable As DataTable
                    Dim totVal As Decimal = 0

                    totVision.Append("select dp.AMNT")
                    totVision.Append(" from DLTOTS  dt ")
                    totVision.Append(" join DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL ")
                    totVision.Append(" where ((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))")
                    totVision.Append(" and dt.DATE1 = ")
                    totVision.Append("'" & cDateVal & "'")
                    totVisionTable = _Oasys3DB.ExecuteSql(totVision.ToString()).Tables(0)
                    For indexelm As Integer = 0 To totVisionTable.Rows.Count - 1
                        totVal += CDec(totVisionTable.Rows(indexelm)(0))
                    Next
                    sheet.Cells(sheet.RowCount - 1, col.Total).Value = -(totVal)
            End Select

            'add floats
            Dim floats As Decimal = 0
            sheet.Rows.Add(sheet.RowCount, 1)
            Select Case _accountType
                Case AccountabilityTypes.Cashier : floats = cashiers.Cashiers.FloatIssued(defaultCurId)
                Case AccountabilityTypes.Till : floats = tills.Tills.FloatIssued(defaultCurId)
            End Select
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "+ Floats"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = floats

            'add total row
            sheet.AddRows(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Total"
            sheet.Cells(sheet.RowCount - 1, col.Total).Formula = "SUM(R" & rowSubs & "C:R" & sheet.RowCount - 1 & "C)"
            sheet.Cells(sheet.RowCount - 1, col.Total).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            Dim rowTotal As Integer = sheet.RowCount

            'Add tender breakdown headers.
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).CellType = New CellType.TextCellType
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.SetValue(sheet.RowCount - 1, col.Tenders, "Tenders")
            sheet.SetValue(sheet.RowCount - 1, col.Pickups, "Pick ups")
            sheet.SetValue(sheet.RowCount - 1, col.Float, "Float")
            sheet.SetValue(sheet.RowCount - 1, col.Variance, "Variance")


            'Add each currency to sheet for tender breakdown.
            For Each currency As BOSystemCurrency.cSystemCurrency In _SysCurrency.Currencies
                sheet.AddRows(sheet.RowCount, 1)
                sheet.SetValue(sheet.RowCount - 1, col.Category, currency.ID.Value & Space(1) & "(" & currency.Symbol.Value & ")")
                sheet.SetValue(sheet.RowCount - 1, col.Tenders, dicHeaderTenders.Item(currency.ID.Value))
                sheet.SetValue(sheet.RowCount - 1, col.Pickups, dicHeaderPickups.Item(currency.ID.Value))
                sheet.SetValue(sheet.RowCount - 1, col.Float, 0)
                If currency.IsDefault.Value Then
                    sheet.SetValue(sheet.RowCount - 1, col.Float, floats)
                End If
                sheet.SetFormula(sheet.RowCount - 1, col.Variance, "RC[-2]-RC[-3]-RC[-1]")
                sheet.Cells(sheet.RowCount - 1, col.Category).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            Next


            'Add tender breakdown totals.
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.Category).Value = "Total (" & _SysCurrency.Currency(defaultCurId).Symbol.Value & ")"
            sheet.Cells(sheet.RowCount - 1, col.Category).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Tenders).Value = headerTender
            sheet.Cells(sheet.RowCount - 1, col.Pickups).Value = headerPickup
            sheet.Cells(sheet.RowCount - 1, col.Float).Value = floats
            sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-3]-RC[-1]"
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Pickups"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = headerPickup
            sheet.Cells(sheet.RowCount - 1, col.Category, sheet.RowCount - 1, col.Variance).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)

            'add grand total row
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Variance"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0.0
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, col.TotalText).Value = "Currency Variance"
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0.0

            spdReport.Visible = True
            If sheet.RowCount > 0 Then
                sheet.ColumnHeaderVisible = True
                spd_Resize(spdReport, New EventArgs)
                btnPrint.Visible = True
            Else
                btnPrint.Visible = False
            End If
        Else
            MessageBox.Show(My.Resources.NoSales)
            Exit Sub
        End If
    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdReport.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub



    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click

        Try
            Cursor = Cursors.WaitCursor
            Dim periodId As Integer = CInt(cmbDate.SelectedValue)
            Dim periodDate As Date = _Period.Period(periodId).StartDate.Value
            Dim visionQuery As StringBuilder = New StringBuilder()
            Dim cDateVal As String



            cDateVal = periodDate.ToString().Substring(6, 4) + "-"
            cDateVal += periodDate.ToString().Substring(3, 2) + "-"
            cDateVal += periodDate.ToString().Substring(0, 2)

            DisplayProgress(0, My.Resources.GetSales)
            _SaleHeader = New BOSales.cSalesHeader(_Oasys3DB)
            _SaleHeader.LoadTransactionsForFeedPeriod(periodId)

            _VisionHeader = New BOVisionSales.cPVTotals(_Oasys3DB)
            _VisionHeader.LoadValidTransactions(periodDate, True)
            visionQuery.Append("select COUNT(*) from DLTOTS  as SDT")
            visionQuery.Append(" join DLPAID as SDP on SDT.[TRAN] = SDP.[TRAN] and sdt.TILL = SDP.TILL ")
            visionQuery.Append(" and sdt.DATE1 = sdp.DATE1")
            visionQuery.Append(" where (sdt.TCOD = 'M+' OR  sdt.TCOD = 'M-')  and sdt.DOCN")
            visionQuery.Append(" is not null and sdt.MISC = 20  ")
            visionQuery.Append(" and sdt.DATE1 = ")
            visionQuery.Append("'" & cDateVal & "'")
            visionData = _Oasys3DB.ExecuteSql(visionQuery.ToString()).Tables(0)

            If _SaleHeader.Headers.Count = 0 Then

                If (CInt(visionData.Rows(0).Item(0)) > 0) Then
                    spdReport_AddRowsVision()
                Else
                    MessageBox.Show(My.Resources.NoSales)
                End If
            Else
                DisplayProgress(70, My.Resources.InitSheetAddRows)
                spdReport_AddRows()
            End If

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim sheet As SheetView = spdReport.ActiveSheet
        sheet.PrintInfo.Header = "/lStore: " & _storeIdName
        sheet.PrintInfo.Header &= "/c/fz""14""" & My.Resources.ReportTitle
        sheet.PrintInfo.Header &= "/n/c/fz""10""" & My.Resources.ReportDates & _Period.Period(CInt(cmbDate.SelectedValue)).StartDate.Value
        sheet.PrintInfo.Header &= "/n "
        spdReport.PrintSheet(-1)

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub


End Class