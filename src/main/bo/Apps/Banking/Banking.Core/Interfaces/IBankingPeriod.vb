﻿Public Interface IBankingPeriod

    ReadOnly Property PeriodDate() As Date
    ReadOnly Property PeriodId() As Integer
    ReadOnly Property IsClosed() As Boolean
    ReadOnly Property Bags() As List(Of BOBanking.cSafeBags)

    Property Safe() As BOBanking.cSafe
    Property SafeMain(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
    Property SafeChange(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
    Property SafeSystem(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal

    Sub LoadSafe(ByVal periodId As Integer)
    Sub LoadSafe(ByVal periodId As Integer, ByVal userId As Integer)
    Sub LoadSafe(ByVal PeriodId As Integer, ByVal UserId As Integer, ByVal PeriodDate As Date)

    Sub SetSafeUser1(ByVal id As Integer)
    Sub SetSafeUser2(ByVal id As Integer)

    Function GetSafeDatatable() As DataTable
    Function GetBagsCount(ByVal type As String) As Integer
    Function IsTenderUsed(ByVal tenderId As Integer) As Boolean
    Function AllPickupsToSafe(ByVal PickupPeriodId As Integer) As Boolean
    Function BankingOkToComm(ByVal periodId As Integer, ByRef message As String) As Boolean

    Sub UpdateSafe()
    Sub NewBankingSafeMaintenance(ByVal intBankingPeriodID As Integer)
    Sub NewBankingSafeMaintenanceTransactionSafe(ByVal BankingPeriodID As Integer)


End Interface
