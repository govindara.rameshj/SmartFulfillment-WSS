﻿Public Interface IFloatBanking

    Function OutPeriodID(ByVal CurrentPeriodID As Integer, ByVal BankingPeriodID As Integer) As Integer

    Function OutDate(ByVal BankingPeriodID As Integer) As System.Nullable(Of Date)

End Interface