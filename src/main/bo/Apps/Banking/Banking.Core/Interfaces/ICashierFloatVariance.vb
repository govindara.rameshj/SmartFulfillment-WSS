﻿'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Partha
' Date     : 01/06/2011
' Referral : 811
' Notes    : Calculate cashier's float variance(difference between starting float and new float)
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Interface ICashierFloatVariance

    Sub UpdateFloatVariance(ByRef DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal PeriodID As Integer, ByVal CashierId As Integer, _
                            ByVal CurrencyID As String, ByVal FloatVariance As Decimal)

End Interface
