Public Interface IPickup

    ReadOnly Property RelatedBag() As cSafeBags
    ReadOnly Property CashierTills() As List(Of CashierTill)
    ReadOnly Property PreviousPickups() As List(Of cSafeBags)
    ReadOnly Property AccountId() As Integer
    ReadOnly Property AccountType() As String
    ReadOnly Property StoreIdName() As String
    Property State() As Pickup.States
    ReadOnly Property OldPickupPeriodId() As Integer
    ReadOnly Property OldComments() As String
    ReadOnly Property OldSealNumber() As String
    ReadOnly Property OldDenomValue(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
    Function FloatsIssued(ByVal currencyID As String) As Decimal
    Function CashTendered(ByVal currencyID As String) As Decimal
    Function NonCashTendered(ByVal currencyID As String, ByVal tenderId As Integer) As Decimal
    Sub LoadSystemFigures(ByVal periodId As Integer, ByVal accountId As Integer)
    Sub LoadOriginalBag(ByVal id As Integer)
    Function BagOkToOpen() As Boolean
    Sub SetFloatReturned(ByVal amount As Decimal)
    Sub SetDenomination(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer, ByVal amount As Decimal)
    Sub SetComments(ByVal comments As String)
    Sub SetSealNumber(ByVal sealNumber As String)
    Sub SetBagState(ByVal bagState As String)
    Sub SetRelatedBagId(ByVal id As Integer)
    Function GetAccountType() As String
    Sub Save()
    Sub Save(ByVal decNewFloatValue As Decimal)
    Sub AddPickupsToCashierTills(ByRef cashierTills As List(Of CashierTill))
    Sub MinusPickupsFromCashierTills(ByRef cashierTills As List(Of CashierTill))

    ''' <summary>
    ''' New Banking - Updated to write to the new "CashDrop" field
    ''' </summary>
    ''' <history></history>
    ''' <remarks></remarks>
    Sub NewBankingSaveCashDrop()

    ''' <summary>
    ''' New Banking - Banking - Pickup Recheck, return new PickupID
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Function NewBankingPickupReCheckSave() As Integer

End Interface