﻿Imports BOBanking

Public Class CashierTill
    Private _periodId As Integer
    Private _currencyId As String
    Private _Id As Integer
    Private _name As String = String.Empty
    Private _floatIssued As Decimal
    Private _floatReturned As Decimal
    Private _tenders As New List(Of CashierTillTender)
    Private _variances As New List(Of CashierTillVariance)

    Public ReadOnly Property PeriodId() As Integer
        Get
            Return _periodId
        End Get
    End Property
    Public ReadOnly Property CurrencyId() As String
        Get
            Return _currencyId
        End Get
    End Property
    Public ReadOnly Property Id() As Integer
        Get
            Return _Id
        End Get
    End Property
    Public ReadOnly Property Tenders() As List(Of CashierTillTender)
        Get
            Return _tenders
        End Get
    End Property
    Public ReadOnly Property Variances() As List(Of CashierTillVariance)
        Get
            Return _variances
        End Get
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Public Property FloatIssued() As Decimal
        Get
            Return _floatIssued
        End Get
        Set(ByVal value As Decimal)
            _floatIssued = value
        End Set
    End Property
    Public Property FloatReturned() As Decimal
        Get
            Return _floatReturned
        End Get
        Set(ByVal value As Decimal)
            _floatReturned = value
        End Set
    End Property


    Public Overrides Function ToString() As String
        Return _name
    End Function

    Public Sub New(ByVal cashier As cCashBalCashier)
        _periodId = cashier.PeriodID.Value
        _currencyId = cashier.CurrencyID.Value
        _Id = cashier.CashierID.Value
        _floatIssued = cashier.FloatIssued.Value
        _floatReturned = cashier.FloatReturned.Value
    End Sub

    Public Sub New(ByVal till As cCashBalTill)
        _periodId = till.PeriodID.Value
        _currencyId = till.CurrencyID.Value
        _Id = till.TillID.Value
        _floatIssued = till.FloatIssued.Value
        _floatReturned = till.FloatReturned.Value
    End Sub

    Public Sub New(ByVal periodId As Integer, ByVal currencyId As String, ByVal id As Integer)
        _periodId = periodId
        _currencyId = currencyId
        _Id = id
    End Sub

End Class