﻿Public Class BankingPeriod
    Implements IDisposable
    Implements IBankingPeriod

    Protected _oasys3DB As clsOasys3DB
    Protected _safe As cSafe

    Public Enum Column
        Id
        [Date]
        User1
        User2
        LastAmended
        IsClosed
        Display
    End Enum

#Region "Properties"

    Public Property Safe() As BOBanking.cSafe Implements IBankingPeriod.Safe
        Get
            Return _safe
        End Get
        Set(ByVal value As BOBanking.cSafe)
            _safe = value
        End Set
    End Property

    Public ReadOnly Property PeriodDate() As Date Implements IBankingPeriod.PeriodDate
        Get
            Return _safe.PeriodDate.Value
        End Get
    End Property
    Public ReadOnly Property PeriodId() As Integer Implements IBankingPeriod.PeriodId
        Get
            Return _safe.PeriodID.Value
        End Get
    End Property
    Public ReadOnly Property IsClosed() As Boolean Implements IBankingPeriod.IsClosed
        Get
            Return _safe.IsClosed.Value
        End Get
    End Property
    Public ReadOnly Property Bags() As List(Of BOBanking.cSafeBags) Implements IBankingPeriod.Bags
        Get
            Return _safe.Bags
        End Get
    End Property

    Public Property SafeMain(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal Implements IBankingPeriod.SafeMain
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).SafeValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).SafeValue.Value = value
        End Set
    End Property
    Public Property SafeChange(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal Implements IBankingPeriod.SafeChange
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).ChangeValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).ChangeValue.Value = value
        End Set
    End Property
    Public Property SafeSystem(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal Implements IBankingPeriod.SafeSystem
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).SystemValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).SystemValue.Value = value
        End Set
    End Property

#End Region

    Public Sub New()

        _oasys3DB = New clsOasys3DB(My.Resources.DsnName, 0)
        _safe = New BOBanking.cSafe(_oasys3DB)

    End Sub

    Public Sub New(ByRef OdbcConnection As clsOasys3DB)


        If OdbcConnection Is Nothing Then

            _oasys3DB = New clsOasys3DB(My.Resources.DsnName, 0)

        Else

            _oasys3DB = OdbcConnection

        End If

        _safe = New BOBanking.cSafe(OdbcConnection)

    End Sub

    Public Sub LoadSafe(ByVal periodId As Integer) Implements IBankingPeriod.LoadSafe
        _safe = New BOBanking.cSafe(_oasys3DB)
        _safe.Load(periodId)
    End Sub

    Public Sub LoadSafe(ByVal periodId As Integer, ByVal userId As Integer) Implements IBankingPeriod.LoadSafe
        _safe = New BOBanking.cSafe(_oasys3DB)
        _safe.Load(periodId, userId)
    End Sub

    Public Sub LoadSafe(ByVal PeriodId As Integer, ByVal UserId As Integer, ByVal PeriodDate As Date) Implements IBankingPeriod.LoadSafe

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author   : Partha
        ' Date     : 020/06/2011
        ' Referral : 828
        ' Notes    : Load safe record
        '            The BOBanking->cSafe->Load() function does not create the safe correctly in certain scenerios
        '            A new overloaded version that also accepts a date parameter does
        '            This version calls the new version
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        _safe = New BOBanking.cSafe(_oasys3DB)
        _safe.Load(PeriodId, UserId, PeriodDate)

    End Sub

    Public Sub SetSafeUser1(ByVal id As Integer) Implements IBankingPeriod.SetSafeUser1
        _safe.UserID1.Value = id
    End Sub

    Public Sub SetSafeUser2(ByVal id As Integer) Implements IBankingPeriod.SetSafeUser2
        _safe.UserID2.Value = id
    End Sub

    Public Function GetSafeDatatable() As DataTable Implements IBankingPeriod.GetSafeDatatable

        Dim dt As New DataTable
        dt.Columns.Add(Column.Id.ToString, GetType(Integer))
        dt.Columns.Add(Column.Date.ToString, GetType(Date))
        dt.Columns.Add(Column.User1.ToString, GetType(Integer))
        dt.Columns.Add(Column.User2.ToString, GetType(Integer))
        dt.Columns.Add(Column.LastAmended.ToString, GetType(Date))
        dt.Columns.Add(Column.IsClosed.ToString, GetType(Boolean))
        dt.Columns.Add(Column.Display.ToString, GetType(String))
        dt.PrimaryKey = New DataColumn() {dt.Columns(Column.Id)}

        Using newSafe As New BOBanking.cSafe(_oasys3DB)
            Dim safes As List(Of cSafe) = newSafe.GetAllRecords

            For Each s As BOBanking.cSafe In safes
                Dim dr As DataRow = dt.NewRow
                dr(Column.Id) = s.PeriodID.Value
                dr(Column.Date) = s.PeriodDate.Value
                dr(Column.User1) = s.UserID1.Value
                dr(Column.User2) = s.UserID2.Value
                dr(Column.LastAmended) = s.LastAmended.Value
                dr(Column.IsClosed) = s.IsClosed.Value

                Dim sb As New StringBuilder
                sb.Append(s.PeriodID.Value & Space(1) & s.PeriodDate.Value.ToShortDateString)
                If s.IsClosed.Value Then sb.Append(Space(2) & "Closed")

                dr(Column.Display) = sb.ToString
                dt.Rows.Add(dr)
            Next
        End Using

        Return dt

    End Function

    Public Function GetBagsCount(ByVal type As String) As Integer Implements IBankingPeriod.GetBagsCount

        Dim total As Integer = 0
        For Each b As cSafeBags In _safe.Bags
            If b.Type.Value.Equals(type) Then total += 1
        Next
        Return total

    End Function


    Public Function IsTenderUsed(ByVal tenderId As Integer) As Boolean Implements IBankingPeriod.IsTenderUsed

        For Each denom As cSafeDenoms In _safe.Denoms
            If denom.TenderID.Value = tenderId AndAlso denom.SystemValue.Value <> 0 Then
                Return True
            End If
        Next
        Return False

    End Function

    Public Function AllPickupsToSafe(ByVal PickupPeriodId As Integer) As Boolean Implements IBankingPeriod.AllPickupsToSafe

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 07/10/2010
        ' Referral No : 388
        ' Notes       : In certain situations where multiple days worth of banking needs to be done, the system prevents banking with the
        '               message - "Banking cannot be done whilst there are open pickup bags for the day being banked"
        '
        '               After investigation this situation occurs when an open pickup bag exists for today
        '               If all open pickup bags exist for previous days then the banking process can be done
        '
        '               The PickupPeriodId being passed in is the system date, the correct values should be the "correct" banking day
        '
        '               This function is called by the "return pickup bags to the safe" & "prepare for comms" processess               
        '
        '               I have come to the conclusion that I have no idea how the safebags are being worked out,
        '               why it is being worked out multiple times in mutliple places using different filter conditions, really don't!!!
        '
        '               I have decided that I will never work this out, the previous programmer clearly works on a higher plane than me
        '
        '               I have decided to go get the safe bags data myself using the follow alogrithm
        '
        '               For the "correct" banking date there should not be any open pickup bags
        '               Go to the SafeBags table, check that no records exist for the following filter condition
        '               PickupPeriodID = "correct" banking date
        '               Type           = P
        '               State          = S
        '
        '               I am labouring the point, but for such a simple process (look above) I do not know why the code is so complex!!!
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim intCorrectBankingPeriod As Integer = 0
        Dim paramCorrectBankingPeriod As System.Data.SqlClient.SqlParameter
        Dim paramDoesOpenPickupBagsExist As System.Data.SqlClient.SqlParameter

        Using con As New Cts.Oasys.Data.Connection
            'get "correct" banking period
            Using com As Cts.Oasys.Data.Command = con.NewCommand(My.Resources.Procedures.CorrectBankingDate)
                paramCorrectBankingPeriod = com.AddOutputParameter(My.Resources.Parameters.PeriodId, SqlDbType.Int, ParameterDirection.Output)
                com.ExecuteNonQuery()
                intCorrectBankingPeriod = CType(paramCorrectBankingPeriod.Value, Integer)
            End Using
            'do open pickup bags exist
            Using com As Cts.Oasys.Data.Command = con.NewCommand(My.Resources.Procedures.DoesOpenPickupBagsExist)
                com.AddParameter(My.Resources.Parameters.BankingPeriodId, intCorrectBankingPeriod, SqlDbType.Int)
                paramDoesOpenPickupBagsExist = com.AddOutputParameter(My.Resources.Parameters.Result, SqlDbType.Bit, ParameterDirection.Output)
                com.ExecuteNonQuery()
            End Using
        End Using
        Return Not CType(paramDoesOpenPickupBagsExist.Value, Boolean)

        'Old code
        'For Each bag As BOBanking.cSafeBags In _safe.Bags.Where(Function(b As BOBanking.cSafeBags) b.Type.Value = Banking.Core.BagTypes.Pickup)
        '    If bag.PickupPeriodID.Value <> _safe.PeriodID.Value Then Continue For
        '    If bag.State.Value = Banking.Core.BagStates.Sealed Then
        '        If bag.PickupPeriodID.Value = PickupPeriodId Then
        '            Return False
        '        End If
        '    End If
        'Next
        'Return True

    End Function

    Public Function BankingOkToComm(ByVal periodId As Integer, ByRef message As String) As Boolean Implements IBankingPeriod.BankingOkToComm

        'get all banking bags for this period and check all tenders banked
        Using bankingSafe As New BOBanking.cSafe(_oasys3DB)
            bankingSafe.Load(periodId)
            Dim sb As New StringBuilder()
            Dim cashOk As Boolean
            Dim chequeOk As Boolean
            Dim othersOk As Boolean
            Dim allOk As Boolean = True

            'loop over bags and add to string builder
            For Each bag As BOBanking.cSafeBags In bankingSafe.BagsUsingPickupPeriod.Where(Function(b As BOBanking.cSafeBags) b.Type.Value = Banking.Core.BagTypes.Banking)
                If bag.PickupPeriodID.Value <> periodId Then Continue For
                If bag.State.Value = BagStates.BackToSafe Then Continue For

                sb.Append("Seal " & bag.SealNumber.Value & Space(4))

                For Each denom As BOBanking.cSafeBagsDenoms In bag.Denoms
                    Select Case denom.TenderID.Value
                        Case 1
                            cashOk = True
                            sb.Append("Cash  " & vbTab)
                        Case 2
                            chequeOk = True
                            sb.Append("Cheque" & vbTab)
                        Case Else
                            othersOk = True
                            sb.Append("Others" & vbTab)
                    End Select
                    Exit For
                Next

                If (bag.State.Value = Core.BagStates.ManagerChecked) OrElse (bag.State.Value = Core.BagStates.ReleasedNotPrepared) Then
                    sb.Append("Confirmed by " & User.GetUserIdName(bag.InUserID2.Value, IncludeExternalIds:=True, OnlyBankingIds:=True))
                Else
                    allOk = False
                    sb.Append("Not Confirmed")
                End If

                sb.Append(Environment.NewLine)
            Next

            'check if all tenders entered
            If Not cashOk Then
                sb.Append("No cash banking bags have been entered" & Environment.NewLine)
                allOk = False
            End If

            If IsTenderUsed(2) AndAlso Not chequeOk Then
                sb.Append("No cheque banking bags have been entered" & Environment.NewLine)
                allOk = False
            End If

            If Not othersOk Then
                sb.Append("No other tender banking bags have been entered" & Environment.NewLine)
                allOk = False
            End If

            message = sb.ToString
            Return allOk
        End Using

        'Dim todayPeriodId As Integer = _safe.PeriodID.Value
        'Dim sb As New StringBuilder()
        'Dim cashOk As Boolean
        'Dim chequeOk As Boolean
        'Dim othersOk As Boolean
        'Dim allOk As Boolean = True

        ''loop over bags and add to string builder
        'For Each bag As BOBanking.cSafeBags In _safe.BagsUsingPickupPeriod.Where(Function(b As BOBanking.cSafeBags) b.Type.Value = Banking.Core.BagTypes.Banking)
        '    If bag.PickupPeriodID.Value <> todayPeriodId Then Continue For
        '    If bag.State.Value = BagStates.BackToSafe Then Continue For

        '    sb.Append("Seal " & bag.SealNumber.Value & Space(4))

        '    For Each denom As BOBanking.cSafeBagsDenoms In bag.Denoms
        '        Select Case denom.TenderID.Value
        '            Case 1
        '                cashOk = True
        '                sb.Append("Cash  " & vbTab)
        '            Case 2
        '                chequeOk = True
        '                sb.Append("Cheque" & vbTab)
        '            Case Else
        '                othersOk = True
        '                sb.Append("Others" & vbTab)
        '        End Select
        '        Exit For
        '    Next

        '    If (bag.State.Value = Core.BagStates.ManagerChecked) OrElse (bag.State.Value = Core.BagStates.ReleasedNotPrepared) Then
        '        sb.Append("Confirmed by " & User.GetUserIdName(bag.InUserID2.Value))
        '    Else
        '        allOk = False
        '        sb.Append("Not Confirmed")
        '    End If

        '    sb.Append(Environment.NewLine)
        'Next

        ''check if all tenders entered
        'If Not cashOk Then
        '    sb.Append("No cash banking bags have been entered" & Environment.NewLine)
        '    allOk = False
        'End If

        'If IsTenderUsed(2) AndAlso Not chequeOk Then
        '    sb.Append("No cheque banking bags have been entered" & Environment.NewLine)
        '    allOk = False
        'End If

        'If Not othersOk Then
        '    sb.Append("No other tender banking bags have been entered" & Environment.NewLine)
        '    allOk = False
        'End If

        'message = sb.ToString
        'Return allOk

    End Function



    Public Sub UpdateSafe() Implements IBankingPeriod.UpdateSafe
        _safe.Update()
    End Sub

    Public Sub NewBankingSafeMaintenance(ByVal intBankingPeriodID As Integer) Implements IBankingPeriod.NewBankingSafeMaintenance
        _safe.NewBankingSafeMaintenance(intBankingPeriodID)
    End Sub

    Public Overridable Sub NewBankingSafeMaintenanceTransactionSafe(ByVal BankingPeriodID As Integer) Implements IBankingPeriod.NewBankingSafeMaintenanceTransactionSafe

        Me.NewBankingSafeMaintenance(BankingPeriodID)

    End Sub


#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _safe.Dispose()
                _oasys3DB.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub


    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class