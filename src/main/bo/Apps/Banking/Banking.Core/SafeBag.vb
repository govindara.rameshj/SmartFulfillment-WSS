﻿Public Class SafeBag
    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
    Private _safebag As New BOBanking.cSafeBags(_oasys3Db)

    Friend Property Denoms() As List(Of BOBanking.cSafeBagsDenoms)
        Get
            Return _safebag.Denoms
        End Get
        Set(ByVal value As List(Of BOBanking.cSafeBagsDenoms))
            _safebag.Denoms = value
        End Set
    End Property

    Public ReadOnly Property Id() As Integer
        Get
            Return _safebag.ID.Value
        End Get
    End Property
    Public Property InPeriodId() As Integer
        Get
            Return _safebag.InPeriodID.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.InPeriodID.Value = value
        End Set
    End Property
    Public Property InDate() As Date
        Get
            Return _safebag.InDate.Value
        End Get
        Set(ByVal value As Date)
            _safebag.InDate.Value = value
        End Set
    End Property
    Public Property InUser1() As Integer
        Get
            Return _safebag.InUserID1.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.InUserID1.Value = value
        End Set
    End Property
    Public Property InUser2() As Integer
        Get
            Return _safebag.InUserID2.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.InUserID2.Value = value
        End Set
    End Property

    Public Property OutPeriodId() As Integer
        Get
            Return _safebag.OutPeriodID.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.OutPeriodID.Value = value
        End Set
    End Property
    Public Property OutDate() As Date
        Get
            Return _safebag.OutDate.Value
        End Get
        Set(ByVal value As Date)
            _safebag.OutDate.Value = value
        End Set
    End Property
    Public Property OutUser1() As Integer
        Get
            Return _safebag.OutUserID1.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.OutUserID1.Value = value
        End Set
    End Property
    Public Property OutUser2() As Integer
        Get
            Return _safebag.OutUserID2.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.OutUserID2.Value = value
        End Set
    End Property

    Public Property SealNumber() As String
        Get
            Return _safebag.SealNumber.Value
        End Get
        Set(ByVal value As String)
            _safebag.SealNumber.Value = value
        End Set
    End Property
    Public Property AccountId() As Integer
        Get
            Return _safebag.AccountabilityID.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.AccountabilityID.Value = value
        End Set
    End Property
    Public Property AccountType() As String
        Get
            Return _safebag.AccountabilityType.Value
        End Get
        Set(ByVal value As String)
            _safebag.AccountabilityType.Value = value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _safebag.Comments.Value
        End Get
        Set(ByVal value As String)
            _safebag.Comments.Value = value
        End Set
    End Property
    Public Property PickupPeriodId() As Integer
        Get
            Return _safebag.PickupPeriodID.Value
        End Get
        Set(ByVal value As Integer)
            _safebag.PickupPeriodID.Value = value
        End Set
    End Property
    Public Property Float() As Decimal
        Get
            Return _safebag.FloatValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safebag.FloatValue.Value = value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _safebag.Type.Value
        End Get
        Set(ByVal value As String)
            _safebag.Type.Value = value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _safebag.State.Value
        End Get
        Set(ByVal value As String)
            _safebag.State.Value = value
        End Set
    End Property


    Public Property DenomValue(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _safebag.Denom(currencyId, denominationId, tenderId).Value.Value
        End Get
        Set(ByVal value As Decimal)
            _safebag.Denom(currencyId, denominationId, tenderId).Value.Value = value
        End Set
    End Property


    Public Sub New()
    End Sub
    Public Sub New(ByVal id As Integer)
        _safebag.Load(id)
    End Sub


End Class
