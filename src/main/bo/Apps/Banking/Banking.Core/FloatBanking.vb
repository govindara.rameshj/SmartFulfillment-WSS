﻿Public Class FloatBanking

    Public Enum Types
        Float
        Banking
    End Enum

    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Protected _safe As New cSafe(_oasys3Db)
    Protected _newBag As cSafeBags = Nothing
    Private _originalBag As cSafeBags = Nothing
    Private _defaultCurrencyId As String = String.Empty

    Public Sub New()

    End Sub

    Public ReadOnly Property AccountType() As String
        Get
            Return _newBag.AccountabilityType.Value
        End Get
    End Property
    Public ReadOnly Property Type() As String
        Get
            Return _newBag.Type.Value
        End Get
    End Property

    Public Property Comments() As String
        Get
            If _originalBag IsNot Nothing Then Return _originalBag.Comments.Value
            Return _originalBag.Comments.Value
        End Get
        Set(ByVal value As String)
            _newBag.Comments.Value = value
            If _originalBag IsNot Nothing Then _originalBag.Comments.Value = value
        End Set
    End Property
    Public ReadOnly Property OldSealNumber() As String
        Get
            If _originalBag IsNot Nothing Then Return _originalBag.SealNumber.Value
            Return String.Empty
        End Get
    End Property

    Public ReadOnly Property LastAmended() As Date
        Get
            Return _safe.LastAmended.Value
        End Get
    End Property
    Public ReadOnly Property OldValue(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _originalBag.Denom(currencyId, denominationId, tenderId).Value.Value
        End Get
    End Property
    Public ReadOnly Property OldSlipNumber(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As String
        Get
            Return _originalBag.Denom(currencyId, denominationId, tenderId).SlipNumber.Value
        End Get
    End Property
    Public ReadOnly Property OldPickupPeriodId() As Integer
        Get
            Return _originalBag.PickupPeriodID.Value
        End Get
    End Property

    Public Property SafeMain(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).SafeValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).SafeValue.Value = value
        End Set
    End Property
    Public Property SafeChange(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).ChangeValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).ChangeValue.Value = value
        End Set
    End Property
    Public Property SafeSystem(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).SystemValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).SystemValue.Value = value
        End Set
    End Property
    Public Property SafeSuggested(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _safe.Denom(currencyId, denominationId, tenderId).SuggestedValue.Value
        End Get
        Set(ByVal value As Decimal)
            _safe.Denom(currencyId, denominationId, tenderId).SuggestedValue.Value = value
        End Set
    End Property

    Public Property NewValue(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal
        Get
            Return _newBag.Denom(currencyId, denominationId, tenderId).Value.Value
        End Get
        Set(ByVal value As Decimal)
            _newBag.Denom(currencyId, denominationId, tenderId).Value.Value = value
        End Set
    End Property
    Public Property NewSlipNumber(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As String
        Get
            Return _newBag.Denom(currencyId, denominationId, tenderId).SlipNumber.Value
        End Get
        Set(ByVal value As String)
            _newBag.Denom(currencyId, denominationId, tenderId).SlipNumber.Value = value
        End Set
    End Property
    Public ReadOnly Property NewId() As Integer
        Get
            Return _newBag.ID.Value
        End Get
    End Property
    Public ReadOnly Property NewSealNumber() As String
        Get
            Return _newBag.SealNumber.Value
        End Get
    End Property


    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal type As Types, ByVal accountType As String)

        'instantiate new bag
        _newBag = New cSafeBags(_oasys3Db)
        _newBag.State.Value = BagStates.Sealed
        _newBag.InPeriodID.Value = Period.GetCurrent.Id
        _newBag.InUserID1.Value = user1
        _newBag.InUserID2.Value = user2
        _newBag.InDate.Value = Now
        _newBag.PickupPeriodID.Value = _newBag.InPeriodID.Value
        _newBag.AccountabilityType.Value = accountType

        Select Case type
            Case Types.Banking : _newBag.Type.Value = BagTypes.Banking
            Case Types.Float : _newBag.Type.Value = BagTypes.Float
        End Select

        'load current safe records
        _safe.Load(_newBag.InPeriodID.Value)

        'get default currency id
        _defaultCurrencyId = Currency.GetDefaultCurrencyId

    End Sub

    Public Sub New(ByVal intPickupPeriodID As Integer, ByVal intTodayPeriodID As Integer, ByVal intUserID1 As Integer, ByVal intUserID2 As Integer, ByVal enumType As Types, ByVal strAccountType As String)
        _newBag = New cSafeBags(_oasys3Db)

        _newBag.State.Value = BagStates.Sealed

        'new float from safe processing
        'ideally the InPeriodID represents when the float was created, along with the InDate
        'but if this is set to today then the float will be taken from today's safe rather than the PickupPeriodID safe
        _newBag.InDate.Value = Now
        '_newBag.InPeriodID.Value = Period.GetCurrent.Id
        _newBag.InPeriodID.Value = intTodayPeriodID
        _newBag.PickupPeriodID.Value = intPickupPeriodID

        _newBag.InUserID1.Value = intUserID1
        _newBag.InUserID2.Value = intUserID2

        _newBag.AccountabilityType.Value = strAccountType

        Select Case enumType
            Case Types.Banking : _newBag.Type.Value = BagTypes.Banking
            Case Types.Float : _newBag.Type.Value = BagTypes.Float
        End Select
        LoadSafe()

        _defaultCurrencyId = Currency.GetDefaultCurrencyId
    End Sub

    Public Sub New(ByRef Connection As clsOasys3DB, ByVal PickupPeriodID As Integer, _
                   ByVal TodayPeriodID As Integer, ByVal UserID1 As Integer, _
                   ByVal UserID2 As Integer, ByVal Type As Types, ByVal AccountType As String)

        _oasys3Db = Connection
        _newBag = New cSafeBags(_oasys3Db)
        With _newBag
            .State.Value = BagStates.Sealed
            'new float from safe processing
            'ideally the InPeriodID represents when the float was created, along with the InDate
            'but if this is set to today then the float will be taken from today's safe rather than the PickupPeriodID safe
            .InDate.Value = Now
            '_newBag.InPeriodID.Value = Period.GetCurrent.Id
            .InPeriodID.Value = TodayPeriodID
            .PickupPeriodID.Value = PickupPeriodID
            .InUserID1.Value = UserID1
            .InUserID2.Value = UserID2
            .AccountabilityType.Value = AccountType
            Select Case Type
                Case Types.Banking
                    _newBag.Type.Value = BagTypes.Banking
                Case Types.Float
                    _newBag.Type.Value = BagTypes.Float
            End Select
        End With
        LoadSafe()

        _defaultCurrencyId = Currency.GetDefaultCurrencyId
    End Sub

    Protected Overridable Sub LoadSafe()

        _safe.Load(_newBag.InPeriodID.Value)

    End Sub

    Public Sub New(ByVal intPickupPeriodID As Integer, ByVal intTodayPeriodID As Integer, ByVal intUserID1 As Integer, ByVal intUserID2 As Integer, ByVal enumType As Types, ByVal strAccountType As String, _
                   ByVal intPickupBagID As Integer)
        'instantiate new bag
        _newBag = New cSafeBags(_oasys3Db)

        _newBag.State.Value = BagStates.Sealed


        'new float from safe processing
        'ideally the InPeriodID represents when the float was created, along with the InDate
        'but if this is set to today then the float will be taken from today's safe rather than the PickupPeriodID safe
        _newBag.InDate.Value = Now
        '_newBag.InPeriodID.Value = Period.GetCurrent.Id     
        _newBag.InPeriodID.Value = intTodayPeriodID
        _newBag.PickupPeriodID.Value = intPickupPeriodID

        _newBag.InUserID1.Value = intUserID1
        _newBag.InUserID2.Value = intUserID2

        _newBag.AccountabilityType.Value = strAccountType

        Select Case enumType
            Case Types.Banking : _newBag.Type.Value = BagTypes.Banking
            Case Types.Float : _newBag.Type.Value = BagTypes.Float
        End Select
        'load current safe records
        _safe.Load(_newBag.InPeriodID.Value)
        'get default currency id
        _defaultCurrencyId = Currency.GetDefaultCurrencyId

        _newBag.RelatedBagId.Value = intPickupBagID
    End Sub

    Public Sub New(ByRef Connection As clsOasys3DB, ByVal PickupPeriodID As Integer, _
                   ByVal TodayPeriodID As Integer, ByVal UserID1 As Integer, _
                   ByVal UserID2 As Integer, ByVal Type As Types, _
                   ByVal AccountType As String, ByVal PickupBagID As Integer)

        _oasys3Db = Connection
        _newBag = New cSafeBags(_oasys3Db)
        With _newBag
            .State.Value = BagStates.Sealed
            'new float from safe processing
            'ideally the InPeriodID represents when the float was created, along with the InDate
            'but if this is set to today then the float will be taken from today's safe rather than the PickupPeriodID safe
            .InDate.Value = Now
            '.InPeriodID.Value = Period.GetCurrent.Id     
            .InPeriodID.Value = TodayPeriodID
            .PickupPeriodID.Value = PickupPeriodID
            .InUserID1.Value = UserID1
            .InUserID2.Value = UserID2
            .AccountabilityType.Value = AccountType
            Select Case Type
                Case Types.Banking
                    .Type.Value = BagTypes.Banking
                Case Types.Float
                    .Type.Value = BagTypes.Float
            End Select
            'load current safe records
            _safe.Load(_newBag.InPeriodID.Value)
            .RelatedBagId.Value = PickupBagID
        End With
        'get default currency id
        _defaultCurrencyId = Currency.GetDefaultCurrencyId
    End Sub

    Public Sub LoadOriginalBag(ByVal id As Integer)

        'load original bag and system figures
        _originalBag = New cSafeBags(_oasys3Db)
        _originalBag.Load(id)

    End Sub


    Public Sub SetDenomination(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer, ByVal amount As Decimal, ByVal slipNumber As String)
        Dim denom As cSafeBagsDenoms = _newBag.Denom(currencyId, denominationId, tenderId)
        denom.Value.Value = amount
        denom.SlipNumber.Value = slipNumber
    End Sub

    Public Sub SetSealNumber(ByVal sealNumber As String)
        _newBag.SealNumber.Value = sealNumber
    End Sub

    Public Sub SetBagState(ByVal bagState As String, Optional ByVal accountId As Integer = 0)
        _newBag.State.Value = bagState
        _newBag.AccountabilityID.Value = accountId
    End Sub

    Public Sub SetAccountId(ByVal accountId As Integer)
        _newBag.AccountabilityID.Value = accountId
    End Sub

    Public Sub SetBankingPeriodId(ByVal id As Integer)
        _newBag.PickupPeriodID.Value = id
    End Sub

    Public Sub Save()

        Try
            'start transaction
            _oasys3Db.BeginTransaction()

            Select Case _newBag.State.Value
                Case BagStates.BackToSafe
                    'returning bag back to safe so check if the old bag has had changes
                    Select Case True
                        Case _originalBag IsNot Nothing AndAlso BagsAreIdentical(_originalBag, _newBag)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.BackToSafe
                            _originalBag.Update()
                            _safe.AddBackToSafe(_originalBag)
                            _newBag = _originalBag

                        Case _originalBag IsNot Nothing
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()
                            _safe.AddBackToSafe(_originalBag)

                            _newBag.SealNumber.Value = _originalBag.SealNumber.Value
                            _newBag.OutDate.Value = Now
                            _newBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _newBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _newBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _newBag.State.Value = BagStates.BackToSafe
                            _newBag.Update()

                        Case Else : Throw New Exception
                    End Select

                Case BagStates.Sealed, BagStates.ManagerChecked
                    'could be a reseal or new float so check if old bag exists
                    Select Case True
                        Case _originalBag Is Nothing
                            _newBag.Update()
                            _safe.RemoveFromSafe(_newBag)

                        Case _originalBag IsNot Nothing
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()

                            _safe.AddBackToSafe(_originalBag)
                            _newBag.Update()
                            _safe.RemoveFromSafe(_newBag)

                        Case Else : Throw New Exception
                    End Select

                Case BagStates.Released
                    'could only come from checking bag so check if any changes made
                    Select Case True
                        Case _originalBag IsNot Nothing AndAlso Not BagsAreIdentical(_originalBag, _newBag)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()
                            _safe.AddBackToSafe(_originalBag)

                            _newBag.SealNumber.Value = _originalBag.SealNumber.Value
                            _newBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _newBag.OutDate.Value = _newBag.InDate.Value
                            _newBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _newBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _newBag.Update()
                            _safe.RemoveFromSafe(_newBag)

                            'assign to cashier/till if a float bag
                            If _originalBag.Type.Value = BagTypes.Float Then
                                AssignFloatToCashierTill(_newBag.AccountabilityType.Value, _newBag.AccountabilityID.Value, _newBag.Value.Value)
                            End If

                        Case _originalBag IsNot Nothing 'and bags are identical
                            _originalBag.AccountabilityID.Value = _newBag.AccountabilityID.Value
                            _originalBag.State.Value = BagStates.Released
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.SaveIfExists()

                            'assign to cashier/till if a float bag
                            If _originalBag.Type.Value = BagTypes.Float Then
                                AssignFloatToCashierTill(_originalBag.AccountabilityType.Value, _originalBag.AccountabilityID.Value, _originalBag.Value.Value)
                            End If

                        Case Else : Throw New Exception
                    End Select
            End Select

            'commit transaction
            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha
    ' Date     : 01/6/2011
    ' Referral : 839
    ' Notes    : Assigning a float to a cashier on a previous selected banking day; writing to CashBalCashier for today
    '
    '            Overloaded Save() function will accept banking period id, this will be different to today's banking period id
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub Save(ByVal BankingPeriodId As Integer)

        Try
            _oasys3Db.BeginTransaction()

            _originalBag.AccountabilityID.Value = _newBag.AccountabilityID.Value
            _originalBag.State.Value = BagStates.Released






            Dim FB As IFloatBanking
            Dim StartDate As Nullable(Of Date)

            FB = (New FloatBankingFactory).GetImplementation
            StartDate = FB.OutDate(BankingPeriodId)


            If StartDate.HasValue Then _originalBag.OutDate.Value = StartDate.Value
            _originalBag.OutPeriodID.Value = FB.OutPeriodID(_newBag.InPeriodID.Value, BankingPeriodId)
            '_originalBag.OutDate.Value = Now
            '_originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value







            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
            _originalBag.SaveIfExists()

            'assign to cashier/till if a float bag
            If _originalBag.Type.Value = BagTypes.Float Then
                AssignFloatToCashierTill(_originalBag.AccountabilityID.Value, BankingPeriodId, _defaultCurrencyId, _originalBag.AccountabilityType.Value, _originalBag.Value.Value)
            End If

            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

    Public Sub SaveFromPickup()

        Try
            'start transaction
            _oasys3Db.BeginTransaction()
            _newBag.Update()

            'commit transaction
            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

    Public Function IsOldBagNothing() As Boolean
        If _originalBag Is Nothing Then Return True
        Return False
    End Function

    Public Function NewTotal(ByVal currencyId As String) As Decimal

        Dim total As Decimal = 0
        For Each denom As cSafeBagsDenoms In _newBag.Denoms
            If denom.CurrencyID.Value = currencyId Then total += denom.Value.Value
        Next
        Return total

    End Function

    Private Sub AssignFloatToCashierTill(ByVal accountType As String, ByVal accountId As Integer, ByVal float As Decimal)

        'update cashier/till header with issued float
        Select Case accountType
            Case AccountabilityTypes.Cashier
                Using cashier As New cCashBalCashier(_oasys3Db)
                    cashier.LoadCashier(accountId, _safe.PeriodID.Value, _defaultCurrencyId)
                    cashier.FloatUpdateIssued(float)
                End Using

            Case AccountabilityTypes.Till
                Using till As New cCashBalTill(_safe.Oasys3DB)
                    till.LoadTill(accountId, _safe.PeriodID.Value, _defaultCurrencyId)
                    till.FloatUpdateIssued(float)
                End Using
        End Select

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha
    ' Date     : 01/6/2011
    ' Referral : 839
    ' Notes    : Assigning a float to a cashier on a previous selected banking day; writing to CashBalCashier for today
    '
    '            Overloaded Save() function will accept banking period id, this will be different to today's banking period id
    '            The Save function will call this overloaded AssignFloatToCashierTill() function and pass the banking period id provided
    '            This function parameter list has been re-ordered
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub AssignFloatToCashierTill(ByVal AccountID As Integer, ByVal PeriodID As Integer, ByVal CurrencyId As String, _
                                         ByVal AccountType As String, ByVal FloatValue As Decimal)

        'update cashier/till header with issued float
        Select Case AccountType
            Case AccountabilityTypes.Cashier
                Using cashier As New cCashBalCashier(_oasys3Db)
                    cashier.LoadCashier(AccountID, PeriodID, CurrencyId)
                    cashier.FloatUpdateIssued(FloatValue)
                End Using

            Case AccountabilityTypes.Till
                Using till As New cCashBalTill(_safe.Oasys3DB)
                    till.LoadTill(AccountID, PeriodID, CurrencyId)
                    till.FloatUpdateIssued(FloatValue)
                End Using
        End Select

    End Sub

    Public Sub AddPickupsToCashierTills(ByRef cashierTills As List(Of CashierTill))

        For Each denom As BOBanking.cSafeBagsDenoms In _newBag.Denoms
            Dim cashTillTender As CashierTillTender = cashierTills.GetTender(_newBag.PickupPeriodID.Value, denom.CurrencyID.Value, _newBag.AccountabilityID.Value, denom.TenderID.Value)
            cashTillTender.PickUp += denom.Value.Value
        Next

    End Sub

    ''' <summary>
    ''' New Banking - Manual check result in a new float created, old one cancelled, safe updated accordinly
    '''               First and second check plus that check has happend stored against both the cancelled and new bag
    ''' </summary>
    ''' <param name="intUSerID1"></param>
    ''' <param name="intUserID2"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function NewBankingManualCheck(ByVal intUSerID1 As Integer, ByVal intUserID2 As Integer) As Integer
        Try
            Dim strSql As String
            Dim intOriginalFloatID As Integer

            intOriginalFloatID = _originalBag.ID.Value

            _oasys3Db.BeginTransaction()

            _originalBag.OutDate.Value = Now
            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
            _originalBag.State.Value = BagStates.Cancelled
            _originalBag.Update()

            'store link to cancelled float
            _newBag.RelatedBagId.Value = intOriginalFloatID

            _safe.AddBackToSafe(_originalBag)
            _newBag.Update()
            _safe.RemoveFromSafe(_newBag)

            'manual check - original float
            strSql = "exec NewBankingFloatChecked @FloatBagID = " & intOriginalFloatID.ToString.Trim & _
                     ", @UserID1 = " & intUSerID1.ToString.Trim & ", @UserID2 = " & intUserID2.ToString.Trim
            _oasys3Db.NewBankingNonQueryExecuteSql(strSql)

            'manual check - new float
            strSql = "exec NewBankingFloatChecked @FloatBagID = " & _newBag.ID.Value.ToString.Trim & _
                     ", @UserID1 = " & intUSerID1.ToString.Trim & ", @UserID2 = " & intUserID2.ToString.Trim
            _oasys3Db.NewBankingNonQueryExecuteSql(strSql)

            _oasys3Db.CommitTransaction()

            'return new float id
            NewBankingManualCheck = _newBag.ID.Value
        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' New Banking - Manual check result in a new float created, old one cancelled, safe updated accordingly
    '''               First and second check plus that check has happened stored against both the cancelled and new bag
    '''               All done with existing business object transaction started and committed elsewhere.
    ''' </summary>
    ''' <param name="UserID1"></param>
    ''' <param name="UserID2"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function NewBankingManualCheckAsPartOfWiderTransaction(ByVal UserID1 As Integer, ByVal UserID2 As Integer) As Integer
        Dim NewBankingFloatCheckedSql As String
        Dim OriginalFloatID As Integer

        OriginalFloatID = _originalBag.ID.Value

        With _originalBag
            .OutDate.Value = Now
            .OutPeriodID.Value = _newBag.InPeriodID.Value
            .OutUserID1.Value = _newBag.InUserID1.Value
            .OutUserID2.Value = _newBag.InUserID2.Value
            .State.Value = BagStates.Cancelled
            .Update()
        End With

        'store link to cancelled float
        With _newBag
            .RelatedBagId.Value = OriginalFloatID
            _safe.AddBackToSafe(_originalBag)
            .Update()
        End With
        _safe.RemoveFromSafe(_newBag)

        'manual check - original float
        NewBankingFloatCheckedSql = _
            "exec NewBankingFloatChecked @FloatBagID = " & OriginalFloatID.ToString.Trim & _
            ", @UserID1 = " & UserID1.ToString.Trim & _
            ", @UserID2 = " & UserID2.ToString.Trim
        _oasys3Db.NewBankingNonQueryExecuteSql(NewBankingFloatCheckedSql)

        'manual check - new float
        NewBankingFloatCheckedSql = _
            "exec NewBankingFloatChecked @FloatBagID = " & _newBag.ID.Value.ToString.Trim & _
            ", @UserID1 = " & UserID1.ToString.Trim & _
            ", @UserID2 = " & UserID2.ToString.Trim
        _oasys3Db.NewBankingNonQueryExecuteSql(NewBankingFloatCheckedSql)

        'return new float id
        NewBankingManualCheckAsPartOfWiderTransaction = _newBag.ID.Value
    End Function

    Public Function NewBankingUnAssignFloatAsPartOfWiderTransaction(ByVal BankingPeriodId As Integer) As Integer

        With _originalBag
            .OutDate.Value = Now
            .OutPeriodID.Value = _newBag.InPeriodID.Value
            .OutUserID1.Value = _newBag.InUserID1.Value
            .OutUserID2.Value = _newBag.InUserID2.Value
            .State.Value = BagStates.Cancelled
            .Update()
        End With
        'store link to cancelled float
        With _newBag
            .RelatedBagId.Value = _originalBag.ID.Value
            _safe.AddBackToSafe(_originalBag)
            .Update()
        End With
        _safe.RemoveFromSafe(_newBag)

        'reduce cashbal(cashier/till): floatissued
        AssignFloatToCashierTill(_originalBag.AccountabilityID.Value, BankingPeriodId, _defaultCurrencyId, _originalBag.AccountabilityType.Value, -_originalBag.Value.Value)
    End Function

    Public Sub NewBankingReAssignFloat(ByVal ExistingAccountabilityID As Integer, ByVal NewAccountabilityID As Integer, _
                                       ByVal BankingPeriodID As Integer, ByVal CurrencyID As String, ByVal strAccountType As String, _
                                       ByVal FloatID As Integer, ByVal FloatValue As Decimal)

        Try
            _oasys3Db.BeginTransaction()

            _oasys3Db.NewBankingNonQueryExecuteSql("exec NewBankingFloatReAssigned @FloatBagID       = " & FloatID.ToString & ", " & _
                                                   "                               @AccountabilityID = " & NewAccountabilityID.ToString)

            AssignFloatToCashierTill(ExistingAccountabilityID, BankingPeriodID, CurrencyID, strAccountType, -FloatValue)
            AssignFloatToCashierTill(NewAccountabilityID, BankingPeriodID, CurrencyID, strAccountType, FloatValue)

            _oasys3Db.CommitTransaction()
        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

End Class

Public Class FloatBankingUsingLatestSafe
    Inherits FloatBanking

    Public Sub New(ByVal intPickupPeriodID As Integer, _
                   ByVal intTodayPeriodID As Integer, _
                   ByVal intUserID1 As Integer, _
                   ByVal intUserID2 As Integer, _
                   ByVal enumType As Types, _
                   ByVal strAccountType As String)

        MyBase.New(intPickupPeriodID, intTodayPeriodID, intUserID1, intUserID2, enumType, strAccountType)

        _newBag.InPeriodID.Value = Period.GetCurrent.Id

    End Sub

    Protected Overrides Sub LoadSafe()

        Dim latestSafePeriodId As Integer = Period.GetLatestSafe()

        _safe.Load(latestSafePeriodId)

    End Sub

End Class
