﻿Imports Cts.Oasys.Core.System

Public Class Authorisation
    Private _authType(15) As Boolean

    Private Enum AuthTypes
        SafeCheck
        FloatIn
        FloatOut
        FloatCheck
        PropertyIn
        PropertyOut
        PropertyCheck
        GiftTokenIn
        GiftTokenOut
        GiftTokenCheck
        BankingIn
        BankingOut
        BankingCheck
        PickupIn
        PickupOut
        PickupCheck
    End Enum

    Public Enum States
        Safe
        Check
        [In]
        Out
    End Enum

    Public Sub New()
        _authType = Parameter.GetAuthorisationParameters
    End Sub

    ''' <summary>
    ''' Check whether authorisation is required
    ''' </summary>
    ''' <param name="state"></param>
    ''' <param name="bagType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AuthorisationRequired(ByVal state As States, ByVal bagType As String) As Boolean

        'check whether authorisation is required
        Dim authRequired As Boolean = False
        Select Case state
            Case States.Safe
                authRequired = _authType(AuthTypes.SafeCheck)

            Case States.Check
                Select Case bagType
                    Case BagTypes.Banking : authRequired = _authType(AuthTypes.BankingCheck)
                    Case BagTypes.Float : authRequired = _authType(AuthTypes.FloatCheck)
                    Case BagTypes.GiftToken : authRequired = _authType(AuthTypes.GiftTokenCheck)
                    Case BagTypes.Pickup : authRequired = _authType(AuthTypes.PickupCheck)
                    Case BagTypes.Property : authRequired = _authType(AuthTypes.PropertyCheck)
                End Select

            Case States.In
                Select Case bagType
                    Case BagTypes.Float : authRequired = _authType(AuthTypes.FloatIn)
                    Case BagTypes.Banking : authRequired = _authType(AuthTypes.BankingIn)
                    Case BagTypes.GiftToken : authRequired = _authType(AuthTypes.GiftTokenIn)
                    Case BagTypes.Property : authRequired = _authType(AuthTypes.PropertyIn)
                    Case BagTypes.Pickup : authRequired = _authType(AuthTypes.PickupIn)
                End Select

            Case States.Out
                Select Case bagType
                    Case BagTypes.Banking : authRequired = _authType(AuthTypes.BankingOut)
                    Case BagTypes.Float : authRequired = _authType(AuthTypes.FloatOut)
                    Case BagTypes.GiftToken : authRequired = _authType(AuthTypes.GiftTokenOut)
                    Case BagTypes.Pickup : authRequired = _authType(AuthTypes.PickupOut)
                    Case BagTypes.Property : authRequired = _authType(AuthTypes.PropertyOut)
                End Select
        End Select

        Return authRequired

    End Function

End Class
