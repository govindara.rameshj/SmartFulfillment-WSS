﻿Public Class FloatBankingRepository
    Implements IFloatBankingRepository

    Protected _Connection As IConnection
    Protected _Command As ICommand
    Protected _DR As DataRow
    Protected _PeriodID As Integer

#Region "Interface"

    Public Function GetSystemPeriod(ByRef Con As Connection, ByVal PeriodID As Integer) As DataRow Implements IFloatBankingRepository.GetSystemPeriod

        Reset()

        SetConnection(Con)

        SetPeriodID(PeriodID)

        ExecuteGetSystemPeriod()

        Return _DR

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DR = Nothing
        _PeriodID = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then

            _Connection = New Connection

        Else

            _Connection = Con

        End If

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value

    End Sub

    Friend Overridable Sub ExecuteGetSystemPeriod()

        ConfigureGetSystemPeriodCommand()
        ExecuteGetSystemPeriodCommand()

    End Sub

    Friend Overridable Sub ConfigureGetSystemPeriodCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = "SystemPeriodGet"

        _Command.AddParameter("@ID", _PeriodID)

    End Sub

    Friend Overridable Sub ExecuteGetSystemPeriodCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteDataRow()

    End Sub

#Region "Command Execution"

    'Friend Overridable Sub ExecuteDataTable()

    '    _DT = _Command.ExecuteDataTable()

    'End Sub

    Friend Overridable Sub ExecuteDataRow()

        Dim DT As DataTable

        DT = _Command.ExecuteDataTable()

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            _DR = DT.Rows.Item(0)

        End If

    End Sub

    'Friend Overridable Sub ExecuteNonQuery()

    '    _Command.ExecuteDataTable()

    'End Sub

#End Region

#Region "Test Conditions"

    Friend Function ConnectionIsNothing() As Boolean

        If _Connection Is Nothing Then Return True

    End Function

    Friend Function CommandIsNothing() As Boolean

        If _Command Is Nothing Then Return True

    End Function

#End Region

#End Region

End Class