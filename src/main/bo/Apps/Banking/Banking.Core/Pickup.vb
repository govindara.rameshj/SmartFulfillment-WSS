﻿Public Class Pickup
    Implements IPickup

    Protected _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Protected _newBag As cSafeBags = Nothing
    Friend _originalBag As cSafeBags = Nothing
    Protected _cashierTills As New List(Of CashierTill)
    Protected _previousPickups As New List(Of cSafeBags)
    Protected _storeIdName As String = String.Empty
    Protected _state As States = States.Pickup
    Protected _defaultCurrencyId As String = String.Empty

    Public Enum States
        Pickup
        PickupFloat
        Check
    End Enum

    Public ReadOnly Property RelatedBag() As cSafeBags Implements IPickup.RelatedBag
        Get
            If _originalBag Is Nothing Then Return Nothing
            Return _originalBag.RelatedBag
        End Get
    End Property
    Public ReadOnly Property CashierTills() As List(Of CashierTill) Implements IPickup.CashierTills
        Get
            Return _cashierTills
        End Get
    End Property
    Public ReadOnly Property PreviousPickups() As List(Of cSafeBags) Implements IPickup.PreviousPickups
        Get
            Return _previousPickups
        End Get
    End Property
    Public ReadOnly Property AccountId() As Integer Implements IPickup.AccountId
        Get
            Return _newBag.AccountabilityID.Value
        End Get
    End Property
    Public ReadOnly Property AccountType() As String Implements IPickup.AccountType
        Get
            Return _newBag.AccountabilityType.Value
        End Get
    End Property
    Public ReadOnly Property StoreIdName() As String Implements IPickup.StoreIdName
        Get
            Return _storeIdName
        End Get
    End Property
    Public Property State() As States Implements IPickup.State
        Get
            Return _state
        End Get
        Set(ByVal value As States)
            _state = value
        End Set
    End Property

    Public ReadOnly Property OldPickupPeriodId() As Integer Implements IPickup.OldPickupPeriodId
        Get
            Return _originalBag.PickupPeriodID.Value
        End Get
    End Property
    Public ReadOnly Property OldComments() As String Implements IPickup.OldComments
        Get
            Return _originalBag.Comments.Value
        End Get
    End Property
    Public ReadOnly Property OldSealNumber() As String Implements IPickup.OldSealNumber
        Get
            Return _originalBag.SealNumber.Value
        End Get
    End Property
    Public ReadOnly Property OldDenomValue(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer) As Decimal Implements IPickup.OldDenomValue
        Get
            Return _originalBag.Denom(currencyId, denominationId, tenderId).Value.Value
        End Get
    End Property


    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal accountType As String)

        'instantiate new bag
        _newBag = New cSafeBags(_oasys3Db)
        _newBag.Type.Value = BagTypes.Pickup
        _newBag.State.Value = BagStates.Sealed
        _newBag.InPeriodID.Value = Period.GetCurrent.Id
        _newBag.InUserID1.Value = user1
        _newBag.InUserID2.Value = user2
        _newBag.InDate.Value = Now
        _newBag.AccountabilityType.Value = accountType
        _newBag.PickupPeriodID.Value = _newBag.InPeriodID.Value

        'get default currency id
        _defaultCurrencyId = Currency.GetDefaultCurrencyId

    End Sub



    Public Function FloatsIssued(ByVal currencyID As String) As Decimal Implements IPickup.FloatsIssued

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            total += cashTill.FloatIssued
        Next
        Return total

    End Function

    Public Function CashTendered(ByVal currencyID As String) As Decimal Implements IPickup.CashTendered

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = 1 Then total += cashTillTender.Amount
                If cashTillTender.TenderId = 99 Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function

    Public Function NonCashTendered(ByVal currencyID As String, ByVal tenderId As Integer) As Decimal Implements IPickup.NonCashTendered

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = tenderId Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function



    Public Sub LoadSystemFigures(ByVal periodId As Integer, ByVal accountId As Integer) Implements IPickup.LoadSystemFigures

        'reset values
        _newBag.PickupPeriodID.Value = periodId
        _newBag.AccountabilityID.Value = accountId
        _cashierTills = New List(Of CashierTill)
        _previousPickups = New List(Of cSafeBags)


        'get system figures for allocated period and accountId into cashierTill list
        Select Case _newBag.AccountabilityType.Value
            Case AccountabilityTypes.Cashier
                Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                    'add each cashier record to cashTills
                    Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(periodId, accountId)
                    For Each cashier As cCashBalCashier In cashiers
                        Dim cashierTill As New CashierTill(cashier)
                        For Each cashierTender As cCashBalCashierTen In cashier.TenderTypes
                            cashierTill.Tenders.Add(New CashierTillTender(cashierTender))
                        Next
                        _cashierTills.Add(cashierTill)
                    Next
                End Using

            Case AccountabilityTypes.Till
                Using cashBalTill As New cCashBalTill(_oasys3Db)
                    'add each till record to cashTills
                    Dim tills As List(Of cCashBalTill) = cashBalTill.GetTills(periodId, accountId)
                    For Each till As cCashBalTill In tills
                        Dim cashierTill As New CashierTill(till)
                        For Each tillTender As cCashBalTillTen In till.TenderTypes
                            cashierTill.Tenders.Add(New CashierTillTender(tillTender))
                        Next
                        _cashierTills.Add(cashierTill)
                    Next
                End Using
        End Select


        'get all previous pick up bags for this accountability id and allocated period
        Using safebags As New cSafeBags(_oasys3Db)
            _previousPickups = safebags.GetBagsForPeriod(periodId, accountId, BagTypes.Pickup)
        End Using

        'remove original bag if exists from previous bag collection
        If _originalBag IsNot Nothing Then
            _previousPickups.RemoveAll(Function(f) f.ID.Value = _originalBag.ID.Value)
        End If

    End Sub

    Public Sub LoadOriginalBag(ByVal id As Integer) Implements IPickup.LoadOriginalBag

        'load original bag and system figures
        _originalBag = New cSafeBags(_oasys3Db)
        _originalBag.Load(id)
        LoadSystemFigures(_originalBag.InPeriodID.Value, _originalBag.AccountabilityID.Value)
        _newBag.RelatedBagId.Value = _originalBag.RelatedBagId.Value

    End Sub

    Public Function BagOkToOpen() As Boolean Implements IPickup.BagOkToOpen

        'load bag first to get its period id and all non closed period ids
        Dim nonClosedPeriodIds As ArrayList = Banking.Core.GetNonClosedPeriodIds
        Using oasys3 As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
            Using bag As New BOBanking.cSafeBags(oasys3)
                bag.Load(_originalBag.ID.Value)

                'check that there are no banking periods in the arraylist less than this bags period id
                For Each periodId As Integer In nonClosedPeriodIds
                    If periodId < bag.PickupPeriodID.Value Then
                        Return False
                    End If
                Next
            End Using
        End Using

        Return True

    End Function


    Public Sub SetFloatReturned(ByVal amount As Decimal) Implements IPickup.SetFloatReturned
        _newBag.FloatValue.Value = amount
    End Sub

    Public Sub SetDenomination(ByVal currencyId As String, ByVal denominationId As Decimal, ByVal tenderId As Integer, ByVal amount As Decimal) Implements IPickup.SetDenomination
        _newBag.Denom(currencyId, denominationId, tenderId).Value.Value = amount
    End Sub

    Public Overridable Sub SetComments(ByVal comments As String) Implements IPickup.SetComments
        _newBag.Comments.Value = comments
        If _originalBag IsNot Nothing Then _originalBag.Comments.Value = comments
    End Sub

    Public Sub SetSealNumber(ByVal sealNumber As String) Implements IPickup.SetSealNumber
        _newBag.SealNumber.Value = sealNumber
    End Sub

    Public Sub SetBagState(ByVal bagState As String) Implements IPickup.SetBagState
        _newBag.State.Value = bagState
    End Sub

    Public Sub SetRelatedBagId(ByVal id As Integer) Implements IPickup.SetRelatedBagId
        _newBag.RelatedBagId.Value = id
    End Sub


    Public Function GetAccountType() As String Implements IPickup.GetAccountType
        Return _newBag.AccountabilityType.Value
    End Function

    Protected Overridable Sub ReturnPickupBackToSafe()

        Using safe As New BOBanking.cSafe(_oasys3Db)
            safe.Load(_originalBag.OutPeriodID.Value)
            safe.AddBackToSafe(_originalBag)
        End Using

    End Sub

    Public Sub Save() Implements IPickup.Save

        Try
            'begin transaction
            _oasys3Db.BeginTransaction()

            Select Case _newBag.State.Value
                Case BagStates.BackToSafe
                    'returning bag back to safe so check if the old bag has had changes
                    Select Case True
                        Case _originalBag IsNot Nothing AndAlso BagsAreIdentical(_originalBag, _newBag)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.BackToSafe
                            _originalBag.Update()

                            ReturnPickupBackToSafe()

                            _newBag = _originalBag

                        Case _originalBag IsNot Nothing
                            MinusPickupsFromCashierTills(_cashierTills)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()

                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.SealNumber.Value = _originalBag.SealNumber.Value
                            _newBag.OutDate.Value = Now
                            _newBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _newBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _newBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _newBag.State.Value = BagStates.BackToSafe
                            _newBag.PickupPeriodID.Value = _originalBag.PickupPeriodID.Value
                            _newBag.Update()

                            Using safe As New BOBanking.cSafe(_oasys3Db)
                                safe.Load(_newBag.OutPeriodID.Value)
                                safe.AddBackToSafe(_newBag)
                            End Using

                        Case Else : Throw New Exception
                    End Select

                Case BagStates.Sealed, BagStates.ManagerChecked
                    'could be a reseal or new pickup so check if old bag exists
                    Select Case True
                        Case _originalBag Is Nothing
                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.Update()

                            'update cashTill record float returned amount as is a new pickup bag
                            Dim cashTill As CashierTill = _cashierTills.First(Function(f As CashierTill) f.CurrencyId = _defaultCurrencyId)
                            If cashTill IsNot Nothing Then
                                cashTill.FloatReturned += _newBag.FloatValue.Value
                            End If

                        Case _originalBag IsNot Nothing
                            MinusPickupsFromCashierTills(_cashierTills)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()

                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.PickupPeriodID.Value = _originalBag.PickupPeriodID.Value
                            _newBag.Update()

                        Case Else : Throw New Exception
                    End Select

                Case Else : Throw New Exception
            End Select


            'Save cashier/till pickup values to database 
            Select Case _newBag.AccountabilityType.Value
                Case AccountabilityTypes.Cashier
                    Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                        'get cashiers from table
                        Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                        For Each cashtill As CashierTill In _cashierTills
                            'get cashier from collection or create if not exist
                            Dim currencyId As String = cashtill.CurrencyId
                            Dim cashier As cCashBalCashier = cashiers.Find(Function(f As cCashBalCashier) f.CurrencyID.Value = currencyId)
                            If cashier Is Nothing Then
                                cashier = New cCashBalCashier(_oasys3Db)
                                cashier.CurrencyID.Value = currencyId
                                cashier.PeriodID.Value = cashtill.PeriodId
                                cashier.CashierID.Value = cashtill.Id
                                cashiers.Add(cashier)
                            End If

                            'set cashier values
                            cashier.FloatIssued.Value = cashtill.FloatIssued
                            cashier.FloatReturned.Value = cashtill.FloatReturned
                            If Not cashier.SaveIfExists Then cashier.SaveIfNew()

                            'delete all tenders first then add new tenders from cashtill tenders
                            cashier.DeleteTenders()
                            For Each cashtillTender As CashierTillTender In cashtill.Tenders
                                Dim tender As cCashBalCashierTen = cashier.TenderType(cashtillTender.TenderId)
                                tender.Quantity.Value = cashtillTender.Quantity
                                tender.Amount.Value = cashtillTender.Amount
                                tender.PickUp.Value = cashtillTender.PickUp
                                tender.SaveIfNew()
                            Next

                        Next

                    End Using

                Case AccountabilityTypes.Till
                    Using cashBaltill As New cCashBalTill(_oasys3Db)
                        'get tills from table
                        Dim tills As List(Of cCashBalTill) = cashBaltill.GetTills(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                        For Each cashtill As CashierTill In _cashierTills
                            'get till from collection or create if not exist
                            Dim currencyId As String = cashtill.CurrencyId
                            Dim till As cCashBalTill = tills.Find(Function(f As cCashBalTill) f.CurrencyID.Value = currencyId)
                            If till Is Nothing Then
                                till = New cCashBalTill(_oasys3Db)
                                till.CurrencyID.Value = currencyId
                                till.PeriodID.Value = cashtill.PeriodId
                                till.TillID.Value = cashtill.Id
                                tills.Add(till)
                            End If

                            'set till values
                            till.FloatIssued.Value = cashtill.FloatIssued
                            till.FloatReturned.Value = cashtill.FloatReturned
                            If Not till.SaveIfExists Then till.SaveIfNew()

                            'delete all tenders first then add new tenders from cashtill tenders
                            till.DeleteTenders()
                            For Each cashtillTender As CashierTillTender In cashtill.Tenders
                                Dim tender As cCashBalTillTen = till.TenderType(cashtillTender.TenderId)
                                tender.Quantity.Value = cashtillTender.Quantity
                                tender.Amount.Value = cashtillTender.Amount
                                tender.PickUp.Value = cashtillTender.PickUp
                                tender.SaveIfNew()
                            Next
                        Next

                    End Using
            End Select

            'commit transaction
            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw New OasysDbException("Problem saving pickup", ex)
        End Try

    End Sub

    Public Sub Save(ByVal decNewFloatValue As Decimal) Implements IPickup.Save

        Try
            'begin transaction
            _oasys3Db.BeginTransaction()

            Select Case _newBag.State.Value
                Case BagStates.BackToSafe
                    'returning bag back to safe so check if the old bag has had changes
                    Select Case True
                        Case _originalBag IsNot Nothing AndAlso BagsAreIdentical(_originalBag, _newBag)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.BackToSafe
                            _originalBag.Update()

                            Using safe As New BOBanking.cSafe(_oasys3Db)
                                safe.Load(_originalBag.OutPeriodID.Value)
                                safe.AddBackToSafe(_originalBag)
                            End Using

                            _newBag = _originalBag

                        Case _originalBag IsNot Nothing
                            MinusPickupsFromCashierTills(_cashierTills)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()

                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.SealNumber.Value = _originalBag.SealNumber.Value
                            _newBag.OutDate.Value = Now
                            _newBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _newBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _newBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _newBag.State.Value = BagStates.BackToSafe
                            _newBag.PickupPeriodID.Value = _originalBag.PickupPeriodID.Value
                            _newBag.Update()

                            Using safe As New BOBanking.cSafe(_oasys3Db)
                                safe.Load(_newBag.OutPeriodID.Value)
                                safe.AddBackToSafe(_newBag)
                            End Using

                        Case Else : Throw New Exception
                    End Select

                Case BagStates.Sealed, BagStates.ManagerChecked
                    'could be a reseal or new pickup so check if old bag exists
                    Select Case True
                        Case _originalBag Is Nothing
                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.Update()

                            'update cashTill record float returned amount as is a new pickup bag
                            Dim cashTill As CashierTill = _cashierTills.First(Function(f As CashierTill) f.CurrencyId = _defaultCurrencyId)
                            If cashTill IsNot Nothing Then
                                cashTill.FloatReturned += _newBag.FloatValue.Value
                            End If

                        Case _originalBag IsNot Nothing
                            MinusPickupsFromCashierTills(_cashierTills)
                            _originalBag.OutDate.Value = Now
                            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
                            _originalBag.State.Value = BagStates.Cancelled
                            _originalBag.Update()

                            AddPickupsToCashierTills(_cashierTills)
                            _newBag.PickupPeriodID.Value = _originalBag.PickupPeriodID.Value
                            _newBag.Update()

                        Case Else : Throw New Exception
                    End Select

                Case Else : Throw New Exception
            End Select


            'Save cashier/till pickup values to database 
            Select Case _newBag.AccountabilityType.Value
                Case AccountabilityTypes.Cashier
                    Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                        'get cashiers from table
                        Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                        For Each cashtill As CashierTill In _cashierTills
                            'get cashier from collection or create if not exist
                            Dim currencyId As String = cashtill.CurrencyId
                            Dim cashier As cCashBalCashier = cashiers.Find(Function(f As cCashBalCashier) f.CurrencyID.Value = currencyId)
                            If cashier Is Nothing Then
                                cashier = New cCashBalCashier(_oasys3Db)
                                cashier.CurrencyID.Value = currencyId
                                cashier.PeriodID.Value = cashtill.PeriodId
                                cashier.CashierID.Value = cashtill.Id
                                cashiers.Add(cashier)
                            End If

                            'set cashier values
                            cashier.FloatIssued.Value = cashtill.FloatIssued
                            cashier.FloatReturned.Value = cashtill.FloatReturned
                            If Not cashier.SaveIfExists Then cashier.SaveIfNew()

                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            ' Author   : Partha
                            ' Date     : 01/06/2011
                            ' Referral : 811
                            ' Notes    : Calculate cashier's float variance(difference between starting float and new float)
                            '
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            Dim cFloatVariance As ICashierFloatVariance

                            cFloatVariance = (New CashierFloatVarianceFactory).GetImplementation
                            cFloatVariance.UpdateFloatVariance(_oasys3Db, _newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value, currencyId, (decNewFloatValue - cashtill.FloatReturned))

                            'delete all tenders first then add new tenders from cashtill tenders
                            cashier.DeleteTenders()
                            For Each cashtillTender As CashierTillTender In cashtill.Tenders
                                Dim tender As cCashBalCashierTen = cashier.TenderType(cashtillTender.TenderId)
                                tender.Quantity.Value = cashtillTender.Quantity
                                tender.Amount.Value = cashtillTender.Amount
                                tender.PickUp.Value = cashtillTender.PickUp
                                tender.SaveIfNew()
                            Next

                        Next

                    End Using

                Case AccountabilityTypes.Till
                    Using cashBaltill As New cCashBalTill(_oasys3Db)
                        'get tills from table
                        Dim tills As List(Of cCashBalTill) = cashBaltill.GetTills(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                        For Each cashtill As CashierTill In _cashierTills
                            'get till from collection or create if not exist
                            Dim currencyId As String = cashtill.CurrencyId
                            Dim till As cCashBalTill = tills.Find(Function(f As cCashBalTill) f.CurrencyID.Value = currencyId)
                            If till Is Nothing Then
                                till = New cCashBalTill(_oasys3Db)
                                till.CurrencyID.Value = currencyId
                                till.PeriodID.Value = cashtill.PeriodId
                                till.TillID.Value = cashtill.Id
                                tills.Add(till)
                            End If

                            'set till values
                            till.FloatIssued.Value = cashtill.FloatIssued
                            till.FloatReturned.Value = cashtill.FloatReturned
                            If Not till.SaveIfExists Then till.SaveIfNew()

                            'delete all tenders first then add new tenders from cashtill tenders
                            till.DeleteTenders()
                            For Each cashtillTender As CashierTillTender In cashtill.Tenders
                                Dim tender As cCashBalTillTen = till.TenderType(cashtillTender.TenderId)
                                tender.Quantity.Value = cashtillTender.Quantity
                                tender.Amount.Value = cashtillTender.Amount
                                tender.PickUp.Value = cashtillTender.PickUp
                                tender.SaveIfNew()
                            Next
                        Next

                    End Using
            End Select

            'commit transaction
            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw New OasysDbException("Problem saving pickup", ex)
        End Try

    End Sub

    Public Sub AddPickupsToCashierTills(ByRef cashierTills As List(Of CashierTill)) Implements IPickup.AddPickupsToCashierTills

        For Each denom As BOBanking.cSafeBagsDenoms In _newBag.Denoms
            Dim cashTillTender As CashierTillTender = cashierTills.GetTender(_newBag.PickupPeriodID.Value, denom.CurrencyID.Value, _newBag.AccountabilityID.Value, denom.TenderID.Value)
            cashTillTender.PickUp += denom.Value.Value
        Next

    End Sub

    Public Sub MinusPickupsFromCashierTills(ByRef cashierTills As List(Of CashierTill)) Implements IPickup.MinusPickupsFromCashierTills

        For Each denom As BOBanking.cSafeBagsDenoms In _originalBag.Denoms
            Dim cashTillTender As CashierTillTender = cashierTills.GetTender(_originalBag.InPeriodID.Value, denom.CurrencyID.Value, _originalBag.AccountabilityID.Value, denom.TenderID.Value)
            cashTillTender.PickUp -= denom.Value.Value
        Next

    End Sub

    ''' <summary>
    ''' New Banking - Updated to write to the new "CashDrop" field
    ''' </summary>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub NewBankingSaveCashDrop() Implements IPickup.NewBankingSaveCashDrop
        Try
            _oasys3Db.BeginTransaction()

            AddPickupsToCashierTills(_cashierTills)
            _newBag.Update()

            'update cashTill record float returned amount as is a new pickup bag
            Dim cashTillXXX As CashierTill = _cashierTills.First(Function(f As CashierTill) f.CurrencyId = _defaultCurrencyId)
            If cashTillXXX IsNot Nothing Then
                cashTillXXX.FloatReturned += _newBag.FloatValue.Value
            End If

            Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                'get cashiers from table
                Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                For Each cashtill As CashierTill In _cashierTills
                    'get cashier from collection or create if not exist
                    Dim currencyId As String = cashtill.CurrencyId
                    Dim cashier As cCashBalCashier = cashiers.Find(Function(f As cCashBalCashier) f.CurrencyID.Value = currencyId)
                    If cashier Is Nothing Then
                        cashier = New cCashBalCashier(_oasys3Db)
                        cashier.CurrencyID.Value = currencyId
                        cashier.PeriodID.Value = cashtill.PeriodId
                        cashier.CashierID.Value = cashtill.Id
                        cashiers.Add(cashier)
                    End If

                    'set cashier values
                    cashier.FloatIssued.Value = cashtill.FloatIssued
                    cashier.FloatReturned.Value = cashtill.FloatReturned
                    If Not cashier.SaveIfExists Then cashier.SaveIfNew()

                    'delete all tenders first then add new tenders from cashtill tenders
                    cashier.DeleteTenders()
                    For Each cashtillTender As CashierTillTender In cashtill.Tenders
                        Dim tender As cCashBalCashierTen = cashier.TenderType(cashtillTender.TenderId)
                        tender.Quantity.Value = cashtillTender.Quantity
                        tender.Amount.Value = cashtillTender.Amount
                        tender.PickUp.Value = cashtillTender.PickUp
                        tender.SaveIfNew()
                    Next
                Next
            End Using

            'update "cashdrop" field
            _oasys3Db.NewBankingNonQueryExecuteSql("update SafeBags set CashDrop = 1 where ID = " & _newBag.ID.Value.ToString)

            _oasys3Db.CommitTransaction()
        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw New OasysDbException("Problem saving pickup", ex)
        End Try
    End Sub

    ''' <summary>
    ''' New Banking - Banking - Pickup Recheck, return new PickupID
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function NewBankingPickupReCheckSave() As Integer Implements IPickup.NewBankingPickupReCheckSave
        Try
            NewBankingPickupReCheckSave = 0

            _oasys3Db.BeginTransaction()

            MinusPickupsFromCashierTills(_cashierTills)
            _originalBag.OutDate.Value = Now
            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
            _originalBag.State.Value = BagStates.Cancelled
            _originalBag.Update()

            AddPickupsToCashierTills(_cashierTills)
            _newBag.PickupPeriodID.Value = _originalBag.PickupPeriodID.Value
            _newBag.Update()

            'save cashier/till pickup values to database
            Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                'get cashiers from table
                Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(_newBag.PickupPeriodID.Value, _newBag.AccountabilityID.Value)

                For Each cashtill As CashierTill In _cashierTills
                    'get cashier from collection or create if not exist
                    Dim currencyId As String = cashtill.CurrencyId
                    Dim cashier As cCashBalCashier = cashiers.Find(Function(f As cCashBalCashier) f.CurrencyID.Value = currencyId)
                    If cashier Is Nothing Then
                        cashier = New cCashBalCashier(_oasys3Db)
                        cashier.CurrencyID.Value = currencyId
                        cashier.PeriodID.Value = cashtill.PeriodId
                        cashier.CashierID.Value = cashtill.Id
                        cashiers.Add(cashier)
                    End If

                    'set cashier values
                    cashier.FloatIssued.Value = cashtill.FloatIssued
                    cashier.FloatReturned.Value = cashtill.FloatReturned
                    If Not cashier.SaveIfExists Then cashier.SaveIfNew()

                    'delete all tenders first then add new tenders from cashtill tenders
                    cashier.DeleteTenders()
                    For Each cashtillTender As CashierTillTender In cashtill.Tenders
                        Dim tender As cCashBalCashierTen = cashier.TenderType(cashtillTender.TenderId)
                        tender.Quantity.Value = cashtillTender.Quantity
                        tender.Amount.Value = cashtillTender.Amount
                        tender.PickUp.Value = cashtillTender.PickUp
                        tender.SaveIfNew()
                    Next
                Next
            End Using

            _oasys3Db.CommitTransaction()


            NewBankingPickupReCheckSave = _newBag.ID.Value
        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw New OasysDbException("Problem saving pickup", ex)
        End Try
    End Function

End Class

Public Class PickupUsingLatestSafe
    Inherits Pickup

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal accountType As String)

        MyBase.New(user1, user2, accountType)

    End Sub

    Protected Overrides Sub ReturnPickupBackToSafe()

        Dim latestSafePeriodId As Integer = Period.GetLatestSafe()

        Using safe As New cSafe(_oasys3Db)

            safe.Load(latestSafePeriodId)

            safe.AddBackToSafe(_originalBag)

        End Using

    End Sub



End Class
