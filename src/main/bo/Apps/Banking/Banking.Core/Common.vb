﻿Imports BOBanking
Imports System.Data

Public Module Common

    ''' <summary>
    ''' Returns cashtill tender given values. If not found then one will be created and added to collection
    ''' </summary>
    ''' <param name="cashTills"></param>
    ''' <param name="periodId"></param>
    ''' <param name="currencyId"></param>
    ''' <param name="accountId"></param>
    ''' <param name="tenderId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Runtime.CompilerServices.Extension()> Function GetTender(ByRef cashTills As List(Of CashierTill), ByVal periodId As Integer, ByVal currencyId As String, ByVal accountId As Integer, ByVal tenderId As Integer) As CashierTillTender

        'get cash till record for this currency or create one if none exist
        Dim cashTill As CashierTill = cashTills.Find(Function(f As CashierTill) f.CurrencyId = currencyId)
        If cashTill Is Nothing Then
            cashTill = New CashierTill(periodId, currencyId, accountId)
            cashTills.Add(cashTill)
        End If

        'get cash till tender record for this denom else create one if none exist
        Dim cashTillTender As CashierTillTender = cashTill.Tenders.Find(Function(f As CashierTillTender) f.TenderId = tenderId)
        If cashTillTender Is Nothing Then
            cashTillTender = New CashierTillTender(tenderId)
            cashTill.Tenders.Add(cashTillTender)
        End If

        Return cashTillTender

    End Function

    ''' <summary>
    ''' Returns total amount for given currency, period and tender ids
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Runtime.CompilerServices.Extension()> Function GetAmount(ByRef cashVars As List(Of CashierTillVariance), ByVal currencyId As String, ByVal tenderId As Integer, ByVal periodId As Integer) As Decimal

        Dim total As Decimal = 0

        For Each cashVar As CashierTillVariance In cashVars
            If cashVar.CurrencyId <> currencyId Then Continue For
            If cashVar.TenderId <> tenderId Then Continue For
            If cashVar.PeriodId <> periodId Then Continue For
            total += cashVar.Amount
        Next

        Return total

    End Function

    ''' <summary>
    ''' Returns total amount for given currency, account, period and tender ids
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Runtime.CompilerServices.Extension()> Function GetAmount(ByRef cashVars As List(Of CashierTillVariance), ByVal currencyId As String, ByVal accountId As Integer, ByVal tenderId As Integer, ByVal periodId As Integer) As Decimal

        Dim total As Decimal = 0

        For Each cashVar As CashierTillVariance In cashVars
            If cashVar.CurrencyId <> currencyId Then Continue For
            If cashVar.AccountId <> accountId Then Continue For
            If cashVar.TenderId <> tenderId Then Continue For
            If cashVar.PeriodId <> periodId Then Continue For
            total += cashVar.Amount
        Next

        Return total

    End Function

    ''' <summary>
    ''' checks whether 2 safe bags are identical
    ''' </summary>
    ''' <param name="bag1"></param>
    ''' <param name="bag2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BagsAreIdentical(ByVal bag1 As cSafeBags, ByVal bag2 As cSafeBags) As Boolean

        'check bag2 against bag1
        For Each denom1 As cSafeBagsDenoms In bag1.Denoms
            If denom1.Value.Value = 0 Then Continue For
            Dim id As Decimal = denom1.ID.Value
            Dim tenderId As Integer = denom1.TenderID.Value

            Dim denom2 As cSafeBagsDenoms = bag2.Denoms.Find(Function(f) f.ID.Value = id AndAlso f.TenderID.Value = tenderId)
            If denom2 Is Nothing Then
                Return False
            End If

            If denom1.Value.Value <> denom2.Value.Value Then
                Return False
            End If
        Next

        'check bag1 against bag2
        For Each denom2 As cSafeBagsDenoms In bag2.Denoms
            If denom2.Value.Value = 0 Then Continue For
            Dim id As Decimal = denom2.ID.Value
            Dim tenderId As Integer = denom2.TenderID.Value

            Dim denom1 As cSafeBagsDenoms = bag1.Denoms.Find(Function(f) f.ID.Value = id AndAlso f.TenderID.Value = tenderId)
            If denom1 Is Nothing Then
                Return False
            End If

            If denom2.Value.Value <> denom1.Value.Value Then
                Return False
            End If
        Next

        'if here then bags are identical
        Return True

    End Function

    ''' <summary>
    ''' checks whether 2 safe bags are identical
    ''' </summary>
    ''' <param name="bag1"></param>
    ''' <param name="bag2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BagsAreIdentical(ByVal bag1 As SafeBag, ByVal bag2 As SafeBag) As Boolean

        'check bag2 against bag1
        For Each denom1 As cSafeBagsDenoms In bag1.Denoms
            If denom1.Value.Value = 0 Then Continue For
            Dim id As Decimal = denom1.ID.Value
            Dim tenderId As Integer = denom1.TenderID.Value

            Dim denom2 As cSafeBagsDenoms = bag2.Denoms.Find(Function(f) f.ID.Value = id AndAlso f.TenderID.Value = tenderId)
            If denom2 Is Nothing Then
                Return False
            End If

            If denom1.Value.Value <> denom2.Value.Value Then
                Return False
            End If
        Next

        'check bag1 against bag2
        For Each denom2 As cSafeBagsDenoms In bag2.Denoms
            If denom2.Value.Value = 0 Then Continue For
            Dim id As Decimal = denom2.ID.Value
            Dim tenderId As Integer = denom2.TenderID.Value

            Dim denom1 As cSafeBagsDenoms = bag1.Denoms.Find(Function(f) f.ID.Value = id AndAlso f.TenderID.Value = tenderId)
            If denom1 Is Nothing Then
                Return False
            End If

            If denom2.Value.Value <> denom1.Value.Value Then
                Return False
            End If
        Next

        'if here then bags are identical
        Return True

    End Function

    ''' <summary>
    ''' checks to see if given seal number is valid
    ''' </summary>
    ''' <param name="sealNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsSealNumberValid(ByVal sealNumber As String) As Boolean

        'check that seal number is valid
        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using safeBag As New BOBanking.cSafeBags(oasys3db)
                Return safeBag.IsSealNumberValid(sealNumber)
            End Using
        End Using

    End Function

    ''' <summary>
    ''' return non-closed period ids as arraylist including today regardless
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNonClosedPeriodIds() As ArrayList

        Dim al As New ArrayList()

        Using oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB(My.Resources.DsnName, 0)
            Using safe As New BOBanking.cSafe(oasys3db)
                Dim dt As DataTable = safe.GetNonClosedSafeRecords
                For Each dr As DataRow In dt.Rows
                    al.Add(dr(0))
                Next
            End Using
        End Using

        Return al

    End Function


End Module

Public Structure BagTypes
    Private _value As String
    Public Shared Float As String = "F"
    Public Shared Pickup As String = "P"
    Public Shared Banking As String = "B"
    Public Shared [Property] As String = "L"
    Public Shared GiftToken As String = "G"
End Structure

Public Structure BagStates
    Private _value As String
    Public Shared Released As String = "R"
    Public Shared Cancelled As String = "C"
    Public Shared Sealed As String = "S"
    Public Shared BackToSafe As String = "B"
    Public Shared PreparedNotReleased As String = "P"
    Public Shared ReleasedNotPrepared As String = "Q"
    Public Shared ManagerChecked As String = "M"
End Structure

Public Structure AccountabilityTypes
    Private _value As String
    Public Shared Cashier As String = "C"
    Public Shared Till As String = "T"
End Structure