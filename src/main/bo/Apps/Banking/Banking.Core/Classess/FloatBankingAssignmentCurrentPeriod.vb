﻿Public Class FloatBankingAssignmentCurrentPeriod
    Implements IFloatBanking

    Public Function OutPeriodID(ByVal CurrentPeriodID As Integer, ByVal BankingPeriodID As Integer) As Integer Implements IFloatBanking.OutPeriodID

        Return CurrentPeriodID

    End Function

    Public Function OutDate(ByVal BankingPeriodID As Integer) As Nullable(Of Date) Implements IFloatBanking.OutDate

        Return Now

    End Function

End Class