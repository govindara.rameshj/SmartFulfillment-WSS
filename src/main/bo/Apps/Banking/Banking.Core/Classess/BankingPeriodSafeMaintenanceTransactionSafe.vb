﻿Public Class BankingPeriodSafeMaintenanceTransactionSafe
    Inherits BankingPeriod

    Public Sub New(ByRef OdbcConnection As clsOasys3DB)

        MyBase.New(OdbcConnection)

    End Sub

    Public Overrides Sub NewBankingSafeMaintenanceTransactionSafe(ByVal BankingPeriodID As Integer)

        _safe.NewBankingSafeMaintenanceTransactionSafe(BankingPeriodID)

    End Sub

End Class