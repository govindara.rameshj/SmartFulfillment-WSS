﻿
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Partha
' Date     : 01/6/2011
' Referral : 811
' Notes    : Calculate cashier's float variance(difference between starting float and new float)
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Class CashierFloatVarianceNew
    Implements ICashierFloatVariance

    Public Sub UpdateFloatVariance(ByRef DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal PeriodID As Integer, ByVal CashierId As Integer, ByVal CurrencyID As String, ByVal FloatVariance As Decimal) Implements ICashierFloatVariance.UpdateFloatVariance

        DB.NewBankingNonQueryExecuteSql("exec NewBankingCashierFloatVariance @PeriodID      = " & PeriodID.ToString & _
                                        "                                   ,@CashierID     = " & CashierId.ToString & _
                                        "                                   ,@CurrencyID    = '" & CurrencyID & "'" & _
                                        "                                   ,@FloatVariance = " & FloatVariance.ToString)

    End Sub

End Class
