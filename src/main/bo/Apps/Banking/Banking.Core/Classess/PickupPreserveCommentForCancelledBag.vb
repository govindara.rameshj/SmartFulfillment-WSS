﻿Public Class PickupPreserveCommentForCancelledBag
    Inherits Pickup

    Public Sub New(ByVal FirstUserID As Integer, ByVal SecondUserID As Integer, ByVal AccountingModel As String)

        MyBase.New(FirstUserID, SecondUserID, AccountingModel)

    End Sub

    Public Overrides Sub SetComments(ByVal comments As String)

        _newBag.Comments.Value = comments

    End Sub

End Class