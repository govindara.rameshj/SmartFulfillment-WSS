﻿Imports BOBanking

Public Class CashierTillTender
    Private _tenderId As Integer
    Private _quantity As Integer
    Private _amount As Decimal
    Private _pickUp As Decimal

    Public ReadOnly Property TenderId() As Integer
        Get
            Return _tenderId
        End Get
    End Property
    Public ReadOnly Property Amount() As Decimal
        Get
            Return _amount
        End Get
    End Property
    Public ReadOnly Property Quantity() As Integer
        Get
            Return _quantity
        End Get
    End Property
    Public Property PickUp() As Decimal
        Get
            Return _pickUp
        End Get
        Set(ByVal value As Decimal)
            _pickUp = value
        End Set
    End Property

    Public Sub New(ByVal cashierTender As cCashBalCashierTen)
        _tenderId = cashierTender.ID.Value
        _amount = cashierTender.Amount.Value
        _pickUp = cashierTender.PickUp.Value
        _quantity = cashierTender.Quantity.Value
    End Sub

    Public Sub New(ByVal tillTender As cCashBalTillTen)
        _tenderId = tillTender.ID.Value
        _amount = tillTender.Amount.Value
        _pickUp = tillTender.PickUp.Value
        _quantity = tillTender.Quantity.Value
    End Sub

    Public Sub New(ByVal tenderId As Integer)
        _tenderId = tenderId
    End Sub

End Class