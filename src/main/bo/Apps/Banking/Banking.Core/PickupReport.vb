﻿Imports BOBanking
Imports Cts.Oasys.Core.System

Public Class PickupReport
    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _cashierTills As New List(Of CashierTill)
    Private _pickupBags As New List(Of cSafeBags)
    Private _accountType As String = String.Empty
    'Private _accountIds As New ArrayList

    Public ReadOnly Property CashierTills() As List(Of CashierTill)
        Get
            Return _cashierTills
        End Get
    End Property
    Public ReadOnly Property PickupBags() As List(Of cSafeBags)
        Get
            Return _pickupBags
        End Get
    End Property
    'Public ReadOnly Property AccountIds() As ArrayList
    '    Get
    '        Return _accountIds
    '    End Get
    'End Property

    Public Sub New(ByVal accountType As String)
        _accountType = accountType
    End Sub


    Public Function FloatsIssued(ByVal currencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            total += cashTill.FloatIssued
        Next
        Return total

    End Function

    Public Function FloatsIssued(ByVal currencyID As String, ByVal id As Integer) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f) f.CurrencyId = currencyID AndAlso f.Id = id)
            total += cashTill.FloatIssued
        Next
        Return total

    End Function

    Public Function CashTendered(ByVal currencyID As String) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = 1 Then total += cashTillTender.Amount
                If cashTillTender.TenderId = 99 Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function

    Public Function CashTendered(ByVal currencyID As String, ByVal id As Integer) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f) f.CurrencyId = currencyID AndAlso f.Id = id)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = 1 Then total += cashTillTender.Amount
                If cashTillTender.TenderId = 99 Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function

    Public Function NonCashTendered(ByVal currencyID As String, ByVal tenderId As Integer) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f As CashierTill) f.CurrencyId = currencyID)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = tenderId Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function

    Public Function NonCashTendered(ByVal currencyID As String, ByVal tenderId As Integer, ByVal id As Integer) As Decimal

        Dim total As Decimal = 0
        For Each cashTill As CashierTill In _cashierTills.Where(Function(f) f.CurrencyId = currencyID AndAlso f.Id = id)
            For Each cashTillTender As CashierTillTender In cashTill.Tenders
                If cashTillTender.TenderId = tenderId Then total += cashTillTender.Amount
            Next
        Next

        Return total

    End Function


    Public Function GetAccountType() As String
        Return _accountType
    End Function

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub LoadSystemFigures(ByVal periodId As Integer)

        'load pickup bags for this period only
        Using safebag As New cSafeBags(_oasys3Db)
            _pickupBags = safebag.GetPickupBags(periodId)
        End Using


        ''get cashier/till ids for pickup bags above
        '_accountIds = New ArrayList
        'For Each bag As cSafeBags In _pickupBags
        '    If Not _accountIds.Contains(bag.AccountabilityID.Value) Then _accountIds.Add(bag.AccountabilityID.Value)
        'Next


        'load cashier/till records dependent on accountability
        _cashierTills = New List(Of CashierTill)

        Select Case _accountType
            Case AccountabilityTypes.Cashier
                Using cashBalCashier As New cCashBalCashier(_oasys3Db)
                    'add each cashier record to cashTills
                    Dim cashiers As List(Of cCashBalCashier) = cashBalCashier.GetCashiers(periodId)
                    For Each cashier As cCashBalCashier In cashiers
                        Dim cashierTill As New CashierTill(cashier)

                        'add tenders
                        For Each cashierTender As cCashBalCashierTen In cashier.TenderTypes
                            cashierTill.Tenders.Add(New CashierTillTender(cashierTender))
                        Next

                        'add variances
                        For Each variance As cCashBalCashierTenVar In cashier.TenderVariances
                            cashierTill.Variances.Add(New CashierTillVariance(variance))
                        Next

                        _cashierTills.Add(cashierTill)
                    Next
                End Using

                'get cashier names
                For Each cashTill As CashierTill In _cashierTills
                    cashTill.Name = User.GetUserIdName(cashTill.Id, IncludeExternalIds:=True, OnlyBankingIds:=True)
                Next

            Case AccountabilityTypes.Till
                Using cashBalTill As New cCashBalTill(_oasys3Db)
                    'add each till record to cashTills
                    Dim tills As List(Of cCashBalTill) = cashBalTill.GetTills(periodId)
                    For Each till As cCashBalTill In tills
                        Dim cashierTill As New CashierTill(till)

                        'add tenders
                        For Each tillTender As cCashBalTillTen In till.TenderTypes
                            cashierTill.Tenders.Add(New CashierTillTender(tillTender))
                        Next

                        'add variances
                        For Each variance As cCashBalTillTenVar In till.TenderVariances
                            cashierTill.Variances.Add(New CashierTillVariance(variance))
                        Next

                        _cashierTills.Add(cashierTill)
                    Next
                End Using

                'get till names
                For Each cashTill As CashierTill In _cashierTills
                    cashTill.Name = Workstation.GetIdDescription(cashTill.Id)
                Next

        End Select

    End Sub

    Public Function GetVariances(ByVal periodId As Integer) As List(Of CashierTillVariance)

        Dim variances As New List(Of CashierTillVariance)

        Select Case _accountType
            Case AccountabilityTypes.Cashier
                Dim cashVar As New cCashBalCashierTenVar(_oasys3Db)
                Dim cashVars As List(Of cCashBalCashierTenVar) = cashVar.GetVariances(periodId)
                For Each cv As cCashBalCashierTenVar In cashVars
                    variances.Add(New CashierTillVariance(cv))
                Next

            Case AccountabilityTypes.Till
                Dim cashVar As New cCashBalTillTenVar(_oasys3Db)
                Dim cashVars As List(Of cCashBalTillTenVar) = cashVar.GetVariances(periodId)
                For Each cv As cCashBalTillTenVar In cashVars
                    variances.Add(New CashierTillVariance(cv))
                Next

        End Select

        Return variances

    End Function

End Class