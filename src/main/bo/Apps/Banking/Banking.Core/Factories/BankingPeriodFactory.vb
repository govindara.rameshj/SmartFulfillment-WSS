﻿Public Class BankingPeriodFactory
    Inherits RequirementSwitchFactory(Of IBankingPeriod)

    Friend _SwitchP022024 As Boolean
    Friend _SwitchP022073 As Boolean
    Friend _OdbcConnection As clsOasys3DB

    Public Overrides Function ImplementationA() As IBankingPeriod

        Return New BankingPeriodSafeMaintenanceTransactionSafe(_OdbcConnection)

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()

        _SwitchP022024 = SwitchRepository.IsSwitchPresentAndEnabled(-22024)
        _SwitchP022073 = SwitchRepository.IsSwitchPresentAndEnabled(-22073)

        Return RequirementSwitch()

    End Function

    Public Overrides Function ImplementationB() As IBankingPeriod

        Return New BankingPeriod

    End Function

    Public Sub SetOdbcConnection(ByRef Connection As clsOasys3DB)

        _OdbcConnection = Connection

    End Sub

#Region "Private Functions And Procedures"

    Friend Function RequirementSwitch() As Boolean

        If _SwitchP022024 = False And _SwitchP022073 = False Then Return False

        Return True

    End Function

#End Region

End Class