﻿Public Class PickupFactory
    Inherits RequirementSwitchFactory(Of IPickup)

    Friend _FirstUserID As Integer
    Friend _SecondUserID As Integer
    Friend _AccountingModel As String

    Public Sub PickupConstructor(ByVal FirstUserID As Integer, ByVal SecondUserID As Integer, ByVal AccountingModel As String)

        _FirstUserID = FirstUserID
        _SecondUserID = SecondUserID
        _AccountingModel = AccountingModel

    End Sub

    Public Overrides Function ImplementationA() As IPickup

        Return New PickupPreserveCommentForCancelledBag(_FirstUserID, _SecondUserID, _AccountingModel)

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22017)

    End Function

    Public Overrides Function ImplementationB() As IPickup

        Return New Pickup(_FirstUserID, _SecondUserID, _AccountingModel)

    End Function

End Class