﻿
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Partha
' Date     : 01/6/2011
' Referral : 811
' Notes    : Calculate cashier's float variance(difference between starting float and new float)
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Class CashierFloatVarianceFactory
    Inherits BaseFactory(Of ICashierFloatVariance)

    Public Overrides Function Implementation() As ICashierFloatVariance

        Return New CashierFloatVarianceNew
    End Function
End Class