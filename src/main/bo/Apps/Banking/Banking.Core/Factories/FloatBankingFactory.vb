﻿Public Class FloatBankingFactory
    Inherits RequirementSwitchFactory(Of IFloatBanking)

    Public Overrides Function ImplementationA() As IFloatBanking

        Return New FloatBankingAssignmentSelectedPeriod

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-951)

    End Function

    Public Overrides Function ImplementationB() As IFloatBanking

        Return New FloatBankingAssignmentCurrentPeriod

    End Function

End Class