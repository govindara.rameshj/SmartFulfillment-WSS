﻿Public Class FloatBankingRepositoryFactory
    Inherits BaseFactory(Of IFloatBankingRepository)

    Public Overrides Function Implementation() As IFloatBankingRepository

        Return New FloatBankingRepository

    End Function

End Class