﻿Imports BOBanking
Imports Cts.Oasys.Core.System

Public Class GiftTokenProperty
    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _newBag As cSafeBags = Nothing
    Private _originalBag As cSafeBags = Nothing

    Public ReadOnly Property AccountType() As String
        Get
            Return _newBag.AccountabilityType.Value
        End Get
    End Property
    Public ReadOnly Property Type() As String
        Get
            Return _newBag.Type.Value
        End Get
    End Property

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal bagType As String, ByVal accountType As String)

        'instantiate new bag
        _newBag = New cSafeBags(_oasys3Db)
        _newBag.Type.Value = bagType
        _newBag.State.Value = BagStates.Sealed
        _newBag.InPeriodID.Value = Period.GetCurrent.Id
        _newBag.InUserID1.Value = user1
        _newBag.InUserID2.Value = user2
        _newBag.InDate.Value = Now
        _newBag.AccountabilityType.Value = accountType
        _newBag.PickupPeriodID.Value = _newBag.InPeriodID.Value

    End Sub

    Public Sub LoadOriginalBag(ByVal id As Integer)
        _originalBag = New cSafeBags(_oasys3Db)
        _originalBag.Load(id)
    End Sub


    Public Sub SetComments(ByVal comments As String)
        _newBag.Comments.Value = comments
        If _originalBag IsNot Nothing Then _originalBag.Comments.Value = comments
    End Sub

    Public Sub SetSealNumber(ByVal sealNumber As String)
        _newBag.SealNumber.Value = sealNumber
    End Sub

    Public Sub SetValue(ByVal value As Decimal)
        _newBag.Value.Value = value
    End Sub

    Public Sub SetBagState(ByVal bagState As String, Optional ByVal accountId As Integer = 0)
        _newBag.State.Value = bagState
        _newBag.AccountabilityID.Value = accountId
    End Sub

    Public Sub SetAccountId(ByVal accountId As Integer)
        _newBag.AccountabilityID.Value = accountId
    End Sub


    Public Function GetComments() As String
        If _originalBag IsNot Nothing Then Return _originalBag.Comments.Value
        Return _newBag.Comments.Value
    End Function

    Public Function GetValue() As Decimal
        If _originalBag IsNot Nothing Then Return _originalBag.Value.Value
        Return _newBag.Value.Value
    End Function

    Public Function GetSealNumber() As String
        If _originalBag IsNot Nothing Then Return _originalBag.SealNumber.Value
        Return _newBag.SealNumber.Value
    End Function


    Public Function IsBankingAndNotManagerChecked() As Boolean
        Return (_originalBag.Type.Value = BagTypes.Banking AndAlso _originalBag.State.Value <> BagStates.ManagerChecked)
    End Function


    Public Sub Save(Optional ByVal toRelease As Boolean = False)

        Try
            _oasys3Db.BeginTransaction()

            If _originalBag Is Nothing Then
                _newBag.SaveIfNew()
            Else
                _originalBag.AccountabilityID.Value = _newBag.AccountabilityID.Value
                _originalBag.OutDate.Value = Now
                _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
                _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
                _originalBag.OutUserID2.Value = _newBag.InUserID2.Value

                'if banking then check what to set state to
                If _originalBag.Type.Value = BagTypes.Banking Then
                    Select Case _originalBag.State.Value
                        Case BagStates.PreparedNotReleased : _originalBag.State.Value = BagStates.Released
                        Case Else : _originalBag.State.Value = BagStates.ReleasedNotPrepared
                    End Select
                Else
                    _originalBag.State.Value = BagStates.Cancelled
                    If toRelease Then _originalBag.State.Value = BagStates.Released
                End If

                _originalBag.SaveIfExists()

                If Not (toRelease AndAlso _newBag.Value.Value = _originalBag.Value.Value) Then
                    _newBag.SaveIfNew()
                End If
            End If

            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' New Banking - Need to pass the PeriodID
    ''' </summary>
    ''' <param name="intPeriodID"></param>
    ''' <param name="intUserID1"></param>
    ''' <param name="intUserID2"></param>
    ''' <param name="strBagType"></param>
    ''' <param name="strAccountType"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub New(ByVal intPeriodID As Integer, ByVal intUserID1 As Integer, ByVal intUserID2 As Integer, ByVal strBagType As String, ByVal strAccountType As String)
        _newBag = New cSafeBags(_oasys3Db)

        _newBag.Type.Value = strBagType
        _newBag.State.Value = BagStates.Sealed

        'issues with set float banking business object, safer to set the InPeriodID to the PickupPeriodID
        _newBag.InDate.Value = Now
        '_newBag.InPeriodID.Value = Period.GetCurrent.Id
        _newBag.InPeriodID.Value = intPeriodID
        _newBag.PickupPeriodID.Value = intPeriodID

        _newBag.InUserID1.Value = intUserID1
        _newBag.InUserID2.Value = intUserID2

        _newBag.AccountabilityType.Value = strAccountType
    End Sub

    ''' <summary>
    ''' New Banking - Updated to write to the new "BagCollectionSlipNo" and "BagCollectionComment" fields
    ''' </summary>
    ''' <param name="intBagID"></param>
    ''' <param name="strSlipNo"></param>
    ''' <param name="strComment"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub NewBankingSave(ByVal intBagID As Integer, ByVal strSlipNo As String, ByVal strComment As String)
        Try
            _oasys3Db.BeginTransaction()

            _originalBag.AccountabilityID.Value = _newBag.AccountabilityID.Value
            _originalBag.OutDate.Value = Now
            _originalBag.OutPeriodID.Value = _newBag.InPeriodID.Value
            _originalBag.OutUserID1.Value = _newBag.InUserID1.Value
            _originalBag.OutUserID2.Value = _newBag.InUserID2.Value
            _originalBag.State.Value = BagStates.ReleasedNotPrepared
            _originalBag.SaveIfExists()

            'collect slip & comment
            _oasys3Db.NewBankingNonQueryExecuteSql("exec NewBankingBagCollectionUpdate @BagID   = " & intBagID.ToString & "," & _
                                                                                     " @SlipNo  = '" & strSlipNo & "', " & _
                                                                                     " @Comment = '" & strComment & "'")
            _oasys3Db.CommitTransaction()

        Catch ex As Exception
            If _oasys3Db.Transaction Then _oasys3Db.RollBackTransaction()
            Throw ex
        End Try

    End Sub

End Class
