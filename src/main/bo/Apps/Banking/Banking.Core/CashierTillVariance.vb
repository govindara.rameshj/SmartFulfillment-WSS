﻿Imports BOBanking

Public Class CashierTillVariance
    Private _periodId As Integer
    Private _tradingPeriodId As Integer
    Private _accountId As Integer
    Private _currencyId As String
    Private _tenderId As Integer
    Private _quantity As Integer
    Private _amount As Decimal
    Private _pickUp As Decimal
    Private _periodDate As Date

    Public ReadOnly Property PeriodId() As Integer
        Get
            Return _periodId
        End Get
    End Property
    Public ReadOnly Property TradingPeriodId() As Integer
        Get
            Return _tradingPeriodId
        End Get
    End Property
    Public ReadOnly Property AccountId() As Integer
        Get
            Return _accountId
        End Get
    End Property
    Public ReadOnly Property CurrencyId() As String
        Get
            Return _currencyId
        End Get
    End Property
    Public ReadOnly Property TenderId() As Integer
        Get
            Return _tenderId
        End Get
    End Property
    Public ReadOnly Property Amount() As Decimal
        Get
            Return _amount
        End Get
    End Property
    Public ReadOnly Property Quantity() As Integer
        Get
            Return _quantity
        End Get
    End Property
    Public ReadOnly Property PickUp() As Decimal
        Get
            Return _pickUp
        End Get
    End Property
    Public Property PeriodDate() As Date
        Get
            Return _periodDate
        End Get
        Set(ByVal value As Date)
            _periodDate = value
        End Set
    End Property

    Public Sub New(ByVal cashierVariance As cCashBalCashierTenVar)
        _periodId = cashierVariance.PeriodID.Value
        _tradingPeriodId = cashierVariance.TradingPeriodID.Value
        _accountId = cashierVariance.CashierID.Value
        _currencyId = cashierVariance.CurrencyID.Value
        _tenderId = cashierVariance.ID.Value
        _amount = cashierVariance.Amount.Value
        _pickUp = cashierVariance.PickUp.Value
        _quantity = cashierVariance.Quantity.Value
    End Sub

    Public Sub New(ByVal tillVariance As cCashBalTillTenVar)
        _periodId = tillVariance.PeriodID.Value
        _tradingPeriodId = tillVariance.TradingPeriodID.Value
        _accountId = tillVariance.TillID.Value
        _currencyId = tillVariance.CurrencyID.Value
        _tenderId = tillVariance.ID.Value
        _amount = tillVariance.Amount.Value
        _pickUp = tillVariance.PickUp.Value
        _quantity = tillVariance.Quantity.Value
    End Sub

End Class