﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FloatEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnAccept = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdSafe = New FarPoint.Win.Spread.FpSpread
        Me.stsStatus = New System.Windows.Forms.StatusStrip
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.grpDetails = New System.Windows.Forms.GroupBox
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblSealMessage = New System.Windows.Forms.Label
        Me.cmbCashierTill = New System.Windows.Forms.ComboBox
        Me.grpOptions = New System.Windows.Forms.GroupBox
        Me.rdoIssue = New System.Windows.Forms.RadioButton
        Me.rdoReturn = New System.Windows.Forms.RadioButton
        Me.rdoReseal = New System.Windows.Forms.RadioButton
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stsStatus.SuspendLayout()
        Me.grpDetails.SuspendLayout()
        Me.grpOptions.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Enabled = False
        Me.btnAccept.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAccept.Location = New System.Drawing.Point(6, 649)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 1
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(410, 649)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdSafe
        '
        Me.spdSafe.About = "3.0.2004.2005"
        Me.spdSafe.AccessibleDescription = ""
        Me.spdSafe.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdSafe.EditModeReplace = True
        Me.spdSafe.Location = New System.Drawing.Point(6, 6)
        Me.spdSafe.Name = "spdSafe"
        Me.spdSafe.Size = New System.Drawing.Size(480, 562)
        Me.spdSafe.TabIndex = 0
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdSafe.TextTipAppearance = TipAppearance1
        Me.spdSafe.ActiveSheetIndex = -1
        '
        'stsStatus
        '
        Me.stsStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.stsStatus.Location = New System.Drawing.Point(3, 691)
        Me.stsStatus.Name = "stsStatus"
        Me.stsStatus.Size = New System.Drawing.Size(486, 22)
        Me.stsStatus.TabIndex = 19
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = False
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(471, 17)
        Me.lblStatus.Spring = True
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpDetails
        '
        Me.grpDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDetails.Controls.Add(Me.txtSeal)
        Me.grpDetails.Controls.Add(Me.txtComments)
        Me.grpDetails.Controls.Add(Me.Label3)
        Me.grpDetails.Controls.Add(Me.Label1)
        Me.grpDetails.Controls.Add(Me.lblSealMessage)
        Me.grpDetails.Location = New System.Drawing.Point(6, 574)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(480, 69)
        Me.grpDetails.TabIndex = 2
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Enter Details"
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(82, 16)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 1
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(82, 43)
        Me.txtComments.MaxLength = 255
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(392, 20)
        Me.txtComments.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(70, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "&Comments"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(70, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Seal Number"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSealMessage
        '
        Me.lblSealMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSealMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblSealMessage.ForeColor = System.Drawing.Color.Red
        Me.lblSealMessage.Location = New System.Drawing.Point(177, 16)
        Me.lblSealMessage.Name = "lblSealMessage"
        Me.lblSealMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSealMessage.Size = New System.Drawing.Size(297, 20)
        Me.lblSealMessage.TabIndex = 2
        Me.lblSealMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbCashierTill
        '
        Me.cmbCashierTill.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbCashierTill.FormattingEnabled = True
        Me.cmbCashierTill.Location = New System.Drawing.Point(155, 39)
        Me.cmbCashierTill.Name = "cmbCashierTill"
        Me.cmbCashierTill.Size = New System.Drawing.Size(319, 21)
        Me.cmbCashierTill.TabIndex = 3
        Me.cmbCashierTill.Visible = False
        '
        'grpOptions
        '
        Me.grpOptions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpOptions.Controls.Add(Me.cmbCashierTill)
        Me.grpOptions.Controls.Add(Me.rdoIssue)
        Me.grpOptions.Controls.Add(Me.rdoReturn)
        Me.grpOptions.Controls.Add(Me.rdoReseal)
        Me.grpOptions.Location = New System.Drawing.Point(6, 499)
        Me.grpOptions.Name = "grpOptions"
        Me.grpOptions.Size = New System.Drawing.Size(480, 69)
        Me.grpOptions.TabIndex = 1
        Me.grpOptions.TabStop = False
        Me.grpOptions.Text = "Choose Option"
        Me.grpOptions.Visible = False
        '
        'rdoIssue
        '
        Me.rdoIssue.Location = New System.Drawing.Point(155, 16)
        Me.rdoIssue.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoIssue.Name = "rdoIssue"
        Me.rdoIssue.Size = New System.Drawing.Size(177, 19)
        Me.rdoIssue.TabIndex = 2
        Me.rdoIssue.Text = "Issue to "
        Me.rdoIssue.UseVisualStyleBackColor = True
        Me.rdoIssue.Visible = False
        '
        'rdoReturn
        '
        Me.rdoReturn.Location = New System.Drawing.Point(6, 41)
        Me.rdoReturn.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoReturn.Name = "rdoReturn"
        Me.rdoReturn.Size = New System.Drawing.Size(143, 19)
        Me.rdoReturn.TabIndex = 1
        Me.rdoReturn.Text = "Return Bag to Safe"
        Me.rdoReturn.UseVisualStyleBackColor = True
        '
        'rdoReseal
        '
        Me.rdoReseal.Location = New System.Drawing.Point(6, 16)
        Me.rdoReseal.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoReseal.Name = "rdoReseal"
        Me.rdoReseal.Size = New System.Drawing.Size(143, 19)
        Me.rdoReseal.TabIndex = 0
        Me.rdoReseal.Text = "Re-Seal Bag"
        Me.rdoReseal.UseVisualStyleBackColor = True
        '
        'FloatEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(492, 716)
        Me.Controls.Add(Me.grpOptions)
        Me.Controls.Add(Me.grpDetails)
        Me.Controls.Add(Me.stsStatus)
        Me.Controls.Add(Me.spdSafe)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "FloatEntry"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Float Bag"
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stsStatus.ResumeLayout(False)
        Me.stsStatus.PerformLayout()
        Me.grpDetails.ResumeLayout(False)
        Me.grpDetails.PerformLayout()
        Me.grpOptions.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents spdSafe As FarPoint.Win.Spread.FpSpread
    Friend WithEvents stsStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents lblSealMessage As System.Windows.Forms.Label
    Private WithEvents txtComments As System.Windows.Forms.TextBox
    Private WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbCashierTill As System.Windows.Forms.ComboBox
    Friend WithEvents grpOptions As System.Windows.Forms.GroupBox
    Friend WithEvents rdoIssue As System.Windows.Forms.RadioButton
    Friend WithEvents rdoReturn As System.Windows.Forms.RadioButton
    Friend WithEvents rdoReseal As System.Windows.Forms.RadioButton
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
End Class
