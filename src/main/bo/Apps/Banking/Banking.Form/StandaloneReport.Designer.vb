﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StandaloneReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdSafe = New FarPoint.Win.Spread.FpSpread
        Me.btnPrint = New System.Windows.Forms.Button
        Me.grpDate = New System.Windows.Forms.GroupBox
        Me.cmbDate = New System.Windows.Forms.ComboBox
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDate.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(405, 671)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdSafe
        '
        Me.spdSafe.About = "3.0.2004.2005"
        Me.spdSafe.AccessibleDescription = ""
        Me.spdSafe.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdSafe.Location = New System.Drawing.Point(6, 61)
        Me.spdSafe.Name = "spdSafe"
        Me.spdSafe.Size = New System.Drawing.Size(475, 604)
        Me.spdSafe.TabIndex = 1
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdSafe.TextTipAppearance = TipAppearance1
        Me.spdSafe.ActiveSheetIndex = -1
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(323, 671)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'grpDate
        '
        Me.grpDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDate.Controls.Add(Me.cmbDate)
        Me.grpDate.Location = New System.Drawing.Point(6, 8)
        Me.grpDate.Name = "grpDate"
        Me.grpDate.Size = New System.Drawing.Size(237, 47)
        Me.grpDate.TabIndex = 30
        Me.grpDate.TabStop = False
        Me.grpDate.Text = "PickUp Date"
        '
        'cmbDate
        '
        Me.cmbDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDate.FormatString = "d"
        Me.cmbDate.FormattingEnabled = True
        Me.cmbDate.Location = New System.Drawing.Point(9, 15)
        Me.cmbDate.Name = "cmbDate"
        Me.cmbDate.Size = New System.Drawing.Size(222, 21)
        Me.cmbDate.TabIndex = 0
        '
        'StandaloneReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(487, 716)
        Me.Controls.Add(Me.grpDate)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.spdSafe)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "StandaloneReport"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Standalone Variance Report"
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDate.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents spdSafe As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents grpDate As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDate As System.Windows.Forms.ComboBox
End Class
