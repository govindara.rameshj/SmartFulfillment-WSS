﻿Imports System
Imports System.Windows.Forms
Imports FarPoint.Win.Spread
Imports Microsoft.VisualBasic
Imports Farpoint.Win
Imports Farpoint.PlusMinusCellType

Public Class PickupFloat
    Private Enum col
        Expand
        Denom
        Float
    End Enum
    Private _floatBanking As Banking.Core.FloatBanking = Nothing
    Private _originalCellValue As Decimal

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal accountType As String)
        InitializeComponent()
        _floatBanking = New Banking.Core.FloatBanking(user1, user2, Core.FloatBanking.Types.Float, accountType)
    End Sub

    Public ReadOnly Property Float() As Banking.Core.FloatBanking
        Get
            Return _floatBanking
        End Get
    End Property

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor
            spdSafe_Initialise()
            spdSafe_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub



    Private Sub spdSafe_Initialise()

        'Input maps
        Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)

        Dim sheet As New SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.Transparent
        sheet.SelectionUnit = Model.SelectionUnit.Cell

        'Column types
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        'Column Headers
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center

        'Columns
        sheet.Columns(col.Expand).Label = " "
        sheet.Columns(col.Expand).Width = 20
        sheet.Columns(col.Expand).Resizable = False
        sheet.Columns(col.Denom).Label = " "
        sheet.Columns(col.Float).CellType = typeNumber
        sheet.Columns(col.Float).Locked = False
        sheet.Columns(col.Float).Label = "Float"

        spdSafe.Sheets.Add(sheet)

    End Sub

    Private Sub spdSafe_AddRows()

        Dim sheet As SheetView = spdSafe.ActiveSheet

        For Each currency As Oasys.Currency.Currency In Oasys.Currency.GetCurrencies
            'Add currency header
            Dim rowHeader As Integer = sheet.RowCount
            Dim currencyId As String = currency.Id
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

            'add expand button
            Dim btn As PlusMinusCellType = Nothing
            Select Case currency.IsDefault
                Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
            End Select

            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
            sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
            sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
            sheet.Cells(sheet.RowCount - 1, col.Expand).VerticalAlignment = Spread.CellVerticalAlignment.Center
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash (" & currency.Symbol & ")"

            'add all denominations
            For Each denom As Oasys.Currency.CurrencyDenomination In currency.Denominations
                Dim denominationId As Decimal = denom.Id
                Dim tenderId As Integer = denom.TenderId
                'skip non-cash tenders for float bags
                If _floatBanking.Type = Banking.Core.BagTypes.Float AndAlso tenderId <> 1 Then
                    Continue For
                End If

                'add row
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = denom
                sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
                sheet.Cells(sheet.RowCount - 1, col.Float).Value = 0
            Next

            'Insert totals
            sheet.Cells(rowHeader, col.Float).Formula = "SUM(R" & rowHeader + 2 & "C:R" & sheet.RowCount & "C)"
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        Next

        'set actice cell and focus
        sheet.SetActiveCell(1, col.Float)
        spd_Resize(spdSafe, New EventArgs)
        spdSafe.Focus()

    End Sub

    Private Sub spdSafe_EditModeOn(ByVal sender As Object, ByVal e As System.EventArgs) Handles spdSafe.EditModeOn

        If spdSafe.ActiveSheet.ActiveColumnIndex = col.Float Then
            btnAccept.Enabled = False
            _originalCellValue = spdSafe.ActiveSheet.ActiveCell.Value
        End If

    End Sub

    Private Sub spdSafe_LeaveCell(ByVal sender As Object, ByVal e As Farpoint.Win.Spread.LeaveCellEventArgs) Handles spdSafe.LeaveCell

        Dim sheet As SheetView = spdSafe.ActiveSheet

        Try
            If sheet.Rows(e.Row).Tag Is Nothing Then Exit Sub
            Dim denom As Oasys.Currency.CurrencyDenomination = sheet.Rows(e.Row).Tag
            Dim cell As Cell = sheet.ActiveCell

            Select Case sheet.ActiveColumnIndex
                Case col.Float
                    If cell.Value = 0 Then Exit Select
                    btnAccept.Enabled = False

                    'Check that entered value is a multiple of denomination
                    If Not denom.IsMultiple(cell.Value) Then
                        MessageBox.Show(My.Resources.Errors.WarnQtyMultipleDenom, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        sheet.SetValue(sheet.ActiveRowIndex, sheet.ActiveColumnIndex, _originalCellValue)
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Check that value is positive for cash tenders
                    If cell.Value < 0 AndAlso denom.TenderId = 1 Then
                        MessageBox.Show(My.Resources.Errors.WarnQtyCannotBeNegative, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        sheet.SetValue(sheet.ActiveRowIndex, sheet.ActiveColumnIndex, _originalCellValue)
                        e.Cancel = True
                        Exit Sub
                    End If
            End Select

        Finally
            sheet.ShowNotes(True)
        End Try

    End Sub

    Private Sub spdSafe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdSafe.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdSafe.ActiveSheet
            spdSafe_LeaveCell(sender, New LeaveCellEventArgs(New SpreadView(spdSafe), sheet.ActiveRowIndex, sheet.ActiveColumnIndex, sheet.ActiveRowIndex, sheet.ActiveColumnIndex))
        End If

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check that are sheets available
        If spread.Sheets.Count = 0 Then Exit Sub

        'get tab strip and border style sizes
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim spreadWidth As Single = spread.Width
        Dim spreadHeight As Single = spread.Height

        'make allowance for tab strip and border
        If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            spreadHeight -= (borderHeight * 2)
            spreadWidth -= (borderWidth * 2)
        End If


        For Each sheet As SheetView In spread.Sheets
            'check sheet has any rows first
            If sheet.RowCount = 0 Then Continue For

            Dim sheetHeight As Single = spreadHeight
            Dim sheetWidth As Single = spreadWidth

            'Get row header widths
            If sheet.Visible = False Then Continue For
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                Next
            End If

            'Get column header heights
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                Next
            End If

            'get rowsHeight and colsWidth
            Dim rowsHeight As Integer = sheet.RowCount * sheet.Rows.Default.Height
            Dim colsWidth As Integer = 0
            For Each col As Column In sheet.Columns
                If col.Visible Then colsWidth += col.Width
            Next

            'check whether need scrollbars
            If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

            If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'check whether need horizontal scrollbar
            If colsTotal > sheetWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                'if resizing then fill up empty space
                If colsResizable = 0 Then Continue For
                Dim increase As Integer = (sheetWidth - colsTotal) / colsResizable
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If

        Next

    End Sub



    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress

        lblSealMessage.Text = ""
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSeal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.TextChanged
        btnAccept.Enabled = False
    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave

        'check that cancel button has not been clicked
        If Me.ActiveControl Is btnExit Then Exit Sub

        'check that seal number has been entered
        lblSealMessage.Text = ""

        If Not txtSeal.MaskFull Then
            lblSealMessage.Text = My.Resources.Messages.SealMustBeEntered
            lblSealMessage.Visible = True
            txtSeal.Focus()
            btnAccept.Enabled = False
            Exit Sub
        End If

        'check that seal number is valid
        If Banking.Core.IsSealNumberValid(txtSeal.Text) Then
            _floatBanking.SetSealNumber(txtSeal.Text)
        Else
            lblSealMessage.Text = My.Resources.Messages.SealInUse
            lblSealMessage.Visible = True
            txtSeal.SelectAll()
            txtSeal.Focus()
            btnAccept.Enabled = False
            Exit Sub
        End If

        CheckAccept()

    End Sub

    Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtComments_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComments.Leave
        _floatBanking.Comments = txtComments.Text.Trim
    End Sub

    Private Sub CheckAccept()

        'check that the form has been loaded
        If Not Me.Visible Then Exit Sub

        'check whether to make seal text box enabled
        txtSeal.Enabled = True

        'check if seal number empty (and enabled) and disable accept button if so
        If Not txtSeal.MaskFull Then
            btnAccept.Enabled = False
            Exit Sub
        End If

        'check if there is a warning message visible
        If lblSealMessage.Text.Length > 0 Then
            btnAccept.Enabled = False
            Exit Sub
        End If

        btnAccept.Enabled = True

    End Sub



    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        'set values from sheet to pickup new bag
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            'check this is a denomination  row
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For

            'check for non zero amount and to float if true
            Dim actual As Decimal = sheet.Cells(rowIndex, col.Float).Value
            If actual <> 0 Then
                Dim denom As Oasys.Currency.CurrencyDenomination = sheet.Rows(rowIndex).Tag
                _floatBanking.SetDenomination(denom.CurrencyId, denom.Id, denom.TenderId, actual, "")
            End If
        Next

        'set comments and save
        _floatBanking.Comments = txtComments.Text.Trim

        DialogResult = Windows.Forms.DialogResult.OK
        Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub


End Class