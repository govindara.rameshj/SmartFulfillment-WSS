﻿Imports System
Imports System.Windows.Forms
Imports FarPoint.Win.Spread
Imports Microsoft.VisualBasic
Imports FarPoint.Win
Imports FarPoint.PlusMinusCellType
Imports System.Diagnostics
Imports System.Data
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System

Public Class FloatForm
    Private Enum col
        Expand
        Denom
        System
        Suggested
        Original
        Bag
        SafeBalance
        SlipNumber
    End Enum
    Private _oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _floatBanking As Banking.Core.FloatBanking = Nothing
    Private _storeIdName As String = String.Empty
    Private _originalCellValue As Decimal

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal accountType As String, ByVal storeIdName As String)
        InitializeComponent()
        _storeIdName = storeIdName
        _floatBanking = New Banking.Core.FloatBanking(user1, user2, Core.FloatBanking.Types.Float, accountType)
        rdoIssue.Visible = True
        Select Case _floatBanking.AccountType
            Case Accountabilities.Cashier : rdoIssue.Text = My.Resources.Messages.SelectCashier
            Case Accountabilities.Till : rdoIssue.Text = My.Resources.Messages.SelectTill
        End Select
    End Sub


    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor

            cmbCashierTill_Initialise()
            spdSafe_Initialise()
            spdSafe_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub DisplayStatus(Optional ByVal text As String = Nothing, Optional ByVal warn As Boolean = False)

        lblStatus.BackColor = Drawing.SystemColors.Control
        If text = Nothing Then
            lblStatus.Text = ""
            lblStatus.ToolTipText = ""
        Else
            If warn Then lblStatus.BackColor = Drawing.Color.Yellow
            lblStatus.Text = text
            lblStatus.ToolTipText = text
        End If
        stsStatus.Refresh()

    End Sub


    Public Sub LoadOriginalBag(ByVal id As Integer)

        _floatBanking.LoadOriginalBag(id)

        'reduce spread sheet height and sow options group
        spdSafe.Height -= grpOptions.Height
        spdSafe.Height -= grpOptions.Margin.Top
        spdSafe.Height -= grpOptions.Margin.Bottom
        grpOptions.Visible = True
        rdoReseal.Checked = True

        Me.Text = My.Resources.Controls.F6CheckBag.Substring(3) & " Seal Number: " & _floatBanking.OldSealNumber
        txtComments.Text = _floatBanking.Comments

    End Sub



    Private Sub spdSafe_Initialise()

        Try
            'Input maps
            Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
            im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
            imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)

            Dim sheet As New SheetView
            sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
            sheet.RowCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
            sheet.ColumnHeader.RowCount = 1
            sheet.RowHeader.ColumnCount = 0

            sheet.GrayAreaBackColor = Drawing.Color.White
            sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
            sheet.DefaultStyle.CellType = New CellType.TextCellType
            sheet.DefaultStyle.Locked = True
            sheet.SelectionBackColor = Drawing.Color.Transparent
            sheet.SelectionUnit = Model.SelectionUnit.Cell

            'Print info.
            sheet.PrintInfo.Header = "/lStore: " & _storeIdName
            sheet.PrintInfo.Header &= "/c/fz""14""Banking Report"
            sheet.PrintInfo.Header &= "/n/c/fz""10""Data for: " & _floatBanking.LastAmended.ToShortDateString
            sheet.PrintInfo.Header &= "/n "
            sheet.PrintInfo.Footer = "/lPrinted: " & Now
            sheet.PrintInfo.Footer &= "/cPage /p of /pc"
            sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            sheet.PrintInfo.Margin.Left = 50
            sheet.PrintInfo.Margin.Right = 50
            sheet.PrintInfo.Margin.Top = 20
            sheet.PrintInfo.Margin.Bottom = 20
            sheet.PrintInfo.ShowBorder = False
            sheet.PrintInfo.ShowPrintDialog = True
            sheet.PrintInfo.UseSmartPrint = True
            sheet.PrintInfo.Orientation = PrintOrientation.Portrait

            'Column types
            Dim typeNumber As New CellType.NumberCellType
            typeNumber.DecimalPlaces = 2
            typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
            typeNumber.NegativeRed = True

            'Column Headers
            sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center

            'Columns
            sheet.Columns(col.Expand).Label = " "
            sheet.Columns(col.Expand).Width = 20
            sheet.Columns(col.Expand).Resizable = False
            sheet.Columns(col.Denom).Label = " "
            sheet.Columns(col.System, col.SafeBalance).CellType = typeNumber
            sheet.Columns(col.System).Label = "System"
            sheet.Columns(col.Original).Label = "Original"
            sheet.Columns(col.Original).Visible = (Not _floatBanking.IsOldBagNothing)
            sheet.Columns(col.Bag).Locked = False
            sheet.Columns(col.Suggested).Label = "Suggested"
            sheet.Columns(col.SafeBalance).Label = "Safe Balance"
            sheet.Columns(col.SlipNumber).Label = "Slip Number"
            sheet.Columns(col.SlipNumber).Locked = False
            sheet.Columns(col.SlipNumber).HorizontalAlignment = CellHorizontalAlignment.Right

            'Hide columns dependent on bag type
            Select Case _floatBanking.Type
                Case Banking.Core.BagTypes.Float
                    sheet.Columns(col.Bag).Label = "Float"
                    sheet.Columns(col.Suggested).Visible = False
                    sheet.Columns(col.SlipNumber).Visible = False

                Case Banking.Core.BagTypes.Banking
                    sheet.Columns(col.Bag).Label = "To Bank"
            End Select

            spdSafe.Sheets.Add(sheet)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdSafe_AddRows()

        Try
            Dim sheet As SheetView = spdSafe.ActiveSheet

            Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
            For Each currency As Currency.Header In currencies
                'Add currency header
                Dim rowHeader As Integer = sheet.RowCount
                Dim currencyId As String = currency.Id
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = Nothing
                sheet.Rows(sheet.RowCount - 1).Locked = True
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

                'add expand button
                Dim btn As PlusMinusCellType = Nothing
                Select Case currency.IsDefault
                    Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                    Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
                End Select

                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
                sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
                sheet.Cells(sheet.RowCount - 1, col.Expand).VerticalAlignment = Spread.CellVerticalAlignment.Center
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash (" & currency.Symbol & ")"

                'add all denominations
                For Each denom As Currency.Line In currency.Lines
                    Dim denominationId As Decimal = denom.Id
                    Dim tenderId As Integer = denom.TenderId
                    'skip non-cash tenders for float bags
                    If _floatBanking.Type = Banking.Core.BagTypes.Float AndAlso tenderId <> 1 Then
                        Continue For
                    End If

                    'add row
                    sheet.Rows.Add(sheet.RowCount, 1)
                    sheet.Rows(sheet.RowCount - 1).Tag = denom
                    sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                    sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
                    sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
                    sheet.Cells(sheet.RowCount - 1, col.System).Value = _floatBanking.SafeSystem(currencyId, denominationId, tenderId)

                    'if banking and bullion then lock row
                    If _floatBanking.Type = Banking.Core.BagTypes.Banking And denom.BullionMultiple = 0 Then
                        sheet.Cells(sheet.RowCount - 1, col.Suggested).Value = 0
                        sheet.Rows(sheet.RowCount - 1).Locked = True
                        sheet.Rows(sheet.RowCount - 1).ForeColor = Drawing.Color.Gray
                    Else
                        sheet.Cells(sheet.RowCount - 1, col.Suggested).Value = _floatBanking.SafeSuggested(currencyId, denominationId, tenderId)
                    End If

                    Select Case True
                        Case _floatBanking.IsOldBagNothing
                            sheet.Cells(sheet.RowCount - 1, col.Original).Value = _floatBanking.NewValue(currencyId, denominationId, tenderId)
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _floatBanking.NewValue(currencyId, denominationId, tenderId)
                            sheet.Cells(sheet.RowCount - 1, col.SlipNumber).Value = _floatBanking.NewSlipNumber(currencyId, denominationId, tenderId)

                        Case Else
                            sheet.Cells(sheet.RowCount - 1, col.Original).Value = _floatBanking.OldValue(currencyId, denominationId, tenderId)
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _floatBanking.OldValue(currencyId, denominationId, tenderId)
                            sheet.Cells(sheet.RowCount - 1, col.SlipNumber).Value = _floatBanking.OldSlipNumber(currencyId, denominationId, tenderId)
                    End Select

                    sheet.Cells(sheet.RowCount - 1, col.SafeBalance).Formula = "RC[-4] - RC[-1] + RC[-2]"
                Next

                'Insert totals
                sheet.Cells(rowHeader, col.System, rowHeader, col.SafeBalance).Formula = "SUM(R" & rowHeader + 2 & "C:R" & sheet.RowCount & "C)"
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            Next

            'set actice cell and focus
            sheet.SetActiveCell(1, col.Bag)
            spd_Resize(spdSafe, New EventArgs)
            spdSafe.Focus()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdSafe_EditModeOn(ByVal sender As Object, ByVal e As System.EventArgs) Handles spdSafe.EditModeOn

        If spdSafe.ActiveSheet.ActiveColumnIndex = col.Bag Then
            btnAccept.Enabled = False
            _originalCellValue = CDec(spdSafe.ActiveSheet.ActiveCell.Value)
        End If

    End Sub

    Private Sub spdSafe_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdSafe.LeaveCell

        Dim sheet As SheetView = spdSafe.ActiveSheet

        Try
            DisplayStatus()

            If sheet.Rows(e.Row).Tag Is Nothing Then Exit Sub
            Dim denom As Currency.Line = CType(sheet.Rows(e.Row).Tag, Currency.Line)
            Dim cell As Cell = sheet.ActiveCell

            Select Case sheet.ActiveColumnIndex
                Case col.Bag
                    If CDec(cell.Value) = 0 Then Exit Select
                    btnAccept.Enabled = False

                    'Check that entered value is a multiple of denomination
                    If Not denom.IsMultiple(CDec(cell.Value)) Then
                        DisplayStatus(My.Resources.Errors.WarnQtyMultipleDenom, True)
                        sheet.SetValue(sheet.ActiveRowIndex, sheet.ActiveColumnIndex, _originalCellValue)
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Check that value is positive for cash tenders
                    If CDec(cell.Value) < 0 AndAlso denom.TenderId = 1 Then
                        DisplayStatus(My.Resources.Errors.WarnQtyCannotBeNegative, True)
                        sheet.SetValue(sheet.ActiveRowIndex, sheet.ActiveColumnIndex, _originalCellValue)
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Do banking bag checks
                    If _floatBanking.Type = Banking.Core.BagTypes.Banking Then
                        'check that multiple of bullion
                        If denom.BullionMultiple > 0 And Math.Round(CDec(cell.Value) Mod denom.BullionMultiple, 1) <> 0 Then
                            DisplayStatus(My.Resources.Errors.WarnQtyMultipleBullion & FormatNumber(denom.BullionMultiple, 2), True)
                            e.Cancel = True
                            Exit Sub
                        End If

                        'banking limit check
                        If CDec(sheet.Cells(sheet.ActiveRowIndex, col.Bag).Value) > denom.BankingBagLimit Then
                            DisplayStatus(My.Resources.Errors.WarnGreaterBagLimit & FormatNumber(denom.BankingBagLimit, 2), True)
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If

                Case col.SlipNumber
                    'Copy entered slip number for all tenders on sheet if slip not empty
                    Dim slip As String = sheet.ActiveCell.Value.ToString
                    If slip <> String.Empty Then
                        For rowIndex As Integer = 0 To (sheet.RowCount - 1)
                            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
                            Dim rowDenom As Currency.Line = CType(sheet.Rows(rowIndex).Tag, Currency.Line)
                            If rowDenom.TenderId = denom.TenderId Then sheet.Cells(rowIndex, col.SlipNumber).Value = slip
                        Next
                    End If
            End Select

            'do check for accept button if resealed
            Select Case True
                Case rdoIssue.Checked Or rdoReturn.Checked : btnAccept.Enabled = True
                Case Else : CheckAccept()
            End Select

        Finally
            sheet.ShowNotes(True)
        End Try

    End Sub

    Private Sub spdSafe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdSafe.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdSafe.ActiveSheet
            spdSafe_LeaveCell(sender, New LeaveCellEventArgs(New SpreadView(spdSafe), sheet.ActiveRowIndex, sheet.ActiveColumnIndex, sheet.ActiveRowIndex, sheet.ActiveColumnIndex))
        End If

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Try
            Dim spread As FpSpread = CType(sender, FpSpread)
            spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
            spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

            'check that are sheets available
            If spread.Sheets.Count = 0 Then Exit Sub

            'get tab strip and border style sizes
            Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
            Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
            Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
            Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
            Dim spreadWidth As Single = spread.Width
            Dim spreadHeight As Single = spread.Height

            'make allowance for tab strip and border
            If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
            If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
                spreadHeight -= (borderHeight * 2)
                spreadWidth -= (borderWidth * 2)
            End If


            For Each sheet As SheetView In spread.Sheets
                'check sheet has any rows first
                If sheet.RowCount = 0 Then Continue For

                Dim sheetHeight As Single = spreadHeight
                Dim sheetWidth As Single = spreadWidth

                'Get row header widths
                If sheet.Visible = False Then Continue For
                If sheet.RowHeaderVisible Then
                    For Each header As Column In sheet.RowHeader.Columns
                        If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                    Next
                End If

                'Get column header heights
                If sheet.ColumnHeaderVisible Then
                    For Each header As Row In sheet.ColumnHeader.Rows
                        If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                    Next
                End If

                'get rowsHeight and colsWidth
                Dim rowsHeight As Single = sheet.RowCount * sheet.Rows.Default.Height
                Dim colsWidth As Single = 0
                For Each col As Column In sheet.Columns
                    If col.Visible Then colsWidth += col.Width
                Next

                'check whether need scrollbars
                If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

                If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


                'get columns to resize and new widths
                Dim colsTotal As Single = 0
                Dim colsResizable As Integer = 0
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then
                        c.Width = c.GetPreferredWidth
                        colsResizable += 1
                    End If
                    If c.Visible Then colsTotal += c.Width
                Next

                'check whether need horizontal scrollbar
                If colsTotal > sheetWidth Then
                    spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                Else
                    'if resizing then fill up empty space
                    If colsResizable = 0 Then Continue For
                    Dim increase As Integer = CInt((sheetWidth - colsTotal) / colsResizable)
                    For Each c As Column In sheet.Columns
                        If c.Visible And c.Resizable Then c.Width += increase
                    Next
                End If

            Next

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.FindForm.Text)
        End Try

    End Sub



    Private Sub cmbCashierTill_Initialise()

        Select Case _floatBanking.AccountType
            Case Accountabilities.Cashier
                'Dim users As User.UserCollection = User.GetUsers
                'cmbCashierTill.ValueMember = GetPropertyName(Function(f As User.User) f.Id)
                'cmbCashierTill.DisplayMember = GetPropertyName(Function(f As User.User) f.IdName)
                'cmbCashierTill.DataSource = users

                Dim users As New Oasys.Security.Users
                Dim dt As DataTable = users.GetSystemUsersDataTable(True)
                cmbCashierTill.ValueMember = Oasys.Security.Users.Column.Id.ToString
                cmbCashierTill.DisplayMember = Oasys.Security.Users.Column.Display.ToString
                cmbCashierTill.DataSource = dt

            Case Accountabilities.Till
                Dim workstations As Workstation.WorkstationCollection = Workstation.GetWorkstations
                cmbCashierTill.ValueMember = GetPropertyName(Function(f As Workstation.Workstation) f.Id)
                cmbCashierTill.DisplayMember = GetPropertyName(Function(f As Workstation.Workstation) f.IdDescription)
                cmbCashierTill.DataSource = workstations

                'Dim workstations As New Oasys.Security.Workstations
                'Dim dt As DataTable = workstations.GetWorkstationsDataTable(True)
                'cmbCashierTill.ValueMember = Oasys.Security.Workstations.Column.Id.ToString
                'cmbCashierTill.DisplayMember = Oasys.Security.Workstations.Column.Display.ToString
                'cmbCashierTill.DataSource = dt
        End Select

    End Sub

    Private Sub cmbCashierTill_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbCashierTill.KeyDown

        e.Handled = False
        Select Case e.KeyData
            Case Keys.Down, Keys.Up
                If cmbCashierTill.DroppedDown = False Then
                    cmbCashierTill.DroppedDown = True
                    e.Handled = True
                End If
        End Select

    End Sub

    Private Sub cmbCashierTill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCashierTill.SelectedIndexChanged

        Select Case True
            Case cmbCashierTill.Visible = False
            Case cmbCashierTill.Visible AndAlso cmbCashierTill.SelectedIndex = 0 : btnAccept.Enabled = False
            Case Else : btnAccept.Enabled = True
        End Select

    End Sub

    Private Sub cmbCashierTill_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbCashierTill.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        End If

    End Sub



    Private Sub rdo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIssue.CheckedChanged, rdoReseal.CheckedChanged, rdoReturn.CheckedChanged

        cmbCashierTill.Visible = rdoIssue.Checked

        Select Case True
            Case rdoReseal.Checked
                _floatBanking.SetBagState(Banking.Core.BagStates.Sealed)
                CheckAccept()

            Case rdoIssue.Checked
                _floatBanking.SetBagState(Banking.Core.BagStates.Released)
                lblSealMessage.Text = ""
                txtSeal.Clear()
                txtSeal.Enabled = False
                cmbCashierTill.SelectedIndex = 0
                btnAccept.Enabled = False

            Case rdoReturn.Checked
                _floatBanking.SetBagState(Banking.Core.BagStates.BackToSafe)
                lblSealMessage.Text = ""
                txtSeal.Clear()
                txtSeal.Enabled = False
                btnAccept.Enabled = True

            Case Else : Exit Sub
        End Select

    End Sub

    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress

        lblSealMessage.Text = ""
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSeal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.TextChanged
        btnAccept.Enabled = False
    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave

        'check that cancel button has not been clicked
        If Me.ActiveControl Is btnExit Then Exit Sub

        'check that seal number has been entered
        lblSealMessage.Text = ""

        If Not txtSeal.MaskFull Then
            lblSealMessage.Text = My.Resources.Messages.SealMustBeEntered
            lblSealMessage.Visible = True
            txtSeal.Focus()
            btnAccept.Enabled = False
            Exit Sub
        End If

        'check that seal number is valid
        If Banking.Core.IsSealNumberValid(txtSeal.Text) Then
            _floatBanking.SetSealNumber(txtSeal.Text)
        Else
            lblSealMessage.Text = My.Resources.Messages.SealInUse
            lblSealMessage.Visible = True
            txtSeal.SelectAll()
            txtSeal.Focus()
            btnAccept.Enabled = False
            Exit Sub
        End If

        'check that all slip numbers entered (banking only)
        Dim rowIndex As Integer = 1
        If _floatBanking.Type = Banking.Core.BagTypes.Banking AndAlso Not BagSlipsEntered(rowIndex) Then
            lblSealMessage.Text = My.Resources.Messages.SlipsMustBeEntered
            spdSafe.ActiveSheet.SetActiveCell(rowIndex, col.SlipNumber)
            spdSafe.Focus()
            btnAccept.Enabled = False
            Exit Sub
        End If

        CheckAccept()

    End Sub

    Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtComments_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComments.Leave
        _floatBanking.Comments = txtComments.Text.Trim
    End Sub



    Private Function BagValuesChanged() As Boolean

        'check if any changes made to bag
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
            If CDec(sheet.Cells(rowIndex, col.Original).Value) <> CDec(sheet.Cells(rowIndex, col.Bag).Value) Then
                Return True
            End If
        Next

        Return False

    End Function

    Private Function BagSlipsEntered(Optional ByRef firstRowWithNoSlip As Integer = 0) As Boolean

        'check if slip numbers entered for cash/cheques only
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For

            Dim denom As Currency.Line = CType(sheet.Rows(rowIndex).Tag, Currency.Line)
            If denom.TenderId > 2 Then Continue For

            If CDec(sheet.Cells(rowIndex, col.Bag).Value) <> 0 Then
                If sheet.Cells(rowIndex, col.SlipNumber).Value.ToString.Length = 0 Then
                    firstRowWithNoSlip = rowIndex
                    Return False
                End If
            End If
        Next

        Return True

    End Function

    Private Sub CheckAccept()

        'check that the form has been loaded
        If Not Me.Visible Then Exit Sub

        'check whether to make seal text box enabled
        txtSeal.Enabled = True

        'check if seal number empty (and enabled) and disable accept button if so
        If Not txtSeal.MaskFull Then
            btnAccept.Enabled = False
            Exit Sub
        End If

        'check slip numbers (if banking)
        If _floatBanking.Type = Banking.Core.BagTypes.Banking Then
            If Not BagSlipsEntered() Then
                btnAccept.Enabled = False
                Exit Sub
            Else
                If lblSealMessage.Text = My.Resources.Messages.SlipsMustBeEntered Then
                    lblSealMessage.Text = ""
                End If
            End If
        End If

        'check if there is a warning message visible
        If lblSealMessage.Text.Length > 0 Then
            btnAccept.Enabled = False
            Exit Sub
        End If

        btnAccept.Enabled = True

    End Sub



    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        Try
            DisplayStatus(My.Resources.Messages.BagSave)
            Cursor = Cursors.WaitCursor

            'set values from sheet to pickup new bag
            Dim sheet As SheetView = spdSafe.ActiveSheet
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                'check this is a denomination  row
                If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For

                'check for non zero amount and to float if true
                Dim actual As Decimal = CDec(sheet.Cells(rowIndex, col.Bag).Value)
                If actual <> 0 Then
                    Dim denom As Currency.Line = CType(sheet.Rows(rowIndex).Tag, Currency.Line)
                    Dim slip As String = sheet.Cells(rowIndex, col.SlipNumber).Value.ToString
                    _floatBanking.SetDenomination(denom.CurrencyId, denom.Id, denom.TenderId, actual, slip)
                End If
            Next

            'check if need to set account id
            If cmbCashierTill.Visible AndAlso cmbCashierTill.SelectedIndex > 0 Then
                _floatBanking.SetAccountId(CInt(cmbCashierTill.SelectedValue))
            End If

            'set comments and save
            _floatBanking.Comments = txtComments.Text.Trim
            _floatBanking.Save()

            DialogResult = Windows.Forms.DialogResult.OK
            Close()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()

    End Sub


End Class