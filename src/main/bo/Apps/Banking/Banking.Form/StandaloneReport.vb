﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports Farpoint.Win.Spread
Imports Farpoint.Win
Imports Farpoint.PlusMinusCellType
Imports Microsoft.VisualBasic
Imports Banking.Core
Imports Cts.Oasys.Core.System

Public Class StandaloneReport
    Private Enum col
        Expand
        Denom
        Total
        System
        Variance
    End Enum
    Private _bankingPeriod As Banking.Core.BankingPeriod
    Private _pickupReport As Banking.Core.PickupReport
    Private _variances As New List(Of CashierTillVariance)
    Private _commsAllowed As Boolean
    Private _accountType As String = String.Empty
    Private _storeIdName As String = String.Empty

    Public Sub New(ByVal accountType As String, ByVal storeIdName As String)
        InitializeComponent()
        _storeIdName = storeIdName
        _accountType = accountType
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor

            'check if btnComms can be used from parameter 2160
            _commsAllowed = Parameter.GetBoolean(2160)

            'get all safe records
            Dim dt As New DataTable
            Using bank As New Banking.Core.BankingPeriod
                dt = bank.GetSafeDatatable
            End Using

            Dim dv As DataView = New DataView(dt, "", Banking.Core.BankingPeriod.Column.Id.ToString & " desc", DataViewRowState.Added)
            cmbDate.DataSource = dv
            cmbDate.DisplayMember = Banking.Core.BankingPeriod.Column.Display.ToString
            cmbDate.ValueMember = Banking.Core.BankingPeriod.Column.Id.ToString

            cmbDate_SelectionChangeCommitted(Me, New EventArgs)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub



    Private Sub cmbDate_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDate.SelectionChangeCommitted

        'clear all sheets first
        spdSafe.Sheets.Clear()

        'load safe bags for selected period
        _bankingPeriod = New Banking.Core.BankingPeriod
        _bankingPeriod.LoadSafe(CInt(cmbDate.SelectedValue))

        'load cashier/tills and add to report
        _pickupReport = New Banking.Core.PickupReport(_accountType)
        _pickupReport.LoadSystemFigures(CInt(cmbDate.SelectedValue))

        'get variances and date 
        _variances = _pickupReport.GetVariances(CInt(cmbDate.SelectedValue))
        For Each var As CashierTillVariance In _variances
            var.PeriodDate = Period.GetPeriod(var.PeriodId).StartDate
        Next

        spdSafe_LoadSummary()
        spdSafe_LoadCashierTills()

        'resize all sheets
        spdSafe_Resize(spdSafe, New EventArgs)

    End Sub

    Private Sub sheetInitialise(ByRef sheet As SheetView, Optional ByVal accountId As Integer = 0)

        'Column types
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.Transparent
        sheet.SelectionUnit = Model.SelectionUnit.Cell

        sheet.PrintInfo.Header = "/lStore: " & _storeIdName
        sheet.PrintInfo.Header &= "/c/fz""14""Standalone Report"
        sheet.PrintInfo.Header &= "/n/c/fz""10""" & sheet.SheetName
        sheet.PrintInfo.Header &= "/n/c/fz""10""Data for: " & _bankingPeriod.PeriodDate.ToShortDateString
        sheet.PrintInfo.Header &= "/n "
        sheet.PrintInfo.Orientation = PrintOrientation.Portrait
        sheet.PrintInfo.Margin.Left = 50
        sheet.PrintInfo.Margin.Right = 50
        sheet.PrintInfo.Margin.Top = 20
        sheet.PrintInfo.Margin.Bottom = 20
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.UseMax = False
        sheet.PrintInfo.ShowPrintDialog = False

        'Column headers.
        sheet.ColumnHeader.VerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.HorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.DefaultStyle.Font = New Font(FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
        sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Color.Black, 1, False, False, False, True)

        'Columns
        sheet.Columns(col.Expand).Label = " "
        sheet.Columns(col.Expand).Width = 20
        sheet.Columns(col.Expand).Resizable = False
        sheet.Columns(col.Denom).Label = " "
        sheet.Columns(col.Total, col.Variance).CellType = typeNumber
        sheet.Columns(col.Total).Label = "Total"
        sheet.Columns(col.System).Label = "System"
        sheet.Columns(col.Variance).Label = "Variance"

        For Each var As CashierTillVariance In _variances
            'check if column already exists
            Dim columnFound As Boolean = False

            For colIndex As Integer = 0 To sheet.ColumnCount - 1
                If sheet.Columns(colIndex).Tag IsNot Nothing AndAlso CInt(sheet.Columns(colIndex).Tag) = var.PeriodId Then
                    columnFound = True
                    Exit For
                End If
            Next

            If Not columnFound Then
                If accountId > 0 AndAlso var.AccountId = accountId Then
                    sheet.ColumnCount += 1
                    sheet.Columns(sheet.ColumnCount - 1).CellType = typeNumber
                    sheet.Columns(sheet.ColumnCount - 1).Tag = var.PeriodId
                    sheet.Columns(sheet.ColumnCount - 1).Label = var.PeriodDate.ToShortDateString
                End If
            End If
        Next

    End Sub

    Private Sub sheetAddCurrencyHeader(ByRef sheet As SheetView, ByVal currency As Currency.Header)

        'add currency header marking the row tag as nothing as is a header
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

        'add expand button
        Dim btn As PlusMinusCellType = Nothing
        Select Case currency.IsDefault
            Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
            Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
        End Select

        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
        sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Currency (" & currency.Symbol & ")"
        sheet.Cells(sheet.RowCount - 1, col.Denom).Tag = -1

    End Sub

    Private Sub sheetAddCashDenomsWithTotals(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal bags As List(Of BOBanking.cSafeBags), ByVal accountId As Integer)

        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId = 1)
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = denom
            sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
            sheet.Cells(sheet.RowCount - 1, col.System).Value = 0

            'add total values
            For Each bag As BOBanking.cSafeBags In bags
                sheet.Cells(sheet.RowCount - 1, col.Total).Value = CDec(sheet.Cells(sheet.RowCount - 1, col.Total).Value) + bag.Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
            Next

            'if not summary sheet then add bag total to column
            If sheet.ColumnCount > colLength AndAlso bags.Count > 0 Then
                For index As Integer = colLength To sheet.ColumnCount - 1
                    sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
                Next
            End If
        Next

        'insert line border to mark end of cash
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

    End Sub

    Private Sub sheetAddNonCashDenomsWithTotals(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal denom As Currency.Line, ByVal bags As List(Of BOBanking.cSafeBags))

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = denom
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
        sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
        sheet.Cells(sheet.RowCount - 1, col.System).Value = 0
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"

        'add total values
        For Each bag As BOBanking.cSafeBags In bags
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = CDec(sheet.Cells(sheet.RowCount - 1, col.Total).Value) + bag.Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
        Next

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength AndAlso bags.Count > 0 Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
            Next
        End If

    End Sub

    Private Sub sheetAddFloat(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal float As Decimal, ByVal bags As List(Of BOBanking.cSafeBags))

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Float"
        sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
        sheet.Cells(sheet.RowCount - 1, col.System).Value = float
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "ROUND(RC[-2]-RC[-1],2)"

        If bags.Count > 0 Then
            For Each bag As BOBanking.cSafeBags In bags
                sheet.Cells(sheet.RowCount - 1, col.Total).Value = CDec(sheet.Cells(sheet.RowCount - 1, col.Total).Value) + bag.FloatValue.Value
            Next
        End If

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength AndAlso bags.Count > 0 Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).FloatValue.Value
            Next
        End If

    End Sub

    Private Sub sheetAddCashTotal(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal cashFormula As String, ByVal float As Decimal)

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash Total"
        sheet.Cells(sheet.RowCount - 1, col.Total).Formula = cashFormula
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "ROUND(RC[-2]-RC[-1],2)"

        If sheet.ColumnCount > col.Variance Then
            For colIndex As Integer = col.Variance + 1 To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, colIndex).Formula = cashFormula
            Next
        End If

    End Sub

    Private Sub sheetAddCurrencyTotals(ByRef sheet As SheetView, ByVal rowCurrency As Integer, ByVal rowTotals As Integer, ByVal float As Decimal)

        Dim formula As String = "SUM(R" & rowTotals + 1 & "C:R" & sheet.RowCount & "C)"
        sheet.Cells(rowCurrency, col.Total).Formula = formula
        sheet.Cells(rowCurrency, col.System).Formula = formula '& "+" & float
        sheet.Cells(rowCurrency, col.Variance).Formula = "RC[-2]-RC[-1]"

        If sheet.ColumnCount > col.Variance Then
            For colIndex As Integer = col.Variance + 1 To sheet.ColumnCount - 1
                sheet.Cells(rowCurrency, colIndex).Formula = formula
            Next
        End If

        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

    End Sub



    Private Sub spdSafe_LoadSummary()

        'initialise sheet
        Dim sheet As New SheetView
        sheet.SheetName = "Summary"
        sheetInitialise(sheet)

        Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
        For Each currency As Currency.Header In currencies
            Dim rowCurrency As Integer = sheet.RowCount
            sheetAddCurrencyHeader(sheet, currency)
            sheetAddCashDenomsWithTotals(sheet, currency, _pickupReport.PickupBags, Nothing)

            Dim float As Decimal = 0
            Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"

            'insert float for default currency only
            If currency.IsDefault Then
                float = _pickupReport.FloatsIssued(currency.Id)
                sheetAddFloat(sheet, currency, float, _pickupReport.PickupBags)
            End If

            'insert cash total and cash system value (with change)
            Dim rowTotals As Integer = sheet.RowCount
            sheetAddCashTotal(sheet, currency, formula, float)
            sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.CashTendered(currency.Id)

            'add cash variances
            For colIndex As Integer = 0 To sheet.ColumnCount - 1
                If sheet.Columns(colIndex).Tag IsNot Nothing Then
                    Dim periodId As Integer = CInt(sheet.Columns(colIndex).Tag)
                    sheet.Cells(sheet.RowCount - 1, colIndex).Value = _variances.GetAmount(currency.Id, 1, periodId)
                End If
            Next

            'add other tenders now and system value
            For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId <> 1)
                sheetAddNonCashDenomsWithTotals(sheet, currency, denom, _pickupReport.PickupBags)
                sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.NonCashTendered(currency.Id, denom.TenderId)

                'add non cash variances
                For colIndex As Integer = 0 To sheet.ColumnCount - 1
                    If sheet.Columns(colIndex).Tag IsNot Nothing Then
                        Dim periodId As Integer = CInt(sheet.Columns(colIndex).Tag)
                        sheet.Cells(sheet.RowCount - 1, colIndex).Value = _variances.GetAmount(currency.Id, denom.TenderId, periodId)
                    End If
                Next
            Next

            sheetAddCurrencyTotals(sheet, rowCurrency, rowTotals, float)
        Next

        'add sheet to spreadsheet control
        spdSafe.Sheets.Add(sheet)

    End Sub

    Private Sub spdSafe_LoadCashierTills()

        For Each cashTill As Core.CashierTill In _pickupReport.CashierTills
            Dim id As Integer = cashTill.Id
            Dim bags As List(Of BOBanking.cSafeBags) = _pickupReport.PickupBags.FindAll(Function(f) f.AccountabilityID.Value = id)
            Dim sheet As New SheetView
            Dim variances As List(Of CashierTillVariance) = _pickupReport.CashierTills.First(Function(f) f.Id = id).Variances
            sheet.SheetName = _pickupReport.CashierTills.First(Function(f) f.Id = id).Name

            'add bag details
            sheetInitialise(sheet, id)

            Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
            For Each currency As Currency.Header In currencies
                Dim rowCurrency As Integer = sheet.RowCount
                sheetAddCurrencyHeader(sheet, currency)
                sheetAddCashDenomsWithTotals(sheet, currency, bags, id)

                Dim float As Decimal = 0
                Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"

                'insert float for default currency only
                If currency.IsDefault Then
                    float = _pickupReport.FloatsIssued(currency.Id, id)
                    sheetAddFloat(sheet, currency, float, bags)
                End If

                'insert cash total and cash system value (with change)
                Dim rowTotals As Integer = sheet.RowCount
                sheetAddCashTotal(sheet, currency, formula, float)
                sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.CashTendered(currency.Id, id)

                'add cash variances
                For colIndex As Integer = 0 To sheet.ColumnCount - 1
                    If sheet.Columns(colIndex).Tag IsNot Nothing Then
                        Dim periodId As Integer = CInt(sheet.Columns(colIndex).Tag)
                        sheet.Cells(sheet.RowCount - 1, colIndex).Value = _variances.GetAmount(currency.Id, id, 1, periodId)
                    End If
                Next

                'add other tenders now and system value
                For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId <> 1)
                    sheetAddNonCashDenomsWithTotals(sheet, currency, denom, bags)
                    sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.NonCashTendered(currency.Id, denom.TenderId, id)

                    'add non-cash variances
                    For colIndex As Integer = 0 To sheet.ColumnCount - 1
                        If sheet.Columns(colIndex).Tag IsNot Nothing Then
                            Dim periodId As Integer = CInt(sheet.Columns(colIndex).Tag)
                            sheet.Cells(sheet.RowCount - 1, colIndex).Value = _variances.GetAmount(currency.Id, id, denom.TenderId, periodId)
                        End If
                    Next
                Next

                sheetAddCurrencyTotals(sheet, rowCurrency, rowTotals, float)
            Next

            'add sheet to spreadsheet control
            spdSafe.Sheets.Add(sheet)
        Next

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spdSafe_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check that are sheets available
        If spread.Sheets.Count = 0 Then Exit Sub

        'get tab strip and border style sizes
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim spreadWidth As Single = spread.Width
        Dim spreadHeight As Single = spread.Height

        'make allowance for tab strip and border
        If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            spreadHeight -= (borderHeight * 2)
            spreadWidth -= (borderWidth * 2)
        End If


        For Each sheet As SheetView In spread.Sheets
            'check sheet has any rows first
            If sheet.RowCount = 0 Then Continue For

            Dim sheetHeight As Single = spreadHeight
            Dim sheetWidth As Single = spreadWidth

            'Get row header widths
            If sheet.Visible = False Then Continue For
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                Next
            End If

            'Get column header heights
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                Next
            End If

            'get rowsHeight and colsWidth
            Dim rowsHeight As Single = sheet.RowCount * sheet.Rows.Default.Height
            Dim colsWidth As Single = 0
            For Each col As Column In sheet.Columns
                If col.Visible Then colsWidth += col.Width
            Next

            'check whether need scrollbars
            If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

            If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'check whether need horizontal scrollbar
            If colsTotal > sheetWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                'if resizing then fill up empty space

                If colsResizable = 0 Then Continue For
                Dim increase As Integer = CInt((sheetWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If

        Next

    End Sub



    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim dialog As New PrintDialog
        If dialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim nowTime As Date = Now
            Dim numDetails As Integer = spdSafe.Sheets.Count - 1
            Dim type As String = String.Empty

            Select Case _pickupReport.GetAccountType
                Case Accountabilities.Cashier : type = "Cashier"
                Case Accountabilities.Till : type = "Till"
            End Select

            'add printer details and footer to each sheet
            For index As Integer = 0 To numDetails
                spdSafe.Sheets(index).PrintInfo.Printer = dialog.PrinterSettings.PrinterName
                spdSafe.Sheets(index).PrintInfo.Footer = "/lPrinted: " & nowTime

                If index = 0 Then
                    Dim sb As New StringBuilder()
                    sb.Append("/cSummary (" & numDetails & Space(1) & type)
                    If numDetails > 1 Then sb.Append("s")
                    sb.Append(")    Page /p of /pc")
                    spdSafe.Sheets(index).PrintInfo.Footer &= sb.ToString
                Else
                    Dim sb As New StringBuilder()
                    sb.Append("/c" & type & Space(1) & index & " of " & numDetails)
                    sb.Append("    Page /p of /pc")
                    spdSafe.Sheets(index).PrintInfo.Footer &= sb.ToString
                End If

                spdSafe.Sheets(index).PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            Next

            'print sheets individually
            spdSafe.PrintSheet(-1)
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub

End Class