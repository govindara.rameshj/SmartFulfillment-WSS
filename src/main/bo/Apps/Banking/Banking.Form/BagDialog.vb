Imports System
Imports System.Windows.Forms
Imports Microsoft.VisualBasic
Imports Banking.Core

Public Class BagDialog
    Public Enum States
        Add
        Remove
        Reseal
    End Enum
    Private _giftTokenProperty As Banking.Core.GiftTokenProperty
    Private _accountType As String = String.Empty
    Private _State As States = States.Add
    Private _decimalPlaces As Integer = 2
    Private _SealOk As Boolean = False
    Private _ValueOk As Boolean = False

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal bagType As String, ByVal accountType As String)
        InitializeComponent()
        _accountType = accountType

        'instantiate new bag
        _giftTokenProperty = New Banking.Core.GiftTokenProperty(user1, user2, bagType, _accountType)

        'set header text
        Select Case bagType
            Case Banking.Core.BagTypes.Property : Me.Text = My.Resources.Controls.F5AddProperty.Substring(3)
            Case Banking.Core.BagTypes.GiftToken
                Me.Text = My.Resources.Controls.F5AddGiftToken.Substring(3)
                lblValue.Text = My.Resources.Messages.NumberGiftTokens
                _decimalPlaces = 0
        End Select

    End Sub

    Private Sub BagDialog_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyData
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
        End Select

    End Sub

    Public Sub LoadOriginalBag(ByVal id As Integer, ByVal state As States)

        'load original bag
        _giftTokenProperty.LoadOriginalBag(id)
        _State = state

        'set comments, header and value from bag
        txtComments.Text = _giftTokenProperty.GetComments ' _originalbag.Comments.Value
        txtValue.Text = FormatNumber(_giftTokenProperty.GetValue, _decimalPlaces) ' FormatNumber(_originalbag.Value.Value, _decimalPlaces)

        'set enabled status of controls and header text
        Select Case state
            Case States.Remove
                txtSeal.Text = _giftTokenProperty.GetSealNumber
                txtSeal.Enabled = False
                txtValue.Enabled = False
                btnAccept.Enabled = True
                Me.Text = My.Resources.Controls.F7Remove.Substring(3) & " - " & My.Resources.Messages.SealNumber & ": " & txtSeal.Text

            Case States.Reseal
                Me.Text = My.Resources.Controls.F6CheckBag.Substring(3) & " - " & My.Resources.Messages.SealNumber & ": " & _giftTokenProperty.GetSealNumber
        End Select


    End Sub


    Public Function IsBankingAndNotManagerChecked() As Boolean
        Return (_giftTokenProperty.IsBankingAndNotManagerChecked)
    End Function

    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress

        lblSealMessage.Visible = False
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave

        Try
            _SealOk = False

            'check that cancel button has not been clicked
            If Me.ActiveControl Is btnExit Then Exit Sub

            'check that seal number has been entered
            If Not txtSeal.MaskFull Then
                lblSealMessage.Text = My.Resources.Messages.SealMustBeEntered
                lblSealMessage.Visible = True
                txtSeal.Focus()
                Exit Sub
            End If

            'check that seal number is not already in use
            If Not Banking.Core.IsSealNumberValid(txtSeal.Text) Then
                lblSealMessage.Text = My.Resources.Messages.SealInUse
                lblSealMessage.Visible = True
                txtSeal.SelectAll()
                txtSeal.Focus()
                Exit Sub
            End If

            _SealOk = True

        Finally
            CheckAccept()
        End Try

    End Sub

    Private Sub txtValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValue.KeyPress

        lblValueMessage.Visible = False

        'if enter button then go to next control
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

        'allow decimal point, tab and delete and digits
        e.Handled = True
        If e.KeyChar = "."c AndAlso (Not txtValue.Text.Contains(".")) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Tab) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Delete) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Back) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub

    Private Sub txtValue_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.Leave

        Try
            _ValueOk = False

            'check that cancel button has not been clicked
            If Me.ActiveControl Is btnExit Then Exit Sub

            'if txtvalue empty then set to zero
            If txtValue.Text = String.Empty Then txtValue.Text = "0"

            'check that number is numeric and format it true else do not allow leave
            Dim value As Decimal = 0
            If Decimal.TryParse(txtValue.Text, value) Then
                txtValue.Text = FormatNumber(value, _decimalPlaces)
            Else
                txtValue.SelectAll()
                txtValue.Focus()
                Exit Sub
            End If

            'check that value is positive
            If value < 0 Then
                lblValueMessage.Text = My.Resources.Errors.WarnQtyCannotBeNegative
                lblValueMessage.Visible = True
                txtValue.SelectAll()
                txtValue.Focus()
                Exit Sub
            End If

            _ValueOk = True

        Finally
            CheckAccept()
        End Try

    End Sub

    Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub


    Private Sub CheckAccept()

        btnAccept.Enabled = False
        If _SealOk And _ValueOk Then btnAccept.Enabled = True

    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        'set comments, value etc
        _giftTokenProperty.SetComments(txtComments.Text)
        _giftTokenProperty.SetSealNumber(txtSeal.Text)
        _giftTokenProperty.SetValue(CDec(txtValue.Text))

        'save bags
        _giftTokenProperty.Save((_State = States.Remove))

        'close
        DialogResult = Windows.Forms.DialogResult.OK
        Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub


End Class