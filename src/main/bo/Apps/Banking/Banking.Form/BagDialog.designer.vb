<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BagDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.grpBag = New System.Windows.Forms.GroupBox
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        Me.lblValueMessage = New System.Windows.Forms.Label
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.lblSealMessage = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblValue = New System.Windows.Forms.Label
        Me.txtValue = New System.Windows.Forms.TextBox
        Me.btnAccept = New System.Windows.Forms.Button
        Me.grpBag.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(369, 114)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(125, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Tag = ""
        Me.Label1.Text = "&Seal Number"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpBag
        '
        Me.grpBag.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpBag.Controls.Add(Me.txtSeal)
        Me.grpBag.Controls.Add(Me.Label1)
        Me.grpBag.Controls.Add(Me.lblValueMessage)
        Me.grpBag.Controls.Add(Me.txtComments)
        Me.grpBag.Controls.Add(Me.lblSealMessage)
        Me.grpBag.Controls.Add(Me.Label3)
        Me.grpBag.Controls.Add(Me.lblValue)
        Me.grpBag.Controls.Add(Me.txtValue)
        Me.grpBag.Location = New System.Drawing.Point(6, 6)
        Me.grpBag.Name = "grpBag"
        Me.grpBag.Size = New System.Drawing.Size(439, 99)
        Me.grpBag.TabIndex = 0
        Me.grpBag.TabStop = False
        Me.grpBag.Text = "Enter Bag Details"
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(137, 15)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 1
        '
        'lblValueMessage
        '
        Me.lblValueMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblValueMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblValueMessage.ForeColor = System.Drawing.Color.Red
        Me.lblValueMessage.Location = New System.Drawing.Point(232, 42)
        Me.lblValueMessage.Name = "lblValueMessage"
        Me.lblValueMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblValueMessage.Size = New System.Drawing.Size(192, 20)
        Me.lblValueMessage.TabIndex = 5
        Me.lblValueMessage.Tag = ""
        Me.lblValueMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblValueMessage.Visible = False
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(137, 68)
        Me.txtComments.MaxLength = 255
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(290, 20)
        Me.txtComments.TabIndex = 7
        '
        'lblSealMessage
        '
        Me.lblSealMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSealMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblSealMessage.ForeColor = System.Drawing.Color.Red
        Me.lblSealMessage.Location = New System.Drawing.Point(232, 16)
        Me.lblSealMessage.Name = "lblSealMessage"
        Me.lblSealMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSealMessage.Size = New System.Drawing.Size(192, 20)
        Me.lblSealMessage.TabIndex = 2
        Me.lblSealMessage.Tag = ""
        Me.lblSealMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSealMessage.Visible = False
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(125, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Tag = ""
        Me.Label3.Text = "&Comments"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue
        '
        Me.lblValue.BackColor = System.Drawing.Color.Transparent
        Me.lblValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblValue.Location = New System.Drawing.Point(6, 41)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblValue.Size = New System.Drawing.Size(125, 20)
        Me.lblValue.TabIndex = 3
        Me.lblValue.Tag = ""
        Me.lblValue.Text = "&Value"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValue
        '
        Me.txtValue.Location = New System.Drawing.Point(137, 41)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(89, 20)
        Me.txtValue.TabIndex = 4
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Enabled = False
        Me.btnAccept.Location = New System.Drawing.Point(6, 114)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 1
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'BagDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(451, 159)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.grpBag)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "BagDialog"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " "
        Me.grpBag.ResumeLayout(False)
        Me.grpBag.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents btnExit As System.Windows.Forms.Button
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents grpBag As System.Windows.Forms.GroupBox
    Private WithEvents txtComments As System.Windows.Forms.TextBox
    Private WithEvents Label3 As System.Windows.Forms.Label
    Private WithEvents txtValue As System.Windows.Forms.TextBox
    Private WithEvents lblValue As System.Windows.Forms.Label
    Private WithEvents btnAccept As System.Windows.Forms.Button
    Private WithEvents lblSealMessage As System.Windows.Forms.Label
    Private WithEvents lblValueMessage As System.Windows.Forms.Label
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
End Class
