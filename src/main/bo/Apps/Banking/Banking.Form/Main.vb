Imports System
Imports System.Data
Imports System.Drawing
Imports System.Diagnostics
Imports System.Windows.Forms
Imports System.Linq
Imports Microsoft.VisualBasic
Imports Banking
Imports FarPoint.Win.Spread
Imports FarPoint.Win
Imports FarPoint.PlusMinusCellType
Imports System.Collections
Imports System.Text
Imports Banking.Core
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System
Imports Cts.Oasys.WinForm.User


Public Class Main
    Private Class Col
        Public Enum Safe
            Expand
            Denom
            System
            Actual
        End Enum
        Public Enum Print
            Denom
            System
            Main
            Change
            Comments
        End Enum
        Public Enum Bags
            Expand
            Seal
            Actual
            InUser1
            InUser2
            InDate
            OutUser1
            OutUser2
            OutDate
            Comments
            ForTrading
        End Enum
    End Class
    Private _authorisation As Banking.Core.Authorisation = Nothing
    Private _bankingPeriod As Banking.Core.BankingPeriod = Nothing
    Private _storeIdName As String = String.Empty
    Private _accountType As String = String.Empty

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        btnF6.Text = My.Resources.Controls.F6CheckBag
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3 : btnPrintSafe.PerformClick()
            Case Keys.F4 : btnSafe.PerformClick()
            Case Keys.F5 : btnF5.PerformClick()
            Case Keys.F6 : btnF6.PerformClick()
            Case Keys.F7 : btnF7.PerformClick()
            Case Keys.F8 : btnStandalone.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(0, My.Resources.Messages.GetRetailOptions)
            _storeIdName = Store.GetIdName

            DisplayProgress(10, My.Resources.Messages.GetRetailOptions)
            _accountType = SystemOption.GetAccountability

            DisplayProgress(20, My.Resources.Messages.GetBankingPeriods)
            LoadSystemPeriods()

            DisplayProgress(50, My.Resources.Messages.InitSpreads)
            spdSafe_Initialise()

            DisplayProgress(70, My.Resources.Messages.InitSpreads)
            spdPrint_Initialise()

            DisplayProgress(85, My.Resources.Messages.InitSpreads)
            spdBags_Initialise()

            cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)

        Finally
            DisplayProgress(100)
            DisplayProgress()
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub LoadSystemPeriods()
        Dim periods As Period.PeriodCollection = Period.GetAllToDate
        cmbPeriod.DataSource = periods
        cmbPeriod.ValueMember = GetPropertyName(Function(f As Period.Period) f.Id)
        cmbPeriod.DisplayMember = GetPropertyName(Function(f As Period.Period) f.IdStartDate)

        dtpPeriod.Value = Now.Date
        dtpPeriod.MaxDate = Now.Date
        If periods.Count > 0 Then dtpPeriod.MinDate = periods(0).StartDate

        'Dim response As DialogResult = DialogResult.Ignore
        'Dim periods As New Oasys.System.Periods
        'Dim dt As New DataTable

        'Do
        '    dt = periods.GetPeriodsDataTable
        '    If dt.Rows.Count = 0 Then
        '        response = MessageBox.Show("No system periods have been loaded", FindForm.Text, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation)
        '    End If
        'Loop While response = DialogResult.Retry


        'Select Case response
        '    Case DialogResult.Abort
        '        Throw New Exception("No system periods have been loaded")

        '    Case DialogResult.Ignore
        'End Select

    End Sub


    Private Sub cmbPeriod_SelectionChangeCommitted(ByVal sender As Object, ByVal e As EventArgs) Handles cmbPeriod.SelectionChangeCommitted

        If cmbPeriod.SelectedIndex < 0 Then Exit Sub

        Try
            Cursor = Cursors.WaitCursor
            Dim periodId As Integer = CInt(cmbPeriod.SelectedValue)

            ''set picker value to selected date
            'Dim startDate As Date = CDate(CType(cmbPeriod.SelectedItem, DataRowView).Item(Oasys.System.Periods.Column.Start))
            'dtpPeriod.Value = startDate

            DisplayProgress(0, My.Resources.Messages.GetSafeValues)
            _bankingPeriod = New Banking.Core.BankingPeriod
            _bankingPeriod.LoadSafe(periodId, UserId)
            '_safe = New BOBanking.cSafe(_oasys3DB)
            '_safe.Load(periodId, UserId)

            DisplayProgress(25, My.Resources.Messages.AddSafeToSpread)
            spdSafe_Refresh()

            DisplayProgress(50, My.Resources.Messages.AddBagsToSpread)
            spdBags.ActiveSheet.RowCount = 0
            spdBags_AddBags(Core.BagTypes.Float)
            spdBags_AddBags(Core.BagTypes.Pickup)
            spdBags_AddBags(Core.BagTypes.Banking)
            spdBags_AddBags(Core.BagTypes.Property)
            spdBags_AddBags(Core.BagTypes.GiftToken)

            spdBags.ActiveSheet.ColumnHeaderVisible = True
            spd_Resize(spdBags, New EventArgs)

            'set focus to spdBags and select first row
            spdBags.Focus()
            spdBags.ActiveSheet.SetActiveCell(0, Col.Bags.Seal)
            spdBags.ActiveSheet.AddSelection(0, 0, 1, [Enum].GetValues(GetType(Col.Bags)).Length)
            spdBags_Change()

            'only allow button actions for current period (ie index 0)
            If cmbPeriod.SelectedIndex = 0 Then
                btnSafe.Visible = True
                pnlButtons.Visible = True
            Else
                btnSafe.Visible = False
                pnlButtons.Visible = False
            End If

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try

    End Sub

    Private Sub btnPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPeriod.Click

        If dtpPeriod.Visible Then
            dtpPeriod.Focus()
        Else
            dtpPeriod.Visible = True
            dtpPeriod.Focus()
        End If

    End Sub

    Private Sub dtpPeriod_CloseUp(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPeriod.CloseUp

        ''finds system period relating to selected date and sets cmbPeriod to this system period
        'If dtpPeriod.Visible Then
        '    Dim dt As DataTable = CType(cmbPeriod.DataSource, DataTable)
        '    For Each dr As DataRow In dt.Rows
        '        If CDate(dr(Oasys.System.Periods.Column.Start)) <= dtpPeriod.Value And CDate(dr(Oasys.System.Periods.Column.End)) >= dtpPeriod.Value Then
        '            cmbPeriod.SelectedValue = dr(Oasys.System.Periods.Column.Id)
        '            dtpPeriod.Visible = False
        '            cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
        '        End If
        '    Next
        'End If

    End Sub



    Private Sub spdPrint_Initialise()

        'check that spdsafe been done
        If spdSafe.Sheets.Count = 0 Then Exit Sub

        Dim print As New SheetView
        print.PrintInfo.ShowBorder = False
        print.PrintInfo.UseSmartPrint = True
        print.PrintInfo.UseMax = False

        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        'columns
        print.ColumnHeader.RowCount = 1
        print.RowHeader.ColumnCount = 0
        print.ColumnCount = [Enum].GetValues(GetType(Col.Print)).Length
        print.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom

        print.HorizontalGridLine = New GridLine(GridLineType.Flat, Color.Gray)
        print.VerticalGridLine = New GridLine(GridLineType.Flat, Color.Gray)

        print.ColumnHeader.VerticalGridLine = New GridLine(GridLineType.None)
        print.ColumnHeader.HorizontalGridLine = New GridLine(GridLineType.None)
        print.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        print.ColumnHeader.DefaultStyle.Font = New Font(FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
        print.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        print.ColumnHeader.DefaultStyle.Border = New LineBorder(Color.Black, 1, False, False, False, True)

        'print info
        print.PrintInfo.Margin.Left = 50
        print.PrintInfo.Margin.Right = 50
        print.PrintInfo.Margin.Top = 20
        print.PrintInfo.Margin.Bottom = 20
        print.PrintInfo.ShowBorder = False
        print.PrintInfo.ShowPrintDialog = True
        print.PrintInfo.UseSmartPrint = True
        print.PrintInfo.Orientation = PrintOrientation.Portrait
        print.PrintInfo.ShowPrintDialog = True

        print.Columns(Col.Print.Denom).Label = " "
        print.Columns(Col.Print.Denom).Width = 200
        print.Columns(Col.Print.System).CellType = typeNumber
        print.Columns(Col.Print.System).Label = "System"
        print.Columns(Col.Print.Main).Label = "Main Safe"
        print.Columns(Col.Print.Change).Label = "Change Safe"
        print.Columns(Col.Print.Comments).Label = "Comments"
        print.Columns(Col.Print.Comments).Width = 200
        print.Columns(Col.Print.System, Col.Print.Change).Width = 100
        print.Columns(Col.Print.Main, print.ColumnCount - 1).ForeColor = Drawing.Color.Transparent

        spdPrint.Sheets.Add(print)
        spdPrint.Visible = True

    End Sub


    Private Sub spdSafe_Initialise()

        'input maps
        Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)

        Dim sheet As New SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(Col.Safe)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnHeaderVisible = False

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = Spread.CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.Transparent
        sheet.SelectionUnit = Model.SelectionUnit.Cell

        'Column types.
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        'Columns.
        sheet.Columns(Col.Safe.Expand).Label = " "
        sheet.Columns(Col.Safe.Expand).Width = 20
        sheet.Columns(Col.Safe.Expand).Resizable = False
        sheet.Columns(Col.Safe.Denom).Label = " "
        sheet.Columns(Col.Safe.System).Label = "System"
        sheet.Columns(Col.Safe.Actual).Label = "Actual"
        sheet.Columns(Col.Safe.System, Col.Safe.Actual).CellType = typeNumber

        spdSafe.Sheets.Add(sheet)
        AddHandler spdSafe.Resize, AddressOf spd_Resize

    End Sub

    Private Sub spdSafe_Refresh()

        'get sheet, remove any existing rows and add all currencies
        Dim sheet As SheetView = spdSafe.ActiveSheet
        If sheet.RowCount > 0 Then sheet.Rows.Remove(0, sheet.RowCount)

        Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
        For Each currency As Currency.Header In currencies
            'add header
            Dim curID As String = currency.Id
            Dim rowCurrency As Integer = sheet.RowCount
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

            'add expand button and currency text
            Dim btn As PlusMinusCellType
            Select Case currency.IsDefault
                Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                Case Else : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
            End Select
            sheet.Cells(sheet.RowCount - 1, Col.Safe.Expand).Tag = currency.Id
            sheet.Cells(sheet.RowCount - 1, Col.Safe.Expand).CellType = btn
            sheet.Cells(sheet.RowCount - 1, Col.Safe.Expand).Locked = False
            sheet.Cells(sheet.RowCount - 1, Col.Safe.Expand).VerticalAlignment = CellVerticalAlignment.Center
            sheet.Cells(sheet.RowCount - 1, Col.Safe.Denom).Value = "Safe(" & currency.Symbol & ")"

            'add system currency denominations and system and safe bag values.
            Dim totalSystem As Decimal = 0
            Dim totalActual As Decimal = 0
            For Each denom As Currency.Line In currency.Lines
                Dim denID As Decimal = denom.Id
                Dim tenID As Integer = denom.TenderId
                Dim main As Decimal = _bankingPeriod.Safe.Denom(curID, denID, tenID).SafeValue.Value
                Dim change As Decimal = _bankingPeriod.Safe.Denom(curID, denID, tenID).ChangeValue.Value
                Dim system As Decimal = _bankingPeriod.Safe.Denom(curID, denID, tenID).SystemValue.Value
                totalActual += main + change
                totalSystem += system

                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Cells(sheet.RowCount - 1, Col.Safe.Expand).Tag = curID
                sheet.Cells(sheet.RowCount - 1, Col.Safe.Denom).Tag = denID
                sheet.Cells(sheet.RowCount - 1, Col.Safe.Denom).Value = denom.DisplayText
                sheet.Cells(sheet.RowCount - 1, Col.Safe.System).Value = system
                sheet.Cells(sheet.RowCount - 1, Col.Safe.Actual).Value = main + change

                If Not currency.IsDefault Then
                    sheet.Rows(sheet.RowCount - 1).Visible = False
                End If
            Next

            'insert currency totals.
            sheet.Cells(rowCurrency, Col.Safe.System).Value = totalSystem
            sheet.Cells(rowCurrency, Col.Safe.Actual).Value = totalActual
        Next

        'resize if any rows added
        If sheet.RowCount > 0 Then
            sheet.ColumnHeaderVisible = True
            spd_Resize(spdSafe, New EventArgs)
        End If

    End Sub

    Private Sub spdSafe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdSafe.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdSafe.ActiveSheet

            'perform button click if button there for current row
            Dim ct As CellType.ICellType = CType(sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).CellType, CellType.ICellType)
            If TypeOf ct Is PlusMinusCellType Then
                CType(ct, PlusMinusCellType).PerformClick()
            End If

        End If

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.Cells(sheet.ActiveRowIndex, Col.Safe.Expand).Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, Col.Safe.Expand).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub



    Private Sub spdBags_Initialise()

        'Make F keys available in spread via input maps
        Dim im As InputMap = spdBags.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdBags.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Left, Keys.None), SpreadActions.None)

        Dim sheet As New Spread.SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(Col.Bags)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnHeaderVisible = False

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = Spread.CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.AliceBlue
        sheet.SelectionUnit = Model.SelectionUnit.Row

        'Column types.
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        Dim typeInt As New CellType.NumberCellType
        typeInt.DecimalPlaces = 0

        'Columns
        sheet.Columns(Col.Bags.Expand).Label = " "
        sheet.Columns(Col.Bags.Expand).Width = 20
        sheet.Columns(Col.Bags.Expand).Resizable = False
        sheet.Columns(Col.Bags.Seal).Label = " "
        sheet.Columns(Col.Bags.Actual).Label = "Actual"
        sheet.Columns(Col.Bags.Actual).CellType = typeNumber
        sheet.Columns(Col.Bags.InDate).Label = "Date In"
        sheet.Columns(Col.Bags.InDate).CellType = New CellType.DateTimeCellType
        sheet.Columns(Col.Bags.InDate).HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.Columns(Col.Bags.InUser1).Label = "1 In"
        sheet.Columns(Col.Bags.InUser2).Label = "2 In"
        sheet.Columns(Col.Bags.InUser1, Col.Bags.InUser2).CellType = typeInt
        sheet.Columns(Col.Bags.OutDate).Label = "Date Out"
        sheet.Columns(Col.Bags.OutDate).CellType = New CellType.DateTimeCellType
        sheet.Columns(Col.Bags.OutDate).HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.Columns(Col.Bags.OutUser1).Label = "1 Out"
        sheet.Columns(Col.Bags.OutUser2).Label = "2 Out"
        sheet.Columns(Col.Bags.OutUser1, Col.Bags.OutUser2).CellType = typeInt
        sheet.Columns(Col.Bags.Comments).Label = "Comments"
        sheet.Columns(Col.Bags.ForTrading).Label = "For Trading Period"

        spdBags.Sheets.Add(sheet)
        AddHandler spdBags.Resize, AddressOf spd_Resize

    End Sub

    Private Sub spdBags_AddBags(ByVal bagType As String)

        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim headerIndex As Integer = sheet.RowCount

        'insert header and set tag to nothing as is a header
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Rows(sheet.RowCount - 1).Tag = -1

        'add expand button if any bags exist 
        If _bankingPeriod.GetBagsCount(bagType) > 0 Then
            Dim btn As New PlusMinusCellType(AddressOf spdBags_Button, PlusMinusCellType.States.Plus, bagType)
            sheet.Cells(sheet.RowCount - 1, Col.Bags.Expand).CellType = btn
            sheet.Cells(sheet.RowCount - 1, Col.Bags.Expand).Locked = False
        End If

        sheet.Cells(sheet.RowCount - 1, Col.Bags.Expand).Tag = bagType
        sheet.Cells(sheet.RowCount - 1, Col.Bags.Actual).Value = 0

        Select Case bagType
            Case Banking.Core.BagTypes.Float : sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = My.Resources.Messages.BagsFloat
            Case Banking.Core.BagTypes.Banking : sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = My.Resources.Messages.BagsBanking
            Case Banking.Core.BagTypes.Pickup : sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = My.Resources.Messages.BagsPickup
            Case Banking.Core.BagTypes.Property : sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = My.Resources.Messages.BagsProperty
            Case Banking.Core.BagTypes.GiftToken
                sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = My.Resources.Messages.BagsGiftTokens
                sheet.Cells(sheet.RowCount - 1, Col.Bags.Actual).CellType = New CellType.GeneralCellType
        End Select

        'insert bag details
        For Each bag As BOBanking.cSafeBags In _bankingPeriod.Safe.Bags.Where(Function(b As BOBanking.cSafeBags) b.Type.Value = bagType)
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = bag.ID.Value
            sheet.Rows(sheet.RowCount - 1).Visible = False

            sheet.Cells(sheet.RowCount - 1, Col.Bags.Expand).Tag = bagType
            sheet.Cells(sheet.RowCount - 1, Col.Bags.Seal).Value = bag.SealNumber.Value
            If bagType = BOBanking.BagTypes.GiftToken Then sheet.Cells(sheet.RowCount - 1, Col.Bags.Actual).CellType = New CellType.GeneralCellType
            sheet.Cells(sheet.RowCount - 1, Col.Bags.Actual).Value = bag.Value.Value
            sheet.Cells(sheet.RowCount - 1, Col.Bags.InDate).Value = bag.InDate.Value
            sheet.Cells(sheet.RowCount - 1, Col.Bags.InUser1).Value = bag.InUserID1.Value
            sheet.Cells(sheet.RowCount - 1, Col.Bags.InUser2).Value = bag.InUserID2.Value
            sheet.Cells(sheet.RowCount - 1, Col.Bags.Comments).Value = bag.Comments.Value

            If bag.PickupPeriodID.Value <> bag.InPeriodID.Value Then
                sheet.Cells(sheet.RowCount - 1, Col.Bags.ForTrading).Value = bag.PickupPeriodID.Value & " - " & Period.GetPeriod(bag.PickupPeriodID.Value).StartDate
            End If

            'check if released
            If bag.OutDate.Value <> Nothing OrElse (bag.OutPeriodID.Value > 0) Then
                sheet.Cells(sheet.RowCount - 1, Col.Bags.OutDate).Value = bag.OutDate.Value
                sheet.Cells(sheet.RowCount - 1, Col.Bags.OutUser1).Value = bag.OutUserID1.Value
                sheet.Cells(sheet.RowCount - 1, Col.Bags.OutUser2).Value = bag.OutUserID2.Value
                sheet.Rows(sheet.RowCount - 1).ForeColor = Drawing.Color.Gray
            End If

            'check if commed
            If bag.State.Value = Banking.Core.BagStates.PreparedNotReleased Then
                sheet.Rows(sheet.RowCount - 1).ForeColor = Drawing.Color.Blue
            End If

            'update totals
            sheet.Cells(headerIndex, Col.Bags.Actual).Value = CDec(sheet.Cells(headerIndex, Col.Bags.Actual).Value) + bag.Value.Value
        Next

    End Sub

    Private Sub spdBags_Expand(ByVal bagType As String)

        Dim sheet As SheetView = spdBags.ActiveSheet

        For rowIndex As Integer = 0 To sheet.RowCount - 1
            If sheet.Cells(rowIndex, Col.Bags.Expand).Tag.ToString = bagType Then
                'force expansion of bag section
                Dim ct As CellType.ICellType = CType(sheet.Cells(rowIndex, Col.Bags.Expand).CellType, CellType.ICellType)
                If TypeOf ct Is PlusMinusCellType Then
                    Dim btn As PlusMinusCellType = CType(ct, PlusMinusCellType)
                    If btn.State = PlusMinusCellType.States.Plus Then btn.PerformClick()
                End If

                Exit Sub
            End If
        Next

    End Sub

    Private Sub spdBags_Change() Handles spdBags.SelectionChanged, spdBags.CellClick, spdBags.Change

        'determine if not header row and if bag has been released
        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim notHeaderRow As Boolean = (CInt(sheet.Rows(sheet.ActiveRowIndex).Tag) >= 0)
        Dim bagNotReleased As Boolean = (sheet.Rows(sheet.ActiveRowIndex).ForeColor <> Drawing.Color.Gray)
        Dim bagCommed As Boolean = (sheet.Rows(sheet.ActiveRowIndex).ForeColor = Drawing.Color.Blue)
        btnF6.Enabled = bagNotReleased
        If bagCommed Then btnF6.Enabled = False

        'set visibility and text of buttons
        Dim bagType As String = sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).Tag.ToString
        Select Case True
            Case bagType.Equals(Core.BagTypes.Float)
                btnF5.Text = My.Resources.Controls.F5AddFloat
                btnF6.Visible = notHeaderRow
                btnF7.Visible = False
                btnF7.Enabled = bagNotReleased
                btnPrint.Visible = False
                btnStandalone.Visible = False

            Case bagType.Equals(Core.BagTypes.Banking)
                btnF5.Text = My.Resources.Controls.F5AddBanking
                btnF6.Visible = notHeaderRow
                btnF7.Visible = notHeaderRow
                btnF7.Text = My.Resources.Controls.F7BagCollected
                btnF7.Enabled = bagNotReleased
                btnPrint.Text = My.Resources.Controls.F9EodBanking
                btnPrint.Visible = True
                btnStandalone.Visible = False

            Case bagType.Equals(Core.BagTypes.Pickup)
                btnF5.Text = My.Resources.Controls.F5AddPickup
                btnF6.Visible = notHeaderRow
                btnF7.Visible = True
                btnF7.Text = My.Resources.Controls.F7AddFinalPickup
                btnF7.Enabled = True
                btnPrint.Text = My.Resources.Controls.F9EodPickup
                btnPrint.Visible = True
                btnStandalone.Visible = True

            Case bagType.Equals(Core.BagTypes.GiftToken)
                btnF5.Text = My.Resources.Controls.F5AddGiftToken
                btnF6.Visible = notHeaderRow
                btnF7.Visible = notHeaderRow
                btnF7.Text = My.Resources.Controls.F7Remove
                btnF7.Enabled = bagNotReleased
                btnPrint.Visible = False
                btnStandalone.Visible = False

            Case bagType.Equals(Core.BagTypes.Property)
                btnF5.Text = My.Resources.Controls.F5AddProperty
                btnF6.Visible = notHeaderRow
                btnF7.Visible = notHeaderRow
                btnF7.Text = My.Resources.Controls.F7Remove
                btnF7.Enabled = bagNotReleased
                btnPrint.Visible = False
                btnStandalone.Visible = False
        End Select

    End Sub

    Private Sub spdBags_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdBags.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdBags.ActiveSheet

            'perform button click if button there for current row
            Dim ct As CellType.ICellType = CType(sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).CellType, CellType.ICellType)
            If TypeOf ct Is PlusMinusCellType Then
                CType(ct, PlusMinusCellType).PerformClick()
            End If

        End If

    End Sub

    Private Sub spdBags_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim btn As PlusMinusCellType = CType(sender, PlusMinusCellType)
        Dim tag As String = btn.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        'find row with button first
        Dim rowButton As Integer = 0
        For rowIndex As Integer = 0 To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, Col.Bags.Expand).Tag.ToString = tag Then
                rowButton = rowIndex
                Exit For
            End If
        Next

        'loop over following rows to set visibility
        For rowIndex As Integer = (rowButton + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, Col.Bags.Expand).Tag.ToString = tag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit For
            End If
        Next

        'check how many visible rows to determine whether to show seal column header
        Dim header As String = " "
        If sheet.Rows.VisibleRows > 5 Then header = "Seal Number"
        sheet.Columns(Col.Bags.Seal).Label = header

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs)

        Dim spread As FpSpread = CType(sender, FpSpread)
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check that are sheets available
        If spread.Sheets.Count = 0 Then Exit Sub

        'get tab strip and border style sizes
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim spreadWidth As Single = spread.Width
        Dim spreadHeight As Single = spread.Height

        'make allowance for tab strip and border
        If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            spreadHeight -= (borderHeight * 2)
            spreadWidth -= (borderWidth * 2)
        End If


        For Each sheet As SheetView In spread.Sheets
            'check sheet has any rows first
            If sheet.RowCount = 0 Then Continue For

            Dim sheetHeight As Single = spreadHeight
            Dim sheetWidth As Single = spreadWidth

            'Get row header widths
            If sheet.Visible = False Then Continue For
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                Next
            End If

            'Get column header heights
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                Next
            End If

            'get rowsHeight and colsWidth
            Dim rowsHeight As Single = sheet.RowCount * sheet.Rows.Default.Height
            Dim colsWidth As Single = 0
            For Each col As Column In sheet.Columns
                If col.Visible Then colsWidth += col.Width
            Next

            'check whether need scrollbars
            If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

            If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'check whether need horizontal scrollbar
            If colsTotal > sheetWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                'if resizing then fill up empty space
                If colsResizable = 0 Then Continue For
                Dim increase As Integer = CInt((sheetWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If

        Next

    End Sub

    Private Function Authorised(ByRef userId2 As Integer, ByVal bagType As String, ByVal state As Core.Authorisation.States) As Boolean

        'check whether authorisation is required
        If _authorisation Is Nothing Then _authorisation = New Core.Authorisation
        If _authorisation.AuthorisationRequired(state, bagType) Then
            Using login As New UserAuthorise()
                If login.ShowDialog(Me) = DialogResult.OK Then
                    userId2 = login.UserId
                    ParentForm.Refresh()

                    If userId2 = Me.UserId Then
                        DisplayWarning(My.Resources.Errors.ValidFirstSecondAuthMustDiffer)
                        Return False
                    End If
                Else
                    DisplayWarning(My.Resources.Errors.ValidSecondAuthNotProvided)
                    Return False
                End If
            End Using
        End If

        Return True

    End Function

    Private Sub btnSafe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSafe.Click

        'check whether a second authorisation is needed and display input box if true
        Dim userId2 As Integer = UserId
        If Not Authorised(userId2, "", Core.Authorisation.States.Safe) Then
            Exit Sub
        End If

        'load safe check window
        Dim periodId As Integer = CInt(cmbPeriod.SelectedValue)
        Dim check As New SafeForm(UserId, userId2, periodId)
        Dim result As DialogResult = check.ShowDialog(Me)
        check.Dispose()

        If result = DialogResult.OK Then
            _bankingPeriod.LoadSafe(periodId)
            '_safe = New BOBanking.cSafe(_oasys3DB)
            '_safe.Load(periodId)
            spdSafe_Refresh()
        End If

    End Sub

    Private Sub btnF5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnF5.Click

        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim bagType As String = sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).Tag.ToString
        Dim userId2 As Integer = UserId

        'check authorisation
        If Not Authorised(userId2, bagType, Core.Authorisation.States.In) Then
            Exit Sub
        End If

        'open new window for bag entry dependent on bag type
        Select Case bagType
            Case Core.BagTypes.Float
                Using fe As New FloatForm(UserId, userId2, _accountType, _storeIdName)
                    If fe.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

            Case Core.BagTypes.Banking
                'check that there are no open pickup bags for the selected day
                Dim PeriodId As Integer = _bankingPeriod.PeriodId
                If _bankingPeriod.AllPickupsToSafe(PeriodId) Then
                    Using be As New BankingForm(UserId, userId2, _accountType, _storeIdName)

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        ' Author      : Partha Dutta
                        ' Date        : 07/09/2010
                        ' Referral No : 373
                        ' Notes       : Prevent incorrect banking date to be selected by user, let system do job
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'banking date already selected by system
                        be.DisableDateSelection()

                        If be.ShowDialog(Me) = DialogResult.OK Then
                            cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                            spdBags_Expand(bagType)
                        End If
                    End Using

                Else
                    MessageBox.Show(My.Resources.Messages.OpenPickups, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If

            Case Core.BagTypes.Pickup
                Using pf As New PickupForm(UserId, userId2, _accountType)
                    If pf.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

            Case Else
                Using bd As New BagDialog(UserId, userId2, bagType, _accountType)
                    If bd.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

        End Select

        Me.ParentForm.Refresh()

    End Sub

    Private Sub btnF6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnF6.Click

        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim bagType As String = sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).Tag.ToString
        Dim bagId As Integer = CInt(sheet.Rows(sheet.ActiveRowIndex).Tag)
        Dim userId2 As Integer = UserId

        'check whether a second authorisation is needed
        If Not Authorised(userId2, bagType, Core.Authorisation.States.Check) Then
            Exit Sub
        End If

        Select Case bagType
            Case Core.BagTypes.Float
                Using float As New FloatForm(UserId, userId2, _accountType, _storeIdName)
                    float.LoadOriginalBag(bagId)
                    If float.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

            Case Core.BagTypes.Banking
                Using banking As New BankingForm(UserId, userId2, _accountType, _storeIdName)
                    banking.LoadOriginalBag(bagId)
                    If banking.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)

                        Dim message As String = String.Empty
                        If _bankingPeriod.BankingOkToComm(banking.PickupPeriodId, message) Then message = My.Resources.Messages.BankingConfirmed
                        MessageBox.Show(message, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End Using

            Case Core.BagTypes.Pickup
                'Dim b As BagPickup = CType(Bag.GetBag(bagId), BagPickup)
                'b.OutMovement.User1 = UserId
                'b.OutMovement.User2 = userId2

                'Using pf As New NewPickupForm(b)
                '    If pf.ShowDialog(Me) = DialogResult.OK Then
                '        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                '        spdBags_Expand(bagType)
                '    End If
                'End Using

                Using pf As New PickupForm(UserId, userId2, _accountType)
                    pf.LoadOriginalBag(bagId)
                    If pf.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

            Case Else
                Using dialog As New BagDialog(UserId, userId2, bagType, _accountType)
                    dialog.LoadOriginalBag(bagId, BagDialog.States.Reseal)
                    If dialog.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

        End Select

        Me.ParentForm.Refresh()

    End Sub

    Private Sub btnF7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnF7.Click

        Dim sheet As SheetView = spdBags.ActiveSheet
        Dim bagType As String = sheet.Cells(sheet.ActiveRowIndex, Col.Bags.Expand).Tag.ToString
        Dim bagId As Integer = CInt(sheet.Rows(sheet.ActiveRowIndex).Tag)
        Dim userId2 As Integer = UserId

        Select Case btnF7.Text
            Case My.Resources.Controls.F7AddFinalPickup
                'check whether a second authorisation is needed
                If Not Authorised(userId2, bagType, Core.Authorisation.States.In) Then
                    Exit Sub
                End If

                Using pf As New PickupForm(UserId, userId2, _accountType, True)
                    If pf.ShowDialog(Me) = DialogResult.OK Then
                        cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                        spdBags_Expand(bagType)
                    End If
                End Using

            Case Else
                'check whether a second authorisation is needed
                If Not Authorised(userId2, bagType, Core.Authorisation.States.Out) Then
                    Exit Sub
                End If

                'load bag dialog window
                Using bd As New BagDialog(UserId, userId2, bagType, _accountType)
                    bd.LoadOriginalBag(bagId, BagDialog.States.Remove)

                    If bd.IsBankingAndNotManagerChecked Then
                        MessageBox.Show(My.Resources.Messages.BankingBagNotManagerChecked)
                    Else
                        If bd.ShowDialog(Me) = DialogResult.OK Then
                            cmbPeriod_SelectionChangeCommitted(Me, New EventArgs)
                            spdBags_Expand(bagType)
                        End If
                    End If
                End Using

        End Select

        Me.ParentForm.Refresh()

    End Sub


    Private Sub btnStandalone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStandalone.Click

        Using standalone As New StandaloneReport(_accountType, _storeIdName)
            standalone.ShowDialog()
        End Using

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Select Case btnPrint.Text
            Case My.Resources.Controls.F9EodPickup
                Dim pickup As New EodPickupReport(_accountType, _storeIdName)
                pickup.ShowDialog(Me)
                pickup.Dispose()

            Case My.Resources.Controls.F9EodBanking
                Dim report As New EodBankingReport(CInt(cmbPeriod.SelectedValue), _storeIdName)
                report.ShowDialog(Me)
        End Select

    End Sub

    Private Sub btnPrintSafe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintSafe.Click

        'remove any existing rows and reset
        Dim print As SheetView = spdPrint.Sheets(0)
        print.Rows.Remove(0, print.RowCount)

        'copy safe rows to print spread
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            print.Rows.Add(print.RowCount, 1)

            'copy borders and font
            print.Rows(print.RowCount - 1).Border = sheet.Rows(rowIndex).Border
            print.Rows(print.RowCount - 1).Font = sheet.Rows(rowIndex).Font

            'copy values from sheet rows to print rows
            print.Cells(print.RowCount - 1, Col.Print.Denom).Value = sheet.Cells(rowIndex, Col.Bags.Seal).Value
            print.Cells(print.RowCount - 1, Col.Print.System).Value = sheet.Cells(rowIndex, Col.Bags.Actual).Value
        Next

        'copy bags rows to print spread
        sheet = spdBags.ActiveSheet
        print.Rows.Add(print.RowCount, 2)
        print.Rows(print.RowCount - 1).Font = New Font(FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
        print.Cells(print.RowCount - 1, Col.Print.Denom).Value = "Bags(Seal Number and Type)"

        For rowIndex As Integer = 0 To sheet.RowCount - 1
            'exclude header rows and any released bags
            If CInt(sheet.Rows(rowIndex).Tag) < 0 Then Continue For
            If sheet.Rows(rowIndex).ForeColor = Drawing.Color.Gray Then Continue For

            'add new row and copy borders and font
            print.Rows.Add(print.RowCount, 1)
            print.Rows(print.RowCount - 1).Border = sheet.Rows(rowIndex).Border
            print.Rows(print.RowCount - 1).Font = sheet.Rows(rowIndex).Font

            'copy values from sheet rows to print rows
            Dim bag As BOBanking.cSafeBags = _bankingPeriod.Safe.Bag(CInt(sheet.Rows(rowIndex).Tag))
            print.Cells(print.RowCount - 1, Col.Print.Denom).Value = bag.SealNumber.Value & Space(3) & BagTypeString(bag.Type.Value)
            print.Cells(print.RowCount - 1, Col.Print.System).Value = sheet.Cells(rowIndex, Col.Bags.Actual).Value
        Next

        'make all rows visible
        print.Rows.Visible(True)

        'print info header and footer
        print.PrintInfo.Header = "/lStore: " & _storeIdName
        print.PrintInfo.Header &= "/c/fz""14""Safe Check Report"
        print.PrintInfo.Header &= "/n/c/fz""10""Date for " & _bankingPeriod.PeriodDate.ToShortDateString
        print.PrintInfo.Header &= "/n "
        print.PrintInfo.Footer = "/lPrinted: " & Now
        print.PrintInfo.Footer &= "/cPage /p of /pc"
        print.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion

        spdPrint.PrintSheet(-1)

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class

