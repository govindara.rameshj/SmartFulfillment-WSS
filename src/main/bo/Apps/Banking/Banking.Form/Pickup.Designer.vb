﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pickup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnAccept = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdSafe = New FarPoint.Win.Spread.FpSpread
        Me.stsStatus = New System.Windows.Forms.StatusStrip
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.cmbCashierTill = New System.Windows.Forms.ComboBox
        Me.btnPrint = New System.Windows.Forms.Button
        Me.grpDetails = New System.Windows.Forms.GroupBox
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblSealMessage = New System.Windows.Forms.Label
        Me.grpOptions = New System.Windows.Forms.GroupBox
        Me.rdoReturn = New System.Windows.Forms.RadioButton
        Me.rdoReseal = New System.Windows.Forms.RadioButton
        Me.spdPrint = New FarPoint.Win.Spread.FpSpread
        Me.grpCashierTill = New System.Windows.Forms.GroupBox
        Me.grpDate = New System.Windows.Forms.GroupBox
        Me.cmbDate = New System.Windows.Forms.ComboBox
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stsStatus.SuspendLayout()
        Me.grpDetails.SuspendLayout()
        Me.grpOptions.SuspendLayout()
        CType(Me.spdPrint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCashierTill.SuspendLayout()
        Me.grpDate.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnReset.Location = New System.Drawing.Point(423, 649)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(81, 39)
        Me.btnReset.TabIndex = 7
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Enabled = False
        Me.btnAccept.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAccept.Location = New System.Drawing.Point(6, 649)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(84, 39)
        Me.btnAccept.TabIndex = 5
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(510, 649)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdSafe
        '
        Me.spdSafe.About = "3.0.2004.2005"
        Me.spdSafe.AccessibleDescription = ""
        Me.spdSafe.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdSafe.Location = New System.Drawing.Point(6, 59)
        Me.spdSafe.Name = "spdSafe"
        Me.spdSafe.Size = New System.Drawing.Size(580, 509)
        Me.spdSafe.TabIndex = 2
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdSafe.TextTipAppearance = TipAppearance1
        Me.spdSafe.ActiveSheetIndex = -1
        '
        'stsStatus
        '
        Me.stsStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.stsStatus.Location = New System.Drawing.Point(3, 691)
        Me.stsStatus.Name = "stsStatus"
        Me.stsStatus.Size = New System.Drawing.Size(586, 22)
        Me.stsStatus.TabIndex = 19
        Me.stsStatus.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = False
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(571, 17)
        Me.lblStatus.Spring = True
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbCashierTill
        '
        Me.cmbCashierTill.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbCashierTill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCashierTill.FormattingEnabled = True
        Me.cmbCashierTill.Location = New System.Drawing.Point(9, 15)
        Me.cmbCashierTill.Name = "cmbCashierTill"
        Me.cmbCashierTill.Size = New System.Drawing.Size(322, 21)
        Me.cmbCashierTill.TabIndex = 0
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(341, 649)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "F9 PickUp Report"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'grpDetails
        '
        Me.grpDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDetails.Controls.Add(Me.txtSeal)
        Me.grpDetails.Controls.Add(Me.txtComments)
        Me.grpDetails.Controls.Add(Me.Label3)
        Me.grpDetails.Controls.Add(Me.Label2)
        Me.grpDetails.Controls.Add(Me.lblSealMessage)
        Me.grpDetails.Location = New System.Drawing.Point(6, 574)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(580, 69)
        Me.grpDetails.TabIndex = 4
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Enter Details"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(82, 43)
        Me.txtComments.MaxLength = 255
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(492, 20)
        Me.txtComments.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(70, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "&Comments"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(70, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "&Seal Number"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSealMessage
        '
        Me.lblSealMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSealMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblSealMessage.ForeColor = System.Drawing.Color.Red
        Me.lblSealMessage.Location = New System.Drawing.Point(177, 16)
        Me.lblSealMessage.Name = "lblSealMessage"
        Me.lblSealMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSealMessage.Size = New System.Drawing.Size(397, 20)
        Me.lblSealMessage.TabIndex = 2
        Me.lblSealMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSealMessage.Visible = False
        '
        'grpOptions
        '
        Me.grpOptions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpOptions.Controls.Add(Me.rdoReturn)
        Me.grpOptions.Controls.Add(Me.rdoReseal)
        Me.grpOptions.Location = New System.Drawing.Point(6, 499)
        Me.grpOptions.Name = "grpOptions"
        Me.grpOptions.Size = New System.Drawing.Size(580, 69)
        Me.grpOptions.TabIndex = 3
        Me.grpOptions.TabStop = False
        Me.grpOptions.Text = "Choose Option"
        Me.grpOptions.Visible = False
        '
        'rdoReturn
        '
        Me.rdoReturn.Location = New System.Drawing.Point(6, 41)
        Me.rdoReturn.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoReturn.Name = "rdoReturn"
        Me.rdoReturn.Size = New System.Drawing.Size(143, 19)
        Me.rdoReturn.TabIndex = 1
        Me.rdoReturn.Text = "Return Bag to Safe"
        Me.rdoReturn.UseVisualStyleBackColor = True
        '
        'rdoReseal
        '
        Me.rdoReseal.Location = New System.Drawing.Point(6, 16)
        Me.rdoReseal.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoReseal.Name = "rdoReseal"
        Me.rdoReseal.Size = New System.Drawing.Size(143, 19)
        Me.rdoReseal.TabIndex = 0
        Me.rdoReseal.Text = "Re-Seal Bag"
        Me.rdoReseal.UseVisualStyleBackColor = True
        '
        'spdPrint
        '
        Me.spdPrint.About = "3.0.2004.2005"
        Me.spdPrint.AccessibleDescription = ""
        Me.spdPrint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdPrint.Location = New System.Drawing.Point(170, 126)
        Me.spdPrint.Name = "spdPrint"
        Me.spdPrint.Size = New System.Drawing.Size(230, 182)
        Me.spdPrint.TabIndex = 27
        Me.spdPrint.TabStop = False
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdPrint.TextTipAppearance = TipAppearance2
        Me.spdPrint.ActiveSheetIndex = -1
        '
        'grpCashierTill
        '
        Me.grpCashierTill.Controls.Add(Me.cmbCashierTill)
        Me.grpCashierTill.Location = New System.Drawing.Point(6, 6)
        Me.grpCashierTill.Name = "grpCashierTill"
        Me.grpCashierTill.Size = New System.Drawing.Size(337, 47)
        Me.grpCashierTill.TabIndex = 0
        Me.grpCashierTill.TabStop = False
        Me.grpCashierTill.Text = "GroupBox1"
        '
        'grpDate
        '
        Me.grpDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDate.Controls.Add(Me.cmbDate)
        Me.grpDate.Location = New System.Drawing.Point(349, 6)
        Me.grpDate.Name = "grpDate"
        Me.grpDate.Size = New System.Drawing.Size(237, 47)
        Me.grpDate.TabIndex = 1
        Me.grpDate.TabStop = False
        Me.grpDate.Text = "PickUp Date"
        Me.grpDate.Visible = False
        '
        'cmbDate
        '
        Me.cmbDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDate.FormatString = "d"
        Me.cmbDate.FormattingEnabled = True
        Me.cmbDate.Location = New System.Drawing.Point(9, 15)
        Me.cmbDate.Name = "cmbDate"
        Me.cmbDate.Size = New System.Drawing.Size(222, 21)
        Me.cmbDate.TabIndex = 0
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(82, 16)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 1
        '
        'CheckPickup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 716)
        Me.Controls.Add(Me.grpDate)
        Me.Controls.Add(Me.grpCashierTill)
        Me.Controls.Add(Me.grpOptions)
        Me.Controls.Add(Me.grpDetails)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.stsStatus)
        Me.Controls.Add(Me.spdSafe)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.spdPrint)
        Me.KeyPreview = True
        Me.Name = "CheckPickup"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "PickUp"
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stsStatus.ResumeLayout(False)
        Me.stsStatus.PerformLayout()
        Me.grpDetails.ResumeLayout(False)
        Me.grpDetails.PerformLayout()
        Me.grpOptions.ResumeLayout(False)
        CType(Me.spdPrint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCashierTill.ResumeLayout(False)
        Me.grpDate.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents spdSafe As FarPoint.Win.Spread.FpSpread
    Friend WithEvents stsStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cmbCashierTill As System.Windows.Forms.ComboBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Private WithEvents txtComments As System.Windows.Forms.TextBox
    Private WithEvents Label3 As System.Windows.Forms.Label
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents lblSealMessage As System.Windows.Forms.Label
    Friend WithEvents grpOptions As System.Windows.Forms.GroupBox
    Friend WithEvents rdoReturn As System.Windows.Forms.RadioButton
    Friend WithEvents rdoReseal As System.Windows.Forms.RadioButton
    Friend WithEvents spdPrint As Farpoint.Win.Spread.FpSpread
    Friend WithEvents grpCashierTill As System.Windows.Forms.GroupBox
    Friend WithEvents grpDate As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDate As System.Windows.Forms.ComboBox
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
End Class
