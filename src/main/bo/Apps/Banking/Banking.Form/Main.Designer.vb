<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdBags = New FarPoint.Win.Spread.FpSpread
        Me.cmbPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker
        Me.btnPrint = New System.Windows.Forms.Button
        Me.spdSafe = New FarPoint.Win.Spread.FpSpread
        Me.grpSafe = New System.Windows.Forms.GroupBox
        Me.spdPrint = New FarPoint.Win.Spread.FpSpread
        Me.btnPrintSafe = New System.Windows.Forms.Button
        Me.btnSafe = New System.Windows.Forms.Button
        Me.grpBags = New System.Windows.Forms.GroupBox
        Me.pnlButtons = New System.Windows.Forms.Panel
        Me.btnF5 = New System.Windows.Forms.Button
        Me.btnF6 = New System.Windows.Forms.Button
        Me.btnF7 = New System.Windows.Forms.Button
        Me.btnPeriod = New System.Windows.Forms.Button
        Me.btnStandalone = New System.Windows.Forms.Button
        CType(Me.spdBags, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSafe.SuspendLayout()
        CType(Me.spdPrint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBags.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(871, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdBags
        '
        Me.spdBags.About = "3.0.2004.2005"
        Me.spdBags.AccessibleDescription = "SupplierSpread, Direct, Row 0, Column 0, "
        Me.spdBags.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdBags.EditModeReplace = True
        Me.spdBags.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdBags.Location = New System.Drawing.Point(3, 16)
        Me.spdBags.Name = "spdBags"
        Me.spdBags.Size = New System.Drawing.Size(642, 452)
        Me.spdBags.TabIndex = 2
        Me.spdBags.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdBags.TextTipAppearance = TipAppearance1
        Me.spdBags.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdBags.VerticalScrollBarWidth = 15
        Me.spdBags.ActiveSheetIndex = -1
        '
        'cmbPeriod
        '
        Me.cmbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPeriod.FormattingEnabled = True
        Me.cmbPeriod.Location = New System.Drawing.Point(136, 4)
        Me.cmbPeriod.Name = "cmbPeriod"
        Me.cmbPeriod.Size = New System.Drawing.Size(157, 21)
        Me.cmbPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblPeriod.Location = New System.Drawing.Point(6, 3)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(124, 22)
        Me.lblPeriod.TabIndex = 15
        Me.lblPeriod.Text = "Select Banking Day"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPeriod
        '
        Me.dtpPeriod.Location = New System.Drawing.Point(324, 4)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.Size = New System.Drawing.Size(138, 20)
        Me.dtpPeriod.TabIndex = 1
        Me.dtpPeriod.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(765, 508)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 39)
        Me.btnPrint.TabIndex = 4
        Me.btnPrint.Text = "F9 End of Day Report"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'spdSafe
        '
        Me.spdSafe.About = "3.0.2004.2005"
        Me.spdSafe.AccessibleDescription = "SupplierSpread, Direct, Row 0, Column 0, "
        Me.spdSafe.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdSafe.EditModeReplace = True
        Me.spdSafe.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdSafe.Location = New System.Drawing.Point(3, 16)
        Me.spdSafe.Name = "spdSafe"
        Me.spdSafe.Size = New System.Drawing.Size(284, 452)
        Me.spdSafe.TabIndex = 8
        Me.spdSafe.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdSafe.TextTipAppearance = TipAppearance2
        Me.spdSafe.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdSafe.VerticalScrollBarWidth = 15
        Me.spdSafe.ActiveSheetIndex = -1
        '
        'grpSafe
        '
        Me.grpSafe.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpSafe.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpSafe.Controls.Add(Me.spdSafe)
        Me.grpSafe.Controls.Add(Me.spdPrint)
        Me.grpSafe.Location = New System.Drawing.Point(3, 31)
        Me.grpSafe.Name = "grpSafe"
        Me.grpSafe.Size = New System.Drawing.Size(290, 471)
        Me.grpSafe.TabIndex = 9
        Me.grpSafe.TabStop = False
        Me.grpSafe.Text = "Safe"
        '
        'spdPrint
        '
        Me.spdPrint.About = "3.0.2004.2005"
        Me.spdPrint.AccessibleDescription = ""
        Me.spdPrint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdPrint.Location = New System.Drawing.Point(6, 19)
        Me.spdPrint.Name = "spdPrint"
        Me.spdPrint.Size = New System.Drawing.Size(278, 142)
        Me.spdPrint.TabIndex = 29
        Me.spdPrint.TabStop = False
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdPrint.TextTipAppearance = TipAppearance3
        Me.spdPrint.Visible = False
        Me.spdPrint.ActiveSheetIndex = -1
        '
        'btnPrintSafe
        '
        Me.btnPrintSafe.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSafe.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrintSafe.Location = New System.Drawing.Point(3, 508)
        Me.btnPrintSafe.Name = "btnPrintSafe"
        Me.btnPrintSafe.Size = New System.Drawing.Size(82, 39)
        Me.btnPrintSafe.TabIndex = 30
        Me.btnPrintSafe.Text = "F3 Safe Check Report"
        Me.btnPrintSafe.UseVisualStyleBackColor = True
        '
        'btnSafe
        '
        Me.btnSafe.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSafe.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSafe.Location = New System.Drawing.Point(91, 508)
        Me.btnSafe.Name = "btnSafe"
        Me.btnSafe.Size = New System.Drawing.Size(82, 39)
        Me.btnSafe.TabIndex = 9
        Me.btnSafe.Text = "F4 Safe Balance"
        Me.btnSafe.UseVisualStyleBackColor = True
        '
        'grpBags
        '
        Me.grpBags.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpBags.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpBags.Controls.Add(Me.spdBags)
        Me.grpBags.Location = New System.Drawing.Point(299, 31)
        Me.grpBags.Name = "grpBags"
        Me.grpBags.Size = New System.Drawing.Size(648, 471)
        Me.grpBags.TabIndex = 10
        Me.grpBags.TabStop = False
        Me.grpBags.Text = "Bags"
        '
        'pnlButtons
        '
        Me.pnlButtons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlButtons.Controls.Add(Me.btnF5)
        Me.pnlButtons.Controls.Add(Me.btnF6)
        Me.pnlButtons.Controls.Add(Me.btnF7)
        Me.pnlButtons.Location = New System.Drawing.Point(299, 508)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(318, 39)
        Me.pnlButtons.TabIndex = 9
        Me.pnlButtons.Visible = False
        '
        'btnF5
        '
        Me.btnF5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnF5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnF5.Location = New System.Drawing.Point(0, 0)
        Me.btnF5.Name = "btnF5"
        Me.btnF5.Size = New System.Drawing.Size(100, 39)
        Me.btnF5.TabIndex = 5
        Me.btnF5.Text = "F5"
        Me.btnF5.UseVisualStyleBackColor = True
        '
        'btnF6
        '
        Me.btnF6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnF6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnF6.Location = New System.Drawing.Point(106, 0)
        Me.btnF6.Name = "btnF6"
        Me.btnF6.Size = New System.Drawing.Size(100, 39)
        Me.btnF6.TabIndex = 7
        Me.btnF6.Text = "F6"
        Me.btnF6.UseVisualStyleBackColor = True
        Me.btnF6.Visible = False
        '
        'btnF7
        '
        Me.btnF7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnF7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnF7.Location = New System.Drawing.Point(212, 0)
        Me.btnF7.Name = "btnF7"
        Me.btnF7.Size = New System.Drawing.Size(100, 39)
        Me.btnF7.TabIndex = 6
        Me.btnF7.Text = "F7"
        Me.btnF7.UseVisualStyleBackColor = True
        Me.btnF7.Visible = False
        '
        'btnPeriod
        '
        Me.btnPeriod.BackgroundImage = CType(resources.GetObject("btnPeriod.BackgroundImage"), System.Drawing.Image)
        Me.btnPeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPeriod.FlatAppearance.BorderSize = 0
        Me.btnPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPeriod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPeriod.Location = New System.Drawing.Point(298, 4)
        Me.btnPeriod.Name = "btnPeriod"
        Me.btnPeriod.Size = New System.Drawing.Size(20, 20)
        Me.btnPeriod.TabIndex = 31
        Me.btnPeriod.UseVisualStyleBackColor = True
        '
        'btnStandalone
        '
        Me.btnStandalone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStandalone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnStandalone.Location = New System.Drawing.Point(659, 508)
        Me.btnStandalone.Name = "btnStandalone"
        Me.btnStandalone.Size = New System.Drawing.Size(100, 39)
        Me.btnStandalone.TabIndex = 32
        Me.btnStandalone.Text = "F8 Standalone Variance Report"
        Me.btnStandalone.UseVisualStyleBackColor = True
        Me.btnStandalone.Visible = False
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnStandalone)
        Me.Controls.Add(Me.btnPeriod)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnPrintSafe)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.btnSafe)
        Me.Controls.Add(Me.dtpPeriod)
        Me.Controls.Add(Me.grpBags)
        Me.Controls.Add(Me.grpSafe)
        Me.Controls.Add(Me.cmbPeriod)
        Me.Controls.Add(Me.lblPeriod)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(950, 550)
        Me.Name = "Main"
        Me.Size = New System.Drawing.Size(950, 550)
        CType(Me.spdBags, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spdSafe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSafe.ResumeLayout(False)
        CType(Me.spdPrint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBags.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents spdBags As FarPoint.Win.Spread.FpSpread
    Friend WithEvents cmbPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents spdSafe As FarPoint.Win.Spread.FpSpread
    Friend WithEvents grpSafe As System.Windows.Forms.GroupBox
    Friend WithEvents grpBags As System.Windows.Forms.GroupBox
    Friend WithEvents btnF7 As System.Windows.Forms.Button
    Friend WithEvents btnF5 As System.Windows.Forms.Button
    Friend WithEvents btnF6 As System.Windows.Forms.Button
    Friend WithEvents btnSafe As System.Windows.Forms.Button
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents spdPrint As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnPrintSafe As System.Windows.Forms.Button
    Friend WithEvents btnPeriod As System.Windows.Forms.Button
    Friend WithEvents btnStandalone As System.Windows.Forms.Button

End Class
