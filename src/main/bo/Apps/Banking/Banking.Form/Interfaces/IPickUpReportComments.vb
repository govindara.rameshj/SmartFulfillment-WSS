﻿Imports FarPoint.Win.Spread
Imports System.Collections.Generic

Public Interface IPickUpReportComments
    Sub DisplayPickUpReportComments(ByRef SheetView As SheetView, _
                                    ByVal colLength As Integer, _
                                    ByVal bags As List(Of BOBanking.cSafeBags))

    Function GetTradingDayDescription() As String
    Sub ResetCommentsRowHeight(ByRef SheetView As FarPoint.Win.Spread.SheetView)
End Interface
