﻿Imports TpWickes.Library

Public Class PickUpReportCommentsFactory
    Inherits RequirementSwitchFactory(Of IPickUpReportComments)

    Public Overrides Function ImplementationA() As IPickUpReportComments
        Return New PickUpReportDisplayComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22017)
    End Function

    Public Overrides Function ImplementationB() As IPickUpReportComments
        Return New PickUpReportHideComments
    End Function
End Class
