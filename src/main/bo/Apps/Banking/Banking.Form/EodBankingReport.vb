﻿Imports System
Imports System.Linq
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports Microsoft.VisualBasic
Imports Farpoint.Win.Spread
Imports Farpoint.Win
Imports System.Text
Imports Farpoint.PlusMinusCellType
Imports BOBanking
Imports Cts.Oasys.Core.System

Public Class EodBankingReport
    Private Enum col
        Expand
        Denom
        Total
        System
        Variance
    End Enum
    Private _bankingPeriod As Banking.Core.BankingPeriod
    Private _storeIdName As String = String.Empty
    Private _bags As List(Of BOBanking.cSafeBags)


    Public Sub New(ByVal periodId As Integer, ByVal storeIdName As String)
        InitializeComponent()
        _storeIdName = storeIdName

        'load safe
        _bankingPeriod = New Banking.Core.BankingPeriod
        _bankingPeriod.LoadSafe(periodId)

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor
            _bags = New List(Of BOBanking.cSafeBags)
            For Each bag As BOBanking.cSafeBags In _bankingPeriod.Bags.Where(Function(b) b.Type.Value = BagTypes.Banking)
                _bags.Add(bag)
            Next

            spdSafe_Initialise()
            spdSafe_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub DisplayStatus(Optional ByVal text As String = Nothing, Optional ByVal warn As Boolean = False)

        lblStatus.BackColor = Drawing.SystemColors.Control
        If text = Nothing Then
            lblStatus.Text = ""
            lblStatus.ToolTipText = ""
        Else
            If warn Then lblStatus.BackColor = Drawing.Color.Yellow
            lblStatus.Text = text
            lblStatus.ToolTipText = text
        End If
        stsStatus.Refresh()

    End Sub



    Private Sub spdSafe_Initialise()

        Try
            'Make F keys available in spread via input maps
            spdSafe.EditModeReplace = True
            Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
            im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
            imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
            imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
            imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)

            Dim sheet As New SheetView
            sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
            sheet.RowCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length + _bags.Count
            sheet.ColumnHeader.RowCount = 1
            sheet.RowHeader.ColumnCount = 0

            sheet.GrayAreaBackColor = Drawing.Color.White
            sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
            sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
            sheet.DefaultStyle.CellType = New CellType.TextCellType
            sheet.DefaultStyle.Locked = True
            sheet.SelectionBackColor = Drawing.Color.Transparent
            sheet.SelectionUnit = Model.SelectionUnit.Cell

            'Print info.
            sheet.PrintInfo.Header = "/lStore: " & _storeIdName
            sheet.PrintInfo.Header &= "/c/fz""14""" & Me.Text
            sheet.PrintInfo.Header &= "/n/c/fz""10""Data for: " & _bankingPeriod.PeriodDate.ToShortDateString
            sheet.PrintInfo.Header &= "/n "
            sheet.PrintInfo.Footer = "/lPrinted: " & Now
            sheet.PrintInfo.Footer &= "/cPage /p of /pc"
            sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            sheet.PrintInfo.Margin.Left = 50
            sheet.PrintInfo.Margin.Right = 50
            sheet.PrintInfo.Margin.Top = 20
            sheet.PrintInfo.Margin.Bottom = 20
            sheet.PrintInfo.ShowBorder = False
            sheet.PrintInfo.ShowPrintDialog = True
            sheet.PrintInfo.UseSmartPrint = True
            sheet.PrintInfo.Orientation = PrintOrientation.Portrait

            'Column types
            Dim typeNumber As New CellType.NumberCellType
            typeNumber.DecimalPlaces = 2
            typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
            typeNumber.NegativeRed = True

            'Column headers.
            sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
            sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)

            'Columns
            sheet.Columns(col.Expand).Label = " "
            sheet.Columns(col.Expand).Width = 20
            sheet.Columns(col.Expand).Resizable = False
            sheet.Columns(col.Expand).HorizontalAlignment = CellHorizontalAlignment.Center
            sheet.Columns(col.Denom).Label = " "
            sheet.Columns(col.Denom).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.Columns(col.Total).Label = "Banking Total"
            sheet.Columns(col.Total, col.Variance).CellType = typeNumber
            sheet.Columns(col.System).Label = "System"
            sheet.Columns(col.Variance).Label = "Variance"
            sheet.Columns(col.System, col.Variance).Visible = False

            For Each bag As BOBanking.cSafeBags In _bags
                sheet.Columns([Enum].GetValues(GetType(col)).Length + _bags.IndexOf(bag)).Label = "Banking Bag " & (_bags.IndexOf(bag) + 1)
                sheet.Columns([Enum].GetValues(GetType(col)).Length + _bags.IndexOf(bag)).CellType = typeNumber
            Next

            spdSafe.Sheets.Add(sheet)
            AddHandler spdSafe.Resize, AddressOf spdSafe_Resize

        Catch ex As Exception
            Throw New Exception(My.Resources.Errors.ProblemInitControls, ex)
        End Try

    End Sub

    Private Sub spdSafe_AddHeaders()

        Try
            Dim sheet As SheetView = spdSafe.ActiveSheet
            Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length

            If _bags.Count > 0 Then
                'add previous bag details - time
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = Nothing
                sheet.Rows(sheet.RowCount - 1).Locked = True
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Bag Details"
                For Each bag As BOBanking.cSafeBags In _bags
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).CellType = New CellType.GeneralCellType
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Value = Format(bag.InDate.Value, "HH:mm")
                Next

                'add previous bag details - seal number
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = Nothing
                sheet.Rows(sheet.RowCount - 1).Locked = True
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Seal Number"
                For Each bag As BOBanking.cSafeBags In _bags
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).CellType = New CellType.GeneralCellType
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Value = bag.SealNumber.Value
                Next

                'add previous bag details - users in/out
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = Nothing
                sheet.Rows(sheet.RowCount - 1).Locked = True
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Users (In) / (Out)"

                For Each bag As BOBanking.cSafeBags In _bags
                    Dim sb As New StringBuilder("(" & bag.InUserID1.Value.ToString("000") & Space(1))
                    sb.Append(bag.InUserID2.Value.ToString("000") & ")")
                    If bag.OutUserID1.Value <> 0 Then
                        sb.Append(" / (" & bag.OutUserID1.Value.ToString("000") & Space(1))
                        sb.Append(bag.OutUserID2.Value.ToString("000") & ")")
                    End If
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).CellType = New CellType.GeneralCellType
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Value = sb.ToString
                Next

                'add border
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            End If

        Catch ex As Exception
            Throw New Exception(My.Resources.Errors.ProblemInitControls, ex)
        End Try

    End Sub

    Private Sub spdSafe_AddRows()

        Try
            spdSafe_AddHeaders()

            Dim sheet As SheetView = spdSafe.ActiveSheet
            Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length

            'add currencies
            Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
            For Each currency As Currency.Header In currencies
                Dim rowCurrency As Integer = sheet.RowCount
                Dim curID As String = currency.Id

                'add currency header
                sheet.Rows.Add(rowCurrency, 1)
                sheet.Rows(rowCurrency).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

                'add expand button
                Dim btn As PlusMinusCellType = Nothing
                Select Case currency.IsDefault
                    Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                    Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
                End Select
                sheet.Cells(rowCurrency, col.Expand).Tag = curID
                sheet.Cells(rowCurrency, col.Expand).CellType = btn
                sheet.Cells(rowCurrency, col.Expand).Locked = False
                sheet.Cells(rowCurrency, col.Denom).Value = "Currency (" & currency.Symbol & ")"

                'add cash denominations first
                For Each denom As Currency.Line In currency.Lines.Where(Function(c As Currency.Line) c.TenderId = 1)
                    sheet.Rows.Add(sheet.RowCount, 1)
                    sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                    sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
                    sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText

                    Dim sb As New StringBuilder()
                    For Each bag As BOBanking.cSafeBags In _bags
                        sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Value = bag.Denom(curID, denom.Id, denom.TenderId).Value.Value
                        sb.Append(" + RC" & (colLength + _bags.IndexOf(bag) + 1))
                    Next

                    sheet.Cells(sheet.RowCount - 1, col.Total).Formula = sb.ToString
                Next

                'add line border
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

                'insert float for default currency only if pickup
                Dim system As Decimal = 0
                Dim rowTenders As Integer = sheet.RowCount

                'insert cash total
                Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash Total"
                sheet.Cells(sheet.RowCount - 1, col.Total).Formula = formula
                sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"

                For Each bag As BOBanking.cSafeBags In _bags
                    sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Formula = formula
                Next

                'add other tenders
                For Each denom As Currency.Line In currency.Lines.Where(Function(c As Currency.Line) c.TenderId <> 1)
                    sheet.Rows.Add(sheet.RowCount, 1)
                    sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                    sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
                    sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
                    sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"

                    Dim sb As New StringBuilder()
                    For Each bag As BOBanking.cSafeBags In _bags
                        sheet.Cells(sheet.RowCount - 1, colLength + _bags.IndexOf(bag)).Value = bag.Denom(curID, denom.Id, denom.TenderId).Value.Value
                        sb.Append(" + RC" & (colLength + _bags.IndexOf(bag) + 1))
                    Next

                    sheet.Cells(sheet.RowCount - 1, col.Total).Formula = sb.ToString
                Next

                'insert total
                formula = "SUM(R" & rowTenders + 1 & "C:R" & sheet.RowCount & "C)"
                sheet.Cells(rowCurrency, col.Total, rowCurrency, sheet.ColumnCount - 1).Formula = formula
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            Next

            If sheet.RowCount > 0 Then
                btnPrint.Visible = True
                spdSafe_Resize(spdSafe, New EventArgs)
                spdSafe.Focus()
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdSafe_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths in arraylist
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub



    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        spdSafe.PrintSheet(-1)
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class