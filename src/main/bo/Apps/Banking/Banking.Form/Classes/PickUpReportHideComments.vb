﻿Public Class PickUpReportHideComments
    Implements IPickUpReportComments

#Region "Private Variables"
    Private Const TradingDay As String = "For Trading Period"
#End Region

#Region "Public functions & procedures"
    Public Sub DisplayPickUpReportComments(ByRef SheetView As FarPoint.Win.Spread.SheetView, _
                                           ByVal colLength As Integer, _
                                           ByVal bags As System.Collections.Generic.List(Of BOBanking.cSafeBags)) Implements IPickUpReportComments.DisplayPickUpReportComments

    End Sub

    Public Function GetTradingDayDescription() As String Implements IPickUpReportComments.GetTradingDayDescription
        Return TradingDay
    End Function

    Public Sub ResetCommentsRowHeight(ByRef SheetView As FarPoint.Win.Spread.SheetView) Implements IPickUpReportComments.ResetCommentsRowHeight

    End Sub
#End Region
End Class
