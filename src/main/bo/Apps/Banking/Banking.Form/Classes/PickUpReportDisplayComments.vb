﻿Imports System
Imports System.Text

Public Class PickUpReportDisplayComments
    Implements IPickUpReportComments

#Region "Private Variables"
    Private Const Comments As String = "Comments"
    Private Const TradingDay As String = "For Trading Day"
    Private Const CommentColumn As Integer = 1
    Private Const CommentValueColumn As Integer = 5
    Private _CommentsRowIndex As Integer
#End Region

#Region "Public Procedures "
    Public Sub DisplayPickUpReportComments(ByRef SheetView As FarPoint.Win.Spread.SheetView, _
                                           ByVal colLength As Integer, _
                                           ByVal bags As System.Collections.Generic.List(Of BOBanking.cSafeBags)) Implements IPickUpReportComments.DisplayPickUpReportComments

        SheetView.Rows.Add(SheetView.RowCount, 1)
        SheetView.Rows(SheetView.RowCount - 1).Tag = Nothing
        SheetView.Rows(SheetView.RowCount - 1).Locked = True
        SheetView.Rows(SheetView.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        SpreadCellSetAsMultiLine(SheetView, SheetView.RowCount - 1, CommentValueColumn, True)
        SheetView.Cells(SheetView.RowCount - 1, CommentColumn).Value = Comments
        SheetView.Cells(SheetView.RowCount - 1, CommentColumn).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.Top

        For index As Integer = colLength To SheetView.ColumnCount - 1
            SpreadCellSetAsMultiLine(SheetView, SheetView.RowCount - 1, index, True)
            SheetView.Cells(SheetView.RowCount - 1, index).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.Top
            SheetView.Cells(SheetView.RowCount - 1, index).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            SheetView.Cells(SheetView.RowCount - 1, index).Value = bags(index - colLength).Comments.Value
        Next

        _CommentsRowIndex = SheetView.RowCount - 1
        SetCommentsRowHeight(SheetView)
    End Sub

    Public Function GetTradingDayDescription() As String Implements IPickUpReportComments.GetTradingDayDescription
        Return TradingDay
    End Function

    Public Sub ResetCommentsRowHeight(ByRef SheetView As FarPoint.Win.Spread.SheetView) Implements IPickUpReportComments.ResetCommentsRowHeight

        SetCommentsRowHeight(SheetView)
    End Sub
#End Region

#Region "Private procedures"

    Private Sub SpreadCellSetAsMultiLine(ByRef refSV As FarPoint.Win.Spread.SheetView, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer, ByVal WordWrapAsWell As Boolean)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If .ColumnCount > 0 And ColumnIndex < .ColumnCount Then
                        Dim NewCell As New FarPoint.Win.Spread.CellType.TextCellType

                        With NewCell
                            .Multiline = True
                            .WordWrap = WordWrapAsWell
                        End With
                        With refSV.Cells(RowIndex, ColumnIndex)
                            .CellType = NewCell
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Private Sub SpreadRowSetHeightToFitText(ByRef refSV As FarPoint.Win.Spread.SheetView, ByVal RowIndex As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    Dim RowHeight As Single

                    With .Rows(RowIndex)
                        RowHeight = .GetPreferredHeight()
                        .Height = RowHeight
                    End With
                End If
            End With
        End If
    End Sub

    Private Function GetRowHeight(ByVal Text As String, ByVal RowFont As Drawing.Font, ByVal ColumnWidth As Single) As Single
        Dim RowHeight As New Drawing.Bitmap(1, 1)
        Dim RowHeightGraphics As Drawing.Graphics = Drawing.Graphics.FromImage(RowHeight)
        Dim fontSize As Drawing.SizeF

        fontSize = RowHeightGraphics.MeasureString(Text, RowFont)

        With fontSize
            GetRowHeight = .Height * (.Width / ColumnWidth)
        End With
    End Function

    Friend Overridable Sub SetCommentsRowHeight(ByRef SheetCommentsAreOn As FarPoint.Win.Spread.SheetView)

        SpreadRowSetHeightToFitText(SheetCommentsAreOn, _CommentsRowIndex)
    End Sub
#End Region
End Class
