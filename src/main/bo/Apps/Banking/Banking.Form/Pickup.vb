﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports FarPoint.Win.Spread
Imports Microsoft.VisualBasic
Imports FarPoint.Win
Imports FarPoint.PlusMinusCellType

Public Class Pickup
    Private Enum col
        Expand
        Denom
        Count
        Check
        Original
        Bag
        BagsTotal
        System
        Variance
    End Enum
    Private _bankingPeriod As Banking.Core.BankingPeriod
    Private _pickup As Banking.Core.Pickup
    Private _float As Banking.Core.FloatBanking = Nothing
    Private _originalValue As Decimal

    Public Sub New(ByVal user1 As Integer, ByVal user2 As Integer, ByVal accountType As String, Optional ByVal withFloat As Boolean = False)
        InitializeComponent()

        'create new pickup
        _pickup = New Banking.Core.Pickup(user1, user2, accountType)

        'check if with float or not as is a new pickup
        Select Case withFloat
            Case True
                _pickup.State = Core.Pickup.States.PickupFloat
                Me.Text = My.Resources.Controls.F7AddFinalPickup.Substring(3)

                'load float entry screen
                Using float As New PickupFloat(user1, user2, accountType)
                    If float.ShowDialog = Windows.Forms.DialogResult.OK Then
                        _float = float.Float
                    End If
                End Using

            Case False
                _pickup.State = Core.Pickup.States.Pickup
                Me.Text = My.Resources.Controls.F5AddPickup.Substring(3)
        End Select

    End Sub


    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F5 : btnAccept.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor

            cmbCashierTill_Initialise()
            cmbDate_Initialise()

            Select Case _pickup.State
                Case Core.Pickup.States.Check
                    cmbDate.SelectedValue = _pickup.OldPickupPeriodId
                    cmbCashierTill.SelectedValue = _pickup.AccountId
                    cmbCashierTill_SelectionChangeCommitted(cmbCashierTill, New EventArgs)
                    cmbCashierTill.Enabled = False

                Case Core.Pickup.States.Pickup, Core.Pickup.States.PickupFloat
                    grpDate.Visible = True
                    cmbCashierTill.Focus()
            End Select

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub DisplayStatus(Optional ByVal text As String = Nothing, Optional ByVal warn As Boolean = False)

        lblStatus.BackColor = Drawing.SystemColors.Control
        If text = Nothing Then
            lblStatus.Text = ""
            lblStatus.ToolTipText = ""
        Else
            If warn Then lblStatus.BackColor = Drawing.Color.Yellow
            lblStatus.Text = text
            lblStatus.ToolTipText = text
        End If
        stsStatus.Refresh()

    End Sub



    Public Sub LoadOriginalBag(ByVal id As Integer)

        _pickup.State = Core.Pickup.States.Check
        _pickup.LoadOriginalBag(id)

        'reduce spread sheet height and sow options group
        spdSafe.Height -= grpOptions.Height
        spdSafe.Height -= grpOptions.Margin.Top
        spdSafe.Height -= grpOptions.Margin.Bottom
        grpOptions.Visible = True

        'set fields
        Me.Text = My.Resources.Controls.F6CheckBag.Substring(3) & " Seal Number: " & _pickup.OldSealNumber
        txtComments.Text = _pickup.OldComments

    End Sub



    Private Sub spdPrint_Initialise()

        Dim print As SheetView = spdSafe.ActiveSheet.Clone
        print.HorizontalGridLine = New GridLine(GridLineType.Flat, Color.Gray)
        print.VerticalGridLine = New GridLine(GridLineType.Flat, Color.Gray)

        print.Columns(col.Expand).Visible = False
        print.Columns(col.Denom).Width = 150
        print.Columns(col.Count, [Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.Count - 1).Width = 100
        print.Columns(col.Bag).Visible = False
        print.Columns(col.Count, col.Check).Visible = True

        spdPrint.Sheets.Add(print)

    End Sub

    Private Sub spdSafe_Initialise()

        'Column types
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        If spdSafe.Sheets.Count > 0 Then
            spdSafe.ActiveSheet.Rows.Remove(0, spdSafe.ActiveSheet.RowCount)
            spdSafe.ActiveSheet.ColumnCount = [Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.Count

            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                spdSafe.ActiveSheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).Label = "PickUp " & (_pickup.PreviousPickups.IndexOf(bag) + 1)
                spdSafe.ActiveSheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).HorizontalAlignment = CellHorizontalAlignment.Right
                spdSafe.ActiveSheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).CellType = typeNumber
            Next

            Exit Sub
        End If

        'Make F keys available in spread via input maps
        Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        'Make F keys available in spread via input maps
        Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)


        Dim sheet As New SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.Count
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.Transparent
        sheet.SelectionUnit = Model.SelectionUnit.Cell

        'Print info
        Dim title As String = String.Empty
        Select Case _pickup.AccountType
            Case Oasys.System.Accountabilities.Cashier : title = "Cashier PickUp Report"
            Case Oasys.System.Accountabilities.Till : title = "Till PickUp Report"
        End Select

        sheet.PrintInfo.Header = "/lStore: " & _pickup.StoreIdName
        sheet.PrintInfo.Header &= "/c/fz""14""" & title
        sheet.PrintInfo.Header &= "/n/c/fz""10""Data for: " & _bankingPeriod.PeriodDate.ToShortDateString
        sheet.PrintInfo.Header &= "/n "
        sheet.PrintInfo.Footer = "/lPrinted: " & Now
        sheet.PrintInfo.Footer &= "/cPage /p of /pc"
        sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        sheet.PrintInfo.Orientation = PrintOrientation.Landscape
        sheet.PrintInfo.Margin.Left = 50
        sheet.PrintInfo.Margin.Right = 50
        sheet.PrintInfo.Margin.Top = 20
        sheet.PrintInfo.Margin.Bottom = 20
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.UseMax = False
        sheet.PrintInfo.ShowPrintDialog = True

        'Column headers.
        sheet.ColumnHeader.VerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.HorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.DefaultStyle.Font = New Font(FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
        sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Color.Black, 1, False, False, False, True)


        'Columns
        sheet.Columns(col.Expand).Label = " "
        sheet.Columns(col.Expand).Width = 20
        sheet.Columns(col.Expand).Resizable = False
        sheet.Columns(col.Denom).Label = " "
        sheet.Columns(col.Count).Label = "Count"
        sheet.Columns(col.Check).Label = "Check"
        sheet.Columns(col.Count, col.Check).Visible = False
        sheet.Columns(col.Original, col.Variance).CellType = typeNumber
        sheet.Columns(col.Original).Label = "Previous"
        sheet.Columns(col.Bag).Label = "PickUp Entry"
        sheet.Columns(col.Bag).Locked = False
        sheet.Columns(col.BagsTotal).Label = "Total P/ups"
        sheet.Columns(col.BagsTotal).Visible = (_pickup.PreviousPickups.Count > 0)
        sheet.Columns(col.System).Label = "System"
        sheet.Columns(col.Variance).Label = "Variance"

        'check whether columns are visible
        Select Case _pickup.State
            Case Core.Pickup.States.Pickup, Core.Pickup.States.PickupFloat
                sheet.Columns(col.Original).Visible = False
        End Select

        For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
            sheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).Label = "PickUp " & (_pickup.PreviousPickups.IndexOf(bag) + 1)
            sheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).HorizontalAlignment = CellHorizontalAlignment.Right
            sheet.Columns([Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).CellType = typeNumber
        Next

        spdSafe.EditModeReplace = True
        spdSafe.Sheets.Add(sheet)

    End Sub

    Private Sub spdSafe_AddHeaders()

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length

        If _pickup.PreviousPickups.Count > 0 Then
            'add previous bag details - time
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Bag Details"
            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).CellType = New CellType.GeneralCellType
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = Format(bag.InDate.Value, "HH:mm")
            Next

            'add previous bag details - seal number
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Seal Number"
            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).CellType = New CellType.GeneralCellType
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.SealNumber.Value
            Next

            'add previous bag details - users in/out
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Users (In) / (Out)"
            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                Dim sb As New StringBuilder("(" & bag.InUserID1.Value.ToString("000") & Space(1))
                sb.Append(bag.InUserID2.Value.ToString("000") & ")")
                If bag.OutUserID1.Value <> 0 Then
                    sb.Append(" / (" & bag.OutUserID1.Value.ToString("000") & Space(1))
                    sb.Append(bag.OutUserID2.Value.ToString("000") & ")")
                End If
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).CellType = New CellType.GeneralCellType
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = sb.ToString
            Next

            'add previous bag details - pickup period id
            'Dim period As New BOSystem.cSystemPeriods(_safe.Oasys3DB)
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "For Trading Period"
            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).CellType = New CellType.GeneralCellType
                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.PickupPeriodID.Value & " - " & Oasys.System.GetStartDate(bag.PickupPeriodID.Value).ToShortDateString

                'highlight column if not this trading day
                If bag.PickupPeriodID.Value <> _bankingPeriod.PeriodId Then
                    sheet.Columns(colLength + _pickup.PreviousPickups.IndexOf(bag)).BackColor = Color.LightGray
                End If
            Next

            'add border
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        End If

    End Sub

    Private Sub spdSafe_AddRows()

        spdSafe_AddHeaders()

        'set up mask cell type
        Dim typeMask As New CellType.MaskCellType
        typeMask.Mask = "000-00000000-0"

        'get bags total formula
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        Dim formulaBagsTotal As New StringBuilder("RC" & col.Bag + 1)
        For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
            formulaBagsTotal.Append(" + RC" & (colLength + 1 + _pickup.PreviousPickups.IndexOf(bag)))
        Next

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim rowFirstDenom As Integer = 0
        For Each currency As Oasys.Currency.Currency In Oasys.Currency.GetCurrencies
            Dim rowCurrency As Integer = sheet.RowCount
            Dim currencyId As String = currency.Id

            'add currency header marking the row tag as nothing as is a header
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

            'add expand button
            Dim btn As PlusMinusCellType = Nothing
            Select Case currency.IsDefault
                Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
            End Select
            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
            sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
            sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Currency (" & currency.Symbol & ")"
            sheet.Cells(sheet.RowCount - 1, col.Denom).Tag = -1


            'add cash denominations first
            For Each denom As Oasys.Currency.CurrencyDenomination In currency.Denominations.Where(Function(c As Oasys.Currency.CurrencyDenomination) c.TenderId = 1)
                If rowFirstDenom = 0 Then rowFirstDenom = sheet.RowCount
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = denom
                sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
                sheet.Cells(sheet.RowCount - 1, col.BagsTotal).Formula = formulaBagsTotal.ToString

                Select Case _pickup.State
                    Case Core.Pickup.States.Check
                        sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _pickup.OldDenomValue(currencyId, denom.Id, denom.TenderId)
                        sheet.Cells(sheet.RowCount - 1, col.Original).Value = _pickup.OldDenomValue(currencyId, denom.Id, denom.TenderId)
                    Case Else
                        sheet.Cells(sheet.RowCount - 1, col.Bag).Value = 0
                        sheet.Cells(sheet.RowCount - 1, col.Original).Value = 0
                End Select

                For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                    sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.Denom(currencyId, denom.Id, denom.TenderId).Value.Value
                Next
            Next

            'insert line border to mark end of cash
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

            'insert pickup total row
            Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).CanFocus = False
            sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Pickup Total"
            sheet.Cells(sheet.RowCount - 1, col.Original).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.Bag).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.BagsTotal).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickup.CashTendered(currencyId)
            sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"
            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                sheet.Cells(sheet.RowCount - 1, [Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).Formula = formula
            Next

            'insert float for default currency only if final pickup or report
            Dim pickupRow As Integer = sheet.RowCount
            formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"

            If currency.IsDefault Then
                Select Case _pickup.State
                    Case Core.Pickup.States.PickupFloat, Core.Pickup.States.Check
                        sheet.Rows.Add(sheet.RowCount, 1)
                        sheet.Rows(sheet.RowCount - 1).Locked = True
                        sheet.Rows(sheet.RowCount - 1).CanFocus = False
                        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Float"
                        sheet.Cells(sheet.RowCount - 1, col.Original).Value = 0
                        sheet.Cells(sheet.RowCount - 1, col.Bag).Value = 0
                        sheet.Cells(sheet.RowCount - 1, col.BagsTotal).Formula = formulaBagsTotal.ToString
                        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"

                        If _float IsNot Nothing Then
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _float.NewTotal(currency.Id)
                        End If

                        If _pickup.RelatedBag IsNot Nothing Then
                            sheet.Cells(sheet.RowCount - 1, col.Original).Value = _pickup.RelatedBag.Value.Value
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _pickup.RelatedBag.Value.Value
                        End If

                        'enter system floats
                        Dim systemFloat As Decimal = _pickup.FloatsIssued(currencyId)
                        For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                            sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.FloatValue.Value
                            systemFloat -= bag.FloatValue.Value

                            'check if there is a related bag and get value
                            If bag.RelatedBag IsNot Nothing Then
                                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.RelatedBag.Value.Value
                            End If
                        Next
                        sheet.Cells(sheet.RowCount - 1, col.System).Value = systemFloat

                        'add float seal number row
                        sheet.Rows.Add(sheet.RowCount, 1)
                        sheet.Rows(sheet.RowCount - 1).Locked = True
                        sheet.Rows(sheet.RowCount - 1).CanFocus = False
                        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Float Seal Number"
                        sheet.Cells(sheet.RowCount - 1, col.Bag).CellType = typeMask
                        sheet.Cells(sheet.RowCount - 1, col.Bag).HorizontalAlignment = CellHorizontalAlignment.Right

                        If _float IsNot Nothing Then
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _float.NewSealNumber
                        End If

                        If _pickup.RelatedBag IsNot Nothing Then
                            sheet.Cells(sheet.RowCount - 1, col.Original).CellType = typeMask
                            sheet.Cells(sheet.RowCount - 1, col.Original).HorizontalAlignment = CellHorizontalAlignment.Right
                            sheet.Cells(sheet.RowCount - 1, col.Original).Value = _pickup.RelatedBag.SealNumber.Value
                            sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _pickup.RelatedBag.SealNumber.Value
                        End If

                        For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                            'check if there is a related bag and get seal number 
                            If bag.RelatedBag IsNot Nothing Then
                                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).CellType = typeMask
                                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).HorizontalAlignment = CellHorizontalAlignment.Right
                                sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.RelatedBag.SealNumber.Value
                            End If
                        Next
                End Select
            End If

            'insert cash total
            Dim rowTotals As Integer = sheet.RowCount
            formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = Nothing
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
            sheet.Rows(sheet.RowCount - 1).Locked = True
            sheet.Rows(sheet.RowCount - 1).CanFocus = False
            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash Total"
            sheet.Cells(sheet.RowCount - 1, col.Original).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.Bag).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.BagsTotal).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = formula
            sheet.Cells(sheet.RowCount - 1, col.System).Formula = formula

            For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                sheet.Cells(sheet.RowCount - 1, [Enum].GetValues(GetType(col)).Length + _pickup.PreviousPickups.IndexOf(bag)).Formula = formula
            Next

            'add other tenders now
            For Each denom As Oasys.Currency.CurrencyDenomination In currency.Denominations.Where(Function(c As Oasys.Currency.CurrencyDenomination) c.TenderId <> 1)
                Dim denominationId As Decimal = denom.Id
                Dim tenderId As Integer = denom.TenderId
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                sheet.Rows(sheet.RowCount - 1).Tag = denom
                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText

                Select Case _pickup.State
                    Case Core.Pickup.States.Check
                        sheet.Cells(sheet.RowCount - 1, col.Bag).Value = _pickup.OldDenomValue(currencyId, denom.Id, denom.TenderId)
                        sheet.Cells(sheet.RowCount - 1, col.Original).Value = _pickup.OldDenomValue(currencyId, denom.Id, denom.TenderId)
                    Case Else
                        sheet.Cells(sheet.RowCount - 1, col.Bag).Value = 0
                        sheet.Cells(sheet.RowCount - 1, col.Original).Value = 0
                End Select

                Dim sb As New StringBuilder("RC" & col.Bag + 1)
                For Each bag As BOBanking.cSafeBags In _pickup.PreviousPickups
                    sheet.Cells(sheet.RowCount - 1, colLength + _pickup.PreviousPickups.IndexOf(bag)).Value = bag.Denom(currencyId, denominationId, tenderId).Value.Value
                    sb.Append(" + RC" & (colLength + 1 + _pickup.PreviousPickups.IndexOf(bag)))
                Next
                sheet.Cells(sheet.RowCount - 1, col.BagsTotal).Formula = sb.ToString
                sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"
                sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickup.NonCashTendered(currencyId, tenderId)
            Next

            'insert totals into currency header
            formula = "SUM(R" & rowTotals + 1 & "C:R" & sheet.RowCount & "C)"
            sheet.Cells(rowCurrency, col.Original, rowCurrency, sheet.ColumnCount - 1).Formula = formula
            sheet.Cells(rowCurrency, col.System).Formula = formula '& "+" & systemFloat
            sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        Next

        If sheet.RowCount > 0 Then
            btnPrint.Visible = True
            spd_Resize(spdSafe, New EventArgs)
            spdSafe.ActiveSheet.SetActiveCell(rowFirstDenom, col.Bag)
            spdSafe.Focus()
        End If

    End Sub

    Private Sub spdSafe_EditModeOn(ByVal sender As Object, ByVal e As System.EventArgs) Handles spdSafe.EditModeOn

        _originalValue = spdSafe.ActiveSheet.ActiveCell.Value

    End Sub

    Private Sub spdSafe_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdSafe.LeaveCell

        DisplayStatus()

        'exit if not pickup column
        If e.Column <> col.Bag Then Exit Sub

        Dim sheet As SheetView = spdSafe.ActiveSheet
        If sheet.Rows(e.Row).Tag Is Nothing Then Exit Sub

        'check that multiple of denomination
        Dim denom As Oasys.Currency.CurrencyDenomination = sheet.Rows(e.Row).Tag
        If Not denom.IsMultiple(sheet.Cells(e.Row, e.Column).Value) Then
            DisplayStatus("Quantity must be multiple of denomination", True)
            sheet.Cells(e.Row, e.Column).Value = _originalValue
            e.Cancel = True
            Exit Sub
        End If

        'check changes made
        Dim nothingEntered As Boolean = True
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
            If sheet.Cells(rowIndex, col.Original).Value <> sheet.Cells(rowIndex, col.Bag).Value Then
                If txtSeal.Enabled = False Then
                    txtSeal.Enabled = True
                    btnAccept.Enabled = False
                End If
                Exit Sub
            End If
            If sheet.Cells(rowIndex, col.Bag).Value <> 0 Then nothingEntered = False
        Next

        'check if in reseal mode and nothing entered
        If nothingEntered AndAlso rdoReseal.Checked Then
            txtSeal.Enabled = True
            btnAccept.Enabled = False
            Exit Sub
        End If

        'no changes so reset seal box
        txtSeal.Clear()
        txtSeal.Enabled = False
        btnAccept.Enabled = True

    End Sub

    Private Sub spdSafe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdSafe.KeyPress

        DisplayStatus()
        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdSafe.ActiveSheet
            spdSafe_LeaveCell(sender, New LeaveCellEventArgs(New SpreadView(spdSafe), sheet.ActiveRowIndex, sheet.ActiveColumnIndex, sheet.ActiveRowIndex, sheet.ActiveColumnIndex))
        End If

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Try
            Dim spread As FpSpread = CType(sender, FpSpread)
            spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
            spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

            'check that are sheets available
            If spread.Sheets.Count = 0 Then Exit Sub

            'get tab strip and border style sizes
            Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
            Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
            Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
            Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
            Dim spreadWidth As Single = spread.Width
            Dim spreadHeight As Single = spread.Height

            'make allowance for tab strip and border
            If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
            If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
                spreadHeight -= (borderHeight * 2)
                spreadWidth -= (borderWidth * 2)
            End If


            For Each sheet As SheetView In spread.Sheets
                'check sheet has any rows first
                If sheet.RowCount = 0 Then Continue For

                Dim sheetHeight As Single = spreadHeight
                Dim sheetWidth As Single = spreadWidth

                'Get row header widths
                If sheet.Visible = False Then Continue For
                If sheet.RowHeaderVisible Then
                    For Each header As Column In sheet.RowHeader.Columns
                        If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                    Next
                End If

                'Get column header heights
                If sheet.ColumnHeaderVisible Then
                    For Each header As Row In sheet.ColumnHeader.Rows
                        If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                    Next
                End If

                'get rowsHeight and colsWidth
                Dim rowsHeight As Integer = sheet.RowCount * sheet.Rows.Default.Height
                Dim colsWidth As Integer = 0
                For Each col As Column In sheet.Columns
                    If col.Visible Then colsWidth += col.Width
                Next

                'check whether need scrollbars
                If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

                If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


                'get columns to resize and new widths
                Dim colsTotal As Single = 0
                Dim colsResizable As Integer = 0
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then
                        c.Width = c.GetPreferredWidth
                        colsResizable += 1
                    End If
                    If c.Visible Then colsTotal += c.Width
                Next

                'check whether need horizontal scrollbar
                If colsTotal > sheetWidth Then
                    spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                Else
                    'if resizing then fill up empty space

                    If colsResizable = 0 Then Continue For
                    Dim increase As Integer = (sheetWidth - colsTotal) / colsResizable
                    For Each c As Column In sheet.Columns
                        If c.Visible And c.Resizable Then c.Width += increase
                    Next
                End If

            Next

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.FindForm.Text)
        End Try

    End Sub




    Private Sub cmbDate_Initialise()

        'load non-closed periods into date selection but include today regardless
        Dim nonClosedPeriodIds As ArrayList = Banking.Core.GetNonClosedPeriodIds
        Dim periods As New Oasys.System.Periods
        Dim dt As DataTable = periods.GetPeriodsDataTable(nonClosedPeriodIds)
        Dim dv As DataView = New DataView(dt, "", Oasys.System.Periods.Column.Id.ToString & " desc", DataViewRowState.Added)
        cmbDate.DataSource = dv
        cmbDate.DisplayMember = Oasys.System.Periods.Column.Display.ToString
        cmbDate.ValueMember = Oasys.System.Periods.Column.Id.ToString

    End Sub

    Private Sub cmbCashierTill_Initialise()

        'get table of either cashiers or tills to populate combo box
        Select Case _pickup.AccountType
            Case Oasys.System.Accountabilities.Cashier
                grpCashierTill.Text = My.Resources.Messages.SelectCashier
                Dim users As New Oasys.Security.Users
                Dim dt As DataTable = users.GetSystemUsersDataTable(True)
                cmbCashierTill.ValueMember = Oasys.Security.Users.Column.Id.ToString
                cmbCashierTill.DisplayMember = Oasys.Security.Users.Column.Display.ToString
                cmbCashierTill.DataSource = dt

            Case Oasys.System.Accountabilities.Till
                grpCashierTill.Text = My.Resources.Messages.SelectTill
                Dim workstations As New Oasys.Security.Workstations
                Dim dt As DataTable = workstations.GetWorkstationsDataTable(True)
                cmbCashierTill.ValueMember = Oasys.Security.Workstations.Column.Id.ToString
                cmbCashierTill.DisplayMember = Oasys.Security.Workstations.Column.Display.ToString
                cmbCashierTill.DataSource = dt

        End Select

    End Sub

    Private Sub cmbCashierTill_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbCashierTill.KeyDown

        e.Handled = False
        Select Case e.KeyData
            Case Keys.Down, Keys.Up
                If cmbCashierTill.DroppedDown = False Then
                    cmbCashierTill.DroppedDown = True
                    e.Handled = True
                End If
        End Select

    End Sub

    Private Sub cmbCashierTill_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbCashierTill.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            cmbCashierTill.DroppedDown = False
            cmbCashierTill_SelectionChangeCommitted(sender, New EventArgs)
        End If

    End Sub

    Private Sub cmbCashierTill_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCashierTill.SelectionChangeCommitted, cmbDate.SelectionChangeCommitted

        Try
            If cmbCashierTill.SelectedIndex > 0 Then
                Cursor = Cursors.WaitCursor

                'load system figures
                _pickup.LoadSystemFigures(cmbDate.SelectedValue, cmbCashierTill.SelectedValue)

                'load safe records
                _bankingPeriod = New Banking.Core.BankingPeriod
                _bankingPeriod.LoadSafe(cmbDate.SelectedValue)

                spdSafe_Initialise()
                spdPrint_Initialise()
                spdSafe_AddRows()

                rdoReseal.Checked = True
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub rdo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoReseal.CheckedChanged, rdoReturn.CheckedChanged

        Select Case True
            Case rdoReseal.Checked
                _pickup.SetBagState(Banking.Core.BagStates.Sealed)
                txtSeal.Enabled = True
                btnAccept.Enabled = False

            Case rdoReturn.Checked
                _pickup.SetBagState(Banking.Core.BagStates.BackToSafe)
                txtSeal.Clear()
                txtSeal.Enabled = False
                btnAccept.Enabled = True
        End Select

    End Sub



    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress

        lblSealMessage.Visible = False
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave

        lblSealMessage.Text = ""

        'check that cancel button has not been clicked
        If Me.ActiveControl Is btnExit Then Exit Sub

        'check that seal number has been entered
        If Not txtSeal.MaskFull Then
            lblSealMessage.Text = My.Resources.Messages.SealMustBeEntered
            lblSealMessage.Visible = True
            txtSeal.Focus()
            Exit Sub
        End If

        'check that seal for float not entered
        If _float IsNot Nothing Then
            If txtSeal.Text = _float.NewSealNumber Then
                lblSealMessage.Text = My.Resources.Messages.SealInUse
                lblSealMessage.Visible = True
                txtSeal.SelectAll()
                txtSeal.Focus()
                btnAccept.Enabled = False
                Exit Sub
            End If
        End If

        'check that seal number is valid
        If Banking.Core.IsSealNumberValid(txtSeal.Text) Then
            btnAccept.Enabled = True
            _pickup.SetSealNumber(txtSeal.Text)
        Else
            lblSealMessage.Text = My.Resources.Messages.SealInUse
            lblSealMessage.Visible = True
            txtSeal.SelectAll()
            txtSeal.Focus()
            btnAccept.Enabled = False
        End If

    End Sub

    Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
    End Sub

    Private Sub txtComments_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComments.Leave
        _pickup.SetComments(txtComments.Text.Trim)
    End Sub



    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        Try
            DisplayStatus(My.Resources.Messages.BagSave)
            Cursor = Cursors.WaitCursor

            'set values from sheet to pickup new bag
            Dim sheet As SheetView = spdSafe.ActiveSheet
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If sheet.Cells(rowIndex, col.Denom).Value = "Float" Then
                    _pickup.SetFloatReturned(sheet.Cells(rowIndex, col.System).Value)
                    Continue For
                End If

                'check this is a denomination  row
                If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For

                'check for non zero amount and to pickup if true
                Dim actual As Decimal = sheet.Cells(rowIndex, col.Bag).Value
                If actual <> 0 Then
                    Dim denom As Oasys.Currency.CurrencyDenomination = sheet.Rows(rowIndex).Tag
                    _pickup.SetDenomination(denom.CurrencyId, denom.Id, denom.TenderId, actual)
                End If
            Next

            'save float if exists not taking float amount from safe and adding to cashier/till pickups
            If _float IsNot Nothing Then
                _float.SetAccountId(_pickup.AccountId)
                _float.SaveFromPickup()
                _float.AddPickupsToCashierTills(_pickup.CashierTills)
                _pickup.SetRelatedBagId(_float.NewId)
            End If

            'set comments and save
            _pickup.SetComments(txtComments.Text.Trim)
            _pickup.Save()

            DialogResult = Windows.Forms.DialogResult.OK
            Close()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Select Case btnPrint.Text
            Case My.Resources.Controls.F9Print
                spdSafe.PrintSheet(-1)

            Case Else
                Dim sheet As SheetView = spdSafe.ActiveSheet
                Dim print As SheetView = spdPrint.ActiveSheet

                'remove any existing rows and reset
                print.Rows.Remove(0, print.RowCount)
                print.RowCount = sheet.RowCount

                For rowIndex As Integer = 0 To sheet.RowCount - 1
                    'copy borders and font
                    print.Rows(rowIndex).Border = sheet.Rows(rowIndex).Border
                    print.Rows(rowIndex).Font = sheet.Rows(rowIndex).Font

                    'copy celltypes and values from sheet rows to print rows
                    For colIndex As Integer = 1 To sheet.ColumnCount - 1
                        print.Cells(rowIndex, colIndex).CellType = sheet.Cells(rowIndex, colIndex).CellType
                        print.Cells(rowIndex, colIndex).Value = sheet.Cells(rowIndex, colIndex).Value
                    Next
                Next

                spdPrint.PrintSheet(-1)
        End Select

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        'loop through rows resetting all values back to zero
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To (sheet.RowCount - 1)
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
            sheet.Cells(rowIndex, col.Bag).Value = sheet.Cells(rowIndex, col.Original).Value
        Next

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub


End Class