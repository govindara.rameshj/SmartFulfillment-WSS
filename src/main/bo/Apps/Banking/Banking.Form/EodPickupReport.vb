﻿Imports Banking.Core
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports OasysDBBO.Oasys3.DB
Imports FarPoint.Win.Spread
Imports FarPoint.Win
Imports FarPoint.PlusMinusCellType
Imports Microsoft.VisualBasic
Imports System.Diagnostics
Imports System.Collections
Imports Cts.Oasys.Core.System

Public Class EodPickupReport
    Private _bankingPeriod As Banking.Core.BankingPeriod
    Private _pickupReport As Banking.Core.PickupReport
    Private _commsAllowed As Boolean
    Private _accountType As String = String.Empty
    Private _storeIdName As String = String.Empty
    Private _pickupReportComments As IPickUpReportComments = (New PickUpReportCommentsFactory).GetImplementation

    Private _intSelectedPeriodID As Integer = 0

    Private Enum col
        Expand
        Denom
        Total
        System
        Variance
    End Enum

    Public Sub New(ByVal accountType As String, ByVal storeIdName As String)
        InitializeComponent()
        _storeIdName = storeIdName
        _accountType = accountType
    End Sub

    'new banking interface needs to select the period id
    Public Sub New(ByVal accountType As String, ByVal storeIdName As String, ByVal intSelectedPeriodID As Integer)
        InitializeComponent()

        _storeIdName = storeIdName
        _accountType = accountType
        _intSelectedPeriodID = intSelectedPeriodID
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F5 : btnComms.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor

            'check if btnComms can be used from parameter 2160
            _commsAllowed = Parameter.GetBoolean(2160)

            'get all safe records
            Dim dt As New DataTable
            Using bank As New Banking.Core.BankingPeriod
                dt = bank.GetSafeDatatable
            End Using

            Dim dv As DataView = New DataView(dt, "", Banking.Core.BankingPeriod.Column.Id.ToString & " desc", DataViewRowState.Added)
            cmbDate.DataSource = dv
            cmbDate.DisplayMember = Banking.Core.BankingPeriod.Column.Display.ToString
            cmbDate.ValueMember = Banking.Core.BankingPeriod.Column.Id.ToString

            cmbDate_SelectionChangeCommitted(Me, New EventArgs)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub



    Private Sub cmbDate_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDate.SelectionChangeCommitted

        'clear all sheets first
        spdSafe.Sheets.Clear()


        'if called from the new banking interface, the period id will already be know
        'existing (old) banking interface allows the user to select the period
        If _intSelectedPeriodID <> 0 Then
            cmbDate.SelectedValue = _intSelectedPeriodID

            _bankingPeriod = New Banking.Core.BankingPeriod
            _bankingPeriod.LoadSafe(_intSelectedPeriodID)

            _pickupReport = New Banking.Core.PickupReport(_accountType)
            _pickupReport.LoadSystemFigures(_intSelectedPeriodID)

            btnComms.Enabled = False
            btnComms.Visible = False

            cmbDate.Enabled = False
        Else
            'existing code

            'load safe bags for selected period
            _bankingPeriod = New Banking.Core.BankingPeriod
            _bankingPeriod.LoadSafe(CInt(cmbDate.SelectedValue))
            'load cashier/tills and add to report
            _pickupReport = New Banking.Core.PickupReport(_accountType)
            _pickupReport.LoadSystemFigures(CInt(cmbDate.SelectedValue))
            'check if safe is open to perform comms if not already disabled from parameter
            btnComms.Enabled = _commsAllowed AndAlso Not _bankingPeriod.IsClosed
        End If

        spdSafe_LoadSummary()
        spdSafe_LoadCashierTills()

        'resize all sheets
        spdSafe_Resize(spdSafe, New EventArgs)

    End Sub

    Private Sub sheetInitialise(ByRef sheet As SheetView, Optional ByVal bags As List(Of BOBanking.cSafeBags) = Nothing)

        'Column types
        Dim typeNumber As New CellType.NumberCellType
        typeNumber.DecimalPlaces = 2
        typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
        typeNumber.NegativeRed = True

        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.RowCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.RowHeader.ColumnCount = 0

        sheet.GrayAreaBackColor = Drawing.Color.White
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.SelectionBackColor = Drawing.Color.Transparent
        sheet.SelectionUnit = Model.SelectionUnit.Cell

        sheet.PrintInfo.Header = "/lStore: " & _storeIdName
        sheet.PrintInfo.Header &= "/c/fz""14""End of Day PickUp Report"
        sheet.PrintInfo.Header &= "/n/c/fz""10""" & sheet.SheetName
        sheet.PrintInfo.Header &= "/n/c/fz""10""Data for: " & _bankingPeriod.PeriodDate.ToShortDateString
        sheet.PrintInfo.Header &= "/n "
        sheet.PrintInfo.Orientation = PrintOrientation.Portrait
        sheet.PrintInfo.Margin.Left = 50
        sheet.PrintInfo.Margin.Right = 50
        sheet.PrintInfo.Margin.Top = 20
        sheet.PrintInfo.Margin.Bottom = 20
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.UseMax = False
        sheet.PrintInfo.ShowPrintDialog = False

        'Column headers.
        sheet.ColumnHeader.VerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.HorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.DefaultStyle.Font = New Font(FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
        sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Color.Black, 1, False, False, False, True)

        'Columns
        sheet.Columns(col.Expand).Label = " "
        sheet.Columns(col.Expand).Width = 20
        sheet.Columns(col.Expand).Resizable = False
        sheet.Columns(col.Denom).Label = " "
        sheet.Columns(col.Total, col.Variance).CellType = typeNumber
        sheet.Columns(col.Total).Label = "Total"
        sheet.Columns(col.System).Label = "System"
        sheet.Columns(col.Variance).Label = "Variance"

        'add bag columns
        If bags IsNot Nothing Then
            sheet.ColumnCount += bags.Count
            For index As Integer = [Enum].GetValues(GetType(col)).Length To sheet.ColumnCount - 1
                sheet.Columns(index).Label = "PickUp " & index - [Enum].GetValues(GetType(col)).Length + 1
                sheet.Columns(index).HorizontalAlignment = CellHorizontalAlignment.Right
                sheet.Columns(index).CellType = typeNumber
            Next
        End If

    End Sub

    Private Sub sheetAddHeader(ByRef sheet As SheetView, ByVal bags As List(Of BOBanking.cSafeBags))

        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length

        'add previous bag details - time
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Bag Details"
        For index As Integer = colLength To sheet.ColumnCount - 1
            sheet.Cells(sheet.RowCount - 1, index).CellType = New CellType.GeneralCellType
            sheet.Cells(sheet.RowCount - 1, index).Value = Format(bags(index - colLength).InDate.Value, "HH:mm")
        Next

        'add previous bag details - seal number
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Seal Number"
        For index As Integer = colLength To sheet.ColumnCount - 1
            sheet.Cells(sheet.RowCount - 1, index).CellType = New CellType.GeneralCellType
            sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).SealNumber.Value
        Next


        'add previous bag details - users in/out
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Users (In) / (Out)"
        For index As Integer = colLength To sheet.ColumnCount - 1
            Dim sb As New StringBuilder("(" & bags(index - colLength).InUserID1.Value.ToString("000") & Space(1))
            sb.Append(bags(index - colLength).InUserID2.Value.ToString("000") & ")")
            If bags(index - colLength).OutUserID1.Value <> 0 Then
                sb.Append(" / (" & bags(index - colLength).OutUserID1.Value.ToString("000") & Space(1))
                sb.Append(bags(index - colLength).OutUserID2.Value.ToString("000") & ")")
            End If

            sheet.Cells(sheet.RowCount - 1, index).CellType = New CellType.GeneralCellType
            sheet.Cells(sheet.RowCount - 1, index).Value = sb.ToString
        Next


        'add previous bag details - pickup period id
        'Dim period As New BOSystem.cSystemPeriods(_safe.Oasys3DB)
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = _pickupReportComments.GetTradingDayDescription
        For index As Integer = colLength To sheet.ColumnCount - 1
            sheet.Cells(sheet.RowCount - 1, index).CellType = New CellType.GeneralCellType
            sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).PickupPeriodID.Value & " - " & Period.GetPeriod(bags(index - colLength).PickupPeriodID.Value).StartDate

            'highlight column if not this trading day
            If bags(index - colLength).PickupPeriodID.Value <> _bankingPeriod.PeriodId Then
                sheet.Columns(index).BackColor = Color.LightGray
            End If
        Next

        _pickupReportComments.DisplayPickUpReportComments(sheet, colLength, bags)

        'add border
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

    End Sub

    Private Sub sheetAddCurrencyHeader(ByRef sheet As SheetView, ByVal currency As Currency.Header)

        'add currency header marking the row tag as nothing as is a header
        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)

        'add expand button
        Dim btn As PlusMinusCellType = Nothing
        Select Case currency.IsDefault
            Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
            Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
        End Select

        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
        sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Currency (" & currency.Symbol & ")"
        sheet.Cells(sheet.RowCount - 1, col.Denom).Tag = -1

    End Sub

    Private Sub sheetAddCashDenomsWithTotals(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal bags As List(Of BOBanking.cSafeBags))

        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length

        For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId = 1)
            sheet.Rows.Add(sheet.RowCount, 1)
            sheet.Rows(sheet.RowCount - 1).Tag = denom
            sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
            sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
            sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
            sheet.Cells(sheet.RowCount - 1, col.System).Value = 0

            'add total values
            For Each bag As BOBanking.cSafeBags In bags
                sheet.Cells(sheet.RowCount - 1, col.Total).Value = CDec(sheet.Cells(sheet.RowCount - 1, col.Total).Value) + bag.Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
            Next

            'if not summary sheet then add bag total to column
            If sheet.ColumnCount > colLength Then
                For index As Integer = colLength To sheet.ColumnCount - 1
                    sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
                Next
            End If
        Next

        'insert line border to mark end of cash
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

    End Sub

    Private Sub sheetAddNonCashDenomsWithTotals(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal denom As Currency.Line, ByVal bags As List(Of BOBanking.cSafeBags))

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = denom
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
        sheet.Cells(sheet.RowCount - 1, col.Total).Value = 0
        sheet.Cells(sheet.RowCount - 1, col.System).Value = 0
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "RC[-2]-RC[-1]"

        'add total values
        For Each bag As BOBanking.cSafeBags In bags
            sheet.Cells(sheet.RowCount - 1, col.Total).Value = CDec(sheet.Cells(sheet.RowCount - 1, col.Total).Value) + bag.Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
        Next

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).Denom(currency.Id, denom.Id, denom.TenderId).Value.Value
            Next
        End If

    End Sub

    Private Sub sheetAddPickupTotal(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal cashFormula As String, ByVal cashSystem As Decimal)

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Pickup Total"
        sheet.Cells(sheet.RowCount - 1, col.Total).Formula = cashFormula
        sheet.Cells(sheet.RowCount - 1, col.System).Value = cashSystem
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "ROUND(RC[-2]-RC[-1],2)"

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Formula = cashFormula
            Next
        End If

    End Sub

    Private Sub sheetAddFloat(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal floatSystem As Decimal, ByVal bags As List(Of BOBanking.cSafeBags))

        Dim float As Decimal = 0
        For Each bag As BOBanking.cSafeBags In bags
            If bag.RelatedBag IsNot Nothing Then
                float += bag.RelatedBag.Value.Value
            End If
        Next

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Float"
        sheet.Cells(sheet.RowCount - 1, col.Total).Value = float
        sheet.Cells(sheet.RowCount - 1, col.System).Value = floatSystem
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "ROUND(RC[-2]-RC[-1],2)"

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                If bags(index - colLength).RelatedBag IsNot Nothing Then
                    sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).RelatedBag.Value.Value
                Else
                    sheet.Cells(sheet.RowCount - 1, index).Value = 0
                End If
            Next
        End If

    End Sub

    Private Sub sheetAddFloatSealNumber(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal bags As List(Of BOBanking.cSafeBags))

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Float Seal Number"

        'set up mask cell type
        Dim typeMask As New CellType.MaskCellType
        typeMask.Mask = "000-00000000-0"

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                If bags(index - colLength).RelatedBag IsNot Nothing Then
                    sheet.Cells(sheet.RowCount - 1, index).CellType = typeMask
                    sheet.Cells(sheet.RowCount - 1, index).HorizontalAlignment = CellHorizontalAlignment.Right
                    sheet.Cells(sheet.RowCount - 1, index).Value = bags(index - colLength).RelatedBag.SealNumber.Value
                End If
            Next
        End If

    End Sub

    Private Sub sheetAddCashTotal(ByRef sheet As SheetView, ByVal currency As Currency.Header, ByVal totalFormula As String)

        sheet.Rows.Add(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = Nothing
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
        sheet.Rows(sheet.RowCount - 1).Locked = True
        sheet.Rows(sheet.RowCount - 1).CanFocus = False
        sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
        sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currency.Id
        sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Cash Total"
        sheet.Cells(sheet.RowCount - 1, col.Total).Formula = totalFormula
        sheet.Cells(sheet.RowCount - 1, col.System).Formula = totalFormula
        sheet.Cells(sheet.RowCount - 1, col.Variance).Formula = "ROUND(RC[-2]-RC[-1],2)"

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Formula = totalFormula
            Next
        End If

    End Sub

    Private Sub sheetAddCurrencyTotals(ByRef sheet As SheetView, ByVal rowCurrency As Integer, ByVal rowTotals As Integer)

        Dim formula As String = "SUM(R" & rowTotals + 1 & "C:R" & sheet.RowCount & "C)"
        sheet.Cells(rowCurrency, col.Total).Formula = formula
        sheet.Cells(rowCurrency, col.System).Formula = formula
        sheet.Cells(rowCurrency, col.Variance).Formula = "RC[-2]-RC[-1]"
        sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

        'if not summary sheet then add bag total to column
        Dim colLength As Integer = [Enum].GetValues(GetType(col)).Length
        If sheet.ColumnCount > colLength Then
            For index As Integer = colLength To sheet.ColumnCount - 1
                sheet.Cells(sheet.RowCount - 1, index).Formula = formula
            Next
        End If

    End Sub

    Private Sub spdSafe_LoadSummary()

        'initialise sheet
        Dim sheet As New SheetView
        sheet.SheetName = "Summary"
        sheetInitialise(sheet)

        Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
        For Each currency As Currency.Header In currencies
            Dim rowCurrency As Integer = sheet.RowCount
            sheetAddCurrencyHeader(sheet, currency)
            sheetAddCashDenomsWithTotals(sheet, currency, _pickupReport.PickupBags)

            Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"
            sheetAddPickupTotal(sheet, currency, formula, _pickupReport.CashTendered(currency.Id))

            'insert float for default currency only
            Dim pickupRow As Integer = sheet.RowCount
            formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"
            If currency.IsDefault Then
                sheetAddFloat(sheet, currency, _pickupReport.FloatsIssued(currency.Id), _pickupReport.PickupBags)
                formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"
            End If

            'insert cash total and cash system value (with change)
            Dim rowTotals As Integer = sheet.RowCount
            sheetAddCashTotal(sheet, currency, formula)

            'add other tenders now and system value
            For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId <> 1)
                sheetAddNonCashDenomsWithTotals(sheet, currency, denom, _pickupReport.PickupBags)
                sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.NonCashTendered(currency.Id, denom.TenderId)
            Next

            sheetAddCurrencyTotals(sheet, rowCurrency, rowTotals)
        Next

        'add sheet to spreadsheet control
        spdSafe.Sheets.Add(sheet)

    End Sub

    Private Sub spdSafe_LoadCashierTills()

        For Each cashTill As Core.CashierTill In _pickupReport.CashierTills
            Dim id As Integer = cashTill.Id
            Dim bags As List(Of BOBanking.cSafeBags) = _pickupReport.PickupBags.FindAll(Function(f) f.AccountabilityID.Value = id)
            Dim sheet As New SheetView
            sheet.SheetName = _pickupReport.CashierTills.First(Function(f) f.Id = id).Name

            'add bag details
            sheetInitialise(sheet, bags)
            sheetAddHeader(sheet, bags)

            Dim currencies As Currency.HeaderCollection = Currency.GetCurrencies
            For Each currency As Currency.Header In currencies
                Dim rowCurrency As Integer = sheet.RowCount
                sheetAddCurrencyHeader(sheet, currency)
                sheetAddCashDenomsWithTotals(sheet, currency, bags)

                Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"
                sheetAddPickupTotal(sheet, currency, formula, _pickupReport.CashTendered(currency.Id, id))

                'insert float for default currency only
                Dim pickupRow As Integer = sheet.RowCount
                formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"
                If currency.IsDefault Then
                    sheetAddFloat(sheet, currency, _pickupReport.FloatsIssued(currency.Id, id), bags)
                    sheetAddFloatSealNumber(sheet, currency, bags)
                    formula = "SUM(R" & pickupRow & "C:R" & sheet.RowCount & "C)"
                End If

                'insert cash total and cash system value (with change)
                Dim rowTotals As Integer = sheet.RowCount
                sheetAddCashTotal(sheet, currency, formula)

                'add other tenders now and system value
                For Each denom As Currency.Line In currency.Lines.Where(Function(c) c.TenderId <> 1)
                    sheetAddNonCashDenomsWithTotals(sheet, currency, denom, bags)
                    sheet.Cells(sheet.RowCount - 1, col.System).Value = _pickupReport.NonCashTendered(currency.Id, denom.TenderId, id)
                Next

                sheetAddCurrencyTotals(sheet, rowCurrency, rowTotals)
            Next

            'add sheet to spreadsheet control
            spdSafe.Sheets.Add(sheet)
        Next

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spdSafe_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check that are sheets available
        If spread.Sheets.Count = 0 Then Exit Sub

        'get tab strip and border style sizes
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim spreadWidth As Single = spread.Width
        Dim spreadHeight As Single = spread.Height

        'make allowance for tab strip and border
        If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            spreadHeight -= (borderHeight * 2)
            spreadWidth -= (borderWidth * 2)
        End If


        For Each sheet As SheetView In spread.Sheets
            'check sheet has any rows first
            If sheet.RowCount = 0 Then Continue For

            Dim sheetHeight As Single = spreadHeight
            Dim sheetWidth As Single = spreadWidth

            'Get row header widths
            If sheet.Visible = False Then Continue For
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                Next
            End If

            'Get column header heights
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                Next
            End If

            'get rowsHeight and colsWidth
            Dim rowsHeight As Single = sheet.RowCount * sheet.Rows.Default.Height
            Dim colsWidth As Single = 0
            For Each col As Column In sheet.Columns
                If col.Visible Then colsWidth += col.Width
            Next

            'check whether need scrollbars
            If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

            If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'check whether need horizontal scrollbar
            If colsTotal > sheetWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                'if resizing then fill up empty space

                If colsResizable = 0 Then Continue For
                Dim increase As Integer = CInt((sheetWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
            _pickupReportComments.ResetCommentsRowHeight(sheet)
        Next
    End Sub

    Private Sub btnComms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComms.Click

        ''ask user if all pickups returned
        'Dim result As DialogResult = MessageBox.Show(My.Resources.Messages.YesNoAllPickupsEntered, Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        'If result <> Windows.Forms.DialogResult.Yes Then
        '    Exit Sub
        'End If

        'check that there are no outstanding uncommed control headers
        Dim message As String = _bankingPeriod.Safe.GetOutstandingPeriods
        If message.Length > 0 Then
            MessageBox.Show(message, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim PeriodId As Integer = _bankingPeriod.PeriodId
        'check that all pickup bags have been entered
        If Not _bankingPeriod.AllPickupsToSafe(PeriodId) Then
            MessageBox.Show(My.Resources.Messages.OpenPickups, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'check that banking bags been created and checked
        message = String.Empty
        If Not _bankingPeriod.BankingOkToComm(_bankingPeriod.PeriodId, message) Then
            MessageBox.Show(message, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        _bankingPeriod.Safe.ClosePeriod()

        'do process transmissions
        Dim sb As New StringBuilder
        sb.Append(Application.StartupPath)
        sb.Append("\ProcessTransmissions.exe DB PDATE=")
        sb.Append(_bankingPeriod.Safe.PeriodDate.Value.ToString("dd/MM/yy"))

        Trace.WriteLine(sb.ToString, Me.GetType.ToString)
        Using proc As New Process
            proc.StartInfo.FileName = Application.StartupPath & "\ProcessTransmissions.exe"
            proc.StartInfo.Arguments = "DB PDATE=" & _bankingPeriod.Safe.PeriodDate.Value.ToString("dd/MM/yy")
            proc.Start()
            proc.WaitForExit()
        End Using

        'Process.Start(Application.StartupPath & "\ProcessTransmissions.exe", "DB PDATE=" & _bankingPeriod.Safe.PeriodDate.Value.ToString("dd/MM/yy"))

        'disable button
        MessageBox.Show(My.Resources.Messages.CommsPrepared, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        btnComms.Enabled = False


    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim dialog As New PrintDialog
        If dialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim nowTime As Date = Now
            Dim numDetails As Integer = spdSafe.Sheets.Count - 1
            Dim type As String = String.Empty

            Select Case _pickupReport.GetAccountType
                Case AccountabilityTypes.Cashier : type = "Cashier"
                Case AccountabilityTypes.Till : type = "Till"
            End Select

            'add printer details and footer to each sheet
            For index As Integer = 0 To numDetails
                spdSafe.Sheets(index).PrintInfo.Printer = dialog.PrinterSettings.PrinterName
                spdSafe.Sheets(index).PrintInfo.Footer = "/lPrinted: " & nowTime

                If index = 0 Then
                    Dim sb As New StringBuilder()
                    sb.Append("/cSummary (" & numDetails & Space(1) & type)
                    If numDetails > 1 Then sb.Append("s")
                    sb.Append(")    Page /p of /pc")
                    spdSafe.Sheets(index).PrintInfo.Footer &= sb.ToString
                Else
                    Dim sb As New StringBuilder()
                    sb.Append("/c" & type & Space(1) & index & " of " & numDetails)
                    sb.Append("    Page /p of /pc")
                    spdSafe.Sheets(index).PrintInfo.Footer &= sb.ToString
                End If

                spdSafe.Sheets(index).PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            Next

            'print sheets individually
            spdSafe.PrintSheet(-1)
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub


End Class