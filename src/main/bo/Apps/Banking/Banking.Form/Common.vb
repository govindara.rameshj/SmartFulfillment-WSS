﻿Imports Farpoint.Win

Module Common

    <System.Runtime.CompilerServices.Extension()> Function VisibleRows(ByRef Rows As Spread.Rows) As Decimal

        Dim count As Integer = 0
        For Each r As Spread.Row In Rows
            If r.Visible Then count += 1
        Next
        Return count

    End Function

    <System.Runtime.CompilerServices.Extension()> Sub Visible(ByRef Rows As Spread.Rows, ByVal Visible As Boolean)

        For Each r As Spread.Row In Rows
            r.Visible = Visible
        Next

    End Sub

    Public Function BagTypeString(ByVal bagtype As String) As String
        Select Case bagtype
            Case Banking.Core.BagTypes.Banking : Return My.Resources.Strings.BagBanking
            Case Banking.Core.BagTypes.Float : Return My.Resources.Strings.BagFloat
            Case Banking.Core.BagTypes.GiftToken : Return My.Resources.Strings.BagGift
            Case Banking.Core.BagTypes.Pickup : Return My.Resources.Strings.BagPickup
            Case Banking.Core.BagTypes.Property : Return My.Resources.Strings.BagProperty
            Case Else : Return String.Empty
        End Select
    End Function

End Module