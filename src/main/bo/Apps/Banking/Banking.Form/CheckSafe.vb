﻿Imports System
Imports System.Diagnostics
Imports System.Windows.Forms
Imports Microsoft.VisualBasic
Imports FarPoint.Win.Spread
Imports FarPoint.PlusMinusCellType
Imports FarPoint.Win

Public Class CheckSafe
    Private Enum col
        Expand
        Denom
        System
        Main
        Change
    End Enum
    Private _bankingPeriod As Banking.Core.BankingPeriod
    Private _originalCellValue As Decimal = 0

    Public Sub New(ByVal userID1 As Integer, ByVal userID2 As Integer, ByVal periodId As Integer)
        InitializeComponent()

        'load safe bag and assign new user ids
        _bankingPeriod = New Banking.Core.BankingPeriod
        _bankingPeriod.LoadSafe(periodId)
        _bankingPeriod.SetSafeUser1(userID1)
        _bankingPeriod.SetSafeUser2(userID2)
    End Sub

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor

            spdSafe_Initialise()
            spdSafe_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub DisplayStatus(Optional ByVal text As String = Nothing, Optional ByVal warn As Boolean = False)

        lblStatus.BackColor = Drawing.SystemColors.Control
        If text = Nothing Then
            lblStatus.Text = ""
            lblStatus.ToolTipText = ""
        Else
            If warn Then lblStatus.BackColor = Drawing.Color.Yellow
            lblStatus.Text = text
            lblStatus.ToolTipText = text
        End If
        stsStatus.Refresh()

    End Sub



    Private Sub spdSafe_Initialise()

        Try
            'input maps
            Dim im As InputMap = spdSafe.GetInputMap(InputMapMode.WhenAncestorOfFocused)
            im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            Dim imFocused As InputMap = spdSafe.GetInputMap(InputMapMode.WhenFocused)
            imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)


            Dim sheet As New SheetView
            sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
            sheet.RowCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
            sheet.ColumnHeader.RowCount = 1
            sheet.RowHeader.ColumnCount = 0

            sheet.GrayAreaBackColor = Drawing.Color.White
            sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
            sheet.DefaultStyle.CellType = New CellType.TextCellType
            sheet.DefaultStyle.Locked = True
            sheet.SelectionBackColor = Drawing.Color.Transparent
            sheet.SelectionUnit = Model.SelectionUnit.Cell


            'Column types.
            Dim typeNumber As New CellType.NumberCellType
            typeNumber.DecimalPlaces = 2
            typeNumber.LeadingZero = CellType.LeadingZero.UseRegional
            typeNumber.NegativeRed = True

            'Columns.
            sheet.Columns(col.Expand).Label = " "
            sheet.Columns(col.Expand).Width = 20
            sheet.Columns(col.Expand).Resizable = False
            sheet.Columns(col.Denom).Label = " "
            sheet.Columns(col.System).Label = "System"
            sheet.Columns(col.Main).Label = "Main Safe"
            sheet.Columns(col.Change).Label = "Change Safe"
            sheet.Columns(col.System, col.Change).CellType = typeNumber
            sheet.Columns(col.Main, col.Change).Locked = False

            spdSafe.Sheets.Add(sheet)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdSafe_AddRows()

        Try
            Dim sheet As SheetView = spdSafe.ActiveSheet

            For Each currency As Oasys.Currency.Currency In Oasys.Currency.GetCurrencies
                Dim currencyId As String = currency.Id
                Dim rowCurrency As Integer = sheet.RowCount

                'add header
                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = Nothing
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.25, Drawing.FontStyle.Bold)
                sheet.Rows(sheet.RowCount - 1).Locked = True

                'add expand button and currency text
                Dim btn As PlusMinusCellType = Nothing
                Select Case currency.IsDefault
                    Case True : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Minus)
                    Case False : btn = New PlusMinusCellType(AddressOf spdSafe_Button, PlusMinusCellType.States.Plus)
                End Select
                sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                sheet.Cells(sheet.RowCount - 1, col.Expand).CellType = btn
                sheet.Cells(sheet.RowCount - 1, col.Expand).Locked = False
                sheet.Cells(sheet.RowCount - 1, col.Denom).Value = "Safe(" & currency.Symbol & ")"

                'add system currency denominations and system and safe bag values
                For Each denom As Oasys.Currency.CurrencyDenomination In currency.Denominations
                    Dim denominationId As Decimal = denom.Id
                    Dim tenderId As Integer = denom.TenderId

                    sheet.Rows.Add(sheet.RowCount, 1)
                    sheet.Rows(sheet.RowCount - 1).Tag = denom
                    sheet.Rows(sheet.RowCount - 1).Visible = currency.IsDefault
                    sheet.Cells(sheet.RowCount - 1, col.Expand).Tag = currencyId
                    sheet.Cells(sheet.RowCount - 1, col.Denom).Value = denom.DisplayText
                    sheet.Cells(sheet.RowCount - 1, col.System).Value = _bankingPeriod.SafeSystem(currencyId, denominationId, tenderId)
                    sheet.Cells(sheet.RowCount - 1, col.Main).Value = _bankingPeriod.SafeMain(currencyId, denominationId, tenderId)
                    sheet.Cells(sheet.RowCount - 1, col.Change).Value = _bankingPeriod.SafeChange(currencyId, denominationId, tenderId)
                Next

                'insert currency totals
                Dim formula As String = "SUM(R" & rowCurrency + 2 & "C:R" & sheet.RowCount & "C)"
                sheet.Cells(rowCurrency, col.System).Formula = formula
                sheet.Cells(rowCurrency, col.Main).Formula = formula
                sheet.Cells(rowCurrency, col.Change).Formula = formula
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            Next

            'resize if any rows added and set active cell
            If sheet.RowCount > 0 Then
                spd_Resize(spdSafe, New EventArgs)
                sheet.SetActiveCell(1, col.Main)
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdSafe_EditModeOn(ByVal sender As Object, ByVal e As System.EventArgs) Handles spdSafe.EditModeOn

        _originalCellValue = CDec(spdSafe.ActiveSheet.ActiveCell.Value)

    End Sub

    Private Sub spdSafe_LeaveCell(ByVal sender As Object, ByVal e As Farpoint.Win.Spread.LeaveCellEventArgs) Handles spdSafe.LeaveCell

        DisplayStatus()

        'check that row is not an expand row and exit if true
        Dim sheet As SheetView = spdSafe.ActiveSheet
        If sheet.Rows(sheet.ActiveRowIndex).Tag Is Nothing Then Exit Sub


        Select Case sheet.ActiveColumnIndex
            Case col.Main, col.Change
                Dim value As Decimal = CDec(sheet.ActiveCell.Value)
                Select Case value
                    Case 0
                        Exit Sub
                    Case Is < 0
                        DisplayStatus(My.Resources.Errors.WarnQtyCannotBeNegative, True)
                        sheet.Cells(sheet.ActiveRowIndex, sheet.ActiveColumnIndex).Value = _originalCellValue
                        e.Cancel = True

                    Case Else
                        'Check that entered value is a multiple of denomination.
                        Dim denom As Oasys.Currency.CurrencyDenomination = CType(sheet.Rows(sheet.ActiveRowIndex).Tag, Oasys.Currency.CurrencyDenomination)
                        If Not denom.IsMultiple(value) Then
                            DisplayStatus(My.Resources.Errors.WarnQtyMultipleDenom, True)
                            sheet.Cells(sheet.ActiveRowIndex, sheet.ActiveColumnIndex).Value = _originalCellValue
                            e.Cancel = True
                        End If

                End Select
        End Select

    End Sub

    Private Sub spdSafe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdSafe.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim sheet As SheetView = spdSafe.ActiveSheet
            spdSafe_LeaveCell(sender, New LeaveCellEventArgs(New SpreadView(spdSafe), sheet.ActiveRowIndex, sheet.ActiveColumnIndex, sheet.ActiveRowIndex, sheet.ActiveColumnIndex))
        End If

    End Sub

    Private Sub spdSafe_Button(ByVal sender As Object, ByVal state As PlusMinusCellType.States)

        Dim sheet As SheetView = spdSafe.ActiveSheet
        Dim btnTag As String = sheet.ActiveCell.Tag.ToString
        Dim visible As Boolean = (state = PlusMinusCellType.States.Plus)

        For rowIndex As Integer = (sheet.ActiveRowIndex + 1) To (sheet.RowCount - 1)
            If sheet.Cells(rowIndex, sheet.ActiveColumnIndex).Tag.ToString = btnTag Then
                sheet.Rows(rowIndex).Visible = visible
            Else
                Exit Sub
            End If
        Next

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdSafe.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check that are sheets available
        If spread.Sheets.Count = 0 Then Exit Sub

        'get tab strip and border style sizes
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim spreadWidth As Single = spread.Width
        Dim spreadHeight As Single = spread.Height

        'make allowance for tab strip and border
        If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            spreadHeight -= (borderHeight * 2)
            spreadWidth -= (borderWidth * 2)
        End If


        For Each sheet As SheetView In spread.Sheets
            'check sheet has any rows first
            If sheet.RowCount = 0 Then Continue For

            Dim sheetHeight As Single = spreadHeight
            Dim sheetWidth As Single = spreadWidth

            'Get row header widths
            If sheet.Visible = False Then Continue For
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                Next
            End If

            'Get column header heights
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                Next
            End If

            'get rowsHeight and colsWidth
            Dim rowsHeight As Integer = CInt(sheet.RowCount * sheet.Rows.Default.Height)
            Dim colsWidth As Single = 0
            For Each col As Column In sheet.Columns
                If col.Visible Then colsWidth += col.Width
            Next

            'check whether need scrollbars
            If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

            If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'check whether need horizontal scrollbar
            If colsTotal > sheetWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                'if resizing then fill up empty space
                If colsResizable = 0 Then Continue For
                Dim increase As Integer = CInt(Math.Floor((sheetWidth - colsTotal) / colsResizable))
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If

        Next

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            Dim sheet As SheetView = spdSafe.ActiveSheet
            Dim system As Decimal = 0
            Dim total As Decimal = 0
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
                system += CDec(sheet.Cells(rowIndex, col.System).Value)
                total += CDec(sheet.Cells(rowIndex, col.Main).Value)
                total += CDec(sheet.Cells(rowIndex, col.Change).Value)
            Next

            'Check totals are equal
            If system <> total Then
                DisplayStatus(My.Resources.Errors.WarnSystemActualEquate, True)
                Exit Sub
            End If

            DisplayStatus(My.Resources.Messages.BagSave)
            Cursor = Cursors.WaitCursor

            'Get new denom values
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
                Dim denom As Oasys.Currency.CurrencyDenomination = CType(sheet.Rows(rowIndex).Tag, Oasys.Currency.CurrencyDenomination)
                Dim currencyId As String = denom.CurrencyId
                Dim denominationId As Decimal = denom.Id
                Dim tenderId As Integer = denom.TenderId

                Dim safeMain As Decimal = CDec(sheet.Cells(rowIndex, col.Main).Value)
                Dim safeChange As Decimal = CDec(sheet.Cells(rowIndex, col.Change).Value)

                _bankingPeriod.SafeMain(currencyId, denominationId, tenderId) = safeMain
                _bankingPeriod.SafeChange(currencyId, denominationId, tenderId) = safeChange
                _bankingPeriod.SafeSystem(currencyId, denominationId, tenderId) = safeMain + safeChange
            Next

            'update safe and commit
            _bankingPeriod.UpdateSafe()

            DialogResult = Windows.Forms.DialogResult.OK
            Close()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        'loop through rows clearing all entered values
        Dim sheet As SheetView = spdSafe.ActiveSheet
        For rowIndex As Integer = 0 To (sheet.RowCount - 1)
            If sheet.Rows(rowIndex).Tag Is Nothing Then Continue For
            sheet.Cells(rowIndex, col.Main).Value = 0
            sheet.Cells(rowIndex, col.Change).Value = 0
        Next

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub


End Class