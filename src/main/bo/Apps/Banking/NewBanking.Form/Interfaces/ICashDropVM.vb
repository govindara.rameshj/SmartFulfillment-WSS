﻿Public Interface ICashDropVM

    Sub CashDropGridPopulate()
    Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.Pickup)

    Sub SetCashDropGridControl(ByRef CashDropGrid As FpSpread)
    Sub CashDropGridFormat()
    Sub SetCashDropForm(ByRef CashDropForm As System.Windows.Forms.Form)
    Sub CashDropFormFormat()

    Function CashDropGridActiveCellIsCommentsCell() As Boolean
    Function GetCashDropGridTotalsRowIndex() As Integer
End Interface
