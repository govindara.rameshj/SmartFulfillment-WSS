﻿Public Interface IUnFloatedPickupVM

    Function GetUnFloatedPickupList(ByVal BankingPeriodID As Integer) As UnFloatedPickupListCollection
    Sub UnFloatedPickupGridPopulate(ByRef UnFloatedPickupLists As UnFloatedPickupListCollection)
    Sub SetUnFloatedPickupGridControl(ByRef UnFloatedPickupGrid As FpSpread)
    Sub UnFloatedPickupGridFormat()
End Interface
