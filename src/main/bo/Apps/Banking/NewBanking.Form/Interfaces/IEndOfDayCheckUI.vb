﻿Public Interface IEndOfDayCheckUI

    Function ButtonCaptionName() As String

    Function ButtonEnabled(ByVal CurrentDate As Date, ByVal SelectedDate As Date, ByVal EndOfDayCheckDone As Boolean) As Boolean

    Function ExecuteEndOfDayCheckProcessUI() As Boolean

End Interface