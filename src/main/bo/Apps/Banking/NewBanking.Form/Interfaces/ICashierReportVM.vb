﻿Public Interface ICashierReportVM

    Sub SetCashierReportDisplayGrid(ByRef DisplayGrid As FarPoint.Win.Spread.FpSpread)
    Sub SetCashierReportCurrentSheet(ByRef CurrentSheet As FarPoint.Win.Spread.SheetView)
    Sub DisplayCashierOnCurrentSheet(ByRef Cashier As ActiveCashierList)
    Sub CashierReportFormFormat()
    Sub SetCashierReportForm(ByRef CashierReportForm As System.Windows.Forms.Form)
End Interface
