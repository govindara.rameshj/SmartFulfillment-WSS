﻿Public Interface IBankingPickupCheckVM

    Sub PersistCommentsIfChanged(ByVal PickupBagID As Integer)

    Sub SetBankingPickupCheckGridControl(ByRef BankingPickupCheckGridControl As FpSpread)
    Sub SetBankingPickupCheckForm(ByRef BankingPickupCheckForm As System.Windows.Forms.Form)
    Sub BankingPickupCheckGridFormat()
    Sub BankingPickupCheckFormFormat()
    Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer)
    Sub BankingPickupCheckGridPopulate(ByVal ActiveCashierList As ActiveCashierListCollection)
    Function GetActiveCashierListCollection() As ActiveCashierListCollection
    Function ActiveCellIsCommentsCell() As Boolean
    Sub ReadyActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)
    Sub UndoReadinessOfActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)
    Function GetComments(ByVal PickupBagID As Integer) As String
    Function UseGenericCommentForAllBankingBags() As Boolean
    Function GetCommentsRowIndex() As Integer

    Function BankingVariance() As Decimal

End Interface