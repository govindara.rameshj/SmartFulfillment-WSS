﻿Public Interface IFloatAssignVM

    Sub FloatAssignGridPopulate(ByVal FloatID As Integer)
    Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.FloatBanking)
    Sub FloatChecked(ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer)

    Sub SetFloatAssignGridControl(ByRef FloatAssignGrid As FpSpread)
    Sub FloatAssignGridFormat()
    Sub FloatAssignGridSetupCommentsCell(ByVal MakeEditable As Boolean)
    Sub SetFloatAssignForm(ByRef FloatAssignForm As System.Windows.Forms.Form)
    Sub FloatAssignFormFormat()
    Function FloatAssignGridActiveCellIsCommentsCell() As Boolean
    Function GetFloatAssignGridTotalsRowIndex() As Integer
    Function GetCommentsFromGrid() As String
End Interface
