﻿Public Interface ISafeCheckValidation

    Function AllowSafeCheckCompletion() As Boolean

    Sub SaveSafeCheckUserDetails(ByRef OdbcConnection As clsOasys3DB, _
                                 ByVal selectedPeriod As Period, _
                                 ByVal UserId1 As Integer, _
                                 ByVal UserId2 As Integer, _
                                 ByVal LastAmended As Date)

End Interface
