﻿Public Interface IEndOfDayCheckSummaryVM

    Property SheetView() As SheetView

    Sub Summary(ByRef Grid As FpSpread, ByVal PeriodID As Integer)
    Sub SummaryPrint(ByRef Grid As FpSpread, ByVal PeriodID As Integer)

End Interface