﻿Public Interface ISafeMaintenance

    Event DataChange()

    Sub ShowDialog(ByRef SafeMaintenence As NewBanking.Form.Main)
End Interface
