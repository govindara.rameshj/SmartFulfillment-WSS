﻿Public Interface IEndOfDayCheckVM

    'engine
    Sub Initialise(ByVal PeriodID As Integer)

    Sub AdditionalInformation()

    Sub CreatePickupBag(ByVal SealNumber As String)
    Sub CreateBankingBag(ByVal SealNumber As String)
    Sub CreatePickupBag(ByVal SealNumber As String, ByVal SealComment As String)
    Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String)

    Sub CreatePickupComment(ByVal Value As String)
    Sub CreateBankingComment(ByVal Value As String)

    Function PickupBagAlreadyScanned(ByVal SealNumber As String) As Boolean
    Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean

    Function PickupBagHasValidSeal(ByVal SealNumber As String) As Boolean
    Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean

    Function PickupBagExistInSafe(ByVal SealNumber As String) As Boolean
    Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean

    Function MatchSystemPickupExpectation() As Boolean
    Function MatchSystemBankingExpectation() As Boolean

    Function Persist() As Boolean

    Function PickupBagList() As List(Of ISafeBagScannedModel)
    Function BankingBagList() As List(Of ISafeBagScannedModel)

    Function PickupCommentList() As List(Of ISafeCommentModel)
    Function BankingCommentList() As List(Of ISafeCommentModel)

    'view
    Enum ScanMode
        PickupMode
        BankingMode
    End Enum


    WriteOnly Property LoggedOnUserID() As Integer
    ReadOnly Property ManagerCheckID() As Integer
    ReadOnly Property WitnessCheckID() As Integer

    Function ManagerCheck() As Boolean
    Function WitnessCheck() As Boolean

    Sub SetSafeLockedFrom(ByVal LockedFromDate As Date, ByVal LockedFromTime As Date)
    Sub SetSafeLockedTo(ByVal LockedToDate As Date, ByVal LockedToTime As Date)

    Function GetScanMode() As ScanMode

    Sub SetPickupPanelControl(ByRef Panel As GroupBox)
    Sub SetBankingPanelControl(ByRef Panel As GroupBox)

    Sub SetPickupBagListControl(ByRef Grid As FpSpread)
    Sub SetPickupCommentListControl(ByRef Grid As FpSpread)
    Sub SetBankingBagListControl(ByRef Grid As FpSpread)
    Sub SetBankingCommentListControl(ByRef Grid As FpSpread)

    Sub PickupPanelEnabled()
    Sub BankingPanelEnabled()

    Sub FormatPickupBagListControl()
    Sub FormatPickupCommentListControl()
    Sub FormatBankingBagListControl()
    Sub FormatBankingCommentListControl()

    Sub PopulatePickupBagListControl()
    Sub PopulatePickupCommentListControl()
    Sub PopulateBankingBagListControl()
    Sub PopulateBankingCommentListControl()

End Interface