﻿Public Interface IFloatedPickupVM

    Function GetFloatedPickupList(ByVal BankingPeriodID As Integer) As FloatedPickupListCollection
    Sub FloatedPickupGridPopulate(ByRef FloatedPickupLists As FloatedPickupListCollection)
    Sub SetFloatedPickupGridControl(ByRef FloatedPickupGrid As FpSpread)
    Sub FloatedPickupGridFormat()
    Sub SetFloatedPickupForm(ByRef FloatedPickupForm As System.Windows.Forms.Form)
    Sub FloatedPickupFormFormat()
End Interface
