﻿Public Interface IFloatVM

    Function GetFloatList(ByVal BankingPeriodID As Integer) As FloatListCollection
    Sub FloatGridPopulate(ByRef FloatLists As FloatListCollection)
    Sub SetFloatGridControl(ByRef FloatGrid As FpSpread)
    Sub FloatGridFormat()
    Sub SetFloatForm(ByRef FloatForm As System.Windows.Forms.Form)
    Sub FloatFormFormat()


    Function IsManualCheckRequired() As Boolean
    Function PerformManualCheck(ByVal StartFloatID As Integer) As Boolean
    ReadOnly Property NewFloatValue() As Decimal

    Sub CompleteUnAssigningTheFloatWithManualCheck(ByVal FloatID As Integer, _
                                                   ByVal PickupBagID As Integer, _
                                                   ByVal LoggedOnUserID As Integer, _
                                                   ByVal SecondUserID As Integer, _
                                                   ByVal BankingPeriodID As Integer, _
                                                   ByVal TodayPeriodID As Integer, _
                                                   ByVal AccountingModel As String, _
                                                   ByVal FloatSealNumber As String, _
                                                   ByVal CurrencyID As String)

    Sub CompleteUnAssigningTheFloat(ByVal FloatID As Integer, _
                                    ByVal PickupBagID As Integer, _
                                    ByVal LoggedOnUserID As Integer, _
                                    ByVal SecondUserID As Integer, _
                                    ByVal BankingPeriodID As Integer, _
                                    ByVal TodayPeriodID As Integer, _
                                    ByVal AccountingModel As String, _
                                    ByVal FloatSealNumber As String, _
                                    ByVal CurrencyID As String)

End Interface