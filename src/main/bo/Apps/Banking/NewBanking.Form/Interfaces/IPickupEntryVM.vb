﻿Public Interface IPickupEntryVM

    Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer)
    Sub SetCashierID(ByVal CashierID As Integer)
    Sub SetPickupModel(ByVal Model As NewBanking.Form.PickupEntry.PickupModel)
    Sub SetStartFloatValue(ByVal StartFloatValue As Nullable(Of Decimal))
    Sub SetPickupEntryGrid(ByRef PickupEntryGrid As FarPoint.Win.Spread.FpSpread)
    Sub FormatPickupEntryGrid()
    Sub PopulatePickupEntryGrid()
    Sub SetPickupEntryForm(ByRef PickupEntryForm As Windows.Forms.Form)
    Sub FormatPickupEntryForm()
    Function GetPickupBagComment() As String
    Function ActiveCellIsNotCommentsCell() As Boolean
    Function GetTotalVariance() As Decimal
End Interface
