﻿Public Interface ISafeMaintenanceVM

    'banking bag - engine
    Sub InitialiseBSE(ByVal PeriodID As Integer)

    Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean
    Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean
    Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean
    Function MatchSystemBankingExpectation() As Boolean

    Sub CreateBankingBag(ByVal SealNumber As String)
    Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String)

    Sub CreateBankingComment(ByVal Value As String)

    Sub SafeMaintenancePersist(ByRef OdbcConnection As clsOasys3DB)

    'banking bag - view
    Sub SetBankingBagListControl(ByRef BankingBagsUI As FpSpread)
    Sub FormatBankingBagListControl()
    Sub PopulateBankingBagListControl()

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'engine
    Function CreateCurrentSafeMaintenance(ByVal PeriodID As Integer) As Boolean
    Function CreateSelectedSafeMaintenance(ByVal PeriodID As Integer) As Boolean
    Function LoadCurrentSafe(ByVal PeriodID As Integer) As Boolean
    Function LoadSelectedSafe(ByVal PeriodID As Integer) As Boolean

    Function SafeMaintenanceAlreadyPerformed() As Boolean

    Function Persist() As Boolean
    Sub Persist(ByRef OdbcConnection As clsOasys3DB)

    'view
    Sub SetMiscellaneousPropertyControl(ByRef MicellaneousPropertyUI As TextBox)
    Sub SetGiftTokenSealControl(ByRef GiftTokenSealUI As TextBox)
    Sub SetCommentsControl(ByRef CommentsUI As TextBox)

    Sub PopulateMiscellaneousPropertyControl()
    Sub PopulateGiftTokenSealControl()
    Sub PopulateCommentsControl()

End Interface