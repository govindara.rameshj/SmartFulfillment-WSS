﻿Public Interface ISafeCheckReportUI

    Sub RunReport(ByRef SelectedPeriod As Period, _
                  ByVal StoreID As String, _
                  ByVal StoreName As String)

    Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                       ByVal SheetView As SheetView, _
                                       ByRef Index As Integer, _
                                       ByVal PeriodID As Integer)

    Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                          ByVal SheetView As SheetView, _
                                          ByRef Index As Integer, _
                                          ByVal SelectedPeriod As Period)

    Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal PeriodID As Integer)

    Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FpSpread, _
                                       ByVal SheetView As SheetView, _
                                       ByRef Index As Integer, _
                                       ByVal PeriodID As Integer)

End Interface