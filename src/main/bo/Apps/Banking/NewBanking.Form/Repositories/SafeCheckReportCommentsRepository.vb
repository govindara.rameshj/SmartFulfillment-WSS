﻿Imports System.Data
Imports Oasys.Data


Public Class SafeCheckReportCommentsRepository

    Public Function GetSafeCheckRecordComments(ByVal periodId As Integer) As System.Data.DataTable

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetSafeCheckReportComments"
                com.AddParameter("@PeriodId", periodId, SqlDbType.Int)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function

End Class
