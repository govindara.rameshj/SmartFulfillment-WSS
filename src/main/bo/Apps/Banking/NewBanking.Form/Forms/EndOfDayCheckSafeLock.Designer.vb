﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EndOfDayCheckSafeLock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControlLockedFrom = New DevExpress.XtraEditors.LabelControl
        Me.LabelControlLockedTo = New DevExpress.XtraEditors.LabelControl
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.DateTimePickerLockedFromDate = New System.Windows.Forms.DateTimePicker
        Me.DateTimePickerLockedFromTime = New System.Windows.Forms.DateTimePicker
        Me.DateTimePickerLockedToTime = New System.Windows.Forms.DateTimePicker
        Me.DateTimePickerLockedToDate = New System.Windows.Forms.DateTimePicker
        Me.SuspendLayout()
        '
        'LabelControlLockedFrom
        '
        Me.LabelControlLockedFrom.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControlLockedFrom.Appearance.Options.UseFont = True
        Me.LabelControlLockedFrom.Location = New System.Drawing.Point(25, 29)
        Me.LabelControlLockedFrom.Name = "LabelControlLockedFrom"
        Me.LabelControlLockedFrom.Size = New System.Drawing.Size(113, 16)
        Me.LabelControlLockedFrom.TabIndex = 4
        Me.LabelControlLockedFrom.Text = "Safe Locked From"
        '
        'LabelControlLockedTo
        '
        Me.LabelControlLockedTo.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControlLockedTo.Appearance.Options.UseFont = True
        Me.LabelControlLockedTo.Location = New System.Drawing.Point(25, 69)
        Me.LabelControlLockedTo.Name = "LabelControlLockedTo"
        Me.LabelControlLockedTo.Size = New System.Drawing.Size(97, 16)
        Me.LabelControlLockedTo.TabIndex = 5
        Me.LabelControlLockedTo.Text = "Safe Locked To"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(191, 114)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F12 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(125, 114)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 44)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DateTimePickerLockedFromDate
        '
        Me.DateTimePickerLockedFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePickerLockedFromDate.Location = New System.Drawing.Point(160, 29)
        Me.DateTimePickerLockedFromDate.Name = "DateTimePickerLockedFromDate"
        Me.DateTimePickerLockedFromDate.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePickerLockedFromDate.TabIndex = 1
        '
        'DateTimePickerLockedFromTime
        '
        Me.DateTimePickerLockedFromTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePickerLockedFromTime.Location = New System.Drawing.Point(270, 29)
        Me.DateTimePickerLockedFromTime.Name = "DateTimePickerLockedFromTime"
        Me.DateTimePickerLockedFromTime.ShowUpDown = True
        Me.DateTimePickerLockedFromTime.Size = New System.Drawing.Size(98, 20)
        Me.DateTimePickerLockedFromTime.TabIndex = 2
        '
        'DateTimePickerLockedToTime
        '
        Me.DateTimePickerLockedToTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePickerLockedToTime.Location = New System.Drawing.Point(270, 67)
        Me.DateTimePickerLockedToTime.Name = "DateTimePickerLockedToTime"
        Me.DateTimePickerLockedToTime.ShowUpDown = True
        Me.DateTimePickerLockedToTime.Size = New System.Drawing.Size(98, 20)
        Me.DateTimePickerLockedToTime.TabIndex = 4
        '
        'DateTimePickerLockedToDate
        '
        Me.DateTimePickerLockedToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePickerLockedToDate.Location = New System.Drawing.Point(160, 67)
        Me.DateTimePickerLockedToDate.Name = "DateTimePickerLockedToDate"
        Me.DateTimePickerLockedToDate.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePickerLockedToDate.TabIndex = 3
        '
        'EndOfDayCheckSafeLock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(392, 189)
        Me.ControlBox = False
        Me.Controls.Add(Me.DateTimePickerLockedToTime)
        Me.Controls.Add(Me.DateTimePickerLockedToDate)
        Me.Controls.Add(Me.DateTimePickerLockedFromTime)
        Me.Controls.Add(Me.DateTimePickerLockedFromDate)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.LabelControlLockedTo)
        Me.Controls.Add(Me.LabelControlLockedFrom)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "EndOfDayCheckSafeLock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "End Of Day Check Safe Lock"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControlLockedFrom As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlLockedTo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents DateTimePickerLockedFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerLockedFromTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerLockedToTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerLockedToDate As System.Windows.Forms.DateTimePicker
End Class
