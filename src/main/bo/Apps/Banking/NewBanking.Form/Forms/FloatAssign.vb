﻿Public Class FloatAssign

    Private _AccountingModel As String
    Private _TodayPeriodID As Integer
    Private _BankingPeriodID As Integer
    Private _LoggedOnUserID As Integer
    Private _CurrencyID As String
    Private _CurrencySymbol As String
    Private _FloatID As Integer
    Private _FloatSealNumber As String
    Private _FloatChecked As Boolean
    Private _PerformManualCheckOnly As Boolean

    Public Event DataChange()

    Private mblnLoadingForm As Boolean = True
    Private _FloatAssignVM As IFloatAssignVM

    Public Sub New(ByVal AccountingModel As String, _
                   ByVal FirstUserID As Integer, _
                   ByVal TodayPeriodID As Integer, _
                   ByVal BankingPeriodID As Integer, _
                   ByVal CurrencyID As String, _
                   ByVal CurrencySymbol As String, _
                   ByVal FloatID As Integer, _
                   ByVal FloatSealNumber As String, _
                   ByVal FloatCheck As Boolean, _
                   ByVal PerformManualCheckOnly As Boolean)

        InitializeComponent()

        _AccountingModel = AccountingModel
        _LoggedOnUserID = FirstUserID
        _TodayPeriodID = TodayPeriodID
        _BankingPeriodID = BankingPeriodID
        _CurrencyID = CurrencyID
        _CurrencySymbol = CurrencySymbol
        _FloatID = FloatID
        _FloatSealNumber = FloatSealNumber
        _FloatChecked = FloatCheck
        _PerformManualCheckOnly = PerformManualCheckOnly

        _FloatAssignVM = (New FloatAssignVMFactory).GetImplementation

    End Sub

    Private Sub FloatAssign_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleFloatAssign

            LoadUnFloatedCashiers(_BankingPeriodID)
            If cmbCashier.Items.Count > 0 Then
                cmbCashier.SelectedIndex = 0
            End If
            With _FloatAssignVM
                .SetFloatAssignForm(Me)
                .SetFloatAssignGridControl(spdInput)
                .FloatAssignGridFormat()
                .FloatAssignGridPopulate(_FloatID)
                .FloatAssignFormFormat()
            End With

            SetupManualCheckControls()
            'if float has already been checked, need to set "manual check" checkbox to true, this puts form in "manual edit" mode
            mblnLoadingForm = False

            If _PerformManualCheckOnly = True Then
                cmbCashier.Enabled = False
                btnAssignFloat.Enabled = False

            End If


        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub FloatAssign_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnAssignFloat.PerformClick()
                Case Keys.F2 : btnCancelManualCheck.PerformClick()
                Case Keys.F3
                    'amount typed in and F3 pressed
                    'if validation passed, spread does not calculate the calculated totals setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnCompleteManualCheck.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub chkManualCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkManualCheck.CheckedChanged

        Try
            'if float has already been checked, need to set "manual check" checkbox to true, this puts form in "manual edit" mode
            If mblnLoadingForm = True Then Exit Sub

            If chkManualCheck.Checked = True Then
                EnableManualCheckControls(True, _PerformManualCheckOnly)
                DisplayGridManualCheckFormula(True)
            Else
                EnableManualCheckControls(False, _PerformManualCheckOnly)
                DisplayGridManualCheckFormula(False)
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell

        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow                                          'active row
            ActiveCell = ActiveSheet.ActiveCell                                        'active cell

            'exit if denomination column
            If ActiveSheet.ActiveColumnIndex <> 0 _
            And ActiveSheet.ActiveColumnIndex <> 1 _
            And Not _FloatAssignVM.FloatAssignGridActiveCellIsCommentsCell Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
                SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

#Region "Button Event Handlers"

    Private Sub btnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignFloat.Click

        Try
            Dim ActiveSheet As SheetView
            Dim intRowCount As Integer

            Dim SecondUserID As Integer
            Dim decFloatValue As Decimal
            Dim decDenominationValue As Decimal
            Dim strSlipNo As String

            Dim AssignFloat As Banking.Core.FloatBanking

            ActiveSheet = spdInput.ActiveSheet      'active sheet

            'display cash drop total
            decFloatValue = CDec(ActiveSheet.Cells(_FloatAssignVM.GetFloatAssignGridTotalsRowIndex, 1).Value)
            If QuestionMessageBox("Float total " & _CurrencySymbol & decFloatValue.ToString("C", SetCurrencyFormat) & " - Are you sure?") = Windows.Forms.DialogResult.No Then Exit Sub
            'second verification

            'Dim Factory As IFloatAssignAuthorise = (New FloatAssignAuthoriseFactory).FactoryGet()
            'If Factory.FloatAssignAuthorise(mintFirstUserID, intSecondUserID) = False Then
            If AuthoriseCodeSecurity(False, _LoggedOnUserID, SecondUserID) = False Then Exit Sub
            'End If

            Cursor = Cursors.WaitCursor

            'persist float assignment
            strSlipNo = String.Empty
            AssignFloat = New Banking.Core.FloatBanking(_BankingPeriodID, _TodayPeriodID, _LoggedOnUserID, SecondUserID, Banking.Core.FloatBanking.Types.Float, _AccountingModel)
            AssignFloat.SetBagState(Banking.Core.BagStates.Released)
            AssignFloat.SetAccountId(CInt(cmbCashier.SelectedValue))
            AssignFloat.LoadOriginalBag(_FloatID)
            'AssignFloat.Comments = ""

            'loop around grid and write non-zero amounts away
            intRowCount = _FloatAssignVM.GetFloatAssignGridTotalsRowIndex - 1
            For intRowIndex As Integer = 0 To intRowCount
                decDenominationValue = CDec(ActiveSheet.Cells(intRowIndex, 1).Value)
                If decDenominationValue > 0 Then
                    AssignFloat.SetDenomination(_CurrencyID, CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID, _
                                                                CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID, _
                                                                decDenominationValue, strSlipNo)

                End If
            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author   : Partha
            ' Date     : 01/6/2011
            ' Referral : 839
            ' Notes    : Assigning a float to a cashier on a previous selected banking day; writing to CashBalCashier for today
            '
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim AssignFloatSaveImplementation As IFloatAssignSave = (New FloatAssignSaveFactory).GetImplementation()

            AssignFloatSaveImplementation.SaveFloatAssign(AssignFloat, _BankingPeriodID, _TodayPeriodID)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            'indicate that a float assignement has occurred
            RaiseEvent DataChange()

            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnCancelManualCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelManualCheck.Click

        Try
            chkManualCheck.Checked = False
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub btnCompleteManualCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompleteManualCheck.Click

        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            Dim ExistingFloatValue As Decimal
            Dim NewFloatValue As Decimal
            Dim SecondUserID As Integer
            Dim TotalsMatch As Boolean

            ActiveSheet = spdInput.ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow       'active row
            ActiveCell = ActiveSheet.ActiveCell     'active cell

            ExistingFloatValue = CDec(ActiveSheet.Cells(_FloatAssignVM.GetFloatAssignGridTotalsRowIndex, 1).Value)
            NewFloatValue = CDec(ActiveSheet.Cells(_FloatAssignVM.GetFloatAssignGridTotalsRowIndex, 2).Value)

            'able to leave grid without the last input being validated
            'exit if denomination column
            'If ActiveSheet.ActiveColumnIndex = 0 Or ActiveSheet.ActiveColumnIndex = 1 Then Exit Sub

            If Not _FloatAssignVM.FloatAssignGridActiveCellIsCommentsCell Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
            End If

            'zero value
            If NewFloatValue = 0 Then
                InformationMessageBox("Cannot enter a zero float amount")
                Exit Sub
            End If

            'second verification
            If AuthoriseCodeSecurity(False, _LoggedOnUserID, SecondUserID) = False Then Exit Sub
            'totals match
            If ExistingFloatValue <> NewFloatValue Then
                TotalsMatch = False
                InformationMessageBox("Total do not match. This float will be altered and stamped as having a MANUAL CHECK done")
            Else
                TotalsMatch = True
                InformationMessageBox("Totals match. The float will be stamped as having a MANUAL CHECK done")
            End If
            'are you sure
            If QuestionMessageBox("Are you sure") = Windows.Forms.DialogResult.No Then Exit Sub

            'persist manual float check
            Cursor = Cursors.WaitCursor
            If TotalsMatch = True Then
                _FloatAssignVM.FloatChecked(_FloatID, _LoggedOnUserID, SecondUserID)
            Else
                Dim ManualCheckFloat As Banking.Core.FloatBanking
                Dim decDenominationValue As Decimal
                Dim strSlipNo As String

                strSlipNo = String.Empty
                ManualCheckFloat = New Banking.Core.FloatBanking(_BankingPeriodID, _TodayPeriodID, _LoggedOnUserID, SecondUserID, Banking.Core.FloatBanking.Types.Float, _AccountingModel)
                With ManualCheckFloat

                    .LoadOriginalBag(_FloatID)
                    .SetBagState(Banking.Core.BagStates.Sealed)
                    .SetSealNumber(_FloatSealNumber)

                    For intRowIndex As Integer = 0 To _FloatAssignVM.GetFloatAssignGridTotalsRowIndex - 1
                        decDenominationValue = CDec(ActiveSheet.Cells(intRowIndex, 2).Value)

                        If decDenominationValue > 0 Then
                            .SetDenomination(_CurrencyID, CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID, _
                                                             CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID, _
                                                             decDenominationValue, strSlipNo)
                        End If
                    Next
                    _FloatAssignVM.AssignCommentsFromGrid(ManualCheckFloat)
                    _FloatID = .NewBankingManualCheck(_LoggedOnUserID, SecondUserID)

                End With

            End If

            'reload float
            _FloatChecked = True
            _FloatAssignVM.FloatAssignGridPopulate(_FloatID)

            chkManualCheck.Checked = True
            'above will cause the "manual check" feature to be enabled, need to disable
            EnableManualCheckControls(False, _PerformManualCheckOnly)
            DisplayGridManualCheckFormula(False)
            'prevent another "manual check" to happen
            chkManualCheck.Enabled = False

            'indicate that a float assignement has occurred
            RaiseEvent DataChange()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub LoadUnFloatedCashiers(ByVal PeriodID As Integer)

        Dim CashierList As New CashierCollection

        CashierList.LoadDataAssignFloat(PeriodID)

        cmbCashier.DataSource = CashierList
        cmbCashier.ValueMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.UserId)
        cmbCashier.DisplayMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.Employee)

    End Sub

    Private Sub SetupManualCheckControls()

        chkManualCheck.Checked = _FloatChecked
        chkManualCheck.Enabled = Not _FloatChecked

        'setting checkbox fires it's event handler, buttons need to be set here!
        btnCancelManualCheck.Enabled = False
        btnCompleteManualCheck.Enabled = False

    End Sub

    Private Sub EnableManualCheckControls(ByVal State As Boolean, ByVal PerformManualCheckOnly As Boolean)

        SpreadColumnEditable(spdInput.ActiveSheet, 2, State)
        _FloatAssignVM.FloatAssignGridSetupCommentsCell(State)

        btnCancelManualCheck.Enabled = State
        btnCompleteManualCheck.Enabled = State

        chkManualCheck.Enabled = Not State

        btnExit.Enabled = Not State

        If PerformManualCheckOnly = False Then cmbCashier.Enabled = Not State
        If PerformManualCheckOnly = False Then btnAssignFloat.Enabled = Not State

    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridManualCheckFormula(ByVal Enable As Boolean)

        Dim ActiveSheet As New SheetView
        Dim RowCount As Integer

        ActiveSheet = spdInput.ActiveSheet
        RowCount = _FloatAssignVM.GetFloatAssignGridTotalsRowIndex 'ActiveSheet.RowCount - 1

        'screen column - manual check, sum denominations
        SpreadCellFormula(ActiveSheet, RowCount, 2, CType(IIf(Enable, "SUM(R1C:R" & RowCount.ToString & "C)", ""), String))
        'reset "manual check" column
        If Enable = False Then
            For intRowIndex As Integer = 0 To RowCount Step 1
                SpreadCellValue(ActiveSheet, intRowIndex, 2, New System.Nullable(Of Integer))
            Next
        End If

    End Sub

#End Region

End Class