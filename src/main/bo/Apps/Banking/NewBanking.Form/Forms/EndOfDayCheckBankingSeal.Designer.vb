﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EndOfDayCheckBankingSeal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSeal = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        Me.SuspendLayout()
        '
        'lblSeal
        '
        Me.lblSeal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSeal.Location = New System.Drawing.Point(31, 18)
        Me.lblSeal.Name = "lblSeal"
        Me.lblSeal.Size = New System.Drawing.Size(91, 22)
        Me.lblSeal.TabIndex = 57
        Me.lblSeal.Text = "Enter Seal"
        Me.lblSeal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(116, 70)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F12 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(50, 70)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 44)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(122, 18)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 1
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(5, 81)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(39, 44)
        Me.UserControlScan1.TabIndex = 58
        Me.UserControlScan1.Visible = False
        '
        'EndOfDayCheckBankingSeal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(244, 135)
        Me.ControlBox = False
        Me.Controls.Add(Me.UserControlScan1)
        Me.Controls.Add(Me.txtSeal)
        Me.Controls.Add(Me.lblSeal)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "EndOfDayCheckBankingSeal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "End Of Day Check Banking Seal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSeal As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
End Class
