﻿Public Class SafeMaintenanceNew
    Implements ISafeMaintenance


    Private mintFirstUserID As Integer
    Private mSelectedPeriod As Period
    Private mCurrentPeriod As Period
    Private mstrStoreID As String
    Private mstrStoreName As String

    Private _SafeMaintenanceVM As ISafeMaintenanceVM

    Public Event DataChange() Implements ISafeMaintenance.DataChange

    Public Sub New(ByVal intFirstUserID As Integer, ByVal CurrentPeriod As Period, ByVal SelectedPeriod As Period, ByVal strStoreID As String, ByVal strStoreName As String)

        InitializeComponent()

        _SafeMaintenanceVM = (New SafeMaintenanceVMFactory).GetImplementation
        If _SafeMaintenanceVM IsNot Nothing Then
            _SafeMaintenanceVM.CreateSafeMaintenance(SelectedPeriod.PeriodID)
        End If

        mintFirstUserID = intFirstUserID
        mCurrentPeriod = CurrentPeriod
        mSelectedPeriod = SelectedPeriod
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
    End Sub

    Public Overloads Sub ShowDialog(ByRef SafeMaintenence As Main) Implements ISafeMaintenance.ShowDialog

        MyBase.ShowDialog(SafeMaintenence)
    End Sub

    Private Sub SafeMaintenance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleSafeMaintenance
            DisplayGridFormat()
            'DisplayGridPopulate(mSelectedPeriod.PeriodID)
            DisplayGridPopulate(mCurrentPeriod.PeriodID)

            If _SafeMaintenanceVM IsNot Nothing Then
                With _SafeMaintenanceVM
                    .SetCommentsControl(Me.txtSafeComments)
                    .SetGiftTokenSealControl(Me.txtGiftTokenSealNumber)
                    .SetMiscellaneousPropertyControl(Me.txtMiscellaneousProperty)
                    .SetBankingBagsControl(Me.spdBankingBagList)
                    .FormatBankingBagListControl()

                    If .Load(mSelectedPeriod.PeriodID) Then
                        .PopulateCommentsControl()
                        .PopulateGiftTokenSealControl()
                        .PopulateMiscellaneousPropertyControl()
                        .PopulateBankingBagsControl()
                    End If
                End With
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SafeMaintenance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the calculated totals
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F7
                    'same reason as above
                    spdInput.Focus()
                    btnSafeCheckReport.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow                                          'active row
            ActiveCell = ActiveSheet.ActiveCell                                        'active cell

            'exit if denomination or system column
            If ActiveSheet.ActiveColumnIndex = 0 Or ActiveSheet.ActiveColumnIndex = 1 Then Exit Sub
            'exit if "total" column
            If ActiveRow.Tag Is Nothing Then Exit Sub

            'money cannot be negative
            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'money needs to be a multiple of the denomination
            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, NewBanking.Core.SafeDenomination).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            Dim decSystemTotal As Decimal
            Dim decMainPlusChangeTotal As Decimal
            Dim intSecondManagerUserID As Integer

            ActiveSheet = spdInput.ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow       'active row
            ActiveCell = ActiveSheet.ActiveCell     'active cell

            'no check required if denomination or system column or "total" column
            If Not (ActiveSheet.ActiveColumnIndex = 0 Or ActiveSheet.ActiveColumnIndex = 1 Or ActiveRow.Tag Is Nothing) Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If

                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, NewBanking.Core.SafeDenomination).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
            End If
            'system total
            decSystemTotal = CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 1).Value)
            'main safe + change safe
            decMainPlusChangeTotal = CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 2).Value) + CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 3).Value)

            'match system
            If decSystemTotal <> decMainPlusChangeTotal Then
                InformationMessageBox("Totals do not match")
                Exit Sub
            End If

            'second manager's verification
            If AuthoriseCodeSecurity(True, mintFirstUserID, intSecondManagerUserID) = False Then Exit Sub

            'persist safe check
            Cursor = Cursors.WaitCursor

            Dim BankingPeriod As Banking.Core.BankingPeriod
            Dim Denom As NewBanking.Core.SafeDenomination
            Dim strCurrencyID As String
            Dim decDenominationID As Decimal
            Dim intTenderID As Integer
            Dim decSafeMain As Decimal
            Dim decSafeChange As Decimal
            Dim intRowCount As Integer

            BankingPeriod = New Banking.Core.BankingPeriod
            'BankingPeriod.LoadSafe(mSelectedPeriod.PeriodID)
            BankingPeriod.LoadSafe(mCurrentPeriod.PeriodID)
            BankingPeriod.SetSafeUser1(mintFirstUserID)
            BankingPeriod.SetSafeUser2(intSecondManagerUserID)
            'loop around cash denominations
            intRowCount = ActiveSheet.RowCount - 1
            For rowIndex As Integer = 0 To intRowCount
                If ActiveSheet.Rows(rowIndex).Tag Is Nothing Then Continue For

                Denom = CType(ActiveSheet.Rows(rowIndex).Tag, NewBanking.Core.SafeDenomination)
                strCurrencyID = Denom.CurrencyID
                decDenominationID = Denom.DenominationID
                intTenderID = Denom.TenderID

                decSafeMain = CType(ActiveSheet.Cells(rowIndex, 2).Value, Decimal)
                decSafeChange = CType(ActiveSheet.Cells(rowIndex, 3).Value, Decimal)

                BankingPeriod.SafeMain(strCurrencyID, decDenominationID, intTenderID) = decSafeMain
                BankingPeriod.SafeChange(strCurrencyID, decDenominationID, intTenderID) = decSafeChange
                BankingPeriod.SafeSystem(strCurrencyID, decDenominationID, intTenderID) = decSafeMain + decSafeChange
            Next

            If _SafeMaintenanceVM IsNot Nothing Then
                With _SafeMaintenanceVM
                    If Not .Persist() Then
                        Throw New Exception("Failed saving comments for safe maintenance")
                    End If
                End With
            End If

            BankingPeriod.NewBankingSafeMaintenance(mSelectedPeriod.PeriodID)
            'indicate that a safe maintenance has happened
            RaiseEvent DataChange()

            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSafeCheckReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSafeCheckReport.Click
        Try

            Dim Report As ISafeCheckReportUI

            Report = (New SafeCheckReportUIFactory).GetImplementation

            Report.RunReport(mCurrentPeriod, mstrStoreID, mstrStoreName)

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn1, gcintColumnWidthCashTender, False)     'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - System
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn3, gcintColumnWidthMoney, False)          'Screen Column - Main Safe
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn4, gcintColumnWidthMoney, False)          'Screen Column - Change Safe

        SpreadColumnMoney(NewSheet, 1, True, "0.00")
        SpreadColumnMoney(NewSheet, 2, True, "0.00")
        SpreadColumnMoney(NewSheet, 3, True, "0.00")
        SpreadColumnEditable(NewSheet, 2, True)
        SpreadColumnEditable(NewSheet, 3, True)

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(spdInput)
    End Sub

    Private Sub DisplayGridPopulate(ByVal intPeriodID As Integer)
        Dim colSafeMaintenance As SafeDenominationCollection

        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data - cash tender
        colSafeMaintenance = New SafeDenominationCollection
        colSafeMaintenance.LoadCashTender(intPeriodID)

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As NewBanking.Core.SafeDenomination In colSafeMaintenance
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)              'Primary Key   - Contains all relevent properties required later to write to safe
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)  'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.SystemSafe)  'Screen Column - System
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.MainSafe)    'Screen Column - Main Safe
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.ChangeSafe)  'Screen Column - Change Safe
        Next
        'total cash entered - calculated
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadCellLocked(CurrentSheet, intRowIndex, 2, True)
        SpreadCellLocked(CurrentSheet, intRowIndex, 2, True)

        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash")
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)")
        SpreadCellFormula(CurrentSheet, intRowIndex, 2, "SUM(R1C:R13C)")
        SpreadCellFormula(CurrentSheet, intRowIndex, 3, "SUM(R1C:R13C)")
    End Sub

#End Region
End Class