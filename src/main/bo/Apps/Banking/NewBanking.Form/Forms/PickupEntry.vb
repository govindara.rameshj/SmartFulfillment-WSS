﻿Public Class PickupEntry
    Public Enum PickupModel
        Floated
        UnFloated
        SpotCheck
    End Enum

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String

    Private mPickupModel As PickupModel
    Private mintCashierID As Integer
    Private mdecStartFloatValue As System.Nullable(Of Decimal)

    Public Event DataChange()

    Private _PickupEntryVM As IPickupEntryVM = Nothing
    Private WithEvents _Comment As PickupBagComment
    Private _CommentReturned As String
    Private _AuditRollEnquiry As IAuditRollEnquiry

    Private Const LockEntityId As String = "NewBanking.Form.PickupEntry"
    Private formLock As Locking.EntityLock

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, _
                   ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, _
                   ByVal enumPickupModel As PickupModel, _
                   ByVal intCashierID As Integer, ByVal strCashierUserName As String, ByVal strCashierEmployeeCode As String, _
                   ByVal decStartFloatValue As System.Nullable(Of Decimal), ByVal strStartFloatSeal As String)
        InitializeComponent()

        If enumPickupModel <> PickupModel.SpotCheck Then
            While True
                Try
                    formLock = New Locking.EntityLock(LockEntityId, intCashierID.ToString())
                    Exit While
                Catch exc As Locking.LockException
                    If MessageBox.Show(String.Format("This form is already opened by {0} at {1}. Are you sure you wish to continue?", exc.OwnerId, exc.LockTime), Application.ProductName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        Locking.EntityLock.RemoveLock(LockEntityId)
                    Else
                        Throw exc
                    End If
                End Try
            End While
        End If

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol

        mPickupModel = enumPickupModel

        mintCashierID = intCashierID
        mdecStartFloatValue = decStartFloatValue

        lblFloatSealValue.Text = strStartFloatSeal
        lblCashierValue.Text = strCashierEmployeeCode & " - " & strCashierUserName
    End Sub

    Private Sub PickupEntry_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If Not formLock Is Nothing Then
            formLock.Dispose()
        End If
    End Sub

    Private Sub PickupEntry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Select Case mPickupModel
                Case PickupModel.Floated
                    Me.Text = My.Resources.Screen.ScreenTitlePickupEntryFloated
                Case PickupModel.UnFloated
                    Me.Text = My.Resources.Screen.ScreenTitlePickupEntryUnFloated
                Case PickupModel.SpotCheck
                    Me.Text = My.Resources.Screen.ScreenTitleSpotCheck
            End Select
            btnSave.Enabled = False   'do not allow saving of e.o.d pickup until tray has been entered

            _PickupEntryVM = (New PickupEntryVMFactory).GetImplementation
            _AuditRollEnquiry = (New LaunchAuditRollEnquiryFactory).GetImplementation
            If _AuditRollEnquiry Is Nothing Then
                btnAuditRollEnquiry.Visible = False
            End If
            With _PickupEntryVM
                .SetBankingPeriodID(mintBankingPeriodID)
                .SetCashierID(mintCashierID)
                .SetPickupModel(mPickupModel)
                .SetStartFloatValue(mdecStartFloatValue)
                .SetPickupEntryForm(Me)
                .FormatPickupEntryForm()
                .SetPickupEntryGrid(spdInput)
                .FormatPickupEntryGrid()
                .PopulatePickupEntryGrid()
            End With

            RearrangeButtons()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub PickupEntry_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1
                    'amount typed in and F1 pressed
                    'if validation passed, spread does not calculate the calculated totals
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnConfirmTrayEntry.PerformClick()
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the total amount before the "zero amount" validation occurrs
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F9
                    If Not _AuditRollEnquiry Is Nothing Then
                        btnAuditRollEnquiry.PerformClick()
                    End If
                    btnPrint.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        If Not (mPickupModel = PickupModel.Floated Or mPickupModel = PickupModel.UnFloated) Then
            Exit Sub
        End If

        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveColumn As Column
            Dim ActiveCell As Cell
            Dim intColumnCount As Integer
            Dim intRowCount As Integer
            Dim intActiveColumnIndex As Integer
            Dim intActiveRowIndex As Integer

            Dim decTrayEntry As Decimal
            Dim decFloatValue As Decimal
            Dim decPickupValue As Decimal
            Dim decTemp As Decimal

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet
            ActiveRow = ActiveSheet.ActiveRow
            ActiveColumn = ActiveSheet.ActiveColumn
            ActiveCell = ActiveSheet.ActiveCell

            intColumnCount = ActiveSheet.ColumnCount
            intRowCount = ActiveSheet.RowCount

            intActiveColumnIndex = ActiveSheet.ActiveColumnIndex
            intActiveRowIndex = ActiveSheet.ActiveRowIndex

            If intActiveColumnIndex > 0 And _PickupEntryVM.ActiveCellIsNotCommentsCell Then

                Select Case mPickupModel
                    Case PickupModel.UnFloated
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TRAY" Then
                            'money cannot be negative - cash tender only
                            If CDec(ActiveCell.Value) < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                    Case PickupModel.Floated
                        decTrayEntry = CType(ActiveSheet.Cells(intActiveRowIndex, 1).Value, Decimal)
                        'float entry
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "FLOAT" Then
                            'calculated row or non-cash tender or zero cash tender denomination in the tray
                            If ActiveRow.Tag Is Nothing OrElse CType(ActiveRow.Tag, TenderDenominationList).TenderID <> 1 OrElse decTrayEntry = 0 Then Exit Sub

                            decFloatValue = CType(ActiveCell.Value, Decimal)
                            'money cannot be negative - cash tender only
                            If decFloatValue < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(decFloatValue, CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'adjust pickup value accordinly
                            decTemp = decTrayEntry - decFloatValue
                            If decTemp < 0 Then decTemp = 0
                            ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex + 1).Value = decTemp
                            'money cannot be greater than tray
                            If decFloatValue + decTemp > decTrayEntry Then
                                InformationMessageBox("Combined pickup & float value cannot be greater than the tray entry")

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                        'pickup entry
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "PICKUP" Then
                            'calculated row or non-cash tender or zero cash tender denomination in the tray
                            If ActiveRow.Tag Is Nothing OrElse CType(ActiveRow.Tag, TenderDenominationList).TenderID <> 1 OrElse decTrayEntry = 0 Then Exit Sub

                            decPickupValue = CDec(ActiveCell.Value)
                            'money cannot be negative - cash tender only
                            If decPickupValue < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(decPickupValue, CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'adjust pickup value accordinly
                            decTemp = decTrayEntry - decPickupValue
                            If decTemp < 0 Then decTemp = 0
                            ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex - 1).Value = decTemp
                            'money cannot be greater than tray
                            If decTemp + decPickupValue > decTrayEntry Then
                                InformationMessageBox("Combined pickup & float value cannot be greater than the tray entry")

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TRAY" Then
                            'money cannot be negative - cash tender only
                            If CDec(ActiveCell.Value) < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                End Select
                SpreadCellBlack(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnConfirmTrayEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmTrayEntry.Click
        Try
            If _PickupEntryVM.ActiveCellIsNotCommentsCell Then
                Dim ActiveSheet As SheetView
                Dim ActiveRow As Row
                Dim ActiveCell As Cell


                'able to leave grid without the last input being validated
                ActiveSheet = spdInput.ActiveSheet      'active sheet
                ActiveRow = ActiveSheet.ActiveRow       'active row
                ActiveCell = ActiveSheet.ActiveCell     'active cell

                'money cannot be negative - cash tender only
                If CDec(ActiveCell.Value) < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
            End If

            'ask for confirmation
            'If QuestionMessageBox("Happy with tray entries?") = Windows.Forms.DialogResult.No Then Exit Sub
            If QuestionMessageBox("Are you sure you wish to confirm variance?. Any amendments cannot be made afterwards") = Windows.Forms.DialogResult.No Then Exit Sub

            'make tray column read-only, add new "starting float" column if floated pickup, add new pickup column
            CloseTray()

            Select Case mPickupModel
                Case PickupModel.Floated
                    FloatedPickup()
                Case PickupModel.UnFloated
                    UnFloatedPickup()
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveColumn As Column
            Dim ActiveCell As Cell
            Dim intColumnCount As Integer
            Dim intRowCount As Integer
            Dim intActiveColumnIndex As Integer
            Dim intActiveRowIndex As Integer

            Dim decTrayEntry As Decimal
            Dim decFloatValue As Decimal
            Dim decPickupValue As Decimal
            Dim decTemp As Decimal

            'validation required in case the "leave cell" event does not get fired : e.g. pressing "F5" will bypass the said event!
            ActiveSheet = spdInput.ActiveSheet
            ActiveRow = ActiveSheet.ActiveRow
            ActiveColumn = ActiveSheet.ActiveColumn
            ActiveCell = ActiveSheet.ActiveCell

            intColumnCount = ActiveSheet.ColumnCount
            intRowCount = ActiveSheet.RowCount

            intActiveColumnIndex = ActiveSheet.ActiveColumnIndex
            intActiveRowIndex = ActiveSheet.ActiveRowIndex

            If intActiveColumnIndex > 0 And _PickupEntryVM.ActiveCellIsNotCommentsCell Then

                Select Case mPickupModel
                    Case PickupModel.UnFloated
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TRAY" Then
                            'money cannot be negative - cash tender only
                            If CDec(ActiveCell.Value) < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                        End If
                    Case PickupModel.Floated
                        decTrayEntry = CType(ActiveSheet.Cells(intActiveRowIndex, 1).Value, Decimal)
                        'float entry
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "FLOAT" Then
                            'calculated row or non-cash tender or zero cash tender denomination in the tray
                            If ActiveRow.Tag Is Nothing OrElse CType(ActiveRow.Tag, TenderDenominationList).TenderID <> 1 OrElse decTrayEntry = 0 Then Exit Select

                            decFloatValue = CType(ActiveCell.Value, Decimal)
                            'money cannot be negative - cash tender only
                            If decFloatValue < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(decFloatValue, CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                            'adjust pickup value accordinly
                            decTemp = decTrayEntry - decFloatValue
                            If decTemp < 0 Then decTemp = 0
                            ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex + 1).Value = decTemp
                            'money cannot be greater than tray
                            If decFloatValue + decTemp > decTrayEntry Then
                                InformationMessageBox("Combined pickup & float value cannot be greater than the tray entry")

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                        End If
                        'pickup entry
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "PICKUP" Then
                            'calculated row or non-cash tender or zero cash tender denomination in the tray
                            If ActiveRow.Tag Is Nothing OrElse CType(ActiveRow.Tag, TenderDenominationList).TenderID <> 1 OrElse decTrayEntry = 0 Then Exit Select

                            decPickupValue = CDec(ActiveCell.Value)
                            'money cannot be negative - cash tender only
                            If decPickupValue < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(decPickupValue, CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                            'adjust pickup value accordinly
                            decTemp = decTrayEntry - decPickupValue
                            If decTemp < 0 Then decTemp = 0
                            ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex - 1).Value = decTemp
                            'money cannot be greater than tray
                            If decTemp + decPickupValue > decTrayEntry Then
                                InformationMessageBox("Combined pickup & float value cannot be greater than the tray entry")

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                        End If
                        If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TRAY" Then
                            'money cannot be negative - cash tender only
                            If CDec(ActiveCell.Value) < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                            'money needs to be a multiple of the denomination
                            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                                SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                                Exit Sub
                            End If
                        End If
                End Select
                SpreadCellBlack(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
            End If



            Dim decVariance As Decimal
            Dim decTotalPickupAmount As Decimal
            Dim decTotalFloatAmount As Decimal
            Dim decValue As Decimal = 0

            Dim strFloatSealNo As String = String.Empty
            Dim strPickupSealNo As String = String.Empty
            Dim strSlipNo As String = String.Empty

            Dim intSecondUserID As Integer

            Dim EndOfDayPickup As Banking.Core.Pickup = Nothing
            Dim NewFloat As Banking.Core.FloatBanking = Nothing
            Dim TenderDenoms As TenderDenominationList

            'decVariance = CType(ActiveSheet.Cells(intRowCount - 1, 1).Value, Decimal)
            decVariance = _PickupEntryVM.GetTotalVariance

            'float cash tender total      - calculated, will be at row 14
            'pickup cash tender total     - calculated, will be at row 14
            'pickup non-cash tender total - calculated, will be at row 26
            decTotalPickupAmount = CType(ActiveSheet.Cells(13, intColumnCount - 1).Value, Decimal) + CType(ActiveSheet.Cells(26, intColumnCount - 1).Value, Decimal)
            If mPickupModel = PickupModel.Floated Then
                decTotalFloatAmount = CType(ActiveSheet.Cells(13, intColumnCount - 2).Value, Decimal)
            End If

            'float total cannot be zero
            If mPickupModel = PickupModel.Floated AndAlso decTotalFloatAmount = 0 Then
                InformationMessageBox("Float cannot be zero")
                Exit Sub
            End If
            'final variance
            If QuestionMessageBox("Total variance is " & FormattedCurrency(decVariance, mstrCurrencySymbol, 2) & vbCrLf & _
                                  "Variance greater than " & mstrCurrencySymbol & "2.00 will be investigated" & vbCrLf & _
                                  "Are you sure? ") = Windows.Forms.DialogResult.No Then Exit Sub

            'second verification
            If AuthoriseCodeSecurity(False, mintFirstUserID, intSecondUserID) = False Then Exit Sub

            'float seal
            If mPickupModel = PickupModel.Floated Then
                Using frmFloatSeal As New Seal(Seal.SealMode.Float, mstrCurrencySymbol & decTotalFloatAmount.ToString("C", SetCurrencyFormat))
                    If frmFloatSeal.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
                    strFloatSealNo = frmFloatSeal.txtSealInput.Text
                End Using
            End If
            'pickup seal
            Using frmPickupSeal As New Seal(Seal.SealMode.Pickup, mstrCurrencySymbol & decTotalPickupAmount.ToString("C", SetCurrencyFormat))
                If frmPickupSeal.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
                strPickupSealNo = frmPickupSeal.txtSealInput.Text
            End Using

            'seals need to be unique
            If strFloatSealNo = strPickupSealNo Then
                InformationMessageBox("The seal number for both the new float & e.o.d pickup clash")
                Exit Sub
            End If

            'persist pickup to database
            If mPickupModel = PickupModel.Floated Then
                NewFloat = New Banking.Core.FloatBanking(mintBankingPeriodID, mintTodayPeriodID, mintFirstUserID, intSecondUserID, Banking.Core.FloatBanking.Types.Float, mstrAccountingModel)
                With NewFloat
                    .SetSealNumber(strFloatSealNo)
                    .Comments = ""
                    'loop around float denoms
                    For intRowIndex As Integer = 0 To intRowCount - 1
                        If ActiveSheet.Rows(intRowIndex).Tag Is Nothing Then Continue For 'calculation rows

                        TenderDenoms = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList)  'cash/non-cash tenders left
                        If TenderDenoms.TenderID <> 1 Then Continue For 'non-cash tenders

                        'cash tenders left
                        decValue = CType(ActiveSheet.Cells(intRowIndex, intColumnCount - 2).Value, Decimal)

                        'If decValue > 0 Then .SetDenomination(mstrCurrencyID, TenderDenoms.DenominationID, TenderDenoms.TenderID, decValue, strSlipNo)
                        If decValue <> 0 Then .SetDenomination(mstrCurrencyID, TenderDenoms.DenominationID, TenderDenoms.TenderID, decValue, strSlipNo)
                    Next
                End With
            End If
            EndOfDayPickup = New Banking.Core.Pickup(mintFirstUserID, intSecondUserID, mstrAccountingModel)
            With EndOfDayPickup
                .State = Banking.Core.Pickup.States.PickupFloat
                .LoadSystemFigures(mintBankingPeriodID, mintCashierID)
                .SetBagState(Banking.Core.BagStates.Sealed)
                .SetSealNumber(strPickupSealNo)
                .SetComments(_PickupEntryVM.GetPickupBagComment)

                .SetFloatReturned(If(mdecStartFloatValue.HasValue = True, mdecStartFloatValue.Value, 0))

                'loop around pickup denoms
                For intRowIndex As Integer = 0 To intRowCount - 1
                    If ActiveSheet.Rows(intRowIndex).Tag Is Nothing Then Continue For 'calculation rows
                    'cash/non-cash tenders left
                    TenderDenoms = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList)

                    decValue = CType(ActiveSheet.Cells(intRowIndex, intColumnCount - 1).Value, Decimal)
                    'If decValue > 0 Then .SetDenomination(mstrCurrencyID, TenderDenoms.DenominationID, TenderDenoms.TenderID, decValue)
                    If decValue <> 0 Then .SetDenomination(mstrCurrencyID, TenderDenoms.DenominationID, TenderDenoms.TenderID, decValue)
                Next
                'new float created
                If mPickupModel = PickupModel.Floated AndAlso NewFloat IsNot Nothing Then
                    With NewFloat
                        .SetAccountId(mintCashierID)
                        .SaveFromPickup()
                        .AddPickupsToCashierTills(EndOfDayPickup.CashierTills)
                        EndOfDayPickup.SetRelatedBagId(.NewId)
                    End With
                End If

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author   : Partha
                ' Date     : 01/06/2011
                ' Referral : 811
                ' Notes    : Calculate cashier's float variance(difference between starting float and new float)
                '
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                '.Save()
                .Save(decTotalFloatAmount)
            End With

            MessageBox.Show("Pickup Completed")

            'indicate that a pickup has happened
            RaiseEvent DataChange()

            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnAuditRollEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuditRollEnquiry.Click
        _AuditRollEnquiry.LaunchAuditRollEnquiry(mintFirstUserID, Me)
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            ConfigurePrint()
            spdInput.PrintSheet(spdInput.ActiveSheet)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Information Event Handlers"

    Private Sub _Comment_CommentEntered(ByVal Value As String) Handles _Comment.CommentEntered

        _CommentReturned = Value
    End Sub
#End Region

#Region "Private Procedures And Functions"

    Private Sub CloseTray()
        btnSave.Enabled = True
        btnConfirmTrayEntry.Enabled = False

        SpreadColumnEditable(spdInput.ActiveSheet, 1, False)
    End Sub

    Private Sub FloatedPickup()
        Dim ActiveSheet As SheetView
        Dim intFloatColumnIndex As Integer
        Dim intPickupColumnIndex As Integer
        Dim intRowCount As Integer
        Dim decTrayValue As Decimal

        Dim TenderDenoms As TenderDenominationList

        ActiveSheet = spdInput.ActiveSheet      'active sheet
        intRowCount = ActiveSheet.RowCount

        'add float
        SpreadColumnAdd(ActiveSheet, intFloatColumnIndex)
        SpreadColumnCustomise(ActiveSheet, intFloatColumnIndex, My.Resources.ScreenColumns.PickupEntryInputGridColumn3, gcintColumnWidthMoney, False)
        SpreadColumnMoney(ActiveSheet, intFloatColumnIndex, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnEditable(ActiveSheet, intFloatColumnIndex, True)
        SpreadColumnTagValue(ActiveSheet, intFloatColumnIndex, "FLOAT")
        'add pickup
        SpreadColumnAdd(ActiveSheet, intPickupColumnIndex)
        SpreadColumnCustomise(ActiveSheet, intPickupColumnIndex, My.Resources.ScreenColumns.PickupEntryInputGridColumn4, gcintColumnWidthMoney, False)
        SpreadColumnMoney(ActiveSheet, intPickupColumnIndex, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnEditable(ActiveSheet, intPickupColumnIndex, True)
        SpreadColumnTagValue(ActiveSheet, intPickupColumnIndex, "PICKUP")
        'populate from tray column - cash tender denoms 2 pounds or less goes to float, rest goes to pickup inc non-cash items
        For intRowIndex As Integer = 0 To intRowCount - 1
            If ActiveSheet.Rows(intRowIndex).Tag Is Nothing Then Continue For 'calculation row

            'cash/non-cash tenders left
            TenderDenoms = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList)
            decTrayValue = CType(ActiveSheet.Cells(intRowIndex, 1).Value, Decimal)
            If TenderDenoms.TenderID <> 1 OrElse TenderDenoms.DenominationID > 2 Then
                'pickup
                'If decTrayValue > 0 Then SpreadCellValue(ActiveSheet, intRowIndex, intPickupColumnIndex, decTrayValue)
                SpreadCellValue(ActiveSheet, intRowIndex, intPickupColumnIndex, decTrayValue)
            Else
                'float
                If decTrayValue > 0 Then SpreadCellValue(ActiveSheet, intRowIndex, intFloatColumnIndex, decTrayValue)
            End If
            'non-cash tenders & zero value cash tender/denominations are read-only
            If TenderDenoms.TenderID <> 1 OrElse decTrayValue = 0 Then
                SpreadCellLocked(ActiveSheet, intRowIndex, intFloatColumnIndex, True)
                SpreadCellLocked(ActiveSheet, intRowIndex, intPickupColumnIndex, True)
            End If
        Next
        SpreadCellFormula(ActiveSheet, 13, intFloatColumnIndex, "SUM(R1C:R13C)")   'float cash tender total      - calculated, will be row 14
        SpreadCellFormula(ActiveSheet, 13, intPickupColumnIndex, "SUM(R1C:R13C)")  'pickup cash tender total     - calculated, will be row 14 
        SpreadCellFormula(ActiveSheet, 26, intPickupColumnIndex, "SUM(R17C:R26C)") 'pickup non-cash tender total - calculated, will be row 26
    End Sub

    Private Sub UnFloatedPickup()
        Dim ActiveSheet As SheetView
        Dim intPickupColumnIndex As Integer
        Dim intRowCount As Integer
        Dim decTrayValue As Decimal

        ActiveSheet = spdInput.ActiveSheet      'active sheet
        intRowCount = ActiveSheet.RowCount
        'add pickup
        SpreadColumnAdd(ActiveSheet, intPickupColumnIndex)
        SpreadColumnCustomise(ActiveSheet, intPickupColumnIndex, My.Resources.ScreenColumns.PickupEntryInputGridColumn4, gcintColumnWidthMoney, False)
        SpreadColumnMoney(ActiveSheet, intPickupColumnIndex, True, gstrFarPointMoneyNullDisplay)
        'populate from tray column
        For intRowIndex As Integer = 0 To intRowCount - 1
            If ActiveSheet.Rows(intRowIndex).Tag Is Nothing Then Continue For 'calculation row
            'cash/non-cash tenders left
            decTrayValue = CType(ActiveSheet.Cells(intRowIndex, 1).Value, Decimal)
            'If decTrayValue > 0 Then SpreadCellValue(ActiveSheet, intRowIndex, intPickupColumnIndex, decTrayValue)
            SpreadCellValue(ActiveSheet, intRowIndex, intPickupColumnIndex, decTrayValue)
        Next
        SpreadCellFormula(ActiveSheet, 13, intPickupColumnIndex, "SUM(R1C:R13C)")   'pickup cash tender total     - calculated, will be row 14
        SpreadCellFormula(ActiveSheet, 26, intPickupColumnIndex, "SUM(R17C:R26C)")  'pickup non-cash tender total - calculated, will be row 26
    End Sub

    Private Sub RearrangeButtons()
        If mPickupModel = PickupModel.SpotCheck Then
            btnPrint.Location = New Point(btnSave.Location.X, btnSave.Location.Y)
            btnExit.Location = New Point(btnAuditRollEnquiry.Location.X, btnAuditRollEnquiry.Location.Y)

            btnConfirmTrayEntry.Visible = False
            btnSave.Visible = False
            If btnAuditRollEnquiry.Visible Then
                btnAuditRollEnquiry.Visible = False
            End If
        Else
            btnPrint.Visible = False
        End If

    End Sub

    Friend Sub ConfigurePrint()

        With spdInput.ActiveSheet
            .PrintInfo.ShowBorder = False
            .PrintInfo.UseSmartPrint = True
            .PrintInfo.UseMax = False

            .PrintInfo.Margin.Left = 50
            .PrintInfo.Margin.Right = 50
            .PrintInfo.Margin.Top = 20
            .PrintInfo.Margin.Bottom = 20
            .PrintInfo.ShowBorder = False
            .PrintInfo.ShowPrintDialog = True
            .PrintInfo.UseSmartPrint = True
            .PrintInfo.Orientation = PrintOrientation.Portrait
            .PrintInfo.ShowPrintDialog = True

            .PrintInfo.Header &= "/c/fz""14""" & My.Resources.Screen.ScreenTitleSpotCheck
            .PrintInfo.Header &= "/n/c/fz""10""" & lblCashierValue.Text
            .PrintInfo.Header &= "/n/c/fz""10""Data for: " & DateTime.Today.ToShortDateString()
            .PrintInfo.Header &= "/n "

            .PrintInfo.Footer = "/lPrinted: " & Now
            .PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        End With

    End Sub

#End Region

End Class