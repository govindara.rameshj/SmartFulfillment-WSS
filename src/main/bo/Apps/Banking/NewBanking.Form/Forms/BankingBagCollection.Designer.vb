﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BankingBagCollection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.spdInput = New FarPoint.Win.Spread.FpSpread
        Me.txtSlipNo = New System.Windows.Forms.TextBox
        Me.lblSlip = New System.Windows.Forms.Label
        Me.lblComment = New System.Windows.Forms.Label
        Me.txtComment = New System.Windows.Forms.TextBox
        Me.btnPrint = New System.Windows.Forms.Button
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(389, 361)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(203, 361)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(113, 44)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "F5 Set As Collected"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(12, 12)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(643, 263)
        Me.spdInput.TabIndex = 1
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance2
        Me.spdInput.ActiveSheetIndex = -1
        '
        'txtSlipNo
        '
        Me.txtSlipNo.Location = New System.Drawing.Point(108, 287)
        Me.txtSlipNo.MaxLength = 13
        Me.txtSlipNo.Name = "txtSlipNo"
        Me.txtSlipNo.Size = New System.Drawing.Size(117, 20)
        Me.txtSlipNo.TabIndex = 2
        '
        'lblSlip
        '
        Me.lblSlip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSlip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSlip.Location = New System.Drawing.Point(12, 287)
        Me.lblSlip.Name = "lblSlip"
        Me.lblSlip.Size = New System.Drawing.Size(96, 22)
        Me.lblSlip.TabIndex = 43
        Me.lblSlip.Text = "Collection Slip"
        Me.lblSlip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblComment
        '
        Me.lblComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComment.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblComment.Location = New System.Drawing.Point(12, 325)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(96, 22)
        Me.lblComment.TabIndex = 44
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComment
        '
        Me.txtComment.Location = New System.Drawing.Point(108, 325)
        Me.txtComment.MaxLength = 100
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(526, 20)
        Me.txtComment.TabIndex = 3
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(322, 361)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(61, 44)
        Me.btnPrint.TabIndex = 45
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'BankingBagCollection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(669, 416)
        Me.ControlBox = False
        Me.Controls.Add(Me.spdInput)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.lblComment)
        Me.Controls.Add(Me.txtSlipNo)
        Me.Controls.Add(Me.lblSlip)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "BankingBagCollection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BankingBagCollection"
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents txtSlipNo As System.Windows.Forms.TextBox
    Friend WithEvents lblSlip As System.Windows.Forms.Label
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
End Class
