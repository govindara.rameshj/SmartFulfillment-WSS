﻿Public Class EndOfDayCheckPickupSeal

    Public Event SealEntered(ByVal Value As String)

    Private Sub EndOfDayCheckPickupSeal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True

        Select Case e.KeyData

            Case Keys.Enter : btnSave.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False

        End Select

    End Sub

    Private Sub txtSealInput_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSealInput.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSealInput_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSealInput.Leave

        If Me.ActiveControl Is btnExit Then Exit Sub

        If IsNumeric(txtSealInput.Text.Trim) = False Then
            InformationMessageBox(My.Resources.InformationMessages.SealNumeric)
            txtSealInput.Focus()
            Exit Sub
        End If

        txtSealInput.Text = txtSealInput.Text.Trim.PadLeft(8, CType("0", Char))

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If txtSealInput.Text.Trim.Length = 0 Then

            txtSealInput.Focus()
            Exit Sub

        End If

        RaiseEvent SealEntered(txtSealInput.Text)

        Me.DialogResult = Windows.Forms.DialogResult.OK

        UserControlScan1.CloseScanner()

        FindForm.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

        UserControlScan1.CloseScanner()

        FindForm.Close()

    End Sub

    Private Sub UserControlScan1_InputReceived(ByVal strInput As String) Handles UserControlScan1.ReceivedScannerInput

        txtSealInput.Text = strInput
        btnSave.PerformClick()

    End Sub

End Class