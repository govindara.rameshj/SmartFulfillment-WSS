﻿Public Class Seal

    Public Enum SealMode
        Float
        Pickup
    End Enum

    Private mSealMode As SealMode
    Private mstrBagAmount As String

    Public Sub New(ByVal enumSealMode As SealMode, ByVal strBagAmount As String)
        Try
            mSealMode = enumSealMode
            mstrBagAmount = strBagAmount

            InitializeComponent()

        Catch ex As Exception

            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub Seal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleSeal
            Select Case mSealMode
                Case SealMode.Float
                    Me.Text = My.Resources.Screen.ScreenTitleSealFloat

                    lblInformationOne.Text = My.Resources.Screen.SealFloatInformationLineOne & Space(1) & mstrBagAmount & Environment.NewLine & _
                                             My.Resources.Screen.SealFloatInformationLineTwo
                Case SealMode.Pickup
                    Me.Text = My.Resources.Screen.ScreenTitleSealPickup

                    lblInformationOne.Text = My.Resources.Screen.SealPickupInformationLineOne & Space(1) & mstrBagAmount & Environment.NewLine & _
                                             My.Resources.Screen.SealPickupInformationLineTwo
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub Seal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5 : btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSealInput_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSealInput.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSealInput_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSealInput.Leave
        Try
            If Me.ActiveControl Is btnExit Then Exit Sub

            If IsNumeric(txtSealInput.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SealNumeric)
                txtSealInput.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSealInput.Text = txtSealInput.Text.Trim.PadLeft(8, CType("0", Char))

            btnSave.PerformClick()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsNumeric(txtSealInput.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SealNumeric)
                txtSealInput.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSealInput.Text = txtSealInput.Text.Trim.PadLeft(8, CType("0", Char))

            If UniqueSeal(txtSealInput.Text) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SealNotUnique)
                Exit Sub
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
            UserControlScan1.CloseScanner()
            FindForm.Close()

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            UserControlScan1.CloseScanner()
            FindForm.Close()

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub UserControlScan1_InputReceived(ByVal strInput As String) Handles UserControlScan1.ReceivedScannerInput
        txtSealInput.Text = strInput
        btnSave.PerformClick()
    End Sub

End Class