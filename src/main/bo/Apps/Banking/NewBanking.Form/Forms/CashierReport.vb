﻿Public Class CashierReport

    Private mintBankingPeriodID As Integer
    Private mdtmBankingPeriodDate As Date
    Private mintCashierID As System.Nullable(Of Integer)
    Private mstrStoreID As String
    Private mstrStoreName As String
    Private CashierReportVM As ICashierReportVM

    Public Sub New(ByVal intBankingPeriodID As Integer, ByVal dtmBankingPeriodDate As Date, ByVal intCashierID As System.Nullable(Of Integer), _
                   ByVal strStoreID As String, ByVal strStoreName As String)
        InitializeComponent()

        mintBankingPeriodID = intBankingPeriodID
        mdtmBankingPeriodDate = dtmBankingPeriodDate
        mintCashierID = intCashierID
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
        CashierReportVM = (New CashierReportVMFactory).GetImplementation
    End Sub

    Private Sub CashierReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleCashierReport

            Cursor = Cursors.WaitCursor
            With CashierReportVM
                .SetCashierReportForm(Me)
                .CashierReportFormFormat()
                .SetCashierReportDisplayGrid(spdDisplay)
            End With
            DisplayGrid(spdDisplay, mintBankingPeriodID)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub CashierReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F8 : btnPrint.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            'process each sheet
            For intSheetIndex As Integer = 0 To spdDisplay.Sheets.Count - 1 Step 1
                Dim PrintSheet As SheetView
                'Dim ExistingGridLineVertical As GridLine
                'Dim ExistingGridLineHorizontal As GridLine
                'Dim ExistingBorder As FarPoint.Win.IBorder

                PrintSheet = spdDisplay.Sheets.Item(intSheetIndex)
                With PrintSheet
                    .PrintInfo.ShowBorder = False
                    .PrintInfo.UseSmartPrint = True
                    .PrintInfo.UseMax = False

                    .PrintInfo.Margin.Left = 50
                    .PrintInfo.Margin.Right = 50
                    .PrintInfo.Margin.Top = 20
                    .PrintInfo.Margin.Bottom = 20
                    .PrintInfo.ShowBorder = False
                    .PrintInfo.ShowPrintDialog = True
                    .PrintInfo.UseSmartPrint = True
                    .PrintInfo.Orientation = PrintOrientation.Landscape
                    .PrintInfo.ShowPrintDialog = True
                    .PrintInfo.BestFitCols = True

                    .PrintInfo.Header = "/lStore: " & mstrStoreID & " " & mstrStoreName
                    .PrintInfo.Header &= "/c/fz""14""Cashier Report"
                    .PrintInfo.Header &= "/n/c/fz""10""Banking Period : " & mintBankingPeriodID.ToString & " - " & mdtmBankingPeriodDate.ToShortDateString
                    .PrintInfo.Header &= "/n "

                    .PrintInfo.Footer = "/lPrinted: " & Now
                    .PrintInfo.Footer &= "/cPage /p of /pc"
                    .PrintInfo.Footer &= "/rVersion " & Application.ProductVersion

                    'ExistingGridLineVertical = .ColumnHeader.VerticalGridLine
                    'ExistingGridLineHorizontal = .ColumnHeader.HorizontalGridLine
                    'ExistingBorder = .ColumnHeader.DefaultStyle.Border

                    .ColumnHeader.VerticalGridLine = New GridLine(GridLineType.None)
                    .ColumnHeader.HorizontalGridLine = New GridLine(GridLineType.None)
                    .ColumnHeader.DefaultStyle.Border = New LineBorder(Color.Black, 1, False, False, False, True)
                End With
                spdDisplay.PrintSheet(PrintSheet)

                ''remove print formatting, screen display is as it was
                'With PrintSheet
                '    .ColumnHeader.VerticalGridLine = ExistingGridLineVertical
                '    .ColumnHeader.HorizontalGridLine = ExistingGridLineHorizontal
                '    .ColumnHeader.DefaultStyle.Border = ExistingBorder
                'End With
            Next
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGrid(ByRef spd As FarPoint.Win.Spread.FpSpread, ByVal intBankingPeriodID As Integer)
        Dim CurrentSheet As SheetView
        Dim colSalesCashierList As ActiveCashierListCollection

        SpreadGridScrollBar(spd, ScrollBarPolicy.Never, ScrollBarPolicy.AsNeeded)
        'load data
        colSalesCashierList = New ActiveCashierListCollection

        If mintCashierID.HasValue = True Then
            'single cashier
            colSalesCashierList.LoadCashier(intBankingPeriodID, mintCashierID.Value)
        Else
            'all cashiers
            colSalesCashierList.LoadAllCashiers(intBankingPeriodID)
        End If

        'cashier list
        For Each objCashier As ActiveCashierList In colSalesCashierList
            CurrentSheet = New SheetView
            CustomiseSheet(spd, CurrentSheet, objCashier.CashierEmployeeCode & " " & objCashier.CashierUserName)
            With CashierReportVM
                .SetCashierReportCurrentSheet(CurrentSheet)
                .DisplayCashierOnCurrentSheet(objCashier)
            End With
        Next
    End Sub

    Private Sub CustomiseSheet(ByRef spd As FarPoint.Win.Spread.FpSpread, ByRef CurrentSheet As FarPoint.Win.Spread.SheetView, ByVal strSheetName As String)

        SpreadSheetCustomise(CurrentSheet, 1, Model.SelectionUnit.Cell)
        SpreadSheetCustomiseHeader(CurrentSheet, gintHeaderHeightTriple)
        SpreadColumnCustomise(CurrentSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTendersWide, False)     'Screen Column - Denomination
        SpreadColumnAlignLeft(CurrentSheet, 0)
        SpreadGridSheetAdd(spd, CurrentSheet, True, strSheetName)
    End Sub
#End Region
End Class