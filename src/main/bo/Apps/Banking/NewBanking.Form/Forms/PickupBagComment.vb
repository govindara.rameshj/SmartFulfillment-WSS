﻿Public Class PickupBagComment

    Public Event CommentEntered(ByVal Value As String)

    Public Sub New(ByVal InformationDisplay As String)

        InitializeComponent()
        lblInformation.Text = InformationDisplay
    End Sub

    Private Sub PickupBagComment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.Enter, Keys.F5
                btnSave.PerformClick()
            Case Keys.Escape, Keys.F12
                btnExit.PerformClick()
            Case Else
                e.Handled = False
        End Select
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If txtComment.Text.Trim.Length > 0 Then
            RaiseEvent CommentEntered(txtComment.Text)
            Me.DialogResult = Windows.Forms.DialogResult.OK
            FindForm.Close()
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        FindForm.Close()
    End Sub

    Private Sub txtComment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComment.TextChanged

        If txtComment.Text.Length > 0 Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub
End Class