﻿Public Class MainCutDown

    Private mstrAccountingModel As String
    Private mintFirstUserID As Integer
    Private mintTodayPeriodId As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mstrStoreName As String

    Private mcolCutDownPeriods As PeriodCollection
    Private mSelectedPeriod As Period

    Private mPrevent As Boolean = True

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodId As Integer, _
                   ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, ByVal strStoreID As String, ByVal strStoreName As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodId = intTodayPeriodId
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
    End Sub

    Private Sub MainCutDown_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadNonBankedDates(Today)

            mPrevent = False
            cmbPeriod_SelectedIndexChanged(Me, New EventArgs)   'set banking date
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub MainCutDown_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnAssignFloat.PerformClick()
                Case Keys.F2 : btnCashDrop.PerformClick()
                Case Keys.F3 : btnFloatedPickup.PerformClick()
                Case Keys.F4 : btnUnFloatedPickup.PerformClick()
                Case Keys.F6 : btnBagCollection.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub cmbPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPeriod.SelectedIndexChanged
        Try
            If mPrevent = True Then Exit Sub
            If cmbPeriod.SelectedIndex < 0 Then Exit Sub

            mSelectedPeriod = New Period
            mSelectedPeriod = mcolCutDownPeriods.SelectedPeriod(CInt(cmbPeriod.SelectedValue))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author   : Partha
            ' Date     : 020/06/2011
            ' Referral : 828
            ' Notes    : Load safe record
            '            The safe is not being correctly created in certain scenerios; passing a data parameter rectifies this
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CreateSafe(mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignFloat.Click
        Try
            Dim frmFloat As New Float(mstrAccountingModel, mintFirstUserID, mintTodayPeriodId, mSelectedPeriod.PeriodID, mstrCurrencyID, mstrCurrencySymbol)

            frmFloat.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnCashDrop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCashDrop.Click
        Try
            Dim frmCashDrop As New CashDrop(mstrAccountingModel, mintFirstUserID, mSelectedPeriod.PeriodID, mstrCurrencyID, mstrCurrencySymbol)

            frmCashDrop.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnFloatedPickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFloatedPickup.Click
        Try
            Dim frmFloatedPickup As New FloatedPickup(mstrAccountingModel, mintFirstUserID, mintTodayPeriodId, mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate, _
                                                      mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)

            frmFloatedPickup.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnUnFloatedPickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnFloatedPickup.Click
        Try
            Dim frmUnFloatedPickup As New UnFloatedPickup(mstrAccountingModel, mintFirstUserID, mintTodayPeriodId, mSelectedPeriod.PeriodID, _
                                                          mSelectedPeriod.PeriodDate, mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)

            frmUnFloatedPickup.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnBagCollection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBagCollection.Click
        Try
            Dim frmBankingBagCollection As New BankingBagCollection( _
                mSelectedPeriod.PeriodID, mstrAccountingModel, mintFirstUserID, mstrStoreID, mstrStoreName)

            frmBankingBagCollection.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub LoadNonBankedDates(ByVal dtmToday As Date)
        mcolCutDownPeriods = New PeriodCollection

        mcolCutDownPeriods.LoadNonBankedDates(Today)
        cmbPeriod.DataSource = mcolCutDownPeriods
        cmbPeriod.ValueMember = GetPropertyName(Function(f As NewBanking.Core.Period) f.PeriodID)
        cmbPeriod.DisplayMember = GetPropertyName(Function(f As NewBanking.Core.Period) f.Description)

        'no non banked dates
        If mcolCutDownPeriods.Count = 0 Then
            btnAssignFloat.Enabled = False
            btnCashDrop.Enabled = False
            btnFloatedPickup.Enabled = False
            btnUnFloatedPickup.Enabled = False
            btnBagCollection.Enabled = False
        End If
    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha
    ' Date     : 020/06/2011
    ' Referral : 828
    ' Notes    : Load safe record
    '            The BOBanking->cSafe->Load() function does not create the safe correctly in certain scenerios
    '            A new overloaded version that also accepts a date parameter does
    '            Banking.Core.BankingPeriod->LoadSafe() function has been overloaded to accept a date parameter and so can call use above function
    '            This procedure has been modified to accept a data parameter to pass down the line
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub CreateSafe(ByVal intPeriodID As Integer, ByVal PeriodDate As Date)

        Dim safeForNonTradingDayCreated = CreateSafeForNonTradingDay(PeriodDate)

        Dim safeForCurrentDayCreated = False
        If DoesSafeExist(intPeriodID) = False Then
            Dim BP = New Banking.Core.BankingPeriod
            BP.LoadSafe(intPeriodID, mintFirstUserID, PeriodDate)
            safeForCurrentDayCreated = True
        End If

        If safeForNonTradingDayCreated Or safeForCurrentDayCreated Then
            LoadNonBankedDates(Today)
            cmbPeriod.SelectedValue = intPeriodID
        End If

    End Sub

#End Region

End Class