﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PickupEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnConfirmTrayEntry = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.spdInput = New FarPoint.Win.Spread.FpSpread()
        Me.lblCashierValue = New System.Windows.Forms.Label()
        Me.lblFloatSealValue = New System.Windows.Forms.Label()
        Me.lblCashier = New System.Windows.Forms.Label()
        Me.lblFloatSeal = New System.Windows.Forms.Label()
        Me.btnAuditRollEnquiry = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(489, 788)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(105, 44)
        Me.btnExit.TabIndex = 48
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnConfirmTrayEntry
        '
        Me.btnConfirmTrayEntry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConfirmTrayEntry.Location = New System.Drawing.Point(129, 788)
        Me.btnConfirmTrayEntry.Name = "btnConfirmTrayEntry"
        Me.btnConfirmTrayEntry.Size = New System.Drawing.Size(105, 44)
        Me.btnConfirmTrayEntry.TabIndex = 46
        Me.btnConfirmTrayEntry.Text = "F1 Confirm Tray Entry"
        Me.btnConfirmTrayEntry.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(249, 788)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(105, 44)
        Me.btnSave.TabIndex = 47
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(20, 57)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(682, 721)
        Me.spdInput.TabIndex = 45
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance1
        Me.spdInput.ActiveSheetIndex = -1
        '
        'lblCashierValue
        '
        Me.lblCashierValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashierValue.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCashierValue.Location = New System.Drawing.Point(315, 32)
        Me.lblCashierValue.Name = "lblCashierValue"
        Me.lblCashierValue.Size = New System.Drawing.Size(171, 22)
        Me.lblCashierValue.TabIndex = 52
        Me.lblCashierValue.Text = "XXXX"
        Me.lblCashierValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFloatSealValue
        '
        Me.lblFloatSealValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFloatSealValue.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblFloatSealValue.Location = New System.Drawing.Point(315, 10)
        Me.lblFloatSealValue.Name = "lblFloatSealValue"
        Me.lblFloatSealValue.Size = New System.Drawing.Size(171, 22)
        Me.lblFloatSealValue.TabIndex = 51
        Me.lblFloatSealValue.Text = "XXXX"
        Me.lblFloatSealValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCashier
        '
        Me.lblCashier.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashier.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCashier.Location = New System.Drawing.Point(237, 32)
        Me.lblCashier.Name = "lblCashier"
        Me.lblCashier.Size = New System.Drawing.Size(78, 22)
        Me.lblCashier.TabIndex = 50
        Me.lblCashier.Text = "Cashier :"
        Me.lblCashier.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFloatSeal
        '
        Me.lblFloatSeal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFloatSeal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblFloatSeal.Location = New System.Drawing.Point(237, 10)
        Me.lblFloatSeal.Name = "lblFloatSeal"
        Me.lblFloatSeal.Size = New System.Drawing.Size(78, 22)
        Me.lblFloatSeal.TabIndex = 49
        Me.lblFloatSeal.Text = "Float Seal :"
        Me.lblFloatSeal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAuditRollEnquiry
        '
        Me.btnAuditRollEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAuditRollEnquiry.Location = New System.Drawing.Point(369, 788)
        Me.btnAuditRollEnquiry.Name = "btnAuditRollEnquiry"
        Me.btnAuditRollEnquiry.Size = New System.Drawing.Size(105, 44)
        Me.btnAuditRollEnquiry.TabIndex = 53
        Me.btnAuditRollEnquiry.Text = "F9 Audit Roll Enquiry"
        Me.btnAuditRollEnquiry.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(12, 788)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(105, 44)
        Me.btnPrint.TabIndex = 54
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'PickupEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 844)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnAuditRollEnquiry)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnConfirmTrayEntry)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.spdInput)
        Me.Controls.Add(Me.lblCashierValue)
        Me.Controls.Add(Me.lblFloatSealValue)
        Me.Controls.Add(Me.lblCashier)
        Me.Controls.Add(Me.lblFloatSeal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "PickupEntry"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PickupEntry"
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnConfirmTrayEntry As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents lblCashierValue As System.Windows.Forms.Label
    Friend WithEvents lblFloatSealValue As System.Windows.Forms.Label
    Friend WithEvents lblCashier As System.Windows.Forms.Label
    Friend WithEvents lblFloatSeal As System.Windows.Forms.Label
    Friend WithEvents btnAuditRollEnquiry As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
End Class
