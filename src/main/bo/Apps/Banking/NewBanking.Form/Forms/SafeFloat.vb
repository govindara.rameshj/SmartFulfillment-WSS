﻿Public Class SafeFloat

    Private mcolList As FloatListCollection
    Private mSelected As FloatList

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mintMaxFloatCount As Integer = 0

    Private WithEvents frmSafeFloatCreate As SafeFloatCreate

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
    End Sub

    Private Sub SafeFloat_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleSafeFloat
            DisplayGridFormat()
            DisplayGridPopulate(mintBankingPeriodID)
            'empty data
            btnReturnFloat.Enabled = False
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SafeFloat_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnCreateFloat.PerformClick()
                Case Keys.F2 : btnReturnFloat.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdDisplay_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles spdDisplay.SelectionChanged
        Try
            Dim FP As FarPoint.Win.Spread.FpSpread
            Dim intFloatID As Integer

            FP = CType(sender, FarPoint.Win.Spread.FpSpread)

            If FP.ActiveSheet.ActiveRow Is Nothing Then Exit Sub 'grid is empty

            intFloatID = CType(FP.ActiveSheet.ActiveRow.Tag, Integer)

            mSelected = New FloatList
            mSelected = mcolList.SelectedFloat(intFloatID)

            ButtonAvailability(mSelected)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnCreateFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateFloat.Click
        Try
            If fnCheckMaxFloatCount() Then
                frmSafeFloatCreate = New SafeFloatCreate(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mintBankingPeriodID, mstrCurrencyID, mstrCurrencySymbol)
                frmSafeFloatCreate.ShowDialog(Me)
            Else
                MessageBox.Show("You can't create more than " & mintMaxFloatCount.ToString() & " floats", My.Resources.Screen.InformationMessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnReturnFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturnFloat.Click
        Try
            Dim intSecondUserID As Integer

            Dim FloatBagDenomValues As FloatBagCollection
            Dim NewFloat As Banking.Core.FloatBanking
            Dim intFloatID As Integer
            Dim strSlipNo As String

            'ask for confirmation
            If QuestionMessageBox("Are you sure you want to return this float to the safe?") = Windows.Forms.DialogResult.No Then Exit Sub
            'second verification
            If AuthoriseCodeSecurity(False, mintFirstUserID, intSecondUserID) = False Then Exit Sub
            'return float
            intFloatID = mSelected.FloatID
            'get denom values for this float
            FloatBagDenomValues = New FloatBagCollection
            FloatBagDenomValues.LoadData(intFloatID)

            NewFloat = New Banking.Core.FloatBanking(mintBankingPeriodID, mintTodayPeriodID, mintFirstUserID, intSecondUserID, Banking.Core.FloatBanking.Types.Float, mstrAccountingModel)
            With NewFloat
                .LoadOriginalBag(intFloatID)
                .SetBagState(Banking.Core.BagStates.BackToSafe)
                'loop around collection and write non-zero amounts away
                strSlipNo = String.Empty
                For Each obj As FloatBag In FloatBagDenomValues
                    If obj.FloatValue.HasValue = True AndAlso obj.FloatValue > 0 Then
                        .SetDenomination(mstrCurrencyID, obj.DenominationID, obj.TenderID, obj.FloatValue.Value, strSlipNo)
                    End If
                Next
                .Save()
            End With
            'refresh screen
            DisplayGridPopulate(mintBankingPeriodID)
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Child Form Events"

    Private Sub frmSafeFloatCreate_DataChange() Handles frmSafeFloatCreate.DataChange
        Try
            Cursor = Cursors.WaitCursor

            DisplayGridPopulate(mintBankingPeriodID)
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ButtonAvailability(ByRef objSelected As FloatList)
        btnReturnFloat.Enabled = True
        'float assigned
        If objSelected.AssignedToUserID.HasValue = True Then btnReturnFloat.Enabled = False
    End Sub

    Private Function fnCheckMaxFloatCount() As Boolean
        mintMaxFloatCount = Parameter.GetInteger(51)

        If mcolList.Count < mintMaxFloatCount Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.SafeFloatDisplayGridColumn1, gcintColumnWidthSeal, False)     'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.SafeFloatDisplayGridColumn2, gcintColumnWidthMoney, False)    'Screen Column - Float Value
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.SafeFloatDisplayGridColumn3, gcintColumnWidthUser, False)     'Screen Column - Float Created From
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.SafeFloatDisplayGridColumn4, gcintColumnWidthUser, False)     'Screen Column - Assigned To

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)

        SpreadGridSheetAdd(spdDisplay, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdDisplay, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

        SpreadGridDeactiveKey(spdDisplay, Keys.F2)
    End Sub

    Private Sub DisplayGridPopulate(ByVal intPeriodID As Integer)
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        mcolList = New FloatListCollection
        mcolList.LoadData(intPeriodID)

        CurrentSheet = spdDisplay.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatList In mcolList
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj.FloatID)                     'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.FloatSealNumber)            'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.FloatValue)                 'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.FloatCreatedFromUserName)   'Screen Column - Float Created From
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.AssignedToUserName)         'Screen Column - Assigned To
        Next
    End Sub

    Private Sub DisplayGridSetActiveRow(ByVal intRowID As Integer)
        With spdDisplay
            .Focus()
            .ActiveSheet.SetActiveCell(intRowID, 0)
            .ActiveSheet.AddSelection(intRowID, 0, 1, 1)
        End With

        spdDisplay_SelectionChanged(spdDisplay, Nothing)
    End Sub

#End Region

End Class