﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EndOfDayCheckPickupSeal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtSealInput = New System.Windows.Forms.TextBox
        Me.lblSeal = New System.Windows.Forms.Label
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(116, 74)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F12 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(50, 74)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 44)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtSealInput
        '
        Me.txtSealInput.Location = New System.Drawing.Point(127, 26)
        Me.txtSealInput.MaxLength = 8
        Me.txtSealInput.Name = "txtSealInput"
        Me.txtSealInput.Size = New System.Drawing.Size(80, 20)
        Me.txtSealInput.TabIndex = 1
        '
        'lblSeal
        '
        Me.lblSeal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSeal.Location = New System.Drawing.Point(36, 26)
        Me.lblSeal.Name = "lblSeal"
        Me.lblSeal.Size = New System.Drawing.Size(91, 22)
        Me.lblSeal.TabIndex = 62
        Me.lblSeal.Text = "Enter Seal"
        Me.lblSeal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(12, 74)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(32, 39)
        Me.UserControlScan1.TabIndex = 63
        Me.UserControlScan1.Visible = False
        '
        'EndOfDayCheckPickupSeal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(244, 135)
        Me.ControlBox = False
        Me.Controls.Add(Me.UserControlScan1)
        Me.Controls.Add(Me.lblSeal)
        Me.Controls.Add(Me.txtSealInput)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "EndOfDayCheckPickupSeal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "End Of Day Check Pickup Seal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtSealInput As System.Windows.Forms.TextBox
    Friend WithEvents lblSeal As System.Windows.Forms.Label
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
End Class
