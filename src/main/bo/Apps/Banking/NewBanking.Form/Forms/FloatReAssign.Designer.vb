﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FloatReAssign
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.spdInput = New FarPoint.Win.Spread.FpSpread
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAssignFloat = New System.Windows.Forms.Button
        Me.cmbCashier = New System.Windows.Forms.ComboBox
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(14, 37)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(337, 322)
        Me.spdInput.TabIndex = 2
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance1
        Me.spdInput.ActiveSheetIndex = -1
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(205, 374)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAssignFloat
        '
        Me.btnAssignFloat.Location = New System.Drawing.Point(106, 374)
        Me.btnAssignFloat.Name = "btnAssignFloat"
        Me.btnAssignFloat.Size = New System.Drawing.Size(91, 44)
        Me.btnAssignFloat.TabIndex = 3
        Me.btnAssignFloat.Text = "F1 Assign Float"
        Me.btnAssignFloat.UseVisualStyleBackColor = True
        '
        'cmbCashier
        '
        Me.cmbCashier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCashier.FormattingEnabled = True
        Me.cmbCashier.Location = New System.Drawing.Point(14, 10)
        Me.cmbCashier.Name = "cmbCashier"
        Me.cmbCashier.Size = New System.Drawing.Size(188, 21)
        Me.cmbCashier.TabIndex = 1
        '
        'FloatReAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(365, 443)
        Me.ControlBox = False
        Me.Controls.Add(Me.spdInput)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAssignFloat)
        Me.Controls.Add(Me.cmbCashier)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FloatReAssign"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FloatReAssign"
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnAssignFloat As System.Windows.Forms.Button
    Friend WithEvents cmbCashier As System.Windows.Forms.ComboBox
End Class
