﻿Public Class BankingBagCreation

    Private mcolActiveCashierList As ActiveCashierListCollection
    Private mcolPickupTotals As TenderDenominationListCollection

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mdtmBankingPeriodDate As Date
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mstrBankingBagComment As String

    Public Event DataChange()

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, _
                   ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, _
                   ByVal dtmBankingPeriodDate As Date, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, ByVal strStoreID As String, _
                   ByVal strBankingBagComment As String, _
                   ByVal colActiveCashierList As ActiveCashierListCollection, ByVal PickupTotals As TenderDenominationListCollection)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mdtmBankingPeriodDate = dtmBankingPeriodDate
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
        mstrStoreID = strStoreID
        mstrBankingBagComment = strBankingBagComment

        mcolPickupTotals = PickupTotals
        mcolActiveCashierList = colActiveCashierList
    End Sub

    Private Sub BankingBagCreation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleBankingBagCreation

            Cursor = Cursors.WaitCursor
            DisplayGridFormat()
            'DisplayGridPopulate(mintBankingPeriodID)
            DisplayGridPopulate(mintTodayPeriodID)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub BankingBagCreation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the total amount before the "zero amount" validation occurrs
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveColumn As Column
            Dim intActiveColumnIndex As Integer
            Dim intActiveRowIndex As Integer

            Dim objActiveCellTag As NewBanking.Core.SafeDenomination

            Dim decInputValue As Decimal
            Dim decSystemValue As Decimal
            Dim decTemp As Decimal

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet
            ActiveColumn = ActiveSheet.ActiveColumn
            intActiveColumnIndex = ActiveSheet.ActiveColumnIndex
            intActiveRowIndex = ActiveSheet.ActiveRowIndex

            If ActiveSheet.ActiveColumnIndex = 0 Then Exit Sub 'column - tender / denom
            If ActiveSheet.ActiveColumnIndex = 1 Then Exit Sub 'column - system value
            If ActiveSheet.ActiveColumnIndex = 2 Then Exit Sub 'column - suggested
            If ActiveSheet.ActiveColumnIndex = 4 Then Exit Sub 'column - safe balance

            decInputValue = CDec(ActiveSheet.ActiveCell.Value)
            objActiveCellTag = CType(ActiveSheet.ActiveRow.Tag, NewBanking.Core.SafeDenomination)

            'money cannot be negative - cash tender only
            If decInputValue < 0 AndAlso objActiveCellTag.TenderID = 1 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'money needs to be a multiple of the denomination
            If Not IsMultiple(decInputValue, objActiveCellTag.DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'money needs to be a multiple of the banking amount
            If objActiveCellTag.BankingAmountMultiple <> 0 AndAlso Not IsMultiple(decInputValue, objActiveCellTag.BankingAmountMultiple) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'cannot bank over suggested amount
            If decInputValue > CDec(ActiveSheet.Cells(intActiveRowIndex, 2).Value) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingExceedSuggestedAmount)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If

            '"to bank" & "safe balance" does not exceede "system"
            decSystemValue = CType(ActiveSheet.Cells(intActiveRowIndex, 1).Value, Decimal)

            If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TO BANK" Then
                decTemp = decSystemValue - decInputValue
                If decTemp < 0 Then decTemp = 0
                ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex + 1).Value = decTemp

                'money cannot be greater than tray
                If decInputValue + decTemp > decSystemValue Then
                    InformationMessageBox("Combined 'To Bank' & 'Safe Balance' cannot be greater than the 'System'")

                    SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
            End If
            If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "SAFE BALANCE" Then
                decTemp = decSystemValue - decInputValue
                If decTemp < 0 Then decTemp = 0
                ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex - 1).Value = decTemp

                'money cannot be greater than tray
                If decInputValue + decTemp > decSystemValue Then
                    InformationMessageBox("Combined 'To Bank' & 'Safe Balance' cannot be greater than the 'System'")

                    SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
            End If

            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveColumn As Column
            Dim intActiveColumnIndex As Integer
            Dim intActiveRowIndex As Integer

            Dim objActiveCellTag As NewBanking.Core.SafeDenomination
            Dim objTag As NewBanking.Core.SafeDenomination

            Dim decInputValue As Decimal
            Dim decSystemValue As Decimal
            Dim decTemp As Decimal
            Dim decTotal As Decimal

            Dim colCashTenderDenoms As TenderDenominationListCollection = Nothing
            Dim colChequeTenderDenoms As TenderDenominationListCollection = Nothing
            Dim colNonCashTenderDenoms As TenderDenominationListCollection = Nothing

            Dim strCashSlipNo As String = String.Empty
            Dim strCashSealNo As String = String.Empty

            Dim strChequeSlipNo As String = String.Empty
            Dim strChequeSealNo As String = String.Empty

            Dim strNonCashSlipNo As String = String.Empty
            Dim strNonCashSealNo As String = String.Empty

            Dim intNewCashBankingBagID As Integer = 0
            Dim intNewChequeBankingBagID As Integer = 0
            Dim intNewNonCashBankingBagID As Integer = 0

            Dim intSecondUserID As Integer

            Dim NoSaleBanking As INoSaleBanking

            ActiveSheet = spdInput.ActiveSheet
            ActiveColumn = ActiveSheet.ActiveColumn
            intActiveColumnIndex = ActiveSheet.ActiveColumnIndex
            intActiveRowIndex = ActiveSheet.ActiveRowIndex

            decInputValue = CDec(ActiveSheet.ActiveCell.Value)
            objActiveCellTag = CType(ActiveSheet.ActiveRow.Tag, NewBanking.Core.SafeDenomination)

            'validate "to bank" column only
            If ActiveSheet.ActiveColumnIndex = 3 Then
                'money cannot be negative - cash tender only
                If decInputValue < 0 AndAlso objActiveCellTag.TenderID = 1 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(decInputValue, objActiveCellTag.DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
                'money needs to be a multiple of the banking amount
                If objActiveCellTag.BankingAmountMultiple <> 0 AndAlso Not IsMultiple(decInputValue, objActiveCellTag.BankingAmountMultiple) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
                'cannot bank over suggested amount
                If decInputValue > CDec(ActiveSheet.Cells(intActiveRowIndex, 2).Value) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingExceedSuggestedAmount)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If

                '"to bank" & "safe balance" does not excede "system"
                decSystemValue = CType(ActiveSheet.Cells(intActiveRowIndex, 1).Value, Decimal)

                If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "TO BANK" Then
                    decTemp = decSystemValue - decInputValue
                    If decTemp < 0 Then decTemp = 0
                    ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex + 1).Value = decTemp

                    'money cannot be greater than tray
                    If decInputValue + decTemp > decSystemValue Then
                        InformationMessageBox("Combined 'To Bank' & 'Safe Balance' cannot be greater than the 'System'")

                        SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                        Exit Sub
                    End If
                End If
                If ActiveColumn.Tag IsNot Nothing AndAlso CType(ActiveColumn.Tag, String) = "SAFE BALANCE" Then
                    decTemp = decSystemValue - decInputValue
                    If decTemp < 0 Then decTemp = 0
                    ActiveSheet.Cells(intActiveRowIndex, intActiveColumnIndex - 1).Value = decTemp

                    'money cannot be greater than tray
                    If decInputValue + decTemp > decSystemValue Then
                        InformationMessageBox("Combined 'To Bank' & 'Safe Balance' cannot be greater than the 'System'")

                        SpreadCellRed(ActiveSheet, intActiveRowIndex, intActiveColumnIndex)
                        Exit Sub
                    End If
                End If
            End If

            'require cash seal & slip no
            decTotal = CType(ActiveSheet.Cells(13, 3).Value, Decimal)         'total cash column

            'no sale banking check
            NoSaleBanking = (New NoSaleBankingFactory).GetImplementation

            If NoSaleBanking.AllowNoSaleBanking = True Then

                'do nothing - existing validation ignored

            Else

                'if cash bag is zero then prevent banking
                If decTotal = 0 Then
                    InformationMessageBox("Nothing has been entered in ""To Bank"" for Cash")
                    Exit Sub
                End If

            End If

            If decTotal <> 0 Then
                'load data
                colCashTenderDenoms = New TenderDenominationListCollection
                colCashTenderDenoms.LoadData()
                For intRowIndex As Integer = 0 To 12 Step 1
                    objTag = CType(ActiveSheet.Rows(intRowIndex).Tag, NewBanking.Core.SafeDenomination)
                    colCashTenderDenoms.TenderDenom(objTag.TenderID, objTag.DenominationID).Amount = CType(ActiveSheet.Cells(intRowIndex, 3).Value, Decimal)
                Next
                Using frm As New BankingSeal(BankingSeal.BankingBagType.Cash, colCashTenderDenoms, mstrCurrencySymbol, mstrStoreID, decTotal)
                    If frm.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub

                    strCashSlipNo = frm.txtStoreNo.Text & frm.txtSlipNo.Text
                    strCashSealNo = frm.txtSeal.Text
                End Using
            End If

            'require cheque seal & slip no
            decTotal = CType(ActiveSheet.Cells(14, 3).Value, Decimal)         'total cheque column
            If decTotal <> 0 Then
                'load data
                colChequeTenderDenoms = New TenderDenominationListCollection
                colChequeTenderDenoms.LoadData()

                objTag = CType(ActiveSheet.Rows(14).Tag, NewBanking.Core.SafeDenomination)
                colChequeTenderDenoms.TenderDenom(objTag.TenderID, objTag.DenominationID).Amount = CType(ActiveSheet.Cells(14, 3).Value, Decimal)

                Using frm As New BankingSeal(BankingSeal.BankingBagType.Cheque, colChequeTenderDenoms, mstrCurrencySymbol, mstrStoreID, decTotal)
                    If frm.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub

                    strChequeSlipNo = frm.txtStoreNo.Text & frm.txtSlipNo.Text
                    strChequeSealNo = frm.txtSeal.Text
                End Using
            End If

            'require non-cash seal & slip no
            If (CType(ActiveSheet.Cells(24, 3).Value, Decimal) - CType(ActiveSheet.Cells(14, 3).Value, Decimal)) <> 0 Then       'total non-cash excluding cheques
                'load data
                colNonCashTenderDenoms = New TenderDenominationListCollection
                colNonCashTenderDenoms.LoadData()

                For intRowIndex As Integer = 15 To 23 Step 1
                    objTag = CType(ActiveSheet.Rows(intRowIndex).Tag, NewBanking.Core.SafeDenomination)
                    colNonCashTenderDenoms.TenderDenom(objTag.TenderID, objTag.DenominationID).Amount = CType(ActiveSheet.Cells(intRowIndex, 3).Value, Decimal)
                Next

                strNonCashSlipNo = "000000"
                strNonCashSealNo = "00000000"
            End If

            'no sale banking check
            NoSaleBanking = (New NoSaleBankingFactory).GetImplementation

            If NoSaleBanking.AllowNoSaleBanking = True Then

                'do nothing - existing validation ignored

            Else

                'banking bag slip no uniqueness not required
                ''make sure that cash & cheque slips & seals do not clash
                'If strCashSlipNo = strChequeSlipNo Then
                '    InformationMessageBox("The slip no for both cash & cheque tender types clash")
                '    Exit Sub
                'End If

                If strCashSealNo = strChequeSealNo Then
                    InformationMessageBox("The seal number for both cash & cheque tender types clash")
                    Exit Sub
                End If

            End If

            'second manager's verification
            If AuthoriseCodeSecurity(True, mintFirstUserID, intSecondUserID) = False Then Exit Sub

            'pickup(s) - return to safe
            'loop around pickups
            For Each objCashier As ActiveCashierList In mcolActiveCashierList
                For Each objEndOfDayPickup As Pickup In objCashier.EndOfDayPickupList
                    PickupReturnToSafe(mintBankingPeriodID, objCashier.CashierID, objEndOfDayPickup.PickupID, mintFirstUserID, intSecondUserID, mstrAccountingModel, mstrCurrencyID, objEndOfDayPickup.PickupSales)
                Next

                For Each objCashDropPickup As Pickup In objCashier.CashDropsPickupList
                    PickupReturnToSafe(mintBankingPeriodID, objCashier.CashierID, objCashDropPickup.PickupID, mintFirstUserID, intSecondUserID, mstrAccountingModel, mstrCurrencyID, objCashDropPickup.PickupSales)
                Next
            Next

            'banking bag(s) - cash, cheque & non-cash
            'cash
            If colCashTenderDenoms IsNot Nothing Then intNewCashBankingBagID = CreateBankingBag(mintTodayPeriodID, mintBankingPeriodID, mintFirstUserID, intSecondUserID, strCashSealNo, strCashSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colCashTenderDenoms)
            'cheque
            If colChequeTenderDenoms IsNot Nothing Then intNewChequeBankingBagID = CreateBankingBag(mintTodayPeriodID, mintBankingPeriodID, mintFirstUserID, intSecondUserID, strChequeSealNo, strChequeSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colChequeTenderDenoms)
            'non-cash
            If colNonCashTenderDenoms IsNot Nothing Then intNewNonCashBankingBagID = CreateBankingBag(mintTodayPeriodID, mintBankingPeriodID, mintFirstUserID, intSecondUserID, strNonCashSealNo, strNonCashSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colNonCashTenderDenoms)

            'banking bag(s) - manager check
            If intNewCashBankingBagID <> 0 Then ManagerCheckBankingBags(mintTodayPeriodID, mintBankingPeriodID, intNewCashBankingBagID, mintFirstUserID, intSecondUserID, strCashSealNo, strCashSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colCashTenderDenoms)
            If intNewChequeBankingBagID <> 0 Then ManagerCheckBankingBags(mintTodayPeriodID, mintBankingPeriodID, intNewChequeBankingBagID, mintFirstUserID, intSecondUserID, strChequeSealNo, strChequeSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colChequeTenderDenoms)
            If intNewNonCashBankingBagID <> 0 Then ManagerCheckBankingBags(mintTodayPeriodID, mintBankingPeriodID, intNewNonCashBankingBagID, mintFirstUserID, intSecondUserID, strNonCashSealNo, strNonCashSlipNo, mstrAccountingModel, mstrCurrencyID, mstrBankingBagComment, colNonCashTenderDenoms)





            'prepare for Comms
            Dim BankingPeriod As Banking.Core.BankingPeriod
            Dim sbCommand As New System.Text.StringBuilder

            'Prepare for Comms - Run
            BankingPeriod = New Banking.Core.BankingPeriod

            BankingPeriod.LoadSafe(mintBankingPeriodID)
            BankingPeriod.Safe.ClosePeriod()

            sbCommand.Append(Application.StartupPath)
            sbCommand.Append("\ProcessTransmissions.exe DB PDATE=")
            sbCommand.Append(BankingPeriod.Safe.PeriodDate.Value.ToString("dd/MM/yy"))

            Trace.WriteLine(sbCommand.ToString, Me.GetType.ToString)
            Using proc As New Process
                proc.StartInfo.FileName = Application.StartupPath & "\ProcessTransmissions.exe"
                proc.StartInfo.Arguments = "DB PDATE=" & BankingPeriod.Safe.PeriodDate.Value.ToString("dd/MM/yy")
                proc.Start()
                proc.WaitForExit()
            End Using

            InformationMessageBox("Banking has been completed for banking period : " & mintBankingPeriodID.ToString & " " & mdtmBankingPeriodDate.ToShortDateString)

            RaiseEvent DataChange()

            Me.DialogResult = Windows.Forms.DialogResult.OK
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub PickupReturnToSafe(ByVal intBankingPeriodId As Integer, ByVal intCashierID As Integer, ByVal intPickupID As Integer, _
                                   ByVal intUserID1 As Integer, ByVal intUserID2 As Integer, ByVal strAccountabilityModel As String, _
                                   ByVal strCurrencyID As String, ByVal colPickupSale As PickupSaleCollection)
        Dim ReturnToSafe As Banking.Core.Pickup
        Dim decTemp As Decimal

        ReturnToSafe = New Banking.Core.Pickup(intUserID1, intUserID2, strAccountabilityModel)

        With ReturnToSafe
            .State = Banking.Core.Pickup.States.Pickup
            .State = Banking.Core.Pickup.States.Check

            .LoadOriginalBag(intPickupID)

            .LoadSystemFigures(intBankingPeriodId, intCashierID)

            .SetBagState(Banking.Core.BagStates.Sealed)
            .SetBagState(Banking.Core.BagStates.BackToSafe)

            'pickup(s) with zero value & all tender/denoms are zero, when returned to safe causes the safe to be destroyed

            'by illiterate around all tender/denominations of the pickup causes an safedenom business object to be created if one does not exist
            'even if it is for zero value
            For Each obj As PickupSale In colPickupSale
                decTemp = .OldDenomValue(strCurrencyID, obj.DenominationID, obj.TenderID)
            Next


            .SetFloatReturned(0)

            For Each obj As PickupSale In colPickupSale
                If obj.PickupValue.HasValue = True AndAlso obj.PickupValue.Value <> 0 Then _
                                               .SetDenomination(strCurrencyID, obj.DenominationID, obj.TenderID, obj.PickupValue.Value)
            Next
            .Save()
        End With
    End Sub

    Private Function CreateBankingBag(ByVal intTodayPeriodId As Integer, ByVal intBankingPeriodId As Integer, ByVal intUserID1 As Integer, ByVal intUserID2 As Integer, _
                                      ByVal strSealNo As String, ByVal strSlipNo As String, _
                                      ByVal strAccountabilityModel As String, ByVal strCurrencyID As String, ByVal strBankingBagComment As String, _
                                      ByVal colTenderDenoms As TenderDenominationListCollection) As Integer
        Dim CreateBanking As Banking.Core.FloatBanking

        CreateBanking = New Banking.Core.FloatBanking(intBankingPeriodId, intTodayPeriodId, intUserID1, intUserID2, Banking.Core.FloatBanking.Types.Banking, strAccountabilityModel)
        With CreateBanking
            .SetBankingPeriodId(intBankingPeriodId)
            .SetSealNumber(strSealNo)
            .Comments = strBankingBagComment


            For Each obj As TenderDenominationList In colTenderDenoms
                If obj.Amount.HasValue = True AndAlso obj.Amount.Value <> 0 Then .SetDenomination(strCurrencyID, obj.DenominationID, obj.TenderID, obj.Amount.Value, strSlipNo)
            Next
            .Save()
            CreateBankingBag = .NewId
        End With
    End Function

    Private Sub ManagerCheckBankingBags(ByVal intTodayPeriodId As Integer, ByVal intBankingPeriodId As Integer, ByVal intBankingBagID As Integer, ByVal intUserID1 As Integer, _
                                        ByVal intUserID2 As Integer, ByVal strSealNo As String, ByVal strSlipNo As String, _
                                        ByVal strAccountabilityModel As String, ByVal strCurrencyID As String, ByVal strBankingBagComment As String, _
                                        ByVal colTenderDenoms As TenderDenominationListCollection)
        Dim ManagerCheck As Banking.Core.FloatBanking

        ManagerCheck = New Banking.Core.FloatBanking(intBankingPeriodId, intTodayPeriodId, intUserID1, intUserID2, Banking.Core.FloatBanking.Types.Banking, strAccountabilityModel)
        With ManagerCheck
            .LoadOriginalBag(intBankingBagID)
            .SetBagState(Banking.Core.BagStates.ManagerChecked)
            .SetBankingPeriodId(intBankingPeriodId)

            .Comments = strBankingBagComment
            .SetSealNumber(strSealNo)

            For Each obj As TenderDenominationList In colTenderDenoms
                If obj.Amount.HasValue = True AndAlso obj.Amount.Value <> 0 Then .SetDenomination(strCurrencyID, obj.DenominationID, obj.TenderID, obj.Amount.Value, strSlipNo)
            Next
            .Save()
        End With
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 5, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.BankingBagCreationColumn1, gcintColumnWidthAllTenders, False)     'Screen Column - Tender / Denom
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.BankingBagCreationColumn2, gcintColumnWidthCashTender, False)     'Screen Column - System
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.BankingBagCreationColumn3, gcintColumnWidthCashTender, False)     'Screen Column - Suggested
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.BankingBagCreationColumn4, gcintColumnWidthCashTender, False)     'Screen Column - To Bank
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.BankingBagCreationColumn5, gcintColumnWidthCashTender, False)     'Screen Column - Safe Balance

        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, "0.00")
        SpreadColumnMoney(NewSheet, 2, True, "0.00")
        SpreadColumnMoney(NewSheet, 3, True, "0.00")
        SpreadColumnMoney(NewSheet, 4, True, "0.00")

        SpreadColumnEditable(NewSheet, 3, True)
        
        SpreadColumnTagValue(NewSheet, 3, "TO BANK")
        SpreadColumnTagValue(NewSheet, 4, "SAFE BALANCE")

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(spdInput)
    End Sub

    Private Sub DisplayGridPopulate(ByVal intPeriodID As Integer)
        Dim colSafe As SafeDenominationCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        colSafe = New SafeDenominationCollection

        colSafe.LoadAllTenders(intPeriodID)

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        'cash tender
        For Each obj As NewBanking.Core.SafeDenomination In colSafe
            If obj.TenderID <> 1 Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)

            If obj.SystemSafe.HasValue = True Then SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.SystemSafe.Value)

            If NewBanking.Core.AbleToBankCashTenderDenomination(obj.DenominationID) = False Then SpreadRowLocked(CurrentSheet, intRowIndex, True)
        Next
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'total cash entered - calculated
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash Entered")
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)")    'Column - System
        SpreadCellFormula(CurrentSheet, intRowIndex, 2, "SUM(R1C:R13C)")    'Column - Suggested
        SpreadCellFormula(CurrentSheet, intRowIndex, 3, "SUM(R1C:R13C)")    'Column - To Bank
        SpreadCellFormula(CurrentSheet, intRowIndex, 4, "SUM(R1C:R13C)")    'Column - Safe Balance
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'non-cash tenders - amendable
        For Each obj As NewBanking.Core.SafeDenomination In colSafe
            If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = True) Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)

            If obj.SystemSafe.HasValue = True Then SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.SystemSafe.Value)
        Next
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'non-cash tenders - not amendable
        For Each obj As NewBanking.Core.SafeDenomination In colSafe
            If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = False) Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)

            SpreadRowLocked(CurrentSheet, intRowIndex, True)

            If obj.SystemSafe.HasValue = True Then SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.SystemSafe.Value)
        Next
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'total non-cash entered - calculated
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Non Cash Entered")
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R15C:R24C)")    'Column - System
        SpreadCellFormula(CurrentSheet, intRowIndex, 2, "SUM(R15C:R24C)")    'Column - Suggested
        SpreadCellFormula(CurrentSheet, intRowIndex, 3, "SUM(R15C:R24C)")    'Column - To Bank
        SpreadCellFormula(CurrentSheet, intRowIndex, 4, "SUM(R15C:R24C)")    'Column - Safe Balance
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)




        'populate "system", "suggested", "to bank" & "safe balance" column(s) with pickup totals
        Dim Tenders As IBankingBagCreationUI

        Tenders = (New BankingBagCreationUIFactory).GetImplementation
        Tenders.PopulateTenders(CurrentSheet, mcolPickupTotals)




        SpreadCellMakeActive(CurrentSheet, 0, 3)      'default active cursor to the 100 pounds denom row, "to bank" column
    End Sub

#End Region

End Class