﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SafeCheckReportView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.GroupBoxPrint = New System.Windows.Forms.GroupBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.GroupBoxSafeCheckReport = New System.Windows.Forms.GroupBox
        Me.spdSafeCheckReport = New FarPoint.Win.Spread.FpSpread
        Me.GroupBoxPrint.SuspendLayout()
        Me.GroupBoxSafeCheckReport.SuspendLayout()
        CType(Me.spdSafeCheckReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBoxPrint
        '
        Me.GroupBoxPrint.Controls.Add(Me.btnExit)
        Me.GroupBoxPrint.Controls.Add(Me.btnPrint)
        Me.GroupBoxPrint.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBoxPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxPrint.Location = New System.Drawing.Point(0, 744)
        Me.GroupBoxPrint.Name = "GroupBoxPrint"
        Me.GroupBoxPrint.Size = New System.Drawing.Size(741, 70)
        Me.GroupBoxPrint.TabIndex = 8
        Me.GroupBoxPrint.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(393, 13)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(293, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(94, 44)
        Me.btnPrint.TabIndex = 8
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'GroupBoxSafeCheckReport
        '
        Me.GroupBoxSafeCheckReport.Controls.Add(Me.spdSafeCheckReport)
        Me.GroupBoxSafeCheckReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBoxSafeCheckReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxSafeCheckReport.Location = New System.Drawing.Point(0, 0)
        Me.GroupBoxSafeCheckReport.Name = "GroupBoxSafeCheckReport"
        Me.GroupBoxSafeCheckReport.Size = New System.Drawing.Size(741, 744)
        Me.GroupBoxSafeCheckReport.TabIndex = 9
        Me.GroupBoxSafeCheckReport.TabStop = False
        '
        'spdSafeCheckReport
        '
        Me.spdSafeCheckReport.About = "3.0.2004.2005"
        Me.spdSafeCheckReport.AccessibleDescription = ""
        Me.spdSafeCheckReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdSafeCheckReport.EditModeReplace = True
        Me.spdSafeCheckReport.Location = New System.Drawing.Point(3, 16)
        Me.spdSafeCheckReport.Name = "spdSafeCheckReport"
        Me.spdSafeCheckReport.Size = New System.Drawing.Size(735, 725)
        Me.spdSafeCheckReport.TabIndex = 5
        Me.spdSafeCheckReport.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdSafeCheckReport.TextTipAppearance = TipAppearance1
        Me.spdSafeCheckReport.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        Me.spdSafeCheckReport.ActiveSheetIndex = -1
        '
        'SafeCheckReportView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(741, 814)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBoxSafeCheckReport)
        Me.Controls.Add(Me.GroupBoxPrint)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "SafeCheckReportView"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SafeCheckReport"
        Me.GroupBoxPrint.ResumeLayout(False)
        Me.GroupBoxSafeCheckReport.ResumeLayout(False)
        CType(Me.spdSafeCheckReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBoxPrint As System.Windows.Forms.GroupBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents GroupBoxSafeCheckReport As System.Windows.Forms.GroupBox
    Private WithEvents spdSafeCheckReport As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
