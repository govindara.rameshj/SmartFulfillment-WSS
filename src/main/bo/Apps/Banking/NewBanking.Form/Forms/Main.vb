﻿Imports Cts.Oasys.Core.Locking

Public Class Main

#Region "Module Level Variables"

    Private mstrAccountingModel As String
    Private mcolPeriods As PeriodCollection
    Private mCurrentPeriod As Period
    Private mSelectedPeriod As Period
    Private mintFirstUserID As Integer
    Private mstrStoreID As String
    Private mstrStoreName As String
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String

    Private mPrevent As Boolean = True

    Private _EndOfDayCheckUI As IEndOfDayCheckUI
    Private _EndOfDayCheckSummary As IEndOfDayCheckSummaryVM
    Private _SafeCheckValidation As ISafeCheckValidation
    Private _BankingBagCollectionRefresh As IBankingCollection

    Private WithEvents _Float As Float
    Private WithEvents _CashDrop As CashDrop
    Private WithEvents _FloatedPickup As FloatedPickup
    Private WithEvents _UnFloatedPickup As UnFloatedPickup
    Private WithEvents _BankingPickupCheck As BankingPickupCheck
    Private WithEvents _BankingBagCollection As BankingBagCollection
    Private WithEvents _SafeMaintenance As ISafeMaintenance
    Private WithEvents _EndOfDayCheck As EndOfDayCheck

    Private _MainVM As IMainVM

#End Region

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        'store logged on user, other forms not been sub-classed from Cts.Oasys.WinForm.Form, do not have access to "userId"
        mintFirstUserID = userId
    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            _EndOfDayCheckUI = (New EndOfDayCheckUIFactory).GetImplementation
            _EndOfDayCheckSummary = (New EndOfDayCheckSummaryVMFactory).GetImplementation
            _SafeCheckValidation = (New SafeCheckValidationFactory).GetImplementation
            _MainVM = (New MainVMFactory).GetImplementation
            _BankingBagCollectionRefresh = (New BankingCollectionFactory).GetImplementation

            DisplayProgress(0, "Load Banking Dates")
            LoadBankingDates(Today)

            DisplayProgress(10, "Load Store Details")
            LoadStore()

            DisplayProgress(20, "Load Currency Symbol")
            LoadCurrencySymbol()

            DisplayProgress(30, "Load Accounting Model")
            LoadAccountingModel()

            DisplayProgress(40, "Load Currency ID")
            LoadCurrencyID()

            DisplayProgress(50, "Configure Floated Cashier Display")
            With _MainVM
                .SetFloatedCashierGridControl(spdFloatedCashier)
                .FloatedCashierGridFormat()
            End With

            DisplayProgress(60, "Configure Cash Drop UnFloated Pickup Display")
            With _MainVM
                .SetCashDropAndUnFloatedPickupGridControl(spdCashDropAndUnFloatedPickup)
                .CashDropAndUnFloatedPickupGridFormat()
            End With

            DisplayProgress(100, "Get Banking Data For This Date")
            mPrevent = False
            cmbPeriod_SelectedIndexChanged(Me, New EventArgs)   'set banking date and populate grid

            btnEndOfDayCheck.Text = _EndOfDayCheckUI.ButtonCaptionName

        Finally
            DisplayProgress(100)
            DisplayProgress()
            Cursor = Cursors.Default
        End Try
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Static blnControlKeyPressed As Boolean = False

        MyBase.Form_KeyDown(sender, e)

        e.Handled = True
        'trap control+key combination
        If e.KeyCode = Keys.ControlKey Then
            blnControlKeyPressed = True
        Else
            Select Case e.KeyData
                Case Keys.F1 : btnAssignFloat.PerformClick()
                Case Keys.F2 : btnCashDrop.PerformClick()
                Case Keys.F3 : btnFloatedPickup.PerformClick()
                Case Keys.F4 : btnUnFloatedPickup.PerformClick()
                Case Keys.F5 : btnBanking.PerformClick()
                Case Keys.F6 : btnBagCollection.PerformClick()
                Case Keys.F7 : btnSafeCheckReport.PerformClick()
                Case Keys.F8 : btnPickupReport.PerformClick()
                Case Keys.F9 : btnEndOfDayCheck.PerformClick()
                Case Keys.F10 : btnCreateReturnFloat.PerformClick()
                Case Keys.F11 : btnSafeMaintenance.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
            End Select

            'key part of the control+key combination
            If e.KeyCode = Keys.P Then
                blnControlKeyPressed = False
                btnScreenPrint.PerformClick()
            End If
        End If
        e.Handled = False
    End Sub

    Private Sub cmbPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPeriod.SelectedIndexChanged

        If mPrevent = True Then Exit Sub
        If cmbPeriod.SelectedIndex < 0 Then Exit Sub

        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(20, "Store Selected Period")
            mSelectedPeriod = New Period
            mSelectedPeriod = mcolPeriods.SelectedPeriod(CInt(cmbPeriod.SelectedValue))

            DisplayProgress(30, "Safe Creation") 'safe does not exist create, use existing banking

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author   : Partha
            ' Date     : 020/06/2011
            ' Referral : 828
            ' Notes    : Load safe record
            '            The safe is not being correctly created in certain scenerios; passing a data parameter rectifies this
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CreateSafe(mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate)

            DisplayProgress(40, "Floated Cashiers")
            With _MainVM
                .FloatedCashierGridPopulate(.GetFloatedCashierDisplayList(mSelectedPeriod.PeriodID))
            End With

            DisplayProgress(60, "Cash Drops / Unfloated Pickup")
            With _MainVM
                .CashDropUnFloatedPickupGridPopulate(.GetCashDropAndUnFloatedPickupDisplayList(mSelectedPeriod.PeriodID))
            End With

            DisplayProgress(80, "End Of Day Management Check")
            _EndOfDayCheckSummary.Summary(spdEndOfDayManagementCheck, mSelectedPeriod.PeriodID)

            DisplayProgress(90, "Button Availability")
            ButtonAvailability()

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()

        End Try

    End Sub

#Region "Button Event Handlers"

    Private Sub btnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignFloat.Click
        _Float = New Float(mstrAccountingModel, mintFirstUserID, mcolPeriods.TodaysPeriod.PeriodID, mSelectedPeriod.PeriodID, mstrCurrencyID, mstrCurrencySymbol)

        _Float.ShowDialog(Me)
    End Sub

    Private Sub btnCashDrop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCashDrop.Click
        _CashDrop = New CashDrop(mstrAccountingModel, mintFirstUserID, mSelectedPeriod.PeriodID, mstrCurrencyID, mstrCurrencySymbol)

        _CashDrop.ShowDialog(Me)
    End Sub

    Private Sub btnFloatedPickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFloatedPickup.Click
        _FloatedPickup = New FloatedPickup(mstrAccountingModel, mintFirstUserID, mcolPeriods.TodaysPeriod.PeriodID, mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate, _
                                             mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)

        _FloatedPickup.ShowDialog(Me)
    End Sub

    Private Sub btnUnFloatedPickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnFloatedPickup.Click
        _UnFloatedPickup = New UnFloatedPickup(mstrAccountingModel, mintFirstUserID, mcolPeriods.TodaysPeriod.PeriodID, mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate, _
                                                 mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)

        _UnFloatedPickup.ShowDialog(Me)
    End Sub
    Private Function CheckForSafeMaintenance() As Boolean
        Dim V As IValidation

        V = BankingSafeMaintenanceValidationFactory.FactoryGet

        If V.RequirementEnabled = False OrElse V.PerformCheck = True Then
            ' safe maintenance check required            
            If mSelectedPeriod.SafeMaintenanceDone = False Then
                InformationMessageBox("Sorry but a safe maintenance has not been done")
                Return True
            End If
        End If
        Return False
    End Function



    Private Sub btnBanking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBanking.Click
        Dim Validation As BankingValidation

        Try
            If (mSelectedPeriod.PeriodDate = Today.Date And BankingValidation.ClickAndCollectExistsForPeriod(mSelectedPeriod.PeriodID)) Then
                InformationMessageBox("Sorry, cannot complete banking for today because there are Click and Collect sales")
                Exit Sub
            End If

            Dim autoPickupForCnC As New AutoPickupForClickAndCollect
            autoPickupForCnC.CompletePickupForClickAndCollect(mSelectedPeriod.PeriodID, UserId)
        Catch ex As Exception
            InformationMessageBox("Automatic Pickup Completion for Click and Collect failed")
            Trace.WriteLine(ex)
        End Try

        Validation = New BankingValidation(mSelectedPeriod.PeriodID)

        If Validation.FloatedCashierNoSales = True Then
            InformationMessageBox("Sorry but one or more cashier(s) exist with floats assigned, but no till sales")
            Exit Sub
        End If
        If Validation.FloatedCashierSalesNoPickupBag = True Then
            InformationMessageBox("Sorry but one or more floated cashier(s) exist without an e.o.d pickup bag")
            Exit Sub
        End If

        If Validation.UnFloatedCashierSalesNoPickupBag = True Then
            InformationMessageBox("Sorry but one or more unfloated cashier(s) exist without an e.o.d pickup bag")
            Exit Sub
        End If


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author   : Partha
        ' Date     : 04/07/2011
        ' Referral : 120-06
        ' Notes    : Load safe record
        '            The safe is not being correctly created in certain scenerios; passing a data parameter rectifies this
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not _SafeCheckValidation.AllowSafeCheckCompletion Then
            If CheckForSafeMaintenance() Then
                Exit Sub
            End If
        End If


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim lockEntityId = "NewBanking.Form.BankingPickupCheck"
        Dim ownerId = mintFirstUserID.ToString("d3")
        Using lockWrapper As New BankingLockWrapper(lockEntityId, ownerId)
            If lockWrapper.IsLockAcquired Then
                _BankingPickupCheck = New BankingPickupCheck(mstrAccountingModel, mintFirstUserID, mcolPeriods.TodaysPeriod.PeriodID, mSelectedPeriod.PeriodID, mSelectedPeriod.PeriodDate, _
                                                               mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)
                _BankingPickupCheck.ShowDialog(Me)
            Else
                MessageBox.Show(String.Format("Banking process has already started by {0}", ownerId))
            End If
        End Using
    End Sub

    Private Sub btnBagCollection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBagCollection.Click
        _BankingBagCollection = New BankingBagCollection( _
            mSelectedPeriod.PeriodID, mstrAccountingModel, mintFirstUserID, mstrStoreID, mstrStoreName)

        _BankingBagCollection.ShowDialog(Me)
    End Sub

    Private Sub btnSafeCheckReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSafeCheckReport.Click

        Dim Report As ISafeCheckReportUI

        Report = (New SafeCheckReportUIFactory).GetImplementation

        Report.RunReport(mSelectedPeriod, mstrStoreID, mstrStoreName)

    End Sub

    Private Sub btnPickupReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPickupReport.Click
        Dim pickup As New Banking.Form.EodPickupReport(mstrAccountingModel, mstrStoreID & " " & mstrStoreName, mSelectedPeriod.PeriodID)

        pickup.ShowDialog(Me)
        pickup.Dispose()
    End Sub

    Private Sub btnEndOfDayCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEndOfDayCheck.Click

        If _EndOfDayCheckUI.ExecuteEndOfDayCheckProcessUI = False Then Exit Sub

        _EndOfDayCheck = New EndOfDayCheck(mSelectedPeriod.PeriodID, mintFirstUserID)
        _EndOfDayCheck.ShowDialog(Me)

    End Sub

    Private Sub btnCreateReturnFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateReturnFloat.Click
        Dim frmSafeFloat As New SafeFloat(mstrAccountingModel, mintFirstUserID, mcolPeriods.TodaysPeriod.PeriodID, mSelectedPeriod.PeriodID, mstrCurrencyID, mstrCurrencySymbol)

        frmSafeFloat.ShowDialog(Me)
    End Sub

    Private Sub btnSafeMaintenance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSafeMaintenance.Click

        Dim Factory As New SafeMaintenanceFormFactory

        Factory.Initialise(mintFirstUserID, mCurrentPeriod, mSelectedPeriod, mstrStoreID, mstrStoreName)
        _SafeMaintenance = Factory.GetImplementation
        _SafeMaintenance.ShowDialog(Me)

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Sub btnScreenPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScreenPrint.Click
        PrintScreenReport(mSelectedPeriod, mstrStoreID, mstrStoreName)
    End Sub

#End Region

#Region "Child Form Events"

    Private Sub FloatDataChange() Handles _Float.DataChange
        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(40, "Floated Cashiers")
            With _MainVM
                .FloatedCashierGridPopulate(.GetFloatedCashierDisplayList(mSelectedPeriod.PeriodID))
            End With
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub CashDropScreenUpdate() Handles _CashDrop.DataChange
        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(50, "Cash Drops / Unfloated Pickup")
            With _MainVM
                .CashDropUnFloatedPickupGridPopulate(.GetCashDropAndUnFloatedPickupDisplayList(mSelectedPeriod.PeriodID))
            End With

            DisplayProgress(90, "Button Availability")
            ButtonAvailability()
            'ButtonAvailability(mSelectedPeriod.PeriodID)
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub FloatedPickupDataChange() Handles _FloatedPickup.DataChange
        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(50, "Floated Cashiers")
            With _MainVM
                .FloatedCashierGridPopulate(.GetFloatedCashierDisplayList(mSelectedPeriod.PeriodID))
            End With

            DisplayProgress(90, "Button Availability")
            ButtonAvailability()
            'ButtonAvailability(mSelectedPeriod.PeriodID)
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub UnFloatedPickupDataChange() Handles _UnFloatedPickup.DataChange
        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(50, "Cash Drops / Unfloated Pickup")
            With _MainVM
                .CashDropUnFloatedPickupGridPopulate(.GetCashDropAndUnFloatedPickupDisplayList(mSelectedPeriod.PeriodID))
            End With

            DisplayProgress(90, "Button Availability")
            ButtonAvailability()
            'ButtonAvailability(mSelectedPeriod.PeriodID)
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub frmBankingPickupCheck_DataChange() Handles _BankingPickupCheck.DataChange
        Try
            Dim intSelectedIndex As Integer

            Cursor = Cursors.WaitCursor
            intSelectedIndex = cmbPeriod.SelectedIndex
            'reload data
            DisplayProgress(20, "Store Selected Period")
            LoadBankingDates(Today)

            cmbPeriod.SelectedIndex = intSelectedIndex
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub frmBankingBagCollection_DataChange() Handles _BankingBagCollection.DataChange
        Try
            Dim intSelectedIndex As Integer
            Cursor = Cursors.WaitCursor
            intSelectedIndex = cmbPeriod.SelectedIndex

            mSelectedPeriod.BankingBagCollectionRequired = False

            If _BankingBagCollectionRefresh.RefreshBagCollection Then
                LoadBankingDates(Today)
            End If

            cmbPeriod.SelectedIndex = intSelectedIndex
            DisplayProgress(90, "Button Availability")
            ButtonAvailability()
            'ButtonAvailability(mSelectedPeriod.PeriodID)
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

    Private Sub _EndOfDayCheck_DataChange() Handles _EndOfDayCheck.DataChange

        Try
            Cursor = Cursors.WaitCursor

            DisplayProgress(80, "End Of Day Management Check")
            _EndOfDayCheckSummary.Summary(spdEndOfDayManagementCheck, mSelectedPeriod.PeriodID)

            mSelectedPeriod.EndOfDayCheckDone = True  'safe collection not reloaded

            ButtonAvailability()

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()

        End Try

    End Sub

    Private Sub frmSafeMaintenance_DataChange() Handles _SafeMaintenance.DataChange
        Try
            Cursor = Cursors.WaitCursor

            'update selected period safe main flag, allow the next step of the banking process to happen
            DisplayProgress(50, "Safe maintenance done")
            mSelectedPeriod.SafeMaintenanceDone = True

            DisplayProgress(90, "Button Availability")
            ButtonAvailability()
            'ButtonAvailability(mSelectedPeriod.PeriodID)
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub LoadBankingDates(ByVal dtmToday As Date)
        mcolPeriods = New PeriodCollection

        mcolPeriods.LoadDates(Today)
        cmbPeriod.DataSource = mcolPeriods
        cmbPeriod.ValueMember = GetPropertyName(Function(f As NewBanking.Core.Period) f.PeriodID)
        cmbPeriod.DisplayMember = GetPropertyName(Function(f As NewBanking.Core.Period) f.Description)

        mCurrentPeriod = mcolPeriods.TodaysPeriod
    End Sub

    Private Sub LoadStore()
        NewBanking.Core.Library.GetStoreID(mstrStoreID, mstrStoreName)
    End Sub

    Private Sub LoadCurrencySymbol()
        mstrCurrencySymbol = NewBanking.Core.Library.CurrencySymbol
    End Sub

    Private Sub LoadAccountingModel()
        mstrAccountingModel = NewBanking.Core.Library.AccountabilityModel
    End Sub

    Private Sub LoadCurrencyID()
        mstrCurrencyID = NewBanking.Core.Library.ActiveCurrencyID
    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha
    ' Date     : 020/06/2011
    ' Referral : 828
    ' Notes    : Load safe record
    '            The BOBanking->cSafe->Load() function does not create the safe correctly in certain scenerios
    '            A new overloaded version that also accepts a date parameter does
    '            Banking.Core.BankingPeriod->LoadSafe() function has been overloaded to accept a date parameter and so can call use above function
    '            This procedure has been modified to accept a data parameter to pass down the line
    '
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub CreateSafe(ByVal intPeriodID As Integer, ByVal PeriodDate As Date)

        Dim safeForNonTradingDayCreated = CreateSafeForNonTradingDay(PeriodDate)

        Dim safeForCurrentDayCreated = False
        If DoesSafeExist(intPeriodID) = False Then
            Dim BP = New Banking.Core.BankingPeriod
            BP.LoadSafe(intPeriodID, UserId, PeriodDate)
            safeForCurrentDayCreated = True
        End If

        If safeForNonTradingDayCreated Or safeForCurrentDayCreated Then
            LoadBankingDates(Today)
            cmbPeriod.SelectedValue = intPeriodID
        End If

    End Sub

    Private Sub ButtonAvailability()
        EnableDisableButtons(True)

        If mSelectedPeriod.BankingComplete = True Then
            btnAssignFloat.Enabled = False
            btnCashDrop.Enabled = False
            btnFloatedPickup.Enabled = False
            btnUnFloatedPickup.Enabled = False
            btnSafeMaintenance.Enabled = False
            btnBanking.Enabled = False
        End If
        If mSelectedPeriod.NextDateToBeBanked = False Then btnBanking.Enabled = False

        If mSelectedPeriod.LastDateBanked = False Then btnBagCollection.Enabled = False
        If mSelectedPeriod.BankingBagCollectionRequired = False Then btnBagCollection.Enabled = False


        Dim Value As System.Nullable(Of Boolean) = mSelectedPeriod.EndOfDayCheckDone
        If mSelectedPeriod.EndOfDayCheckDone Is Nothing Then Value = False

        btnEndOfDayCheck.Enabled = _EndOfDayCheckUI.ButtonEnabled(mCurrentPeriod.PeriodDate, mSelectedPeriod.PeriodDate, Value.Value)

    End Sub

    Private Sub EnableDisableButtons(ByVal blnState As Boolean)
        btnAssignFloat.Enabled = blnState
        btnCashDrop.Enabled = blnState
        btnFloatedPickup.Enabled = blnState
        btnUnFloatedPickup.Enabled = blnState
        btnSafeMaintenance.Enabled = blnState
        btnBanking.Enabled = blnState
        btnBagCollection.Enabled = blnState
        btnSafeCheckReport.Enabled = blnState
        btnPickupReport.Enabled = blnState
        btnEndOfDayCheck.Enabled = blnState
    End Sub

#End Region

End Class
