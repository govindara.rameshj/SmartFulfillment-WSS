﻿Public Class ManualCheck

    Public Enum ExitMode

        SaveAndExit
        CancelAndExit

    End Enum

    Private _ManualCheckedVM As IFloatAssignVM
    Private _FloatID As Integer
    Friend _Exit As ExitMode
    Private _ExistingFloatValue As Decimal
    Private _NewFloatValue As Decimal
    Private _NewFloatBagDenoms As FloatBagCollection

#Region "Public Proerties"

    Public ReadOnly Property FormExitState() As ExitMode
        Get
            Return _Exit
        End Get
    End Property

    Public ReadOnly Property ExistingFloatValue() As Decimal
        Get
            Return _ExistingFloatValue
        End Get
    End Property

    Public ReadOnly Property NewFloatValue() As Decimal
        Get
            Return _NewFloatValue
        End Get
    End Property

    Public ReadOnly Property ManualCheckedViewModel() As IFloatAssignVM
        Get
            Return _ManualCheckedVM
        End Get
    End Property

    Public Function GetNewFloatBagDenoms() As FloatBagCollection

        If _NewFloatBagDenoms Is Nothing Then
            LoadNewFloatBagDenoms()
        End If
        Return _NewFloatBagDenoms
    End Function

#End Region

#Region "Form Events"

    Public Sub New(ByVal FloatID As Integer)

        InitializeComponent()

        _FloatID = FloatID
        _ManualCheckedVM = (New FloatAssignVMFactory).GetImplementation
    End Sub

    Private Sub FormLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Text = My.Resources.Screen.ScreenTitleFloatAssign
        With _ManualCheckedVM
            .SetFloatAssignForm(Me)
            .SetFloatAssignGridControl(spdInput)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(_FloatID)
            .FloatAssignFormFormat()
            .FloatAssignGridSetupCommentsCell(True)
            SpreadColumnEditable(spdInput.ActiveSheet, 2, True)
            SpreadCellValue(spdInput.ActiveSheet, .GetFloatAssignGridTotalsRowIndex, 2, 0)
            SpreadCellFormula(spdInput.ActiveSheet, .GetFloatAssignGridTotalsRowIndex, 2, "SUM(R1C:R13C)")
        End With
    End Sub

    Private Sub FormKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3
                spdInput.Focus()
                btnCompleteAndExit.PerformClick()
            Case Keys.F12
                btnCancelAndExit.PerformClick()
            Case Else
                e.Handled = False
        End Select
    End Sub

#End Region

#Region "Spread Grid Events"

    Private Sub InputGridLeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Dim ActiveSheet As SheetView
        Dim ActiveRow As Row
        Dim ActiveCell As Cell

        ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
        ActiveRow = ActiveSheet.ActiveRow                                          'active row
        ActiveCell = ActiveSheet.ActiveCell                                        'active cell

        'exit if denomination column
        If ActiveSheet.ActiveColumnIndex <> 0 And _
           ActiveSheet.ActiveColumnIndex <> 1 And Not _
           _ManualCheckedVM.FloatAssignGridActiveCellIsCommentsCell Then

            'money cannot be negative
            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If

            'money needs to be a multiple of the denomination
            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If

            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)

        End If

    End Sub

#End Region

#Region "Button Event Handlers"

    Private Sub CompleteAndExitClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompleteAndExit.Click

        Dim ActiveSheet As SheetView
        Dim ActiveRow As Row
        Dim ActiveCell As Cell

        ActiveSheet = spdInput.ActiveSheet      'active sheet
        ActiveRow = ActiveSheet.ActiveRow       'active row
        ActiveCell = ActiveSheet.ActiveCell     'active cell

        _ExistingFloatValue = CDec(ActiveSheet.Cells(_ManualCheckedVM.GetFloatAssignGridTotalsRowIndex, 1).Value)
        _NewFloatValue = CDec(ActiveSheet.Cells(_ManualCheckedVM.GetFloatAssignGridTotalsRowIndex, 2).Value)

        If Not _ManualCheckedVM.FloatAssignGridActiveCellIsCommentsCell Then

            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                Exit Sub
            End If

            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                Exit Sub
            End If

        End If

        If _NewFloatValue = 0 Then

            InformationMessageBox("Cannot enter a zero float amount")
            Exit Sub

        End If

        _Exit = ManualCheck.ExitMode.SaveAndExit

        FindForm.Close()

    End Sub

    Private Sub CancelAndExitClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAndExit.Click

        _Exit = ManualCheck.ExitMode.CancelAndExit

        FindForm.Close()

    End Sub

#End Region

#Region "Internals"

    Private Sub LoadNewFloatBagDenoms()
        Dim DenominationValue As Decimal
        Dim NewFloatBagDenom As FloatBag

        _NewFloatBagDenoms = New FloatBagCollection
        For RowIndex As Integer = 0 To ManualCheckedViewModel.GetFloatAssignGridTotalsRowIndex - 1
            With spdInput.ActiveSheet
                DenominationValue = CDec(.Cells(RowIndex, 2).Value)
                If DenominationValue > 0 Then
                    NewFloatBagDenom = New FloatBag
                    NewFloatBagDenom.FloatValue = DenominationValue
                    NewFloatBagDenom.DenominationID = CType(.Rows(RowIndex).Tag, TenderDenominationList).DenominationID
                    NewFloatBagDenom.TenderID = CType(.Rows(RowIndex).Tag, TenderDenominationList).TenderID
                    _NewFloatBagDenoms.Add(NewFloatBagDenom)
                End If
            End With
        Next
    End Sub

#End Region

End Class