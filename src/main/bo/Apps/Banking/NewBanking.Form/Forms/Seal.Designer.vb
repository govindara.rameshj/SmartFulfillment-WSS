﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Seal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblSeal = New System.Windows.Forms.Label
        Me.lblInformationOne = New System.Windows.Forms.Label
        Me.txtSealInput = New System.Windows.Forms.TextBox
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(216, 100)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F12 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(148, 100)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 44)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblSeal
        '
        Me.lblSeal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSeal.Location = New System.Drawing.Point(12, 61)
        Me.lblSeal.Name = "lblSeal"
        Me.lblSeal.Size = New System.Drawing.Size(124, 22)
        Me.lblSeal.TabIndex = 41
        Me.lblSeal.Text = "Scan Security Seal"
        Me.lblSeal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInformationOne
        '
        Me.lblInformationOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformationOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblInformationOne.Location = New System.Drawing.Point(12, 9)
        Me.lblInformationOne.Name = "lblInformationOne"
        Me.lblInformationOne.Size = New System.Drawing.Size(416, 38)
        Me.lblInformationOne.TabIndex = 42
        Me.lblInformationOne.Text = "XXXXXXXX"
        Me.lblInformationOne.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSealInput
        '
        Me.txtSealInput.Location = New System.Drawing.Point(136, 61)
        Me.txtSealInput.MaxLength = 8
        Me.txtSealInput.Name = "txtSealInput"
        Me.txtSealInput.Size = New System.Drawing.Size(193, 20)
        Me.txtSealInput.TabIndex = 1
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(38, 100)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(40, 44)
        Me.UserControlScan1.TabIndex = 43
        Me.UserControlScan1.Visible = False
        '
        'Seal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 165)
        Me.ControlBox = False
        Me.Controls.Add(Me.UserControlScan1)
        Me.Controls.Add(Me.txtSealInput)
        Me.Controls.Add(Me.lblInformationOne)
        Me.Controls.Add(Me.lblSeal)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "Seal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblSeal As System.Windows.Forms.Label
    Friend WithEvents lblInformationOne As System.Windows.Forms.Label
    Friend WithEvents txtSealInput As System.Windows.Forms.TextBox
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
End Class
