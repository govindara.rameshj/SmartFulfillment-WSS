﻿Public Class BankingSeal

    Public Enum BankingBagType
        Cash
        Cheque
        Rest
    End Enum

    Private mBankingBagType As BankingBagType
    Private mTenderDenomValues As TenderDenominationListCollection
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mdecTotal As Decimal

    Public Sub New(ByVal enumBankingBagType As BankingBagType, ByVal TenderDenomValues As TenderDenominationListCollection, _
                   ByVal strCurrencySymbol As String, ByVal strStoreID As String, ByVal decTotal As Decimal)
        Try
            mBankingBagType = enumBankingBagType
            mTenderDenomValues = TenderDenomValues
            mstrCurrencySymbol = strCurrencySymbol
            mstrStoreID = strStoreID
            mdecTotal = decTotal
            InitializeComponent()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub BankingSeal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleSeal
            Select Case mBankingBagType
                Case BankingBagType.Cash
                    Me.Text = My.Resources.Screen.ScreenTitleBankingSealCash

                    lblInformationOne.Text = "Banking Bag Confirmation - CASH" & Environment.NewLine & _
                                             "The bag has been set to " & mstrCurrencySymbol & mdecTotal.ToString("C", SetCurrencyFormat) & Environment.NewLine & _
                                             "Please place this in a collection bag and place a seal in the security device"
                Case BankingBagType.Cheque
                    Me.Text = My.Resources.Screen.ScreenTitleBankingSealCheque

                    lblInformationOne.Text = "Banking Bag Confirmation - CHEQUE" & Environment.NewLine & _
                                             "The bag has been set to " & mstrCurrencySymbol & mdecTotal.ToString("C", SetCurrencyFormat) & Environment.NewLine & _
                                             "Please place this in a collection bag and place a seal in the security device"
                Case BankingBagType.Rest
                    'not required for now!
            End Select
            DisplayGridFormat()
            DisplayGridPopulate()

            txtStoreNo.Text = mstrStoreID
            txtSlipNo.Focus()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub BankingSeal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5 : btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSlipNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSlipNo.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSlipNo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSlipNo.Leave
        Try
            If Me.ActiveControl Is btnExit Then Exit Sub

            If IsNumeric(txtSlipNo.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SlipNoNumeric)
                txtSlipNo.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSlipNo.Text = txtSlipNo.Text.Trim.PadLeft(3, CType("0", Char))
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave
        Try
            If Me.ActiveControl Is btnExit Then Exit Sub

            If Not txtSeal.MaskFull Then
                InformationMessageBox(My.Resources.InformationMessages.SealBanking)
                txtSeal.Focus()
                Exit Sub
            End If
            btnSave.PerformClick()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'validate slip no
            If IsNumeric(txtSlipNo.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SlipNoNumeric)
                txtSlipNo.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSlipNo.Text = txtSlipNo.Text.Trim.PadLeft(3, CType("0", Char))

            'validate seal no
            If Not txtSeal.MaskFull Then
                InformationMessageBox(My.Resources.InformationMessages.SealBanking)
                txtSeal.Focus()
                Exit Sub
            End If

            If UniqueSeal(txtSeal.Text) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SealNotUnique)
                Exit Sub
            End If

            Me.DialogResult = Windows.Forms.DialogResult.OK
            UserControlScan1.CloseScanner()
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.BankingSealGridColumn1, gcintColumnWidthAllTenders, False)     'Screen Column - Denom
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.BankingSealGridColumn2, gcintColumnWidthCashTender, False)     'Screen Column - Banking Value

        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, "0.00")

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        spdInput.Enabled = False
    End Sub

    Private Sub DisplayGridPopulate()
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As TenderDenominationList In mTenderDenomValues
            Select Case mBankingBagType
                Case BankingBagType.Cash
                    If obj.TenderID <> 1 Then Continue For

                Case BankingBagType.Cheque
                    If obj.TenderID <> 2 Then Continue For

                Case BankingBagType.Rest
                    'not required for now!
            End Select

            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)   'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.Amount)       'Screen Column - Banking Value
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Amounts")   'Screen Column - Denomination

        Select Case mBankingBagType
            Case BankingBagType.Cash
                SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)") 'Screen Column - Banking Value

            Case BankingBagType.Cheque
                SpreadCellFormula(CurrentSheet, intRowIndex, 1, "R1C")           'Screen Column - Banking Value

            Case BankingBagType.Rest
                'not required for now!
        End Select
    End Sub

#End Region

    Private Sub UserControlScan1_InputReceived(ByVal strInput As String) Handles UserControlScan1.ReceivedScannerInput
        txtSeal.Text = strInput
        btnSave.PerformClick()
    End Sub

End Class