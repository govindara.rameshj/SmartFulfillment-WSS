﻿Public Class CashDrop

    Private mstrAccountingModel As String
    Private mintBankingPeriodID As Integer
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String

    Public Event DataChange()

    Friend _CashDropVM As ICashDropVM

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intBankingPeriodID As Integer, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintBankingPeriodID = intBankingPeriodID
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
    End Sub

    Private Sub CashDrop_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleCashDrop
            LoadCashierDropDown()
            _CashDropVM = (New CashDropVMFactory).GetImplementation
            With _CashDropVM
                .SetCashDropGridControl(spdInput)
                .SetCashDropForm(Me)
                .CashDropGridFormat()
                .CashDropGridPopulate()
                .CashDropFormFormat()
            End With
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub CashDrop_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the total amount before the "zero amount" validation occurrs
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow                                          'active row
            ActiveCell = ActiveSheet.ActiveCell                                        'active cell

            'exit if denomination column
            If ActiveSheet.ActiveColumnIndex = 0 Then Exit Sub

            If Not _CashDropVM.CashDropGridActiveCellIsCommentsCell Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    btnSave.Enabled = False
                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    btnSave.Enabled = False
                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
            End If

            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
            btnSave.Enabled = True
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            Dim decTotalCashDrop As Decimal
            Dim strPickupSealNo As String
            Dim intSecondUserID As Integer

            Dim CashDropPickup As Banking.Core.Pickup
            Dim intRowCount As Integer
            Dim decDenominationValue As Decimal

            'able to leave grid without the last input being validated
            ActiveSheet = spdInput.ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow       'active row
            ActiveCell = ActiveSheet.ActiveCell     'active cell
            If Not _CashDropVM.CashDropGridActiveCellIsCommentsCell Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    btnSave.Enabled = False
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    btnSave.Enabled = False
                    Exit Sub
                End If
            End If
            'active cell validated, total automatically recalculated by spread
            'cash drop cannot be zero
            decTotalCashDrop = CDec(ActiveSheet.Cells(_CashDropVM.GetCashDropGridTotalsRowIndex, 1).Value)
            If decTotalCashDrop = 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountZero)

                btnSave.Enabled = False
                Exit Sub
            End If
            'display cash drop total
            If QuestionMessageBox("Total amount for cash drop is " & mstrCurrencySymbol & decTotalCashDrop.ToString("C", SetCurrencyFormat)) = Windows.Forms.DialogResult.No Then Exit Sub

            'second verification
            If AuthoriseCodeSecurity(False, mintFirstUserID, intSecondUserID) = False Then Exit Sub

            'pickup seal
            Using frmPickupSeal As New Seal(Seal.SealMode.Pickup, mstrCurrencySymbol & decTotalCashDrop.ToString("C", SetCurrencyFormat))
                If frmPickupSeal.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
                strPickupSealNo = frmPickupSeal.txtSealInput.Text
            End Using
            'persist cash drop
            CashDropPickup = New Banking.Core.Pickup(mintFirstUserID, intSecondUserID, mstrAccountingModel)
            With CashDropPickup
                .State = Banking.Core.Pickup.States.Pickup
                .LoadSystemFigures(mintBankingPeriodID, CInt(cmbUser.SelectedValue))

                .SetBagState(Banking.Core.BagStates.Sealed)
                '.SetComments("")
                .SetSealNumber(strPickupSealNo)

                'loop around grid and write non-zero amounts away
                intRowCount = _CashDropVM.GetCashDropGridTotalsRowIndex - 1
                For intRowIndex As Integer = 0 To intRowCount
                    decDenominationValue = CDec(ActiveSheet.Cells(intRowIndex, 1).Value)
                    If decDenominationValue > 0 Then
                        .SetDenomination(mstrCurrencyID, CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID, _
                                                         CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID, _
                                                         decDenominationValue)
                    End If
                Next
                _CashDropVM.AssignCommentsFromGrid(CashDropPickup)
                .NewBankingSaveCashDrop()
            End With
            'indicate that a cash drop has occurred
            RaiseEvent DataChange()

            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub LoadCashierDropDown()
        Dim Cashiers As New CashierCollection
        Dim CashDropCashierFilterVM As ICashDropCashierFilterVM = (New CashDropCashierFilterVMFactory).GetImplementation

        CashDropCashierFilterVM.LoadCashierDropDown(Cashiers, mintBankingPeriodID)

        cmbUser.DataSource = Cashiers
        cmbUser.ValueMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.UserId)
        cmbUser.DisplayMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.Employee)
    End Sub
#End Region
End Class