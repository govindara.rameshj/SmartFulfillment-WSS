﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EndOfDayCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance5 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance6 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance7 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance8 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnCompleteBanking = New System.Windows.Forms.Button
        Me.btnAddBankingComment = New System.Windows.Forms.Button
        Me.btnAddBankingBag = New System.Windows.Forms.Button
        Me.btnCancelAndExit = New System.Windows.Forms.Button
        Me.spdBankingCommentList = New FarPoint.Win.Spread.FpSpread
        Me.spdBankingBagList = New FarPoint.Win.Spread.FpSpread
        Me.GroupBoxBanking = New System.Windows.Forms.GroupBox
        Me.btnCompletePickup = New System.Windows.Forms.Button
        Me.btnAddPickupComment = New System.Windows.Forms.Button
        Me.btnAddPickupBag = New System.Windows.Forms.Button
        Me.spdPickupCommentList = New FarPoint.Win.Spread.FpSpread
        Me.GroupBoxPickup = New System.Windows.Forms.GroupBox
        Me.spdPickupBagList = New FarPoint.Win.Spread.FpSpread
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        CType(Me.spdBankingCommentList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spdBankingBagList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxBanking.SuspendLayout()
        CType(Me.spdPickupCommentList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxPickup.SuspendLayout()
        CType(Me.spdPickupBagList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCompleteBanking
        '
        Me.btnCompleteBanking.Location = New System.Drawing.Point(898, 123)
        Me.btnCompleteBanking.Name = "btnCompleteBanking"
        Me.btnCompleteBanking.Size = New System.Drawing.Size(172, 44)
        Me.btnCompleteBanking.TabIndex = 10
        Me.btnCompleteBanking.Text = "F6 Finalise Banking Bag(s) Entry"
        Me.btnCompleteBanking.UseVisualStyleBackColor = True
        '
        'btnAddBankingComment
        '
        Me.btnAddBankingComment.Location = New System.Drawing.Point(898, 71)
        Me.btnAddBankingComment.Name = "btnAddBankingComment"
        Me.btnAddBankingComment.Size = New System.Drawing.Size(172, 44)
        Me.btnAddBankingComment.TabIndex = 9
        Me.btnAddBankingComment.Text = "F5 Add Banking Comment"
        Me.btnAddBankingComment.UseVisualStyleBackColor = True
        '
        'btnAddBankingBag
        '
        Me.btnAddBankingBag.Location = New System.Drawing.Point(898, 19)
        Me.btnAddBankingBag.Name = "btnAddBankingBag"
        Me.btnAddBankingBag.Size = New System.Drawing.Size(172, 44)
        Me.btnAddBankingBag.TabIndex = 8
        Me.btnAddBankingBag.Text = "F4 Scan / Key"
        Me.btnAddBankingBag.UseVisualStyleBackColor = True
        '
        'btnCancelAndExit
        '
        Me.btnCancelAndExit.Location = New System.Drawing.Point(469, 690)
        Me.btnCancelAndExit.Name = "btnCancelAndExit"
        Me.btnCancelAndExit.Size = New System.Drawing.Size(166, 44)
        Me.btnCancelAndExit.TabIndex = 11
        Me.btnCancelAndExit.Text = "F12 Cancel And Exit"
        Me.btnCancelAndExit.UseVisualStyleBackColor = True
        '
        'spdBankingCommentList
        '
        Me.spdBankingCommentList.About = "3.0.2004.2005"
        Me.spdBankingCommentList.AccessibleDescription = ""
        Me.spdBankingCommentList.EditModeReplace = True
        Me.spdBankingCommentList.Location = New System.Drawing.Point(7, 161)
        Me.spdBankingCommentList.Name = "spdBankingCommentList"
        Me.spdBankingCommentList.Size = New System.Drawing.Size(885, 71)
        Me.spdBankingCommentList.TabIndex = 7
        Me.spdBankingCommentList.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance5.BackColor = System.Drawing.SystemColors.Info
        TipAppearance5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance5.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdBankingCommentList.TextTipAppearance = TipAppearance5
        Me.spdBankingCommentList.ActiveSheetIndex = -1
        '
        'spdBankingBagList
        '
        Me.spdBankingBagList.About = "3.0.2004.2005"
        Me.spdBankingBagList.AccessibleDescription = ""
        Me.spdBankingBagList.EditModeReplace = True
        Me.spdBankingBagList.Location = New System.Drawing.Point(6, 19)
        Me.spdBankingBagList.Name = "spdBankingBagList"
        Me.spdBankingBagList.Size = New System.Drawing.Size(886, 136)
        Me.spdBankingBagList.TabIndex = 6
        Me.spdBankingBagList.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance6.BackColor = System.Drawing.SystemColors.Info
        TipAppearance6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance6.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdBankingBagList.TextTipAppearance = TipAppearance6
        Me.spdBankingBagList.ActiveSheetIndex = -1
        '
        'GroupBoxBanking
        '
        Me.GroupBoxBanking.Controls.Add(Me.btnCompleteBanking)
        Me.GroupBoxBanking.Controls.Add(Me.btnAddBankingComment)
        Me.GroupBoxBanking.Controls.Add(Me.btnAddBankingBag)
        Me.GroupBoxBanking.Controls.Add(Me.spdBankingCommentList)
        Me.GroupBoxBanking.Controls.Add(Me.spdBankingBagList)
        Me.GroupBoxBanking.Location = New System.Drawing.Point(5, 444)
        Me.GroupBoxBanking.Name = "GroupBoxBanking"
        Me.GroupBoxBanking.Size = New System.Drawing.Size(1091, 240)
        Me.GroupBoxBanking.TabIndex = 17
        Me.GroupBoxBanking.TabStop = False
        Me.GroupBoxBanking.Text = "Banking"
        '
        'btnCompletePickup
        '
        Me.btnCompletePickup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCompletePickup.Location = New System.Drawing.Point(898, 123)
        Me.btnCompletePickup.Name = "btnCompletePickup"
        Me.btnCompletePickup.Size = New System.Drawing.Size(172, 44)
        Me.btnCompletePickup.TabIndex = 5
        Me.btnCompletePickup.Text = "F3 Finalize Pickup Bag(s) Entry"
        Me.btnCompletePickup.UseVisualStyleBackColor = True
        '
        'btnAddPickupComment
        '
        Me.btnAddPickupComment.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAddPickupComment.Location = New System.Drawing.Point(898, 71)
        Me.btnAddPickupComment.Name = "btnAddPickupComment"
        Me.btnAddPickupComment.Size = New System.Drawing.Size(172, 44)
        Me.btnAddPickupComment.TabIndex = 4
        Me.btnAddPickupComment.Text = "F2 Add Pickup Comment"
        Me.btnAddPickupComment.UseVisualStyleBackColor = True
        '
        'btnAddPickupBag
        '
        Me.btnAddPickupBag.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAddPickupBag.Location = New System.Drawing.Point(898, 19)
        Me.btnAddPickupBag.Name = "btnAddPickupBag"
        Me.btnAddPickupBag.Size = New System.Drawing.Size(172, 44)
        Me.btnAddPickupBag.TabIndex = 3
        Me.btnAddPickupBag.Text = "F1 Scan / Key"
        Me.btnAddPickupBag.UseVisualStyleBackColor = True
        '
        'spdPickupCommentList
        '
        Me.spdPickupCommentList.About = "3.0.2004.2005"
        Me.spdPickupCommentList.AccessibleDescription = ""
        Me.spdPickupCommentList.EditModeReplace = True
        Me.spdPickupCommentList.Location = New System.Drawing.Point(7, 352)
        Me.spdPickupCommentList.Name = "spdPickupCommentList"
        Me.spdPickupCommentList.Size = New System.Drawing.Size(885, 71)
        Me.spdPickupCommentList.TabIndex = 2
        Me.spdPickupCommentList.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance7.BackColor = System.Drawing.SystemColors.Info
        TipAppearance7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance7.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdPickupCommentList.TextTipAppearance = TipAppearance7
        Me.spdPickupCommentList.ActiveSheetIndex = -1
        '
        'GroupBoxPickup
        '
        Me.GroupBoxPickup.Controls.Add(Me.btnCompletePickup)
        Me.GroupBoxPickup.Controls.Add(Me.btnAddPickupComment)
        Me.GroupBoxPickup.Controls.Add(Me.btnAddPickupBag)
        Me.GroupBoxPickup.Controls.Add(Me.spdPickupCommentList)
        Me.GroupBoxPickup.Controls.Add(Me.spdPickupBagList)
        Me.GroupBoxPickup.Location = New System.Drawing.Point(5, 6)
        Me.GroupBoxPickup.Name = "GroupBoxPickup"
        Me.GroupBoxPickup.Size = New System.Drawing.Size(1091, 432)
        Me.GroupBoxPickup.TabIndex = 16
        Me.GroupBoxPickup.TabStop = False
        Me.GroupBoxPickup.Text = "Pickup"
        '
        'spdPickupBagList
        '
        Me.spdPickupBagList.About = "3.0.2004.2005"
        Me.spdPickupBagList.AccessibleDescription = ""
        Me.spdPickupBagList.EditModeReplace = True
        Me.spdPickupBagList.Location = New System.Drawing.Point(6, 19)
        Me.spdPickupBagList.Name = "spdPickupBagList"
        Me.spdPickupBagList.Size = New System.Drawing.Size(886, 327)
        Me.spdPickupBagList.TabIndex = 1
        Me.spdPickupBagList.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance8.BackColor = System.Drawing.SystemColors.Info
        TipAppearance8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance8.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdPickupBagList.TextTipAppearance = TipAppearance8
        Me.spdPickupBagList.ActiveSheetIndex = -1
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(34, 691)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(43, 44)
        Me.UserControlScan1.TabIndex = 18
        Me.UserControlScan1.Visible = False
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(11, 703)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 19
        Me.txtSeal.Visible = False
        '
        'EndOfDayCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1101, 740)
        Me.ControlBox = False
        Me.Controls.Add(Me.UserControlScan1)
        Me.Controls.Add(Me.btnCancelAndExit)
        Me.Controls.Add(Me.GroupBoxBanking)
        Me.Controls.Add(Me.GroupBoxPickup)
        Me.Controls.Add(Me.txtSeal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "EndOfDayCheck"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "End Of Day Check"
        CType(Me.spdBankingCommentList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spdBankingBagList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxBanking.ResumeLayout(False)
        CType(Me.spdPickupCommentList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxPickup.ResumeLayout(False)
        CType(Me.spdPickupBagList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCompleteBanking As System.Windows.Forms.Button
    Friend WithEvents btnAddBankingComment As System.Windows.Forms.Button
    Friend WithEvents btnAddBankingBag As System.Windows.Forms.Button
    Friend WithEvents btnCancelAndExit As System.Windows.Forms.Button
    Private WithEvents spdBankingCommentList As FarPoint.Win.Spread.FpSpread
    Private WithEvents spdBankingBagList As FarPoint.Win.Spread.FpSpread
    Friend WithEvents GroupBoxBanking As System.Windows.Forms.GroupBox
    Friend WithEvents btnCompletePickup As System.Windows.Forms.Button
    Friend WithEvents btnAddPickupComment As System.Windows.Forms.Button
    Friend WithEvents btnAddPickupBag As System.Windows.Forms.Button
    Private WithEvents spdPickupCommentList As FarPoint.Win.Spread.FpSpread
    Friend WithEvents GroupBoxPickup As System.Windows.Forms.GroupBox
    Private WithEvents spdPickupBagList As FarPoint.Win.Spread.FpSpread
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
End Class
