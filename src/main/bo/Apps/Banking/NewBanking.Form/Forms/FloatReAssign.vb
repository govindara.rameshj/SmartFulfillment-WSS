﻿Public Class FloatReAssign

    Private mAccountingModel As String
    Private mintBankingPeriodID As Integer
    Private mFirstUserID As Integer
    Private mExistingCashierID As Integer
    Private mintFloatID As Integer
    Private mFloatValue As Decimal

    Public Event DataChange()

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intBankingPeriodID As Integer, ByVal intFloatID As Integer)
        InitializeComponent()

        mintBankingPeriodID = intBankingPeriodID
        mintFloatID = intFloatID
    End Sub

    Public Sub New(ByVal AccountingModel As String, ByVal intBankingPeriodID As Integer, _
                   ByVal FirstUserID As Integer, ByVal ExistingCashierID As Integer, _
                   ByVal intFloatID As Integer, ByVal FloatValue As Decimal)

        InitializeComponent()

        mAccountingModel = AccountingModel
        mintBankingPeriodID = intBankingPeriodID
        mFirstUserID = FirstUserID
        mExistingCashierID = ExistingCashierID
        mintFloatID = intFloatID
        mFloatValue = FloatValue

    End Sub

    Private Sub FloatAssign_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleFloatReAssign

            LoadUnFloatedCashiers(mintBankingPeriodID)
            If cmbCashier.Items.Count > 0 Then cmbCashier.SelectedIndex = 0
            DisplayGridFormat()
            DisplayGridPopulate(mintFloatID)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub FloatAssign_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnAssignFloat.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignFloat.Click

        Try

            If QuestionMessageBox("Are you sure you want to re-assign this float?") = Windows.Forms.DialogResult.No Then Exit Sub

            Dim ReAssignFloat As Banking.Core.FloatBanking

            ReAssignFloat = New Banking.Core.FloatBanking()
            ReAssignFloat.NewBankingReAssignFloat(mExistingCashierID, CInt(cmbCashier.SelectedValue), mintBankingPeriodID, Currency.GetDefaultCurrencyId(), mAccountingModel, mintFloatID, mFloatValue)

            'indicate that a float re-assignement has occurred
            RaiseEvent DataChange()

            FindForm.Close()

        Catch ex As Exception

            ErrorHandler(ex)

        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub LoadUnFloatedCashiers(ByVal intPeriodID As Integer)
        Dim colCashier As New CashierCollection

        colCashier.LoadDataAssignFloat(intPeriodID)

        cmbCashier.DataSource = colCashier
        cmbCashier.ValueMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.UserId)
        cmbCashier.DisplayMember = GetPropertyName(Function(f As NewBanking.Core.Cashier) f.Employee)
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatAssignInputGridColumn1, gcintColumnWidthCashTender, False)   'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatAssignInputGridColumn2, gcintColumnWidthMoney, False)        'Screen Column - Float

        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
    End Sub

    Private Sub DisplayGridPopulate(ByVal intFloatID As Integer)
        Dim colTenderDenominationList As TenderDenominationListCollection
        Dim colFloatBag As FloatBagCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        colTenderDenominationList = New TenderDenominationListCollection
        colTenderDenominationList.LoadData()

        colFloatBag = New FloatBagCollection
        colFloatBag.LoadData(intFloatID)

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As TenderDenominationList In colTenderDenominationList
            If obj.TenderID <> 1 Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)                                                'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)                                    'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, colFloatBag.DenominationValue(obj.DenominationID)) 'Screen Column - Tray Entry
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)

        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Amounts")   'Screen Column - Denomination
        SpreadCellValue(CurrentSheet, intRowIndex, 1, 0)                 'Screen Column - Tray Entry
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)") 'Screen Column - Tray Entry, sum denominations
    End Sub

#End Region

End Class