﻿Public Class SafeCheckReportView

    Friend _Report As SafeCheckReport

    Friend _SelectedPeriod As Period
    Friend _StoreID As String
    Friend _StoreName As String

    Public Sub New(ByVal SelectedPeriod As Period, ByVal StoreID As String, ByVal StoreName As String)

        InitializeComponent()

        SetSelectedPeriod(SelectedPeriod)
        SetStoreID(StoreID)
        SetStoreName(StoreName)

        CreateReport()

    End Sub

    Private Sub SafeCheckReportView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ConfigureReport()

        spdSafeCheckReport.VerticalScrollBarPolicy = ScrollBarPolicy.AsNeeded

    End Sub

    Private Sub SafeCheckDisplayReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F9 : btnPrint.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select

        Catch ex As Exception
            ErrorHandler(ex)

        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Try
            _Report.ExecuteReport()

        Catch ex As Exception
            ErrorHandler(ex)

        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Try
            Me.Close()

        Catch ex As Exception
            ErrorHandler(ex)

        End Try

    End Sub


#Region "Private Functions & Procedures"

    Friend Overridable Sub SetSelectedPeriod(ByVal SelectedPeriod As Core.Period)

        _SelectedPeriod = SelectedPeriod

    End Sub

    Friend Overridable Sub SetStoreID(ByVal StoreID As String)

        _StoreID = StoreID

    End Sub

    Friend Overridable Sub SetStoreName(ByVal StoreName As String)

        _StoreName = StoreName

    End Sub

    Friend Overridable Sub CreateReport()

        _Report = New SafeCheckReport(_SelectedPeriod, _StoreID, _StoreName, spdSafeCheckReport)

    End Sub

    Friend Overridable Sub ConfigureReport()

        _Report.ConfigureReport()

    End Sub

#End Region

End Class