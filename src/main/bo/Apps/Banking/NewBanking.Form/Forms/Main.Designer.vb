﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.splitTop = New System.Windows.Forms.SplitContainer
        Me.lblBankingDate = New System.Windows.Forms.Label
        Me.cmbPeriod = New System.Windows.Forms.ComboBox
        Me.splitFloatedCashier = New System.Windows.Forms.SplitContainer
        Me.gbFloatedCashier = New System.Windows.Forms.GroupBox
        Me.spdFloatedCashier = New FarPoint.Win.Spread.FpSpread
        Me.splitCashDropAndUnFloatedPickup = New System.Windows.Forms.SplitContainer
        Me.gbCashDropUnFloatedPickup = New System.Windows.Forms.GroupBox
        Me.spdCashDropAndUnFloatedPickup = New FarPoint.Win.Spread.FpSpread
        Me.splitBottom = New System.Windows.Forms.SplitContainer
        Me.gbEndOfDayManagementCheck = New System.Windows.Forms.GroupBox
        Me.spdEndOfDayManagementCheck = New FarPoint.Win.Spread.FpSpread
        Me.btnScreenPrint = New System.Windows.Forms.Button
        Me.btnCreateReturnFloat = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnEndOfDayCheck = New System.Windows.Forms.Button
        Me.btnPickupReport = New System.Windows.Forms.Button
        Me.btnSafeCheckReport = New System.Windows.Forms.Button
        Me.btnBagCollection = New System.Windows.Forms.Button
        Me.btnBanking = New System.Windows.Forms.Button
        Me.btnFloatedPickup = New System.Windows.Forms.Button
        Me.btnSafeMaintenance = New System.Windows.Forms.Button
        Me.btnAssignFloat = New System.Windows.Forms.Button
        Me.btnUnFloatedPickup = New System.Windows.Forms.Button
        Me.btnCashDrop = New System.Windows.Forms.Button
        Me.splitTop.Panel1.SuspendLayout()
        Me.splitTop.Panel2.SuspendLayout()
        Me.splitTop.SuspendLayout()
        Me.splitFloatedCashier.Panel1.SuspendLayout()
        Me.splitFloatedCashier.Panel2.SuspendLayout()
        Me.splitFloatedCashier.SuspendLayout()
        Me.gbFloatedCashier.SuspendLayout()
        CType(Me.spdFloatedCashier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitCashDropAndUnFloatedPickup.Panel1.SuspendLayout()
        Me.splitCashDropAndUnFloatedPickup.Panel2.SuspendLayout()
        Me.splitCashDropAndUnFloatedPickup.SuspendLayout()
        Me.gbCashDropUnFloatedPickup.SuspendLayout()
        CType(Me.spdCashDropAndUnFloatedPickup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitBottom.Panel1.SuspendLayout()
        Me.splitBottom.Panel2.SuspendLayout()
        Me.splitBottom.SuspendLayout()
        Me.gbEndOfDayManagementCheck.SuspendLayout()
        CType(Me.spdEndOfDayManagementCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'splitTop
        '
        Me.splitTop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitTop.IsSplitterFixed = True
        Me.splitTop.Location = New System.Drawing.Point(0, 0)
        Me.splitTop.Name = "splitTop"
        Me.splitTop.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitTop.Panel1
        '
        Me.splitTop.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.splitTop.Panel1.Controls.Add(Me.lblBankingDate)
        Me.splitTop.Panel1.Controls.Add(Me.cmbPeriod)
        Me.splitTop.Panel1MinSize = 40
        '
        'splitTop.Panel2
        '
        Me.splitTop.Panel2.Controls.Add(Me.splitFloatedCashier)
        Me.splitTop.Size = New System.Drawing.Size(1214, 779)
        Me.splitTop.SplitterDistance = 40
        Me.splitTop.TabIndex = 0
        '
        'lblBankingDate
        '
        Me.lblBankingDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankingDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblBankingDate.Location = New System.Drawing.Point(13, 9)
        Me.lblBankingDate.Name = "lblBankingDate"
        Me.lblBankingDate.Size = New System.Drawing.Size(124, 22)
        Me.lblBankingDate.TabIndex = 40
        Me.lblBankingDate.Text = "Select Banking Day"
        Me.lblBankingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbPeriod
        '
        Me.cmbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPeriod.FormattingEnabled = True
        Me.cmbPeriod.Location = New System.Drawing.Point(143, 9)
        Me.cmbPeriod.Name = "cmbPeriod"
        Me.cmbPeriod.Size = New System.Drawing.Size(248, 21)
        Me.cmbPeriod.TabIndex = 1
        '
        'splitFloatedCashier
        '
        Me.splitFloatedCashier.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitFloatedCashier.Location = New System.Drawing.Point(0, 0)
        Me.splitFloatedCashier.Name = "splitFloatedCashier"
        Me.splitFloatedCashier.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitFloatedCashier.Panel1
        '
        Me.splitFloatedCashier.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.splitFloatedCashier.Panel1.Controls.Add(Me.gbFloatedCashier)
        '
        'splitFloatedCashier.Panel2
        '
        Me.splitFloatedCashier.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.splitFloatedCashier.Panel2.Controls.Add(Me.splitCashDropAndUnFloatedPickup)
        Me.splitFloatedCashier.Size = New System.Drawing.Size(1214, 735)
        Me.splitFloatedCashier.SplitterDistance = 383
        Me.splitFloatedCashier.TabIndex = 0
        '
        'gbFloatedCashier
        '
        Me.gbFloatedCashier.Controls.Add(Me.spdFloatedCashier)
        Me.gbFloatedCashier.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFloatedCashier.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFloatedCashier.Location = New System.Drawing.Point(0, 0)
        Me.gbFloatedCashier.Name = "gbFloatedCashier"
        Me.gbFloatedCashier.Size = New System.Drawing.Size(1214, 383)
        Me.gbFloatedCashier.TabIndex = 0
        Me.gbFloatedCashier.TabStop = False
        Me.gbFloatedCashier.Text = "Floated Cashier"
        '
        'spdFloatedCashier
        '
        Me.spdFloatedCashier.About = "3.0.2004.2005"
        Me.spdFloatedCashier.AccessibleDescription = ""
        Me.spdFloatedCashier.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdFloatedCashier.EditModeReplace = True
        Me.spdFloatedCashier.Location = New System.Drawing.Point(3, 16)
        Me.spdFloatedCashier.Name = "spdFloatedCashier"
        Me.spdFloatedCashier.Size = New System.Drawing.Size(1208, 364)
        Me.spdFloatedCashier.TabIndex = 2
        Me.spdFloatedCashier.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdFloatedCashier.TextTipAppearance = TipAppearance1
        Me.spdFloatedCashier.ActiveSheetIndex = -1
        '
        'splitCashDropAndUnFloatedPickup
        '
        Me.splitCashDropAndUnFloatedPickup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitCashDropAndUnFloatedPickup.Location = New System.Drawing.Point(0, 0)
        Me.splitCashDropAndUnFloatedPickup.Name = "splitCashDropAndUnFloatedPickup"
        Me.splitCashDropAndUnFloatedPickup.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitCashDropAndUnFloatedPickup.Panel1
        '
        Me.splitCashDropAndUnFloatedPickup.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.splitCashDropAndUnFloatedPickup.Panel1.Controls.Add(Me.gbCashDropUnFloatedPickup)
        '
        'splitCashDropAndUnFloatedPickup.Panel2
        '
        Me.splitCashDropAndUnFloatedPickup.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.splitCashDropAndUnFloatedPickup.Panel2.Controls.Add(Me.splitBottom)
        Me.splitCashDropAndUnFloatedPickup.Size = New System.Drawing.Size(1214, 348)
        Me.splitCashDropAndUnFloatedPickup.SplitterDistance = 220
        Me.splitCashDropAndUnFloatedPickup.TabIndex = 0
        '
        'gbCashDropUnFloatedPickup
        '
        Me.gbCashDropUnFloatedPickup.Controls.Add(Me.spdCashDropAndUnFloatedPickup)
        Me.gbCashDropUnFloatedPickup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCashDropUnFloatedPickup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCashDropUnFloatedPickup.Location = New System.Drawing.Point(0, 0)
        Me.gbCashDropUnFloatedPickup.Name = "gbCashDropUnFloatedPickup"
        Me.gbCashDropUnFloatedPickup.Size = New System.Drawing.Size(1214, 220)
        Me.gbCashDropUnFloatedPickup.TabIndex = 0
        Me.gbCashDropUnFloatedPickup.TabStop = False
        Me.gbCashDropUnFloatedPickup.Text = "(UnFloated Cashier/Design Consultant Pickup) / Cash Drops"
        '
        'spdCashDropAndUnFloatedPickup
        '
        Me.spdCashDropAndUnFloatedPickup.About = "3.0.2004.2005"
        Me.spdCashDropAndUnFloatedPickup.AccessibleDescription = ""
        Me.spdCashDropAndUnFloatedPickup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdCashDropAndUnFloatedPickup.EditModeReplace = True
        Me.spdCashDropAndUnFloatedPickup.Location = New System.Drawing.Point(3, 16)
        Me.spdCashDropAndUnFloatedPickup.Name = "spdCashDropAndUnFloatedPickup"
        Me.spdCashDropAndUnFloatedPickup.Size = New System.Drawing.Size(1208, 201)
        Me.spdCashDropAndUnFloatedPickup.TabIndex = 3
        Me.spdCashDropAndUnFloatedPickup.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdCashDropAndUnFloatedPickup.TextTipAppearance = TipAppearance2
        Me.spdCashDropAndUnFloatedPickup.ActiveSheetIndex = -1
        '
        'splitBottom
        '
        Me.splitBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitBottom.IsSplitterFixed = True
        Me.splitBottom.Location = New System.Drawing.Point(0, 0)
        Me.splitBottom.Name = "splitBottom"
        Me.splitBottom.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitBottom.Panel1
        '
        Me.splitBottom.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.splitBottom.Panel1.Controls.Add(Me.gbEndOfDayManagementCheck)
        '
        'splitBottom.Panel2
        '
        Me.splitBottom.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.splitBottom.Panel2.Controls.Add(Me.btnScreenPrint)
        Me.splitBottom.Panel2.Controls.Add(Me.btnCreateReturnFloat)
        Me.splitBottom.Panel2.Controls.Add(Me.btnExit)
        Me.splitBottom.Panel2.Controls.Add(Me.btnEndOfDayCheck)
        Me.splitBottom.Panel2.Controls.Add(Me.btnPickupReport)
        Me.splitBottom.Panel2.Controls.Add(Me.btnSafeCheckReport)
        Me.splitBottom.Panel2.Controls.Add(Me.btnBagCollection)
        Me.splitBottom.Panel2.Controls.Add(Me.btnBanking)
        Me.splitBottom.Panel2.Controls.Add(Me.btnFloatedPickup)
        Me.splitBottom.Panel2.Controls.Add(Me.btnSafeMaintenance)
        Me.splitBottom.Panel2.Controls.Add(Me.btnAssignFloat)
        Me.splitBottom.Panel2.Controls.Add(Me.btnUnFloatedPickup)
        Me.splitBottom.Panel2.Controls.Add(Me.btnCashDrop)
        Me.splitBottom.Panel2MinSize = 50
        Me.splitBottom.Size = New System.Drawing.Size(1214, 124)
        Me.splitBottom.SplitterDistance = 70
        Me.splitBottom.TabIndex = 0
        '
        'gbEndOfDayManagementCheck
        '
        Me.gbEndOfDayManagementCheck.Controls.Add(Me.spdEndOfDayManagementCheck)
        Me.gbEndOfDayManagementCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbEndOfDayManagementCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEndOfDayManagementCheck.Location = New System.Drawing.Point(0, 0)
        Me.gbEndOfDayManagementCheck.Name = "gbEndOfDayManagementCheck"
        Me.gbEndOfDayManagementCheck.Size = New System.Drawing.Size(1214, 70)
        Me.gbEndOfDayManagementCheck.TabIndex = 0
        Me.gbEndOfDayManagementCheck.TabStop = False
        Me.gbEndOfDayManagementCheck.Text = "E.O.D Management Check"
        '
        'spdEndOfDayManagementCheck
        '
        Me.spdEndOfDayManagementCheck.About = "3.0.2004.2005"
        Me.spdEndOfDayManagementCheck.AccessibleDescription = ""
        Me.spdEndOfDayManagementCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdEndOfDayManagementCheck.EditModeReplace = True
        Me.spdEndOfDayManagementCheck.Location = New System.Drawing.Point(3, 16)
        Me.spdEndOfDayManagementCheck.Name = "spdEndOfDayManagementCheck"
        Me.spdEndOfDayManagementCheck.Size = New System.Drawing.Size(1208, 51)
        Me.spdEndOfDayManagementCheck.TabIndex = 4
        Me.spdEndOfDayManagementCheck.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdEndOfDayManagementCheck.TextTipAppearance = TipAppearance3
        Me.spdEndOfDayManagementCheck.VerticalScrollBarWidth = 15
        Me.spdEndOfDayManagementCheck.ActiveSheetIndex = -1
        '
        'btnScreenPrint
        '
        Me.btnScreenPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnScreenPrint.Location = New System.Drawing.Point(1157, 6)
        Me.btnScreenPrint.Name = "btnScreenPrint"
        Me.btnScreenPrint.Size = New System.Drawing.Size(54, 44)
        Me.btnScreenPrint.TabIndex = 17
        Me.btnScreenPrint.Text = "Screen Print"
        Me.btnScreenPrint.UseVisualStyleBackColor = True
        '
        'btnCreateReturnFloat
        '
        Me.btnCreateReturnFloat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCreateReturnFloat.Location = New System.Drawing.Point(848, 6)
        Me.btnCreateReturnFloat.Name = "btnCreateReturnFloat"
        Me.btnCreateReturnFloat.Size = New System.Drawing.Size(126, 44)
        Me.btnCreateReturnFloat.TabIndex = 14
        Me.btnCreateReturnFloat.Text = "F10 Create / Return Float"
        Me.btnCreateReturnFloat.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.Location = New System.Drawing.Point(1097, 6)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 16
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnEndOfDayCheck
        '
        Me.btnEndOfDayCheck.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnEndOfDayCheck.Location = New System.Drawing.Point(772, 6)
        Me.btnEndOfDayCheck.Name = "btnEndOfDayCheck"
        Me.btnEndOfDayCheck.Size = New System.Drawing.Size(70, 44)
        Me.btnEndOfDayCheck.TabIndex = 13
        Me.btnEndOfDayCheck.Text = "F9 Banking Report"
        Me.btnEndOfDayCheck.UseVisualStyleBackColor = True
        '
        'btnPickupReport
        '
        Me.btnPickupReport.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPickupReport.Location = New System.Drawing.Point(700, 6)
        Me.btnPickupReport.Name = "btnPickupReport"
        Me.btnPickupReport.Size = New System.Drawing.Size(66, 44)
        Me.btnPickupReport.TabIndex = 12
        Me.btnPickupReport.Text = "F8 Pickup Report"
        Me.btnPickupReport.UseVisualStyleBackColor = True
        '
        'btnSafeCheckReport
        '
        Me.btnSafeCheckReport.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSafeCheckReport.Location = New System.Drawing.Point(608, 6)
        Me.btnSafeCheckReport.Name = "btnSafeCheckReport"
        Me.btnSafeCheckReport.Size = New System.Drawing.Size(86, 44)
        Me.btnSafeCheckReport.TabIndex = 11
        Me.btnSafeCheckReport.Text = "F7 Safe Check Report"
        Me.btnSafeCheckReport.UseVisualStyleBackColor = True
        '
        'btnBagCollection
        '
        Me.btnBagCollection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnBagCollection.Location = New System.Drawing.Point(510, 6)
        Me.btnBagCollection.Name = "btnBagCollection"
        Me.btnBagCollection.Size = New System.Drawing.Size(92, 44)
        Me.btnBagCollection.TabIndex = 10
        Me.btnBagCollection.Text = "F6 Banking Bag Collection"
        Me.btnBagCollection.UseVisualStyleBackColor = True
        '
        'btnBanking
        '
        Me.btnBanking.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnBanking.Location = New System.Drawing.Point(431, 6)
        Me.btnBanking.Name = "btnBanking"
        Me.btnBanking.Size = New System.Drawing.Size(73, 44)
        Me.btnBanking.TabIndex = 9
        Me.btnBanking.Text = "F5 Banking"
        Me.btnBanking.UseVisualStyleBackColor = True
        '
        'btnFloatedPickup
        '
        Me.btnFloatedPickup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnFloatedPickup.Location = New System.Drawing.Point(197, 6)
        Me.btnFloatedPickup.Name = "btnFloatedPickup"
        Me.btnFloatedPickup.Size = New System.Drawing.Size(104, 44)
        Me.btnFloatedPickup.TabIndex = 7
        Me.btnFloatedPickup.Text = "F3 Floated Pickup"
        Me.btnFloatedPickup.UseVisualStyleBackColor = True
        '
        'btnSafeMaintenance
        '
        Me.btnSafeMaintenance.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSafeMaintenance.Location = New System.Drawing.Point(980, 6)
        Me.btnSafeMaintenance.Name = "btnSafeMaintenance"
        Me.btnSafeMaintenance.Size = New System.Drawing.Size(111, 44)
        Me.btnSafeMaintenance.TabIndex = 15
        Me.btnSafeMaintenance.Text = "F11 Safe Maintenance"
        Me.btnSafeMaintenance.UseVisualStyleBackColor = True
        '
        'btnAssignFloat
        '
        Me.btnAssignFloat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAssignFloat.Location = New System.Drawing.Point(8, 6)
        Me.btnAssignFloat.Name = "btnAssignFloat"
        Me.btnAssignFloat.Size = New System.Drawing.Size(94, 44)
        Me.btnAssignFloat.TabIndex = 5
        Me.btnAssignFloat.Text = "F1 Assign Float"
        Me.btnAssignFloat.UseVisualStyleBackColor = True
        '
        'btnUnFloatedPickup
        '
        Me.btnUnFloatedPickup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnUnFloatedPickup.Location = New System.Drawing.Point(307, 6)
        Me.btnUnFloatedPickup.Name = "btnUnFloatedPickup"
        Me.btnUnFloatedPickup.Size = New System.Drawing.Size(118, 44)
        Me.btnUnFloatedPickup.TabIndex = 8
        Me.btnUnFloatedPickup.Text = "F4 UnFloated Pickup"
        Me.btnUnFloatedPickup.UseVisualStyleBackColor = True
        '
        'btnCashDrop
        '
        Me.btnCashDrop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCashDrop.Location = New System.Drawing.Point(108, 6)
        Me.btnCashDrop.Name = "btnCashDrop"
        Me.btnCashDrop.Size = New System.Drawing.Size(83, 44)
        Me.btnCashDrop.TabIndex = 6
        Me.btnCashDrop.Text = "F2 Cash Drop"
        Me.btnCashDrop.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.splitTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        Me.Size = New System.Drawing.Size(1214, 779)
        Me.splitTop.Panel1.ResumeLayout(False)
        Me.splitTop.Panel2.ResumeLayout(False)
        Me.splitTop.ResumeLayout(False)
        Me.splitFloatedCashier.Panel1.ResumeLayout(False)
        Me.splitFloatedCashier.Panel2.ResumeLayout(False)
        Me.splitFloatedCashier.ResumeLayout(False)
        Me.gbFloatedCashier.ResumeLayout(False)
        CType(Me.spdFloatedCashier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitCashDropAndUnFloatedPickup.Panel1.ResumeLayout(False)
        Me.splitCashDropAndUnFloatedPickup.Panel2.ResumeLayout(False)
        Me.splitCashDropAndUnFloatedPickup.ResumeLayout(False)
        Me.gbCashDropUnFloatedPickup.ResumeLayout(False)
        CType(Me.spdCashDropAndUnFloatedPickup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitBottom.Panel1.ResumeLayout(False)
        Me.splitBottom.Panel2.ResumeLayout(False)
        Me.splitBottom.ResumeLayout(False)
        Me.gbEndOfDayManagementCheck.ResumeLayout(False)
        CType(Me.spdEndOfDayManagementCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splitTop As System.Windows.Forms.SplitContainer
    Friend WithEvents splitFloatedCashier As System.Windows.Forms.SplitContainer
    Friend WithEvents splitCashDropAndUnFloatedPickup As System.Windows.Forms.SplitContainer
    Friend WithEvents splitBottom As System.Windows.Forms.SplitContainer
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnEndOfDayCheck As System.Windows.Forms.Button
    Friend WithEvents btnPickupReport As System.Windows.Forms.Button
    Friend WithEvents btnSafeCheckReport As System.Windows.Forms.Button
    Friend WithEvents btnBagCollection As System.Windows.Forms.Button
    Friend WithEvents btnBanking As System.Windows.Forms.Button
    Friend WithEvents btnFloatedPickup As System.Windows.Forms.Button
    Friend WithEvents btnSafeMaintenance As System.Windows.Forms.Button
    Friend WithEvents btnAssignFloat As System.Windows.Forms.Button
    Friend WithEvents btnUnFloatedPickup As System.Windows.Forms.Button
    Friend WithEvents btnCashDrop As System.Windows.Forms.Button
    Friend WithEvents lblBankingDate As System.Windows.Forms.Label
    Friend WithEvents cmbPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents gbFloatedCashier As System.Windows.Forms.GroupBox
    Friend WithEvents gbCashDropUnFloatedPickup As System.Windows.Forms.GroupBox
    Friend WithEvents gbEndOfDayManagementCheck As System.Windows.Forms.GroupBox
    Private WithEvents spdFloatedCashier As FarPoint.Win.Spread.FpSpread
    Private WithEvents spdCashDropAndUnFloatedPickup As FarPoint.Win.Spread.FpSpread
    Private WithEvents spdEndOfDayManagementCheck As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnCreateReturnFloat As System.Windows.Forms.Button
    Friend WithEvents btnScreenPrint As System.Windows.Forms.Button

End Class
