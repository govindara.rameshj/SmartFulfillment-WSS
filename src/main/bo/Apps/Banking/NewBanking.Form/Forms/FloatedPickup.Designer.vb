﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FloatedPickup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnCashierReport = New System.Windows.Forms.Button
        Me.btnCompletePickup = New System.Windows.Forms.Button
        Me.btnReAssignFloat = New System.Windows.Forms.Button
        Me.spdDisplay = New FarPoint.Win.Spread.FpSpread
        CType(Me.spdDisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(433, 418)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnCashierReport
        '
        Me.btnCashierReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCashierReport.Location = New System.Drawing.Point(321, 418)
        Me.btnCashierReport.Name = "btnCashierReport"
        Me.btnCashierReport.Size = New System.Drawing.Size(104, 44)
        Me.btnCashierReport.TabIndex = 4
        Me.btnCashierReport.Text = "F8 Cashier Report"
        Me.btnCashierReport.UseVisualStyleBackColor = True
        '
        'btnCompletePickup
        '
        Me.btnCompletePickup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCompletePickup.Location = New System.Drawing.Point(87, 418)
        Me.btnCompletePickup.Name = "btnCompletePickup"
        Me.btnCompletePickup.Size = New System.Drawing.Size(113, 44)
        Me.btnCompletePickup.TabIndex = 2
        Me.btnCompletePickup.Text = "F1 Complete Pickup"
        Me.btnCompletePickup.UseVisualStyleBackColor = True
        '
        'btnReAssignFloat
        '
        Me.btnReAssignFloat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReAssignFloat.Location = New System.Drawing.Point(208, 418)
        Me.btnReAssignFloat.Name = "btnReAssignFloat"
        Me.btnReAssignFloat.Size = New System.Drawing.Size(105, 44)
        Me.btnReAssignFloat.TabIndex = 3
        Me.btnReAssignFloat.Text = "F2 Re-Assign Float"
        Me.btnReAssignFloat.UseVisualStyleBackColor = True
        '
        'spdDisplay
        '
        Me.spdDisplay.About = "3.0.2004.2005"
        Me.spdDisplay.AccessibleDescription = ""
        Me.spdDisplay.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdDisplay.EditModeReplace = True
        Me.spdDisplay.Location = New System.Drawing.Point(17, 12)
        Me.spdDisplay.Name = "spdDisplay"
        Me.spdDisplay.Size = New System.Drawing.Size(540, 400)
        Me.spdDisplay.TabIndex = 1
        Me.spdDisplay.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdDisplay.TextTipAppearance = TipAppearance1
        Me.spdDisplay.ActiveSheetIndex = -1
        '
        'FloatedPickup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(575, 473)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCashierReport)
        Me.Controls.Add(Me.btnCompletePickup)
        Me.Controls.Add(Me.btnReAssignFloat)
        Me.Controls.Add(Me.spdDisplay)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FloatedPickup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FloatedPickup"
        CType(Me.spdDisplay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnCashierReport As System.Windows.Forms.Button
    Friend WithEvents btnCompletePickup As System.Windows.Forms.Button
    Friend WithEvents btnReAssignFloat As System.Windows.Forms.Button
    Private WithEvents spdDisplay As FarPoint.Win.Spread.FpSpread
End Class
