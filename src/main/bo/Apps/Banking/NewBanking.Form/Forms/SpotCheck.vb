﻿Public Class SpotCheck

    Private mcolList As FloatedPickupListCollection
    Private mSelected As FloatedPickupList

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String

    Private WithEvents frmPickupEntry As PickupEntry

    Friend _FloatedPickupVM As IFloatedPickupVM

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        mintFirstUserID = userId
    End Sub

    Private Sub SpotCheck_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor

            mstrAccountingModel = NewBanking.Core.Library.AccountabilityModel
            mstrCurrencyID = NewBanking.Core.Library.ActiveCurrencyID
            mstrCurrencySymbol = NewBanking.Core.Library.CurrencySymbol

            Dim mcolPeriods As PeriodCollection = New PeriodCollection
            mcolPeriods.LoadDates(Today)

            mintTodayPeriodID = mcolPeriods.TodaysPeriod.PeriodID
            mintBankingPeriodID = mintTodayPeriodID

            _FloatedPickupVM = (New FloatedPickupVMFactory).GetImplementation
            With _FloatedPickupVM
                .SetFloatedPickupGridControl(spdDisplay)
                .FloatedPickupGridFormat()
                .FloatedPickupFormFormat()
                mcolList = New SpotCheckListCollection()
                mcolList.LoadData(mintBankingPeriodID)
                .FloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub FloatedPickup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F8 : btnReport.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdDisplay_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles spdDisplay.SelectionChanged
        Try
            Dim FP As FarPoint.Win.Spread.FpSpread
            Dim intCashierID As Integer

            FP = CType(sender, FarPoint.Win.Spread.FpSpread)

            If FP.ActiveSheet.ActiveRow Is Nothing Then
                btnReport.Enabled = False
                Exit Sub 'grid is empty
            End If

            intCashierID = CType(FP.ActiveSheet.ActiveRow.Tag, Integer)

            mSelected = New FloatedPickupList
            mSelected = mcolList.SelectedCashier(intCashierID)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            With mSelected
                frmPickupEntry = New PickupEntry(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mintBankingPeriodID, mstrCurrencyID, mstrCurrencySymbol, _
                                                 PickupEntry.PickupModel.SpotCheck, .CashierID, .CashierUserName, .CashierEmployeeCode, .StartFloatValue, .StartFloatSealNumber)
            End With

            frmPickupEntry.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridSetActiveRow(ByVal intRowID As Integer)
        With spdDisplay
            .Focus()
            .ActiveSheet.SetActiveCell(intRowID, 0)
            .ActiveSheet.AddSelection(intRowID, 0, 1, 1)
        End With

        spdDisplay_SelectionChanged(spdDisplay, Nothing)
    End Sub
#End Region
End Class