﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Float
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.spdDisplay = New FarPoint.Win.Spread.FpSpread
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnUnAssignFloat = New System.Windows.Forms.Button
        Me.btnAssignFloat = New System.Windows.Forms.Button
        Me.btnReAssignFloat = New System.Windows.Forms.Button
        Me.btnFloatCheck = New System.Windows.Forms.Button
        CType(Me.spdDisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'spdDisplay
        '
        Me.spdDisplay.About = "3.0.2004.2005"
        Me.spdDisplay.AccessibleDescription = ""
        Me.spdDisplay.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdDisplay.EditModeReplace = True
        Me.spdDisplay.Location = New System.Drawing.Point(39, 14)
        Me.spdDisplay.Name = "spdDisplay"
        Me.spdDisplay.Size = New System.Drawing.Size(440, 400)
        Me.spdDisplay.TabIndex = 1
        Me.spdDisplay.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdDisplay.TextTipAppearance = TipAppearance1
        Me.spdDisplay.ActiveSheetIndex = -1
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(453, 423)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnUnAssignFloat
        '
        Me.btnUnAssignFloat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnAssignFloat.Location = New System.Drawing.Point(223, 423)
        Me.btnUnAssignFloat.Name = "btnUnAssignFloat"
        Me.btnUnAssignFloat.Size = New System.Drawing.Size(112, 44)
        Me.btnUnAssignFloat.TabIndex = 4
        Me.btnUnAssignFloat.Text = "F3 Un-Assign Float"
        Me.btnUnAssignFloat.UseVisualStyleBackColor = True
        '
        'btnAssignFloat
        '
        Me.btnAssignFloat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssignFloat.Location = New System.Drawing.Point(11, 423)
        Me.btnAssignFloat.Name = "btnAssignFloat"
        Me.btnAssignFloat.Size = New System.Drawing.Size(91, 44)
        Me.btnAssignFloat.TabIndex = 2
        Me.btnAssignFloat.Text = "F1 Assign Float"
        Me.btnAssignFloat.UseVisualStyleBackColor = True
        '
        'btnReAssignFloat
        '
        Me.btnReAssignFloat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReAssignFloat.Location = New System.Drawing.Point(110, 423)
        Me.btnReAssignFloat.Name = "btnReAssignFloat"
        Me.btnReAssignFloat.Size = New System.Drawing.Size(105, 44)
        Me.btnReAssignFloat.TabIndex = 3
        Me.btnReAssignFloat.Text = "F2 Re-Assign Float"
        Me.btnReAssignFloat.UseVisualStyleBackColor = True
        '
        'btnFloatCheck
        '
        Me.btnFloatCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFloatCheck.Location = New System.Drawing.Point(343, 423)
        Me.btnFloatCheck.Name = "btnFloatCheck"
        Me.btnFloatCheck.Size = New System.Drawing.Size(102, 44)
        Me.btnFloatCheck.TabIndex = 5
        Me.btnFloatCheck.Text = "F4 Float Check"
        Me.btnFloatCheck.UseVisualStyleBackColor = True
        '
        'Float
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 473)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnFloatCheck)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnUnAssignFloat)
        Me.Controls.Add(Me.btnAssignFloat)
        Me.Controls.Add(Me.btnReAssignFloat)
        Me.Controls.Add(Me.spdDisplay)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "Float"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Float"
        CType(Me.spdDisplay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnUnAssignFloat As System.Windows.Forms.Button
    Friend WithEvents btnAssignFloat As System.Windows.Forms.Button
    Friend WithEvents btnReAssignFloat As System.Windows.Forms.Button
    Friend WithEvents btnFloatCheck As System.Windows.Forms.Button
    Private WithEvents spdDisplay As FarPoint.Win.Spread.FpSpread
End Class
