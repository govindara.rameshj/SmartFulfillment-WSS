﻿Public Class SafeFloatCreate

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String

    Public Event DataChange()

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
    End Sub

    Private Sub SafeFloatCreate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleSafeCreateFloat
            DisplayGridFormat()
            'DisplayGridPopulate(mintBankingPeriodID)
            DisplayGridPopulate(mintTodayPeriodID)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SafeFloatCreate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the total amount before the "zero amount" validation occurrs
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow                                          'active row
            ActiveCell = ActiveSheet.ActiveCell                                        'active cell

            'exit if denomination column
            If ActiveSheet.ActiveColumnIndex = 0 Then Exit Sub 'Screen Column - Denomination
            If ActiveSheet.ActiveColumnIndex = 1 Then Exit Sub 'Screen Column - System
            If ActiveSheet.ActiveColumnIndex = 3 Then Exit Sub 'Screen Column - Safe Balance

            'money cannot be negative
            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'money needs to be a multiple of the denomination
            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, SafeDenomination).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If

            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            Dim decTotalFloat As Decimal
            Dim intSecondUserID As Integer
            Dim strFloatSealNo As String

            Dim NewFloat As Banking.Core.FloatBanking
            Dim decDenomValue As Decimal

            'able to leave grid without the last input being validated
            ActiveSheet = spdInput.ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow       'active row
            ActiveCell = ActiveSheet.ActiveCell     'active cell
            'money cannot be negative
            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                Exit Sub
            End If
            'money needs to be a multiple of the denomination
            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, SafeDenomination).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                Exit Sub
            End If

            decTotalFloat = CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 2).Value)
            'float cannot be zero
            If decTotalFloat = 0 Then
                InformationMessageBox("Float cannot be zero")
                Exit Sub
            End If

            'display cash drop total
            If QuestionMessageBox("Float total is " & mstrCurrencySymbol & decTotalFloat.ToString("C", SetCurrencyFormat)) = Windows.Forms.DialogResult.No Then Exit Sub

            'second verification
            If AuthoriseCodeSecurity(False, mintFirstUserID, intSecondUserID) = False Then Exit Sub

            'float seal
            Using frmFloatSeal As New Seal(Seal.SealMode.Float, mstrCurrencySymbol & decTotalFloat.ToString("C", SetCurrencyFormat))
                If frmFloatSeal.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
                strFloatSealNo = frmFloatSeal.txtSealInput.Text
            End Using

            'create float
            NewFloat = New Banking.Core.FloatBanking(mintBankingPeriodID, mintTodayPeriodID, mintFirstUserID, intSecondUserID, Banking.Core.FloatBanking.Types.Float, mstrAccountingModel)
            With NewFloat
                .SetSealNumber(strFloatSealNo)
                .Comments = ""
                For intRowIndex As Integer = 0 To ActiveSheet.RowCount - 2 Step 1   'last row is the "total" column
                    decDenomValue = CDec(ActiveSheet.Cells(intRowIndex, 2).Value)
                    If decDenomValue > 0 Then
                        .SetDenomination(mstrCurrencyID, CType(ActiveSheet.Rows(intRowIndex).Tag, SafeDenomination).DenominationID, _
                                                         CType(ActiveSheet.Rows(intRowIndex).Tag, SafeDenomination).TenderID, _
                                                         decDenomValue, String.Empty)
                    End If
                Next
                .Save()
            End With

            'indicate that a cash drop has occurred
            RaiseEvent DataChange()

            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.SafeFloatCreateInputGridColumn1, gcintColumnWidthCashTender, False)     'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.SafeFloatCreateInputGridColumn2, gcintColumnWidthCashTender, False)     'Screen Column - System
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.SafeFloatCreateInputGridColumn3, gcintColumnWidthMoney, False)          'Screen Column - Float
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.SafeFloatCreateInputGridColumn4, gcintColumnWidthMoney, False)          'Screen Column - Safe Balance

        SpreadColumnAlignLeft(NewSheet, 0)

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 2, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 3, True, gstrFarPointMoneyNullDisplay)

        SpreadColumnEditable(NewSheet, 2, True)

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(spdInput)
    End Sub

    Private Sub DisplayGridPopulate(ByVal intPeriodID As Integer)
        Dim colSafeDenom As SafeDenominationCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        colSafeDenom = New SafeDenominationCollection
        colSafeDenom.LoadCashTender(intPeriodID)

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As SafeDenomination In colSafeDenom
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)                                                            'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)                                                'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, If(obj.SystemSafe.HasValue = True, obj.SystemSafe.Value, 0))   'Screen Column - System
            SpreadCellValue(CurrentSheet, intRowIndex, 2, 0)                                                             'Screen Column - Float
            SpreadCellFormula(CurrentSheet, intRowIndex, 3, "RC[-2] - RC[-1]")                                           'Screen Column - Safe Balance
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)

        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total")           'Screen Column - Denomination
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)") 'Screen Column - System
        SpreadCellFormula(CurrentSheet, intRowIndex, 2, "SUM(R1C:R13C)") 'Screen Column - Float
        SpreadCellFormula(CurrentSheet, intRowIndex, 3, "SUM(R1C:R13C)") 'Screen Column - Safe Balance
    End Sub

#End Region

End Class