﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainCutDown
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBagCollection = New System.Windows.Forms.Button
        Me.btnFloatedPickup = New System.Windows.Forms.Button
        Me.btnAssignFloat = New System.Windows.Forms.Button
        Me.btnUnFloatedPickup = New System.Windows.Forms.Button
        Me.btnCashDrop = New System.Windows.Forms.Button
        Me.lblBankingDate = New System.Windows.Forms.Label
        Me.cmbPeriod = New System.Windows.Forms.ComboBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnBagCollection
        '
        Me.btnBagCollection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnBagCollection.Location = New System.Drawing.Point(443, 48)
        Me.btnBagCollection.Name = "btnBagCollection"
        Me.btnBagCollection.Size = New System.Drawing.Size(92, 44)
        Me.btnBagCollection.TabIndex = 6
        Me.btnBagCollection.Text = "F6 Banking Bag Collection"
        Me.btnBagCollection.UseVisualStyleBackColor = True
        '
        'btnFloatedPickup
        '
        Me.btnFloatedPickup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnFloatedPickup.Location = New System.Drawing.Point(205, 48)
        Me.btnFloatedPickup.Name = "btnFloatedPickup"
        Me.btnFloatedPickup.Size = New System.Drawing.Size(104, 44)
        Me.btnFloatedPickup.TabIndex = 4
        Me.btnFloatedPickup.Text = "F3 Floated Pickup"
        Me.btnFloatedPickup.UseVisualStyleBackColor = True
        '
        'btnAssignFloat
        '
        Me.btnAssignFloat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAssignFloat.Location = New System.Drawing.Point(12, 48)
        Me.btnAssignFloat.Name = "btnAssignFloat"
        Me.btnAssignFloat.Size = New System.Drawing.Size(94, 44)
        Me.btnAssignFloat.TabIndex = 2
        Me.btnAssignFloat.Text = "F1 Assign Float"
        Me.btnAssignFloat.UseVisualStyleBackColor = True
        '
        'btnUnFloatedPickup
        '
        Me.btnUnFloatedPickup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnUnFloatedPickup.Location = New System.Drawing.Point(317, 48)
        Me.btnUnFloatedPickup.Name = "btnUnFloatedPickup"
        Me.btnUnFloatedPickup.Size = New System.Drawing.Size(118, 44)
        Me.btnUnFloatedPickup.TabIndex = 5
        Me.btnUnFloatedPickup.Text = "F4 UnFloated Pickup"
        Me.btnUnFloatedPickup.UseVisualStyleBackColor = True
        '
        'btnCashDrop
        '
        Me.btnCashDrop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCashDrop.Location = New System.Drawing.Point(114, 48)
        Me.btnCashDrop.Name = "btnCashDrop"
        Me.btnCashDrop.Size = New System.Drawing.Size(83, 44)
        Me.btnCashDrop.TabIndex = 3
        Me.btnCashDrop.Text = "F2 Cash Drop"
        Me.btnCashDrop.UseVisualStyleBackColor = True
        '
        'lblBankingDate
        '
        Me.lblBankingDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankingDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblBankingDate.Location = New System.Drawing.Point(115, 9)
        Me.lblBankingDate.Name = "lblBankingDate"
        Me.lblBankingDate.Size = New System.Drawing.Size(124, 22)
        Me.lblBankingDate.TabIndex = 42
        Me.lblBankingDate.Text = "Select Banking Day"
        Me.lblBankingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbPeriod
        '
        Me.cmbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPeriod.FormattingEnabled = True
        Me.cmbPeriod.Location = New System.Drawing.Point(245, 9)
        Me.cmbPeriod.Name = "cmbPeriod"
        Me.cmbPeriod.Size = New System.Drawing.Size(248, 21)
        Me.cmbPeriod.TabIndex = 1
        '
        'btnExit
        '
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.Location = New System.Drawing.Point(543, 48)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'MainCutDown
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 112)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblBankingDate)
        Me.Controls.Add(Me.cmbPeriod)
        Me.Controls.Add(Me.btnBagCollection)
        Me.Controls.Add(Me.btnFloatedPickup)
        Me.Controls.Add(Me.btnAssignFloat)
        Me.Controls.Add(Me.btnUnFloatedPickup)
        Me.Controls.Add(Me.btnCashDrop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "MainCutDown"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MainCutDown"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnBagCollection As System.Windows.Forms.Button
    Friend WithEvents btnFloatedPickup As System.Windows.Forms.Button
    Friend WithEvents btnAssignFloat As System.Windows.Forms.Button
    Friend WithEvents btnUnFloatedPickup As System.Windows.Forms.Button
    Friend WithEvents btnCashDrop As System.Windows.Forms.Button
    Friend WithEvents lblBankingDate As System.Windows.Forms.Label
    Friend WithEvents cmbPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
