﻿Public Class SafeMaintenanceWithBankingBagsAndComments
    Implements ISafeMaintenance

    Private _FirstUserID As Integer
    Private _SelectedPeriod As Period
    Private _CurrentPeriod As Period
    Private _StoreID As String
    Private _StoreName As String

    Private _ViewModel As ISafeMaintenanceVM
    Private _SealNumberReturned As String
    Private _CommentReturned As String

    Private WithEvents _BankingSeal As EndOfDayCheckBankingSeal
    Private WithEvents _Comment As EndOfDayCheckComment

    Private Event DataChange() Implements ISafeMaintenance.DataChange

    Public Sub New(ByVal FirstUserID As Integer, ByVal CurrentPeriod As Period, _
                   ByVal SelectedPeriod As Period, ByVal StoreID As String, ByVal StoreName As String)

        InitializeComponent()

        _ViewModel = (New SafeMaintenanceVMFactory).GetImplementation
        _ViewModel.CreateCurrentSafeMaintenance(CurrentPeriod.PeriodID)
        _ViewModel.CreateSelectedSafeMaintenance(SelectedPeriod.PeriodID)
        _ViewModel.InitialiseBSE(CurrentPeriod.PeriodID)

        _FirstUserID = FirstUserID
        _SelectedPeriod = SelectedPeriod
        _CurrentPeriod = CurrentPeriod
        _StoreID = StoreID
        _StoreName = StoreName

    End Sub

    Public Overloads Sub ShowDialog(ByRef SafeMaintenence As Main) Implements ISafeMaintenance.ShowDialog

        MyBase.ShowDialog(SafeMaintenence)

    End Sub

    Private Sub SafeMaintenance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Cursor = Cursors.WaitCursor

            Me.Text = My.Resources.Screen.ScreenTitleSafeMaintenance
            DisplayGridFormat()
            DisplayGridPopulate(_CurrentPeriod.PeriodID)

            With _ViewModel

                .SetCommentsControl(txtSafeComments)
                .SetGiftTokenSealControl(txtGiftTokenSealNumber)
                .SetMiscellaneousPropertyControl(txtMiscellaneousProperty)

                .SetBankingBagListControl(spdBankingBagList)
                .FormatBankingBagListControl()

                If .LoadCurrentSafe(_CurrentPeriod.PeriodID) Then
                    .PopulateCommentsControl()
                    .PopulateGiftTokenSealControl()
                    .PopulateMiscellaneousPropertyControl()
                End If

                .LoadSelectedSafe(_SelectedPeriod.PeriodID)
                If .SafeMaintenanceAlreadyPerformed = True Then

                    If MessageBox.Show("Safe maintenance already performed." & vbCrLf & _
                                       "Completion of this maintenance operation will result in existing being overwritten." & vbCrLf & _
                                       "Do you wish to proceed?", _
                                       String.Empty, MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then

                        FindForm.Close()
                        UserControlScan1.CloseScanner()

                    End If

                End If

            End With

        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SafeMaintenance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData

                Case Keys.F1 : btnAddBankingBag.PerformClick()

                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the calculated totals
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()

                Case Keys.F7
                    'same reason as above
                    spdInput.Focus()
                    btnSafeCheckReport.PerformClick()

                Case Keys.F12 : btnExit.PerformClick()

                Case Else : e.Handled = False

            End Select

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow                                          'active row
            ActiveCell = ActiveSheet.ActiveCell                                        'active cell

            'exit if denomination or system column
            If ActiveSheet.ActiveColumnIndex = 0 Or ActiveSheet.ActiveColumnIndex = 1 Then Exit Sub
            'exit if "total" column
            If ActiveRow.Tag Is Nothing Then Exit Sub

            'money cannot be negative
            If CDec(ActiveCell.Value) < 0 Then
                InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            'money needs to be a multiple of the denomination
            If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, SafeDenomination).DenominationID) Then
                InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                e.Cancel = True
                Exit Sub
            End If
            SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnAddBankingBag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBankingBag.Click

        _BankingSeal = New EndOfDayCheckBankingSeal

        _CommentReturned = Nothing

        If _BankingSeal.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            If ValidBankingSeal() = False Then Exit Sub

            _ViewModel.CreateBankingBag(_SealNumberReturned, _CommentReturned)
            _ViewModel.PopulateBankingBagListControl()

        End If

    End Sub

    Private Sub UserControlScan1_ReceivedScannerInput(ByVal strInput As String) Handles UserControlScan1.ReceivedScannerInput

        _SealNumberReturned = strInput

        txtSeal.Text = strInput
        _SealNumberReturned = txtSeal.Text 'This is to get the number in the correct seal format as the scanner doesn't do '-' character

        If ValidBankingSeal() = False Then Exit Sub

        _ViewModel.CreateBankingBag(_SealNumberReturned, _CommentReturned)
        _ViewModel.PopulateBankingBagListControl()

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveCell As Cell

            Dim SystemTotal As Decimal
            Dim MainPlusChangeTotal As Decimal
            Dim SecondManagerUserID As Integer

            ActiveSheet = spdInput.ActiveSheet      'active sheet
            ActiveRow = ActiveSheet.ActiveRow       'active row
            ActiveCell = ActiveSheet.ActiveCell     'active cell

            'no check required if denomination or system column or "total" column
            If Not (ActiveSheet.ActiveColumnIndex = 0 Or ActiveSheet.ActiveColumnIndex = 1 Or ActiveRow.Tag Is Nothing) Then
                'money cannot be negative
                If CDec(ActiveCell.Value) < 0 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If

                'money needs to be a multiple of the denomination
                If Not IsMultiple(CDec(ActiveCell.Value), CType(ActiveRow.Tag, SafeDenomination).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    Exit Sub
                End If
            End If
            'system total
            SystemTotal = CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 1).Value)
            'main safe + change safe
            MainPlusChangeTotal = CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 2).Value) + CDec(ActiveSheet.Cells(ActiveSheet.RowCount - 1, 3).Value)

            'match system
            If SystemTotal <> MainPlusChangeTotal Then
                InformationMessageBox("Totals do not match")
                Exit Sub
            End If





            If _ViewModel.MatchSystemBankingExpectation = False Then

                If QuestionMessageBox("Have not matched system expectations. Accept anyway") = Windows.Forms.DialogResult.No Then Exit Sub

                If BankingComment("Have not matched system expectations - Please provide explanation") = False Then

                    InformationMessageBox("Sorry no explanations provided; banking process cannot be completed")

                    Exit Sub

                End If

            End If








            'second manager's verification
            If AuthoriseCodeSecurity(True, _FirstUserID, SecondManagerUserID) = False Then Exit Sub

            'persist safe check
            Cursor = Cursors.WaitCursor

            Dim OdbcConnection As clsOasys3DB
            Dim BankingPeriod As Banking.Core.IBankingPeriod
            Dim Factory As New Banking.Core.BankingPeriodFactory

            Dim Denom As SafeDenomination
            Dim CurrencyID As String
            Dim DenomID As Decimal
            Dim TenderID As Integer
            Dim SafeMain As Decimal
            Dim SafeChange As Decimal
            Dim RowCount As Integer

            OdbcConnection = New clsOasys3DB("Default", 0)
            Factory.SetOdbcConnection(OdbcConnection)
            BankingPeriod = Factory.GetImplementation

            BankingPeriod.LoadSafe(_CurrentPeriod.PeriodID)
            BankingPeriod.SetSafeUser1(_FirstUserID)
            BankingPeriod.SetSafeUser2(SecondManagerUserID)

            'loop around cash denominations
            RowCount = ActiveSheet.RowCount - 1
            For RowIndex As Integer = 0 To RowCount
                If ActiveSheet.Rows(RowIndex).Tag Is Nothing Then Continue For

                Denom = CType(ActiveSheet.Rows(RowIndex).Tag, SafeDenomination)
                CurrencyID = Denom.CurrencyID
                DenomID = Denom.DenominationID
                TenderID = Denom.TenderID

                SafeMain = CType(ActiveSheet.Cells(RowIndex, 2).Value, Decimal)
                SafeChange = CType(ActiveSheet.Cells(RowIndex, 3).Value, Decimal)

                BankingPeriod.SafeMain(CurrencyID, DenomID, TenderID) = SafeMain
                BankingPeriod.SafeChange(CurrencyID, DenomID, TenderID) = SafeChange
                BankingPeriod.SafeSystem(CurrencyID, DenomID, TenderID) = SafeMain + SafeChange
            Next






            Dim SafeMaintenanceAuthorisers As ISafeCheckValidation

            SafeMaintenanceAuthorisers = (New SafeCheckValidationFactory).GetImplementation

            Try
                OdbcConnection.BeginTransaction()

                BankingPeriod.NewBankingSafeMaintenanceTransactionSafe(_SelectedPeriod.PeriodID)

                SafeMaintenanceAuthorisers.SaveSafeCheckUserDetails(OdbcConnection, _
                                                                     _SelectedPeriod, _
                                                                     BankingPeriod.Safe.UserID1.Value, _
                                                                     BankingPeriod.Safe.UserID2.Value, _
                                                                     BankingPeriod.Safe.LastAmended.Value)

                _ViewModel.Persist(OdbcConnection)

                _ViewModel.SafeMaintenancePersist(OdbcConnection)

                OdbcConnection.CommitTransaction()

            Catch ex As Exception

                OdbcConnection.RollBackTransaction()

                Throw New OasysDbException("Error occurred performing safe maintenance. Changes abandoned", ex)

            Finally

                OdbcConnection = Nothing

            End Try

            RaiseEvent DataChange()
            FindForm.Close()

        Catch ex As Exception

            ErrorHandler(ex)

        Finally

            Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnSafeCheckReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSafeCheckReport.Click
        Try

            Dim Report As ISafeCheckReportUI

            Report = (New SafeCheckReportUIFactory).GetImplementation

            Report.RunReport(_CurrentPeriod, _StoreID, _StoreName)

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try

            FindForm.Close()
            UserControlScan1.CloseScanner()

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Information Event Handlers"

    Private Sub _BankingSeal_SealEntered(ByVal Value As String) Handles _BankingSeal.SealEntered

        _SealNumberReturned = Value

    End Sub

    Private Sub _Comment_CommentEntered(ByVal Value As String) Handles _Comment.CommentEntered

        _CommentReturned = Value

    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridFormat()
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn1, gcintColumnWidthCashTender, False)     'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - System
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn3, gcintColumnWidthMoney, False)          'Screen Column - Main Safe
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn4, gcintColumnWidthMoney, False)          'Screen Column - Change Safe

        SpreadColumnMoney(NewSheet, 1, True, "0.00")
        SpreadColumnMoney(NewSheet, 2, True, "0.00")
        SpreadColumnMoney(NewSheet, 3, True, "0.00")
        SpreadColumnEditable(NewSheet, 2, True)
        SpreadColumnEditable(NewSheet, 3, True)

        SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
        SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(spdInput)
    End Sub

    Private Sub DisplayGridPopulate(ByVal PeriodID As Integer)
        Dim colSafeMaintenance As SafeDenominationCollection

        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data - cash tender
        colSafeMaintenance = New SafeDenominationCollection
        colSafeMaintenance.LoadCashTender(PeriodID)

        CurrentSheet = spdInput.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As SafeDenomination In colSafeMaintenance
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)              'Primary Key   - Contains all relevent properties required later to write to safe
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)  'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.SystemSafe)  'Screen Column - System
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.MainSafe)    'Screen Column - Main Safe
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.ChangeSafe)  'Screen Column - Change Safe
        Next
        'total cash entered - calculated
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
        SpreadCellLocked(CurrentSheet, intRowIndex, 2, True)
        SpreadCellLocked(CurrentSheet, intRowIndex, 2, True)

        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash")
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)")
        SpreadCellFormula(CurrentSheet, intRowIndex, 2, "SUM(R1C:R13C)")
        SpreadCellFormula(CurrentSheet, intRowIndex, 3, "SUM(R1C:R13C)")
    End Sub

    Private Function ValidBankingSeal() As Boolean

        If _ViewModel.BankingBagAlreadyScanned(_SealNumberReturned) = True Then

            InformationMessageBox("Sorry banking bag already scanned")
            Return False

        End If

        If _ViewModel.BankingBagHasValidSeal(_SealNumberReturned) = False Then

            If QuestionMessageBox("Invalid seal number. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

            If BankingSealComment("Invalid seal number - Please provide explanation") = False Then

                InformationMessageBox("Sorry no explanations provided; banking bag rejected")
                Return False

            End If

        Else

            If _ViewModel.BankingBagExistInSafe(_SealNumberReturned) = False Then

                If QuestionMessageBox("Banking bag does not exists in safe. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

                If BankingSealComment("Banking bag does not exists in safe - Please provide explanation") = False Then

                    InformationMessageBox("Sorry no explanations provided; banking bag rejected")
                    Return False

                End If

            End If

        End If

        Return True

    End Function

    Private Function BankingSealComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            BankingSealComment = True

        Else

            BankingSealComment = False

        End If

    End Function

    Private Function BankingComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            _ViewModel.CreateBankingComment(_CommentReturned)

            BankingComment = True

        Else

            BankingComment = False

        End If

    End Function

#End Region

End Class