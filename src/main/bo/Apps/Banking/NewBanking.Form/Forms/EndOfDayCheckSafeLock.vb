﻿Public Class EndOfDayCheckSafeLock

    Public Event SafeLocked(ByVal LockedFromDate As DateTime, _
                            ByVal LockedFromTime As DateTime, _
                            ByVal LockedToDate As DateTime, _
                            ByVal LockedToTime As DateTime)

    Private Sub EndOfDayCheckSafeLock_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True

        Select Case e.KeyData

            Case Keys.Enter : btnSave.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False

        End Select

    End Sub

    Private Sub EndOfDayCheckSafeLock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DateTimePickerLockedFromDate.Value = Now
        DateTimePickerLockedFromTime.Value = New DateTime(Now.Year, Now.Month, Now.Day, 20, 0, 0)

        DateTimePickerLockedToDate.Value = Now.AddDays(1)
        DateTimePickerLockedToTime.Value = New DateTime(Now.AddDays(1).Year, Now.AddDays(1).Month, Now.AddDays(1).Day, 6, 0, 0)

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        RaiseEvent SafeLocked(DateTimePickerLockedFromDate.Value, _
                              DateTimePickerLockedFromTime.Value, _
                              DateTimePickerLockedToDate.Value, _
                              DateTimePickerLockedToTime.Value)

        Me.DialogResult = Windows.Forms.DialogResult.OK

        FindForm.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

        FindForm.Close()

    End Sub

End Class