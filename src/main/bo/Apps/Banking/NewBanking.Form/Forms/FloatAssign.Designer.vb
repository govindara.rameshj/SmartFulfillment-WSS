﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FloatAssign
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.cmbCashier = New System.Windows.Forms.ComboBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAssignFloat = New System.Windows.Forms.Button
        Me.spdInput = New FarPoint.Win.Spread.FpSpread
        Me.btnCompleteManualCheck = New System.Windows.Forms.Button
        Me.btnCancelManualCheck = New System.Windows.Forms.Button
        Me.chkManualCheck = New System.Windows.Forms.CheckBox
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbCashier
        '
        Me.cmbCashier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbCashier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCashier.FormattingEnabled = True
        Me.cmbCashier.Location = New System.Drawing.Point(66, 12)
        Me.cmbCashier.Name = "cmbCashier"
        Me.cmbCashier.Size = New System.Drawing.Size(188, 21)
        Me.cmbCashier.TabIndex = 1
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(405, 390)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(54, 44)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAssignFloat
        '
        Me.btnAssignFloat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssignFloat.Location = New System.Drawing.Point(9, 390)
        Me.btnAssignFloat.Name = "btnAssignFloat"
        Me.btnAssignFloat.Size = New System.Drawing.Size(91, 44)
        Me.btnAssignFloat.TabIndex = 4
        Me.btnAssignFloat.Text = "F1 Assign Float"
        Me.btnAssignFloat.UseVisualStyleBackColor = True
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(66, 62)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(337, 322)
        Me.spdInput.TabIndex = 3
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance1
        Me.spdInput.ActiveSheetIndex = -1
        '
        'btnCompleteManualCheck
        '
        Me.btnCompleteManualCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCompleteManualCheck.Location = New System.Drawing.Point(251, 390)
        Me.btnCompleteManualCheck.Name = "btnCompleteManualCheck"
        Me.btnCompleteManualCheck.Size = New System.Drawing.Size(146, 44)
        Me.btnCompleteManualCheck.TabIndex = 6
        Me.btnCompleteManualCheck.Text = "F3 Complete Manual Check"
        Me.btnCompleteManualCheck.UseVisualStyleBackColor = True
        '
        'btnCancelManualCheck
        '
        Me.btnCancelManualCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelManualCheck.Location = New System.Drawing.Point(108, 390)
        Me.btnCancelManualCheck.Name = "btnCancelManualCheck"
        Me.btnCancelManualCheck.Size = New System.Drawing.Size(135, 44)
        Me.btnCancelManualCheck.TabIndex = 5
        Me.btnCancelManualCheck.Text = "F2 Cancel Manual Check"
        Me.btnCancelManualCheck.UseVisualStyleBackColor = True
        '
        'chkManualCheck
        '
        Me.chkManualCheck.AutoSize = True
        Me.chkManualCheck.Location = New System.Drawing.Point(66, 39)
        Me.chkManualCheck.Name = "chkManualCheck"
        Me.chkManualCheck.Size = New System.Drawing.Size(95, 17)
        Me.chkManualCheck.TabIndex = 2
        Me.chkManualCheck.Text = "&Manual Check"
        Me.chkManualCheck.UseVisualStyleBackColor = True
        '
        'FloatAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(469, 443)
        Me.ControlBox = False
        Me.Controls.Add(Me.chkManualCheck)
        Me.Controls.Add(Me.btnCancelManualCheck)
        Me.Controls.Add(Me.btnCompleteManualCheck)
        Me.Controls.Add(Me.spdInput)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAssignFloat)
        Me.Controls.Add(Me.cmbCashier)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FloatAssign"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FloatAssign"
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbCashier As System.Windows.Forms.ComboBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnAssignFloat As System.Windows.Forms.Button
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnCompleteManualCheck As System.Windows.Forms.Button
    Friend WithEvents btnCancelManualCheck As System.Windows.Forms.Button
    Friend WithEvents chkManualCheck As System.Windows.Forms.CheckBox
End Class
