﻿Public Class BankingBagCollection

    Private mintBankingPeriodID As Integer
    Private mstrAccountingModel As String
    Private mintFirstUserID As Integer
    Private mData As New NewBanking.Core.BankingBagCollection
    Private mstrStoreID As String
    Private mstrStoreName As String

    Public Event DataChange()

    Public Sub New(ByVal intBankingPeriodID As Integer, ByVal strAccountingModel As String, _
                   ByVal intFirstUserID As Integer, ByVal strStoreID As String, ByVal strStoreName As String)
        Try
            mintBankingPeriodID = intBankingPeriodID
            mstrAccountingModel = strAccountingModel
            mintFirstUserID = intFirstUserID
            mstrStoreID = strStoreID
            mstrStoreName = strStoreName

            InitializeComponent()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub BankingBagCollection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleBankingBagCollection

            Cursor = Cursors.WaitCursor

            mData.LoadData()

            Dim sheetView = CreateSheetView(mData, Function(rowN) False)

            SpreadGridSheetAdd(spdInput, sheetView, True, String.Empty)
            SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub BankingBagCollection_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F5 : btnSave.PerformClick()
                Case Keys.F9 : btnPrint.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSlipNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSlipNo.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtSlipNo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSlipNo.Leave
        Try
            If Me.ActiveControl Is btnExit Then Exit Sub

            If IsNumeric(txtSlipNo.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SlipNoNumeric)
                txtSlipNo.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSlipNo.Text = txtSlipNo.Text.Trim.PadLeft(13, CType("0", Char))
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub txtComment_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComment.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim BankingBag As Banking.Core.GiftTokenProperty
            Dim ActiveSheet As SheetView
            Dim intSecondUserID As Integer

            ActiveSheet = spdInput.ActiveSheet
            'bag selected
            If BagSelected() = False Then
                InformationMessageBox("Select at least one bag to be collected")
                Exit Sub
            End If

            'validate slip no
            If IsNumeric(txtSlipNo.Text.Trim) = False Then
                InformationMessageBox(My.Resources.InformationMessages.SlipNoNumeric)
                txtSlipNo.Focus()
                Exit Sub
            End If
            'pad with leading zeros
            txtSlipNo.Text = txtSlipNo.Text.Trim.PadLeft(13, CType("0", Char))

            'second manager's verification
            If AuthoriseCodeSecurity(True, mintFirstUserID, intSecondUserID) = False Then Exit Sub
            'update bags
            For intRowIndex As Integer = 0 To ActiveSheet.RowCount - 1
                If CType(ActiveSheet.Cells(intRowIndex, 0).Value, Boolean) = False Then Continue For 'bag not selected

                BankingBag = New Banking.Core.GiftTokenProperty(mintBankingPeriodID, mintFirstUserID, intSecondUserID, Banking.Core.BagTypes.Banking, mstrAccountingModel)

                With BankingBag
                    .LoadOriginalBag(CType(ActiveSheet.Rows(intRowIndex).Tag, Integer))
                    .SetComments(CType(ActiveSheet.Cells(intRowIndex, 4).Value, String))
                    .SetSealNumber(CType(ActiveSheet.Cells(intRowIndex, 2).Value, String))
                    .SetValue(CType(ActiveSheet.Cells(intRowIndex, 3).Value, Decimal))
                    .NewBankingSave(CType(ActiveSheet.Rows(intRowIndex).Tag, Integer), txtSlipNo.Text.Trim, txtComment.Text.Trim)
                End With
            Next

            FindForm.Close()
            'indicate that a banking bag collection has occurred
            RaiseEvent DataChange()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim spread = CreateReportForPrint(mData, _
                Function(rowN) CType(spdInput.ActiveSheet.Cells(rowN, 0).Value, Boolean), _
                txtSlipNo.Text, txtComment.Text, mstrStoreID, mstrStoreName)

            spread.PrintSheet(0)

        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function BagSelected() As Boolean
        Dim ActiveSheet As SheetView
        Dim ActiveRow As Row
        Dim ActiveColumn As Column
        Dim ActiveCell As Cell

        ActiveSheet = spdInput.ActiveSheet
        ActiveRow = ActiveSheet.ActiveRow
        ActiveColumn = ActiveSheet.ActiveColumn
        ActiveCell = ActiveSheet.ActiveCell

        BagSelected = False
        For intRowIndex As Integer = 0 To ActiveSheet.RowCount - 1 Step 1
            If CType(ActiveSheet.Cells(intRowIndex, 0).Value, Boolean) = True Then BagSelected = True
        Next
    End Function

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Shared Function CreateSheetView(ByRef data As NewBanking.Core.BankingBagCollection, ByRef getCheckboxValue As Func(Of Integer, Boolean)) As SheetView
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 5, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.BankingCollectionDisplayGridColumn1, gcintColumnWidthPickup, False)          'Screen Column - Checkbox
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.BankingCollectionDisplayGridColumn2, gcintColumnWidthDefault, False)         'Screen Column - Period ID & Date
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.BankingCollectionDisplayGridColumn3, gcintColumnWidthSeal, False)            'Screen Column - Seal Number
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.BankingCollectionDisplayGridColumn4, gcintColumnWidthMoney, False)           'Screen Column - Value
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.BankingCollectionDisplayGridColumn5, gcintColumnWidthCommentsWide, False)    'Screen Column - Comment

        SpreadColumnEditable(NewSheet, 0, True)

        SpreadColumnMoney(NewSheet, 3, True, gstrFarPointMoneyNullDisplay)

        Dim intRowIndex As Integer = 0

        SpreadSheetClearDown(NewSheet)

        For Each obj As BankingBag In data
            SpreadRowAdd(NewSheet, intRowIndex)

            SpreadRowTagValue(NewSheet, intRowIndex, obj.BagID)
            'add checkbox
            SpreadCellButton(NewSheet, intRowIndex, 0, getCheckboxValue(intRowIndex))                                                                 'Screen Column - Checkbox
            SpreadCellValue(NewSheet, intRowIndex, 1, obj.BagPeriodID.ToString & " : " & obj.BagPeriodDate.ToShortDateString) 'Screen Column - Period ID & Date
            SpreadCellValue(NewSheet, intRowIndex, 2, obj.BagSealNumber)                                                      'Screen Column - Seal Number
            SpreadCellValue(NewSheet, intRowIndex, 3, obj.BagValue)                                                           'Screen Column - Value
            SpreadCellValue(NewSheet, intRowIndex, 4, obj.BagComment)                                                         'Screen Column - Comment
        Next

        Return NewSheet
    End Function

    Public Shared Function CreateReportForPrint(ByVal data As NewBanking.Core.BankingBagCollection, _
        ByVal getCheckboxValue As Func(Of Integer, Boolean), _
        ByVal collectionSlip As String, ByVal comment As String, _
        ByVal strStoreID As String, ByVal strStoreName As String) As FpSpread

        Dim reportGridView = CreateSheetView(data, getCheckboxValue)

        Dim reportGrid As New FpSpread

        SpreadGridSheetAdd(reportGrid, reportGridView, True, String.Empty)
        SpreadGridScrollBar(reportGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        Dim RowIndex = 0

        SpreadRowAdd(reportGridView, RowIndex)

        SpreadRowAdd(reportGridView, RowIndex)
        SpreadRowBold(reportGrid, reportGridView, RowIndex)
        SpreadCellValue(reportGridView, RowIndex, 0, "Collection Slip")
        SpreadCellValue(reportGridView, RowIndex, 1, collectionSlip)

        SpreadRowAdd(reportGridView, RowIndex)
        SpreadRowBold(reportGrid, reportGridView, RowIndex)
        SpreadCellValue(reportGridView, RowIndex, 0, "Comment")
        reportGridView.Cells(RowIndex, 0).RowSpan = 3
        reportGridView.Cells(RowIndex, 0).VerticalAlignment = CellVerticalAlignment.Center

        SpreadCellValue(reportGridView, RowIndex, 1, comment)

        reportGridView.Cells(RowIndex, 1).ColumnSpan = 4
        reportGridView.Cells(RowIndex, 1).RowSpan = 3
        reportGridView.Cells(RowIndex, 1).VerticalAlignment = CellVerticalAlignment.Center

        Dim wrapCellType As New FarPoint.Win.Spread.CellType.EditBaseCellType
        wrapCellType.WordWrap = True
        reportGridView.Cells(RowIndex, 1).CellType = wrapCellType

        SpreadRowAdd(reportGridView, RowIndex)
        SpreadRowAdd(reportGridView, RowIndex)

        CustomiseSheetForPrint(reportGridView, strStoreID, strStoreName, _
                               "Banking Collection Confirmation", "", _
                               PrintOrientation.Portrait)

        reportGridView.PrintInfo.ShowGrid = False

        Return reportGrid
    End Function

#End Region

End Class