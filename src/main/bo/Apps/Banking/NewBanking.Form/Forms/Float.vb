﻿Public Class Float

    Private _FloatList As FloatListCollection
    Private _SelectedFloat As FloatList

    Private _AccountingModel As String
    Private _TodayPeriodID As Integer
    Private _BankingPeriodID As Integer
    Private _LoggedOnUserID As Integer
    Private _CurrencyID As String
    Private _CurrencySymbol As String
    Private _DataHasBeenModified As Boolean = False

    Friend _FloatViewModel As IFloatVM
    Friend _UnassignFloatViewModel As IFloatVM

    Public Event DataChange()

    Private WithEvents frmFloatAssign As FloatAssign
    Private WithEvents frmFloatReAssign As FloatReAssign

    Public Sub New(ByVal AccountingModel As String, _
                   ByVal FirstUserID As Integer, _
                   ByVal TodayPeriodID As Integer, _
                   ByVal BankingPeriodID As Integer, _
                   ByVal CurrencyID As String, _
                   ByVal CurrencySymbol As String)

        InitializeComponent()

        _AccountingModel = AccountingModel
        _LoggedOnUserID = FirstUserID
        _TodayPeriodID = TodayPeriodID
        _BankingPeriodID = BankingPeriodID
        _CurrencyID = CurrencyID
        _CurrencySymbol = CurrencySymbol

        _UnassignFloatViewModel = (New UnAssignFloatVMFactory).GetImplementation

    End Sub

    Private Sub Float_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleFloat
            _FloatViewModel = (New FloatVMFactory).GetImplementation
            With _FloatViewModel
                .SetFloatGridControl(spdDisplay)
                .SetFloatForm(Me)
                .FloatGridFormat()
                .FloatFormFormat()
                _FloatList = .GetFloatList(_BankingPeriodID)
                .FloatGridPopulate(_FloatList)
            End With
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub Float_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnAssignFloat.PerformClick()
                Case Keys.F2 : btnReAssignFloat.PerformClick()
                Case Keys.F3 : btnUnAssignFloat.PerformClick()
                Case Keys.F4 : btnFloatCheck.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub spdDisplay_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles spdDisplay.SelectionChanged

        Try
            Dim FP As FarPoint.Win.Spread.FpSpread
            Dim FloatID As Integer

            FP = CType(sender, FarPoint.Win.Spread.FpSpread)

            If FP.ActiveSheet.ActiveRow Is Nothing Then Exit Sub 'grid is empty

            FloatID = CType(FP.ActiveSheet.ActiveRow.Tag, Integer)

            _SelectedFloat = New FloatList
            _SelectedFloat = _FloatList.SelectedFloat(FloatID)

            ButtonAvailability(_SelectedFloat)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

#Region "Button Event Handlers"

    Private Sub btnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignFloat.Click

        Try
            frmFloatAssign = New FloatAssign(_AccountingModel, _LoggedOnUserID, _TodayPeriodID, _BankingPeriodID, _CurrencyID, _CurrencySymbol, _
                                             _SelectedFloat.FloatID, _SelectedFloat.FloatSealNumber, _SelectedFloat.FloatChecked, False)

            frmFloatAssign.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub btnReAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReAssignFloat.Click

        Try

            frmFloatReAssign = New FloatReAssign(_AccountingModel, _BankingPeriodID, _LoggedOnUserID, _SelectedFloat.AssignedToUserID.Value, _SelectedFloat.FloatID, _SelectedFloat.FloatValue)
            frmFloatReAssign.ShowDialog(Me)

        Catch ex As Exception

            ErrorHandler(ex)

        End Try

    End Sub

    Private Sub btnUnAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnAssignFloat.Click

        Try
            Dim PickupBagID As Integer
            Dim SecondCheckUserID As Integer
            Dim SealNumber As String = String.Empty
            Dim SealEntryNotCancelled As Boolean
            Dim ManualCheckDone As Boolean

            If QuestionMessageBox("Are you sure you want to un-assign this float?") <> Windows.Forms.DialogResult.No Then

                If _UnassignFloatViewModel.IsManualCheckRequired() Then
                    ManualCheckDone = _UnassignFloatViewModel.PerformManualCheck(_SelectedFloat.FloatID)
                End If

                If AuthoriseCodeSecurity(False, _LoggedOnUserID, SecondCheckUserID) Then
                    Dim FloatValue As Decimal = _SelectedFloat.FloatValue

                    If ManualCheckDone Then
                        FloatValue = _UnassignFloatViewModel.NewFloatValue
                    End If
                    Using frmSeal As New Seal(Seal.SealMode.Float, _CurrencySymbol & FloatValue.ToString("C", SetCurrencyFormat))
                        If frmSeal.ShowDialog(Me) <> Windows.Forms.DialogResult.Cancel Then
                            SealNumber = frmSeal.txtSealInput.Text
                            SealEntryNotCancelled = True
                        End If
                    End Using
                    If SealEntryNotCancelled Then
                        Cursor = Cursors.WaitCursor
                        'FloatID = _SelectedFloat.FloatID
                        PickupBagID = CType(IIf(_SelectedFloat.FloatCreatedFromPickupBagID.HasValue, _SelectedFloat.FloatCreatedFromPickupBagID, 0), Integer)
                        If ManualCheckDone Then
                            _UnassignFloatViewModel.CompleteUnAssigningTheFloatWithManualCheck(_SelectedFloat.FloatID, PickupBagID, _LoggedOnUserID, SecondCheckUserID, _BankingPeriodID, _TodayPeriodID, _AccountingModel, SealNumber, _CurrencyID)
                        Else
                            _UnassignFloatViewModel.CompleteUnAssigningTheFloat(_SelectedFloat.FloatID, PickupBagID, _LoggedOnUserID, SecondCheckUserID, _BankingPeriodID, _TodayPeriodID, _AccountingModel, SealNumber, _CurrencyID)
                        End If
                        With _FloatViewModel
                            _FloatList = .GetFloatList(_BankingPeriodID)
                            .FloatGridPopulate(_FloatList)
                        End With
                        DisplayGridSetActiveRow(0)
                        _DataHasBeenModified = True
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnFloatCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFloatCheck.Click

        Try
            frmFloatAssign = New FloatAssign(_AccountingModel, _LoggedOnUserID, _TodayPeriodID, _BankingPeriodID, _CurrencyID, _CurrencySymbol, _
                                             _SelectedFloat.FloatID, _SelectedFloat.FloatSealNumber, _SelectedFloat.FloatChecked, True)

            frmFloatAssign.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Try
            If _DataHasBeenModified = True Then RaiseEvent DataChange()
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

#End Region

#Region "Child Form Events"

    Private Sub frmFloatAssign_DataChange() Handles frmFloatAssign.DataChange

        Try
            Cursor = Cursors.WaitCursor

            With _FloatViewModel
                _FloatList = .GetFloatList(_BankingPeriodID)
                .FloatGridPopulate(_FloatList)
            End With
            DisplayGridSetActiveRow(0)

            _DataHasBeenModified = True
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub frmFloatReAssign_DataChange() Handles frmFloatReAssign.DataChange

        Try
            Cursor = Cursors.WaitCursor

            With _FloatViewModel
                _FloatList = .GetFloatList(_BankingPeriodID)
                .FloatGridPopulate(_FloatList)
            End With
            DisplayGridSetActiveRow(0)

            _DataHasBeenModified = True
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ButtonAvailability(ByRef SelectedFloat As FloatList)

        EnableDisableButtons(True)

        If SelectedFloat.AssignedToUserID.HasValue = True Then
            btnAssignFloat.Enabled = False
            btnFloatCheck.Enabled = False
        End If

        If SelectedFloat.AssignedToUserID.HasValue = False Then
            btnReAssignFloat.Enabled = False
            btnUnAssignFloat.Enabled = False
        End If

        If SelectedFloat.SaleTaken = True Then
            btnReAssignFloat.Enabled = False
            btnUnAssignFloat.Enabled = False
        End If

        If SelectedFloat.FloatChecked = True Then btnFloatCheck.Enabled = False

    End Sub

    Private Sub EnableDisableButtons(ByVal State As Boolean)

        btnAssignFloat.Enabled = State
        btnReAssignFloat.Enabled = State
        btnUnAssignFloat.Enabled = State
        btnFloatCheck.Enabled = State

    End Sub

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridSetActiveRow(ByVal RowID As Integer)

        EnableDisableButtons(False)

        With spdDisplay
            .Focus()
            .ActiveSheet.SetActiveCell(RowID, 0)
            .ActiveSheet.AddSelection(RowID, 0, 1, 1)
        End With

        spdDisplay_SelectionChanged(spdDisplay, Nothing)

    End Sub

#End Region

End Class