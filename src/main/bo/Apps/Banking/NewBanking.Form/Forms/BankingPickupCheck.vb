﻿Public Class BankingPickupCheck

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mdtmBankingPeriodDate As Date
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mstrStoreName As String

    Private mcolActiveCashierList As ActiveCashierListCollection

    Private mblnDirtyData As Boolean = False

    Public Event DataChange()

    Private WithEvents frmBankingBagCreation As BankingBagCreation

    Private _BankingPickupCheckVM As IBankingPickupCheckVM

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, _
                   ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, _
                   ByVal dtmBankingPeriodDate As Date, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, _
                   ByVal strStoreID As String, ByVal strStoreName As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mdtmBankingPeriodDate = dtmBankingPeriodDate
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
    End Sub

    Private Sub BankingPickupCheck_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.ScreenTitleBankingPickupCheck

            Cursor = Cursors.WaitCursor
            _BankingPickupCheckVM = (New BankingPickupCheckVMFactory).GetImplementation
            With _BankingPickupCheckVM
                .SetBankingPickupCheckGridControl(spdInput)
                .BankingPickupCheckGridFormat()
                .SetBankingPickupCheckForm(Me)
                .BankingPickupCheckFormFormat()
                .SetBankingPeriodID(mintBankingPeriodID)
                mcolActiveCashierList = .GetActiveCashierListCollection
                .BankingPickupCheckGridPopulate(mcolActiveCashierList)
            End With
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub BankingPickupCheck_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnExistingBanking.PerformClick()
                Case Keys.F5
                    'amount typed in and F5 pressed
                    'if validation passed, spread does not calculate the total amount before the "zero amount" validation occurrs
                    'setting the focus on spread fixes this!
                    spdInput.Focus()
                    btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_ButtonClicked(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.EditorNotifyEventArgs) Handles spdInput.ButtonClicked
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveColumn As Column
            Dim ActiveCell As Cell

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet
            ActiveColumn = ActiveSheet.ActiveColumn
            ActiveCell = ActiveSheet.ActiveCell


            'check with user that this was not done by accident
            If QuestionMessageBox("Are you sure the pickup check is complete and can be acknowledged" & vbCrLf & _
                                  "Once acknowledged this pickup cannot be changed") = Windows.Forms.DialogResult.No Then
                ActiveCell.Value = False

                Exit Sub
            End If

            If CType(ActiveCell.Value, Boolean) = True Then
                'read-only
                ActiveColumn.Locked = True
                'banking now started, no choice but to complete
                btnExit.Enabled = False
                ActiveCell.Locked = True
            Else
                ''amendable
                'ActiveColumn.Locked = False
            End If
            'ActiveCell.Locked = False
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdInput_EnterCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.EnterCellEventArgs) Handles spdInput.EnterCell

        With e
            _BankingPickupCheckVM.ReadyActiveCommentsCellForEditing(.Row, .Column)
        End With
    End Sub

    Private Sub spdInput_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles spdInput.LeaveCell
        Try
            Dim ActiveSheet As SheetView
            Dim ActiveRow As Row
            Dim ActiveColumn As Column
            Dim ActiveCell As Cell

            Dim objTenderDenom As TenderDenominationList
            Dim strColumnKey As String()

            Dim decInputValue As Decimal
            Dim decOriginalValue As Decimal

            ActiveSheet = CType(sender, FarPoint.Win.Spread.FpSpread).ActiveSheet
            ActiveRow = ActiveSheet.ActiveRow
            ActiveColumn = ActiveSheet.ActiveColumn
            ActiveCell = ActiveSheet.ActiveCell

            'exit - denomination column
            If ActiveSheet.ActiveColumnIndex = 0 Then Exit Sub
            'exit - "Total" column
            If ActiveSheet.ActiveColumnIndex = ActiveSheet.ColumnCount - 1 Then Exit Sub
            'exit - bag check row
            If ActiveSheet.ActiveRowIndex = 0 Then Exit Sub

            If _BankingPickupCheckVM.ActiveCellIsCommentsCell Then
                With e
                    _BankingPickupCheckVM.UndoReadinessOfActiveCommentsCellForEditing(.Row, .Column)
                End With
            Else
                decInputValue = CDec(ActiveCell.Value)
                strColumnKey = CType(ActiveColumn.Tag, String).Split(CType("|", Char))
                objTenderDenom = CType(ActiveRow.Tag, TenderDenominationList)

                'money cannot be negative - cash tender only
                If decInputValue < 0 AndAlso CType(ActiveRow.Tag, TenderDenominationList).TenderID = 1 Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingAmountNegative)

                    btnSave.Enabled = False
                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If
                'money needs to be a multiple of the denomination
                If Not IsMultiple(decInputValue, CType(ActiveRow.Tag, TenderDenominationList).DenominationID) Then
                    InformationMessageBox(My.Resources.InformationMessages.BankingDenominationMultiple)

                    btnSave.Enabled = False
                    SpreadCellRed(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                    e.Cancel = True
                    Exit Sub
                End If

                SpreadCellBlack(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex)
                btnSave.Enabled = True

                'entered value different to original
                If mcolActiveCashierList.TenderDenominationValue(CType(strColumnKey(0), Integer), _
                                                                 CType(strColumnKey(1), Integer), _
                                                                 objTenderDenom.TenderID, objTenderDenom.DenominationID).HasValue = False Then
                    decOriginalValue = 0
                Else
                    decOriginalValue = mcolActiveCashierList.TenderDenominationValue(CType(strColumnKey(0), Integer), _
                                                                                     CType(strColumnKey(1), Integer), _
                                                                                     objTenderDenom.TenderID, objTenderDenom.DenominationID).Value
                End If
                If decInputValue <> decOriginalValue Then SpreadCellColour(ActiveSheet, ActiveSheet.ActiveRowIndex, ActiveSheet.ActiveColumnIndex, Color.Green)
            End If
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnExistingBanking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExistingBanking.Click
        Try
            Dim frm As New MainCutDown(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, mstrStoreName)

            frm.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ActiveSheet As SheetView
            Dim intColumnCount As Integer
            Dim decEnteredVariance As Decimal

            Dim intSecondUserID As Integer
            Dim blnSecurityFailed As Boolean
            Dim strBankingBagComment As String = String.Empty

            ActiveSheet = spdInput.ActiveSheet
            intColumnCount = ActiveSheet.ColumnCount

            'all bags acknowledged
            If BagsChecked() = False Then
                InformationMessageBox("All pickup bags need to be acknowledged")
                Exit Sub
            End If
            'show banking variance
            decEnteredVariance = CType(ActiveSheet.Cells(_BankingPickupCheckVM.GetCommentsRowIndex, ActiveSheet.ColumnCount - 1).Value, Decimal)
            If QuestionMessageBox("Total banking variance is " & FormattedCurrency(decEnteredVariance, mstrCurrencySymbol, 2) & vbCrLf & _
                                  "No further adjustments can be made" & vbCrLf & _
                                  "Are you sure? ") = Windows.Forms.DialogResult.No Then Exit Sub

            'banking tolerance exceeded - second manager verification required
            If CType(IIf(decEnteredVariance < 0, System.Decimal.Negate(decEnteredVariance), decEnteredVariance), Decimal) > NewBanking.Core.BankingVariance Then
                InformationMessageBox("Banking Tolerance exceeded, second manager's check now required")
                If AuthoriseCodeSecurity(True, mintFirstUserID, intSecondUserID) = False Then Exit Sub
            End If

            If _BankingPickupCheckVM.UseGenericCommentForAllBankingBags Then
                'allow comment against banking bag(s) that will be created
                Using frm As New BankingBagComment
                    If frm.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub

                    strBankingBagComment = frm.txtComment.Text
                End Using
            End If

            'perist pickups that have been changed
            Cursor = Cursors.WaitCursor
            If PickupPersist(ActiveSheet, blnSecurityFailed) = True Then
                If blnSecurityFailed = True Then
                    InformationMessageBox("Security failure will result in pickup(s) not being altered ")
                    Exit Sub
                End If

                're-load persisted pickups for next banking screen
                mcolActiveCashierList = Nothing
                With _BankingPickupCheckVM
                    .SetBankingPeriodID(mintBankingPeriodID)
                    mcolActiveCashierList = .GetActiveCashierListCollection
                End With
            End If
            Cursor = Cursors.Default

            'pass pickup totals across
            Dim colPickupTotal As New TenderDenominationListCollection
            Dim objTag As TenderDenominationList

            colPickupTotal.LoadData()
            For intRowIndex As Integer = 1 To ActiveSheet.RowCount - 1 Step 1
                'no tag - ignore
                If ActiveSheet.Rows(intRowIndex).Tag Is Nothing Then Continue For
                'tag - string type
                If ActiveSheet.Rows(intRowIndex).Tag.GetType.ToString = "System.String" Then Continue For

                objTag = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList)
                colPickupTotal.TenderDenom(objTag.TenderID, objTag.DenominationID).Amount = CType(ActiveSheet.Cells(intRowIndex, intColumnCount - 1).Value, Decimal)
            Next

            'banking bags
            frmBankingBagCreation = New BankingBagCreation(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mintBankingPeriodID, mdtmBankingPeriodDate, mstrCurrencyID, mstrCurrencySymbol, mstrStoreID, strBankingBagComment, mcolActiveCashierList, colPickupTotal)
            If frmBankingBagCreation.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub

            'main grid needs to be updated
            If mblnDirtyData = True Then RaiseEvent DataChange()
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Child Form Events"

    Private Sub frmBankingBagCreation_DataChange() Handles frmBankingBagCreation.DataChange
        Try
            mblnDirtyData = True
        Catch ex As Exception
            ErrorHandler(ex)
        End Try

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function BagsChecked() As Boolean
        Dim ActiveSheet As SheetView
        Dim ActiveRow As Row
        Dim ActiveColumn As Column
        Dim ActiveCell As Cell

        ActiveSheet = spdInput.ActiveSheet
        ActiveRow = ActiveSheet.ActiveRow
        ActiveColumn = ActiveSheet.ActiveColumn
        ActiveCell = ActiveSheet.ActiveCell

        BagsChecked = True
        'check bags - row 0
        'last column is "Total", ignore
        For intColumnIndex As Integer = 1 To ActiveSheet.ColumnCount - 2 Step 1
            If CType(ActiveSheet.Cells(0, intColumnIndex).Value, Boolean) = False Then BagsChecked = False
        Next
    End Function

    Private Function PickupPersist(ByRef ActiveSheet As SheetView, ByRef blnSecurityFailed As Boolean) As Boolean
        Dim intColumnCount As Integer
        Dim blnPickupAltered As Boolean
        Dim blnSecondCheckHappened As Boolean
        Dim intSecondUserID As Integer

        Dim strColumnKey As String()
        Dim intCashierID As Integer
        Dim intPickupID As Integer
        Dim blnFirstEndOfDayPickup As Boolean

        Dim decInputValue As Decimal
        Dim decOriginalValue As Decimal

        Dim intTenderID As Integer
        Dim decDenominationID As Decimal

        Dim intNewPickupID As Integer

        intColumnCount = ActiveSheet.ColumnCount
        PickupPersist = False
        blnSecondCheckHappened = False
        blnSecurityFailed = False

        'loop around columns(pickups) on grid - ignore first(tender/denom) & last(total) columns
        For intColumnIndex = 1 To intColumnCount - 2 Step 1
            'loop around cash & non-cash amendable tenders to check for changes
            blnPickupAltered = False

            strColumnKey = CType(ActiveSheet.Columns(intColumnIndex).Tag, String).Split(CType("|", Char))
            intCashierID = CType(strColumnKey(0), Integer)
            intPickupID = CType(strColumnKey(1), Integer)
            blnFirstEndOfDayPickup = CType(IIf(strColumnKey(2) = "1", True, False), Boolean)

            'cash tender(s)
            For intRowIndex As Integer = 1 To 13 Step 1
                intTenderID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID
                decDenominationID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID

                decInputValue = CDec(ActiveSheet.Cells(intRowIndex, intColumnIndex).Value)

                If mcolActiveCashierList.TenderDenominationValue(intCashierID, intPickupID, intTenderID, decDenominationID).HasValue = False Then
                    decOriginalValue = 0
                Else
                    decOriginalValue = mcolActiveCashierList.TenderDenominationValue(intCashierID, intPickupID, intTenderID, decDenominationID).Value
                End If
                If decInputValue <> decOriginalValue Then
                    blnPickupAltered = True
                    'no need to check any further, pickup needs to amended
                    Exit For
                End If
            Next

            'non-cash amendable tender(s)
            If blnPickupAltered = False Then

                Dim NonCashTenders As IProcessAllNonCashTenders
                NonCashTenders = (New ProcessAllNonCashTendersFactory).GetImplementation

                For intRowIndex As Integer = 15 To NonCashTenders.ProcessAllNonCashTenders Step 1

                    intTenderID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID
                    decDenominationID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID

                    decInputValue = CDec(ActiveSheet.Cells(intRowIndex, intColumnIndex).Value)

                    If mcolActiveCashierList.TenderDenominationValue(intCashierID, intPickupID, intTenderID, decDenominationID).HasValue = False Then
                        decOriginalValue = 0
                    Else
                        decOriginalValue = mcolActiveCashierList.TenderDenominationValue(intCashierID, intPickupID, intTenderID, decDenominationID).Value
                    End If
                    If decInputValue <> decOriginalValue Then
                        blnPickupAltered = True
                        'no need to check any further, pickup needs to amended
                        Exit For
                    End If

                Next
            End If

            'if one or more pickups have been altered inform user
            If blnPickupAltered = True Then PickupPersist = True

            'cancel existing pickup and create new one
            If blnPickupAltered = True Then
                'second manager's check req, ask for it only once even if several pickups are effected
                If blnSecondCheckHappened = False Then
                    blnSecondCheckHappened = True

                    InformationMessageBox("One or more pickup(s) have been altered, a second manager's check required")
                    If AuthoriseCodeSecurity(True, mintFirstUserID, intSecondUserID) = False Then
                        blnSecurityFailed = True   '
                        Exit Function
                    End If
                End If



                Dim NewPickup As Banking.Core.IPickup
                Dim Factory As New Banking.Core.PickupFactory

                Factory.PickupConstructor(mintFirstUserID, intSecondUserID, mstrAccountingModel)
                NewPickup = Factory.GetImplementation

                With NewPickup
                    .State = Banking.Core.Pickup.States.Pickup
                    .LoadOriginalBag(intPickupID)

                    .State = Banking.Core.Pickup.States.Check
                    .LoadOriginalBag(intPickupID)

                    .LoadSystemFigures(mintBankingPeriodID, intCashierID)
                    .SetComments(_BankingPickupCheckVM.GetComments(intPickupID))
                    .SetSealNumber(.OldSealNumber)

                    .SetFloatReturned(0)

                    'process cash tenders
                    For intRowIndex As Integer = 1 To 13 Step 1
                        intTenderID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID
                        decDenominationID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID
                        decInputValue = CDec(ActiveSheet.Cells(intRowIndex, intColumnIndex).Value)

                        'If decInputValue > 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                        If decInputValue <> 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                    Next
                    'process amendable non-cash tenders - applies to e.o.d pickups only
                    For intRowIndex As Integer = 15 To 20 Step 1
                        intTenderID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID
                        decDenominationID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID
                        decInputValue = CDec(ActiveSheet.Cells(intRowIndex, intColumnIndex).Value)

                        'If decInputValue > 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                        If decInputValue <> 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                    Next
                    'process non-amendable non-cash tenders - applies to e.o.d pickups only
                    For intRowIndex As Integer = 21 To 24 Step 1
                        intTenderID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).TenderID
                        decDenominationID = CType(ActiveSheet.Rows(intRowIndex).Tag, TenderDenominationList).DenominationID
                        decInputValue = CDec(ActiveSheet.Cells(intRowIndex, intColumnIndex).Value)

                        'If decInputValue > 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                        If decInputValue <> 0 Then .SetDenomination(mstrCurrencyID, decDenominationID, intTenderID, decInputValue)
                    Next
                    intNewPickupID = .NewBankingPickupReCheckSave


                    'update tag with new pickup id
                    strColumnKey = CType(ActiveSheet.Columns(intColumnIndex).Tag, String).Split(CType("|", Char))
                    intCashierID = CType(strColumnKey(0), Integer)
                    intPickupID = CType(strColumnKey(1), Integer)
                    blnFirstEndOfDayPickup = CType(IIf(strColumnKey(2) = "1", True, False), Boolean)

                    SpreadColumnTagValue(ActiveSheet, intColumnIndex, intCashierID.ToString & "|" & intNewPickupID.ToString & "|" & CType(IIf(blnFirstEndOfDayPickup = True, "1", "0"), String))
                End With
            Else
                _BankingPickupCheckVM.PersistCommentsIfChanged(intPickupID)
            End If
        Next
    End Function

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    'Private Sub DisplayGridFormat()
    '    Dim NewSheet As New SheetView

    '    SpreadSheetCustomise(NewSheet, 1, Model.SelectionUnit.Cell)
    '    SpreadSheetCustomiseHeader(NewSheet, gintHeaderHeightTriple)
    '    SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTendersWide, False)     'Screen Column - Denomination
    '    SpreadColumnAlignLeft(NewSheet, 0)
    '    SpreadColumnBackgroundColour(NewSheet, 0, Color.Beige)
    '    SpreadSheetCustomiseFreezeLeadingColumns(NewSheet, 1)

    '    SpreadGridSheetAdd(spdInput, NewSheet, True, String.Empty)
    '    SpreadGridScrollBar(spdInput, ScrollBarPolicy.Never, ScrollBarPolicy.AsNeeded)
    '    SpreadGridInputMaps(spdInput)
    'End Sub

    'Private Sub DisplayGridPopulate(ByVal intBankingPeriodID As Integer)
    '    Dim colTenderDenomList As TenderDenominationListCollection
    '    Dim objTenderDenom As TenderDenominationList

    '    Dim CurrentSheet As SheetView
    '    Dim intRowIndex As Integer = 0
    '    Dim intColumnIndex As Integer = 0
    '    Dim strColumnTag As String
    '    Dim blnFirstEndOfDayPickup As Boolean

    '    'load data
    '    mcolActiveCashierList = New ActiveCashierListCollection
    '    mcolActiveCashierList.LoadAllCashiers(intBankingPeriodID)

    '    colTenderDenomList = New TenderDenominationListCollection
    '    colTenderDenomList.LoadData()

    '    CurrentSheet = spdInput.ActiveSheet
    '    SpreadSheetClearDown(CurrentSheet)

    '    'bag check
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Bag Check")
    '    SpreadRowHeight(CurrentSheet, intRowIndex, 30)
    '    SpreadRowVerticalAlignment(CurrentSheet, intRowIndex, CellVerticalAlignment.Center)
    '    'cash tender
    '    For Each obj As TenderDenominationList In colTenderDenomList
    '        If obj.TenderID <> 1 Then Continue For

    '        SpreadRowAdd(CurrentSheet, intRowIndex)
    '        SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
    '        SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
    '    Next
    '    SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
    '    'total cash entered - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash Entered")
    '    SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
    '    'non-cash tenders - amendable
    '    For Each obj As TenderDenominationList In colTenderDenomList
    '        If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = True) Then Continue For

    '        SpreadRowAdd(CurrentSheet, intRowIndex)
    '        SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
    '        SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
    '    Next
    '    SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

    '    'non-cash tenders - not amendable
    '    For Each obj As TenderDenominationList In colTenderDenomList
    '        If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = False) Then Continue For

    '        SpreadRowAdd(CurrentSheet, intRowIndex)
    '        SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
    '        SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
    '    Next
    '    SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

    '    'total non-cash entered - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Non Cash Entered")
    '    'total entered - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Entered")
    '    'total entered (all pickups) - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Entered (All Pickups)")
    '    SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
    '    'start float - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Start Float")
    '    'start float - new float
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "New Float")
    '    'system total
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Total")
    '    'variance - calculated
    '    SpreadRowAdd(CurrentSheet, intRowIndex)
    '    SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
    '    SpreadRowBold(spdInput, CurrentSheet, intRowIndex)
    '    SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
    '    SpreadCellValue(CurrentSheet, intRowIndex, 0, "Variance")

    '    'cashier list
    '    For Each objCashier As ActiveCashierList In mcolActiveCashierList
    '        'cashier - end of day pickup (should only be one), just in case the flag will be reset for each cashier and a tag added
    '        '          to the first e.o.d pickup
    '        blnFirstEndOfDayPickup = True

    '        'cashier e.o.d pickup(s)
    '        For Each objPickup As Pickup In objCashier.EndOfDayPickupList
    '            'add new pickup column
    '            strColumnTag = objCashier.CashierID.ToString & "|" & objPickup.PickupID.ToString & "|" & CType(IIf(blnFirstEndOfDayPickup = True, "1", "0"), String)

    '            SpreadColumnAdd(CurrentSheet, intColumnIndex)
    '            SpreadColumnEditable(CurrentSheet, intColumnIndex, True)
    '            SpreadColumnCustomise(CurrentSheet, intColumnIndex, objCashier.CashierEmployeeCode & " " & objCashier.CashierUserName & vbCrLf & _
    '                                                                "E.O.D Pickup" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
    '                                                                objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)
    '            SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
    '            SpreadColumnTagValue(CurrentSheet, intColumnIndex, strColumnTag)

    '            'bag check
    '            SpreadCellButton(CurrentSheet, 0, intColumnIndex, False)

    '            SpreadCellFormula(CurrentSheet, 14, intColumnIndex, "SUM(R2C:R14C)")
    '            SpreadCellFormula(CurrentSheet, 25, intColumnIndex, "SUM(R16C:R25C)")
    '            SpreadCellFormula(CurrentSheet, 26, intColumnIndex, "R15C+R26C")

    '            If blnFirstEndOfDayPickup = True Then
    '                blnFirstEndOfDayPickup = False    'first column for this cashier will be tagged

    '                SpreadCellFormula(CurrentSheet, 27, intColumnIndex, "R27C" & (intColumnIndex + 1).ToString)  'row - total entered (all pickups)
    '                SpreadCellValue(CurrentSheet, 28, intColumnIndex, objCashier.StartFloatValue)
    '                SpreadCellValue(CurrentSheet, 29, intColumnIndex, objCashier.NewFloatValue)
    '                SpreadCellValue(CurrentSheet, 30, intColumnIndex, objCashier.SystemSales)
    '                SpreadCellFormula(CurrentSheet, 31, intColumnIndex, "R28C+R30C-R29C-R31C")                   'row - variance
    '            Else
    '                'row - total entered (all pickups), amend
    '                For intIndex As Integer = intColumnIndex To 0 Step -1
    '                    If CurrentSheet.Columns(intIndex).Tag IsNot Nothing AndAlso CType(CurrentSheet.Columns(intIndex).Tag, String).Split(CType("|", Char))(2) = "1" Then
    '                        SpreadCellFormulaAppend(CurrentSheet, 27, intIndex, "+R27C" & (intColumnIndex + 1).ToString)
    '                        Exit For
    '                    End If
    '                Next
    '            End If

    '            For Each obj As PickupSale In objPickup.PickupSales
    '                'iliterate around rows for this pickup, find cash tender/denomination match
    '                For intIndex As Integer = 1 To CurrentSheet.RowCount - 1 Step 1
    '                    'no tag - ignore
    '                    If CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
    '                    'tag - string type
    '                    If CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

    '                    objTenderDenom = CType(CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
    '                    'make credit/debit non-cash tenders read-only
    '                    If objTenderDenom.TenderID <> 1 AndAlso objTenderDenom.TenderReadOnly = True Then SpreadCellLocked(CurrentSheet, intIndex, intColumnIndex, True)
    '                    If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
    '                        'match found
    '                        If obj.PickupValue.HasValue = True Then SpreadCellValue(CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
    '                    End If
    '                Next
    '            Next
    '        Next
    '        'cashier - cash drop(s)
    '        For Each objPickup As Pickup In objCashier.CashDropsPickupList
    '            'add new pickup column
    '            strColumnTag = objCashier.CashierID.ToString & "|" & objPickup.PickupID.ToString & "|" & CType(IIf(blnFirstEndOfDayPickup = True, "1", "0"), String)

    '            SpreadColumnAdd(CurrentSheet, intColumnIndex)
    '            SpreadColumnEditable(CurrentSheet, intColumnIndex, True)
    '            SpreadColumnCustomise(CurrentSheet, intColumnIndex, objCashier.CashierEmployeeCode & " " & objCashier.CashierUserName & vbCrLf & _
    '                                                                "Cash Drop" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
    '                                                                objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)

    '            SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
    '            SpreadColumnTagValue(CurrentSheet, intColumnIndex, strColumnTag)

    '            'bag check
    '            SpreadCellButton(CurrentSheet, 0, intColumnIndex, False)

    '            SpreadCellFormula(CurrentSheet, 14, intColumnIndex, "SUM(R2C:R14C)")
    '            SpreadCellFormula(CurrentSheet, 25, intColumnIndex, "SUM(R16C:R25C)")
    '            SpreadCellFormula(CurrentSheet, 26, intColumnIndex, "R15C+R26C")

    '            'row - total entered (all pickups), amend
    '            For intIndex As Integer = intColumnIndex To 0 Step -1
    '                If CurrentSheet.Columns(intIndex).Tag IsNot Nothing AndAlso CType(CurrentSheet.Columns(intIndex).Tag, String).Split(CType("|", Char))(2) = "1" Then
    '                    SpreadCellFormulaAppend(CurrentSheet, 27, intIndex, "+R27C" & (intColumnIndex + 1).ToString)
    '                    Exit For
    '                End If
    '            Next

    '            For Each obj As PickupSale In objPickup.PickupSales
    '                'iliterate around rows for this pickup, find cash tender/denomination match
    '                For intIndex As Integer = 0 To CurrentSheet.RowCount - 1 Step 1
    '                    'no tag - ignore
    '                    If CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
    '                    'tag - string type
    '                    If CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

    '                    objTenderDenom = CType(CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
    '                    'make ALL non-cash tenders read-only
    '                    If objTenderDenom.TenderID <> 1 Then SpreadCellLocked(CurrentSheet, intIndex, intColumnIndex, True)
    '                    If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
    '                        'match found
    '                        If obj.PickupValue.HasValue = True Then SpreadCellValue(CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
    '                    End If
    '                Next
    '            Next
    '        Next
    '    Next
    '    'total column
    '    SpreadColumnAdd(CurrentSheet, intColumnIndex)
    '    SpreadColumnCustomise(CurrentSheet, intColumnIndex, "Total", gcintColumnWidthMoney, False)
    '    SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
    '    'cash tender
    '    'total cash entered - calculated
    '    'non-cash tenders - amendable
    '    'non-cash tenders - not amendable
    '    'total non-cash entered - calculated
    '    'total entered - calculated
    '    'total entered (all pickups) - calculated
    '    'start float - calculated
    '    'start float - new float
    '    'system total
    '    'variance - calculated
    '    For intIndex As Integer = 1 To CurrentSheet.RowCount - 1 Step 1
    '        If intIndex = 27 Then Continue For 'IGNORE - total entered (all pickups)
    '        SpreadCellFormula(CurrentSheet, intIndex, intColumnIndex, "SUM(R" & (intIndex + 1).ToString & "C2:R" & (intIndex + 1).ToString & "C" & intColumnIndex.ToString.Trim & ")")
    '    Next
    'End Sub

#End Region

End Class