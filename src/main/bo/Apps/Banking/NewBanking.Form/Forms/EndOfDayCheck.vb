﻿Public Class EndOfDayCheck
    Private _ViewModel As IEndOfDayCheckVM

    Private _CommentReturned As String
    Private _SealNumberReturned As String

    Public Event DataChange()

    Private WithEvents _Comment As EndOfDayCheckComment
    Private WithEvents _PickupSeal As EndOfDayCheckPickupSeal
    Private WithEvents _BankingSeal As EndOfDayCheckBankingSeal
    Private WithEvents _SafeLock As EndOfDayCheckSafeLock

    Public Sub New(ByVal PeriodID As Integer, ByVal LoggedOnUserID As Integer)

        InitializeComponent()

        _ViewModel = (New EndOfDayCheckVMFactory).GetImplementation
        _ViewModel.Initialise(PeriodID)
        _ViewModel.LoggedOnUserID = LoggedOnUserID

    End Sub

#Region "Form Event Handlers"

    Private Sub EndOfDayCheck_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        With _ViewModel

            .SetPickupBagListControl(spdPickupBagList)
            .SetPickupCommentListControl(spdPickupCommentList)
            .SetBankingBagListControl(spdBankingBagList)
            .SetBankingCommentListControl(spdBankingCommentList)

            .SetBankingPanelControl(GroupBoxBanking)
            .SetPickupPanelControl(GroupBoxPickup)

            .FormatPickupBagListControl()
            .FormatPickupCommentListControl()
            .FormatBankingBagListControl()
            .FormatBankingCommentListControl()

            .PickupPanelEnabled()

        End With

    End Sub

    Private Sub EndOfDayCheck_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True

        Select Case e.KeyData

            Case Keys.F1 : btnAddPickupBag.PerformClick()
            Case Keys.F2 : btnAddPickupComment.PerformClick()
            Case Keys.F3 : btnCompletePickup.PerformClick()

            Case Keys.F4 : btnAddBankingBag.PerformClick()
            Case Keys.F5 : btnAddBankingComment.PerformClick()
            Case Keys.F6 : btnCompleteBanking.PerformClick()

            Case Keys.F12 : btnCancelAndExit.PerformClick()

            Case Else : e.Handled = False

        End Select

    End Sub

#End Region

#Region "Button Event Handlers: Pickup"

    Private Sub btnAddPickupBag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPickupBag.Click

        _PickupSeal = New EndOfDayCheckPickupSeal

        _CommentReturned = Nothing

        If _PickupSeal.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            If ValidPickupSeal() = False Then Exit Sub

            _ViewModel.CreatePickupBag(_SealNumberReturned, _CommentReturned)
            _ViewModel.PopulatePickupBagListControl()

        End If

    End Sub

    Private Sub btnAddPickupComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPickupComment.Click

        PickupComment("Pickup Comment")

    End Sub

    Private Sub btnCompletePickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompletePickup.Click

        If _ViewModel.MatchSystemPickupExpectation = False Then

            If QuestionMessageBox("Have not matched system expectations. Accept anyway") = Windows.Forms.DialogResult.No Then Exit Sub

            If PickupComment("Have not matched system expectations - Please provide explanation") = False Then

                InformationMessageBox("Sorry no explanations provided; pickup process cannot be completed")

                Exit Sub

            End If

        End If

        _ViewModel.BankingPanelEnabled()

    End Sub

#End Region

#Region "Button Event Handlers: Banking"

    Private Sub btnAddBankingBag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBankingBag.Click

        _BankingSeal = New EndOfDayCheckBankingSeal

        _CommentReturned = Nothing

        If _BankingSeal.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            If ValidBankingSeal() = False Then Exit Sub

            _ViewModel.CreateBankingBag(_SealNumberReturned, _CommentReturned)
            _ViewModel.PopulateBankingBagListControl()

        End If

    End Sub

    Private Sub btnAddBankingComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBankingComment.Click

        BankingComment("Banking Comment")

    End Sub

    Private Sub btnCompleteBanking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompleteBanking.Click

        If _ViewModel.MatchSystemBankingExpectation = False Then

            If QuestionMessageBox("Have not matched system expectations. Accept anyway") = Windows.Forms.DialogResult.No Then Exit Sub

            If BankingComment("Have not matched system expectations - Please provide explanation") = False Then

                InformationMessageBox("Sorry no explanations provided; banking process cannot be completed")

                Exit Sub

            End If

        End If

        _SafeLock = New EndOfDayCheckSafeLock

        If _SafeLock.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub

        If _ViewModel.ManagerCheck() = False Then Exit Sub

        If _ViewModel.WitnessCheck() = False Then Exit Sub

        _ViewModel.AdditionalInformation()

        _ViewModel.Persist()

        RaiseEvent DataChange()

        FindForm.Close()

    End Sub

#End Region

#Region "Button Event Handlers: Rest"

    Private Sub btnCancelAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAndExit.Click

        UserControlScan1.CloseScanner()

        FindForm.Close()

    End Sub

#End Region

#Region "Information Event Handlers"

    Private Sub _Comment_CommentEntered(ByVal Value As String) Handles _Comment.CommentEntered

        _CommentReturned = Value

    End Sub

    Private Sub _PickupSeal_SealEntered(ByVal Value As String) Handles _PickupSeal.SealEntered

        _SealNumberReturned = Value

    End Sub

    Private Sub _BankingSeal_SealEntered(ByVal Value As String) Handles _BankingSeal.SealEntered

        _SealNumberReturned = Value

    End Sub

    Private Sub _SafeLock_SafeLocked(ByVal LockedFromDate As Date, ByVal LockedFromTime As Date, ByVal LockedToDate As Date, ByVal LockedToTime As Date) Handles _SafeLock.SafeLocked

        _ViewModel.SetSafeLockedFrom(LockedFromDate, LockedFromTime)

        _ViewModel.SetSafeLockedTo(LockedToDate, LockedToTime)

    End Sub

#End Region

#Region "Scanner Event Handlers"

    Private Sub UserControlScan1_InputReceived(ByVal strInput As String) Handles UserControlScan1.ReceivedScannerInput

        _SealNumberReturned = strInput

        Select Case _ViewModel.GetScanMode

            Case IEndOfDayCheckVM.ScanMode.PickupMode

                If ValidPickupSeal() = False Then Exit Sub

                _ViewModel.CreatePickupBag(_SealNumberReturned, _CommentReturned)
                _ViewModel.PopulatePickupBagListControl()

            Case IEndOfDayCheckVM.ScanMode.BankingMode
                txtSeal.Text = strInput
                _SealNumberReturned = txtSeal.Text 'This is to get the number in the correct seal format as the scanner doesn't do '-' character
                If ValidBankingSeal() = False Then Exit Sub

                _ViewModel.CreateBankingBag(_SealNumberReturned, _CommentReturned)
                _ViewModel.PopulateBankingBagListControl()

        End Select

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Private Function ValidPickupSeal() As Boolean

        If _ViewModel.PickupBagAlreadyScanned(_SealNumberReturned) = True Then

            InformationMessageBox("Sorry pickup bag already scanned")
            Return False

        End If

        If _ViewModel.PickupBagHasValidSeal(_SealNumberReturned) = False Then

            If QuestionMessageBox("Invalid seal number. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

            If PickupSealComment("Invalid seal number - Please provide explanation") = False Then

                InformationMessageBox("Sorry no explanations provided; pickup rejected")
                Return False

            End If

        Else

            If _ViewModel.PickupBagExistInSafe(_SealNumberReturned) = False Then

                If QuestionMessageBox("Pickup bag does not exists in safe. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

                If PickupSealComment("Pickup bag does not exists in safe - Please provide explanation") = False Then

                    InformationMessageBox("Sorry no explanations provided; pickup rejected")
                    Return False

                End If

            End If

        End If

        Return True

    End Function

    Private Function ValidBankingSeal() As Boolean

        If _ViewModel.BankingBagAlreadyScanned(_SealNumberReturned) = True Then

            InformationMessageBox("Sorry banking bag already scanned")
            Return False

        End If

        If _ViewModel.BankingBagHasValidSeal(_SealNumberReturned) = False Then

            If QuestionMessageBox("Invalid seal number. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

            If BankingSealComment("Invalid seal number - Please provide explanation") = False Then

                InformationMessageBox("Sorry no explanations provided; banking bag rejected")
                Return False

            End If

        Else

            If _ViewModel.BankingBagExistInSafe(_SealNumberReturned) = False Then

                If QuestionMessageBox("Banking bag does not exists in safe. Accept anyway") = Windows.Forms.DialogResult.No Then Return False

                If BankingSealComment("Banking bag does not exists in safe - Please provide explanation") = False Then

                    InformationMessageBox("Sorry no explanations provided; banking bag rejected")
                    Return False

                End If

            End If

        End If

        Return True

    End Function

    Private Function PickupComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            _ViewModel.CreatePickupComment(_CommentReturned)
            _ViewModel.PopulatePickupCommentListControl()

            PickupComment = True

        Else

            PickupComment = False

        End If

    End Function

    Private Function PickupSealComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            PickupSealComment = True

        Else

            PickupSealComment = False

        End If

    End Function

    Private Function BankingComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            _ViewModel.CreateBankingComment(_CommentReturned)
            _ViewModel.PopulateBankingCommentListControl()

            BankingComment = True

        Else

            BankingComment = False

        End If

    End Function

    Private Function BankingSealComment(ByVal Info As String) As Boolean

        _Comment = New EndOfDayCheckComment(Info)

        If _Comment.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

            BankingSealComment = True

        Else

            BankingSealComment = False

        End If

    End Function

#End Region

End Class