﻿Public Class EndOfDayCheckBankingSeal

    Public Event SealEntered(ByVal Value As String)

    Private Sub EndOfDayCheckBankingSeal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True

        Select Case e.KeyData

            Case Keys.Enter : btnSave.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False

        End Select

    End Sub

    Private Sub txtSeal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSeal.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

    End Sub

    Private Sub txtSeal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSeal.Leave

        If Me.ActiveControl Is btnExit Then Exit Sub

        If Not txtSeal.MaskFull Then
            InformationMessageBox(My.Resources.InformationMessages.SealBanking)
            txtSeal.Focus()
            Exit Sub
        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If txtSeal.Text.Trim = "-        -" Then

            txtSeal.Focus()
            Exit Sub

        End If

        RaiseEvent SealEntered(txtSeal.Text)

        Me.DialogResult = Windows.Forms.DialogResult.OK
        UserControlScan1.CloseScanner()
        FindForm.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        UserControlScan1.CloseScanner()
        FindForm.Close()

    End Sub

    Private Sub UserControlScan1_InputReceived(ByVal Input As String) Handles UserControlScan1.ReceivedScannerInput
        txtSeal.Text = Input
    End Sub

End Class