﻿Public Class UnFloatedPickup

    Private mcolList As UnFloatedPickupListCollection
    Private mSelected As NewBanking.Core.UnFloatedPickupList

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mdtmBankingPeriodDate As Date
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mstrStoreName As String

    Private mblnDirtyData As Boolean = False

    Public Event DataChange()

    Private WithEvents frmPickupEntry As PickupEntry

    Private _UnfloatedPickupVM As IUnFloatedPickupVM

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, _
                   ByVal dtmBankingPeriodDate As Date, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, _
                   ByVal strStoreID As String, ByVal strStoreName As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mdtmBankingPeriodDate = dtmBankingPeriodDate
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
    End Sub

    Private Sub UnFloatedPickup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleUnFloatedPickup
            _UnfloatedPickupVM = (New UnFloatedPickupVMFactory).GetImplementation
            With _UnfloatedPickupVM
                .SetUnFloatedPickupGridControl(spdDisplay)
                .UnFloatedPickupGridFormat()
                mcolList = .GetUnFloatedPickupList(mintBankingPeriodID)
                .UnFloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub UnFloatedPickup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnCompletePickup.PerformClick()
                Case Keys.F8 : btnCashierReport.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdDisplay_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles spdDisplay.SelectionChanged
        Try
            Dim FP As FarPoint.Win.Spread.FpSpread
            Dim intCashierID As Integer

            FP = CType(sender, FarPoint.Win.Spread.FpSpread)

            If FP.ActiveSheet.ActiveRow Is Nothing Then Exit Sub 'grid is emptyy

            intCashierID = CType(FP.ActiveSheet.ActiveRow.Tag, Integer)

            mSelected = New NewBanking.Core.UnFloatedPickupList
            mSelected = mcolList.SelectedCashier(intCashierID)

            ButtonAvailability(mSelected)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnCompletePickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompletePickup.Click
        Try
            With mSelected
                frmPickupEntry = New PickupEntry(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mintBankingPeriodID, mstrCurrencyID, mstrCurrencySymbol, _
                                                 PickupEntry.PickupModel.UnFloated, .CashierID, .CashierUserName, .CashierEmployeeCode, New System.Nullable(Of Decimal), String.Empty)
            End With

            frmPickupEntry.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnCashierReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCashierReport.Click
        Try
            Dim frmReport As New CashierReport(mintBankingPeriodID, mdtmBankingPeriodDate, mSelected.CashierID, mstrStoreID, mstrStoreName)

            frmReport.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            If mblnDirtyData = True Then RaiseEvent DataChange()
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub
#End Region

#Region "Child Form Events"

    Private Sub PickupEntryDataChange() Handles frmPickupEntry.DataChange
        Try
            Cursor = Cursors.WaitCursor
            With _UnfloatedPickupVM
                mcolList = .GetUnFloatedPickupList(mintBankingPeriodID)
                .UnFloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)

            'main grid needs to be updated
            mblnDirtyData = True
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub
#End Region

#Region "Private Procedures And Functions"

    Private Sub ButtonAvailability(ByRef objSelected As NewBanking.Core.UnFloatedPickupList)
        EnableDisableButtons(True)

        SetCompletePickupAvailability(objSelected)
        'pickup not created
        If objSelected.PickupID.HasValue = False Then btnCashierReport.Enabled = False
    End Sub

    Friend Sub SetCompletePickupAvailability(ByRef Selected As UnFloatedPickupList)
        Dim CompletePickupButtonAvailablity As ICompletePickupAvailability

        CompletePickupButtonAvailablity = (New CompletePickupAvailabilityImplementationFactory).GetImplementation
        With btnCompletePickup
            .Enabled = CompletePickupButtonAvailablity.IsAvailable(Selected, .Enabled, BankingPeriodDateIsToday(mdtmBankingPeriodDate))
        End With
    End Sub

    Private Sub EnableDisableButtons(ByVal blnState As Boolean)
        btnCompletePickup.Enabled = blnState
        btnCashierReport.Enabled = blnState
    End Sub

    Friend Function BankingPeriodDateIsToday(ByVal BankingPeriodDate As Date) As Boolean

        Return DateDiff(DateInterval.Day, BankingPeriodDate, Now()) = 0
    End Function
#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridSetActiveRow(ByVal intRowID As Integer)
        EnableDisableButtons(False)
        With spdDisplay
            .Focus()
            .ActiveSheet.SetActiveCell(intRowID, 0)
            .ActiveSheet.AddSelection(intRowID, 0, 1, 1)
        End With

        spdDisplay_SelectionChanged(spdDisplay, Nothing)
    End Sub
#End Region
End Class