﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BankingSeal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblInformationOne = New System.Windows.Forms.Label
        Me.spdInput = New FarPoint.Win.Spread.FpSpread
        Me.lblSeal = New System.Windows.Forms.Label
        Me.txtSlipNo = New System.Windows.Forms.TextBox
        Me.blSlipNo = New System.Windows.Forms.Label
        Me.txtStoreNo = New System.Windows.Forms.TextBox
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(256, 453)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 44)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "F12 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(188, 453)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 44)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblInformationOne
        '
        Me.lblInformationOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformationOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblInformationOne.Location = New System.Drawing.Point(8, 9)
        Me.lblInformationOne.Name = "lblInformationOne"
        Me.lblInformationOne.Size = New System.Drawing.Size(504, 60)
        Me.lblInformationOne.TabIndex = 1
        Me.lblInformationOne.Text = "XXXXXXXX"
        Me.lblInformationOne.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(150, 72)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(249, 308)
        Me.spdInput.TabIndex = 2
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance1
        Me.spdInput.ActiveSheetIndex = -1
        '
        'lblSeal
        '
        Me.lblSeal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSeal.Location = New System.Drawing.Point(147, 416)
        Me.lblSeal.Name = "lblSeal"
        Me.lblSeal.Size = New System.Drawing.Size(118, 22)
        Me.lblSeal.TabIndex = 46
        Me.lblSeal.Text = "Scan Security Seal"
        Me.lblSeal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSlipNo
        '
        Me.txtSlipNo.Location = New System.Drawing.Point(301, 393)
        Me.txtSlipNo.MaxLength = 3
        Me.txtSlipNo.Name = "txtSlipNo"
        Me.txtSlipNo.Size = New System.Drawing.Size(50, 20)
        Me.txtSlipNo.TabIndex = 4
        '
        'blSlipNo
        '
        Me.blSlipNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.blSlipNo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.blSlipNo.Location = New System.Drawing.Point(147, 393)
        Me.blSlipNo.Name = "blSlipNo"
        Me.blSlipNo.Size = New System.Drawing.Size(118, 22)
        Me.blSlipNo.TabIndex = 48
        Me.blSlipNo.Text = "Slip Number"
        Me.blSlipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStoreNo
        '
        Me.txtStoreNo.Location = New System.Drawing.Point(264, 393)
        Me.txtStoreNo.MaxLength = 3
        Me.txtStoreNo.Name = "txtStoreNo"
        Me.txtStoreNo.ReadOnly = True
        Me.txtStoreNo.Size = New System.Drawing.Size(37, 20)
        Me.txtStoreNo.TabIndex = 3
        Me.txtStoreNo.TabStop = False
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(264, 416)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 5
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(28, 442)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(46, 44)
        Me.UserControlScan1.TabIndex = 49
        Me.UserControlScan1.Visible = False
        '
        'BankingSeal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 508)
        Me.ControlBox = False
        Me.Controls.Add(Me.UserControlScan1)
        Me.Controls.Add(Me.txtSeal)
        Me.Controls.Add(Me.txtStoreNo)
        Me.Controls.Add(Me.txtSlipNo)
        Me.Controls.Add(Me.blSlipNo)
        Me.Controls.Add(Me.lblSeal)
        Me.Controls.Add(Me.spdInput)
        Me.Controls.Add(Me.lblInformationOne)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "BankingSeal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BankingSeal"
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblInformationOne As System.Windows.Forms.Label
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents lblSeal As System.Windows.Forms.Label
    Friend WithEvents txtSlipNo As System.Windows.Forms.TextBox
    Friend WithEvents blSlipNo As System.Windows.Forms.Label
    Friend WithEvents txtStoreNo As System.Windows.Forms.TextBox
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
End Class
