﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SafeMaintenanceWithBankingBagsAndComments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance4 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnSafeCheckReport = New System.Windows.Forms.Button
        Me.lblMiscellaneousProperty = New System.Windows.Forms.Label
        Me.txtMiscellaneousProperty = New System.Windows.Forms.TextBox
        Me.txtGiftTokenSealNumber = New System.Windows.Forms.TextBox
        Me.lblGiftTokenSealNumber = New System.Windows.Forms.Label
        Me.txtSafeComments = New System.Windows.Forms.TextBox
        Me.lblSafeComments = New System.Windows.Forms.Label
        Me.GroupBoxBanking = New System.Windows.Forms.GroupBox
        Me.UserControlScan1 = New BarCodeScanner.UserControlScan
        Me.txtSeal = New System.Windows.Forms.MaskedTextBox
        Me.btnAddBankingBag = New System.Windows.Forms.Button
        Me.spdBankingBagList = New FarPoint.Win.Spread.FpSpread
        Me.GroupBoxCash = New System.Windows.Forms.GroupBox
        Me.spdInput = New FarPoint.Win.Spread.FpSpread
        Me.GroupBoxBanking.SuspendLayout()
        CType(Me.spdBankingBagList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxCash.SuspendLayout()
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnExit.Location = New System.Drawing.Point(616, 730)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(125, 44)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSave.Location = New System.Drawing.Point(356, 730)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(125, 44)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnSafeCheckReport
        '
        Me.btnSafeCheckReport.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSafeCheckReport.Location = New System.Drawing.Point(487, 730)
        Me.btnSafeCheckReport.Name = "btnSafeCheckReport"
        Me.btnSafeCheckReport.Size = New System.Drawing.Size(125, 44)
        Me.btnSafeCheckReport.TabIndex = 8
        Me.btnSafeCheckReport.Text = "F7 Safe Check Report"
        Me.btnSafeCheckReport.UseVisualStyleBackColor = True
        '
        'lblMiscellaneousProperty
        '
        Me.lblMiscellaneousProperty.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblMiscellaneousProperty.AutoSize = True
        Me.lblMiscellaneousProperty.Location = New System.Drawing.Point(15, 534)
        Me.lblMiscellaneousProperty.Name = "lblMiscellaneousProperty"
        Me.lblMiscellaneousProperty.Size = New System.Drawing.Size(116, 13)
        Me.lblMiscellaneousProperty.TabIndex = 4
        Me.lblMiscellaneousProperty.Text = "&Miscellaneous Property"
        '
        'txtMiscellaneousProperty
        '
        Me.txtMiscellaneousProperty.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtMiscellaneousProperty.Location = New System.Drawing.Point(137, 534)
        Me.txtMiscellaneousProperty.Multiline = True
        Me.txtMiscellaneousProperty.Name = "txtMiscellaneousProperty"
        Me.txtMiscellaneousProperty.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMiscellaneousProperty.Size = New System.Drawing.Size(952, 56)
        Me.txtMiscellaneousProperty.TabIndex = 4
        '
        'txtGiftTokenSealNumber
        '
        Me.txtGiftTokenSealNumber.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtGiftTokenSealNumber.Location = New System.Drawing.Point(137, 596)
        Me.txtGiftTokenSealNumber.Multiline = True
        Me.txtGiftTokenSealNumber.Name = "txtGiftTokenSealNumber"
        Me.txtGiftTokenSealNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtGiftTokenSealNumber.Size = New System.Drawing.Size(952, 56)
        Me.txtGiftTokenSealNumber.TabIndex = 5
        '
        'lblGiftTokenSealNumber
        '
        Me.lblGiftTokenSealNumber.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblGiftTokenSealNumber.AutoSize = True
        Me.lblGiftTokenSealNumber.Location = New System.Drawing.Point(10, 596)
        Me.lblGiftTokenSealNumber.Name = "lblGiftTokenSealNumber"
        Me.lblGiftTokenSealNumber.Size = New System.Drawing.Size(121, 13)
        Me.lblGiftTokenSealNumber.TabIndex = 6
        Me.lblGiftTokenSealNumber.Text = "&Gift Token Seal Number"
        '
        'txtSafeComments
        '
        Me.txtSafeComments.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtSafeComments.Location = New System.Drawing.Point(137, 657)
        Me.txtSafeComments.Multiline = True
        Me.txtSafeComments.Name = "txtSafeComments"
        Me.txtSafeComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSafeComments.Size = New System.Drawing.Size(952, 56)
        Me.txtSafeComments.TabIndex = 6
        '
        'lblSafeComments
        '
        Me.lblSafeComments.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblSafeComments.AutoSize = True
        Me.lblSafeComments.Location = New System.Drawing.Point(75, 657)
        Me.lblSafeComments.Name = "lblSafeComments"
        Me.lblSafeComments.Size = New System.Drawing.Size(56, 13)
        Me.lblSafeComments.TabIndex = 8
        Me.lblSafeComments.Text = "&Comments"
        '
        'GroupBoxBanking
        '
        Me.GroupBoxBanking.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBoxBanking.Controls.Add(Me.UserControlScan1)
        Me.GroupBoxBanking.Controls.Add(Me.txtSeal)
        Me.GroupBoxBanking.Controls.Add(Me.btnAddBankingBag)
        Me.GroupBoxBanking.Controls.Add(Me.spdBankingBagList)
        Me.GroupBoxBanking.Location = New System.Drawing.Point(10, 357)
        Me.GroupBoxBanking.Name = "GroupBoxBanking"
        Me.GroupBoxBanking.Size = New System.Drawing.Size(1079, 166)
        Me.GroupBoxBanking.TabIndex = 18
        Me.GroupBoxBanking.TabStop = False
        Me.GroupBoxBanking.Text = "Banking Bags"
        '
        'UserControlScan1
        '
        Me.UserControlScan1.Location = New System.Drawing.Point(966, 110)
        Me.UserControlScan1.Name = "UserControlScan1"
        Me.UserControlScan1.Size = New System.Drawing.Size(43, 44)
        Me.UserControlScan1.TabIndex = 20
        Me.UserControlScan1.Visible = False
        '
        'txtSeal
        '
        Me.txtSeal.Location = New System.Drawing.Point(943, 122)
        Me.txtSeal.Mask = "000-00000000-0"
        Me.txtSeal.Name = "txtSeal"
        Me.txtSeal.Size = New System.Drawing.Size(89, 20)
        Me.txtSeal.TabIndex = 21
        Me.txtSeal.Visible = False
        '
        'btnAddBankingBag
        '
        Me.btnAddBankingBag.Location = New System.Drawing.Point(898, 60)
        Me.btnAddBankingBag.Name = "btnAddBankingBag"
        Me.btnAddBankingBag.Size = New System.Drawing.Size(172, 44)
        Me.btnAddBankingBag.TabIndex = 3
        Me.btnAddBankingBag.Text = "F1 Scan / Key"
        Me.btnAddBankingBag.UseVisualStyleBackColor = True
        '
        'spdBankingBagList
        '
        Me.spdBankingBagList.About = "3.0.2004.2005"
        Me.spdBankingBagList.AccessibleDescription = ""
        Me.spdBankingBagList.EditModeReplace = True
        Me.spdBankingBagList.Location = New System.Drawing.Point(6, 19)
        Me.spdBankingBagList.Name = "spdBankingBagList"
        Me.spdBankingBagList.Size = New System.Drawing.Size(886, 136)
        Me.spdBankingBagList.TabIndex = 2
        Me.spdBankingBagList.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdBankingBagList.TextTipAppearance = TipAppearance3
        Me.spdBankingBagList.ActiveSheetIndex = -1
        '
        'GroupBoxCash
        '
        Me.GroupBoxCash.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBoxCash.Controls.Add(Me.spdInput)
        Me.GroupBoxCash.Location = New System.Drawing.Point(10, 1)
        Me.GroupBoxCash.Name = "GroupBoxCash"
        Me.GroupBoxCash.Size = New System.Drawing.Size(1079, 350)
        Me.GroupBoxCash.TabIndex = 19
        Me.GroupBoxCash.TabStop = False
        Me.GroupBoxCash.Text = "Cash Tenders"
        '
        'spdInput
        '
        Me.spdInput.About = "3.0.2004.2005"
        Me.spdInput.AccessibleDescription = ""
        Me.spdInput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdInput.EditModeReplace = True
        Me.spdInput.Location = New System.Drawing.Point(351, 19)
        Me.spdInput.Name = "spdInput"
        Me.spdInput.Size = New System.Drawing.Size(376, 325)
        Me.spdInput.TabIndex = 1
        Me.spdInput.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance4.BackColor = System.Drawing.SystemColors.Info
        TipAppearance4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance4.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdInput.TextTipAppearance = TipAppearance4
        Me.spdInput.ActiveSheetIndex = -1
        '
        'SafeMaintenanceWithBankingBagsAndComments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1096, 784)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblMiscellaneousProperty)
        Me.Controls.Add(Me.GroupBoxCash)
        Me.Controls.Add(Me.GroupBoxBanking)
        Me.Controls.Add(Me.txtSafeComments)
        Me.Controls.Add(Me.lblSafeComments)
        Me.Controls.Add(Me.txtGiftTokenSealNumber)
        Me.Controls.Add(Me.lblGiftTokenSealNumber)
        Me.Controls.Add(Me.txtMiscellaneousProperty)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnSafeCheckReport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "SafeMaintenanceWithBankingBagsAndComments"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Safe Maintenance"
        Me.GroupBoxBanking.ResumeLayout(False)
        Me.GroupBoxBanking.PerformLayout()
        CType(Me.spdBankingBagList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxCash.ResumeLayout(False)
        CType(Me.spdInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnSafeCheckReport As System.Windows.Forms.Button
    Friend WithEvents lblMiscellaneousProperty As System.Windows.Forms.Label
    Friend WithEvents txtMiscellaneousProperty As System.Windows.Forms.TextBox
    Friend WithEvents txtGiftTokenSealNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblGiftTokenSealNumber As System.Windows.Forms.Label
    Friend WithEvents txtSafeComments As System.Windows.Forms.TextBox
    Friend WithEvents lblSafeComments As System.Windows.Forms.Label
    Friend WithEvents GroupBoxBanking As System.Windows.Forms.GroupBox
    Friend WithEvents btnAddBankingBag As System.Windows.Forms.Button
    Private WithEvents spdBankingBagList As FarPoint.Win.Spread.FpSpread
    Friend WithEvents GroupBoxCash As System.Windows.Forms.GroupBox
    Private WithEvents spdInput As FarPoint.Win.Spread.FpSpread
    Friend WithEvents UserControlScan1 As BarCodeScanner.UserControlScan
    Friend WithEvents txtSeal As System.Windows.Forms.MaskedTextBox
End Class
