﻿Public Class FloatedPickup

    Private mcolList As FloatedPickupListCollection
    Private mSelected As FloatedPickupList

    Private mstrAccountingModel As String
    Private mintTodayPeriodID As Integer
    Private mintBankingPeriodID As Integer
    Private mdtmBankingPeriodDate As Date
    Private mintFirstUserID As Integer
    Private mstrCurrencyID As String
    Private mstrCurrencySymbol As String
    Private mstrStoreID As String
    Private mstrStoreName As String

    Private mblnDirtyData As Boolean = False

    Public Event DataChange()

    Private WithEvents frmPickupEntry As PickupEntry
    Private WithEvents frmFloatReAssign As FloatReAssign

    Friend _FloatedPickupVM As IFloatedPickupVM

    Public Sub New(ByVal strAccountingModel As String, ByVal intFirstUserID As Integer, ByVal intTodayPeriodID As Integer, ByVal intBankingPeriodID As Integer, _
                   ByVal dtmBankingPeriodDate As Date, ByVal strCurrencyID As String, ByVal strCurrencySymbol As String, _
                   ByVal strStoreID As String, ByVal strStoreName As String)
        InitializeComponent()

        mstrAccountingModel = strAccountingModel
        mintFirstUserID = intFirstUserID
        mintTodayPeriodID = intTodayPeriodID
        mintBankingPeriodID = intBankingPeriodID
        mdtmBankingPeriodDate = dtmBankingPeriodDate
        mstrCurrencyID = strCurrencyID
        mstrCurrencySymbol = strCurrencySymbol
        mstrStoreID = strStoreID
        mstrStoreName = strStoreName
    End Sub

    Private Sub FloatedPickup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Cursor = Cursors.WaitCursor
            Me.Text = My.Resources.Screen.ScreenTitleFloatedPickup
            _FloatedPickupVM = (New FloatedPickupVMFactory).GetImplementation
            With _FloatedPickupVM
                .SetFloatedPickupGridControl(spdDisplay)
                .SetFloatedPickupForm(Me)
                .FloatedPickupGridFormat()
                .FloatedPickupFormFormat()
                mcolList = .GetFloatedPickupList(mintBankingPeriodID)
                .FloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub FloatedPickup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.F1 : btnCompletePickup.PerformClick()
                Case Keys.F2 : btnReAssignFloat.PerformClick()
                Case Keys.F8 : btnCashierReport.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub spdDisplay_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles spdDisplay.SelectionChanged
        Try
            Dim FP As FarPoint.Win.Spread.FpSpread
            Dim intCashierID As Integer

            FP = CType(sender, FarPoint.Win.Spread.FpSpread)

            If FP.ActiveSheet.ActiveRow Is Nothing Then Exit Sub 'grid is emptyy

            intCashierID = CType(FP.ActiveSheet.ActiveRow.Tag, Integer)

            mSelected = New FloatedPickupList
            mSelected = mcolList.SelectedCashier(intCashierID)

            ButtonAvailability(mSelected)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#Region "Button Event Handlers"

    Private Sub btnCompletePickup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompletePickup.Click
        Try
            With mSelected
                frmPickupEntry = New PickupEntry(mstrAccountingModel, mintFirstUserID, mintTodayPeriodID, mintBankingPeriodID, mstrCurrencyID, mstrCurrencySymbol, _
                                                 PickupEntry.PickupModel.Floated, .CashierID, .CashierUserName, .CashierEmployeeCode, .StartFloatValue, .StartFloatSealNumber)
            End With

            frmPickupEntry.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnReAssignFloat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReAssignFloat.Click
        Try
            frmFloatReAssign = New FloatReAssign(mintBankingPeriodID, mSelected.StartFloatID)

            frmFloatReAssign.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnCashierReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCashierReport.Click
        Try
            Dim frmReport As New CashierReport(mintBankingPeriodID, mdtmBankingPeriodDate, mSelected.CashierID, mstrStoreID, mstrStoreName)

            frmReport.ShowDialog(Me)
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            If mblnDirtyData = True Then RaiseEvent DataChange()
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

#End Region

#Region "Child Form Events"

    Private Sub PickupEntryDataChange() Handles frmPickupEntry.DataChange
        Try
            Cursor = Cursors.WaitCursor
            With _FloatedPickupVM
                mcolList = .GetFloatedPickupList(mintBankingPeriodID)
                .FloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)

            'main grid needs to be updated
            mblnDirtyData = True
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub frmFloatReAssign_DataChange() Handles frmFloatReAssign.DataChange
        Try
            Cursor = Cursors.WaitCursor

            With _FloatedPickupVM
                mcolList = .GetFloatedPickupList(mintBankingPeriodID)
                .FloatedPickupGridPopulate(mcolList)
            End With
            DisplayGridSetActiveRow(0)

            'main grid needs to be updated
            mblnDirtyData = True
        Catch ex As Exception
            ErrorHandler(ex)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ButtonAvailability(ByRef objSelected As FloatedPickupList)
        EnableDisableButtons(True)

        SetCompletePickupAvailability(objSelected)

        'float assigned but has a sale
        If objSelected.SaleTaken = True Then btnReAssignFloat.Enabled = False
        'pickup not created
        If objSelected.PickupID.HasValue = False Then btnCashierReport.Enabled = False

        ReAssignButtonAvailability()
    End Sub

    Private Sub EnableDisableButtons(ByVal blnState As Boolean)
        btnCompletePickup.Enabled = blnState
        btnReAssignFloat.Enabled = blnState
        btnCashierReport.Enabled = blnState
    End Sub

    Friend Sub SetCompletePickupAvailability(ByRef Selected As FloatedPickupList)

        With btnCompletePickup
            .Enabled = GetCompletePickupAvailability(Selected, .Enabled)
        End With
    End Sub

    Friend Overridable Function GetCompletePickupAvailability(ByRef Selected As FloatedPickupList, ByVal CurrentAvailability As Boolean) As Boolean
        Dim CompletePickupButtonAvailablity As ICompletePickupAvailability

        CompletePickupButtonAvailablity = (New CompletePickupAvailabilityImplementationFactory).GetImplementation
        Return CompletePickupButtonAvailablity.IsAvailable(Selected, CurrentAvailability, BankingPeriodDateIsToday(mdtmBankingPeriodDate))
    End Function

    Friend Sub ReAssignButtonAvailability()

        Dim UI As IFloatedPickupUI

        UI = (New FloatedPickupUIFactory).GetImplementation

        btnReAssignFloat.Enabled = UI.ReassignButtonActive
        btnReAssignFloat.Visible = UI.ReassignButtonActive

    End Sub

    Friend Function BankingPeriodDateIsToday(ByVal BankingPeriodDate As Date) As Boolean

        Return DateDiff(DateInterval.Day, BankingPeriodDate, Now()) = 0
    End Function

#End Region

#Region "Private Procedures And Functions - Populate And Format Grids"

    Private Sub DisplayGridSetActiveRow(ByVal intRowID As Integer)
        EnableDisableButtons(False)
        With spdDisplay
            .Focus()
            .ActiveSheet.SetActiveCell(intRowID, 0)
            .ActiveSheet.AddSelection(intRowID, 0, 1, 1)
        End With

        spdDisplay_SelectionChanged(spdDisplay, Nothing)
    End Sub
#End Region
End Class