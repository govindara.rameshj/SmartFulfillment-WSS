﻿Public Class BankingBagComment

    Private Sub BankingBagComment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = My.Resources.Screen.SeenTitleBankingBagComment
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub BankingBagComment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            e.Handled = True
            Select Case e.KeyData
                Case Keys.Enter : btnSave.PerformClick()
                Case Keys.F5 : btnSave.PerformClick()
                Case Keys.F12 : btnExit.PerformClick()
                Case Else : e.Handled = False
            End Select
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.OK
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            FindForm.Close()
        Catch ex As Exception
            ErrorHandler(ex)
        End Try
    End Sub

End Class