﻿Public Module Library

    Public Sub ErrorHandler(ByRef Ex As Exception)
        MessageBox.Show(Ex.Source & vbLf & Ex.Message, My.Resources.Screen.ErrorMessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Sub InformationMessageBox(ByVal strMessage As String)
        MessageBox.Show(strMessage, My.Resources.Screen.InformationMessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Function QuestionMessageBox(ByVal strMessage As String) As Windows.Forms.DialogResult
        QuestionMessageBox = MessageBox.Show(strMessage, My.Resources.Screen.QuestionMessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
    End Function

    Public Function IsMultiple(ByVal decValue As Decimal, ByVal decMultiple As Decimal) As Boolean
        Return (decValue Mod decMultiple = 0)
    End Function

    Friend Class UserAuthority

        Friend _Login As UserAuthorise
        Friend _PrimaryWitnessUserID As Integer = 0
        Friend _ManagerUserID As Integer

#Region "Properties"

        Public ReadOnly Property PrimaryWitnessUserID() As Integer
            Get
                Return _PrimaryWitnessUserID
            End Get
        End Property

        Friend Sub SetLoggedOnUserID(ByVal intFirstUserID As Integer)
            _ManagerUserID = intFirstUserID
        End Sub

#End Region

#Region "Public Interface"

        Public Function IsPrimaryWitnessValid(ByVal FirstUserID As Integer) As Boolean

            SetLoggedOnUserID(FirstUserID)

            If Not WitnessSecurityIsValid() Then

                InformationMessageBox(My.Resources.InformationMessages.AuthorisationValidSecondCheckFailed)
                Return False

            End If

            If Not UsersAreDifferent() Then

                InformationMessageBox(My.Resources.InformationMessages.AuthorisationFirstAndSecondUserIDNotDifferent)
                Return False

            End If

            Return True

        End Function

#End Region

#Region "Private Functions & Subs"

        Friend Shared Function CreateLoginDialog() As UserAuthorise
            Return New UserAuthorise(LoginLevel.User, PasswordMode.Standard)
        End Function

        Friend Function UserSecurityValidated() As Boolean
            Return _Login.ShowDialog() = DialogResult.OK
        End Function

        Friend Function WitnessSecurityIsValid() As Boolean
            _Login = CreateLoginDialog()

            If UserSecurityValidated() Then

                _PrimaryWitnessUserID = _Login.UserID
                Return True

            End If

        End Function

        Friend Function UsersAreDifferent() As Boolean

            Return _ManagerUserID <> _PrimaryWitnessUserID

        End Function

#End Region

    End Class

    Public Function AuthoriseCodeSecurity(ByVal blnManagerCheck As Boolean, ByVal intFirstUserID As Integer, ByRef SecondCheckUserID As Integer) As Boolean
        Dim Login As UserAuthorise

        Dim enumLoginLevel As LoginLevel

        Select Case blnManagerCheck
            Case True
                enumLoginLevel = LoginLevel.Manager
            Case False
                'enumLoginLevel = LoginLevel.User
                enumLoginLevel = LoginLevel.Supervisor
        End Select

        Login = New UserAuthorise(enumLoginLevel, PasswordMode.Supervisor)
        If Login.ShowDialog() = DialogResult.OK Then
            If Login.UserID = intFirstUserID Then
                Dim Factory As IFloatAssignAuthorise = (New FloatAssignAuthoriseFactory).FactoryGet()
                If Factory.FloatAssignAuthorise(intFirstUserID, SecondCheckUserID) = False Then
                    InformationMessageBox(My.Resources.InformationMessages.AuthorisationFirstAndSecondUserIDNotDifferent)
                    Return False
                End If
            End If
            If blnManagerCheck = True And Login.IsManager = False Then
                InformationMessageBox(My.Resources.InformationMessages.AuthorisationNotManager)
                Return False
            End If
            SecondCheckUserID = Login.UserID
            Return True
        Else
            InformationMessageBox(My.Resources.InformationMessages.AuthorisationValidSecondCheckFailed)
            Return False
        End If
    End Function

    Public Function BagType(ByVal strBagType As String) As String
        BagType = ""
        Select Case strBagType
            Case "F"
                BagType = "Float Bag"
            Case "P"
                BagType = "Pickup Bag"
            Case "B"
                BagType = "Banking Bag"
        End Select
    End Function

    Public Function FormattedCurrency(ByVal decValue As Decimal, ByVal strCurrencySymbol As String, ByVal intDecimalPlaces As Integer) As String
        Dim decRoundedValue As Decimal

        decRoundedValue = Math.Round(decValue, 2, MidpointRounding.ToEven)

        FormattedCurrency = If(decRoundedValue < 0, "-", "").ToString & strCurrencySymbol & _
                            If(decRoundedValue < 0, System.Decimal.Negate(decRoundedValue).ToString("C", SetCurrencyFormat), decRoundedValue.ToString("C", SetCurrencyFormat))
    End Function

    Public Sub PrintScreenReport(ByRef SelectedPeriod As Period, ByVal strStoreID As String, ByVal strStoreName As String)
        Dim spd As New FarPoint.Win.Spread.FpSpread
        Dim FloatedCashierSheet As New SheetView
        Dim UnFloatedCashierSheet As New SheetView

        Dim colFloatedCashier As New FloatedCashierDisplayCollection
        Dim colUnFloatedCashier As New CashDropAndUnFloatedPickupDisplayCollection

        Dim intRowIndex As Integer = 0
        Dim intRowCount As Integer

        Dim Dialog As New PrintDialog

        SpreadGridScrollBar(spd, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        'format columns - floated
        SpreadSheetCustomise(FloatedCashierSheet, 12, Model.SelectionUnit.Row)
        SpreadColumnCustomise(FloatedCashierSheet, 0, My.Resources.ScreenColumns.MainFloatedCashierGridColumn1, 90, False)                                   'Screen Column - Float Checked
        SpreadColumnCustomise(FloatedCashierSheet, 1, My.Resources.ScreenColumns.MainFloatedCashierGridColumn2, gcintColumnWidthMoney, False)                'Screen Column - Float Value
        SpreadColumnCustomise(FloatedCashierSheet, 2, My.Resources.ScreenColumns.MainFloatedCashierGridColumn3, gcintColumnWidthSeal, False)                 'Screen Column - Float Seal
        SpreadColumnCustomise(FloatedCashierSheet, 3, My.Resources.ScreenColumns.MainFloatedCashierGridColumn4, gcintColumnWidthUser, False)                 'Screen Column - Assigned To
        SpreadColumnCustomise(FloatedCashierSheet, 4, My.Resources.ScreenColumns.MainFloatedCashierGridColumn5, gcintColumnWidthUser, False)                 'Screen Column - Assigned By
        SpreadColumnCustomise(FloatedCashierSheet, 5, My.Resources.ScreenColumns.MainFloatedCashierGridColumn6, gcintColumnWidthMoney, False)                'Screen Column - Till Variance
        SpreadColumnCustomise(FloatedCashierSheet, 6, My.Resources.ScreenColumns.MainFloatedCashierGridColumn7, gcintColumnWidthSealWide, False)             'Screen Column - Float Seal (E.O.S)
        SpreadColumnCustomise(FloatedCashierSheet, 7, My.Resources.ScreenColumns.MainFloatedCashierGridColumn8, gcintColumnWidthSeal, False)                 'Screen Column - Pickup Seal
        SpreadColumnCustomise(FloatedCashierSheet, 8, My.Resources.ScreenColumns.MainFloatedCashierGridColumn9, gcintColumnWidthUser, False)                 'Screen Column - Cashier ID
        SpreadColumnCustomise(FloatedCashierSheet, 9, My.Resources.ScreenColumns.MainFloatedCashierGridColumn10, gcintColumnWidthUser, False)                'Screen Column - Supervisor ID
        SpreadColumnCustomise(FloatedCashierSheet, 10, My.Resources.ScreenColumns.MainFloatedCashierGridColumn11, gcintColumnWidthEodBagCheck + 40, False)   'Screen Column - E.O.D Bag Check
        SpreadColumnCustomise(FloatedCashierSheet, 11, My.Resources.ScreenColumns.MainFloatedCashierGridColumn12, gcintColumnWidthComments + 20, False)      'Screen Column - Comments

        SpreadColumnMoney(FloatedCashierSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(FloatedCashierSheet, 5, True, gstrFarPointMoneyNullDisplay)
        SpreadSheetCustomiseHeaderAlignLeft(FloatedCashierSheet, 11)

        SpreadGridSheetAdd(spd, FloatedCashierSheet, True, String.Empty)

        'load data - floated
        colFloatedCashier.LoadData(SelectedPeriod.PeriodID)

        SpreadSheetClearDown(FloatedCashierSheet)
        For Each obj As FloatedCashierDisplay In colFloatedCashier
            SpreadRowAdd(FloatedCashierSheet, intRowIndex)
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 0, obj.StartFloatChecked)                                                                'Screen Column - Float Checked
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 1, obj.StartFloatValue)                                                                  'Screen Column - Float Value
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 2, obj.StartFloatSealNumber)                                                             'Screen Column - Float Seal
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 3, obj.StartFloatAssignedToUserID.ToString & " " & obj.StartFloatAssignedToUserName)     'Screen Column - Assigned To
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 4, obj.StartFloatAssignedByUserID.ToString & " " & obj.StartFloatAssignedByUserName)     'Screen Column - Assigned By
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 5, obj.TillVariance)                                                                     'Screen Column - Till Variance
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 6, obj.NewFloatSealNumber)                                                               'Screen Column - Float Seal (E.O.S)
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 7, obj.PickupSealNumber)                                                                 'Screen Column - Pickup Seal
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 8, obj.PickupAssignedByUserID.ToString & " " & obj.PickupAssignedByUserName)             'Screen Column - Supervisor ID
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 9, obj.PickupAssignedBySecondUserID.ToString & " " & obj.PickupAssignedBySecondUserName) 'Screen Column - Cashier ID
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 10, obj.EODBagCheck)                                                                     'Screen Column - E.O.D Bag Check
            SpreadCellValue(FloatedCashierSheet, intRowIndex, 11, obj.Comments)                                                                        'Screen Column - Comments
        Next
        intRowCount = FloatedCashierSheet.RowCount

        'empty row
        SpreadRowAdd(FloatedCashierSheet, intRowIndex)
        SpreadRowDrawLine(FloatedCashierSheet, intRowIndex, Color.Black, 1)

        'total row
        SpreadRowAdd(FloatedCashierSheet, intRowIndex)
        SpreadRowBold(spd, FloatedCashierSheet, intRowIndex)

        SpreadCellFormula(FloatedCashierSheet, intRowIndex, 2, intRowCount.ToString)
        SpreadCellFormula(FloatedCashierSheet, intRowIndex, 6, intRowCount.ToString)
        SpreadCellFormula(FloatedCashierSheet, intRowIndex, 5, "SUM(R1C:R" & intRowIndex.ToString & "C)")
        SpreadCellFormula(FloatedCashierSheet, intRowIndex, 7, intRowCount.ToString)

        CustomiseCacheControlLogSheetForPrint(FloatedCashierSheet, SelectedPeriod, strStoreID, strStoreName, "Floated Cashier(s)")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'format columns - unfloated
        SpreadSheetCustomise(UnFloatedCashierSheet, 7, Model.SelectionUnit.Row)
        SpreadColumnCustomise(UnFloatedCashierSheet, 0, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn1, gcintColumnWidthUser, False)              'Screen Column - Assigned To
        SpreadColumnCustomise(UnFloatedCashierSheet, 1, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn2, gcintColumnWidthMoney, False)             'Screen Column - Till Variance
        SpreadColumnCustomise(UnFloatedCashierSheet, 2, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn3, gcintColumnWidthSeal, False)              'Screen Column - Pickup Seal
        SpreadColumnCustomise(UnFloatedCashierSheet, 3, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn4, gcintColumnWidthUser, False)              'Screen Column - Cashier ID
        SpreadColumnCustomise(UnFloatedCashierSheet, 4, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn5, gcintColumnWidthUser, False)              'Screen Column - Supervisor ID
        SpreadColumnCustomise(UnFloatedCashierSheet, 5, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn6, gcintColumnWidthEodBagCheck + 40, False)  'Screen Column - E.O.D Bag Check
        SpreadColumnCustomise(UnFloatedCashierSheet, 6, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn7, gcintColumnWidthComments + 20, False)     'Screen Column - Comments

        SpreadColumnMoney(UnFloatedCashierSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnAlignLeft(UnFloatedCashierSheet, 6)

        SpreadGridSheetAdd(spd, UnFloatedCashierSheet, True, String.Empty)

        'load data - unfloated
        colUnFloatedCashier.LoadData(SelectedPeriod.PeriodID)

        SpreadSheetClearDown(UnFloatedCashierSheet)
        For Each obj As CashDropAndUnFloatedPickupDisplay In colUnFloatedCashier
            SpreadRowAdd(UnFloatedCashierSheet, intRowIndex)
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 0, obj.AssignedToUserID.ToString & " " & obj.AssignedToUserName)                                                                         'Screen Column - Float Checked
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 1, obj.TillVariance)                                                                       'Screen Column - Float Value
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 2, obj.PickupSealNumber)                                                                   'Screen Column - Float Seal
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 3, obj.PickupAssignedByUserID.ToString & " " & obj.PickupAssignedByUserName)               'Screen Column - Assigned To
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 4, obj.PickupAssignedBySecondUserID.ToString & " " & obj.PickupAssignedBySecondUserName)   'Screen Column - Assigned By
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 5, obj.EODBagCheck)                                                                        'Screen Column - Till Variance
            SpreadCellValue(UnFloatedCashierSheet, intRowIndex, 6, obj.Comments)                                                                           'Screen Column - Float Seal (E.O.S)
        Next
        intRowCount = UnFloatedCashierSheet.RowCount

        'empty row
        SpreadRowAdd(UnFloatedCashierSheet, intRowIndex)
        SpreadRowDrawLine(UnFloatedCashierSheet, intRowIndex, Color.Black, 1)

        'total row
        SpreadRowAdd(UnFloatedCashierSheet, intRowIndex)
        SpreadRowBold(spd, UnFloatedCashierSheet, intRowIndex)
        SpreadCellFormula(UnFloatedCashierSheet, intRowIndex, 1, "SUM(R1C:R" & intRowIndex.ToString & "C)")
        SpreadCellFormula(UnFloatedCashierSheet, intRowIndex, 2, intRowCount.ToString)

        CustomiseCacheControlLogSheetForPrint(UnFloatedCashierSheet, SelectedPeriod, strStoreID, strStoreName, "Un-Floated Cashier(s) / Design Consultants / Cash Drops")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim EndOfDayCheck As IEndOfDayCheckSummaryVM

        EndOfDayCheck = (New EndOfDayCheckSummaryVMFactory).GetImplementation

        EndOfDayCheck.SummaryPrint(spd, SelectedPeriod.PeriodID)

        CustomiseCacheControlLogSheetForPrint(EndOfDayCheck.SheetView, SelectedPeriod, strStoreID, strStoreName, " EOD Management Check")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Dialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            FloatedCashierSheet.PrintInfo.Printer = Dialog.PrinterSettings.PrinterName
            UnFloatedCashierSheet.PrintInfo.Printer = Dialog.PrinterSettings.PrinterName
            EndOfDayCheck.SheetView.PrintInfo.Printer = Dialog.PrinterSettings.PrinterName

            spd.PrintSheet(-1)
        End If
    End Sub

    Public Sub CustomiseSheetForPrint(ByRef PrintSheet As SheetView, _
                                       ByVal strStoreID As String, ByVal strStoreName As String, _
                                       ByVal strHeading As String, ByVal strSubheader As String, _
                                       ByVal printOrientation As PrintOrientation)
        With PrintSheet
            .PrintInfo.UseSmartPrint = True
            .PrintInfo.UseMax = False

            .PrintInfo.Margin.Left = 50
            .PrintInfo.Margin.Right = 50
            .PrintInfo.Margin.Top = 20
            .PrintInfo.Margin.Bottom = 20
            .PrintInfo.ShowBorder = False
            .PrintInfo.Orientation = printOrientation

            'With .PrintInfo
            '    .ZoomFactor = 0.5
            'End With

            .PrintInfo.Header = "/lStore: " & strStoreID & " " & strStoreName
            .PrintInfo.Header &= "/c/fz""14""" & strHeading
            .PrintInfo.Header &= "/n/c/fz""10""" & strSubheader
            .PrintInfo.Header &= "/n "

            .PrintInfo.Footer = "/lPrinted: " & Now
            .PrintInfo.Footer &= "/cPage /p of /pc"
            .PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        End With
    End Sub

    Private Sub CustomiseCacheControlLogSheetForPrint(ByRef PrintSheet As SheetView, ByRef SelectedPeriod As Period, _
                                       ByVal strStoreID As String, ByVal strStoreName As String, ByVal strHeading As String)

        CustomiseSheetForPrint(PrintSheet, strStoreID, strStoreName, _
                               "Cash Control Log Report - " & strHeading, _
                               "Banking Period : " & SelectedPeriod.PeriodID.ToString & " - " & SelectedPeriod.PeriodDate.ToShortDateString, _
                               PrintOrientation.Landscape)

    End Sub

    Public Function SetCurrencyFormat() As System.Globalization.NumberFormatInfo
        Dim CurrencyFormat As New System.Globalization.NumberFormatInfo

        CurrencyFormat.CurrencySymbol = ""

        SetCurrencyFormat = CurrencyFormat
    End Function

    Public Function SuggestedBankingAmount(ByVal decAmount As Decimal, ByVal BankingMultiple As Decimal, ByVal decDenominationID As Decimal) As Decimal

        SuggestedBankingAmount = 0

        If BankingMultiple >= decDenominationID AndAlso decAmount >= BankingMultiple Then

            SuggestedBankingAmount = BankingMultiple * Math.Floor(decAmount / BankingMultiple)

        End If

    End Function

#Region "FarPoint Spread"

#Region "Grid"

    Public Sub SpreadGridScrollBar(ByRef refGrid As FpSpread, ByVal VerticalScroll As ScrollBarPolicy, ByVal HorizontalScoll As ScrollBarPolicy)
        With refGrid
            .VerticalScrollBarPolicy = VerticalScroll
            .HorizontalScrollBarPolicy = HorizontalScoll
            .ScrollBarMaxAlign = False
        End With
    End Sub

    Public Sub SpreadGridSheetAdd(ByRef refGrid As FpSpread, ByRef refSheet As SheetView, ByVal blnHeaderBold As Boolean, ByVal strSheetName As String)
        With refGrid
            .Sheets.Add(refSheet)
            If blnHeaderBold = True Then refSheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(.Font, FontStyle.Bold)
        End With
        refSheet.SheetName = strSheetName
    End Sub

    Public Sub SpreadGridInputMaps(ByRef refGrid As FpSpread)
        Dim im As InputMap
        Dim imFocused As InputMap

        im = refGrid.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)

        imFocused = refGrid.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
    End Sub

    Public Sub SpreadGridDeactiveKey(ByRef refGrid As FpSpread, ByVal objKey As System.Windows.Forms.Keys)
        Dim IM As New FarPoint.Win.Spread.InputMap

        'deactivate key for cells being edited
        IM = refGrid.GetInputMap(FarPoint.Win.Spread.InputMapMode.WhenAncestorOfFocused)
        IM.Put(New FarPoint.Win.Spread.Keystroke(objKey, Keys.None), FarPoint.Win.Spread.SpreadActions.None)

        'deactivate key for cells not being edited
        IM = refGrid.GetInputMap(FarPoint.Win.Spread.InputMapMode.WhenFocused)
        IM.Put(New FarPoint.Win.Spread.Keystroke(objKey, Keys.None), FarPoint.Win.Spread.SpreadActions.None)
    End Sub

    Public Sub SpreadGridRepositionAndResize(ByRef refGrid As FpSpread, ByVal NewLeft As Nullable(Of Integer), ByVal NewTop As Nullable(Of Integer), ByVal NewWidth As Nullable(Of Integer), ByVal NewHeight As Nullable(Of Integer))

        If refGrid IsNot Nothing Then
            With refGrid
                If NewLeft.HasValue Then
                    If NewTop.HasValue Then
                        .Location = New System.Drawing.Point(NewLeft.Value, NewTop.Value)
                    Else
                        .Location = New System.Drawing.Point(NewLeft.Value, .Location.Y)
                    End If
                Else
                    If NewTop.HasValue Then
                        .Location = New System.Drawing.Point(.Location.X, NewTop.Value)
                    End If
                End If
                If NewWidth.HasValue Then
                    .Width = NewWidth.Value
                End If
                If NewHeight.HasValue Then
                    .Height = NewHeight.Value
                End If
            End With
        End If
    End Sub
#End Region

#Region "Sheet"

    Public Sub SpreadSheetCustomise(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal Selector As FarPoint.Win.Spread.Model.SelectionUnit)
        With refSheet
            .ReferenceStyle = Model.ReferenceStyle.R1C1
            .RowCount = 0
            .ColumnCount = intColumnIndex
            .ColumnHeader.RowCount = 1
            .RowHeader.ColumnCount = 0
            .ColumnHeaderVisible = True

            .GrayAreaBackColor = Drawing.Color.White
            .DefaultStyle.HorizontalAlignment = Spread.CellHorizontalAlignment.Center
            .DefaultStyle.VerticalAlignment = Spread.CellVerticalAlignment.Bottom
            .DefaultStyle.CellType = New CellType.TextCellType
            .DefaultStyle.Locked = True
            .SelectionBackColor = Drawing.Color.AliceBlue
            .SelectionUnit = Selector
        End With
    End Sub

    Public Sub SpreadSheetClearDown(ByRef refSheet As SheetView)
        refSheet.Rows.Remove(0, refSheet.RowCount)
    End Sub

    Public Sub SpreadSheetCustomiseHeader(ByRef refSheet As SheetView, ByVal intHeaderHeight As Integer)
        refSheet.ColumnHeader.Rows(0).Height = intHeaderHeight
    End Sub

    Public Sub SpreadSheetCustomiseHeaderAlignLeft(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer)
        refSheet.ColumnHeader.Cells(0, intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Left
    End Sub

    Public Sub SpreadSheetCustomiseFreezeLeadingColumns(ByRef refSheet As SheetView, ByVal intColumnsToFreeze As Integer)
        refSheet.FrozenColumnCount = intColumnsToFreeze
    End Sub

#End Region

#Region "Column"

    Public Sub SpreadColumnAdd(ByRef refSV As SheetView, ByRef intColumnID As Integer)
        With refSV
            .Columns.Add(.ColumnCount, 1)          'add one column to the end of the sheet
            intColumnID = .ColumnCount - 1         'return index of added row
        End With
    End Sub

    Public Sub SpreadColumnCustomise(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal strLabelName As String, _
                                     ByVal intWidth As System.Nullable(Of Integer), ByVal blnResizeColumn As Boolean)
        refSheet.Columns(intColumnIndex).Label = strLabelName
        refSheet.ColumnHeader.Columns(intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Left
        If intWidth.HasValue = True Then refSheet.Columns(intColumnIndex).Width = intWidth.Value
        refSheet.Columns(intColumnIndex).Resizable = blnResizeColumn
        refSheet.Columns(intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Left
    End Sub

    Public Sub SpreadColumnCustomise(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal strLabelName As String, _
                                     ByVal intWidth As System.Nullable(Of Integer), ByVal blnResizeColumn As Boolean, _
                                     ByVal NewHorizontalAlignment As CellHorizontalAlignment)

        With refSheet.Columns(intColumnIndex)
            .Label = strLabelName
            If intWidth.HasValue = True Then
                .Width = intWidth.Value
            End If
            '.Width = .GetPreferredWidth
            .Resizable = blnResizeColumn
            .HorizontalAlignment = NewHorizontalAlignment

        End With
    End Sub

    Public Sub SpreadColumnMoney(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal blnNegativeRed As Boolean, ByVal strNullDisplay As String)
        Dim oMoneyType As New CellType.NumberCellType

        With oMoneyType
            .DecimalPlaces = 2
            .LeadingZero = CellType.LeadingZero.UseRegional
            If blnNegativeRed = True Then .NegativeRed = True
            .NullDisplay = strNullDisplay
        End With
        refSheet.Columns(intColumnIndex).CellType = oMoneyType
        refSheet.Columns(intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Right
    End Sub

    Public Sub SpreadColumnNumber(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal blnNegativeRed As Boolean, ByVal strNullDisplay As String)
        Dim oIntegerType As New CellType.NumberCellType

        With oIntegerType
            .DecimalPlaces = 0
            If blnNegativeRed = True Then .NegativeRed = True
            .NullDisplay = strNullDisplay
        End With
        refSheet.Columns(intColumnIndex).CellType = oIntegerType
    End Sub

    Public Sub SpreadColumnAlignLeft(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer)
        refSheet.Columns(intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Left
    End Sub

    Public Sub SpreadColumnBackgroundColour(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal BackgroundColour As System.Drawing.Color)
        refSheet.Columns(intColumnIndex).BackColor = BackgroundColour
    End Sub

    Public Sub SpreadColumnEditable(ByRef refSheet As SheetView, ByVal intColumnIndex As Integer, ByVal blnEdit As Boolean)
        refSheet.Columns(intColumnIndex).Locked = Not blnEdit
    End Sub

    Public Sub SpreadColumnTagValue(ByRef refSV As SheetView, ByVal intColumnIndex As Integer, ByVal strTag As String)
        refSV.Columns(intColumnIndex).Tag = strTag
    End Sub

    Public Sub SpreadColumnSetWidth(ByRef refSV As SheetView, ByVal intColumnIndex As Integer, ByVal columnWidthValue As Integer)
        refSV.Columns(intColumnIndex).Width = columnWidthValue
    End Sub

    Public Sub SpreadColumnSetResizable(ByRef refSV As SheetView, ByVal intColumnIndex As Integer, ByVal resizable As Boolean)
        refSV.Columns(intColumnIndex).Resizable = resizable

    End Sub

#End Region

#Region "Row"

    Public Sub SpreadRowAdd(ByRef refSV As SheetView, ByRef intRowID As Integer)
        With refSV
            .Rows.Add(.RowCount, 1)          'add one row to the end of the sheet
            intRowID = .RowCount - 1         'return index of added row
        End With
    End Sub

    Public Sub SpreadRowAdd(ByRef refSV As SheetView, ByRef RowID As Integer, ByVal NumberOfRows As Integer)
        With refSV
            .Rows.Add(.RowCount, NumberOfRows) 'add one row to the end of the sheet
            RowID = .RowCount - 1           'return index of added row
        End With
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal blnValue As Boolean)
        refSV.Rows(intRowIndex).Tag = blnValue
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intValue As Integer)
        refSV.Rows(intRowIndex).Tag = intValue
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal decValue As Decimal)
        refSV.Rows(intRowIndex).Tag = decValue
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal strValue As String)
        refSV.Rows(intRowIndex).Tag = strValue
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal objValue As TenderDenominationList)
        refSV.Rows(intRowIndex).Tag = objValue
    End Sub

    Public Sub SpreadRowTagValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal objValue As NewBanking.Core.SafeDenomination)
        refSV.Rows(intRowIndex).Tag = objValue
    End Sub

    Public Sub SpreadRowDrawLine(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal LineColour As System.Drawing.Color, ByVal intLineThickness As Integer)
        refSV.Rows(intRowIndex).Border = New LineBorder(LineColour, intLineThickness, False, False, False, True)
    End Sub

    Public Sub SpreadRowBold(ByRef refGrid As FpSpread, ByRef refSV As SheetView, ByVal intRowIndex As Integer)
        'grid font used, when row first created font does not exist
        refSV.Rows(intRowIndex).Font = New Drawing.Font(refGrid.Font, FontStyle.Bold)
    End Sub

    Public Sub SpreadRowColour(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal RowColour As System.Drawing.Color)
        refSV.Rows(intRowIndex).ForeColor = RowColour
    End Sub

    Public Sub SpreadRowRemoveFocus(ByRef refSV As SheetView, ByVal intRowIndex As Integer)
        refSV.Rows(intRowIndex).CanFocus = False
    End Sub

    Public Sub SpreadRowHeight(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intHeight As Integer)
        refSV.Rows(intRowIndex).Height = intHeight
    End Sub

    Public Sub SpreadRowVerticalAlignment(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal VerticalAlignment As FarPoint.Win.Spread.CellVerticalAlignment)
        refSV.Rows(intRowIndex).VerticalAlignment = VerticalAlignment
    End Sub

    Public Sub SpreadRowLocked(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal blnLocked As Boolean)
        refSV.Rows(intRowIndex).Locked = blnLocked
    End Sub

    Public Sub SpreadRowMergeColumns(ByRef refSV As SheetView, ByVal RowIndex As Integer, ByVal StartColumn As Integer, ByVal EndColumn As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If StartColumn < EndColumn Then
                        If .ColumnCount > 0 And EndColumn < .ColumnCount Then
                            With refSV
                                .AddSpanCell(RowIndex, StartColumn, 1, (EndColumn - StartColumn) + 1)
                            End With
                        End If
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadRowSetHeightToFitText(ByRef refSV As SheetView, ByVal RowIndex As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    Dim RowHeight As Single

                    With .Rows(RowIndex)
                        RowHeight = .GetPreferredHeight()
                        .Height = RowHeight
                    End With
                End If
            End With
        End If
    End Sub

#End Region

#Region "Cell"

    Public Sub SpreadCellMakeActive(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer)
        refSV.SetActiveCell(intRowIndex, intColumnIndex)
    End Sub

    Public Sub SpreadCellValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal blnValue As Boolean)
        refSV.Cells(intRowIndex, intColumnIndex).Value = blnValue
    End Sub

    Public Sub SpreadCellValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal intValue As Integer)
        refSV.Cells(intRowIndex, intColumnIndex).Value = intValue
    End Sub

    Public Sub SpreadCellValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal decValue As Decimal)
        refSV.Cells(intRowIndex, intColumnIndex).Value = decValue
    End Sub

    Public Sub SpreadCellValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal decValue As System.Nullable(Of Decimal))
        refSV.Cells(intRowIndex, intColumnIndex).Value = decValue
    End Sub

    Public Sub SpreadCellButton(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal blnChecked As Boolean)
        Dim ckbxCell As New FarPoint.Win.Spread.CellType.CheckBoxCellType()

        With ckbxCell
            .ThreeState = False
            .TextTrue = ""
            .TextFalse = ""
        End With
        refSV.Cells(intRowIndex, intColumnIndex).CellType = ckbxCell
        refSV.Cells(intRowIndex, intColumnIndex).Value = blnChecked
        refSV.Cells(intRowIndex, intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Center
        refSV.Cells(intRowIndex, intColumnIndex).VerticalAlignment = CellVerticalAlignment.Center
    End Sub

    Public Sub SpreadCellValue(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal strValue As String)
        refSV.Cells(intRowIndex, intColumnIndex).Value = strValue
    End Sub

    Public Sub SpreadCellLocked(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal blnLocked As Boolean)
        refSV.Cells(intRowIndex, intColumnIndex).Locked = blnLocked
    End Sub

    Public Sub SpreadCellRed(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer)
        refSV.Cells(intRowIndex, intColumnIndex).ForeColor = Color.Red
    End Sub

    Public Sub SpreadCellBlack(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer)
        refSV.Cells(intRowIndex, intColumnIndex).ForeColor = Color.Black
    End Sub

    Public Sub SpreadCellColour(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal NewColour As System.Drawing.Color)
        refSV.Cells(intRowIndex, intColumnIndex).ForeColor = NewColour
    End Sub

    Public Sub SpreadCellFormula(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal strFormula As String)
        refSV.Cells(intRowIndex, intColumnIndex).Formula = strFormula
    End Sub

    Public Sub SpreadCellFormulaAppend(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer, ByVal strFormula As String)
        With refSV.Cells(intRowIndex, intColumnIndex)
            .Formula += strFormula
        End With
    End Sub

    Public Sub SpreadCellSetAsTextFormat(ByRef refSV As SheetView, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If .ColumnCount > 0 And ColumnIndex < .ColumnCount Then
                        With refSV.Cells(RowIndex, ColumnIndex)
                            .CellType = New CellType.TextCellType
                            .HorizontalAlignment = CellHorizontalAlignment.Left
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadCellSetAsBold(ByRef refGrid As FpSpread, ByRef refSV As SheetView, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If .ColumnCount > 0 And ColumnIndex < .ColumnCount Then
                        With refSV.Cells(RowIndex, ColumnIndex)
                            .Font = New Drawing.Font(refGrid.Font, FontStyle.Bold)
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadCellSetVerticalAlignment(ByRef refSV As SheetView, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer, ByVal VerticalAlignment As CellVerticalAlignment)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If .ColumnCount > 0 And ColumnIndex < .ColumnCount Then
                        With refSV.Cells(RowIndex, ColumnIndex)
                            .VerticalAlignment = VerticalAlignment
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadCellSetAsMultiLine(ByRef refSV As SheetView, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer, ByVal WordWrapAsWell As Boolean)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And RowIndex < .RowCount Then
                    If .ColumnCount > 0 And ColumnIndex < .ColumnCount Then
                        Dim NewCell As New FarPoint.Win.Spread.CellType.RichTextCellType

                        With NewCell
                            .Multiline = True
                            .WordWrap = WordWrapAsWell
                        End With
                        With refSV.Cells(RowIndex, ColumnIndex)
                            .CellType = NewCell
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadCellMergeCells(ByRef refSV As SheetView, ByVal StartRow As Integer, ByVal RowCount As Integer, ByVal StartColumn As Integer, ByVal ColumnCount As Integer)

        If refSV IsNot Nothing Then
            With refSV
                If .RowCount > 0 And StartRow + RowCount <= .RowCount Then
                    If .ColumnCount > 0 And StartColumn + ColumnCount <= .ColumnCount Then
                        With refSV
                            .AddSpanCell(StartRow, StartColumn, RowCount, ColumnCount)
                        End With
                    End If
                End If
            End With
        End If
    End Sub

    Public Sub SpreadCellAlignCenter(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer)

        refSV.Cells(intRowIndex, intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Center

    End Sub

    Public Sub SpreadCellAlignRight(ByRef refSV As SheetView, ByVal intRowIndex As Integer, ByVal intColumnIndex As Integer)

        refSV.Cells(intRowIndex, intColumnIndex).HorizontalAlignment = CellHorizontalAlignment.Right

    End Sub

#End Region

#End Region

End Module
