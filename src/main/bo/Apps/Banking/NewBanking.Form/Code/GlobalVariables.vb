﻿Public Module GlobalVariables

#Region "Existing Banking"

#End Region

#Region "New Banking"

    Public gstrFarPointMoneyNullDisplay As String = ""
    Public gstrFarPointNumberNullDisplay As String = "---"

#Region "Screen Column Constant"

    Public Const gcintColumnWidthMoney As Integer = 80
    Public Const gcintColumnWidthMoneyWide As Integer = 120
    Public Const gcintColumnWidthSeal As Integer = 100
    Public Const gcintColumnWidthSealWide As Integer = 120
    Public Const gcintColumnWidthUser As Integer = 120
    Public Const gcintColumnWidthUserWide As Integer = 180

    Public Const gcintColumnWidthEodBagCheck As Integer = 120
    Public Const gcintColumnWidthComments As Integer = 200
    Public Const gcintColumnWidthCommentsWide As Integer = 240
    Public Const gcintColumnWidthCommentsWider As Integer = 400
    Public Const gcintColumnWidthCommentsWidest As Integer = 662

    Public Const gcintColumnWidthDefault As Integer = 100
    Public Const gcintColumnWidthDefaultWide As Integer = 140

    Public Const gcintColumnWidthCashTender As Integer = 100
    Public Const gcintColumnWidthAllTenders As Integer = 140
    Public Const gcintColumnWidthAllTendersWide As Integer = 170
    Public Const gcintColumnWidthPickup As Integer = 100
    Public Const gcintColumnWidthPickupWide As Integer = 150

    Public Const gintHeaderHeightDouble As Integer = 40
    Public Const gintHeaderHeightTriple As Integer = 60

#End Region

#End Region

End Module
