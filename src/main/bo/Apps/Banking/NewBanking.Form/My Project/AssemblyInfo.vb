﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.String

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NewBanking.Form")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TP-Wickes")> 
<Assembly: AssemblyProduct("NewBanking.Form")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ca7f49d6-516f-4f0d-ad6a-697e0f4c2172")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.4.0.0")> 
<Assembly: AssemblyFileVersion("3.4.0.0")> 
