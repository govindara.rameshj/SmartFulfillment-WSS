﻿Public Class MainVMWithoutComments
    Implements IMainVM

    Friend _FloatedCashierGrid As FpSpread
    Friend _CashDropAndUnFloatedPickupGrid As FpSpread

    Public Function GetFloatedCashierDisplayList(ByVal SelectbankingPeriodID As Integer) As Core.FloatedCashierDisplayCollection Implements IMainVM.GetFloatedCashierDisplayList

        GetFloatedCashierDisplayList = New FloatedCashierDisplayCollection
        GetFloatedCashierDisplayList.LoadData(SelectbankingPeriodID)
    End Function

    Public Function GetCashDropAndUnFloatedPickupDisplayList(ByVal SelectbankingPeriodID As Integer) As Core.CashDropAndUnFloatedPickupDisplayCollection Implements IMainVM.GetCashDropAndUnFloatedPickupDisplayList

        GetCashDropAndUnFloatedPickupDisplayList = New CashDropAndUnFloatedPickupDisplayCollection
        GetCashDropAndUnFloatedPickupDisplayList.LoadData(SelectbankingPeriodID)
    End Function

    Public Sub CashDropAndUnFloatedPickupGridFormat() Implements IMainVM.CashDropAndUnFloatedPickupGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 7, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn1, gcintColumnWidthUser, False)        'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn2, gcintColumnWidthMoney, False)       'Screen Column - Till Variance
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn3, gcintColumnWidthSeal, False)        'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn4, gcintColumnWidthUser, False)        'Screen Column - Cashier ID
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn5, gcintColumnWidthUser, False)        'Screen Column - Supervisor ID
        SpreadColumnCustomise(NewSheet, 5, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn6, gcintColumnWidthEodBagCheck, False) 'Screen Column - E.O.D Bag Check
        SpreadColumnCustomise(NewSheet, 6, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn7, gcintColumnWidthComments, False)    'Screen Column - Comments

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)

        SpreadColumnAlignLeft(NewSheet, 6)

        SpreadGridSheetAdd(_CashDropAndUnFloatedPickupGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_CashDropAndUnFloatedPickupGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)
    End Sub

    Public Sub CashDropUnFloatedPickupGridPopulate(ByVal CashDropAndUnFloatedPickupDisplayList As CashDropAndUnFloatedPickupDisplayCollection) Implements IMainVM.CashDropUnFloatedPickupGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0


        CurrentSheet = _CashDropAndUnFloatedPickupGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As CashDropAndUnFloatedPickupDisplay In CashDropAndUnFloatedPickupDisplayList
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.AssignedToUserName)               'Screen Column - Float Checked
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.TillVariance)                     'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.PickupSealNumber)                 'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.PickupAssignedByUserName)         'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 4, obj.PickupAssignedBySecondUserName)   'Screen Column - Assigned By
            SpreadCellValue(CurrentSheet, intRowIndex, 5, obj.EODBagCheck)                      'Screen Column - Till Variance
            SpreadCellValue(CurrentSheet, intRowIndex, 6, obj.Comments)                         'Screen Column - Float Seal (E.O.S)
        Next
    End Sub

    Public Sub FloatedCashierGridFormat() Implements IMainVM.FloatedCashierGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 12, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.MainFloatedCashierGridColumn1, 90, False)                             'Screen Column - Float Checked
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.MainFloatedCashierGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - Float Value
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.MainFloatedCashierGridColumn3, gcintColumnWidthSeal, False)           'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.MainFloatedCashierGridColumn4, gcintColumnWidthUser, False)           'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.MainFloatedCashierGridColumn5, gcintColumnWidthUser, False)           'Screen Column - Assigned By
        SpreadColumnCustomise(NewSheet, 5, My.Resources.ScreenColumns.MainFloatedCashierGridColumn6, gcintColumnWidthMoney, False)          'Screen Column - Till Variance
        SpreadColumnCustomise(NewSheet, 6, My.Resources.ScreenColumns.MainFloatedCashierGridColumn7, gcintColumnWidthSealWide, False)       'Screen Column - Float Seal (E.O.S)
        SpreadColumnCustomise(NewSheet, 7, My.Resources.ScreenColumns.MainFloatedCashierGridColumn8, gcintColumnWidthSeal, False)           'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 8, My.Resources.ScreenColumns.MainFloatedCashierGridColumn9, gcintColumnWidthUser, False)           'Screen Column - Cashier ID
        SpreadColumnCustomise(NewSheet, 9, My.Resources.ScreenColumns.MainFloatedCashierGridColumn10, gcintColumnWidthUser, False)          'Screen Column - Supervisor ID
        SpreadColumnCustomise(NewSheet, 10, My.Resources.ScreenColumns.MainFloatedCashierGridColumn11, gcintColumnWidthEodBagCheck, False)  'Screen Column - E.O.D Bag Check
        SpreadColumnCustomise(NewSheet, 11, My.Resources.ScreenColumns.MainFloatedCashierGridColumn12, gcintColumnWidthComments, False)     'Screen Column - Comments

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 5, True, gstrFarPointMoneyNullDisplay)

        SpreadSheetCustomiseHeaderAlignLeft(NewSheet, 11)

        SpreadGridSheetAdd(_FloatedCashierGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatedCashierGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)
    End Sub

    Public Sub FloatedCashierGridPopulate(ByVal FloatedCashierDisplayList As FloatedCashierDisplayCollection) Implements IMainVM.FloatedCashierGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _FloatedCashierGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatedCashierDisplay In FloatedCashierDisplayList
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.StartFloatChecked)              'Screen Column - Float Checked
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.StartFloatValue)                'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.StartFloatSealNumber)           'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.StartFloatAssignedToUserName)   'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 4, obj.StartFloatAssignedByUserName)   'Screen Column - Assigned By
            SpreadCellValue(CurrentSheet, intRowIndex, 5, obj.TillVariance)                   'Screen Column - Till Variance
            SpreadCellValue(CurrentSheet, intRowIndex, 6, obj.NewFloatSealNumber)             'Screen Column - Float Seal (E.O.S)
            SpreadCellValue(CurrentSheet, intRowIndex, 7, obj.PickupSealNumber)               'Screen Column - Pickup Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 8, obj.PickupAssignedByUserName)       'Screen Column - Supervisor ID
            SpreadCellValue(CurrentSheet, intRowIndex, 9, obj.PickupAssignedBySecondUserName) 'Screen Column - Cashier ID
            SpreadCellValue(CurrentSheet, intRowIndex, 10, obj.EODBagCheck)                   'Screen Column - E.O.D Bag Check
            SpreadCellValue(CurrentSheet, intRowIndex, 11, obj.Comments)                      'Screen Column - Comments
        Next
    End Sub

    Public Sub SetCashDropAndUnFloatedPickupGridControl(ByRef CashDropAndUnFloatedPickupGrid As FarPoint.Win.Spread.FpSpread) Implements IMainVM.SetCashDropAndUnFloatedPickupGridControl

        _CashDropAndUnFloatedPickupGrid = CashDropAndUnFloatedPickupGrid
    End Sub

    Public Sub SetFloatedCashierGridControl(ByRef FloatedCashierGrid As FarPoint.Win.Spread.FpSpread) Implements IMainVM.SetFloatedCashierGridControl

        _FloatedCashierGrid = FloatedCashierGrid
    End Sub
End Class
