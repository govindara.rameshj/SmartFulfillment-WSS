﻿Public Class FloatedPickupVMWithComments
    Implements IFloatedPickupVM

    Friend _FloatedPickupGrid As FpSpread
    Friend _FloatedPickupForm As System.Windows.Forms.Form
    Friend _CommentsColumnIndex As Integer = 3

    Public Overridable Sub FloatedPickupFormFormat() Implements IFloatedPickupVM.FloatedPickupFormFormat

        If FloatedPickupFormIsSet() Then
            _FloatedPickupForm.Width += 40
        End If
    End Sub

    Public Overridable Sub FloatedPickupGridFormat() Implements IFloatedPickupVM.FloatedPickupGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, _CommentsColumnIndex + 1, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn1, gcintColumnWidthSeal, False)     'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn2, gcintColumnWidthUser, False)     'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn3, gcintColumnWidthSeal, False)     'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, _CommentsColumnIndex, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn4, gcintColumnWidthCommentsWide, True) 'Screen Column - Pickup Comment

        SpreadColumnAlignLeft(NewSheet, _CommentsColumnIndex)
        SpreadSheetCustomiseHeaderAlignLeft(NewSheet, _CommentsColumnIndex)

        SpreadGridSheetAdd(_FloatedPickupGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatedPickupGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)

        SpreadGridDeactiveKey(_FloatedPickupGrid, Keys.F2)
    End Sub

    Public Overridable Sub FloatedPickupGridPopulate(ByRef FloatedPickupLists As Core.FloatedPickupListCollection) Implements IFloatedPickupVM.FloatedPickupGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _FloatedPickupGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatedPickupList In FloatedPickupLists
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj.CashierID)                         'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.StartFloatSealNumber)             'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.CashierUserName)                  'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.PickupSealNumber)                 'Screen Column - Pickup Seal
            SpreadCellValue(CurrentSheet, intRowIndex, _CommentsColumnIndex, obj.PickupComment) 'Screen Column - Pickup Comment
        Next
        CurrentSheet.Columns(_CommentsColumnIndex).Width = CurrentSheet.GetPreferredColumnWidth(_CommentsColumnIndex)
    End Sub

    Public Overridable Function GetFloatedPickupList(ByVal BankingPeriodID As Integer) As Core.FloatedPickupListCollection Implements IFloatedPickupVM.GetFloatedPickupList

        GetFloatedPickupList = New Core.FloatedPickupListCollection
        GetFloatedPickupList.LoadData(BankingPeriodID)
    End Function

    Public Overridable Sub SetFloatedPickupForm(ByRef FloatedPickupForm As System.Windows.Forms.Form) Implements IFloatedPickupVM.SetFloatedPickupForm

        _FloatedPickupForm = FloatedPickupForm
    End Sub

    Public Overridable Sub SetFloatedPickupGridControl(ByRef FloatedPickupGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatedPickupVM.SetFloatedPickupGridControl

        _FloatedPickupGrid = FloatedPickupGrid
    End Sub

#Region "Internals"

    Friend Overridable Function FloatedPickupFormIsSet() As Boolean

        Return _FloatedPickupForm IsNot Nothing
    End Function
#End Region
End Class
