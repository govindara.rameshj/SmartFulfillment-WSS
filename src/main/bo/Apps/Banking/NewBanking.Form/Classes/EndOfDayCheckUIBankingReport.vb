﻿Public Class EndOfDayCheckUIBankingReport
    Implements IEndOfDayCheckUI

#Region "Interface"

    Public Function ButtonCaptionName() As String Implements IEndOfDayCheckUI.ButtonCaptionName

        Return "F9 Banking Report"

    End Function

    Public Function ButtonEnabled(ByVal CurrentDate As Date, ByVal SelectedDate As Date, ByVal EndOfDayCheckDone As Boolean) As Boolean Implements IEndOfDayCheckUI.ButtonEnabled

        Return True

    End Function

    Public Function ExecuteEndOfDayCheckProcessUI() As Boolean Implements IEndOfDayCheckUI.ExecuteEndOfDayCheckProcessUI

        Return False

    End Function

#End Region

End Class