﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("NewBanking.Form.UnitTest")> 

Public Class FloatAssignSave
    Implements IFloatAssignSave

    Public Sub SaveFloatAssign(ByRef AssigningFloat As Banking.Core.FloatBanking, ByVal BankingPeriodId As Integer, ByVal TodaysPeriodId As Integer) Implements IFloatAssignSave.SaveFloatAssign

        If BankingPeriodId <> TodaysPeriodId Then
            'assigning float for selected banking which is not today
            SaveWithBankingPeriod(AssigningFloat, BankingPeriodId)
        Else
            'assigning float where today's banking has been selected; use existing code
            SaveWithoutBankingPeriod(AssigningFloat)
        End If
    End Sub

    Protected Friend Overridable Sub SaveWithBankingPeriod(ByRef AssigningFloat As Banking.Core.FloatBanking, ByVal BankingPeriodId As Integer)

        AssigningFloat.Save(BankingPeriodId)
    End Sub

    Protected Friend Overridable Sub SaveWithoutBankingPeriod(ByRef AssigningFloat As Banking.Core.FloatBanking)

        AssigningFloat.Save()
    End Sub
End Class
