﻿Public Class FloatViewModelUnassignFloatWithManualCheck
    Implements IFloatVM

    Private _FloatAssignVM As IFloatAssignVM
    Private _ExistingFloatValue As Decimal
    Private _NewFloatValue As Decimal
    Private _NewFloatBagDenoms As FloatBagCollection

#Region "Interface: Float Comments"

    Public Sub FloatFormFormat() Implements IFloatVM.FloatFormFormat

    End Sub

    Public Sub FloatGridFormat() Implements IFloatVM.FloatGridFormat

    End Sub

    Public Sub FloatGridPopulate(ByRef FloatLists As Core.FloatListCollection) Implements IFloatVM.FloatGridPopulate

    End Sub

    Public Function GetFloatList(ByVal BankingPeriodID As Integer) As Core.FloatListCollection Implements IFloatVM.GetFloatList
        Return Nothing
    End Function

    Public Sub SetFloatForm(ByRef FloatForm As System.Windows.Forms.Form) Implements IFloatVM.SetFloatForm

    End Sub

    Public Sub SetFloatGridControl(ByRef FloatGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatVM.SetFloatGridControl

    End Sub

#End Region

#Region "Interface: Unassign Float"

    Friend _ManualCheckScreen As ManualCheck
    Friend _UnAssignManualCheck As IUnAssignManualCheckModel

    Public Function IsManualCheckRequired() As Boolean Implements IFloatVM.IsManualCheckRequired

        Return AskManualCheckQuestion()
    End Function

    Public Function PerformManualCheck(ByVal StartFloatID As Integer) As Boolean Implements IFloatVM.PerformManualCheck

        CreateManualCheckScreen(StartFloatID)
        ShowManualCheckScreen()
        Return ReturnManualCheckScreenState()

    End Function

    Public Sub CompleteUnAssigningTheFloatWithManualCheck(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloatWithManualCheck
        Dim TotalsMatch As Boolean

        _UnAssignManualCheck = (New UnAssignManualCheckModelFactory).GetImplementation
        With _UnAssignManualCheck

            .Initialise(FloatID, PickupBagID, LoggedOnUserID, SecondUserID, BankingPeriodID, TodayPeriodID, AccountingModel, FloatSealNumber, CurrencyID, GetComments())

            If _ExistingFloatValue <> _NewFloatValue Then
                TotalsMatch = False
                InformationMessageBox("Total do not match. This float will be altered and stamped as having a MANUAL CHECK done")
            Else
                TotalsMatch = True
                InformationMessageBox("Totals match. The float will be stamped as having a MANUAL CHECK done")
            End If

            If QuestionMessageBox("Are you sure?") <> Windows.Forms.DialogResult.No Then
                Try
                    .StartTransaction()
                    .UnAssignTheFloat()

                    If TotalsMatch = True Then
                        .SetUnchangedFloatAsChecked()
                    Else
                        .SetCheckedFloatAsChanged(_NewFloatBagDenoms)
                    End If

                    .Commit()

                Catch ex As Exception
                    If .IsInTransaction Then
                        .RollBack()
                    End If

                    Throw New Exception("There was a problem saving the 'unassign float' changes.", ex)

                End Try
            End If

        End With

    End Sub

    Public Sub CompleteUnAssigningTheFloat(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloat

        _UnAssignManualCheck = (New UnAssignManualCheckModelFactory).GetImplementation
        With _UnAssignManualCheck
            Try
                .Initialise(FloatID, PickupBagID, LoggedOnUserID, SecondUserID, BankingPeriodID, TodayPeriodID, AccountingModel, FloatSealNumber, CurrencyID, GetComments())
                .StartTransaction()
                .UnAssignTheFloat()
                .Commit()
            Catch ex As Exception
                If .IsInTransaction Then
                    .RollBack()
                End If
                Throw New Exception("There was a problem saving the 'unassign float' changes.", ex)
            End Try
        End With
    End Sub

    Public ReadOnly Property NewFloatValue() As Decimal Implements IFloatVM.NewFloatValue
        Get
            Return _NewFloatValue
        End Get
    End Property
#End Region

#Region "Internals"

    Friend Overridable Function AskManualCheckQuestion() As Boolean

        AskManualCheckQuestion = QuestionMessageBox("Do you wish to check float?") = Windows.Forms.DialogResult.Yes
    End Function

    Friend Overridable Function GetComments() As String

        GetComments = String.Empty
        If _FloatAssignVM IsNot Nothing Then
            GetComments = _FloatAssignVM.GetCommentsFromGrid
        End If
    End Function

    Friend Overridable Sub CreateManualCheckScreen(ByVal StartFloatID As Integer)

        _ManualCheckScreen = New ManualCheck(StartFloatID)

    End Sub

    Friend Overridable Sub ShowManualCheckScreen()

        With _ManualCheckScreen

            .ShowDialog()

            If .FormExitState = ManualCheck.ExitMode.SaveAndExit Then
                _ExistingFloatValue = .ExistingFloatValue
                _NewFloatValue = .NewFloatValue
                _FloatAssignVM = .ManualCheckedViewModel
                _NewFloatBagDenoms = .GetNewFloatBagDenoms
            End If

        End With

    End Sub

    Friend Function ReturnManualCheckScreenState() As Boolean

        If _ManualCheckScreen._Exit = ManualCheck.ExitMode.SaveAndExit Then Return True

        Return False

    End Function

#End Region

End Class