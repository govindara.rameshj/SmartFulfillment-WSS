﻿Public Class FloatAssignAuthorise
    Implements IFloatAssignAuthorise

    Friend Overridable Function IsUserAManager(ByVal UserId As Integer) As Boolean
        Return Cts.Oasys.Core.System.User.IsManager(UserId)
    End Function

    Public Function FloatAssignAuthorise(ByVal intFirstUserID As Integer, ByRef SecondCheckUserID As Integer) As Boolean Implements IFloatAssignAuthorise.FloatAssignAuthorise
        Dim result As Boolean = False
        If IsUserAManager(intFirstUserID) Then
            SecondCheckUserID = intFirstUserID
            result = True
        End If
        Return result
    End Function

End Class
