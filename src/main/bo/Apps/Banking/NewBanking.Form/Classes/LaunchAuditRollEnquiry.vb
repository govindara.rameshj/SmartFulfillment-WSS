﻿Public Class LaunchAuditRollEnquiry
    Implements IAuditRollEnquiry

    Public Sub LaunchAuditRollEnquiry(ByVal UserId As Integer, ByRef myForm As Windows.Forms.Form) Implements IAuditRollEnquiry.LaunchAuditRollEnquiry

        Dim workStationId As Integer = CInt(My.Computer.Registry.GetValue("HKEY_local_machine\software\cts retail\default\oasys\2.0\wickes\enterprise", "WorkstationID", "20"))
        Dim hostForm As New Cts.Oasys.WinForm.HostForm("Reporting.dll", "Reporting.ReportViewer", _
                                                       "Audit Roll Enquiry", UserId, workStationId, 9, _
                                                       "310", "icons\icon_g.ico", True, 1)

        If hostForm IsNot Nothing Then
            'get activity log id needed to update log when app is closed
            hostForm.Tag = ActivityLog.RecordAppStarted(3410, UserId, workStationId)
            AddHandler hostForm.FormClosed, AddressOf WindowClosed

            hostForm.Owner = myForm
            hostForm.ShowDialog()
        Else
            MessageBox.Show("Audit Roll Enquiry failed, creating report for Please contact Help Desk.", "Cannot create report", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
        End If

    End Sub

    Private Sub WindowClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs)

        Select Case True
            Case TypeOf sender Is Cts.Oasys.WinForm.HostForm
                Dim hostForm As Cts.Oasys.WinForm.HostForm = CType(sender, Cts.Oasys.WinForm.HostForm)
                ActivityLog.RecordAppEnded(CInt(hostForm.Tag))
        End Select

    End Sub

End Class
