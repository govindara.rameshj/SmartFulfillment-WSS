﻿Public Class MainVMWithComments
    Implements IMainVM

    Friend _FloatedCashierGrid As FpSpread
    Friend _CashDropAndUnFloatedPickupGrid As FpSpread

    Friend _FloatedCashierGrid_EndOfDayBagCheck_ColumnHeader As String = My.Resources.ScreenColumns.MainFloatedCashierGridColumn11
    Friend _CashDropAndUnFloatedPickupGrid_EndOfDayBagCheck_ColumnHeader As String = My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn6
    Friend _MainModel As IMainModel
    Friend _CommentsDelimiter As String

    Public Function GetFloatedCashierDisplayList(ByVal SelectbankingPeriodID As Integer) As Core.FloatedCashierDisplayCollection Implements IMainVM.GetFloatedCashierDisplayList

        GetFloatedCashierDisplayList = New FloatedCashierDisplayCollection
        GetFloatedCashierDisplayList.LoadData(SelectbankingPeriodID)
    End Function

    Public Function GetCashDropAndUnFloatedPickupDisplayList(ByVal SelectbankingPeriodID As Integer) As Core.CashDropAndUnFloatedPickupDisplayCollection Implements IMainVM.GetCashDropAndUnFloatedPickupDisplayList

        GetCashDropAndUnFloatedPickupDisplayList = New CashDropAndUnFloatedPickupDisplayCollection
        GetCashDropAndUnFloatedPickupDisplayList.LoadData(SelectbankingPeriodID)
    End Function

    Public Sub CashDropAndUnFloatedPickupGridFormat() Implements IMainVM.CashDropAndUnFloatedPickupGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 6, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn1, gcintColumnWidthUser, False)         'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn2, gcintColumnWidthMoney, False)        'Screen Column - Till Variance
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn3, gcintColumnWidthSeal, False)         'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn4, gcintColumnWidthUser, False)         'Screen Column - Cashier ID
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn5, gcintColumnWidthUser, False)         'Screen Column - Supervisor ID
        SpreadColumnCustomise(NewSheet, 5, My.Resources.ScreenColumns.MainCashDropAndUnFloatedPickupGridColumn7, gcintColumnWidthCommentsWidest, True) 'Screen Column - Comments

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)

        SpreadColumnAlignLeft(NewSheet, 5)

        SpreadGridSheetAdd(_CashDropAndUnFloatedPickupGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_CashDropAndUnFloatedPickupGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)
    End Sub

    Public Sub CashDropUnFloatedPickupGridPopulate(ByVal CashDropAndUnFloatedPickupDisplayList As Core.CashDropAndUnFloatedPickupDisplayCollection) Implements IMainVM.CashDropUnFloatedPickupGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _CashDropAndUnFloatedPickupGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As CashDropAndUnFloatedPickupDisplay In CashDropAndUnFloatedPickupDisplayList
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.AssignedToUserName)               'Screen Column - Float Checked
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.TillVariance)                     'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.PickupSealNumber)                 'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.PickupAssignedByUserName)         'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 4, obj.PickupAssignedBySecondUserName)   'Screen Column - Assigned By
            AssignCommentsToCell(CurrentSheet, intRowIndex, 5, obj.Comments)                    'Screen Column - Comments
        Next
        CurrentSheet.Columns(5).Width = CurrentSheet.GetPreferredColumnWidth(5)
    End Sub

    Public Sub FloatedCashierGridFormat() Implements IMainVM.FloatedCashierGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 11, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.MainFloatedCashierGridColumn1, 90, False)                                                              'Screen Column - Float Checked
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.MainFloatedCashierGridColumn2, gcintColumnWidthMoney, False)                                           'Screen Column - Float Value
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.MainFloatedCashierGridColumn3, gcintColumnWidthSeal, False)                                            'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.MainFloatedCashierGridColumn4, gcintColumnWidthUser, False)                                            'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 4, My.Resources.ScreenColumns.MainFloatedCashierGridColumn5, gcintColumnWidthUser, False)                                            'Screen Column - Assigned By
        SpreadColumnCustomise(NewSheet, 5, My.Resources.ScreenColumns.MainFloatedCashierGridColumn6, gcintColumnWidthMoney, False)                                           'Screen Column - Till Variance
        SpreadColumnCustomise(NewSheet, 6, My.Resources.ScreenColumns.MainFloatedCashierGridColumn7, gcintColumnWidthSealWide, False)                                        'Screen Column - Float Seal (E.O.S)
        SpreadColumnCustomise(NewSheet, 7, My.Resources.ScreenColumns.MainFloatedCashierGridColumn8, gcintColumnWidthSeal, False)                                            'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 8, My.Resources.ScreenColumns.MainFloatedCashierGridColumn9, gcintColumnWidthUser, False)                                            'Screen Column - Cashier ID
        SpreadColumnCustomise(NewSheet, 9, My.Resources.ScreenColumns.MainFloatedCashierGridColumn10, gcintColumnWidthUser, False)                                           'Screen Column - Supervisor ID
        SpreadColumnCustomise(NewSheet, 10, My.Resources.ScreenColumns.MainFloatedCashierGridColumn12, gcintColumnWidthCommentsWider, True, CellHorizontalAlignment.General) 'Screen Column - Comments

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 5, True, gstrFarPointMoneyNullDisplay)

        SpreadSheetCustomiseHeaderAlignLeft(NewSheet, 10)

        SpreadGridSheetAdd(_FloatedCashierGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatedCashierGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)
    End Sub

    Public Sub FloatedCashierGridPopulate(ByVal FloatedCashierDisplayList As Core.FloatedCashierDisplayCollection) Implements IMainVM.FloatedCashierGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0
        Dim FormattedComments As String = String.Empty

        CurrentSheet = _FloatedCashierGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatedCashierDisplay In FloatedCashierDisplayList
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.StartFloatChecked)              'Screen Column - Float Checked
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.StartFloatValue)                'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.StartFloatSealNumber)           'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.StartFloatAssignedToUserName)   'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 4, obj.StartFloatAssignedByUserName)   'Screen Column - Assigned By
            SpreadCellValue(CurrentSheet, intRowIndex, 5, obj.TillVariance)                   'Screen Column - Till Variance
            SpreadCellValue(CurrentSheet, intRowIndex, 6, obj.NewFloatSealNumber)             'Screen Column - Float Seal (E.O.S)
            SpreadCellValue(CurrentSheet, intRowIndex, 7, obj.PickupSealNumber)               'Screen Column - Pickup Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 8, obj.PickupAssignedByUserName)       'Screen Column - Supervisor ID
            SpreadCellValue(CurrentSheet, intRowIndex, 9, obj.PickupAssignedBySecondUserName) 'Screen Column - Cashier ID
            FormattedComments = FormatDelimitedComments(obj.Comments)                         'Screen Column - Comments
            AssignCommentsToCell(CurrentSheet, intRowIndex, 10, FormattedComments)
        Next
        CurrentSheet.Columns(10).Width = CurrentSheet.GetPreferredColumnWidth(10)
    End Sub

    Public Sub SetCashDropAndUnFloatedPickupGridControl(ByRef CashDropAndUnFloatedPickupGrid As FarPoint.Win.Spread.FpSpread) Implements IMainVM.SetCashDropAndUnFloatedPickupGridControl

        _CashDropAndUnFloatedPickupGrid = CashDropAndUnFloatedPickupGrid
    End Sub

    Public Sub SetFloatedCashierGridControl(ByRef FloatedCashierGrid As FarPoint.Win.Spread.FpSpread) Implements IMainVM.SetFloatedCashierGridControl

        _FloatedCashierGrid = FloatedCashierGrid
    End Sub

    Friend Overridable Sub SetupCommentsDelimiter()

        SetMainModel()
        GetCommentsDelimiter()
    End Sub

    Friend Overridable Sub GetCommentsDelimiter()

        If CommentsDelimiterIsNotSet Then
            _CommentsDelimiter = _MainModel.GetCommentsDelimiter
        End If
    End Sub

    Friend Overridable Sub SetMainModel()

        If MainModelIsNotSet() Then
            _MainModel = (New MainModelFactory).GetImplementation
        End If
    End Sub

    Friend Overridable Function FormatDelimitedComments(ByVal Comments As String) As String

        SetupCommentsDelimiter()
        FormatDelimitedComments = Comments
        If Comments.Contains(_CommentsDelimiter) Then
            Dim FormattedComments(1) As String

            With Comments
                FormattedComments(0) = "Manual Check: " & .Substring(0, .IndexOf(_CommentsDelimiter))
                FormattedComments(1) = "Pickup Bag:     " & .Substring(.IndexOf(_CommentsDelimiter) + Len(_CommentsDelimiter))
            End With
            FormatDelimitedComments = FormattedComments(0) & vbNewLine & FormattedComments(1)
        End If
    End Function

    Friend Overridable Function CommentsDelimiterIsNotSet() As Boolean

        Return String.IsNullOrEmpty(_CommentsDelimiter)
    End Function

    Friend Overridable Function MainModelIsNotSet() As Boolean

        Return _MainModel Is Nothing
    End Function

    Friend Overridable Sub AssignCommentsToCell(ByRef SheetCellIsOn As SheetView, ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer, ByVal Comments As String)

        If CommentsContainALineBreak(Comments) Then
            AssignCommentsWithLineBreakToCell(SheetCellIsOn, CellRowIndex, CellColumnIndex, Comments)
        Else
            SpreadCellValue(SheetCellIsOn, CellRowIndex, CellColumnIndex, Comments)
        End If
    End Sub

    Friend Overridable Function CommentsContainALineBreak(ByVal Comments As String) As Boolean

        If String.IsNullOrEmpty(Comments) Then
            CommentsContainALineBreak = False
        Else
            CommentsContainALineBreak = Comments.Contains(vbNewLine) Or Comments.Contains(vbCrLf) Or Comments.Contains(vbLf)
        End If
    End Function

    Friend Overridable Sub AssignCommentsWithLineBreakToCell(ByRef SheetCellIsOn As SheetView, ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer, ByVal Comments As String)

        SpreadCellSetAsMultiLine(SheetCellIsOn, CellRowIndex, CellColumnIndex, False)
        SpreadCellValue(SheetCellIsOn, CellRowIndex, CellColumnIndex, Comments)
        SpreadRowSetHeightToFitText(SheetCellIsOn, CellRowIndex)
    End Sub
End Class
