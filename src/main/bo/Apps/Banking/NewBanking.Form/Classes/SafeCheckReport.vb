﻿Public Class SafeCheckReport

    Friend _SelectedPeriod As Period
    Friend _StoreID As String
    Friend _StoreName As String

    Friend _ReportGrid As FpSpread
    Friend _ReportGridView As New SheetView

    Friend _DisplayMainSafe As Boolean
    Friend _DisplayChangeSafe As Boolean

    Public Sub New(ByVal SelectedPeriod As Period, ByVal StoreID As String, ByVal StoreName As String)

        SetSelectedPeriod(SelectedPeriod)
        SetStoreID(StoreID)
        SetStoreName(StoreName)

        SetReportGrid(Nothing)

        DisplayMainSafe(False)
        DisplayChangeSafe(False)

    End Sub

    Public Sub New(ByVal SelectedPeriod As Period, ByVal StoreID As String, ByVal StoreName As String, ByRef Value As FpSpread)

        SetSelectedPeriod(SelectedPeriod)
        SetStoreID(StoreID)
        SetStoreName(StoreName)

        SetReportGrid(Value)

        DisplayMainSafe(True)
        DisplayChangeSafe(True)

    End Sub

    Public Sub ConfigureReport()

        Dim colSafeMaintenance As SafeDenominationCollection
        Dim RowIndex As Integer = 0

        SpreadSheetCustomise(_ReportGridView, 5, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(_ReportGridView, 0, " ", 160, False)                                                        'Screen Column - Denomination
        SpreadColumnCustomise(_ReportGridView, 1, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn2, 90, False)  'Screen Column - System
        SpreadColumnCustomise(_ReportGridView, 2, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn3, 90, False)  'Screen Column - Main Safe
        SpreadColumnCustomise(_ReportGridView, 3, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn4, 90, False)  'Screen Column - Change Safe
        SpreadColumnCustomise(_ReportGridView, 4, My.Resources.ScreenColumns.SafeMaintenanceInputGridColumn5, 280, False) 'Screen Column - Comments

        SpreadColumnAlignLeft(_ReportGridView, 0)
        SpreadColumnMoney(_ReportGridView, 1, True, "")
        SpreadColumnMoney(_ReportGridView, 2, True, "")
        SpreadColumnMoney(_ReportGridView, 3, True, "")

        SpreadGridSheetAdd(_ReportGrid, _ReportGridView, True, String.Empty)
        SpreadGridScrollBar(_ReportGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        'load data - all tenders
        colSafeMaintenance = New SafeDenominationCollection
        colSafeMaintenance.LoadAllTenders(_SelectedPeriod.PeriodID)

        SpreadSheetClearDown(_ReportGridView)
        For Each obj As NewBanking.Core.SafeDenomination In colSafeMaintenance
            SpreadRowAdd(_ReportGridView, RowIndex)

            SpreadCellValue(_ReportGridView, RowIndex, 0, obj.TenderText)                                                  'Screen Column - Denomination
            SpreadCellValue(_ReportGridView, RowIndex, 1, CType(IIf(obj.SystemSafe.HasValue, obj.SystemSafe, 0), Decimal)) 'Screen Column - System
            DisplayMainSafe(RowIndex, obj.MainSafe)                                                                     'Screen Column - Main Safe
            DisplayChangeSafe(RowIndex, obj.ChangeSafe)                                                                   'Screen Column - Change Safe

        Next
        'total cash entered - calculated
        SpreadRowDrawLine(_ReportGridView, RowIndex, Color.Black, 1)

        SpreadRowAdd(_ReportGridView, RowIndex)
        SpreadRowBold(_ReportGrid, _ReportGridView, RowIndex)

        SpreadCellValue(_ReportGridView, RowIndex, 0, "Safe")
        SpreadCellFormula(_ReportGridView, RowIndex, 1, "SUM(R1C:R13C)")

        SpreadCellFormula(_ReportGridView, RowIndex, 2, "SUM(R1C:R13C)")
        SpreadCellFormula(_ReportGridView, RowIndex, 3, "SUM(R1C:R13C)")

        DisplayMainSafeTotal(RowIndex)
        DisplaychangeSafeTotal(RowIndex)

        SpreadRowAdd(_ReportGridView, RowIndex)

        Dim SafeMaintenanceInfo As ISafeCheckReportUI
        Dim Authorisers As ISafeCheckReportUI
        Dim SafeMaintenanceBagComments As ISafeCheckReportUI

        Authorisers = (New SafeCheckReportUISafeMaintenanceAuthoriserFactory).GetImplementation
        Authorisers.DisplaySafeMaintenanceAuthorisers(_ReportGrid, _ReportGridView, RowIndex, _SelectedPeriod)

        SafeMaintenanceInfo = (New SafeCheckReportUISafeMaintenanceInfoFactory).GetImplementation
        SafeMaintenanceInfo.DisplaySafeMaintenanceComments(_ReportGrid, _ReportGridView, RowIndex, _SelectedPeriod.PeriodID)
        SafeMaintenanceInfo.DisplaySafeMaintenanceScannedBankingBags(_ReportGrid, _ReportGridView, RowIndex, _SelectedPeriod.PeriodID)

        SafeMaintenanceBagComments = (New SafeCheckReportUISafeMaintenanceBagCommentsFactory).GetImplementation
        SafeMaintenanceBagComments.DisplaySafeMaintenanceCommentsForAllBags(_ReportGrid, _ReportGridView, RowIndex, _SelectedPeriod.PeriodID)

        ConfigurePrint()

    End Sub

    Public Sub ExecuteReport()

        _ReportGrid.PrintSheet(_ReportGridView)

    End Sub

#Region "Private Functions & Procedures"

    Friend Overridable Sub SetSelectedPeriod(ByVal SelectedPeriod As Core.Period)

        _SelectedPeriod = SelectedPeriod

    End Sub

    Friend Overridable Sub SetStoreID(ByVal StoreID As String)

        _StoreID = StoreID

    End Sub

    Friend Overridable Sub SetStoreName(ByVal StoreName As String)

        _StoreName = StoreName

    End Sub

    Friend Overridable Sub SetReportGrid(ByRef Value As FpSpread)

        If Value Is Nothing Then

            _ReportGrid = New FpSpread

        Else

            _ReportGrid = Value

        End If

    End Sub

    Friend Overridable Sub DisplayMainSafe(ByVal Value As Boolean)

        _DisplayMainSafe = Value

    End Sub

    Friend Overridable Sub DisplayChangeSafe(ByVal Value As Boolean)

        _DisplayChangeSafe = Value

    End Sub

    Friend Sub DisplayMainSafe(ByVal RowIndex As Integer, ByVal MainSafeValue As System.Nullable(Of Decimal))

        If _DisplayMainSafe = True Then SpreadCellValue(_ReportGridView, RowIndex, 2, CType(IIf(MainSafeValue.HasValue, MainSafeValue, 0), Decimal)) 'Screen Column - Main Safe

    End Sub

    Friend Sub DisplayChangeSafe(ByVal RowIndex As Integer, ByVal ChangeSafeValue As System.Nullable(Of Decimal))

        If _DisplayChangeSafe = True Then SpreadCellValue(_ReportGridView, RowIndex, 3, CType(IIf(ChangeSafeValue.HasValue, ChangeSafeValue, 0), Decimal)) 'Screen Column - Main Safe

    End Sub

    Friend Sub DisplayMainSafeTotal(ByVal RowIndex As Integer)

        If _DisplayMainSafe = True Then SpreadCellFormula(_ReportGridView, RowIndex, 2, "SUM(R1C:R13C)")

    End Sub

    Friend Sub DisplaychangeSafeTotal(ByVal RowIndex As Integer)

        If _DisplayChangeSafe = True Then SpreadCellFormula(_ReportGridView, RowIndex, 3, "SUM(R1C:R13C)")

    End Sub

    Friend Sub ConfigurePrint()

        With _ReportGridView
            .PrintInfo.ShowBorder = False
            .PrintInfo.UseSmartPrint = True
            .PrintInfo.UseMax = False

            .PrintInfo.Margin.Left = 50
            .PrintInfo.Margin.Right = 50
            .PrintInfo.Margin.Top = 20
            .PrintInfo.Margin.Bottom = 20
            .PrintInfo.ShowBorder = False
            .PrintInfo.ShowPrintDialog = True
            .PrintInfo.UseSmartPrint = True
            .PrintInfo.Orientation = PrintOrientation.Portrait
            .PrintInfo.ShowPrintDialog = True

            .PrintInfo.Header = "/lStore: " & _StoreID & " " & _StoreName
            .PrintInfo.Header &= "/c/fz""14""Safe Check Report"
            .PrintInfo.Header &= "/n/c/fz""10""Banking Period : " & _SelectedPeriod.PeriodID.ToString & " - " & _SelectedPeriod.PeriodDate.ToShortDateString
            .PrintInfo.Header &= "/n "

            .PrintInfo.Footer = "/lPrinted: " & Now
            .PrintInfo.Footer &= "/cPage /p of /pc"
            .PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        End With

    End Sub

#End Region

End Class