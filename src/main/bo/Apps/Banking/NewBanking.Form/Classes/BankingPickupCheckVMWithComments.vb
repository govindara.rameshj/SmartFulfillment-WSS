﻿Imports System.Text

Public Class BankingPickupCheckVMWithComments
    Implements IBankingPickupCheckVM

    Friend _BankingPickupCheckGrid As FpSpread
    Friend _BankingPickupCheckForm As System.Windows.Forms.Form
    Friend _BankingPeriodID As Nullable(Of Integer)
    Friend _IsFirstEndOfDayPickup As Boolean
    Friend _TenderDenominationList As TenderDenominationListCollection
    Friend _CurrentSheet As SheetView
    Friend _BankingPickupCheckGridCurrentRowIndex As Nullable(Of Integer)
    Friend _ActiveCashierLists As ActiveCashierListCollection
    Friend _ActiveCashierList As ActiveCashierList
    Friend _BankingPickupCheckGridCurrentColumnIndex As Nullable(Of Integer)
    Friend _ActiveCashier As ActiveCashierList
    Friend _ActivePickupBag As Pickup
    Friend _ActivePickupSale As PickupSale
    Friend _CommentsRowIndex As Integer
    Friend _TotalsColumnIndex As Integer
    Friend _NumberOfCommentsRowsToAdd As Integer
    Friend _BagCheckRowIndex As Nullable(Of Integer)

#Region "IBankingPickupCheckVM Implementation"

    Public Overridable Sub BankingPickupCheckGridFormat() Implements IBankingPickupCheckVM.BankingPickupCheckGridFormat
        Dim NewSheet As New SheetView

        If BankingPickupCheckGridControlIsSet() Then
            SpreadSheetCustomise(NewSheet, 1, Model.SelectionUnit.Cell)
            SpreadSheetCustomiseHeader(NewSheet, gintHeaderHeightTriple)
            SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTendersWide, False)     'Screen Column - Denomination
            SpreadColumnAlignLeft(NewSheet, 0)
            SpreadColumnBackgroundColour(NewSheet, 0, Color.Beige)
            SpreadSheetCustomiseFreezeLeadingColumns(NewSheet, 1)

            SpreadGridSheetAdd(_BankingPickupCheckGrid, NewSheet, True, String.Empty)
            SpreadGridScrollBar(_BankingPickupCheckGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)
            SpreadGridInputMaps(_BankingPickupCheckGrid)
        End If
    End Sub

    Public Overridable Sub SetBankingPickupCheckForm(ByRef BankingPickupCheckForm As System.Windows.Forms.Form) Implements IBankingPickupCheckVM.SetBankingPickupCheckForm

        _BankingPickupCheckForm = BankingPickupCheckForm
    End Sub

    Public Overridable Sub BankingPickupCheckFormFormat() Implements IBankingPickupCheckVM.BankingPickupCheckFormFormat

        If BankingPickupCheckFormIsSet() Then
            With _BankingPickupCheckForm
                .Height += 100
            End With
        End If
    End Sub

    Public Overridable Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer) Implements IBankingPickupCheckVM.SetBankingPeriodID

        _BankingPeriodID = BankingPeriodID
    End Sub

    Public Overridable Sub SetBankingPickupCheckGridControl(ByRef BankingPickupCheckGridControl As FarPoint.Win.Spread.FpSpread) Implements IBankingPickupCheckVM.SetBankingPickupCheckGridControl

        _BankingPickupCheckGrid = BankingPickupCheckGridControl
    End Sub

    Public Overridable Sub BankingPickupCheckGridPopulate(ByVal ActiveCashierList As Core.ActiveCashierListCollection) Implements IBankingPickupCheckVM.BankingPickupCheckGridPopulate

        If BankingPickupCheckGridControlIsSet() Then
            If PopulateTenderDenominationList() Then
                SetCurrentSheetForBankingPickupGrid()
                ClearCurrentSheetForBankingPickupGrid()
                InitialiseBankingPickupCheckGridRowIndex()
                AddBagCheckRowToBankingPickupGrid()
                AddCashTenderRowsToBankingPickupGrid()
                AddTotalCashRowToBankingPickupGrid()
                AddAmendableNonCashTenderRowsToBankingPickupGrid()
                AddUnAmendableNonCashTenderRowsToBankingPickupGrid()
                AddTotalNonCashRowToBankingPickupGrid()
                AddTotalEnteredRowToBankingPickupGrid()
                AddTotalEnteredAllPickupsRowToBankingPickupGrid()
                AddStartFloatRowToBankingPickupGrid()
                AddNewFloatRowToBankingPickupGrid()
                AddSystemTotalRowToBankingPickupGrid()
                AddVarianceRowToBankingPickupGrid()
                AddCommentsRowToBankingPickupGrid()
                SetActiveCashierLists(ActiveCashierList)
                AddActiveCashierListPickupBagColumnsAndCashDropBagColumnsToBankingPickupGrid()
                AddTotalsColumnToBankingPickupGrid()
                SetTotalsColumnFormulaIgnoringTotalEnteredAllPickupsAndCommentsRows()
            End If
        End If
    End Sub

    Public Overridable Function GetActiveCashierListCollection() As Core.ActiveCashierListCollection Implements IBankingPickupCheckVM.GetActiveCashierListCollection
        Dim Cashiers As New ActiveCashierListCollection

        If BankingPeriodIDIsSet() Then
            LoadAllCashiers(Cashiers)
        End If
        GetActiveCashierListCollection = Cashiers
    End Function

    Public Overridable Function ActiveCellIsCommentsCell() As Boolean Implements IBankingPickupCheckVM.ActiveCellIsCommentsCell

        ActiveCellIsCommentsCell = False
        If BankingPickupCheckGridControlIsSet() AndAlso ActiveSheetIsSet() Then
            Return CellIsCommentsCell(_BankingPickupCheckGrid.ActiveSheet.ActiveCell)
        End If
    End Function

    Public Overridable Function GetComments(ByVal BagID As Integer) As String Implements IBankingPickupCheckVM.GetComments

        GetComments = ""
        If BankingPickupCheckGridControlIsSet() Then
            If ActiveSheetIsSet() Then
                For Each PickupOrCashDropColumn As FarPoint.Win.Spread.Column In _BankingPickupCheckGrid.ActiveSheet.Columns
                    With PickupOrCashDropColumn
                        If TypeOf .Tag Is String Then
                            Dim TagParam() As String = .Tag.ToString.Split("|"c)

                            If TagParam.Length > 1 Then
                                Dim ColumnPickupId As Integer = -1

                                If Integer.TryParse(TagParam(1), ColumnPickupId) Then
                                    If ColumnPickupId = BagID Then
                                        GetComments = _BankingPickupCheckGrid.ActiveSheet.Cells(_CommentsRowIndex, .Index).Text
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If
                    End With
                Next
            End If
        End If
    End Function

    Public Overridable Sub PersistCommentsIfChanged(ByVal PickupBagID As Integer) Implements IBankingPickupCheckVM.PersistCommentsIfChanged

        If ActiveCashierListsIsSet() Then
            Dim NewComments As String = GetComments(PickupBagID)
            Dim OldComments As String = GetBagCommentFromActiveCashierLists(PickupBagID)

            If NewCommentsDoNotMatchOldComments(NewComments, OldComments) Then
                Dim PickupBag As Pickup = _ActiveCashierLists.GetBag(PickupBagID)

                PickupBagSaveComments(PickupBag, NewComments)
            End If
        End If
    End Sub

    Public Overridable Sub ReadyActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer) Implements IBankingPickupCheckVM.ReadyActiveCommentsCellForEditing

        If BankingPickupCheckGridControlIsSet() Then
            If ActiveSheetIsSet() Then
                With _BankingPickupCheckGrid
                    If CellIsCommentsCell(.ActiveSheet.Cells(CellRowIndex, CellColumnIndex)) Then
                        If ColumnIsPotentiallyEditable(CellColumnIndex) And BagNotAlreadyMarkedAsChecked(CellColumnIndex) Then
                            SetUpActiveCellForTextEditingAndStart(CellRowIndex, CellColumnIndex)
                        End If
                    End If
                End With
            End If
        End If
    End Sub

    Public Overridable Sub UndoReadinessOfActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer) Implements IBankingPickupCheckVM.UndoReadinessOfActiveCommentsCellForEditing

        If BankingPickupCheckGridControlIsSet() Then
            If ActiveSheetIsSet() Then
                With _BankingPickupCheckGrid
                    If CellIsCommentsCell(.ActiveSheet.Cells(CellRowIndex, CellColumnIndex)) Then
                        If ColumnIsPotentiallyEditable(CellColumnIndex) And BagNotAlreadyMarkedAsChecked(CellColumnIndex) Then
                            ReSetBankingPickupGridActiveCellAfterTextCellEditing(CellRowIndex, CellColumnIndex)
                        End If
                    End If
                End With
            End If
        End If
    End Sub

    Public Overridable Function UseGenericCommentForAllBankingBags() As Boolean Implements IBankingPickupCheckVM.UseGenericCommentForAllBankingBags

        Return False
    End Function

    Public Overridable Function GetCommentsRowIndex() As Integer Implements IBankingPickupCheckVM.GetCommentsRowIndex

        Return _CommentsRowIndex - 1
    End Function

    Public Function BankingVariance() As Decimal Implements IBankingPickupCheckVM.BankingVariance

        Dim ActiveSheet As SheetView

        ActiveSheet = _BankingPickupCheckGrid.ActiveSheet

        Return CType(ActiveSheet.Cells(_CommentsRowIndex - 1, ActiveSheet.ColumnCount - 1).Value, Decimal)

    End Function

#End Region

#Region "Internals"

#Region "Condition Checks"

    Friend Overridable Function BankingPickupCheckGridControlIsSet() As Boolean

        Return _BankingPickupCheckGrid IsNot Nothing
    End Function

    Friend Overridable Function ActiveSheetIsSet() As Boolean

        ActiveSheetIsSet = False
        If BankingPickupCheckGridControlIsSet() Then
            ActiveSheetIsSet = _BankingPickupCheckGrid.ActiveSheet IsNot Nothing
        End If
    End Function

    Friend Overridable Function BankingPeriodIDIsSet() As Boolean

        Return _BankingPeriodID.HasValue
    End Function

    Friend Overridable Function TenderDenominationListIsSet() As Boolean

        Return _TenderDenominationList IsNot Nothing
    End Function

    Friend Overridable Function TenderDenominationListHasData() As Boolean

        TenderDenominationListHasData = False
        If TenderDenominationListIsSet() Then
            TenderDenominationListHasData = _TenderDenominationList.Count > 0
        End If
    End Function

    Friend Overridable Function CurrentSheetIsSet() As Boolean

        Return _CurrentSheet IsNot Nothing
    End Function

    Friend Overridable Function BankingPickupCheckGridCurrentRowIndexIsSet() As Boolean

        Return _BankingPickupCheckGridCurrentRowIndex.HasValue
    End Function

    Friend Overridable Function ActiveCashierListsIsSet() As Boolean

        Return _ActiveCashierLists IsNot Nothing
    End Function

    Friend Overridable Function BankingPickupCheckGridCurrentColumnIndexIsSet() As Boolean

        Return _BankingPickupCheckGridCurrentColumnIndex.HasValue
    End Function

    Friend Overridable Function ActiveCashierIsSet() As Boolean

        Return _ActiveCashier IsNot Nothing
    End Function

    Friend Overridable Function ActivePickupBagIsSet() As Boolean

        Return _ActivePickupBag IsNot Nothing
    End Function

    Friend Overridable Function ActivePickupSaleIsSet() As Boolean

        Return _ActivePickupSale IsNot Nothing
    End Function

    Friend Overridable Function BankingPickupCheckFormIsSet() As Boolean

        Return _BankingPickupCheckForm IsNot Nothing
    End Function

    Friend Overridable Function ColumnIsPotentiallyEditable(ByVal ColumnIndex As Integer) As Boolean

        Return ColumnIndex > 0 And ColumnIndex < _TotalsColumnIndex
    End Function

    Friend Overridable Function BagCheckRowIndexIsSet() As Boolean

        Return _BagCheckRowIndex.HasValue
    End Function

    Friend Overridable Function CellIsCommentsCell(ByRef AmIACommentsCell As Cell) As Boolean

        CellIsCommentsCell = False
        If BankingPickupCheckGridControlIsSet() Then
            If ActiveSheetIsSet() Then
                With _BankingPickupCheckGrid.ActiveSheet
                    For ColumnIndex As Integer = 0 To _TotalsColumnIndex - 1
                        If Object.Equals(AmIACommentsCell, .Cells(_CommentsRowIndex, ColumnIndex)) Then
                            CellIsCommentsCell = True
                            Exit For
                        End If
                    Next
                End With
            End If
        End If
    End Function

    Friend Overridable Function NewCommentsDoNotMatchOldComments(ByVal NewComments As String, ByVal OldComments As String) As Boolean

        Return String.Compare(NewComments, OldComments) <> 0
    End Function
#End Region

#Region "Data"

    Friend Overridable Function PopulateTenderDenominationList() As Boolean

        SetTenderDenominationList()
        GetTenderDenominationListData()

        Return TenderDenominationListHasData()
    End Function

    Friend Overridable Sub SetTenderDenominationList()

        If Not TenderDenominationListIsSet() Then
            _TenderDenominationList = New TenderDenominationListCollection
        End If
    End Sub

    Friend Overridable Sub GetTenderDenominationListData()

        If TenderDenominationListIsSet() Then
            _TenderDenominationList.LoadData()
        End If
    End Sub

    Friend Overridable Sub SetActiveCashierLists(ByRef ActiveCashierLists As ActiveCashierListCollection)

        _ActiveCashierLists = ActiveCashierLists
    End Sub

    Friend Overridable Sub SetActiveCashier(ByRef ActiveCashier As ActiveCashierList)

        _ActiveCashier = ActiveCashier
    End Sub

    Friend Overridable Sub SetActivePickupBag(ByRef ActivePickup As Pickup)

        _ActivePickupBag = ActivePickup
    End Sub

    Friend Overridable Sub SetIsFirstEndOfDayPickup(ByVal IsFirstEndOfDayPickup As Boolean)

        _IsFirstEndOfDayPickup = IsFirstEndOfDayPickup
    End Sub

    Friend Overridable Sub SetActivePickupSale(ByVal ActivePickupSale As PickupSale)

        _ActivePickupSale = ActivePickupSale
    End Sub

    Friend Overridable Sub SetNumberOfCommentsRowsToAdd()

        _NumberOfCommentsRowsToAdd = 6
    End Sub

    Friend Overridable Sub LoadAllCashiers(ByRef ToLoadInto As ActiveCashierListCollection)

        If ToLoadInto IsNot Nothing Then
            ToLoadInto.LoadAllCashiers(_BankingPeriodID.Value)
        End If
    End Sub

    Friend Overridable Sub PickupBagSaveComments(ByVal PickupBag As Pickup, ByVal NewComments As String)

        If PickupBag IsNot Nothing Then
            PickupBag.SaveComments(NewComments)
        End If
    End Sub

    Friend Overridable Function GetBagCommentFromActiveCashierLists(ByVal PickupBagID As Integer) As String

        GetBagCommentFromActiveCashierLists = ""
        If ActiveCashierListsIsSet() Then
            GetBagCommentFromActiveCashierLists = _ActiveCashierLists.GetBagComment(PickupBagID)
        End If
    End Function
#End Region

#Region "UI"

    Friend Overridable Sub SetCurrentSheetForBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() Then
            _CurrentSheet = _BankingPickupCheckGrid.ActiveSheet
        End If
    End Sub

    Friend Overridable Sub ClearCurrentSheetForBankingPickupGrid()

        If CurrentSheetIsSet() Then
            SpreadSheetClearDown(_CurrentSheet)
        End If
    End Sub

    Friend Overridable Sub InitialiseBankingPickupCheckGridRowIndex()

        _BankingPickupCheckGridCurrentRowIndex = 0
    End Sub

    Friend Overridable Sub AddBagCheckRowToBankingPickupGrid()

        If CurrentSheetIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Bag Check")
            SpreadRowHeight(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 30)
            SpreadRowVerticalAlignment(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, CellVerticalAlignment.Center)
            SetBagCheckRowIndexToCurrentRowIndex()
        End If
    End Sub

    Friend Overridable Sub AddCashTenderRowsToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() _
        And BankingPickupCheckGridCurrentRowIndexIsSet() _
        And TenderDenominationListIsSet() Then
            'cash tender
            For Each TenderDenomination As TenderDenominationList In _TenderDenominationList
                With TenderDenomination
                    If .IsCashTenderType Then
                        AddRowToBankingPickupGrid()
                        SpreadRowTagValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, .TenderText)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddTotalCashRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Total Cash Entered")
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddAmendableNonCashTenderRowsToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() _
        And BankingPickupCheckGridCurrentRowIndexIsSet() _
        And TenderDenominationListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenominationList
                With TenderDenomination
                    If .IsAmendableNonCashTenderType Then
                        AddRowToBankingPickupGrid()
                        SpreadRowTagValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, .TenderText)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddUnAmendableNonCashTenderRowsToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() _
        And BankingPickupCheckGridCurrentRowIndexIsSet() _
        And TenderDenominationListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenominationList
                With TenderDenomination
                    If .IsUnAmendableNonCashTenderType Then
                        AddRowToBankingPickupGrid()
                        SpreadRowTagValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, .TenderText)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddTotalNonCashRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Total Non Cash Entered")
        End If
    End Sub

    Friend Overridable Sub AddTotalEnteredRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Total Entered")
        End If
    End Sub

    Friend Overridable Sub AddTotalEnteredAllPickupsRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Total Entered (All Pickups)")
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddStartFloatRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Start Float")
        End If
    End Sub

    Friend Overridable Sub AddNewFloatRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "New Float")
        End If
    End Sub

    Friend Overridable Sub AddSystemTotalRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "System Total")
        End If
    End Sub

    Friend Overridable Sub AddVarianceRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            AddRowToBankingPickupGrid()
            SpreadRowColour(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, _CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, 0, "Variance")
        End If
    End Sub

    Friend Overridable Sub AddCommentsRowToBankingPickupGrid()

        If BankingPickupCheckGridControlIsSet() And BankingPickupCheckGridCurrentRowIndexIsSet() Then
            _NumberOfCommentsRowsToAdd = 6
            'line above
            SpreadRowDrawLine(_CurrentSheet, _BankingPickupCheckGridCurrentRowIndex.Value, Color.Black, 1)

            _CommentsRowIndex = _BankingPickupCheckGridCurrentRowIndex.Value + 1
            AddRowsToBankingPickupGrid(_NumberOfCommentsRowsToAdd)

            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex, _NumberOfCommentsRowsToAdd, 0, 1)
            SpreadCellSetAsBold(_BankingPickupCheckGrid, _CurrentSheet, _CommentsRowIndex, 0)
            SpreadCellSetVerticalAlignment(_CurrentSheet, _CommentsRowIndex, 0, CellVerticalAlignment.Top)
            SpreadCellValue(_CurrentSheet, _CommentsRowIndex, 0, "Comments")
        End If
    End Sub

    Friend Overridable Sub AddActiveCashierListPickupBagColumnsAndCashDropBagColumnsToBankingPickupGrid()

        InitialiseCurrentColumnIndex()
        If BankingPickupCheckGridControlIsSet() _
        And BankingPickupCheckGridCurrentRowIndexIsSet() _
        And ActiveCashierListsIsSet() _
        And BankingPickupCheckGridCurrentColumnIndexIsSet() Then
            For Each ActiveCashier As ActiveCashierList In _ActiveCashierLists
                SetActiveCashier(ActiveCashier)
                'cashier - end of day pickup (should only be one), just in case the flag will be reset for each cashier and a tag added
                '          to the first e.o.d pickup
                SetIsFirstEndOfDayPickup(True)
                SetupActiveCashiersEndOfDayPickupsCells()
                SetupActiveCashiersCashDropsCells()
            Next
        End If
    End Sub

    Friend Overridable Sub AddTotalsColumnToBankingPickupGrid()

        AddColumnToBankingPickupGrid()
        _TotalsColumnIndex = _BankingPickupCheckGridCurrentColumnIndex.Value
        SpreadColumnCustomise(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, "Total", gcintColumnWidthMoney, False)
        SpreadColumnMoney(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, True, gstrFarPointMoneyNullDisplay)
    End Sub

    Friend Overridable Sub SetTotalsColumnFormulaIgnoringTotalEnteredAllPickupsAndCommentsRows()
        Dim TotalEnteredAllPickupsRow As Integer = 27 'IGNORE - total entered (all pickups)

        For RowIndex As Integer = 1 To _CommentsRowIndex - 1 Step 1  ' _CurrentSheet.RowCount 
            If RowIndex <> TotalEnteredAllPickupsRow Then
                SpreadCellFormula(_CurrentSheet, RowIndex, _BankingPickupCheckGridCurrentColumnIndex.Value, "SUM(R" & (RowIndex + 1).ToString & "C2:R" & (RowIndex + 1).ToString & "C" & _BankingPickupCheckGridCurrentColumnIndex.Value.ToString.Trim & ")")
            End If
        Next
    End Sub

    Friend Overridable Sub InitialiseCurrentColumnIndex()

        _BankingPickupCheckGridCurrentColumnIndex = 0
    End Sub

    Friend Overridable Sub SetupActiveCashiersEndOfDayPickupsCells()

        'cashier e.o.d pickup(s)
        For Each ActivePickup As Pickup In _ActiveCashier.EndOfDayPickupList
            SetActivePickupBag(ActivePickup)
            AddActiveCashierPickupColumn("E.O.D Pickup")
            AddActiveCashierBagCheck()
            If _IsFirstEndOfDayPickup = True Then
                SetIsFirstEndOfDayPickup(False)    'first column for this cashier will be tagged
                SetActiveCashierTotalForEnteredAllPickupsFormula()
                SetActiveCashierFloatAndSalesValuesAndVarianceFormula()
            Else
                AmendTotalForEnteredFirstEndOfDayPickupPickupsFormula()
            End If
            SetupActivePickupBagsPickupSalesCells(True)
            SetupActivePickupBagsCommentsCell()
        Next
    End Sub

    Friend Overridable Sub SetupActiveCashiersCashDropsCells()

        'cashier - cash drop(s)
        For Each ActivePickup As Pickup In _ActiveCashier.CashDropsPickupList
            SetActivePickupBag(ActivePickup)
            AddActiveCashierPickupColumn("Cash Drop")
            AddActiveCashierBagCheck()
            AmendTotalForEnteredFirstEndOfDayPickupPickupsFormula()
            SetupActivePickupBagsPickupSalesCells(False)
            SetupActivePickupBagsCommentsCell()
        Next
    End Sub

    Friend Overridable Sub AddActiveCashierPickupColumn(ByVal Caption As String)

        If ActiveCashierIsSet() And ActivePickupBagIsSet() Then

            With _ActiveCashier
                AddColumnToBankingPickupGrid()
                SpreadColumnEditable(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, True)
                SpreadColumnCustomise(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, GetActiveCashierPickupColumnContents(Caption), gcintColumnWidthPickupWide, False)
                SpreadColumnMoney(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, True, gstrFarPointMoneyNullDisplay)
                SpreadColumnTagValue(_CurrentSheet, _BankingPickupCheckGridCurrentColumnIndex.Value, GetPickupColumnTagForActiveCashierAndActivePickupBag)
            End With
        End If
    End Sub

    Friend Overridable Sub AddActiveCashierBagCheck()

        SpreadCellButton(_CurrentSheet, 0, _BankingPickupCheckGridCurrentColumnIndex.Value, False)
        SpreadCellFormula(_CurrentSheet, 14, _BankingPickupCheckGridCurrentColumnIndex.Value, "SUM(R2C:R14C)")
        SpreadCellFormula(_CurrentSheet, 25, _BankingPickupCheckGridCurrentColumnIndex.Value, "SUM(R16C:R25C)")
        SpreadCellFormula(_CurrentSheet, 26, _BankingPickupCheckGridCurrentColumnIndex.Value, "R15C+R26C")
    End Sub

    Friend Overridable Sub SetActiveCashierTotalForEnteredAllPickupsFormula()

        SpreadCellFormula(_CurrentSheet, 27, _BankingPickupCheckGridCurrentColumnIndex.Value, "R27C" & (_BankingPickupCheckGridCurrentColumnIndex.Value + 1).ToString)  'row - total entered (all pickups)
    End Sub

    Friend Overridable Sub SetActiveCashierFloatAndSalesValuesAndVarianceFormula()

        If ActiveCashierIsSet() Then
            With _ActiveCashier
                SpreadCellValue(_CurrentSheet, 28, _BankingPickupCheckGridCurrentColumnIndex.Value, .StartFloatValue)
                SpreadCellValue(_CurrentSheet, 29, _BankingPickupCheckGridCurrentColumnIndex.Value, .NewFloatValue)
                SpreadCellValue(_CurrentSheet, 30, _BankingPickupCheckGridCurrentColumnIndex.Value, .SystemSales)
            End With
            SpreadCellFormula(_CurrentSheet, 31, _BankingPickupCheckGridCurrentColumnIndex.Value, "R28C+R30C-R29C-R31C")                   'row - variance
        End If
    End Sub

    Friend Overridable Sub AmendTotalForEnteredFirstEndOfDayPickupPickupsFormula()

        'row - total entered (all pickups), amend
        For ColumnIndex As Integer = _BankingPickupCheckGridCurrentColumnIndex.Value To 0 Step -1
            If PickupColumnIsFirstEndOfDayPickup(_CurrentSheet.Columns(ColumnIndex).Tag) Then
                SpreadCellFormulaAppend(_CurrentSheet, 27, ColumnIndex, "+R27C" & (_BankingPickupCheckGridCurrentColumnIndex.Value + 1).ToString)
                Exit For
            End If
        Next
    End Sub

    Friend Overridable Sub SetupActivePickupBagsPickupSalesCells(ByVal LockUnAmendableNonCashTendersOnly As Boolean)

        If ActivePickupBagIsSet() Then
            For Each Sale As PickupSale In _ActivePickupBag.PickupSales
                SetActivePickupSale(Sale)
                'loop round rows for this pickup, find cash tender/denomination match
                For RowIndex As Integer = 1 To _CurrentSheet.RowCount - 1 Step 1
                    'no tag - ignore
                    If _CurrentSheet.Rows(RowIndex).Tag IsNot Nothing Then
                        'tag - string type
                        If _CurrentSheet.Rows(RowIndex).Tag.GetType.ToString <> "System.String" Then
                            Dim TenderDenom = CType(_CurrentSheet.Rows(RowIndex).Tag, TenderDenominationList)

                            If TenderDenom IsNot Nothing Then
                                With TenderDenom
                                    If .IsUnAmendableNonCashTenderType And (LockUnAmendableNonCashTendersOnly Or Not .IsCashTenderType) Then
                                        SpreadCellLocked(_CurrentSheet, RowIndex, _BankingPickupCheckGridCurrentColumnIndex.Value, True)
                                    End If
                                    ' Set corresponding cell value for any sales value
                                    If .TenderID = _ActivePickupSale.TenderID _
                                    And .DenominationID = _ActivePickupSale.DenominationID Then
                                        With _ActivePickupSale
                                            If .PickupValue.HasValue Then
                                                SpreadCellValue(_CurrentSheet, RowIndex, _BankingPickupCheckGridCurrentColumnIndex.Value, .PickupValue)
                                            End If
                                        End With
                                    End If
                                End With
                            End If
                        End If
                    End If
                Next
            Next
        End If
    End Sub

    Friend Overridable Sub SetupActivePickupBagsCommentsCell()

        If ActivePickupBagIsSet() And BankingPickupCheckGridCurrentColumnIndexIsSet() Then
            Dim CommentsColumnIndex As Integer = _BankingPickupCheckGridCurrentColumnIndex.Value

            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex, _NumberOfCommentsRowsToAdd, CommentsColumnIndex, 1)
            SpreadCellSetAsTextFormat(_CurrentSheet, _CommentsRowIndex, CommentsColumnIndex)
            SpreadCellSetAsMultiLine(_CurrentSheet, _CommentsRowIndex, CommentsColumnIndex, True)
            SpreadCellValue(_CurrentSheet, _CommentsRowIndex, CommentsColumnIndex, _ActivePickupBag.PickupComment)
        End If
    End Sub

    Friend Overridable Sub AddRowToBankingPickupGrid()

        If BankingPickupCheckGridCurrentRowIndexIsSet() Then
            Dim CurrentRowIndex As Integer = _BankingPickupCheckGridCurrentRowIndex.Value

            SpreadRowAdd(_CurrentSheet, CurrentRowIndex)
            _BankingPickupCheckGridCurrentRowIndex = CurrentRowIndex
        End If
    End Sub

    Friend Overridable Sub AddRowsToBankingPickupGrid(ByVal NumberOfRowsToAdd As Integer)

        If BankingPickupCheckGridCurrentRowIndexIsSet() Then
            Dim CurrentRowIndex As Integer = _BankingPickupCheckGridCurrentRowIndex.Value

            SpreadRowAdd(_CurrentSheet, CurrentRowIndex, NumberOfRowsToAdd)
            _BankingPickupCheckGridCurrentRowIndex = CurrentRowIndex
        End If
    End Sub

    Friend Overridable Sub AddColumnToBankingPickupGrid()

        If BankingPickupCheckGridCurrentColumnIndexIsSet() Then
            Dim CurrentColumnIndex As Integer = _BankingPickupCheckGridCurrentColumnIndex.Value

            SpreadColumnAdd(_CurrentSheet, CurrentColumnIndex)
            _BankingPickupCheckGridCurrentColumnIndex = CurrentColumnIndex
        End If
    End Sub

    Friend Overridable Function GetActiveCashierPickupColumnContents(ByVal Caption As String) As String

        GetActiveCashierPickupColumnContents = ""
        If ActiveCashierIsSet() And ActivePickupBagIsSet() Then
            Return GetActiveCashierEmployeeCodeAndUserName() & vbCrLf & GetActivePickupBagEndOfDayPickupSealNumberAndPickupPeriodIDAndPickupDate(Caption)
        End If
    End Function

    Friend Overridable Function GetActiveCashierEmployeeCodeAndUserName() As String

        GetActiveCashierEmployeeCodeAndUserName = ""
        If ActiveCashierIsSet() Then
            With _ActiveCashier
                GetActiveCashierEmployeeCodeAndUserName = .CashierEmployeeCode & " " & .CashierUserName
            End With
        End If
    End Function

    Friend Overridable Function GetActivePickupBagEndOfDayPickupSealNumberAndPickupPeriodIDAndPickupDate(ByVal Caption As String) As String

        GetActivePickupBagEndOfDayPickupSealNumberAndPickupPeriodIDAndPickupDate = ""
        If ActivePickupBagIsSet() Then
            With _ActivePickupBag
                GetActivePickupBagEndOfDayPickupSealNumberAndPickupPeriodIDAndPickupDate = _
                    Caption & vbCrLf & _
                    "(" & .PickupSealNumber & ")" & vbCrLf & _
                    .PickupPeriodID.ToString & " : " & _
                    .PickupDate.ToShortDateString
            End With
        End If
    End Function

    Friend Overridable Function GetPickupColumnTagForActiveCashierAndActivePickupBag() As String

        GetPickupColumnTagForActiveCashierAndActivePickupBag = ""
        If ActiveCashierIsSet() And ActivePickupBagIsSet() Then
            GetPickupColumnTagForActiveCashierAndActivePickupBag = _
                CreatePickupColumnTag(_ActiveCashier.CashierID, _ActivePickupBag.PickupID, _IsFirstEndOfDayPickup)
        End If
    End Function

    Friend Overridable Function CreatePickupColumnTag(ByVal CashierID As Integer, ByVal PickupID As Integer, ByVal IsFirstEndOfDayPickup As Boolean) As String
        Dim ColumnTag As New StringBuilder

        With ColumnTag
            .Append(CashierID.ToString)
            .Append("|")
            .Append(PickupID.ToString)
            .Append("|")
            .Append(CType(IIf(IsFirstEndOfDayPickup = True, "1", "0"), String))
        End With

        Return ColumnTag.ToString
    End Function

    Friend Overridable Function PickupColumnIsFirstEndOfDayPickup(ByVal ColumnTag As Object) As Boolean

        PickupColumnIsFirstEndOfDayPickup = False
        If ColumnTag IsNot Nothing Then
            Dim TagParams() As String = ColumnTag.ToString.Split("|"c)

            If TagParams.Length > 2 Then
                Dim IsFirstEndOfDayParam As Integer

                If Integer.TryParse(TagParams(2), IsFirstEndOfDayParam) Then
                    If IsFirstEndOfDayParam = 1 Then
                        PickupColumnIsFirstEndOfDayPickup = True
                    End If
                End If
            End If
        End If
    End Function

    Friend Overridable Sub SetBagCheckRowIndexToCurrentRowIndex()

        If BankingPickupCheckGridCurrentRowIndexIsSet() Then
            _BagCheckRowIndex = _BankingPickupCheckGridCurrentRowIndex.Value
        End If
    End Sub

    Friend Overridable Function BagNotAlreadyMarkedAsChecked(ByVal BagColumnIndex As Integer) As Boolean

        If BankingPickupCheckGridControlIsSet() And BagCheckRowIndexIsSet() Then
            If ColumnIsPotentiallyEditable(BagColumnIndex) Then
                If ActiveSheetIsSet() Then
                    With _BankingPickupCheckGrid.ActiveSheet
                        If .Cells(_BagCheckRowIndex.Value, BagColumnIndex) IsNot Nothing Then
                            With .Cells(_BagCheckRowIndex.Value, BagColumnIndex)
                                If TypeOf .CellType Is FarPoint.Win.Spread.CellType.CheckBoxCellType Then
                                    Return Not CType(.Value, Boolean)
                                End If
                            End With
                        End If
                    End With
                End If
            End If
        End If
    End Function

    Friend Overridable Sub SetUpActiveCellForTextEditingAndStart(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)

        SetBankingPickupGridActiveCell(CellRowIndex, CellColumnIndex)
        SetBankingPickupGridForTextCellEditing()
        StartEditingInBankingPickupGridActiveCell()
    End Sub

    Friend Overridable Sub ReSetBankingPickupGridActiveCellAfterTextCellEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)

        SetBankingPickupGridActiveCell(CellRowIndex, CellColumnIndex)
        ReSetBankingPickupGridAfterTextCellEditing()
    End Sub

    Friend Overridable Sub SetBankingPickupGridActiveCell(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)

        If BankingPickupCheckGridControlIsSet() AndAlso ActiveSheetIsSet() Then
            _BankingPickupCheckGrid.ActiveSheet.SetActiveCell(CellRowIndex, CellColumnIndex)
        End If
    End Sub

    Friend Overridable Sub SetBankingPickupGridForTextCellEditing()

        If BankingPickupCheckGridControlIsSet() Then
            With _BankingPickupCheckGrid
                .EditMode = True
                .EditModeReplace = False
                .EditModePermanent = True
            End With
        End If
    End Sub

    Friend Overridable Sub ReSetBankingPickupGridAfterTextCellEditing()

        If BankingPickupCheckGridControlIsSet() Then
            With _BankingPickupCheckGrid
                .EditMode = False
                .EditModeReplace = True
                .EditModePermanent = False
            End With
        End If
    End Sub

    Friend Overridable Sub StartEditingInBankingPickupGridActiveCell()

        If BankingPickupCheckGridControlIsSet() Then
            With _BankingPickupCheckGrid
                .ActiveSheet.ActiveCell.Editor.StartEditing(New EventArgs, False, True)
            End With
        End If
    End Sub
#End Region
#End Region

End Class