﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("NewBanking.Form.UnitTest")> 
Public Class SafeCheckReportUIView
    Implements ISafeCheckReportUI


    Friend _View As SafeCheckReportView

    Friend _SelectedPeriod As Period
    Friend _StoreID As String
    Friend _StoreName As String

#Region "Interface"

    Public Overridable Sub RunReport(ByRef SelectedPeriod As Period, _
                                     ByVal StoreID As String, _
                                     ByVal StoreName As String) Implements ISafeCheckReportUI.RunReport

        SetSelectedPeriod(SelectedPeriod)
        SetStoreID(StoreID)
        SetStoreName(StoreName)

        CreateView()
        ShowView()

    End Sub

    Public Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                              ByVal SheetView As SheetView, _
                                              ByRef Index As Integer, _
                                              ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceComments

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal SelectedPeriod As Period) Implements ISafeCheckReportUI.DisplaySafeMaintenanceAuthorisers

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, _
                                                        ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceScannedBankingBags

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceCommentsForAllBags

        'N/A

    End Sub

#End Region

#Region "Private Functions & Procedures"

    Friend Overridable Sub SetSelectedPeriod(ByVal SelectedPeriod As Core.Period)

        _SelectedPeriod = SelectedPeriod

    End Sub

    Friend Overridable Sub SetStoreID(ByVal StoreID As String)

        _StoreID = StoreID

    End Sub

    Friend Overridable Sub SetStoreName(ByVal StoreName As String)

        _StoreName = StoreName

    End Sub

    Friend Overridable Sub CreateView()

        _View = New SafeCheckReportView(_SelectedPeriod, _StoreID, _StoreName)

    End Sub

    Friend Overridable Sub ShowView()

        _View.ShowDialog()

    End Sub

#End Region

   
End Class