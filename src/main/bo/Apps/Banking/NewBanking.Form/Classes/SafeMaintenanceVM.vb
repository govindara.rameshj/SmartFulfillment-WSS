﻿Public Class SafeMaintenanceVM
    Implements ISafeMaintenanceVM

#Region "Banking Bags"

    Friend _ScanEngine As IBagScanningEngine
    Friend _BankingBagList As FpSpread

#Region "Engine"

    Public Sub InitialiseBSE(ByVal PeriodID As Integer) Implements ISafeMaintenanceVM.InitialiseBSE

        _ScanEngine = (New BagScanningEngineFactory).GetImplementation

        _ScanEngine.Initialise(PeriodID, IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess)

    End Sub

    Public Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean Implements ISafeMaintenanceVM.BankingBagAlreadyScanned

        Return _ScanEngine.BankingBagAlreadyScanned(SealNumber)

    End Function

    Public Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean Implements ISafeMaintenanceVM.BankingBagExistInSafe

        Return _ScanEngine.BankingBagExistInSafe(SealNumber)

    End Function

    Public Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean Implements ISafeMaintenanceVM.BankingBagHasValidSeal

        Return _ScanEngine.BankingBagHasValidSeal(SealNumber)

    End Function

    Public Function MatchSystemBankingExpectation() As Boolean Implements ISafeMaintenanceVM.MatchSystemBankingExpectation

        Return _ScanEngine.MatchSystemBankingExpectation

    End Function

    Public Sub CreateBankingBag(ByVal SealNumber As String) Implements ISafeMaintenanceVM.CreateBankingBag

        _ScanEngine.CreateBankingBag(SealNumber)

    End Sub

    Public Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String) Implements ISafeMaintenanceVM.CreateBankingBag

        _ScanEngine.CreateBankingBag(SealNumber, SealComment)

    End Sub

    Public Sub CreateBankingComment(ByVal Value As String) Implements ISafeMaintenanceVM.CreateBankingComment

        _ScanEngine.CreateBankingComment(Value)

    End Sub

    Public Sub SafeMaintenancePersist(ByRef OdbcConnection As clsOasys3DB) Implements ISafeMaintenanceVM.SafeMaintenancePersist

        _ScanEngine.SafeMaintenancePersist(OdbcConnection)

    End Sub

#End Region

#Region "View"

    Public Sub SetBankingBagListControl(ByRef BankingBagsUI As FarPoint.Win.Spread.FpSpread) Implements ISafeMaintenanceVM.SetBankingBagListControl

        _BankingBagList = BankingBagsUI

    End Sub

    Public Sub FormatBankingBagListControl() Implements ISafeMaintenanceVM.FormatBankingBagListControl

        BagListFormat(_BankingBagList)

    End Sub

    Public Sub PopulateBankingBagListControl() Implements ISafeMaintenanceVM.PopulateBankingBagListControl

        BagListPopulate(_BankingBagList, _ScanEngine.BankingBagList)

    End Sub

#End Region

#Region "Private Functions & Procedures"

    Friend Overridable Sub BagListFormat(ByRef Grid As FpSpread)

        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, "Seal Number", 125, False)
        SpreadColumnCustomise(NewSheet, 1, "Seal Comment", 755, False)       '886

        SpreadGridSheetAdd(Grid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(Grid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

    End Sub

    Friend Overridable Sub BagListPopulate(ByRef Grid As FpSpread, ByRef BagData As List(Of ISafeBagScannedModel))

        Dim ActiveSheet As SheetView
        Dim RowIndex As Integer = 0

        ActiveSheet = Grid.ActiveSheet
        SpreadSheetClearDown(ActiveSheet)

        For Each obj As ISafeBagScannedModel In BagData

            SpreadRowAdd(ActiveSheet, RowIndex)

            SpreadRowTagValue(ActiveSheet, RowIndex, obj.ID)
            SpreadCellValue(ActiveSheet, RowIndex, 0, obj.SealNumber)
            SpreadCellValue(ActiveSheet, RowIndex, 1, obj.Comment)

        Next

    End Sub

#End Region

#End Region

#Region "Rest"

    Friend _CurrentSafeMaintenanceEngine As ISafeMaintenanceEngine
    Friend _SelectedSafeMaintenanceEngine As ISafeMaintenanceEngine

    Friend _MiscellaneousPropertyControl As TextBox
    Friend _GiftTokenSealControl As TextBox
    Friend _CommentsControl As TextBox

#Region "ISafeMaintenanceVM Implemetation: Engine"

    Public Function CreateCurrentSafeMaintenance(ByVal PeriodID As Integer) As Boolean Implements ISafeMaintenanceVM.CreateCurrentSafeMaintenance

        Try
            _CurrentSafeMaintenanceEngine = (New SafeMaintenanceEngineFactory).GetImplementation
            _CurrentSafeMaintenanceEngine.CreateSafeMaintenance(PeriodID)
            CreateCurrentSafeMaintenance = True
        Catch ex As Exception
            CreateCurrentSafeMaintenance = False
        End Try

    End Function

    Public Function CreateSelectedSafeMaintenance(ByVal PeriodID As Integer) As Boolean Implements ISafeMaintenanceVM.CreateSelectedSafeMaintenance

        Try

            _SelectedSafeMaintenanceEngine = (New SafeMaintenanceEngineFactory).GetImplementation
            _SelectedSafeMaintenanceEngine.CreateSafeMaintenance(PeriodID)
            CreateSelectedSafeMaintenance = True

        Catch ex As Exception

            CreateSelectedSafeMaintenance = False

        End Try

    End Function

    Public Function Persist() As Boolean Implements ISafeMaintenanceVM.Persist

        Try
            If CurrentSafeMaintenanceEngineIsCreated() Then
                SetSafeMaintenanceEngineMiscellaneousProperty()
                SetSafeMaintenanceEngineGiftTokenSeal()
                SetSafeMaintenanceEngineComments()
                Persist = _CurrentSafeMaintenanceEngine.Persist
            End If
        Catch ex As Exception
            Persist = False
        End Try
    End Function

    Public Sub Persist(ByRef OdbcConnection As clsOasys3DB) Implements ISafeMaintenanceVM.Persist

        If CurrentSafeMaintenanceEngineIsCreated() Then

            SetSafeMaintenanceEngineMiscellaneousProperty()
            SetSafeMaintenanceEngineGiftTokenSeal()
            SetSafeMaintenanceEngineComments()

            _CurrentSafeMaintenanceEngine.Persist(OdbcConnection)

        End If

    End Sub

    Public Function LoadCurrentSafe(ByVal PeriodID As Integer) As Boolean Implements ISafeMaintenanceVM.LoadCurrentSafe

        If CurrentSafeMaintenanceEngineIsCreated() Then
            LoadCurrentSafe = _CurrentSafeMaintenanceEngine.Load(PeriodID)
        End If
    End Function

    Public Function LoadSelectedSafe(ByVal PeriodID As Integer) As Boolean Implements ISafeMaintenanceVM.LoadSelectedSafe

        If SelectedSafeMaintenanceEngineIsCreated() = True Then Return _SelectedSafeMaintenanceEngine.Load(PeriodID)

    End Function

#End Region

#Region "ISafeMaintenanceVM Implemetation: View"

    Public Sub PopulateCommentsControl() Implements ISafeMaintenanceVM.PopulateCommentsControl

        _CommentsControl.Text = _CurrentSafeMaintenanceEngine.Comments

    End Sub

    Public Sub PopulateGiftTokenSealControl() Implements ISafeMaintenanceVM.PopulateGiftTokenSealControl

        _GiftTokenSealControl.Text = _CurrentSafeMaintenanceEngine.GiftTokenSeal

    End Sub

    Public Sub PopulateMiscellaneousPropertyControl() Implements ISafeMaintenanceVM.PopulateMiscellaneousPropertyControl

        _MiscellaneousPropertyControl.Text = _CurrentSafeMaintenanceEngine.MiscellaneousProperty

    End Sub

    Public Sub SetCommentsControl(ByRef CommentsUI As System.Windows.Forms.TextBox) Implements ISafeMaintenanceVM.SetCommentsControl

        _CommentsControl = CommentsUI
    End Sub

    Public Sub SetGiftTokenSealControl(ByRef GiftTokenSealUI As System.Windows.Forms.TextBox) Implements ISafeMaintenanceVM.SetGiftTokenSealControl

        _GiftTokenSealControl = GiftTokenSealUI
    End Sub

    Public Sub SetMiscellaneousPropertyControl(ByRef MicellaneousPropertyUI As System.Windows.Forms.TextBox) Implements ISafeMaintenanceVM.SetMiscellaneousPropertyControl

        _MiscellaneousPropertyControl = MicellaneousPropertyUI
    End Sub

    Public Function SafeMaintenanceAlreadyPerformed() As Boolean Implements ISafeMaintenanceVM.SafeMaintenanceAlreadyPerformed

        Return _SelectedSafeMaintenanceEngine.SafeMaintenanceAlreadyPerformed

    End Function

#End Region

#Region "Internals"

    Friend Overridable Function CurrentSafeMaintenanceEngineIsCreated() As Boolean

        Return _CurrentSafeMaintenanceEngine IsNot Nothing

    End Function

    Friend Overridable Function SelectedSafeMaintenanceEngineIsCreated() As Boolean

        Return _SelectedSafeMaintenanceEngine IsNot Nothing

    End Function


    Friend Overridable Sub SetSafeMaintenanceEngineMiscellaneousProperty()

        _CurrentSafeMaintenanceEngine.MiscellaneousProperty = _MiscellaneousPropertyControl.Text

    End Sub

    Friend Overridable Sub SetSafeMaintenanceEngineGiftTokenSeal()

        _CurrentSafeMaintenanceEngine.GiftTokenSeal = _GiftTokenSealControl.Text

    End Sub

    Friend Overridable Sub SetSafeMaintenanceEngineComments()

        _CurrentSafeMaintenanceEngine.Comments = _CommentsControl.Text

    End Sub

#End Region

#End Region

End Class