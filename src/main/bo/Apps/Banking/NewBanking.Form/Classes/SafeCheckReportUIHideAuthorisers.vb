﻿Public Class SafeCheckReportUIHideAuthorisers
    Implements ISafeCheckReportUI


#Region "Interface"

    Public Sub RunReport(ByRef SelectedPeriod As Period, _
                         ByVal StoreID As String, _
                         ByVal StoreName As String) Implements ISafeCheckReportUI.RunReport

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                              ByVal SheetView As SheetView, _
                                              ByRef Index As Integer, _
                                              ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceComments

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal SelectedPeriod As Period) Implements ISafeCheckReportUI.DisplaySafeMaintenanceAuthorisers
        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, _
                                                        ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceScannedBankingBags

        'N/A

    End Sub
    Public Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceCommentsForAllBags

        'N/A

    End Sub

#End Region

   
End Class
