﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("NewBanking.Form.UnitTest")> 
Public Class BankingBagInfo

    Friend _SafeTotal As Nullable(Of Decimal)
    Friend _SafeTenderID As Integer
    Friend _SafeDenominationID As Decimal

    Friend _PickupTotal As Nullable(Of Decimal)
    Friend _PickupSafeMinimum As Decimal
    Friend _PickupBullionMultiple As Decimal

    Public Sub New(ByVal Safe As SafeDenomination, ByVal Pickup As TenderDenominationListCollection)

        _SafeTotal = Safe.SystemSafe
        _SafeTenderID = Safe.TenderID
        _SafeDenominationID = Safe.DenominationID



        Dim PickupLine As TenderDenominationList = Pickup.TenderDenom(Safe.TenderID, Safe.DenominationID)

        If PickupLine IsNot Nothing Then _PickupTotal = PickupLine.Amount
        If PickupLine IsNot Nothing Then _PickupSafeMinimum = PickupLine.SafeMinimum
        If PickupLine IsNot Nothing Then _PickupBullionMultiple = PickupLine.BullionMultiple

    End Sub

#Region "Private Properties"

    Private ReadOnly Property PickupTotal() As Decimal

        Get

            If _PickupTotal.HasValue = False Then Return 0

            Return _PickupTotal.Value

        End Get

    End Property

    Private ReadOnly Property SafeTotal() As Decimal

        Get

            If _SafeTotal.HasValue = False Then Return 0

            Return _SafeTotal.Value

        End Get

    End Property

#End Region

#Region "Public Properties"

    ReadOnly Property SystemTotal() As Decimal

        Get

            Return Me.SafeTotal + Me.PickupTotal

        End Get

    End Property

    ReadOnly Property SuggestedTotal() As Decimal

        Get

            Select Case _SafeTenderID

                Case 1

                    Return CashTenderSuggestedAmount((Me.SafeTotal + Me.PickupTotal) - _PickupSafeMinimum, _PickupBullionMultiple, _SafeDenominationID)

                Case Else

                    Return Me.SystemTotal

            End Select

        End Get

    End Property

#End Region

#Region "Private Procedures & Functions"

    Private Function CashTenderSuggestedAmount(ByVal decAmount As Decimal, ByVal BankingMultiple As Decimal, ByVal decDenominationID As Decimal) As Decimal

        CashTenderSuggestedAmount = 0

        If BankingMultiple >= decDenominationID AndAlso decAmount >= BankingMultiple Then

            CashTenderSuggestedAmount = BankingMultiple * Math.Floor(decAmount / BankingMultiple)

        End If

    End Function

#End Region

End Class