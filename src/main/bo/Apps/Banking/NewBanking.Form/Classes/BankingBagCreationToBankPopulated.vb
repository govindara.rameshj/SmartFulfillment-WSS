﻿Public Class BankingBagCreationToBankPopulated
    Implements IBankingBagCreationUI

    Public Sub PopulateTenders(ByRef CurrentSheet As SheetView, _
                                   ByRef Pickup As TenderDenominationListCollection) Implements IBankingBagCreationUI.PopulateTenders

        Dim SafeDenom As SafeDenomination
        Dim Info As BankingBagInfo

        For RowIndex As Integer = 0 To CurrentSheet.RowCount - 1 Step 1

            If CurrentSheet.Rows(RowIndex).Tag Is Nothing Then Continue For

            SafeDenom = CType(CurrentSheet.Rows(RowIndex).Tag, NewBanking.Core.SafeDenomination)
            Info = New BankingBagInfo(SafeDenom, Pickup)

            CurrentSheet.Cells(RowIndex, 1).Value = Info.SystemTotal
            CurrentSheet.Cells(RowIndex, 2).Value = Info.SuggestedTotal
            CurrentSheet.Cells(RowIndex, 3).Value = Info.SuggestedTotal
            CurrentSheet.Cells(RowIndex, 4).Value = Info.SystemTotal - Info.SuggestedTotal

        Next

    End Sub

End Class