﻿Public Class CashDropVMWithoutComments
    Implements ICashDropVM

    Friend _CashDropGrid As FpSpread

    Public Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.Pickup) Implements ICashDropVM.AssignCommentsFromGrid

    End Sub

    Public Sub CashDropFormFormat() Implements ICashDropVM.CashDropFormFormat

    End Sub

    Public Sub CashDropGridFormat() Implements ICashDropVM.CashDropGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.CashDropInputGridColumn1, gcintColumnWidthCashTender, False)     'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.CashDropInputGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - Tray Entry
        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnEditable(NewSheet, 1, True)

        SpreadGridSheetAdd(_CashDropGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_CashDropGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(_CashDropGrid)
    End Sub

    Public Sub CashDropGridPopulate() Implements ICashDropVM.CashDropGridPopulate
        Dim colTenderDenomination As TenderDenominationListCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        colTenderDenomination = New TenderDenominationListCollection
        colTenderDenomination.LoadData()

        CurrentSheet = _CashDropGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As TenderDenominationList In colTenderDenomination
            If obj.TenderID <> 1 Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)               'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)   'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, 0)                'Screen Column - Tray Entry
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(_CashDropGrid, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)

        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total")           'Screen Column - Denomination
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)") 'Screen Column - Tray Entry, sum denominations
    End Sub

    Public Sub SetCashDropForm(ByRef CashDropForm As System.Windows.Forms.Form) Implements ICashDropVM.SetCashDropForm

    End Sub

    Public Sub SetCashDropGridControl(ByRef CashDropGrid As FarPoint.Win.Spread.FpSpread) Implements ICashDropVM.SetCashDropGridControl

        _CashDropGrid = CashDropGrid
    End Sub

    Public Function CashDropGridActiveCellIsCommentsCell() As Boolean Implements ICashDropVM.CashDropGridActiveCellIsCommentsCell

        CashDropGridActiveCellIsCommentsCell = False
    End Function

    Public Function GetCashDropGridTotalsRowIndex() As Integer Implements ICashDropVM.GetCashDropGridTotalsRowIndex

        Return _CashDropGrid.ActiveSheet.RowCount - 1
    End Function
End Class
