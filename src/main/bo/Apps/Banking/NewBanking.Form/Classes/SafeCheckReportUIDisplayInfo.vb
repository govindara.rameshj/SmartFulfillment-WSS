﻿Public Class SafeCheckReportUIDisplayInfo
    Implements ISafeCheckReportUI
#Region "Private constants"
    Private Const Comments As String = "Comments"
    Private Const Bags As String = "Bags(Seal & Type)"
#End Region

#Region "Interface"

    Public Sub RunReport(ByRef SelectedPeriod As Period, _
                         ByVal StoreID As String, _
                         ByVal StoreName As String) Implements ISafeCheckReportUI.RunReport

    End Sub

    Public Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                              ByVal SheetView As SheetView, _
                                              ByRef Index As Integer, _
                                              ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceComments

        Dim MiscellaneousProperty As String = "Miscellaneous Property"
        Dim GiftTokenSeal As String = "Gift Token Seal"


        Dim Repository As New SafeCheckReportRepository
        Dim DT As DataTable


        DT = Repository.GetSafeCheckRecordComments(PeriodID)
        SpreadRowDrawLine(SheetView, Index, Color.Black, 1)

        SpreadRowAdd(SheetView, Index)
        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadCellValue(SheetView, Index, 0, MiscellaneousProperty)
        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadRowMergeColumns(SheetView, Index, 1, 4)
        SpreadCellValue(SheetView, Index, 1, DT.Rows(0)(0).ToString)

        Index += 1
        AddNewRowAndMakeFirstCellBold(Grid, SheetView, Index)
        SpreadCellValue(SheetView, Index, 0, GiftTokenSeal)
        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadRowMergeColumns(SheetView, Index, 1, 4)
        SpreadCellValue(SheetView, Index, 1, DT.Rows(0)(1).ToString)

        Index += 1
        AddNewRowAndMakeFirstCellBold(Grid, SheetView, Index)
        SpreadCellValue(SheetView, Index, 0, Comments)
        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadRowMergeColumns(SheetView, Index, 1, 4)
        SpreadCellValue(SheetView, Index, 1, DT.Rows(0)(2).ToString)

        SpreadRowDrawLine(SheetView, Index, Color.Black, 1)
        Index += 1
        AddNewRowAndMakeTheTextBold(Grid, SheetView, Index)
        SpreadRowDrawLine(SheetView, Index, Color.Black, 1)

    End Sub

    Public Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal SelectedPeriod As Period) Implements ISafeCheckReportUI.DisplaySafeMaintenanceAuthorisers

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, _
                                                        ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceScannedBankingBags

        BankingTitle(Grid, SheetView, Index)

        LoadBankingSummary(PeriodID, Grid, SheetView, Index)

        BankingBagsHeader(Grid, SheetView, Index)
        LoadBankingBags(PeriodID, SheetView, Index)

        BankingCommentsHeader(Grid, SheetView, Index)
        LoadBankingComments(PeriodID, SheetView, Index)

        BankingFooter(SheetView, Index)

    End Sub
    Public Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FarPoint.Win.Spread.FpSpread, _
                                                        ByVal SheetView As FarPoint.Win.Spread.SheetView, _
                                                        ByRef Index As Integer, ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceCommentsForAllBags

        Dim bagsHeldInSafe As BagsHeldInSafeCollection

        SpreadRowAdd(SheetView, Index)
        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadCellValue(SheetView, Index, 0, Bags)
        SpreadCellValue(SheetView, Index, 4, Comments)
        SpreadCellSetAsBold(Grid, SheetView, Index, 4)
        Grid.HorizontalScrollBarPolicy = ScrollBarPolicy.AsNeeded
        bagsHeldInSafe = New BagsHeldInSafeCollection
        bagsHeldInSafe = LoadBagsHeldInSafe(bagsHeldInSafe, PeriodID)
        For Each bag As BagsHeldInSafe In bagsHeldInSafe
            SpreadRowAdd(SheetView, Index)
            SpreadCellValue(SheetView, Index, 0, bag.SealNumber & " " & BagType(bag.BagType))
            SpreadCellValue(SheetView, Index, 1, bag.BagValue)
            SpreadColumnSetResizable(SheetView, 4, True)
            SpreadCellValue(SheetView, Index, 4, bag.Comments)
        Next


    End Sub

#End Region

#Region "Private Functions & Procedures"

#Region "Display Safe Maintenance Scanned Banking Bags"

    Private Sub BankingTitle(ByVal Grid As FpSpread, ByVal SheetView As SheetView, ByRef Index As Integer)

        SpreadRowDrawLine(SheetView, Index, Color.Black, 1)
        SpreadRowAdd(SheetView, Index)
        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

        SpreadCellValue(SheetView, Index, 0, "Banking Bags (Scanned)")

        SpreadRowAdd(SheetView, Index)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

    End Sub

    Private Sub BankingBagsHeader(ByVal Grid As FpSpread, ByVal SheetView As SheetView, ByRef Index As Integer)

        SpreadRowAdd(SheetView, Index)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

        SpreadRowAdd(SheetView, Index)

        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadCellAlignCenter(SheetView, Index, 0)

        SpreadCellSetAsBold(Grid, SheetView, Index, 1)
        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadCellAlignCenter(SheetView, Index, 1)

        SpreadCellSetAsBold(Grid, SheetView, Index, 2)
        SpreadCellSetAsTextFormat(SheetView, Index, 2)
        SpreadRowMergeColumns(SheetView, Index, 2, 4)

        SpreadCellValue(SheetView, Index, 0, "Seal Number")
        SpreadCellValue(SheetView, Index, 1, "Seal Value")
        SpreadCellValue(SheetView, Index, 2, "Seal Comment")

    End Sub

    Private Sub BankingCommentsHeader(ByVal Grid As FpSpread, ByVal SheetView As SheetView, ByRef Index As Integer)

        SpreadRowAdd(SheetView, Index)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

        SpreadRowAdd(SheetView, Index)
        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

        SpreadCellValue(SheetView, Index, 0, "General Comments")

    End Sub

    Private Sub BankingFooter(ByVal SheetView As SheetView, ByRef Index As Integer)

        SpreadRowAdd(SheetView, Index)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)
        SpreadRowDrawLine(SheetView, Index, Color.Black, 1)

        SpreadRowAdd(SheetView, Index)
        SpreadRowMergeColumns(SheetView, Index, 0, 4)

    End Sub

    Private Sub AddBankingSummary(ByVal Grid As FpSpread, _
                                  ByVal SheetView As SheetView, _
                                  ByRef Index As Integer, _
                                  ByVal Statistic As String, _
                                  ByVal Value As Integer)

        SpreadRowAdd(SheetView, Index)

        SpreadCellSetAsBold(Grid, SheetView, Index, 0)

        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadCellAlignCenter(SheetView, Index, 1)

        SpreadRowMergeColumns(SheetView, Index, 2, 4)

        SpreadCellValue(SheetView, Index, 0, Statistic)
        SpreadCellValue(SheetView, Index, 1, Value.ToString)

    End Sub

    Private Sub AddBankingBag(ByVal SheetView As SheetView, _
                              ByRef Index As Integer, _
                              ByVal SealNumber As String, _
                              ByVal SealValue As Decimal, _
                              ByVal SealComment As String)

        SpreadRowAdd(SheetView, Index)

        SpreadCellAlignCenter(SheetView, Index, 0)

        SpreadCellSetAsTextFormat(SheetView, Index, 1)
        SpreadCellAlignCenter(SheetView, Index, 1)

        SpreadCellSetAsTextFormat(SheetView, Index, 2)
        SpreadRowMergeColumns(SheetView, Index, 2, 4)

        SpreadCellValue(SheetView, Index, 0, SealNumber)
        If SealValue <> 0 Then SpreadCellValue(SheetView, Index, 1, SealValue)
        SpreadCellValue(SheetView, Index, 2, SealComment)

    End Sub

    Private Sub AddBankingComment(ByVal SheetView As SheetView, ByRef Index As Integer, ByVal Comment As String)

        SpreadRowAdd(SheetView, Index)

        SpreadRowMergeColumns(SheetView, Index, 0, 4)
        SpreadCellValue(SheetView, Index, 0, Comment)

    End Sub

    Private Sub LoadBankingSummary(ByVal PeriodID As Integer, ByVal Grid As FpSpread, ByVal SheetView As SheetView, ByRef Index As Integer)

        Dim Repository As New SafeCheckReportRepository
        Dim SummaryDT As DataTable

        SummaryDT = Repository.GetSafeCheckRecordScannedBankingSummary(PeriodID)

        For Each DR As DataRow In SummaryDT.Rows

            AddBankingSummary(Grid, SheetView, Index, DR.Item(1).ToString, CType(DR.Item(2).ToString, Integer))

        Next

    End Sub

    Private Sub LoadBankingBags(ByVal PeriodID As Integer, ByVal SheetView As SheetView, ByRef Index As Integer)

        Dim Repository As New SafeCheckReportRepository
        Dim BagsDT As DataTable
        Dim SealValue As Decimal

        BagsDT = Repository.GetSafeCheckRecordScannedBankingBags(PeriodID)

        If BagsDT Is Nothing Then Exit Sub

        For Each DR As DataRow In BagsDT.Rows

            If DR.Item(2).ToString.Length = 0 Then

                SealValue = 0

            Else

                SealValue = CType(DR.Item(2).ToString, Decimal)

            End If

            AddBankingBag(SheetView, Index, DR.Item(1).ToString, SealValue, DR.Item(3).ToString)

        Next

    End Sub

    Private Sub LoadBankingComments(ByVal PeriodID As Integer, ByVal SheetView As SheetView, ByRef Index As Integer)

        Dim Repository As New SafeCheckReportRepository
        Dim CommentsDT As DataTable

        CommentsDT = Repository.GetSafeCheckRecordScannedBankingComments(PeriodID)

        If CommentsDT Is Nothing Then Exit Sub

        For Each DR As DataRow In CommentsDT.Rows

            AddBankingComment(SheetView, Index, DR.Item(0).ToString)

        Next

    End Sub

#End Region

    Private Sub AddNewRowAndMakeTheTextBold(ByVal reportGrid As FarPoint.Win.Spread.FpSpread, ByVal reportsheetView As FarPoint.Win.Spread.SheetView, ByVal index As Integer)

        SpreadRowAdd(reportsheetView, index)
        SpreadRowBold(reportGrid, reportsheetView, index)

    End Sub

    Private Sub AddNewRowAndMakeFirstCellBold(ByVal reportGrid As FarPoint.Win.Spread.FpSpread, ByVal reportsheetView As FarPoint.Win.Spread.SheetView, ByVal index As Integer)

        SpreadRowAdd(reportsheetView, index)
        SpreadCellSetAsBold(reportGrid, reportsheetView, index, 0)

    End Sub

    Friend Overridable Function LoadBagsHeldInSafe(ByVal bagsHeldInSafe As BagsHeldInSafeCollection, ByVal periodId As Integer) As BagsHeldInSafeCollection
        bagsHeldInSafe.LoadData(periodId)
        Return bagsHeldInSafe

    End Function

#End Region


End Class
