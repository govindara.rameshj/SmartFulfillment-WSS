﻿Public Class CashDropCashierFilterVMWithNoSaleFilter
    Implements ICashDropCashierFilterVM

    Friend _CashiersToLoad As Core.CashierCollection
    Friend _BankingPeriodID As Nullable(Of Integer)

    Public Sub LoadCashierDropDown(ByRef CashiersToLoad As Core.CashierCollection, ByVal BankingPeriodID As Integer) Implements ICashDropCashierFilterVM.LoadCashierDropDown

        Initialise(CashiersToLoad, BankingPeriodID)
        If HaveInitialised() Then
            LoadCashiers()
        End If
    End Sub

    Friend Overridable Sub Initialise(ByRef CashiersToLoad As Core.CashierCollection, ByVal BankingPeriodID As Integer)

        SetCashiersToLoad(CashiersToLoad)
        SetBankingPeriodID(BankingPeriodID)
    End Sub

    Friend Overridable Function HaveInitialised() As Boolean

        Return CashiersToLoadIsSet() And BankingPeriodIDIsSet()
    End Function

    Friend Overridable Sub SetCashiersToLoad(ByVal CashiersToLoad As CashierCollection)

        _CashiersToLoad = CashiersToLoad
    End Sub

    Friend Overridable Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer)

        _BankingPeriodID = BankingPeriodID
    End Sub

    Friend Overridable Function CashiersToLoadIsSet() As Boolean

        Return _CashiersToLoad IsNot Nothing
    End Function

    Friend Overridable Function BankingPeriodIDIsSet() As Boolean

        Return _BankingPeriodID.HasValue
    End Function

    Friend Overridable Sub LoadCashiers()

        _CashiersToLoad.LoadDataCashDrop(_BankingPeriodID.Value)
    End Sub
End Class
