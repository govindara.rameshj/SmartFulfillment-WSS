﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("NewBanking.Form.UnitTest")> 
Public Class FloatAssignVMWithComments
    Implements IFloatAssignVM

    Friend _FloatAssignGrid As FpSpread
    Friend _FloatAssignForm As System.Windows.Forms.Form
    Friend _CommentsRowIndex As Integer
    Friend _CommentsColumnIndex As Integer

    Public Overridable Sub FloatAssignGridFormat() Implements IFloatAssignVM.FloatAssignGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 3, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatAssignInputGridColumn1, gcintColumnWidthCashTender, False)   'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatAssignInputGridColumn2, gcintColumnWidthMoney, False)        'Screen Column - Float
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.FloatAssignInputGridColumn3, gcintColumnWidthMoneyWide, False)    'Screen Column - Manual Check Float

        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 2, True, gstrFarPointMoneyNullDisplay)

        SpreadGridSheetAdd(_FloatAssignGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatAssignGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        SpreadGridInputMaps(_FloatAssignGrid)

        SpreadGridDeactiveKey(_FloatAssignGrid, Keys.F2)
        SpreadGridDeactiveKey(_FloatAssignGrid, Keys.F3)
    End Sub

    Public Overridable Sub FloatAssignGridPopulate(ByVal FloatID As Integer) Implements IFloatAssignVM.FloatAssignGridPopulate
        Dim TenderDenominationList As TenderDenominationListCollection
        Dim FloatBagContents As FloatBagCollection
        Dim CurrentSheet As SheetView
        Dim RowIndex As Integer = 0
        Dim FloatBagComments As String = String.Empty

        'load data
        TenderDenominationList = GetTenderDenominationListCollection()
        FloatBagContents = GetFloatBagCollection(FloatID)
        FloatBagComments = GetFloatBagComments(FloatID)

        CurrentSheet = _FloatAssignGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each TenderDenomination As TenderDenominationList In TenderDenominationList
            If TenderDenomination.TenderID = 1 Then
                SpreadRowAdd(CurrentSheet, RowIndex)
                SpreadRowTagValue(CurrentSheet, RowIndex, TenderDenomination)                                                     'Primary Key
                SpreadCellValue(CurrentSheet, RowIndex, 0, TenderDenomination.TenderText)                                         'Screen Column - Denomination
                SpreadCellValue(CurrentSheet, RowIndex, 1, FloatBagContents.DenominationValue(TenderDenomination.DenominationID)) 'Screen Column - Float
                SpreadCellValue(CurrentSheet, RowIndex, 2, New System.Nullable(Of Decimal))                                       'Screen Column - Manual Check Float
            End If
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, RowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, RowIndex)
        SpreadRowBold(_FloatAssignGrid, CurrentSheet, RowIndex)
        SpreadRowRemoveFocus(CurrentSheet, RowIndex)
        SpreadCellValue(CurrentSheet, RowIndex, 0, "Total Amounts")
        'screen column - float, sum denominations
        SpreadCellValue(CurrentSheet, RowIndex, 1, 0)
        SpreadCellFormula(CurrentSheet, RowIndex, 1, "SUM(R1C:R13C)")
        'line underneath totals
        SpreadRowDrawLine(CurrentSheet, RowIndex, Color.Black, 1)
        'add "Comments" row (actually 4 rows, to be merged in column blocks)
        _CommentsRowIndex = RowIndex + 1

        SpreadRowAdd(CurrentSheet, RowIndex, 4)
        SpreadCellMergeCells(CurrentSheet, _CommentsRowIndex, 4, 0, 1)
        SpreadCellSetAsBold(_FloatAssignGrid, CurrentSheet, _CommentsRowIndex, 0)
        SpreadCellSetVerticalAlignment(CurrentSheet, _CommentsRowIndex, 0, CellVerticalAlignment.Top)
        SpreadCellValue(CurrentSheet, _CommentsRowIndex, 0, "Comments")
        _CommentsColumnIndex = 1
        SpreadCellMergeCells(CurrentSheet, _CommentsRowIndex, 4, _CommentsColumnIndex, 2)
        SpreadCellSetAsTextFormat(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex)
        SpreadCellSetAsMultiLine(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex, True)
        SpreadCellValue(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex, FloatBagComments)

        SpreadCellMakeActive(CurrentSheet, 0, 2)      'default active cursor to the 100 pounds denom row, "manual check" column
    End Sub

    Public Overridable Sub SetFloatAssignGridControl(ByRef FloatAssignGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatAssignVM.SetFloatAssignGridControl

        _FloatAssignGrid = FloatAssignGrid
    End Sub

    Public Overridable Sub FloatAssignGridSetupCommentsCell(ByVal MakeEditable As Boolean) Implements IFloatAssignVM.FloatAssignGridSetupCommentsCell

        If FloatAssignGridControlIsSet() Then
            If FloatAssignGridActiveSheetIsSet() Then
                If FloatAssignCommentsCellExists() Then
                    SpreadCellLocked(_FloatAssignGrid.ActiveSheet, _CommentsRowIndex, _CommentsColumnIndex, Not MakeEditable)
                    'If Not MakeEditable Then
                    '    ClearCommentsCell()
                    'End If
                End If
            End If
        End If
    End Sub

    Public Overridable Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.FloatBanking) Implements IFloatAssignVM.AssignCommentsFromGrid

        If AssignTo IsNot Nothing Then
            AssignTo.Comments = GetCommentsFromGrid()
        End If
    End Sub

    Public Overridable Sub FloatChecked(ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer) Implements IFloatAssignVM.FloatChecked
        Dim Comments As String = String.Empty

        If FloatAssignGridControlIsSet() Then
            If FloatAssignGridActiveSheetIsSet() Then
                If FloatAssignCommentsCellExists() Then
                    With _FloatAssignGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex)
                        If .Value IsNot Nothing Then
                            Comments = .Text
                        End If
                    End With
                End If
            End If
        End If
        If String.IsNullOrEmpty(Comments) Then
            NewBanking.Core.FloatChecked(FloatBagID, UserID1, UserID2)
        Else
            NewBanking.Core.FloatChecked(FloatBagID, UserID1, UserID2, Comments)
        End If
    End Sub

    Public Overridable Function FloatAssignGridActiveCellIsCommentsCell() As Boolean Implements IFloatAssignVM.FloatAssignGridActiveCellIsCommentsCell

        FloatAssignGridActiveCellIsCommentsCell = False
        If FloatAssignGridControlIsSet() Then
            If FloatAssignGridActiveSheetIsSet() Then
                If FloatAssignCommentsCellExists() Then
                    FloatAssignGridActiveCellIsCommentsCell = Object.Equals(_FloatAssignGrid.ActiveSheet.ActiveCell, _FloatAssignGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex))
                End If
            End If
        End If
    End Function

    Public Function GetFloatAssignGridTotalsRowIndex() As Integer Implements IFloatAssignVM.GetFloatAssignGridTotalsRowIndex

        Return _CommentsRowIndex - 1
    End Function

    Public Overridable Sub SetFloatAssignForm(ByRef FloatAssignForm As System.Windows.Forms.Form) Implements IFloatAssignVM.SetFloatAssignForm

        _FloatAssignForm = FloatAssignForm
    End Sub

    Public Overridable Sub FloatAssignFormFormat() Implements IFloatAssignVM.FloatAssignFormFormat

        If FloatAssignFormIsSet() Then
            _FloatAssignForm.Height += 80
        End If
    End Sub

    Public Function GetCommentsFromGrid() As String Implements IFloatAssignVM.GetCommentsFromGrid

        GetCommentsFromGrid = String.Empty
        If FloatAssignGridControlIsSet() Then
            If FloatAssignGridActiveSheetIsSet() Then
                If FloatAssignCommentsCellExists() Then
                    With _FloatAssignGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex)
                        If .Value IsNot Nothing Then
                            GetCommentsFromGrid = .Text
                        End If
                    End With
                End If
            End If
        End If
    End Function

    Friend Overridable Function GetTenderDenominationListCollection() As TenderDenominationListCollection

        GetTenderDenominationListCollection = New TenderDenominationListCollection
        GetTenderDenominationListCollection.LoadData()
    End Function

    Friend Overridable Function GetFloatBagCollection(ByVal FloatID As Integer) As FloatBagCollection

        GetFloatBagCollection = New FloatBagCollection
        GetFloatBagCollection.LoadData(FloatID)
    End Function

    Friend Overridable Function GetFloatBagComments(ByVal FloatID As Integer) As String
        Dim GetCommentsFrom As New SafeBagCollectionModel

        GetFloatBagComments = String.Empty
        If GetCommentsFrom IsNot Nothing Then
            With GetCommentsFrom
                .LoadSafeBag(FloatID)
                If .Count > 0 Then
                    GetFloatBagComments = .Item(0).Comments
                End If
            End With
        End If
    End Function

    Friend Overridable Sub EnableCommentsCell(ByVal MakeEditable As Boolean)

        If _FloatAssignGrid IsNot Nothing Then
            With _FloatAssignGrid
                If .ActiveSheet IsNot Nothing Then
                    With .ActiveSheet
                        If .RowCount > 0 And _CommentsRowIndex < .RowCount Then
                            If .ColumnCount > 0 And _CommentsColumnIndex < .ColumnCount Then
                                SpreadCellLocked(_FloatAssignGrid.ActiveSheet, _CommentsRowIndex, _CommentsColumnIndex, Not MakeEditable)
                            End If
                        End If
                    End With
                End If
            End With
        End If
    End Sub

    Friend Overridable Sub ClearCommentsCell()

        If FloatAssignGridControlIsSet() Then
            If FloatAssignGridActiveSheetIsSet() Then
                If FloatAssignCommentsCellExists() Then
                    _FloatAssignGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex).Value = Nothing
                End If
            End If
        End If
    End Sub

    Friend Function FloatAssignGridControlIsSet() As Boolean

        Return _FloatAssignGrid IsNot Nothing
    End Function

    Friend Function FloatAssignGridActiveSheetIsSet() As Boolean

        Return _FloatAssignGrid.ActiveSheet IsNot Nothing
    End Function

    Friend Function FloatAssignCommentsCellExists() As Boolean

        With _FloatAssignGrid.ActiveSheet
            If .RowCount > 0 And _CommentsRowIndex < .RowCount Then
                If .ColumnCount > 0 And _CommentsColumnIndex < .ColumnCount Then
                    FloatAssignCommentsCellExists = True
                End If
            End If
        End With
    End Function

    Friend Function FloatAssignFormIsSet() As Boolean

        Return _FloatAssignForm IsNot Nothing
    End Function
End Class
