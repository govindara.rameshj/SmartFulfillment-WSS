﻿Public Class CashierReportVMWithComments
    Implements ICashierReportVM

    Friend _CashierReportDisplayGrid As FarPoint.Win.Spread.FpSpread
    Friend _CurrentSheet As FarPoint.Win.Spread.SheetView
    Private _NumberOfCommentsRowsToAdd As Integer
    Friend _TenderDenomList As TenderDenominationListCollection
    Friend _IsFirstEndOfDayPickup As Boolean
    Friend _CommentsRowIndex As Integer
    Friend _TotalAllPickupsEnteredRow As Integer
    Friend _CashierReportForm As Windows.Forms.Form

#Region "ICashierReport Implementation"

    Public Overridable Sub DisplayCashierOnCurrentSheet(ByRef Cashier As Core.ActiveCashierList) Implements ICashierReportVM.DisplayCashierOnCurrentSheet

        Dim RowIndex As Integer = 0
        Dim ColumnIndex As Integer = 0

        LoadTenderDenominations()

        If CashierReportDisplayGridIsSet() AndAlso CurrentSheetIsSet() Then
            SpreadSheetClearDown(_CurrentSheet)
            DisplayCashTendersOnCurrentSheet(RowIndex)
            DisplayTotalCashEnteredOnCurrentSheet(RowIndex)
            DisplayNonCashAmendableTenders(RowIndex)
            DisplayNonCashUnAmendableTenders(RowIndex)
            DisplayTotalNonCashEntered(RowIndex)
            DisplayTotalEntered(RowIndex)
            DisplayTotalAllPickupsEntered(RowIndex)
            DisplayCalculatedStartFloat(RowIndex)
            DisplayNewStartFloat(RowIndex)
            DisplaySystemTotal(RowIndex)
            DisplayCalculatedVariance(RowIndex)
            DisplayCommentsRow(RowIndex)
            DisplayCashierEndOfDayPickups(Cashier, ColumnIndex)
            DisplayCashierCashDrops(Cashier, ColumnIndex)
            DisplayTotalColumn(ColumnIndex)
        End If
    End Sub

    Public Overridable Sub SetCashierReportCurrentSheet(ByRef CurrentSheet As FarPoint.Win.Spread.SheetView) Implements ICashierReportVM.SetCashierReportCurrentSheet

        _CurrentSheet = CurrentSheet
    End Sub

    Public Overridable Sub SetCashierReportDisplayGrid(ByRef DisplayGrid As FarPoint.Win.Spread.FpSpread) Implements ICashierReportVM.SetCashierReportDisplayGrid

        _CashierReportDisplayGrid = DisplayGrid
    End Sub

    Public Sub CashierReportFormFormat() Implements ICashierReportVM.CashierReportFormFormat

        If CashierReportFormIsSet() Then
            _CashierReportForm.Height += 80
        End If
    End Sub

    Public Sub SetCashierReportForm(ByRef CashierReportForm As System.Windows.Forms.Form) Implements ICashierReportVM.SetCashierReportForm

        _CashierReportForm = CashierReportForm
    End Sub
#End Region

#Region "Conditions"
    Friend Overridable Function CashierReportDisplayGridIsSet() As Boolean

        Return _CashierReportDisplayGrid IsNot Nothing
    End Function

    Friend Overridable Function CurrentSheetIsSet() As Boolean

        Return _CurrentSheet IsNot Nothing
    End Function

    Friend Overridable Function TenderDenomListIsSet() As Boolean

        Return _TenderDenomList IsNot Nothing
    End Function

    Friend Overridable Function CashierReportFormIsSet() As Boolean

        Return _CashierReportForm IsNot Nothing
    End Function
#End Region

#Region "Data"

    Friend Overridable Sub LoadTenderDenominations()

        _TenderDenomList = New TenderDenominationListCollection
        _TenderDenomList.LoadData()
    End Sub
#End Region

#Region "UI"

    Friend Overridable Sub DisplayCashTendersOnCurrentSheet(ByRef RowIndex As Integer)

        If CurrentSheetIsSet() Then
            For Each obj As TenderDenominationList In _TenderDenomList
                If obj.TenderID = 1 Then
                    SpreadRowAdd(_CurrentSheet, RowIndex)
                    SpreadRowTagValue(_CurrentSheet, RowIndex, obj)
                    SpreadCellValue(_CurrentSheet, RowIndex, 0, obj.TenderText)
                End If
            Next
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub DisplayTotalCashEnteredOnCurrentSheet(ByRef RowIndex As Integer)

        If CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Total Cash Entered")
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub DisplayNonCashAmendableTenders(ByRef RowIndex As Integer)

        If CurrentSheetIsSet() And TenderDenomListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenomList
                With TenderDenomination
                    If .IsAmendableNonCashTenderType Then
                        SpreadRowAdd(_CurrentSheet, RowIndex)
                        SpreadRowTagValue(_CurrentSheet, RowIndex, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, RowIndex, 0, .TenderText)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub DisplayNonCashUnAmendableTenders(ByRef RowIndex As Integer)

        If CurrentSheetIsSet() And TenderDenomListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenomList
                With TenderDenomination
                    If .IsUnAmendableNonCashTenderType Then
                        SpreadRowAdd(_CurrentSheet, RowIndex)
                        SpreadRowTagValue(_CurrentSheet, RowIndex, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, RowIndex, 0, .TenderText)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub DisplayTotalNonCashEntered(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Total Non Cash Entered")
        End If
    End Sub

    Friend Overridable Sub DisplayTotalEntered(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Total Entered")
        End If
    End Sub

    Friend Overridable Sub DisplayTotalAllPickupsEntered(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            _TotalAllPickupsEnteredRow = RowIndex
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Total Entered (All Pickups)")
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub DisplayCalculatedStartFloat(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Start Float")
        End If
    End Sub

    Friend Overridable Sub DisplayNewStartFloat(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "New Float")
        End If
    End Sub

    Friend Overridable Sub DisplaySystemTotal(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "System Total")
        End If
    End Sub

    Friend Overridable Sub DisplayCalculatedVariance(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            SpreadRowAdd(_CurrentSheet, RowIndex)
            SpreadRowColour(_CurrentSheet, RowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, RowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, RowIndex)
            SpreadCellValue(_CurrentSheet, RowIndex, 0, "Variance")
        End If
    End Sub

    Friend Overridable Sub DisplayCommentsRow(ByRef RowIndex As Integer)

        If CashierReportDisplayGridIsSet() And CurrentSheetIsSet() Then
            _NumberOfCommentsRowsToAdd = 6
            SpreadRowDrawLine(_CurrentSheet, RowIndex, Color.Black, 1)
            _CommentsRowIndex = RowIndex + 1
            SpreadRowAdd(_CurrentSheet, RowIndex, _NumberOfCommentsRowsToAdd)
            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex, _NumberOfCommentsRowsToAdd, 0, 1)
            SpreadCellSetAsBold(_CashierReportDisplayGrid, _CurrentSheet, _CommentsRowIndex, 0)
            SpreadCellSetVerticalAlignment(_CurrentSheet, _CommentsRowIndex, 0, CellVerticalAlignment.Top)
            SpreadCellValue(_CurrentSheet, _CommentsRowIndex, 0, "Comments")
        End If
    End Sub

    Friend Overridable Sub DisplayCashierEndOfDayPickups(ByRef Cashier As Core.ActiveCashierList, ByRef ColumnIndex As Integer)

        IsFirstEndOfDayPickup = True
        For Each NextPickup As Pickup In Cashier.EndOfDayPickupList
            AddNewPickupColumnForCashier(Cashier, ColumnIndex, NextPickup)
            AddTotalsFormulae(ColumnIndex)
            If IsFirstEndOfDayPickup Then
                IsFirstEndOfDayPickup = False
                SetCashierFormulaeAndTotalsForFirstColumn(Cashier, ColumnIndex)
            Else
                AmendAllPickupsTotalEnteredFormula(ColumnIndex)
            End If
            PopulatePickupSalesForTenderDenominationsAndLockDownUnAmendableNonCashTenders(ColumnIndex, NextPickup)
            PopulatePickupOrCashDropComment(ColumnIndex, NextPickup)
        Next
    End Sub

    Friend Overridable Sub AddNewPickupColumnForCashier(ByRef Cashier As Core.ActiveCashierList, ByRef ColumnIndex As Integer, ByVal NextPickup As Pickup)

        If CurrentSheetIsSet() Then
            SpreadColumnAdd(_CurrentSheet, ColumnIndex)
            SpreadColumnEditable(_CurrentSheet, ColumnIndex, True)
            SpreadColumnCustomise(_CurrentSheet, ColumnIndex, CreateColumnHeaderForCashierPickup(Cashier, NextPickup), gcintColumnWidthPickupWide, False)
            SpreadColumnMoney(_CurrentSheet, ColumnIndex, True, gstrFarPointMoneyNullDisplay)
        End If
    End Sub

    Friend Overridable Function CreateColumnHeaderForCashierPickup(ByRef Cashier As Core.ActiveCashierList, ByVal Pickup As Pickup) As String

        CreateColumnHeaderForCashierPickup = CreatePickupOrCashDropColumnHeader(Cashier, Pickup, "E.O.D Pickup")
    End Function

    Friend Overridable Sub SetCashierFormulaeAndTotalsForFirstColumn(ByRef Cashier As Core.ActiveCashierList, ByRef ColumnIndex As Integer)

        If CurrentSheetIsSet() Then
            Dim TotalStartFloatValueRowIndex As Integer = _TotalAllPickupsEnteredRow + 1
            Dim TotalNewFloatValueRowIndex As Integer = _TotalAllPickupsEnteredRow + 2
            Dim TotalSystemSalesRowIndex As Integer = _TotalAllPickupsEnteredRow + 3
            Dim TotalVarianceRowIndex As Integer = _TotalAllPickupsEnteredRow + 4

            SpreadCellFormula(_CurrentSheet, _TotalAllPickupsEnteredRow, ColumnIndex, "R" & _TotalAllPickupsEnteredRow.ToString & "C" & (ColumnIndex + 1).ToString)  'row - total entered (all pickups)
            SpreadCellValue(_CurrentSheet, TotalStartFloatValueRowIndex, ColumnIndex, Cashier.StartFloatValue)
            SpreadCellValue(_CurrentSheet, TotalNewFloatValueRowIndex, ColumnIndex, Cashier.NewFloatValue)
            SpreadCellValue(_CurrentSheet, TotalSystemSalesRowIndex, ColumnIndex, Cashier.SystemSales)
            SpreadCellFormula(_CurrentSheet, TotalVarianceRowIndex, ColumnIndex, "R" & TotalStartFloatValueRowIndex.ToString & "C+R" & TotalSystemSalesRowIndex.ToString & "C-R" & TotalNewFloatValueRowIndex.ToString & "C-R" & TotalVarianceRowIndex.ToString & "C")
        End If
    End Sub

    Friend Overridable Sub PopulatePickupSalesForTenderDenominationsAndLockDownUnAmendableNonCashTenders(ByRef ColumnIndex As Integer, ByVal NextPickup As Pickup)

        PopulateSalesForTenderDenominationsAndLockDownNonCashTenders(ColumnIndex, NextPickup, False)
    End Sub

    Friend Overridable Sub DisplayCashierCashDrops(ByRef Cashier As Core.ActiveCashierList, ByRef ColumnIndex As Integer)

        For Each CashDrop As Pickup In Cashier.CashDropsPickupList
            AddNewCashDropColumnForCashier(Cashier, ColumnIndex, CashDrop)
            AddTotalsFormulae(ColumnIndex)
            AmendAllPickupsTotalEnteredFormula(ColumnIndex)
            PopulateCashDropSalesForTenderDenominationsAndLockDownNonCashTenders(ColumnIndex, CashDrop)
            PopulatePickupOrCashDropComment(ColumnIndex, CashDrop)
        Next
    End Sub

    Friend Overridable Sub AddNewCashDropColumnForCashier(ByRef Cashier As Core.ActiveCashierList, ByRef ColumnIndex As Integer, ByVal CashDrop As Pickup)

        If CurrentSheetIsSet() Then
            SpreadColumnAdd(_CurrentSheet, ColumnIndex)
            SpreadColumnEditable(_CurrentSheet, ColumnIndex, True)
            SpreadColumnCustomise(_CurrentSheet, ColumnIndex, CreateCashDropColumnHeader(Cashier, CashDrop), gcintColumnWidthPickupWide, False)
            SpreadColumnMoney(_CurrentSheet, ColumnIndex, True, gstrFarPointMoneyNullDisplay)
        End If
    End Sub

    Friend Overridable Function CreateCashDropColumnHeader(ByRef Cashier As Core.ActiveCashierList, ByVal CashDrop As Pickup) As String

        CreateCashDropColumnHeader = CreatePickupOrCashDropColumnHeader(Cashier, CashDrop, "Cash Drop")
    End Function

    Friend Overridable Function CreatePickupOrCashDropColumnHeader(ByRef Cashier As Core.ActiveCashierList, ByVal PickupOrCashDrop As Pickup, ByVal Caption As String) As String

        CreatePickupOrCashDropColumnHeader = ""
        If Cashier IsNot Nothing And PickupOrCashDrop IsNot Nothing Then
            With PickupOrCashDrop
                CreatePickupOrCashDropColumnHeader = _
                     Cashier.CashierEmployeeCode & " " _
                   & Cashier.CashierUserName & vbCrLf _
                   & Caption & vbCrLf & "(" _
                   & .PickupSealNumber & ")" & vbCrLf _
                   & .PickupPeriodID.ToString & " : " _
                   & .PickupDate.ToShortDateString
            End With
        End If
    End Function

    Friend Overridable Sub AddTotalsFormulae(ByRef ColumnIndex As Integer)

        If CurrentSheetIsSet() Then
            SpreadCellFormula(_CurrentSheet, 13, ColumnIndex, "SUM(R1C:R13C)")
            SpreadCellFormula(_CurrentSheet, 24, ColumnIndex, "SUM(R15C:R24C)")
            SpreadCellFormula(_CurrentSheet, 25, ColumnIndex, "R14C+R25C")
        End If
    End Sub

    Friend Overridable Sub AmendAllPickupsTotalEnteredFormula(ByRef ColumnIndex As Integer)

        SpreadCellFormulaAppend(_CurrentSheet, _TotalAllPickupsEnteredRow, 1, "+R" & _TotalAllPickupsEnteredRow.ToString & "C" & (ColumnIndex + 1).ToString)
    End Sub

    Friend Overridable Sub PopulateCashDropSalesForTenderDenominationsAndLockDownNonCashTenders(ByRef ColumnIndex As Integer, ByVal CashDrop As Pickup)

        PopulateSalesForTenderDenominationsAndLockDownNonCashTenders(ColumnIndex, CashDrop, True)
    End Sub

    Friend Overridable Sub PopulatePickupOrCashDropComment(ByRef ColumnIndex As Integer, ByVal PickupOrCashDrop As Pickup)

        If CurrentSheetIsSet() Then
            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex, _NumberOfCommentsRowsToAdd, ColumnIndex, 1)
            SpreadCellSetAsTextFormat(_CurrentSheet, _CommentsRowIndex, ColumnIndex)
            SpreadCellSetAsMultiLine(_CurrentSheet, _CommentsRowIndex, ColumnIndex, True)
            SpreadCellValue(_CurrentSheet, _CommentsRowIndex, ColumnIndex, PickupOrCashDrop.PickupComment)
        End If
    End Sub

    Friend Overridable Sub PopulateSalesForTenderDenominationsAndLockDownNonCashTenders(ByRef ColumnIndex As Integer, ByVal PickupOrCashDrop As Pickup, ByVal LockDownAllNonCashTenders As Boolean)

        If PickupOrCashDrop IsNot Nothing Then
            For Each NextSale As PickupSale In PickupOrCashDrop.PickupSales
                For Index As Integer = 0 To _CurrentSheet.RowCount - 1 Step 1
                    If _CurrentSheet.Rows(Index).Tag IsNot Nothing Then
                        If TypeOf _CurrentSheet.Rows(Index).Tag Is TenderDenominationList Then
                            Dim TenderDenom As TenderDenominationList = CType(_CurrentSheet.Rows(Index).Tag, TenderDenominationList)

                            With TenderDenom
                                If (LockDownAllNonCashTenders And Not .IsCashTenderType) Or .IsUnAmendableNonCashTenderType Then
                                    SpreadCellLocked(_CurrentSheet, Index, ColumnIndex, True)
                                End If
                                If .TenderID = NextSale.TenderID And .DenominationID = NextSale.DenominationID Then
                                    If NextSale.PickupValue.HasValue Then
                                        SpreadCellValue(_CurrentSheet, Index, ColumnIndex, NextSale.PickupValue)
                                    End If
                                End If
                            End With
                        End If
                    End If
                Next
            Next
        End If
    End Sub

    Friend Overridable Sub DisplayTotalColumn(ByRef ColumnIndex As Integer)

        If CurrentSheetIsSet() Then
            SpreadColumnAdd(_CurrentSheet, ColumnIndex)
            SpreadColumnCustomise(_CurrentSheet, ColumnIndex, "Total", gcintColumnWidthMoney, False)
            SpreadColumnMoney(_CurrentSheet, ColumnIndex, True, gstrFarPointMoneyNullDisplay)
            SetTotalColumnFormulae(ColumnIndex)
        End If
    End Sub

    Friend Overridable Sub SetTotalColumnFormulae(ByRef ColumnIndex As Integer)

        For RowIndex As Integer = 0 To _CurrentSheet.RowCount - _NumberOfCommentsRowsToAdd - 1 Step 1
            If RowIndex <> _TotalAllPickupsEnteredRow Then
                SpreadCellFormula(_CurrentSheet, RowIndex, ColumnIndex, "SUM(R" & (RowIndex + 1).ToString & "C2:R" & (RowIndex + 1).ToString & "C" & ColumnIndex.ToString.Trim & ")")
            End If
        Next
    End Sub

    Friend Property IsFirstEndOfDayPickup() As Boolean
        Get
            Return _IsFirstEndOfDayPickup
        End Get
        Set(ByVal value As Boolean)
            _IsFirstEndOfDayPickup = value
        End Set
    End Property
#End Region
End Class
