﻿Public Class EndOfDayCheckSummaryVM
    Implements IEndOfDayCheckSummaryVM

    Friend _PeriodID As Integer
    Friend _Grid As FpSpread
    Friend _View As New SheetView
    Friend _Data As EndOfDayCheckSummaryModel

#Region "Public Property"

    Public Property SheetView() As SheetView Implements IEndOfDayCheckSummaryVM.SheetView
        Get
            Return _View
        End Get
        Set(ByVal value As FarPoint.Win.Spread.SheetView)
            _View = value
        End Set
    End Property

#End Region

#Region "Public Interface"

    Public Sub Summary(ByRef Grid As FpSpread, ByVal PeriodID As Integer) Implements IEndOfDayCheckSummaryVM.Summary

        SummaryLoadData(PeriodID)
        FormatGrid(Grid)
        PopulateGrid()

    End Sub

    Public Sub SummaryPrint(ByRef Grid As FpSpread, ByVal PeriodID As Integer) Implements IEndOfDayCheckSummaryVM.SummaryPrint

        SummaryLoadData(PeriodID)
        FormatGrid(Grid)
        PopulateGrid()

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Friend Overridable Sub SummaryLoadData(ByVal PeriodID As Integer)

        SetPeriodID(PeriodID)

        GetData()

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value

    End Sub

    Friend Overridable Sub GetData()

        _Data = New EndOfDayCheckSummaryModel

        _Data.LoadData(_PeriodID)

    End Sub

    Friend Overridable Sub FormatGrid(ByRef Grid As FpSpread)

        _Grid = Grid

        SpreadSheetCustomise(_View, 8, Model.SelectionUnit.Row)

        SpreadColumnCustomise(_View, 0, "Pickup Bag Count", 120, False)
        SpreadColumnCustomise(_View, 1, "Pickup Bag System Count", 160, False)
        SpreadColumnCustomise(_View, 2, "Banking Cash Count", 140, False)
        SpreadColumnCustomise(_View, 3, "Banking Cash System Count", 180, False)
        SpreadColumnCustomise(_View, 4, "Banking Cheque Count", 150, False)
        SpreadColumnCustomise(_View, 5, "Banking Cheque System Count", 185, False)
        SpreadColumnCustomise(_View, 6, "Manager", 150, False)
        SpreadColumnCustomise(_View, 7, "Witness", 150, False)

        SpreadGridSheetAdd(_Grid, _View, True, String.Empty)
        SpreadGridScrollBar(_Grid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

    End Sub

    Friend Overridable Sub PopulateGrid()

        SpreadSheetClearDown(_View)
        SpreadRowAdd(_View, 0)
        SpreadCellValue(_View, 0, 0, _Data.PickupBagCount)
        SpreadCellValue(_View, 0, 1, _Data.PickupBagSystemCount)
        SpreadCellValue(_View, 0, 2, _Data.BankingCashCount)
        SpreadCellValue(_View, 0, 3, _Data.BankingCashSystemCount)
        SpreadCellValue(_View, 0, 4, _Data.BankingChequeCount)
        SpreadCellValue(_View, 0, 5, _Data.BankingChequeSystemCount)
        SpreadCellValue(_View, 0, 6, _Data.Manager)
        SpreadCellValue(_View, 0, 7, _Data.Witness)

    End Sub

#End Region

End Class