﻿Public Class SafeCheckValidationRequired
    Implements ISafeCheckValidation

    Public Function AllowSafeCheckCompletion() As Boolean Implements ISafeCheckValidation.AllowSafeCheckCompletion

        Return True

    End Function

    Public Sub SaveSafeCheckUserDetails(ByRef OdbcConnection As clsOasys3DB, _
                                        ByVal selectedPeriod As Period, _
                                        ByVal UserId1 As Integer, _
                                        ByVal UserId2 As Integer, _
                                        ByVal LastAmended As Date) Implements ISafeCheckValidation.SaveSafeCheckUserDetails

        Dim Repository As SafeCheckReportAuthorisersRepository = New SafeCheckReportAuthorisersRepository

        Repository.UpdateSafeCheckedUsers(OdbcConnection, selectedPeriod.PeriodID, UserId1, UserId2, LastAmended)

    End Sub

End Class
