﻿Public Class EndOfDayCheckSummaryVMUnPopulated
    Implements IEndOfDayCheckSummaryVM

    Friend _Grid As FpSpread
    Friend _View As New SheetView
    Friend _Data As EndOfDayCheckSummaryModelUnPopulated

#Region "Public Property"

    Public Property SheetView() As SheetView Implements IEndOfDayCheckSummaryVM.SheetView
        Get
            Return _View
        End Get
        Set(ByVal value As FarPoint.Win.Spread.SheetView)
            _View = value
        End Set
    End Property

#End Region

#Region "Public Interface"

    Public Sub Summary(ByRef Grid As FpSpread, ByVal PeriodID As Integer) Implements IEndOfDayCheckSummaryVM.Summary

        SummaryLoadData()
        FormatGrid(Grid)
        PopulateGrid()

    End Sub

    Public Sub SummaryPrint(ByRef Grid As FpSpread, ByVal PeriodID As Integer) Implements IEndOfDayCheckSummaryVM.SummaryPrint

        SummaryPrintLoadData()
        FormatGrid(Grid)
        PopulateGrid()
        RowZeroFormatting()

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Friend Overridable Sub SummaryLoadData()

        _Data = New EndOfDayCheckSummaryModelUnPopulated

    End Sub

    Friend Overridable Sub SummaryPrintLoadData()

        _Data = New EndOfDayCheckSummaryModelUnPopulated

        With _Data

            .ManagerID = "<Name + Signature>"
            .WitnessID = "<Name + Signature>"
            .TimelockSet = "<Time>"
            .Duration = "<Hours>"
            .BagCount = "<Manual entry for now>"
            .SystemCount = "<System Count of Floats unassigned + Pickups not Banked>"
            .ChequeBagNumber = "<Manual entry for now>"
            .CashBagNumber = "<Manual entry for now>"
            .Comments = "<Comments>"

        End With

    End Sub

    Friend Overridable Sub FormatGrid(ByRef Grid As FpSpread)

        _Grid = Grid

        SpreadSheetCustomise(_View, 9, Model.SelectionUnit.Row)

        SpreadColumnCustomise(_View, 0, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn1, gcintColumnWidthUser, False)     'Screen Column - Manager ID
        SpreadColumnCustomise(_View, 1, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn2, gcintColumnWidthUser, False)     'Screen Column - Witness ID
        SpreadColumnCustomise(_View, 2, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn3, gcintColumnWidthDefault, False)  'Screen Column - Timelock Set
        SpreadColumnCustomise(_View, 3, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn4, gcintColumnWidthDefault, False)  'Screen Column - Duration
        SpreadColumnCustomise(_View, 4, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn5, gcintColumnWidthDefault, False)  'Screen Column - Bag Count
        SpreadColumnCustomise(_View, 5, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn6, gcintColumnWidthDefault, False)  'Screen Column - System Count
        SpreadColumnCustomise(_View, 6, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn7, 150, False)                      'Screen Column - Cheque Bag Numbers
        SpreadColumnCustomise(_View, 7, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn8, 150, False)                      'Screen Column - Cash bag Number
        SpreadColumnCustomise(_View, 8, My.Resources.ScreenColumns.MainEndOfDayManagementCheckGridColumn9, gcintColumnWidthDefault, False)  'Screen Column - Comments

        SpreadGridSheetAdd(_Grid, _View, True, String.Empty)
        SpreadGridScrollBar(_Grid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

    End Sub

    Friend Overridable Sub PopulateGrid()

        SpreadSheetClearDown(_View)
        SpreadRowAdd(_View, 0)
        SpreadCellValue(_View, 0, 0, _Data.ManagerID)
        SpreadCellValue(_View, 0, 1, _Data.WitnessID)
        SpreadCellValue(_View, 0, 2, _Data.TimelockSet)
        SpreadCellValue(_View, 0, 3, _Data.Duration)
        SpreadCellValue(_View, 0, 4, _Data.BagCount)
        SpreadCellValue(_View, 0, 5, _Data.SystemCount)
        SpreadCellValue(_View, 0, 6, _Data.ChequeBagNumber)
        SpreadCellValue(_View, 0, 7, _Data.CashBagNumber)
        SpreadCellValue(_View, 0, 8, _Data.Comments)

    End Sub

    Friend Overridable Sub RowZeroFormatting()

        SpreadRowHeight(_View, 0, 80)
        SpreadRowVerticalAlignment(_View, 0, CellVerticalAlignment.Center)
        SpreadRowColour(_View, 0, Color.LightGray)

    End Sub

#End Region

End Class