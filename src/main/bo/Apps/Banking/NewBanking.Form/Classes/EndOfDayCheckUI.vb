﻿Public Class EndOfDayCheckUI
    Implements IEndOfDayCheckUI

#Region "Interface"

    Public Function ButtonCaptionName() As String Implements IEndOfDayCheckUI.ButtonCaptionName

        Return "F9 End Of Day"

    End Function

    Public Function ButtonEnabled(ByVal CurrentDate As Date, ByVal SelectedDate As Date, ByVal EndOfDayCheckDone As Boolean) As Boolean Implements IEndOfDayCheckUI.ButtonEnabled

        ButtonEnabled = False

        If EndOfDayCheckDone = True Then Return False

        If Date.Compare(CurrentDate, SelectedDate) = 0 Then Return True

    End Function

    Public Function ExecuteEndOfDayCheckProcessUI() As Boolean Implements IEndOfDayCheckUI.ExecuteEndOfDayCheckProcessUI

        Return True

    End Function

#End Region

End Class