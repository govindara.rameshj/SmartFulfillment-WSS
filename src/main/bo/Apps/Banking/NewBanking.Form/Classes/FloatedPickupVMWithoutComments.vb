﻿Public Class FloatedPickupVMWithoutComments
    Implements IFloatedPickupVM

    Friend _FloatedPickupGrid As FpSpread

    Public Sub FloatedPickupFormFormat() Implements IFloatedPickupVM.FloatedPickupFormFormat

    End Sub

    Public Sub FloatedPickupGridFormat() Implements IFloatedPickupVM.FloatedPickupGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn1, gcintColumnWidthSeal, False)     'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn2, gcintColumnWidthUser, False)     'Screen Column - Assigned To
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn3, gcintColumnWidthSeal, False)     'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.FloatedPickupDisplayGridColumn4, gcintColumnWidthComments, False) 'Screen Column - Pickup Comment

        SpreadColumnAlignLeft(NewSheet, 3)
        SpreadSheetCustomiseHeaderAlignLeft(NewSheet, 3)

        SpreadGridSheetAdd(_FloatedPickupGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatedPickupGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

        SpreadGridDeactiveKey(_FloatedPickupGrid, Keys.F2)
    End Sub

    Public Sub FloatedPickupGridPopulate(ByRef FloatedPickupLists As Core.FloatedPickupListCollection) Implements IFloatedPickupVM.FloatedPickupGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _FloatedPickupGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatedPickupList In FloatedPickupLists
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj.CashierID)              'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.StartFloatSealNumber)  'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.CashierUserName)       'Screen Column - Assigned To
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.PickupSealNumber)      'Screen Column - Pickup Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.PickupComment)         'Screen Column - Pickup Comment
        Next
    End Sub

    Public Function GetFloatedPickupList(ByVal BankingPeriodID As Integer) As Core.FloatedPickupListCollection Implements IFloatedPickupVM.GetFloatedPickupList

        GetFloatedPickupList = New Core.FloatedPickupListCollection
        GetFloatedPickupList.LoadData(BankingPeriodID)
    End Function

    Public Sub SetFloatedPickupForm(ByRef FloatedPickupForm As System.Windows.Forms.Form) Implements IFloatedPickupVM.SetFloatedPickupForm

    End Sub

    Public Sub SetFloatedPickupGridControl(ByRef FloatedPickupGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatedPickupVM.SetFloatedPickupGridControl

        _FloatedPickupGrid = FloatedPickupGrid
    End Sub
End Class
