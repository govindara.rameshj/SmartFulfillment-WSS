﻿Public Class BankingBagCreationToBankNotPopulated
    Implements IBankingBagCreationUI

    Public Sub PopulateTenders(ByRef CurrentSheet As SheetView, _
                                   ByRef mcolPickupTotals As TenderDenominationListCollection) Implements IBankingBagCreationUI.PopulateTenders

        Dim objTag As SafeDenomination
        Dim decValue As Decimal

        For intLocalRowIndex As Integer = 0 To CurrentSheet.RowCount - 1 Step 1
            'no tag - ignore
            If CurrentSheet.Rows(intLocalRowIndex).Tag Is Nothing Then Continue For

            objTag = CType(CurrentSheet.Rows(intLocalRowIndex).Tag, NewBanking.Core.SafeDenomination)
            decValue = CType(CurrentSheet.Cells(intLocalRowIndex, 1).Value, Decimal) + mcolPickupTotals.TenderDenom(objTag.TenderID, objTag.DenominationID).Amount.Value
            CurrentSheet.Cells(intLocalRowIndex, 1).Value = decValue

            'non cash tender(s)
            If objTag.TenderID <> 1 Then
                CurrentSheet.Cells(intLocalRowIndex, 2).Value = decValue   'suggested amount
                CurrentSheet.Cells(intLocalRowIndex, 3).Value = decValue   'to bank
            End If
            'cash tender
            If objTag.TenderID = 1 Then
                'suggested amount
                If decValue <> 0 Then CurrentSheet.Cells(intLocalRowIndex, 2).Value = SuggestedBankingAmount(decValue - mcolPickupTotals.TenderDenom(objTag.TenderID, objTag.DenominationID).SafeMinimum, mcolPickupTotals.TenderDenom(objTag.TenderID, objTag.DenominationID).BullionMultiple, objTag.DenominationID)
                CurrentSheet.Cells(intLocalRowIndex, 4).Value = decValue       'safe balance
            End If
        Next

    End Sub

End Class