﻿Public Class CashDropCashierFilterVMWithoutNoSaleFilter
    Implements ICashDropCashierFilterVM

    Friend _CashiersToLoad As Core.CashierCollection

    Public Sub LoadCashierDropDown(ByRef CashiersToLoad As Core.CashierCollection, ByVal BankingPeriodID As Integer) Implements ICashDropCashierFilterVM.LoadCashierDropDown

        SetCashiersToLoad(CashiersToLoad)
        If CashiersToLoadIsSet() Then
            LoadCashiers()
        End If
    End Sub

    Friend Overridable Sub SetCashiersToLoad(ByVal CashiersToLoad As CashierCollection)

        _CashiersToLoad = CashiersToLoad
    End Sub

    Friend Overridable Function CashiersToLoadIsSet() As Boolean

        Return _CashiersToLoad IsNot Nothing
    End Function

    Friend Overridable Sub LoadCashiers()

        _CashiersToLoad.LoadDataCashDrop()
    End Sub
End Class
