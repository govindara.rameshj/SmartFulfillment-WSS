﻿Public Class CashDropVMWithComments
    Implements ICashDropVM

    Friend _CashDropGrid As FpSpread
    Friend _CommentsRowIndex As Integer
    Friend _CommentsColumnIndex As Integer
    Friend _CashDropForm As System.Windows.Forms.Form

    Public Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.Pickup) Implements ICashDropVM.AssignCommentsFromGrid

        If CashDropGridControlIsSet() And AssignTo IsNot Nothing Then
            If CashDropGridActiveSheetIsSet() Then
                If CashDropCommentsCellExists() Then
                    With _CashDropGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex)
                        If .Value IsNot Nothing Then
                            AssignTo.SetComments(.Text)
                        End If
                    End With
                End If
            End If
        End If
    End Sub

    Public Function CashDropGridActiveCellIsCommentsCell() As Boolean Implements ICashDropVM.CashDropGridActiveCellIsCommentsCell

        CashDropGridActiveCellIsCommentsCell = False
        If CashDropGridControlIsSet() Then
            If CashDropGridActiveSheetIsSet() Then
                If CashDropCommentsCellExists() Then
                    CashDropGridActiveCellIsCommentsCell = _
                        Object.Equals(_CashDropGrid.ActiveSheet.ActiveCell, _CashDropGrid.ActiveSheet.Cells(_CommentsRowIndex, _CommentsColumnIndex)) _
                    Or Object.Equals(_CashDropGrid.ActiveSheet.ActiveCell, _CashDropGrid.ActiveSheet.Cells(_CommentsRowIndex - 1, _CommentsColumnIndex))
                End If
            End If
        End If
    End Function

    Public Sub CashDropFormFormat() Implements ICashDropVM.CashDropFormFormat

        If CashDropFormIsSet() Then
            _CashDropForm.Height += 120
        End If
    End Sub

    Public Sub CashDropGridFormat() Implements ICashDropVM.CashDropGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.CashDropInputGridColumn1, gcintColumnWidthCashTender, False)     'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.CashDropInputGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - Tray Entry
        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnEditable(NewSheet, 1, True)

        SpreadGridSheetAdd(_CashDropGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_CashDropGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)
        SpreadGridInputMaps(_CashDropGrid)
    End Sub

    Public Sub CashDropGridPopulate() Implements ICashDropVM.CashDropGridPopulate
        Dim colTenderDenomination As TenderDenominationListCollection
        Dim CurrentSheet As SheetView
        Dim RowIndex As Integer = 0

        'load data
        colTenderDenomination = New TenderDenominationListCollection
        colTenderDenomination.LoadData()

        CurrentSheet = _CashDropGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As TenderDenominationList In colTenderDenomination
            If obj.TenderID = 1 Then
                SpreadRowAdd(CurrentSheet, RowIndex)
                SpreadRowTagValue(CurrentSheet, RowIndex, obj)               'Primary Key
                SpreadCellValue(CurrentSheet, RowIndex, 0, obj.TenderText)   'Screen Column - Denomination
                SpreadCellValue(CurrentSheet, RowIndex, 1, 0)                'Screen Column - Tray Entry
            End If
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, RowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, RowIndex)
        SpreadRowBold(_CashDropGrid, CurrentSheet, RowIndex)
        SpreadRowRemoveFocus(CurrentSheet, RowIndex)

        SpreadCellValue(CurrentSheet, RowIndex, 0, "Total")           'Screen Column - Denomination
        SpreadCellFormula(CurrentSheet, RowIndex, 1, "SUM(R1C:R13C)") 'Screen Column - Tray Entry, sum denominations

        'line underneath totals
        SpreadRowDrawLine(CurrentSheet, RowIndex, Color.Black, 1)
        'add "Comments" row (actually 6 rows, to be variously merged)
        _CommentsRowIndex = RowIndex + 2

        SpreadRowAdd(CurrentSheet, RowIndex, 6)
        SpreadCellMergeCells(CurrentSheet, _CommentsRowIndex - 1, 1, 0, 2)
        SpreadCellSetAsBold(_CashDropGrid, CurrentSheet, _CommentsRowIndex - 1, 0)
        SpreadCellValue(CurrentSheet, _CommentsRowIndex - 1, 0, "Comments")
        _CommentsColumnIndex = 0
        SpreadCellMergeCells(CurrentSheet, _CommentsRowIndex, 5, _CommentsColumnIndex, 2)
        SpreadCellSetAsTextFormat(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex)
        SpreadCellSetAsMultiLine(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex, True)
        SpreadCellLocked(CurrentSheet, _CommentsRowIndex, _CommentsColumnIndex, False)
    End Sub

    Public Sub SetCashDropForm(ByRef CashDropForm As System.Windows.Forms.Form) Implements ICashDropVM.SetCashDropForm

        _CashDropForm = CashDropForm
    End Sub

    Public Sub SetCashDropGridControl(ByRef CashDropGrid As FarPoint.Win.Spread.FpSpread) Implements ICashDropVM.SetCashDropGridControl

        _CashDropGrid = CashDropGrid
    End Sub

    Public Function GetCashDropGridTotalsRowIndex() As Integer Implements ICashDropVM.GetCashDropGridTotalsRowIndex

        Return _CommentsRowIndex - 2
    End Function

#Region "Internals"

    Friend Overridable Function GetTenderDenominationListCollection() As TenderDenominationListCollection

        GetTenderDenominationListCollection = New TenderDenominationListCollection
    End Function

    Friend Overridable Function GetCashDropComments(ByVal FloatID As Integer) As String
        Dim GetCommentsFrom As New SafeBagCollectionModel

        GetCashDropComments = String.Empty
        If GetCommentsFrom IsNot Nothing Then
            With GetCommentsFrom
                .LoadSafeBag(FloatID)
                If .Count > 0 Then
                    GetCashDropComments = .Item(0).Comments
                End If
            End With
        End If
    End Function

    Friend Function CashDropGridControlIsSet() As Boolean

        Return _CashDropGrid IsNot Nothing
    End Function

    Friend Function CashDropGridActiveSheetIsSet() As Boolean

        Return _CashDropGrid.ActiveSheet IsNot Nothing
    End Function

    Friend Function CashDropCommentsCellExists() As Boolean

        With _CashDropGrid.ActiveSheet
            If .RowCount > 0 And _CommentsRowIndex < .RowCount Then
                If .ColumnCount > 0 And _CommentsColumnIndex < .ColumnCount Then
                    CashDropCommentsCellExists = True
                End If
            End If
        End With
    End Function

    Friend Function CashDropFormIsSet() As Boolean

        Return _CashDropForm IsNot Nothing
    End Function
#End Region
End Class
