﻿Public Class SafeCheckReportUIHideInfo
    Implements ISafeCheckReportUI

#Region "Private constants"
    Private Const Comments As String = "Comments"
    Private Const Bags As String = "Bags(Seal & Type)"
#End Region

#Region "Interface"

    Public Sub RunReport(ByRef SelectedPeriod As Period, _
                         ByVal StoreID As String, _
                         ByVal StoreName As String) Implements ISafeCheckReportUI.RunReport

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                              ByVal SheetView As SheetView, _
                                              ByRef Index As Integer, _
                                              ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceComments

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal SelectedPeriod As Period) Implements ISafeCheckReportUI.DisplaySafeMaintenanceAuthorisers

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, _
                                                        ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceScannedBankingBags

        'N/A

    End Sub
    Public Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FarPoint.Win.Spread.FpSpread, _
                                                        ByVal SheetView As FarPoint.Win.Spread.SheetView, _
                                                        ByRef Index As Integer, ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceCommentsForAllBags
        Dim bagsHeldInSafe As BagsHeldInSafeCollection

        SpreadRowAdd(SheetView, Index)
        SpreadCellSetAsBold(Grid, SheetView, Index, 0)
        SpreadCellValue(SheetView, Index, 0, Bags)
        bagsHeldInSafe = New BagsHeldInSafeCollection

        bagsHeldInSafe.LoadData(PeriodID)
        For Each bag As BagsHeldInSafe In bagsHeldInSafe
            SpreadRowAdd(SheetView, Index)
            SpreadCellValue(SheetView, Index, 0, bag.SealNumber & " " & BagType(bag.BagType))
            SpreadCellValue(SheetView, Index, 1, bag.BagValue)
        Next

    End Sub

#End Region

  
End Class
