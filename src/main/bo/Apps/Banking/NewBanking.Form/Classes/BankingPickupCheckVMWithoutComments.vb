﻿Public Class BankingPickupCheckVMWithoutComments
    Implements IBankingPickupCheckVM

    Friend _BankingPickupCheckGrid As FpSpread
    Friend _BankingPeriodID As Nullable(Of Integer)
    Friend _ActiveCashierLists As ActiveCashierListCollection

    Public Overridable Sub PersistCommentsIfChanged(ByVal PickupBagID As Integer) Implements IBankingPickupCheckVM.PersistCommentsIfChanged

    End Sub

    Public Overridable Sub BankingPickupCheckGridFormat() Implements IBankingPickupCheckVM.BankingPickupCheckGridFormat
        Dim NewSheet As New SheetView

        If BankingPickupCheckGridControlIsSet() Then
            SpreadSheetCustomise(NewSheet, 1, Model.SelectionUnit.Cell)
            SpreadSheetCustomiseHeader(NewSheet, gintHeaderHeightTriple)
            SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTendersWide, False)     'Screen Column - Denomination
            SpreadColumnAlignLeft(NewSheet, 0)
            SpreadColumnBackgroundColour(NewSheet, 0, Color.Beige)
            SpreadSheetCustomiseFreezeLeadingColumns(NewSheet, 1)

            SpreadGridSheetAdd(_BankingPickupCheckGrid, NewSheet, True, String.Empty)
            SpreadGridScrollBar(_BankingPickupCheckGrid, ScrollBarPolicy.Never, ScrollBarPolicy.AsNeeded)
            SpreadGridInputMaps(_BankingPickupCheckGrid)
        End If
    End Sub

    Public Overridable Sub BankingPickupCheckGridPopulate(ByVal ActiveCashierList As ActiveCashierListCollection) Implements IBankingPickupCheckVM.BankingPickupCheckGridPopulate
        Dim colTenderDenomList As TenderDenominationListCollection
        Dim objTenderDenom As TenderDenominationList

        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0
        Dim intColumnIndex As Integer = 0
        Dim strColumnTag As String
        Dim blnFirstEndOfDayPickup As Boolean

        If BankingPickupCheckGridControlIsSet() Then

            colTenderDenomList = New TenderDenominationListCollection
            colTenderDenomList.LoadData()

            CurrentSheet = _BankingPickupCheckGrid.ActiveSheet
            SpreadSheetClearDown(CurrentSheet)

            'bag check
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Bag Check")
            SpreadRowHeight(CurrentSheet, intRowIndex, 30)
            SpreadRowVerticalAlignment(CurrentSheet, intRowIndex, CellVerticalAlignment.Center)
            'cash tender
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID <> 1 Then Continue For

                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
            'total cash entered - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash Entered")
            SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
            'non-cash tenders - amendable
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = True) Then Continue For

                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

            'non-cash tenders - not amendable
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = False) Then Continue For

                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

            'total non-cash entered - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Non Cash Entered")
            'total entered - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Entered")
            'total entered (all pickups) - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Entered (All Pickups)")
            SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
            'start float - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Start Float")
            'start float - new float
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "New Float")
            'system total
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Total")
            'variance - calculated
            SpreadRowAdd(CurrentSheet, intRowIndex)
            SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_BankingPickupCheckGrid, CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
            SpreadCellValue(CurrentSheet, intRowIndex, 0, "Variance")

            'cashier list
            For Each objCashier As ActiveCashierList In ActiveCashierList
                'cashier - end of day pickup (should only be one), just in case the flag will be reset for each cashier and a tag added
                '          to the first e.o.d pickup
                blnFirstEndOfDayPickup = True

                'cashier e.o.d pickup(s)
                For Each objPickup As Pickup In objCashier.EndOfDayPickupList
                    'add new pickup column
                    strColumnTag = objCashier.CashierID.ToString & "|" & objPickup.PickupID.ToString & "|" & CType(IIf(blnFirstEndOfDayPickup = True, "1", "0"), String)

                    SpreadColumnAdd(CurrentSheet, intColumnIndex)
                    SpreadColumnEditable(CurrentSheet, intColumnIndex, True)
                    SpreadColumnCustomise(CurrentSheet, intColumnIndex, objCashier.CashierEmployeeCode & " " & objCashier.CashierUserName & vbCrLf & _
                                                                        "E.O.D Pickup" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
                                                                        objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)
                    SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
                    SpreadColumnTagValue(CurrentSheet, intColumnIndex, strColumnTag)

                    'bag check
                    SpreadCellButton(CurrentSheet, 0, intColumnIndex, False)

                    SpreadCellFormula(CurrentSheet, 14, intColumnIndex, "SUM(R2C:R14C)")
                    SpreadCellFormula(CurrentSheet, 25, intColumnIndex, "SUM(R16C:R25C)")
                    SpreadCellFormula(CurrentSheet, 26, intColumnIndex, "R15C+R26C")

                    If blnFirstEndOfDayPickup = True Then
                        blnFirstEndOfDayPickup = False    'first column for this cashier will be tagged

                        SpreadCellFormula(CurrentSheet, 27, intColumnIndex, "R27C" & (intColumnIndex + 1).ToString)  'row - total entered (all pickups)
                        SpreadCellValue(CurrentSheet, 28, intColumnIndex, objCashier.StartFloatValue)
                        SpreadCellValue(CurrentSheet, 29, intColumnIndex, objCashier.NewFloatValue)
                        SpreadCellValue(CurrentSheet, 30, intColumnIndex, objCashier.SystemSales)
                        SpreadCellFormula(CurrentSheet, 31, intColumnIndex, "R28C+R30C-R29C-R31C")                   'row - variance
                    Else
                        'row - total entered (all pickups), amend
                        For intIndex As Integer = intColumnIndex To 0 Step -1
                            If CurrentSheet.Columns(intIndex).Tag IsNot Nothing AndAlso CType(CurrentSheet.Columns(intIndex).Tag, String).Split(CType("|", Char))(2) = "1" Then
                                SpreadCellFormulaAppend(CurrentSheet, 27, intIndex, "+R27C" & (intColumnIndex + 1).ToString)
                                Exit For
                            End If
                        Next
                    End If

                    For Each obj As PickupSale In objPickup.PickupSales
                        'iliterate around rows for this pickup, find cash tender/denomination match
                        For intIndex As Integer = 1 To CurrentSheet.RowCount - 1 Step 1
                            'no tag - ignore
                            If CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
                            'tag - string type
                            If CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

                            objTenderDenom = CType(CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
                            'make credit/debit non-cash tenders read-only
                            If objTenderDenom.TenderID <> 1 AndAlso objTenderDenom.TenderReadOnly = True Then SpreadCellLocked(CurrentSheet, intIndex, intColumnIndex, True)
                            If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
                                'match found
                                If obj.PickupValue.HasValue = True Then SpreadCellValue(CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
                            End If
                        Next
                    Next
                Next
                'cashier - cash drop(s)
                For Each objPickup As Pickup In objCashier.CashDropsPickupList
                    'add new pickup column
                    strColumnTag = objCashier.CashierID.ToString & "|" & objPickup.PickupID.ToString & "|" & CType(IIf(blnFirstEndOfDayPickup = True, "1", "0"), String)

                    SpreadColumnAdd(CurrentSheet, intColumnIndex)
                    SpreadColumnEditable(CurrentSheet, intColumnIndex, True)
                    SpreadColumnCustomise(CurrentSheet, intColumnIndex, objCashier.CashierEmployeeCode & " " & objCashier.CashierUserName & vbCrLf & _
                                                                        "Cash Drop" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
                                                                        objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)

                    SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
                    SpreadColumnTagValue(CurrentSheet, intColumnIndex, strColumnTag)

                    'bag check
                    SpreadCellButton(CurrentSheet, 0, intColumnIndex, False)

                    SpreadCellFormula(CurrentSheet, 14, intColumnIndex, "SUM(R2C:R14C)")
                    SpreadCellFormula(CurrentSheet, 25, intColumnIndex, "SUM(R16C:R25C)")
                    SpreadCellFormula(CurrentSheet, 26, intColumnIndex, "R15C+R26C")

                    'row - total entered (all pickups), amend
                    For intIndex As Integer = intColumnIndex To 0 Step -1
                        If CurrentSheet.Columns(intIndex).Tag IsNot Nothing AndAlso CType(CurrentSheet.Columns(intIndex).Tag, String).Split(CType("|", Char))(2) = "1" Then
                            SpreadCellFormulaAppend(CurrentSheet, 27, intIndex, "+R27C" & (intColumnIndex + 1).ToString)
                            Exit For
                        End If
                    Next

                    For Each obj As PickupSale In objPickup.PickupSales
                        'iliterate around rows for this pickup, find cash tender/denomination match
                        For intIndex As Integer = 0 To CurrentSheet.RowCount - 1 Step 1
                            'no tag - ignore
                            If CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
                            'tag - string type
                            If CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

                            objTenderDenom = CType(CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
                            'make ALL non-cash tenders read-only
                            If objTenderDenom.TenderID <> 1 Then SpreadCellLocked(CurrentSheet, intIndex, intColumnIndex, True)
                            If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
                                'match found
                                If obj.PickupValue.HasValue = True Then SpreadCellValue(CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
                            End If
                        Next
                    Next
                Next
            Next
            'total column
            SpreadColumnAdd(CurrentSheet, intColumnIndex)
            SpreadColumnCustomise(CurrentSheet, intColumnIndex, "Total", gcintColumnWidthMoney, False)
            SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
            'cash tender
            'total cash entered - calculated
            'non-cash tenders - amendable
            'non-cash tenders - not amendable
            'total non-cash entered - calculated
            'total entered - calculated
            'total entered (all pickups) - calculated
            'start float - calculated
            'start float - new float
            'system total
            'variance - calculated
            For intIndex As Integer = 1 To CurrentSheet.RowCount - 1 Step 1
                If intIndex = 26 Then Continue For 'IGNORE - total entered (all pickups)
                SpreadCellFormula(CurrentSheet, intIndex, intColumnIndex, "SUM(R" & (intIndex + 1).ToString & "C2:R" & (intIndex + 1).ToString & "C" & intColumnIndex.ToString.Trim & ")")
            Next
        End If
    End Sub

    Public Overridable Sub SetBankingPickupCheckForm(ByRef BankingPickupCheckForm As System.Windows.Forms.Form) Implements IBankingPickupCheckVM.SetBankingPickupCheckForm

    End Sub

    Public Overridable Sub BankingPickupCheckFormFormat() Implements IBankingPickupCheckVM.BankingPickupCheckFormFormat

    End Sub

    Public Overridable Function ActiveCellIsCommentsCell() As Boolean Implements IBankingPickupCheckVM.ActiveCellIsCommentsCell

        Return False
    End Function

    Public Overridable Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer) Implements IBankingPickupCheckVM.SetBankingPeriodID

        _BankingPeriodID = BankingPeriodID
    End Sub

    Public Overridable Sub SetBankingPickupCheckGridControl(ByRef BankingPickupCheckGridControl As FarPoint.Win.Spread.FpSpread) Implements IBankingPickupCheckVM.SetBankingPickupCheckGridControl

        _BankingPickupCheckGrid = BankingPickupCheckGridControl
    End Sub

    Public Overridable Function GetActiveCashierListCollection() As Core.ActiveCashierListCollection Implements IBankingPickupCheckVM.GetActiveCashierListCollection

        _ActiveCashierLists = New ActiveCashierListCollection
        If BankingPeriodIDIsSet() Then
            _ActiveCashierLists.LoadAllCashiers(_BankingPeriodID.Value)
        End If

        Return _ActiveCashierLists
    End Function

    Public Overridable Function GetComments(ByVal PickupBagID As Integer) As String Implements IBankingPickupCheckVM.GetComments

        GetComments = ""
        If ActiveCashierListsIsSet() Then
            For Each Cashier As ActiveCashierList In _ActiveCashierLists
                With Cashier
                    If .EndOfDayPickupList IsNot Nothing Then
                        For Each PickupBag As Pickup In .EndOfDayPickupList
                            With PickupBag
                                If .PickupID = PickupBagID Then
                                    GetComments = .PickupComment
                                    Exit For
                                End If
                            End With
                        Next
                    End If
                    If .CashDropsPickupList IsNot Nothing Then
                        For Each CashDrop As Pickup In .CashDropsPickupList
                            With CashDrop
                                If .PickupID = PickupBagID Then
                                    GetComments = .PickupComment
                                    Exit For
                                End If
                            End With
                        Next
                    End If
                End With
            Next
        End If
    End Function

    Public Overridable Sub ReadyActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer) Implements IBankingPickupCheckVM.ReadyActiveCommentsCellForEditing

    End Sub

    Public Overridable Sub UndoReadinessOfActiveCommentsCellForEditing(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer) Implements IBankingPickupCheckVM.UndoReadinessOfActiveCommentsCellForEditing

    End Sub

    Public Function UseGenericCommentForAllBankingBags() As Boolean Implements IBankingPickupCheckVM.UseGenericCommentForAllBankingBags

        Return True
    End Function

    Public Overridable Function GetCommentsRowIndex() As Integer Implements IBankingPickupCheckVM.GetCommentsRowIndex

        GetCommentsRowIndex = 0
        If _BankingPickupCheckGrid IsNot Nothing AndAlso _BankingPickupCheckGrid.ActiveSheet IsNot Nothing Then
            GetCommentsRowIndex = _BankingPickupCheckGrid.ActiveSheet.RowCount - 1
        End If
    End Function

    Public Function BankingVariance() As Decimal Implements IBankingPickupCheckVM.BankingVariance

        Dim ActiveSheet As SheetView

        ActiveSheet = _BankingPickupCheckGrid.ActiveSheet

        Return CType(ActiveSheet.Cells(ActiveSheet.RowCount - 1, ActiveSheet.ColumnCount - 1).Value, Decimal)

    End Function

#Region "Internals"

    Friend Function BankingPickupCheckGridControlIsSet() As Boolean

        Return _BankingPickupCheckGrid IsNot Nothing
    End Function

    Friend Function BankingPeriodIDIsSet() As Boolean

        Return _BankingPeriodID.HasValue
    End Function

    Friend Function ActiveCashierListsIsSet() As Boolean

        Return _ActiveCashierLists IsNot Nothing
    End Function
#End Region

End Class