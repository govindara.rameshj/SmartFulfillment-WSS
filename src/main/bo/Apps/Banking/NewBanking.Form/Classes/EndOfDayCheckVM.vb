﻿Public Class EndOfDayCheckVM
    Implements IEndOfDayCheckVM

    Friend _LoggedOnUserID As Integer

    Friend _ManagerID As Integer
    Friend _WitnessID As Integer

    Friend _SafeLockedFrom As DateTime
    Friend _SafeLockedTo As DateTime

    Friend _ScanEngine As IBagScanningEngine

    Friend _PickupBagList As FpSpread
    Friend _PickupCommentList As FpSpread
    Friend _BankingBagList As FpSpread
    Friend _BankingCommentList As FpSpread

    Friend _PickupPanel As GroupBox
    Friend _BankingPanel As GroupBox

    Friend _ScanMode As IEndOfDayCheckVM.ScanMode

    Public Enum BagType
        PickupBag
        BankingBag
    End Enum

#Region "Interface: Scan Engine"

    Public WriteOnly Property LoggedOnUserID() As Integer Implements IEndOfDayCheckVM.LoggedOnUserID
        Set(ByVal Value As Integer)
            _LoggedOnUserID = value
        End Set
    End Property

    Public ReadOnly Property ManagerCheckID() As Integer Implements IEndOfDayCheckVM.ManagerCheckID
        Get
            Return _ManagerID
        End Get
    End Property

    Public ReadOnly Property WitnessCheckID() As Integer Implements IEndOfDayCheckVM.WitnessCheckID
        Get
            Return _WitnessID
        End Get
    End Property

    Public Function ManagerCheck() As Boolean Implements IEndOfDayCheckVM.ManagerCheck

        Return AuthoriseCodeSecurity(True, 0, _ManagerID)

    End Function

    Public Function WitnessCheck() As Boolean Implements IEndOfDayCheckVM.WitnessCheck

        Dim User As New UserAuthority

        WitnessCheck = User.IsPrimaryWitnessValid(_ManagerID)

        _WitnessID = User.PrimaryWitnessUserID

    End Function


    Public Sub SetSafeLockedFrom(ByVal LockedFromDate As Date, ByVal LockedFromTime As Date) Implements IEndOfDayCheckVM.SetSafeLockedFrom

        _SafeLockedFrom = New DateTime(LockedFromDate.Year, LockedFromDate.Month, LockedFromDate.Day, LockedFromTime.Hour, LockedFromTime.Minute, 0)


    End Sub

    Public Sub SetSafeLockedTo(ByVal LockedToDate As Date, ByVal LockedToTime As Date) Implements IEndOfDayCheckVM.SetSafeLockedTo

        _SafeLockedTo = New DateTime(LockedToDate.Year, LockedToDate.Month, LockedToDate.Day, LockedToTime.Hour, LockedToTime.Minute, 0)

    End Sub


    Public Sub Initialise(ByVal PeriodID As Integer) Implements IEndOfDayCheckVM.Initialise

        _ScanEngine = (New BagScanningEngineFactory).GetImplementation

        _ScanEngine.Initialise(PeriodID, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

    End Sub

    Public Sub AdditionalInformation() Implements IEndOfDayCheckVM.AdditionalInformation

        _ScanEngine.EndOfDayCheckAdditionalInformation(True, _SafeLockedFrom, _SafeLockedTo, ManagerCheckID, WitnessCheckID)

    End Sub


    Public Sub CreateBankingBag(ByVal SealNumber As String) Implements IEndOfDayCheckVM.CreateBankingBag

        _ScanEngine.CreateBankingBag(SealNumber)

    End Sub

    Public Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String) Implements IEndOfDayCheckVM.CreateBankingBag

        _ScanEngine.CreateBankingBag(SealNumber, SealComment)

    End Sub

    Public Sub CreateBankingComment(ByVal Value As String) Implements IEndOfDayCheckVM.CreateBankingComment

        _ScanEngine.CreateBankingComment(Value)

    End Sub

    Public Sub CreatePickupBag(ByVal SealNumber As String) Implements IEndOfDayCheckVM.CreatePickupBag

        _ScanEngine.CreatePickupBag(SealNumber)

    End Sub

    Public Sub CreatePickupBag(ByVal SealNumber As String, ByVal SealComment As String) Implements IEndOfDayCheckVM.CreatePickupBag

        _ScanEngine.CreatePickupBag(SealNumber, SealComment)

    End Sub

    Public Sub CreatePickupComment(ByVal Value As String) Implements IEndOfDayCheckVM.CreatePickupComment

        _ScanEngine.CreatePickupComment(Value)

    End Sub


    Public Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.BankingBagAlreadyScanned

        Return _ScanEngine.BankingBagAlreadyScanned(SealNumber)

    End Function

    Public Function PickupBagAlreadyScanned(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.PickupBagAlreadyScanned

        Return _ScanEngine.PickupBagAlreadyScanned(SealNumber)

    End Function

    Public Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.BankingBagHasValidSeal

        Return _ScanEngine.BankingBagHasValidSeal(SealNumber)

    End Function

    Public Function PickupBagHasValidSeal(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.PickupBagHasValidSeal

        Return _ScanEngine.PickupBagHasValidSeal(SealNumber)

    End Function

    Public Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.BankingBagExistInSafe

        Return _ScanEngine.BankingBagExistInSafe(SealNumber)

    End Function

    Public Function PickupBagExistInSafe(ByVal SealNumber As String) As Boolean Implements IEndOfDayCheckVM.PickupBagExistInSafe

        Return _ScanEngine.PickupBagExistInSafe(SealNumber)

    End Function

    Public Function MatchSystemBankingExpectation() As Boolean Implements IEndOfDayCheckVM.MatchSystemBankingExpectation

        Return _ScanEngine.MatchSystemBankingExpectation

    End Function

    Public Function MatchSystemPickupExpectation() As Boolean Implements IEndOfDayCheckVM.MatchSystemPickupExpectation

        Return _ScanEngine.MatchSystemPickupExpectation

    End Function


    Public Function BankingBagList() As List(Of ISafeBagScannedModel) Implements IEndOfDayCheckVM.BankingBagList

        Return _ScanEngine.BankingBagList()

    End Function

    Public Function BankingCommentList() As List(Of ISafeCommentModel) Implements IEndOfDayCheckVM.BankingCommentList

        Return _ScanEngine.BankingCommentList()

    End Function

    Public Function PickupBagList() As List(Of ISafeBagScannedModel) Implements IEndOfDayCheckVM.PickupBagList

        Return _ScanEngine.PickupBagList()

    End Function

    Public Function PickupCommentList() As List(Of ISafeCommentModel) Implements IEndOfDayCheckVM.PickupCommentList

        Return _ScanEngine.PickupCommentList()

    End Function


    Public Function Persist() As Boolean Implements IEndOfDayCheckVM.Persist

        Return _ScanEngine.EndOfDayCheckPersist()

    End Function

#End Region

#Region "Interface: Set Panel Controls"

    Public Sub SetPickupPanelControl(ByRef Panel As GroupBox) Implements IEndOfDayCheckVM.SetPickupPanelControl

        _PickupPanel = Panel

    End Sub

    Public Sub SetBankingPanelControl(ByRef Panel As GroupBox) Implements IEndOfDayCheckVM.SetBankingPanelControl

        _BankingPanel = Panel

    End Sub

#End Region

#Region "Interface: Set Grid Controls"

    Public Sub SetPickupBagListControl(ByRef Grid As FpSpread) Implements IEndOfDayCheckVM.SetPickupBagListControl

        _PickupBagList = Grid

    End Sub

    Public Sub SetPickupCommentListControl(ByRef Grid As FpSpread) Implements IEndOfDayCheckVM.SetPickupCommentListControl

        _PickupCommentList = Grid

    End Sub

    Public Sub SetBankingBagListControl(ByRef Grid As FpSpread) Implements IEndOfDayCheckVM.SetBankingBagListControl

        _BankingBagList = Grid

    End Sub

    Public Sub SetBankingCommentListControl(ByRef Grid As FpSpread) Implements IEndOfDayCheckVM.SetBankingCommentListControl

        _BankingCommentList = Grid

    End Sub

#End Region

#Region "Interface: Form Control Logic"

    Public Sub PickupPanelEnabled() Implements IEndOfDayCheckVM.PickupPanelEnabled

        _ScanMode = IEndOfDayCheckVM.ScanMode.PickupMode

        _PickupPanel.Enabled = True
        _BankingPanel.Enabled = False

    End Sub

    Public Sub BankingPanelEnabled() Implements IEndOfDayCheckVM.BankingPanelEnabled

        _ScanMode = IEndOfDayCheckVM.ScanMode.BankingMode

        _PickupPanel.Enabled = False
        _BankingPanel.Enabled = True

    End Sub

    Public Function GetScanMode() As IEndOfDayCheckVM.ScanMode Implements IEndOfDayCheckVM.GetScanMode

        Return _ScanMode

    End Function

#End Region

#Region "Interface: Format Grid Controls"

    Public Sub FormatPickupBagListControl() Implements IEndOfDayCheckVM.FormatPickupBagListControl

        BagListFormat(_PickupBagList, BagType.PickupBag)

    End Sub

    Public Sub FormatPickupCommentListControl() Implements IEndOfDayCheckVM.FormatPickupCommentListControl

        CommentListFormat(_PickupCommentList)

    End Sub

    Public Sub FormatBankingBagListControl() Implements IEndOfDayCheckVM.FormatBankingBagListControl

        BagListFormat(_BankingBagList, BagType.BankingBag)

    End Sub

    Public Sub FormatBankingCommentListControl() Implements IEndOfDayCheckVM.FormatBankingCommentListControl

        CommentListFormat(_BankingCommentList)

    End Sub

#End Region

#Region "Interface: Populate Grid Controls"

    Public Sub PopulatePickupBagListControl() Implements IEndOfDayCheckVM.PopulatePickupBagListControl

        BagListPopulate(_PickupBagList, _ScanEngine.PickupBagList)

    End Sub

    Public Sub PopulatePickupCommentListControl() Implements IEndOfDayCheckVM.PopulatePickupCommentListControl

        CommentListPopulate(_PickupCommentList, _ScanEngine.PickupCommentList)

    End Sub

    Public Sub PopulateBankingBagListControl() Implements IEndOfDayCheckVM.PopulateBankingBagListControl

        BagListPopulate(_BankingBagList, _ScanEngine.BankingBagList)

    End Sub

    Public Sub PopulateBankingCommentListControl() Implements IEndOfDayCheckVM.PopulateBankingCommentListControl

        CommentListPopulate(_BankingCommentList, _ScanEngine.BankingCommentList)

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Friend Overridable Sub BagListFormat(ByRef Grid As FpSpread, ByVal Bag As BagType)

        Dim NewSheet As New SheetView
        Dim SealNUmber As String

        SealNUmber = String.Empty
        Select Case Bag
            Case BagType.PickupBag
                SealNUmber = "Pickup Seal Number"

            Case BagType.BankingBag
                SealNUmber = "Banking Seal Number"

        End Select

        SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, SealNUmber, 125, False)
        SpreadColumnCustomise(NewSheet, 1, "Seal Comment", 755, False)       '886

        SpreadGridSheetAdd(Grid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(Grid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

    End Sub

    Friend Overridable Sub CommentListFormat(ByRef Grid As FpSpread)

        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 1, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, "Comments", 880, False)
        SpreadColumnAlignLeft(NewSheet, 0)

        SpreadGridSheetAdd(Grid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(Grid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)


    End Sub

    Friend Overridable Sub BagListPopulate(ByRef Grid As FpSpread, ByRef BagData As List(Of ISafeBagScannedModel))

        Dim ActiveSheet As SheetView
        Dim RowIndex As Integer = 0

        ActiveSheet = Grid.ActiveSheet
        SpreadSheetClearDown(ActiveSheet)

        For Each Bag As ISafeBagScannedModel In BagData

            SpreadRowAdd(ActiveSheet, RowIndex)

            SpreadRowTagValue(ActiveSheet, RowIndex, Bag.ID)
            SpreadCellValue(ActiveSheet, RowIndex, 0, Bag.SealNumber)
            SpreadCellValue(ActiveSheet, RowIndex, 1, Bag.Comment)

        Next

    End Sub

    Friend Overridable Sub CommentListPopulate(ByRef Grid As FpSpread, ByRef CommentData As List(Of ISafeCommentModel))

        Dim ActiveSheet As SheetView
        Dim RowIndex As Integer = 0

        ActiveSheet = Grid.ActiveSheet
        SpreadSheetClearDown(ActiveSheet)

        For Each Bag As ISafeCommentModel In CommentData

            SpreadRowAdd(ActiveSheet, RowIndex)

            SpreadRowTagValue(ActiveSheet, RowIndex, Bag.ID)
            SpreadCellValue(ActiveSheet, RowIndex, 0, Bag.Comment)
        Next

    End Sub

#End Region

End Class