﻿Public Class FloatViewModelUnassignFloatWithoutManualCheck
    Implements IFloatVM

#Region "Interface: Float Comments"

    Public Sub FloatFormFormat() Implements IFloatVM.FloatFormFormat

    End Sub

    Public Sub FloatGridFormat() Implements IFloatVM.FloatGridFormat

    End Sub

    Public Sub FloatGridPopulate(ByRef FloatLists As Core.FloatListCollection) Implements IFloatVM.FloatGridPopulate

    End Sub

    Public Function GetFloatList(ByVal BankingPeriodID As Integer) As Core.FloatListCollection Implements IFloatVM.GetFloatList
        Return Nothing
    End Function

    Public Sub SetFloatForm(ByRef FloatForm As System.Windows.Forms.Form) Implements IFloatVM.SetFloatForm

    End Sub

    Public Sub SetFloatGridControl(ByRef FloatGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatVM.SetFloatGridControl

    End Sub

#End Region

#Region "Interface: Unassign Float"

    Friend _UnAssignManualCheck As IUnAssignManualCheckModel

    Public Function IsManualCheckRequired() As Boolean Implements IFloatVM.IsManualCheckRequired

        Return False
    End Function

    Public Function PerformManualCheck(ByVal StartFloatID As Integer) As Boolean Implements IFloatVM.PerformManualCheck

        'N/A

    End Function

    Public Sub CompleteUnAssigningTheFloatWithManualCheck(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloatWithManualCheck

    End Sub

    Public Sub CompleteUnAssigningTheFloat(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloat

        _UnAssignManualCheck = (New UnAssignManualCheckModelFactory).GetImplementation
        With _UnAssignManualCheck
            Try
                .Initialise(FloatID, PickupBagID, LoggedOnUserID, SecondUserID, BankingPeriodID, TodayPeriodID, AccountingModel, FloatSealNumber, CurrencyID, String.Empty)
                .StartTransaction()
                .UnAssignTheFloat()
                .Commit()
            Catch ex As Exception
                If .IsInTransaction Then
                    .RollBack()
                End If
                Throw New Exception("There was a problem saving the 'unassign float' changes.", ex)
            End Try
        End With
    End Sub

    Public ReadOnly Property NewFloatValue() As Decimal Implements IFloatVM.NewFloatValue
        Get

        End Get
    End Property
#End Region

End Class