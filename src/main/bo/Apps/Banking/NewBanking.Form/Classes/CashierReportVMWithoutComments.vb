﻿Public Class CashierReportVMWithoutComments
    Implements ICashierReportVM

    Friend _CashierReportDisplayGrid As FarPoint.Win.Spread.FpSpread
    Friend _CurrentSheet As FarPoint.Win.Spread.SheetView

    Public Overridable Sub DisplayCashierOnCurrentSheet(ByRef Cashier As Core.ActiveCashierList) Implements ICashierReportVM.DisplayCashierOnCurrentSheet
        Dim colTenderDenomList As TenderDenominationListCollection
        Dim objTenderDenom As TenderDenominationList

        Dim intRowIndex As Integer = 0
        Dim intColumnIndex As Integer = 0
        Dim blnFirstEndOfDayPickup As Boolean

        colTenderDenomList = New TenderDenominationListCollection
        colTenderDenomList.LoadData()

        If CashierReportDisplayGridIsSet() AndAlso CurrentSheetIsSet() Then
            SpreadSheetClearDown(_CurrentSheet)

            'cash tender
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID <> 1 Then Continue For

                SpreadRowAdd(_CurrentSheet, intRowIndex)
                SpreadRowTagValue(_CurrentSheet, intRowIndex, obj)
                SpreadCellValue(_CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(_CurrentSheet, intRowIndex, Color.Black, 1)
            'total cash entered - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Total Cash Entered")
            SpreadRowDrawLine(_CurrentSheet, intRowIndex, Color.Black, 1)
            'non-cash tenders - amendable
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = True) Then Continue For

                SpreadRowAdd(_CurrentSheet, intRowIndex)
                SpreadRowTagValue(_CurrentSheet, intRowIndex, obj)
                SpreadCellValue(_CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(_CurrentSheet, intRowIndex, Color.Black, 1)

            'non-cash tenders - not amendable
            For Each obj As TenderDenominationList In colTenderDenomList
                If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = False) Then Continue For

                SpreadRowAdd(_CurrentSheet, intRowIndex)
                SpreadRowTagValue(_CurrentSheet, intRowIndex, obj)
                SpreadCellValue(_CurrentSheet, intRowIndex, 0, obj.TenderText)
            Next
            SpreadRowDrawLine(_CurrentSheet, intRowIndex, Color.Black, 1)

            'total non-cash entered - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Total Non Cash Entered")
            'total entered - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Total Entered")
            'total entered (all pickups) - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Purple)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Total Entered (All Pickups)")
            SpreadRowDrawLine(_CurrentSheet, intRowIndex, Color.Black, 1)
            'start float - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Start Float")
            'start float - new float
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "New Float")
            'system total
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "System Total")
            'variance - calculated
            SpreadRowAdd(_CurrentSheet, intRowIndex)
            SpreadRowColour(_CurrentSheet, intRowIndex, Color.Maroon)
            SpreadRowBold(_CashierReportDisplayGrid, _CurrentSheet, intRowIndex)
            SpreadRowRemoveFocus(_CurrentSheet, intRowIndex)
            SpreadCellValue(_CurrentSheet, intRowIndex, 0, "Variance")

            'cashier - end of day pickup (should only be one), just in case the flag will be reset for each cashier and a tag added
            '          to the first e.o.d pickup
            blnFirstEndOfDayPickup = True

            'cashier e.o.d pickup(s)
            For Each objPickup As Pickup In Cashier.EndOfDayPickupList
                'add new pickup column
                SpreadColumnAdd(_CurrentSheet, intColumnIndex)
                SpreadColumnEditable(_CurrentSheet, intColumnIndex, True)
                SpreadColumnCustomise(_CurrentSheet, intColumnIndex, Cashier.CashierEmployeeCode & " " & Cashier.CashierUserName & vbCrLf & _
                                                                    "E.O.D Pickup" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
                                                                    objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)
                SpreadColumnMoney(_CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)

                SpreadCellFormula(_CurrentSheet, 13, intColumnIndex, "SUM(R1C:R13C)")
                SpreadCellFormula(_CurrentSheet, 24, intColumnIndex, "SUM(R15C:R24C)")
                SpreadCellFormula(_CurrentSheet, 25, intColumnIndex, "R14C+R25C")

                If blnFirstEndOfDayPickup = True Then
                    blnFirstEndOfDayPickup = False    'first column for this cashier will be tagged

                    SpreadCellFormula(_CurrentSheet, 26, intColumnIndex, "R26C" & (intColumnIndex + 1).ToString)  'row - total entered (all pickups)
                    SpreadCellValue(_CurrentSheet, 27, intColumnIndex, Cashier.StartFloatValue)
                    SpreadCellValue(_CurrentSheet, 28, intColumnIndex, Cashier.NewFloatValue)
                    SpreadCellValue(_CurrentSheet, 29, intColumnIndex, Cashier.SystemSales)
                    SpreadCellFormula(_CurrentSheet, 30, intColumnIndex, "R27C+R29C-R28C-R30C")                   'row - variance
                Else
                    'row - total entered (all pickups), amend
                    SpreadCellFormulaAppend(_CurrentSheet, 26, 1, "+R26C" & (intColumnIndex + 1).ToString)
                End If

                For Each obj As PickupSale In objPickup.PickupSales
                    'iliterate around rows for this pickup, find cash tender/denomination match
                    For intIndex As Integer = 0 To _CurrentSheet.RowCount - 1 Step 1
                        'no tag - ignore
                        If _CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
                        'tag - string type
                        If _CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

                        objTenderDenom = CType(_CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
                        'make credit/debit non-cash tenders read-only
                        If objTenderDenom.TenderID <> 1 AndAlso objTenderDenom.TenderReadOnly = True Then SpreadCellLocked(_CurrentSheet, intIndex, intColumnIndex, True)
                        If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
                            'match found
                            If obj.PickupValue.HasValue = True Then SpreadCellValue(_CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
                        End If
                    Next
                Next
            Next
            'cashier - cash drop(s)
            For Each objPickup As Pickup In Cashier.CashDropsPickupList
                SpreadColumnAdd(_CurrentSheet, intColumnIndex)
                SpreadColumnEditable(_CurrentSheet, intColumnIndex, True)
                SpreadColumnCustomise(_CurrentSheet, intColumnIndex, Cashier.CashierEmployeeCode & " " & Cashier.CashierUserName & vbCrLf & _
                                                                    "Cash Drop" & vbCrLf & "(" & objPickup.PickupSealNumber & ")" & vbCrLf & _
                                                                    objPickup.PickupPeriodID.ToString & " : " & objPickup.PickupDate.ToShortDateString, gcintColumnWidthPickupWide, False)
                SpreadColumnMoney(_CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)

                SpreadCellFormula(_CurrentSheet, 13, intColumnIndex, "SUM(R1C:R13C)")
                SpreadCellFormula(_CurrentSheet, 24, intColumnIndex, "SUM(R15C:R24C)")
                SpreadCellFormula(_CurrentSheet, 25, intColumnIndex, "R14C+R25C")

                'row - total entered (all pickups), amend
                SpreadCellFormulaAppend(_CurrentSheet, 26, 1, "+R26C" & (intColumnIndex + 1).ToString)

                For Each obj As PickupSale In objPickup.PickupSales
                    'iliterate around rows for this pickup, find cash tender/denomination match
                    For intIndex As Integer = 0 To _CurrentSheet.RowCount - 1 Step 1
                        'no tag - ignore
                        If _CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For
                        'tag - string type
                        If _CurrentSheet.Rows(intIndex).Tag.GetType.ToString = "System.String" Then Continue For

                        objTenderDenom = CType(_CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
                        'make ALL non-cash tenders read-only
                        If objTenderDenom.TenderID <> 1 Then SpreadCellLocked(_CurrentSheet, intIndex, intColumnIndex, True)
                        If objTenderDenom.TenderID = obj.TenderID And objTenderDenom.DenominationID = obj.DenominationID Then
                            'match found
                            If obj.PickupValue.HasValue = True Then SpreadCellValue(_CurrentSheet, intIndex, intColumnIndex, obj.PickupValue)
                        End If
                    Next
                Next
            Next

            'total column
            SpreadColumnAdd(_CurrentSheet, intColumnIndex)
            SpreadColumnCustomise(_CurrentSheet, intColumnIndex, "Total", gcintColumnWidthMoney, False)
            SpreadColumnMoney(_CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)
            'cash tender
            'total cash entered - calculated
            'non-cash tenders - amendable
            'non-cash tenders - not amendable
            'total non-cash entered - calculated
            'total entered - calculated
            'total entered (all pickups) - calculated
            'start float - calculated
            'start float - new float
            'system total
            'variance - calculated

            'ignore last four rows
            For intIndex As Integer = 0 To _CurrentSheet.RowCount - 5 Step 1
                If intIndex = 26 Then Continue For 'IGNORE - total entered (all pickups)
                SpreadCellFormula(_CurrentSheet, intIndex, intColumnIndex, "SUM(R" & (intIndex + 1).ToString & "C2:R" & (intIndex + 1).ToString & "C" & intColumnIndex.ToString.Trim & ")")
            Next
        End If
    End Sub

    Public Overridable Sub SetCashierReportCurrentSheet(ByRef CurrentSheet As FarPoint.Win.Spread.SheetView) Implements ICashierReportVM.SetCashierReportCurrentSheet

        _CurrentSheet = CurrentSheet
    End Sub

    Public Overridable Sub SetCashierReportDisplayGrid(ByRef DisplayGrid As FarPoint.Win.Spread.FpSpread) Implements ICashierReportVM.SetCashierReportDisplayGrid

        _CashierReportDisplayGrid = DisplayGrid
    End Sub

    Friend Overridable Function CashierReportDisplayGridIsSet() As Boolean

        Return _CashierReportDisplayGrid IsNot Nothing
    End Function

    Friend Overridable Function CurrentSheetIsSet() As Boolean

        Return _CurrentSheet IsNot Nothing
    End Function

    Public Sub CashierReportFormFormat() Implements ICashierReportVM.CashierReportFormFormat

    End Sub

    Public Sub SetCashierReportForm(ByRef CashierReportForm As System.Windows.Forms.Form) Implements ICashierReportVM.SetCashierReportForm

    End Sub
End Class
