﻿Public Class PickupEntryVMWithoutComments
    Implements IPickupEntryVM

    Friend _PickupEntryForm As Windows.Forms.Form
    Friend _PickupEntryGrid As FpSpread
    Friend _BankingPeriodID As Nullable(Of Integer)
    Friend _CashierID As Nullable(Of Integer)
    Friend _PickupModel As Nullable(Of PickupEntry.PickupModel)
    Friend _StartFloatValue As Nullable(Of Decimal)

#Region "IPickupEntryVM Implementation"

    Public Function ActiveCellIsNotCommentsCell() As Boolean Implements IPickupEntryVM.ActiveCellIsNotCommentsCell

        Return True
    End Function

    Public Sub FormatPickupEntryForm() Implements IPickupEntryVM.FormatPickupEntryForm

    End Sub

    Public Sub FormatPickupEntryGrid() Implements IPickupEntryVM.FormatPickupEntryGrid

        If PickupEntryGridIsSet() Then
            Dim NewSheet As New SheetView

            SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
            SpreadSheetCustomiseHeader(NewSheet, gintHeaderHeightDouble)
            SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTenders, False)     'Screen Column - Denomination
            SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.PickupEntryInputGridColumn2, gcintColumnWidthMoney, False)          'Screen Column - Tray Entry

            SpreadColumnAlignLeft(NewSheet, 0)

            SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
            SpreadColumnEditable(NewSheet, 1, _PickupModel.Value <> PickupEntry.PickupModel.SpotCheck)
            SpreadColumnTagValue(NewSheet, 1, "TRAY")

            SpreadGridSheetAdd(_PickupEntryGrid, NewSheet, True, String.Empty)
            SpreadGridScrollBar(_PickupEntryGrid, ScrollBarPolicy.Never, ScrollBarPolicy.AsNeeded)
            SpreadGridInputMaps(_PickupEntryGrid)
        End If
    End Sub

    Public Function GetPickupBagComment() As String Implements IPickupEntryVM.GetPickupBagComment

        Return ""
    End Function

    Public Function GetTotalVariance() As Decimal Implements IPickupEntryVM.GetTotalVariance

        If PickupEntryGridIsSet() Then
            With _PickupEntryGrid
                If .ActiveSheet IsNot Nothing Then
                    With .ActiveSheet
                        GetTotalVariance = CType(.Cells(.RowCount - 1, 1).Value, Decimal)
                    End With
                End If
            End With
        End If
    End Function

    Public Sub PopulatePickupEntryGrid() Implements IPickupEntryVM.PopulatePickupEntryGrid
        Dim colCashierSystemSales As ActiveCashierSystemSaleCollection
        Dim colTenderDenomList As TenderDenominationListCollection
        Dim colCashDrop As PickupCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0
        Dim intColumnIndex As Integer = 0
        Dim decCashDropTotal As Decimal

        'load data
        colTenderDenomList = New TenderDenominationListCollection
        colTenderDenomList.LoadData()

        If BankingPeriodIDIsSet() And CashierIDIsSet() Then
            colCashierSystemSales = New ActiveCashierSystemSaleCollection
            colCashierSystemSales.LoadData(_BankingPeriodID.Value, _CashierID.Value)

            colCashDrop = New PickupCollection
            colCashDrop.LoadCashDropPickupList(_BankingPeriodID.Value, _CashierID.Value)
            'sum total cash drops made
            For Each obj As Pickup In colCashDrop
                If obj.PickupValue.HasValue = True Then decCashDropTotal += obj.PickupValue.Value
            Next

            If PickupEntryGridIsSet() And PickupModelIsSet() Then
                CurrentSheet = _PickupEntryGrid.ActiveSheet
                SpreadSheetClearDown(CurrentSheet)
                'cash tender
                For Each obj As TenderDenominationList In colTenderDenomList
                    If obj.TenderID <> 1 Then Continue For

                    SpreadRowAdd(CurrentSheet, intRowIndex)
                    SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                    SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
                    If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                        SpreadCellValue(CurrentSheet, intRowIndex, 1, String.Empty)
                    Else
                        SpreadCellValue(CurrentSheet, intRowIndex, 1, 0)
                    End If
                Next
                SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

                'total cash entered - calculated
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Cash Entered")
                If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                    SpreadCellValue(CurrentSheet, intRowIndex, 1, String.Empty)
                Else
                    SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)")
                End If

                'system cash total
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                Select Case _PickupModel.Value
                    Case PickupEntry.PickupModel.Floated
                        SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Cash (inc float)")
                    Case PickupEntry.PickupModel.UnFloated, PickupEntry.PickupModel.SpotCheck
                        SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Cash")
                End Select
                SpreadCellValue(CurrentSheet, intRowIndex, 1, colCashierSystemSales.SystemCashTotal + CType(If(_StartFloatValue.HasValue = True, _StartFloatValue.Value, 0), Decimal))

                'empty row
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)

                'non-cash tenders - amendable
                For Each obj As TenderDenominationList In colTenderDenomList
                    If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = True) Then Continue For

                    SpreadRowAdd(CurrentSheet, intRowIndex)
                    SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                    SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
                    SpreadCellValue(CurrentSheet, intRowIndex, 1, colCashierSystemSales.TenderTotal(obj.TenderID))
                Next
                SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

                'non-cash tenders - not amendable
                For Each obj As TenderDenominationList In colTenderDenomList
                    If obj.TenderID = 1 Or (obj.TenderID <> 1 And obj.TenderReadOnly = False) Then Continue For

                    SpreadRowAdd(CurrentSheet, intRowIndex)
                    SpreadRowTagValue(CurrentSheet, intRowIndex, obj)
                    SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)
                    SpreadCellValue(CurrentSheet, intRowIndex, 1, colCashierSystemSales.TenderTotal(obj.TenderID))

                    SpreadCellLocked(CurrentSheet, intRowIndex, 1, True)
                Next
                SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

                'total non-cash entered - calculated
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Non Cash Entered")
                SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R17C:R26C)")

                'system non-cash total
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Non Cash")
                SpreadCellValue(CurrentSheet, intRowIndex, 1, colCashierSystemSales.SystemNonCashTotal)

                'empty row
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

                'total entered - calculated
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Purple)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Entered")
                If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                    SpreadCellFormula(CurrentSheet, intRowIndex, 1, "R27C")
                Else
                    SpreadCellFormula(CurrentSheet, intRowIndex, 1, "R14C+R27C")
                End If

                'total cash drops
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Cash Drop(s)")
                SpreadCellValue(CurrentSheet, intRowIndex, 1, decCashDropTotal)

                'system total
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowColour(CurrentSheet, intRowIndex, Color.Maroon)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                Select Case _PickupModel.Value
                    Case PickupEntry.PickupModel.Floated
                        SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Total (inc float)")
                    Case PickupEntry.PickupModel.UnFloated, PickupEntry.PickupModel.SpotCheck
                        SpreadCellValue(CurrentSheet, intRowIndex, 0, "System Total")
                End Select
                SpreadCellValue(CurrentSheet, intRowIndex, 1, colCashierSystemSales.SystemTotal + CType(If(_StartFloatValue.HasValue = True, _StartFloatValue.Value, 0), Decimal))

                SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)

                'start float
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Start Float")
                SpreadCellValue(CurrentSheet, intRowIndex, 1, CType(If(_StartFloatValue.HasValue = True, _StartFloatValue.Value, 0), Decimal))

                'variance - calculated
                SpreadRowAdd(CurrentSheet, intRowIndex)
                SpreadRowBold(_PickupEntryGrid, CurrentSheet, intRowIndex)
                SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
                SpreadCellValue(CurrentSheet, intRowIndex, 0, "Variance")
                If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                    SpreadCellValue(CurrentSheet, intRowIndex, 1, String.Empty)
                Else
                    SpreadCellFormula(CurrentSheet, intRowIndex, 1, "R30C+R31C-R32C")     '(total entered + cash drops) - system total
                End If

                'add cash drops
                Dim objTenderDenom As TenderDenominationList

                'list of pickups
                For Each obj As Pickup In colCashDrop
                    'add new pickup column
                    SpreadColumnAdd(CurrentSheet, intColumnIndex)
                    SpreadColumnCustomise(CurrentSheet, intColumnIndex, "Cash Drop" & vbCrLf & "(" & obj.PickupSealNumber & ")", gcintColumnWidthPickup, False)
                    SpreadColumnMoney(CurrentSheet, intColumnIndex, True, gstrFarPointMoneyNullDisplay)

                    'cash tender values
                    For Each objPickupSales As PickupSale In obj.PickupSales
                        'iliterate around rows for this pickup, find cash tender/denomination match
                        For intIndex As Integer = 0 To intRowIndex Step 1
                            'no tag - ignore
                            If CurrentSheet.Rows(intIndex).Tag Is Nothing Then Continue For

                            objTenderDenom = CType(CurrentSheet.Rows(intIndex).Tag, TenderDenominationList)
                            If objTenderDenom.TenderID = objPickupSales.TenderID And objTenderDenom.DenominationID = objPickupSales.DenominationID Then
                                'match found
                                If objPickupSales.PickupValue.HasValue = True Then SpreadCellValue(CurrentSheet, intIndex, intColumnIndex, objPickupSales.PickupValue)
                            End If
                        Next
                    Next
                    'cash drop total - calculated, will be row 13
                    SpreadCellFormula(CurrentSheet, 13, intColumnIndex, "SUM(R1C:R13C)")
                Next
                SpreadCellMakeActive(CurrentSheet, 0, 1)      'default active cursor to the 100 pounds denom row, "tray entry" column
            End If
        End If
    End Sub

    Public Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer) Implements IPickupEntryVM.SetBankingPeriodID

        _BankingPeriodID = BankingPeriodID
    End Sub

    Public Sub SetCashierID(ByVal CashierID As Integer) Implements IPickupEntryVM.SetCashierID

        _CashierID = CashierID
    End Sub

    Public Sub SetPickupEntryForm(ByRef PickupEntryForm As System.Windows.Forms.Form) Implements IPickupEntryVM.SetPickupEntryForm

    End Sub

    Public Sub SetPickupEntryGrid(ByRef PickupEntryGrid As FarPoint.Win.Spread.FpSpread) Implements IPickupEntryVM.SetPickupEntryGrid

        _PickupEntryGrid = PickupEntryGrid
    End Sub

    Public Sub SetPickupModel(ByVal Model As PickupEntry.PickupModel) Implements IPickupEntryVM.SetPickupModel

        _PickupModel = Model
    End Sub

    Public Sub SetStartFloatValue(ByVal StartFloatValue As Decimal?) Implements IPickupEntryVM.SetStartFloatValue

        _StartFloatValue = StartFloatValue
    End Sub
#End Region

#Region "Conditions"

    Friend Overridable Function PickupEntryGridIsSet() As Boolean

        Return _PickupEntryGrid IsNot Nothing
    End Function

    Friend Overridable Function BankingPeriodIDIsSet() As Boolean

        Return _BankingPeriodID.HasValue
    End Function

    Friend Overridable Function CashierIDIsSet() As Boolean

        Return _CashierID.HasValue
    End Function

    Friend Overridable Function PickupModelIsSet() As Boolean

        Return _PickupModel.HasValue
    End Function
#End Region
End Class
