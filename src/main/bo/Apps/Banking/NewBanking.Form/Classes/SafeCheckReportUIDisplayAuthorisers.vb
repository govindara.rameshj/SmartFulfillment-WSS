﻿Public Class SafeCheckReportUIDisplayAuthorisers
    Implements ISafeCheckReportUI


#Region "Interface"

    Public Sub RunReport(ByRef SelectedPeriod As Period, _
                         ByVal StoreID As String, _
                         ByVal StoreName As String) Implements ISafeCheckReportUI.RunReport

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceComments(ByVal Grid As FpSpread, _
                                               ByVal SheetView As SheetView, _
                                               ByRef Index As Integer, _
                                               ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceComments

        'N/A

    End Sub

    Public Sub DisplaySafeMaintenanceAuthorisers(ByVal Grid As FpSpread, _
                                                 ByVal SheetView As SheetView, _
                                                 ByRef Index As Integer, _
                                                 ByVal SelectedPeriod As Period) Implements ISafeCheckReportUI.DisplaySafeMaintenanceAuthorisers

        Dim DT As DataTable
        Dim Repository As New SafeCheckReportRepository

        Dim UserID As String = "User ID"
        Dim UserName As String = "User Name"
        Dim LastUpdated As String = "Authorised Date/Time"
        Dim AuthorisingManagerID As String = "Authorising Manager ID"
        Dim AuthorisingManagerName As String = "Authorising Manager Name"

        If SelectedPeriod.SafeMaintenanceDone Then

            DT = Repository.GetSafeCheckRecordAuthorisers(SelectedPeriod.PeriodID)

            If Not DT Is Nothing Then

                If ValidateRecords(DT) Then

                    SpreadRowDrawLine(SheetView, Index, Color.Black, 1)

                    SpreadColumnSetWidth(SheetView, 3, 150)

                    SpreadRowAdd(SheetView, Index)
                    SpreadCellSetAsBold(Grid, SheetView, Index, 0)
                    SpreadCellValue(SheetView, Index, 0, UserID)
                    SpreadCellSetAsTextFormat(SheetView, Index, 1)
                    SpreadRowMergeColumns(SheetView, Index, 1, 2)
                    SpreadCellValue(SheetView, Index, 1, DT.Rows(0)(0).ToString)
                    SpreadCellSetAsBold(Grid, SheetView, Index, 3)
                    SpreadCellValue(SheetView, Index, 3, AuthorisingManagerID)
                    SpreadCellSetAsTextFormat(SheetView, Index, 3)
                    SpreadColumnAlignLeft(SheetView, 4)
                    SpreadCellValue(SheetView, Index, 4, DT.Rows(0)(1).ToString)

                    Index += 1
                    AddNewRowAndMakeFirstCellBold(Grid, SheetView, Index)
                    SpreadCellValue(SheetView, Index, 0, UserName)
                    SpreadCellSetAsTextFormat(SheetView, Index, 1)
                    SpreadRowMergeColumns(SheetView, Index, 1, 2)
                    SpreadCellValue(SheetView, Index, 1, DT.Rows(0)(2).ToString)
                    SpreadCellSetAsBold(Grid, SheetView, Index, 3)
                    SpreadCellValue(SheetView, Index, 3, AuthorisingManagerName)
                    SpreadCellSetAsTextFormat(SheetView, Index, 3)
                    SpreadColumnAlignLeft(SheetView, 4)
                    SpreadCellValue(SheetView, Index, 4, DT.Rows(0)(3).ToString)


                    Index += 1
                    AddNewRowAndMakeFirstCellBold(Grid, SheetView, Index)
                    SpreadRowMergeColumns(SheetView, Index, 1, 2)
                    SpreadCellSetAsBold(Grid, SheetView, Index, 3)
                    SpreadCellValue(SheetView, Index, 3, LastUpdated)
                    SpreadCellSetAsTextFormat(SheetView, Index, 3)
                    SpreadColumnAlignLeft(SheetView, 4)
                    SpreadCellValue(SheetView, Index, 4, DT.Rows(0)(4).ToString)

                    SpreadRowDrawLine(SheetView, Index, Color.Black, 1)
                    Index += 1
                    AddNewRowAndMakeTheTextBold(Grid, SheetView, Index)
                    SpreadRowDrawLine(SheetView, Index, Color.Black, 1)

                End If

            End If

        End If

    End Sub

    Public Sub DisplaySafeMaintenanceScannedBankingBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, _
                                                        ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceScannedBankingBags

        'N/A

    End Sub
    Public Sub DisplaySafeMaintenanceCommentsForAllBags(ByVal Grid As FpSpread, _
                                                        ByVal SheetView As SheetView, _
                                                        ByRef Index As Integer, ByVal PeriodID As Integer) Implements ISafeCheckReportUI.DisplaySafeMaintenanceCommentsForAllBags

        'N/A


    End Sub

#End Region

#Region "Private Functions & Procedures"

    Private Sub AddNewRowAndMakeTheTextBold(ByVal reportGrid As FarPoint.Win.Spread.FpSpread, ByVal reportsheetView As FarPoint.Win.Spread.SheetView, ByVal index As Integer)

        SpreadRowAdd(reportsheetView, index)
        SpreadRowBold(reportGrid, reportsheetView, index)

    End Sub

    Private Sub AddNewRowAndMakeFirstCellBold(ByVal reportGrid As FarPoint.Win.Spread.FpSpread, ByVal reportsheetView As FarPoint.Win.Spread.SheetView, ByVal index As Integer)

        SpreadRowAdd(reportsheetView, index)
        SpreadCellSetAsBold(reportGrid, reportsheetView, index, 0)

    End Sub

    Private Function ValidateRecords(ByVal record As DataTable) As Boolean

        If ((record.Rows(0)(0).ToString.Equals(String.Empty)) Or _
            (record.Rows(0)(1).ToString.Equals(String.Empty)) Or _
            (record.Rows(0)(2).ToString.Equals(String.Empty)) Or _
            (record.Rows(0)(3).ToString.Equals(String.Empty)) Or _
            (record.Rows(0)(4).ToString.Equals(String.Empty))) Then

            Return False

        End If

        Return True

    End Function

#End Region

   
End Class