﻿Public Class FloatVMWithoutComments
    Implements IFloatVM

    Friend _FloatGrid As FpSpread

    Public Sub SetFloatGridControl(ByRef FloatGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatVM.SetFloatGridControl

        _FloatGrid = FloatGrid
    End Sub

    Public Sub FloatGridFormat() Implements IFloatVM.FloatGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 4, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatDisplayGridColumn1, gcintColumnWidthSeal, False)     'Screen Column - Float Seal
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatDisplayGridColumn2, gcintColumnWidthMoney, False)    'Screen Column - Float Value
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.FloatDisplayGridColumn3, gcintColumnWidthUser, False)     'Screen Column - Float Created From
        SpreadColumnCustomise(NewSheet, 3, My.Resources.ScreenColumns.FloatDisplayGridColumn4, gcintColumnWidthUser, False)     'Screen Column - Assigned To

        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)

        SpreadGridSheetAdd(_FloatGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)

        SpreadGridDeactiveKey(_FloatGrid, Keys.F2)
        SpreadGridDeactiveKey(_FloatGrid, Keys.F3)
    End Sub

    Public Function GetFloatList(ByVal BankingPeriodID As Integer) As Core.FloatListCollection Implements IFloatVM.GetFloatList

        GetFloatList = New FloatListCollection
        GetFloatList.LoadData(BankingPeriodID)
    End Function

    Public Sub FloatGridPopulate(ByRef FloatLists As FloatListCollection) Implements IFloatVM.FloatGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _FloatGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As FloatList In FloatLists
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj.FloatID)                     'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.FloatSealNumber)            'Screen Column - Float Seal
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.FloatValue)                 'Screen Column - Float Value
            SpreadCellValue(CurrentSheet, intRowIndex, 2, obj.FloatCreatedFromUserName)   'Screen Column - Float Created From
            SpreadCellValue(CurrentSheet, intRowIndex, 3, obj.AssignedToUserName)         'Screen Column - Assigned To
        Next
    End Sub

    Public Overridable Sub SetFloatForm(ByRef FloatForm As System.Windows.Forms.Form) Implements IFloatVM.SetFloatForm

    End Sub

    Public Overridable Sub FloatFormFormat() Implements IFloatVM.FloatFormFormat

    End Sub

#Region "Interface: Unassign Float"

    Public Function IsManualCheckRequired() As Boolean Implements IFloatVM.IsManualCheckRequired

    End Function

    Public Function PerformManualCheck(ByVal StartFloatID As Integer) As Boolean Implements IFloatVM.PerformManualCheck

    End Function

    Public Sub CompleteUnAssigningTheFloatWithManualCheck(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloatWithManualCheck

    End Sub

    Public Sub CompleteUnAssigningTheFloat(ByVal FloatID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String) Implements IFloatVM.CompleteUnAssigningTheFloat

    End Sub

    Public ReadOnly Property NewFloatValue() As Decimal Implements IFloatVM.NewFloatValue
        Get

        End Get
    End Property
#End Region
End Class