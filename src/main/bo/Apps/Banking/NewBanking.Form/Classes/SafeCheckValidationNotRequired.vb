﻿Public Class SafeCheckValidationNotRequired
    Implements ISafeCheckValidation

    Public Function AllowSafeCheckCompletion() As Boolean Implements ISafeCheckValidation.AllowSafeCheckCompletion

        Return False

    End Function

    Public Sub SaveSafeCheckUserDetails(ByRef OdbcConnection As clsOasys3DB, _
                                        ByVal selectedPeriod As Period, _
                                        ByVal UserId1 As Integer, _
                                        ByVal UserId2 As Integer, _
                                        ByVal LastAmended As Date) Implements ISafeCheckValidation.SaveSafeCheckUserDetails

    End Sub

End Class