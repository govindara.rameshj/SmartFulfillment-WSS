﻿Public Class UnFloatedPickupVMWithoutComments
    Implements IUnFloatedPickupVM

    Friend _UnFloatedPickupGrid As FpSpread

    Public Function GetUnFloatedPickupList(ByVal BankingPeriodID As Integer) As Core.UnFloatedPickupListCollection Implements IUnFloatedPickupVM.GetUnFloatedPickupList

        GetUnFloatedPickupList = New UnFloatedPickupListCollection
        GetUnFloatedPickupList.LoadData(BankingPeriodID)
    End Function

    Public Sub SetUnFloatedPickupGridControl(ByRef UnFloatedPickupGrid As FarPoint.Win.Spread.FpSpread) Implements IUnFloatedPickupVM.SetUnFloatedPickupGridControl

        _UnFloatedPickupGrid = UnFloatedPickupGrid
    End Sub

    Public Sub UnFloatedPickupGridFormat() Implements IUnFloatedPickupVM.UnFloatedPickupGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 3, Model.SelectionUnit.Row)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.UnFloatedPickupDisplayGridColumn1, gcintColumnWidthUserWide, False)     'Screen Column - Cashier / Design Consultant
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.UnFloatedPickupDisplayGridColumn2, gcintColumnWidthSeal, False)         'Screen Column - Pickup Seal
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.UnFloatedPickupDisplayGridColumn3, gcintColumnWidthCommentsWide, False) 'Screen Column - Comment

        SpreadColumnAlignLeft(NewSheet, 2)
        SpreadSheetCustomiseHeaderAlignLeft(NewSheet, 2)

        SpreadGridSheetAdd(_UnFloatedPickupGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_UnFloatedPickupGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.Never)
    End Sub

    Public Sub UnFloatedPickupGridPopulate(ByRef UnFloatedPickupLists As Core.UnFloatedPickupListCollection) Implements IUnFloatedPickupVM.UnFloatedPickupGridPopulate
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        CurrentSheet = _UnFloatedPickupGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)
        For Each obj As NewBanking.Core.UnFloatedPickupList In UnFloatedPickupLists
            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj.CashierID)         'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.CashierUserName)  'Screen Column - Cashier / Design Consultant
            SpreadCellValue(CurrentSheet, intRowIndex, 1, obj.PickupSealNumber) 'Screen Column - Pickup Seal

            Dim comment = obj.PickupComment

            ' Special comment for the uncompleted pickup for WebOrders(C&C) cashier
            If (obj.CashierID = GlobalConstants.WebOrderCashierID AndAlso Not obj.PickupID.HasValue) Then
                comment = "Pickup is completed automatically"
            End If

            SpreadCellValue(CurrentSheet, intRowIndex, 2, comment)    'Screen Column - Pickup Comment
        Next
    End Sub
End Class
