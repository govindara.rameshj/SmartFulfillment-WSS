﻿Public Class PickupEntryVMWithComments
    Implements IPickupEntryVM

    Friend _PickupEntryForm As Windows.Forms.Form
    Friend _PickupEntryGrid As FpSpread
    Friend _BankingPeriodID As Nullable(Of Integer)
    Friend _CashierID As Nullable(Of Integer)
    Friend _PickupModel As Nullable(Of PickupEntry.PickupModel)
    Friend _StartFloatValue As Nullable(Of Decimal)
    Friend _CommentsRowIndex As Nullable(Of Integer)
    Friend _TenderDenomList As TenderDenominationListCollection
    Friend _CashierSystemSales As ActiveCashierSystemSaleCollection
    Friend _CashDrops As PickupCollection
    Friend _CurrentSheet As SheetView
    Friend _CurrentRowIndex As Nullable(Of Integer)
    Friend _CurrentColumnIndex As Nullable(Of Integer)
    Friend _CashDropTotal As Decimal
    Friend _NumberOfRowsToAddForComments As Integer
    Friend _TrayColumnWidth As Integer
    Friend _FormAdditionalHeight As Integer
    Friend _VarianceRowIndex As Nullable(Of Integer)

#Region "IPickupEntryVM Implementation"

    Public Function ActiveCellIsNotCommentsCell() As Boolean Implements IPickupEntryVM.ActiveCellIsNotCommentsCell

        If CurrentSheetIsSet() And CommentsRowIndexIsSet() Then
            Return _CurrentSheet.ActiveCell.Row.Index <> _CommentsRowIndex.Value
        End If
    End Function

    Public Sub FormatPickupEntryForm() Implements IPickupEntryVM.FormatPickupEntryForm

        If PickupEntryFormIsSet() Then
            SetFormAdditionalHeight()
            With _PickupEntryForm
                If .Top < _FormAdditionalHeight Then
                    .Height += .Top
                    .Top = 0
                Else
                    .Height += _FormAdditionalHeight
                    .Top -= _FormAdditionalHeight
                End If
            End With
        End If
    End Sub

    Public Sub FormatPickupEntryGrid() Implements IPickupEntryVM.FormatPickupEntryGrid

        If PickupEntryGridIsSet() Then
            Dim NewSheet As New SheetView

            SetTrayColumnWidth()
            SpreadSheetCustomise(NewSheet, 2, Model.SelectionUnit.Cell)
            SpreadSheetCustomiseHeader(NewSheet, gintHeaderHeightDouble)
            SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.PickupEntryInputGridColumn1, gcintColumnWidthAllTenders, False)     'Screen Column - Denomination
            SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.PickupEntryInputGridColumn2, _TrayColumnWidth, False)          'Screen Column - Tray Entry

            SpreadColumnAlignLeft(NewSheet, 0)

            SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
            SpreadColumnEditable(NewSheet, 1, _PickupModel.Value <> PickupEntry.PickupModel.SpotCheck)
            SpreadColumnTagValue(NewSheet, 1, "TRAY")

            SpreadGridSheetAdd(_PickupEntryGrid, NewSheet, True, String.Empty)
            SpreadGridScrollBar(_PickupEntryGrid, ScrollBarPolicy.AsNeeded, ScrollBarPolicy.AsNeeded)
            SpreadGridInputMaps(_PickupEntryGrid)
        End If
    End Sub

    Public Function GetPickupBagComment() As String Implements IPickupEntryVM.GetPickupBagComment

        GetPickupBagComment = ""
        If CurrentSheetIsSet() And CommentsRowIndexIsSet Then
            GetPickupBagComment = _CurrentSheet.Cells(_CommentsRowIndex.Value, 1).Text
        End If
    End Function

    Public Function GetTotalVariance() As Decimal Implements IPickupEntryVM.GetTotalVariance

        If PickupEntryGridIsSet() And VarianceRowIndexIsSet() Then
            With _PickupEntryGrid
                If .ActiveSheet IsNot Nothing Then
                    With .ActiveSheet
                        GetTotalVariance = CType(.Cells(_VarianceRowIndex.Value, 1).Value, Decimal)
                    End With
                End If
            End With
        End If
    End Function

    Public Sub PopulatePickupEntryGrid() Implements IPickupEntryVM.PopulatePickupEntryGrid

        InitialiseCurrentGridPoistionIndices()
        LoadTenderDenominationList()
        If BankingPeriodIDIsSet() And CashierIDIsSet() Then
            LoadCashierSystemSales()
            If PickupEntryGridIsSet() And PickupModelIsSet() And CashierSystemSalesIsSet() Then
                SetCurrentSheet()
                SpreadSheetClearDown(_CurrentSheet)
                AddCashTenderRow()
                AddTotalCashEnteredRow()
                AddSystemCashTotalRow()
                AddEmptyRow()
                AddAmendableNonCashTendersRow()
                AddUnAmendableNonCashTenders()
                AddNonCashEnteredTotalRow()
                AddSystemNonCashTotalRow()
                AddEmptyRowFollowedByLine()
                AddTotalEnteredRow()
                AddCashDropsTotalRow()
                AddSystemTotalRow()
                AddStartFloatRow()
                AddVarianceRow()
                AddCommentsRow()
                AddCashDropColumns()
                SpreadCellMakeActive(_CurrentSheet, 0, 1)      'default active cursor to the 100 pounds denom row, "tray entry" column
            End If
        End If
    End Sub

    Public Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer) Implements IPickupEntryVM.SetBankingPeriodID

        _BankingPeriodID = BankingPeriodID
    End Sub

    Public Sub SetCashierID(ByVal CashierID As Integer) Implements IPickupEntryVM.SetCashierID

        _CashierID = CashierID
    End Sub

    Public Sub SetPickupEntryForm(ByRef PickupEntryForm As System.Windows.Forms.Form) Implements IPickupEntryVM.SetPickupEntryForm

        _PickupEntryForm = PickupEntryForm
    End Sub

    Public Sub SetPickupEntryGrid(ByRef PickupEntryGrid As FarPoint.Win.Spread.FpSpread) Implements IPickupEntryVM.SetPickupEntryGrid

        _PickupEntryGrid = PickupEntryGrid
    End Sub

    Public Sub SetPickupModel(ByVal Model As PickupEntry.PickupModel) Implements IPickupEntryVM.SetPickupModel

        _PickupModel = Model
    End Sub

    Public Sub SetStartFloatValue(ByVal StartFloatValue As Decimal?) Implements IPickupEntryVM.SetStartFloatValue

        _StartFloatValue = StartFloatValue
    End Sub
#End Region

#Region "Conditions"

    Friend Overridable Function BankingPeriodIDIsSet() As Boolean

        Return _BankingPeriodID.HasValue
    End Function

    Private Function CashDropsIsSet() As Boolean

        Return _CashDrops IsNot Nothing
    End Function

    Friend Overridable Function CashierIDIsSet() As Boolean

        Return _CashierID.HasValue
    End Function

    Friend Overridable Function PickupEntryGridIsSet() As Boolean

        Return _PickupEntryGrid IsNot Nothing
    End Function

    Friend Overridable Function PickupEntryFormIsSet() As Boolean

        Return _PickupEntryForm IsNot Nothing
    End Function

    Friend Overridable Function PickupModelIsSet() As Boolean

        Return _PickupModel.HasValue
    End Function

    Friend Overridable Function CurrentSheetIsSet() As Boolean

        Return _CurrentSheet IsNot Nothing
    End Function

    Friend Overridable Function CurrentRowIndexIsSet() As Boolean

        Return _CurrentRowIndex.HasValue
    End Function

    Friend Overridable Function CashierSystemSalesIsSet() As Boolean

        Return _CashierSystemSales IsNot Nothing
    End Function

    Friend Overridable Function TenderDenominationListIsSet() As Boolean

        Return _TenderDenomList IsNot Nothing
    End Function

    Friend Overridable Function CurrentColumnIndexIsSet() As Boolean

        Return _CurrentColumnIndex.HasValue
    End Function

    Friend Overridable Function CommentsRowIndexIsSet() As Boolean

        Return _CommentsRowIndex.HasValue
    End Function

    Friend Overridable Function VarianceRowIndexIsSet() As Boolean

        Return _VarianceRowIndex.HasValue
    End Function
#End Region

#Region "Data"

    Friend Overridable Function CalculateCashDropTotal() As Decimal

        LoadCashDrops()
        If CashDropsIsSet() Then
            For Each CashDrop As Pickup In _CashDrops
                With CashDrop
                    If .PickupValue.HasValue Then
                        _CashDropTotal += .PickupValue.Value
                    End If
                End With
            Next
        End If
    End Function

    Friend Overridable Sub LoadCashDrops()

        If Not CashDropsIsSet() Then
            _CashDrops = New PickupCollection
            If BankingPeriodIDIsSet() And CashierIDIsSet() Then
                _CashDrops.LoadCashDropPickupList(_BankingPeriodID.Value, _CashierID.Value)
            End If
        End If
    End Sub

    Friend Overridable Sub LoadCashierSystemSales()

        If BankingPeriodIDIsSet() And CashierIDIsSet() Then
            _CashierSystemSales = New ActiveCashierSystemSaleCollection
            _CashierSystemSales.LoadData(_BankingPeriodID.Value, _CashierID.Value)
        End If
    End Sub

    Friend Overridable Sub LoadTenderDenominationList()

        _TenderDenomList = New TenderDenominationListCollection
        _TenderDenomList.LoadData()
    End Sub
#End Region

#Region "UI"

    Friend Overridable Sub AddAmendableNonCashTendersRow()

        If TenderDenominationListIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() And CashierSystemSalesIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenomList
                With TenderDenomination
                    If .IsAmendableNonCashTenderType Then
                        AddRowToCurrentSheet()
                        SpreadRowTagValue(_CurrentSheet, _CurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, .TenderText)
                        SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashierSystemSales.TenderTotal(.TenderID))
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddCashDropsTotalRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            CalculateCashDropTotal()
            If CashDropsIsSet() Then
                AddRowToCurrentSheet()
                SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Maroon)
                SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
                SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
                SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Total Cash Drop(s)")
                SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashDropTotal)
            End If
        End If
    End Sub

    Friend Overridable Sub AddCashDropColumns()

        If CashDropsIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() And CurrentColumnIndexIsSet() Then
            Dim TenderDenom As TenderDenominationList

            For Each CashDrop As Pickup In _CashDrops
                AddColumnToCurrentSheet()
                SpreadColumnCustomise(_CurrentSheet, _CurrentColumnIndex.Value, "Cash Drop" & vbCrLf & "(" & CashDrop.PickupSealNumber & ")", gcintColumnWidthPickup, False)
                SpreadColumnMoney(_CurrentSheet, _CurrentColumnIndex.Value, True, gstrFarPointMoneyNullDisplay)
                For Each CashDropPickupSale As PickupSale In CashDrop.PickupSales
                    For Index As Integer = 0 To _CurrentRowIndex.Value Step 1
                        If _CurrentSheet.Rows(Index).Tag IsNot Nothing Then
                            TenderDenom = CType(_CurrentSheet.Rows(Index).Tag, TenderDenominationList)
                            With CashDropPickupSale
                                If TenderDenom.TenderID = .TenderID And TenderDenom.DenominationID = .DenominationID Then
                                    If .PickupValue.HasValue Then
                                        SpreadCellValue(_CurrentSheet, Index, _CurrentColumnIndex.Value, .PickupValue)
                                    End If
                                End If
                            End With
                        End If
                    Next
                Next
                'cash drop total - calculated, will be row 13
                SpreadCellFormula(_CurrentSheet, 13, _CurrentColumnIndex.Value, "SUM(R1C:R13C)")
                FormatCommentsCell()
            Next
        End If
    End Sub

    Friend Overridable Sub AddCashTenderRow()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() And TenderDenominationListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenomList
                With TenderDenomination
                    If .IsCashTenderType Then
                        AddRowToCurrentSheet()
                        SpreadRowTagValue(_CurrentSheet, _CurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, .TenderText)
                        If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, String.Empty)
                        Else
                            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, 0)
                        End If
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddCommentsRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            SetNumberOfCommentsRowsToAdd()
            SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
            _CommentsRowIndex = _CurrentRowIndex.Value + 1
            AddRowsToCurrentSheet(_NumberOfRowsToAddForComments)
            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex.Value, _NumberOfRowsToAddForComments, 0, 1)
            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex.Value, _NumberOfRowsToAddForComments, 1, 1)
            SpreadCellSetAsBold(_PickupEntryGrid, _CurrentSheet, _CommentsRowIndex.Value, 0)
            SpreadCellSetVerticalAlignment(_CurrentSheet, _CommentsRowIndex.Value, 0, CellVerticalAlignment.Top)
            SpreadCellValue(_CurrentSheet, _CommentsRowIndex.Value, 0, "Comments")
            _CurrentColumnIndex = 1
            FormatCommentsCell()
        End If
    End Sub

    Friend Overridable Sub AddEmptyRow()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
        End If
    End Sub

    Friend Overridable Sub AddEmptyRowFollowedByLine()

        AddEmptyRow()
        SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
    End Sub

    Friend Overridable Sub AddNonCashEnteredTotalRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Non Cash Entered")
            SpreadCellFormula(_CurrentSheet, _CurrentRowIndex.Value, 1, "SUM(R17C:R26C)")
        End If
    End Sub

    Friend Overridable Sub AddStartFloatRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Start Float")
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, CType(If(_StartFloatValue.HasValue, _StartFloatValue.Value, 0), Decimal))
        End If
    End Sub

    Friend Overridable Sub AddSystemCashTotalRow()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() And CashierSystemSalesIsSet() And PickupModelIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            Select Case _PickupModel
                Case PickupEntry.PickupModel.Floated
                    SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "System Cash (inc float)")
                Case PickupEntry.PickupModel.UnFloated, PickupEntry.PickupModel.SpotCheck
                    SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "System Cash")
            End Select
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashierSystemSales.SystemCashTotal + CType(If(_StartFloatValue.HasValue = True, _StartFloatValue.Value, 0), Decimal))
        End If
    End Sub

    Friend Overridable Sub AddSystemNonCashTotalRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() And CashierSystemSalesIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "System Non Cash")
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashierSystemSales.SystemNonCashTotal)
        End If
    End Sub

    Friend Overridable Sub AddSystemTotalRow()

        If PickupEntryGridIsSet() And PickupModelIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Maroon)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            Select Case _PickupModel
                Case PickupEntry.PickupModel.Floated
                    SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "System Total (inc float)")
                Case PickupEntry.PickupModel.UnFloated, PickupEntry.PickupModel.SpotCheck
                    SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "System Total")
            End Select
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashierSystemSales.SystemTotal + CType(If(_StartFloatValue.HasValue = True, _StartFloatValue.Value, 0), Decimal))
            SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub AddVarianceRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Variance")
            If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, String.Empty)
            Else
                SpreadCellFormula(_CurrentSheet, _CurrentRowIndex.Value, 1, "R30C+R31C-R32C")     '(total entered + cash drops) - system total
            End If
            SetVarianceRowIndex(_CurrentRowIndex.Value)
        End If
    End Sub

    Friend Overridable Sub AddTotalCashEnteredRow()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Cash Entered")
            If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, String.Empty)
            Else
                SpreadCellFormula(_CurrentSheet, _CurrentRowIndex.Value, 1, "SUM(R1C:R13C)")
            End If
        End If
    End Sub

    Friend Overridable Sub AddTotalEnteredRow()

        If PickupEntryGridIsSet() And CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            AddRowToCurrentSheet()
            SpreadRowColour(_CurrentSheet, _CurrentRowIndex.Value, Color.Purple)
            SpreadRowBold(_PickupEntryGrid, _CurrentSheet, _CurrentRowIndex.Value)
            SpreadRowRemoveFocus(_CurrentSheet, _CurrentRowIndex.Value)
            SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, "Total Entered")
            If _PickupModel = PickupEntry.PickupModel.SpotCheck Then
                SpreadCellFormula(_CurrentSheet, _CurrentRowIndex.Value, 1, "R27C")
            Else
                SpreadCellFormula(_CurrentSheet, _CurrentRowIndex.Value, 1, "R14C+R27C")
            End If
        End If
    End Sub

    Friend Overridable Sub AddUnAmendableNonCashTenders()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() And TenderDenominationListIsSet() Then
            For Each TenderDenomination As TenderDenominationList In _TenderDenomList
                With TenderDenomination
                    If .IsUnAmendableNonCashTenderType Then
                        AddRowToCurrentSheet()
                        SpreadRowTagValue(_CurrentSheet, _CurrentRowIndex.Value, TenderDenomination)
                        SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 0, .TenderText)
                        SpreadCellValue(_CurrentSheet, _CurrentRowIndex.Value, 1, _CashierSystemSales.TenderTotal(.TenderID))
                        SpreadCellLocked(_CurrentSheet, _CurrentRowIndex.Value, 1, True)
                    End If
                End With
            Next
            SpreadRowDrawLine(_CurrentSheet, _CurrentRowIndex.Value, Color.Black, 1)
        End If
    End Sub

    Friend Overridable Sub FormatCommentsCell()

        If CurrentSheetIsSet() And CurrentColumnIndexIsSet() And CommentsRowIndexIsSet() Then
            SpreadCellSetVerticalAlignment(_CurrentSheet, _CommentsRowIndex.Value, 1, CellVerticalAlignment.Top)
            SpreadCellMergeCells(_CurrentSheet, _CommentsRowIndex.Value, _NumberOfRowsToAddForComments, 1, _CurrentColumnIndex.Value)
            SpreadCellSetVerticalAlignment(_CurrentSheet, _CommentsRowIndex.Value, _CurrentColumnIndex.Value, CellVerticalAlignment.Top)
            SpreadCellSetAsTextFormat(_CurrentSheet, _CommentsRowIndex.Value, 1)
            SpreadCellSetAsMultiLine(_CurrentSheet, _CommentsRowIndex.Value, _CurrentColumnIndex.Value, _NumberOfRowsToAddForComments > 1)
        End If
    End Sub

    Friend Overridable Sub InitialiseCurrentGridPoistionIndices()

        _CurrentRowIndex = 0
        _CurrentColumnIndex = 0
    End Sub

    Friend Overridable Sub SetCurrentSheet()

        If PickupEntryGridIsSet() Then
            _CurrentSheet = _PickupEntryGrid.ActiveSheet
        End If
    End Sub

    Friend Overloads Sub SetFormAdditionalHeight()

        LoadCashDrops()
        SetNumberOfCommentsRowsToAdd()
        _FormAdditionalHeight = _NumberOfRowsToAddForComments * 20
    End Sub

    Friend Overridable Sub SetTrayColumnWidth()

        LoadCashDrops()
        If CashDropsIsSet() AndAlso _CashDrops.Count > 0 Then
            _TrayColumnWidth = gcintColumnWidthMoney
        Else
            _TrayColumnWidth = gcintColumnWidthComments
        End If
    End Sub

    Friend Overridable Sub AddRowToCurrentSheet()

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            Dim NewIndex As Integer = _CurrentRowIndex.Value

            SpreadRowAdd(_CurrentSheet, NewIndex)
            _CurrentRowIndex = NewIndex
        End If
    End Sub

    Friend Overridable Sub AddRowsToCurrentSheet(ByVal NumberOfRowstoAdd As Integer)

        If CurrentSheetIsSet() And CurrentRowIndexIsSet() Then
            Dim NewIndex As Integer = _CurrentRowIndex.Value

            SpreadRowAdd(_CurrentSheet, NewIndex, NumberOfRowstoAdd)
            _CurrentRowIndex = NewIndex
        End If
    End Sub

    Friend Overridable Sub AddColumnToCurrentSheet()

        If CurrentSheetIsSet() And CurrentColumnIndexIsSet() Then
            Dim NewIndex As Integer = _CurrentColumnIndex.Value

            SpreadColumnAdd(_CurrentSheet, NewIndex)
            _CurrentColumnIndex = NewIndex
        End If
    End Sub

    Friend Overridable Sub SetNumberOfCommentsRowsToAdd()

        If CashDropsIsSet() Then
            If _CashDrops.Count < 6 Then
                _NumberOfRowsToAddForComments = Integer.Parse((6 / (_CashDrops.Count + 1)).ToString) + 1
            Else
                _NumberOfRowsToAddForComments = 1
            End If
        Else
            _NumberOfRowsToAddForComments = 6
        End If
    End Sub

    Friend Overridable Sub SetVarianceRowIndex(ByVal Index As Integer)

        _VarianceRowIndex = Index
    End Sub
#End Region
End Class
