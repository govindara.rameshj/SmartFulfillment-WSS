﻿Public Class FloatAssignVMWithoutComments
    Implements IFloatAssignVM

    Friend _FloatAssignGrid As FpSpread

    Public Sub FloatAssignGridFormat() Implements IFloatAssignVM.FloatAssignGridFormat
        Dim NewSheet As New SheetView

        SpreadSheetCustomise(NewSheet, 3, Model.SelectionUnit.Cell)
        SpreadColumnCustomise(NewSheet, 0, My.Resources.ScreenColumns.FloatAssignInputGridColumn1, gcintColumnWidthCashTender, False)   'Screen Column - Denomination
        SpreadColumnCustomise(NewSheet, 1, My.Resources.ScreenColumns.FloatAssignInputGridColumn2, gcintColumnWidthMoney, False)        'Screen Column - Float
        SpreadColumnCustomise(NewSheet, 2, My.Resources.ScreenColumns.FloatAssignInputGridColumn3, gcintColumnWidthMoneyWide, False)    'Screen Column - Manual Check Float

        SpreadColumnAlignLeft(NewSheet, 0)
        SpreadColumnMoney(NewSheet, 1, True, gstrFarPointMoneyNullDisplay)
        SpreadColumnMoney(NewSheet, 2, True, gstrFarPointMoneyNullDisplay)

        SpreadGridSheetAdd(_FloatAssignGrid, NewSheet, True, String.Empty)
        SpreadGridScrollBar(_FloatAssignGrid, ScrollBarPolicy.Never, ScrollBarPolicy.Never)

        SpreadGridInputMaps(_FloatAssignGrid)

        SpreadGridDeactiveKey(_FloatAssignGrid, Keys.F2)
        SpreadGridDeactiveKey(_FloatAssignGrid, Keys.F3)
    End Sub

    Public Sub FloatAssignGridPopulate(ByVal FloatID As Integer) Implements IFloatAssignVM.FloatAssignGridPopulate
        Dim colTenderDenominationList As TenderDenominationListCollection
        Dim colFloatBag As FloatBagCollection
        Dim CurrentSheet As SheetView
        Dim intRowIndex As Integer = 0

        'load data
        colTenderDenominationList = New TenderDenominationListCollection
        colTenderDenominationList.LoadData()

        colFloatBag = New FloatBagCollection
        colFloatBag.LoadData(FloatID)

        CurrentSheet = _FloatAssignGrid.ActiveSheet
        SpreadSheetClearDown(CurrentSheet)

        For Each obj As TenderDenominationList In colTenderDenominationList
            If obj.TenderID <> 1 Then Continue For

            SpreadRowAdd(CurrentSheet, intRowIndex)

            SpreadRowTagValue(CurrentSheet, intRowIndex, obj)                                                'Primary Key
            SpreadCellValue(CurrentSheet, intRowIndex, 0, obj.TenderText)                                    'Screen Column - Denomination
            SpreadCellValue(CurrentSheet, intRowIndex, 1, colFloatBag.DenominationValue(obj.DenominationID)) 'Screen Column - Float
            SpreadCellValue(CurrentSheet, intRowIndex, 2, New System.Nullable(Of Decimal)) 'Screen Column - Manual Check Float
        Next
        'line underneath cash denominations
        SpreadRowDrawLine(CurrentSheet, intRowIndex, Color.Black, 1)
        'add "total" row
        SpreadRowAdd(CurrentSheet, intRowIndex)
        SpreadRowBold(_FloatAssignGrid, CurrentSheet, intRowIndex)
        SpreadRowRemoveFocus(CurrentSheet, intRowIndex)
        SpreadCellValue(CurrentSheet, intRowIndex, 0, "Total Amounts")
        'screen column - float, sum denominations
        SpreadCellValue(CurrentSheet, intRowIndex, 1, 0)
        SpreadCellFormula(CurrentSheet, intRowIndex, 1, "SUM(R1C:R13C)")

        SpreadCellMakeActive(CurrentSheet, 0, 2)      'default active cursor to the 100 pounds denom row, "manual check" column
    End Sub

    Public Overridable Sub FloatChecked(ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer) Implements IFloatAssignVM.FloatChecked

        NewBanking.Core.FloatChecked(FloatBagID, UserID1, UserID2)
    End Sub

    Public Sub SetFloatAssignGridControl(ByRef FloatAssignGrid As FarPoint.Win.Spread.FpSpread) Implements IFloatAssignVM.SetFloatAssignGridControl

        _FloatAssignGrid = FloatAssignGrid
    End Sub

    Public Sub FloatAssignGridSetupCommentsCell(ByVal MakeEditable As Boolean) Implements IFloatAssignVM.FloatAssignGridSetupCommentsCell

    End Sub

    Public Sub SetFloatAssignForm(ByRef FloatAssignForm As System.Windows.Forms.Form) Implements IFloatAssignVM.SetFloatAssignForm

    End Sub

    Public Sub FloatAssignFormFormat() Implements IFloatAssignVM.FloatAssignFormFormat

    End Sub

    Public Sub AssignCommentsFromGrid(ByRef AssignTo As Banking.Core.FloatBanking) Implements IFloatAssignVM.AssignCommentsFromGrid

    End Sub

    Public Function FloatAssignGridActiveCellIsCommentsCell() As Boolean Implements IFloatAssignVM.FloatAssignGridActiveCellIsCommentsCell

        Return False
    End Function

    Public Function GetFloatAssignGridTotalsRowIndex() As Integer Implements IFloatAssignVM.GetFloatAssignGridTotalsRowIndex

        Return _FloatAssignGrid.ActiveSheet.RowCount - 1
    End Function

    Public Function GetCommentsFromGrid() As String Implements IFloatAssignVM.GetCommentsFromGrid

        Return String.Empty
    End Function
End Class
