﻿Public Class SafeCheckReportUISafeMaintenanceInfoFactory
    Inherits RequirementSwitchFactory(Of ISafeCheckReportUI)

    Public Overrides Function ImplementationA() As ISafeCheckReportUI

        Return New SafeCheckReportUIDisplayInfo

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22073)

    End Function

    Public Overrides Function ImplementationB() As ISafeCheckReportUI

        Return New SafeCheckReportUIHideInfo

    End Function

End Class