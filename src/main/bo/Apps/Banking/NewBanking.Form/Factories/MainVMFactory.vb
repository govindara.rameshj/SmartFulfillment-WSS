﻿Public Class MainVMFactory
    Inherits RequirementSwitchFactory(Of IMainVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IMainVM

        Return New MainVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IMainVM

        Return New MainVMWithoutComments
    End Function
End Class
