﻿Public Class EndOfDayCheckVMFactory
    Inherits BaseFactory(Of IEndOfDayCheckVM)

    Public Overrides Function Implementation() As IEndOfDayCheckVM

        Return New EndOfDayCheckVM

    End Function

End Class