﻿Public Class SafeMaintenanceFormFactory
    Inherits RequirementSwitchFactory(Of ISafeMaintenance)

    Private _FirstUserID As Integer
    Private _SelectedPeriod As Period
    Private _CurrentPeriod As Period
    Private _strStoreID As String
    Private _StoreName As String

    Private _RequirementSwitchP022073 As Integer = -22073

    Public Overrides Function ImplementationA() As ISafeMaintenance

        Return New SafeMaintenanceWithBankingBagsAndComments(_FirstUserID, _CurrentPeriod, _SelectedPeriod, _strStoreID, _StoreName)
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchP022073)
    End Function

    Public Overrides Function ImplementationB() As ISafeMaintenance

        Return New SafeMaintenance(_FirstUserID, _CurrentPeriod, _SelectedPeriod, _strStoreID, _StoreName)
    End Function

    Public Sub Initialise(ByVal FirstUserID As Integer, ByVal CurrentPeriod As Period, ByVal SelectedPeriod As Period, ByVal StoreID As String, ByVal StoreName As String)

        _FirstUserID = FirstUserID
        _CurrentPeriod = CurrentPeriod
        _SelectedPeriod = SelectedPeriod
        _strStoreID = StoreID
        _StoreName = StoreName
    End Sub
End Class
