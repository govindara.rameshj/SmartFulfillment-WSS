﻿Public Class BankingBagCreationUIFactory
    Inherits RequirementSwitchFactory(Of IBankingBagCreationUI)

    Public Overrides Function ImplementationA() As IBankingBagCreationUI

        Return New BankingBagCreationToBankPopulated

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-597)

    End Function

    Public Overrides Function ImplementationB() As IBankingBagCreationUI

        Return New BankingBagCreationToBankNotPopulated

    End Function

End Class