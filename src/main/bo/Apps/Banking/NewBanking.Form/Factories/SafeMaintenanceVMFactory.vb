﻿Public Class SafeMaintenanceVMFactory
    Inherits BaseFactory(Of ISafeMaintenanceVM)

    Public Overrides Function Implementation() As ISafeMaintenanceVM

        Return New SafeMaintenanceVM
    End Function
End Class
