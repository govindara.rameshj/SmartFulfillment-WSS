﻿Public Class CashDropCashierFilterVMFactory
    Inherits RequirementSwitchFactory(Of ICashDropCashierFilterVM)

    Friend _requirementSwitchParameterId As Integer = -1031

    Public Overrides Function ImplementationA() As ICashDropCashierFilterVM

        Return New CashDropCashierFilterVMWithNoSaleFilter
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As ICashDropCashierFilterVM

        Return New CashDropCashierFilterVMWithoutNoSaleFilter
    End Function
End Class
