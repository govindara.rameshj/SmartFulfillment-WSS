﻿Public Class BankingCollectionFactory
    Inherits RequirementSwitchFactory(Of IBankingCollection)


    Public Overrides Function ImplementationA() As IBankingCollection
        Return New BankingCollectionRefresh
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22025)
    End Function

    Public Overrides Function ImplementationB() As IBankingCollection
        Return New BankingCollectionNotRefresh
    End Function
End Class
