﻿Public Class LaunchAuditRollEnquiryFactory
    Inherits RequirementSwitchFactory(Of IAuditRollEnquiry)

    Friend _requirementSwitchParameterId As Integer = -22013

    Public Overrides Function ImplementationA() As IAuditRollEnquiry

        Return New LaunchAuditRollEnquiry
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IAuditRollEnquiry

        Return Nothing
    End Function
End Class
