﻿Public Class FloatAssignSaveFactory
    Inherits BaseFactory(Of IFloatAssignSave)

    Public Overrides Function Implementation() As IFloatAssignSave

        Return New FloatAssignSave
    End Function
End Class
