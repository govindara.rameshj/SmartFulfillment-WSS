﻿Public Class CashierReportVMFactory
    Inherits RequirementSwitchFactory(Of ICashierReportVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As ICashierReportVM

        Return New CashierReportVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As ICashierReportVM

        Return New CashierReportVMWithoutComments
    End Function
End Class
