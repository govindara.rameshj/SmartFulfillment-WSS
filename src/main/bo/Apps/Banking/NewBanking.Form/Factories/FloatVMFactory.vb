﻿Public Class FloatVMFactory
    Inherits RequirementSwitchFactory(Of IFloatVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IFloatVM

        Return New FloatVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IFloatVM

        Return New FloatVMWithoutComments
    End Function
End Class
