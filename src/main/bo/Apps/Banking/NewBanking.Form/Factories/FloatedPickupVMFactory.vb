﻿Public Class FloatedPickupVMFactory
    Inherits RequirementSwitchFactory(Of IFloatedPickupVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IFloatedPickupVM

        Return New FloatedPickupVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IFloatedPickupVM

        Return New FloatedPickupVMWithoutComments
    End Function
End Class
