﻿Public Class EndOfDayCheckUIFactory
    Inherits RequirementSwitchFactory(Of IEndOfDayCheckUI)

    Public Overrides Function ImplementationA() As IEndOfDayCheckUI

        Return New EndOfDayCheckUI

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22012)

    End Function

    Public Overrides Function ImplementationB() As IEndOfDayCheckUI

        Return New EndOfDayCheckUIBankingReport

    End Function

End Class
