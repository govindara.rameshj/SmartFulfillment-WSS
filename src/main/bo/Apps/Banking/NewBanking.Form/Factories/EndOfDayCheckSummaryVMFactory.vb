﻿Public Class EndOfDayCheckSummaryVMFactory
    Inherits RequirementSwitchFactory(Of IEndOfDayCheckSummaryVM)

    Public Overrides Function ImplementationA() As IEndOfDayCheckSummaryVM

        Return New EndOfDayCheckSummaryVM

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22012)

    End Function

    Public Overrides Function ImplementationB() As IEndOfDayCheckSummaryVM

        Return New EndOfDayCheckSummaryVMUnPopulated

    End Function

End Class