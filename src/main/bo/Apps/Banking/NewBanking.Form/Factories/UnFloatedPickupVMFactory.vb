﻿Public Class UnFloatedPickupVMFactory
    Inherits RequirementSwitchFactory(Of IUnFloatedPickupVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IUnFloatedPickupVM

        Return New UnFloatedPickupVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IUnFloatedPickupVM

        Return New UnFloatedPickupVMWithoutComments
    End Function
End Class
