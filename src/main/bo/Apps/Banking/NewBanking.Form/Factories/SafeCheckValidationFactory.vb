﻿Public Class SafeCheckValidationFactory
    Inherits RequirementSwitchFactory(Of ISafeCheckValidation)

    Public Overrides Function ImplementationA() As ISafeCheckValidation
        Return New SafeCheckValidationRequired
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22024)
    End Function

    Public Overrides Function ImplementationB() As ISafeCheckValidation
        Return New SafeCheckValidationNotRequired
    End Function

End Class
