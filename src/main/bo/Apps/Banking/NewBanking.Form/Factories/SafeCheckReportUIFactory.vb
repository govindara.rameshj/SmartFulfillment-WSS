﻿Public Class SafeCheckReportUIFactory
    Inherits RequirementSwitchFactory(Of ISafeCheckReportUI)

    Public Overrides Function ImplementationA() As ISafeCheckReportUI

        Return New SafeCheckReportUIView

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22011)

    End Function

    Public Overrides Function ImplementationB() As ISafeCheckReportUI

        Return New SafeCheckReportUIPrint

    End Function

End Class