﻿Public Class BankingPickupCheckVMFactory
    Inherits RequirementSwitchFactory(Of IBankingPickupCheckVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IBankingPickupCheckVM

        Return New BankingPickupCheckVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IBankingPickupCheckVM

        Return New BankingPickupCheckVMWithoutComments
    End Function
End Class
