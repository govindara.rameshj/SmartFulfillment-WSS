﻿Public Class SafeCheckReportUISafeMaintenanceAuthoriserFactory
    Inherits RequirementSwitchFactory(Of ISafeCheckReportUI)

    Public Overrides Function ImplementationA() As ISafeCheckReportUI

        Return New SafeCheckReportUIDisplayAuthorisers

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22024)

    End Function

    Public Overrides Function ImplementationB() As ISafeCheckReportUI

        Return New SafeCheckReportUIHideAuthorisers

    End Function

End Class