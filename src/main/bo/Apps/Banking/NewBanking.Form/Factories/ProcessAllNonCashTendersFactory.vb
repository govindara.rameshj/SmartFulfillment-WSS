﻿Public Class ProcessAllNonCashTendersFactory
    Inherits RequirementSwitchFactory(Of IProcessAllNonCashTenders)

    Private Const _RequirementSwitchPO15 As Integer = -964

    Public Overrides Function ImplementationA() As IProcessAllNonCashTenders

        Return New ProcessAllNonCashTenders

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IProcessAllNonCashTenders

        Return New ProcessAllNonCashTendersCurrent

    End Function

End Class