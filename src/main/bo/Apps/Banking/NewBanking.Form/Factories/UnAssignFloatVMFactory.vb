﻿Public Class UnAssignFloatVMFactory
    Inherits RequirementSwitchFactory(Of IFloatVM)

    Public Overrides Function ImplementationA() As IFloatVM

        Return New FloatViewModelUnassignFloatWithManualCheck

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22016)

    End Function

    Public Overrides Function ImplementationB() As IFloatVM

        Return New FloatViewModelUnassignFloatWithoutManualCheck

    End Function

End Class