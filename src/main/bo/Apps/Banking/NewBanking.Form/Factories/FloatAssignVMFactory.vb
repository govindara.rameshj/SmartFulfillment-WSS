﻿Public Class FloatAssignVMFactory
    Inherits RequirementSwitchFactory(Of IFloatAssignVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IFloatAssignVM

        Return New FloatAssignVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IFloatAssignVM

        Return New FloatAssignVMWithoutComments
    End Function
End Class
