﻿Public Class FloatAssignAuthoriseFactory

    Private parameterID As Integer = -22023

    Public Function FactoryGet() As IFloatAssignAuthorise
        If IsEnabled() = True Then
            'using new implementation
            Return New FloatAssignAuthorise
        Else
            'using existing implementation
            Return New FloatAssignAuthoriseNotImplemented
        End If
    End Function

    Public Overridable Function IsEnabled() As Boolean
        Return Cts.Oasys.Core.System.Parameter.GetBoolean(parameterID)
    End Function
End Class

