﻿Public Class PickupEntryVMFactory
    Inherits RequirementSwitchFactory(Of IPickupEntryVM)

    Friend _requirementSwitchParameterId As Integer = -22017

    Public Overrides Function ImplementationA() As IPickupEntryVM

        Return New PickupEntryVMWithComments
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IPickupEntryVM

        Return New PickupEntryVMWithoutComments
    End Function
End Class
