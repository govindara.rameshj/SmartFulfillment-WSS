﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.lblStatus = New System.Windows.Forms.Label
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblFilesToProcess = New System.Windows.Forms.Label
        Me.lblInfo = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblSucessful = New System.Windows.Forms.Label
        Me.lblFailed = New System.Windows.Forms.Label
        Me.txtReport = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.White
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus.Location = New System.Drawing.Point(12, 20)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(433, 23)
        Me.lblStatus.TabIndex = 0
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 326)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(457, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblFilesToProcess
        '
        Me.lblFilesToProcess.AutoSize = True
        Me.lblFilesToProcess.Location = New System.Drawing.Point(102, 57)
        Me.lblFilesToProcess.Name = "lblFilesToProcess"
        Me.lblFilesToProcess.Size = New System.Drawing.Size(13, 13)
        Me.lblFilesToProcess.TabIndex = 2
        Me.lblFilesToProcess.Text = "0"
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Location = New System.Drawing.Point(12, 57)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(84, 13)
        Me.lblInfo.TabIndex = 3
        Me.lblInfo.Text = "Files to Process:"
        Me.lblInfo.UseMnemonic = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(156, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Files Successful:"
        Me.Label1.UseMnemonic = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(326, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Files Failed:"
        Me.Label2.UseMnemonic = False
        '
        'lblSucessful
        '
        Me.lblSucessful.AutoSize = True
        Me.lblSucessful.Location = New System.Drawing.Point(246, 57)
        Me.lblSucessful.Name = "lblSucessful"
        Me.lblSucessful.Size = New System.Drawing.Size(13, 13)
        Me.lblSucessful.TabIndex = 6
        Me.lblSucessful.Text = "0"
        '
        'lblFailed
        '
        Me.lblFailed.AutoSize = True
        Me.lblFailed.Location = New System.Drawing.Point(416, 57)
        Me.lblFailed.Name = "lblFailed"
        Me.lblFailed.Size = New System.Drawing.Size(13, 13)
        Me.lblFailed.TabIndex = 7
        Me.lblFailed.Text = "0"
        '
        'txtReport
        '
        Me.txtReport.Location = New System.Drawing.Point(12, 88)
        Me.txtReport.Multiline = True
        Me.txtReport.Name = "txtReport"
        Me.txtReport.ReadOnly = True
        Me.txtReport.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtReport.Size = New System.Drawing.Size(433, 220)
        Me.txtReport.TabIndex = 8
        '
        'frmImportVisionSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 348)
        Me.Controls.Add(Me.txtReport)
        Me.Controls.Add(Me.lblFailed)
        Me.Controls.Add(Me.lblSucessful)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.lblFilesToProcess)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblStatus)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportVisionSales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Performing Vision Sales Import"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblFilesToProcess As System.Windows.Forms.Label
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblSucessful As System.Windows.Forms.Label
    Friend WithEvents lblFailed As System.Windows.Forms.Label
    Friend WithEvents txtReport As System.Windows.Forms.TextBox

End Class
