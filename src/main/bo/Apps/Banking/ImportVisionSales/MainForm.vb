﻿Public Class MainForm

    Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Trace.WriteLine("Started", Me.Name)
        AddHandler Application.ThreadException, New ThreadExceptionEventHandler(AddressOf ApplicationThreadException)

        ProcessFiles()
        Application.Exit()

    End Sub

    Private Sub MainForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Trace.WriteLine("Closing", Me.Name)

    End Sub

    Private Sub ApplicationThreadException(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)

        Trace.WriteLine(e.Exception.ToString, Me.Name)
        EventLog.WriteEntry("Application", e.Exception.ToString, EventLogEntryType.Error)
        Application.Exit()

    End Sub

    Private Sub ProcessFiles()

        Dim pathCommunication As String
        Dim pathVisdone As String
        Dim pathVisin As String
        Dim pathViserr As String

        Dim KitchenAndBathroomSaleTag As String

        Dim XmlFiles As String()
        Dim XmlFileContent As String

        Dim KitchenAndBathroomSale As KitchenAndBathroom
        Dim NonKitchenAndBathroomSale As NonKitchenAndBathroom

        pathCommunication = Cts.Oasys.Data.GetCommunicationPath()
        pathVisdone = pathCommunication & "\" & "VISDONE"
        pathVisin = pathCommunication & "\" & "VISIN"
        pathViserr = pathCommunication & "\" & "VISERR"

        KitchenAndBathroomSaleTag = "<CTSVISIONDEPOSIT>"

        CheckFolderExistsOrCreate(pathVisin)
        CheckFolderExistsOrCreate(pathViserr)
        CheckFolderExistsOrCreate(pathVisdone)

        XmlFiles = GetXmlFileNames(pathVisin)

        lblFilesToProcess.Text = XmlFiles.Count().ToString

        If XmlFiles.Count > 0 Then

            For Each XmlFile As String In XmlFiles

                Try

                    lblStatus.Text = "Importing " & XmlFile

                    If IsFileZeroLength(XmlFile) Then

                        ProcessFileFailed(XmlFile, "Zero Length file")
                        Continue For

                    End If

                    XmlFileContent = IO.File.ReadAllText(XmlFile)

                    If (XmlFileContent.Contains(KitchenAndBathroomSaleTag)) Then

                        KitchenAndBathroomSale = KitchenAndBathroom.Deserialise(XmlFileContent)

                        If KitchenAndBathroomSale.Persist = True Then

                            lblStatus.Text = "Update Banking Data for " & XmlFile

                            UpdateBankingTables("T", KitchenAndBathroomSale.Total.TranDate, _
                                                     KitchenAndBathroomSale.Total.TillId, _
                                                     KitchenAndBathroomSale.Total.TranNumber)

                            ProcessFileSuccessful(XmlFile)

                        Else

                            ProcessFileFailed(XmlFile, "Vision sales not saved")

                        End If

                    Else

                        NonKitchenAndBathroomSale = NonKitchenAndBathroom.Deserialise(XmlFileContent)

                        If NonKitchenAndBathroomSale.PersistAndUpdateFlashTotals = True Then

                            lblStatus.Text = "Update Banking Data for " & XmlFile

                            UpdateBankingTables("V", NonKitchenAndBathroomSale.Total.TranDate, _
                                                     NonKitchenAndBathroomSale.Total.TillId.ToString("00"), _
                                                     NonKitchenAndBathroomSale.Total.TranNumber.ToString("0000"))

                            ProcessFileSuccessful(XmlFile)

                        Else

                            ProcessFileFailed(XmlFile, "Vision sales not saved")

                        End If

                    End If

                Catch ex As Exception

                    ProcessFileFailed(XmlFile, ex.Message)

                End Try

            Next

        End If

    End Sub

    Private Sub UpdateBankingTables(ByVal Type As String, ByVal TransactionDate As Date, ByVal TillID As String, ByVal TransactionNumber As String)

        Dim ExecutablePathAndName As String
        Dim ExecutableArguments As String

        ExecutablePathAndName = Chr(34) & Application.StartupPath & "\CashierBalancingUpdate.exe" & Chr(34)
        ExecutableArguments = Type & TransactionDate.ToString("yyyyMMdd") & TillID & TransactionNumber

        RunShell(ExecutablePathAndName, ExecutableArguments, Application.StartupPath)

    End Sub

    Private Function GetXmlFileNames(ByVal folderPath As String) As String()

        Trace.WriteLine("Searching for XML file names", Me.Name)

        Dim fileNames As String() = IO.Directory.GetFiles(folderPath, "CTS*.XML")
        Return fileNames

    End Function

    Private Function IsFileZeroLength(ByVal fileName As String) As Boolean

        Dim fi As New FileInfo(fileName)

        Return (fi.Length = 0)

    End Function

    Private Sub ProcessFileFailed(ByVal fileName As String, ByVal errorDescription As String)

        lblFailed.Text = (CInt(lblFailed.Text) + 1).ToString
        lblFailed.Refresh()
        txtReport.Text &= errorDescription & Environment.NewLine

        Dim destFileName As String = fileName.Replace("VISIN", "VISERR")

        MoveFile(fileName, destFileName)

    End Sub

    Private Sub ProcessFileSuccessful(ByVal fileName As String)

        lblSucessful.Text = (CInt(lblSucessful.Text) + 1).ToString
        lblSucessful.Refresh()
        txtReport.Text &= fileName & " processed successfully" & Environment.NewLine

        Dim destFileName As String = fileName.Replace("VISIN", "VISDONE")

        MoveFile(fileName, destFileName)

    End Sub

    Private Sub MoveFile(ByVal sourcefileName As String, ByVal destfileName As String)

        Trace.WriteLine("Moving " & sourcefileName & " to " & destfileName, Me.Name)
        IO.File.Move(sourcefileName, destfileName)

    End Sub

    Private Sub CheckFolderExistsOrCreate(ByVal path As String)

        Trace.WriteLine("Looking for " & path, Me.Name)

        If IO.Directory.Exists(path) = False Then
            IO.Directory.CreateDirectory(path)
            Trace.WriteLine("Created " & path, Me.Name)
        End If

    End Sub

    Private Sub RunShell(ByVal command As String, ByVal args As String, ByVal workingDirectory As String)

        Try
            Cursor = Cursors.WaitCursor

            Trace.WriteLine("process - Working directory " & workingDirectory, Me.Name)
            Trace.WriteLine("process - Application run directory " & command, Me.Name)
            Trace.WriteLine("process - Arguments " & args, Me.Name)

            Dim newProc As Diagnostics.Process = New System.Diagnostics.Process()

            newProc.StartInfo.WorkingDirectory = workingDirectory
            newProc.StartInfo.FileName = command
            newProc.StartInfo.Arguments = args
            newProc.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            newProc.StartInfo.UseShellExecute = False
            newProc.EnableRaisingEvents = True
            newProc.Start()
            newProc.WaitForExit()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

End Class