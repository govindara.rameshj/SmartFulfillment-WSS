﻿''' <summary>
''' Common banking procedures and functions
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Module Library

    ''' <summary>
    ''' Retrieve the store ID and Name
    ''' </summary>
    ''' <param name="strStoreID"></param>
    ''' <param name="strStoreName"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub GetStoreID(ByRef strStoreID As String, ByRef strStoreName As String)
        Dim paramStoreID As SqlParameter
        Dim paramStoreName As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingStore")
                paramStoreID = com.AddOutputParameter("@StoreID", SqlDbType.Char, 3)
                paramStoreName = com.AddOutputParameter("@StoreName", SqlDbType.Char, 30)

                com.ExecuteNonQuery()
            End Using
        End Using

        strStoreID = paramStoreID.Value.ToString
        strStoreName = paramStoreName.Value.ToString
    End Sub

    ''' <summary>
    ''' Does a Safe for this period exist
    ''' </summary>
    ''' <param name="intPeriodID"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function DoesSafeExist(ByVal intPeriodID As Integer) As Boolean
        Dim paramCount As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSafeExist")
                com.AddParameter("@PeriodID", intPeriodID)
                paramCount = com.AddOutputParameter("@Count", SqlDbType.Int)
                com.ExecuteNonQuery()
            End Using
        End Using

        Select Case CType(paramCount.Value, Integer)
            Case 1
                DoesSafeExist = True
            Case 0
                DoesSafeExist = False
        End Select
    End Function

    ''' <summary>
    ''' Checks and creates a Safe for non-trading day
    ''' </summary>
    ''' <param name="periodDate"></param>
    Public Function CreateSafeForNonTradingDay(ByVal periodDate As Date) As Boolean

        Using con As New Connection
            Using com As Command = con.NewCommand("CreateSafeForNonTradingDay")
                com.AddParameter("@periodDate", periodDate)
                Return com.ExecuteNonQuery() > 0
            End Using
        End Using

    End Function

    ''' <summary>
    ''' Retrieve symbol for active currency
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function CurrencySymbol() As String
        Dim paramCurrencySymbol As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingCurrencySymbol")
                paramCurrencySymbol = com.AddOutputParameter("@CurrencySymbol", SqlDbType.Char, 3)
                com.ExecuteNonQuery()
            End Using
        End Using

        CurrencySymbol = paramCurrencySymbol.Value.ToString.Trim
    End Function

    ''' <summary>
    ''' Retrieve active currency ID
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function ActiveCurrencyID() As String
        Dim paramCurrencySymbol As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingActiveCurrencyID")
                paramCurrencySymbol = com.AddOutputParameter("@CurrencyID", SqlDbType.Char, 3)
                com.ExecuteNonQuery()
            End Using
        End Using

        ActiveCurrencyID = paramCurrencySymbol.Value.ToString.Trim
    End Function

    ''' <summary>
    ''' Check user credentials - normal check
    ''' </summary>
    ''' <param name="intUserID"></param>
    ''' <param name="strPassword"></param>
    ''' <param name="blnManager"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub SecondVerificationCheck(ByVal intUserID As Integer, ByRef strUserName As String, ByRef strPassword As String, ByRef blnManager As Boolean)
        Dim paramUserName As SqlParameter
        Dim paramPassword As SqlParameter
        Dim paramManager As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSecondUserCheck")
                com.AddParameter("@UserID", intUserID, SqlDbType.Int)

                paramUserName = com.AddOutputParameter("@UserName", SqlDbType.VarChar, 50)
                paramPassword = com.AddOutputParameter("@Password", SqlDbType.Char, 5)
                paramManager = com.AddOutputParameter("@Manager", SqlDbType.Bit)

                com.ExecuteNonQuery()
            End Using
        End Using

        strUserName = paramUserName.Value.ToString.Trim
        strPassword = paramPassword.Value.ToString.Trim
        If paramManager.Value IsNot System.DBNull.Value Then
            blnManager = CType(paramManager.Value, Boolean)
        Else
            blnManager = False
        End If
    End Sub

    ''' <summary>
    ''' Check user credentials - authorisation check
    ''' </summary>
    ''' <param name="intUserID"></param>
    ''' <param name="strUserName"></param>
    ''' <param name="strSupervisorPassword"></param>
    ''' <param name="blnManager"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub SecondAuthorisationCheck(ByVal intUserID As Integer, ByRef strUserName As String, ByRef strSupervisorPassword As String, ByRef blnManager As Boolean)
        Dim paramUserName As SqlParameter
        Dim paramSupervisorPassword As SqlParameter
        Dim paramManager As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSecondSupervisorCheck")
                com.AddParameter("@UserID", intUserID, SqlDbType.Int)

                paramUserName = com.AddOutputParameter("@UserName", SqlDbType.VarChar, 50)
                paramSupervisorPassword = com.AddOutputParameter("@Password", SqlDbType.Char, 5)
                paramManager = com.AddOutputParameter("@Manager", SqlDbType.Bit)

                com.ExecuteNonQuery()
            End Using
        End Using

        strUserName = paramUserName.Value.ToString.Trim
        strSupervisorPassword = paramSupervisorPassword.Value.ToString.Trim

        If paramManager.Value IsNot System.DBNull.Value Then
            blnManager = CType(paramManager.Value, Boolean)
        Else
            blnManager = False
        End If
    End Sub

    ''' <summary>
    ''' Accounting model used by banking
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function AccountabilityModel() As String
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("SystemOptionGet")
                dt = com.ExecuteDataTable
                Return CStr(dt.Rows(0).Item(3))          'Field - AccountabilityType
            End Using
        End Using
    End Function

    ''' <summary>
    ''' Decide if seal is unique
    ''' </summary>
    ''' <param name="strSealNo"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function UniqueSeal(ByVal strSealNo As String) As Boolean
        Dim paramUniqueSeal As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingUniqueSealNumber")
                com.AddParameter("@SealNumber", strSealNo)
                paramUniqueSeal = com.AddOutputParameter("@UniqueSeal", SqlDbType.Bit)
                com.ExecuteNonQuery()
            End Using
        End Using
        UniqueSeal = CType(paramUniqueSeal.Value, Boolean)
    End Function

    ''' <summary>
    ''' Decide if slip no is unique
    ''' </summary>
    ''' <param name="strSlipNo"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function UniqueSlipNo(ByVal strSlipNo As String) As Boolean
        Dim paramUniqueSeal As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingUniqueSlipNumber")
                com.AddParameter("@SlipNumber", strSlipNo)
                paramUniqueSeal = com.AddOutputParameter("@UniqueSlip", SqlDbType.Bit)
                com.ExecuteNonQuery()
            End Using
        End Using
        UniqueSlipNo = CType(paramUniqueSeal.Value, Boolean)
    End Function

    ''' <summary>
    ''' Re-assign existing float to new cashier
    ''' </summary>
    ''' <param name="intFloatBagID"></param>
    ''' <param name="intCashierID"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function FloatReAssigned(ByVal intFloatBagID As Integer, ByVal intCashierID As Integer) As Boolean
        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatReAssigned")
                com.AddParameter("@FloatBagID", intFloatBagID)
                com.AddParameter("@AccountabilityID", intCashierID)

                com.ExecuteNonQuery()
            End Using
        End Using
    End Function

    ''' <summary>
    ''' Prepare existing float to be un-assigned using the existing banking float "re-seal" feature
    ''' </summary>
    ''' <param name="intFloatBagID"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatPrepareUnAssigned(ByVal intFloatBagID As Integer)

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatPrepareUnAssign")
                com.AddParameter("@FloatBagID", intFloatBagID)
                com.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' Prepare existing float to be un-assigned using the existing banking float "re-seal" feature
    ''' </summary>
    ''' <param name="FloatBagID"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatPrepareUnAssigned(ByRef con As clsOasys3DB, ByVal FloatBagID As Integer)

        If con IsNot Nothing Then
            Dim FloatPrepareUnAssignSQL As String = "exec NewBankingFloatPrepareUnAssign @FloatBagID = " & FloatBagID.ToString.Trim

            con.NewBankingNonQueryExecuteSql(FloatPrepareUnAssignSQL)
        End If
    End Sub

    ''' <summary>
    ''' Manual check of float
    ''' </summary>
    ''' <param name="intFloatBagID"></param>
    ''' <param name="intUserID1"></param>
    ''' <param name="intUserID2"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatChecked(ByVal intFloatBagID As Integer, ByVal intUserID1 As Integer, ByVal intUserID2 As Integer)

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatChecked")
                com.AddParameter("@FloatBagID", intFloatBagID)
                com.AddParameter("@UserID1", intUserID1)
                com.AddParameter("@UserID2", intUserID2)

                com.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' Manual check of float
    ''' </summary>
    ''' <param name="con"></param>
    ''' <param name="FloatBagID"></param>
    ''' <param name="UserID1"></param>
    ''' <param name="UserID2"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatChecked(ByRef con As clsOasys3DB, ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer)

        If con IsNot Nothing Then
            Dim FloatCheckedSQL As String = _
                "exec NewBankingFloatChecked @FloatBagID = " & FloatBagID.ToString.Trim & _
                ", @UserID1 = " & UserID1.ToString.Trim & _
                ", @UserID2 = " & UserID2.ToString.Trim

            con.NewBankingNonQueryExecuteSql(FloatCheckedSQL)
        End If
    End Sub

    ''' <summary>
    ''' Manual check of float
    ''' </summary>
    ''' <param name="FloatBagID"></param>
    ''' <param name="UserID1"></param>
    ''' <param name="UserID2"></param>
    ''' <param name="Comments"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatChecked(ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer, ByVal Comments As String)

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatChecked")
                With com
                    .AddParameter("@FloatBagID", FloatBagID)
                    .AddParameter("@UserID1", UserID1)
                    .AddParameter("@UserID2", UserID2)
                    .AddParameter("@Comments", Comments)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' Manual check of float
    ''' </summary>
    ''' <param name="con"></param>
    ''' <param name="FloatBagID"></param>
    ''' <param name="UserID1"></param>
    ''' <param name="UserID2"></param>
    ''' <param name="Comments"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Sub FloatChecked(ByRef con As clsOasys3DB, ByVal FloatBagID As Integer, ByVal UserID1 As Integer, ByVal UserID2 As Integer, ByVal Comments As String)

        If con IsNot Nothing Then
            Dim FloatCheckedSQL As String = _
                "exec NewBankingFloatChecked @FloatBagID = " & FloatBagID.ToString.Trim & _
                ", @UserID1 = " & UserID1.ToString.Trim & _
                ", @UserID2 = " & UserID2.ToString.Trim & _
                ", @Comments = '" & Comments.Trim & "'"

            con.NewBankingNonQueryExecuteSql(FloatCheckedSQL)
        End If
    End Sub

    ''' <summary>
    ''' Acceptable banking variance
    ''' </summary>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function BankingVariance() As System.Nullable(Of Decimal)
        Dim paramValue As SqlParameter

        Using con As New Connection
            Using com As Command = con.NewCommand("ParameterGetDecimalValueByID")
                com.AddParameter("@ID", 2201)
                paramValue = com.AddOutputParameter("@Value", SqlDbType.Decimal)

                com.ExecuteNonQuery()
            End Using
        End Using

        BankingVariance = CType(paramValue.Value, System.Nullable(Of Decimal))
    End Function

    ''' <summary>
    ''' Decide whether you can bank this denomination
    ''' </summary>
    ''' <param name="decDenominationID"></param>
    ''' <returns></returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Public Function AbleToBankCashTenderDenomination(ByVal decDenominationID As Decimal) As Boolean
        Dim paramValue As SqlParameter
        Dim decLocal As System.Nullable(Of Decimal)

        AbleToBankCashTenderDenomination = False
        Using con As New Connection
            Using com As Command = con.NewCommand("ParameterGetDecimalValueByID")
                com.AddParameter("@ID", 2202)
                paramValue = com.AddOutputParameter("@Value", SqlDbType.Decimal)

                com.ExecuteNonQuery()
            End Using
        End Using

        decLocal = CType(paramValue.Value, System.Nullable(Of Decimal))

        If decLocal.HasValue = False Then Exit Function
        If decDenominationID > decLocal Then AbleToBankCashTenderDenomination = True
    End Function

End Module
