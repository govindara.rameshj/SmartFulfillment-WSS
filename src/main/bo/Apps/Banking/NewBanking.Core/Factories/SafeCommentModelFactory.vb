﻿Public Class SafeCommentModelFactory
    Inherits BaseFactory(Of ISafeCommentModel)

    Public Overrides Function Implementation() As ISafeCommentModel

        Return New SafeCommentModel

    End Function

End Class