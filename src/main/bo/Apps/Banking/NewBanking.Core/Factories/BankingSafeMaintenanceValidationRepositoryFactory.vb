﻿Public Class BankingSafeMaintenanceValidationRepositoryFactory

    Private Shared m_FactoryMember As IValidationRepository

    Public Shared Function FactoryGet() As IValidationRepository

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New BankingSafeMaintenanceValidationRepository            'live implementation
        Else
            Return m_FactoryMember                                           'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IValidationRepository)

        m_FactoryMember = obj

    End Sub

End Class