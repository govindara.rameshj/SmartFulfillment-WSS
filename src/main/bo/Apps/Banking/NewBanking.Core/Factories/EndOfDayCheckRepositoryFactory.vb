﻿Public Class EndOfDayCheckRepositoryFactory
    Inherits BaseFactory(Of IEndOfDayCheckRepository)

    Public Overrides Function Implementation() As IEndOfDayCheckRepository

        Return New EndOfDayCheckRepository

    End Function

End Class