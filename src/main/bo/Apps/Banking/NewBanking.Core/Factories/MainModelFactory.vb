﻿Public Class MainModelFactory
    Inherits BaseFactory(Of IMainModel)

    Public Overrides Function Implementation() As IMainModel

        Return New MainModel
    End Function
End Class
