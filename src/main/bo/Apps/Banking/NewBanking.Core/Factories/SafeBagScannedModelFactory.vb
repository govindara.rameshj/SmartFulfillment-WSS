﻿Public Class SafeBagScannedModelFactory
    Inherits BaseFactory(Of ISafeBagScannedModel)

    Public Overrides Function Implementation() As ISafeBagScannedModel

        Return New SafeBagScannedModel

    End Function

End Class