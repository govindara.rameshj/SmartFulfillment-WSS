﻿Public Class SafeMaintenanceModelFactory
    Inherits BaseFactory(Of ISafeMaintenanceModel)

    Public Overrides Function Implementation() As ISafeMaintenanceModel

        Return New SafeMaintenanceModel
    End Function
End Class
