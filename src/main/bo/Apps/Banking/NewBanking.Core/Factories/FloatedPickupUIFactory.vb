﻿Public Class FloatedPickupUIFactory
    Inherits RequirementSwitchFactory(Of IFloatedPickupUI)

    Private Const _RequirementSwitchCR0096 As Integer = -96

    Public Overrides Function ImplementationA() As IFloatedPickupUI

        Return New FloatedPickupUIReassignButtonInActive

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchCR0096)


    End Function

    Public Overrides Function ImplementationB() As IFloatedPickupUI

        Return New FloatedPickupUIReassignButtonActive

    End Function

End Class