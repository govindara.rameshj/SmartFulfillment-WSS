﻿Public Class SafeMaintenanceEngineFactory
    Inherits BaseFactory(Of ISafeMaintenanceEngine)

    Public Overrides Function Implementation() As ISafeMaintenanceEngine

        Return New SafeMaintenanceEngineModel
    End Function
End Class
