﻿Namespace TpWickes

    Public Class NoSaleBankingFactory
        Inherits BaseFactory(Of INoSaleBanking)

        Public Overrides Function Implementation() As INoSaleBanking

            Return New NoSaleBanking()

        End Function

    End Class

End Namespace