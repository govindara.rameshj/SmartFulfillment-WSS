﻿Public Class SafeModelFactory
    Inherits BaseFactory(Of ISafeModel)

    Public Overrides Function Implementation() As ISafeModel

        Return New SafeModel

    End Function

End Class