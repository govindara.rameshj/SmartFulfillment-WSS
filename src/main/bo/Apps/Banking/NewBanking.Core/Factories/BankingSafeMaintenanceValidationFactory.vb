﻿Public Class BankingSafeMaintenanceValidationFactory

    Private Shared m_FactoryMember As IValidation

    Public Shared Function FactoryGet() As IValidation

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New BankingSafeMaintenanceValidation        'live implementation
        Else
            Return m_FactoryMember                             'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IValidation)

        m_FactoryMember = obj

    End Sub

End Class