﻿Public Class BagScanningEngineFactory
    Inherits BaseFactory(Of IBagScanningEngine)

    Public Overrides Function Implementation() As IBagScanningEngine

        Return New BagScanningEngineModel

    End Function

End Class