﻿Namespace TpWickes

    Public Class NoSaleBankingRepositoryFactory

        Private Shared m_FactoryMember As INoSaleBankingRepository

        Public Shared Function FactoryGet() As INoSaleBankingRepository

            If m_FactoryMember Is Nothing Then

                Return New NoSaleBankingRepository            'live implementation

            Else

                Return m_FactoryMember                        'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As INoSaleBankingRepository)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace