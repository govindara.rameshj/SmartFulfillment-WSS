﻿Public Class CompletePickupAvailabilityImplementationFactory
    Inherits RequirementSwitchFactory(Of ICompletePickupAvailability)

    Private Const _RequirementSwitchCR0087 As Integer = -87

    Public Overrides Function ImplementationA() As ICompletePickupAvailability

        Return New CompletePickupAvailabilityAlsoCheckIsLoggedOn
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchCR0087)
    End Function

    Public Overrides Function ImplementationB() As ICompletePickupAvailability

        Return New CompletePickupAvailabilityLive
    End Function
End Class
