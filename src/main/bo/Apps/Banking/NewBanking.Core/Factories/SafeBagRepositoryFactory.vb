﻿Public Class SafeBagRepositoryFactory
    Inherits BaseFactory(Of ISafeBagRepository)

    Public Overrides Function Implementation() As ISafeBagRepository

        Return New SafeBagRepository
    End Function
End Class
