﻿Public Class SafeMaintenanceRepositoryFactory
    Inherits BaseFactory(Of ISafeMaintenanceRepository)

    Public Overrides Function Implementation() As ISafeMaintenanceRepository

        Return New SafeMaintenanceRepository
    End Function
End Class
