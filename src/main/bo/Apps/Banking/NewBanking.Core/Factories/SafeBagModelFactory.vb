﻿Public Class SafeBagModelFactory
    Inherits BaseFactory(Of ISafeBagModel)

    Public Overrides Function Implementation() As ISafeBagModel

        Return New SafeBagModel

    End Function

End Class

Public Class SafeBagCollectionModelFactory
    Inherits BaseFactory(Of ISafeBagCollectionModel)

    Public Overrides Function Implementation() As ISafeBagCollectionModel

        Return New SafeBagCollectionModel

    End Function

End Class