﻿Public Class MainRepositoryFactory
    Inherits BaseFactory(Of IMainRepository)

    Public Overrides Function Implementation() As IMainRepository

        Return New MainRepository
    End Function
End Class
