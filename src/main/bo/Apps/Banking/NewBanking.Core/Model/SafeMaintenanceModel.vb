﻿Public Class SafeMaintenanceModel
    Inherits Base
    Implements ISafeMaintenanceModel

    Private _PeriodID As Integer
    Private _BankingBags As List(Of String) = New List(Of String)

    Private _SafeChecked As Boolean
    Private _Comments As String = String.Empty
    Private _GiftTokenSeal As String = String.Empty
    Private _MiscellaneousProperty As String = String.Empty

#Region "ISafeMaintenanceModel Implementation"

#Region "Properties"

    <ColumnMapping("SafeChecked")> Public Property SafeChecked() As Boolean Implements ISafeMaintenanceModel.SafeChecked
        Get
            Return _SafeChecked
        End Get
        Set(ByVal value As Boolean)
            _SafeChecked = value
        End Set
    End Property

    <ColumnMapping("PeriodID")> _
    Public Property PeriodID() As Integer Implements ISafeMaintenanceModel.PeriodID
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As Integer)
            _PeriodID = value
        End Set
    End Property

    <ColumnMapping("Comments")> _
    Public Property Comments() As String Implements ISafeMaintenanceModel.Comments
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

    <ColumnMapping("GiftTokenSeal")> _
    Public Property GiftTokenSeal() As String Implements ISafeMaintenanceModel.GiftTokenSeal
        Get
            Return _GiftTokenSeal
        End Get
        Set(ByVal value As String)
            _GiftTokenSeal = value
        End Set
    End Property

    <ColumnMapping("MiscellaneousProperty")> _
    Public Property MiscellaneousProperty() As String Implements ISafeMaintenanceModel.MiscellaneousProperty
        Get
            Return _MiscellaneousProperty
        End Get
        Set(ByVal value As String)
            _MiscellaneousProperty = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub LoadSafeMaintenance(ByRef Con As Cts.Oasys.Data.Connection, ByVal PeriodID As Integer) Implements ISafeMaintenanceModel.LoadSafeMaintenance

        Dim SafeMaintenanceRep As ISafeMaintenanceRepository
        Dim SafeMaintenance As DataTable = Nothing

        SafeMaintenanceRep = (New SafeMaintenanceRepositoryFactory).GetImplementation
        SafeMaintenance = SafeMaintenanceRep.Load(Con, PeriodID)
        If SafeMaintenance IsNot Nothing AndAlso SafeMaintenance.Rows.Count > 0 Then
            Me.Load(SafeMaintenance.Rows(0))
            'LoadBankingBags(Con, PeriodID)
        End If

    End Sub

    Public Sub PersistSafeMaintenance(ByRef Con As Cts.Oasys.Data.Connection) Implements ISafeMaintenanceModel.PersistSafeMaintenance
        Dim SafeMaintenanceRep As ISafeMaintenanceRepository

        SafeMaintenanceRep = (New SafeMaintenanceRepositoryFactory).GetImplementation
        SafeMaintenanceRep.Persist(Con, Me)
    End Sub

    Public Sub PersistSafeMaintenance(ByRef OdbcConnection As clsOasys3DB) Implements ISafeMaintenanceModel.PersistSafeMaintenance

        Dim Repository As ISafeMaintenanceRepository

        Repository = (New SafeMaintenanceRepositoryFactory).GetImplementation
        Repository.Persist(OdbcConnection, Me)

    End Sub

    'Public Function GetBankingBags() As List(Of String) Implements ISafeMaintenanceModel.GetBankingBags

    '    Return _BankingBags

    'End Function

#End Region

#End Region

#Region "Internals"

    'Friend Sub LoadBankingBags(ByRef Con As Cts.Oasys.Data.Connection, ByVal PeriodID As Integer)
    '    Dim SafeMaintenanceRep As ISafeMaintenanceRepository
    '    Dim BankingBags As DataTable

    '    SafeMaintenanceRep = (New SafeMaintenanceRepositoryFactory).GetImplementation
    '    BankingBags = SafeMaintenanceRep.LoadBankingBags(Con, PeriodID)
    '    If BankingBags IsNot Nothing Then
    '        _BankingBags = New List(Of String)
    '        For Each BankingBag As DataRow In BankingBags.Rows
    '            _BankingBags.Add(SafeMaintenanceRep.GetBankingBagSealNumber(BankingBag))
    '        Next
    '    End If
    'End Sub

#End Region

End Class