﻿Public Class EndOfDayCheckSummaryModelUnPopulated
    Inherits Base

#Region "Table Fields And Properties"

    Private _ManagerID As String = ""        'no data access
    Private _WitnessID As String = ""        'no data access
    Private _TimelockSet As String = ""      'no data access
    Private _Duration As String = ""         'no data access
    Private _BagCount As String = ""         'no data access
    Private _SystemCount As String = ""      'no data access
    Private _ChequeBagNumber As String = ""  'no data access
    Private _CashBagNumber As String = ""    'no data access
    Private _Comments As String = ""         'no data access

    <ColumnMapping("ManagerID")> Public Property ManagerID() As String
        Get
            Return _ManagerID
        End Get
        Set(ByVal value As String)
            _ManagerID = value
        End Set
    End Property

    <ColumnMapping("WitnessID")> Public Property WitnessID() As String
        Get
            Return _WitnessID
        End Get
        Set(ByVal value As String)
            _WitnessID = value
        End Set
    End Property

    <ColumnMapping("TimelockSet")> Public Property TimelockSet() As String
        Get
            Return _TimelockSet
        End Get
        Set(ByVal value As String)
            _TimelockSet = value
        End Set
    End Property

    <ColumnMapping("Duration")> Public Property Duration() As String
        Get
            Return _Duration
        End Get
        Set(ByVal value As String)
            _Duration = value
        End Set
    End Property

    <ColumnMapping("BagCount")> Public Property BagCount() As String
        Get
            Return _BagCount
        End Get
        Set(ByVal value As String)
            _BagCount = value
        End Set
    End Property

    <ColumnMapping("SystemCount")> Public Property SystemCount() As String
        Get
            Return _SystemCount
        End Get
        Set(ByVal value As String)
            _SystemCount = value
        End Set
    End Property

    <ColumnMapping("ChequeBagNumber")> Public Property ChequeBagNumber() As String
        Get
            Return _ChequeBagNumber
        End Get
        Set(ByVal value As String)
            _ChequeBagNumber = value
        End Set
    End Property

    <ColumnMapping("CashBagNumber")> Public Property CashBagNumber() As String
        Get
            Return _CashBagNumber
        End Get
        Set(ByVal value As String)
            _CashBagNumber = value
        End Set
    End Property

    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

#End Region

End Class