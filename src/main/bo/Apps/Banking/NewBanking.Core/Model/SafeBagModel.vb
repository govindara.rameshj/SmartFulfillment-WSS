﻿Public Class SafeBagModel
    Inherits Base
    Implements ISafeBagModel

#Region "Interface: Properties"

    Private _ID As Integer
    Private _SealNumber As String
    Private _BagType As String
    Private _BagState As String
    Private _Value As Decimal
    Private _Comments As String

    <ColumnMapping("ID")> _
    Public Property ID() As Integer Implements ISafeBagModel.ID
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    <ColumnMapping("SealNumber")> _
    Public Property SealNumber() As String Implements ISafeBagModel.SealNumber
        Get
            Return _SealNumber
        End Get
        Set(ByVal value As String)
            _SealNumber = value
        End Set
    End Property

    <ColumnMapping("Type")> _
    Public Property BagType() As String Implements ISafeBagModel.BagType
        Get
            Return _BagType
        End Get
        Set(ByVal value As String)
            _BagType = value
        End Set
    End Property

    <ColumnMapping("State")> _
    Public Property BagState() As String Implements ISafeBagModel.BagState
        Get
            Return _BagState
        End Get
        Set(ByVal value As String)
            _BagState = value
        End Set
    End Property

    <ColumnMapping("Value")> _
    Public Property Value() As Decimal Implements ISafeBagModel.Value
        Get
            Return _Value
        End Get
        Set(ByVal value As Decimal)
            _Value = value
        End Set
    End Property

    <ColumnMapping("Comments")> _
    Public Property Comments() As String Implements ISafeBagModel.Comments
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property
#End Region

End Class

Public Class SafeBagCollectionModel
    Inherits BaseCollection(Of SafeBagModel)
    Implements ISafeBagCollectionModel

    Friend _EndOfDayRepository As IEndOfDayCheckRepository
    Friend _EndOfDayData As DataTable
    Friend _PeriodID As Integer
    Friend _BagID As Integer
    Friend _SafeBagData As DataTable
    Friend _SafeBagRepository As ISafeBagRepository

#Region "Methods"

    Public Sub LoadBankingBagExistInSafe(ByVal PeriodID As Integer) Implements ISafeBagCollectionModel.LoadBankingBagExistInSafe

        SetEndOfDayRepositoryImplementation()
        SetPeriodID(PeriodID)
        LoadBankingBagExistInSafeData()
        PopulateBOFromEndOfDay()

    End Sub

    Public Sub LoadPickupBagExistInSafe(ByVal PeriodID As Integer) Implements ISafeBagCollectionModel.LoadPickupBagExistInSafe

        SetEndOfDayRepositoryImplementation()

        SetPeriodID(PeriodID)

        LoadPickupBagExistInSafeData()

        PopulateBOFromEndOfDay()

    End Sub

    Public Function ListBags() As IList(Of SafeBagModel) Implements ISafeBagCollectionModel.ListBags

        Return Me.Items

    End Function

    Public Sub LoadSafeBag(ByVal BagID As Integer) Implements ISafeBagCollectionModel.LoadSafeBag

        SetSafeBagRepositoryImplementation()
        SetBagID(BagID)
        LoadSafeBag()
        PopulateBOFromSafeBag()
    End Sub
#End Region

#Region "Private Procedures And Functions"

    Friend Overridable Sub SetEndOfDayRepositoryImplementation()

        _EndOfDayRepository = (New EndOfDayCheckRepositoryFactory).GetImplementation

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value

    End Sub

    Friend Overridable Sub SetSafeBagRepositoryImplementation()

        _SafeBagRepository = (New SafeBagRepositoryFactory).GetImplementation
    End Sub

    Friend Overridable Sub SetBagID(ByVal Value As Integer)

        _BagID = Value
    End Sub

    Friend Overridable Sub LoadPickupBagExistInSafeData()

        _EndOfDayData = _EndOfDayRepository.GetPickupBagExistInSafe(New Connection, _PeriodID)

    End Sub

    Friend Overridable Sub LoadBankingBagExistInSafeData()

        _EndOfDayData = _EndOfDayRepository.GetBankingBagExistInSafe(New Connection, _PeriodID)

    End Sub

    Friend Overridable Sub LoadSafeBag()

        _SafeBagData = _SafeBagRepository.GetSafeBag(New Connection, _BagID)
    End Sub

    Friend Overridable Sub PopulateBOFromEndOfDay()

        If _EndOfDayData IsNot Nothing AndAlso _EndOfDayData.Rows.Count >= 1 Then Me.Load(_EndOfDayData)

    End Sub

    Friend Overridable Sub PopulateBOFromSafeBag()

        If _SafeBagData IsNot Nothing AndAlso _SafeBagData.Rows.Count >= 1 Then
            Me.Load(_SafeBagData)
        End If
    End Sub
#End Region
End Class
