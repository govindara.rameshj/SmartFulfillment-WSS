﻿Public Class SafeBagScannedModel
    Implements ISafeBagScannedModel

#Region "Interface: Properties"

    Private _ID As Integer
    Private _ProcessID As Integer
    Private _PeriodID As Integer
    Private _SafeBagsID As System.Nullable(Of Integer)
    Private _SealNumber As String
    Private _BagType As String
    Private _Comment As String

    <ColumnMapping("ID")> Public Property ID() As Integer Implements ISafeBagScannedModel.ID
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    <ColumnMapping("BusinessProcessID")> Public Property ProcessID() As Integer Implements ISafeBagScannedModel.ProcessID
        Get
            Return _ProcessID
        End Get
        Set(ByVal value As Integer)
            _ProcessID = value
        End Set
    End Property

    <ColumnMapping("SafePeriodID")> Public Property PeriodID() As Integer Implements ISafeBagScannedModel.PeriodID
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As Integer)
            _PeriodID = value
        End Set
    End Property

    <ColumnMapping("SafeBagsID")> Public Property SafeBagsID() As System.Nullable(Of Integer) Implements ISafeBagScannedModel.SafeBagsID
        Get
            Return _SafeBagsID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _SafeBagsID = value
        End Set
    End Property

    <ColumnMapping("SealNumber")> Public Property SealNumber() As String Implements ISafeBagScannedModel.SealNumber
        Get
            Return _SealNumber
        End Get
        Set(ByVal value As String)
            _SealNumber = value
        End Set
    End Property

    <ColumnMapping("BagType")> Public Property BagType() As String Implements ISafeBagScannedModel.BagType
        Get
            Return _BagType
        End Get
        Set(ByVal value As String)
            _BagType = value
        End Set
    End Property

    Public Overridable Property Comment() As String Implements ISafeBagScannedModel.Comment
        Get
            Return _Comment
        End Get
        Set(ByVal value As String)
            _Comment = value
        End Set
    End Property

#End Region

#Region "Interface: Methods"

    Public Overridable Sub Persist(ByRef Con As Connection) Implements ISafeBagScannedModel.Persist

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeBagsScannedPersist(Con, Me)

    End Sub

    Public Sub Persist(ByRef OdbcConnection As clsOasys3DB) Implements ISafeBagScannedModel.Persist

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeBagsScannedPersist(OdbcConnection, Me)

    End Sub

    Public Sub SafeMaintenanceDeleteExisting(ByRef OdbcConnection As clsOasys3DB) Implements ISafeBagScannedModel.SafeMaintenanceDeleteExisting

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeBagsScannedDeleteExisting(OdbcConnection, _ProcessID, _PeriodID)

    End Sub

#End Region

End Class