﻿Public Class EndOfDayCheckSummaryModel
    Inherits Base

    Friend _Repository As IEndOfDayCheckRepository
    Friend _DT As DataTable
    Friend _PeriodID As Integer

#Region "Table Fields And Properties"

    Private _PickupBagCount As Integer
    Private _PickupBagSystemCount As Integer

    Private _BankingCashCount As Integer
    Private _BankingCashSystemCount As Integer

    Private _BankingChequeCount As Integer
    Private _BankingChequeSystemCount As Integer

    Private _Manager As String
    Private _Witness As String

    <ColumnMapping("PickupBagCount")> Public Property PickupBagCount() As Integer
        Get
            Return _PickupBagCount
        End Get
        Set(ByVal value As Integer)
            _PickupBagCount = value
        End Set
    End Property

    <ColumnMapping("PickupBagSystemCount")> Public Property PickupBagSystemCount() As Integer
        Get
            Return _PickupBagSystemCount
        End Get
        Set(ByVal value As Integer)
            _PickupBagSystemCount = value
        End Set
    End Property

    <ColumnMapping("BankingCashCount")> Public Property BankingCashCount() As Integer
        Get
            Return _BankingCashCount
        End Get
        Set(ByVal value As Integer)
            _BankingCashCount = value
        End Set
    End Property

    <ColumnMapping("BankingCashSystemCount")> Public Property BankingCashSystemCount() As Integer
        Get
            Return _BankingCashSystemCount
        End Get
        Set(ByVal value As Integer)
            _BankingCashSystemCount = value
        End Set
    End Property

    <ColumnMapping("BankingChequeCount")> Public Property BankingChequeCount() As Integer
        Get
            Return _BankingChequeCount
        End Get
        Set(ByVal value As Integer)
            _BankingChequeCount = value
        End Set
    End Property

    <ColumnMapping("BankingChequeSystemCount")> Public Property BankingChequeSystemCount() As Integer
        Get
            Return _BankingChequeSystemCount
        End Get
        Set(ByVal value As Integer)
            _BankingChequeSystemCount = value
        End Set
    End Property

    <ColumnMapping("Manager")> Public Property Manager() As String
        Get
            Return _Manager
        End Get
        Set(ByVal value As String)
            _Manager = value
        End Set
    End Property

    <ColumnMapping("Witness")> Public Property Witness() As String
        Get
            Return _Witness
        End Get
        Set(ByVal value As String)
            _Witness = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadData(ByVal PeriodID As Integer)

        SetRepositoryImplementation()

        SetPeriodID(PeriodID)

        PopulateDataTable()

        PopulateBO()

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Friend Overridable Sub SetRepositoryImplementation()

        _Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value

    End Sub

    Friend Overridable Sub PopulateDataTable()

        _DT = _Repository.GetSummary(New Connection, _PeriodID)

    End Sub

    Friend Overridable Sub PopulateBO()

        If _DT IsNot Nothing AndAlso _DT.Rows.Count = 1 Then Me.Load(_DT.Rows(0))

    End Sub

#End Region

End Class