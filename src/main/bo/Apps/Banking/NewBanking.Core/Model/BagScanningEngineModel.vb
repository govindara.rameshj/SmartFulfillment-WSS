﻿Public Class BagScanningEngineModel
    Implements IBagScanningEngine

    Friend _PeriodID As Integer
    Friend _ProcessID As IBagScanningEngine.BusinessProcess
    Friend _SealNumber As String
    Friend _SealComment As String

    Friend _Safe As ISafeModel

    Friend _PickupBagList As New List(Of ISafeBagScannedModel)
    Friend _BankingBagList As New List(Of ISafeBagScannedModel)

    Friend _PickupCommentList As New List(Of ISafeCommentModel)
    Friend _BankingCommentList As New List(Of ISafeCommentModel)

    Friend _PickupBagExistInSafeList As List(Of ISafeBagModel)
    Friend _BankingBagExistInSafeList As List(Of ISafeBagModel)
    'Friend _PickupBagExistInSafeList As ISafeBagCollectionModel
    'Friend _BankingBagExistInSafeList As ISafeBagCollectionModel

    Private Enum BagType
        Banking
        Pickup
    End Enum

#Region "Interface"

    Public Sub Initialise(ByVal PeriodID As Integer, _
                          ByVal ProcessID As IBagScanningEngine.BusinessProcess) Implements IBagScanningEngine.Initialise

        SetPeriodID(PeriodID)

        SetProcessID(ProcessID)

        CreateSafeModel()

    End Sub

    Public Sub EndOfDayCheckAdditionalInformation(ByVal CheckDone As Boolean, _
                                                  ByVal LockedFrom As Date, _
                                                  ByVal LockedTo As Date, _
                                                  ByVal ManagerID As Integer, _
                                                  ByVal WitnessID As Integer) Implements IBagScanningEngine.EndOfDayCheckAdditionalInformation

        If SafeIsNothing() = True Then Exit Sub

        With _Safe

            .EndOfDayCheckDone = CheckDone
            .EndOfDayCheckLockedFrom = LockedFrom
            .EndOfDayCheckLockedTo = LockedTo
            .EndOfDayCheckManagerID = ManagerID
            .EndOfDayCheckWitnessID = WitnessID

        End With

    End Sub

    Public Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.BankingBagAlreadyScanned

        Return _BankingBagList.Any(Function(x As ISafeBagScannedModel) x.SealNumber = SealNumber)

    End Function

    Public Function PickupBagAlreadyScanned(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.PickupBagAlreadyScanned

        Return _PickupBagList.Any(Function(x As ISafeBagScannedModel) x.SealNumber = SealNumber)

    End Function

    Public Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.BankingBagHasValidSeal

        SetSealNumber(SealNumber)

        Return ValidSeal()

    End Function

    Public Function PickupBagHasValidSeal(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.PickupBagHasValidSeal

        SetSealNumber(SealNumber)

        Return ValidSeal()

    End Function

    Public Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.BankingBagExistInSafe

        SetSealNumber(SealNumber)

        GetBankingBagExistInSafeData()

        Return CheckBankingBagExistInSafe()

    End Function

    Public Function PickupBagExistInSafe(ByVal SealNumber As String) As Boolean Implements IBagScanningEngine.PickupBagExistInSafe

        SetSealNumber(SealNumber)

        GetPickupBagExistInSafeData()

        Return CheckPickupBagExistInSafe()

    End Function

    Public Function MatchSystemBankingExpectation() As Boolean Implements IBagScanningEngine.MatchSystemBankingExpectation

        Dim SystemCount As Integer

        GetBankingBagExistInSafeData()

        If _BankingBagExistInSafeList Is Nothing Then

            SystemCount = 0

        Else

            SystemCount = _BankingBagExistInSafeList.Count

        End If

        If SystemCount <> _BankingBagList.Count Then Return False




        Dim Temp As String

        For Each Bag As ISafeBagScannedModel In _BankingBagList

            Temp = Bag.SealNumber

            If _BankingBagExistInSafeList.Any(Function(x As ISafeBagModel) x.SealNumber = Temp) = False Then Return False

        Next

        Return True

    End Function

    Public Function MatchSystemPickupExpectation() As Boolean Implements IBagScanningEngine.MatchSystemPickupExpectation

        Dim SystemCount As Integer

        GetPickupBagExistInSafeData()

        If _PickupBagExistInSafeList Is Nothing Then

            SystemCount = 0

        Else

            SystemCount = _PickupBagExistInSafeList.Count

        End If

        If SystemCount <> _PickupBagList.Count Then Return False




        Dim Temp As String

        For Each Bag As ISafeBagScannedModel In _PickupBagList

            Temp = Bag.SealNumber

            If _PickupBagExistInSafeList.Any(Function(x As ISafeBagModel) x.SealNumber = Temp) = False Then Return False

        Next

        Return True

    End Function

    Public Sub CreateBankingBag(ByVal SealNumber As String) Implements IBagScanningEngine.CreateBankingBag

        Dim Obj As ISafeBagScannedModel

        SetSealNumber(SealNumber)

        Obj = GetBagModelImplementation()

        With Obj

            .BagType = "B"
            .SealNumber = _SealNumber
            .PeriodID = CurrentPeriodID()
            .SafeBagsID = SealNumberBagID()

        End With

        _BankingBagList.Add(Obj)

    End Sub

    Public Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String) Implements IBagScanningEngine.CreateBankingBag

        Dim Obj As ISafeBagScannedModel

        SetSealNumber(SealNumber)

        SetSealComment(SealComment)

        Obj = GetBagModelImplementation()

        With Obj

            .BagType = "B"
            .SealNumber = _SealNumber
            .Comment = _SealComment
            .PeriodID = CurrentPeriodID()
            .SafeBagsID = SealNumberBagID()

        End With

        _BankingBagList.Add(Obj)

    End Sub

    Public Sub CreateBankingComment(ByVal Value As String) Implements IBagScanningEngine.CreateBankingComment

        Dim Obj As ISafeCommentModel

        Obj = GetCommentModelImplementation()

        With Obj

            .PeriodID = CurrentPeriodID()
            .BagType = "B"
            .Comment = Value

        End With

        _BankingCommentList.Add(Obj)

    End Sub

    Public Sub CreatePickupBag(ByVal SealNumber As String) Implements IBagScanningEngine.CreatePickupBag

        Dim Obj As ISafeBagScannedModel

        SetSealNumber(SealNumber)

        Obj = GetBagModelImplementation()

        With Obj

            .BagType = "P"
            .SealNumber = _SealNumber
            .PeriodID = CurrentPeriodID()
            .SafeBagsID = SealNumberBagID()

        End With

        _PickupBagList.Add(Obj)

    End Sub

    Public Sub CreatePickupBag(ByVal SealNumber As String, ByVal SealComment As String) Implements IBagScanningEngine.CreatePickupBag

        Dim Obj As ISafeBagScannedModel

        SetSealNumber(SealNumber)

        SetSealComment(SealComment)

        Obj = GetBagModelImplementation()

        With Obj

            .BagType = "P"
            .SealNumber = _SealNumber
            .Comment = _SealComment
            .PeriodID = CurrentPeriodID()
            .SafeBagsID = SealNumberBagID()

        End With

        _PickupBagList.Add(Obj)

    End Sub

    Public Sub CreatePickupComment(ByVal Value As String) Implements IBagScanningEngine.CreatePickupComment

        Dim Obj As ISafeCommentModel

        Obj = GetCommentModelImplementation()

        With Obj

            .PeriodID = CurrentPeriodID()
            .BagType = "P"
            .Comment = Value

        End With

        _PickupCommentList.Add(Obj)

    End Sub

    Public Function PickupBagList() As List(Of ISafeBagScannedModel) Implements IBagScanningEngine.PickupBagList

        Return _PickupBagList

    End Function

    Public Function BankingBagList() As System.Collections.Generic.List(Of ISafeBagScannedModel) Implements IBagScanningEngine.BankingBagList

        Return _BankingBagList

    End Function

    Public Function PickupCommentList() As System.Collections.Generic.List(Of ISafeCommentModel) Implements IBagScanningEngine.PickupCommentList

        Return _PickupCommentList

    End Function

    Public Function BankingCommentList() As System.Collections.Generic.List(Of ISafeCommentModel) Implements IBagScanningEngine.BankingCommentList

        Return _BankingCommentList

    End Function

    Public Function EndOfDayCheckPersist() As Boolean Implements IBagScanningEngine.EndOfDayCheckPersist

        UpdatePickupBagProperties()
        UpdatePickupCommentProperties()
        UpdateBankingBagProperties()
        UpdateBankingCommentProperties()



        _Safe.PeriodID = _PeriodID



        Using Con As New Connection

            Try
                Con.StartTransaction()

                SavePickupBagList(Con)
                SavePickupCommentList(Con)
                SaveBankingBagList(Con)
                SaveBankingCommentList(Con)
                SaveSafe(Con)

                Con.CommitTransaction()

                EndOfDayCheckPersist = True

            Catch ex As Exception

                Con.RollbackTransaction()

                EndOfDayCheckPersist = False

            End Try

        End Using

    End Function

    Public Sub SafeMaintenancePersist(ByRef OdbcConnection As clsOasys3DB) Implements IBagScanningEngine.SafeMaintenancePersist

        UpdateBankingBagProperties()
        UpdateBankingCommentProperties()

        DeleteBankingBagList(OdbcConnection)
        DeleteBankingCommentList(OdbcConnection)

        SaveBankingBagList(OdbcConnection)
        SaveBankingCommentList(OdbcConnection)

    End Sub

#End Region

#Region "Private Functions & Procedures"

    Friend Function CurrentPeriodID() As Integer

        If SafeIsNothing() = True Then Return 0

        Return _PeriodID

    End Function

    Friend Sub CreateSafeModel()

        Select Case _ProcessID

            Case IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess

                _Safe = (New SafeModelFactory).GetImplementation

            Case IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess

                'n/a

        End Select

    End Sub



    Friend Sub UpdatePickupBagProperties()

        If SafeIsNothing() = True Then Exit Sub

        For Each Bag As SafeBagScannedModel In _PickupBagList

            Bag.PeriodID = _PeriodID
            Bag.ProcessID = _ProcessID

        Next

    End Sub

    Friend Sub UpdatePickupCommentProperties()

        If SafeIsNothing() = True Then Exit Sub

        For Each Comment As SafeCommentModel In _PickupCommentList

            Comment.PeriodID = _PeriodID
            Comment.ProcessID = _ProcessID

        Next

    End Sub

    Friend Sub UpdateBankingBagProperties()

        'If SafeIsNothing() = True Then Exit Sub

        For Each Bag As SafeBagScannedModel In _BankingBagList

            Bag.PeriodID = _PeriodID
            Bag.ProcessID = _ProcessID

        Next

    End Sub

    Friend Sub UpdateBankingCommentProperties()

        'If SafeIsNothing() = True Then Exit Sub

        For Each Comment As SafeCommentModel In _BankingCommentList

            Comment.PeriodID = _PeriodID
            Comment.ProcessID = _ProcessID

        Next

    End Sub









    Friend Function SafeIsNothing() As Boolean

        If _Safe Is Nothing Then Return True

    End Function

    Friend Sub SavePickupBagList(ByRef Con As Connection)

        For Each Bag As SafeBagScannedModel In _PickupBagList

            Bag.Persist(Con)

        Next

    End Sub

    Friend Sub SavePickupCommentList(ByRef Con As Connection)

        For Each Comment As SafeCommentModel In _PickupCommentList

            Comment.Persist(Con)

        Next

    End Sub

    Friend Sub SaveBankingBagList(ByRef Con As Connection)

        For Each Bag As SafeBagScannedModel In _BankingBagList

            Bag.Persist(Con)

        Next

    End Sub

    Friend Sub SaveBankingBagList(ByRef OdbcConnection As clsOasys3DB)

        For Each Bag As SafeBagScannedModel In _BankingBagList

            Bag.Persist(OdbcConnection)

        Next

    End Sub

    Friend Sub SaveBankingCommentList(ByRef Con As Connection)

        For Each Comment As SafeCommentModel In _BankingCommentList

            Comment.Persist(Con)

        Next

    End Sub

    Friend Sub SaveBankingCommentList(ByRef OdbcConnection As clsOasys3DB)

        For Each Comment As SafeCommentModel In _BankingCommentList

            Comment.Persist(OdbcConnection)

        Next

    End Sub




    Friend Sub DeleteBankingBagList(ByRef OdbcConnection As clsOasys3DB)

        Dim Model As ISafeBagScannedModel

        Model = (New SafeBagScannedModelFactory).GetImplementation

        Model.ProcessID = _ProcessID
        Model.PeriodID = _PeriodID
        Model.SafeMaintenanceDeleteExisting(OdbcConnection)

    End Sub

    Friend Sub DeleteBankingCommentList(ByRef OdbcConnection As clsOasys3DB)

        Dim Model As ISafeCommentModel

        Model = (New SafeCommentModelFactory).GetImplementation

        Model.ProcessID = _ProcessID
        Model.PeriodID = _PeriodID
        Model.SafeMaintenanceDeleteExisting(OdbcConnection)

    End Sub






    Friend Sub SaveSafe(ByRef Con As Connection)

        _Safe.Persist(Con)

    End Sub

    Friend Overridable Sub SetSealNumber(ByVal SealNumber As String)

        _SealNumber = SealNumber

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal PeriodID As Integer)

        _PeriodID = PeriodID

    End Sub

    Friend Overridable Sub SetProcessID(ByVal ProcessID As IBagScanningEngine.BusinessProcess)

        _ProcessID = ProcessID

    End Sub

    Friend Overridable Function SealNumberBagID() As Integer

        Dim Repository As IEndOfDayCheckRepository

        Repository = GetRepository()

        Return ValidateSealNumber(Repository)

    End Function

    Friend Overridable Sub SetSealComment(ByVal SealComment As String)

        _SealComment = SealComment

    End Sub



    Friend Overridable Function ValidSeal() As Boolean

        If SealNumberBagID() > 0 Then Return True

    End Function


    Friend Overridable Sub GetPickupBagExistInSafeData()

        If PickupBagExistInSafeDataExist() = True Then Exit Sub

        GetBagExistInSafeData(BagType.Pickup, _PickupBagExistInSafeList)

    End Sub

    Friend Overridable Sub GetBankingBagExistInSafeData()

        If BankingBagExistInSafeDataExist() = True Then Exit Sub

        GetBagExistInSafeData(BagType.Banking, _BankingBagExistInSafeList)

    End Sub

    Friend Function PickupBagExistInSafeDataExist() As Boolean

        Return BagExistInSafeDataExist(_PickupBagExistInSafeList)

    End Function

    Friend Function BankingBagExistInSafeDataExist() As Boolean

        Return BagExistInSafeDataExist(_BankingBagExistInSafeList)

    End Function

    Private Function BagExistInSafeDataExist(ByRef Value As List(Of ISafeBagModel)) As Boolean

        If Value IsNot Nothing AndAlso Value.Count > 0 Then Return True

    End Function

    Private Sub GetBagExistInSafeData(ByVal Type As BagType, ByRef Value As List(Of ISafeBagModel))

        Dim Model As ISafeBagCollectionModel
        Model = (New SafeBagCollectionModelFactory).GetImplementation

        Select Case Type
            Case BagType.Banking
                Model.LoadBankingBagExistInSafe(_PeriodID)

            Case BagType.Pickup
                Model.LoadPickupBagExistInSafe(_PeriodID)

        End Select

        Value = New List(Of ISafeBagModel)

        For Each Obj As ISafeBagModel In Model.ListBags

            Value.Add(Obj)

        Next

    End Sub




    Friend Overridable Function CheckPickupBagExistInSafe() As Boolean

        Return CheckBagExistInSafe(_PickupBagExistInSafeList)

    End Function

    Friend Overridable Function CheckBankingBagExistInSafe() As Boolean

        Return CheckBagExistInSafe(_BankingBagExistInSafeList)

    End Function

    Private Function CheckBagExistInSafe(ByRef Value As List(Of ISafeBagModel)) As Boolean

        If Value Is Nothing Then Return False

        For Each Obj As ISafeBagModel In Value

            If Obj.SealNumber = _SealNumber Then Return True

        Next

    End Function

#Region "Override Factory"

    Friend Overridable Function GetBagModelImplementation() As ISafeBagScannedModel

        Return (New SafeBagScannedModelFactory).GetImplementation

    End Function

    Friend Overridable Function GetCommentModelImplementation() As ISafeCommentModel

        Return (New SafeCommentModelFactory).GetImplementation

    End Function

    Friend Overridable Function GetRepository() As IEndOfDayCheckRepository

        Return (New EndOfDayCheckRepositoryFactory).GetImplementation

    End Function

    Friend Overridable Function ValidateSealNumber(ByVal Repository As IEndOfDayCheckRepository) As Integer

        Return Repository.ValidateSealNumber(_SealNumber)

    End Function

#End Region

#End Region

End Class