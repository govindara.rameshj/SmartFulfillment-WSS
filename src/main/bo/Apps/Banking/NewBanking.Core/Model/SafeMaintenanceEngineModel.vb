﻿Public Class SafeMaintenanceEngineModel
    Implements ISafeMaintenanceEngine

    Private _SafeMaintenanceModel As ISafeMaintenanceModel

#Region "Implementation ISafeMaintenanceEngine"

    Public Sub CreateSafeMaintenance(ByVal PeriodID As Integer) Implements ISafeMaintenanceEngine.CreateSafeMaintenance

        CreateSafeMaintenanceModel(PeriodID)

    End Sub

    Public Function Persist() As Boolean Implements ISafeMaintenanceEngine.Persist

        If SafeMaintenanceModelIsSetUp() Then

            Using Con As New Connection

                Try

                    Con.StartTransaction()

                    SaveSafeMaintenance(Con)

                    Con.CommitTransaction()
                    Persist = True

                Catch ex As Exception

                    Con.RollbackTransaction()

                    Persist = False

                End Try

            End Using

        End If

    End Function

    Public Sub Persist(ByRef OdbcConnection As clsOasys3DB) Implements ISafeMaintenanceEngine.Persist

        SaveSafeMaintenance(OdbcConnection)

    End Sub

    Public Function Load(ByVal PeriodID As Integer) As Boolean Implements ISafeMaintenanceEngine.Load

        If SafeMaintenanceModelIsSetUp() Then
            Using Con As New Connection
                Try
                    Con.StartTransaction()
                    LoadSafeMaintenance(Con, PeriodID)
                    Con.CommitTransaction()
                    Load = True
                Catch ex As Exception
                    Con.RollbackTransaction()
                    Load = False
                End Try
            End Using
        End If
    End Function

    Public Function SafeMaintenanceAlreadyPerformed() As Boolean Implements ISafeMaintenanceEngine.SafeMaintenanceAlreadyPerformed

        If _SafeMaintenanceModel IsNot Nothing Then Return _SafeMaintenanceModel.SafeChecked

        Return False

    End Function

    'Public Function GetBankingBags() As List(Of String) Implements ISafeMaintenanceEngine.GetBankingBags

    '    If SafeMaintenanceModelIsSetUp() Then
    '        GetBankingBags = _SafeMaintenanceModel.GetBankingBags
    '    Else
    '        GetBankingBags = New List(Of String)
    '    End If
    'End Function

    Public Property Comments() As String Implements ISafeMaintenanceEngine.Comments
        Get
            Comments = String.Empty
            If SafeMaintenanceModelIsSetUp() Then
                Return _SafeMaintenanceModel.Comments
            End If
        End Get
        Set(ByVal value As String)
            _SafeMaintenanceModel.Comments = value
        End Set
    End Property

    Public Property GiftTokenSeal() As String Implements ISafeMaintenanceEngine.GiftTokenSeal
        Get
            GiftTokenSeal = String.Empty
            If SafeMaintenanceModelIsSetUp() Then
                Return _SafeMaintenanceModel.GiftTokenSeal
            End If
        End Get
        Set(ByVal value As String)
            _SafeMaintenanceModel.GiftTokenSeal = value
        End Set
    End Property

    Public Property MiscellaneousProperty() As String Implements ISafeMaintenanceEngine.MiscellaneousProperty
        Get
            MiscellaneousProperty = String.Empty
            If SafeMaintenanceModelIsSetUp() Then
                Return _SafeMaintenanceModel.MiscellaneousProperty
            End If
        End Get
        Set(ByVal value As String)
            _SafeMaintenanceModel.MiscellaneousProperty = value
        End Set
    End Property

#End Region

#Region "Internals"

    Friend Overridable Function SafeMaintenanceModelIsSetUp() As Boolean

        Return Not SafeMaintenanceEngineIsNothing()
    End Function

    Friend Overridable Sub CreateSafeMaintenanceModel(ByVal PeriodID As Integer)

        If SafeMaintenanceEngineIsNothing() Then
            _SafeMaintenanceModel = (New SafeMaintenanceModelFactory).GetImplementation
        End If
        _SafeMaintenanceModel.PeriodID = PeriodID
    End Sub

    Friend Overridable Function SafeMaintenanceEngineIsNothing() As Boolean

        Return _SafeMaintenanceModel Is Nothing
    End Function

    Friend Overridable Sub SaveSafeMaintenance(ByRef Con As Connection)

        _SafeMaintenanceModel.PersistSafeMaintenance(Con)

    End Sub

    Friend Overridable Sub SaveSafeMaintenance(ByRef OdbcConnection As clsOasys3DB)

        _SafeMaintenanceModel.PersistSafeMaintenance(OdbcConnection)

    End Sub

    Friend Overridable Sub LoadSafeMaintenance(ByRef Con As Connection, ByVal PeriodId As Integer)

        _SafeMaintenanceModel.LoadSafeMaintenance(Con, PeriodId)
    End Sub

#End Region

End Class