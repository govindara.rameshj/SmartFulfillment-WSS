﻿Public Class SafeModel
    Implements ISafeModel

#Region "Interface: Properties"

    Private _PeriodID As Integer
    Private _EndOfDayCheckDone As Boolean
    Private _EndOfDayCheckLockedFrom As System.Nullable(Of Date)
    Private _EndOfDayCheckLockedTo As System.Nullable(Of Date)
    Private _EndOfDayCheckManagerID As System.Nullable(Of Integer)
    Private _EndOfDayCheckWitnessID As System.Nullable(Of Integer)

    <ColumnMapping("PeriodID")> Public Property PeriodID() As Integer Implements ISafeModel.PeriodID
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As Integer)
            _PeriodID = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckDone")> Public Property EndOfDayCheckDone() As Boolean Implements ISafeModel.EndOfDayCheckDone
        Get
            Return _EndOfDayCheckDone
        End Get
        Set(ByVal value As Boolean)
            _EndOfDayCheckDone = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckLockedFrom")> Public Property EndOfDayCheckLockedFrom() As System.Nullable(Of Date) Implements ISafeModel.EndOfDayCheckLockedFrom
        Get
            Return _EndOfDayCheckLockedFrom
        End Get
        Set(ByVal value As System.Nullable(Of Date))
            _EndOfDayCheckLockedFrom = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckLockedTo")> Public Property EndOfDayCheckLockedTo() As System.Nullable(Of Date) Implements ISafeModel.EndOfDayCheckLockedTo
        Get
            Return _EndOfDayCheckLockedTo
        End Get
        Set(ByVal value As System.Nullable(Of Date))
            _EndOfDayCheckLockedTo = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckManagerID")> Public Property EndOfDayCheckManagerID() As System.Nullable(Of Integer) Implements ISafeModel.EndOfDayCheckManagerID
        Get
            Return _EndOfDayCheckManagerID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _EndOfDayCheckManagerID = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckWitnessID")> Public Property EndOfDayCheckWitnessID() As System.Nullable(Of Integer) Implements ISafeModel.EndOfDayCheckWitnessID
        Get
            Return _EndOfDayCheckWitnessID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _EndOfDayCheckWitnessID = value
        End Set
    End Property

#End Region

#Region "Interface: Methods"

    Public Sub Persist(ByRef Con As Connection) Implements ISafeModel.Persist

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.PersistSafe(Con, Me)

    End Sub

#End Region

End Class