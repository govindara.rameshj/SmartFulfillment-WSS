﻿Public Class SafeCommentModel
    Implements ISafeCommentModel

#Region "Interface: Properties"

    Private _ID As Integer
    Private _ProcessID As Integer
    Private _PeriodID As Integer
    Private _BagType As String
    Private _Comment As String

    <ColumnMapping("ID")> Public Property ID() As Integer Implements ISafeCommentModel.ID
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    <ColumnMapping("BusinessProcessID")> Public Property ProcessID() As Integer Implements ISafeCommentModel.ProcessID
        Get
            Return _ProcessID
        End Get
        Set(ByVal value As Integer)
            _ProcessID = value
        End Set
    End Property

    <ColumnMapping("SafePeriodID")> Public Property PeriodID() As Integer Implements ISafeCommentModel.PeriodID
        Get
            Return _PeriodID
        End Get
        Set(ByVal value As Integer)
            _PeriodID = value
        End Set
    End Property

    <ColumnMapping("BagType")> Public Property BagType() As String Implements ISafeCommentModel.BagType
        Get
            Return _BagType
        End Get
        Set(ByVal value As String)
            _BagType = value
        End Set
    End Property

    <ColumnMapping("Comment")> Public Property Comment() As String Implements ISafeCommentModel.Comment
        Get
            Return _Comment
        End Get
        Set(ByVal value As String)
            _Comment = value
        End Set
    End Property

#End Region

#Region "Interface: Methods"

    Public Sub Persist(ByRef Con As Connection) Implements ISafeCommentModel.Persist

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeCommentPersist(Con, Me)

    End Sub

    Public Sub Persist(ByRef OdbcConnection As clsOasys3DB) Implements ISafeCommentModel.Persist

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeCommentPersist(OdbcConnection, Me)

    End Sub

    Public Sub SafeMaintenanceDeleteExisting(ByRef OdbcConnection As clsOasys3DB) Implements ISafeCommentModel.SafeMaintenanceDeleteExisting

        Dim Repository As IEndOfDayCheckRepository

        Repository = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Repository.SafeCommentDeleteExisting(OdbcConnection, _ProcessID, _PeriodID)

    End Sub

#End Region

End Class