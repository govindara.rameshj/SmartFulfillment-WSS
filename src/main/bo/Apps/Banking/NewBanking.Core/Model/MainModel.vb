﻿Public Class MainModel
    Implements IMainModel

    Friend _MainRepository As IMainRepository

#Region "IMainModel Implementation"

    Public Function GetCommentsDelimiter() As String Implements IMainModel.GetCommentsDelimiter

        SetMainRepository()
        GetCommentsDelimiter = GetCommentsDelimiterFromMainRepository()
    End Function
#End Region

#Region "Internals"

    Friend Overridable Function MainRepositoryIsSet() As Boolean

        Return _MainRepository IsNot Nothing
    End Function

    Friend Overridable Sub SetMainRepository()

        _MainRepository = (New MainRepositoryFactory).GetImplementation
    End Sub

    Friend Overridable Function GetCommentsDelimiterFromMainRepository() As String

        GetCommentsDelimiterFromMainRepository = String.Empty
        If MainRepositoryIsSet() Then
            GetCommentsDelimiterFromMainRepository = _MainRepository.GetCommentsDelimiter()
        End If
    End Function
#End Region
End Class
