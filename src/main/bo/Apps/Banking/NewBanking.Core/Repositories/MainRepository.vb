﻿Public Class MainRepository
    Implements IMainRepository

    Friend _Connection As IConnection
    Friend _Command As ICommand
    Friend _CommentsDelimiterData As DataTable
    Friend _CommentsDelimiter As String

    Public Function GetCommentsDelimiter() As String Implements IMainRepository.GetCommentsDelimiter

        Reset()
        SetConnection(Nothing)
        ExecuteGetCommentsDelimiter()
        Return GetCommentsDelimiterFromData
    End Function

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _CommentsDelimiterData = Nothing
        _CommentsDelimiter = String.Empty
    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then
            _Connection = New Connection
        Else
            _Connection = Con
        End If
    End Sub

    Friend Overridable Sub ExecuteGetCommentsDelimiter()

        ConfigureGetCommentsDelimiterCommand()
        ExecuteGetCommentsDelimiterCommand()
    End Sub

    Friend Overridable Function GetCommentsDelimiterFromData() As String

        GetCommentsDelimiterFromData = String.Empty
        If CommentsDelimiterDataHasData Then
            Return _CommentsDelimiterData.Rows(0).Item(0).ToString
        End If
    End Function

    Friend Overridable Sub ConfigureGetCommentsDelimiterCommand()

        If ConnectionIsSet() Then
            _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
            _Command.StoredProcedureName = "NewBankingGetCommentsDelimiter"
        End If
    End Sub

    Friend Overridable Sub ExecuteGetCommentsDelimiterCommand()

        If CommandIsSet() Then
            ExecuteDataTable()
        End If
    End Sub

    Friend Function ConnectionIsSet() As Boolean

        Return _Connection IsNot Nothing
    End Function

    Friend Function CommandIsSet() As Boolean

        Return _Command IsNot Nothing
    End Function

    Friend Overridable Function CommentsDelimiterDataHasData() As Boolean

        CommentsDelimiterDataHasData = False
        If _CommentsDelimiterData IsNot Nothing Then
            If _CommentsDelimiterData.Rows.Count > 0 Then
                If _CommentsDelimiterData.Rows(0).ItemArray.Count > 0 Then
                    CommentsDelimiterDataHasData = True
                End If
            End If
        End If
    End Function

    Friend Overridable Sub ExecuteDataTable()

        _CommentsDelimiterData = _Command.ExecuteDataTable()
    End Sub
End Class
