﻿Public Class SafeCheckReportAuthorisersRepository

    Public Sub UpdateSafeCheckedUsers(ByVal periodId As Integer, ByVal UserId1 As Integer, ByVal UserId2 As Integer, ByVal LastAmended As Date)

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_SafeCheckedUsersUpdate"
                com.AddParameter("@PeriodId", periodId, SqlDbType.Int)
                com.AddParameter("@SafeCheckedUserId1", UserId1, SqlDbType.Int)
                com.AddParameter("@SafeCheckedUserId2", UserId2, SqlDbType.Int)
                com.AddParameter("@SafeCheckedLastAmended", LastAmended, SqlDbType.DateTime)
                com.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Public Sub UpdateSafeCheckedUsers(ByRef OdbcConnection As clsOasys3DB, _
                                      ByVal periodId As Integer, _
                                      ByVal UserId1 As Integer, _
                                      ByVal UserId2 As Integer, _
                                      ByVal LastAmended As Date)

        Using Com As New Data.Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL usp_SafeCheckedUsersUpdate( ?, ?, ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("PeriodId", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafeCheckedUserId1", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafeCheckedUserId2", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafeCheckedLastAmended", Odbc.OdbcType.DateTime).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = periodId
            Com.Parameters(1).Value = UserId1
            Com.Parameters(2).Value = UserId2
            Com.Parameters(3).Value = LastAmended.ToString("yyyy-MM-dd HH:mm:ss")

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

End Class