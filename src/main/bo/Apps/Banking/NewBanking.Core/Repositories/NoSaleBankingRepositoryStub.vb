﻿Namespace TpWickes

    Public Class NoSaleBankingRepositoryStub
        Implements INoSaleBankingRepository

#Region "Stub Code"

        Private _NoSaleBanking As Nullable(Of Boolean)

        Friend Sub ConfigureStub(ByVal NoSaleBanking As Nullable(Of Boolean))

            _NoSaleBanking = NoSaleBanking

        End Sub

#End Region

#Region "Interface"

        Public Function AllowNoSaleBanking() As Boolean? Implements INoSaleBankingRepository.AllowNoSaleBanking

            'ignore parameters

            Return _NoSaleBanking

        End Function

#End Region

    End Class

End Namespace