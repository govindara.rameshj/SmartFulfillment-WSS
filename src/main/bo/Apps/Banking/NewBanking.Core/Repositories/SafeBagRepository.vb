﻿Public Class SafeBagRepository
    Implements ISafeBagRepository

    Friend _Connection As IConnection
    Friend _Command As ICommand
    Friend _DT As DataTable

    Friend _BagID As Integer

    Public Function GetSafeBag(ByRef Con As Cts.Oasys.Data.Connection, ByVal BagID As Integer) As System.Data.DataTable Implements ISafeBagRepository.GetSafeBag

        Reset()
        SetConnection(Con)
        SetBagID(BagID)
        ExecuteGetSafeBag()

        Return _DT
    End Function

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DT = Nothing
        _BagID = 0
    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then
            _Connection = New Connection
        Else
            _Connection = Con
        End If
    End Sub

    Friend Overridable Sub SetBagID(ByVal Value As Integer)

        _BagID = Value
    End Sub

    Friend Overridable Sub ExecuteDataTable()

        _DT = _Command.ExecuteDataTable()
    End Sub

#Region "ExecuteGetSafeBag"

    Friend Overridable Sub ExecuteGetSafeBag()

        ConfigureGetSafeBagCommand()
        ExecuteGetSafeBagCommand()
    End Sub

    Friend Overridable Sub ConfigureGetSafeBagCommand()

        If ConnectionIsSetup() Then
            If BagIdIsSetup() Then
                _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
                _Command.StoredProcedureName = "NewBankingSafeBagGet"
                _Command.AddParameter("@BagID", _BagID)
            End If
        End If
    End Sub

    Friend Overridable Sub ExecuteGetSafeBagCommand()

        If CommandIsSetup Then
            ExecuteDataTable()
        End If
    End Sub

    Friend Overridable Function ConnectionIsSetup() As Boolean

        Return _Connection IsNot Nothing
    End Function

    Friend Overridable Function BagIdIsSetup() As Boolean

        Return _BagID <> 0
    End Function

    Friend Overridable Function CommandIsSetup() As Boolean

        Return _Command IsNot Nothing
    End Function
#End Region
End Class
