﻿Public Class SafeMaintenanceRepository
    Implements ISafeMaintenanceRepository

    Friend _Connection As IConnection
    Friend _Command As ICommand
    Friend _LoadedSafeMaintenance As DataTable
    Friend _LoadedBankingBags As DataTable

    Friend _PeriodID As Nullable(Of Integer)
    Friend _SafeMaintenance As ISafeMaintenanceModel

    Const _BankingBagSealNumberFieldName As String = "SealNumber"

#Region "ISafeMaintenanceRepository Implementation"

    Public Function Load(ByRef Con As Cts.Oasys.Data.Connection, ByVal PeriodID As Integer) As System.Data.DataTable Implements ISafeMaintenanceRepository.Load

        Initialise(Con)
        SetPeriodID(PeriodID)
        ExecuteGetSafeMaintenance()

        Return _LoadedSafeMaintenance
    End Function

    Public Sub Persist(ByRef Con As Cts.Oasys.Data.Connection, ByRef SafeMaintenance As ISafeMaintenanceModel) Implements ISafeMaintenanceRepository.Persist

        Initialise(Con)
        SetSafeMaintenance(SafeMaintenance)
        ExecuteSaveSafeMaintenance()
    End Sub

    Public Sub Persist(ByRef OdbcConnection As clsOasys3DB, ByRef SafeMaintenance As ISafeMaintenanceModel) Implements ISafeMaintenanceRepository.Persist

        SetSafeMaintenance(SafeMaintenance)

        Using Com As New Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL NewBankingSafeMaintenanceUpdate( ?, ?, ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("PeriodID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("MiscellaneousProperty", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input
            Com.Parameters.Add("GiftTokenSeal", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input
            Com.Parameters.Add("Comments", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = _SafeMaintenance.PeriodID
            Com.Parameters(1).Value = _SafeMaintenance.MiscellaneousProperty
            Com.Parameters(2).Value = _SafeMaintenance.GiftTokenSeal
            Com.Parameters(3).Value = _SafeMaintenance.Comments

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

    'Public Function GetBankingBagSealNumber(ByVal BankingBag As System.Data.DataRow) As String Implements ISafeMaintenanceRepository.GetBankingBagSealNumber

    '    If BankingBag IsNot Nothing Then
    '        Try
    '            GetBankingBagSealNumber = BankingBag(_BankingBagSealNumberFieldName).ToString
    '        Catch ex As Exception
    '            GetBankingBagSealNumber = String.Empty
    '        End Try
    '    Else
    '        GetBankingBagSealNumber = String.Empty
    '    End If
    'End Function

    'Public Function LoadBankingBags(ByRef Con As Cts.Oasys.Data.Connection, ByVal PeriodID As Integer) As System.Data.DataTable Implements ISafeMaintenanceRepository.LoadBankingBags

    '    Initialise(Con)
    '    SetPeriodID(PeriodID)
    '    ExecuteGetBankingBags()
    '    LoadBankingBags = _LoadedBankingBags
    'End Function

#End Region

#Region "Internal functionality"

    Friend Overridable Sub Initialise(ByRef Con As Cts.Oasys.Data.Connection)

        ResetMe()
        SetConnection(Con)
    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value
    End Sub

    Friend Overridable Sub ExecuteGetSafeMaintenance()

        ConfigureGetSafeMaintenanceCommand()
        ExecuteGetSafeMaintenanceCommand()
    End Sub

    Friend Overridable Sub SetSafeMaintenance(ByRef SafeMaintenance As ISafeMaintenanceModel)

        _SafeMaintenance = SafeMaintenance
    End Sub

    Friend Overridable Sub ExecuteSaveSafeMaintenance()

        ConfigureSaveSafeMaintenanceCommand()
        ExecuteSaveSafeMaintenanceCommand()
    End Sub

    Friend Overridable Sub ExecuteGetBankingBags()

        ConfigureGetBankingBagsCommand()
        ExecuteGetBankingBagsCommand()
    End Sub

    Friend Overridable Sub ResetMe()

        _Connection = Nothing
        _Command = Nothing
        _LoadedSafeMaintenance = Nothing
        _PeriodID = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then
            _Connection = New Connection
        Else
            _Connection = Con
        End If
    End Sub

    Friend Overridable Sub ConfigureGetSafeMaintenanceCommand()

        If ConnectionIsSetUp() Then
            If PeriodIDIsSet() Then
                _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
                _Command.StoredProcedureName = "NewBankingSafeMaintenanceGet"
                _Command.AddParameter("@PeriodID", _PeriodID.Value)
            End If
        End If
    End Sub

    Friend Overridable Sub ExecuteGetSafeMaintenanceCommand()

        If CommandIsSetUp() Then
            _LoadedSafeMaintenance = ExecuteDataTable()
        End If
    End Sub

    Friend Overridable Sub ConfigureSaveSafeMaintenanceCommand()

        If ConnectionIsSetUp() Then
            If SafeMaintenanceIsSetUp() Then
                _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
                _Command.StoredProcedureName = "NewBankingSafeMaintenanceUpdate"
                _Command.AddParameter("@PeriodID", _SafeMaintenance.PeriodID)
                _Command.AddParameter("@MiscellaneousProperty", _SafeMaintenance.MiscellaneousProperty)
                _Command.AddParameter("@GiftTokenSeal", _SafeMaintenance.GiftTokenSeal)
                _Command.AddParameter("@Comments", _SafeMaintenance.Comments)
            End If
        End If
    End Sub

    Friend Overridable Sub ExecuteSaveSafeMaintenanceCommand()

        If CommandIsSetUp() Then
            _LoadedBankingBags = ExecuteDataTable()
        End If
    End Sub

    Friend Overridable Sub ConfigureGetBankingBagsCommand()

        If ConnectionIsSetUp() Then
            If PeriodIDIsSet() Then
                _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
                _Command.StoredProcedureName = "NewBankingPhysicalBankingBags"
                _Command.AddParameter("@PeriodID", _PeriodID)
            End If
        End If
    End Sub

    Friend Overridable Sub ExecuteGetBankingBagsCommand()

        If CommandIsSetUp() Then
            _LoadedBankingBags = ExecuteDataTable()
        End If
    End Sub

    Friend Overridable Function ConnectionIsSetUp() As Boolean

        Return _Connection IsNot Nothing
    End Function

    Friend Overridable Function SafeMaintenanceIsSetUp() As Boolean

        Return _SafeMaintenance IsNot Nothing
    End Function

    Friend Overridable Function PeriodIDIsSet() As Boolean

        Return _PeriodID.HasValue
    End Function

    Friend Overridable Function CommandIsSetUp() As Boolean

        Return _Command IsNot Nothing
    End Function

    Friend Overridable Function ExecuteDataTable() As DataTable

        ExecuteDataTable = _Command.ExecuteDataTable
        If ExecuteDataTable Is Nothing Then
            ExecuteDataTable = New DataTable
        End If
    End Function

    Friend Overridable Sub ExecuteNonQuery()

        _Command.ExecuteNonQuery()
    End Sub

#End Region

End Class