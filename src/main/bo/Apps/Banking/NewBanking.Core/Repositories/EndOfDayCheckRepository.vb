﻿Public Class EndOfDayCheckRepository
    Implements IEndOfDayCheckRepository

    Friend _Connection As IConnection
    Friend _Command As ICommand
    Friend _DT As DataTable

    Friend _PeriodID As Integer
    Friend _Safe As ISafeModel
    Friend _Bag As ISafeBagScannedModel
    Friend _Comment As ISafeCommentModel
    Friend _SealNumber As String
    Friend _ValidatedSealNumberID As Integer

#Region "Interface"

    Public Function GetSummary(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable Implements IEndOfDayCheckRepository.GetSummary

        Reset()

        SetConnection(Con)

        SetPeriodID(PeriodID)

        ExecuteGetSummary()

        Return _DT

    End Function

    Public Function GetBankingBagExistInSafe(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable Implements IEndOfDayCheckRepository.GetBankingBagExistInSafe

        Reset()

        SetConnection(Con)

        SetPeriodID(PeriodID)

        ExecuteGetBankingBagExistInSafe()

        Return _DT

    End Function

    Public Function GetPickupBagExistInSafe(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable Implements IEndOfDayCheckRepository.GetPickupBagExistInSafe

        Reset()

        SetConnection(Con)

        SetPeriodID(PeriodID)

        ExecuteGetPickupBagExistInSafe()

        Return _DT

    End Function

#Region "Safe Bag Scanned"

    Public Sub SafeBagsScannedPersist(ByRef Con As Connection, ByRef Bag As ISafeBagScannedModel) Implements IEndOfDayCheckRepository.SafeBagsScannedPersist

        Reset()

        SetConnection(Con)

        SetBags(Bag)

        ExecuteSaveBag()

    End Sub

    Public Sub SafeBagsScannedPersist(ByRef OdbcConnection As clsOasys3DB, ByRef Bag As ISafeBagScannedModel) Implements IEndOfDayCheckRepository.SafeBagsScannedPersist

        SetBags(Bag)

        Using Com As New Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL NewBankingSafeBagScannedInsert( ?, ?, ?, ?, ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("BusinessProcessID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafePeriodID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafeBagsID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SealNumber", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input
            Com.Parameters.Add("BagType", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input
            Com.Parameters.Add("Comment", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = _Bag.ProcessID
            Com.Parameters(1).Value = _Bag.PeriodID
            Com.Parameters(2).Value = _Bag.SafeBagsID
            Com.Parameters(3).Value = _Bag.SealNumber
            Com.Parameters(4).Value = _Bag.BagType
            Com.Parameters(5).Value = _Bag.Comment

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

    Public Sub SafeBagsScannedDeleteExisting(ByRef OdbcConnection As clsOasys3DB, ByVal ProcessID As Integer, ByVal PeriodID As Integer) Implements IEndOfDayCheckRepository.SafeBagsScannedDeleteExisting

        Using Com As New Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL NewBankingSafeBagScannedDeleteByPeriodAndProcessID( ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("BusinessProcessID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafePeriodID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = ProcessID
            Com.Parameters(1).Value = PeriodID

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

#End Region

#Region "Safe Comment"

    Public Sub SafeCommentPersist(ByRef Con As Connection, ByRef Comment As ISafeCommentModel) Implements IEndOfDayCheckRepository.SafeCommentPersist

        Reset()

        SetConnection(Con)

        SetComments(Comment)

        ExecuteSaveComment()

    End Sub

    Public Sub SafeCommentPersist(ByRef OdbcConnection As clsOasys3DB, ByRef Comment As ISafeCommentModel) Implements IEndOfDayCheckRepository.SafeCommentPersist

        SetComments(Comment)

        Using Com As New Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL NewBankingSafeCommentInsert( ?, ?, ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("BusinessProcessID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafePeriodID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("BagType", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input
            Com.Parameters.Add("Comment", Odbc.OdbcType.NVarChar).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = _Comment.ProcessID
            Com.Parameters(1).Value = _Comment.PeriodID
            Com.Parameters(2).Value = _Comment.BagType
            Com.Parameters(3).Value = _Comment.Comment

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

    Public Sub SafeCommentDeleteExisting(ByRef OdbcConnection As clsOasys3DB, ByVal ProcessID As Integer, ByVal PeriodID As Integer) Implements IEndOfDayCheckRepository.SafeCommentDeleteExisting

        Using Com As New Odbc.OdbcCommand

            Com.Connection = OdbcConnection.Connection

            Com.CommandText = "{CALL NewBankingSafeCommentDeleteByPeriodAndProcessID( ?, ? )}"
            Com.CommandType = CommandType.StoredProcedure

            Com.Parameters.Add("BusinessProcessID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input
            Com.Parameters.Add("SafePeriodID", Odbc.OdbcType.Int).Direction = ParameterDirection.Input

            Com.Parameters(0).Value = ProcessID
            Com.Parameters(1).Value = PeriodID

            Com.Transaction = OdbcConnection.OdbcTransaction

            Com.ExecuteNonQuery()

        End Using

    End Sub

#End Region

    Public Sub PersistSafe(ByRef Con As Connection, ByRef Safe As ISafeModel) Implements IEndOfDayCheckRepository.PersistSafe

        Reset()

        SetConnection(Con)

        SetSafe(Safe)

        ExecuteSaveSafe()

    End Sub

    Public Function ValidateSealNumber(ByVal SealNumber As String) As Integer Implements IEndOfDayCheckRepository.ValidateSealNumber

        Reset()

        SetConnection(Nothing)

        SetSealNumber(SealNumber)

        ExecuteValidateSealNumber()

        Return _ValidatedSealNumberID

    End Function

#End Region

#Region "Private Procedures & Functions"

    Friend Function ConnectionIsNothing() As Boolean

        If _Connection Is Nothing Then Return True

    End Function

    Friend Function CommandIsNothing() As Boolean

        If _Command Is Nothing Then Return True

    End Function

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        If Con Is Nothing Then

            _Connection = New Connection

        Else

            _Connection = Con

        End If

    End Sub

    Friend Overridable Sub SetPeriodID(ByVal Value As Integer)

        _PeriodID = Value

    End Sub

    Friend Overridable Sub ExecuteDataTable()

        _DT = _Command.ExecuteDataTable()

    End Sub

    Friend Overridable Sub ExecuteNonQuery()

        _Command.ExecuteNonQuery()

    End Sub


    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DT = Nothing

        _PeriodID = Nothing
        _Bag = Nothing
        _Comment = Nothing
        _Safe = Nothing
        _SealNumber = Nothing

    End Sub

    Friend Function PeriodIdIsZero() As Boolean

        If _PeriodID = 0 Then Return True

    End Function

    Friend Function BagsIsNothing() As Boolean

        If _Bag Is Nothing Then Return True

    End Function

    Friend Function CommentsIsNothing() As Boolean

        If _Comment Is Nothing Then Return True

    End Function

    Friend Function SafeIsNothing() As Boolean

        If _Safe Is Nothing Then Return True

    End Function

    Friend Function SealNumberIsNothing() As Boolean

        If _SealNumber Is Nothing Then Return True

    End Function

#Region "Get Summary"

    Friend Overridable Sub ExecuteGetSummary()

        ConfigureGetSummaryCommand()
        ExecuteGetSummaryCommand()

    End Sub

    Friend Overridable Sub ConfigureGetSummaryCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If PeriodIdIsZero() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingEndOfDayCheckSummary"
        _Command.AddParameter("@PeriodID", _PeriodID)

    End Sub

    Friend Overridable Sub ExecuteGetSummaryCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteDataTable()

    End Sub

#End Region

#Region "ExecuteGetBankingBagExistInSafe"

    Friend Overridable Sub ExecuteGetBankingBagExistInSafe()

        ConfigureGetPhysicalBankingBagsCommand()
        ExecuteGetPhysicalBankingBagsCommand()

    End Sub

    Friend Overridable Sub ConfigureGetPhysicalBankingBagsCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If PeriodIdIsZero() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingPhysicalBankingBags"
        _Command.AddParameter("@PeriodID", _PeriodID)

    End Sub

    Friend Overridable Sub ExecuteGetPhysicalBankingBagsCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteDataTable()

    End Sub

#End Region

#Region "ExecuteGetPickupBagExistInSafe"

    Friend Overridable Sub ExecuteGetPickupBagExistInSafe()

        ConfigureGetPhysicalPickupBagsCommand()
        ExecuteGetPhysicalPickupBagsCommand()

    End Sub

    Friend Overridable Sub ConfigureGetPhysicalPickupBagsCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If PeriodIdIsZero() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingPhysicalPickupBags"
        _Command.AddParameter("@PeriodID", _PeriodID)

    End Sub

    Friend Overridable Sub ExecuteGetPhysicalPickupBagsCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteDataTable()

    End Sub

#End Region

#Region "Persist Bag"

    Friend Overridable Sub SetBags(ByRef Bags As ISafeBagScannedModel)

        _Bag = Bags

    End Sub

    Friend Overridable Sub ExecuteSaveBag()

        ConfigureSaveBagCommand()
        ExecuteSaveBagCommand()

    End Sub

    Friend Overridable Sub ConfigureSaveBagCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If BagsIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingSafeBagScannedInsert"
        _Command.AddParameter("@BusinessProcessID", _Bag.ProcessID)
        _Command.AddParameter("@SafePeriodID", _Bag.PeriodID)
        _Command.AddParameter("@SafeBagsID", _Bag.SafeBagsID)
        _Command.AddParameter("@SealNumber", _Bag.SealNumber)
        _Command.AddParameter("@BagType", _Bag.BagType)
        _Command.AddParameter("@Comment", _Bag.Comment)

    End Sub

    Friend Overridable Sub ExecuteSaveBagCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

#End Region

#Region "Persist Comment"

    Friend Overridable Sub SetComments(ByRef Comments As ISafeCommentModel)

        _Comment = Comments

    End Sub

    Friend Overridable Sub ExecuteSaveComment()

        ConfigureSaveCommentCommand()
        ExecuteSaveCommentCommand()

    End Sub

    Friend Overridable Sub ConfigureSaveCommentCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If CommentsIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingSafeCommentInsert"
        _Command.AddParameter("@BusinessProcessID", _Comment.ProcessID)
        _Command.AddParameter("@SafePeriodID", _Comment.PeriodID)
        _Command.AddParameter("@BagType", _Comment.BagType)
        _Command.AddParameter("@Comment", _Comment.Comment)

    End Sub

    Friend Overridable Sub ExecuteSaveCommentCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

#End Region

#Region "Persist Safe"

    Friend Overridable Sub SetSafe(ByRef Safe As ISafeModel)

        _Safe = Safe

    End Sub

    Friend Overridable Sub ExecuteSaveSafe()

        ConfigureSaveSafeCommand()
        ExecuteSaveSafeCommand()

    End Sub

    Friend Overridable Sub ConfigureSaveSafeCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If SafeIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingSafeUpdate"
        _Command.AddParameter("@PeriodID", _Safe.PeriodID)
        _Command.AddParameter("@EndOfDayCheckDone", _Safe.EndOfDayCheckDone)
        _Command.AddParameter("@EndOfDayCheckLockedFrom", _Safe.EndOfDayCheckLockedFrom)
        _Command.AddParameter("@EndOfDayCheckLockedTo", _Safe.EndOfDayCheckLockedTo)
        _Command.AddParameter("@EndOfDayCheckManagerID", _Safe.EndOfDayCheckManagerID)
        _Command.AddParameter("@EndOfDayCheckWitnessID", _Safe.EndOfDayCheckWitnessID)

    End Sub

    Friend Overridable Sub ExecuteSaveSafeCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

#End Region

#Region "Validate Seal"

    Friend Overridable Sub SetSealNumber(ByVal SealNumber As String)

        If SealNumber Is Nothing Then SealNumber = String.Empty

        _SealNumber = SealNumber

    End Sub

    Friend Overridable Sub ExecuteValidateSealNumber()

        ConfigureValidateSealNumberCommand()
        ExecuteValidateSealNumberCommand()

        _ValidatedSealNumberID = CType(_Command.GetParameterValue(_Command.ParameterName(1)), Integer)

    End Sub

    Friend Overridable Sub ConfigureValidateSealNumberCommand()

        If ConnectionIsNothing() = True Then Exit Sub

        If SealNumberIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))
        _Command.StoredProcedureName = "NewBankingValidateSeal"
        _Command.AddParameter("@SealNumber", _SealNumber)
        _Command.AddOutputParameter("@SafeBagsID", SqlDbType.Int)

    End Sub

    Friend Overridable Sub ExecuteValidateSealNumberCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

#End Region

#End Region

End Class