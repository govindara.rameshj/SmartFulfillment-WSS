﻿Namespace TpWickes

    Public Class NoSaleBankingRepository
        Implements INoSaleBankingRepository

        Public Function AllowNoSaleBanking() As Boolean? Implements INoSaleBankingRepository.AllowNoSaleBanking

            Return Cts.Oasys.Core.System.Parameter.GetBoolean(2203)

        End Function

    End Class

End Namespace