﻿Public Class SafeCheckReportRepository

    Public Function GetSafeCheckRecordComments(ByVal PeriodID As Integer) As DataTable

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "NewBankingSafeMaintenanceGet"
                com.AddParameter("@PeriodId", PeriodID, SqlDbType.Int)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing

    End Function

    Public Function GetSafeCheckRecordAuthorisers(ByVal PeriodID As Integer) As DataTable

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.StoredProcedureName = "usp_GetSafeMaintenanceAuthorisers"
                com.AddParameter("@PeriodId", PeriodID, SqlDbType.Int)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using

        End Using

        Return Nothing

    End Function

    Public Function GetSafeCheckRecordScannedBankingBags(ByVal PeriodID As Integer) As DataTable

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.StoredProcedureName = "NewBankingSafeCheckReportScannedBankingBags"
                com.AddParameter("@PeriodId", PeriodID, SqlDbType.Int)
                com.AddParameter("@BusinessProcessID", 2, SqlDbType.Int)

                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using

        End Using

        Return Nothing

    End Function

    Public Function GetSafeCheckRecordScannedBankingSummary(ByVal PeriodID As Integer) As DataTable

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.StoredProcedureName = "NewBankingSafeCheckReportScannedBankingSummary"
                com.AddParameter("@PeriodId", PeriodID, SqlDbType.Int)
                com.AddParameter("@BusinessProcessID", 2, SqlDbType.Int)

                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using

        End Using

        Return Nothing

    End Function

    Public Function GetSafeCheckRecordScannedBankingComments(ByVal PeriodID As Integer) As DataTable

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.StoredProcedureName = "NewBankingSafeCheckReportScannedBankingComments"
                com.AddParameter("@PeriodId", PeriodID, SqlDbType.Int)
                com.AddParameter("@BusinessProcessID", 2, SqlDbType.Int)

                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT

            End Using

        End Using

        Return Nothing

    End Function

End Class