﻿Public Class ValidationRepositoryStub
    Implements IValidationRepository

#Region "Stub Code"

    Private _RequirementEnabled As System.Nullable(Of Boolean)
    Private _StoreID As Integer
    Private _eStoreShopID As Integer
    Private _eStoreWarehouseID As Integer

    Friend Sub ConfigureStub(ByVal RequirementEnabled As System.Nullable(Of Boolean), ByVal StoreID As Integer, ByVal eStoreShopID As Integer, ByVal eStoreWarehouseID As Integer)

        _RequirementEnabled = RequirementEnabled
        _StoreID = StoreID + 8000
        _eStoreShopID = eStoreShopID
        _eStoreWarehouseID = eStoreWarehouseID

    End Sub

#End Region

#Region "Interface"

    Public Function RequirementEnabled() As Boolean? Implements IValidationRepository.RequirementEnabled

        Return _RequirementEnabled

    End Function

    Public Function eStoreShopID() As Integer Implements IValidationRepository.eStoreShopID

        Return _eStoreShopID

    End Function

    Public Function eStoreWarhouseID() As Integer Implements IValidationRepository.eStoreWarhouseID

        Return _eStoreWarehouseID

    End Function

    Public Function StoreID() As Integer Implements IValidationRepository.StoreID

        Return _StoreID

    End Function

#End Region

End Class