﻿Public Class BankingSafeMaintenanceValidationRepository
    Implements IValidationRepository

    Public Function RequirementEnabled() As Boolean? Implements IValidationRepository.RequirementEnabled

        Return Parameter.GetBoolean(-3)

    End Function

    Public Function eStoreShopID() As Integer Implements IValidationRepository.eStoreShopID

        Return Parameter.GetInteger(3020)

    End Function

    Public Function eStoreWarhouseID() As Integer Implements IValidationRepository.eStoreWarhouseID

        Return Parameter.GetInteger(3030)

    End Function

    Public Function StoreID() As Integer Implements IValidationRepository.StoreID

        Return Store.GetId4()

    End Function

End Class