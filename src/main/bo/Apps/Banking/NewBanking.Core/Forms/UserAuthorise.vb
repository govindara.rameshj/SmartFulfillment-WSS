'3 login levels (with increasing security) : user, supervisor & manager
'2 password levels                         : normal (systemusers:password), supervisor (systemusers:supervisorpassword)
'in total 6 possible combinations

'operationally "authorisation code" security requires login level of "supervisor" or "manager" & "supervisor password"

Public Enum PasswordMode
    Standard
    Supervisor
End Enum

Public Enum LoginLevel
    User
    Supervisor
    Manager
End Enum

Public Class UserAuthorise

    Private menumPasswordMode As PasswordMode
    Private mintUserID As Integer
    Private mstrReturnPassword As String
    Private mblnReturnManager As Boolean

    Public Sub New(ByVal Level As LoginLevel, ByVal enumPasswordMode As PasswordMode)
        InitializeComponent()

        menumPasswordMode = enumPasswordMode

        Select Case Level
            Case LoginLevel.User : Me.Text = My.Resources.Screen.UserAuthoriseUser
            Case LoginLevel.Supervisor : Me.Text = My.Resources.Screen.UserAuthoriseSupervisor
            Case LoginLevel.Manager : Me.Text = My.Resources.Screen.UserAuthoriseManager
        End Select
    End Sub

    Public ReadOnly Property UserID() As Integer
        Get
            Return mintUserID
        End Get
    End Property

    Public ReadOnly Property IsManager() As Boolean
        Get
            Return mblnReturnManager
        End Get
    End Property

    Private Sub UserAuthorise_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyData
            Case Keys.F10 : uxCancelButton.PerformClick()
        End Select
    End Sub

    Private Sub uxUserIdText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUserIdText.TextChanged
        uxUserNameLabel.Text = String.Empty
        uxPasswordText.EditValue = Nothing
    End Sub

    Private Sub uxUserIdText_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxUserIdText.Validating
        Dim strReturnUserName As String

        If uxUserIdText.EditValue Is Nothing Then Exit Sub 'nothing entered

        'check second user
        strReturnUserName = String.Empty
        mintUserID = CInt(uxUserIdText.EditValue)

        Select Case menumPasswordMode
            Case PasswordMode.Standard
                SecondVerificationCheck(mintUserID, strReturnUserName, mstrReturnPassword, mblnReturnManager)

            Case PasswordMode.Supervisor
                SecondAuthorisationCheck(mintUserID, strReturnUserName, mstrReturnPassword, mblnReturnManager)
        End Select

        If mstrReturnPassword = "" Then
            'user does not exist
            uxUserNameLabel.Text = My.Resources.Screen.UserAuthoriseUserNotRecognised
            uxUserIdText.Focus()
            uxUserIdText.SelectAll()
        Else
            uxUserNameLabel.Text = strReturnUserName
            uxPasswordText.Focus()
        End If
    End Sub

    Private Sub uxPasswordText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPasswordText.TextChanged
        uxPasswordLabel.Text = String.Empty
        uxChangePasswordButton.Visible = False
    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        Me.Close()
    End Sub

    Private Sub uxChangePasswordButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPasswordText.Validating, uxChangePasswordButton.Click
        If uxPasswordText.EditValue Is Nothing Then Exit Sub 'nothing entered

        'check password
        If mstrReturnPassword = uxPasswordText.EditValue.ToString Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            'password expiry check - ignored for now
        Else
            'password failed
            uxPasswordLabel.Text = My.Resources.Screen.UserAuthorisePasswordNotValid
            uxPasswordText.Focus()
            uxPasswordText.SelectAll()
        End If
    End Sub

End Class