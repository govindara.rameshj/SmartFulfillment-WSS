<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.uxUserIdText = New DevExpress.XtraEditors.TextEdit
        Me.uxUserNameLabel = New DevExpress.XtraEditors.LabelControl
        Me.uxPasswordText = New DevExpress.XtraEditors.TextEdit
        Me.uxCancelButton = New System.Windows.Forms.Button
        Me.uxGroup = New DevExpress.XtraEditors.GroupControl
        Me.uxPasswordLabel = New DevExpress.XtraEditors.LabelControl
        Me.uxChangePasswordButton = New System.Windows.Forms.Button
        CType(Me.uxUserIdText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPasswordText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(5, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(71, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Tag = "110"
        Me.Label1.Text = "&User ID"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(5, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(71, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Tag = "112"
        Me.Label3.Text = "&Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxUserIdText
        '
        Me.uxUserIdText.Location = New System.Drawing.Point(82, 27)
        Me.uxUserIdText.Name = "uxUserIdText"
        Me.uxUserIdText.Properties.Mask.EditMask = "\d{0,5}"
        Me.uxUserIdText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxUserIdText.Properties.Mask.ShowPlaceHolders = False
        Me.uxUserIdText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxUserIdText.Properties.MaxLength = 5
        Me.uxUserIdText.Properties.ValidateOnEnterKey = True
        Me.uxUserIdText.Size = New System.Drawing.Size(65, 20)
        Me.uxUserIdText.TabIndex = 1
        '
        'uxUserNameLabel
        '
        Me.uxUserNameLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxUserNameLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxUserNameLabel.Appearance.Options.UseFont = True
        Me.uxUserNameLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxUserNameLabel.Location = New System.Drawing.Point(153, 26)
        Me.uxUserNameLabel.Name = "uxUserNameLabel"
        Me.uxUserNameLabel.Size = New System.Drawing.Size(210, 21)
        Me.uxUserNameLabel.TabIndex = 2
        '
        'uxPasswordText
        '
        Me.uxPasswordText.Location = New System.Drawing.Point(82, 53)
        Me.uxPasswordText.Name = "uxPasswordText"
        Me.uxPasswordText.Properties.Mask.EditMask = "\d{0,5}"
        Me.uxPasswordText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxPasswordText.Properties.Mask.ShowPlaceHolders = False
        Me.uxPasswordText.Properties.MaxLength = 5
        Me.uxPasswordText.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxPasswordText.Properties.ValidateOnEnterKey = True
        Me.uxPasswordText.Size = New System.Drawing.Size(65, 20)
        Me.uxPasswordText.TabIndex = 2
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancelButton.Location = New System.Drawing.Point(290, 101)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(76, 39)
        Me.uxCancelButton.TabIndex = 4
        Me.uxCancelButton.TabStop = False
        Me.uxCancelButton.Text = "F10 Cancel"
        Me.uxCancelButton.UseVisualStyleBackColor = True
        '
        'uxGroup
        '
        Me.uxGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxGroup.Controls.Add(Me.uxPasswordLabel)
        Me.uxGroup.Controls.Add(Me.Label1)
        Me.uxGroup.Controls.Add(Me.uxPasswordText)
        Me.uxGroup.Controls.Add(Me.Label3)
        Me.uxGroup.Controls.Add(Me.uxUserNameLabel)
        Me.uxGroup.Controls.Add(Me.uxUserIdText)
        Me.uxGroup.Location = New System.Drawing.Point(4, 3)
        Me.uxGroup.Name = "uxGroup"
        Me.uxGroup.Size = New System.Drawing.Size(370, 87)
        Me.uxGroup.TabIndex = 0
        Me.uxGroup.Text = "Enter your user ID and password"
        '
        'uxPasswordLabel
        '
        Me.uxPasswordLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPasswordLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxPasswordLabel.Appearance.Options.UseFont = True
        Me.uxPasswordLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxPasswordLabel.Location = New System.Drawing.Point(153, 52)
        Me.uxPasswordLabel.Name = "uxPasswordLabel"
        Me.uxPasswordLabel.Size = New System.Drawing.Size(210, 21)
        Me.uxPasswordLabel.TabIndex = 5
        '
        'uxChangePasswordButton
        '
        Me.uxChangePasswordButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxChangePasswordButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxChangePasswordButton.Location = New System.Drawing.Point(157, 101)
        Me.uxChangePasswordButton.Name = "uxChangePasswordButton"
        Me.uxChangePasswordButton.Size = New System.Drawing.Size(127, 39)
        Me.uxChangePasswordButton.TabIndex = 3
        Me.uxChangePasswordButton.TabStop = False
        Me.uxChangePasswordButton.Text = "F5 Change Password"
        Me.uxChangePasswordButton.UseVisualStyleBackColor = True
        Me.uxChangePasswordButton.Visible = False
        '
        'UserAuthorise
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancelButton
        Me.ClientSize = New System.Drawing.Size(378, 152)
        Me.ControlBox = False
        Me.Controls.Add(Me.uxChangePasswordButton)
        Me.Controls.Add(Me.uxGroup)
        Me.Controls.Add(Me.uxCancelButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "UserAuthorise"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Authentication"
        CType(Me.uxUserIdText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPasswordText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxGroup.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents uxUserIdText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxUserNameLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPasswordText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxCancelButton As System.Windows.Forms.Button
    Friend WithEvents uxGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxPasswordLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxChangePasswordButton As System.Windows.Forms.Button
End Class
