﻿Public Class BankingSafeMaintenanceValidation
    Inherits Validation
    Implements IValidation

    Public Sub New()

        Dim V As IValidationRepository

        V = BankingSafeMaintenanceValidationRepositoryFactory.FactoryGet

        _RequirementEnabled = V.RequirementEnabled
        _StoreID = V.StoreID
        _eStoreShopID = V.eStoreShopID
        _eStoreWarehouseID = V.eStoreWarhouseID

    End Sub

End Class
