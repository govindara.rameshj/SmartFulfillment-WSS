﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("NewBanking.Form.UnitTest")> 
Public Class SafeDenomination
    Inherits Base

#Region "Table Fields And Properties"

    Private _CurrencyID As String
    Private _TenderID As Integer
    Private _DenominationID As Decimal
    Private _TenderText As String
    Private _TenderReadOnly As Boolean
    Private _BankingAmountMultiple As Decimal
    Private _SystemSafe As System.Nullable(Of Decimal)
    Private _MainSafe As System.Nullable(Of Decimal)
    Private _ChangeSafe As System.Nullable(Of Decimal)

    <ColumnMapping("CurrencyID")> Public Property CurrencyID() As String
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As String)
            _CurrencyID = value
        End Set
    End Property

    <ColumnMapping("TenderID")> Public Property TenderID() As Integer
        Get
            Return _TenderID
        End Get
        Set(ByVal value As Integer)
            _TenderID = value
        End Set
    End Property

    <ColumnMapping("DenominationID")> Public Property DenominationID() As Decimal
        Get
            Return _DenominationID
        End Get
        Set(ByVal value As Decimal)
            _DenominationID = value
        End Set
    End Property

    <ColumnMapping("TenderText")> Public Property TenderText() As String
        Get
            Return _TenderText
        End Get
        Set(ByVal value As String)
            _TenderText = value
        End Set
    End Property

    <ColumnMapping("TenderReadOnly")> Public Property TenderReadOnly() As Boolean
        Get
            Return _TenderReadOnly
        End Get
        Set(ByVal value As Boolean)
            _TenderReadOnly = value
        End Set
    End Property

    <ColumnMapping("BankingAmountMultiple")> Public Property BankingAmountMultiple() As Decimal
        Get
            Return _BankingAmountMultiple
        End Get
        Set(ByVal value As Decimal)
            _BankingAmountMultiple = value
        End Set
    End Property

    <ColumnMapping("SystemSafe")> Public Property SystemSafe() As System.Nullable(Of Decimal)
        Get
            Return _SystemSafe
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _SystemSafe = value
        End Set
    End Property

    <ColumnMapping("MainSafe")> Public Property MainSafe() As System.Nullable(Of Decimal)
        Get
            Return _MainSafe
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _MainSafe = value
        End Set
    End Property

    <ColumnMapping("ChangeSafe")> Public Property ChangeSafe() As System.Nullable(Of Decimal)
        Get
            Return _ChangeSafe
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _ChangeSafe = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class SafeDenominationCollection
    Inherits BaseCollection(Of SafeDenomination)

#Region "Methods"

    Public Sub LoadCashTender(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSafeCashTender")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Sub LoadAllTenders(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSafeAllTenders")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region

End Class