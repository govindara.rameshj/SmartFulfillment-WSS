﻿Public Class AutoPickupForClickAndCollect
    Public Sub CompletePickupForClickAndCollect(ByVal periodID As Integer, ByVal userId As Integer)
        Dim connectionString As String = SharedFunctions.GetConnectionString()

        Using conn As New SqlConnection(connectionString)
            conn.Open()
            Dim cmd As New SqlCommand("ClickAndCollectCashierAutoPickup", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@periodId", periodID)
            cmd.Parameters.AddWithValue("@userId", userId)
            cmd.ExecuteNonQuery()
            conn.Close()
        End Using
    End Sub
End Class
