﻿''' <summary>
''' Provides list of unfloated cashiers/design consultants with and without pickups
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class UnFloatedPickupList
    Inherits Base

#Region "Table Fields And Properties"

    Private _PickupID As System.Nullable(Of Integer)
    Private _PickupSealNumber As String
    Private _PickupComment As String
    Private _CashierID As Integer
    Private _CashierUserName As String
    Private _CashierEmployeeCode As String
    Private _LoggedOnTillId As String

    <ColumnMapping("PickupID")> Public Property PickupID() As System.Nullable(Of Integer)
        Get
            Return _PickupID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupID = value
        End Set
    End Property

    <ColumnMapping("PickupSealNumber")> Public Property PickupSealNumber() As String
        Get
            Return _PickupSealNumber
        End Get
        Set(ByVal value As String)
            _PickupSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupComment")> Public Property PickupComment() As String
        Get
            Return _PickupComment
        End Get
        Set(ByVal value As String)
            _PickupComment = value
        End Set
    End Property

    <ColumnMapping("CashierID")> Public Property CashierID() As Integer
        Get
            Return _CashierID
        End Get
        Set(ByVal value As Integer)
            _CashierID = value
        End Set
    End Property

    <ColumnMapping("CashierUserName")> Public Property CashierUserName() As String
        Get
            Return _CashierUserName
        End Get
        Set(ByVal value As String)
            _CashierUserName = value
        End Set
    End Property

    <ColumnMapping("CashierEmployeeCode")> Public Property CashierEmployeeCode() As String
        Get
            Return _CashierEmployeeCode
        End Get
        Set(ByVal value As String)
            _CashierEmployeeCode = value
        End Set
    End Property

    <ColumnMapping("LoggedOnTillId")> Public Property LoggedOnTillId() As String
        Get
            Return _LoggedOnTillId
        End Get
        Set(ByVal value As String)
            _LoggedOnTillId = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Function IsLoggedOn() As Boolean

        If LoggedOnTillId IsNot Nothing AndAlso LoggedOnTillId.Length > 0 Then
            IsLoggedOn = String.Compare(LoggedOnTillId, "99") <> 0
        End If
    End Function
#End Region

End Class

Public Class UnFloatedPickupListCollection
    Inherits BaseCollection(Of UnFloatedPickupList)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingUnFloatedPickupList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function SelectedCashier(ByVal intCashierID As Integer) As UnFloatedPickupList
        SelectedCashier = Nothing
        For Each obj As UnFloatedPickupList In Me.Items
            If obj.CashierID = intCashierID Then
                SelectedCashier = obj
                Exit Function
            End If
        Next
    End Function

    Public Function CashierIsLoggedOn(ByVal CashierID As Integer) As Boolean
        Dim Cashier As UnFloatedPickupList = Me.SelectedCashier(CashierID)

        Return Cashier.IsLoggedOn
    End Function
#End Region

End Class