﻿''' <summary>
''' Provides starting float information
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class FloatList
    Inherits Base

#Region "Table Fields And Properties"

    Private _FloatID As Integer
    Private _FloatSealNumber As String
    Private _FloatValue As Decimal
    Private _FloatCreatedFromUserName As String
    Private _FloatCreatedFromPickupBagID As System.Nullable(Of Integer)
    Private _AssignedToUserName As String
    Private _AssignedToUserID As System.Nullable(Of Integer)
    Private _SaleTaken As Boolean
    Private _FloatChecked As Boolean
    Private _Comments As String

    <ColumnMapping("FloatID")> _
    Public Property FloatID() As Integer
        Get
            Return _FloatID
        End Get
        Set(ByVal value As Integer)
            _FloatID = value
        End Set
    End Property

    <ColumnMapping("FloatSealNumber")> _
    Public Property FloatSealNumber() As String
        Get
            Return _FloatSealNumber
        End Get
        Set(ByVal value As String)
            _FloatSealNumber = value
        End Set
    End Property

    <ColumnMapping("FloatValue")> _
    Public Property FloatValue() As Decimal
        Get
            Return _FloatValue
        End Get
        Set(ByVal value As Decimal)
            _FloatValue = value
        End Set
    End Property

    <ColumnMapping("FloatCreatedFromUserName")> _
    Public Property FloatCreatedFromUserName() As String
        Get
            Return _FloatCreatedFromUserName
        End Get
        Set(ByVal value As String)
            _FloatCreatedFromUserName = value
        End Set
    End Property

    <ColumnMapping("FloatCreatedFromPickupBagID")> _
    Public Property FloatCreatedFromPickupBagID() As System.Nullable(Of Integer)
        Get
            Return _FloatCreatedFromPickupBagID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _FloatCreatedFromPickupBagID = value
        End Set
    End Property

    <ColumnMapping("AssignedToUserName")> _
    Public Property AssignedToUserName() As String
        Get
            Return _AssignedToUserName
        End Get
        Set(ByVal value As String)
            _AssignedToUserName = value
        End Set
    End Property

    <ColumnMapping("AssignedToUserID")> _
    Public Property AssignedToUserID() As System.Nullable(Of Integer)
        Get
            Return _AssignedToUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _AssignedToUserID = value
        End Set
    End Property

    <ColumnMapping("SaleTaken")> _
    Public Property SaleTaken() As Boolean
        Get
            Return _SaleTaken
        End Get
        Set(ByVal value As Boolean)
            _SaleTaken = value
        End Set
    End Property

    <ColumnMapping("FloatChecked")> _
    Public Property FloatChecked() As Boolean
        Get
            Return _FloatChecked
        End Get
        Set(ByVal value As Boolean)
            _FloatChecked = value
        End Set
    End Property

    <ColumnMapping("Comments")> _
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class FloatListCollection
    Inherits BaseCollection(Of FloatList)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function SelectedFloat(ByVal intFloatID As Integer) As FloatList
        SelectedFloat = Nothing
        For Each FMD As FloatList In Me.Items
            If FMD.FloatID = intFloatID Then
                SelectedFloat = FMD
                Exit Function
            End If
        Next
    End Function

#End Region

End Class
