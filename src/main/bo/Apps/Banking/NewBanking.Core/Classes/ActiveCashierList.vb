﻿''' <summary>
''' List of cashiers that have taken sales
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class ActiveCashierList
    Inherits Base

#Region "Table Fields And Properties"

    Private _CashierID As Integer
    Private _CashierUserName As String
    Private _CashierEmployeeCode As String
    Private _SystemSales As System.Nullable(Of Decimal)

    Private _TotalPickups As System.Nullable(Of Decimal)
    Private _StartFloatValue As System.Nullable(Of Decimal)
    Private _NewFloatValue As System.Nullable(Of Decimal)

    Private _EndOfDayPickupList As New PickupCollection
    Private _CashDropsPickupList As New PickupCollection

    <ColumnMapping("CashierID")> Public Property CashierID() As Integer
        Get
            Return _CashierID
        End Get
        Set(ByVal value As Integer)
            _CashierID = value
        End Set
    End Property

    <ColumnMapping("CashierUserName")> Public Property CashierUserName() As String
        Get
            Return _CashierUserName
        End Get
        Set(ByVal value As String)
            _CashierUserName = value
        End Set
    End Property

    <ColumnMapping("CashierEmployeeCode")> Public Property CashierEmployeeCode() As String
        Get
            Return _CashierEmployeeCode
        End Get
        Set(ByVal value As String)
            _CashierEmployeeCode = value
        End Set
    End Property

    <ColumnMapping("SystemSales")> Public Property SystemSales() As System.Nullable(Of Decimal)
        Get
            Return _SystemSales
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _SystemSales = value
        End Set
    End Property

    <ColumnMapping("TotalPickups")> Public Property TotalPickups() As System.Nullable(Of Decimal)
        Get
            Return _TotalPickups
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _TotalPickups = value
        End Set
    End Property

    <ColumnMapping("StartFloatValue")> Public Property StartFloatValue() As System.Nullable(Of Decimal)
        Get
            Return _StartFloatValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _StartFloatValue = value
        End Set
    End Property

    <ColumnMapping("NewFloatValue")> Public Property NewFloatValue() As System.Nullable(Of Decimal)
        Get
            Return _NewFloatValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _NewFloatValue = value
        End Set
    End Property

    Public Property EndOfDayPickupList() As PickupCollection
        Get
            Return _EndOfDayPickupList
        End Get
        Set(ByVal value As PickupCollection)
            _EndOfDayPickupList = value
        End Set
    End Property

    Public Property CashDropsPickupList() As PickupCollection
        Get
            Return _CashDropsPickupList
        End Get
        Set(ByVal value As PickupCollection)
            _CashDropsPickupList = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class ActiveCashierListCollection
    Inherits BaseCollection(Of ActiveCashierList)

#Region "Methods"

    Public Sub LoadAllCashiers(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingActiveCashierList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@CashierID", System.DBNull.Value, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)

        'loop around cashier list and retrieve end of day pickups
        For Each obj As ActiveCashierList In Me.Items
            obj.EndOfDayPickupList.LoadEndOfDayPickupList(intPeriodID, obj.CashierID)
        Next

        'loop around cashier list and retrieve cash drop pickups
        For Each obj As ActiveCashierList In Me.Items
            obj.CashDropsPickupList.LoadCashDropPickupList(intPeriodID, obj.CashierID)
        Next
    End Sub

    Public Sub LoadCashier(ByVal intPeriodID As Integer, ByVal intCashierID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingActiveCashierList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@CashierID", intCashierID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)

        'loop around cashier list and retrieve end of day pickups
        For Each obj As ActiveCashierList In Me.Items
            obj.EndOfDayPickupList.LoadEndOfDayPickupList(intPeriodID, obj.CashierID)
        Next

        'loop around cashier list and retrieve cash drop pickups
        For Each obj As ActiveCashierList In Me.Items
            obj.CashDropsPickupList.LoadCashDropPickupList(intPeriodID, obj.CashierID)
        Next
    End Sub

    Public Function TenderDenominationValue(ByVal intCashierID As Integer, ByVal intPickupID As Integer, ByVal intTenderID As Integer, ByVal decDenominationID As Decimal) As System.Nullable(Of Decimal)
        Dim decEndOfDayValue As System.Nullable(Of Decimal)
        Dim decCashDropValue As System.Nullable(Of Decimal)

        For Each obj As ActiveCashierList In Me.Items
            If obj.CashierID = intCashierID Then
                'loop around e.o.d pickup list
                decEndOfDayValue = obj.EndOfDayPickupList.TenderDenominationValue(intPickupID, intTenderID, decDenominationID)
                'loop around cash drop list
                decCashDropValue = obj.CashDropsPickupList.TenderDenominationValue(intPickupID, intTenderID, decDenominationID)
            End If
        Next
        If decEndOfDayValue.HasValue = True Then TenderDenominationValue = decEndOfDayValue
        If decCashDropValue.HasValue = True Then TenderDenominationValue = decCashDropValue

    End Function

    Public Function GetBagComment(ByVal BagId As Integer) As String
        Dim Bag As Pickup = GetPickupBagOrCashDrop(BagId)

        GetBagComment = ""
        If Bag IsNot Nothing Then
            GetBagComment = Bag.PickupComment
        End If
    End Function

    Public Overridable Function GetBag(ByVal BagId As Integer) As Pickup
        GetBag = GetPickupBagOrCashDrop(BagId)
    End Function
#End Region

#Region "Internals"

    Friend Overridable Function GetPickupBagOrCashDrop(ByVal BagId As Integer) As Pickup

        GetPickupBagOrCashDrop = GetPickupBag(BagId)
        If GetPickupBagOrCashDrop Is Nothing Then
            GetPickupBagOrCashDrop = GetCashDrop(BagId)
        End If
    End Function

    Friend Overridable Function GetPickupBag(ByVal BagId As Integer) As Pickup

        GetPickupBag = Nothing
        For Each CashierList As ActiveCashierList In Me.Items
            With CashierList
                If .EndOfDayPickupList IsNot Nothing Then
                    For Each PickupBag As Pickup In .EndOfDayPickupList
                        With PickupBag
                            If .PickupID = BagId Then
                                GetPickupBag = PickupBag
                                Exit For
                            End If
                        End With
                    Next
                End If
            End With
        Next
    End Function

    Friend Overridable Function GetCashDrop(ByVal BagId As Integer) As Pickup

        GetCashDrop = Nothing
        For Each CashierList As ActiveCashierList In Me.Items
            With CashierList
                If .CashDropsPickupList IsNot Nothing Then
                    For Each CashDrop As Pickup In .CashDropsPickupList
                        With CashDrop
                            If .PickupID = BagId Then
                                GetCashDrop = CashDrop
                                Exit For
                            End If
                        End With
                    Next
                End If
            End With
        Next
    End Function
#End Region
End Class