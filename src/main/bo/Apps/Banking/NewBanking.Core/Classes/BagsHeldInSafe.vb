﻿''' <summary>
''' List of bags (float, cash drop, e.o.d day pickup, banking) held in safe
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class BagsHeldInSafe
    Inherits Base

#Region "Table Fields And Properties"

    Private _BagID As Integer
    Private _BagType As String
    Private _SealNumber As String
    Private _BagValue As Decimal
    Private _Comments As String

    <ColumnMapping("BagID")> Public Property BagID() As Integer
        Get
            Return _BagID
        End Get
        Set(ByVal value As Integer)
            _BagID = value
        End Set
    End Property

    <ColumnMapping("BagType")> Public Property BagType() As String
        Get
            Return _BagType
        End Get
        Set(ByVal value As String)
            _BagType = value
        End Set
    End Property

    <ColumnMapping("SealNumber")> Public Property SealNumber() As String
        Get
            Return _SealNumber
        End Get
        Set(ByVal value As String)
            _SealNumber = value
        End Set
    End Property

    <ColumnMapping("BagValue")> Public Property BagValue() As Decimal
        Get
            Return _BagValue
        End Get
        Set(ByVal value As Decimal)
            _BagValue = value
        End Set
    End Property
    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class BagsHeldInSafeCollection
    Inherits BaseCollection(Of BagsHeldInSafe)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSafeMaintenanceBagsHeld")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region
End Class