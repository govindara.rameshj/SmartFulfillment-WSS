﻿''' <summary>
''' List of denomination values for float
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class FloatBag
    Inherits Base

#Region "Table Fields And Properties"

    Private _TenderID As Integer
    Private _DenominationID As Decimal
    Private _FloatValue As System.Nullable(Of Decimal)

    <ColumnMapping("TenderID")> Public Property TenderID() As Integer
        Get
            Return _TenderID
        End Get
        Set(ByVal value As Integer)
            _TenderID = value
        End Set
    End Property

    <ColumnMapping("DenominationID")> Public Property DenominationID() As Decimal
        Get
            Return _DenominationID
        End Get
        Set(ByVal value As Decimal)
            _DenominationID = value
        End Set
    End Property

    <ColumnMapping("FloatValue")> Public Property FloatValue() As System.Nullable(Of Decimal)
        Get
            Return _FloatValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _FloatValue = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class FloatBagCollection
    Inherits BaseCollection(Of FloatBag)

#Region "Methods"

    Public Sub LoadData(ByVal intFloatID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatDenomination")
                com.AddParameter("@FloatID", intFloatID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function DenominationValue(ByVal intDenomID As Decimal) As System.Nullable(Of Decimal)

        For Each obj As FloatBag In Me.Items
            If obj.DenominationID = intDenomID Then DenominationValue = obj.FloatValue
        Next
    End Function

#End Region

End Class
