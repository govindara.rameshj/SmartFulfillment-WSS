﻿''' <summary>
''' Decides if banking is allowed for this banking period
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class BankingValidation

#Region "Table Fields And Properties"

    Private _FloatedCashierNoSales As Boolean
    Private _FloatedCashierSalesNoPickupBag As Boolean
    Private _UnFloatedCashierSalesNoPickupBag As Boolean

    <ColumnMapping("FloatedCashierNoSales")> Public Property FloatedCashierNoSales() As Boolean
        Get
            Return _FloatedCashierNoSales
        End Get
        Set(ByVal value As Boolean)
            _FloatedCashierNoSales = value
        End Set
    End Property

    <ColumnMapping("FloatedCashierSalesNoPickupBag")> Public Property FloatedCashierSalesNoPickupBag() As Boolean
        Get
            Return _FloatedCashierSalesNoPickupBag
        End Get
        Set(ByVal value As Boolean)
            _FloatedCashierSalesNoPickupBag = value
        End Set
    End Property

    <ColumnMapping("UnFloatedCashierSalesNoPickupBag")> Public Property UnFloatedCashierSalesNoPickupBag() As Boolean
        Get
            Return _UnFloatedCashierSalesNoPickupBag
        End Get
        Set(ByVal value As Boolean)
            _UnFloatedCashierSalesNoPickupBag = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New(ByVal intPeriodID As Integer)
        Dim dt As DataTable
        Dim dr As DataRow

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingBankingValidation")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        dr = dt.Rows(0)
        _FloatedCashierNoSales = CType(dr.Item("FloatedCashierNoSales"), Boolean)
        _FloatedCashierSalesNoPickupBag = CType(dr.Item("FloatedCashierSalesNoPickupBag"), Boolean)
        _UnFloatedCashierSalesNoPickupBag = CType(dr.Item("UnFloatedCashierSalesNoPickupBag"), Boolean)
    End Sub

    Public Shared Function ClickAndCollectExistsForPeriod(ByVal PeriodID As Integer) As Boolean
        Dim strSql As String = "SELECT 1 FROM CashBalCashier WHERE CashierID = 499 and PeriodID = " & PeriodID
        Dim connectionString As String = SharedFunctions.GetConnectionString()
        Dim salesExists As Boolean

        Using conn As New SqlConnection(connectionString)
            Dim cmd As New SqlCommand(strSql, conn)
            conn.Open()
            If Convert.ToInt32(cmd.ExecuteScalar()) = 1 Then
                salesExists = True
            Else
                salesExists = False
            End If
            conn.Close()
        End Using
        Return salesExists
    End Function


#End Region

End Class
