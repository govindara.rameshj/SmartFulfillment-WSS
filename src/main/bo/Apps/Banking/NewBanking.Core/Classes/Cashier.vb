﻿''' <summary>
''' Provides a list of cashiers or design consultants
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class Cashier
    Inherits Base

#Region "Table Fields And Properties"

    Private _UserId As Integer
    Private _Employee As String

    <ColumnMapping("UserId")> Public Property UserId() As Integer
        Get
            Return _UserId
        End Get
        Set(ByVal value As Integer)
            _UserId = value
        End Set
    End Property

    <ColumnMapping("Employee")> Public Property Employee() As String
        Get
            Return _Employee
        End Get
        Set(ByVal value As String)
            _Employee = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class CashierCollection
    Inherits BaseCollection(Of Cashier)

#Region "Methods"

    Public Sub LoadDataAssignFloat(ByVal intPeriodID As Integer)
        Dim dt As DataTable
        Dim choiceOfCashier As Integer

        choiceOfCashier = Parameter.GetInteger(52)

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingUnFloatedCashier")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@ChoiceOfCashier", choiceOfCashier, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Sub LoadDataCashDrop()
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingCashier")
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Sub LoadDataCashDrop(ByVal BankingPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingCashier")
                com.AddParameter("@PeriodID", BankingPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region

End Class