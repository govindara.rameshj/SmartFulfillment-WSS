﻿''' <summary>
''' Provides list of cashiers for spot check report
''' </summary>
Public Class SpotCheckListCollection
    Inherits FloatedPickupListCollection

    Public Overrides Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSpotCheckList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

End Class
