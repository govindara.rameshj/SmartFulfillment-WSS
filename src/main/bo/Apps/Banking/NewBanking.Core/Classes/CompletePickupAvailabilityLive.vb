﻿Public Class CompletePickupAvailabilityLive
    Implements ICompletePickupAvailability

    Public Function IsAvailable(ByRef Selected As FloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean Implements ICompletePickupAvailability.IsAvailable

        IsAvailable = CurrentAvailablity
        'pickup already assigned
        If Selected.PickupID.HasValue = True Then
            IsAvailable = False
        End If
        'no sale taken
        If Selected.SaleTaken = False Then
            IsAvailable = False
        End If
    End Function

    Public Function IsAvailable(ByRef Selected As UnFloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean Implements ICompletePickupAvailability.IsAvailable

        IsAvailable = CurrentAvailablity

        'No manual pickup for WebOrder virtual cashier 
        ' or if pickup is already assigned
        If Selected.CashierID = GlobalConstants.WebOrderCashierID OrElse Selected.PickupID.HasValue = True Then
            IsAvailable = False
        End If
    End Function
End Class
