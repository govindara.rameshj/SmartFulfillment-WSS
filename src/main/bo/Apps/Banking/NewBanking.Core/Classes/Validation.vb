﻿Public Class Validation
    Implements IValidation

    Protected _RequirementEnabled As System.Nullable(Of Boolean)
    Protected _StoreID As Integer
    Protected _eStoreShopID As Integer
    Protected _eStoreWarehouseID As Integer

#Region "Properties"

    Public ReadOnly Property RequirementEnabled() As Boolean? Implements IValidation.RequirementEnabled
        Get
            Return _RequirementEnabled
        End Get
    End Property

    Public ReadOnly Property StoreID() As Integer Implements IValidation.StoreID
        Get
            Return _StoreID
        End Get
    End Property

    Public ReadOnly Property eStoreShopID() As Integer Implements IValidation.eStoreShopID
        Get
            Return _eStoreShopID
        End Get
    End Property

    Public ReadOnly Property eStoreWarehouseID() As Integer Implements IValidation.eStoreWarehouseID
        Get
            Return _eStoreWarehouseID
        End Get
    End Property

#End Region

    Public Function PerformCheck() As Boolean Implements IValidation.PerformCheck

        Return Not EStore()

    End Function

#Region "Private  Procedures And Functions"

    Private Function EStore() As Boolean

        'requirement parameter does not exists
        If _RequirementEnabled.HasValue = False Then Return False

        'requirement parameter exist and false
        If _RequirementEnabled = False Then Return False

        'no store id
        If _StoreID = 0 + 8000 Then Return False

        'estore shop or warehouse parameters does not exists
        If _eStoreShopID = 0 And _eStoreWarehouseID = 0 Then Return False

        'store id match estore shop or warehouse
        If _StoreID = _eStoreShopID OrElse _StoreID = _eStoreWarehouseID Then Return True

        'NOT estore shop or warehouse
        Return False

    End Function

#End Region

End Class