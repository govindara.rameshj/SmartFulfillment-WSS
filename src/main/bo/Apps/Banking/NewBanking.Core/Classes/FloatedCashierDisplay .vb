﻿''' <summary>
''' Provides list of floated cashiers with and without pickups
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class FloatedCashierDisplay
    Inherits Base

#Region "Table Fields And Properties"

    Private _StartFloatChecked As Boolean
    Private _StartFloatValue As Decimal
    Private _StartFloatSealNumber As String
    Private _StartFloatAssignedToUserID As System.Nullable(Of Integer)
    Private _StartFloatAssignedToUserName As String
    Private _StartFloatAssignedByUserID As System.Nullable(Of Integer)
    Private _StartFloatAssignedByUserName As String
    Private _TillVariance As System.Nullable(Of Decimal)
    Private _NewFloatSealNumber As String
    Private _PickupSealNumber As String
    Private _PickupAssignedByUserID As System.Nullable(Of Integer)
    Private _PickupAssignedByUserName As String
    Private _PickupAssignedBySecondUserID As System.Nullable(Of Integer)
    Private _PickupAssignedBySecondUserName As String
    Private _EODBagCheck As String = ""  'no data access
    Private _Comments As String = ""     'no data access

    <ColumnMapping("StartFloatChecked")> Public Property StartFloatChecked() As Boolean
        Get
            Return _StartFloatChecked
        End Get
        Set(ByVal value As Boolean)
            _StartFloatChecked = value
        End Set
    End Property

    <ColumnMapping("StartFloatValue")> Public Property StartFloatValue() As Decimal
        Get
            Return _StartFloatValue
        End Get
        Set(ByVal value As Decimal)
            _StartFloatValue = value
        End Set
    End Property

    <ColumnMapping("StartFloatSealNumber")> Public Property StartFloatSealNumber() As String
        Get
            Return _StartFloatSealNumber
        End Get
        Set(ByVal value As String)
            _StartFloatSealNumber = value
        End Set
    End Property

    <ColumnMapping("StartFloatAssignedToUserID")> Public Property StartFloatAssignedToUserID() As System.Nullable(Of Integer)
        Get
            Return _StartFloatAssignedToUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _StartFloatAssignedToUserID = value
        End Set
    End Property

    <ColumnMapping("StartFloatAssignedToUserName")> Public Property StartFloatAssignedToUserName() As String
        Get
            Return _StartFloatAssignedToUserName
        End Get
        Set(ByVal value As String)
            _StartFloatAssignedToUserName = value
        End Set
    End Property

    <ColumnMapping("StartFloatAssignedByUserID")> Public Property StartFloatAssignedByUserID() As System.Nullable(Of Integer)
        Get
            Return _StartFloatAssignedByUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _StartFloatAssignedByUserID = value
        End Set
    End Property

    <ColumnMapping("StartFloatAssignedByUserName")> Public Property StartFloatAssignedByUserName() As String
        Get
            Return _StartFloatAssignedByUserName
        End Get
        Set(ByVal value As String)
            _StartFloatAssignedByUserName = value
        End Set
    End Property

    <ColumnMapping("TillVariance")> Public Property TillVariance() As System.Nullable(Of Decimal)
        Get
            Return _TillVariance
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _TillVariance = value
        End Set
    End Property

    <ColumnMapping("NewFloatSealNumber")> Public Property NewFloatSealNumber() As String
        Get
            Return _NewFloatSealNumber
        End Get
        Set(ByVal value As String)
            _NewFloatSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupSealNumber")> Public Property PickupSealNumber() As String
        Get
            Return _PickupSealNumber
        End Get
        Set(ByVal value As String)
            _PickupSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedByUserID")> Public Property PickupAssignedByUserID() As System.Nullable(Of Integer)
        Get
            Return _PickupAssignedByUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupAssignedByUserID = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedByUserName")> Public Property PickupAssignedByUserName() As String
        Get
            Return _PickupAssignedByUserName
        End Get
        Set(ByVal value As String)
            _PickupAssignedByUserName = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedBySecondUserID")> Public Property PickupAssignedBySecondUserID() As System.Nullable(Of Integer)
        Get
            Return _PickupAssignedBySecondUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupAssignedBySecondUserID = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedBySecondUserName")> Public Property PickupAssignedBySecondUserName() As String
        Get
            Return _PickupAssignedBySecondUserName
        End Get
        Set(ByVal value As String)
            _PickupAssignedBySecondUserName = value
        End Set
    End Property

    <ColumnMapping("EODBagCheck")> Public Property EODBagCheck() As String
        Get
            Return _EODBagCheck
        End Get
        Set(ByVal value As String)
            _EODBagCheck = value
        End Set
    End Property

    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class FloatedCashierDisplayCollection
    Inherits BaseCollection(Of FloatedCashierDisplay)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatedCashierDisplay")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region

End Class
