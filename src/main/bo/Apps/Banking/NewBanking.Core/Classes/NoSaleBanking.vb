﻿Namespace TpWickes

    Public Class NoSaleBanking
        Implements INoSaleBanking

        Public Function AllowNoSaleBanking() As Boolean Implements INoSaleBanking.AllowNoSaleBanking

            Dim Repository As INoSaleBankingRepository

            Repository = NoSaleBankingRepositoryFactory.FactoryGet

            If Repository.AllowNoSaleBanking.HasValue = False Then Return False

            Return Repository.AllowNoSaleBanking.Value

        End Function

    End Class

End Namespace