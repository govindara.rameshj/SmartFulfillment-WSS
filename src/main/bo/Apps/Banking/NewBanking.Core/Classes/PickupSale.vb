﻿''' <summary>
''' Pickup broken down by tender and denomination
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class PickupSale
    Inherits Base

#Region "Table Fields And Properties"

    Private _TenderID As Integer
    Private _DenominationID As Decimal
    Private _PickupValue As System.Nullable(Of Decimal)

    <ColumnMapping("TenderID")> Public Property TenderID() As Integer
        Get
            Return _TenderID
        End Get
        Set(ByVal value As Integer)
            _TenderID = value
        End Set
    End Property

    <ColumnMapping("DenominationID")> Public Property DenominationID() As Decimal
        Get
            Return _DenominationID
        End Get
        Set(ByVal value As Decimal)
            _DenominationID = value
        End Set
    End Property

    <ColumnMapping("PickupValue")> Public Property PickupValue() As System.Nullable(Of Decimal)
        Get
            Return _PickupValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _PickupValue = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class PickupSaleCollection
    Inherits BaseCollection(Of PickupSale)

#Region "Methods"

    Public Sub LoadPickupSalesByTenderDenomination(ByVal intPickupID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingPickupSale")
                com.AddParameter("@PickupID", intPickupID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function TenderDenominationValue(ByVal intTenderID As Integer, ByVal decDenominationID As Decimal) As System.Nullable(Of Decimal)
        For Each obj As PickupSale In Me.Items
            If obj.TenderID = intTenderID AndAlso obj.DenominationID = decDenominationID Then TenderDenominationValue = obj.PickupValue
        Next
    End Function

#End Region

End Class
