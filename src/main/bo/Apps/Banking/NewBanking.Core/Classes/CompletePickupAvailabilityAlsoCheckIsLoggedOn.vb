﻿Public Class CompletePickupAvailabilityAlsoCheckIsLoggedOn
    Implements ICompletePickupAvailability

    Public Function IsAvailable(ByRef Selected As FloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean Implements ICompletePickupAvailability.IsAvailable

        IsAvailable = CurrentAvailablity
        If Selected.PickupID.HasValue = True Then
            IsAvailable = False
        End If
        If Selected.SaleTaken = False Then
            IsAvailable = False
        End If
        If Selected.IsLoggedOn And CheckCashierIsNotLoggedOnToTill Then
            IsAvailable = False
        End If
    End Function

    Public Function IsAvailable(ByRef Selected As UnFloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean Implements ICompletePickupAvailability.IsAvailable

        IsAvailable = CurrentAvailablity

        'No manual pickup for WebOrder virtual cashier 
        ' or if pickup is already assigned
        If Selected.CashierID = GlobalConstants.WebOrderCashierID OrElse Selected.PickupID.HasValue = True Then
            IsAvailable = False
        End If
        If Selected.IsLoggedOn And CheckCashierIsNotLoggedOnToTill Then
            IsAvailable = False
        End If
    End Function
End Class
