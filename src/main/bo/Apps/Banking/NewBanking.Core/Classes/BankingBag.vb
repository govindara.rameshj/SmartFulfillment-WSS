﻿''' <summary>
''' List of banking bags awaiting collection
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class BankingBag
    Inherits Base

#Region "Table Fields And Properties"

    Private _BagID As Integer
    Private _BagPeriodID As Integer
    Private _BagPeriodDate As Date
    Private _BagSealNumber As String
    Private _BagValue As Decimal
    Private _BagComment As String

    <ColumnMapping("BagID")> Public Property BagID() As Integer
        Get
            Return _BagID
        End Get
        Set(ByVal value As Integer)
            _BagID = value
        End Set
    End Property

    <ColumnMapping("BagPeriodID")> Public Property BagPeriodID() As Integer
        Get
            Return _BagPeriodID
        End Get
        Set(ByVal value As Integer)
            _BagPeriodID = value
        End Set
    End Property

    <ColumnMapping("BagPeriodDate")> Public Property BagPeriodDate() As Date
        Get
            Return _BagPeriodDate
        End Get
        Set(ByVal value As Date)
            _BagPeriodDate = value
        End Set
    End Property

    <ColumnMapping("BagSealNumber")> Public Property BagSealNumber() As String
        Get
            Return _BagSealNumber
        End Get
        Set(ByVal value As String)
            _BagSealNumber = value
        End Set
    End Property

    <ColumnMapping("BagValue")> Public Property BagValue() As Decimal
        Get
            Return _BagValue
        End Get
        Set(ByVal value As Decimal)
            _BagValue = value
        End Set
    End Property

    <ColumnMapping("BagComment")> Public Property BagComment() As String
        Get
            Return _BagComment
        End Get
        Set(ByVal value As String)
            _BagComment = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class BankingBagCollection
    Inherits BaseCollection(Of BankingBag)

#Region "Methods"

    Public Sub LoadData()
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingBagCollection")
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region

End Class