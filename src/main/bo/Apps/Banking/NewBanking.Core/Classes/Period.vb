﻿Public Class Period
    Inherits Base
    Implements ISafe

#Region "Table Fields And Properties"

    Private _PeriodId As Integer
    Private _PeriodDate As Date
    Private _BankingComplete As Boolean
    Private _NextDateToBeBanked As Boolean
    Private _LastDateBanked As Boolean
    Private _BankingBagCollectionRequired As Boolean
    Private _Description As String
    Private _SafeMaintenanceDone As Boolean
    Private _EndOfDayCheckDone As System.Nullable(Of Boolean)

    <ColumnMapping("PeriodID")> Public Property PeriodID() As Integer Implements ISafe.PeriodID
        Get
            Return _PeriodId
        End Get
        Set(ByVal value As Integer)
            _PeriodId = value
        End Set
    End Property

    <ColumnMapping("PeriodDate")> Public Property PeriodDate() As Date Implements ISafe.PeriodDate
        Get
            Return _PeriodDate
        End Get
        Set(ByVal value As Date)
            _PeriodDate = value
        End Set
    End Property

    <ColumnMapping("BankingComplete")> Public Property BankingComplete() As Boolean Implements ISafe.BankingComplete
        Get
            Return _BankingComplete
        End Get
        Set(ByVal value As Boolean)
            _BankingComplete = value
        End Set
    End Property

    <ColumnMapping("NextDateToBeBanked")> Public Property NextDateToBeBanked() As Boolean Implements ISafe.NextDateToBeBanked
        Get
            Return _NextDateToBeBanked
        End Get
        Set(ByVal value As Boolean)
            _NextDateToBeBanked = value
        End Set
    End Property

    <ColumnMapping("LastDateBanked")> Public Property LastDateBanked() As Boolean Implements ISafe.LastDateBanked
        Get
            Return _LastDateBanked
        End Get
        Set(ByVal value As Boolean)
            _LastDateBanked = value
        End Set
    End Property

    <ColumnMapping("BankingBagCollectionRequired")> Public Property BankingBagCollectionRequired() As Boolean Implements ISafe.BankingBagCollectionRequired
        Get
            Return _BankingBagCollectionRequired
        End Get
        Set(ByVal value As Boolean)
            _BankingBagCollectionRequired = value
        End Set
    End Property

    <ColumnMapping("Description")> Public Property Description() As String Implements ISafe.Description
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    <ColumnMapping("SafeMaintenanceDone")> Public Property SafeMaintenanceDone() As Boolean Implements ISafe.SafeMaintenanceDone
        Get
            Return _SafeMaintenanceDone
        End Get
        Set(ByVal value As Boolean)
            _SafeMaintenanceDone = value
        End Set
    End Property

    <ColumnMapping("EndOfDayCheckDone")> Public Property EndOfDayCheckDone() As System.Nullable(Of Boolean) Implements ISafe.EndOfDayCheckDone
        Get
            Return _EndOfDayCheckDone
        End Get
        Set(ByVal value As System.Nullable(Of Boolean))
            _EndOfDayCheckDone = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class PeriodCollection
    Inherits BaseCollection(Of Period)

#Region "Methods"

    Public Overridable Sub LoadDates(ByVal dtmToday As Date?)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingDates")
                com.AddParameter("@Today", dtmToday.Value, SqlDbType.Date)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Overridable Sub LoadNonBankedDates(ByVal dtmToday As Date?)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingNonBankedDates")
                com.AddParameter("@Today", dtmToday.Value, SqlDbType.Date)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Overridable Function SelectedPeriod(ByVal intPeriodID As Integer) As Period
        SelectedPeriod = Nothing
        For Each P As Period In Me.Items
            If P.PeriodID = intPeriodID Then
                SelectedPeriod = P
                Exit Function
            End If
        Next
    End Function

    Public Overridable Function TodaysPeriod() As Period
        TodaysPeriod = Nothing
        For Each P As Period In Me.Items
            If TodaysPeriod Is Nothing Then
                TodaysPeriod = P
                Continue For
            End If
            If P.PeriodDate > TodaysPeriod.PeriodDate Then TodaysPeriod = P
        Next
    End Function

#End Region

End Class



