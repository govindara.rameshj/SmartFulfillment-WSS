﻿''' <summary>
''' List of tenders and cash tender denominations
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class TenderDenominationList
    Inherits Base

#Region "Table Fields And Properties"

    Private _DenominationID As Decimal
    Private _TenderText As String
    Private _TenderID As Integer
    Private _TenderReadOnly As Boolean
    Private _BullionMultiple As Decimal
    Private _SafeMinimum As Decimal

    Private _Amount As System.Nullable(Of Decimal)

    <ColumnMapping("DenominationID")> Public Property DenominationID() As Decimal
        Get
            Return _DenominationID
        End Get
        Set(ByVal value As Decimal)
            _DenominationID = value
        End Set
    End Property

    <ColumnMapping("TenderText")> Public Property TenderText() As String
        Get
            Return _TenderText
        End Get
        Set(ByVal value As String)
            _TenderText = value
        End Set
    End Property

    <ColumnMapping("TenderID")> Public Property TenderID() As Integer
        Get
            Return _TenderID
        End Get
        Set(ByVal value As Integer)
            _TenderID = value
        End Set
    End Property

    <ColumnMapping("TenderReadOnly")> Public Property TenderReadOnly() As Boolean
        Get
            Return _TenderReadOnly
        End Get
        Set(ByVal value As Boolean)
            _TenderReadOnly = value
        End Set
    End Property

    <ColumnMapping("BullionMultiple")> Public Property BullionMultiple() As Decimal
        Get
            Return _BullionMultiple
        End Get
        Set(ByVal value As Decimal)
            _BullionMultiple = value
        End Set
    End Property

    <ColumnMapping("SafeMinimum")> Public Property SafeMinimum() As Decimal
        Get
            Return _SafeMinimum
        End Get
        Set(ByVal value As Decimal)
            _SafeMinimum = value
        End Set
    End Property

    Public Property Amount() As System.Nullable(Of Decimal)
        Get
            Return _Amount
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _Amount = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Function IsCashTenderType() As Boolean

        Return _TenderID = 1
    End Function

    Public Function IsAmendableNonCashTenderType() As Boolean

        Return _TenderID <> 1 And Not _TenderReadOnly
    End Function

    Public Function IsUnAmendableNonCashTenderType() As Boolean

        Return _TenderID <> 1 And _TenderReadOnly
    End Function
#End Region

End Class

Public Class TenderDenominationListCollection
    Inherits BaseCollection(Of TenderDenominationList)

#Region "Methods"

    Public Sub LoadData()
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingTenderDenominationList")
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function TenderDenom(ByVal intTenderID As Integer, ByVal decDenominationID As Decimal) As TenderDenominationList
        TenderDenom = Nothing
        For Each obj As TenderDenominationList In Me.Items
            If obj.TenderID = intTenderID AndAlso obj.DenominationID = decDenominationID Then
                TenderDenom = obj
                Exit For
            End If
        Next
    End Function

#End Region

End Class