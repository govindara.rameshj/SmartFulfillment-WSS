﻿''' <summary>
''' Till sales taken by cashier broken down by tender
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class ActiveCashierSystemSale
    Inherits Base

#Region "Table Fields And Properties"

    Private _TenderID As Integer
    Private _TenderValue As System.Nullable(Of Decimal)

    <ColumnMapping("TenderID")> Public Property TenderID() As Integer
        Get
            Return _TenderID
        End Get
        Set(ByVal value As Integer)
            _TenderID = value
        End Set
    End Property

    <ColumnMapping("TenderValue")> Public Property TenderValue() As System.Nullable(Of Decimal)
        Get
            Return _TenderValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _TenderValue = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class ActiveCashierSystemSaleCollection
    Inherits BaseCollection(Of ActiveCashierSystemSale)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer, ByVal intCashierID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingActiveCashierSystemSale")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@CashierID", intCashierID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function TenderTotal(ByVal intTenderID As Integer) As System.Nullable(Of Decimal)
        For Each objTender As ActiveCashierSystemSale In Me.Items
            If objTender.TenderID = intTenderID Then
                TenderTotal = objTender.TenderValue
                Exit Function
            End If
        Next
    End Function

    Public Function SystemCashTotal() As System.Nullable(Of Decimal)
        For Each objTender As ActiveCashierSystemSale In Me.Items
            If objTender.TenderID = 1 Then
                SystemCashTotal = objTender.TenderValue
                Exit Function
            End If
        Next
    End Function

    Public Function SystemNonCashTotal() As System.Nullable(Of Decimal)
        SystemNonCashTotal = 0
        For Each objTender As ActiveCashierSystemSale In Me.Items
            If objTender.TenderID = 1 Then Continue For
            If objTender.TenderValue.HasValue = True Then SystemNonCashTotal += objTender.TenderValue
        Next
    End Function

    Public Function SystemTotal() As System.Nullable(Of Decimal)
        SystemTotal = 0
        For Each objTender As ActiveCashierSystemSale In Me.Items
            If objTender.TenderValue.HasValue = True Then SystemTotal += objTender.TenderValue
        Next
    End Function

#End Region

End Class