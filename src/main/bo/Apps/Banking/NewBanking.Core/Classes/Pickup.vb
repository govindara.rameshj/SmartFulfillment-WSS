﻿''' <summary>
''' List of "cash drops" or e.o.d pickup for cashier
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class Pickup
    Inherits Base

#Region "Table Fields And Properties"

    Private _PickupID As Integer
    Private _PickupPeriodID As Integer
    Private _PickupDate As Date
    Private _PickupSealNumber As String
    Private _PickupValue As System.Nullable(Of Decimal)
    Private _PickupComment As String
    Private _PTDSC As New PickupSaleCollection

    <ColumnMapping("PickupID")> _
    Public Property PickupID() As Integer
        Get
            Return _PickupID
        End Get
        Set(ByVal value As Integer)
            _PickupID = value
        End Set
    End Property

    <ColumnMapping("PickupPeriodID")> _
    Public Property PickupPeriodID() As Integer
        Get
            Return _PickupPeriodID
        End Get
        Set(ByVal value As Integer)
            _PickupPeriodID = value
        End Set
    End Property

    <ColumnMapping("PickupDate")> _
    Public Property PickupDate() As Date
        Get
            Return _PickupDate
        End Get
        Set(ByVal value As Date)
            _PickupDate = value
        End Set
    End Property

    <ColumnMapping("PickupSealNumber")> _
    Public Property PickupSealNumber() As String
        Get
            Return _PickupSealNumber
        End Get
        Set(ByVal value As String)
            _PickupSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupValue")> _
    Public Property PickupValue() As System.Nullable(Of Decimal)
        Get
            Return _PickupValue
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _PickupValue = value
        End Set
    End Property

    Public Property _
    PickupSales() As PickupSaleCollection
        Get
            Return _PTDSC
        End Get
        Set(ByVal value As PickupSaleCollection)
            _PTDSC = value
        End Set
    End Property

    <ColumnMapping("PickupComment")> _
    Public Property PickupComment() As String
        Get
            Return _PickupComment
        End Get
        Set(ByVal value As String)
            _PickupComment = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Function SaveComments(ByVal Comments As String) As Boolean
        Dim Success As Boolean = False
        Dim AffectedRows As Integer = 0

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingSafeBagPersistComments")
                com.AddParameter("@BagID", PickupID, SqlDbType.Int)
                com.AddParameter("@Comments", Comments, SqlDbType.Char)

                AffectedRows = com.ExecuteNonQuery()
            End Using
        End Using

        Return AffectedRows = 1
    End Function
#End Region

End Class

Public Class PickupCollection
    Inherits BaseCollection(Of Pickup)

#Region "Methods"

    Public Sub LoadCashDropPickupList(ByVal intPeriodID As Integer, ByVal intCashierID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingPickupCashDrop")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@CashierID", intCashierID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)

        'loop around cash drop pickup list and retrieve individual sales tender/denomination values
        For Each obj As Pickup In Me.Items
            obj.PickupSales.LoadPickupSalesByTenderDenomination(obj.PickupID)
        Next
    End Sub

    Public Sub LoadEndOfDayPickupList(ByVal intPeriodID As Integer, ByVal intCashierID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingPickupEndOfDay")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                com.AddParameter("@CashierID", intCashierID, SqlDbType.Int)

                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)

        'loop around cash drop pickup list and retrieve individual sales tender/denomination values
        For Each obj As Pickup In Me.Items
            obj.PickupSales.LoadPickupSalesByTenderDenomination(obj.PickupID)
        Next
    End Sub

    Public Function TenderDenominationValue(ByVal intPickupID As Integer, ByVal intTenderID As Integer, ByVal decDenominationID As Decimal) As System.Nullable(Of Decimal)
        For Each obj As Pickup In Me.Items
            If obj.PickupID = intPickupID Then
                TenderDenominationValue = obj.PickupSales.TenderDenominationValue(intTenderID, decDenominationID)
            End If
        Next
    End Function

    Public Function GetBag(ByVal BagId As Integer) As Pickup

        GetBag = GetPickupBag(BagId)
    End Function
#End Region

#Region "Internals"

    Friend Overridable Function GetPickupBag(ByVal BagId As Integer) As Pickup

        GetPickupBag = Nothing
        For Each PickupBag As Pickup In Me.Items
            If PickupBag.PickupID = BagId Then
                GetPickupBag = PickupBag
                Exit For
            End If
        Next
    End Function
#End Region
End Class
