﻿''' <summary>
''' Provides list of floated cashiers with and without pickups
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class FloatedPickupList
    Inherits Base

#Region "Table Fields And Properties"

    Private _PickupID As System.Nullable(Of Integer)
    Private _PickupSealNumber As String
    Private _PickupComment As String
    Private _StartFloatID As Integer
    Private _StartFloatSealNumber As String
    Private _StartFloatValue As Decimal
    Private _SaleTaken As Boolean
    Private _CashierID As Integer
    Private _CashierUserName As String
    Private _CashierEmployeeCode As String
    Private _LoggedOnTillId As String

    <ColumnMapping("PickupID")> Public Property PickupID() As System.Nullable(Of Integer)
        Get
            Return _PickupID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupID = value
        End Set
    End Property

    <ColumnMapping("PickupSealNumber")> Public Property PickupSealNumber() As String
        Get
            Return _PickupSealNumber
        End Get
        Set(ByVal value As String)
            _PickupSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupComment")> Public Property PickupComment() As String
        Get
            Return _PickupComment
        End Get
        Set(ByVal value As String)
            _PickupComment = value
        End Set
    End Property

    <ColumnMapping("StartFloatID")> Public Property StartFloatID() As Integer
        Get
            Return _StartFloatID
        End Get
        Set(ByVal value As Integer)
            _StartFloatID = value
        End Set
    End Property

    <ColumnMapping("StartFloatSealNumber")> Public Property StartFloatSealNumber() As String
        Get
            Return _StartFloatSealNumber
        End Get
        Set(ByVal value As String)
            _StartFloatSealNumber = value
        End Set
    End Property

    <ColumnMapping("StartFloatValue")> Public Property StartFloatValue() As Decimal
        Get
            Return _StartFloatValue
        End Get
        Set(ByVal value As Decimal)
            _StartFloatValue = value
        End Set
    End Property

    <ColumnMapping("SaleTaken")> Public Property SaleTaken() As Boolean
        Get
            Return _SaleTaken
        End Get
        Set(ByVal value As Boolean)
            _SaleTaken = value
        End Set
    End Property

    <ColumnMapping("CashierID")> Public Property CashierID() As Integer
        Get
            Return _CashierID
        End Get
        Set(ByVal value As Integer)
            _CashierID = value
        End Set
    End Property

    <ColumnMapping("CashierUserName")> Public Property CashierUserName() As String
        Get
            Return _CashierUserName
        End Get
        Set(ByVal value As String)
            _CashierUserName = value
        End Set
    End Property

    <ColumnMapping("CashierEmployeeCode")> Public Property CashierEmployeeCode() As String
        Get
            Return _CashierEmployeeCode
        End Get
        Set(ByVal value As String)
            _CashierEmployeeCode = value
        End Set
    End Property

    <ColumnMapping("LoggedOnTillId")> Public Property LoggedOnTillId() As String
        Get
            Return _LoggedOnTillId
        End Get
        Set(ByVal value As String)
            _LoggedOnTillId = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Function IsLoggedOn() As Boolean

        If LoggedOnTillId IsNot Nothing AndAlso LoggedOnTillId.Length > 0 Then
            IsLoggedOn = String.Compare(LoggedOnTillId, "99") <> 0
        End If
    End Function
#End Region
End Class

Public Class FloatedPickupListCollection
    Inherits BaseCollection(Of FloatedPickupList)

#Region "Methods"

    Public Overridable Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingFloatedPickupList")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

    Public Function SelectedCashier(ByVal intCashierID As Integer) As FloatedPickupList
        SelectedCashier = Nothing
        For Each FPD As FloatedPickupList In Me.Items
            If FPD.CashierID = intCashierID Then
                SelectedCashier = FPD
                Exit Function
            End If
        Next
    End Function

    Public Function CashierIsLoggedOn(ByVal CashierID As Integer) As Boolean
        Dim Cashier As FloatedPickupList = Me.SelectedCashier(CashierID)

        Return Cashier.IsLoggedOn
    End Function
#End Region

End Class
