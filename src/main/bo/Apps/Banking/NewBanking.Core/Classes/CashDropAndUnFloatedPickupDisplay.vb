﻿''' <summary>
''' Provides cash drops and end of day pickup information
''' Cash Drops        - List of unfloated and floated cashiers
'''                     List of design consultants
''' End of Day Pickup - List of unfloated cashiers and design consultants pickups
''' </summary>
''' <history></history>
''' <remarks></remarks>
Public Class CashDropAndUnFloatedPickupDisplay
    Inherits Base

#Region "Table Fields And Properties"

    Private _AssignedToUserID As System.Nullable(Of Integer)
    Private _AssignedToUserName As String
    Private _TillVariance As System.Nullable(Of Decimal)
    Private _PickupSealNumber As String
    Private _PickupAssignedByUserID As System.Nullable(Of Integer)
    Private _PickupAssignedByUserName As String
    Private _PickupAssignedBySecondUserID As System.Nullable(Of Integer)
    Private _PickupAssignedBySecondUserName As String
    Private _EODBagCheck As String = ""  'no data access
    Private _Comments As String = ""     'no data access

    <ColumnMapping("AssignedToUserID")> Public Property AssignedToUserID() As System.Nullable(Of Integer)
        Get
            Return _AssignedToUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _AssignedToUserID = value
        End Set
    End Property

    <ColumnMapping("AssignedToUserName")> Public Property AssignedToUserName() As String
        Get
            Return _AssignedToUserName
        End Get
        Set(ByVal value As String)
            _AssignedToUserName = value
        End Set
    End Property

    <ColumnMapping("TillVariance")> Public Property TillVariance() As System.Nullable(Of Decimal)
        Get
            Return _TillVariance
        End Get
        Set(ByVal value As System.Nullable(Of Decimal))
            _TillVariance = value
        End Set
    End Property

    <ColumnMapping("PickupSealNumber")> Public Property PickupSealNumber() As String
        Get
            Return _PickupSealNumber
        End Get
        Set(ByVal value As String)
            _PickupSealNumber = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedByUserID")> Public Property PickupAssignedByUserID() As System.Nullable(Of Integer)
        Get
            Return _PickupAssignedByUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupAssignedByUserID = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedByUserName")> Public Property PickupAssignedByUserName() As String
        Get
            Return _PickupAssignedByUserName
        End Get
        Set(ByVal value As String)
            _PickupAssignedByUserName = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedBySecondUserID")> Public Property PickupAssignedBySecondUserID() As System.Nullable(Of Integer)
        Get
            Return _PickupAssignedBySecondUserID
        End Get
        Set(ByVal value As System.Nullable(Of Integer))
            _PickupAssignedBySecondUserID = value
        End Set
    End Property

    <ColumnMapping("PickupAssignedBySecondUserName")> Public Property PickupAssignedBySecondUserName() As String
        Get
            Return _PickupAssignedBySecondUserName
        End Get
        Set(ByVal value As String)
            _PickupAssignedBySecondUserName = value
        End Set
    End Property

    <ColumnMapping("EODBagCheck")> Public Property EODBagCheck() As String
        Get
            Return _EODBagCheck
        End Get
        Set(ByVal value As String)
            _EODBagCheck = value
        End Set
    End Property

    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

End Class

Public Class CashDropAndUnFloatedPickupDisplayCollection
    Inherits BaseCollection(Of CashDropAndUnFloatedPickupDisplay)

#Region "Methods"

    Public Sub LoadData(ByVal intPeriodID As Integer)
        Dim dt As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand("NewBankingCashDropAndUnFloatedPickupsDisplay")
                com.AddParameter("@PeriodID", intPeriodID, SqlDbType.Int)
                dt = com.ExecuteDataTable
            End Using
        End Using

        Me.Load(dt)
    End Sub

#End Region

End Class


