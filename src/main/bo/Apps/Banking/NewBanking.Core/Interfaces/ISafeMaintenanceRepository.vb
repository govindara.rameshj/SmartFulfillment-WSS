﻿Public Interface ISafeMaintenanceRepository

    Function Load(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable
    'Function LoadBankingBags(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable
    'Function GetBankingBagSealNumber(ByVal BankingBag As DataRow) As String

    Sub Persist(ByRef Con As Connection, ByRef SafeMaintenance As ISafeMaintenanceModel)
    Sub Persist(ByRef OdbcConnection As clsOasys3DB, ByRef SafeMaintenance As ISafeMaintenanceModel)

End Interface