﻿Namespace TpWickes

    Public Interface INoSaleBankingRepository

        Function AllowNoSaleBanking() As System.Nullable(Of Boolean)

    End Interface

End Namespace