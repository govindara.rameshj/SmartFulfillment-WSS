﻿Public Interface ISafeBagModel

    Property ID() As Integer
    Property SealNumber() As String
    Property BagType() As String
    Property BagState() As String
    Property Value() As Decimal
    Property Comments() As String
End Interface

Public Interface ISafeBagCollectionModel

    Sub LoadPickupBagExistInSafe(ByVal PeriodID As Integer)
    Sub LoadBankingBagExistInSafe(ByVal PeriodID As Integer)
    Sub LoadSafeBag(ByVal BagID As Integer)

    'Function PickupBagExistInSafe(ByVal SealNumber As String) As Boolean
    'Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean

    Function ListBags() As IList(Of SafeBagModel)

End Interface
