﻿Public Interface ISafeMaintenanceEngine

    Sub CreateSafeMaintenance(ByVal PeriodID As Integer)

    Property MiscellaneousProperty() As String
    Property GiftTokenSeal() As String
    Property Comments() As String


    Function Load(ByVal PeriodID As Integer) As Boolean
    'Function GetBankingBags() As List(Of String)
    Function SafeMaintenanceAlreadyPerformed() As Boolean

    Function Persist() As Boolean
    Sub Persist(ByRef OdbcConnection As clsOasys3DB)

End Interface