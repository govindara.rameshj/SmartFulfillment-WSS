﻿Public Interface ICompletePickupAvailability

    Function IsAvailable(ByRef Selected As FloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean
    Function IsAvailable(ByRef Selected As UnFloatedPickupList, ByVal CurrentAvailablity As Boolean, ByVal CheckCashierIsNotLoggedOnToTill As Boolean) As Boolean
End Interface
