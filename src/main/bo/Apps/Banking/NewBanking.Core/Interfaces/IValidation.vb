﻿Public Interface IValidation

    ReadOnly Property RequirementEnabled() As System.Nullable(Of Boolean)
    ReadOnly Property StoreID() As Integer
    ReadOnly Property eStoreShopID() As Integer
    ReadOnly Property eStoreWarehouseID() As Integer

    Function PerformCheck() As Boolean

End Interface