﻿Public Interface ISafeMaintenanceModel

    Property SafeChecked() As Boolean
    Property MiscellaneousProperty() As String
    Property GiftTokenSeal() As String
    Property Comments() As String
    Property PeriodID() As Integer

    'Function GetBankingBags() As List(Of String)
    Sub LoadSafeMaintenance(ByRef Con As Connection, ByVal PeriodID As Integer)

    Sub PersistSafeMaintenance(ByRef Con As Connection)
    Sub PersistSafeMaintenance(ByRef OdbcConnection As clsOasys3DB)

End Interface