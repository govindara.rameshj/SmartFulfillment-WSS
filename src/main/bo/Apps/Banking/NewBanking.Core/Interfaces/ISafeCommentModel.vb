﻿Public Interface ISafeCommentModel

    Property ID() As Integer
    Property ProcessID() As Integer
    Property PeriodID() As Integer
    Property BagType() As String
    Property Comment() As String

    Sub Persist(ByRef Con As Connection)
    Sub Persist(ByRef OdbcConnection As clsOasys3DB)
    Sub SafeMaintenanceDeleteExisting(ByRef OdbcConnection As clsOasys3DB)

End Interface