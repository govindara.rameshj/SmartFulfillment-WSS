﻿Public Interface ISafe

    Property PeriodID() As Integer
    Property PeriodDate() As Date
    Property BankingComplete() As Boolean
    Property NextDateToBeBanked() As Boolean
    Property LastDateBanked() As Boolean
    Property BankingBagCollectionRequired() As Boolean
    Property Description() As String
    Property SafeMaintenanceDone() As Boolean

    Property EndOfDayCheckDone() As System.Nullable(Of Boolean)

End Interface