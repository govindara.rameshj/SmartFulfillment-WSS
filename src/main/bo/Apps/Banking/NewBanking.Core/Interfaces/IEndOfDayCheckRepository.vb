﻿Public Interface IEndOfDayCheckRepository

    Function GetSummary(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable
    Function GetBankingBagExistInSafe(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable
    Function GetPickupBagExistInSafe(ByRef Con As Connection, ByVal PeriodID As Integer) As DataTable

    Sub SafeBagsScannedPersist(ByRef Con As Connection, ByRef Bag As ISafeBagScannedModel)
    Sub SafeBagsScannedPersist(ByRef OdbcConnection As clsOasys3DB, ByRef Bag As ISafeBagScannedModel)

    Sub SafeCommentPersist(ByRef Con As Connection, ByRef Comment As ISafeCommentModel)
    Sub SafeCommentPersist(ByRef OdbcConnection As clsOasys3DB, ByRef Comment As ISafeCommentModel)

    Sub SafeBagsScannedDeleteExisting(ByRef OdbcConnection As clsOasys3DB, ByVal ProcessID As Integer, ByVal PeriodID As Integer)
    Sub SafeCommentDeleteExisting(ByRef OdbcConnection As clsOasys3DB, ByVal ProcessID As Integer, ByVal PeriodID As Integer)

    Sub PersistSafe(ByRef Con As Connection, ByRef Safe As ISafeModel)

    Function ValidateSealNumber(ByVal SealNumber As String) As Integer

End Interface