﻿Public Interface ISafeModel

    Property PeriodID() As Integer
    Property EndOfDayCheckDone() As Boolean
    Property EndOfDayCheckLockedFrom() As System.Nullable(Of Date)
    Property EndOfDayCheckLockedTo() As System.Nullable(Of Date)
    Property EndOfDayCheckManagerID() As System.Nullable(Of Integer)
    Property EndOfDayCheckWitnessID() As System.Nullable(Of Integer)

    Sub Persist(ByRef Con As Connection)

End Interface