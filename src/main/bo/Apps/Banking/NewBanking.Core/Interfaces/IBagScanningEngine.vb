﻿Public Interface IBagScanningEngine

    Enum BusinessProcess

        EndOfDayCheckProcess = 1
        SafeMaintenanceProcess = 2

    End Enum

    Sub Initialise(ByVal PeriodID As Integer, ByVal ProcessID As BusinessProcess)

    Sub EndOfDayCheckAdditionalInformation(ByVal CheckDone As Boolean, _
                                           ByVal LockedFrom As Date, _
                                           ByVal LockedTo As Date, _
                                           ByVal ManagerID As Integer, _
                                           ByVal WitnessID As Integer)

    Sub CreatePickupBag(ByVal SealNumber As String)
    Sub CreatePickupBag(ByVal SealNumber As String, ByVal SealComment As String)

    Sub CreateBankingBag(ByVal SealNumber As String)
    Sub CreateBankingBag(ByVal SealNumber As String, ByVal SealComment As String)

    Sub CreatePickupComment(ByVal Value As String)
    Sub CreateBankingComment(ByVal Value As String)

    Function PickupBagAlreadyScanned(ByVal SealNumber As String) As Boolean
    Function BankingBagAlreadyScanned(ByVal SealNumber As String) As Boolean

    Function PickupBagHasValidSeal(ByVal SealNumber As String) As Boolean
    Function BankingBagHasValidSeal(ByVal SealNumber As String) As Boolean

    Function PickupBagExistInSafe(ByVal SealNumber As String) As Boolean
    Function BankingBagExistInSafe(ByVal SealNumber As String) As Boolean

    Function MatchSystemPickupExpectation() As Boolean
    Function MatchSystemBankingExpectation() As Boolean

    Function EndOfDayCheckPersist() As Boolean
    Sub SafeMaintenancePersist(ByRef OdbcConnection As clsOasys3DB)

    Function PickupBagList() As List(Of ISafeBagScannedModel)
    Function BankingBagList() As List(Of ISafeBagScannedModel)

    Function PickupCommentList() As List(Of ISafeCommentModel)
    Function BankingCommentList() As List(Of ISafeCommentModel)

End Interface