﻿Imports System.Text

Public Class Form1
    Private _Oasys3db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Private Sub btnRetrieve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click

        lstResults.Items.Clear()
        btnAccept.Enabled = False

        Dim date1 As Date = dte.Value
        Dim till As String = txtTill.Text.Trim
        Dim tran As String = txtTran.Text.Trim

        Dim sb As New StringBuilder
        sb.Append("select DATE1,TILL,[TRAN] from DLTOTS where DATE1='" & date1.ToString("yyyy-MM-dd") & "'")
        If till.Length = 2 Then sb.Append(" and TILL=" & till)
        If tran.Length = 4 Then sb.Append(" and [TRAN]=" & tran)

        Dim dt As DataTable = _Oasys3db.ExecuteSql(sb.ToString).Tables(0)
        For Each dr As DataRow In dt.Rows
            sb = New StringBuilder
            sb.Append("""" & Application.StartupPath & "\CashierBalancingUpdate.exe" & """")
            sb.Append("|")
            sb.Append("T")
            sb.Append(date1.Year.ToString & Format(date1.Month, "00") & Format(date1.Day, "00"))
            sb.Append(CStr(dr("TILL")) & CStr(dr("TRAN")))

            lstResults.Items.Add(sb.ToString)
        Next

        If lstResults.Items.Count > 0 Then btnAccept.Enabled = True

    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        For index As Integer = lstResults.Items.Count - 1 To 0 Step -1
            Dim parts() As String = lstResults.Items(index).ToString.Split("|"c)
            RunShell(parts(0), parts(1), Application.StartupPath)
            lstResults.Items.RemoveAt(index)
        Next

    End Sub

    Private Sub RunShell(ByVal cmdline As String, ByVal cdArguements As String, ByVal WorkingDirectory As String)

        Dim newProc As Diagnostics.Process = New System.Diagnostics.Process()
        Trace.WriteLine("Working directory " & WorkingDirectory, Me.Name)
        newProc.StartInfo.WorkingDirectory = WorkingDirectory
        newProc.StartInfo.FileName = cmdline
        Trace.WriteLine("Application run directory " & cmdline, Me.Name)
        newProc.StartInfo.Arguments = cdArguements
        Trace.WriteLine("Arguments " & cdArguements, Me.Name)
        newProc.StartInfo.WindowStyle = ProcessWindowStyle.Normal
        newProc.StartInfo.UseShellExecute = False
        newProc.Start()
        newProc.WaitForExit()

    End Sub

End Class
