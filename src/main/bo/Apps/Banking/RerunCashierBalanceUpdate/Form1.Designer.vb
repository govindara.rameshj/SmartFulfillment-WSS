﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dte = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnAccept = New System.Windows.Forms.Button
        Me.lstResults = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtTill = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtTran = New System.Windows.Forms.TextBox
        Me.btnRetrieve = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'dte
        '
        Me.dte.Location = New System.Drawing.Point(81, 6)
        Me.dte.Name = "dte"
        Me.dte.Size = New System.Drawing.Size(115, 20)
        Me.dte.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Enter Date"
        '
        'btnAccept
        '
        Me.btnAccept.Location = New System.Drawing.Point(217, 32)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(59, 21)
        Me.btnAccept.TabIndex = 2
        Me.btnAccept.Text = "Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'lstResults
        '
        Me.lstResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstResults.FormattingEnabled = True
        Me.lstResults.Location = New System.Drawing.Point(12, 93)
        Me.lstResults.Name = "lstResults"
        Me.lstResults.Size = New System.Drawing.Size(398, 264)
        Me.lstResults.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Enter Till"
        '
        'txtTill
        '
        Me.txtTill.Location = New System.Drawing.Point(81, 31)
        Me.txtTill.Name = "txtTill"
        Me.txtTill.Size = New System.Drawing.Size(54, 20)
        Me.txtTill.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Enter Tran"
        '
        'txtTran
        '
        Me.txtTran.Location = New System.Drawing.Point(81, 56)
        Me.txtTran.Name = "txtTran"
        Me.txtTran.Size = New System.Drawing.Size(54, 20)
        Me.txtTran.TabIndex = 7
        '
        'btnRetrieve
        '
        Me.btnRetrieve.Location = New System.Drawing.Point(217, 5)
        Me.btnRetrieve.Name = "btnRetrieve"
        Me.btnRetrieve.Size = New System.Drawing.Size(59, 21)
        Me.btnRetrieve.TabIndex = 8
        Me.btnRetrieve.Text = "Retrieve"
        Me.btnRetrieve.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(425, 374)
        Me.Controls.Add(Me.btnRetrieve)
        Me.Controls.Add(Me.txtTran)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtTill)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstResults)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dte)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dte As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents lstResults As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTill As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTran As System.Windows.Forms.TextBox
    Friend WithEvents btnRetrieve As System.Windows.Forms.Button

End Class
