﻿Public Class UnAssignManualCheckModel
    Implements IUnAssignManualCheckModel

    Friend _FloatBagID As Integer
    Friend _PickupBagID As Integer
    Friend _LoggedOnUserID As Integer
    Friend _SecondUserID As Integer
    Friend _BankingPeriodID As Integer
    Friend _TodayPeriodID As Integer
    Friend _AccountingModel As String
    Friend _FloatSealNumber As String
    Friend _CurrencyID As String
    Friend _Comments As String
    Friend _Connection As clsOasys3DB
    Friend _NewUnAssignedFloatBagId As Integer
    Friend _NewCheckedFloatBagId As Integer

    Public Sub Initialise(ByVal FloatBagID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String, ByVal Comments As String) Implements IUnAssignManualCheckModel.Initialise

        _FloatBagID = FloatBagID
        _PickupBagID = PickupBagID
        _LoggedOnUserID = LoggedOnUserID
        _SecondUserID = SecondUserID
        _BankingPeriodID = BankingPeriodID
        _TodayPeriodID = TodayPeriodID
        _AccountingModel = AccountingModel
        _FloatSealNumber = FloatSealNumber
        _CurrencyID = CurrencyID
        _Comments = Comments
    End Sub

    Public Sub StartTransaction() Implements IUnAssignManualCheckModel.StartTransaction

        _Connection = New clsOasys3DB("Default", 0)
        _Connection.BeginTransaction()
    End Sub

    Public Sub UnAssignTheFloat() Implements IUnAssignManualCheckModel.UnAssignTheFloat
        Dim FloatBagDenomValues As New FloatBagCollection
        Dim UnAssignFloat As Banking.Core.FloatBanking

        FloatBagDenomValues.LoadData(_FloatBagID)
        NewBanking.Core.Library.FloatPrepareUnAssigned(_Connection, _FloatBagID)
        UnAssignFloat = New Banking.Core.FloatBanking(_Connection, _BankingPeriodID, _TodayPeriodID, _LoggedOnUserID, _SecondUserID, Banking.Core.FloatBanking.Types.Float, _AccountingModel, _PickupBagID)
        With UnAssignFloat
            .LoadOriginalBag(_FloatBagID)
            .SetBagState(Banking.Core.BagStates.Sealed)
            .SetSealNumber(_FloatSealNumber)
            For Each Bag As FloatBag In FloatBagDenomValues
                With Bag
                    If .FloatValue.HasValue AndAlso .FloatValue > 0 Then
                        UnAssignFloat.SetDenomination(_CurrencyID, .DenominationID, .TenderID, .FloatValue.Value, String.Empty)
                    End If
                End With
            Next
            .NewBankingUnAssignFloatAsPartOfWiderTransaction(_BankingPeriodID)
            _NewUnAssignedFloatBagId = .NewId
        End With
    End Sub

    Public Sub SetUnchangedFloatAsChecked() Implements IUnAssignManualCheckModel.SetUnchangedFloatAsChecked

        If String.IsNullOrEmpty(_Comments) Then
            NewBanking.Core.FloatChecked(_Connection, _NewUnAssignedFloatBagId, _LoggedOnUserID, _SecondUserID)
        Else
            NewBanking.Core.FloatChecked(_Connection, _NewUnAssignedFloatBagId, _LoggedOnUserID, _SecondUserID, _Comments)
        End If
    End Sub

    Public Sub SetCheckedFloatAsChanged(ByVal NewFloatBagDenoms As FloatBagCollection) Implements IUnAssignManualCheckModel.SetCheckedFloatAsChanged
        Dim ManualCheckFloat As Banking.Core.FloatBanking

        ManualCheckFloat = New Banking.Core.FloatBanking(_Connection, _BankingPeriodID, _TodayPeriodID, _LoggedOnUserID, _SecondUserID, Banking.Core.FloatBanking.Types.Float, _AccountingModel)
        With ManualCheckFloat
            .LoadOriginalBag(_NewUnAssignedFloatBagId)
            .SetBagState(Banking.Core.BagStates.Sealed)
            .SetSealNumber(_FloatSealNumber)
            For Each Bag As FloatBag In NewFloatBagDenoms
                With Bag
                    If .FloatValue.HasValue AndAlso .FloatValue > 0 Then
                        ManualCheckFloat.SetDenomination(_CurrencyID, .DenominationID, .TenderID, .FloatValue.Value, String.Empty)
                    End If
                End With
            Next
            .Comments = _Comments
            _NewCheckedFloatBagId = .NewBankingManualCheckAsPartOfWiderTransaction(_LoggedOnUserID, _SecondUserID)
        End With
    End Sub

    Public Sub Commit() Implements IUnAssignManualCheckModel.Commit

        _Connection.CommitTransaction()
    End Sub

    Public Sub RollBack() Implements IUnAssignManualCheckModel.RollBack

        _Connection.RollBackTransaction()
    End Sub

    Public Function IsInTransaction() As Boolean Implements IUnAssignManualCheckModel.IsInTransaction

        Return _Connection.Transaction
    End Function
End Class
