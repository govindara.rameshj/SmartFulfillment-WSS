﻿Public Interface IUnAssignManualCheckModel

    Sub Initialise(ByVal FloatBagID As Integer, ByVal PickupBagID As Integer, ByVal LoggedOnUserID As Integer, ByVal SecondUserID As Integer, ByVal BankingPeriodID As Integer, ByVal TodayPeriodID As Integer, ByVal AccountingModel As String, ByVal FloatSealNumber As String, ByVal CurrencyID As String, ByVal Comments As String)
    Sub StartTransaction()
    Sub UnAssignTheFloat()
    Sub SetUnchangedFloatAsChecked()
    Sub SetCheckedFloatAsChanged(ByVal NewFloatBagDenoms As FloatBagCollection)
    Sub Commit()
    Sub RollBack()
    Function IsInTransaction() As Boolean
End Interface
