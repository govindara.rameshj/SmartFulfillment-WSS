﻿Public Class UnAssignManualCheckModelFactory
    Inherits BaseFactory(Of IUnAssignManualCheckModel)

    Public Overrides Function Implementation() As IUnAssignManualCheckModel

        Return New UnAssignManualCheckModel
    End Function
End Class
