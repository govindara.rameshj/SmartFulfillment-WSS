﻿Imports Cts.Oasys.Core.Tests
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.SystemEnvironment

'http://mono-project.com/Main_Page
'http://www.mono-project.com/Moonlight
'http://www.microsoft.com/silverlight/overview/ria/ria-details.aspx
''' <summary>
''' TRANSMISSIONS OUT/IN
'''  27/08/2008 - WIX1300 - Back Office Rewrite - Open/Close/Overnight.
'''  Builds transmission files in the comms folder specified in PARAMETERS record 914.
''' </summary>
''' <remarks>
''' Uses command arguments to determine which transmission file is to be created/added to
''' Current arguments that have been catered for are :
''' "DR" - Receipts             - Build STHOA Receipts (based on Uptrac program TOBADR)
''' "AD" - Adjustments          - Build STHOA Adjustments (based on Uptrac program TOBAAD)
''' "DB" - Banking              - Build STHOA Banking (based on Uptrac program TOBADB)
''' "SA" - Sales                - Build STHOA Sales (based on Uptrac program TOBASA)
''' "RI" - Weekly Related Items - Build STHOA Weekly Related Itetms (based on Uptrac program TOBARI
''' "OJ" - JDA Compare          - Build STHOJ JDA Compare (based on Uptrac program TOBHOJ)
''' "SP" - Out Of Stocks        - Build STHPA Out of Stocks (based on Uptrac program TOBASP)
''' "RF" - Refunds              - Build STHPA Refunds (based on Uptrac program TOBARF)
''' "PO" - Purchase Orders      - Build STHPO Orders (based on Uptrac program TOBHPO)
''' "EE" - JDA Markups          - Build STHOA JDA Markups (based on Uptrac program TOBAEE)
''' "OL" - STHOL 15 minute extract - Build STHOL  (based on Uptrac program TOBHOL)
''' "OE" - STHOY Coupons        - Build STHOY Coupons (based on Uptrac program TOBCOE)
''' "MU" - Markups              - Build STHOA Markups (based on Uptrac program TOBAMU)
''' "OT" - STHOT Transactions   - Build STHOT Transactions (based on Uptrac program TOBHOT)
'''   HTRSUM is used......
''' "ER" - Build STHOC end of daye - Add endof day record to STHOC
''' "OC" - Post Codes           - Build Comms\PCODEsss (based on Uptrac program TOBPOC)
''' 
''' Still working on 
''' Will be finished when PRCCHG sorted
''' "OF" - STHOF Flash Totals   - Build STHOF Flash Totals (based on Uptrac program TOBHOF)
''' Mostly completed but there are quite a few of the figures taken from Daily Highlights
''' 
''' MU DR AD SA DB EE OT OF PO OJ OE OC RF SP OL RI er
''' 
''' Other arguments used
''' "NIGHT=Y"        - to indicate program running in NIGHT
''' "NIGHT=N"        - to indicate program not running in NIGHT (this is the default for this argument)
''' "SDATE=DD/MM/YY" - indicate a start date
''' "EDATE=DD/MM/YY" - indicate an end date
''' "PDATE=DD/MM/YY" - indicate a preparation date
''' "FLAGASCOMMED=Y" - indicate that any files where the commed to ho flag is used
'''                    will have these flags set on when processed. To stop this use FLAGASCOMMED=N
''' 
''' So if you wanted to run this program overnight and build all the transmission files
''' run BUILDSTHOA.EXE DR AD db Oj Sa ri sp rf po mu ee ot of oe oc ol night=y
''' obviously the program can be run with specific parameters to enable the user to run
''' the program after other events have occurred.
''' 
''' The SA, OJ, SP functions all pass STKMAS so to speed things up, it may be advisable
''' to at least run these functions together .... BUILDSTHOA sa OJ sP NIGHT=Y
''' Another parameter added is the SOCLOS parameter which allows the user to "CLOSE"
''' the transmission files. Use this parameter on its own as other parameteres will be ignored
''' 
''' SOCLAS=STHOA, STHPO, STHOJ will result in the STHOA.STHPO and STHOJ files being processed
''' to add headers and trailers and the copying to the TORTI directory
''' 
''' TRANSMISSIONS IN
''' Extracts transmission files from the comms folder specified in PARAMETERS record 914
''' Uses command arguments to determine which transmission file is to be processed
''' The transmission files are Copied from the FROMRTI folder (this is the basic SIOPEN
''' function mentioned in the BSD.
''' Current arguments that have been catered for are :
''' "HOSTU" - Process Product updates.
'''         - Based on the UPTRAC - TIBHOU
'''         - Performs the TIBHUS - Products updates and TIBHUE - Event Updates functionality
''' </remarks>

Module MainModule

#Region "Module level declarations"

    Private Const OriginalSthoFileSeparator As String = ChrW(9619)

    Private _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Dim _WorkstationID As String
    Dim intMaximumSthoOutputLength As Integer = 10000
    Dim dateLowestDateAllowed As Date = CDate("01/01/1990")
    Dim _RunningInNight As Boolean = True
    Dim _PromptForDate As Boolean = False
    Dim strOutputText As String
    Dim strCsvWorkingArea As String

    Dim _StartDate As Date = Today.Date
    Dim _EndDate As Date = Today.Date
    Dim _StartDateToUse As Date = Today.Date
    Dim _EndDateToUse As Date = Today.Date
    Dim _DatePassedIn As Boolean = False
    Dim _ReportDate As String = String.Empty
    Dim dateCurrentTime As Date
    Dim dateStartTime As Date
    Dim dateEndTime As Date
    Dim dateTobhotStartDate As Date
    Dim dateTobhotEndDate As Date
    Dim arrstrTobHotTypes As String() = New String(20) {}

    Dim dateHashDate As Date
    Dim strStockAdjustmentSign As String = String.Empty ' Sign
    Dim strSthoaFileName As String = String.Empty '"C:\COMMS\STHOA.TXT"
    Dim strSthoaText As String = String.Empty
    Dim strSthojText As String = String.Empty
    Dim strSthpaText As String = String.Empty
    Dim decHashValue As Decimal = 0

    Dim intUnitsSoldToday As Integer
    Dim intMarkdownUnitsToday As Integer
    Dim decMarkdownValueToday As Decimal
    Dim intClosingMarkdownUnits As Integer
    Dim decValueOfOpenPurchaseOrders As Decimal = 0

    Public _StoreNumber As String = String.Empty
    Dim _StoreName As String = String.Empty
    Dim _NoOfVATRates As Integer = 0
    ' HOSTU counts
    Dim intHeadOfficeNumberOfRecords As Integer = 0 ' Records in the file
    Dim intHeadOfficeWrongStore As Integer = 0 ' Records in the file for the wrong store
    Dim intHeadOfficeBadType As Integer = 0 ' Bad Record Types
    Dim intHeadOfficeNewItems As Integer = 0 'Type 1 Records
    Dim intHeadOfficeBadNew As Integer = 0 'Type 1 Records -Already on file
    Dim intHeadOfficeChanges As Integer = 0 'Type 4 Records
    Dim intHeadOfficeBadChanges As Integer = 0 'Type 4 Records - NOT on file
    Dim intHeadOfficeModels As Integer = 0 'Type 9 Records in file
    Dim arrintProcessedCounts As Integer() = New Integer(15) {}
    Dim arrintRejectedCounts As Integer() = New Integer(15) {}
    Dim arrstrRecordTypes As String() = New String(15) {} ' {" ", "U1", "U2", "U3", "U4", "U5", "U6", "UA", "UB", "UC", "UD", "UE", "UF", "UK", "UW"}
    Dim intCCProcessedCount As Integer = 0
    Dim intCCRejectedCount As Integer = 0
    Dim intCMProcessedCount As Integer = 0
    Dim intCMRejectedCount As Integer = 0
    Dim intCYProcessedCount As Integer = 0
    Dim intCYRejectedCount As Integer = 0
    Dim intRecordOccurrence As Integer = 0
    Dim boolDoingAStoreLoad As Boolean = False
    Dim boolNeedSecondPass As Boolean = False
    Dim boolNeedToRunTibhue As Boolean = False
    '    Dim intLatestAvailableVersion As Integer ' Latest version number processed
    '    Dim intLatestAvailableSequence As Integer ' Latest version number processed
    Dim arrRecordTypes As String() = New String(15) {} 'Record Type(2) & Record Count (8) & hash (14.2)

    Dim strSthoRecordType As String = String.Empty   'STHOA Record Type
    Dim strSthoRecordDate As String          'Date
    Dim strSthoRecordHashValue As String = "       0.00 " '10.2 Signed Numeric - For Trailer Hash 

    Dim _STHOARecTypeA6SkuNumber As String = "000000"       'Sku Number
    Dim _STHOARecTypeA6AdjustmentCode As String = "IN"           'Adjustment Code
    Dim _STHOARecTypeA6AdjustmentUnits As String = "      0 "     'Units Received
    Dim _STHOARecTypeA6AdjustmentValue As String = "       0.00 " 'Value Received
    Dim _STHOARecTypeA6AdjustmentReference As String = "      "       'BBC Issue Note Number
    Dim _STHOARecTypeA6ItemSellingPrice As String = "       0.00 " '10.2 Signed Numeric Receipt Price               
    Dim _STHOARecTypeA6AssemblyDepotNumber As String = "173"          'BBC Store Number
    Dim _STHOARecTypeA6TransferValue As String = "       0.00 " 'Transfer Value
    Dim _STHOARecTypeA6AdjustmentType As String = " "            'Adjustment Type
    Dim _STHOARecTypeA6DRLNumber As String = "000000" 'DRL Number    
    Dim _STHOARecTypeA6CommentText As String = "   "      'Drl Number

    Dim _STHOARecTypeA7SkuNumber As String ' Sku number                                           - Data +22  
    Dim _STHOARecTypeA7MarkupOrDownCode As String ' Markup/Down Code                                     - Data +28
    '                    ' 1 = Price Changes
    '                    ' 2 = Price Violations
    '                    ' 3 = Related Items
    '                    ' 4 = Event Erosions
    Dim _STHOARecTypeA7MarkupOrDownUnits As String '  7.0 Signed Numeric Units Mark Up/Down               - Data +29
    Dim _STHOARecTypeA7MarkupOrDownValue As String ' 10.2 Signed Numeric Value Mark Up/Down               - Data +37
    Dim _STHOARecTypeA7OldSellingPrice As String ' 10.2 Signed Numeric Old Price                        - Data +49
    Dim _STHOARecTypeA7NewSellingPrice As String ' 10.2 Signed Numeric New Price                        - Data +61
    Dim _STHOARecTypeA7BulkSkuNumber As String ' Bulk Sku number                                      - Data +73
    Dim _STHOARecTypeA7BulkUnitsAdjusted As String '  7.0 Signed Numeric Units Adjusted Bulk              - Data +79
    Dim _STHOARecTypeA7PriceOverrideReasonCode As String ' Price Override Reason Code                           - Data +87
    '                    ' 01 = Damaged Goods
    '                    ' 02 = Wrong Price On Shelf                    
    '                    ' 03 = Refund Price Change              
    '                    ' 04 = Wrong Price on Stock
    '                    ' 05 = Wrong P.O.S. on sign
    '                    ' 06 = Wrong Booklet Price
    '                    ' 07 = Deleted Item
    '                    ' 08 = Price Match
    '                    ' 09 = Deleted Item
    '                    ' 10 = Other
    '                    ' 53 = Damaged Goods Sale (from Mark-down stock)
    '                    ' 59 = HDC Stock Cleanse (from Mark-down stock)

    'Dim ahctgy As String
    'Dim ahvalu As String

    Dim strStockAdjustmentText As String  ' free form ref      - data +83

    ' Run as a batch file 
    Dim writer As StreamWriter
    Dim reader As StreamReader
    Dim TransmissionFileReader As StreamReader
    'Dim dfflag As Boolean = False

    'write data to ascii file 
    '   Type HD   Header Record                                         

    Dim strSthofRecordTypeFHDate As String = String.Empty        ' DATA+24  A8     Transaction date - dd/mm/yy  
    Dim strSthofRecordTypeFHStoreNumber As String = String.Empty        ' DATA+32  A3     Store Number    
    Dim strSthofRecordTypeFHNumberOfRecords As String = String.Empty        ' DATA+35  A7     6.0 Signed Numeric number of records

    '       Type 01  Sales Counts --- 4.0 Signed numeric                

    Dim _STHOFRecTypeF1SaleTransactionCount As String = String.Empty        ' DATA+24  A5     SA Count  (SALES          )         
    Dim _STHOFRecTypeF1SaleCorrectTransactionCount As String = String.Empty        ' DATA+29  A5     SC Count  (SALES CORRECT  )                   
    Dim _STHOFRecTypeF1RefundTransactionCount As String = String.Empty        ' DATA+34  A5     RF Count  (REFUNDS        )                   
    Dim _STHOFRecTypeF1RefundCorrectTransactionCount As String = String.Empty       ' DATA+39  A5     RC Count  (REFUND CORRECT )                   
    Dim _STHOFRecTypeF1MiscellaneousInTransactionCount As String = String.Empty      ' DATA+44  A5     MA Count  (MISC. AMOUNT   )                   
    Dim _STHOFRecTypeF1OpenDrawerTransactionCount As String = String.Empty       ' DATA+49  A5     OD Count  (OPEN DRAWER    )                   
    Dim _STHOFRecTypeF1VoidedTransactionCount As String        ' DATA+54  A5     VO Count  (VOIDS          )                   
    Dim _STHOFRecTypeF1PriceViolationTransactionCount As String = String.Empty       ' DATA+59  A5     PO Count  (PRICE OVERRIDES)                   

    '       Type 02  Sales Times  --- 24 Hr Clock HHMM (Time of last)   

    Dim _STHOFRecTypeF2SalesTransactionTime As String = String.Empty         ' DATA+24  A4     SA Time   (SALES          )
    Dim _STHOFRecTypeF2SalesCorrectTransactionTime As String = String.Empty         ' DATA+28  A4     SC Time   (SALES CORRECT  )
    Dim _STHOFRecTypeF2RefundTransactionTime As String = String.Empty       ' DATA+32  A4     RF Time   (REFUNDS        )
    Dim _STHOFRecTypeF2RefundCorrectTransactionTime As String = String.Empty       ' DATA+36  A4     RC Time   (REFUND CORRECT )
    Dim _STHOFRecTypeF2MiscellaneousInTransactionTime As String = String.Empty        ' DATA+40  A4     MA Time   (MISC. AMOUNT   )
    Dim _STHOFRecTypeF2OpenDrawerTransactionTime As String = String.Empty         ' DATA+44  A4     OD Time   (OPEN DRAWER    )
    Dim _STHOFRecTypeF2VoidedTransactionTime As String = String.Empty        ' DATA+48  A4     VO Time   (VOIDS          )
    Dim _STHOFRecTypeF2PriceViolationTransactionTime As String = String.Empty        ' DATA+52  A4     PO Time   (PRICE OVERRIDES)

    '    Sub-type 03  Sales Amounts --- 8.2 Signed numeric              

    Dim _STHOFRecTypeF3SalesTransactionAmount As String = String.Empty        ' DATA+24  A10    SA Amount (SALES          )
    Dim _STHOFRecTypeF3SalesCorrectTransactionAmount As String = String.Empty        ' DATA+34  A10    SC Amount (SALES CORRECT  )
    Dim _STHOFRecTypeF3RefundTransactionAmount As String = String.Empty        ' DATA+44  A10    RF Amount (REFUNDS        )
    Dim _STHOFRecTypeF3RefundCorrectTransactionAmount As String = String.Empty         ' DATA+54  A10    RC Amount (REFUND CORRECT )
    Dim _STHOFRecTypeF3MiscellaneousInTransactionAmount As String = String.Empty      ' DATA+64  A10    MA Amount (MISC. AMOUNT   )
    Dim _STHOFRecTypeF3PriceViolationTransactionAmount As String = String.Empty       ' DATA+74  A10    Price Violations amount
    '       Type 04  Tender Amounts -- 8.2 Signed numeric               


    Dim _STHOFRecTypeF4TenderCashAmount As String = String.Empty        ' DATA+24  A10    T1 Amount (CASH           )
    Dim _STHOFRecTypeF4TenderChequeAmount As String = String.Empty        ' DATA+34  A10    T2 Amount (CHEQUE         )
    Dim _STHOFRecTypeF4TenderMastercardAmount As String = String.Empty         ' DATA+44  A10    T3 Amount (ACCESS         )
    Dim _STHOFRecTypeF4TenderVisaAmount As String = String.Empty        ' DATA+54  A10    T4 Amount (VISA           )
    Dim _STHOFRecTypeF4TenderWickesAmount As String = String.Empty        ' DATA+64  A10    T5 Amount (WICKES         )
    Dim _STHOFRecTypeF4TenderVoucherAmount As String = String.Empty         ' DATA+74  A10    T6 Amount (VOUCHER        )
    Dim _STHOFRecTypeF4TenderAmexAmount As String = String.Empty        ' DATA+84  A10    T7 Amount (AMEX           )
    Dim _STHOFRecTypeF4TenderMaestroAmount As String = String.Empty        ' DATA+94  A10    T8 Amount (SWITCH         )
    Dim _STHOFRecTypeF4TenderBadChequeAmount As String = String.Empty        ' DATA+104 A10    T9 Amount (BAD CHEQUE     )
    Dim _STHOFRecTypeF4TenderTradeCreditAmount As String = String.Empty         ' DATA+114 A10    T10 Amount (TRADE CREDIT  )
    Dim _STHOFRecTypeF4TenderType11Amount As String = String.Empty        ' DATA+114 A10    T11 Amount 
    Dim _STHOFRecTypeF4TenderType12Amount As String = String.Empty        ' DATA+114 A10    T12 Amount
    Dim _STHOFRecTypeF4TenderType13Amount As String = String.Empty        ' DATA+114 A10    T13 Amount
    Dim _STHOFRecTypeF4TenderType14Amount As String = String.Empty        ' DATA+114 A10    T14 Amount
    Dim _STHOFRecTypeF4TenderType15Amount As String = String.Empty        ' DATA+114 A10    T15 Amount
    Dim _STHOFRecTypeF4TenderType16Amount As String = String.Empty        ' DATA+114 A10    T16 Amount
    Dim _STHOFRecTypeF4TenderType17Amount As String = String.Empty        ' DATA+114 A10    T17 Amount
    Dim _STHOFRecTypeF4TenderType18Amount As String = String.Empty        ' DATA+114 A10    T18 Amount
    Dim _STHOFRecTypeF4TenderType19Amount As String = String.Empty       ' DATA+114 A10    T19 Amount
    Dim _STHOFRecTypeF4TenderType20Amount As String = String.Empty        ' DATA+114 A10    T20 Amount

    '       Type 05  Tender Counts  -- 4.0 Signed numeric               

    Dim _STHOFRecTypeF5TenderCashCount As String = String.Empty       ' DATA+24  A5     T1 Count  (CASH           )
    Dim _STHOFRecTypeF5TenderChequeCount As String = String.Empty      ' DATA+29  A5     T2 Count  (CHEQUE         )
    Dim _STHOFRecTypeF5TenderMastercardCount As String = String.Empty         ' DATA+34  A5     T3 Count  (ACCESS         )
    Dim _STHOFRecTypeF5TenderVisaCount As String = String.Empty       ' DATA+39  A5     T4 Count  (VISA           )
    Dim _STHOFRecTypeF5TenderWickesCount As String = String.Empty         ' DATA+44  A5     T5 Count  (WICKES         )
    Dim _STHOFRecTypeF5TenderVoucherCount As String = String.Empty        ' DATA+49  A5     T6 Count  (VOUCHER        )
    Dim _STHOFRecTypeF5TenderAmexCount As String = String.Empty    ' DATA+54  A5     T7 Count  (AMEX           )
    Dim _STHOFRecTypeF5TenderMaestroCount As String = String.Empty         ' DATA+59  A5     T8 Count  (SWITCH         )
    Dim _STHOFRecTypeF5TenderBadChequeCount As String = String.Empty        ' DATA+64  A5     T9 Count  (BAD CHEQUE     )
    Dim _STHOFRecTypeF5TenderTradeCreditCount As String = String.Empty         ' DATA+69  A5     T10 Count  (TRADE CREDIT  )

    '       Type 06 Nightly         --   P E R S O N E L    D A T A     

    Dim _STHOFRecTypeF6DailyHoursPaid As String = String.Empty        ' DATA+24  A6     5.0 Signed numeric Daily Hours Paid

    '       Type 07 Nightly         --   S A L E S    D A T A

    Dim _STHOFRecTypeF7ConservatorySales As String = String.Empty       ' DATA+24  A4     3.0 Signed numeric Conservatory sale
    Dim _STHOFRecTypeF7FrontEndSalesValue As String = String.Empty        ' DATA+28  A10    Front end sales value			
    Dim _STHOFRecTypeF7FrontEndSalesCount As String = String.Empty       ' DATA+38  A7     Front end sales count		
    Dim _STHOFRecTypeF7FrontEndRefundsValue As String = String.Empty       ' DATA+45  A10    Front end refund value		        
    Dim _STHOFRecTypeF7FrontEndRefundsCount As String = String.Empty        ' DATA+55  A10    Front end refund count		        
    Dim _STHOFRecTypeF7FrontEndSalesAndRefundVoucherValue As String = String.Empty        ' DATA+62  A10    Sales and refund voucher value	        
    Dim _STHOFRecTypeF7FrontEndSalesAndRefundVoucherCount As String = String.Empty         ' DATA+72  A7     Sales and refund voucher count	        
    Dim _STHOFRecTypeF7FrontEndSalesAndRefundDeliveryValue As String = String.Empty       ' DATA+79  A10    Sales and delivery value		
    Dim _STHOFRecTypeF7FrontEndSalesAndRefundInstallationValue As String = String.Empty       ' DATA+89  A10    Sales and refund Installation Value	
    Dim _STHOFRecTypeF7TotalDeliveriesCount As String = String.Empty       ' DATA+99  A06    Deliveries (Number)                  
    Dim _STHOFRecTypeF7TotalDeliveriesValue As String = String.Empty       ' DATA+105 A11    Deliveries (Value)                   
    Dim _STHOFRecTypeF7TotalDeliveryChargeValue As String = String.Empty         ' DATA+116 A11    Delivery Charge (Value)              
    Dim _STHOFRecTypeF7TotalDeliveryToSalesPercentage As String = String.Empty        ' DATA+127 A07    Delivery % To Sales                  

    '       Type 08 Nightly         --   STOCK   CONTROL   DATA         

    Dim _STHOFRecTypeF8NumberOfSkusOutOfStock As String = String.Empty       ' DATA+25  A5     5.0 Signed numeric No.SKU Out Stock 
    Dim _STHOFRecTypeF8TodaysType2and10Adjustments As String = String.Empty        ' DATA+30  A9     7.2 Signed numeric Today's 2 & 10 adjusts
    Dim _STHOFRecTypeF8TodaysDailyReceiverListingValue As String = String.Empty       ' DATA+39  A11    9.2 Signed numeric Today's DRL Actual    
    Dim _STHOFRecTypeF8ThisWeekToDateDailyReceiverListingValue As String = String.Empty        ' DATA+52  A11    9.2 Signed numeric W.T.D.  DRL Actual    
    Dim _STHOFRecTypeF8NumberOfUnappliedPriceChanges As String = String.Empty        ' DATA+61  A7     Number of outstanding price changes	
    Dim _STHOFRecTypeF8WeeklyMarkdownsValue As String = String.Empty       ' DATA+68  A10    Value of weekly markdowns		            

    '       Type 09 WEEKLY          --   P E R S O N E L    D A T A     

    Dim _STHOFRecTypeF9WeeklyWagesValue As String = String.Empty        ' DATA+24  A9     7.2 Signed numeric Weekly Wages    
    Dim _STHOFRecTypeF9StandardHours As String = String.Empty        ' DATA+33  A6     5.0 Signed numeric Standard Hours  
    Dim _STHOFRecTypeF9NumberOfStarters As String = String.Empty       ' DATA+39  A4     3.0 Signed numeric Starters        
    Dim _STHOFRecTypeF9NumberOfLeavers As String = String.Empty        ' DATA+43  A4     3.0 Signed numeric Leavers         
    Dim _STHOFRecTypeF9NumberOfFullTimeStaff As String = String.Empty        ' DATA+47  A4     3.0 Signed numeric Full Time Staff 
    Dim _STHOFRecTypeF9NumberOfPartTimeStaff As String = String.Empty        ' DATA+51  A4     3.0 signed numeric Part Time Staff 

    '       Type 10 WEEKLY          ---   S A L E S    D A T A          

    Dim strSthofRecordTypeFAMarkdownsValue As String = String.Empty        ' DATA+24  A11    9.2 Signed numeric Mark Downs

    '       Type 11 WEEKLY          --   STOCK   CONTROL   DATA         

    Dim strSthofRecordTypeFBOpenPurchaseOrdersValue As String = String.Empty         ' DATA+24  A11    9.2 Signed numeric Open P/O Value  
    Dim strSthofRecordTypeFBStockValue As String = String.Empty      ' DATA+35  A11    9.2 Signed numeric Stock Value

    ' Now Define Work Areas for STHPO

    Dim strSthpoFileName As String ' = "C:\COMMS\STHPO.TXT"
    Dim strSthojFileName As String '= '"C:\COMMS\STHOJ.TXT"
    Dim strSthoyFileName As String ' = "C:\COMMS\STHOY.TXT"

    Dim DlpaidCoupon As String

    Dim _PostCodeFileName As String ' = "C:\COMMS\PCODE" & RetoptStoreNumber.ToString.PadLeft(3, "0")
    Dim strSthpaFileName As String '= "C:\COMMS\STHPA.TXT"
    Dim strSthotFileName As String '= "C:\COMMS\STHOT.TXT"

    Dim arrstrU1NumericValidation() As String = New String(32) {" ", "022003", "026006", "033006", "039006", "053005", "068003", "085003", "088006", "167001", "174005", "192013", "205007", "212007", "220005", "225004", "229005", "234004", "238004", "242004", "248007", "271006", "294006", "300006", "306006", "312006", "318006", "324002", "327002", "329002", "333012", "358003", "364006"}
    Dim arrstrU5NumericValidation() As String = New String(3) {" ", "022003", "028006", "033003"}
    Dim arrstrU6NumericValidation() As String = New String(3) {" ", "022006", " ", " "}
    Dim arrstrUANumericValidation() As String = New String(3) {" ", "022006", "028006", " "}
    Dim arrstrUBNumericValidation() As String = New String(4) {" ", "022006", "068002", "078004", "090004"}
    Dim arrstrUCNumericValidation() As String = New String(8) {" ", "022006", "030006", "036008", "044010", "054007", "061007", "068010", "078007"}
    Dim arrstrUDNumericValidation() As String = New String(6) {" ", "022006", "028006", "034008", "040006", "046007", "063007"}
    Dim arrstrUENumericValidation() As String = New String(3) {" ", "022006", "028006", "034006"}
    Dim arrstrOANumericValidation() As String = New String(8) {" ", "022006", "028006", "034005", "039006", "045004", "049007", "056007", "063002"}
    Dim arrstrOCNumericValidation() As String = New String(4) {" ", "022006", "028005", "042002", "044006"}

    Dim arrstrU1IndicatorValidation() As String = New String(10) {" ", "219", "246", "247", "326", "332", "354", "355", "357", "361", "362"}
    Dim arrstrU5IndicatorValidation() As String = New String(3) {" ", "027", " ", " "}
    Dim arrstrU6IndicatorValidation() As String = New String(3) {" ", " ", " ", " "}
    Dim arrstrUAIndicatorValidation() As String = New String(3) {" ", "034", " ", " "}
    Dim arrstrUBIndicatorValidation() As String = New String(8) {" ", "094", "095", "096", "097", "098", "099", "100", "101"}
    Dim arrstrUCIndicatorValidation() As String = New String(3) {" ", "085", " ", " "}
    Dim arrstrUDIndicatorValidation() As String = New String(3) {" ", "070", " ", " "}
    Dim arrstrUEIndicatorValidation() As String = New String(3) {" ", "040", " ", " "}

    Dim strSthofFileName As String = String.Empty
    Dim strStholPathName As String = String.Empty
    Dim strStholFileName As String = String.Empty
    Dim strSthocFileName As String = String.Empty
    Dim intSthocRecordsOut As Integer = 0
    Dim intSthoaRecordsOut As Integer = 0
    Dim intSthpaRecordsOut As Integer = 0
    Dim intSthpoRecordsOut As Integer = 0
    Dim intRecordsOutput As Integer = 0
    Dim intVersionNumber As Integer  ' Version number to use for this transmission
    Dim intSequenceNumber As Integer ' Sequence number to use for this transmission

    Dim boolOutputErrorToSthoa As Boolean = False
    Dim boolValidatedOk As Boolean = True
    Dim boolDoMarkups As Boolean = False
    Dim boolFlagPriceChanges As Boolean = False
    Dim boolDoReceipts As Boolean = False
    Dim boolDoAdjustments As Boolean = False
    Dim boolDoSales As Boolean = False
    Dim boolResetSales As Boolean = False
    Dim boolDoBanking As Boolean = False

    'Dim periodenterd As String
    Dim dateEntered As String

    Dim boolDoJDAMarkups As Boolean = False
    Dim boolDoJDACompare As Boolean = False
    Dim boolDoPrepareSthot As Boolean = False
    Dim boolDoPrepareSthof As Boolean = False
    Dim boolDoPreparePurchaseOrders As Boolean = False
    Dim boolDoPrepareCoupons As Boolean = False
    Dim boolDoPreparePostCodes As Boolean = False
    Dim boolDoPrepareSthpaRefunds As Boolean = False
    Dim boolDoSthpaOutOfStocks As Boolean = False
    Dim boolDoPrepareSthol As Boolean = False
    Dim boolDoPrepareRelatedItems As Boolean = False
    Dim boolRunGsbuhl As Boolean = False
    Dim boolRunTibrsh As Boolean = False
    Dim boolDoCloseTransmissionFiles As Boolean = False

    Dim strTobhotParameterFileName As String
    Dim strTobhotParameterWorkFileName As String = String.Empty
    Dim strPendingHpstvFileName As String = String.Empty
    Dim strPendingHpstvWorkFileName As String = String.Empty
    Dim arrTobhotInputLines As String() = New String(99999) {}
    Dim boolGotTobhotParameters As Boolean
    Dim intTobhotTypes As Integer
    Dim strNetId As String = String.Empty
    Dim strNetIdPath As String = String.Empty
    Dim strToRtiPath As String = String.Empty
    Dim strToRtiFile As String = String.Empty
    Dim strFromRtiPath As String = String.Empty
    Dim strFromRtiFile As String = String.Empty
    Dim strWorkPath As String = String.Empty
    Dim strTvcPath As String = String.Empty
    Dim strTvcFile As String = String.Empty
    Dim strTransmissionsPath As String = String.Empty
    Dim strSaveTransmissionFilePath As String = String.Empty
    Dim strNetWorkPath As String = String.Empty
    Dim strSearchPath As String = String.Empty
    Dim strSthocText As String = String.Empty
    Dim strSthofText As String = String.Empty
    Dim strSthpoText As String = String.Empty

    Dim strDeleteSupplierNumber As String = String.Empty
    Dim strDeleteSupplierType As String = String.Empty
    Dim strPendingHpstvText As String = String.Empty
    Dim boolPassedValidation As Boolean = False
    Dim boolIsBlankDate As Boolean = True
    Dim boolBlankStartDate As Boolean = False
    Dim boolBlankEndDate As Boolean = False
    Dim boolBlankStartDate1 As Boolean = False
    Dim boolBlankEndDate1 As Boolean = False
    Dim dateLastHpstvVqRecordUpdated As Date = Date.MinValue
    Dim dateLastHpstvVtRecordUpdated As Date = Date.MinValue
    Dim strLastHpstvVqSupplierUpdated As String = String.Empty
    Dim strLastHpstvVtSupplierUpdated As String = String.Empty
    ' Saved suppler details in case reset required - H/O type "S"
    Dim strSavedHeadOfficeMcpType As String = String.Empty
    Dim decSavedHeadOfficeMcpValue As Decimal = 0
    Dim decSavedHeadOfficeMcpUnits As Decimal = 0
    Dim decSavedHeadOfficeMcpWeight As Decimal = 0
    Dim decSavedHeadOfficeTruckWeight As Decimal = 0
    Dim decSAvedHeadOfficeTruckVolume As Decimal = 0
    Dim decSavedHeadOfficeTruckPallets As Decimal = 0
    Dim boolSavedHeadOfficeTradanet As Boolean = False
    Dim strSavedHeadOfficeBBC As String = String.Empty
    Dim decSavedHeadOfficeLeadTime0 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime1 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime2 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime3 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime4 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime5 As Decimal = 0
    Dim decSavedHeadOfficeLeadTime6 As Decimal = 0
    Dim decSavedHeadOfficeLeadTimeFixed As Decimal = 0
    Dim decSavedHeadOfficeSoqFrequency As Decimal = 0
    Dim blnReviewDay0 As Boolean
    Dim blnReviewDay1 As Boolean
    Dim blnReviewDay2 As Boolean
    Dim blnReviewDay3 As Boolean
    Dim blnReviewDay4 As Boolean
    Dim blnReviewDay5 As Boolean
    Dim blnReviewDay6 As Boolean
    Dim _SavedHODeliveryCheck As String = String.Empty

    ' Saved suppler details in case reset required - Ordering Depot type "O"
    Dim strSavedOrderDepotDepotNumber As String = String.Empty
    Dim strSavedOrderDepotMcpType As String = String.Empty
    Dim decSavedOrderDepotMcpValue As Decimal = 0
    Dim decSavedOrderDepotMcpUnits As Decimal = 0
    Dim decSavedOrderDepotMcpWeight As Decimal = 0
    Dim decSavedOrderDepotTruckWeight As Decimal = 0
    Dim decSAvedOrderDepotTruckVolume As Decimal = 0
    Dim decSavedOrderDepotTruckPallets As Decimal = 0
    Dim boolSavedOrderDepotTradanet As Boolean = False
    Dim strSavedOrderDepotBBC As String = String.Empty
    Dim decSavedOrderDepotLeadTime0 As Decimal = 0
    Dim decSavedOrderDepotLeadTime1 As Decimal = 0
    Dim decSavedOrderDepotLeadTime2 As Decimal = 0
    Dim decSavedOrderDepotLeadTime3 As Decimal = 0
    Dim decSavedOrderDepotLeadTime4 As Decimal = 0
    Dim decSavedOrderDepotLeadTime5 As Decimal = 0
    Dim decSavedOrderDepotLeadTime6 As Decimal = 0
    Dim decSavedOrderDepotLeadTimeFixed As Decimal = 0
    Dim decSavedOrderDepotSoqFrequency As Decimal = 0
    Dim strSavedOrderDepotDeliveryCheck As String = String.Empty

    ' Saved supplier details in case reset required - Returns Depot type "R"

    Dim strSavedReturnsDepotMcpType As String = String.Empty
    Dim decSavedReturnsDepotMcpValue As Decimal = 0
    Dim decSavedReturnsDepotMcpUnits As Decimal = 0
    Dim decSavedReturnsDepotMcpWeight As Decimal = 0
    Dim decSavedReturnsDepotTruckWeight As Decimal = 0
    Dim decSAvedReturnsDepotTruckVolume As Decimal = 0
    Dim decSavedReturnsDepotTruckPallets As Decimal = 0
    Dim boolSavedReturnsDepotTradanet As Boolean = False
    Dim strSavedReturnsDepotBBC As String = String.Empty
    Dim decSavedReturnsDepotLeadTime0 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime1 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime2 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime3 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime4 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime5 As Decimal = 0
    Dim decSavedReturnsDepotLeadTime6 As Decimal = 0
    Dim decSavedReturnsDepotLeadTimeFixed As Decimal = 0
    Dim decSavedReturnsDepotSoqFrequency As Decimal = 0
    Dim strSavedReturnsDepotDeliveryCheck As String = String.Empty
    Dim strSthoLogFileName As String = String.Empty
    Dim strTibhusWorkFile As String = String.Empty
    Dim strTibhueWorkFile As String = String.Empty
    Dim strTibhueText As String = String.Empty
    Dim strTibhusText As String = String.Empty
    Dim strWorkString As String = String.Empty
    Dim intPassNumber As Integer = 0
    Dim _NoRecInTibhus As Integer = 0
    Dim _NoRecInTibhue As Integer = 0
    Dim strRecordType As String = String.Empty
    Dim boolFlagAsCommed As Boolean = True
    Dim boolProcessHosta As Boolean = False
    Dim boolProcessHostu As Boolean = False
    Dim boolProcessHostc As Boolean = False
    Dim boolProcessHostd As Boolean = False
    Dim boolProcessHostf As Boolean = False
    Dim boolProcessHostg As Boolean = False
    Dim boolProcessHosth As Boolean = False
    Dim boolProcessHpsti As Boolean = False
    Dim boolProcessHpsto As Boolean = False
    Dim boolProcessHpstv As Boolean = False
    Dim boolProcessTibpfv As Boolean = False
    Dim intMaximumDayNumberInCycle As Integer = 0 ' For HOSTC
    Dim intNumberOfWeeksInCycle As Integer = 0 ' For HOSTC
    Dim intWorkDayNumberInCycle As Integer = 0 ' For HOSTC
    Dim boolRelatedItemsUpdateRequired As Boolean = False
    Dim boolAddShocEndOfDayReord As Boolean = False
    Dim boolCheckItemPrompts As Boolean = False
    Dim strReportSpoolPath As String = String.Empty
    Dim strTibhpoReportPath As String = String.Empty
    Dim strReportText As String = String.Empty
    Dim intReportPageNumber As Integer = 0
    Dim intReportLineCount As Integer = 0
    Dim _ExpectingOARecords As Boolean = False
    Dim strExpectedPONumber As String = String.Empty
    Dim strExpectedSupplierNumber As String = String.Empty
    Dim strTransmissionFileData As String ' Holds the record data from the file

#End Region

    Public Sub Main() ' Main body of program

        TestEnvironmentSetup.SetupIfTestRun()

        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

        Dim intX As Integer = 0
        Dim intZ As Integer = 0
        Dim intZ1 As Integer = 0
        Dim intZ2 As Integer = 0
        Dim arrstrClosePaths As String() = New String(99) {}

        'Dim ret
        Dim strWorkString As String = String.Empty
        Dim oParam As New BOSystem.cParameter(_Oasys3DB)
        dateStartTime = TimeOfDay
        intSthocRecordsOut = 0
        intSthoaRecordsOut = 0
        intSthpaRecordsOut = 0
        intSthpoRecordsOut = 0


        ' set up path names etc for the transmission files
        If (GlobalVars.IsTestEnvironment) Then
            strTransmissionsPath = GlobalVars.SystemEnvironment.GetCommsDirectoryPath()
        Else
            strTransmissionsPath = oParam.GetParameterString(914)
        End If


        strTransmissionsPath = strTransmissionsPath.TrimEnd("\"c)
        Trace.WriteLine("Tx Path is:" & strTransmissionsPath)

        strToRtiPath = strTransmissionsPath & "\TORTI"
        strFromRtiPath = strTransmissionsPath & "\FROMRTI"
        strSaveTransmissionFilePath = strTransmissionsPath & "\SAVE"
        My.Computer.FileSystem.CreateDirectory(strTransmissionsPath) ' Ensure folder exists
        My.Computer.FileSystem.CreateDirectory(strToRtiPath) ' Ensure folder exists
        My.Computer.FileSystem.CreateDirectory(strSaveTransmissionFilePath) ' Ensure folder exists

        strSthoaFileName = strTransmissionsPath & "\STHOA"
        strSthocFileName = strTransmissionsPath & "\STHOC"
        strSthofFileName = strTransmissionsPath & "\STHOF"
        strSthojFileName = strTransmissionsPath & "\STHOJ"
        strSthotFileName = strTransmissionsPath & "\STHOT"
        strSthoyFileName = strTransmissionsPath & "\STHOY"
        strSthpoFileName = strTransmissionsPath & "\STHPO"
        strSthpaFileName = strTransmissionsPath & "\STHPA"
        strStholPathName = strTransmissionsPath & "\STHOL"
        strPendingHpstvFileName = strTransmissionsPath & "\HPSTV"
        strPendingHpstvWorkFileName = strTransmissionsPath & "\HPSTV.WRK"
        _PostCodeFileName = strTransmissionsPath & "\PCODE"
        'strSthoLogFileName = strTransmissionsPath & "\STHO.LOG"
        strSthoLogFileName = strTransmissionsPath & "\ST" & Mid(Today.Year.ToString, 3, 2).PadLeft(2, "0"c) & Today.Month.ToString.PadLeft(2, "0"c) & Today.Day.ToString.PadLeft(2, "0"c) & ".LOG"
        strTobhotParameterFileName = strTransmissionsPath & "\TOBHOT"
        strTobhotParameterWorkFileName = "C:\TOBHOT"

        ' Set up and log start time in log file - create a new log every time

        'If File.Exists(strSthoLogFileName) Then
        '    My.Computer.FileSystem.DeleteFile(strSthoLogFileName)
        'End If
        strWorkString = "Processing Started : " & dateStartTime.Hour.ToString.PadLeft(2, "0"c) & ":" & dateStartTime.Minute.ToString.PadLeft(2, "0"c) & ":" & dateStartTime.Second.ToString.PadLeft(2, "0"c) '& vbCrLf
        UpdateProgress("Please Wait - Initialisation In Progress", String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'Replace NETID mechanism with Registry Key
        _WorkstationID = CStr(My.Computer.Registry.GetValue("HKEY_local_machine\software\cts retail\default\oasys\2.0\wickes\enterprise", "WorkstationID", "25"))
        _WorkstationID = _WorkstationID.Substring(0, 2).PadLeft(2, "0"c)

        ' get NETWORK ID of this workstation - USING NET_WSID .... THIS MAY NEED TO CHANGE
        strNetIdPath = strTransmissionsPath        ' 

        intZ = strNetIdPath.Length - 1
        intZ1 = intZ - 1
        'For intZ1 = 1 To intZ
        '    If Mid(strNetIdPath, intZ1, 1) = "\" Then
        For intZ1 = 0 To intZ
            If strNetIdPath.Substring(intZ1, 1) = "\" Then
                intZ2 = intZ1 + 1
            End If
        Next
        'strWorkString = Mid(strNetIdPath, 1, intZ2)
        strWorkString = strNetIdPath.Substring(0, intZ2)
        strNetWorkPath = String.Empty & strWorkString.TrimEnd("\"c)
        strNetIdPath = String.Empty & strWorkString & "NET_WSID"
        strReportSpoolPath = String.Empty & strWorkString & "REPORT"

        strSearchPath = String.Empty & strNetWorkPath
        intZ = strSearchPath.Length - 1
        intZ1 = intZ - 1
        'For intZ1 = 1 To intZ
        '    If Mid(strSearchPath, intZ1, 1) = "\" Then
        For intZ1 = 0 To intZ
            If strSearchPath.Substring(intZ1, 1) = "\" Then
                intZ2 = intZ1 + 4
            End If
        Next
        'strSearchPath = String.Empty & Mid(strSearchPath, 1, (intZ2 - 1))
        strSearchPath = String.Empty & strSearchPath.Substring(0, (intZ2))
        If File.Exists(strNetIdPath) Then
            reader = New StreamReader(strNetIdPath, True)
            strNetId = reader.ReadLine
            reader.Close()
        End If
        strTibhueWorkFile = strTransmissionsPath & "\TIBHUE." & _WorkstationID
        strTibhusWorkFile = strTransmissionsPath & "\TIBHUS." & _WorkstationID

        Dim Retopt As New BOSystem.cRetailOptions(_Oasys3DB)
        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadField(Retopt.StoreName)
        Retopt.AddLoadField(Retopt.NoVATrates)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()
        _StoreNumber = Retopt.Store.Value.ToString.PadLeft(3, "0"c)
        _StoreName = Retopt.StoreName.Value.PadRight(30, " "c)
        _NoOfVATRates = CInt(Retopt.NoVATrates.Value)

        Retopt.Dispose()

        ' get command parameters
        _RunningInNight = True

        'Added 1/6/09 - check if in Retry Mode and set Running in Night, if not in Retry
        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)
        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadField(SysDates.RetryMode)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()
        _RunningInNight = (Not SysDates.RetryMode.Value)
        SysDates.Dispose()

        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            ' Input ALL parameters 
            strCmd = cmd.ToUpper
            'If Mid(strCmd, 1, 14) = "FLAGASCOMMED=Y" Then
            If strCmd.StartsWith("FLAGASCOMMED=Y") Then
                boolFlagAsCommed = True
            End If
            If strCmd.StartsWith("FLAGASCOMMED=N") Then
                boolFlagAsCommed = False
            End If
            If strCmd.StartsWith("ASKDATE") Then
                _PromptForDate = True
            End If
            If strCmd.StartsWith("SOCLOS") Then ' Prepare Transmission file for RTI
                boolDoCloseTransmissionFiles = True
                'Build up list of Files/paths to close
                Array.Clear(arrstrClosePaths, 0, 99)
                intZ1 = strCmd.Length
                intZ2 = 7
                intZ = 0
                'step through list and append to Close List
                While intZ1 > 7 And intZ2 < intZ1
                    intZ = intZ + 1
                    arrstrClosePaths(intZ) = strCmd.Substring(intZ2, 5)
                    intZ2 = intZ2 + 6
                End While
                ReDim Preserve arrstrClosePaths(intZ)
            End If ' Prepare Transmission file for RTI
            If strCmd.StartsWith("SDATE") Then ' Start date has been requested
                _StartDateToUse = CDate(strCmd.Substring(6, 8))
                _RunningInNight = False
            End If ' Start date has been requested
            If strCmd.StartsWith("EDATE") Then ' END date has been requested
                _EndDateToUse = CDate(strCmd.Substring(6, 8))
                _RunningInNight = False
            End If ' End date has been requested
            If strCmd.StartsWith("PDATE") Then ' a SINGLE processing date has been requested
                _StartDateToUse = CDate(strCmd.Substring(6, 8))
                _EndDateToUse = CDate(strCmd.Substring(6, 8))
                _RunningInNight = False
                _DatePassedIn = True
            End If ' a SINGLE processing date has been requested
            ' get data from table 

            ''check




            Select Case strCmd
                Case "NIGHT=Y" : _RunningInNight = True
                Case "NIGHT=N" : _RunningInNight = False
                Case "MU" : boolDoMarkups = True
                Case "DR" : boolDoReceipts = True
                Case "AD" : boolDoAdjustments = True
                Case "SA" : boolDoSales = True
                    'Case "DB" : boolDoBanking = True
                Case "EE" : boolDoJDAMarkups = True
                Case "OT" : boolDoPrepareSthot = True
                Case "OF" : boolDoPrepareSthof = True
                Case "PO" : boolDoPreparePurchaseOrders = True
                Case "OJ" : boolDoJDACompare = True
                Case "OE" : boolDoPrepareCoupons = True
                Case "OC" : boolDoPreparePostCodes = True
                Case "RF" : boolDoPrepareSthpaRefunds = True
                Case "SP" : boolDoSthpaOutOfStocks = True
                Case "OL" : boolDoPrepareSthol = True
                Case "RI" : boolDoPrepareRelatedItems = True
                Case "ER" : boolAddShocEndOfDayReord = True
                Case "HOSTA" : boolProcessHosta = True
                Case "HOSTU" : boolProcessHostu = True
                Case "HOSTC" : boolProcessHostc = True
                Case "HOSTD" : boolProcessHostd = True
                Case "HOSTF" : boolProcessHostf = True
                Case "HOSTG" : boolProcessHostg = True
                Case "HOSTH" : boolProcessHosth = True
                Case "HPSTI" : boolProcessHpsti = True
                Case "HPSTO" : boolProcessHpsto = True
                Case "HPSTV" : boolProcessHpstv = True
                Case "TIBPFV" : boolProcessTibpfv = True
                Case "GSBUHL" : boolRunGsbuhl = True
                Case "TIBRSH" : boolRunTibrsh = True
                    'Case "RIBUPD" : boolRelatedItemsUpdateRequired = True
                Case "TIBIPR" : boolCheckItemPrompts = True
            End Select


            Dim periodchk As String() = New String(20) {}


            If strCmd.StartsWith("DB,") Then
                periodchk = strCmd.Split(","c)
                boolDoBanking = True
                dateEntered = periodchk(1)
            End If


        Next ' ALL PARAMETERS INPUT

        If boolDoPrepareSthot = True Then ' SPECIAL PROCESSES FOR STHOT
            boolGotTobhotParameters = False
            If File.Exists(strTobhotParameterFileName) Then ' May have control parameters
                reader = New StreamReader(strTobhotParameterFileName, True)
                boolGotTobhotParameters = True
            End If
            If boolGotTobhotParameters = True Then
                Array.Clear(arrTobhotInputLines, 0, 99999)
                intX = 0
                While reader.EndOfStream = False
                    intX = intX + 1
                    arrTobhotInputLines(intX) = reader.ReadLine.PadRight(50, " "c)
                End While
                reader.Close()
            End If
            If intX > 0 Then
                ReDim Preserve arrTobhotInputLines(intX)
                For y = 1 To intX
                    If arrTobhotInputLines(y).ToUpper.StartsWith("COMPLETED") And arrTobhotInputLines(y).Substring(11, 10) <> "          " Then
                        boolGotTobhotParameters = False
                        Exit For
                    End If
                    If arrTobhotInputLines(y).ToUpper.StartsWith("COMPLETED") Then
                        arrTobhotInputLines(y) = String.Empty
                    End If
                    If arrTobhotInputLines(y).ToUpper.StartsWith("PATH") Then
                        strSthotFileName = String.Empty & strTransmissionsPath '& "\" & Mid(arrTobhotInputLines(y), 11, z)
                        intZ = strSthotFileName.Length - 1
                        intZ1 = intZ - 1
                        For intZ1 = 0 To intZ
                            If strSthotFileName.Substring(intZ1, 1) = "\" Then
                                intZ2 = intZ1 + 1
                            End If
                        Next
                        strWorkString = strSthotFileName.Substring(0, intZ2)
                        intZ = arrTobhotInputLines(y).Length - 12
                        strSthotFileName = String.Empty & strWorkString & arrTobhotInputLines(y).ToUpper.Substring(11, intZ)
                    End If
                    If arrTobhotInputLines(y).ToUpper.StartsWith("START") Then
                        dateTobhotStartDate = CDate(arrTobhotInputLines(y).Substring(11, 8))
                    End If
                    If arrTobhotInputLines(y).ToUpper.StartsWith("END") Then
                        dateTobhotEndDate = CDate(arrTobhotInputLines(y).Substring(11, 8))
                    End If
                    If arrTobhotInputLines(y).ToUpper.StartsWith("TYPES") Then
                        Array.Clear(arrstrTobHotTypes, 1, 20)
                        intZ = 0
                        intZ1 = 9
                        intZ2 = arrTobhotInputLines(y).Length - 10
                        While intZ1 < intZ2
                            intZ = intZ + 1
                            intZ1 = intZ1 + 2
                            arrstrTobHotTypes(intZ) = arrTobhotInputLines(y).ToUpper.Substring(intZ1, 2)
                        End While
                        intTobhotTypes = intZ
                    End If
                Next
            End If ' STHOT Control parameters processed
        End If

        If (boolRunTibrsh = True) Then 'added 21/8/09-M.Milne to check SYSOPT:PHIE is switch On
            Dim SysOpt As New BOSystem.cSystemOptions(_Oasys3DB)
            SysOpt.AddLoadField(SysOpt.RecreatePHL)
            SysOpt.LoadMatches()
            If (SysOpt.RecreatePHL.Value = False) Then
                MessageBox.Show("No need to run this program.", "Run TIBRSH Disabled", MessageBoxButtons.OK)
                End
            End If
            SysOpt.Dispose()
        End If

        ReDim Preserve arrTobhotInputLines(intX)

        If _StartDateToUse > _EndDateToUse Then
            ' Ensure START date is lower than end date
            dateHashDate = _EndDateToUse
            _EndDateToUse = _StartDate
            _StartDateToUse = dateHashDate
        End If
        If boolDoJDACompare = True Or boolDoSthpaOutOfStocks = True Or boolDoSales = True Then
            ' Output to STHOJ for JDACompare - Record type "JD"
            ' Output to STHPA for Out Of Stocks - Record type "AO" & "AS"
            ' Output to STHOA for Out Of SALES - Record type "A5"
            If boolDoSales = True Then
                boolResetSales = True
            End If
            ProcessStockMaster()
        End If
        If boolDoPrepareRelatedItems = True Then
            ' Output to STHOA for Related Items - Record type "A7"
            OutputRelitmSthoa()
        End If
       

        If boolDoBanking = True Then
            Dim banking As New OutputBanking(strSthoaFileName)
            banking.Process(CDate(dateEntered), False)
        End If

        If boolDoReceipts = True Then ProcessReceipts()
        If boolDoMarkups = True Then
            ' Output to STHOA for Price Change Markups - Record type "A7"
            boolFlagPriceChanges = True
            OutputMarkups()
        End If
        If boolDoPrepareSthpaRefunds = True Or boolDoJDAMarkups = True Or boolDoPrepareCoupons = True Or boolDoPrepareSthot = True Or boolDoPrepareSthof = True Or boolDoPreparePostCodes = True Then
            ProcessDlRecords()
        End If
        If boolDoAdjustments = True Then ProcessAdjustments()
        If boolDoPreparePurchaseOrders = True Then ProcessPurchaseOrders()
        If boolDoPrepareSthol = True Then OutputSthol()
        If boolProcessHosta = True Then ImportCardList.ProcessProductUpdates(strTransmissionsPath)
        If boolProcessHostu = True Then ProcessProductUpdates()
        If boolProcessHostc = True Then ProcessCountCycleUpdates()
        If boolProcessHostd = True Then ProcessStoreMasterUpdates()
        If boolProcessHostf = True Then ProcessPlangramUpdates()
        If boolProcessHostg = True Then ProcessHOSTG.ProcessHotCardList(strTransmissionsPath, strSthocFileName)
        If boolProcessHosth = True Then
            If (ProcessHierarchyUpdates() = True) Then
                'Only Run Update programs if files successfully processed
                boolRunGsbuhl = True
                boolRunTibrsh = True
            End If
        End If
        If boolProcessHpstv = True Then ProcessSupplierUpdates(False)
        If boolProcessTibpfv = True Then ProcessSupplierUpdates(True)
        If boolProcessHpsti = True Then ProcessBBCIssues()
        If boolProcessHpsto = True Then ProcessOrderConfirmations()
        'If boolRelatedItemsUpdateRequired = True Then ProcessRelatedItemsUpdates()
        If boolAddShocEndOfDayReord = True Then AddSthocEndOfDayRecord()
        CheckPostCodeFile(Retopt.Store.Value)
        EnsureAllDataOutput()
        FlagAsProcessed()
        If boolCheckItemPrompts = True Then CheckItemPrompts()
        If boolRunGsbuhl = True Then UpdateHierarchyFiles()
        If boolRunTibrsh = True Then ResetStockHierarchyIndex()

        If (boolDoCloseTransmissionFiles = True) Then ' Prepare Transmission file for RTI
            If arrstrClosePaths.Count > 0 Then 'any file names/paths to close
                For intZ = 1 To (arrstrClosePaths.Count - 1)
                    CloseTransmissionFile(arrstrClosePaths(intZ))
                Next
            End If
        End If ' Prepare Transmission file for RTI

        dateEndTime = TimeOfDay
        strWorkString = "Processing Ended : " & dateEndTime.Hour.ToString.PadLeft(2, "0"c) & ":" & dateEndTime.Minute.ToString.PadLeft(2, "0"c) & ":" & dateEndTime.Second.ToString.PadLeft(2, "0"c) '& vbCrLf
        OutputSthoLog(strWorkString)
        'Kill the TASKCOMP file if it exists
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

    End Sub  ' Main body of program

    Private Sub CheckPostCodeFile(ByVal StoreNumber As String)
        If (My.Computer.FileSystem.FileExists(_PostCodeFileName) = True) AndAlso (My.Computer.FileSystem.FileExists(_PostCodeFileName & StoreNumber) = False) Then
            My.Computer.FileSystem.RenameFile(_PostCodeFileName, "PCODE" & StoreNumber)
        End If
    End Sub

    Public Sub EnsureAllDataOutput() ' Ensure that all data is output 
        If strReportText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:REPORT-" & strTibhpoReportPath)
            PutSthoToDisc(strTibhpoReportPath, strReportText)
            strReportText = String.Empty
        End If
        If strSthoaText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:STHOA-" & strSthoaFileName)
            PutSthoToDisc(strSthoaFileName, strSthoaText)
            strSthoaText = String.Empty
        End If
        If strSthocText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:REPORT-" & strSthocFileName)
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        If strSthojText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:STHOJ-" & strSthojFileName)
            PutSthoToDisc(strSthojFileName, strSthojText)
            strSthojText = String.Empty
        End If
        If strSthpaText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:STHPA-" & strSthpaFileName)
            PutSthoToDisc(strSthpaFileName, strSthpaText)
            strSthpaText = String.Empty
        End If
        If strSthpoText.Length > 0 Then
            Trace.WriteLine("EnsureAllDataOutput:STHPO-" & strSthpoFileName)
            PutSthoToDisc(strSthpoFileName, strSthpoText)
            strSthpoText = String.Empty
        End If
    End Sub ' Ensure that all data is output 

    Public Sub AddSthocEndOfDayRecord()
        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)
        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()
        dateHashDate = SysDates.Today.Value()
        decHashValue = 0
        SetupHash("CZ", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "END OF DAY RECORD" '& vbCrLf
        PutSthoToDisc(strSthocFileName, strSthocText)
        strSthocText = String.Empty
        strWorkString = "End of day STHOC record output at : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        OutputSthoLog(strWorkString)
    End Sub

    ''' <summary>
    ''' Log progess of transmission builds
    ''' </summary>
    ''' <param name="strSthoLogText"></param>
    ''' <remarks></remarks>
    Public Sub OutputSthoLog(ByVal strSthoLogText As String)

        PutSthoToDisc(strSthoLogFileName, strSthoLogText)

    End Sub

    Public Sub GetNextTVCVersion(ByVal strTransmissionFileName As String, ByRef VersionNo As String, ByRef SequenceNo As String)
        ' get next version/sequence numbers from TVCSTR
        Dim Tvcstr As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
        Dim ColVs As New List(Of BOStoreTransValCtl.cStoreTransValCtl)
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.FileName, strTransmissionFileName.PadRight(5, " "c))
        Tvcstr.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.VersionNo, "00")
        ColVs = Tvcstr.LoadMatches()
        If ColVs.Count < 1 Then
            Try
                Tvcstr.FileName.Value = strTransmissionFileName.PadRight(5, " "c)
                Tvcstr.VersionNo.Value = "00"
                Tvcstr.Sequence.Value = "000000"
                Tvcstr.SaveIfNew()
            Catch ex As Exception
            End Try
        End If

        VersionNo = (Val(Tvcstr.LastAvailVersionNo.Value) + 1).ToString("00")
        If VersionNo > "31" Then VersionNo = "01"
        SequenceNo = (Val(Tvcstr.LastAvailSeqNo.Value) + 1).ToString("000000")
        If CLng(SequenceNo) > 999999 Then SequenceNo = "000001"

    End Sub

    Public Sub CloseTransmissionsUpdateTVC(ByVal strTransmissionFileName As String, ByVal intj As Integer, ByVal VersionNo As String, ByVal SequenceNo As String)
        ' get next version/sequence numbers from TVCSTR
        Dim Tvcstr As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
        Dim ColVs As New List(Of BOStoreTransValCtl.cStoreTransValCtl)
        Dim arrMyRecordTypes As String() = New String(15) {} 'Record Type(2) & Record Count (8) & hash (14.2)
        Dim intA As Integer = 0
        Dim NoRecTypes As Integer = 0
        Array.Clear(arrMyRecordTypes, 0, 15)

        For intA = 1 To intj
            'Dim mytestttype As String = arrRecordTypes(intA).Substring(0, 2)
            If arrRecordTypes(intA) <> Nothing And arrRecordTypes(intA) <> " " Then
                NoRecTypes += 1
                arrMyRecordTypes(NoRecTypes) = arrRecordTypes(intA)
            End If
        Next
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.FileName, strTransmissionFileName.PadRight(5, " "c))
        Tvcstr.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.VersionNo, "00")
        ColVs = Tvcstr.LoadMatches()
        If ColVs.Count > 0 Then
            Try
                Tvcstr.LastAvailVersionNo.Value = VersionNo
                Tvcstr.LastAvailSeqNo.Value = SequenceNo
                Tvcstr.SaveIfExists()
            Catch ex As Exception
            End Try
        End If
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.FileName, strTransmissionFileName.PadRight(5, " "c))
        Tvcstr.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Tvcstr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Tvcstr.VersionNo, VersionNo)
        ColVs = Tvcstr.LoadMatches()
        If ColVs.Count < 1 Then
            'Try
            Tvcstr.FileName.Value = strTransmissionFileName.PadRight(5, " "c)
            Tvcstr.VersionNo.Value = VersionNo
            Tvcstr.Sequence.Value = SequenceNo
            Tvcstr.TransactionDate.Value = Today.Date.Date
            Tvcstr.TransactionTime.Value = TimeOfDay.ToString("hh:mm:ss") ' 
            For intA = 1 To NoRecTypes Step 1
                Tvcstr.RecordType(intA).Value = arrMyRecordTypes(intA).ToString.Substring(0, 2).PadRight(2, " "c)
                Tvcstr.RecordHash(intA).Value = CDec(arrMyRecordTypes(intA).ToString.Substring(2, 8))
                Tvcstr.ValuePerHash(intA).Value = CDec(arrMyRecordTypes(intA).ToString.Substring(10, 14))
            Next intA

            'intRecordCount = arrRecordTypes(intI).Substring(2, 8)
            'decRecordHash = arrRecordTypes(intI).Substring(10, 14)
            Tvcstr.SaveIfNew()
            'Catch ex As Exception
            'End Try
        End If
        If ColVs.Count > 0 Then
            'Try
            Tvcstr.Sequence.Value = SequenceNo
            Tvcstr.TransactionDate.Value = Today.Date.Date
            Tvcstr.TransactionTime.Value = TimeOfDay.ToString("hh:mm:ss") ' 
            For CntrPos As Integer = 1 To 15 Step 1
                Tvcstr.RecordHash(CntrPos).Value = 0
                Tvcstr.ValuePerHash(CntrPos).Value = 0
                Tvcstr.RecordType(CntrPos).Value = "  "
            Next CntrPos

            For intA = 1 To NoRecTypes Step 1
                Tvcstr.RecordType(intA).Value = arrMyRecordTypes(intA).ToString.Substring(0, 2).PadRight(2, " "c)
                Tvcstr.RecordHash(intA).Value = CDec(arrMyRecordTypes(intA).ToString.Substring(2, 8))
                Tvcstr.ValuePerHash(intA).Value = CDec(arrMyRecordTypes(intA).ToString.Substring(10, 14))
            Next intA
            Tvcstr.SaveIfExists()
            'Catch ex As Exception
            'End Try
        End If
    End Sub

    Public Sub CloseTransmissionFile(ByVal strTransmissionFileName As String) ' CLOSE transmissions
        ' First - Pass the selected file and calculate the hash values
        ' Then output the transmission file plus HEADER and TRAILER to the RTI folder

        Dim Retopt As New BOSystem.cRetailOptions(_Oasys3DB)

        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()

        ' The trailer record allows for up to 15 record type/count/hash combinations

        Dim intRecordCount As Integer ' Count of records in the transmission file 
        Dim intRecordsInput As Integer ' Count of records in the transmission file
        Dim decRecordHash As Decimal  ' Total hash count of records in the transmission file 
        Dim strTransmissionFileToClose As String ' The tranmission file that is to be closed
        'Dim strTransmissionFileData As String ' Holds the record data from the file

        Dim strMyTvcPath As String ' Temporary file to control version & sequence numbers
        Dim strMyTvcData As String ' Temporary TVC data area
        Dim strBatchToRun As String = String.Empty
        Dim strZippedFile As String = String.Empty
        Dim strCopyZipFile As String = String.Empty
        Dim boolCompoundFile As Boolean = False ' For SOCLOS function - compound files
        Dim boolGotDataToProcess As Boolean = False
        Dim arrFilesFound As String() = New String(9999) {}
        Dim NextVersionNo As String = String.Empty
        Dim NextSequenceNo As String = String.Empty
        Dim intI As Integer
        Dim intJ As Integer

        Dim strTransmissionHeader As String ' Save the HEADER details here
        Dim strTransmissionTrailer As String ' Save the TRAILER details here

        Dim intVersionNumber As Integer  ' Version number to use for this transmission
        Dim intSequenceNumber As Integer ' Sequence number to use for this transmission
        ReDim arrRecordTypes(15)
        strTransmissionFileToClose = strTransmissionsPath & "\" & strTransmissionFileName
        strTvcPath = strTransmissionsPath & "\"
        strMyTvcPath = strTvcPath & strTransmissionFileName & ".TVC"
        ' Determine if processing Normal or Compound file.
        boolCompoundFile = False
        strWorkString = "SOCLOS - Closing Transmission File :" & strTransmissionFileToClose & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        'Check if File or Folder passed in
        If Not File.Exists(strTransmissionFileToClose) Then
            If My.Computer.FileSystem.DirectoryExists(strTransmissionFileToClose) = True Then
                Array.Clear(arrFilesFound, 0, 9999)
                intJ = 0
                For Each foundfile As String In My.Computer.FileSystem.GetFiles(strTransmissionFileToClose)
                    intJ = intJ + 1
                    arrFilesFound(intJ) = String.Empty & foundfile
                Next
                If intJ > 0 Then
                    boolCompoundFile = True
                    boolGotDataToProcess = True
                End If
            End If
        End If
        If File.Exists(strTransmissionFileToClose) And boolCompoundFile = False Then
            boolGotDataToProcess = True
        End If
        If boolGotDataToProcess = True Then
            ' Initialise TVC parameters
            strMyTvcData = String.Empty
            NextVersionNo = String.Empty
            NextSequenceNo = String.Empty
            intVersionNumber = 0
            intSequenceNumber = 0
            GetNextTVCVersion(strTransmissionFileName, NextVersionNo, NextSequenceNo)
            'If File.Exists(strMyTvcPath) Then
            '    ' Extract TVC data from ASCII file
            '    reader = New StreamReader(strMyTvcPath, True)
            '    strMyTvcData = reader.ReadLine
            '    reader.Close()
            '    If strMyTvcData <> String.Empty Then
            '        intLatestAvailableVersion = CInt(strMyTvcData.Substring(0, 2))
            '        intLatestAvailableSequence = CInt(strMyTvcData.Substring(2, 6))
            '        intVersionNumber = CInt(strMyTvcData.Substring(8, 2))
            '        intSequenceNumber = CInt(strMyTvcData.Substring(10, 6))
            '    End If
            'End If ' Extract TVC data from ASCII file - Completed
            'If strMyTvcData = String.Empty Then
            '    strMyTvcData = strMyTvcData & intLatestAvailableVersion.ToString.PadLeft(2, "0") & intLatestAvailableSequence.ToString.PadLeft(6, "0") & intVersionNumber.ToString.PadLeft(2, "0") & intSequenceNumber.ToString.PadLeft(6, "0")
            'End If
            'intLatestAvailableVersion = intLatestAvailableVersion + 1
            'If intLatestAvailableVersion > 31 Then
            '    intLatestAvailableVersion = 1
            'End If
            'intLatestAvailableSequence = intLatestAvailableSequence + 1
            'If intLatestAvailableSequence > 999999 Then
            '    intLatestAvailableSequence = 1
            'End If
            dateCurrentTime = TimeOfDay
            strTransmissionHeader = "HR" & Retopt.Store.Value.ToString.PadLeft(3, "0"c) & Today.ToString("dd/MM/yy") & strTransmissionFileName & NextVersionNo & NextSequenceNo & dateCurrentTime.ToString("HHmmss")
            ' Now lets do the hash counts
            strTransmissionTrailer = String.Empty & "TR" & Retopt.Store.Value.ToString.PadLeft(3, "0"c) & Today.ToString("dd/MM/yy") & strTransmissionFileName & NextVersionNo & NextSequenceNo & dateCurrentTime.ToString("HHmmss")
            strToRtiFile = strToRtiPath & "\BLD" & strTransmissionFileName & Retopt.Store.Value.ToString.PadLeft(3, "0"c) & "." & NextVersionNo
            If boolCompoundFile = False Then
                Array.Clear(arrRecordTypes, 1, 15)
                reader = New StreamReader(strTransmissionFileToClose, True) ' Open the file for calculations
                intJ = 1
                intRecordsInput = 0
                Trace.WriteLine("Calculating Trailer Counts")
                While reader.EndOfStream = False ' Calculate Hash & Record counts
                    strTransmissionFileData = reader.ReadLine
                    intRecordsInput = intRecordsInput + 1
                    If strTransmissionFileData <> String.Empty Then
                        strWorkString = strTransmissionFileData
                        ProcessTransmissionsProgress.RecordCountTextBox.Text = intRecordsInput.ToString & " - " & strTransmissionFileName
                        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                        ProcessTransmissionsProgress.Show()
                        For intI = 1 To 15
                            'Use next available Record counter
                            If arrRecordTypes(intI) = String.Empty Then
                                intRecordCount = 0
                                decRecordHash = 0
                                arrRecordTypes(intI) = strTransmissionFileData.Substring(0, 2) & intRecordCount.ToString.PadLeft(8, " "c) & decRecordHash.ToString("##########0.00").PadLeft(14, " "c)
                                intJ += 1
                            End If
                            If arrRecordTypes(intI).StartsWith(strTransmissionFileData.Substring(0, 2)) Then
                                intRecordCount = CInt(arrRecordTypes(intI).Substring(2, 8)) + 1
                                decRecordHash = CDec(arrRecordTypes(intI).Substring(10, 14)) + CDec(strTransmissionFileData.Substring(10, 12))
                                arrRecordTypes(intI) = strTransmissionFileData.Substring(0, 2) & intRecordCount.ToString.PadLeft(8, " "c) & decRecordHash.ToString("##########0.00").PadLeft(14, " "c)
                                Exit For
                            End If
                        Next
                    End If
                End While ' Hash & Record count calculations completed
                Trace.WriteLine("All Records retrieved and counted:" & intJ)
                ReDim Preserve arrRecordTypes(intJ) ' Sort the record counts in
                Array.Sort(arrRecordTypes)          ' alphabetic order  
                For intI = 1 To intJ ' Append hashes and counts to trailer record
                    If arrRecordTypes(intI) <> String.Empty Then
                        intRecordCount = CInt(arrRecordTypes(intI).Substring(2, 8))
                        decRecordHash = CInt(arrRecordTypes(intI).Substring(10, 14))
                        'restrict number of digits 
                        Dim strRecHash As String = decRecordHash.ToString("0.00 ;0.00-").PadLeft(12, " "c)
                        If strRecHash.Length > 12 Then strRecHash = strRecHash.Substring(strRecHash.Length - 12, 12)
                        strTransmissionTrailer = strTransmissionTrailer & arrRecordTypes(intI).Substring(0, 2) & intRecordCount.ToString.PadLeft(6, " "c) & Space(1) & strRecHash
                    End If
                Next ' end hash & record counts append to trailer

                reader.Close() ' Close the transmission file - Hashes & record counts calculated

                If File.Exists(strToRtiFile) Then
                    My.Computer.FileSystem.DeleteFile(strToRtiFile)
                End If
                Trace.WriteLine("Creating Transmission File:" & strToRtiFile)

                OpenTransmissionFile(strToRtiFile)
                reader = New StreamReader(strTransmissionFileToClose, True)
                strOutputText = strTransmissionHeader & vbCrLf & reader.ReadToEnd & strTransmissionTrailer
                writer.WriteLine(strOutputText)
                writer.Close()
                File.Move(strToRtiFile, strToRtiFile.Replace("\BLD", "\"))
                'strWorkString = "SOCLOS - " & strToRtiFile & " Created : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                'OutputSthoLog(strWorkString)
                reader.Close()
            End If
            If boolCompoundFile = True Then ' Procesing a Compound File
                My.Computer.FileSystem.CurrentDirectory = strNetWorkPath
                intJ = 0
                Array.Clear(arrFilesFound, 1, (arrFilesFound.Count - 1))
                Trace.WriteLine("Compound Files : Found" & arrFilesFound.Count)
                'For Each foundFile As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.SpecialDirectories.MyDocuments, FileIO.SearchOption.SearchAllSubDirectories, "PKZIP.EXE")
                For Each foundFile As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory, FileIO.SearchOption.SearchAllSubDirectories, "PKZIP.EXE")
                    intJ = intJ + 1
                    arrFilesFound(intJ) = foundFile
                    Exit For
                Next
                Dim strStholVVH As String = strStholPathName & "\" & strTransmissionFileName & NextVersionNo & "H"
                PutSthoToDisc(strStholVVH, ("HR" & Retopt.Store.Value.ToString.PadLeft(3, "0"c) & Today.ToString("dd/MM/yy") & strTransmissionFileName & NextVersionNo & NextSequenceNo & dateCurrentTime.Hour.ToString.PadLeft(2, "0"c) & dateCurrentTime.Minute.ToString.PadLeft(2, "0"c) & dateCurrentTime.Second.ToString.PadLeft(2, "0"c)))
                strBatchToRun = strNetWorkPath & "\ZIPCOMP.BAT" ' Set up the batch file
                If File.Exists(strBatchToRun) Then ' DELETE IT PRIOR TO CREATION
                    My.Computer.FileSystem.DeleteFile(strBatchToRun)
                End If
                Trace.WriteLine("Opening Next File:" & strBatchToRun)
                OpenTransmissionFile(strBatchToRun) ' Use this function to open the batch file
                'strOutputText = String.Empty & "F:\UTILS\PKZIP.EXE --" & Space(1) & strTransmissionFileToClose & Space(1) & strTransmissionFileToClose & "\*.*"
                strOutputText = arrFilesFound(1) & " --" & Space(1) & strTransmissionFileToClose & Space(1) & strTransmissionFileToClose & "\*.*"
                writer.WriteLine(strOutputText) ' Command added to the batch file
                writer.Close() ' Close the batch file
                My.Computer.FileSystem.CurrentDirectory = strNetWorkPath
                strWorkString = "SOCLOS - " & strToRtiFile & " Creating : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                UpdateProgress(String.Empty, String.Empty, strWorkString)
                Trace.WriteLine("Zipping Next File:" & strOutputText)

                Dim procID As Integer
                ' Run Batch.
                procID = Shell("ZIPCOMP.BAT", AppWinStyle.NormalFocus, True)
                ' Shell("""ZIPCOMP.BAT""") ' Now run the batch to create the ZIP file
                'Shell("""F:\UTILS\PKZIP.EXE -- F:\dev\wix\commstst\sthol F:\DEV\COMMSTST\STHOL\*.*""")
                Trace.WriteLine("Shell completed:" & strZippedFile)
                strZippedFile = strTransmissionFileToClose & ".ZIP "
                ReDim Preserve arrFilesFound(intJ)
                If File.Exists(strToRtiFile) Then
                    My.Computer.FileSystem.DeleteFile(strToRtiFile)
                End If
                Dim boolFileNowZipped As Boolean = False
                Dim intFileWaitCount As Integer = 0
                Trace.WriteLine("Waiting for ZIP to complete:" & strZippedFile)
                While boolFileNowZipped = False
                    Application.DoEvents()
                    If File.Exists(strZippedFile) Then boolFileNowZipped = True
                    intFileWaitCount += 1
                End While
                Trace.WriteLine("Files Zipped")
                If My.Computer.FileSystem.FileExists(strZippedFile) Then
                    Trace.WriteLine("Zip File Moved from :" & strZippedFile & " to " & strToRtiFile)
                    My.Computer.FileSystem.MoveFile(strZippedFile, strToRtiFile)
                    File.Move(strToRtiFile, strToRtiFile.Replace("\BLD", "\"))
                End If
            End If ' Compound file processed
            CloseTransmissionsUpdateTVC(strTransmissionFileName, intJ, NextVersionNo, NextSequenceNo)
            If boolCompoundFile = False Then
                My.Computer.FileSystem.DeleteFile(strTransmissionFileToClose)
            Else
                Trace.WriteLine("Deleteing Files in :" & strTransmissionFileToClose)
                For Each DelFileName As String In My.Computer.FileSystem.GetFiles(strTransmissionFileToClose)
                    My.Computer.FileSystem.DeleteFile(DelFileName)
                Next
            End If
        End If
        strWorkString = "SOCLOS - " & strToRtiFile & " Created : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        strWorkString = "SOCLOS - Closing Transmission File :" & strTransmissionFileToClose & " Ended: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
    End Sub 'CLOSE Transmissions

    Public Sub CompressFile(ByVal FILENAME As String)
        '    'A String object reads the file name (locally)
        '    Dim MYFileName As String = Path.GetFileName(FILENAME)



        '    'Stream object that reads file contents

        ' Dim streamObj As Stream = 






        'Allocate space in buffer according to the length of the file read

        'Dim buffer(FILENAME.Length) As Byte

        'Fill buffer

        'streamObj.Read(buffer, 0, buffer.Length)

        'streamObj.Close()



        'File Stream object used to change the extension of a file

        'Dim compFile As System.IO.FileStream = File.Create((Path.ChangeExtension(FILENAME, "zip")))



        'GZip object that compress the file 

        'Dim zipStreamObj As New GZipStream(compFile, CompressionMode.Compress)



        'Write to the Stream object from the buffer

        'zipStreamObj.Write(Buffer, 0, Buffer.Length)

        'zipStreamObj.Close()



    End Sub

    ''' <summary>
    '''  Open the file required for output
    ''' </summary>
    ''' <param name="TransmissionFileName"></param>
    ''' <remarks></remarks>
    Public Sub OpenTransmissionFile(ByVal TransmissionFileName As String)

        writer = New StreamWriter(TransmissionFileName, True)

    End Sub

    ''' <summary>
    ''' Outputs the data to the selected transmission file
    ''' </summary>
    ''' <param name="strMyFileName"></param>
    ''' <param name="strMyText"></param>
    ''' <remarks></remarks>
    Public Sub PutSthoToDisc(ByVal strMyFileName As String, ByVal strMyText As String, Optional ByVal blnTibpfv As Boolean = False)

        If blnTibpfv = True Then 'Only do this if were already doing the pending supplier changes
            If File.Exists(strMyFileName) Then
                File.Delete(strMyFileName)
            End If
        End If

        OpenTransmissionFile(strMyFileName)
        strOutputText = strMyText
        OutPutStho()
        writer.Close()

    End Sub

    ''' <summary>
    ''' Sets up the HASH fields required in MOST of the transmission files
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <param name="decWorkHashValue"></param>
    ''' <param name="workDate"></param>
    ''' <remarks></remarks>
    Public Sub SetupHash(ByVal workType As String, ByVal decWorkHashValue As Decimal, ByVal workDate As Date)

        strSthoRecordType = workType
        strSthoRecordDate = workDate.ToString("dd/MM/yy")
        FormatDecToString(decWorkHashValue, strSthoRecordHashValue, 11, " "c, "0.00")

    End Sub

    ''' <summary>
    ''' Sets up the HASH fields required in MOST of the transmission files - Set values are passed out
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <param name="decWorkHashValue"></param>
    ''' <param name="workDate"></param>
    ''' <remarks></remarks>
    Public Sub SetupHashOut(ByVal workType As String, ByVal decWorkHashValue As Decimal, ByVal workDate As Date, _
                         ByRef strSTHORecType As String, ByRef strSTHORecDate As String, ByRef strSTHORecHashValue As String)

        strSTHORecType = workType
        strSTHORecDate = workDate.ToString("dd/MM/yy")
        FormatDecToString(decWorkHashValue, strSTHORecHashValue, 11, " "c, "0.00")

    End Sub

    ''' <summary>
    ''' Formats decimals to string format required in the transmission file
    ''' </summary>
    ''' <param name="decValue"></param>
    ''' <param name="stringValue"></param>
    ''' <param name="intPadDigit"></param>
    ''' <param name="strPadcharacter"></param>
    ''' <param name="strWorkMask"></param>
    ''' <remarks></remarks>
    Public Sub FormatDecToString(ByVal decValue As Decimal, ByRef stringValue As String, ByVal intPadDigit As Integer, ByVal strPadcharacter As Char, ByVal strWorkMask As String)

        strStockAdjustmentSign = "+"
        If decValue < 0 Then
            decValue = decValue * -1
            strStockAdjustmentSign = "-"
        End If
        stringValue = decValue.ToString(strWorkMask).PadLeft(intPadDigit, strPadcharacter) & strStockAdjustmentSign

    End Sub

    ''' <summary>
    ''' Formats integers to string format required in the transmission file
    ''' </summary>
    ''' <param name="intWorkInteger"></param>
    ''' <param name="strWorkString"></param>
    ''' <param name="intPadDigit"></param>
    ''' <param name="strPadCharacter"></param>
    ''' <remarks></remarks>
    Public Sub FormatIntToString(ByVal intWorkInteger As Integer, ByRef strWorkString As String, ByVal intPadDigit As Integer, ByVal strPadCharacter As Char)

        strStockAdjustmentSign = "+"
        If intWorkInteger < 0 Then
            intWorkInteger = intWorkInteger * -1
            strStockAdjustmentSign = "-"
        End If
        strWorkString = intWorkInteger.ToString("0").PadLeft(intPadDigit, strPadCharacter) & strStockAdjustmentSign

    End Sub

    ''' <summary>
    ''' Adds data required for CSV to the CSV work area
    ''' </summary>
    ''' <param name="csvField"></param>
    ''' <param name="AddQuotes"></param>
    ''' <remarks></remarks>
    Public Sub CsvData(ByVal csvField As String, ByVal addQuotes As Boolean)

        If (addQuotes = True) Then
            strCsvWorkingArea = strCsvWorkingArea & """" & csvField.ToString.TrimEnd(" "c) & ""","
        Else
            strCsvWorkingArea = strCsvWorkingArea & csvField.ToString.TrimEnd(" "c) & ","
        End If

    End Sub

    ''' <summary>
    ''' Writes the data to the disc - DOES SOME SPECIAL VALIDATION FOR WICKES LAYOUTS
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub OutPutStho()
        'only one reference in project
        ' Remove + signs
        Dim indexMax As Integer = Len(strOutputText.ToString) - 1
        For index As Integer = 0 To indexMax

            Dim indexPrevious As Integer = index - 1
            If indexPrevious > 0 Then

                If strOutputText.Substring(index, 1) = "+" AndAlso strOutputText.Substring(indexPrevious, 1) >= "0" AndAlso strOutputText.Substring(indexPrevious, 1) <= "9" Then
                    If strOutputText.Substring(indexPrevious, 2) <> "++" Then
                        Mid(strOutputText, (index + 1), 1) = " "
                    End If
                End If

            End If
        Next

        If strOutputText.Substring(indexMax, 1) = " " Then
            strOutputText = strOutputText.ToString.TrimEnd(" "c)
        End If

        strOutputText.Replace(OriginalSthoFileSeparator, "+")
        If (strOutputText.Trim.Length > 0) Then
            If (strOutputText.Trim.EndsWith(vbCrLf)) Then
                writer.Write(strOutputText.Trim)
            Else
                writer.WriteLine(strOutputText.Trim)
            End If
        End If

    End Sub

    Public Sub OutputSthol()
        Dim Retopt As New BOSystem.cRetailOptions(_Oasys3DB)
        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()

        Dim sysdat As New BOSystem.cSystemDates(_Oasys3DB)
        sysdat.AddLoadField(sysdat.Today)
        sysdat.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, sysdat.SystemDatesID, "01")
        sysdat.LoadMatches()

        Dim Dltots As New BOSales.cSalesHeader(_Oasys3DB)
        Dim ColDt As New List(Of BOSales.cSalesHeader)
        Dim Dlline As New BOSales.cSalesLine(_Oasys3DB)
        Dim ColDl As New List(Of BOSales.cSalesLine)
        Dim Wsoctl As New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
        Dim Consum As New BOPurchases.cContainerSummary(_Oasys3DB)
        ' Container Summary Record Definition Required        

        Dim dateSelectionDate As Date = Date.MinValue.Date
        Dim dateStartTime As Integer = 600
        Dim intMyTime As Integer = 0
        'Dim intEndTime As Integer = 2200
        Dim arrstrStholRecords As String() = New String(99999) {}
        'Dim arrTills As Boolean() = New Boolean(99) {}

        Dim intMyHour As Integer = 0
        Dim intMyMinutes As Integer = 0
        Dim intStartHour As Integer = 6
        Dim intEndHour As Integer = 22
        Dim decWorkValue As Decimal = 0
        Dim intWorkTransactionCount As Integer = 0
        Dim intMyTransactionCount As Integer = 0
        Dim intTransactionLines As Integer = 0
        Dim intWorkPrimaryFunction As Integer = 0
        Dim strOutputPrimaryFunction As String = String.Empty
        Dim boolIsSale As Boolean = False
        Dim strWorkType As String = String.Empty
        Dim strCategoryType As String = String.Empty
        Dim intWorkCategory As Integer = 0
        Dim strOutputCategory As String = String.Empty
        Dim decCategoryValue As Decimal = 0
        Dim strCheckYear As String = String.Empty
        Dim strCheckDay As String = String.Empty
        Dim strCheckTime As String = String.Empty
        Dim strCheckPrimaryFunction As String = String.Empty
        Dim strCheckType As String = String.Empty
        Dim decCheckValue As Decimal = 0
        Dim intCheckTransactionCount As Integer = 0
        Dim intCheckTransactionLines As Integer = 0
        Dim dateCheckTransactionDate As Date = Date.MinValue.Date
        Dim strWorkString As String = String.Empty
        Dim strFileExtension As String = String.Empty

        'Dim intA As Integer = 0

        Dim intB As Integer = 0
        Dim intC As Integer = 0
        Dim intI As Integer = 0
        Dim intJ As Integer = 0
        Dim intK As Integer = 0
        Dim intL As Integer = 0
        Dim intM As Integer = 0

        Dim decLineKValue As Decimal = 0
        Dim decLineMValue As Decimal = 0
        Dim intLineKTransactionLines As Integer = 0
        Dim intLineMTransactionLines As Integer = 0
        Dim intLineMTransactionCount As Integer = 0
        Dim intLineKTransactionCount As Integer = 0
        Dim intContainerSummaryCount As Integer = 0
        Dim strLineKValue As String = String.Empty
        Dim strLineKTransactionCount As String = String.Empty
        Dim strLineKTransactionLines As String = String.Empty
        Dim arrstrCategories As String() = New String(9999) {}


        If (_DatePassedIn = False) And (_PromptForDate = True) Then
            _RunningInNight = False
            Trace.WriteLine("Requesting Date for STHOL")
            Dim StartSTHOLDate As String = InputBox("Enter starting date to prepare Stock Holding comms file" & vbCrLf & vbCrLf & "Enter date as dd/mm/yy", "Enter Stock Holding Start Date", Today.ToString("dd/MM/yy"))
            strWorkString = "Processing STHOL Stock Holding (OL) Entered Starting Date(" & StartSTHOLDate & ") : " & TimeOfDay.ToString("hh:mm:ss")
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            If IsDate(StartSTHOLDate) = False Then End
            Dim EndSTHOLDate As String = InputBox("Enter ending date to prepare Stock Holding comms file" & vbCrLf & vbCrLf & "Enter date as dd/mm/yy", "Enter Stock Holding End Date", Today.ToString("dd/MM/yy"))
            strWorkString = "Processing STHOL Stock Holding (OL) Entered Ending Date(" & EndSTHOLDate & ") : " & TimeOfDay.ToString("hh:mm:ss")
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            If IsDate(EndSTHOLDate) = False Then End
            _DatePassedIn = True
            _StartDateToUse = CDate(StartSTHOLDate)
            _EndDateToUse = CDate(EndSTHOLDate)
        End If

        _StartDate = _StartDateToUse
        _EndDate = _EndDateToUse
        If _RunningInNight = True Then
            _EndDate = sysdat.Today.Value
            If _EndDate.DayOfWeek = 0 Then 'Sunday=0
                _StartDate = _EndDate.AddDays(-6)
            Else
                _StartDate = _EndDate.AddDays(_EndDate.DayOfWeek * -1)
                _EndDate = _StartDate.AddDays(6)
            End If
        End If

        strFileExtension =  _StartDate.ToString("ddMMyy")
        My.Computer.FileSystem.CreateDirectory(strStholPathName)
        strStholFileName = strStholPathName & "\" & "EI" & strFileExtension & "." & Retopt.Store.Value.ToString.PadLeft(3, "0"c)
        Dim strStholOutputText As String
        strStholOutputText = String.Empty
        dateSelectionDate =  _StartDate
        intI = 0
        intRecordsOutput = 0
        strWorkString = "Processing STHOL (OL) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        While dateSelectionDate <=  _EndDate
            Consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Consum.DeliveryDate, dateSelectionDate)
            Consum.SortBy(Consum.DeliveryDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Consum.SortBy(Consum.AssemblyDepotNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Consum.SortBy(Consum.Number.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            intContainerSummaryCount = Consum.LoadMatches.Count
            Dltots.ClearLists()
            Dltots.ClearLoadField()
            Dltots.ClearLoadFilter()

            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, dateSelectionDate)
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "CO")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "CC")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "M+")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "M-")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "OD")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "RL")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "C+")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "C-")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "ZR")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dltots.TransactionCode, "XR")
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.Voided, False)
            Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TrainingMode, False)
            Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

            ColDt = Dltots.LoadMatches

            'ColDt.Reverse()
            If ColDt.Count > 0 Then
                For Each totl As BOSales.cSalesHeader In ColDt
                    strWorkString = "Checking Transaction : " & totl.TransDate.Value.Date.ToString & Space(1) & totl.TillID.Value.PadLeft(2, "0"c) & Space(1) & totl.TransactionNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    'Commented back in to Retrieve Workstation Function
                    Wsoctl = New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
                    Wsoctl.AddLoadField(Wsoctl.PrimaryFunction)
                    Wsoctl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Wsoctl.ID, CInt(totl.TillID.Value))
                    If (Wsoctl.LoadMatches.Count = 0) Then
                        Trace.WriteLine("Output STHOL:Locate WSOCTL entry for Till:" & totl.TillID.Value & " FAILED - Record Missing")
                        intWorkPrimaryFunction = 0
                    Else
                        If (IsNumeric(Wsoctl.PrimaryFunction.Value) = False) Then
                            Trace.WriteLine("Output STHOL:Locate WSOCTL entry for Till:" & totl.TillID.Value & " FAILED - Primary Function not set")
                            intWorkPrimaryFunction = 0
                        Else
                            intWorkPrimaryFunction = CInt(Wsoctl.PrimaryFunction.Value)
                        End If
                    End If
                    strOutputPrimaryFunction = intWorkPrimaryFunction.ToString.PadLeft(7, "0"c)
                    'intWorkPrimaryFunction = CInt(totl.TillID.Value)
                    'strOutputPrimaryFunction = intWorkPrimaryFunction.ToString.PadLeft(7, "0")

                    intMyHour = CInt(totl.TransactionTime.Value.Substring(0, 2))
                    intMyMinutes = CInt(totl.TransactionTime.Value.Substring(2, 2))
                    If intMyMinutes < 15 Then
                        intMyMinutes = 0
                    End If
                    If intMyMinutes > 15 And intMyMinutes < 30 Then
                        intMyMinutes = 15
                    End If
                    If intMyMinutes > 30 And intMyMinutes < 45 Then
                        intMyMinutes = 30
                    End If
                    If intMyMinutes > 45 Then
                        intMyMinutes = 45
                    End If
                    intMyTime = (intMyHour * 100) + intMyMinutes
                    intI = intI + 1
                    decWorkValue = 0
                    intWorkTransactionCount = 0
                    intTransactionLines = 0
                    FormatIntToString(intWorkTransactionCount, strLineKTransactionCount, 5, "0"c)
                    FormatIntToString(intTransactionLines, strLineKTransactionLines, 5, "0"c)
                    FormatDecToString(decWorkValue, strLineKValue, 14, "0"c, "0.00")
                    'arrstrStholRecords(i) = String.Empty & totl.TransDate.Value.Year.ToString.PadLeft(4, "0") & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0") & intMyTime.ToString.PadLeft(4, "0") & "2LTTE" & strOutputPrimaryFunction & decWorkValue.ToString("##########0.00").PadLeft(14, " ") & strStockAdjustmentSign & Space(1) & intWorkTransactionCount.ToString.PadLeft(4, " ") & Space(1) & intTransactionLines.ToString.PadLeft(4, " ") & Space(1) & totl.TransDate.Value.ToString("dd/MM/yy")
                    arrstrStholRecords(intI) = totl.TransDate.Value.Year.ToString.PadLeft(4, "0"c) & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0"c) & intMyTime.ToString.PadLeft(4, "0"c) & "1LTTS" & strOutputPrimaryFunction & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & totl.TransDate.Value.ToString("dd/MM/yy")
                    intB = arrstrStholRecords(intI).Length - 1
                    For intA = 1 To intB
                        If Mid(arrstrStholRecords(intI), intA, 1) = "+" Then
                            Mid(arrstrStholRecords(intI), intA, 1) = Space(1)
                        End If
                    Next
                    intI = intI + 1
                    arrstrStholRecords(intI) = totl.TransDate.Value.Year.ToString.PadLeft(4, "0"c) & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0"c) & intMyTime.ToString.PadLeft(4, "0"c) & "2LTTE" & strOutputPrimaryFunction & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & totl.TransDate.Value.ToString("dd/MM/yy")
                    intB = arrstrStholRecords(intI).Length - 1
                    For intA = 1 To intB
                        If Mid(arrstrStholRecords(intI), intA, 1) = "+" Then
                            Mid(arrstrStholRecords(intI), intA, 1) = Space(1)
                        End If
                    Next
                    'arrTills(CInt(totl.TillID.Value)) = True
                    Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, totl.TransDate.Value)
                    Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TillID, totl.TillID.Value.ToString.PadLeft(2, "0"c))
                    Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransNo, totl.TransactionNo.Value.ToString.PadLeft(4, "0"c))
                    Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.LineReversed, False)

                    ColDl = Dlline.LoadMatches()
                    'ColDl.Reverse()

                    boolIsSale = True
                    For Each detail As BOSales.cSalesLine In ColDl
                        'If detail.ExtendedValue.Value <> 0 And detail.QuantitySold.Value < 0 Then
                        If detail.ExtendedValue.Value < 0 Or detail.QuantitySold.Value < 0 Then
                            boolIsSale = False
                            Exit For
                        End If
                    Next

                    intJ = 0
                    strWorkType = String.Empty
                    strCategoryType = String.Empty
                    For intK = 1 To 99999
                        If arrstrStholRecords(intK) <> Nothing Then
                            strCheckYear = arrstrStholRecords(intK).Substring(0, 4)
                            decCheckValue = 0
                            If strCheckYear <> String.Empty Then
                                strCheckDay = arrstrStholRecords(intK).Substring(4, 3)
                                strCheckTime = arrstrStholRecords(intK).Substring(7, 4)
                                strCheckType = arrstrStholRecords(intK).Substring(11, 5)
                                strCheckPrimaryFunction = arrstrStholRecords(intK).Substring(16, 7)
                                decCheckValue = CDec(arrstrStholRecords(intK).Substring(23, 15))
                                'If Mid(arrstrStholRecords(k), 38, 1) = "-" Then
                                '    decCheckValue = decCheckValue * -1
                                'End If
                                If strCheckYear = totl.TransDate.Value.Year.ToString.PadLeft(4, "0"c) Then
                                    If strCheckDay = totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0"c) Then
                                        If strCheckTime = intMyTime.ToString.PadLeft(4, "0"c) Then
                                            If strCheckPrimaryFunction = strOutputPrimaryFunction Then
                                                If boolIsSale = True And strCheckType = "1LTTS" Then
                                                    intJ = intK
                                                    strWorkType = strCheckType
                                                    strCategoryType = "3LCDS"
                                                    Exit For
                                                End If
                                                If boolIsSale = False And strCheckType = "2LTTE" Then
                                                    intJ = intK
                                                    strWorkType = strCheckType
                                                    strCategoryType = "4LCDE"
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next

                    If intJ > 0 Then
                        ReDim arrstrCategories(9999)
                        Array.Clear(arrstrCategories, 0, 9999)
                        decWorkValue = decCheckValue
                        intWorkTransactionCount = CInt(arrstrStholRecords(intJ).Substring(38, 6))
                        intTransactionLines = CInt(arrstrStholRecords(intJ).Substring(44, 6))
                        intMyTransactionCount = 1
                        intC = 0
                        For Each detail As BOSales.cSalesLine In ColDl
                            intWorkCategory = 0
                            If detail.HierCategory.Value <> String.Empty Then
                                intWorkCategory = CInt(detail.HierCategory.Value)
                            End If
                            strOutputCategory = intWorkCategory.ToString.PadLeft(7, "0"c)
                            decCategoryValue = detail.ExtendedValue.Value - detail.QtyBreakMarginAmount.Value - detail.DealGroupMarginAmt.Value - detail.MultiBuyMarginAmount.Value - detail.HierarchyMarginAmt.Value
                            'If detail.QuantitySold.Value < 0 And decCategoryValue > 0 Then
                            '    decCategoryValue = decCategoryValue * -1
                            'End If
                            'decWorkValue = decWorkValue + (detail.ExtendedValue.Value - detail.QtyBreakMarginAmount.Value - detail.DealGroupMarginAmt.Value - detail.MultiBuyMarginAmount.Value - detail.HierarchyMarginAmt.Value)
                            decWorkValue = decWorkValue + decCategoryValue

                            'strStockAdjustmentSign = "+"
                            'If decWorkValue < 0 Then
                            '    decWorkValue = decWorkValue * -1
                            '    strStockAdjustmentSign = "-"
                            'End If
                            intWorkTransactionCount = intWorkTransactionCount + intMyTransactionCount
                            intTransactionLines = intTransactionLines + 1
                            intMyTransactionCount = 0
                            ' Add to Category Array 
                            intC = intC + 1
                            arrstrCategories(intC) = String.Empty & strCategoryType & strOutputCategory & decCategoryValue.ToString("##########0.00").PadLeft(14, " "c) & "1".PadLeft(4, "0"c)
                        Next

                        FormatIntToString(intWorkTransactionCount, strLineKTransactionCount, 5, "0"c)
                        FormatIntToString(intTransactionLines, strLineKTransactionLines, 5, "0"c)
                        FormatDecToString(decWorkValue, strLineKValue, 14, "0"c, "0.00")
                        arrstrStholRecords(intJ) = String.Empty & totl.TransDate.Value.Year.ToString.PadLeft(4, "0"c) & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0"c) & intMyTime.ToString.PadLeft(4, "0"c) & strWorkType & strOutputPrimaryFunction & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & totl.TransDate.Value.ToString("dd/MM/yy")
                        intB = arrstrStholRecords(intJ).Length - 1
                        For intA = 1 To intB
                            If Mid(arrstrStholRecords(intJ), intA, 1) = "+" Then
                                Mid(arrstrStholRecords(intJ), intA, 1) = Space(1)
                            End If
                        Next
                        ReDim Preserve arrstrCategories(intC)
                        Array.Sort(arrstrCategories)
                        intC = arrstrCategories.Count
                        intL = intC
                        For intK = 1 To intL
                            intM = intK + 1
                            If intM < intC Then
                                If arrstrCategories(intK).Substring(0, 12) = arrstrCategories(intM).Substring(0, 12) Then
                                    strWorkString = arrstrCategories(intK).Substring(12, 14)
                                    decLineKValue = CDec(arrstrCategories(intK).Substring(12, 14))
                                    decLineMValue = CDec(arrstrCategories(intM).Substring(12, 14)) + decLineKValue
                                    intLineKTransactionLines = CInt(arrstrCategories(intK).Substring(26, 4))
                                    intLineMTransactionLines = CInt(arrstrCategories(intM).Substring(26, 4)) + intLineKTransactionLines
                                    arrstrCategories(intM) = String.Empty & arrstrCategories(intK).Substring(0, 12) & decLineMValue.ToString("##########0.00").PadLeft(14, " "c) & intLineMTransactionLines.ToString.PadLeft(4, "0"c)
                                    arrstrCategories(intK) = String.Empty
                                End If
                            End If
                        Next

                        For intK = 1 To (intL - 1)
                            If arrstrCategories(intK) <> String.Empty Then
                                intI = intI + 1
                                strCategoryType = arrstrCategories(intK).Substring(0, 5)
                                strOutputCategory = arrstrCategories(intK).Substring(5, 7)
                                decLineKValue = CDec(arrstrCategories(intK).Substring(12, 14))
                                intLineKTransactionLines = CInt(arrstrCategories(intK).Substring(26, 4))
                                intLineMTransactionLines = 1
                                FormatIntToString(intLineMTransactionLines, strLineKTransactionCount, 5, "0"c)
                                FormatIntToString(intLineKTransactionLines, strLineKTransactionLines, 5, "0"c)
                                FormatDecToString(decLineKValue, strLineKValue, 14, "0"c, "0.00")
                                arrstrStholRecords(intI) = String.Empty & totl.TransDate.Value.Year.ToString.PadLeft(4, "0"c) & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0"c) & intMyTime.ToString.PadLeft(4, "0"c) & strCategoryType & strOutputCategory & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & totl.TransDate.Value.ToString("dd/MM/yy")
                                intB = arrstrStholRecords(intI).Length - 1
                                For intA = 1 To intB
                                    If Mid(arrstrStholRecords(intI), intA, 1) = "+" Then
                                        Mid(arrstrStholRecords(intI), intA, 1) = Space(1)
                                    End If
                                Next
                                'arrstrStholRecords(i) = String.Empty & totl.TransDate.Value.Year.ToString.PadLeft(4, "0") & totl.TransDate.Value.DayOfYear.ToString.PadLeft(3, "0") & intMyTime.ToString.PadLeft(4, "0") & strCategoryType & strOutputCategory & KValue.ToString("##########0.00").PadLeft(14, " ") & strStockAdjustmentSign & Space(1) & intLineMTransactionLines.ToString.PadLeft(4, " ") & Space(1) & intLineKTransactionLines.ToString.PadLeft(4, " ") & Space(1) & totl.TransDate.Value.ToString("dd/MM/yy")
                            End If
                        Next
                    End If
                Next
            End If
            If intContainerSummaryCount > 0 Then
                intI = intI + 1
                strCategoryType = "5LRDG"
                strOutputCategory = "00000IW"
                decLineKValue = intContainerSummaryCount
                intLineKTransactionLines = 0
                intLineMTransactionLines = 0
                FormatIntToString(intLineMTransactionLines, strLineKTransactionCount, 5, "0"c)
                FormatIntToString(intLineKTransactionLines, strLineKTransactionLines, 5, "0"c)
                FormatDecToString(decLineKValue, strLineKValue, 14, "0"c, "0.00")
                arrstrStholRecords(intI) = String.Empty & dateSelectionDate.Year.ToString.PadLeft(4, "0"c) & dateSelectionDate.DayOfYear.ToString.PadLeft(3, "0"c) & "0900" & strCategoryType & strOutputCategory & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & dateSelectionDate.ToString("dd/MM/yy")
                intB = arrstrStholRecords(intI).Length - 1
                For intA = 1 To intB
                    If Mid(arrstrStholRecords(intI), intA, 1) = "+" Then
                        Mid(arrstrStholRecords(intI), intA, 1) = Space(1)
                    End If
                Next
                'arrstrStholRecords(i) = String.Empty & dateSelectionDate.Year.ToString.PadLeft(4, "0") & dateSelectionDate.DayOfYear.ToString.PadLeft(3, "0") & "0900" & strCategoryType & strOutputCategory & KValue.ToString("##########0.00").PadLeft(14, " ") & strStockAdjustmentSign & Space(1) & intLineMTransactionLines.ToString.PadLeft(4, " ") & Space(1) & intLineKTransactionLines.ToString.PadLeft(4, " ") & Space(1) & dateSelectionDate.ToString("dd/MM/yy")
            End If
            dateSelectionDate = dateSelectionDate.AddDays(1)
        End While
        ReDim Preserve arrstrStholRecords(intI)
        Array.Sort(arrstrStholRecords)
        intL = intI
        For intK = 1 To intL
            intM = intK + 1
            If intM < intI Then
                If arrstrStholRecords(intK).Substring(0, 24) = arrstrStholRecords(intM).Substring(0, 24) Then
                    intB = arrstrStholRecords(intK).Length - 1
                    For intA = 1 To intB
                        If Mid(arrstrStholRecords(intK), intA, 1) = "+" Then
                            Mid(arrstrStholRecords(intK), intA, 1) = Space(1)
                        End If
                    Next
                    intB = arrstrStholRecords(intM).Length - 1
                    For intA = 1 To intB
                        If Mid(arrstrStholRecords(intM), intA, 1) = "+" Then
                            Mid(arrstrStholRecords(intM), intA, 1) = Space(1)
                        End If
                    Next
                    decLineKValue = CDec(arrstrStholRecords(intK).Substring(23, 15))
                    'If Mid(arrstrStholRecords(k), 38, 1) = "-" Then
                    '    KValue = KValue * -1
                    'End If
                    decLineMValue = CDec(arrstrStholRecords(intM).Substring(23, 15))
                    'If Mid(arrstrStholRecords(m), 38, 1) = "-" Then
                    '    decLineMValue = decLineMValue * -1
                    'End If
                    decLineMValue = decLineKValue + decLineMValue
                    'decLineMValue = CDec(Mid(arrstrStholRecords(m), 24, 15)) + KValue
                    intLineKTransactionCount = CInt(arrstrStholRecords(intK).Substring(38, 6))
                    intLineMTransactionCount = CInt(arrstrStholRecords(intM).Substring(38, 6)) + intLineKTransactionCount
                    strLineKTransactionCount = arrstrStholRecords(intK).Substring(44, 6)
                    intLineKTransactionLines = 0
                    If strLineKTransactionCount <> String.Empty Then
                        intLineKTransactionLines = CInt(strLineKTransactionCount)
                    End If
                    'intLineKTransactionLines = CInt(Mid(arrstrStholRecords(k), 45, 4))
                    intLineMTransactionLines = 0
                    strLineKTransactionCount = arrstrStholRecords(intM).Substring(44, 6)
                    If strLineKTransactionCount <> String.Empty Then
                        intLineMTransactionLines = CInt(strLineKTransactionCount)
                    End If
                    'intLineMTransactionLines = CInt(Mid(arrstrStholRecords(m), 45, 4)) + intLineKTransactionLines
                    intLineMTransactionLines = intLineMTransactionLines + intLineKTransactionLines

                    dateCheckTransactionDate = CDate(arrstrStholRecords(intM).Substring(50, 8))
                    strLineKValue = String.Empty
                    strLineKTransactionCount = String.Empty
                    strLineKTransactionLines = String.Empty
                    FormatIntToString(intLineMTransactionCount, strLineKTransactionCount, 5, "0"c)
                    FormatIntToString(intLineMTransactionLines, strLineKTransactionLines, 5, "0"c)
                    FormatDecToString(decLineMValue, strLineKValue, 13, "0"c, "0.00")
                    Dim MyString As String
                    intB = strLineKValue.Length
                    arrstrStholRecords(intM) = String.Empty & arrstrStholRecords(intK).Substring(0, 24) & strLineKValue & strLineKTransactionCount & strLineKTransactionLines & dateCheckTransactionDate.ToString("dd/MM/yy")
                    'arrstrStholRecords(m) = String.Empty & Mid(arrstrStholRecords(k), 1, 24) & decLineMValue.ToString("##########0.00").PadLeft(14, "0") & Space(1) & strLineKTransactionCount & strLineKTransactionLines & dateCheckTransactionDate.ToString("dd/MM/yy")

                    MyString = arrstrStholRecords(intM)
                    intB = arrstrStholRecords(intM).Length - 1
                    For intA = 1 To intB
                        If Mid(arrstrStholRecords(intM), intA, 1) = "+" Then
                            Mid(arrstrStholRecords(intM), intA, 1) = Space(1)
                        End If
                    Next
                    MyString = arrstrStholRecords(intM)
                    'arrstrStholRecords(m) = String.Empty & Mid(arrstrStholRecords(k), 1, 24) & KValue.ToString("##########0.00").PadLeft(14, " ") & strStockAdjustmentSign & Space(1) & intLineMTransactionCount.ToString.PadLeft(4, " ") & Space(1) & intLineMTransactionLines.ToString.PadLeft(4, " ") & Space(1) & dateCheckTransactionDate.ToString("dd/MM/yy")
                    arrstrStholRecords(intK) = String.Empty
                End If
            End If
        Next
        Array.Sort(arrstrStholRecords)
        'k = i - 1
        intK = intI
        For intJ = 1 To intK
            If arrstrStholRecords(intJ) <> String.Empty Then
                strCheckYear = arrstrStholRecords(intJ).Substring(0, 4)
                strCheckDay = arrstrStholRecords(intJ).Substring(4, 3)
                strCheckTime = arrstrStholRecords(intJ).Substring(7, 4)
                strCheckType = arrstrStholRecords(intJ).Substring(11, 5)
                strCheckPrimaryFunction = arrstrStholRecords(intJ).Substring(16, 7)
                'strWorkString = Mid(arrstrStholRecords(j), 24, 15)
                decCheckValue = CDec(arrstrStholRecords(intJ).Substring(23, 15))
                'strWorkString = Mid(arrstrStholRecords(j), 51, 9)
                dateCheckTransactionDate = CDate(arrstrStholRecords(intJ).Substring(50, 8))
                'strWorkString = Mid(arrstrStholRecords(intJ), 37, 4)
                strWorkString = arrstrStholRecords(intJ).Substring(38, 6)
                intCheckTransactionCount = 0
                If strWorkString <> String.Empty Then
                    intCheckTransactionCount = CInt(strWorkString)
                End If
                strWorkString = arrstrStholRecords(intJ).Substring(44, 6)
                intCheckTransactionLines = 0
                If strWorkString <> String.Empty Then
                    intCheckTransactionLines = CInt(strWorkString)
                End If
                dateCheckTransactionDate = CDate(arrstrStholRecords(intJ).Substring(50, 8))
                If decCheckValue <> 0 Or intCheckTransactionCount <> 0 Or intCheckTransactionLines <> 0 Then
                    If strStholOutputText <> String.Empty Then
                        If strStholOutputText.EndsWith(vbCrLf) = False Then strStholOutputText = strStholOutputText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strStholOutputText = strStholOutputText.ToString.TrimEnd(" "c) & Retopt.Store.Value.ToString.PadLeft(8, "0"c) & strCheckType.Substring(3, 1) & dateCheckTransactionDate.ToString("yyyyMMdd") & strCheckTime & strCheckType.Substring(4, 1) & strCheckPrimaryFunction & decCheckValue.ToString("###########0.00").PadLeft(15, " "c) & intCheckTransactionCount.ToString.PadLeft(5, " "c) & intCheckTransactionLines.ToString.PadLeft(5, " "c)
                    intRecordsOutput = intRecordsOutput + 1
                    If strStholOutputText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(strStholFileName, strStholOutputText)
                        strStholOutputText = String.Empty
                    End If
                End If
            End If
        Next
        If strStholOutputText.Length > 0 Then
            PutSthoToDisc(strStholFileName, strStholOutputText)
            strStholOutputText = String.Empty
        End If
        'Create CONTROL FILES
        Dim strStholisControl As String = String.Empty & strStholPathName & "\" & "STHOLIS.CTL"
        Dim strStholttControl As String = String.Empty & strStholPathName & "\" & "STHOLTT." & Retopt.Store.Value.ToString.PadLeft(3, "0"c)
        If File.Exists(strStholisControl) Then
            My.Computer.FileSystem.DeleteFile(strStholisControl)
        End If
        PutSthoToDisc(strStholisControl, "STHOL")
        PutSthoToDisc(strStholttControl, ("STHOL file created on :" & Space(1) & Today.ToString("dd/MM/yy")))

        strWorkString = "Processing STHOL (OL) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strStholFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

    End Sub ' OutputSthol

    Public Sub ProcessStockMaster()

        Const TOT_OBSDEL As Integer = 1
        Const TOT_MDNDMY As Integer = 2
        Const TOT_NO_ORD As Integer = 3
        Const TOT_NONSTK As Integer = 4
        Const TOT_SINGLE As Integer = 5
        Const TOT_NONREC As Integer = 6
        Const TOT_ALLSKU As Integer = 9
        Const TOT_IN_STOCK As Integer = 10
        Const TOT_OUT_STOCK As Integer = 11
        Const TOT_UND_STOCK As Integer = 12

        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)
        Dim DRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)
        Dim intHighSku As Integer = 999999
        Dim intLowSku As Integer = 0
        Dim intSkuDrlsNotCommed As Integer = 0
        Dim arrstrSkusNotCommed As String() = New String(99999) {}
        Dim arrintAsCounts As Integer() = New Integer(14) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        Dim intI As Integer
        Dim intJ As Integer = 0
        Dim intK As Integer = 0
        Dim intX As Integer = 0
        Dim intY As Integer = 0
        Dim intASNumb As Integer = 0

        'Dim strSthoaText As String = String.Empty

        Dim nulldate As Date


        Dim intSkuCount As Integer = 0
        Dim boolNeedAO As Boolean = False
        Dim intWorkMinimum As Integer = 0
        Dim intWorkPromotionMinimum As Integer = 0

        Dim intSthpaRecordsOut As Integer = 0
        Dim intSthoaRecordsOut As Integer = 0
        Dim intSthojRecordsOut As Integer = 0

        Dim strSTHOARecTypeA5SkuNumber As String = String.Empty ' SKU Code                                             - Data +22
        Dim strSTHOARecTypeA5TotalUnitsSold As String = String.Empty '  7.0 Signed Numeric Total Units Sold                 - Data +28  
        Dim strSTHOARecTypeA5TotalValueSold As String = String.Empty ' 10.2 Signed Numeric Total Value Sold                 - Data +36
        Dim strSTHOARecTypeA5ClosingStockQuantity As String = String.Empty '  7.0 Signed Numeric Closing Stock Quantity           - Data +48  
        Dim strSTHOARecTypeA5ClosingStockValue As String = String.Empty ' 10.2 Signed Numeric Closing Stock Value              - Data +56
        Dim strSTHOARecTypeA5JulianDate As String = String.Empty ' Julian Date                                          - Data +68
        Dim strSTHOARecTypeA5ReceiptsQuantity As String = String.Empty '  7.0 Signed Numeric Receipts Quantity                - Data +73
        Dim strSTHOARecTypeA5DepartmentRecord As String = String.Empty ' -- "Y" = THIS IS A DEPT RECORD                       - Data +81
        Dim strSTHOARecTypeA5ReceiptsValue As String = String.Empty ' 10.2 Signed Numeric Receipts Value                   - Data +82
        Dim strSTHOARecTypeA5MarkdownUnitsSold As String = String.Empty '  7.0 Signed Numeric Total Mark-Down Units Sold       - Data +94
        Dim strSTHOARecTypeA5MarkdownValueSold As String = String.Empty ' 10.2 Signed Numeric Total Mark-Down Value Sold       - Data +102
        Dim strSTHOARecTypeA5ClosingMarkdownQuantity As String = String.Empty '  7.0 Signed Numeric Closing Mark-Down Stock Quantity - Data +114
        Dim strSTHOARecTypeA5ClosingMarkdownValue As String = String.Empty '  7.0 Signed Numeric Closing Mark-Down Stock Quantity - Data +122
        Dim strSTHOARecTypeA5OnOrderQuantity As String = String.Empty ' 6.0  Signed Numeric On order quantity - Data +134
        Dim strSTHOJRecTypeJDSkuNumber As String = String.Empty ' Sku Number received
        Dim strSTHOJRecTypeJDOnHandQuantity As String = String.Empty ' Signed numeric On Hand quantity
        Dim strSTHOJRecTypeJDOpenReturnsQuantity As String = String.Empty ' Signed numeric Open returns quantity
        Dim strSTHOJRecTypeJDMarkdownQuantity As String = String.Empty ' Signed numeric Mark Down quantity
        Dim strSTHOJRecTypeJDWriteOffQuantity As String = String.Empty ' Signed numeric Write Off quantity
        Dim strSTHOJRecTypeJDNonStockedItemIndicator As String = String.Empty ' Non-stocked indicator
        Dim strSTHOJRecTypeJDObsoletedItemIndicator As String = String.Empty ' Obsolete indicator
        Dim strSTHOJRecTypeJDValidationIndicator As String = String.Empty ' Validation Indicator - "Y" = Run in Night "N" = Run during trading
        Dim strSTHOJRecTypeJDQuantityDrlsNotYetCommed As String = String.Empty ' Quantity of DRLS not yet commed
        Dim strSTHOJRecTypeJDCurrentSalePrice As String = String.Empty ' Current sale price of sku

        Dim strSTHPARecTypeAOSkuNumber As String = String.Empty ' Sku Number                                   - DATA+22
        Dim strSTHPARecTypeAOOnHandQuantity As String = String.Empty ' 6.0 signed numeric On Hand Stock             - DATA+28 
        Dim strSTHPARecTypeAOOnOrderQuantity As String = String.Empty ' 6.0 signed numeric On Order Quantity         - DATA+35 
        Dim strSTHPARecTypeAOPromotionCurrentIndicator As String = String.Empty ' Promotion Current Indicator                  - DATA+42 
        Dim strSTHPARecTypeAOSuggestedOrderQuantityPattern As String = String.Empty ' Murdoch SOQ Classification (QS:PATT)         - DATA+43 

        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadField(SysDates.RetryMode)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()
        Dim strJulianDate = SysDates.Today.Value.ToString("yy") & SysDates.Today.Value.DayOfYear.ToString("000")
        strWorkString = "Processing Stock Master Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
        If boolDoJDACompare = True Then
            DRLHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, DRLHeader.CommedToHO, False)
            DRLHeader.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            DRLHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, DRLHeader.Type, "0")
            DRLHeader.Headers = DRLHeader.LoadMatches

            intJ = 0
            For Each header As BOPurchases.cDrlHeader In DRLHeader.Headers
                For Each detail As BOPurchases.cDrlDetail In header.Details
                    strWorkString = "Checking : " & detail.DrlNumber.Value.PadLeft(6, "0"c) & Space(1) & detail.SkuNumber.Value.PadLeft(6, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    arrstrSkusNotCommed(intJ) = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c) & detail.ReceivedQty.Value.ToString.PadLeft(6, "0"c)
                    intJ = intJ + 1
                Next
            Next

            ReDim Preserve arrstrSkusNotCommed(intJ)
            Array.Sort(arrstrSkusNotCommed)

        End If
        dateHashDate = SysDates.Today.Value
        strSthojText = String.Empty
        strSthpaText = String.Empty
        strSthoaText = String.Empty

        If SysDates.RetryMode.Value = True Then
            _RunningInNight = False
        End If
        intX = intLowSku
        intSkuCount = 0
        intK = arrstrSkusNotCommed.Count
        intJ = 0
        While intY < intHighSku

            intY = intX + 10000
            If intY > intHighSku Then
                intY = intHighSku
            End If
            strWorkString = "Selecting Sku Range : " & intX.ToString.PadLeft(6, "0"c) & " - " & intY.ToString.PadLeft(6, "0"c)
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            StockMaster.ClearLoadFilter()
            StockMaster.ClearLists()

            StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SkuNumber, intX.ToString.PadLeft(6, "0"c))
            StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intY.ToString.PadLeft(6, "0"c))
            If boolDoSales = True And boolDoJDACompare = False And boolDoSthpaOutOfStocks = False Then
                StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.ActivityToday, True)
            End If
            StockMaster.SortBy(StockMaster.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            StockMaster.Stocks = StockMaster.LoadMatches()

            For Each stocks As BOStock.cStock In StockMaster.Stocks
                strWorkString = "Checking : " & stocks.SkuNumber.Value.PadLeft(6, "0"c) & Space(1) & stocks.Description.Value
                ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                intSkuCount = intSkuCount + 1
                If boolDoJDACompare = True Then
                    intSkuDrlsNotCommed = 0
                    For intI = intJ To (intK - 1)
                        If arrstrSkusNotCommed(intI) IsNot Nothing Then
                            If arrstrSkusNotCommed(intI).Substring(0, 6) > stocks.SkuNumber.Value.ToString.PadLeft(6, "0"c) Then Exit For
                            If stocks.SkuNumber.Value.ToString.PadLeft(6, "0"c) = arrstrSkusNotCommed(intI).Substring(0, 6) Then
                                intSkuDrlsNotCommed += CInt(arrstrSkusNotCommed(intI).Substring(6, 6))
                            End If
                        End If
                    Next
                    If intI < intK Then intJ = intI
                    decHashValue = stocks.StockOnHand.Value
                    If decHashValue < 0 Then decHashValue = decHashValue * -1
                    strSTHOJRecTypeJDOnHandQuantity = String.Empty
                    strSTHOJRecTypeJDOpenReturnsQuantity = String.Empty
                    strSTHOJRecTypeJDMarkdownQuantity = String.Empty
                    strSTHOJRecTypeJDWriteOffQuantity = String.Empty
                    strSTHOJRecTypeJDQuantityDrlsNotYetCommed = String.Empty

                    SetupHash("JD", decHashValue, dateHashDate)
                    strSTHOJRecTypeJDSkuNumber = stocks.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                    FormatIntToString(stocks.StockOnHand.Value, strSTHOJRecTypeJDOnHandQuantity, 6, " "c)
                    FormatIntToString(stocks.UnitsInOpenReturns.Value, strSTHOJRecTypeJDOpenReturnsQuantity, 6, " "c)
                    FormatIntToString(stocks.MarkDownQuantity.Value, strSTHOJRecTypeJDMarkdownQuantity, 6, " "c)
                    FormatIntToString(stocks.WriteOffQuantity.Value, strSTHOJRecTypeJDWriteOffQuantity, 6, " "c)
                    strSTHOJRecTypeJDNonStockedItemIndicator = "N"
                    If stocks.NonStockItem.Value = True Then
                        strSTHOJRecTypeJDNonStockedItemIndicator = "Y"
                    End If
                    strSTHOJRecTypeJDObsoletedItemIndicator = "N"
                    If stocks.ItemObsolete.Value = True Then
                        strSTHOJRecTypeJDObsoletedItemIndicator = "Y"
                    End If
                    strSTHOJRecTypeJDValidationIndicator = "N"
                    If _RunningInNight = True Then
                        strSTHOJRecTypeJDValidationIndicator = "Y"
                    End If
                    FormatIntToString(intSkuDrlsNotCommed, strSTHOJRecTypeJDQuantityDrlsNotYetCommed, 6, " "c)
                    strStockAdjustmentSign = "+"
                    strSTHOJRecTypeJDCurrentSalePrice = stocks.NormalSellPrice.Value.ToString("0.00").PadLeft(9, " "c) ' & strStockAdjustmentSign

                    If strSthojText <> String.Empty Then
                        If strSthojText.EndsWith(vbCrLf) = False Then strSthojText = strSthojText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthojText = strSthojText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSTHOJRecTypeJDSkuNumber & strSTHOJRecTypeJDOnHandQuantity & strSTHOJRecTypeJDOpenReturnsQuantity & strSTHOJRecTypeJDMarkdownQuantity & strSTHOJRecTypeJDWriteOffQuantity & strSTHOJRecTypeJDNonStockedItemIndicator & strSTHOJRecTypeJDObsoletedItemIndicator & strSTHOJRecTypeJDValidationIndicator & strSTHOJRecTypeJDQuantityDrlsNotYetCommed & strSTHOJRecTypeJDCurrentSalePrice '& vbCrLf
                    intSthojRecordsOut = intSthojRecordsOut + 1

                    ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthojRecordsOut.ToString & " - STHOJ"
                    ProcessTransmissionsProgress.Show()
                    If strSthojText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(strSthojFileName, strSthojText)
                        strSthojText = String.Empty
                    End If
                End If

                intASNumb = 0
                If boolDoSthpaOutOfStocks = True Then ' Doing STHPA out of stocks
                    boolNeedAO = False

                    intASNumb = TOT_OBSDEL
                    'Obsolete or deleted
                    If stocks.ItemObsolete.Value = False And stocks.ItemDeleted.Value = False Then
                        intASNumb = TOT_MDNDMY
                        If (stocks.CatchAll.Value = False) And (stocks.IsMarkdown.Value = False) Then ' Catch all and Markdowns
                            intASNumb = TOT_NO_ORD
                            If (stocks.DoNotOrder.Value = False) And ((stocks.FinalOrderDate.Value >= SysDates.Today.Value) Or (stocks.FinalOrderDate.Value = nulldate)) Then ' Do NOT order
                                'If (stocks.DoNotOrder.Value = False) Or ((stocks.DoNotOrder.Value = True) And (stocks.FinalOrderDate.Value >= SysDates.Today.Value)) Then ' Do NOT order
                                intASNumb = TOT_NONSTK
                                If stocks.NonStockItem.Value = False Then ' Non Stocked item
                                    intASNumb = TOT_SINGLE
                                    If stocks.RelatedItemSingle.Value = False Then 'Related item single
                                        intASNumb = TOT_NONREC
                                        If stocks.DateFirstStock.Value <> nulldate Then 'First has stock
                                            intASNumb = TOT_IN_STOCK
                                            If stocks.StockOnHand.Value < 1 Then
                                                intASNumb = TOT_OUT_STOCK
                                                boolNeedAO = True
                                            Else
                                                intWorkMinimum = 0
                                                intWorkPromotionMinimum = 0
                                                intWorkMinimum = stocks.MinQuantity.Value
                                                If stocks.SpacemanDisplayFctr.Value > 0 And stocks.SpacemanDisplayFctr.Value > stocks.MinQuantity.Value Then
                                                    intWorkMinimum = stocks.SpacemanDisplayFctr.Value
                                                End If
                                                If stocks.PromotionalMinStart.Value <= SysDates.Today.Value Then
                                                    If SysDates.Today.Value < stocks.PromotionalMinEnd.Value Then
                                                        intWorkPromotionMinimum = stocks.PromotionalMinimum.Value
                                                    End If
                                                End If
                                                intWorkMinimum += intWorkPromotionMinimum
                                                If stocks.StockOnHand.Value < intWorkMinimum Then
                                                    boolNeedAO = True
                                                    arrintAsCounts(TOT_UND_STOCK) += 1
                                                    Trace.WriteLine(stocks.SkuNumber.Value & "," & stocks.MinQuantity.Value & "," & stocks.SpacemanDisplayFctr.Value & "," & stocks.PromotionalMinStart.Value & "," & stocks.PromotionalMinEnd.Value)
                                                End If
                                            End If
                                        End If 'First has stock
                                    End If 'Related item single
                                End If ' Do NOT order
                            End If ' Catch all
                        End If 'Obsolete or deleted
                    Else
                        If (stocks.StockOnHand.Value > 0) Then arrintAsCounts(13) += 1

                    End If ' Doing STHPA out of stocks
                    If boolNeedAO = True Then
                        strSTHPARecTypeAOOnHandQuantity = String.Empty
                        strSTHPARecTypeAOOnOrderQuantity = String.Empty
                        strSTHPARecTypeAOSkuNumber = stocks.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                        decHashValue = CDec(stocks.SkuNumber.Value)
                        FormatIntToString(stocks.StockOnHand.Value, strSTHPARecTypeAOOnHandQuantity, 6, " "c)
                        FormatIntToString(stocks.StockOnOrder.Value, strSTHPARecTypeAOOnOrderQuantity, 6, " "c)
                        strSTHPARecTypeAOPromotionCurrentIndicator = "N"
                        If stocks.PromotionalMinimum.Value > 0 Then
                            If SysDates.Today.Value >= stocks.PromotionalMinStart.Value Then
                                If SysDates.Today.Value < stocks.PromotionalMinEnd.Value Then
                                    strSTHPARecTypeAOPromotionCurrentIndicator = "Y"
                                End If
                            End If
                        End If

                        strSTHPARecTypeAOSuggestedOrderQuantityPattern = " " 'STKMAS:DEMANDPATTERN is replacing QS:PATT FROM SOQSKU
                        Dim strdempat As String = stocks.DemandPattern.Value.ToUpper
                        If strdempat.StartsWith("   ") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = " "
                        If strdempat.StartsWith("ERRATIC") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "E"
                        If strdempat.StartsWith("FAST") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "F"
                        If strdempat.StartsWith("ISNEW") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "N"
                        If strdempat.StartsWith("LUMPY") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "L"
                        If strdempat.StartsWith("OBSOLETE") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "O"
                        If strdempat.StartsWith("SLOW") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "S"
                        If strdempat.StartsWith("SUPERCEDED") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "D"
                        If strdempat.StartsWith("TRENDDOWN") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "G"
                        If strdempat.StartsWith("TRENDUP") Then strSTHPARecTypeAOSuggestedOrderQuantityPattern = "T"

                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.None Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = " "
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Erratic Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "E"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Fast Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "F"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.IsNew Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "N"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Lumpy Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "L"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Obsolete Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "O"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Slow Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "S"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.Superceded Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "D"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.TrendDown Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "G"
                        'End If
                        'If stocks.DemandPatternEnum = BOStock.cStock.DemandPatterns.TrendUp Then
                        '    strSTHPARecTypeAOSuggestedOrderQuantityPattern = "T"
                        'End If

                        SetupHash("AO", decHashValue, dateHashDate)
                        If strSthpaText <> String.Empty Then
                            If strSthpaText.EndsWith(vbCrLf) = False Then strSthpaText = strSthpaText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthpaText = strSthpaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSTHPARecTypeAOSkuNumber & strSTHPARecTypeAOOnHandQuantity & strSTHPARecTypeAOOnOrderQuantity & strSTHPARecTypeAOPromotionCurrentIndicator & strSTHPARecTypeAOSuggestedOrderQuantityPattern.PadLeft(1, " "c) '& vbCrLf
                        intSthpaRecordsOut = intSthpaRecordsOut + 1
                        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthpaRecordsOut.ToString & " - STHPA"
                        ProcessTransmissionsProgress.Show()
                        If strSthpaText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthpaFileName, strSthpaText)
                            strSthpaText = String.Empty
                        End If
                    End If
                    arrintAsCounts(intASNumb) += 1
                    arrintAsCounts(TOT_ALLSKU) += 1
                    If stocks.ItemDeleted.Value = True Or stocks.ItemObsolete.Value = True Then
                        If stocks.StockOnHand.Value > 0 Then
                            'arrintAsCounts(TOT_UND_STOCK) += 1
                        End If
                    End If
                End If
                If boolDoSales = True Then
                    If stocks.ActivityToday.Value = True Then
                        strSTHOARecTypeA5SkuNumber = String.Empty
                        strSTHOARecTypeA5TotalUnitsSold = String.Empty
                        strSTHOARecTypeA5TotalValueSold = String.Empty
                        strSTHOARecTypeA5ReceiptsQuantity = String.Empty
                        strSTHOARecTypeA5ReceiptsValue = String.Empty
                        strSTHOARecTypeA5MarkdownUnitsSold = String.Empty
                        strSTHOARecTypeA5MarkdownValueSold = String.Empty
                        strSTHOARecTypeA5ClosingMarkdownQuantity = String.Empty
                        strSTHOARecTypeA5ClosingMarkdownValue = String.Empty
                        strSTHOARecTypeA5ClosingStockQuantity = String.Empty
                        strSTHOARecTypeA5ClosingStockValue = String.Empty
                        SoldToday(stocks.SkuNumber.Value.ToString.PadRight(6, "0"c), SysDates.Today.Value)
                        'SetupHash("A5", decHashValue, dateHashDate)
                        FormatIntToString(CInt(stocks.SkuNumber.Value), strSTHOARecTypeA5SkuNumber, 6, "0"c)
                        FormatIntToString(stocks.UnitsSoldYesterday.Value, strSTHOARecTypeA5TotalUnitsSold, 6, " "c)
                        FormatDecToString(stocks.ValueSoldYesterday.Value, strSTHOARecTypeA5TotalValueSold, 11, " "c, "0.00")
                        FormatIntToString((stocks.StockOnHand.Value + stocks.MarkDownQuantity.Value), strSTHOARecTypeA5ClosingStockQuantity, 7, " "c)
                        FormatDecToString(((stocks.StockOnHand.Value + stocks.MarkDownQuantity.Value) * stocks.NormalSellPrice.Value), strSTHOARecTypeA5ClosingStockValue, 11, " "c, "0.00")
                        strSTHOARecTypeA5JulianDate = strJulianDate.PadLeft(5, "0"c)
                        FormatIntToString(stocks.UnitsReceivedToday.Value, strSTHOARecTypeA5ReceiptsQuantity, 7, " "c)
                        FormatDecToString(stocks.ValueReceivedToday.Value, strSTHOARecTypeA5ReceiptsValue, 11, " "c, "0.00")
                        FormatIntToString(intMarkdownUnitsToday, strSTHOARecTypeA5MarkdownUnitsSold, 7, " "c)
                        FormatDecToString(decMarkdownValueToday, strSTHOARecTypeA5MarkdownValueSold, 11, " "c, "0.00")
                        FormatIntToString(stocks.MarkDownQuantity.Value, strSTHOARecTypeA5ClosingMarkdownQuantity, 7, " "c)
                        FormatDecToString((stocks.MarkDownQuantity.Value * stocks.NormalSellPrice.Value), strSTHOARecTypeA5ClosingMarkdownValue, 11, " "c, "0.00")
                        'FormatIntToString(stocks.StockOnOrder.Value, strSTHOARecTypeA5OnOrderQuantity, 6, " ")
                        FormatIntToString(0, strSTHOARecTypeA5OnOrderQuantity, 6, " "c)
                        strSTHOARecTypeA5DepartmentRecord = "N"
                        If strSTHOARecTypeA5DepartmentRecord = "N" Then
                            strSTHOARecTypeA5DepartmentRecord = String.Format(" ")
                        End If

                        decHashValue = stocks.ValueSoldYesterday.Value
                        SetupHash("A5", decHashValue, dateHashDate)
                        If strSthoaText <> String.Empty Then
                            If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSTHOARecTypeA5SkuNumber & strSTHOARecTypeA5TotalUnitsSold & strSTHOARecTypeA5TotalValueSold & strSTHOARecTypeA5ClosingStockQuantity & strSTHOARecTypeA5ClosingStockValue & strSTHOARecTypeA5JulianDate & strSTHOARecTypeA5ReceiptsQuantity & strSTHOARecTypeA5DepartmentRecord & strSTHOARecTypeA5ReceiptsValue & strSTHOARecTypeA5MarkdownUnitsSold & strSTHOARecTypeA5MarkdownValueSold & strSTHOARecTypeA5ClosingMarkdownQuantity & strSTHOARecTypeA5ClosingMarkdownValue '& strSTHOARecTypeA5OnOrderQuantity '& stocks.ValueAnnualUsage.Value & stocks.BallastItem.Value '& vbCrLf
                        intSthoaRecordsOut = intSthoaRecordsOut + 1
                        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthoaRecordsOut.ToString & " - STHOA"
                        ProcessTransmissionsProgress.Show()
                        If strSthoaText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthoaFileName, strSthoaText)
                            strSthoaText = String.Empty
                        End If
                    End If
                End If
            Next
            intX = intY
        End While
        If boolDoSthpaOutOfStocks = True Then
            decHashValue = arrintAsCounts(9)
            ' Outputs type AS record to STHPA
            SetupHash("AS", decHashValue, dateHashDate)
            If strSthpaText <> String.Empty Then
                If strSthpaText.EndsWith(vbCrLf) = False Then strSthpaText = strSthpaText.ToString.TrimEnd(" "c) & vbCrLf
            End If

            Dim ModelBO As New BOStock.cModels(_Oasys3DB)
            arrintAsCounts(14) = ModelBO.NumberOfActiveModels
            ModelBO.Dispose()
            strSthpaText = strSthpaText & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & arrintAsCounts(1).ToString.PadLeft(6, " "c) & arrintAsCounts(2).ToString.PadLeft(7, " "c) & arrintAsCounts(3).ToString.PadLeft(7, " "c) & arrintAsCounts(4).ToString.PadLeft(7, " "c) & arrintAsCounts(5).ToString.PadLeft(7, " "c) & arrintAsCounts(6).ToString.PadLeft(7, " "c) & arrintAsCounts(7).ToString.PadLeft(7, " "c) & arrintAsCounts(8).ToString.PadLeft(7, " "c) & arrintAsCounts(9).ToString.PadLeft(7, " "c) & arrintAsCounts(10).ToString.PadLeft(7, " "c) & arrintAsCounts(11).ToString.PadLeft(7, " "c) & arrintAsCounts(12).ToString.PadLeft(7, " "c) & arrintAsCounts(13).ToString.PadLeft(7, " "c) & arrintAsCounts(14).ToString.PadLeft(7, " "c) & Space(1)
            intSthpaRecordsOut = intSthpaRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthpaRecordsOut.ToString & " - STHPA"
            ProcessTransmissionsProgress.Show()
            PutSthoToDisc(strSthpaFileName, strSthpaText)
            strSthpaText = String.Empty
        End If
        If boolDoJDACompare = True Then
            If strSthojText.Length > 0 Then
                PutSthoToDisc(strSthojFileName, strSthojText)
                strSthojText = String.Empty
            End If
        End If
        If boolDoSales = True Then
            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        strWorkString = "Processing Stock Master Ended : " & TimeOfDay.ToString("hh:mm:ss") & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        strWorkString = strWorkString & "Processing Stock Master - STHOA Records output to : " & strSthoaFileName & ": " & intSthoaRecordsOut.ToString("#####0").PadLeft(6, " "c) & vbCrLf
        strWorkString = strWorkString & "Processing Stock Master - STHOJ Records output to : " & strSthojFileName & ": " & intSthojRecordsOut.ToString("#####0").PadLeft(6, " "c) & vbCrLf
        strWorkString = strWorkString & "Processing Stock Master - STHPA Records output to : " & strSthpaFileName & ": " & intSthpaRecordsOut.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        OutputSthoLog(strWorkString)
    End Sub ' ProcessStockMaster

    Public Sub OutputRelitmSthoa()

        Using SysDates As New BOSystem.cSystemDates(_Oasys3DB)
            SysDates.AddLoadField(SysDates.WeekEnding01)
            SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
            SysDates.LoadMatches()
            dateHashDate = SysDates.WeekEnding01.Value()
        End Using

        _RunningInNight = False
        strSthoaText = String.Empty

        Dim Relitm As New BOStock.cRelatedItems(_Oasys3DB)
        Relitm.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Relitm.Deleted, 0)
        Relitm.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Relitm.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Relitm.MarkUpPriorWeek, 0)
        Relitm.SortBy(Relitm.SingleItem.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        Dim ColRi As List(Of BOStock.cRelatedItems) = Relitm.LoadMatches()

        intRecordsOutput = 0
        strWorkString = "Processing STHOA Related Items (RI) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        Dim decWorkDecimal As Decimal = 0
        Dim intWorkInteger As Integer = 0
        For Each Record As BOStock.cRelatedItems In ColRi
            strWorkString = "Checking : " & Space(1) & Record.SingleItem.Value.PadLeft(6, "0"c)
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()

            _STHOARecTypeA7SkuNumber = Record.SingleItem.Value.ToString.PadLeft(6, "0"c)
            _STHOARecTypeA7MarkupOrDownCode = "3"

            FormatIntToString(CInt(Record.NoSinglesPerPack.Value), _STHOARecTypeA7MarkupOrDownUnits, 7, " "c)
            decWorkDecimal = Record.MarkUpPriorWeek.Value
            FormatDecToString(decWorkDecimal, _STHOARecTypeA7MarkupOrDownValue, 11, " "c, "0.00")
            decHashValue = decWorkDecimal
            decWorkDecimal = 0

            FormatDecToString(decWorkDecimal, _STHOARecTypeA7OldSellingPrice, 11, " "c, "0.00")
            FormatDecToString(decWorkDecimal, _STHOARecTypeA7NewSellingPrice, 11, " "c, "0.00")
            FormatIntToString(CInt(Record.BulkOrPackageItem.Value), _STHOARecTypeA7BulkSkuNumber, 6, "0"c)

            intWorkInteger = CInt(Record.TransForMUPPriorWeek.Value / Record.NoSinglesPerPack.Value)
            FormatIntToString(intWorkInteger, _STHOARecTypeA7BulkUnitsAdjusted, 6, " "c)
            SetupHash("A7", decHashValue, dateHashDate)

            If strSthoaText <> String.Empty Then
                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If

            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA7SkuNumber & _STHOARecTypeA7MarkupOrDownCode & _STHOARecTypeA7MarkupOrDownUnits & _STHOARecTypeA7MarkupOrDownValue & _STHOARecTypeA7OldSellingPrice & _STHOARecTypeA7NewSellingPrice & _STHOARecTypeA7BulkSkuNumber & _STHOARecTypeA7BulkUnitsAdjusted & _STHOARecTypeA7PriceOverrideReasonCode '& vbCrLf
            intRecordsOutput = intRecordsOutput + 1

            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        Next

        If strSthoaText.Length > 0 Then
            PutSthoToDisc(strSthoaFileName, strSthoaText)
            strSthoaText = String.Empty
        End If

        strWorkString = "Processing STHOA Related Items (RI) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

    End Sub ' OutputRelitmSthoa

    Public Sub ProcessPurchaseOrders()
        ' Process Purchase Orders
        Dim SupplierMaster As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim ColSm As New List(Of BOPurchases.cSupplierMaster)
        Dim SupplierDetail As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColSd As New List(Of BOPurchases.cSupplierDetail)
        'Dim SupplierNotes As New BOPurchases.CS
        Dim PurchaseOrderHeader As New BOPurchases.cPurchaseHeader(_Oasys3DB)
        Dim ColPh As New List(Of BOPurchases.cPurchaseHeader)
        Dim PurchaseOrderDetail As New BOPurchases.cPurchaseLine(_Oasys3DB)
        Dim ColPl As New List(Of BOPurchases.cPurchaseLine)
        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)

        Dim intSupplierSOQFrequency As Integer = 0
        Dim intSuggestedOrderQuantityFrequency As Integer = 0
        Dim intCheckSOQDayOfWeek As Integer = 64
        Dim intWorkDayNumber As Integer = 7
        Dim intX1 As Integer = 9
        Dim intLineNumber As Integer = 0
        Dim intDoDayOfWeek1 As Integer = 0
        Dim intDoDayOfWeek2 As Integer = 0
        Dim boolCheckSuggestedOrderQuantity As Boolean = False
        Dim boolDoToday As Boolean = False
        Dim boolNeedNullPurchaseOrder As Boolean = False

        Dim strSthpoRecordTypeOPPurchaseOrderNumber As String = String.Empty ' Store Assigned Purchase Order Number
        Dim strSthpoRecordTypeOPSupplierNumber As String = String.Empty ' Supplier Number
        Dim strSthpoRecordTypeOPSOQControlNumber As String = String.Empty  ' SOQ Control Number
        Dim strSthpoRecordTypeOPDatePurchaseOrderWasRaised As String = String.Empty ' Date Purchase Order Raised
        Dim strSthpoRecordTypeOPExpectedDeliveryDate As String = String.Empty  ' Expected Delivery Date
        Dim strSthpoRecordTypeOPSOQDayCode As String = String.Empty  ' SOQ day code from Store Supplier file
        Dim strSthpoRecordTypeOPBreakBulkCentreFlag As String = String.Empty  ' Store BBC Flag on Order
        Dim strSthpoRecordTypeOPNullOrderForThisSupplier As String = String.Empty ' "Y" = Null Order for this supplier
        Dim strSthpoRecordTypeOPIsATradanetSupplier As String = String.Empty ' "Y" = TRADANET supplier
        Dim strSthpoRecordTypeOPHeadOfficeOrderNumber As String = String.Empty  ' H/O order number (substitution/allocation orders)
        Dim strSthpoRecordTypeOPWhoRaisedThisOrder As String = String.Empty  ' Order Raised by ?????

        Dim strSthpoRecordTypeODSkuNumber As String = String.Empty  ' Sku Number to order
        Dim strSthpoRecordTypeODPurchaseOrderNumber As String = String.Empty ' Store Assigned Purchase Order Number
        Dim strSthpoRecordTypeODintLineNumber As String = String.Empty ' Purchase Order Line Number From Store
        Dim strSthpoRecordTypeODOrderQuantity As String = String.Empty  ' 6.0 signed numeric Order Quantity
        dateHashDate = Today
        strSthpoText = String.Empty

        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadField(SysDates.TodayDayCode)
        SysDates.AddLoadField(SysDates.TomorrowsDayCode)
        SysDates.AddLoadField(SysDates.DaysOpen)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()

        'Set Day of Week for Direct Supplier for SOQ checking(BBC is blank)
        If SysDates.TodayDayCode.Value = SysDates.DaysOpen.Value Then
            intDoDayOfWeek1 = 1
        Else
            intDoDayOfWeek1 = CInt(SysDates.TomorrowsDayCode.Value)
        End If
        'Set Day of Week for BBC is not blank/Non-Direct Supplier for SOQ checking
        intDoDayOfWeek2 = 1 + intDoDayOfWeek1
        If intDoDayOfWeek2 > SysDates.DaysOpen.Value Then
            intDoDayOfWeek2 = CInt(intDoDayOfWeek2 - SysDates.DaysOpen.Value)
        End If

        'Get List of all active supplier to check 
        SupplierMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierMaster.DeletedByHO, 0)
        SupplierMaster.SortBy(SupplierMaster.Number.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ColSm = SupplierMaster.LoadMatches
        boolNeedNullPurchaseOrder = False
        intRecordsOutput = 0
        strWorkString = "Processing STHPO (PO) Started : " & TimeOfDay.ToString("hh:mm:ss")
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        For Each supplier As BOPurchases.cSupplierMaster In ColSm
            strWorkString = "Checking :" & Space(1) & supplier.Number.Value.PadLeft(5, "0"c) & Space(1) & supplier.Name.Value
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.SupplierNumber, supplier.Number.Value)
            SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotType, "S")
            SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotNumber, "0")
            SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.Deleted, False)
            SupplierDetail.SortBy(SupplierDetail.SupplierNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            ColSd = SupplierDetail.LoadMatches
            If ColSd.Count < 1 Then
                SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.SupplierNumber, supplier.Number.Value)
                SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotType, "O")
                SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotNumber, supplier.OrderDepotNumber.Value)
                SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.Deleted, False)
                SupplierDetail.SortBy(SupplierDetail.SupplierNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColSd = SupplierDetail.LoadMatches
                If ColSd.Count < 1 Then
                    SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.SupplierNumber, supplier.Number.Value)
                    SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotType, "S")
                    SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.DepotNumber, "999")
                    SupplierDetail.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    SupplierDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupplierDetail.Deleted, False)
                    SupplierDetail.SortBy(SupplierDetail.SupplierNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    ColSd = SupplierDetail.LoadMatches
                End If
            End If
            If ColSd.Count > 0 Then
                intX1 = 9 'Current run day for this supplier
                intWorkDayNumber = 7
                intCheckSOQDayOfWeek = 64
                intSupplierSOQFrequency = ColSd(0).SOQFrequency.Value
                intSuggestedOrderQuantityFrequency = intSupplierSOQFrequency
                boolCheckSuggestedOrderQuantity = True
                boolDoToday = False
                While boolCheckSuggestedOrderQuantity = True
                    'Check if Day is valid and set X1 to current Day
                    If intSupplierSOQFrequency >= intCheckSOQDayOfWeek Then
                        intX1 = intWorkDayNumber
                        intSupplierSOQFrequency -= intCheckSOQDayOfWeek
                    End If
                    If ((intWorkDayNumber = intDoDayOfWeek1) And (ColSd(0).BBC.Value.Trim = String.Empty)) Or _
                        ((intWorkDayNumber = intDoDayOfWeek2) And (ColSd(0).BBC.Value.Trim <> String.Empty)) Then 'And intX1 <> 9 Then
                        boolCheckSuggestedOrderQuantity = False
                    Else
                        intCheckSOQDayOfWeek = CInt(intCheckSOQDayOfWeek / 2)
                        intWorkDayNumber = intWorkDayNumber - 1
                        If intWorkDayNumber < 1 Then
                            boolCheckSuggestedOrderQuantity = False
                        End If
                    End If
                End While
                '                If ColSd(0).BBC.Value = String.Empty Then
                If (intDoDayOfWeek1 = intX1) Or (intDoDayOfWeek2 = intX1) Then
                    boolDoToday = True
                End If
                '            Else
                '               boolDoToday = boolDoToday
                '          End If
                'UPDATE WINORD
                boolNeedNullPurchaseOrder = True
                PurchaseOrderHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PurchaseOrderHeader.SupplierNumber, supplier.Number.Value)
                PurchaseOrderHeader.SortBy(PurchaseOrderHeader.PONumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColPh = PurchaseOrderHeader.LoadMatches
                For Each purhdr As BOPurchases.cPurchaseHeader In ColPh ' Process Purchase Orders For This supplier
                    If purhdr.CommBBCPrepped.Value = True And purhdr.DateOrderCreated.Value = SysDates.Today.Value Then
                        boolNeedNullPurchaseOrder = False
                    End If
                    If purhdr.CommBBCPrepped.Value = False And purhdr.ReceivedComplete.Value = False Then
                        strSthpoRecordTypeOPPurchaseOrderNumber = purhdr.PONumber.Value.ToString.PadLeft(6, "0"c)
                        strSthpoRecordTypeOPSupplierNumber = purhdr.SupplierNumber.Value.PadLeft(5, " "c)
                        strSthpoRecordTypeOPSOQControlNumber = purhdr.SOQNumber.Value.ToString.PadLeft(3, " "c)
                        strSthpoRecordTypeOPDatePurchaseOrderWasRaised = purhdr.DateOrderCreated.Value.ToString("dd/MM/yy")
                        strSthpoRecordTypeOPExpectedDeliveryDate = purhdr.DateOrderDue.Value.ToString("dd/MM/yy")
                        FormatIntToString(intSuggestedOrderQuantityFrequency, strSthpoRecordTypeOPSOQDayCode, 3, " "c)
                        strSthpoRecordTypeOPSOQDayCode = strSthpoRecordTypeOPSOQDayCode.Substring(0, 3) & " "
                        strSthpoRecordTypeOPBreakBulkCentreFlag = " "
                        If purhdr.SupplierBBC.Value <> String.Empty Then
                            strSthpoRecordTypeOPBreakBulkCentreFlag = purhdr.SupplierBBC.Value
                        End If
                        strSthpoRecordTypeOPNullOrderForThisSupplier = "N"
                        strSthpoRecordTypeOPIsATradanetSupplier = "N"
                        If purhdr.SupplierTradanet.Value = True Then
                            strSthpoRecordTypeOPIsATradanetSupplier = "Y"
                        End If
                        FormatIntToString(CInt(purhdr.HONumber.Value), strSthpoRecordTypeOPHeadOfficeOrderNumber, 6, "0"c)
                        strSthpoRecordTypeOPHeadOfficeOrderNumber = strSthpoRecordTypeOPHeadOfficeOrderNumber.Substring(0, 6)
                        strSthpoRecordTypeOPWhoRaisedThisOrder = purhdr.RaiserInitials.Value.PadLeft(5, " "c)
                        decHashValue = CDec(purhdr.PONumber.Value)
                        SetupHash("OP", decHashValue, dateHashDate)
                        If strSthpoText <> String.Empty Then
                            If strSthpoText.EndsWith(vbCrLf) = False Then strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthpoRecordTypeOPPurchaseOrderNumber & strSthpoRecordTypeOPSupplierNumber & strSthpoRecordTypeOPSOQControlNumber & strSthpoRecordTypeOPDatePurchaseOrderWasRaised & strSthpoRecordTypeOPExpectedDeliveryDate & strSthpoRecordTypeOPSOQDayCode & strSthpoRecordTypeOPBreakBulkCentreFlag & strSthpoRecordTypeOPNullOrderForThisSupplier & strSthpoRecordTypeOPIsATradanetSupplier & strSthpoRecordTypeOPHeadOfficeOrderNumber & strSthpoRecordTypeOPWhoRaisedThisOrder
                        intRecordsOutput = intRecordsOutput + 1
                        If strSthpoText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthpoFileName, strSthpoText)
                            strSthpoText = String.Empty
                        End If
                        PurchaseOrderDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PurchaseOrderDetail.HeaderIdentity, purhdr.PurchaseHeaderID.Value)
                        ColPl = PurchaseOrderDetail.LoadMatches

                        intLineNumber = 0
                        'For Each LINE As BOPurchases.cPurchaseLine In ColPl
                        For Each line As BOPurchases.cPurchaseLine In purhdr.Lines
                            intLineNumber = intLineNumber + 1
                            strSthpoRecordTypeODSkuNumber = line.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                            'formatIntToString(LINE.SKUNumber.Value, strSthpoRecordTypeODSkuNumber, 6, "0")
                            strSthpoRecordTypeODPurchaseOrderNumber = purhdr.PONumber.Value.ToString.PadLeft(6, "0"c)
                            'FormatIntToString(purhdr.PONumber.Value, strSthpoRecordTypeODPurchaseOrderNumber, 6, "0")
                            strSthpoRecordTypeODintLineNumber = intLineNumber.ToString.PadLeft(4, "0"c)
                            'FormatIntToString(intLineNumber, strSthpoRecordTypeODintLineNumber, 6, "0")
                            FormatIntToString(line.OrderQty.Value, strSthpoRecordTypeODOrderQuantity, 6, " "c)
                            SetupHash("OD", decHashValue, dateHashDate)
                            If strSthpoText <> String.Empty Then
                                If strSthpoText.EndsWith(vbCrLf) = False Then strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthpoRecordTypeODSkuNumber & strSthpoRecordTypeODPurchaseOrderNumber & strSthpoRecordTypeODintLineNumber & strSthpoRecordTypeODOrderQuantity
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthpoText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthpoFileName, strSthpoText)
                                strSthpoText = String.Empty
                            End If
                        Next
                        ' Flag as prepared
                        Try
                            purhdr.CommBBCPrepped.Value = True
                            purhdr.SaveIfExists()
                        Catch ex As Exception
                        End Try
                    End If

                Next ' Process Purchase Orders For This supplier
            End If
            If boolNeedNullPurchaseOrder = True And boolDoToday = True Then
                strSthpoRecordTypeOPPurchaseOrderNumber = "000000"
                strSthpoRecordTypeOPSupplierNumber = supplier.Number.Value.PadLeft(5, " "c)
                strSthpoRecordTypeOPSOQControlNumber = "000000"
                strSthpoRecordTypeOPDatePurchaseOrderWasRaised = dateHashDate.ToString("dd/MM/yy")
                strSthpoRecordTypeOPExpectedDeliveryDate = "--/--/--"
                FormatIntToString(intSuggestedOrderQuantityFrequency, strSthpoRecordTypeOPSOQDayCode, 3, " "c)
                strSthpoRecordTypeOPSOQDayCode = strSthpoRecordTypeOPSOQDayCode.Substring(0, 3) & " "
                strSthpoRecordTypeOPBreakBulkCentreFlag = " "
                If ColSd(0).BBC.Value <> String.Empty Then
                    strSthpoRecordTypeOPBreakBulkCentreFlag = ColSd(0).BBC.Value
                End If
                strSthpoRecordTypeOPNullOrderForThisSupplier = "Y"
                strSthpoRecordTypeOPIsATradanetSupplier = "N"
                If ColSd(0).Tradanet.Value = True Then
                    strSthpoRecordTypeOPIsATradanetSupplier = "Y"
                End If
                strSthpoRecordTypeOPHeadOfficeOrderNumber = "000000"
                strSthpoRecordTypeOPWhoRaisedThisOrder = "     "
                decHashValue = 0
                SetupHash("OP", decHashValue, dateHashDate)
                If strSthpoText <> String.Empty Then
                    strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthpoRecordTypeOPPurchaseOrderNumber & strSthpoRecordTypeOPSupplierNumber & strSthpoRecordTypeOPSOQControlNumber & strSthpoRecordTypeOPDatePurchaseOrderWasRaised & strSthpoRecordTypeOPExpectedDeliveryDate & strSthpoRecordTypeOPSOQDayCode & strSthpoRecordTypeOPBreakBulkCentreFlag & strSthpoRecordTypeOPNullOrderForThisSupplier & strSthpoRecordTypeOPIsATradanetSupplier & strSthpoRecordTypeOPHeadOfficeOrderNumber & strSthpoRecordTypeOPWhoRaisedThisOrder
                intRecordsOutput = intRecordsOutput + 1
                If strSthpoText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strSthpoFileName, strSthpoText)
                    strSthpoText = String.Empty
                End If

            End If
        Next
        If _RunningInNight = True Then
            decHashValue = 0
            SetupHash("DE", decHashValue, dateHashDate)
            If strSthpoText <> String.Empty Then
                strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "DAY END" '& vbCrLf
            intRecordsOutput = intRecordsOutput + 1
            If strSthpoText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthpoFileName, strSthpoText)
                strSthpoText = String.Empty
            End If
        End If
        If strSthpoText.Length > 0 Then
            PutSthoToDisc(strSthpoFileName, strSthpoText)
            strSthpoText = String.Empty
        End If
        strWorkString = "Processing STHPO (PO) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthpoFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
    End Sub ' PurchaseOrders

    Public Sub OutputMarkups()

        'get system dates
        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)
        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()

        Dim decWorkDecimal As Decimal = 0
        strSthoaText = String.Empty

        Dim Prcchg As New BOStock.cPriceChange(_Oasys3DB)
        Prcchg.ClearLists()
        Prcchg.ClearLoadField()
        Prcchg.ClearLoadFilter()
        Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Prcchg.ChangeStatus, "U")
        Prcchg.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Prcchg.CommedToHo, False)
        Prcchg.SortBy(Prcchg.PartCode.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        Prcchg.SortBy(Prcchg.EffectiveDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        Dim ColPc As List(Of BOStock.cPriceChange) = Prcchg.LoadMatches

        'Reinstate When Price Changes Sorted Out
        intRecordsOutput = 0
        strWorkString = "Processing STHOA - Price Changes (MU) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        For Each record As BOStock.cPriceChange In ColPc
            strWorkString = "Checking :" & Space(1) & record.EffectiveDate.Value.Date.ToString & record.PartCode.Value.PadLeft(6, "0"c)
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If record.MarkUpChange.Value <> 0 Then
                decHashValue = record.MarkUpChange.Value
                dateHashDate = record.EffectiveDate.Value.Date
                _STHOARecTypeA7SkuNumber = record.PartCode.Value.ToString.PadLeft(6, "0"c)
                _STHOARecTypeA7MarkupOrDownCode = "1"
                _STHOARecTypeA7MarkupOrDownUnits = "      0+"
                FormatDecToString(decHashValue, _STHOARecTypeA7MarkupOrDownValue, 11, " "c, "0.00")
                FormatDecToString(decWorkDecimal, _STHOARecTypeA7OldSellingPrice, 11, " "c, "0.00")
                FormatDecToString(record.NewPrice.Value, _STHOARecTypeA7NewSellingPrice, 11, " "c, "0.00")
                _STHOARecTypeA7PriceOverrideReasonCode = "  "
                SetupHash("A7", decHashValue, dateHashDate)
                If strSthoaText <> String.Empty Then
                    If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA7SkuNumber & _STHOARecTypeA7MarkupOrDownCode & _STHOARecTypeA7MarkupOrDownUnits & _STHOARecTypeA7MarkupOrDownValue & _STHOARecTypeA7OldSellingPrice & _STHOARecTypeA7NewSellingPrice & _STHOARecTypeA7BulkSkuNumber & _STHOARecTypeA7PriceOverrideReasonCode '& vbCrLf
                intRecordsOutput = intRecordsOutput + 1
                If strSthoaText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strSthoaFileName, strSthoaText)
                    strSthoaText = String.Empty
                End If
                ' Flag as commed to H/O
                'record.CommedToHo.Value = True
                'record.SaveIfExists()
            End If
        Next

        If strSthoaText.Length > 0 Then
            PutSthoToDisc(strSthoaFileName, strSthoaText)
            strSthoaText = String.Empty
        End If
        strWorkString = "Processing STHOA - Price Changes (MU) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        OutputSthoLog(strWorkString)
        ' Output Price Override markups/down from DLLINE

        Dim boolSaveIndicator1 As Boolean = boolDoPrepareSthpaRefunds
        Dim boolSaveIndicator2 As Boolean = boolDoJDAMarkups
        Dim boolSaveIndicator3 As Boolean = boolDoPrepareCoupons
        Dim boolSaveIndicator4 As Boolean = boolDoPrepareSthof
        Dim boolSaveIndicator5 As Boolean = boolDoPreparePostCodes

        boolDoPrepareSthpaRefunds = False
        boolDoJDAMarkups = False
        boolDoPrepareCoupons = False
        boolDoPrepareSthof = False
        boolDoPreparePostCodes = False
        ProcessDlRecords()

        boolDoPrepareSthpaRefunds = boolSaveIndicator1
        boolDoJDAMarkups = boolSaveIndicator2
        boolDoPrepareCoupons = boolSaveIndicator3
        boolDoPrepareSthof = boolSaveIndicator4
        boolDoPreparePostCodes = boolSaveIndicator5

    End Sub ' OutputMarkups

    Public Sub CalculateOpenPuchaseOrderValue()
        ' Calculate Value of Open Purchase Orders
        Dim PurchaseOrderHeader As New BOPurchases.cPurchaseHeader(_Oasys3DB)
        Dim ColPh As List(Of BOPurchases.cPurchaseHeader)
        PurchaseOrderHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PurchaseOrderHeader.ReceivedComplete, False)
        PurchaseOrderHeader.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        PurchaseOrderHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PurchaseOrderHeader.Deleted, False)
        ColPh = PurchaseOrderHeader.LoadMatches()
        decValueOfOpenPurchaseOrders = 0
        For Each purhdr As BOPurchases.cPurchaseHeader In ColPh
            decValueOfOpenPurchaseOrders = decValueOfOpenPurchaseOrders + purhdr.OrderValue.Value
        Next
    End Sub ' Calculate Value of Open Purchase Orders

    'Public Function CalculateCurrentStockValue() As Decimal
    '    Dim StockMaster As New BOStock.cStock(_Oasys3DB)
    '    Dim intHighSku As Integer = 999999
    '    Dim intX As Integer = 0
    '    Dim intY As Integer = 0

    '    While intY < intHighSku
    '        intY = intX + 100000
    '        If intY > intHighSku Then
    '            intY = intHighSku
    '        End If
    '        strWorkString = "Calculating Current Stock Value"
    '        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
    '        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
    '        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
    '        strWorkString = "Selecting Sku Range : " & intX.ToString.PadLeft(6, "0") & " - " & intY.ToString.PadLeft(6, "0")
    '        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
    '        ProcessTransmissionsProgress.Show()
    '        StockMaster.ClearLoadFilter()
    '        StockMaster.ClearLists()

    '        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SkuNumber, intX.ToString.PadLeft(6, "0"))
    '        StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intY.ToString.PadLeft(6, "0"))
    '        StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.StockOnHand, 0)
    '        StockMaster.Stocks = StockMaster.LoadMatches()

    '        For Each stocks As BOStock.cStock In StockMaster.Stocks
    '            decEndOfWeekStockValue = decEndOfWeekStockValue + (stocks.StockOnHand.Value * stocks.NormalSellPrice.Value)
    '        Next
    '        intX = intY
    '    End While

    'End Function
    Public Sub ProcessDlRecords()
        ' This subroutine is used to process the "DL" records       ' 

        Dim Dltots As New BOSales.cSalesHeader(_Oasys3DB)
        Dim ColDt As New List(Of BOSales.cSalesHeader)

        Dim Dlline As New BOSales.cSalesLine(_Oasys3DB)
        Dim ColDl As New List(Of BOSales.cSalesLine)

        Dim Dlpaid As New BOSales.cSalesPaid(_Oasys3DB)
        Dim ColDp As New List(Of BOSales.cSalesPaid)

        Dim StockAdj As New BOStock.cStockAdjust(_Oasys3DB)

        Dim intWorkInteger As Integer = 0

        ' Dim strSthoaText As String = String.Empty
        Dim strpostCodeText As String = String.Empty
        Dim strSthotText As String = String.Empty
        Dim strSthoyText As String = String.Empty
        Dim strSthpaText As String = String.Empty
        Dim decWorkExtendedPrice As Decimal = 0
        Dim intWorkQuantity As Integer = 0
        Dim decWorkValue As Decimal = 0

        Dim dateSelectionDate As Date = Date.MinValue.Date

        Dim arrSkus As String() = New String(9999) {}
        Dim arrIntFlashTotalCounts As Integer() = New Integer(13) {}
        Dim arrDecFlashTotalAmounts As Decimal() = New Decimal(13) {}
        Dim arrIntFlashTotalTenderTypeCount As Integer() = New Integer(20) {}
        Dim arrDecFlashTotalTenderTypeAmount As Decimal() = New Decimal(20) {}
        Dim decFlashTotalZReads As Decimal = 0
        Dim intFlashTotalOccurrence As Integer = 0
        Dim decGiftTokenTotalValue As Decimal = 0
        Dim intGiftTokensUsed As Integer = 0
        Dim decGiftTokenTotalVatValue As Decimal = 0
        Dim intGiftTokenSkuNumber As Integer = 0
        ' Daily values for STHOF
        Dim intDailyHoursPaid As Integer = 0
        Dim intDailyConservatoriesSold As Integer = 0
        Dim intDailyFrontEndSalesCount As Integer = 0
        Dim decDailyFrontEndSalesValue As Decimal = 0
        Dim intDailyFrontEndRefundCount As Integer = 0
        Dim decDailyFrontEndRefundValue As Decimal = 0
        Dim decDailyFrontEndSalesAndRefundVoucherValue As Decimal = 0
        Dim intDailyFrontEndSalesAndRefundVoucherCount As Integer = 0
        Dim decDailyFrontEndSalesAndRefundDeliveryValue As Decimal = 0
        Dim decDailyFrontEndSalesAndRefundInstallationValue As Decimal = 0
        Dim decDailyMarkdownsValue As Decimal = 0
        Dim decDailyReceiverListingValue As Decimal = 0

        Dim decWeeklyMarkdownsValue As Decimal = 0
        Dim decWeeklyDailyReceiverListingValue As Decimal = 0
        Dim intNoOfSKUsOutOfStock As Integer = 0

        Dim decValueOfTodaysDailyReceiverListings As Decimal = 0
        Dim decValueOfThisWeeksDailyReceiverListings As Decimal = 0

        Dim decValueOfDailyMarkdowns As Decimal = 0

        Dim decValueOfEndOfWeekStock As Decimal = 0
        Dim decValueOfCode02Adjustments As Decimal = 0
        Dim decTotalValueOfDeliveriesSold As Decimal = 0
        Dim decTotalValueOfDeliveryCharges As Decimal = 0
        Dim intTotalNumberOfDeliveries As Integer = 0
        Dim decDeliveryPercentToSales As Decimal = 0
        Dim decWeeklyWagesPaid As Decimal = 0
        Dim intWeeklyHoursPaid As Integer = 0
        Dim intWeeklyNumberOfStarters As Integer = 0
        Dim intWeeklyNumberOfLeavers As Integer = 0
        Dim intWeeklyNumberOfFullTimers As Integer = 0
        Dim intWeeklyNumberOfPartTimers As Integer = 0

        Dim intNumberOfUnappliedPricesChanges As Integer = 0
        Dim boolThisIsAWeekEnd As Boolean = False
        Dim intSthofRecords As Integer = 0
        Dim intI As Integer = 0
        Dim intJ As Integer = 0

        Dim strArTransactionNumbersactionCode As String = String.Empty ' Transaction Type                             - DATA+22
        Dim strArTransactionNumbersactionTime As String = String.Empty ' transaction time as HH:MM:SS                 - DATA+24  
        Dim strArCashierNumber As String = String.Empty ' Cashier Number                               - DATA+32 
        Dim strArTillId As String = String.Empty ' Till number                                  - DATA+35
        Dim strArTransactionNumber As String = String.Empty ' Transaction number                           - DATA+37
        Dim strArTransactionintLineNumber As String = String.Empty ' Line Number                                  - DATA+41
        Dim strArSkuNumber As String = String.Empty ' Sku Number                                   - DATA+47  
        Dim strArQuantitySold As String = String.Empty ' 6.0 signed numeric - Item Quantity           - DATA+53 
        Dim strArItemValue As String = String.Empty ' 10.2 signed numeric - Item Value (inc Vat)   - DATA+60
        Dim strArReasonCode As String = String.Empty ' Reason Code (A4)                             - DATA+72 
        Dim strDltotsTillId As String = String.Empty  '*DATA+22   A2   DT:TILL    * PC Till Id                                    "nn"
        Dim strDltotsTransactionNumber As String = String.Empty  '*DATA+24   A4   DT:TRAN    * Transaction Number                          "nnnn"
        Dim strDltotsCashierNumber As String = String.Empty '*DATA+28   A3   DT:CASH    * Cashier Number                               "nnn"
        Dim strDltotsTransactionTime As String = String.Empty  '*DATA+31   A6   DT:TIME    * Time of Transaction                       "hhmmss"
        Dim strDltotsSupervisorNumber As String = String.Empty  '*DATA+37   A3   DT:SUPV    * Supervisor Number                            "nnn"
        Dim strDltotsTransactionCode As String = String.Empty '*DATA+40   A2   DT:TCOD    * Transaction Code
        Dim strDltotsOpenDrawerReasonCode As String = String.Empty '*DATA+42   A2   DT:OPEN    * Open Drawer Reason Code - if :TCOD = "OD"     "nn"
        Dim strDltotsReasonDescription As String = String.Empty '*DATA+44   A20  DT:DESC    * Reason Description          "aaaaaaaaaaaaaaaaaaaa"
        Dim strDltotsProjectSalesOrderNumber As String = String.Empty  '*DATA+64   A6   DT:ORDN    * PSS Order Number                          "nnnnnn"
        Dim strDltotsAccountSaleIndicator As String = String.Empty  '*DATA+70   A1   DT:ACCT    * Y = This is an Account Sale             "N" or "Y"
        Dim strDltotsTransactionWasVoided As String = String.Empty  '*DATA+71   A1   DT:VOID    * Y = This Transaction was Voided         "N" or "Y"
        Dim strDltotsVoidSupervisorNumber As String = String.Empty '*DATA+72   A3   DT:VSUP    * Void Supervisor Cashier Number               "nnn"
        Dim strDltotsTrainingTransaction As String = String.Empty  '*DATA+75   A1   DT:TMOD    * Y = Training Mode Transaction           "N" or "Y"
        Dim strDltotsTransactionWasProcessedByRSBUPD As String = String.Empty  '*DATA+76   A1   DT:PROC    * Y = Statisics Updated by RSBUPD         "N" or "Y"
        Dim strDltotsExternalDocumentNumber As String = String.Empty '*DATA+77   A8   DT:DOCN    * External Document number - if required  "nnnnnnnn"
        Dim strDltotsSupervisorWasUsed As String = String.Empty  '*DATA+85   A1   DT:SUSE    * ON = Supervisor used SOMEWHERE in 
        Dim strDltotsStoreNumber As String = String.Empty '*DATA+86   A3   DT:STOR    * Store/Dept number if Employee Discount       "nnn"
        Dim strDltotsMerchandisingValue As String = String.Empty '*DATA+89   A10  DT:MERC    * Merchandise Amount including VAT      "nnnnnn.nn-"
        Dim strDltotsNonMerchandisingValue As String = String.Empty  '*DATA+99   A10  DT:NMER    * Non-Merchandise Amount                "nnnnnn.nn-"
        Dim strDltotsTaxAmount As String = String.Empty '*DATA+109  A10  DT:TAXA    * Tax Amount                            "nnnnnn.nn-"
        Dim strDltotsDiscountAmount As String = String.Empty  '*DATA+119  A10  DT:DISC    * Discount Amount for this Transaction  "nnnnnn.nn-"
        Dim strDltotsDiscountSupervisorNumber As String '*DATA+129  A3   DT:DSUP    * Discount Supervisor                          "nnn"
        Dim strDltotsTotalValue As String = String.Empty '*DATA+132  A10  DT:TOTL    * Total Sale Amount                     "nnnnnn.nn-"
        Dim strDltotsCustomerAccountNumber As String = String.Empty  '*DATA+142  A6   DT:ACCN    * Customer Account Number                    "NNNNNN"
        Dim strDltotsCustomerAccountCardHolderNumber As String = String.Empty '*DATA+148  A2   DT:CARD    * Customer Account Card Holder Number		   "NN"
        Dim strDltotsAccountUpdateIsComplete As String = String.Empty  '*DATA+150  A1   DT:AUPD    * ON = Postcode collection active         "N" or "Y"
        Dim strDltotsTransactionIsComplete As String = String.Empty '*DATA+151  A1   DT:ICOM    * Y = Transaction Complete                "N" or "Y"
        Dim strDltotsEmployeeDiscountOnly As String = String.Empty  '*DATA+152  A1   DT:IEMP    * Y = Transaction was Discounted using    "N" or "Y"
        Dim strDltotsRefundCashierNumber As String = String.Empty  '*DATA+153  A3   DT:RCAS    * Refund Cashier Number                        "nnn"
        Dim strDltotsRefundSupervisorrNumber As String = String.Empty  '*DATA+156  A3   DT:RSUP    * Refund Authorisation Supervisor Number       "nnn"
        Dim strDltotsVatRate1 As String = String.Empty   '*DATA+159  A7   DT:VATR(O=1)	* VAT Rate 1
        Dim strDltotsVatSymbol1 As String = String.Empty   '*DATA+166  A1   DT:VSYM(O=1)	* VAT Symbol 1
        Dim strDltotsVatExclusiveValue1 As String = String.Empty   '*DATA+167  A10  DT:XVAT(O=1)	* Goods Ex-VAT 1
        Dim strDltotsVatValue1 As String = String.Empty  '*DATA+177  A10  DT:VATV(O=1)     * Vat Value of Goods (Rate 1)
        Dim strDltotsVatRate2 As String = String.Empty   '*DATA+187  A7   DT:VATR(O=2)	* VAT Rate 2
        Dim strDltotsVatSymbol2 As String = String.Empty   '*DATA+194  A1   DT:VSYM(O=2)	* VAT Symbol 2
        Dim strDltotsVatExclusiveValue2 As String = String.Empty   '*DATA+195  A10  DT:XVAT(O=2)	* Goods Ex-VAT 2
        Dim strDltotsVatValue2 As String = String.Empty    '*DAT1+005  A10  DT:VATV(O=2)	* Vat Value of Goods (Rate 2)
        Dim strDltotsVatRate3 As String = String.Empty   '*DAT1+015  A7   DT:VATR(O=3)	* VAT Rate 3
        Dim strDltotsVatSymbol3 As String = String.Empty    '*DAT1+022  A1   DT:VSYM(O=3)	* VAT Symbol 3
        Dim strDltotsVatExclusiveValue3 As String = String.Empty    '*DAT1+023  A10  DT:XVAT(O=3)	* Goods Ex-VAT 3
        Dim strDltotsVatValue3 As String = String.Empty    '*DAT1+033  A10  DT:VATV(O=3)	* Vat Value of Goods (Rate 3)
        Dim strDltotsVatRate4 As String = String.Empty  '*DAT1+043  A7   DT:VATR(O=4)	* VAT Rate 4
        Dim strDltotsVatSymbol4 As String = String.Empty    '*DAT1+050  A1   DT:VSYM(O=4)	* VAT Symbol 4
        Dim strDltotsVatExclusiveValue4 As String = String.Empty   '*DAT1+051  A10  DT:XVAT(O=4)	* Goods Ex-VAT 4
        Dim strDltotsVatValue4 As String = String.Empty  '*DAT1+061  A10  DT:VATV(O=3)	* Vat Value of Goods (Rate 4)
        Dim strDltotsVatRate5 As String = String.Empty   '*DAT1+071  A7   DT:VATR(O=5)	* VAT Rate 5
        Dim strDltotsVatSymbol5 As String = String.Empty    '*DAT1+078  A1   DT:VSYM(O=5)	* VAT Symbol 5
        Dim strDltotsVatExclusiveValue5 As String = String.Empty    '*DAT1+079  A10  DT:XVAT(O=5)	* Goods Ex-VAT 
        Dim strDltotsVatValue5 As String = String.Empty  '*DAT1+089  A10  DT:VATV(O=5)     * Vat Value of Goods (Rate 5)
        Dim strDltotsVatRate6 As String = String.Empty  '*DAT1+099  A7   DT:VATR(O=6)	* VAT Rate 6
        Dim strDltotsVatSymbol6 As String = String.Empty    '*DAT1+106  A1   DT:VSYM(O=6)	* VAT Symbol 6
        Dim strDltotsVatExclusiveValue6 As String = String.Empty   '*DAT1+107  A10  DT:XVAT(O=6)	* Goods Ex-VAT 6
        Dim strDltotsVatValue6 As String = String.Empty  '*DAT1+117  A10  DT:VATV(O=6)     * Vat Value of Goods (Rate 6)
        Dim strDltotsVatRate7 As String = String.Empty   '*DAT1+127  A7   DT:VATR(O=7)	* VAT Rate 7
        Dim strDltotsVatSymbol7 As String = String.Empty    '*DAT1+134  A1   DT:VSYM(O=7)	* VAT Symbol 7
        Dim strDltotsVatExclusiveValue7 As String = String.Empty   '*DAT1+135  A10  DT:XVAT(O=7)	* Goods Ex-VAT 7
        Dim strDltotsVatValue7 As String = String.Empty  '*DAT1+145  A10  DT:VATV(O=7)     * Vat Value of Goods (Rate 7)
        Dim strDltotsVatRate8 As String = String.Empty   '*DAT1+155  A7   DT:VATR(O=8)	* VAT Rate 8
        Dim strDltotsVatSymbol8 As String = String.Empty    '*DAT1+162  A1   DT:VSYM(O=8)	* VAT Symbol 8
        Dim strDltotsVatExclusiveValue8 As String = String.Empty   '*DAT1+163  A10  DT:XVAT(O=8)	* Goods Ex-VAT 8
        Dim strDltotsVatValue8 As String = String.Empty '*DAT1+173  A10  DT:VATV(O=8)     * Vat Value of Goods (Rate 8)
        Dim strDltotsVatRate9 As String = String.Empty  '*DAT1+183  A7   DT:VATR(O=9)	* VAT Rate 9
        Dim strDltotsVatSymbol9 As String = String.Empty  '*DAT1+190  A1   DT:VSYM(O=9)	* VAT Symbol 9
        Dim strDltotsVatExclusiveValue9 As String = String.Empty   '*DAT1+191  A10  DT:XVAT(O=9)	* Goods Ex-VAT 9
        Dim strDltotsVatValue9 As String = String.Empty '*DAT2+001  A10  DT:VATV(O=9)     * Vat Value of Goods (Rate 9)
        Dim strDltotsTransactionIsParked As String = String.Empty  '*DAT2+011  A01  DT:PARK    * Y = This Transaction was Voided         "N" or "Y"
        Dim strDltotsColleagueCardNumber As String = String.Empty '*DAT2+012  A09  DT:CCRD    * Colleague Number                        NNNNNNNNN  
        Dim strDltotsRefundManagerNumber As String = String.Empty  '*DAT2+021  A03  DT:RMAN    * Refund Authorisation Manager Number
        Dim strDltotsTenderOverrideCode As String = String.Empty '*DAT2+024  A02  DT:TOCD    * Tender Override Code used by manager
        Dim strDltotsRecoveredFromParkedTransaction As String = String.Empty  '*DAT2+026  A01  DT:PKRC    * Indicate Parked Transaction has been recalled
        Dim strDltotsTransactionWasOffline As String = String.Empty '*DAT2+027  A01  DT:REMO    * Indicate Transaction was on/off line  
        Dim strDltotsNumberOfGiftTokensPrinted As String = String.Empty  '*DAT2+028  A03  DT:GTPN    * Number Of Gift Tokens Printed          NN-
        Dim strDltotsCurrentSaveStatus As String = String.Empty '*DAT2+031  A02  DT:SSTA    * Current save status of DLTOTS & sub records
        Dim strDltotsCurrentSaveStatusSequenceNumber As String = String.Empty '*DAT2+033  A04  DT:SSEQ    * Used with SSTA to hold the sequence
        Dim strDltotsDateUpdatedByCashBalanceUpdateProgram As String = String.Empty  '*DAT2+037  A08  DT:CBBU	  * Indicates updated by CBBUPD - uses RO:DOLR
        Dim strDltotsColleagueAccountCardHolderNumber As String = String.Empty  '*DAT2+045  A19  DT:CARDNO  * Discount Card Number
        Dim DTRtiComp As String = String.Empty '*DAT2+064  A01  DT:RTICOMP * RTI Completed Flag

        '******************************************************************************

        ':DATA for type "DD" - Transaction Line (Version W12)


        Dim strDllineTillId As String = String.Empty '*DATA+22   A2   DL:TILL     * PC Till Id - Network ID Number                "nn"
        Dim strDllineTransactionNumber As String = String.Empty  '*DATA+24   A4   DL:TRAN     * Transaction Number from PC Till             "nnnn"
        Dim strDllineTransactionintLineNumber As String = String.Empty '*DATA+28   A4   DL:NUMB     * Assigned Sequence Number (ie, Line Number)  "nnnn"
        Dim strDllineSkuNumber As String = String.Empty  '*DATA+32   A6   DL:SKUN     * Sku Number of this Item                   "nnnnnn"
        Dim strDllineDepartmentNumber As String = String.Empty '*DATA+38   A2   DL:DEPT     * Department Number of this item                "nn"
        Dim strDllineInputWasByBarcode As String = String.Empty '*DATA+40   A1   DL:IBAR *W03* Y = Input by scanner                    "N" or "Y"
        Dim strDllineSupervisorNumber As String = String.Empty '*DATA+41   A3   DL:SUPV     * Supervisor Cashier Number                    "nnn"
        Dim strDllineQuantitySold As String = String.Empty '*DATA+44   A7   DL:QUAN     * Quantity Sold                            "nnnnnn-"
        Dim strDllineSystemLookupPrice As String = String.Empty '*DATA+51   A10  DL:SPRI     * System Lookup Price                   "nnnnnn.nn-"
        Dim strDllineItemSellingPrice As String = String.Empty  '*DATA+61   A10  DL:PRIC     * Item Price                            "nnnnnn.nn-"
        Dim strDllineItemSellingPriceExclusiveOfVat As String = String.Empty  '*DATA+71   A10  DL:PRVE     * Item Price - VAT Exclusive            "nnnnnn.nn-"
        Dim strDllineExtendedValue As String = String.Empty '*DATA+81   A10  DL:EXTP     * Extended Value - QUAN times PRIC      "nnnnnn.nn-"
        Dim strDllineExtendedCost As String = String.Empty '*DATA+91   A11  DL:EXTC     * Extended Cost                        "nnnnnn.nnn-"
        Dim strDllineItemIsRelatedSingle As String = String.Empty  '*DATA+102  A1   DL:RITM     * Y = Related items single                "N" or "Y"
        Dim strDllinePriceOverrideReasonCode As String = String.Empty '*DATA+103  A2   DL:PORC     * Price Override Reason Code number             "nn"
        Dim strDllinePriceOverrideReasonText As String = String.Empty     '*DATA+105  A20  RO:PORC     * Price Override Reason       "aaaaaaaaaaaaaaaaaaaa"
        Dim strDllineTaggedItem As String = String.Empty '*DATA+125  A1   DL:ITAG     * Y = Was a tagged Item                   "N" or "Y"
        Dim strDllineCatchAllItem As String = String.Empty '*DATA+126  A1   DL:CATA     * Y = Was a Catch-All Item                "N" or "Y"
        Dim strDllineVatSymbol As String = String.Empty '*DATA+127  A1   DL:VSYM     * Vat Symbol used for this item
        Dim strDllineTemporaryPriceChangePriceDifference As String = String.Empty  '*DATA+128  A10  DL:TPPD     * Temporary Price Change Price          "nnnnnn.nn-"
        Dim strDllineTemporaryPriceChangeMarginErosionCode As String = String.Empty  '*DATA+138  A6   DL:TPME     * Temporary Price Change Margin             "nnnnnn"
        Dim strDllinePriceOverridePriceDifference As String = String.Empty  '*DATA+144  A10  DL:POPD     * Price Override Price Difference       "nnnnnn.nn-"
        Dim strDllinePriceOverrideMarginErosionCode As String = String.Empty '*DATA+154  A6   DL:POME     * Price Override Margin Erosion Code
        Dim strDllineQuantityBreakErosionValue As String = String.Empty  '*DATA+160  A10  DL:QBPD     * Quantity Break Erosion allocated to   "nnnnnn.nn-"
        Dim strDllineQuantityBreakMarginErosionCode As String = String.Empty '*DATA+170  A6   DL:QBME     * Quantity Break Margin Erosion Code        "nnnnnn"
        Dim strDllineDealGroupErosionValue As String = String.Empty '*DATA+176  A10  DL:DGPD     * Deal Group Erosion allocated to       "nnnnnn.nn-"
        Dim strDllineDealGroupMarginErosionCode As String = String.Empty '*DATA+186  A6   DL:DGME     * Deal Group Margin Erosion Code            "nnnnnn"
        Dim strDllineMultiBuyErosionValue As String = String.Empty  '*DATA+192  A10  DL:MBPD     * Multibuy Erosion allocated to this    "nnnnnn.nn-"
        Dim strDllineMultiBuyMarginErosionCode As String = String.Empty '*DAT1+2    A6   DL:MBME     * Multibuy Margin Erosion Code              "nnnnnn"
        Dim strDllineHierarchySpendErosionValue As String = String.Empty '*DAT1+8    A10  DL:HSPD     * Hierarchy Spend Level Erosion         "nnnnnn.nn-"
        Dim strDllineHierarchySpendMarginErosionCode As String = String.Empty  '*DAT1+18   A6   DL:HSME     * Hierarchy Spend Level Erosion Code        "nnnnnn"
        Dim strDllinePrimaryEmployeeDiscountErosionValue As String = String.Empty '*DAT1+24   A10  DL:ESPD     * Employee Sale Price Difference        "nnnnnn.nn-"
        Dim strDllinePrimaryEmployeeDiscountMarginErosionCode As String = String.Empty '*DAT1+34   A6   DL:ESME     * Employee Sale Margin Erosion Code         "000000"
        Dim strDllineLineWasReversed As String = String.Empty '*DAT1+40   A1   DL:LREV     * Y = Line has been reversed OR is        "N" or "Y"
        Dim strDllineLastEventSequenceNumberUsedForThisLine As String = String.Empty '*DAT1+41   A6   DL:ESEQ     * Last Event Sequence number used for       "nnnnnn"
        Dim strDllineHierarchyCategoryNumber As String = String.Empty  '*DAT1+47   A6   DL:CTGY     * Hierarchy Category - IM:CTGY - Level 5    "nnnnnn"
        Dim strDllineHierarchyGroupNumber As String = String.Empty  '*DAT1+53   A6   DL:GRUP     * Hierarchy Group    - IM:GRUP -       4    "nnnnnn"
        Dim strDllineHierarchySubGroupNumber As String = String.Empty '*DAT1+59   A6   DL:SGRP     * Hierarchy SubGroup - IM:SGRP -       3    "nnnnnn"
        Dim strDllineHierarchyStyleNumber As String = String.Empty '*DAT1+65   A6   DL:STYL     * Hierarchy Style    - IM:STYL -       2    "nnnnnn"
        Dim strDllineQuarantineSupervisorNumber As String = String.Empty '*DAT1+71   A3   DL:QSUP     * Partial Quarantine (IM:QUAR=B) Supervisor    "nnn"
        Dim strDllineSecondaryEmployeeDiscountMarginErosionValue As String = String.Empty '*DAT1+74   A10  DL:ESEV     * Secondary Emp. Sale Erosion Value.    "nnnnnn.nn-"
        Dim strDllineSoldFromMarkdownStock As String = String.Empty '*DAT1+84   A1   DL:IMDN     * Y = Sold from Mark-down stock           "N" or "Y"
        Dim strDllineSaleTypeAttribute As String = String.Empty '*DAT1+85   A1   DL:SALT     * Sale Type Attribute
        Dim strDllineVatCodeNumber As String = String.Empty '*DAT1+86  A2	DL:VATN	   * VAT CODE
        Dim strDllineVatValueForThisLine As String = String.Empty  '*DAT1+88  A11	DL:VATV	   * VAT value of this line
        Dim DDBdco As String = String.Empty '*DAT1+99  A01	DL:BDCO	   * Collected at back door
        Dim DDBdcoInd As String = String.Empty  '*DAT1+100 A01   DL:BDCOIND * Y = Item to be collected at back door
        Dim DDRcod As String = String.Empty '*DAT1+101 A02   DL:RCOD    * Line Reversal Reason Code
        '******************************************************************************
        '
        ':DATA for type "DP" - Transaction Payment (Version W06)


        Dim strDlpaidTillId As String = String.Empty  '*DATA+22   A2   DP:TILL     * PC Till Id - Network ID Number               "nn"
        Dim strDlpaidTransactionNumber As String = String.Empty   '*DATA+24   A4   DP:TRAN     * Transaction Number from PC Till            "nnnn"
        Dim strDlpaidTransactionSequenceNumber As String = String.Empty  '*DATA+28   A4   DP:NUMB     * Assigned Sequence Number                   "nnnn"
        Dim strDlpaidTenderTypeNumber As String = String.Empty  '*DATA+32   A2   DP:TYPE     * Tender Type  - Retopt Occurrence Number      "nn"
        Dim strDlpaidTenderTypeDescription As String = String.Empty  '*DATA+34   A12  RO:TEND     * Tender Type Description            "aaaaaaaaaaaa"
        Dim strDlpaidTenderTypeAmount As String = String.Empty  '*DATA+46   A10  DP:AMNT     * Tender Amount                        "nnnnnn.nn-"
        Dim strDlpaidCreditCardNumber As String = String.Empty  '*DATA+56   A19  DP:CARD     * Credit Card Number          "nnnnnnnnnnnnnnnnnnn"
        Dim strDlpaidCreditCardExpiryDate As String = String.Empty  '*DATA+75   A4   DP:EXDT     * Credit Card Expire Date as MMYY            "nnnn"
        Dim strDlpaidCouponNumber As String = String.Empty   '*DATA+79   A6   DP:COPN     * Coupon Number                            "nnnnnn"
        Dim strDlpaidCouponClass As String = String.Empty  '*DATA+85   A2   DP:CLAS     * Class of coupon                              "nn"
        Dim strDlpaidAuthorisationCode As String = String.Empty   '*DATA+87   A9   DP:AUTH     * Auth Code if Credit Card and over     "aaaaaaaaa"
        Dim strDlpaidCreditCardWasKeyed As String = String.Empty  '*DATA+96   A1   DP:CKEY     * ON = Credit Card was Keyed not SWIPED  "N" or "Y"
        Dim strDlpaidPostCodeForCoupon As String = String.Empty   '*DATA+97   A8   DP:POST     * Customer Post code - if coupon         "aaaaaaaa"
        Dim strDlpaidChequeAccountNumber As String = String.Empty   '*DATA+105  A10  DP:CKAC     * Cheque account number                "nnnnnnnnnn"
        Dim strDlpaidChequeSortCode As String = String.Empty '*DATA+115  A6   DP:CKSC     * Cheque sort code                         "nnnnnn"
        Dim strDlpaidChequeNumber As String = String.Empty   '*DATA+121  A6   DP:CKNO     * Cheque Number                            "nnnnnn"
        Dim strDlpaidEftposVoucherSequenceNumber As String = String.Empty   '*DATA+127  A4   DP:SEQN     * EFTPOS Voucher Sequence number             "nnnn"
        Dim strDlpaidMaestroCardIssueNumber As String = String.Empty  '*DATA+131  A2   DP:ISSU     * Switch Card Issue number                     "aa"
        Dim strDlpaidAuthorisationType As String = String.Empty  '*DATA+133  A1   DP:ATYP     * Authorisation Type 
        Dim strDlpaidEftposMerchantNumber As String = String.Empty  '*DATA+134  A15  DP:MERC     * EFTPOS merchant number          "aaaaaaaaaaaaaaa"
        Dim strDlpaidCashBackAmount As String = String.Empty  '*DATA+149  A10  DP:CBAM     * Cash back amount                     "nnnnnn.nn-"
        Dim strDlpaidCardNumberDigitCount As String = String.Empty   '*DATA+159  A2   DP:DIGC     * Digits count for Credit & Cheque cards etc.  "nn"
        Dim strDlpaidEftposCommsFileHasBeenPrepared As String = String.Empty  '*DATA+161  A1   DP:ECOM     * Y = EFTPOS Comms file prepared                "Y"
        Dim strDlpaidCardCustomerWasPresent As String = String.Empty  '*DATA+162  A1   DP:ICCP *W02* Y = Card Customer Present                     "Y"
        Dim strDlpaidCardDescription As String = String.Empty  '*DATA+163  A30  DP:CTYP     * Card Description 
        Dim strDlpaidEftposTransactionID As String = String.Empty  '*DATA+193  A6   DP:EFID     * Unique Transaction ID given to EFTPOS 
        Dim strDlpaidEftposPaymentCollected As String = String.Empty  '*DATA+199  A1   DP:EFTC     * Set to C once the payment has been acknowledge
        Dim DPStdt As String = String.Empty  '*DAT1+000  A4   DP:STDT     * 
        Dim strDlpaidAuthorisationCodeDesc As String = String.Empty '*DAT1+004  A20  DP:AUTHDESC *
        Dim DPSecCode As String = String.Empty  '*DAT1+024  A03  DP:SECCODE  *
        Dim DPTenc As String = String.Empty  '*DAT1+027  A01  DP:TENC     * Tender Status Code
        Dim DPMpow As String = String.Empty  '*DAT1+028  A02  DP:MPOW     * Conversion Factor
        Dim DPMrat As String = String.Empty  '*DAT1+030  A02  DP:MRAT     * Conversion Rate
        Dim DPTenv As String = String.Empty   '*DAT1+032  A02  DP:TENV     * Value

        Dim strSthotDVTillNumber As String = String.Empty '*DATA+22   A2   DV:TILL     * PC Till Id                                   "nn"
        Dim strSthotDVTransactionNumber As String = String.Empty  '*DATA+24   A4   DV:TRAN     * Transaction Number                         "nnnn"
        Dim strSthotDVTransactionintLineNumber As String = String.Empty  '*DATA+28   A4   DV:NUMB     * Transaction Line number                    "nnnn"
        Dim strSthotDVSkuNumber As String = String.Empty '*DATA+32   A6   DL:SKUN     * Sku Number of this Item                  "nnnnnn"
        Dim strSthotDVEventSequenceNumber As String = String.Empty  '*DATA+38   A6   DV:ESEQ     * Event Sequence Number                    "nnnnnn"
        Dim strSthotDVEventType As String = String.Empty '*DATA+44   A2   DV:TYPE     * Erosion Type
        Dim strSthotDVEventAmount As String = String.Empty  '*DATA+46   A10  DV:AMNT     * Discount Amount - Calculated by Till "nnnnnn.nn-"
        ':DATA for type "DA" - Transaction ANA not recognised (Version W01)


        Dim strSthotDATillNumber As String = String.Empty '*DATA+22   A2   DA:TILL *W03* PC Till Id                                   "nn"
        Dim strSthotDATransactionNumber As String = String.Empty '*DATA+24   A4   DA:TRAN *W03* Transaction Number                         "nnnn"
        Dim strSthotDATransactionintLineNumber As String = String.Empty '*DATA+28   A4   DA:NUMB *W03* Transaction Line number                    "nnnn"
        Dim strSthotDATransactionLineSequenceNumber As String = String.Empty '*DATA+32   A4   DA:SEQN *W03* Line Sequence Number                       "nnnn"
        Dim strSthotDAEanNumberScanned As String = String.Empty  '*DATA+36   A16  DA:EANN *W03* EAN number                     "nnnnnnnnnnnnnnnn"

        ':DATA for type "DR" - Transaction Refund Customer (Version W03)

        Dim strSthotDRTillNumber As String '*DATA+22   A2   DR:TILL    * PC Till Id - Network ID Number                "nn"
        Dim strSthotDRTransactionNumber As String '*DATA+24   A4   DR:TRAN    * Transaction Number from PC Till             "nnnn"
        Dim strSthotDRCustomerName As String '*DATA+28   A30  DR:NAME    * Customer Name - Mandatory             "aaa....aaa"
        Dim strSthotDRPostCode As String '*DATA+58   A8   DR:POST    * Customer Postcode - NOT Mandatory       "aaaaaaaa"
        Dim strSthotDROriginalTransactionStoreNumber As String '*DATA+66   A3   DR:OSTR    * Store of Original Sale - Mandatory           "nnn"
        Dim strSthotDROriginalTransactionDate As String '*DATA+69   A8   DR:ODAT    * Date of Original Sale - NOT Mandatory   "dd/mm/yy"
        Dim strSthotDROriginalTransactionTillNumber As String '*DATA+77   A2   DR:OTIL    * Till ID of Original Sale - NOT Mandatory      "nn"
        Dim strSthotDROriginalTransactionNumber As String '*DATA+79   A4   DR:OTRN    * Tran No of Original Sale - NOT Mandatory    "nnnn"
        Dim strSthotDRHouseName As String '*DATA+83   A15  DR:HNAM    * House Name/Number - Mandatory    "aaaaaaaaaaaaaaa"
        Dim strSthotDRAddressLine1 As String '*DATA+98   A30  DR:HAD1    * House Address Line 1                  "aaa....aaa"
        Dim strSthotDRAddressLine2 As String '*DATA+128  A30  DR:HAD2    * House Address Line 2                  "aaa....aaa"
        Dim strSthotDRAddressLine3 As String '*DATA+158  A30  DR:HAD3    * House Address Line 3 - NOT Mandatory  "aaa....aaa"
        Dim strSthotDRPhoneNumber As String '*DATA+188  A15  DR:PHON    * House Address Line 3 - NOT Mandatory  "aaa....aaa"
        Dim strSthotDRintLineNumber As String '*DATA1+003 A04  DR:NUMB    * Line Number					
        Dim strSthotDROriginalTransactionValidated As String '*DATA1+007 A01  DR:OVAL    * Indicate original transaction was validated	
        Dim strSthotDROriginalTransactionTenderType As String '*DATA1+008 A02  DR:OTEN    * Original Tender Type (Only if validated 
        Dim strSthotDRRefundReasonCode As String '*DATA1+010 A02  DR:RCOD    * Reason for refund (CODE from SYSCOD)
        Dim strSthotDRLabelRequiredIndicator As String '*DATA1+012 A01  DR:LABL    * Indicate Labels required for this line
        Dim strSthotDRMobilePhoneNumber As String '*DATA1+013 A01  DR:MOBP    * Mobile Phone Number



        'Dim DODate As String = String.Empty  '*  :DO-DATE   A8(@=DF:DATA+2)   	* Transaction Date                  "dd/mm/yy"
        Dim strSthotDOTillNumber As String = String.Empty '*  :DO-TILL   A2(@=DF:DATA+22)   	* PC Till Id                              "nn"               
        Dim strSthotDOTransactionNumber As String = String.Empty '*  :DO-TRAN   A4(@=DF:DATA+24)   	* Transaction Number                    "nnnn"             
        Dim strSthotDOTransactionSequenceNumber As String = String.Empty '*  :DO-NUMB   A4(@=DF:DATA+28)   	* Transaction Line number               "nnnn"             
        Dim strSthotDOCompetitorName As String = String.Empty '*  :DO-CNAM   T30(@=DF:DATA+32)   	* Name
        Dim strSthotDOCompetitorAddressLine1 As String = String.Empty '*  :DO-CAD1   T30(@=DF:DATA+62)    * Address Line 1
        Dim strSthotDOCompetitorAddressLine2 As String = String.Empty '*  :DO-CAD2   T30(@=DF:DATA+92)   	* Address Line 2
        Dim strSthotDOCompetitorAddressLine3 As String = String.Empty '*  :DO-CAD3   T30(@=DF:DATA+122)   * Address Line 3 
        Dim strSthotDOCompetitorAddressLine4 As String = String.Empty '*  :DO-CAD4   T30(@=DF:DATA+152)   * Address Line 4
        Dim strSthotDOCompetitorPostCode As String = String.Empty  '*  :DO-CPST   A08(@=DF:DATA+182)	* Post Code
        Dim strSthotDOCompetitorPhoneNumber As String = String.Empty '*  :DO-CPHN1  A10(@=DF:DATA+190)	* Phone Number 
        Dim strSthotDOCompetitorFaxNumber As String = String.Empty  '*  :DO-CFAX   A15(@=DF:DAT1+005)   * Fax   Number
        Dim strSthotDOCompetitorPrice As String = String.Empty  '*  :DO-EPRI   A08(@=DF:DAT1+020)   * Competitors price (Entered)
        Dim strSthotDoVatInclusivePrices As String = String.Empty '*  :DO-EVAT   A01(@=DF:DAT1+028)   * ON = Entered Prices are VAT Inclusive
        Dim strSthotDOPreviousPurchase As String = String.Empty  '*  :DO-PREV   A01(@=DF:DAT1+029)	* Promise only.  ON = from a previous purchase
        Dim strSthotDOCompetitorPriceConverted As String = String.Empty '*  :DO-CPRI   A08(@=DF:DAT1+030)   * Competitors price (Converted for comparison)
        Dim strSthotDOOriginalStoreNumber As String = String.Empty '*  :DO-OSTR   A03(@=DF:DAT1+038)	* Original Store Number
        Dim strSthotDOOriginalTillNumber As String = String.Empty  '*  :DO-OTIL   A02(@=DF:DAT1+041)	* Original Till  Number
        Dim strSthotDOOriginalTransactionNumber As String = String.Empty  '*  :DO-OTRN   A04(@=DF:DAT1+043)	* Original Trans Number
        Dim strSthotDOOriginalTransactionDate As String = String.Empty '*  :DO-ODAT   A08(@=DF:DAT1+047)	* Original Date  
        Dim strSthotDOOriginalSellingPrice As String = String.Empty '*  :DO-OPRI   A08(@=DF:DAT1+055)   * Original Wickes Selling Price

        '******************************************************************************
        '*DATA for type "DX" - Gift Vouchers Transactions in DLGIFT		(*W09)

        'Dim DXdate As String = String.Empty '*  :DX-DATE   A8(@=DF:DATA+2)   	* Transaction Date                  "dd/mm/yy"
        Dim strSthotDXTillNumber As String = String.Empty '*  :DX-TILL   A2(@=DF:DATA+22)   	* PC Till Id                              "nn"               
        Dim strSthotDXTransactionNumber As String = String.Empty  '*  :DX-TRAN   A4(@=DF:DATA+24)   	* Transaction Number                    "nnnn"             
        Dim strSthotDXTransactionSequenceNumber As String = String.Empty '*  :DX-LINE   A5(@=DF:DATA+28)   	* Transaction Line number               "nnnn"             
        Dim strSthotDXSerialNumber As String = String.Empty '*  :DX-SERI   A8(@=DF:DATA+33)   	* Gift Voucher Serial Number        "nnnnnnnn"          
        Dim strSthotDXEmployeeId As String = String.Empty  '*  :DX-EEID   A3(@=DF:DATA+41)   	* Employee ID performing sale            "nnn"
        Dim strSthotDXTransactionType As String = String.Empty '*  :DX-TYPE   A2(@=DF:DATA+44)   	* Transaction Type                        "AA"
        Dim strSthotDXGiftVoucherValue As String = String.Empty '*  :DX-AMNT   A10(@=DF:DATA+46)   	* Value Of Gift Voucher           "NNNNNN.NN-"

        '*DATA for type "HT" - Daily Trade Summary Details                 

        Dim strSthotHTDate As String = String.Empty '*  :HT-DATE   A8(@=DF:DATA+2)   	* Transaction Date                  "dd/mm/yy"
        Dim strSthotHTStartTime As String = String.Empty  '*  :HT-STIM   A6(@=DF:DATA+22)   	* Start Time                         "nnnnnn"             
        Dim strSthotHTEndTime As String = String.Empty '*  :HT-ETIM   A6(@=DF:DATA+28)   	* End   Time                         "nnnnnn"             
        Dim strSthotHTNoOfSales As String = String.Empty '*  :HT-NSAL   A7(@=DF:DATA+34)   	* Number of sale transactions        "nnnnnn"             
        Dim strSthotHTValueOfSales As String = String.Empty '*  :HT-VSAL   A10(@=DF:DATA+41)   	* Value of sale transactions     "nnnnnn.nn-"         
        Dim strSthotHTNoOfRefunds As String = String.Empty '*  :HT-NREF   A7(@=DF:DATA+51)   	* Number of sale transactions        "nnnnnn"             
        Dim strSthotHTValueOfRefunds As String = String.Empty  '*  :HT-VREF   A10(@=DF:DATA+58)   	* Value of sale transactions     "nnnnnn.nn-"         
        Dim strSthotHTTimeSlot As String = String.Empty '*  :HT-SLOT   A04(@=DF:DATA+68)   	* Time slot (in Minutes)                "nnn"         

        '*****************************************************************************

        '*DATA for type "DO" - Price Match/promise Line Item Information (DLOLIN)  *W13


        '******************************************************************************

        '*DATA for type "DC" - Price Match/promise Customer Information (DLOCUS)  *W13

        Dim strSthotstrSthotDCTransactionNumbersactionDate As String = String.Empty  '*  :OC-DATE   A8(@=DF:DATA+2)   	* Transaction Date                  "dd/mm/yy"
        Dim strSthotDCTillNumber As String = String.Empty '*  :OC-TILL   A2(@=DF:DATA+22)   	* PC Till Id                              "nn"               
        Dim strSthotDCTransactionNumber As String = String.Empty  '*  :OC-TRAN   A4(@=DF:DATA+24)   	* Transaction Number                    "nnnn"             
        Dim strSthotDCCustomerName As String = String.Empty '*  :OC-NAME   T30(@=DF:DATA+28)   	* Name
        Dim strSthotDCCustomerAddressLine1 As String = String.Empty  '*  :OC-ADD1   T30(@=DF:DATA+58)    * Address Line 1
        Dim strSthotDCCustomerAddressLine2 As String = String.Empty '*  :OC-ADD2   T30(@=DF:DATA+88)   	* Address Line 2
        Dim strSthotDCCustomerAddressLine3 As String = String.Empty '*  :OC-ADD3   T30(@=DF:DATA+118)   * Address Line 3 
        Dim strSthotDCCustomerAddressLine4 As String = String.Empty '*  :OC-ADD4   T30(@=DF:DATA+148)   * Address Line 4 
        Dim strSthotDCCustomerPostCode As String = String.Empty '*  :OC-POST   A08(@=DF:DATA+178)	* Post Code
        Dim strSthotDCCustomerPhoneNumber As String = String.Empty '*  :OC-PHON1  A14(@=DF:DAT1+186)	* Phone Number - First 14 chars
        Dim boolProcessThisTransaction As Boolean = False

        Dim dateLastDateChecked As Date = Date.MinValue.Date
        Dim strLastTillChecked As String = String.Empty
        Dim strLastTransactionChecked As String = String.Empty
        Dim boolCanProcessThisDltotsRecord As Boolean = False
        Dim intMyOrderNumber As Integer = 0

        Dim decSecondaryStoreVoucherTenderNumber As Decimal = 0

        Dim Retopt As New BOSystem.cRetailOptions(_Oasys3DB)
        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadField(Retopt.TenderNoStoreVouchs)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde01)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde02)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde03)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde04)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde05)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde06)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde07)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde08)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde09)
        Retopt.AddLoadField(Retopt.PriceOvrdReasonCde10)
        Retopt.AddLoadField(Retopt.TenderTypeDesc01)
        Retopt.AddLoadField(Retopt.TenderTypeDesc02)
        Retopt.AddLoadField(Retopt.TenderTypeDesc03)
        Retopt.AddLoadField(Retopt.TenderTypeDesc04)
        Retopt.AddLoadField(Retopt.TenderTypeDesc05)
        Retopt.AddLoadField(Retopt.TenderTypeDesc06)
        Retopt.AddLoadField(Retopt.TenderTypeDesc07)
        Retopt.AddLoadField(Retopt.TenderTypeDesc08)
        Retopt.AddLoadField(Retopt.TenderTypeDesc09)
        Retopt.AddLoadField(Retopt.TenderTypeDesc10)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc01)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc02)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc03)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc04)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc05)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc06)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc07)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc08)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc09)
        Retopt.AddLoadField(Retopt.TenderTypeFlagDesc10)
        Retopt.AddLoadField(Retopt.CountryCode)
        Retopt.AddLoadField(Retopt.LastReformatDate)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()

        decSecondaryStoreVoucherTenderNumber = Retopt.TenderNoStoreVouchs.Value + 10

        Dim arrOverrideReasonCodes As String() = New String(10) {" ", Retopt.PriceOvrdReasonCde01.Value, Retopt.PriceOvrdReasonCde02.Value, Retopt.PriceOvrdReasonCde03.Value, Retopt.PriceOvrdReasonCde04.Value, Retopt.PriceOvrdReasonCde05.Value, Retopt.PriceOvrdReasonCde06.Value, Retopt.PriceOvrdReasonCde07.Value, Retopt.PriceOvrdReasonCde08.Value, Retopt.PriceOvrdReasonCde09.Value, Retopt.PriceOvrdReasonCde10.Value}
        Dim arrTenderTypeDescriptions As String() = New String(10) {" ", Retopt.TenderTypeDesc01.Value, Retopt.TenderTypeDesc02.Value, Retopt.TenderTypeDesc03.Value, Retopt.TenderTypeDesc04.Value, Retopt.TenderTypeDesc05.Value, Retopt.TenderTypeDesc06.Value, Retopt.TenderTypeDesc07.Value, Retopt.TenderTypeDesc08.Value, Retopt.TenderTypeDesc09.Value, Retopt.TenderTypeDesc10.Value}
        Dim arrTenderTypeFlagDescriptions As String() = New String(10) {" ", Retopt.TenderTypeFlagDesc01.Value, Retopt.TenderTypeFlagDesc02.Value, Retopt.TenderTypeFlagDesc03.Value, Retopt.TenderTypeFlagDesc04.Value, Retopt.TenderTypeFlagDesc05.Value, Retopt.TenderTypeFlagDesc06.Value, Retopt.TenderTypeFlagDesc07.Value, Retopt.TenderTypeFlagDesc08.Value, Retopt.TenderTypeFlagDesc09.Value, Retopt.TenderTypeFlagDesc10.Value}

        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)
        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadField(SysDates.TodayDayCode)
        SysDates.AddLoadField(SysDates.TomorrowsDayCode)
        SysDates.AddLoadField(SysDates.EndofWeekDay)
        SysDates.AddLoadField(SysDates.DaysOpen)
        SysDates.AddLoadField(SysDates.WeekEnding02)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()


        Dim decDayOfWeek As Decimal = 0
        If SysDates.TodayDayCode.Value = SysDates.EndofWeekDay.Value Then
            boolThisIsAWeekEnd = True
        End If
        decDayOfWeek = SysDates.TodayDayCode.Value
        While decDayOfWeek <> SysDates.TodayDayCode.Value 'Checking if this is a week end
            decDayOfWeek += 1
            If decDayOfWeek > 7 Then decDayOfWeek -= 7

            If decDayOfWeek = SysDates.EndofWeekDay.Value Then
                boolThisIsAWeekEnd = True
                Exit While
            End If
        End While ' Check if week end complete

        dateSelectionDate = SysDates.Today.Value
        If boolDoPrepareSthof = True Then
            CalculateOpenPuchaseOrderValue()
            intRecordsOutput = 0
            strWorkString = "Processing STHOF (OF) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            Dim boolCurrentStockValueCalculated As Boolean = False
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse
            dateSelectionDate = SysDates.Today.Value
            If _RunningInNight = True Then
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
            End If
            dateSelectionDate = _StartDate

            'Added 14/09/2009 to calc HOSTF values for FA Record Types
            If (boolThisIsAWeekEnd = True) Then
                Dltots.ClearLists()
                Dltots.ClearLoadField()
                Dltots.ClearLoadFilter()

                'get all DLLINES for week where MarkDown Flag is set
                Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Dlline.TransDate, SysDates.WeekEnding02.Value)
                Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Dlline.TransDate, SysDates.Today.Value)
                Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.MarkDownStock, True)
                Dlline.SortBy(Dlline.SkuNumber.ColumnName,OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                ColDl = Dlline.LoadMatches
                For Each Dlline In ColDl
                    Dltots.ClearLists()
                    Dltots.ClearLoadField()
                    Dltots.ClearLoadFilter()
                    'get all DLTOTS for week, excluding Voided and Training Mode
                    Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, Dlline.TransDate.Value)
                    Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TillID, Dlline.TillID.Value)
                    Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionNo, Dlline.TransNo.Value)
                    Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.Voided, False)
                    Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TrainingMode, False)
                    Dltots.AddLoadField(Dltots.TransactionNo)

                    If (Dltots.LoadMatches.Count > 0) Then 'Valid Transaction so add to totals
                        decWeeklyMarkdownsValue -= (Dlline.PriceOverrideAmount.Value * Dlline.QuantitySold.Value)
                    End If
                Next 'Line to check header against

                'After Adding up sold Mark Downs, step through Stock Adjustments
                Dim StockAdjBO As New BOStock.cStockAdjust(_Oasys3DB)
                StockAdjBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockAdj.DateCreated, SysDates.WeekEnding02.Value)
                StockAdjBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dim AdjCodes() As String = {"W", "T"}
                StockAdjBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pIn, StockAdj.Code, AdjCodes)
                Dim StockAdjs As List(Of BOStock.cStockAdjust)
                StockAdjs = StockAdjBO.LoadMatches
                For Each StockAdjBO In StockAdjs
                    If (StockAdjBO.Code.Value = "W") Then
                        decWeeklyMarkdownsValue += StockAdjBO.QtyAdjusted.Value * StockAdjBO.Price.Value
                    Else
                        decWeeklyMarkdownsValue += StockAdjBO.TransferValue.Value
                    End If
                Next
            End If

            'Get Values of items out of stock
            'Dim oStock As New BOStock.cStock(_Oasys3DB)
            'oStock.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggCount, oStock.SkuNumber, "NoOutStock")
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, oStock.StockOnHand, 0)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.ItemDeleted, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.ItemObsolete, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.NonStockItem, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.RelatedItemSingle, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.CatchAll, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.DoNotOrder, False)
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, oStock.LastSold, CDate("1/1/1900"))
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, oStock.FinalOrderDate, CDate("1/1/1900"))
            'oStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            'oStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, oStock.FinalOrderDate, SysDates.Today.Value)

            ' SELECT @Quantity = COUNT(STKMAS.SKUN) FROM STKMAS WHERE STKMAS.ONHA <= 0
            ' AND STKMAS.IDEL=0 AND STKMAS.IOBS=0 AND STKMAS.INON=0 AND STKMAS.IRIS=0 AND STKMAS.ICAT=0 AND STKMAS.NOOR=0 
            ' AND STKMAS.DATS IS NOT NULL AND ((STKMAS.FODT IS NULL) OR (STKMAS.FODT>=" & @TODT;

            Dim dsCount As Data.DataSet = _Oasys3DB.ExecuteSql("SELECT COUNT(*),SUM(CORHDR.MVST),SUM(CORHDR.DCST) FROM CORHDR INNER JOIN QUOHDR ON " & _
                "CORHDR.NUMB=QUOHDR.NUMB WHERE SOLD=1 AND CORHDR.CANC <> '1'" & _
                " AND CORHDR.DELI=1 AND CORHDR.DATE1 ='" & SysDates.Today.Value.ToString("yyyy-MM-dd") & "'")
            If (dsCount.Tables(0).Rows.Count > 0) Then
                If (IsDBNull(dsCount.Tables(0).Rows(0).Item(0)) = False) Then intTotalNumberOfDeliveries = CInt(dsCount.Tables(0).Rows(0).Item(0))
                If (IsDBNull(dsCount.Tables(0).Rows(0).Item(1)) = False) Then decTotalValueOfDeliveriesSold = CDec(dsCount.Tables(0).Rows(0).Item(1))
                If (IsDBNull(dsCount.Tables(0).Rows(0).Item(2)) = False) Then decTotalValueOfDeliveryCharges = CDec(dsCount.Tables(0).Rows(0).Item(2))
            End If
            dsCount.Dispose()


            'Dim dsCount As Data.DataSet = oStock.GetAggregateDataSet
            dsCount = _Oasys3DB.ExecuteSql("SELECT COUNT(SKUN) FROM STKMAS WHERE ONHA <= 0" & _
                " AND IDEL=0 AND IOBS=0 AND INON=0 AND IRIS=0 AND ICAT=0 AND NOOR=0 AND DATS IS NOT NULL AND ((FODT IS NULL) OR (FODT>='" & SysDates.Today.Value.ToString("yyyy-MM-dd") & "'))")
            If (dsCount.Tables(0).Rows.Count > 0) Then intNoOfSKUsOutOfStock = CInt(dsCount.Tables(0).Rows(0).Item(0))
            dsCount.Dispose()

            While dateSelectionDate <= _EndDate
                Dltots.ClearLists()
                Dltots.ClearLoadField()
                Dltots.ClearLoadFilter()

                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, dateSelectionDate)
                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                ColDt = Dltots.LoadMatches
                'ColDt.Reverse()
                'If boolDoPrepareSthof = True Then ' Preparing Output For STHOF file

                Array.Clear(arrDecFlashTotalAmounts, 1, 13)
                Array.Clear(arrIntFlashTotalCounts, 1, 13)
                Array.Clear(arrIntFlashTotalTenderTypeCount, 1, 20)
                Array.Clear(arrDecFlashTotalTenderTypeAmount, 1, 20)

                For Each record In ColDt ' selecting DLTOTS RECORDS.
                    strWorkString = "Processing :" & Space(1) & record.TransDate.Value.Date.ToString & Space(1) & record.TillID.Value.PadLeft(2, "0"c) & Space(1) & record.TransactionNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    intFlashTotalOccurrence = 13
                    If record.Voided.Value = True Then ' Just add to voided fields
                        arrIntFlashTotalCounts(11) = arrIntFlashTotalCounts(11) + 1
                        arrDecFlashTotalAmounts(11) = arrDecFlashTotalAmounts(11) + record.TotalSalesAmount.Value
                    End If ' Voided fields updated
                    If record.Voided.Value = False Then ' Accumulate Zreads
                        If record.TransactionCode.Value = "ZR" Then
                            decFlashTotalZReads = decFlashTotalZReads + record.TotalSalesAmount.Value
                        End If ' Zreads accumulated
                        If record.TrainingMode.Value = False Then ' Dont process if in traing mod
                            ' assume tender is cash
                            arrDecFlashTotalTenderTypeAmount(1) = arrDecFlashTotalTenderTypeAmount(1) + record.TotalSalesAmount.Value
                            Select Case record.TransactionCode.Value
                                Case "SA"
                                    intFlashTotalOccurrence = 1
                                    If (record.OrderNo.Value <> "000000") Then decDailyFrontEndSalesValue += record.TotalSalesAmount.Value
                                    intDailyFrontEndSalesCount += 1

                                Case "SC"
                                    intFlashTotalOccurrence = 2
                                    If (record.OrderNo.Value <> "000000") Then decDailyFrontEndSalesValue += record.TotalSalesAmount.Value
                                    intDailyFrontEndSalesCount -= 1

                                Case "RF"
                                    intFlashTotalOccurrence = 3
                                    decDailyFrontEndRefundValue += record.TotalSalesAmount.Value
                                    intDailyFrontEndRefundCount += 1

                                Case "RC"
                                    intFlashTotalOccurrence = 4
                                    decDailyFrontEndRefundValue += record.TotalSalesAmount.Value
                                    intDailyFrontEndRefundCount += 1

                                Case "CO" : intFlashTotalOccurrence = 5
                                Case "CC" : intFlashTotalOccurrence = 6
                                Case "M+" : intFlashTotalOccurrence = 7
                                Case "C+" : intFlashTotalOccurrence = 7
                                Case "M-" : intFlashTotalOccurrence = 8
                                Case "C-" : intFlashTotalOccurrence = 8
                                Case "OD" : intFlashTotalOccurrence = 9
                                Case "RL" : intFlashTotalOccurrence = 10
                            End Select

                            arrIntFlashTotalCounts(intFlashTotalOccurrence) += 1
                            arrDecFlashTotalAmounts(intFlashTotalOccurrence) += record.TotalSalesAmount.Value

                            ' Now process DLLINE & DLPAID Records
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, record.TransDate.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TillID, record.TillID.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransNo, record.TransactionNo.Value)
                            Dlline.SortBy(Dlline.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            ColDl = Dlline.LoadMatches()

                            For Each detail As BOSales.cSalesLine In ColDl ' Process the DLLINE records.
                                ' Update PRICE Violation fields
                                If detail.PriceOverrideReason.Value > 0 And record.EmployeeDiscountOnly.Value = False And detail.MarkDownStock.Value = False Then
                                    arrDecFlashTotalAmounts(12) += (detail.PriceOverrideAmount.Value * detail.QuantitySold.Value * -1)
                                    arrIntFlashTotalCounts(12) += 1
                                End If ' Update price violations complete
                                If detail.SaleType.Value = "V" Then ' GIFT TOKEN FOUND
                                    decGiftTokenTotalValue = decGiftTokenTotalValue + detail.ExtendedValue.Value
                                    intGiftTokensUsed = CInt(intGiftTokensUsed + detail.QuantitySold.Value)
                                    intGiftTokenSkuNumber = CInt(detail.SkuNumber.Value)
                                    decGiftTokenTotalVatValue = decGiftTokenTotalVatValue + detail.VatAmount.Value
                                End If ' GIFT TOKEN UPDATES
                                If detail.SaleType.Value = "D" Or detail.SaleType.Value = "I" Then
                                    ' Delivery/installations processed now
                                    If detail.SaleType.Value = "D" Then
                                        decDailyFrontEndSalesAndRefundDeliveryValue += detail.ExtendedValue.Value
                                    End If
                                    If detail.SaleType.Value = "I" Then
                                        decDailyFrontEndSalesAndRefundInstallationValue += detail.ExtendedValue.Value
                                    End If
                                    decWorkExtendedPrice = 0 - detail.ExtendedValue.Value
                                    If record.EmployeeDiscountOnly.Value = False Then
                                        decWorkExtendedPrice += detail.QtyBreakMarginAmount.Value + detail.DealGroupMarginAmt.Value + detail.MultiBuyMarginAmount.Value + detail.HierarchyMarginAmt.Value + detail.EmpSaleSecMarginAmt.Value
                                    End If
                                    arrDecFlashTotalAmounts(intFlashTotalOccurrence) += decWorkExtendedPrice
                                End If ' Delivery/installation updates
                            Next ' Process the DLLINE records - Complete.
                            ' Only select non cash tenders from DLPAID
                            Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TransDate, record.TransDate.Value)
                            Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TillID, record.TillID.Value)
                            Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TransNo, record.TransactionNo.Value)
                            Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Dlpaid.TenderType, 1)
                            Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, Dlpaid.TenderType, 99)
                            Dlpaid.SortBy(Dlpaid.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlpaid.SortBy(Dlpaid.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlpaid.SortBy(Dlpaid.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlpaid.SortBy(Dlpaid.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                            ColDp = Dlpaid.LoadMatches
                            For Each payment As BOSales.cSalesPaid In ColDp 'Update the flash total tenders
                                If payment.TenderType.Value > 1 And payment.TenderType.Value < 99 Then
                                    'Update flash total tender values
                                    arrIntFlashTotalTenderTypeCount(CInt(payment.TenderType.Value)) = arrIntFlashTotalTenderTypeCount(CInt(payment.TenderType.Value)) + 1
                                    arrDecFlashTotalTenderTypeAmount(CInt(payment.TenderType.Value)) = arrDecFlashTotalTenderTypeAmount(CInt(payment.TenderType.Value)) - payment.TenderAmount.Value
                                    arrDecFlashTotalTenderTypeAmount(1) = arrDecFlashTotalTenderTypeAmount(1) + payment.TenderAmount.Value
                                End If ' Flash total tenders updated
                                If payment.TenderType.Value = Retopt.TenderNoStoreVouchs.Value Or payment.TenderType.Value = decSecondaryStoreVoucherTenderNumber Then
                                    ' UPDATE daily front end voucher values
                                    decDailyFrontEndSalesAndRefundVoucherValue = decDailyFrontEndSalesAndRefundVoucherValue - payment.TenderAmount.Value
                                    intDailyFrontEndSalesAndRefundVoucherCount = intDailyFrontEndSalesAndRefundVoucherCount + 1
                                End If ' Daily Front end vouchers updated
                            Next 'Flash Total Tenders updated
                        End If ' Non training mode transactions processed
                    End If ' Non VOIDED transactions processed
                Next ' Selecting DLTOTS records
                Dim Prcchg As New BOStock.cPriceChange(_Oasys3DB)
                Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Prcchg.EffectiveDate, SysDates.Today.Value)
                Prcchg.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Prcchg.ChangeStatus, "U")
                Dim ColIP As List(Of BOStock.cPriceChange) = Prcchg.LoadMatches()
                intNumberOfUnappliedPricesChanges = ColIP.Count
                Dim DRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)
                Dim dateLowDate As Date = SysDates.Today.Value.AddDays((SysDates.Today.Value.DayOfWeek - 1) * -1)
                DRLHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, DRLHeader.AssignedDate, dateLowDate)
                DRLHeader.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                DRLHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, DRLHeader.AssignedDate, SysDates.Today.Value)
                DRLHeader.SortBy(DRLHeader.Number.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                DRLHeader.Headers = DRLHeader.LoadMatches()
                For Each header As BOPurchases.cDrlHeader In DRLHeader.Headers
                    strWorkString = "Processing DRL Number :" & Space(1) & header.Number.Value.PadLeft(6, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    If header.AssignedDate.Value = SysDates.Today.Value Then
                        If header.Type.Value = "0" Or header.Type.Value = "1" Then
                            decValueOfTodaysDailyReceiverListings = decValueOfTodaysDailyReceiverListings + header.Value.Value
                        End If
                        If header.Type.Value = "2" Or header.Type.Value = "3" Then
                            decValueOfTodaysDailyReceiverListings = decValueOfTodaysDailyReceiverListings - header.Value.Value
                        End If
                    End If
                    If header.AssignedDate.Value <= SysDates.Today.Value Then
                        If header.Type.Value = "0" Or header.Type.Value = "1" Then
                            decValueOfThisWeeksDailyReceiverListings = decValueOfThisWeeksDailyReceiverListings + header.Value.Value
                        End If
                        If header.Type.Value = "2" Or header.Type.Value = "3" Then
                            decValueOfThisWeeksDailyReceiverListings = decValueOfThisWeeksDailyReceiverListings - header.Value.Value
                        End If
                    End If
                Next
                Dim StockAdjustments As New BOStock.cStockAdjust(_Oasys3DB)
                Dim ColAd As New List(Of BOStock.cStockAdjust)
                'get valid summary record
                StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdjustments.DateCreated, SysDates.Today.Value)
                StockAdjustments.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, StockAdjustments.Code, "58")
                StockAdjustments.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, StockAdjustments.Code, "59")
                StockAdjustments.SortBy(StockAdjustments.DateCreated.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                StockAdjustments.SortBy(StockAdjustments.Code.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                StockAdjustments.SortBy(StockAdjustments.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                StockAdjustments.SortBy(StockAdjustments.Sequence.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColAd = StockAdjustments.LoadMatches()
                For Each adjustment As BOStock.cStockAdjust In ColAd
                    strWorkString = "Processing Stock Adjustment :" & Space(1) & adjustment.SkuNumber.Value.PadLeft(6, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    decValueOfDailyMarkdowns = decValueOfDailyMarkdowns + adjustment.TransferPrice.Value
                    If CInt(adjustment.SkuNumber.Value) < 800000 Or CInt(adjustment.SkuNumber.Value) > 809999 Then
                        If adjustment.Code.Value = "02" Or adjustment.Code.Value = "05" Or adjustment.Code.Value = "10" Or adjustment.Code.Value = "11" Then
                            decValueOfCode02Adjustments = decValueOfCode02Adjustments + (adjustment.QtyAdjusted.Value * adjustment.Price.Value)
                        End If
                    End If
                Next

                'OpenTransmissionFile(strSthofFileName)
                dateHashDate = dateSelectionDate

                FormatIntToString(arrIntFlashTotalCounts(1), _STHOFRecTypeF1SaleTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(2), _STHOFRecTypeF1SaleCorrectTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(3), _STHOFRecTypeF1RefundTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(4), _STHOFRecTypeF1RefundCorrectTransactionCount, 4, "0"c)
                FormatIntToString((arrIntFlashTotalCounts(7) + arrIntFlashTotalCounts(8)), _STHOFRecTypeF1MiscellaneousInTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(9), _STHOFRecTypeF1OpenDrawerTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(11), _STHOFRecTypeF1VoidedTransactionCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalCounts(12), _STHOFRecTypeF1PriceViolationTransactionCount, 4, "0"c)
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                'Set up the F2 record
                _STHOFRecTypeF2SalesTransactionTime = "0000"
                _STHOFRecTypeF2SalesCorrectTransactionTime = "0000"
                _STHOFRecTypeF2RefundTransactionTime = "0000"
                _STHOFRecTypeF2RefundCorrectTransactionTime = "0000"
                _STHOFRecTypeF2MiscellaneousInTransactionTime = "0000"
                _STHOFRecTypeF2OpenDrawerTransactionTime = "0000"
                _STHOFRecTypeF2VoidedTransactionTime = "0000"
                _STHOFRecTypeF2PriceViolationTransactionTime = "0000"
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                'f3asH = DlftotSamt01
                FormatDecToString(arrDecFlashTotalAmounts(1), _STHOFRecTypeF3SalesTransactionAmount, 9, "0"c, "0.00")
                FormatDecToString(arrDecFlashTotalAmounts(2), _STHOFRecTypeF3SalesCorrectTransactionAmount, 9, "0"c, "0.00")
                FormatDecToString(arrDecFlashTotalAmounts(3), _STHOFRecTypeF3RefundTransactionAmount, 9, "0"c, "0.00")
                FormatDecToString(arrDecFlashTotalAmounts(4), _STHOFRecTypeF3RefundCorrectTransactionAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalAmounts(7) + arrDecFlashTotalAmounts(8)), _STHOFRecTypeF3MiscellaneousInTransactionAmount, 9, "0"c, "0.00")
                FormatDecToString(arrDecFlashTotalAmounts(12), _STHOFRecTypeF3PriceViolationTransactionAmount, 9, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Output the F4 record

                FormatDecToString((arrDecFlashTotalTenderTypeAmount(1) * -1), _STHOFRecTypeF4TenderCashAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(2) * -1), _STHOFRecTypeF4TenderChequeAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(3) * -1), _STHOFRecTypeF4TenderMastercardAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(4) * -1), _STHOFRecTypeF4TenderVisaAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(5) * -1), _STHOFRecTypeF4TenderWickesAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(6) * -1), _STHOFRecTypeF4TenderVoucherAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(7) * -1), _STHOFRecTypeF4TenderAmexAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(8) * -1), _STHOFRecTypeF4TenderMaestroAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(9) * -1), _STHOFRecTypeF4TenderBadChequeAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(10) * -1), _STHOFRecTypeF4TenderTradeCreditAmount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(12) * -1), _STHOFRecTypeF4TenderType12Amount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(13) * -1), _STHOFRecTypeF4TenderType13Amount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(14) * -1), _STHOFRecTypeF4TenderType14Amount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(15) * -1), _STHOFRecTypeF4TenderType15Amount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(17) * -1), _STHOFRecTypeF4TenderType17Amount, 9, "0"c, "0.00")
                FormatDecToString((arrDecFlashTotalTenderTypeAmount(18) * -1), _STHOFRecTypeF4TenderType18Amount, 9, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the F5 record
                FormatIntToString(arrIntFlashTotalTenderTypeCount(1), _STHOFRecTypeF5TenderCashCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(2), _STHOFRecTypeF5TenderChequeCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(3), _STHOFRecTypeF5TenderMastercardCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(4), _STHOFRecTypeF5TenderVisaCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(5), _STHOFRecTypeF5TenderWickesCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(6), _STHOFRecTypeF5TenderVoucherCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(7), _STHOFRecTypeF5TenderAmexCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(8), _STHOFRecTypeF5TenderMaestroCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(9), _STHOFRecTypeF5TenderBadChequeCount, 4, "0"c)
                FormatIntToString(arrIntFlashTotalTenderTypeCount(10), _STHOFRecTypeF5TenderTradeCreditCount, 4, "0"c)
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the F6 record
                _STHOFRecTypeF6DailyHoursPaid = "00000"
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the F7 record
                FormatIntToString(intDailyConservatoriesSold, _STHOFRecTypeF7ConservatorySales, 3, "0"c)
                FormatDecToString(decDailyFrontEndSalesValue, _STHOFRecTypeF7FrontEndSalesValue, 9, "0"c, "0.00")
                FormatIntToString(intDailyFrontEndSalesCount, _STHOFRecTypeF7FrontEndSalesCount, 6, "0"c)
                FormatDecToString(decDailyFrontEndRefundValue, _STHOFRecTypeF7FrontEndRefundsValue, 9, "0"c, "0.00")
                FormatIntToString(intDailyFrontEndRefundCount, _STHOFRecTypeF7FrontEndRefundsCount, 6, "0"c)
                FormatDecToString(decDailyFrontEndSalesAndRefundVoucherValue, _STHOFRecTypeF7FrontEndSalesAndRefundVoucherValue, 9, "0"c, "0.00")
                FormatIntToString(intDailyFrontEndSalesAndRefundVoucherCount, _STHOFRecTypeF7FrontEndSalesAndRefundVoucherCount, 6, "0"c)
                FormatDecToString(decDailyFrontEndSalesAndRefundDeliveryValue, _STHOFRecTypeF7FrontEndSalesAndRefundDeliveryValue, 9, "0"c, "0.00")
                FormatDecToString(decDailyFrontEndSalesAndRefundInstallationValue, _STHOFRecTypeF7FrontEndSalesAndRefundInstallationValue, 9, "0"c, "0.00")
                FormatIntToString(intTotalNumberOfDeliveries, _STHOFRecTypeF7TotalDeliveriesCount, 5, "0"c)
                FormatDecToString(decTotalValueOfDeliveriesSold, _STHOFRecTypeF7TotalDeliveriesValue, 10, "0"c, "0.00")
                FormatDecToString(decTotalValueOfDeliveryCharges, _STHOFRecTypeF7TotalDeliveryChargeValue, 10, "0"c, "0.00")

                'Added 22/9/09 as F7 should have 0's in these fields
                FormatDecToString(0, _STHOFRecTypeF7FrontEndSalesValue, 9, "0"c, "0.00")
                FormatIntToString(0, _STHOFRecTypeF7FrontEndSalesCount, 6, "0"c)
                FormatDecToString(0, _STHOFRecTypeF7FrontEndRefundsValue, 9, "0"c, "0.00")
                FormatIntToString(0, _STHOFRecTypeF7FrontEndRefundsCount, 6, "0"c)

                Trace.WriteLine("sale=" & arrDecFlashTotalAmounts(1) & " Frontendsales=" & decDailyFrontEndSalesValue & "tender=" & decWorkValue = decWorkValue + arrDecFlashTotalTenderTypeAmount(6) & ",TotalDel=" & decTotalValueOfDeliveryCharges)
                decWorkValue = arrDecFlashTotalAmounts(1) - 0 'decDailyFrontEndSalesValue
                decWorkValue = decWorkValue + (arrDecFlashTotalTenderTypeAmount(6) * -1) + decTotalValueOfDeliveryCharges
                decDeliveryPercentToSales = 0
                If decWorkValue <> 0 Then
                    decDeliveryPercentToSales = (decTotalValueOfDeliveriesSold * 100) / decWorkValue
                End If
                FormatDecToString(decDeliveryPercentToSales, _STHOFRecTypeF7TotalDeliveryToSalesPercentage, 6, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the F8 record
                FormatIntToString(intNoOfSKUsOutOfStock, _STHOFRecTypeF8NumberOfSkusOutOfStock, 5, "0"c)
                FormatDecToString(decValueOfCode02Adjustments, _STHOFRecTypeF8TodaysType2and10Adjustments, 8, "0"c, "0.00")
                FormatDecToString(decValueOfTodaysDailyReceiverListings, _STHOFRecTypeF8TodaysDailyReceiverListingValue, 10, "0"c, "0.00")
                FormatDecToString(decValueOfThisWeeksDailyReceiverListings, _STHOFRecTypeF8ThisWeekToDateDailyReceiverListingValue, 10, "0"c, "0.00")
                FormatIntToString(intNumberOfUnappliedPricesChanges, _STHOFRecTypeF8NumberOfUnappliedPriceChanges, 6, "0"c)
                FormatDecToString(decValueOfDailyMarkdowns, _STHOFRecTypeF8WeeklyMarkdownsValue, 10, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the F9 record
                FormatDecToString(decWeeklyWagesPaid, _STHOFRecTypeF9WeeklyWagesValue, 8, "0"c, "0.00")
                FormatIntToString(intWeeklyHoursPaid, _STHOFRecTypeF9StandardHours, 5, "0"c)
                FormatIntToString(intWeeklyNumberOfStarters, _STHOFRecTypeF9NumberOfStarters, 3, "0"c)
                FormatIntToString(intWeeklyNumberOfLeavers, _STHOFRecTypeF9NumberOfLeavers, 3, "0"c)
                FormatIntToString(intWeeklyNumberOfFullTimers, _STHOFRecTypeF9NumberOfFullTimeStaff, 3, "0"c)
                FormatIntToString(intWeeklyNumberOfPartTimers, _STHOFRecTypeF9NumberOfPartTimeStaff, 3, "0"c)
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the FA record
                FormatDecToString(decWeeklyMarkdownsValue, strSthofRecordTypeFAMarkdownsValue, 10, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()
                ' Setup the FB record

                Dim decEndOfWeekStockValue As Decimal = 0
                If boolCurrentStockValueCalculated = False Then
                    Dim stock As New BOStock.cStock(_Oasys3DB)
                    decEndOfWeekStockValue = stock.GetOnHandValueAllStock
                    boolCurrentStockValueCalculated = True
                End If

                FormatDecToString(decValueOfOpenPurchaseOrders, strSthofRecordTypeFBOpenPurchaseOrdersValue, 10, "0"c, "0.00")
                FormatDecToString(decEndOfWeekStockValue, strSthofRecordTypeFBStockValue, 10, "0"c, "0.00")
                intSthofRecords = intSthofRecords + 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intSthofRecords.ToString & " - STHOF"
                ProcessTransmissionsProgress.Show()

                intSthofRecords += 1 'Add 1 on for FH Record
                decHashValue = CDec(intSthofRecords)
                strSthofRecordTypeFHDate = dateHashDate.ToString("dd/MM/yy")
                strSthofRecordTypeFHStoreNumber = Retopt.Store.Value.ToString.PadLeft(3, "0"c)
                If boolThisIsAWeekEnd = False Then intSthofRecords -= 3
                strSthofRecordTypeFHNumberOfRecords = intSthofRecords.ToString.PadLeft(6, "0"c)
                OutputFh()
                decHashValue = CDec(arrIntFlashTotalCounts(1))
                OutputF1()
                decHashValue = 0
                OutputF2()
                decHashValue = arrDecFlashTotalAmounts(1)
                OutputF3()
                decHashValue = arrDecFlashTotalTenderTypeAmount(1) * -1
                OutputF4()
                decHashValue = arrIntFlashTotalTenderTypeCount(1)
                OutputF5()
                decHashValue = 0
                OutputF6()
                decHashValue = intDailyConservatoriesSold
                OutputF7()
                If boolThisIsAWeekEnd = True Then
                    decHashValue = decWeeklyWagesPaid
                    OutputF9()
                End If
                decHashValue = intNoOfSKUsOutOfStock
                OutputF8()
                If boolThisIsAWeekEnd = True Then
                    decHashValue = decWeeklyMarkdownsValue
                    OutputFa()
                    decHashValue = decValueOfOpenPurchaseOrders
                    OutputFB()
                End If
                'End If ' End of Preparing Output For STHOF file
                If strSthofText.Length > 0 Then
                    PutSthoToDisc(strSthofFileName, strSthofText)
                    strSthofText = String.Empty
                End If
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While
            strWorkString = "Processing STHOF (OF) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthofFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolDoPrepareSthof = False
        End If

        If boolDoPrepareSthot = True Then ' Preparing STHOT
            intRecordsOutput = 0
            strWorkString = "Processing STHOT (OT) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse
            dateSelectionDate = SysDates.Today.Value
            If _RunningInNight = True Then
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
            End If
            If boolGotTobhotParameters = True Then
                _StartDate = dateTobhotStartDate
                _EndDate = dateTobhotEndDate
            End If
            dateSelectionDate = _StartDate

            While dateSelectionDate <= _EndDate
                Dltots.ClearLists()
                Dltots.ClearLoadField()
                Dltots.ClearLoadFilter()

                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, dateSelectionDate)
                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                ColDt = Dltots.LoadMatches

                For Each Record As BOSales.cSalesHeader In ColDt
                    strWorkString = "Checking Transaction : " & Record.TransDate.Value.Date.ToString & Space(1) & Record.TillID.Value.PadLeft(2, "0"c) & Space(1) & Record.TransactionNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    boolProcessThisTransaction = True
                    If boolGotTobhotParameters = True And intTobhotTypes > 0 Then
                        boolProcessThisTransaction = False
                        For intX1 = 1 To intTobhotTypes
                            If arrstrTobHotTypes(intX1) = Record.TransactionCode.Value Then
                                boolProcessThisTransaction = True
                                Exit For
                            End If
                        Next
                    End If
                    'Dim arrVatSymbols As String() = New String(9) {" ", Record.VatSymbol1.Value, Record.VatSymbol2.Value, Record.VatSymbol3.Value, Record.VatSymbol4.Value, Record.VatSymbol5.Value, Record.VatSymbol6.Value, Record.VatSymbol7.Value, Record.VatSymbol8.Value, Record.VatSymbol9.Value}
                    If boolProcessThisTransaction = True Then
                        dateHashDate = Record.TransDate.Value
                        decHashValue = CDec(Record.TransactionNo.Value)
                        strDltotsTillId = Record.TillID.Value.ToString.PadLeft(2, "0"c)
                        strDltotsTransactionNumber = Record.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                        strDltotsCashierNumber = Record.CashierID.Value.ToString.PadLeft(3, "0"c)
                        strDltotsTransactionTime = Record.TransactionTime.Value.ToString.PadLeft(6, "0"c)
                        strDltotsSupervisorNumber = Record.SupervisiorNo.Value.ToString.PadLeft(3, "0"c)
                        strDltotsTransactionCode = Record.TransactionCode.Value
                        strDltotsOpenDrawerReasonCode = "  "
                        If Record.TransactionCode.Value = "OD" Then
                            strDltotsOpenDrawerReasonCode = Record.OpenDrawCode.Value.ToString.PadLeft(2, " "c)
                        End If
                        If Record.TransactionCode.Value = "RF" Or Record.TransactionCode.Value = "M+" Or Record.TransactionCode.Value = "C+" Or Record.TransactionCode.Value = "M-" Or Record.TransactionCode.Value = "C-" Then
                            strDltotsOpenDrawerReasonCode = Record.ReasonCode.Value.ToString.PadLeft(2, " "c)
                        End If
                        strDltotsReasonDescription = Record.Description.Value.PadLeft(20, " "c)
                        strDltotsProjectSalesOrderNumber = Record.OrderNo.Value.ToString.PadLeft(6, "0"c)
                        strDltotsAccountSaleIndicator = "N"
                        If Record.AccountSale.Value = True Then
                            strDltotsAccountSaleIndicator = "Y"
                        End If
                        strDltotsTransactionWasVoided = "N"
                        If Record.Voided.Value = True Then
                            strDltotsTransactionWasVoided = "Y"
                        End If
                        strDltotsVoidSupervisorNumber = Record.VoidSupervisor.Value.ToString.PadLeft(3, "0"c)
                        strDltotsTrainingTransaction = "N"
                        If Record.TrainingMode.Value = True Then
                            strDltotsTrainingTransaction = "Y"
                        End If
                        strDltotsTransactionWasProcessedByRSBUPD = "N"
                        If Record.DailyUpdateProc.Value = True Then
                            strDltotsTransactionWasProcessedByRSBUPD = "Y"
                        End If

                        strDltotsExternalDocumentNumber = Record.ExternalDocumentNo.Value.ToString.PadRight(8, " "c)
                        strDltotsSupervisorWasUsed = "N"
                        If Record.SupervisiorUsed.Value = True Then
                            strDltotsSupervisorWasUsed = "Y"
                        End If
                        strDltotsStoreNumber = Record.StoreNo.Value.ToString.PadLeft(3, "0"c)
                        FormatDecToString(Record.MerchandiseAmount.Value, strDltotsMerchandisingValue, 9, " "c, "0.00")
                        FormatDecToString(Record.NonMerchandiseAmount.Value, strDltotsNonMerchandisingValue, 9, " "c, "0.00")
                        FormatDecToString(Record.TaxAmount.Value, strDltotsTaxAmount, 9, " "c, "0.00")
                        FormatDecToString(Record.DicountAmount.Value, strDltotsDiscountAmount, 9, " "c, "0.00")
                        strDltotsDiscountSupervisorNumber = Record.DiscountSupervisor.Value.ToString.PadLeft(3, "0"c)
                        FormatDecToString(Record.TotalSalesAmount.Value, strDltotsTotalValue, 9, " "c, "0.00")
                        strDltotsCustomerAccountNumber = Record.CustomerAcctNo.Value.ToString.PadLeft(6, "0"c)
                        strDltotsCustomerAccountCardHolderNumber = Record.CustomerAcctCardNo.Value.ToString.PadLeft(2, "0"c).Substring(0, 2)
                        strDltotsAccountUpdateIsComplete = "N"
                        If Record.AccountUpdComplete.Value = True Then
                            strDltotsAccountUpdateIsComplete = "Y"
                        End If
                        strDltotsTransactionIsComplete = "N"
                        If Record.TransactionComplete.Value = True Then
                            strDltotsTransactionIsComplete = "Y"
                        End If
                        strDltotsEmployeeDiscountOnly = "N"
                        If Record.EmployeeDiscountOnly.Value = True Then
                            strDltotsEmployeeDiscountOnly = "Y"
                        End If

                        strDltotsRefundCashierNumber = Record.RefundCashier.Value.ToString.PadLeft(3, "0"c)
                        strDltotsRefundSupervisorrNumber = Record.RefundSupervisor.Value.ToString.PadLeft(3, "0"c)

                        strStockAdjustmentSign = "+"
                        strDltotsVatRate1 = Record.VatRate1.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol1 = Record.VatSymbol1.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue1.Value, strDltotsVatExclusiveValue1, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue1.Value, strDltotsVatValue1, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate2 = Record.VatRate2.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol2 = Record.VatSymbol2.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue2.Value, strDltotsVatExclusiveValue2, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue2.Value, strDltotsVatValue2, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate3 = Record.VatRate3.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol3 = Record.VatSymbol3.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue3.Value, strDltotsVatExclusiveValue3, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue3.Value, strDltotsVatValue3, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate4 = Record.VatRate4.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol4 = Record.VatSymbol4.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue4.Value, strDltotsVatExclusiveValue4, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue4.Value, strDltotsVatValue4, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate5 = Record.VatRate5.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol5 = Record.VatSymbol5.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue5.Value, strDltotsVatExclusiveValue5, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue5.Value, strDltotsVatValue5, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate6 = Record.VatRate6.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol6 = Record.VatSymbol6.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue6.Value, strDltotsVatExclusiveValue6, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue6.Value, strDltotsVatValue6, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate7 = Record.VatRate7.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol7 = Record.VatSymbol7.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue7.Value, strDltotsVatExclusiveValue7, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue7.Value, strDltotsVatValue7, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate8 = Record.VatRate8.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol8 = Record.VatSymbol8.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue8.Value, strDltotsVatExclusiveValue8, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue8.Value, strDltotsVatValue8, 9, " "c, "0.00")
                        strStockAdjustmentSign = "+"
                        strDltotsVatRate9 = Record.VatRate9.Value.ToString("#0.000").PadLeft(6, " "c) & strStockAdjustmentSign
                        strDltotsVatSymbol9 = Record.VatSymbol9.Value.PadLeft(1, " "c)
                        FormatDecToString(Record.ExVATValue9.Value, strDltotsVatExclusiveValue9, 9, " "c, "0.00")
                        FormatDecToString(Record.VATValue9.Value, strDltotsVatValue9, 9, " "c, "0.00")
                        strDltotsTransactionIsParked = "N"
                        If Record.TransParked.Value = True Then
                            strDltotsTransactionIsParked = "Y"
                        End If
                        strDltotsColleagueCardNumber = Trim(Record.CardNumber.Value.ToString).PadLeft(10, "0"c).Substring(1, 9)
                        strDltotsRefundManagerNumber = Record.RefundManager.Value.ToString.PadLeft(3, "0"c)
                        'strDltotsRefundManagerNumber = "000" ' Problem with RMAN returning BOOLEAN value
                        strDltotsTenderOverrideCode = Record.TenderOverideCode.Value.ToString.PadLeft(2, "0"c)
                        strDltotsRecoveredFromParkedTransaction = "N"
                        If Record.RecoveredFromParked.Value = True Then
                            strDltotsRecoveredFromParkedTransaction = "Y"
                        End If
                        strDltotsTransactionWasOffline = "Y"
                        If Record.Offline.Value = False Then strDltotsTransactionWasOffline = "N"

                        FormatIntToString(CInt(Record.GiftTokensPrinted.Value), strDltotsNumberOfGiftTokensPrinted, 2, " "c)
                        strDltotsCurrentSaveStatus = Record.SaveStatus.Value.PadLeft(2, "0"c)
                        strDltotsCurrentSaveStatusSequenceNumber = Record.SaveSequenceNo.Value.PadLeft(4, "0"c)
                        strDltotsDateUpdatedByCashBalanceUpdateProgram = Record.CashBalUpdated.Value.Trim
                        If strDltotsDateUpdatedByCashBalanceUpdateProgram = "" Then strDltotsDateUpdatedByCashBalanceUpdateProgram = "--/--/--"
                        strDltotsColleagueAccountCardHolderNumber = Record.CardNumber.Value.ToString.PadRight(19, " "c)

                        'strDltotsCurrentSaveStatus = Record.rrDltotsSsta(y).ToString.PadLeft(2, "0")
                        'strDltotsCurrentSaveStatusSequenceNumber = arrDltotsSeqn(y).ToString.PadLeft(4, "0")
                        'strDltotsDateUpdatedByCashBalanceUpdateProgram = String.Format(arrDltotsCbbu(y), "dd/MM/yy")
                        'strDltotsCustomerAccountCardHolderNumber= arrDltotsCardno(y)
                        'DTRtic = arrDltotsRtic(y

                        SetupHash("DT", decHashValue, dateHashDate)
                        If strSthotText <> String.Empty Then
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strDltotsTillId & strDltotsTransactionNumber & _
                            strDltotsCashierNumber & strDltotsTransactionTime & strDltotsSupervisorNumber & strDltotsTransactionCode & strDltotsOpenDrawerReasonCode & strDltotsReasonDescription & _
                            strDltotsProjectSalesOrderNumber & strDltotsAccountSaleIndicator & strDltotsTransactionWasVoided & strDltotsVoidSupervisorNumber & strDltotsTrainingTransaction & _
                            strDltotsTransactionWasProcessedByRSBUPD & strDltotsExternalDocumentNumber & strDltotsSupervisorWasUsed & strDltotsStoreNumber & strDltotsMerchandisingValue & _
                            strDltotsNonMerchandisingValue & strDltotsTaxAmount & strDltotsDiscountAmount & strDltotsDiscountSupervisorNumber & strDltotsTotalValue & strDltotsCustomerAccountNumber & _
                            strDltotsCustomerAccountCardHolderNumber & strDltotsAccountUpdateIsComplete & strDltotsTransactionIsComplete & strDltotsEmployeeDiscountOnly & strDltotsRefundCashierNumber & _
                            strDltotsRefundSupervisorrNumber & strDltotsVatRate1 & strDltotsVatSymbol1 & strDltotsVatExclusiveValue1 & strDltotsVatValue1 & strDltotsVatRate2 & strDltotsVatSymbol2 & _
                            strDltotsVatExclusiveValue2 & strDltotsVatValue2 & strDltotsVatRate3 & strDltotsVatSymbol3 & strDltotsVatExclusiveValue3 & strDltotsVatValue3 & strDltotsVatRate4 & strDltotsVatSymbol4 & _
                            strDltotsVatExclusiveValue4 & strDltotsVatValue4 & strDltotsVatRate5 & strDltotsVatSymbol5 & strDltotsVatExclusiveValue5 & strDltotsVatValue5 & strDltotsVatRate6 & strDltotsVatSymbol6 & _
                            strDltotsVatExclusiveValue6 & strDltotsVatValue6 & strDltotsVatRate7 & strDltotsVatSymbol7 & strDltotsVatExclusiveValue7 & strDltotsVatValue7 & strDltotsVatRate8 & strDltotsVatSymbol8 & _
                            strDltotsVatExclusiveValue8 & strDltotsVatValue8 & strDltotsVatRate9 & strDltotsVatSymbol9 & strDltotsVatExclusiveValue9 & strDltotsVatValue9 & strDltotsTransactionIsParked & _
                            strDltotsColleagueCardNumber & strDltotsRefundManagerNumber & strDltotsTenderOverrideCode & strDltotsRecoveredFromParkedTransaction & strDltotsTransactionWasOffline & _
                            strDltotsNumberOfGiftTokensPrinted & strDltotsCurrentSaveStatus & strDltotsCurrentSaveStatusSequenceNumber & strDltotsDateUpdatedByCashBalanceUpdateProgram & strDltotsColleagueAccountCardHolderNumber & "*"
                        intRecordsOutput = intRecordsOutput + 1
                        If strSthotText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthotFileName, strSthotText)
                            strSthotText = String.Empty
                        End If
                        Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, Record.TransDate.Value)
                        Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TillID, Record.TillID.Value)
                        Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransNo, Record.TransactionNo.Value)
                        Dlline.SortBy(Dlline.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlline.SortBy(Dlline.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlline.SortBy(Dlline.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlline.SortBy(Dlline.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        ColDl = Dlline.LoadMatches()
                        For Each detail As BOSales.cSalesLine In ColDl
                            dateHashDate = detail.TransDate.Value
                            decHashValue = CDec(detail.TransNo.Value)
                            strDllineTillId = detail.TillID.Value.ToString.PadLeft(2, "0"c)
                            strDllineTransactionNumber = detail.TransNo.Value.ToString.PadLeft(4, "0"c)
                            strDllineTransactionintLineNumber = detail.SequenceNo.Value.ToString.PadLeft(4, " "c)
                            strDllineSkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                            strDllineDepartmentNumber = detail.DepartmentNo.Value.ToString.PadLeft(2, "0"c)
                            strDllineInputWasByBarcode = "N"
                            If detail.BarCodeEntry.Value = True Then
                                strDllineInputWasByBarcode = "Y"
                            End If
                            strDllineSupervisorNumber = detail.SupervisorNo.Value.ToString.PadRight(3, "0"c)
                            FormatIntToString(CInt(detail.QuantitySold.Value), strDllineQuantitySold, 6, " "c)
                            strStockAdjustmentSign = "+"
                            decWorkValue = detail.LoopUpPrice.Value
                            If decWorkValue < +0 Then
                                decWorkValue = decWorkValue * -1
                            End If
                            strDllineSystemLookupPrice = decWorkValue.ToString("#####0.00").PadLeft(9, " "c) & strStockAdjustmentSign
                            strStockAdjustmentSign = "+"
                            decWorkValue = detail.ActualSellPrice.Value
                            If decWorkValue < +0 Then
                                decWorkValue = decWorkValue * -1
                            End If
                            strDllineItemSellingPrice = decWorkValue.ToString("#####0.00").PadLeft(9, " "c) & strStockAdjustmentSign
                            strStockAdjustmentSign = "+"
                            decWorkValue = detail.ActualPriceExVat.Value
                            If decWorkValue < +0 Then
                                decWorkValue = decWorkValue * -1
                            End If
                            strDllineItemSellingPriceExclusiveOfVat = decWorkValue.ToString("#####0.00").PadLeft(9, " "c) & strStockAdjustmentSign
                            FormatDecToString(detail.ExtendedValue.Value, strDllineExtendedValue, 9, " "c, "0.00")
                            FormatDecToString(detail.ExtendedCost.Value, strDllineExtendedCost, 10, " "c, "0.000")
                            strDllineItemIsRelatedSingle = "N"
                            If detail.RelatedItems.Value = True Then
                                strDllineItemIsRelatedSingle = "Y"
                            End If
                            strDllinePriceOverrideReasonCode = detail.PriceOverrideReason.Value.ToString.PadLeft(2, " "c)
                            strDllinePriceOverrideReasonText = String.Format(" ").PadRight(20, " "c)
                            If detail.PriceOverrideReason.Value > 0 And detail.PriceOverrideReason.Value < 11 Then
                                strDllinePriceOverrideReasonText = arrOverrideReasonCodes(detail.PriceOverrideReason.Value).PadRight(20, " "c)
                            End If
                            strDllineTaggedItem = "N"
                            If detail.ItemTagged.Value = True Then
                                strDllineTaggedItem = "Y"
                            End If
                            strDllineCatchAllItem = "N"
                            If detail.CatchAllItem.Value = True Then
                                strDllineCatchAllItem = "Y"
                            End If
                            strDllineVatSymbol = detail.VatCode.Value.ToString.PadLeft(1, " "c)
                            FormatDecToString(detail.TempPriceMarginAmt.Value, strDllineTemporaryPriceChangePriceDifference, 9, " "c, "0.00")
                            strDllineTemporaryPriceChangeMarginErosionCode = detail.TempPriceMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.PriceOverrideAmount.Value, strDllinePriceOverridePriceDifference, 9, " "c, "0.00")
                            strDllinePriceOverrideMarginErosionCode = detail.OverrideMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.QtyBreakMarginAmount.Value, strDllineQuantityBreakErosionValue, 9, " "c, "0.00")
                            strDllineQuantityBreakMarginErosionCode = detail.QtyBreakMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.DealGroupMarginAmt.Value, strDllineDealGroupErosionValue, 9, " "c, "0.00")
                            strDllineDealGroupMarginErosionCode = detail.DealGroupMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.MultiBuyMarginAmount.Value, strDllineMultiBuyErosionValue, 9, " "c, "0.00")
                            strDllineMultiBuyMarginErosionCode = detail.MultiBuyMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.HierarchyMarginAmt.Value, strDllineHierarchySpendErosionValue, 9, " "c, "0.00")
                            strDllineHierarchySpendMarginErosionCode = detail.HierarchyMarginCode.Value.ToString.PadLeft(6, "0"c)
                            FormatDecToString(detail.EmpSalePriMarginAmt.Value, strDllinePrimaryEmployeeDiscountErosionValue, 9, " "c, "0.00")
                            strDllinePrimaryEmployeeDiscountMarginErosionCode = detail.EmpSalePriMarginCode.Value.ToString.PadLeft(6, "0"c)
                            strDllineLineWasReversed = "N"
                            If detail.LineReversed.Value = True Then
                                strDllineLineWasReversed = "Y"
                            End If
                            strDllineLastEventSequenceNumberUsedForThisLine = detail.LastEventSequenceNo.Value.ToString.PadLeft(6, "0"c)
                            strDllineHierarchyCategoryNumber = detail.HierCategory.Value.ToString.PadLeft(6, "0"c)
                            strDllineHierarchyGroupNumber = detail.HierGroup.Value.ToString.PadLeft(6, "0"c)
                            strDllineHierarchySubGroupNumber = detail.HierSubGroup.Value.ToString.PadLeft(6, "0"c)
                            strDllineHierarchyStyleNumber = detail.HierStyle.Value.ToString.PadLeft(6, "0"c)
                            strDllineQuarantineSupervisorNumber = detail.QuarantineSupervisor.Value.ToString.PadLeft(3, "0"c)
                            FormatDecToString(detail.EmpSaleSecMarginAmt.Value, strDllineSecondaryEmployeeDiscountMarginErosionValue, 9, " "c, "0.00")
                            strDllineSoldFromMarkdownStock = "N"
                            If detail.MarkDownStock.Value = True Then
                                strDllineSoldFromMarkdownStock = "Y"
                            End If
                            strDllineSaleTypeAttribute = detail.SaleType.Value.ToString.PadLeft(1, " "c)
                            strDllineVatCodeNumber = CInt(detail.VatCodeNo.Value).ToString.PadLeft(1, "0"c)
                            FormatDecToString(detail.VatAmount.Value, strDllineVatValueForThisLine, 11, " "c, "0.00")
                            DDBdco = detail.BackDoorCollect.Value.ToString.PadLeft(1, " "c)
                            DDBdcoInd = CStr(IIf(detail.BackDoorCollected.Value, "Y", "N"))
                            DDRcod = detail.LineReverseCode.Value.ToString.PadLeft(2, "0"c)

                            SetupHash("DD", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strDllineTillId & strDllineTransactionNumber & strDllineTransactionintLineNumber & strDllineSkuNumber & strDllineDepartmentNumber & strDllineInputWasByBarcode & strDllineSupervisorNumber & strDllineQuantitySold & strDllineSystemLookupPrice & strDllineItemSellingPrice & strDllineItemSellingPriceExclusiveOfVat & strDllineExtendedValue & strDllineExtendedCost & strDllineItemIsRelatedSingle & strDllinePriceOverrideReasonCode & strDllinePriceOverrideReasonText & strDllineTaggedItem & strDllineCatchAllItem & strDllineVatSymbol & strDllineTemporaryPriceChangePriceDifference & strDllineTemporaryPriceChangeMarginErosionCode & strDllinePriceOverridePriceDifference & strDllinePriceOverrideMarginErosionCode & strDllineQuantityBreakErosionValue & strDllineQuantityBreakMarginErosionCode & strDllineDealGroupErosionValue & strDllineDealGroupMarginErosionCode & strDllineMultiBuyErosionValue & strDllineMultiBuyMarginErosionCode & strDllineHierarchySpendErosionValue & strDllineHierarchySpendMarginErosionCode & strDllinePrimaryEmployeeDiscountErosionValue & strDllinePrimaryEmployeeDiscountMarginErosionCode & strDllineLineWasReversed & strDllineLastEventSequenceNumberUsedForThisLine & strDllineHierarchyCategoryNumber & strDllineHierarchyGroupNumber & strDllineHierarchySubGroupNumber & strDllineHierarchyStyleNumber & strDllineQuarantineSupervisorNumber & strDllineSecondaryEmployeeDiscountMarginErosionValue & strDllineSoldFromMarkdownStock & strDllineSaleTypeAttribute & strDllineVatCodeNumber & strDllineVatValueForThisLine & DDBdco & DDBdcoInd & DDRcod
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next

                        Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TransDate, Record.TransDate.Value)
                        Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TillID, Record.TillID.Value)
                        Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TransNo, Record.TransactionNo.Value)
                        Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Dlpaid.TenderType, 1)
                        Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, Dlpaid.TenderType, 99)
                        Dlpaid.SortBy(Dlpaid.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlpaid.SortBy(Dlpaid.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlpaid.SortBy(Dlpaid.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlpaid.SortBy(Dlpaid.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        ColDp = Dlpaid.LoadMatches

                        For Each payment As BOSales.cSalesPaid In ColDp

                            dateHashDate = payment.TransDate.Value
                            decHashValue = CDec(payment.TransNo.Value)
                            strDlpaidTillId = payment.TillID.Value.ToString.PadLeft(2, "0"c)
                            strDlpaidTransactionNumber = payment.TransNo.Value.ToString.PadLeft(4, "0"c)
                            strDlpaidTransactionSequenceNumber = payment.SequenceNo.Value.ToString.PadLeft(4, " "c)

                            intWorkInteger = CInt(payment.TenderType.Value)
                            strDlpaidTenderTypeNumber = intWorkInteger.ToString.PadLeft(2, " "c)
                            strDlpaidTenderTypeDescription = String.Format(" ").PadRight(12, " "c)
                            If intWorkInteger > 0 And intWorkInteger < 11 Then
                                strDlpaidTenderTypeDescription = arrTenderTypeFlagDescriptions(intWorkInteger).PadRight(12, " "c)
                            End If
                            FormatDecToString(payment.TenderAmount.Value, strDlpaidTenderTypeAmount, 9, " "c, "0.00")
                            strDlpaidCreditCardNumber = payment.CreditCardNo.Value.ToString.PadLeft(19, "0"c)
                            strDlpaidCreditCardExpiryDate = payment.CreditCardExpiryDate.Value.ToString.PadLeft(4, "0"c)
                            strDlpaidCouponNumber = payment.CouponNo.Value.ToString.PadLeft(6, "0"c)
                            strDlpaidCouponClass = "00"
                            strDlpaidAuthorisationCode = payment.AuthorisationCode.Value.ToString.PadRight(9, " "c)
                            strDlpaidCreditCardWasKeyed = "N"
                            If payment.CCNumberKeyed.Value = True Then
                                strDlpaidCreditCardWasKeyed = "Y"
                            End If
                            strDlpaidPostCodeForCoupon = payment.CouponPostCode.Value.ToString.PadLeft(8, " "c)
                            strDlpaidChequeAccountNumber = payment.ChequeAccountNo.Value.ToString.PadLeft(10, "0"c)
                            strDlpaidChequeSortCode = payment.ChequeSortCode.Value.ToString.PadLeft(6, "0"c)
                            strDlpaidChequeNumber = payment.ChequeNo.Value.ToString.PadLeft(6, "0"c)
                            strDlpaidEftposVoucherSequenceNumber = payment.EftposVoucherNo.Value.ToString.PadLeft(4, "0"c)
                            strDlpaidMaestroCardIssueNumber = payment.IssueNo.Value.ToString.PadLeft(2, "0"c)
                            strDlpaidAuthorisationType = payment.AutorisationType.Value.ToString.PadLeft(1, " "c)
                            strDlpaidEftposMerchantNumber = payment.EftposMerchantNo.Value.ToString.PadRight(15, " "c)
                            FormatDecToString(payment.CashBackAmount.Value, strDlpaidCashBackAmount, 9, " "c, "0.00")
                            strDlpaidCardNumberDigitCount = payment.DigitCount.Value.ToString.PadLeft(2, "0"c)
                            strDlpaidEftposCommsFileHasBeenPrepared = "N"
                            If payment.EftposCommsPrep.Value = True Then
                                strDlpaidEftposCommsFileHasBeenPrepared = "Y"
                            End If
                            strDlpaidCardCustomerWasPresent = "Y"
                            'strDlpaidCardCustomerWasPresent = arrDlpaidIccp(a).ToString.PadLeft(1, " ")
                            strDlpaidCardDescription = payment.CardDescription.Value.ToString.PadRight(30, " "c)
                            'strDlpaidEftposTransactionID = payment.EarrDlpaidEfid(a).ToString.PadRight(6, " ")
                            'strDlpaidEftposPaymentCollected = arrDlpaidEftc(a).ToString.PadRight(1, " ")
                            'DPStdt = payment.sarrDlpaidStdt(a).ToString.PadRight(4, " ")
                            'strDlpaidAuthorisationCodeDesc = arrDlpaidAuthDesc(a).ToString.PadRight(20, " ")
                            'DPSecCode = arrDlpaidSeccode(a).ToString.PadRight(3, " ")
                            'DPTenc = arrDlpaidTenc(a).ToString.PadLeft(1, " ")
                            'DPMpow = arrDlpaidMpow(a).ToString.PadLeft(2, " ")
                            'DPMrat = arrDlpaidMrat(a).ToString.PadLeft(2, " ")
                            'DPTenv = arrDlpaidTenv(a).ToString.PadLeft(2, " ")
                            strDlpaidEftposTransactionID = "000000"
                            strDlpaidEftposPaymentCollected = " "

                            ' The following fields are set to default values as they are no 
                            ' longer used but are required in the transmission file
                            DPStdt = "0000"
                            strDlpaidAuthorisationCodeDesc = String.Format(" ").PadRight(20, " "c)
                            DPSecCode = "   "
                            DPTenc = String.Format(" ")
                            DPMpow = " 0"
                            DPMrat = " 0"
                            DPTenv = " 0"

                            SetupHash("DP", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strDlpaidTillId & strDlpaidTransactionNumber & strDlpaidTransactionSequenceNumber & strDlpaidTenderTypeNumber & strDlpaidTenderTypeDescription & strDlpaidTenderTypeAmount & strDlpaidCreditCardNumber & strDlpaidCreditCardExpiryDate & strDlpaidCouponNumber & strDlpaidCouponClass & strDlpaidAuthorisationCode & strDlpaidCreditCardWasKeyed & strDlpaidPostCodeForCoupon & strDlpaidChequeAccountNumber & strDlpaidChequeSortCode & strDlpaidChequeNumber & strDlpaidEftposVoucherSequenceNumber & strDlpaidMaestroCardIssueNumber & strDlpaidAuthorisationType & strDlpaidEftposMerchantNumber & strDlpaidCashBackAmount & strDlpaidCardNumberDigitCount & strDlpaidEftposCommsFileHasBeenPrepared & strDlpaidCardCustomerWasPresent & strDlpaidCardDescription & strDlpaidEftposTransactionID & strDlpaidEftposPaymentCollected & DPStdt & strDlpaidAuthorisationCodeDesc & DPSecCode & DPMpow & DPMrat & DPTenv
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next
                        Dim Dlrcus As New BOSales.cSalesCustomers(_Oasys3DB)
                        Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TransactionDate, Record.TransDate.Value)
                        Dlrcus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TillID, Record.TillID.Value)
                        Dlrcus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TransactionNo, Record.TransactionNo.Value)
                        Dlrcus.SortBy(Dlrcus.TransactionDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlrcus.SortBy(Dlrcus.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlrcus.SortBy(Dlrcus.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlrcus.SortBy(Dlrcus.LineNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        Dim ColDr As List(Of BOSales.cSalesCustomers) = Dlrcus.LoadMatches()
                        'ColDr.Reverse()
                        For Each customer As BOSales.cSalesCustomers In ColDr
                            dateHashDate = customer.TransactionDate.Value
                            decHashValue = CDec(customer.TransactionNo.Value)
                            strSthotDRTillNumber = customer.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDRTransactionNumber = customer.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDRCustomerName = customer.CustomerName.Value.ToString.PadRight(30, " "c)
                            strSthotDRPostCode = customer.PostCode.Value.ToString.PadRight(8, " "c)
                            strSthotDROriginalTransactionStoreNumber = customer.OrigTransStore.Value.ToString.PadLeft(3, "0"c)
                            strSthotDROriginalTransactionDate = customer.OrigTransDate.Value.ToString("dd/MM/yy")
                            If strSthotDROriginalTransactionDate = "01/01/01" Then strSthotDROriginalTransactionDate = "--/--/--"
                            strSthotDROriginalTransactionTillNumber = customer.OrigTranTil.Value.ToString.PadLeft(2, "0"c)
                            strSthotDROriginalTransactionNumber = customer.OrigTranNumb.Value.ToString.PadLeft(4, "0"c)
                            strSthotDRHouseName = customer.HouseName.Value.ToString.PadRight(15, " "c)
                            strSthotDRAddressLine1 = customer.AddressLine1.Value.ToString.PadRight(30, " "c)
                            strSthotDRAddressLine2 = customer.AddressLine2.Value.ToString.PadRight(30, " "c)
                            strSthotDRAddressLine3 = customer.AddressLine3.Value.ToString.PadRight(30, " "c)
                            strSthotDRPhoneNumber = customer.PhoneNo.Value.ToString.PadRight(15, " "c)
                            strSthotDRintLineNumber = customer.LineNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDROriginalTransactionValidated = "Y"
                            If customer.OrigTranValidated.Value = False Then
                                strSthotDROriginalTransactionValidated = "N"
                            End If
                            strSthotDROriginalTransactionTenderType = customer.OrigTenType.Value.ToString.PadLeft(2, " "c)
                            strSthotDRRefundReasonCode = customer.RefundReason.Value.ToString.PadLeft(2, "0"c)
                            strSthotDRLabelRequiredIndicator = "N"
                            If customer.LabelsRequired.Value = True Then
                                strSthotDRLabelRequiredIndicator = "Y"
                            End If

                            strSthotDRMobilePhoneNumber = customer.MobilePhoneNo.Value.ToString.PadRight(15, " "c)
                            SetupHash("DR", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDRTillNumber & strSthotDRTransactionNumber & strSthotDRCustomerName & strSthotDRPostCode & strSthotDROriginalTransactionStoreNumber & strSthotDROriginalTransactionDate & strSthotDROriginalTransactionTillNumber & strSthotDROriginalTransactionNumber & strSthotDRHouseName & strSthotDRAddressLine1 & strSthotDRAddressLine2 & strSthotDRAddressLine3 & strSthotDRPhoneNumber & strSthotDRintLineNumber & strSthotDROriginalTransactionValidated & strSthotDROriginalTransactionTenderType & strSthotDRRefundReasonCode & strSthotDRLabelRequiredIndicator & strSthotDRMobilePhoneNumber
                            intRecordsOutput = intRecordsOutput + 1
                            '    OutPutStho() trDlpaidTenderTypeDescription & strDlpaidTenderTypeAmount & strDlpaidCreditCardNumber & strDlpaidCreditCardExpiryDate & strDlpaidCouponNumber & strDlpaidCouponClass & strDlpaidAuthorisationCode & strDlpaidCreditCardWasKeyed & strDlpaidPostCodeForCoupon & strDlpaidChequeAccountNumber & strDlpaidChequeSortCode & strDlpaidChequeNumber & strDlpaidEftposVoucherSequenceNumber & strDlpaidMaestroCardIssueNumber & strDlpaidAuthorisationType & strDlpaidEftposMerchantNumber & strDlpaidCashBackAmount & strDlpaidCardNumberDigitCount & strDlpaidEftposCommsFileHasBeenPrepared & strDlpaidCardCustomerWasPresent & strDlpaidCardDescription & strDlpaidEftposTransactionID & strDlpaidEftposPaymentCollected & DPStdt & strDlpaidAuthorisationCodeDesc & DPSecCode & DPMpow & DPMrat & DPTenv
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next

                        Dim Dlevnt As New BOSales.cSalesTransactionEvent(_Oasys3DB)
                        Dlevnt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlevnt.TranDate, Record.TransDate.Value)
                        Dlevnt.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlevnt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlevnt.TillID, Record.TillID.Value)
                        Dlevnt.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlevnt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlevnt.TransactionNo, Record.TransactionNo.Value)
                        Dlevnt.SortBy(Dlevnt.TranDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlevnt.SortBy(Dlevnt.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlevnt.SortBy(Dlevnt.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlevnt.SortBy(Dlevnt.TransactionLineNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dim ColDv As List(Of BOSales.cSalesTransactionEvent) = Dlevnt.LoadMatches()
                        'ColDr.Reverse()
                        For Each tevent As BOSales.cSalesTransactionEvent In ColDv
                            dateHashDate = tevent.TranDate.Value
                            decHashValue = CDec(tevent.TransactionNo.Value)
                            strSthotDVTillNumber = tevent.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDVTransactionNumber = tevent.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDVTransactionintLineNumber = tevent.TransactionLineNo.Value.ToString.PadLeft(4, " "c)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, Record.TransDate.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TillID, Record.TillID.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransNo, Record.TransactionNo.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.SequenceNo, tevent.TransactionLineNo.Value)
                            Dim ColDs As List(Of BOSales.cSalesLine) = Dlline.LoadMatches()
                            If ColDs.Count > 0 Then
                                For Each skuline As BOSales.cSalesLine In ColDs
                                    If skuline.SkuNumber.Value <> String.Empty Then
                                        strSthotDVSkuNumber = skuline.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                                        Exit For
                                    End If
                                Next
                            End If
                            strSthotDVEventSequenceNumber = tevent.EventSeqNo.Value.ToString.PadLeft(6, "0"c)
                            strSthotDVEventType = tevent.EventType.Value.ToString.PadRight(2, " "c)
                            FormatDecToString(tevent.DiscountAmount.Value, strSthotDVEventAmount, 9, " "c, "0.00")
                            SetupHash("DV", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDVTillNumber & strSthotDVTransactionNumber & strSthotDVTransactionintLineNumber & strSthotDVSkuNumber & strSthotDVEventSequenceNumber & strSthotDVEventType & strSthotDVEventAmount
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next
                        Dim Dlanas As New BOSales.cSalesAnasNotRecognised(_Oasys3DB)
                        Dlanas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlanas.TranDate, Record.TransDate.Value)
                        Dlanas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlanas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlanas.TillID, Record.TillID.Value)
                        Dlanas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlanas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlanas.TransactionNo, Record.TransactionNo.Value)
                        Dlanas.SortBy(Dlanas.TranDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlanas.SortBy(Dlanas.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlanas.SortBy(Dlanas.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlanas.SortBy(Dlanas.TransactionLineNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlanas.SortBy(Dlanas.LineSequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        Dim ColDa As List(Of BOSales.cSalesAnasNotRecognised) = Dlanas.LoadMatches()
                        'ColDr.Reverse()
                        For Each ana As BOSales.cSalesAnasNotRecognised In ColDa
                            dateHashDate = ana.TranDate.Value
                            decHashValue = CDec(ana.TransactionNo.Value)
                            strSthotDATillNumber = ana.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDATransactionNumber = ana.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDATransactionintLineNumber = ana.TransactionLineNo.Value.ToString.PadLeft(4, " "c)
                            strSthotDATransactionLineSequenceNumber = ana.LineSequenceNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDAEanNumberScanned = ana.EanNumberScanned.Value.ToString.PadRight(16, "0"c)

                            SetupHash("DA", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDATillNumber & strSthotDATransactionNumber & strSthotDATransactionintLineNumber & strSthotDATransactionLineSequenceNumber & strSthotDAEanNumberScanned
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next
                        Dim Dlgift As New BOSales.cSalesGiftTokenLines(_Oasys3DB)
                        Dlgift.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlgift.TranDate, Record.TransDate.Value)
                        Dlgift.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlgift.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlgift.TillID, Record.TillID.Value)
                        Dlgift.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlgift.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlgift.TransactionNo, Record.TransactionNo.Value)
                        Dlgift.SortBy(Dlgift.TranDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlgift.SortBy(Dlgift.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlgift.SortBy(Dlgift.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlgift.SortBy(Dlgift.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlgift.SortBy(Dlgift.SerialNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        Dim ColDg As List(Of BOSales.cSalesGiftTokenLines) = Dlgift.LoadMatches()
                        'ColDr.Reverse()
                        For Each gifttoken As BOSales.cSalesGiftTokenLines In ColDg
                            dateHashDate = gifttoken.TranDate.Value
                            decHashValue = CDec(gifttoken.TransactionNo.Value)
                            strSthotDXTillNumber = gifttoken.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDXTransactionNumber = gifttoken.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDXTransactionSequenceNumber = gifttoken.SequenceNo.Value.ToString.PadLeft(5, " "c)
                            strSthotDXSerialNumber = gifttoken.SerialNo.Value.ToString.PadLeft(8, "0"c)
                            strSthotDXTransactionType = gifttoken.TransactionCode.Value.ToString.PadLeft(2, " "c)
                            strSthotDXEmployeeId = gifttoken.CashierNo.Value.ToString.PadLeft(3, "0"c)
                            FormatDecToString(gifttoken.VoucherAmount.Value, strSthotDXGiftVoucherValue, 9, " "c, "0.00")

                            SetupHash("DX", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDXTillNumber & strSthotDXTransactionNumber & strSthotDXTransactionSequenceNumber & strSthotDXSerialNumber & strSthotDXEmployeeId & strSthotDXTransactionType & strSthotDXGiftVoucherValue
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                            Try
                                gifttoken.CommedToHO.Value = True
                                gifttoken.SaveIfExists()
                            Catch ex As Exception
                            End Try

                        Next
                        Dim Dlolin As New BOSales.cPromiseLineItem(_Oasys3DB)
                        Dlolin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlolin.TransactionDate, Record.TransDate.Value)
                        Dlolin.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlolin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlolin.TillID, Record.TillID.Value)
                        Dlolin.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlolin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlolin.TransactionNo, Record.TransactionNo.Value)
                        Dlolin.SortBy(Dlolin.TransactionDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlolin.SortBy(Dlolin.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlolin.SortBy(Dlolin.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlolin.SortBy(Dlolin.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        Dim ColDO As List(Of BOSales.cPromiseLineItem) = Dlolin.LoadMatches()
                        'ColDr.Reverse()
                        For Each pricematchline As BOSales.cPromiseLineItem In ColDO
                            dateHashDate = pricematchline.TransactionDate.Value
                            decHashValue = CDec(pricematchline.TransactionNo.Value)
                            strSthotDOTillNumber = pricematchline.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDOTransactionNumber = pricematchline.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDOTransactionSequenceNumber = pricematchline.SequenceNo.Value.ToString.PadLeft(4, " "c)
                            strSthotDOCompetitorName = pricematchline.CompetitorName.Value.ToString.PadRight(30, " "c)
                            strSthotDOCompetitorAddressLine1 = pricematchline.CompetitorAddressLine1.Value.ToString.PadRight(30, " "c)
                            strSthotDOCompetitorAddressLine2 = pricematchline.CompetitorAddressLine2.Value.ToString.PadRight(30, " "c)
                            strSthotDOCompetitorAddressLine3 = pricematchline.CompetitorAddressLine3.Value.ToString.PadRight(30, " "c)
                            strSthotDOCompetitorAddressLine4 = pricematchline.CompetitorAddressLine4.Value.ToString.PadRight(30, " "c)
                            strSthotDOCompetitorPostCode = pricematchline.CompetitorPostCode.Value.ToString.PadRight(8, " "c)
                            strSthotDOCompetitorPhoneNumber = pricematchline.CompetitorPhoneNo.Value.ToString.PadRight(15, " "c)
                            strSthotDOCompetitorFaxNumber = pricematchline.CompetitorFaxNo.Value.ToString.PadRight(15, " "c)
                            strSthotDOCompetitorPrice = pricematchline.CompetitorPrice.Value.ToString("0.00").PadLeft(8, " "c)
                            strSthotDoVatInclusivePrices = "Y"
                            If pricematchline.VatInclusivePrices.Value = False Then
                                strSthotDoVatInclusivePrices = "N"
                            End If
                            strSthotDOPreviousPurchase = "Y"
                            If pricematchline.PreviousPurchase.Value = False Then
                                strSthotDOPreviousPurchase = "N"
                            End If
                            strSthotDOCompetitorPriceConverted = pricematchline.CompetitorPriceConverted.Value.ToString("0.00").PadLeft(8, " "c)
                            strSthotDOOriginalStoreNumber = pricematchline.OriginalTransStore.Value.ToString.PadLeft(3, "0"c)
                            strSthotDOOriginalTillNumber = pricematchline.OriginalTranTil.Value.ToString.PadLeft(2, "0"c)
                            strSthotDOOriginalTransactionNumber = pricematchline.OriginalTranNumb.Value.ToString.PadLeft(4, "0"c)
                            strSthotDOOriginalTransactionDate = pricematchline.OriginalTransDate.Value.ToString("dd/MM/yy")
                            strSthotDOOriginalSellingPrice = pricematchline.OriginalSellingPrice.Value.ToString("0.00").PadLeft(8, " "c)


                            SetupHash("DO", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDOTillNumber & strSthotDOTransactionNumber & strSthotDOTransactionSequenceNumber & strSthotDOCompetitorName & strSthotDOCompetitorAddressLine1 & strSthotDOCompetitorAddressLine2 & strSthotDOCompetitorAddressLine3 & strSthotDOCompetitorAddressLine4 & strSthotDOCompetitorPostCode & strSthotDOCompetitorPhoneNumber & strSthotDOCompetitorFaxNumber & strSthotDOCompetitorPrice & strSthotDoVatInclusivePrices & strSthotDOPreviousPurchase & strSthotDOCompetitorPriceConverted & strSthotDOOriginalStoreNumber & strSthotDOOriginalTillNumber & strSthotDOOriginalTransactionNumber & strSthotDOOriginalTransactionDate & strSthotDOOriginalSellingPrice
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next
                        Dim Dlocus As New BOSales.cPromiseCustomer(_Oasys3DB)
                        Dlocus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlocus.TransactionDate, Record.TransDate.Value)
                        Dlocus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlocus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlocus.TillID, Record.TillID.Value)
                        Dlocus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dlocus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlocus.TransactionNo, Record.TransactionNo.Value)
                        Dlocus.SortBy(Dlocus.TransactionDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlocus.SortBy(Dlocus.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dlocus.SortBy(Dlocus.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dim ColOc As List(Of BOSales.cPromiseCustomer) = Dlocus.LoadMatches()
                        'ColDr.Reverse()
                        For Each pricematchcustomer As BOSales.cPromiseCustomer In ColOc
                            dateHashDate = pricematchcustomer.TransactionDate.Value
                            decHashValue = CDec(pricematchcustomer.TransactionNo.Value)

                            strSthotDCTillNumber = pricematchcustomer.TillID.Value.ToString.PadLeft(2, "0"c)
                            strSthotDCTransactionNumber = pricematchcustomer.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                            strSthotDCCustomerName = pricematchcustomer.CustomerName.Value.ToString.PadRight(30, " "c)
                            strSthotDCCustomerAddressLine1 = pricematchcustomer.AddressLine1.Value.ToString.PadRight(30, " "c)
                            strSthotDCCustomerAddressLine2 = pricematchcustomer.AddressLine2.Value.ToString.PadRight(30, " "c)
                            strSthotDCCustomerAddressLine3 = pricematchcustomer.AddressLine3.Value.ToString.PadRight(30, " "c)
                            strSthotDCCustomerAddressLine4 = pricematchcustomer.AddressLine4.Value.ToString.PadRight(30, " "c)
                            strSthotDCCustomerPostCode = pricematchcustomer.PostCode.Value.ToString.PadRight(8, " "c)
                            strSthotDCCustomerPhoneNumber = pricematchcustomer.PhoneNo.Value.ToString.PadRight(15, " "c)
                            SetupHash("DC", decHashValue, dateHashDate)
                            If strSthotText <> String.Empty Then
                                strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotstrSthotDCTransactionNumbersactionDate & strSthotDCTillNumber & strSthotDCTransactionNumber & strSthotDCCustomerName & strSthotDCCustomerAddressLine1 & strSthotDCCustomerAddressLine2 & strSthotDCCustomerAddressLine3 & strSthotDCCustomerAddressLine4 & strSthotDCCustomerPostCode & strSthotDCCustomerPhoneNumber
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthotText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthotFileName, strSthotText)
                                strSthotText = String.Empty
                            End If
                        Next
                    End If
                Next 'Header to Process
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While

            'process any non-comm'ed DLGIFTs
            Dim DlgiftTkn As New BOSales.cSalesGiftTokenLines(_Oasys3DB)
            DlgiftTkn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, DlgiftTkn.CommedToHO, False)
            DlgiftTkn.SortBy(DlgiftTkn.TranDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            DlgiftTkn.SortBy(DlgiftTkn.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            DlgiftTkn.SortBy(DlgiftTkn.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            DlgiftTkn.SortBy(DlgiftTkn.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

            Dim ColDLGiftTkn As List(Of BOSales.cSalesGiftTokenLines) = DlgiftTkn.LoadMatches()
            'ColDr.Reverse()
            For Each gifttoken As BOSales.cSalesGiftTokenLines In ColDLGiftTkn
                dateHashDate = gifttoken.TranDate.Value
                decHashValue = CDec(gifttoken.TransactionNo.Value)
                strSthotDXTillNumber = gifttoken.TillID.Value.ToString.PadLeft(2, "0"c)
                strSthotDXTransactionNumber = gifttoken.TransactionNo.Value.ToString.PadLeft(4, "0"c)
                strSthotDXTransactionSequenceNumber = gifttoken.SequenceNo.Value.ToString.PadLeft(5, " "c)
                strSthotDXSerialNumber = gifttoken.SerialNo.Value.ToString.PadLeft(8, "0"c)
                strSthotDXTransactionType = gifttoken.TransactionCode.Value.ToString.PadLeft(2, " "c)
                strSthotDXEmployeeId = gifttoken.CashierNo.Value.ToString.PadLeft(3, "0"c)
                FormatDecToString(gifttoken.VoucherAmount.Value, strSthotDXGiftVoucherValue, 9, " "c, "0.00")

                SetupHash("DX", decHashValue, dateHashDate)
                If strSthotText <> String.Empty Then
                    strSthotText = strSthotText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthotText = strSthotText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthotDXTillNumber & strSthotDXTransactionNumber & strSthotDXTransactionSequenceNumber & strSthotDXSerialNumber & strSthotDXEmployeeId & strSthotDXTransactionType & strSthotDXGiftVoucherValue
                intRecordsOutput = intRecordsOutput + 1
                If strSthotText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strSthotFileName, strSthotText)
                    strSthotText = String.Empty
                End If
                Try
                    gifttoken.CommedToHO.Value = True
                    gifttoken.SaveIfExists()
                Catch ex As Exception
                End Try

            Next

            If boolGotTobhotParameters = True Then
                Dim strTobhotText As String

                dateCurrentTime = TimeOfDay
                arrTobhotInputLines(0) = String.Empty & "COMPLETED: " & Today.ToString("dd/MM/yy").PadRight(10, " "c) & dateCurrentTime.Hour.ToString.PadLeft(2, "0"c) & ":" & dateCurrentTime.Minute.ToString.PadLeft(2, "0"c) & ":" & dateCurrentTime.Second.ToString.PadLeft(2, "0"c)
                My.Computer.FileSystem.DeleteFile(strTobhotParameterFileName)
                strTobhotParameterFileName = strTobhotParameterFileName & "." & strNetId.Substring(0, 2).PadLeft(2, "0"c)
                If File.Exists(strTobhotParameterFileName) Then
                    My.Computer.FileSystem.DeleteFile(strTobhotParameterFileName)
                End If
                strTobhotText = String.Empty
                intTobhotTypes = arrTobhotInputLines.Length - 1
                For intX1 = 0 To intTobhotTypes
                    If strTobhotText <> String.Empty Then
                        strTobhotText = strTobhotText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    If arrTobhotInputLines(intX1) <> String.Empty Then
                        strTobhotText = strTobhotText.ToString.TrimEnd(" "c) & arrTobhotInputLines(intX1)
                    End If
                Next
                If strTobhotText.Length > 0 Then
                    PutSthoToDisc(strTobhotParameterFileName, strTobhotText)
                    strTobhotText = String.Empty
                End If
            End If
            If strSthotText.Length > 0 Then
                PutSthoToDisc(strSthotFileName, strSthotText)
                strSthotText = String.Empty
            End If
            strWorkString = "Processing STHOT (OT) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthotFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c)  ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolDoPrepareSthot = False
            'End If
        End If ' Preparing STHOT Complete
        If boolDoPrepareSthpaRefunds = True Then
            intRecordsOutput = 0
            strWorkString = "Processing STHPA Refunds (RF) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse
            dateSelectionDate = Retopt.LastReformatDate.Value
            If _RunningInNight = True Then
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
            End If
            dateSelectionDate = _StartDate
            While dateSelectionDate <= _EndDate
                Dltots.ClearLists()
                Dltots.ClearLoadField()
                Dltots.ClearLoadFilter()

                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, dateSelectionDate)
                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Dltots.TransDate,  _StartDate)
                'Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionCode, "RF")
                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pOr)
                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionCode, "RC")
                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                ColDt = Dltots.LoadMatches
                'clDt.Reverse()

                For Each Record As BOSales.cSalesHeader In ColDt
                    strWorkString = "Checking Transaction : " & Record.TransDate.Value.Date.ToString & Space(1) & Record.TillID.Value.PadLeft(2, "0"c) & Space(1) & Record.TransactionNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    If Record.Voided.Value = False Then
                        If Record.TrainingMode.Value = False Then
                            strArTransactionNumbersactionCode = Record.TransactionCode.Value
                            strWorkString = Record.TransactionTime.Value.ToString.PadLeft(6, "0"c)
                            strArTransactionNumbersactionTime = (strWorkString.Substring(0, 2) & ":") & (strWorkString.Substring(2, 2) & ":") & (strWorkString.Substring(4, 2))
                            strArCashierNumber = Record.CashierID.Value.PadLeft(3, "0"c)
                            strArTillId = Record.TillID.Value.ToString.PadLeft(2, "0"c)
                            strArTransactionNumber = Record.TransactionNo.Value.ToString.PadLeft(4, "0"c)

                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, Record.TransDate.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TillID, Record.TillID.Value)
                            Dlline.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransNo, Record.TransactionNo.Value)
                            Dlline.SortBy(Dlline.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                            Dlline.SortBy(Dlline.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                            ColDl = Dlline.LoadMatches()
                            For Each detail As BOSales.cSalesLine In ColDl
                                strArQuantitySold = String.Empty
                                strArItemValue = String.Empty

                                strArTransactionintLineNumber = detail.SequenceNo.Value.ToString.PadLeft(6, "0"c)
                                strArSkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                                decWorkExtendedPrice = detail.ExtendedValue.Value
                                intWorkQuantity = CInt(detail.QuantitySold.Value)
                                If Record.TransactionCode.Value = "RF" Then
                                    decWorkExtendedPrice = decWorkExtendedPrice * -1
                                    intWorkQuantity = intWorkQuantity * -1
                                End If
                                decHashValue = decWorkExtendedPrice
                                FormatIntToString(intWorkQuantity, strArQuantitySold, 6, " "c)
                                FormatDecToString(decWorkExtendedPrice, strArItemValue, 11, " "c, "0.00")
                                strArReasonCode = Record.ReasonCode.Value.ToString.PadLeft(2, "0"c)
                                dateHashDate = Record.TransDate.Value
                                SetupHash("AR", decHashValue, dateHashDate)
                                If strSthpaText <> String.Empty Then
                                    strSthpaText = strSthpaText.ToString.TrimEnd(" "c) & vbCrLf
                                End If
                                strSthpaText = strSthpaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strArTransactionNumbersactionCode & strArTransactionNumbersactionTime & strArCashierNumber & strArTillId & strArTransactionNumber & strArTransactionintLineNumber & strArSkuNumber & strArQuantitySold & strArItemValue & strArReasonCode
                                intRecordsOutput = intRecordsOutput + 1
                                If strSthpaText.Length > intMaximumSthoOutputLength Then
                                    PutSthoToDisc(strSthpaFileName, strSthpaText)
                                    strSthpaText = String.Empty
                                End If
                            Next
                        End If
                    End If
                Next
                If strSthpaText.Length > 0 Then
                    PutSthoToDisc(strSthpaFileName, strSthpaText)
                    strSthpaText = String.Empty
                End If
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While
            If strSthpaText.Length > 0 Then
                PutSthoToDisc(strSthpaFileName, strSthpaText)
                strSthpaText = String.Empty
            End If
            strWorkString = "Processing STHPA Refunds (RF) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthpaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolDoPrepareSthpaRefunds = False
        End If

        If boolDoJDAMarkups = True Or boolDoMarkups = True Then
            intRecordsOutput = 0
            strWorkString = "Processing Markups (EE,MU) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse
            dateSelectionDate = Retopt.LastReformatDate.Value
            If _RunningInNight = True Then
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
            End If
            dateSelectionDate = _StartDate

            If boolDoJDAMarkups = True Then
                Array.Clear(arrSkus, 1, 9999)
                'For i = 1 To 9999
                '    arrSkus(i) = String.Empty
                'Next
            End If
            'If boolDoMarkups = True Then
            '    dateSelectionDate = Retopt.LastReformatDate.Value
            '     _EndDate = dateSelectionDate.AddDays(1)
            'End If
            intJ = 0
            While dateSelectionDate <= _EndDate
                Dlline.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlline.TransDate, dateSelectionDate)
                Dlline.SortBy(Dlline.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                '                Dlline.SortBy(Dlline.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                '                Dlline.SortBy(Dlline.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                '               Dlline.SortBy(Dlline.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                '               Dlline.SortBy(Dlline.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                ColDl = Dlline.LoadMatches

                If boolDoJDAMarkups = True Then
                    dateLastDateChecked = dateLowestDateAllowed
                    strLastTillChecked = String.Empty
                    strLastTransactionChecked = String.Empty
                    boolCanProcessThisDltotsRecord = False
                    For Each detail As BOSales.cSalesLine In ColDl
                        strWorkString = "Checking Transaction : " & detail.TransDate.Value.Date.ToString & Space(1) & detail.TillID.Value.PadLeft(2, "0"c) & Space(1) & detail.TransNo.Value.PadLeft(4, "0"c) & Space(1) & detail.SkuNumber.Value.PadLeft(6, "0"c)
                        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                        ProcessTransmissionsProgress.Show()
                        If detail.QtyBreakMarginAmount.Value <> 0 Or detail.DealGroupMarginAmt.Value <> 0 Or detail.MultiBuyMarginAmount.Value <> 0 Or detail.HierarchyMarginAmt.Value <> 0 Or detail.EmpSalePriMarginAmt.Value <> 0 Or detail.EmpSaleSecMarginAmt.Value <> 0 Or detail.PriceOverrideAmount.Value <> 0 Or detail.TempPriceMarginAmt.Value <> 0 Then
                            If dateLastDateChecked <> detail.TransDate.Value Or strLastTillChecked <> detail.TillID.Value Or strLastTransactionChecked <> detail.TransNo.Value Then
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, detail.TransDate.Value)
                                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TillID, detail.TillID.Value)
                                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionNo, detail.TransNo.Value)
                                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                                ColDt = Dltots.LoadMatches()
                                boolCanProcessThisDltotsRecord = False
                                For Each TOTL As BOSales.cSalesHeader In ColDt
                                    dateLastDateChecked = detail.TransDate.Value
                                    strLastTillChecked = detail.TillID.Value
                                    strLastTransactionChecked = detail.TransNo.Value
                                    If TOTL.Voided.Value = False Then
                                        If TOTL.TrainingMode.Value = False Then
                                            If TOTL.TransactionCode.Value <> "ZR" And TOTL.TransactionCode.Value <> "XR" Then
                                                boolCanProcessThisDltotsRecord = True
                                                intMyOrderNumber = CInt(TOTL.OrderNo.Value)
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        If boolCanProcessThisDltotsRecord = True Then
                            For intI = 1 To (arrSkus.Count - 1)
                                If arrSkus(intI) = Nothing Then 'Or arrSkus(i).Substring(0, 6) = String.Empty Then
                                    decWorkExtendedPrice = 0
                                    intWorkQuantity = 0
                                    arrSkus(intI) = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c) & intWorkQuantity.ToString.PadLeft(7, " "c) & decWorkExtendedPrice.ToString("########0.00").PadLeft(12, " "c)
                                    intJ += 1
                                End If
                                If arrSkus(intI).Substring(0, 6) = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c) Then
                                    decWorkExtendedPrice = CDec(arrSkus(intI).Substring(15, 8))
                                    intWorkQuantity = CInt(arrSkus(intI).Substring(6, 7))
                                    'Check for any discount against Line
                                    If detail.QtyBreakMarginAmount.Value <> 0 Or detail.DealGroupMarginAmt.Value <> 0 Or detail.MultiBuyMarginAmount.Value <> 0 Or detail.HierarchyMarginAmt.Value <> 0 Or detail.EmpSalePriMarginAmt.Value <> 0 Or detail.EmpSaleSecMarginAmt.Value <> 0 Or detail.PriceOverrideAmount.Value <> 0 Or detail.TempPriceMarginAmt.Value <> 0 Then

                                        If detail.QtyBreakMarginAmount.Value <> 0 Or detail.DealGroupMarginAmt.Value <> 0 Or detail.MultiBuyMarginAmount.Value <> 0 Or detail.HierarchyMarginAmt.Value <> 0 Or detail.EmpSalePriMarginAmt.Value <> 0 Or detail.EmpSaleSecMarginAmt.Value <> 0 Then
                                            intWorkQuantity += CInt(detail.QuantitySold.Value)
                                        End If

                                        If detail.EmpSalePriMarginAmt.Value <> 0 Then
                                            decWorkExtendedPrice += (detail.EmpSalePriMarginAmt.Value * detail.QuantitySold.Value)
                                        End If
                                        If intMyOrderNumber = 0 Then
                                            decWorkExtendedPrice += detail.QtyBreakMarginAmount.Value + detail.DealGroupMarginAmt.Value + detail.MultiBuyMarginAmount.Value + detail.HierarchyMarginAmt.Value + detail.EmpSaleSecMarginAmt.Value
                                            If detail.TempPriceMarginAmt.Value <> 0 Then
                                                decWorkExtendedPrice += (detail.TempPriceMarginAmt.Value * detail.QuantitySold.Value)
                                                intWorkQuantity += CInt(detail.QuantitySold.Value)
                                            End If
                                        Else
                                            If detail.PriceOverrideAmount.Value <> 0 Or detail.TempPriceMarginAmt.Value <> 0 Then
                                                decWorkExtendedPrice += ((detail.PriceOverrideAmount.Value + detail.TempPriceMarginAmt.Value) * detail.QuantitySold.Value)
                                            End If
                                            intWorkQuantity += CInt(detail.QuantitySold.Value)
                                        End If
                                    End If
                                    arrSkus(intI) = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c) & intWorkQuantity.ToString.PadLeft(7, " "c) & decWorkExtendedPrice.ToString("########0.00").PadLeft(10, " "c)
                                    Exit For
                                End If
                            Next
                        End If
                    Next
                End If
                If boolDoMarkups = True Then ' Processing MARKUPS
                    'strWorkString = "Processing Markups (DL ..MU) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                    'OutputSthoLog(strWorkString)
                    dateLastDateChecked = dateLowestDateAllowed
                    strLastTillChecked = String.Empty
                    strLastTransactionChecked = String.Empty
                    boolCanProcessThisDltotsRecord = False

                    For Each detail As BOSales.cSalesLine In ColDl
                        If detail.PriceOverrideAmount.Value <> 0 Then
                            If dateLastDateChecked <> detail.TransDate.Value Or strLastTillChecked <> detail.TillID.Value Or strLastTransactionChecked <> detail.TransNo.Value Then
                                boolCanProcessThisDltotsRecord = False
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, detail.TransDate.Value)
                                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TillID, detail.TillID.Value)
                                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionNo, detail.TransNo.Value)
                                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                                ColDt = Dltots.LoadMatches()
                                For Each TOTL As BOSales.cSalesHeader In ColDt
                                    dateLastDateChecked = detail.TransDate.Value
                                    strLastTillChecked = detail.TillID.Value
                                    strLastTransactionChecked = detail.TransNo.Value
                                    If TOTL.Voided.Value = False Then
                                        If TOTL.TrainingMode.Value = False Then
                                            If TOTL.TransactionCode.Value <> "ZR" And TOTL.TransactionCode.Value <> "XR" Then
                                                boolCanProcessThisDltotsRecord = True
                                                'intMyOrderNumber = CInt(TOTL.OrderNo.Value)
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        If boolCanProcessThisDltotsRecord = True And detail.PriceOverrideAmount.Value <> 0 Then
                            If detail.MarkDownStock.Value = False Then ' Output non MARKDOWNS to STHOA 
                                dateHashDate = SysDates.Today.Value.Date
                                _STHOARecTypeA7SkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                                _STHOARecTypeA7MarkupOrDownCode = "2"
                                FormatIntToString(CInt(detail.QuantitySold.Value), _STHOARecTypeA7MarkupOrDownUnits, 7, " "c)
                                decWorkExtendedPrice = detail.PriceOverrideAmount.Value * detail.QuantitySold.Value * -1
                                FormatDecToString(decWorkExtendedPrice, _STHOARecTypeA7MarkupOrDownValue, 11, " "c, "0.00")
                                decHashValue = decWorkExtendedPrice
                                _STHOARecTypeA7OldSellingPrice = "       0.00+"
                                _STHOARecTypeA7NewSellingPrice = "       0.00+"
                                _STHOARecTypeA7BulkSkuNumber = "      "
                                _STHOARecTypeA7BulkUnitsAdjusted = "      "
                                FormatIntToString(detail.PriceOverrideReason.Value, _STHOARecTypeA7PriceOverrideReasonCode, 4, " "c)
                                SetupHash("A7", decHashValue, dateHashDate)
                                If strSthoaText <> String.Empty Then
                                    If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                                End If
                                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA7SkuNumber & _STHOARecTypeA7MarkupOrDownCode & _STHOARecTypeA7MarkupOrDownUnits & _STHOARecTypeA7MarkupOrDownValue & _STHOARecTypeA7OldSellingPrice & _STHOARecTypeA7NewSellingPrice & _STHOARecTypeA7BulkSkuNumber & _STHOARecTypeA7BulkUnitsAdjusted & _STHOARecTypeA7PriceOverrideReasonCode '& vbCrLf
                                If strSthoaText.Length > intMaximumSthoOutputLength Then
                                    PutSthoToDisc(strSthoaFileName, strSthoaText)
                                    strSthoaText = String.Empty
                                End If
                            End If ' Output non MARKDOWNS to STHOA 
                            If detail.MarkDownStock.Value = True Then ' Output MARKDOWNS to STHOA
                                dateHashDate = detail.TransDate.Value
                                _STHOARecTypeA6SkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                                FormatIntToString(detail.PriceOverrideReason.Value, _STHOARecTypeA6AdjustmentCode, 2, " "c)
                                FormatIntToString(CInt(detail.QuantitySold.Value * -1), _STHOARecTypeA6AdjustmentUnits, 6, " "c)
                                decWorkExtendedPrice = detail.PriceOverrideAmount.Value * detail.QuantitySold.Value * -1
                                FormatDecToString(decWorkExtendedPrice, _STHOARecTypeA6AdjustmentValue, 11, " "c, "0.00")
                                decHashValue = decWorkExtendedPrice
                                _STHOARecTypeA6AdjustmentReference = "      "
                                FormatDecToString(detail.LoopUpPrice.Value - detail.ActualSellPrice.Value, _STHOARecTypeA6ItemSellingPrice, 11, " "c, "0.00")
                                _STHOARecTypeA6AssemblyDepotNumber = "   "
                                decWorkExtendedPrice = 0
                                FormatDecToString(decWorkExtendedPrice, _STHOARecTypeA6TransferValue, 11, " "c, "0.00")
                                _STHOARecTypeA6CommentText = "M"
                                SetupHash("A6", decHashValue, dateHashDate)
                                If strSthoaText <> String.Empty Then
                                    If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                                End If
                                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA6SkuNumber & _STHOARecTypeA6AdjustmentCode & _STHOARecTypeA6AdjustmentUnits & _STHOARecTypeA6AdjustmentValue & _STHOARecTypeA6AdjustmentReference & _STHOARecTypeA6ItemSellingPrice & _STHOARecTypeA6AssemblyDepotNumber & _STHOARecTypeA6TransferValue & _STHOARecTypeA6CommentText
                                intRecordsOutput = intRecordsOutput + 1
                                If strSthoaText.Length > intMaximumSthoOutputLength Then
                                    PutSthoToDisc(strSthoaFileName, strSthoaText)
                                    strSthoaText = String.Empty
                                End If
                            End If ' Output MARKDOWNS to STHOA
                        End If
                    Next
                End If ' Processing MARKUPS
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While
            If boolDoJDAMarkups = True Then ' Processing JDA Markups for STHOA
                ReDim Preserve arrSkus(intJ)
                Array.Sort(arrSkus)
                'dateHashDate = Today.ToString("dd/MM/yy")
                dateHashDate = _StartDate
                intJ = (arrSkus.Count - 1)
                For intI = 1 To intJ
                    strWorkString = arrSkus(intI).Substring(15, 8)
                    decWorkExtendedPrice = CDec(strWorkString)
                    strWorkString = arrSkus(intI).Substring(6, 7)
                    intWorkQuantity = CInt(strWorkString)
                    If intWorkQuantity <> 0 Or decWorkExtendedPrice <> 0 Then
                        decHashValue = decWorkExtendedPrice
                        _STHOARecTypeA7SkuNumber = arrSkus(intI).Substring(0, 6)
                        _STHOARecTypeA7MarkupOrDownCode = "4"
                        _STHOARecTypeA7MarkupOrDownUnits = arrSkus(intI).Substring(6, 7)
                        FormatDecToString(decWorkExtendedPrice, _STHOARecTypeA7MarkupOrDownValue, 12, " "c, "0.00")
                        _STHOARecTypeA7OldSellingPrice = "       0.00+"
                        _STHOARecTypeA7NewSellingPrice = "       0.00+"
                        _STHOARecTypeA7BulkSkuNumber = "       0.00+"
                        _STHOARecTypeA7PriceOverrideReasonCode = "  10"
                        SetupHash("A7", decHashValue, dateHashDate)
                        If strSthoaText <> String.Empty Then
                            If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA7SkuNumber & _STHOARecTypeA7MarkupOrDownCode & _STHOARecTypeA7MarkupOrDownUnits & _STHOARecTypeA7MarkupOrDownValue & _STHOARecTypeA7OldSellingPrice & _STHOARecTypeA7NewSellingPrice & _STHOARecTypeA7BulkSkuNumber & _STHOARecTypeA7PriceOverrideReasonCode '& vbCrLf
                        intRecordsOutput = intRecordsOutput + 1
                        If strSthoaText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthoaFileName, strSthoaText)
                            strSthoaText = String.Empty
                        End If
                    End If
                Next
            End If ' Processing JDA Markups for STHOA

            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        If boolDoJDAMarkups = True Or boolDoMarkups = True Then
            strWorkString = "Processing Markups (EE,MU) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c)  ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolDoJDAMarkups = False
            boolDoMarkups = False
        End If
        ' COUPONS FOLLOW
        If boolDoPrepareCoupons = True Then
            intRecordsOutput = 0
            strWorkString = "Processing Coupons (OE) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse.AddDays(1)
            dateSelectionDate = Retopt.LastReformatDate.Value
            If _RunningInNight = True Then
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
                _EndDateToUse = dateSelectionDate
                _StartDateToUse = dateSelectionDate
            End If
            dateSelectionDate = _StartDate
            While dateSelectionDate <= _EndDate
                Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlpaid.TransDate, dateSelectionDate)
                Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dlpaid.CouponNo, "000000")
                Dlpaid.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dlpaid.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Dlpaid.CouponNo, String.Empty)
                Dlpaid.SortBy(Dlpaid.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dlpaid.SortBy(Dlpaid.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dlpaid.SortBy(Dlpaid.TransNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dlpaid.SortBy(Dlpaid.SequenceNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColDp = Dlpaid.LoadMatches

                dateLastDateChecked = dateLowestDateAllowed
                strLastTillChecked = String.Empty
                strLastTransactionChecked = String.Empty
                boolCanProcessThisDltotsRecord = False
                intMyOrderNumber = 0

                For Each payment As BOSales.cSalesPaid In ColDp
                    strWorkString = "Checking Transaction : " & payment.TransDate.Value.Date.ToString & Space(1) & payment.TillID.Value.PadLeft(2, "0"c) & Space(1) & payment.TransNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    If dateLastDateChecked <> payment.TransDate.Value Or strLastTillChecked <> payment.TillID.Value Or strLastTransactionChecked <> payment.TransNo.Value Then
                        boolCanProcessThisDltotsRecord = False
                        Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, payment.TransDate.Value)
                        Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TillID, payment.TillID.Value)
                        Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransactionNo, payment.TransNo.Value)
                        Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                        Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

                        ColDt = Dltots.LoadMatches()
                        For Each TOTL As BOSales.cSalesHeader In ColDt
                            dateLastDateChecked = payment.TransDate.Value
                            strLastTillChecked = payment.TillID.Value
                            strLastTransactionChecked = payment.TransNo.Value
                            intMyOrderNumber = 0
                            If TOTL.Voided.Value = False Then
                                If TOTL.TrainingMode.Value = False Then
                                    boolCanProcessThisDltotsRecord = True
                                    decWorkExtendedPrice = TOTL.TotalSalesAmount.Value
                                End If
                            End If
                        Next
                    End If
                    If boolCanProcessThisDltotsRecord = True And payment.TenderAmount.Value <> 0 Then
                        intMyOrderNumber = intMyOrderNumber + 1
                        strCsvWorkingArea = String.Empty
                        dateHashDate = payment.TransDate.Value
                        decHashValue = CDec(payment.TransNo.Value)
                        DlpaidCoupon = payment.CouponNo.Value.ToString.PadLeft(6, "0"c)
                        CsvData(_StartDateToUse.ToString("dd/MM/yy"), True) ' Start Date
                        CsvData(_EndDateToUse.ToString("dd/MM/yy"), True) ' End Date
                        CsvData(Retopt.Store.Value, True)                       ' Store Number
                        CsvData(payment.TransDate.Value.ToString("dd/MM/yy"), True) ' Transaction date from DLPAID
                        CsvData(payment.TillID.Value.ToString.PadLeft(2, "0"c), True) ' Till Number from DLPAID
                        CsvData(payment.TransNo.Value.ToString.PadLeft(4, "0"c), True) ' Transaction Number from DLPAID
                        CsvData(intMyOrderNumber.ToString.PadLeft(2, "0"c), True) ' Line Number from Dlpaid
                        strStockAdjustmentSign = " "
                        If decWorkExtendedPrice < +0 Then
                            decWorkExtendedPrice = decWorkExtendedPrice * -1
                            strStockAdjustmentSign = "-"
                        End If
                        CsvData(decWorkExtendedPrice.ToString("0.00") & strStockAdjustmentSign, True)
                        CsvData(payment.CouponNo.Value.ToString.PadLeft(6, "0"c), True) ' Coupon Number from DLPAID
                        decWorkValue = Math.Abs(payment.TenderAmount.Value) ' Coupon Value from DLPAID
                        strStockAdjustmentSign = " "
                        If decWorkValue < +0 Then
                            decWorkValue = decWorkValue * -1
                            strStockAdjustmentSign = "-"
                        End If
                        CsvData(decWorkValue.ToString("0.00") & strStockAdjustmentSign, True)
                        decWorkValue = Math.Abs((payment.TenderAmount.Value / decWorkExtendedPrice) * 100) ' Percentage Value of coupon to Transction Total
                        strStockAdjustmentSign = " "
                        If decWorkValue < +0 Then
                            decWorkValue = decWorkValue * -1
                            strStockAdjustmentSign = "-"
                        End If
                        CsvData(decWorkValue.ToString("0.00") & strStockAdjustmentSign, True)
                        decHashValue = CDec(payment.TransNo.Value) ' Use transaction number for hash
                        SetupHash("CE", decHashValue, dateHashDate)
                        If strSthoyText <> String.Empty Then
                            strSthoyText = strSthoyText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthoyText = strSthoyText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & DlpaidCoupon & Retopt.Store.Value.ToString.PadLeft(3, "0"c) & strCsvWorkingArea
                        intRecordsOutput = intRecordsOutput + 1
                        If strSthoyText.Length > intMaximumSthoOutputLength Then
                            PutSthoToDisc(strSthoyFileName, strSthoyText)
                            strSthoyText = String.Empty
                        End If
                    End If
                Next
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While
            strWorkString = "Processing Coupons (OE) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoyFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolDoPrepareCoupons = False
        End If
        If strSthoyText.Length > 0 Then
            PutSthoToDisc(strSthoyFileName, strSthoyText)
            strSthoyText = String.Empty
        End If

        If boolDoPreparePostCodes = True Then
            intRecordsOutput = 0
            strWorkString = "Processing Post Codes (OC) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            If (_RunningInNight = False) And (_PromptForDate = True) Then
                Trace.WriteLine("Requesting Date for Post Codes OC")
                Dim StartOCDate As String = InputBox("Enter starting date to prepare Post Codes comms file" & vbCrLf & vbCrLf & "Enter date as dd/mm/yy", "Enter Post Codes Start Date", Today.ToString("dd/MM/yy"))
                strWorkString = "Processing Post Code (OC) Entered Starting Date(" & StartOCDate & ") : " & TimeOfDay.ToString("hh:mm:ss")
                UpdateProgress(String.Empty, String.Empty, strWorkString)
                OutputSthoLog(strWorkString)
                If IsDate(StartOCDate) = False Then End
                Dim EndOCDate As String = InputBox("Enter ending date to prepare Post Code comms file" & vbCrLf & vbCrLf & "Enter date as dd/mm/yy", "Enter Post Codes End Date", Today.ToString("dd/MM/yy"))
                strWorkString = "Processing Post Code (OC) Entered Ending Date(" & EndOCDate & ") : " & TimeOfDay.ToString("hh:mm:ss")
                UpdateProgress(String.Empty, String.Empty, strWorkString)
                OutputSthoLog(strWorkString)
                If IsDate(EndOCDate) = False Then End
                _DatePassedIn = True
                _StartDateToUse = CDate(StartOCDate)
                _EndDateToUse = CDate(EndOCDate)
            End If
            _StartDate = _StartDateToUse
            _EndDate = _EndDateToUse
            dateSelectionDate = Retopt.LastReformatDate.Value
            If _RunningInNight = True Then 'only run for Current Date
                _StartDate = dateSelectionDate
                _EndDate = dateSelectionDate
            End If
            dateSelectionDate = _StartDate 'set to first date to extract
            While dateSelectionDate <= _EndDate
                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.TransDate, dateSelectionDate)
                Dltots.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Dltots.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dltots.AccountUpdComplete, True)
                Dltots.SortBy(Dltots.TransDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                Dltots.SortBy(Dltots.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColDt = Dltots.LoadMatches()

                For Each record As BOSales.cSalesHeader In ColDt
                    strWorkString = "Checking Transaction : " & record.TransDate.Value.Date.ToString & Space(1) & record.TillID.Value.PadLeft(2, "0"c) & Space(1) & record.TransactionNo.Value.PadLeft(4, "0"c)
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                    ProcessTransmissionsProgress.Show()
                    Dim Dlrcus As New BOSales.cSalesCustomers(_Oasys3DB)
                    Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TransactionDate, record.TransDate.Value)
                    Dlrcus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TillID, record.TillID.Value)
                    Dlrcus.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Dlrcus.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Dlrcus.TransactionNo, record.TransactionNo.Value)
                    Dlrcus.SortBy(Dlrcus.TransactionDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    Dlrcus.SortBy(Dlrcus.TillID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    Dlrcus.SortBy(Dlrcus.TransactionNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    Dlrcus.SortBy(Dlrcus.LineNo.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    Dim ColDr As List(Of BOSales.cSalesCustomers) = Dlrcus.LoadMatches()
                    Dim strDlrcusPostcode As String = String.Empty
                    If ColDr.Count <> 0 Then
                        For Each customer As BOSales.cSalesCustomers In ColDr
                            strDlrcusPostcode = customer.PostCode.Value
                            If Retopt.CountryCode.Value = "IE" Then strDlrcusPostcode = customer.AddressLine1.Value
                            Exit For 'only use first entry in DLRCUS as this has Customer Address details
                        Next
                    End If
                    strCsvWorkingArea = String.Empty
                    CsvData(record.TransDate.Value.ToString("dd/MM/yy"), True) ' Transaction date from DLPAID
                    CsvData(record.TillID.Value.ToString.PadLeft(2, "0"c), True) ' Till Number from DLPAID
                    CsvData(record.TransactionNo.Value.ToString.PadLeft(4, "0"c), True) ' Transaction Number from DLPAID
                    decWorkValue = record.TotalSalesAmount.Value ' Transaction Value from DLTOTS
                    strStockAdjustmentSign = "+"
                    If decWorkValue < +0 Then
                        decWorkValue = decWorkValue * -1
                        strStockAdjustmentSign = "-"
                    End If
                    CsvData(strStockAdjustmentSign & decWorkValue.ToString("0.00"), False)
                    'If Retopt.CountryCode.Value = "IE" Then
                    '    CsvData(strDlrcusPostcode.ToString.PadRight(30, " ")) ' Post Code from DLRCUS
                    'Else
                    CsvData(strDlrcusPostcode.ToString.PadRight(8, " "c), True) ' Post Code from DLRCUS
                    'End If
                    'CsvData(arrDlrcusPostCode(y).ToString.PadRight(8, " ")) ' Post Code from DLRCUS
                    CsvData(record.TransactionTime.Value.ToString.PadLeft(6, "0"c), True)
                    If strpostCodeText <> String.Empty Then
                        strpostCodeText = strpostCodeText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strpostCodeText = strpostCodeText.ToString.TrimEnd(" "c) & strCsvWorkingArea
                    intRecordsOutput = intRecordsOutput + 1
                    If strpostCodeText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_PostCodeFileName, strpostCodeText)
                        strpostCodeText = String.Empty
                    End If
                Next
                dateSelectionDate = dateSelectionDate.AddDays(1)
            End While
            strWorkString = "Processing Post Codes (OC) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & _PostCodeFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)


        End If
        If strpostCodeText.Length > 0 Then
            PutSthoToDisc(_PostCodeFileName, strpostCodeText)
            strpostCodeText = String.Empty
        End If

    End Sub ' ProcessDlRecords

    Public Sub OutputFh() ' Output Record Type FH to STHOF.
        SetupHash("FH", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "HD" & strSthofRecordTypeFHDate & strSthofRecordTypeFHStoreNumber & strSthofRecordTypeFHNumberOfRecords
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type FH to STHOF.
    Public Sub OutputF1() ' Output Record Type F1 to STHOF.
        SetupHash("F1", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "01" & _STHOFRecTypeF1SaleTransactionCount & _STHOFRecTypeF1SaleCorrectTransactionCount & _STHOFRecTypeF1RefundTransactionCount & _STHOFRecTypeF1RefundCorrectTransactionCount & _STHOFRecTypeF1MiscellaneousInTransactionCount & _STHOFRecTypeF1OpenDrawerTransactionCount & _STHOFRecTypeF1VoidedTransactionCount & _STHOFRecTypeF1PriceViolationTransactionCount
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F1 to STHOF.
    Public Sub OutputF2() ' Output Record Type F2 to STHOF.
        SetupHash("F2", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "02" & _STHOFRecTypeF2SalesTransactionTime & _STHOFRecTypeF2SalesCorrectTransactionTime & _STHOFRecTypeF2RefundTransactionTime & _STHOFRecTypeF2RefundCorrectTransactionTime & _STHOFRecTypeF2MiscellaneousInTransactionTime & _STHOFRecTypeF2OpenDrawerTransactionTime & _STHOFRecTypeF2VoidedTransactionTime & _STHOFRecTypeF2PriceViolationTransactionTime
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F2 to STHOF.
    Public Sub OutputF3() ' Output Record Type F3 to STHOF.
        SetupHash("F3", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "03" & _STHOFRecTypeF3SalesTransactionAmount & _STHOFRecTypeF3SalesCorrectTransactionAmount & _STHOFRecTypeF3RefundTransactionAmount & _STHOFRecTypeF3RefundCorrectTransactionAmount & _STHOFRecTypeF3MiscellaneousInTransactionAmount & _STHOFRecTypeF3PriceViolationTransactionAmount
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F3 to STHOF.
    Public Sub OutputF4() ' Output Record Type F4 to STHOF.
        SetupHash("F4", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "04" & _STHOFRecTypeF4TenderCashAmount & _STHOFRecTypeF4TenderChequeAmount & _STHOFRecTypeF4TenderMastercardAmount & _STHOFRecTypeF4TenderVisaAmount & _STHOFRecTypeF4TenderWickesAmount & _STHOFRecTypeF4TenderVoucherAmount & _STHOFRecTypeF4TenderAmexAmount & _STHOFRecTypeF4TenderMaestroAmount & _STHOFRecTypeF4TenderBadChequeAmount & _STHOFRecTypeF4TenderTradeCreditAmount & _STHOFRecTypeF4TenderType12Amount & _STHOFRecTypeF4TenderType13Amount & _STHOFRecTypeF4TenderType14Amount & _STHOFRecTypeF4TenderType15Amount & _STHOFRecTypeF4TenderType17Amount & _STHOFRecTypeF4TenderType18Amount
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F4 to STHOF.
    Public Sub OutputF5() ' Output Record Type F5 to STHOF.
        SetupHash("F5", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "05" & _STHOFRecTypeF5TenderCashCount & _STHOFRecTypeF5TenderChequeCount & _STHOFRecTypeF5TenderMastercardCount & _STHOFRecTypeF5TenderVisaCount & _STHOFRecTypeF5TenderWickesCount & _STHOFRecTypeF5TenderVoucherCount & _STHOFRecTypeF5TenderAmexCount & _STHOFRecTypeF5TenderMaestroCount & _STHOFRecTypeF5TenderBadChequeCount & _STHOFRecTypeF5TenderTradeCreditCount
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F5 to STHOF.
    Public Sub OutputF6() ' Output Record Type F6 to STHOF.
        SetupHash("F6", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "06" & _STHOFRecTypeF6DailyHoursPaid
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F6 to STHOF.
    Public Sub OutputF7() ' Output Record Type F7 to STHOF.
        SetupHash("F7", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "07" & _STHOFRecTypeF7ConservatorySales & _STHOFRecTypeF7FrontEndSalesValue & _STHOFRecTypeF7FrontEndSalesCount & _STHOFRecTypeF7FrontEndRefundsValue & _STHOFRecTypeF7FrontEndRefundsCount & _STHOFRecTypeF7FrontEndSalesAndRefundVoucherValue & _STHOFRecTypeF7FrontEndSalesAndRefundVoucherCount & _STHOFRecTypeF7FrontEndSalesAndRefundDeliveryValue & _STHOFRecTypeF7FrontEndSalesAndRefundInstallationValue & _STHOFRecTypeF7TotalDeliveriesCount & _STHOFRecTypeF7TotalDeliveriesValue & _STHOFRecTypeF7TotalDeliveryChargeValue & _STHOFRecTypeF7TotalDeliveryToSalesPercentage
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F7 to STHOF.
    Public Sub OutputF8() ' Output Record Type F8 to STHOF.
        SetupHash("F8", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "08" & _STHOFRecTypeF8NumberOfSkusOutOfStock & _STHOFRecTypeF8TodaysType2and10Adjustments & _STHOFRecTypeF8TodaysDailyReceiverListingValue & _STHOFRecTypeF8ThisWeekToDateDailyReceiverListingValue & _STHOFRecTypeF8NumberOfUnappliedPriceChanges & _STHOFRecTypeF8WeeklyMarkdownsValue
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F8 to STHOF.
    Public Sub OutputF9() ' Output Record Type F9 to STHOF.
        SetupHash("F9", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "09" & _STHOFRecTypeF9WeeklyWagesValue & _STHOFRecTypeF9StandardHours & _STHOFRecTypeF9NumberOfStarters & _STHOFRecTypeF9NumberOfLeavers & _STHOFRecTypeF9NumberOfFullTimeStaff & _STHOFRecTypeF9NumberOfPartTimeStaff
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type F9 to STHOF.
    Public Sub OutputFa() ' Output Record Type FA to STHOF.
        SetupHash("FA", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "10" & strSthofRecordTypeFAMarkdownsValue
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type FA to STHOF.
    Public Sub OutputFB() ' Output Record Type FB to STHOF
        SetupHash("FB", decHashValue, dateHashDate)
        If strSthofText <> String.Empty Then
            strSthofText = strSthofText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthofText = strSthofText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "11" & strSthofRecordTypeFBOpenPurchaseOrdersValue & strSthofRecordTypeFBStockValue
        If strSthofText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthotFileName, strSthofText)
            strSthofText = String.Empty
        End If
        intRecordsOutput = intRecordsOutput + 1
    End Sub ' Output Record Type FB to STHOF.

    Public Sub ProcessAdjustments()
        Dim StockAdjustments As New BOStock.cStockAdjust(_Oasys3DB)
        Dim ColAd As New List(Of BOStock.cStockAdjust)
        'get valid summary record
        StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdjustments.CommedToHeadOffice, 0)
        StockAdjustments.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, StockAdjustments.MarkdownWriteOff, "M")
        'StockAdjustments.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, StockAdjustments.SkuNumber, 0)
        StockAdjustments.SortBy(StockAdjustments.DateCreated.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        StockAdjustments.SortBy(StockAdjustments.Code.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        StockAdjustments.SortBy(StockAdjustments.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        StockAdjustments.SortBy(StockAdjustments.Sequence.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        ColAd = StockAdjustments.LoadMatches()

        Dim boolCanSendThisAdjustment As Boolean
        ' Dim strSthoaText As String = String.Empty
        'Dim SysPeriods As New BOSystem.cSystemPeriods(_Oasys3DB)

        'OpenTransmissionFile(strSthoaFileName)

        Dim decTemporaryValue As Decimal = 0
        Dim strAdjustmentType As String = String.Empty
        Dim strStockAdjustmentDate As String = String.Empty ' Adjustment date    - DATA + 2
        'Dim decStockAdjustmentHashValue As Decimal ' Hash               - data +10
        Dim strStockAdjustmentSkuNumber As String = String.Empty  ' Sku number         - data +22  
        Dim strStockAdjustmentCode As String = String.Empty ' Stock Adjustment Code - data +28
        Dim intStockAdjustmentUnitsAdjusted As Integer = 0 ' Units adjusted     - data +30 
        Dim decStockAdjustmentValueAdjusted As Decimal = 0 ' Value adjusted     - data +38 - 
        Dim strStockAdjustmentReference As String = String.Empty  ' Ref/drl/p/o/t sku  - data +50
        Dim decStockAdjustmentSellingPrice As Decimal = 0  ' Price              - data +56
        Dim strStockAdjustmentAssemblyDepot As String = String.Empty ' Dep 10/20 ibt stor - data +68 
        Dim decStockAdjustmentTransferValue As Decimal = 0 ' tval or container  - data +7
        Dim boolUpdateAsCommed As Boolean = False
        Dim strWriteOffId As String = String.Empty
        intRecordsOutput = 0

        strWorkString = "Processing STHOA - Stock Adjustments (AD) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, "Processing STHOA - Stock Adjustments (AD)")
        OutputSthoLog(strWorkString)
        For Each adjusts In ColAd
            Dim strWkWID As String = adjusts.WriteOffID.Value.ToString
            strWriteOffId = adjusts.WriteOffID.Value.ToString.PadLeft(3, "0"c)

            boolCanSendThisAdjustment = True
            boolUpdateAsCommed = True
            'If adjusts.MarkdownWriteOff.Value = "W" And adjusts.WriteOffID.Value > "   " Then
            If adjusts.MarkdownWriteOff.Value = "W" And strWriteOffId = "000" Then
                boolCanSendThisAdjustment = False
                boolUpdateAsCommed = False
            End If
            If boolCanSendThisAdjustment = True Then 'Can send this now
                'SysPeriods.AddLoadField(SysPeriods.StartDate)
                'SysPeriods.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysPeriods.ID, adjusts.PeriodID.Value)
                'SysPeriods.LoadMatches()

                'ddskun = adjusts.SKUNumber.Value
                strStockAdjustmentAssemblyDepot = "000"
                strStockAdjustmentCode = adjusts.Code.Value

                If strStockAdjustmentCode = "08" Then
                    strStockAdjustmentCode = "02"
                End If

                decTemporaryValue = adjusts.QtyAdjusted.Value * adjusts.Price.Value
                strStockAdjustmentDate = adjusts.DateCreated.Value.ToString("dd/MM/yy")
                If adjusts.MarkdownWriteOff.Value = "W" Then
                    strStockAdjustmentDate = adjusts.WriteOffDate.Value.ToString("dd/MM/yy")
                    'Else
                    '    strStockAdjustmentDate = SysPeriods.StartDate.Value.ToString("dd/MM/yy")
                End If

                If adjusts.Code.Value = "08" Then
                    strStockAdjustmentCode = "02"
                Else
                    strStockAdjustmentCode = adjusts.Code.Value
                End If
                strStockAdjustmentSkuNumber = adjusts.SkuNumber.Value
                intStockAdjustmentUnitsAdjusted = CInt(adjusts.QtyAdjusted.Value)
                decStockAdjustmentValueAdjusted = decTemporaryValue
                _STHOARecTypeA6TransferValue = String.Empty
                If adjusts.Code.Value = "10" Or adjusts.Code.Value = "20" Then
                    Dim strAdjustComment As String = adjusts.Comment.Value
                    If strAdjustComment.Length < 20 Then strAdjustComment = strAdjustComment.PadRight(20, " "c)
                    strStockAdjustmentText = Space(4) 'blank out Type and Write-Off ID
                    adjusts.Comment.Value = adjusts.Comment.Value.ToString.PadRight(20)
                    strStockAdjustmentReference = adjusts.Comment.Value.Substring(0, 6)
                    strStockAdjustmentAssemblyDepot = adjusts.Comment.Value.Substring(7, 3).PadRight(3, "0"c)
                    decStockAdjustmentSellingPrice = adjusts.Price.Value
                    If adjusts.Comment.Value.Substring(11, 9) <> String.Empty Then
                        _STHOARecTypeA6TransferValue = adjusts.Comment.Value.Substring(11, 9).PadRight(9, " "c)
                        'decStockAdjustmentTransferValue = CDec(Mid(adjusts.Comment.Value, 11, 9))
                    End If
                End If
                If adjusts.Code.Value <> "10" And adjusts.Code.Value <> "20" Then

                    If adjusts.Code.Value = "04" Then
                        strStockAdjustmentReference = adjusts.DrlNumber.Value
                    Else
                        strStockAdjustmentReference = adjusts.TransferSKU.Value
                    End If

                    decStockAdjustmentSellingPrice = adjusts.Price.Value
                    decStockAdjustmentTransferValue = 0 - adjusts.TransferValue.Value
                    strAdjustmentType = " "

                    If adjusts.MarkdownWriteOff.Value = "W" Then strAdjustmentType = "W"
                    If adjusts.MarkdownWriteOff.Value = "M" Then strAdjustmentType = "M"
                    '                    If strAdjustmentType <> " " Then
                    strStockAdjustmentText = strAdjustmentType & adjusts.WriteOffID.Value.ToString.PadLeft(3, "0"c)
                    'End If
                    strStockAdjustmentAssemblyDepot = "   "

                End If
                SetupHash("A6", decTemporaryValue, CDate(strStockAdjustmentDate))
                _STHOARecTypeA6SkuNumber = strStockAdjustmentSkuNumber
                _STHOARecTypeA6AdjustmentCode = strStockAdjustmentCode
                _STHOARecTypeA6AdjustmentUnits = String.Format("        ")
                If intStockAdjustmentUnitsAdjusted <> +0 Then
                    FormatIntToString(intStockAdjustmentUnitsAdjusted, _STHOARecTypeA6AdjustmentUnits, 7, " "c)
                End If
                FormatDecToString(decStockAdjustmentValueAdjusted, _STHOARecTypeA6AdjustmentValue, 11, " "c, "0.00")
                _STHOARecTypeA6AdjustmentReference = strStockAdjustmentReference.PadLeft(6, " "c) 'Get DRL Number or Transfer SKU
                If (_STHOARecTypeA6AdjustmentReference.Trim = String.Empty) Then _STHOARecTypeA6AdjustmentReference = "000000"
                _STHOARecTypeA6ItemSellingPrice = String.Format("            ")
                If decStockAdjustmentSellingPrice <> +0 Then
                    FormatDecToString(decStockAdjustmentSellingPrice, _STHOARecTypeA6ItemSellingPrice, 11, " "c, "0.00")
                End If
                _STHOARecTypeA6AssemblyDepotNumber = strStockAdjustmentAssemblyDepot
                '_STHOARecTypeA6TransferValue = Space(12)
                'If decStockAdjustmentTransferValue <> +0 Then
                If _STHOARecTypeA6TransferValue = String.Empty Then
                    FormatDecToString(decStockAdjustmentTransferValue, _STHOARecTypeA6TransferValue, 11, " "c, "0.00")
                End If
                strStockAdjustmentText.Replace("+", OriginalSthoFileSeparator)
                _STHOARecTypeA6CommentText = strStockAdjustmentText

                'SetupHash("A7", decHashValue, strStockAdjustmentDate)
                If strSthoaText <> String.Empty Then
                    If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA6SkuNumber & _STHOARecTypeA6AdjustmentCode & _STHOARecTypeA6AdjustmentUnits & _STHOARecTypeA6AdjustmentValue & _STHOARecTypeA6AdjustmentReference & _STHOARecTypeA6ItemSellingPrice & _STHOARecTypeA6AssemblyDepotNumber & _STHOARecTypeA6TransferValue & _STHOARecTypeA6CommentText
                intRecordsOutput = intRecordsOutput + 1
                If strSthoaText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strSthoaFileName, strSthoaText)
                    strSthoaText = String.Empty
                End If

                'Dim strTemporary As String = adjusts.WriteOffID.Value
                'Dim intTemporaryLength As Integer = strTemporary.Length

                'If adjusts.WriteOffID.Value.Length < 1 Or adjusts.WriteOffID.Value = String.Empty Then
                '    adjusts.WriteOffID.Value = " "
                'End If
                'If adjusts.Comment.Value.Length < 1 Then
                '    adjusts.Comment.Value = " "
                'End If

            End If 'Processing DRLSUM
            If boolUpdateAsCommed = True Then
                Try
                    adjusts.CommedToHeadOffice.Value = True
                    If adjusts.Comment.Value < "  " Then
                        adjusts.Comment.Value = String.Format("  ")
                    End If
                    If adjusts.DrlNumber.Value < "  " Then
                        adjusts.DrlNumber.Value = String.Format("  ")
                    End If
                    'If adjusts.WriteOffID.Value < "  " Then
                    'adjusts.WriteOffID.Value = 0
                    'End If
                    adjusts.RTI.Value = "S"
                    adjusts.SaveIfExists()
                Catch ex As Exception
                End Try

            End If
        Next
        If strSthoaText.Length > 0 Then
            PutSthoToDisc(strSthoaFileName, strSthoaText)
            strSthoaText = String.Empty
        End If

        strWorkString = "Processing STHOA - Flagging All Markdowns as Commed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        OutputSthoLog(strWorkString)
        _Oasys3DB.ClearAllParameters()
        _Oasys3DB.SetTableParameter(StockAdjustments.TableName, OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        _Oasys3DB.SetColumnAndValueParameter(StockAdjustments.CommedToHeadOffice.ColumnName, True)
        _Oasys3DB.SetColumnAndValueParameter(StockAdjustments.RTI.ColumnName, "S")
        _Oasys3DB.SetWhereParameter(StockAdjustments.MarkdownWriteOff.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, "M")
        _Oasys3DB.SetWhereJoinParameter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        _Oasys3DB.SetWhereParameter(StockAdjustments.CommedToHeadOffice.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, False)
        _Oasys3DB.Update()

        strWorkString = "Processing STHOA - Stock Adjustments (AD) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        OutputSthoLog(strWorkString)

    End Sub ' adjustments

    Public Sub SoldToday(ByVal mySkun As String, ByVal MyDate As Date) ' Calculate Sold Today Values from STKLOG.

        Dim intMarkdownUnits As Integer
        Dim decMarkdownValue As Decimal
        Dim Stklog As New BOStock.cStockLog(_Oasys3DB)
        Dim ColSl As New List(Of BOStock.cStockLog)

        Stklog.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stklog.SkuNumber, mySkun)
        Stklog.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Stklog.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stklog.LogDate, MyDate)
        ColSl = Stklog.LoadMatches()

        intUnitsSoldToday = +0
        intMarkdownUnitsToday = +0
        decMarkdownValueToday = +0

        For Each rec In ColSl
            If rec.LogType.Value = "01" Or rec.LogType.Value = "02" Or rec.LogType.Value = "03" Or rec.LogType.Value = "05" Or rec.LogType.Value = "11" Or rec.LogType.Value = "12" Then
                intUnitsSoldToday = intUnitsSoldToday + rec.StockOnHandStart.Value - rec.StockOnHandEnd.Value
            End If
            If rec.LogType.Value = "04" Then
                intMarkdownUnits = rec.StockMarkdownStart.Value - rec.StockMarkdownEnd.Value
                decMarkdownValue = intMarkdownUnits * rec.PriceEnd.Value
                intMarkdownUnitsToday = intMarkdownUnitsToday + intMarkdownUnits
                decMarkdownValueToday = decMarkdownValueToday + decMarkdownValue
            End If
        Next
    End Sub  ' Calculate Sold Today Values from STKLOG.

    Public Sub ProcessReceipts() ' Process receipts for Transmission.

        Dim DRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)

        Dim SysDates As New BOSystem.cSystemDates(_Oasys3DB)

        Dim boolHomeDeliveryCentre As Boolean = False
        Dim intI As Integer = 0
        Dim intJ As Integer = 0
        Dim intDrldetOrderQuantity As Integer = 0
        Dim intDrldetReceivedQuantity As Integer = 0
        Dim intDrldetIBTQuantity As Integer = 0
        Dim intDrldetReturnedQuantity As Integer = 0

        Dim decDrldetValue As Decimal
        Dim decDrldetOrderPrice As Decimal
        Dim decDrldetSellingPrice As Decimal

        Dim strDrlsumPurchaseOrderNumber As String = String.Empty ' Purchase Order Number from DRLSUM
        Dim strDrlsumPurchaseOrderReleaseNumber As String = String.Empty ' Purchase Order Release Number from DRLSUM
        Dim strDrlsumSupplierNumber As String = String.Empty ' Supplier Number from DRLSUM
        Dim dateDrlsumCreatedDate As Date                  ' Date from DRLSUM 

        Dim strDrlsumReference As String = String.Empty ' Reference from DRLSUM
        Dim strDrlsumDailyReceiverListingNumber As String = String.Empty ' DRL Number from DRLSUM
        Dim strDrlsumStoreNumber As String = String.Empty ' Store Number from DRLSUM
        Dim strDrlsumDeliveryNote1 As String = String.Empty ' Delivery Note Number 1 from DRLSUM
        Dim strDrlsumSuggestedOrderQuantityControlNumber As String = String.Empty ' Soq Control number of the P/O
        Dim strDrlsumInformationText As String = String.Empty ' Information text taken from DRLSUM
        Dim strDrldetSkuNumber As String = String.Empty
        Dim strDrldetSequenceNumber As String = String.Empty

        Dim strSthoaRecordTypeA4PurchaseOrderNumber As String ' Purchase Order Number                                 - Data +22
        Dim strSthoaRecordTypeA4PurchaseOrderReleaseNumber As String ' Purchase Order Release Number                         - Data +28
        Dim strSthoaRecordTypeA4SupplierNumber As String ' Supplier Number                                       - Data +30 
        Dim strSthoaRecordTypeA4SkuNumber As String ' SKU Number                                            - Data +35
        Dim strSthoaRecordTypeA4OrderQuantity As String '   7.0 Signed Numeric Quantity Ordered                 - Data +41  
        Dim strSthoaRecordTypeA4ReceivedQuantity As String '   7.0 Signed Numeric Quantity Received                - Data +49  
        Dim strSthoaRecordTypeA4OrderPrice As String = "" '  10.2 Signed Numeric Order Price                      - Data +57
        Dim strSthoaRecordTypeA4DailyReceiverListingNumber As String ' DRL Number                                            - Data +69 
        Dim strSthoaRecordTypeA4OrderDate As String ' Order Date                               dd/mm/yy     - Data +75
        Dim strSthoaRecordTypeA4NormalSellingPrice As String = "" '  10.2 Signed Numeric Receipt Price                    - Data +83
        Dim strSthoaRecordTypeA4SequenceNumber As String ' Line Number                                           - Data +95
        Dim strSthoaRecordTypeA4DeliveryNote1 As String ' Delivery Note Number                                  - Data +99
        Dim strSthoaRecordTypeA4SuggestedOrderQuantityControlNumber As String ' SOQ Control Number                                    - Data +109 
        Dim strSthoaRecordTypeA4InformationText As String ' Comment Entered for supplier returns                  - Data +115

        Dim strStockAdjustmentDate As String = String.Empty ' Adjustment date    - DATA + 2
        'Dim decStockAdjustmentHashValue As Decimal ' Hash               - data +10
        Dim strStockAdjustmentSkuNumber As String = String.Empty  ' Sku number         - data +22  
        Dim strStockAdjustmentCode As String = String.Empty ' Stock Adjustment Code - data +28
        Dim intStockAdjustmentUnitsAdjusted As Integer = 0 ' Units adjusted     - data +30 
        Dim decStockAdjustmentValueAdjusted As Decimal = 0 ' Value adjusted     - data +38 - 
        Dim strStockAdjustmentReference As String = String.Empty  ' Ref/drl/p/o/t sku  - data +50
        Dim decStockAdjustmentSellingPrice As Decimal = 0  ' Price              - data +56
        Dim strStockAdjustmentAssemblyDepot As String = String.Empty ' Dep 10/20 ibt stor - data +68 
        Dim decStockAdjustmentTransferValue As Decimal = 0 ' tval or container  - data +7
        SysDates.AddLoadField(SysDates.Today)
        SysDates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SysDates.SystemDatesID, "01")
        SysDates.LoadMatches()

        If _StoreNumber = "167" Then boolHomeDeliveryCentre = True

        'get valid summary records


        DRLHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, DRLHeader.CommedToHO, False)
        DRLHeader.SortBy(DRLHeader.Number.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        DRLHeader.Headers = DRLHeader.LoadMatches

        strSthoaText = String.Empty
        intRecordsOutput = 0
        strWorkString = "Processing STHOA - Receipts (DR) Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        For Each header As BOPurchases.cDrlHeader In DRLHeader.Headers 'Processing DRLSUM
            If header.Type.Value = "0" Then
                If header.AssignedDate.Value < SysDates.Today.Value Then
                    If header.POSupplierBBC.Value = False Then
                        strDrlsumPurchaseOrderNumber = header.PONumber.Value
                        strDrlsumPurchaseOrderReleaseNumber = header.POReleaseNumber.Value
                        strDrlsumSupplierNumber = header.POSupplierNumber.Value
                        dateDrlsumCreatedDate = header.PODate.Value
                        For Each detail As BOPurchases.cDrlDetail In header.Details
                            intDrldetOrderQuantity = CInt(detail.OrderQty.Value)
                            intDrldetReceivedQuantity = CInt(detail.ReceivedQty.Value)
                            decDrldetOrderPrice = detail.OrderPrice.Value
                            decDrldetSellingPrice = detail.SkuPrice.Value
                            intDrldetIBTQuantity = CInt(detail.IbtInOutQty.Value)
                            intDrldetReturnedQuantity = CInt(detail.ReturnQty.Value)
                            strDrldetSequenceNumber = detail.SequenceNumber.Value
                            strDrldetSkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                            strDrlsumDeliveryNote1 = header.PODeliveryNote1.Value
                            strDrlsumSuggestedOrderQuantityControlNumber = header.POSOQNumber.Value
                            strDrlsumInformationText = header.Comment.Value
                            strDrlsumInformationText.Replace("+", OriginalSthoFileSeparator)

                            decHashValue = detail.ReceivedQty.Value
                            'dateHashDate = dateDrlsumCreatedDate
                            dateHashDate = header.AssignedDate.Value
                            strSthoaRecordTypeA4PurchaseOrderNumber = strDrlsumPurchaseOrderNumber
                            strSthoaRecordTypeA4PurchaseOrderReleaseNumber = strDrlsumPurchaseOrderReleaseNumber
                            strSthoaRecordTypeA4SupplierNumber = strDrlsumSupplierNumber
                            strSthoaRecordTypeA4SkuNumber = strDrldetSkuNumber
                            strSthoaRecordTypeA4ReceivedQuantity = String.Empty
                            strSthoaRecordTypeA4OrderQuantity = String.Empty
                            FormatIntToString(CInt(detail.OrderQty.Value), strSthoaRecordTypeA4OrderQuantity, 7, " "c)
                            FormatIntToString(CInt(detail.ReceivedQty.Value), strSthoaRecordTypeA4ReceivedQuantity, 7, " "c)
                            strSthoaRecordTypeA4OrderPrice = String.Format("            ")
                            'If detail.OrderPrice.Value <> +0 Then
                            FormatDecToString(detail.OrderPrice.Value, strSthoaRecordTypeA4OrderPrice, 11, " "c, "0.00")
                            'End If
                            strSthoaRecordTypeA4DailyReceiverListingNumber = header.Number.Value.ToString.PadLeft(6, "0"c)
                            strSthoaRecordTypeA4OrderDate = dateDrlsumCreatedDate.ToString("dd/MM/yy")
                            strSthoaRecordTypeA4NormalSellingPrice = String.Format("            ")
                            'If detail.SkuPrice.Value <> +0 Then
                            FormatDecToString(detail.SkuPrice.Value, strSthoaRecordTypeA4NormalSellingPrice, 11, " "c, "0.00")
                            'End If
                            strSthoaRecordTypeA4SequenceNumber = strDrldetSequenceNumber.PadLeft(4, "0"c)
                            strSthoaRecordTypeA4DeliveryNote1 = strDrlsumDeliveryNote1.PadRight(10, " "c)
                            strSthoaRecordTypeA4SuggestedOrderQuantityControlNumber = strDrlsumSuggestedOrderQuantityControlNumber.PadRight(6, "0"c)
                            strDrlsumInformationText.Replace("+", OriginalSthoFileSeparator)
                            strSthoaRecordTypeA4InformationText = strDrlsumInformationText
                            intJ = strSthoaRecordTypeA4InformationText.Length
                            For intI = 1 To intJ
                                If Mid(strSthoaRecordTypeA4InformationText, intI, 1) < " " Then
                                    Mid(strSthoaRecordTypeA4InformationText, intI, 1) = " "
                                End If
                            Next
                            SetupHash("A4", decHashValue, dateHashDate)
                            If strSthoaText <> String.Empty Then
                                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthoaRecordTypeA4PurchaseOrderNumber & strSthoaRecordTypeA4PurchaseOrderReleaseNumber & strSthoaRecordTypeA4SupplierNumber & strSthoaRecordTypeA4SkuNumber & strSthoaRecordTypeA4OrderQuantity & strSthoaRecordTypeA4ReceivedQuantity & strSthoaRecordTypeA4OrderPrice & strSthoaRecordTypeA4DailyReceiverListingNumber & strSthoaRecordTypeA4OrderDate & strSthoaRecordTypeA4NormalSellingPrice & strSthoaRecordTypeA4SequenceNumber & strSthoaRecordTypeA4DeliveryNote1 & strSthoaRecordTypeA4SuggestedOrderQuantityControlNumber & strSthoaRecordTypeA4InformationText ' & vbCrLf
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthoaText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthoaFileName, strSthoaText)
                                strSthoaText = String.Empty
                            End If
                        Next
                    End If
                End If
            End If
            If header.Type.Value = "0" Then
                If header.AssignedDate.Value < SysDates.Today.Value Then
                    If header.POSupplierBBC.Value = True Then
                        For Each detail As BOPurchases.cDrlDetail In header.Details
                            decDrldetValue = detail.ReceivedQty.Value * detail.SkuPrice.Value
                            strDrlsumReference = header.PODeliveryNote1.Value.ToString.Substring(0, 6).PadLeft(6, "0"c)
                            strDrlsumDailyReceiverListingNumber = "000000"
                            strDrlsumStoreNumber = "173"
                            strStockAdjustmentCode = "IN"
                            _STHOARecTypeA6SkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                            _STHOARecTypeA6AdjustmentCode = strStockAdjustmentCode
                            FormatIntToString(CInt(detail.ReceivedQty.Value), _STHOARecTypeA6AdjustmentUnits, 7, " "c)
                            FormatDecToString(decDrldetValue, _STHOARecTypeA6AdjustmentValue, 11, " "c, "0.00")
                            _STHOARecTypeA6AdjustmentReference = strDrlsumReference
                            FormatDecToString(detail.SkuPrice.Value, _STHOARecTypeA6ItemSellingPrice, 11, " "c, "0.00")
                            _STHOARecTypeA6AssemblyDepotNumber = strDrlsumStoreNumber
                            _STHOARecTypeA6TransferValue = String.Format("            ")
                            If decStockAdjustmentTransferValue <> +0 Then
                                FormatDecToString(decStockAdjustmentTransferValue, _STHOARecTypeA6TransferValue, 11, " "c, "0.00")
                            End If
                            _STHOARecTypeA6DRLNumber = header.Number.Value.ToString.PadLeft(6, "0"c)
                            _STHOARecTypeA6DRLNumber = "000000"
                            _STHOARecTypeA6CommentText = header.Comment.Value
                            _STHOARecTypeA6CommentText.Replace("+", OriginalSthoFileSeparator)
                            SetupHash("A6", decDrldetValue, header.AssignedDate.Value)
                            If strSthoaText <> String.Empty Then
                                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                            End If
                            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA6SkuNumber & _STHOARecTypeA6AdjustmentCode & _STHOARecTypeA6AdjustmentUnits & _STHOARecTypeA6AdjustmentValue & _STHOARecTypeA6AdjustmentReference & _STHOARecTypeA6ItemSellingPrice & _STHOARecTypeA6AssemblyDepotNumber & _STHOARecTypeA6TransferValue & _STHOARecTypeA6DRLNumber & _STHOARecTypeA6CommentText
                            intRecordsOutput = intRecordsOutput + 1
                            If strSthoaText.Length > intMaximumSthoOutputLength Then
                                PutSthoToDisc(strSthoaFileName, strSthoaText)
                                strSthoaText = String.Empty
                            End If
                        Next
                    End If
                End If
            End If
            If header.Type.Value = "1" Then
                For Each detail As BOPurchases.cDrlDetail In header.Details
                    decDrldetValue = detail.IbtInOutQty.Value * detail.SkuPrice.Value
                    strDrlsumReference = header.Number.Value.ToString.PadLeft(6, "0"c)
                    strDrlsumDailyReceiverListingNumber = header.ISTKeyedIn.Value.ToString.PadLeft(6, "0"c)
                    strDrlsumStoreNumber = header.ISTStoreNumber.Value.ToString.PadLeft(3, "0"c)
                    strStockAdjustmentCode = "12"
                    If header.IsProjectSalesOrder.Value = True Then
                        strStockAdjustmentCode = "HI"
                        If boolHomeDeliveryCentre = True Then
                            strStockAdjustmentCode = header.Comment.Value.Substring(0, 2)
                            strStockAdjustmentSign = header.Comment.Value.Substring(2, 1)
                        End If
                        If boolHomeDeliveryCentre = True Then
                            If strStockAdjustmentSign = "+" Then
                                strStockAdjustmentSign = " "
                            End If
                        End If
                    End If
                    _STHOARecTypeA6SkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                    _STHOARecTypeA6AdjustmentCode = strStockAdjustmentCode
                    FormatIntToString(CInt(detail.IbtInOutQty.Value), _STHOARecTypeA6AdjustmentUnits, 7, " "c)
                    FormatDecToString(decDrldetValue, _STHOARecTypeA6AdjustmentValue, 11, " "c, "0.00")
                    _STHOARecTypeA6AdjustmentReference = strDrlsumReference
                    FormatDecToString(detail.SkuPrice.Value, _STHOARecTypeA6ItemSellingPrice, 11, " "c, "0.00")
                    _STHOARecTypeA6AssemblyDepotNumber = strDrlsumStoreNumber
                    _STHOARecTypeA6TransferValue = String.Format("            ")
                    If decStockAdjustmentTransferValue <> +0 Then
                        FormatDecToString(decStockAdjustmentTransferValue, _STHOARecTypeA6TransferValue, 11, " "c, "0.00")
                    End If
                    _STHOARecTypeA6DRLNumber = header.ISTKeyedIn.Value.ToString.PadLeft(6, "0"c)
                    _STHOARecTypeA6CommentText = header.Comment.Value
                    _STHOARecTypeA6CommentText.Replace("+", OriginalSthoFileSeparator)
                    SetupHash("A6", decDrldetValue, header.AssignedDate.Value)
                    If strSthoaText <> String.Empty Then
                        If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA6SkuNumber & _STHOARecTypeA6AdjustmentCode & _STHOARecTypeA6AdjustmentUnits & _STHOARecTypeA6AdjustmentValue & _STHOARecTypeA6AdjustmentReference & _STHOARecTypeA6ItemSellingPrice & _STHOARecTypeA6AssemblyDepotNumber & _STHOARecTypeA6TransferValue & _STHOARecTypeA6DRLNumber & _STHOARecTypeA6CommentText
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(strSthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                Next
            End If
            If header.Type.Value = "2" Then
                For Each detail As BOPurchases.cDrlDetail In header.Details
                    decDrldetValue = (detail.IbtInOutQty.Value * detail.SkuPrice.Value) * -1
                    strDrlsumReference = header.Number.Value.ToString.PadLeft(6, "0"c)
                    strDrlsumDailyReceiverListingNumber = header.ISTKeyedIn.Value.ToString.PadLeft(6, "0"c)
                    strDrlsumStoreNumber = header.ISTStoreNumber.Value.ToString.PadLeft(3, "0"c)
                    strStockAdjustmentCode = "01"
                    If header.IsProjectSalesOrder.Value = True Then
                        strStockAdjustmentCode = "RO"
                        If boolHomeDeliveryCentre = True Then
                            strStockAdjustmentCode = header.Comment.Value.Substring(0, 2)
                            strStockAdjustmentSign = header.Comment.Value.Substring(2, 1)
                        End If
                        If boolHomeDeliveryCentre = True Then
                            If strStockAdjustmentSign = "+" Then
                                strStockAdjustmentSign = " "
                            End If
                        End If
                    End If
                    _STHOARecTypeA6SkuNumber = detail.SkuNumber.Value.ToString.PadLeft(6, "0"c)
                    _STHOARecTypeA6AdjustmentCode = strStockAdjustmentCode
                    FormatIntToString(CInt(detail.IbtInOutQty.Value * -1), _STHOARecTypeA6AdjustmentUnits, 7, " "c)
                    FormatDecToString(decDrldetValue, _STHOARecTypeA6AdjustmentValue, 11, " "c, "0.00")
                    _STHOARecTypeA6AdjustmentReference = strDrlsumReference
                    FormatDecToString(detail.SkuPrice.Value, _STHOARecTypeA6ItemSellingPrice, 11, " "c, "0.00")
                    _STHOARecTypeA6AssemblyDepotNumber = strDrlsumStoreNumber
                    _STHOARecTypeA6TransferValue = String.Format("            ")
                    If decStockAdjustmentTransferValue <> +0 Then
                        FormatDecToString(decStockAdjustmentTransferValue, _STHOARecTypeA6TransferValue, 11, " "c, "0.00")
                    End If
                    If header.IsProjectSalesOrder.Value = True Then
                        _STHOARecTypeA6DRLNumber = header.ISTKeyedIn.Value.ToString.PadLeft(6, "0"c)
                    Else
                        _STHOARecTypeA6DRLNumber = header.Number.Value.ToString.PadLeft(6, "0"c)
                    End If

                    _STHOARecTypeA6CommentText = header.Comment.Value
                    _STHOARecTypeA6CommentText.Replace("+", OriginalSthoFileSeparator)
                    SetupHash("A6", decDrldetValue, header.AssignedDate.Value)
                    If strSthoaText <> String.Empty Then
                        If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & _STHOARecTypeA6SkuNumber & _STHOARecTypeA6AdjustmentCode & _STHOARecTypeA6AdjustmentUnits & _STHOARecTypeA6AdjustmentValue & _STHOARecTypeA6AdjustmentReference & _STHOARecTypeA6ItemSellingPrice & _STHOARecTypeA6AssemblyDepotNumber & _STHOARecTypeA6TransferValue & _STHOARecTypeA6DRLNumber & _STHOARecTypeA6CommentText
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(strSthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                Next
            End If
            If header.Type.Value = "3" Then
                strDrlsumPurchaseOrderNumber = header.RetNumber.Value
                strDrlsumPurchaseOrderReleaseNumber = "99"
                strDrlsumSupplierNumber = header.RetSupplierNumber.Value
                dateDrlsumCreatedDate = header.RetDate.Value
                For Each detail As BOPurchases.cDrlDetail In header.Details
                    intDrldetOrderQuantity = CInt(detail.OrderQty.Value)
                    intDrldetReceivedQuantity = CInt(detail.ReturnQty.Value * -1)
                    decDrldetOrderPrice = detail.OrderPrice.Value
                    decDrldetSellingPrice = detail.SkuPrice.Value
                    intDrldetIBTQuantity = CInt(detail.IbtInOutQty.Value)
                    intDrldetReturnedQuantity = CInt(detail.ReturnQty.Value)
                    strDrldetSequenceNumber = detail.SequenceNumber.Value
                    strDrldetSkuNumber = detail.SkuNumber.Value
                    strDrlsumDeliveryNote1 = header.PODeliveryNote1.Value
                    strDrlsumSuggestedOrderQuantityControlNumber = header.POSOQNumber.Value
                    strDrlsumInformationText = header.Comment.Value.ToString.PadRight(16, " "c)
                    strDrlsumInformationText.Replace("+", OriginalSthoFileSeparator)
                    decHashValue = detail.ReturnQty.Value
                    'dateHashDate = dateDrlsumCreatedDate
                    dateHashDate = header.AssignedDate.Value
                    strSthoaRecordTypeA4PurchaseOrderNumber = strDrlsumPurchaseOrderNumber
                    strSthoaRecordTypeA4PurchaseOrderReleaseNumber = strDrlsumPurchaseOrderReleaseNumber
                    strSthoaRecordTypeA4SupplierNumber = strDrlsumSupplierNumber
                    strSthoaRecordTypeA4SkuNumber = strDrldetSkuNumber
                    strSthoaRecordTypeA4OrderQuantity = String.Empty
                    strSthoaRecordTypeA4ReceivedQuantity = String.Empty
                    FormatIntToString(CInt(detail.OrderQty.Value), strSthoaRecordTypeA4OrderQuantity, 7, " "c)
                    'FormatIntToString(detail.ReceivedQty.Value, strSthoaRecordTypeA4ReceivedQuantity, 7, " ")
                    FormatIntToString(CInt(detail.ReturnQty.Value * -1), strSthoaRecordTypeA4ReceivedQuantity, 7, " "c)

                    FormatDecToString(detail.OrderPrice.Value, strSthoaRecordTypeA4OrderPrice, 11, " "c, "0.00")
                    strSthoaRecordTypeA4DailyReceiverListingNumber = header.Number.Value.ToString.PadLeft(6, "0"c)
                    strSthoaRecordTypeA4OrderDate = dateDrlsumCreatedDate.ToString("dd/MM/yy")
                    FormatDecToString(detail.SkuPrice.Value, strSthoaRecordTypeA4NormalSellingPrice, 11, " "c, "0.00")
                    strSthoaRecordTypeA4SequenceNumber = strDrldetSequenceNumber.PadLeft(4, "0"c)
                    strSthoaRecordTypeA4DeliveryNote1 = strDrlsumDeliveryNote1.PadRight(10, " "c)
                    strSthoaRecordTypeA4SuggestedOrderQuantityControlNumber = strDrlsumSuggestedOrderQuantityControlNumber.PadRight(6, "0"c)
                    strSthoaRecordTypeA4InformationText = String.Empty & header.Comment.Value.ToString.PadRight(16, " "c)
                    strSthoaRecordTypeA4InformationText.Replace("+", OriginalSthoFileSeparator)
                    intJ = strSthoaRecordTypeA4InformationText.Length
                    For intI = 1 To intJ
                        If Mid(strSthoaRecordTypeA4InformationText, intI, 1) < " " Then
                            Mid(strSthoaRecordTypeA4InformationText, intI, 1) = " "
                        End If
                    Next
                    SetupHash("A4", decHashValue, dateHashDate)
                    If strSthoaText <> String.Empty Then
                        If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strSthoaRecordTypeA4PurchaseOrderNumber & strSthoaRecordTypeA4PurchaseOrderReleaseNumber & strSthoaRecordTypeA4SupplierNumber & strSthoaRecordTypeA4SkuNumber & strSthoaRecordTypeA4OrderQuantity & strSthoaRecordTypeA4ReceivedQuantity & strSthoaRecordTypeA4OrderPrice & strSthoaRecordTypeA4DailyReceiverListingNumber & strSthoaRecordTypeA4OrderDate & strSthoaRecordTypeA4NormalSellingPrice & strSthoaRecordTypeA4SequenceNumber & strSthoaRecordTypeA4DeliveryNote1 & strSthoaRecordTypeA4SuggestedOrderQuantityControlNumber & strSthoaRecordTypeA4InformationText
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(strSthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                Next
            End If
            'If boolFlagAsCommed = True Then
            '    Try
            '        header.CommedToHO.Value = True
            '        header.RTI.Value = "S"
            '        header.SaveIfExists()
            '        For Each detail As BOPurchases.cDrlDetail In header.Details
            '            detail.RTI.Value = "S"
            '            detail.SaveIfExists()
            '        Next
            '    Catch EX As Exception
            '    End Try
            'End If
        Next 'Processing DRLSUM
        If strSthoaText.Length > 0 Then
            PutSthoToDisc(strSthoaFileName, strSthoaText)
            strSthoaText = String.Empty
        End If
        strWorkString = "Processing STHOA - Receipts (DR) Ended : " & TimeOfDay.ToString("hh:mm:ss") & " Records Output to " & strSthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c) ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        If boolFlagAsCommed = True Then
            Try
                For Each header As BOPurchases.cDrlHeader In DRLHeader.Headers
                    If ((header.Type.Value = "0") AndAlso (header.AssignedDate.Value < SysDates.Today.Value)) Or ("123".Contains(header.Type.Value)) Then
                        header.UpdateCommed()
                    End If
                Next

            Catch ex As Exception

            End Try
        End If

    End Sub ' Process receipts for Transmission.

    Public Sub ProcessProductUpdates() ' Process data in HOSTU files

        Dim strTransmissionFileName As String = "HOSTU"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty

        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)

        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        Dim boolGotDataToProcess As Boolean = False

        Dim boolSetObsoleted As Boolean = False
        Dim boolProcessEventsWhenRequired As Boolean = False
        Dim intI As Integer

        arrstrRecordTypes = New String(15) {" ", "U1", "U4", "U5", "U6", "UA", "UB", "UC", "UD", "UE", "UM", "UT", " ", " ", " ", "UK"}

        Dim arrIntRecordTypeCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(15) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}

        Dim strTransmissionFileToOpen As String = strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"


        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)
            If File.Exists(strTibhueWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhueWorkFile)
            End If
            If File.Exists(strTibhusWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhusWorkFile)
            End If
            boolDoingAStoreLoad = False
            boolSetObsoleted = False
            boolProcessEventsWhenRequired = False
            boolOutputErrorToSthoa = False
            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            'Now Process the transmission files in date sequence from the Files to process array
            intPassNumber = 1
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file
            While intPassNumber < 3 ' Perform 2 Passes of the file
                _NoRecInTibhus = 0
                'intPassNumber = intPassNumber + 1
                If intPassNumber = 1 Then
                    Array.Clear(arrIntRecordTypeCounts, 0, 15)
                    Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
                    Array.Clear(arrdecRecordTypeHashes, 0, 15)
                    Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)
                    boolNeedToRunTibhue = False
                End If

                _NoRecInTibhue = 0
                strWorkString = "Validating :" & strCopyToFileName & " Pass Number" & intPassNumber.ToString.PadLeft(2, " "c) & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
                ProcessTransmissionsProgress.ProcessName.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                OutputSthoLog(strWorkString)
                TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations

                While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                    strTransmissionFileData = TransmissionFileReader.ReadLine.PadRight(450, " "c)
                    intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                    ProcessTransmissionsProgress.ProgressTextBox.Text = strTransmissionFileData
                    ProcessTransmissionsProgress.RecordCountTextBox.Text = intHeadOfficeNumberOfRecords.ToString & " - HOSTU"
                    ProcessTransmissionsProgress.Show()
                    If intPassNumber = 1 Then
                        intWhichRecordType = 0
                        For intWhichRecordType = 0 To 15
                            If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                                arrIntRecordTypeCounts(intWhichRecordType) += 1
                                arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                            End If
                        Next
                    End If
                    If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                        intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                        intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                        If (strTransmissionFileData.Substring(32, 4).ToUpper = "LOAD") And (intPassNumber = 1) Then
                            boolDoingAStoreLoad = True
                            Dim EventMaster As New BOEvent.cEvent(_Oasys3DB)
                            EventMaster.FlagAllAsDeleted()
                            Dim EventHeader As New BOEvent.cEventHeader(_Oasys3DB)
                            EventHeader.FlagAllAsDeleted()
                            Dim EventChange As New BOEvent.cEventPriceChange(_Oasys3DB)
                            EventChange.FlagAllAsDeleted()
                            Dim EventMixMatch As New BOEvent.cEventMixMatch(_Oasys3DB)
                            EventMixMatch.FlagAllAsDeleted()
                            Dim EventExcl As New BOEvent.cEventHierarchyExcl(_Oasys3DB)
                            EventExcl.FlagAllAsDeleted()
                            Dim EventDeals As New BOEvent.cEventDealGroup(_Oasys3DB)
                            EventDeals.FlagAllAsDeleted()
                            Dim CouponMaster As New BOEvent.cCouponMaster(_Oasys3DB)
                            CouponMaster.FlagAllAsDeleted()
                            Dim CouponText As New BOEvent.cCouponText(_Oasys3DB)
                            CouponText.FlagAllAsDeleted()
                            Dim StockMaster As New BOStock.cStock(_Oasys3DB)
                            StockMaster.FlagAllAsObselete(Sysdates.Today.Value)
                            Dim ModMaster As New BOStock.cModels(_Oasys3DB)
                            ModMaster.FlagAllAsObselete(Sysdates.Today.Value)
                            Dim RetOpt As New BOSystem.cRetailOptions(_Oasys3DB)
                            RetOpt.LoadMatches()
                            RetOpt.SetHOSTULoad(True)
                        End If
                    End If
                    If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                        Dim intTrailerLength As Integer = strTransmissionFileData.Length
                        intI = 32
                        While intI < (intTrailerLength - 32)
                            intWhichRecordType = 0
                            For intWhichRecordType = 1 To 14
                                Dim strTSTR As String = strTransmissionFileData.Substring(intI, 2)
                                If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                    If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                        If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                            arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                            arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                        End If
                                    End If
                                End If
                            Next
                            intI += 21
                        End While
                    End If
                    DataHOSTUIntegrity(strTransmissionFileData)
                End While
                'reader.Close() ' Close the transmission file - Hashes & record counts calculated

                If strTibhueText.Length > 0 Then
                    PutSthoToDisc(strTibhueWorkFile, strTibhueText)
                    strTibhueText = String.Empty
                End If
                If strTibhusText.Length > 0 Then
                    PutSthoToDisc(strTibhusWorkFile, strTibhusText)
                    strTibhusText = String.Empty
                End If
                strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                UpdateProgress(String.Empty, String.Empty, strWorkString)
                OutputSthoLog(strWorkString)

                'If Records extract into separate files then call processing routines
                If _NoRecInTibhus > 0 Then UpdateDataBaseFromTibhus()
                If _NoRecInTibhue > 0 Then UpdateDataBaseFromTibhue()

                TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
                If boolNeedSecondPass = False Then Exit While
                intPassNumber += 1
            End While ' Performing 2 passes of the file
            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 14
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next
            decHashValue = 1
            dateHashDate = Sysdates.Today.Value
            SetupHash("AU", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            'Added 13/10/09 - reset HO counter as this is 0 in example files
            intHeadOfficeNumberOfRecords = 0

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Partha Dutta
            ' Date        : 10/09/2010
            ' Referral No : 277B
            ' Notes       : Incorrect displacement
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'strSthoaText = strSthoaText.ToString.TrimEnd(" ") & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & strSthoRecordHashValue & intHeadOfficeNumberOfRecords.ToString.PadLeft(7, " ") & Space(1) & intHeadOfficeWrongStore.ToString.PadLeft(7, " ") & Space(1) & intHeadOfficeBadType.ToString.PadLeft(7, " ") & Space(1) & intHeadOfficeNewItems.ToString.PadLeft(7, " ") & Space(1) & intHeadOfficeBadNew.ToString.PadLeft(7, " ") & Space(1) & "      0       0       0       0" & Space(1) & intHeadOfficeChanges.ToString.PadLeft(7, " ") & Space(1) & intHeadOfficeBadChanges.ToString.PadLeft(7, " ") & Space(1) & "      0       0" & Space(1) & intHeadOfficeModels.ToString.PadLeft(7, " ") & "      0       0       0       0" & Space(1)
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & strSthoRecordHashValue & intHeadOfficeNumberOfRecords.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeWrongStore.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadType.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeNewItems.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadNew.ToString.PadLeft(7, " "c) & Space(1) & "      0       0       0       0" & Space(1) & intHeadOfficeChanges.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadChanges.ToString.PadLeft(7, " "c) & Space(1) & "      0       0" & Space(1) & intHeadOfficeModels.ToString.PadLeft(7, " "c) & Space(1) & "      0       0       0       0" & Space(1)

            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
            decHashValue = 1
            SetupHash("CU", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 15
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            boolCheckItemPrompts = True
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
            If (boolDoingAStoreLoad = True) Then
                ProcessEventPriceChangesPCBATP(Sysdates.Today.Value)
                Dim RetOpt As New BOSystem.cRetailOptions(_Oasys3DB)
                RetOpt.LoadMatches()
                RetOpt.SetHOSTULoad(False)
            End If
        End While ' Process the files

        Dim assemblyName As String = "UpdPriceChgFrmEvts.dll"
        Dim className As String = "UpdPriceChgFrmEvts.UpdPriceChgFrmEvts"
        Dim appName As String = "Process Events"
        Dim parameters As String = " (/P=',CFC')"
        Trace.WriteLine("Checking for Price Change Events")
        Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, 0, CInt(_WorkstationID), 9, parameters, "", False)
        'Dim hostForm As New Oasys.Windows.HostForm(assemblyName, className, appName, 0, _WorkstationID, 9, parameters, "", False)
        hostForm.ShowDialog()

        Dim CheckMissingEventsSQL As String = String.Empty
        CheckMissingEventsSQL = "SELECT stkmas.skun from stkmas left join EVTCHG on stkmas.skun=evtchg.skun and sdat<='" & Format(Today, "yyyy-MM-dd") & "' and edat is null where evtchg.skun IS NULL ORDER BY STKMAS.SKUN"
        Trace.WriteLine("Checking for Missing Load Events")
        Dim MissEvents As New Data.DataSet("MissSKU")
        MissEvents = _Oasys3DB.ExecuteSql(CheckMissingEventsSQL)
        SetupHash("CU", 1, Sysdates.Today.Value)
        For Each SKURec As Data.DataRow In MissEvents.Tables(0).Rows
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & "CU" & strSthoRecordDate & strSthoRecordHashValue & "I" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & "Missing Load Event" & CStr(SKURec(0)) & vbCrLf
        Next
        If strSthocText.Length > 0 Then PutSthoToDisc(strSthocFileName, strSthocText)
        strSthocText = String.Empty

    End Sub ' Process data in HOSTU files
    Private Sub ProcessEventPriceChangesPCBATP(ByVal SysDatToday As Date)

        Dim PriceChangeBO As New BOStock.cPriceChange(_Oasys3DB)

        PriceChangeBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PriceChangeBO.ChangeStatus, "U")
        PriceChangeBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        PriceChangeBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, PriceChangeBO.EffectiveDate, SysDatToday)
        PriceChangeBO.SortBy(PriceChangeBO.PartCode.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
        PriceChangeBO.SortBy(PriceChangeBO.EffectiveDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)

        Dim LastSKU As String = String.Empty
        Dim StockBO As New BOStock.cStock(_Oasys3DB)
        StockBO.SkuNumber.Value = "XXXXXX" 'force loop to load SKU
        For Each PriceChangeBO In PriceChangeBO.LoadMatches
            If (StockBO.SkuNumber.Value <> PriceChangeBO.PartCode.Value) Then
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.SkuNumber, PriceChangeBO.PartCode.Value)
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.RetailPriceEventNo, "000000")
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.RetailPricePriority, "00")
                StockBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.NormalSellPrice, PriceChangeBO.NewPrice.Value)
                StockBO.LoadMatches()
            End If
            If (StockBO.SkuNumber.Value = PriceChangeBO.PartCode.Value) Then
                If (LastSKU <> PriceChangeBO.PartCode.Value) Then
                    StockBO.UpdateRetailPrice(PriceChangeBO.EventNo.Value, PriceChangeBO.EventPriority.Value)
                    LastSKU = PriceChangeBO.PartCode.Value
                End If
                PriceChangeBO.ChangeStatus.Value = "A"
                PriceChangeBO.SmallLabel.Value = True
                PriceChangeBO.MediumLabel.Value = True
                PriceChangeBO.LargeLabel.Value = True
                PriceChangeBO.SaveIfExists()
            End If
        Next

    End Sub
    Public Sub ProcessStoreMasterUpdates() ' Process data in HOSTD files
        Dim strTransmissionFileName As String = "HOSTD"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Strmas As New BOStore.cStore(_Oasys3DB)
        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)

        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        ' The trailer record allows for up to 15 record type/count/hash combinations
        'Dim arrRecordTypes As String() = New String(15) {} 'Record Type(2) & Record Count (8) & hash (14.2)

        Dim intCharPos As Integer

        arrstrRecordTypes = New String(16) {" ", "SM", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        'Dim strTransmissionHeader As String ' Save the HEADER details here
        'Dim strTransmissionTrailer As String ' Save the TRAILER details here

        'Dim intLatestAvailableVersion As Integer ' Latest version number processed
        'Dim intLatestAvailableSequence As Integer ' Latest version number processed
        Dim strTransmissionFileToOpen As String = strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = strTransmissionsPath & "\"
        Dim strMyTvcPath As String = strTvcPath & strTransmissionFileName & ".TVC"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)
            If File.Exists(strTibhueWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhueWorkFile)
            End If
            If File.Exists(strTibhusWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhusWorkFile)
            End If
            boolDoingAStoreLoad = False
            'boolOutputErrorToSthoa = False
            boolOutputErrorToSthoa = True 'changed 4/11/09 as per Referral

            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)

            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file

            Array.Clear(arrintProcessedCounts, 0, 15)
            Array.Clear(arrintRejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)
            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                strTransmissionFileData = TransmissionFileReader.ReadLine
                strTransmissionFileData = strTransmissionFileData.PadRight(42, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 1 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrintProcessedCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        intHeadOfficeModels += 1 'Increment Processed Record count
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    boolDoingAStoreLoad = False
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                    If strTransmissionFileData.Substring(32, 4).ToUpper = "LOAD" Then
                        boolDoingAStoreLoad = True
                        strWorkString = "Processing :" & strCopyToFileName & " Store LOAD - Flagging STRMAS as DELETED : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        OutputSthoLog(strWorkString)
                        Strmas.FlagAllAsDeleted()
                    End If
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intCharPos = 32
                    While intCharPos < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intCharPos, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intCharPos, 1).ToUpper = "U" And strTransmissionFileData.Substring((intCharPos + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intCharPos, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intCharPos + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intCharPos + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intCharPos += 21
                    End While
                End If
                DataIntegrityHostd(strTransmissionFileData)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next

            decHashValue = intHeadOfficeNumberOfRecords
            dateHashDate = Sysdates.Today.Value
            SetupHash("AU", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "C" & strSthoRecordHashValue & intHeadOfficeNumberOfRecords.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeWrongStore.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadType.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeNewItems.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadNew.ToString.PadLeft(7, " "c) & Space(1) & "      0       0       0       0" & Space(1) & intHeadOfficeChanges.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadChanges.ToString.PadLeft(7, " "c) & Space(1) & "      0       0" & Space(1) & intHeadOfficeModels.ToString.PadLeft(7, " "c) & "       0       0       0       0" & Space(1)
            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
            dateHashDate = Sysdates.Today.Value
            decHashValue = intHeadOfficeNumberOfRecords
            SetupHash("CD", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "C" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
        End While ' Process the files

    End Sub ' Process data in HOSTD Files
    Public Sub PrintOrderConfirmationsOCDetails(ByVal strTestString As String, ByVal strSupplierName As String, ByVal dateOrdered As Date, ByVal dateExpected As Date, ByVal boolHOOrder As Boolean, ByRef HPSTOReportTable As Data.DataTable)
        Dim ReportLine As Data.DataRow = HPSTOReportTable.NewRow
        ReportLine("OrderNo") = IIf(strTestString.Substring(22, 6) = "0000000", strTestString.Substring(44, 6), strTestString.Substring(22, 6))
        ReportLine("Supplier") = strTestString.Substring(28, 5).PadLeft(5, "0"c) & Space(2) & strSupplierName
        ReportLine("OrderDate") = dateOrdered.ToString("dd/MM/yy")
        ReportLine("DueDate") = dateExpected.ToString("dd/MM/yy")
        If strTestString.Substring(41, 1) = "C" Then ReportLine("OrderStatus") = "Confirmed"
        If strTestString.Substring(41, 1) = "R" Then ReportLine("OrderStatus") = "Rejected"
        If strTestString.Substring(41, 1) = "A" And boolHOOrder = False Then ReportLine("OrderStatus") = "Amended"
        ReportLine("SKU") = ""
        ReportLine("OldQty") = ""
        ReportLine("NewQty") = ""
        ReportLine("Reason") = ""
        HPSTOReportTable.Rows.Add(ReportLine)

    End Sub
    Public Sub PrintOrderConfirmationsOADetails(ByVal strTestString As String, ByRef HPSTOReportTable As Data.DataTable)
        Dim ReportLine As Data.DataRow = HPSTOReportTable.NewRow
        ReportLine("OrderNo") = ""
        ReportLine("Supplier") = ""
        ReportLine("OrderDate") = ""
        ReportLine("DueDate") = ""
        ReportLine("OrderStatus") = ""
        ReportLine("SKU") = strTestString.Substring(39, 6)
        ReportLine("OldQty") = strTestString.Substring(49, 7)
        ReportLine("NewQty") = strTestString.Substring(56, 7)
        ReportLine("Reason") = strTestString.Substring(63, 2)
        intReportLineCount = intReportLineCount + 1
        HPSTOReportTable.Rows.Add(ReportLine)
    End Sub
    Public Sub ProcessOrderConfirmations() ' Process data in HPSTO files

        Dim strTransmissionFileName As String = "HPSTO"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Strmas As New BOStore.cStore(_Oasys3DB)
        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.Tomorrow)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()
        _ReportDate = Sysdates.Tomorrow.Value.ToString("dd/MM/yy")
        intReportPageNumber = 0

        Dim boolGotDataToProcess As Boolean = False
        Dim intI As Integer
        arrstrRecordTypes = New String(16) {" ", "OC", "OA", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", " ", " ", " ", "  ", "  ", "  "}
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        '        Dim strTibhpoReportText As String = String.Empty

        Dim strTransmissionFileToOpen As String = strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        '        strTibhpoReportPath = strReportSpoolPath & "\TIBHPO.PRN"
        Dim strMyTvcPath As String = strTvcPath & strTransmissionFileName & ".TVC"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        If File.Exists(strTibhpoReportPath) Then My.Computer.FileSystem.DeleteFile(strTibhpoReportPath)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)

            Dim HPSTOReportData As Data.DataTable = CreateHPSTOTable() 'used to Purchase Order Summary

            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            _ExpectingOARecords = False
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file

            Array.Clear(arrintProcessedCounts, 0, 15)
            Array.Clear(arrintRejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)
            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                strTransmissionFileData = TransmissionFileReader.ReadLine
                strTransmissionFileData = strTransmissionFileData.PadRight(42, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 0 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrintProcessedCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                DataIntegrityHpsto(strTransmissionFileData, DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1, HPSTOReportData)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") & " Records Processed : " & Space(1) & intHeadOfficeNumberOfRecords.ToString.PadLeft(6, " "c)
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next
            '            decHashValue = intHeadOfficeNumberOfRecords
            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1
            dateHashDate = Sysdates.Today.Value
            SetupHash("OB", decHashValue, dateHashDate)
            If strSthpoText <> String.Empty Then
                strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthpoText = strSthpoText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthpoText = strSthpoText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthpoText.Length > 0 Then
                PutSthoToDisc(strSthpoFileName, strSthpoText)
                strSthpoText = String.Empty
            End If
            dateHashDate = Sysdates.Today.Value
            'decHashValue = intHeadOfficeNumberOfRecords
            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1
            SetupHash("CO", decHashValue, dateHashDate)
            If strSthocText.Trim <> String.Empty Then
                If strSthocText.Trim.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
            If (HPSTOReportData.Rows.Count > 0) Then
                Dim HPSTOForm As New HPSTOReport
                HPSTOForm.ReportSource = HPSTOReportData
                HPSTOForm.StoreID = _StoreNumber & "-" & _StoreName
                HPSTOForm.ShowDialog()
                HPSTOForm.Dispose()
            End If
        End While ' Process the files

    End Sub ' Process data in HPSTO Files
    Public Sub ProcessBBCIssues() ' Process data in HPSTI files

        Dim strTransmissionFileName As String = "HPSTI"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Strmas As New BOStore.cStore(_Oasys3DB)
        Dim Retopt As New BOSystem.cRetailOptions(_Oasys3DB)
        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadField(Retopt.StoreName)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()
        'strStoreNumber = String.Empty & Retopt.Store.Value.ToString.PadLeft(3, "0")
        'strStoreName = String.Empty & Retopt.StoreName.Value.PadRight(30, " ")
        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.Tomorrow)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()
        _ReportDate = Sysdates.Tomorrow.Value.ToString("dd/MM/yy")
        intReportPageNumber = 0

        Dim boolGotDataToProcess As Boolean = False
        Dim intI As Integer
        arrstrRecordTypes = New String(16) {" ", "CS", "CD", "II", "ID", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrFilesToCheck As New ArrayList
        Dim arrstrTrailerHashes() As String = New String(15) {}
        Dim strTibhpoReportText As String = String.Empty

        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        If File.Exists(strTibhpoReportPath) Then
            My.Computer.FileSystem.DeleteFile(strTibhpoReportPath)
        End If
        'PrintOrderConfirmationsHeader(True)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)

            Dim HPSTIReportData As Data.DataTable = CreateHPSTITable() 'used to build Container Report

            If File.Exists(strTibhueWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhueWorkFile)
            End If
            If File.Exists(strTibhusWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhusWorkFile)
            End If

            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            _ExpectingOARecords = False
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file

            Array.Clear(arrintProcessedCounts, 0, 15)
            Array.Clear(arrintRejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)
            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                strTransmissionFileData = TransmissionFileReader.ReadLine
                strTransmissionFileData = strTransmissionFileData.PadRight(42, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 0 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        'arrintProcessedCounts(intWhichRecordType) +=  1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                DataIntegrityHpsti(strTransmissionFileData, HPSTIReportData, DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") & " Records Processed : " & Space(1) & intHeadOfficeNumberOfRecords.ToString.PadLeft(6, " "c)
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next

            dateHashDate = Sysdates.Today.Value
            decHashValue = intHeadOfficeNumberOfRecords
            SetupHash("CI", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If

            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If

            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
            If (HPSTIReportData.Rows.Count > 0) Then
                Dim HPSTIForm As New HPSTIReport
                HPSTIForm.ReportSource = HPSTIReportData
                HPSTIForm.StoreID = _StoreNumber & "-" & _StoreName
                HPSTIForm.ShowDialog()
                HPSTIForm.Dispose()
            End If
        End While ' Process the files

    End Sub ' Process data in HPSTI Files
    Public Sub ProcessPlangramUpdates() ' Process data in HOSTF files
        Dim strTransmissionFileName As String = "HOSTF"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Plangram As New BOStockLocation.cPlanGram(_Oasys3DB)
        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        ' The trailer record allows for up to 15 record type/count/hash combinations
        Dim boolGotDataToProcess As Boolean = False
        Dim intI As Integer

        arrstrRecordTypes = New String(16) {" ", "EL", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}

        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)
            'strFromRtiFile = String.Empty & strFromRtiPath & "\" & strTransmissionFileName & Retopt.Store.Value.ToString.PadLeft(3, "0") & "." & intLatestAvailableVersion.ToString.PadLeft(2, "0")
            If File.Exists(strTibhueWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhueWorkFile)
            End If
            If File.Exists(strTibhusWorkFile) Then
                My.Computer.FileSystem.DeleteFile(strTibhusWorkFile)
            End If
            boolDoingAStoreLoad = False
            boolOutputErrorToSthoa = True 'changed 26/10/09 as per Referral
            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            'Now Process the transmission files in date sequence from the Files to process array
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file
            Array.Clear(arrintProcessedCounts, 0, 15)
            Array.Clear(arrintRejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)

            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            Dim LastPlangramID As Dictionary(Of String, Integer) = Nothing
            Dim LastPlanNo As String = ""
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                strTransmissionFileData = TransmissionFileReader.ReadLine
                strTransmissionFileData = strTransmissionFileData.PadRight(42, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 1 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        arrintProcessedCounts(intWhichRecordType) += 1
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then intHeadOfficeModels += 1
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                    strWorkString = "Processing :" & strCopyToFileName & " Deleting ALL Plangram records : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                    OutputSthoLog(strWorkString)
                    Plangram.DeleteAll()
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                DataIntegrityHostf(strTransmissionFileData, LastPlanNo, LastPlangramID)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            'After processing file then check Totals against TR Record Totals
            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                    Exit For
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                    Exit For
                End If
            Next

            decHashValue = intHeadOfficeNumberOfRecords
            dateHashDate = Sysdates.Today.Value
            SetupHash("AX", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & strSthoRecordHashValue & intHeadOfficeNumberOfRecords.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeWrongStore.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadType.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeNewItems.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadNew.ToString.PadLeft(7, " "c) & Space(1) & "      0       0       0       0" & Space(1) & intHeadOfficeChanges.ToString.PadLeft(7, " "c) & Space(1) & intHeadOfficeBadChanges.ToString.PadLeft(7, " "c) & Space(1) & "      0       0" & Space(1) & intHeadOfficeModels.ToString.PadLeft(7, " "c) & "       0       0       0       0" & Space(1)
            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
            dateHashDate = Sysdates.Today.Value
            decHashValue = intHeadOfficeNumberOfRecords
            SetupHash("CX", decHashValue, dateHashDate)
            If strSthocText.Trim <> String.Empty Then 'previous output so end line with CR
                If strSthocText.Trim.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.TrimEnd & vbCrLf
            End If
            strSthocText = strSthocText.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If strPendingHpstvText.Length > 0 Then
                PutSthoToDisc(strPendingHpstvFileName, strPendingHpstvText)
                strPendingHpstvText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
        End While ' Process the files

    End Sub ' Process data in HOSTF Files

    Private Sub UpdateProgress(ByVal ProgressText As String, ByVal RecordCount As String, ByVal ProcessName As String)

        ProcessTransmissionsProgress.ProgressTextBox.Text = ProgressText
        ProcessTransmissionsProgress.RecordCountTextBox.Text = RecordCount
        ProcessTransmissionsProgress.ProcessName.Text = ProcessName
        ProcessTransmissionsProgress.Show()

    End Sub

    Public Sub ProcessCountCycleUpdates() ' Process data in HOSTC files
        Dim strTransmissionFileName As String = "HOSTC"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim PicControl As New BOStockTake.cPicControl(_Oasys3DB)
        Dim ColPi As New List(Of BOStockTake.cPicControl)
        Dim PicAudit As New BOStockTake.cPicAudit(_Oasys3DB)
        Dim ColPa As New List(Of BOStockTake.cPicAudit)
        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        Dim boolGotDataToProcess As Boolean = False
        Dim boolProcessCCRecordsAllowed As Boolean = False
        Dim intI As Integer

        arrstrRecordTypes = New String(16) {" ", "CY", "CC", "CM", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        'Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)

            boolDoingAStoreLoad = False

            boolOutputErrorToSthoa = False
            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file

            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)

            strWorkString = "Validating :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                strTransmissionFileData = TransmissionFileReader.ReadLine
                intHeadOfficeNumberOfRecords += 1
                ProcessTransmissionsProgress.ProgressTextBox.Text = strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 0 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        'arrintProcessedCounts(intWhichRecordType) += 1 'done in Validate Routine
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                DataIntegrityHostc(strTransmissionFileData)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            boolGotDataToProcess = True
            If intCYProcessedCount <> 1 And intCMProcessedCount < 1 Then boolGotDataToProcess = False
            If intCYRejectedCount <> 0 And intCMProcessedCount < 1 Then boolGotDataToProcess = False
            If intCCProcessedCount < 1 And intCMProcessedCount < 1 Then boolGotDataToProcess = False
            If intCCRejectedCount <> 0 And intCMProcessedCount < 1 Then boolGotDataToProcess = False

            If boolGotDataToProcess = True Then
                strWorkString = "Processing of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                UpdateProgress(String.Empty, String.Empty, strWorkString)
                OutputSthoLog(strWorkString)
                'intCYProcessedCount = 0
                'intCCProcessedCount = 0
                'intCMProcessedCount = 0
                boolProcessCCRecordsAllowed = False
                If intCCProcessedCount > 0 And intCCRejectedCount < 1 Then
                    boolProcessCCRecordsAllowed = True
                    'PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.Deleted, False)
                    'ColPi = PicControl.LoadMatches()
                    'If ColPi.Count > 0 Then
                    '    For Each picctl As BOStockTake.cPicControl In ColPi
                    '        PicControl.Deleted.Value = True
                    '        PicControl.SaveIfExists()
                    '    Next
                    'End If
                    PicControl.FlagAllAsDeleted()
                End If
                TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
                While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                    strTransmissionFileData = TransmissionFileReader.ReadLine
                    If strTransmissionFileData.StartsWith("CY") And boolProcessCCRecordsAllowed = True Then
                        Try
                            Sysdates.WeeksInCycle.Value = CDec(strTransmissionFileData.Substring(22, 2).PadLeft(2, "0"c))
                            If Sysdates.WeekCycleNumber.Value > Sysdates.WeeksInCycle.Value Then Sysdates.WeekCycleNumber.Value = Sysdates.WeeksInCycle.Value - 1
                            Sysdates.SaveIfExists()
                        Catch ex As Exception
                        End Try
                    End If
                    If strTransmissionFileData.StartsWith("CC") And boolProcessCCRecordsAllowed = True Then
                        Dim strMyCategory As String = strTransmissionFileData.Substring(24, 6).PadLeft(6, "0"c)
                        Dim strMyGroup As String = strTransmissionFileData.Substring(30, 6).PadLeft(6, "0"c)
                        Dim strMySubGroup As String = strTransmissionFileData.Substring(36, 6).PadLeft(6, "0"c)
                        Dim strMyStyle As String = strTransmissionFileData.Substring(42, 6).PadLeft(6, "0"c)
                        PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.CycleDayNumber, CInt(strTransmissionFileData.Substring(22, 2)))
                        PicControl.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.HieCategory, strTransmissionFileData.Substring(24, 6).PadLeft(6, "0"c))
                        PicControl.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.HieGroup, strTransmissionFileData.Substring(30, 6).PadLeft(6, "0"c))
                        PicControl.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.HieSubgroup, strTransmissionFileData.Substring(36, 6).PadLeft(6, "0"c))
                        PicControl.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.HieStyle, strTransmissionFileData.Substring(42, 6).PadLeft(6, "0"c))
                        ColPi = PicControl.LoadMatches()
                        If ColPi.Count < 1 Then
                            Try
                                PicControl.CycleDayNumber.Value = CInt(strTransmissionFileData.Substring(22, 2).PadLeft(2, "0"c))
                                PicControl.HieCategory.Value = strMyCategory
                                PicControl.HieGroup.Value = strMyGroup
                                PicControl.HieSubgroup.Value = strMySubGroup
                                PicControl.HieStyle.Value = strMyStyle
                                PicControl.Deleted.Value = False
                                PicControl.SaveIfNew()
                            Catch ex As Exception
                            End Try
                        End If
                        If ColPi.Count > 0 Then
                            Try
                                PicControl.Deleted.Value = False
                                PicControl.SaveIfExists()
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                    If strTransmissionFileData.StartsWith("CM") Then
                        If ValidateCMData(strTransmissionFileData.PadRight(39, " "c), False) = True Then
                            Dim dateAuditDate As Date = CDate(strTransmissionFileData.Substring(22, 10))
                            PicAudit.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicAudit.AuditDate, dateAuditDate)
                            PicAudit.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            PicAudit.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicAudit.SkuNumber, strTransmissionFileData.Substring(32, 6).PadLeft(6, "0"c))
                            ColPa = PicAudit.LoadMatches()
                            If ColPa.Count < 1 And strTransmissionFileData.Substring(38, 1) <> "D" Then
                                Try
                                    PicAudit.AuditDate.Value = CDate(strTransmissionFileData.Substring(22, 10))
                                    PicAudit.SkuNumber.Value = strTransmissionFileData.Substring(32, 6)
                                    PicAudit.Deleted.Value = False
                                    PicAudit.OriginCode.Value = "H"
                                    PicAudit.SaveIfNew()
                                Catch ex As Exception
                                End Try
                            End If
                            If ColPa.Count > 0 Then
                                Try
                                    PicAudit.Deleted.Value = False
                                    If strTransmissionFileData.Substring(38, 1) = "D" Then PicAudit.Deleted.Value = True
                                    PicAudit.OriginCode.Value = "H"
                                    PicAudit.SaveIfExists()
                                Catch ex As Exception
                                End Try
                            End If
                        End If
                    End If
                End While

            End If
            'reader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            'PicControl.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, PicControl.Deleted, True)
            'ColPi = PicControl.LoadMatches()
            'If ColPi.Count > 0 Then
            '    For Each picctl As BOStockTake.cPicControl In ColPi
            '        PicControl.Delete()
            '    Next
            'End If
            PicControl.RemoveAllFlaggedDelete()

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                    Exit For
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                    Exit For
                End If
            Next
            dateHashDate = Sysdates.Today.Value
            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1
            SetupHash("CP", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
        End While ' Process the files

    End Sub ' Process data in HOSTC files

    Public Sub UpdateHierarchyFiles()
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)

        Dim Hiecat As New BOHierarchy.cHierachyCategory(_Oasys3DB)
        Dim Hiegrp As New BOHierarchy.cHierachyGroup(_Oasys3DB)
        Dim Hiesgp As New BOHierarchy.cHierachySubgroup(_Oasys3DB)
        Dim Hiesty As New BOHierarchy.cHierachyStyle(_Oasys3DB)
        Dim Hiemas As New BOHierarchy.cHierarchyMaster(_Oasys3DB)

        strWorkString = "GSBUHL - UPDATING HIERARCHY FILES STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'get list of all unique Department Categories from Stock Master
        StockMaster.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggDistinct, StockMaster.HierCategory, StockMaster.HierCategory.ColumnName)
        Dim dsCategories As System.Data.DataSet = StockMaster.GetAggregateDataSet

        strWorkString = "GSBUHL - UPDATING HIERARCHY CATEGORY STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'Loop through Categories and check they Exist is HIEMAS and HIECAT
        For Each row As Data.DataRow In dsCategories.Tables(0).Rows
            'Load HIEMAS entry to see if Hierarchy Code exists
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.MemberNumber, CStr(row.Item(0)))
            Hiemas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.Level, "5")
            If (Hiemas.LoadMatches.Count < 1) Then
                Try
                    Hiemas.MemberNumber.Value = CStr(row.Item(0))
                    Hiemas.Level.Value = "5"
                    Hiemas.Description.Value = String.Format("Category {0}", row.Item(0))
                    Hiemas.IsDeleted.Value = False
                    Hiemas.MaxOverride.Value = 0
                    For RedWeek As Integer = 1 To 10 Step 1
                        Hiemas.ReductionWeek(RedWeek).Value = 0
                    Next RedWeek
                    If (CInt(row.Item(0)) > 0) Then Hiemas.SaveIfNew()
                Catch ex As Exception
                End Try
            End If 'check if exists in HIEMAS
            'Check if exists in HIECAT and Create/Update if required
            Hiecat.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiecat.Category, CStr(row.Item(0)))
            If (Hiecat.LoadMatches.Count < 1) Then
                Try
                    'create new entry
                    Hiecat.Category.Value = Hiemas.MemberNumber.Value
                    Hiecat.Description.Value = Hiemas.Description.Value
                    Hiecat.Alpha.Value = Hiemas.Description.Value.ToUpper
                    If (Hiemas.MemberNumber.Value > "000000") Then Hiecat.SaveIfNew()
                Catch ex As Exception
                End Try
            Else
                Try
                    'Already exists so check for any changes and update if required
                    If (Hiecat.Description.Value <> Hiemas.Description.Value) Or _
                            (Hiecat.Alpha.Value <> Hiemas.Description.Value.ToUpper) Then
                        Hiecat.Description.Value = Hiemas.Description.Value
                        Hiecat.Alpha.Value = Hiemas.Description.Value.ToUpper
                        Hiecat.SaveIfExists()
                    End If 'update HIECAT if value have been updated
                Catch ex As Exception
                End Try
            End If

        Next 'Department Category to check
        dsCategories.Dispose()


        strWorkString = "GSBUHL - UPDATING HIERARCHY GROUPS STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'get list of all unique Department Groups
        StockMaster.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggDistinct, StockMaster.HierGroup, StockMaster.HierGroup.ColumnName)
        Dim dsGroups As System.Data.DataSet = StockMaster.GetAggregateDataSet

        'Loop through Hierarchy Groups and check they Exist is HIEMAS and HIEGRP
        For Each row As Data.DataRow In dsGroups.Tables(0).Rows
            'Load HIEMAS entry to see if Hierarchy Code exists
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.MemberNumber, CStr(row.Item(0)))
            Hiemas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.Level, "4")
            If (Hiemas.LoadMatches.Count < 1) Then
                Try
                    Hiemas.MemberNumber.Value = CStr(row.Item(0))
                    Hiemas.Level.Value = "4"
                    Hiemas.Description.Value = String.Format("Group ", row.Item(0))
                    Hiemas.IsDeleted.Value = False
                    Hiemas.MaxOverride.Value = 0
                    For RedWeek As Integer = 1 To 10 Step 1
                        Hiemas.ReductionWeek(RedWeek).Value = 0
                    Next RedWeek
                    If (CInt(row.Item(0)) > 0) Then Hiemas.SaveIfNew()
                Catch ex As Exception
                End Try
            End If 'check if exists in HIEMAS
            'Get Hierarchy Code and update Master
            Hiegrp.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiegrp.Group, CStr(row.Item(0)))
            If (Hiegrp.LoadMatches.Count < 1) Then
                Try
                    'Access STKMAS to get Category for Group, for saving to DB
                    Dim StockBO As New BOStock.cStock(_Oasys3DB)
                    StockBO.AddLoadField(StockBO.HierCategory)
                    StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.HierGroup, CStr(row.Item(0)))
                    StockBO.LoadMatches(1)
                    Hiegrp.Category.Value = StockBO.HierCategory.Value
                    Hiegrp.Group.Value = Hiemas.MemberNumber.Value
                    Hiegrp.Description.Value = Hiemas.Description.Value
                    Hiegrp.Alpha.Value = Hiemas.Description.Value.ToUpper
                    If (CInt(Hiegrp.Category.Value) > 0) Then Hiegrp.SaveIfNew()
                    StockBO.Dispose()
                Catch ex As Exception
                End Try
            Else
                Try
                    'Already exists so check for any changes and update if required
                    If (Hiegrp.Description.Value <> Hiemas.Description.Value) Or _
                            (Hiegrp.Alpha.Value <> Hiemas.Description.Value.ToUpper) Then
                        Hiegrp.Description.Value = Hiemas.Description.Value
                        Hiegrp.Alpha.Value = Hiemas.Description.Value.ToUpper
                        Hiegrp.SaveIfExists()
                    End If 'update HIEGRP if value has changed
                Catch ex As Exception
                End Try
            End If

        Next 'Department Group to process


        strWorkString = "GSBUHL - UPDATING HIERARCHY SUB GROUPS STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'get list of all unique Department Sub Groups
        StockMaster.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggDistinct, StockMaster.HierSubGroup, StockMaster.HierSubGroup.ColumnName)
        Dim dsSubGroups As System.Data.DataSet = StockMaster.GetAggregateDataSet

        'Loop through Hierarchy Groups and check they Exist is HIEMAS and HIEGRP
        For Each row As Data.DataRow In dsSubGroups.Tables(0).Rows
            'Load HIEMAS entry to see if Hierarchy Code exists
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.MemberNumber, CStr(row.Item(0)))
            Hiemas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.Level, "3")
            If (Hiemas.LoadMatches.Count < 1) Then
                Try
                    Hiemas.MemberNumber.Value = CStr(row.Item(0))
                    Hiemas.Level.Value = "3"
                    Hiemas.Description.Value = String.Format("Sub Group ", row.Item(0))
                    Hiemas.IsDeleted.Value = False
                    Hiemas.MaxOverride.Value = 0
                    For RedWeek As Integer = 1 To 10 Step 1
                        Hiemas.ReductionWeek(RedWeek).Value = 0
                    Next RedWeek
                    If CInt(row.Item(0)) > 0 Then Hiemas.SaveIfNew()
                Catch ex As Exception
                End Try
            End If 'check if exists in HIEMAS
            'Get Hierarchy Code and update Master
            Hiesgp.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiesgp.SubGroup, CStr(row.Item(0)))
            If (Hiesgp.LoadMatches.Count < 1) Then
                Try
                    'Access STKMAS to get Category for Group, for saving to DB
                    Dim StockBO As New BOStock.cStock(_Oasys3DB)
                    StockBO.AddLoadField(StockBO.HierCategory)
                    StockBO.AddLoadField(StockBO.HierGroup)
                    StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.HierSubGroup, CStr(row.Item(0)))
                    StockBO.LoadMatches(1)
                    Hiesgp.Category.Value = StockBO.HierCategory.Value
                    Hiesgp.Group.Value = StockBO.HierGroup.Value
                    Hiesgp.SubGroup.Value = Hiemas.MemberNumber.Value
                    Hiesgp.Description.Value = Hiemas.Description.Value
                    Hiesgp.Alpha.Value = Hiemas.Description.Value.ToUpper
                    If (Hiesgp.SubGroup.Value > "000000") Then Hiesgp.SaveIfNew()
                    StockBO.Dispose()
                Catch ex As Exception
                End Try
            Else
                Try
                    'Already exists so check for any changes and update if required
                    If (Hiesgp.Description.Value <> Hiemas.Description.Value) Or _
                            (Hiesgp.Alpha.Value <> Hiemas.Description.Value.ToUpper) Then
                        Hiesgp.Description.Value = Hiemas.Description.Value
                        Hiesgp.Alpha.Value = Hiemas.Description.Value.ToUpper
                        Hiesgp.SaveIfExists()
                    End If 'update HIESGRP if value has changed
                Catch ex As Exception
                End Try
            End If

        Next 'Department SubGroup to process

        strWorkString = "GSBUHL - UPDATING HIERARCHY STYLES STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        'get list of all unique Department Styles
        StockMaster.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggDistinct, StockMaster.HierStyle, StockMaster.HierStyle.ColumnName)
        Dim dsStyles As System.Data.DataSet = StockMaster.GetAggregateDataSet

        'Loop through Hierarchy Groups and check they Exist is HIEMAS and HIEGRP
        For Each row As Data.DataRow In dsStyles.Tables(0).Rows
            'Load HIEMAS entry to see if Hierarchy Code exists
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.MemberNumber, CStr(row.Item(0)))
            Hiemas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.Level, "2")
            If (Hiemas.LoadMatches.Count < 1) Then
                Try
                    Hiemas.MemberNumber.Value = CStr(row.Item(0))
                    Hiemas.Level.Value = "2"
                    Hiemas.Description.Value = String.Format("Style ", row.Item(0))
                    Hiemas.IsDeleted.Value = False
                    Hiemas.MaxOverride.Value = 0
                    For RedWeek As Integer = 1 To 10 Step 1
                        Hiemas.ReductionWeek(RedWeek).Value = 0
                    Next RedWeek
                    If (CInt(row.Item(0)) > 0) Then Hiemas.SaveIfNew()
                Catch ex As Exception
                End Try
            End If 'check if exists in HIEMAS
            'Get Hierarchy Code and update Master
            Hiesty.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiesty.Style, CStr(row.Item(0)))
            If (Hiesty.LoadMatches.Count < 1) Then
                Try
                    'Access STKMAS to get Category for Group, for saving to DB
                    Dim StockBO As New BOStock.cStock(_Oasys3DB)
                    StockBO.AddLoadField(StockBO.HierCategory)
                    StockBO.AddLoadField(StockBO.HierGroup)
                    StockBO.AddLoadField(StockBO.HierSubGroup)
                    StockBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockBO.HierStyle, CStr(row.Item(0)))
                    StockBO.LoadMatches(1)
                    Hiesty.Category.Value = StockBO.HierCategory.Value
                    Hiesty.Group.Value = StockBO.HierGroup.Value
                    Hiesty.SubGroup.Value = StockBO.HierSubGroup.Value
                    Hiesty.Style.Value = Hiemas.MemberNumber.Value
                    Hiesty.Description.Value = Hiemas.Description.Value
                    Hiesty.Alpha.Value = Hiemas.Description.Value.ToUpper
                    If (CInt(Hiesty.Category.Value) > 0) Then Hiesty.SaveIfNew()
                    StockBO.Dispose()
                Catch ex As Exception
                End Try
            Else
                Try
                    'Already exists so check for any changes and update if required
                    If (Hiesty.Description.Value <> Hiemas.Description.Value) Or _
                            (Hiesty.Alpha.Value <> Hiemas.Description.Value.ToUpper) Then
                        Hiesty.Description.Value = Hiemas.Description.Value
                        Hiesty.Alpha.Value = Hiemas.Description.Value.ToUpper
                        Hiesty.SaveIfExists()
                    End If 'update HIESTY if value has changed
                Catch ex As Exception
                End Try
            End If

        Next 'Department Styles to process

        strWorkString = "GSBUHL - UPDATING HIERARCHY FILES COMPLETED : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
    End Sub

    Public Sub ResetStockHierarchyIndex()
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        Dim Stkhir As New BOHierarchy.cHierarchyXref(_Oasys3DB)
        Dim ColIr As New List(Of BOHierarchy.cHierarchyXref)
        Dim intX As Integer = 0
        Dim intY As Integer = 0
        Dim intHighSku As Integer = 999999
        strWorkString = "TIBRSH - UPDATING HIERARCHY FILES STARTS : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
        Stkhir.DeleteAll()

        'While intY < intHighSku
        '    intY = intX + 10000
        '    If intY > intHighSku Then
        '        intY = intHighSku
        '    End If
        '    strWorkString = "TIBRSH - Clearing Out SKUS : " & Space(1) & intX.ToString.PadLeft(6, "0") & " To" & Space(1) & intY.ToString.PadRight(6, "0")
        '    OutputSthoLog(strWorkString)
        '    Stkhir.ClearLoadFilter()
        '    Stkhir.ClearLists()

        '    Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, Stkhir.SkuNumber, intX.ToString.PadLeft(6, "0"))
        '    Stkhir.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '    Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intY.ToString.PadLeft(6, "0"))
        '    ColIr = Stkhir.LoadMatches()
        '    For Each record As BOHierarchy.cHierarchyXref In ColIr
        '        record.Delete()
        '    Next
        '    intX = intY
        'End While
        strWorkString = "TIBRSH - Clearing Out SKUS Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)

        Dim SQLHier As String = "INSERT INTO " & Stkhir.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ","
        Dim SQLHier2 As String = ",0,'' FROM " & StockMaster.TableName & " WHERE "
        Dim SQLCat As String = SQLHier & StockMaster.HierCategory.ColumnName & SQLHier2 & StockMaster.HierCategory.ColumnName & ">'000000'"
        Dim SQLGroup As String = SQLHier & StockMaster.HierGroup.ColumnName & SQLHier2 & StockMaster.HierGroup.ColumnName & ">'000000'"
        Dim SQLSubGroup As String = SQLHier & StockMaster.HierSubGroup.ColumnName & SQLHier2 & StockMaster.HierSubGroup.ColumnName & ">'000000'"
        Dim SQLStyle As String = SQLHier & StockMaster.HierStyle.ColumnName & SQLHier2 & StockMaster.HierStyle.ColumnName & ">'000000'"

        _Oasys3DB.ExecuteSql(SQLCat)
        _Oasys3DB.ExecuteSql(SQLGroup)
        _Oasys3DB.ExecuteSql(SQLSubGroup)
        _Oasys3DB.ExecuteSql(SQLStyle)


        'intX = 0
        'intY = 0
        'While intY < intHighSku
        '    strWorkString = "TIBRSH - Adding SKUs TO STKHIR - Processing Sku Range : " & Space(1) & intX.ToString.PadLeft(6, "0") & " To" & Space(1) & intY.ToString.PadRight(6, "0")
        '    UpdateProgress(String.Empty, String.Empty, strWorkString)
        '    OutputSthoLog(strWorkString)
        '    intY = intX + 10000
        '    If intY > intHighSku Then
        '        intY = intHighSku
        '    End If
        '    StockMaster.ClearLoadFilter()
        '    StockMaster.ClearLists()
        '    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SkuNumber, intX.ToString.PadLeft(6, "0"))
        '    StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intY.ToString.PadLeft(6, "0"))
        '    StockMaster.AddLoadField(StockMaster.SkuNumber)
        '    StockMaster.AddLoadField(StockMaster.HierCategory)
        '    StockMaster.AddLoadField(StockMaster.HierGroup)
        '    StockMaster.AddLoadField(StockMaster.HierStyle)
        '    StockMaster.AddLoadField(StockMaster.HierSubGroup)
        '    '            StockMaster.SortBy(StockMaster.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        '    StockMaster.Stocks = StockMaster.LoadMatches()
        '    For Each stocks As BOStock.cStock In StockMaster.Stocks
        '        Stkhir.SkuNumber.Value = stocks.SkuNumber.Value
        '        Stkhir.IsDeleted.Value = False
        '        If stocks.HierCategory.Value <> "000000" Then 'Update CATEGORY INDEX
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.SkuNumber, stocks.SkuNumber.Value)
        '            'Stkhir.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.HierarchyNumber, stocks.HierCategory.Value)
        '            'ColIr = Stkhir.LoadMatches()
        '            'If ColIr.Count < 1 Then
        '            Try
        '                'Stkhir.SkuNumber.Value = stocks.SkuNumber.Value
        '                Stkhir.HierarchyNumber.Value = stocks.HierCategory.Value
        '                'Stkhir.IsDeleted.Value = False
        '                Stkhir.SaveIfNew()
        '            Catch ex As Exception
        '            End Try
        '            'End If
        '            'If ColIr.Count > 0 Then
        '            '    Try
        '            '        Stkhir.IsDeleted.Value = False
        '            '        Stkhir.SaveIfExists()
        '            '    Catch ex As Exception
        '            '    End Try
        '            'End If
        '        End If 'Update CATEGORY INDEX
        '        If stocks.HierGroup.Value <> "000000" Then 'Update GROUP INDEX
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.SkuNumber, stocks.SkuNumber.Value)
        '            'Stkhir.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.HierarchyNumber, stocks.HierGroup.Value)
        '            'ColIr = Stkhir.LoadMatches()
        '            'If ColIr.Count < 1 Then
        '            Try
        '                'Stkhir.SkuNumber.Value = stocks.SkuNumber.Value
        '                Stkhir.HierarchyNumber.Value = stocks.HierGroup.Value
        '                'Stkhir.IsDeleted.Value = False
        '                Stkhir.SaveIfNew()
        '            Catch ex As Exception
        '            End Try
        '            'End If
        '            'If ColIr.Count > 0 Then
        '            '    Try
        '            '        Stkhir.IsDeleted.Value = False
        '            '        Stkhir.SaveIfExists()
        '            '    Catch ex As Exception
        '            '    End Try
        '            'End If
        '        End If 'Update GROUP INDEX
        '        If stocks.HierGroup.Value <> "000000" Then 'Update SUB GROUP INDEX
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.SkuNumber, stocks.SkuNumber.Value)
        '            'Stkhir.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.HierarchyNumber, stocks.HierSubGroup.Value)
        '            'ColIr = Stkhir.LoadMatches()
        '            'If ColIr.Count < 1 Then
        '            Try
        '                'Stkhir.SkuNumber.Value = stocks.SkuNumber.Value
        '                Stkhir.HierarchyNumber.Value = stocks.HierSubGroup.Value
        '                'Stkhir.IsDeleted.Value = False
        '                Stkhir.SaveIfNew()
        '            Catch ex As Exception
        '            End Try
        '            'End If
        '            'If ColIr.Count > 0 Then
        '            '    Try
        '            '        Stkhir.IsDeleted.Value = False
        '            '        Stkhir.SaveIfExists()
        '            '    Catch ex As Exception
        '            '    End Try
        '            'End If
        '        End If 'Update SUB GROUP INDEX
        '        If stocks.HierGroup.Value <> "000000" Then 'Update Style INDEX
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.SkuNumber, stocks.SkuNumber.Value)
        '            'Stkhir.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '            'Stkhir.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkhir.HierarchyNumber, stocks.HierStyle.Value)
        '            'ColIr = Stkhir.LoadMatches()
        '            'If ColIr.Count < 1 Then
        '            Try
        '                'Stkhir.SkuNumber.Value = stocks.SkuNumber.Value
        '                Stkhir.HierarchyNumber.Value = stocks.HierStyle.Value
        '                'Stkhir.IsDeleted.Value = False
        '                Stkhir.SaveIfNew()
        '            Catch ex As Exception
        '            End Try
        '            'End If
        '            'If ColIr.Count > 0 Then
        '            '    Try
        '            '        Stkhir.IsDeleted.Value = False
        '            '        Stkhir.SaveIfExists()
        '            '    Catch ex As Exception
        '            '    End Try
        '            'End If
        '        End If 'Update SUB GROUP INDEX
        '    Next
        '    intX = intY
        'End While
        'added 08/9/09-M.Milne to check set SYSOPT:PHIE Off
        Dim SysOpt As New BOSystem.cSystemOptions(_Oasys3DB)
        SysOpt.LoadMatches()
        SysOpt.RecreatePHL.Value = False
        SysOpt.SaveIfExists()
        SysOpt.Dispose()

        strWorkString = "TIBRSH - Adding SKUS Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
    End Sub

    Public Sub ProcessSupplierUpdates(Optional ByVal blnTibpfv As Boolean = False) ' Process data in HPSTV files

        Dim strTransmissionFileName As String = "HPSTV"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Supmas As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim Supdet As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColVd As New List(Of BOPurchases.cSupplierDetail)
        Dim Supnot As New BOPurchases.cSupplierNote(_Oasys3DB)

        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        Dim boolGotDataToProcess As Boolean = False
        Dim boolProcessCCRecordsAllowed As Boolean = False
        Dim intI As Integer

        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        Dim intSQQFreq As Integer = 0
        Dim strPendingHistoryFile As String = String.Empty & strTransmissionsPath & "\"

        strTvcPath = String.Empty & strTransmissionsPath & "\"
        strPendingHpstvText = String.Empty
        'Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"
        arrstrRecordTypes = New String(16) {" ", "VM", "VC", "VN", "VD", "VR", "VQ", "VT", "VH", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        boolOutputErrorToSthoa = False

        'Now Process the transmission files in date sequence from the Files to process array
        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile, blnTibpfv, strPendingHistoryFile)
            If blnTibpfv Then
                strCopyToFileName = strTvcPath & strTransmissionFileName
            Else
                strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)
            End If

            ProcessPendingSupplierUpdates(strOpenThisFile)
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file
            Array.Clear(arrintProcessedCounts, 0, 15)
            Array.Clear(arrintRejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)

            _NoRecInTibhue = 0
            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            OutputSthoLog(strWorkString)
            If File.Exists(strCopyToFileName) = False Then Exit Sub
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                strTransmissionFileData = TransmissionFileReader.ReadLine
                strTransmissionFileData = strTransmissionFileData.PadRight(42, " "c)
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                'Get Record Type and increment counters to record quantity processed
                intWhichRecordType = 0
                Dim RecordType As String = strTransmissionFileData.ToUpper.Substring(0, 2)
                For intWhichRecordType = 1 To 16
                    If (RecordType = arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        'arrintProcessedCounts(intWhichRecordType) += 1 'done in Validation Routine
                        Exit For
                    End If
                Next

                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If (RecordType = "HR") Then
                    boolDoingAStoreLoad = False
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                    If strTransmissionFileData.Substring(32, 4).ToUpper = "LOAD" Then boolDoingAStoreLoad = True
                    If boolDoingAStoreLoad = True Then
                        strWorkString = "Processing :" & strCopyToFileName & " Store LOAD - Flagging SUPMAS as DELETED : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        OutputSthoLog(strWorkString)
                        Supmas.FlagAllAsDeleted()
                        strWorkString = "Processing :" & strCopyToFileName & " Store LOAD - Flagging SUPDET as DELETED : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        OutputSthoLog(strWorkString)
                        Supdet.FlagAllAsDeleted()
                        strWorkString = "Processing :" & strCopyToFileName & " Store LOAD - Deleting ALL SUPNOT records : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        OutputSthoLog(strWorkString)
                        Supnot.DeleteAll()
                    Else
                        strWorkString = "Processing :" & strCopyToFileName & " Store UPDATE - Initialising all Current SUPDET records : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        OutputSthoLog(strWorkString)
                        Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Supmas.Number, "     ")
                        Supmas.Suppliers = Supmas.LoadMatches()
                        For Each supplier As BOPurchases.cSupplierMaster In Supmas.Suppliers
                            Dim SupDet999 As New BOPurchases.cSupplierDetail(_Oasys3DB)
                            SupDet999.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.SupplierNumber, Supmas.Number.Value)
                            SupDet999.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            SupDet999.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.DepotType, "S")
                            SupDet999.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            SupDet999.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.DepotNumber, "999")
                            ColVd = SupDet999.LoadMatches()
                            If ColVd.Count > 0 Then
                                Supdet = New BOPurchases.cSupplierDetail(_Oasys3DB)
                                Supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupDet999.SupplierNumber, Supmas.Number.Value)
                                Supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SupDet999.DepotType, "S")
                                ColVd = Supdet.LoadMatches()
                                For Each detail As BOPurchases.cSupplierDetail In ColVd
                                    Try
                                        Supdet.DateOrderCloseStart.Value = SupDet999.DateOrderCloseStart.Value
                                        Supdet.DateOrderCloseEnd.Value = SupDet999.DateOrderCloseEnd.Value
                                        Supdet.DateDelCloseStart.Value = SupDet999.DateDelCloseStart.Value
                                        Supdet.DateDelCloseEnd.Value = SupDet999.DateDelCloseEnd.Value
                                        Supdet.OrderMinType.Value = SupDet999.OrderMinType.Value
                                        Supdet.OrderMinValue.Value = SupDet999.OrderMinValue.Value
                                        Supdet.OrderMinUnits.Value = SupDet999.OrderMinWeight.Value
                                        Supdet.OrderMinWeight.Value = SupDet999.OrderMinWeight.Value
                                        Supdet.TruckCapWeight.Value = SupDet999.TruckCapWeight.Value
                                        Supdet.TruckCapVolume.Value = SupDet999.TruckCapVolume.Value
                                        Supdet.TruckCapPallets.Value = SupDet999.TruckCapPallets.Value
                                        Supdet.Tradanet.Value = SupDet999.Tradanet.Value
                                        Supdet.LeadTimeFixed.Value = SupDet999.LeadTimeFixed.Value

                                        Supdet.SOQFrequency.Value = SupDet999.SOQFrequency.Value
                                        intSQQFreq = Supdet.SOQFrequency.Value
                                        If intSQQFreq >= 64 Then
                                            intSQQFreq -= 64
                                            Supdet.ReviewDay0.Value = True
                                        Else
                                            Supdet.ReviewDay0.Value = False
                                        End If

                                        If intSQQFreq >= 32 Then
                                            intSQQFreq -= 32
                                            Supdet.ReviewDay6.Value = True
                                        Else
                                            Supdet.ReviewDay6.Value = False
                                        End If

                                        If intSQQFreq >= 16 Then
                                            intSQQFreq -= 16
                                            Supdet.ReviewDay5.Value = True
                                        Else
                                            Supdet.ReviewDay5.Value = False
                                        End If

                                        If intSQQFreq >= 8 Then
                                            intSQQFreq -= 8
                                            Supdet.ReviewDay4.Value = True
                                        Else
                                            Supdet.ReviewDay4.Value = False
                                        End If

                                        If intSQQFreq >= 4 Then
                                            intSQQFreq -= 4
                                            Supdet.ReviewDay3.Value = True
                                        Else
                                            Supdet.ReviewDay3.Value = False
                                        End If

                                        If intSQQFreq >= 2 Then
                                            intSQQFreq -= 2
                                            Supdet.ReviewDay2.Value = True
                                        Else
                                            Supdet.ReviewDay2.Value = False
                                        End If

                                        If intSQQFreq >= 1 Then
                                            intSQQFreq -= 1
                                            Supdet.ReviewDay1.Value = True
                                        Else
                                            Supdet.ReviewDay1.Value = False
                                        End If

                                        Supdet.DeliveryCheckMethod.Value = SupDet999.DeliveryCheckMethod.Value
                                        Supdet.BBC.Value = SupDet999.BBC.Value
                                        Supdet.LeadTime0.Value = SupDet999.LeadTime0.Value
                                        Supdet.LeadTime1.Value = SupDet999.LeadTime1.Value
                                        Supdet.LeadTime2.Value = SupDet999.LeadTime2.Value
                                        Supdet.LeadTime3.Value = SupDet999.LeadTime3.Value
                                        Supdet.LeadTime4.Value = SupDet999.LeadTime4.Value
                                        Supdet.LeadTime5.Value = SupDet999.LeadTime5.Value
                                        Supdet.LeadTime6.Value = SupDet999.LeadTime6.Value
                                        Supdet.SaveIfExists()
                                    Catch ex As Exception
                                    End Try
                                Next 'Supplier Depot to update with Master Depot Details
                            End If
                        Next
                    End If
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "U" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If 'Trailer Record
                DataIntegrityHpstv(strTransmissionFileData, blnTibpfv)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                    Exit For
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                    Exit For
                End If
            Next

            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1
            dateHashDate = Sysdates.Today.Value
            SetupHash("AV", decHashValue, dateHashDate) 'Create AV Header 'D' Record
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 10 'Use 9 counters and then UK Record instead of 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Or (intWhichRecordType = 10) Then
                    If intWhichRecordType < 10 Then
                        strSthoaText = strSthoaText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthoaText = strSthoaText & arrstrRecordTypes(16) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthoaText = strSthoaText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthpaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
            dateHashDate = Sysdates.Today.Value
            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1

            SetupHash("CV", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If strPendingHpstvText.Length > 0 Then
                PutSthoToDisc(strPendingHpstvFileName, strPendingHpstvText, blnTibpfv)
                strPendingHpstvText = String.Empty
            End If
            If File.Exists(strCopyToFileName) And blnTibpfv = False Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            'Only do below if not running the TIBPFV pending supplier changes
            If blnTibpfv = False Then
                Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
                oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
                oTVCControl.Dispose()
            End If
        End While ' Process the files

    End Sub ' Process data in HPSTV files
    Public Sub ProcessPendingSupplierUpdates(ByVal SupplierFileName As String) ' Process data in HPSTV files

        Dim strTransmissionFileName As String = "HPSTV"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Supmas As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim Supdet As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColVd As New List(Of BOPurchases.cSupplierDetail)
        Dim Supnot As New BOPurchases.cSupplierNote(_Oasys3DB)

        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        Dim boolGotDataToProcess As Boolean = False
        Dim boolProcessCCRecordsAllowed As Boolean = False
        'Dim boolSetObsoleted As Boolean = False
        'Dim boolProcessEventsWhenRequired As Boolean = False
        Dim intK As Integer = 0
        'Dim arrstrRecordTypes As String() = New String(15) {" ", "CY", "CC", "CM", "UK", "  ", "  ", "  ", "  ", "  ", "  ", " ", " ", " ", " ", "  "}

        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        strPendingHpstvText = String.Empty
        'Dim strMyTvcPath As String = String.Empty & strTvcPath & strTransmissionFileName & ".TVC"
        arrstrRecordTypes = New String(16) {" ", "VM", "VC", "VN", "VD", "VR", "VQ", "VT", "VH", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)
        strOpenThisFile = String.Empty & strTvcPath & strTransmissionFileName
        strCopyToFileName = strOpenThisFile & ".PND"
        boolDoingAStoreLoad = False
        If File.Exists(strOpenThisFile) Then
            If File.Exists(strCopyToFileName) Then
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            My.Computer.FileSystem.CopyFile(strOpenThisFile, strCopyToFileName)
            My.Computer.FileSystem.DeleteFile(strOpenThisFile)

            If File.Exists(strCopyToFileName) Then
                intHeadOfficeNumberOfRecords = 0 ' Records in the file
                intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
                intHeadOfficeBadType = 0 ' Bad Record Types
                intHeadOfficeNewItems = 0 'Type 1 Records
                intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
                intHeadOfficeChanges = 0 'Type 4 Records
                intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
                intHeadOfficeModels = 0 'Type 9 Records in file

                Array.Clear(arrIntRecordTypeCounts, 0, 16)
                Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
                Array.Clear(arrdecRecordTypeHashes, 0, 16)
                Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)

                strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
                ProcessTransmissionsProgress.ProcessName.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                OutputSthoLog(strWorkString)
                TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
                While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                    strTransmissionFileData = TransmissionFileReader.ReadLine
                    intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                    ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                    ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                    ProcessTransmissionsProgress.Show()
                    intWhichRecordType = 0
                    For intWhichRecordType = 0 To 16
                        If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                            arrIntRecordTypeCounts(intWhichRecordType) += 1
                            arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                            Exit For
                        End If
                    Next
                    If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                        arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                        If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                    End If
                    DataIntegrityHpstv(strTransmissionFileData)
                End While
                TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
                strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
                ProcessTransmissionsProgress.ProcessName.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                OutputSthoLog(strWorkString)
            End If

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                    Exit For
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                    Exit For
                End If
            Next
            'End If
            decHashValue = 0
            dateHashDate = Sysdates.Today.Value
            SetupHash("AV", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If

            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next intWhichRecordType

            If strSthoaText.Length > 0 Then
                PutSthoToDisc(strSthpaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
            dateHashDate = Sysdates.Today.Value
            decHashValue = 1
            SetupHash("CV", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If strPendingHpstvText.Length > 0 Then
                PutSthoToDisc(strPendingHpstvFileName, strPendingHpstvText)
                strPendingHpstvText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
        End If
    End Sub ' Process data in HPSTV files
    Public Function ProcessHierarchyUpdates() As Boolean ' Process data in HOSTH files

        Dim strTransmissionFileName As String = "HOSTH"
        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty

        Dim Sysdates As New BOSystem.cSystemDates(_Oasys3DB)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()
        dateHashDate = Sysdates.Today.Value()

        Dim boolGotDataToProcess As Boolean = False
        Dim boolProcessCCRecordsAllowed As Boolean = False
        Dim intI As Integer
        arrstrRecordTypes = New String(16) {" ", "HM", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "UK"}

        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(15) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}
        Dim strTransmissionFileToOpen As String = String.Empty & strFromRtiPath & "\" & strTransmissionFileName
        Dim ProcessedOK As Boolean = False
        strTvcPath = String.Empty & strTransmissionsPath & "\"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        While PrepareNextRTIFile(strTransmissionFileName, _StoreNumber, strOpenThisFile)
            strWorkString = "Processing Transmissions In - " & strTransmissionFileName & " : " & strOpenThisFile & " Copied to : " & strCopyToFileName & " : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)
            strCopyToFileName = strTvcPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)

            boolDoingAStoreLoad = False
            intHeadOfficeNumberOfRecords = 0 ' Records in the file
            intHeadOfficeWrongStore = 0 ' Records in the file for the wrong store
            intHeadOfficeBadType = 0 ' Bad Record Types
            intHeadOfficeNewItems = 0 'Type 1 Records
            intHeadOfficeBadNew = 0 'Type 1 Records -Already on file
            intHeadOfficeChanges = 0 'Type 4 Records
            intHeadOfficeBadChanges = 0 'Type 4 Records - NOT on file
            intHeadOfficeModels = 0 'Type 9 Records in file

            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)

            _NoRecInTibhue = 0
            strWorkString = "Validating :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                strTransmissionFileData = TransmissionFileReader.ReadLine
                intHeadOfficeNumberOfRecords = intHeadOfficeNumberOfRecords + 1
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty & strTransmissionFileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty & intHeadOfficeNumberOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 0 To 16
                    If strTransmissionFileData.ToUpper.StartsWith(arrstrRecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(strTransmissionFileData.Substring(10, 12))
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (strTransmissionFileData.ToUpper.StartsWith("HR") = False) And (strTransmissionFileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(strTransmissionFileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(strTransmissionFileData.Substring(10, 12))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(strTransmissionFileData.Substring(18, 2))
                    intSequenceNumber = CInt(strTransmissionFileData.Substring(20, 6))
                End If
                If strTransmissionFileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = strTransmissionFileData.Length
                    intI = 32
                    While intI <= (intTrailerLength - 20)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If strTransmissionFileData.Substring(intI, 1) <> Nothing Then
                                If strTransmissionFileData.Substring(intI, 1).ToUpper = "H" And strTransmissionFileData.Substring((intI + 5), 7) > "       " Then
                                    If strTransmissionFileData.Substring(intI, 2).ToUpper = arrstrRecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(strTransmissionFileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(strTransmissionFileData.Substring((intI + 9), 11))
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                DataIntegrityHosth(strTransmissionFileData)
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Validation of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next

            dateHashDate = Sysdates.Today.Value
            decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1
            SetupHash("CH", decHashValue, dateHashDate)
            If strSthocText <> String.Empty Then
                If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If arrstrRecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & arrintProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & arrintRejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        strSthocText = strSthocText & arrstrRecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    strSthocText = strSthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If strSthocText.Length > 0 Then
                PutSthoToDisc(strSthocFileName, strSthocText)
                strSthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = String.Empty & strSaveTransmissionFilePath & "\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
            ProcessedOK = True
        End While ' Process the files
        'UpdateHierarchyFiles()
        'ResetStockHierarchyIndex()
        Trace.WriteLine("Completed ProcessHierarchyUpdates")
        Return ProcessedOK

    End Function ' Process data in HOSTH files

    Public Sub CheckItemPrompts()
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        Dim ItemPrompts As New BOLookups.cItemPrompts(_Oasys3DB)
        Dim ColMp As New List(Of BOLookups.cItemPrompts)
        '        Dim intLowerSKU As Integer = 0
        '        Dim intUpperSKU As Integer = 111111
        '        Dim intHighSku As Integer = 999999
        Dim boolDeleteItemPrompts As Boolean = False
        Dim boolAddingItemPrompt As Boolean = False
        Dim intItemPromptsDeleted As Integer = 0
        Dim intItemPromptsAdded As Integer = 0
        Dim intItemPromptsUpdated As Integer = 0

        strWorkString = "Checking Item Prompts Started   : " & TimeOfDay.ToString("hh:mm:ss") '& Space(1) & "Records to Reset :" & Space(1) & StockMaster.Stocks.Count.ToString.PadLeft(6, " ") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)
        OutputSthoLog(strWorkString)

        Dim InSQLSelect As String = ""
        InSQLSelect = "UPDATE " & ItemPrompts.TableName & " SET " & ItemPrompts.DateDeleted.ColumnName & "=NULL"
        _Oasys3DB.ExecuteSql(InSQLSelect)

        StockMaster.ClearLoadFilter()
        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, StockMaster.SolventAge, 1)
        StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, StockMaster.OffensiveWeaponAge, 1)
        StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.InsulationItem, False)
        StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.QuarantineFlag, " ")
        StockMaster.AddLoadField(StockMaster.SkuNumber)
        InSQLSelect = StockMaster.GetSQLSelect
        InSQLSelect = "UPDATE " & ItemPrompts.TableName & " SET " & ItemPrompts.DateDeleted.ColumnName & "='" & Today.Date.ToString("yyyy-MM-dd") & "' WHERE " & ItemPrompts.PromptGroupID.ColumnName & "<='000005' AND " & ItemPrompts.SkuNumber.ColumnName & " IN (" & InSQLSelect & ")"
        _Oasys3DB.ExecuteSql(InSQLSelect)
        intItemPromptsDeleted = intItemPromptsDeleted + 1

        'While intLowerSKU <= intHighSku

        'StockMaster.ClearLoadFilter()
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SkuNumber, intLowerSKU.ToString("000000"))
        'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intUpperSKU.ToString.PadLeft(6, "0"))
        'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, StockMaster.SolventAge, 1)
        'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThan, StockMaster.OffensiveWeaponAge, 1)
        'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.InsulationItem, False)
        'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.QuarantineFlag, " ")
        'StockMaster.AddLoadField(StockMaster.SkuNumber)
        'StockMaster.SortBy(StockMaster.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        'StockMaster.Stocks = StockMaster.LoadMatches()
        'If StockMaster.Stocks.Count > 0 Then
        '    'intRecordsFlaggedThisRange = 0
        '    strWorkString = "Checking Item Prompts for Sku Range :" & Space(1) & intLowerSKU.ToString.PadLeft(6, "0") & Space(1) & "to" & Space(1) & intUpperSKU.ToString.PadLeft(6, "0") & Space(1) & "Records to Check :" & Space(1) & StockMaster.Stocks.Count.ToString.PadLeft(6, " ") & " Started   : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        '    OutputSthoLog(strWorkString)
        '    For Each stocks As BOStock.cStock In StockMaster.Stocks
        '        ItemPrompts.ClearLoadFilter()
        '        ItemPrompts.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, ItemPrompts.SkuNumber, stocks.SkuNumber.Value)
        '        ItemPrompts.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '        ItemPrompts.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, ItemPrompts.PromptGroupID, "000005")
        '        ColMp = ItemPrompts.LoadMatches()
        '        If ColMp.Count > 0 Then
        '            Try
        '                ItemPrompts.DateDeleted.Value = Today.Date.Date
        '                ItemPrompts.SaveIfExists()
        '            Catch ex As Exception
        '            End Try
        '            intItemPromptsDeleted = intItemPromptsDeleted + 1
        '        End If
        '    Next
        'End If
        '    StockMaster.ClearLoadFilter()
        '    StockMaster.ClearLists()
        '    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SkuNumber, intLowerSKU.ToString.PadLeft(6, "0"))
        '    StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, StockMaster.SkuNumber, intUpperSKU.ToString.PadLeft(6, "0"))
        '    'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        '    'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.SolventAge, 0)
        '    'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pOr)
        '    'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThan, StockMaster.OffensiveWeaponAge, 0)
        '    'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pOr)
        '    'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.InsulationItem, True)
        '    'StockMaster.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pOr)
        '    'StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, StockMaster.QuarantineFlag, " ")
        '    Dim InSQLSelectMgr = StockMaster.GetSQLSelect
        '    StockMaster.SortBy(StockMaster.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
        '    StockMaster.Stocks = StockMaster.LoadMatches()
        '    For Each stocks As BOStock.cStock In StockMaster.Stocks
        '        If stocks.SolventAge.Value > 0 Or stocks.OffensiveWeaponAge.Value > 0 Or stocks.InsulationItem.Value = True Or stocks.QuarantineFlag.Value <> " " Then
        '            ItemPrompts.ClearLists()
        '            ItemPrompts.ClearLoadFilter()
        '            ItemPrompts.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, ItemPrompts.SkuNumber, stocks.SkuNumber.Value)
        '            ColMp = ItemPrompts.LoadMatches()

        '            If ColMp.Count < 1 Then 'Item Prompt does not exist so create
        '                Try
        '                    ItemPrompts.SkuNumber.Value = stocks.SkuNumber.Value
        '                    If stocks.SolventAge.Value > 0 Then
        '                        ItemPrompts.PromptGroupID.Value = "000001"
        '                    End If
        '                    If stocks.OffensiveWeaponAge.Value > 0 Then
        '                        ItemPrompts.PromptGroupID.Value = "000002"
        '                    End If
        '                    If stocks.InsulationItem.Value = True Then
        '                        ItemPrompts.PromptGroupID.Value = "000003"
        '                    End If
        '                    If stocks.QuarantineFlag.Value = "Q" Then
        '                        ItemPrompts.PromptGroupID.Value = "000004"
        '                    End If
        '                    If stocks.QuarantineFlag.Value = "B" Then
        '                        ItemPrompts.PromptGroupID.Value = "000005"
        '                    End If
        '                    ItemPrompts.Sequence.Value = "0001"
        '                    ItemPrompts.SentToStores.Value = False
        '                    ItemPrompts.DateDeleted.Value = Nothing
        '                    ItemPrompts.SaveIfNew()
        '                Catch ex As Exception
        '                End Try
        '                intItemPromptsAdded = intItemPromptsAdded + 1
        '            End If
        '            If ColMp.Count > 0 Then 'Item Prompt exists to check
        '                For Each prompt As BOLookups.cItemPrompts In ColMp
        '                    If ItemPrompts.PromptGroupID.Value < "000006" Then
        '                        Try
        '                            If stocks.SolventAge.Value > 0 Then
        '                                ItemPrompts.PromptGroupID.Value = "000001"
        '                            End If
        '                            If stocks.OffensiveWeaponAge.Value > 0 Then
        '                                ItemPrompts.PromptGroupID.Value = "000002"
        '                            End If
        '                            If stocks.InsulationItem.Value = True Then
        '                                ItemPrompts.PromptGroupID.Value = "000003"
        '                            End If
        '                            If stocks.QuarantineFlag.Value = "Q" Then
        '                                ItemPrompts.PromptGroupID.Value = "000004"
        '                            End If
        '                            If stocks.QuarantineFlag.Value = "B" Then
        '                                ItemPrompts.PromptGroupID.Value = "000005"
        '                            End If
        '                            ItemPrompts.Sequence.Value = "0001"
        '                            ItemPrompts.SentToStores.Value = False
        '                            ItemPrompts.DateDeleted.Value = Nothing

        '                            ItemPrompts.SaveIfExists()
        '                        Catch ex As Exception
        '                        End Try
        '                        intItemPromptsUpdated = intItemPromptsUpdated + 1
        '                    End If
        '                Next 'Prompt for Item to Update
        '            End If
        '        End If
        '    Next 'Item to check if Prompts valid
        '    'If intRecordsFlaggedThisRange > 0 Then
        '    '    strWorkString = strWorkString & " Records Reset    :" & Space(1) & intRecordsFlaggedThisRange.ToString.PadLeft(6, " ") & " Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        '    '    OutputSthoLog(strWorkString)
        '    'End If
        '    'End If

        '    intLowerSKU = intUpperSKU
        '    intUpperSKU = intUpperSKU + 111111
        'End While

        'Delete all existing active Prompt Groups so that they point to the correct Group ID
        Dim InSQLUpdSelectMgr As String = "DELETE FROM " & ItemPrompts.TableName & " WHERE " & ItemPrompts.DateDeleted.ColumnName & " IS NULL AND " & ItemPrompts.PromptGroupID.ColumnName & "<='000005'"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)


        InSQLUpdSelectMgr = "INSERT INTO " & ItemPrompts.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ",'000005','0001',0,NULL FROM " & _
            StockMaster.TableName & " WHERE " & StockMaster.QuarantineFlag.ColumnName & "='B'"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)

        InSQLUpdSelectMgr = "INSERT INTO " & ItemPrompts.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ",'000004','0001',0,NULL FROM " & _
            StockMaster.TableName & " WHERE " & StockMaster.QuarantineFlag.ColumnName & "='Q'"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)

        InSQLUpdSelectMgr = "INSERT INTO " & ItemPrompts.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ",'000003','0001',0,NULL FROM " & _
            StockMaster.TableName & " WHERE " & StockMaster.QuarantineFlag.ColumnName & "=' ' AND " & StockMaster.InsulationItem.ColumnName & "=1"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)

        InSQLUpdSelectMgr = "INSERT INTO " & ItemPrompts.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ",'000002','0001',0,NULL FROM " & _
            StockMaster.TableName & " WHERE " & StockMaster.QuarantineFlag.ColumnName & "=' ' AND " & StockMaster.InsulationItem.ColumnName & "=0 AND " & _
            StockMaster.OffensiveWeaponAge.ColumnName & " > 0"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)

        InSQLUpdSelectMgr = "INSERT INTO " & ItemPrompts.TableName & " SELECT " & StockMaster.SkuNumber.ColumnName & ",'000001','0001',0,NULL FROM " & _
            StockMaster.TableName & " WHERE " & StockMaster.QuarantineFlag.ColumnName & "=' ' AND " & StockMaster.InsulationItem.ColumnName & "=0 AND " & _
            StockMaster.OffensiveWeaponAge.ColumnName & " = 0 AND " & StockMaster.SolventAge.ColumnName & " > 0"
        _Oasys3DB.ExecuteSql(InSQLUpdSelectMgr)

        intItemPromptsUpdated = intItemPromptsUpdated + 1
        'If intRecordsFlaggedThisRange > 0 Then
        '    strWorkString = strWorkString & " Records Reset    :" & Space(1) & intRecordsFlaggedThisRange.ToString.PadLeft(6, " ") & " Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        '    OutputSthoLog(strWorkString)
        'End If
        'End If

        '        End While
        strWorkString = "Checking Item Prompts Completed :" & Space(1) & TimeOfDay.ToString("hh:mm:ss") & vbCrLf & "Records Deleted : " & intItemPromptsDeleted.ToString.PadLeft(6, " "c) & vbCrLf & "Records Added   : " & intItemPromptsAdded.ToString.PadLeft(6, " "c) & vbCrLf & "Records Updated : " & intItemPromptsUpdated.ToString.PadLeft(6, " "c)
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        OutputSthoLog(strWorkString)
    End Sub
    Public Sub UpdateDataBaseFromTibhue()
        Dim EventMMG As New BOEvent.cEventMixMatch(_Oasys3DB)
        Dim ColVg As New List(Of BOEvent.cEventMixMatch)
        Dim EventHDR As New BOEvent.cEventHeader(_Oasys3DB)
        Dim ColEh As New List(Of BOEvent.cEventHeader)
        Dim EventDLG As New BOEvent.cEventDealGroup(_Oasys3DB)
        Dim ColEg As New List(Of BOEvent.cEventDealGroup)
        Dim EventHEX As New BOEvent.cEventHierarchyExcl(_Oasys3DB)
        Dim ColVh As New List(Of BOEvent.cEventHierarchyExcl)
        Dim EventCHG As New BOEvent.cEventPriceChange(_Oasys3DB)
        Dim ColEc As New List(Of BOEvent.cEventPriceChange)
        Dim EventMAS As New BOEvent.cEvent(_Oasys3DB)
        Dim ColEm As New List(Of BOEvent.cEvent)
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        Dim ColIm As New List(Of BOStock.cStock)

        Dim CouponBO As New BOEvent.cCouponMaster(_Oasys3DB)
        Dim CouponTextBO As New BOEvent.cCouponText(_Oasys3DB)

        Dim strEventNumber As String = String.Empty
        Dim strEventType As String = String.Empty
        Dim strEventKey1 As String = String.Empty
        Dim strEventKey2 As String = String.Empty
        Dim strEventChangeNumber As String = String.Empty
        Dim strEventChangeSkuNumber As String = String.Empty
        Dim decEventChangePrice As Decimal = 0
        Dim boolEventChangeDelete As Boolean = False
        Dim strEventDealGroup As String = String.Empty
        Dim strEventDealGroupType As String = String.Empty

        Dim strMixAndMatchSkuNumber As String = String.Empty
        Dim strCurrentPriority As String = String.Empty
        Dim boolCanAddOrUpdateEvent As Boolean = False
        Dim boolNewEvent As Boolean = False
        Dim boolCreateEventChange As Boolean = False
        Dim boolIsAMMGEventChange As Boolean = False

        Dim strWorkDate As String = String.Empty


        'Coupon's working variables
        Dim CouponNo As String = String.Empty
        Dim CouponSeq As String = String.Empty
        Dim ChangesMade As Boolean = False

        If File.Exists(strTibhueWorkFile) Then ' Process TIBHUE data
            strWorkString = "Processing of :" & strTibhueWorkFile & " Pass " & intPassNumber.ToString.PadRight(2, " "c) & " Begins : " & TimeOfDay.ToString("hh:mm:ss") & " Records to process = " & _NoRecInTibhue.ToString.PadRight(9, " "c) ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            OutputSthoLog(strWorkString)
            reader = New StreamReader(strTibhueWorkFile, True) ' Open the file for database update
            While reader.EndOfStream = False ' Processing NEXT TIBHUE record
                strTransmissionFileData = reader.ReadLine '.PadRight(68, " ")
                boolCreateEventChange = False
                boolIsAMMGEventChange = False
                If strTransmissionFileData.StartsWith("UA") Then ' Processing TIBHUE type UA
                    strTransmissionFileData = strTransmissionFileData.PadRight(35, " "c)
                    strEventNumber = String.Empty & strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
                    strMixAndMatchSkuNumber = String.Empty & strTransmissionFileData.Substring(28, 6).PadLeft(6, "0"c)
                    EventMMG.ClearLoadFilter()
                    EventMMG.ClearLists()
                    EventMMG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMMG.EventMixMatchID, strEventNumber)
                    EventMMG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    EventMMG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMMG.PartCode, strMixAndMatchSkuNumber)
                    ColVg = EventMMG.LoadMatches()
                    If ColVg.Count < 1 Then
                        EventMMG.PartCode.Value = strMixAndMatchSkuNumber
                        EventMMG.EventMixMatchID.Value = strEventNumber
                    End If
                    EventMMG.Deleted.Value = False
                    If strTransmissionFileData.Substring(34, 1) = "Y" Then
                        EventMMG.Deleted.Value = True
                    End If
                    Try
                        If ColVg.Count < 1 Then EventMMG.SaveIfNew()
                        If ColVg.Count > 0 Then EventMMG.SaveIfExists()
                    Catch ex As Exception
                    End Try
                End If ' Processing TIBHUE type UA

                If strTransmissionFileData.StartsWith("UB") Then ' Processing TIBHUE type UB
                    strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
                    strEventNumber = String.Empty & strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
                    EventHDR = New BOEvent.cEventHeader(_Oasys3DB)
                    EventHDR.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHDR.EventNoID, strEventNumber)
                    ColEh = EventHDR.LoadMatches()
                    boolCanAddOrUpdateEvent = True
                    boolNewEvent = True
                    strCurrentPriority = String.Empty
                    If ColEh.Count < 1 Then
                        EventHDR.EventNoID.Value = strEventNumber
                    End If
                    If ColEh.Count > 0 Then
                        boolNewEvent = False
                        For Each eventhead As BOEvent.cEventHeader In ColEh
                            strCurrentPriority = eventhead.Priority.Value
                        Next
                        If strTransmissionFileData.Substring(68, 2).PadLeft(2, "0"c) < strCurrentPriority Then boolCanAddOrUpdateEvent = False
                    End If
                    If boolCanAddOrUpdateEvent = True Then
                        'Priority Number does not match so do not update Header
                        Try
                            EventHDR.Description.Value = strTransmissionFileData.Substring(28, 40).PadRight(40, " "c)
                            EventHDR.Priority.Value = strTransmissionFileData.Substring(68, 2).PadLeft(2, "0"c)
                            strWorkDate = String.Empty & strTransmissionFileData.Substring(70, 2).PadLeft(2, "0"c) & "/" & strTransmissionFileData.Substring(72, 2).PadLeft(2, "0"c) & "/" & strTransmissionFileData.Substring(76, 2).PadLeft(2, "0"c)
                            EventHDR.StartDate.Value = CDate(strWorkDate)
                            EventHDR.StartTime.Value = strTransmissionFileData.Substring(78, 4).PadLeft(4, "0"c)
                            If strTransmissionFileData.Substring(82, 8) <> "00000000" Then
                                strWorkDate = strTransmissionFileData.Substring(82, 2).PadLeft(2, "0"c) & "/" & strTransmissionFileData.Substring(84, 2).PadLeft(2, "0"c) & "/" & strTransmissionFileData.Substring(88, 2).PadLeft(2, "0"c)
                                EventHDR.EndDate.Value = CDate(strWorkDate)
                            Else
                                EventHDR.EndDate.Value = Nothing
                            End If

                            EventHDR.EndTime.Value = strTransmissionFileData.Substring(90, 4).PadLeft(4, "0"c)
                            EventHDR.ActiveDays1.Value = (strTransmissionFileData.Substring(94, 1) = "Y")
                            EventHDR.ActiveDays2.Value = (strTransmissionFileData.Substring(95, 1) = "Y")
                            EventHDR.ActiveDays3.Value = (strTransmissionFileData.Substring(96, 1) = "Y")
                            EventHDR.ActiveDays4.Value = (strTransmissionFileData.Substring(97, 1) = "Y")
                            EventHDR.ActiveDays5.Value = (strTransmissionFileData.Substring(98, 1) = "Y")
                            EventHDR.ActiveDays6.Value = (strTransmissionFileData.Substring(99, 1) = "Y")
                            EventHDR.ActiveDays7.Value = (strTransmissionFileData.Substring(100, 1) = "Y")
                            EventHDR.Deleted.Value = (strTransmissionFileData.Substring(101, 1) = "Y")
                            If boolNewEvent = True Then EventHDR.SaveIfNew()
                            If boolNewEvent = False Then EventHDR.SaveIfExists()
                        Catch ex As Exception
                        End Try
                    End If
                End If ' Processing TIBHUE type UB

                If strTransmissionFileData.StartsWith("UC") Then ' Processing TIBHUE type UC
                    strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
                    strEventNumber = strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
                    EventHDR = New BOEvent.cEventHeader(_Oasys3DB)
                    EventHDR.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHDR.EventNoID, strEventNumber)
                    ColEh = EventHDR.LoadMatches()
                    boolCanAddOrUpdateEvent = True

                    If ColEh.Count > 0 Then ' EVENT HEADER EXISTS?
                        strEventType = strTransmissionFileData.Substring(28, 2).ToUpper
                        strEventKey1 = strTransmissionFileData.Substring(30, 6).PadLeft(6, "0"c)
                        strEventKey2 = strTransmissionFileData.Substring(36, 8).PadLeft(8, "0"c)
                        If strEventType = "PS" Or strEventType = "TS" Or strEventType = "QS" Or strEventType = "MS" Then
                            StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strEventKey1)
                            StockMaster.Stocks = StockMaster.LoadMatches()
                            If StockMaster.Stocks.Count < 1 Then 'Stock master does not exist so do not save
                                boolCanAddOrUpdateEvent = False
                            End If
                        End If
                        If boolCanAddOrUpdateEvent = True Then
                            If strEventType = "PS" Or strEventType = "PM" Or strEventType = "TS" Or strEventType = "TM" Then
                                'Key2 must be empty
                                If strEventKey2 <> "00000000" Then boolCanAddOrUpdateEvent = False
                            Else
                                If strEventKey2 = "00000000" Then boolCanAddOrUpdateEvent = False
                            End If
                        End If
                        If boolCanAddOrUpdateEvent = True Then
                            strEventChangeNumber = strEventNumber
                            strEventChangeSkuNumber = strEventKey1
                            decEventChangePrice = CDec(strTransmissionFileData.Substring(44, 10))
                            boolEventChangeDelete = False
                            If strEventType = "PS" Then
                                If strTransmissionFileData.Substring(85, 1) = "Y" Then boolEventChangeDelete = True
                                boolCreateEventChange = True
                                boolIsAMMGEventChange = False
                            End If
                            If strEventType = "PM" Then
                                boolCreateEventChange = True
                                boolIsAMMGEventChange = True
                            End If
                            If boolCreateEventChange = False Then
                                Try
                                    EventMAS.ClearLoadFilter()
                                    EventMAS.ClearLists()
                                    EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventNo, strEventNumber)
                                    EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                    EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventKey1, strEventKey1)
                                    EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                    EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventKey2, strEventKey2)
                                    EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                    EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventType, strEventType)
                                    ColEm = EventMAS.LoadMatches()
                                    If ColEm.Count < 1 Then
                                        EventMAS.EventType.Value = strEventType
                                        EventMAS.EventKey1.Value = strEventKey1
                                        EventMAS.EventKey2.Value = strEventKey2
                                        EventMAS.EventNo.Value = strEventNumber
                                    End If
                                    EventMAS.SpecialPrice.Value = CDec(strTransmissionFileData.Substring(44, 10))
                                    EventMAS.BuyQuantity.Value = CDec(strTransmissionFileData.Substring(54, 7))
                                    EventMAS.GetQuantity.Value = CDec(strTransmissionFileData.Substring(61, 7))
                                    EventMAS.DiscountAmount.Value = CDec(strTransmissionFileData.Substring(68, 10))
                                    EventMAS.PercentageDiscount.Value = CDec(strTransmissionFileData.Substring(78, 7))
                                    EventMAS.Deleted.Value = False
                                    If strTransmissionFileData.Substring(85, 1) = "Y" Then EventMAS.Deleted.Value = True
                                    EventMAS.StartDate.Value = EventHDR.StartDate.Value
                                    EventMAS.EndDate.Value = EventHDR.EndDate.Value
                                    EventMAS.Priority.Value = EventHDR.Priority.Value
                                    EventMAS.BuyCouponID.Value = strTransmissionFileData.Substring(86, 7)
                                    EventMAS.SellCouponID.Value = strTransmissionFileData.Substring(93, 7)
                                    If (EventMAS.BuyCouponID.Value.Trim.Length = 0) Then EventMAS.BuyCouponID.Value = "0000000"
                                    If (EventMAS.SellCouponID.Value.Trim.Length = 0) Then EventMAS.SellCouponID.Value = "0000000"
                                    If EventMAS.StartDate.Value <> Date.MinValue Or EventMAS.EndDate.Value <> Date.MinValue Then EventMAS.TimeOrDayRelated.Value = True
                                    If ColEm.Count < 1 Then
                                        EventMAS.SaveIfNew()
                                    Else
                                        EventMAS.SaveIfExists()
                                    End If
                                Catch ex As Exception
                                End Try
                            End If
                        End If
                    Else
                        OutputProductUpdateError(strTransmissionFileData, "UC: No Event HDR".PadRight(18, " "c))
                    End If ' EVENT HEADER EXISTS?
                End If ' Processing TIBHUE type UC

        If strTransmissionFileData.StartsWith("UD") Then ' Processing TIBHUE type UD
            Try
                strTransmissionFileData = strTransmissionFileData.PadRight(71, " "c)
                strEventNumber = String.Empty & strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
                strEventDealGroup = String.Empty & strTransmissionFileData.Substring(28, 6).PadLeft(6, "0"c)
                strEventKey1 = String.Empty & strTransmissionFileData.Substring(34, 6).PadLeft(6, "0"c)
                strEventDealGroupType = "S"
                If strEventKey1 = "000000" Then
                    strEventKey1 = String.Empty & strTransmissionFileData.Substring(40, 6).PadLeft(6, "0"c)
                    strEventDealGroupType = "M"
                End If
                EventDLG.ClearLoadFilter()
                EventDLG.ClearLists()
                EventDLG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventDLG.EventNo, strEventNumber)
                EventDLG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                EventDLG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventDLG.DealGroupNo, strEventDealGroup)
                EventDLG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                EventDLG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventDLG.DealType, strEventDealGroupType)
                EventDLG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                EventDLG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventDLG.ItemKey, strEventKey1)
                ColEg = EventDLG.LoadMatches()
                If ColEg.Count < 1 Then
                    EventDLG.EventNo.Value = strEventNumber
                    EventDLG.DealGroupNo.Value = strEventDealGroup
                    EventDLG.DealType.Value = strEventDealGroupType
                    EventDLG.ItemKey.Value = strEventKey1
                End If
                EventDLG.Quantity.Value = CDec(strTransmissionFileData.Substring(46, 7))
                EventDLG.ErosionValue.Value = CDec(strTransmissionFileData.Substring(53, 10))
                EventDLG.ErorsionPercentage.Value = CDec(strTransmissionFileData.Substring(63, 7))
                If EventDLG.ErorsionPercentage.Value <> 0 Then EventDLG.ErosionValue.Value = 0
                If EventDLG.ErosionValue.Value <> 0 Then EventDLG.ErorsionPercentage.Value = 0
                EventDLG.Deleted.Value = False
                If strTransmissionFileData.Substring(70, 1) = "Y" Then EventDLG.Deleted.Value = True
                If ColEg.Count < 1 Then EventDLG.SaveIfNew()
                If ColEg.Count > 0 Then EventDLG.SaveIfExists()
            Catch ex As Exception
            End Try
        End If ' Processing TIBHUE type UD

        If strTransmissionFileData.StartsWith("UE") Then ' Processing TIBHUE type UE
            Try
                strTransmissionFileData = strTransmissionFileData.PadRight(41, " "c)
                strEventNumber = String.Empty & strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
                EventHEX.ClearLoadFilter()
                EventHEX.ClearLists()
                EventHEX.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHEX.EventNo, strEventNumber)
                EventHEX.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                EventHEX.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHEX.HierCategory, strTransmissionFileData.Substring(28, 6).PadLeft(6, "0"c))
                EventHEX.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                EventHEX.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHEX.PartCode, strTransmissionFileData.Substring(34, 6).PadLeft(6, "0"c))
                ColVh = EventHEX.LoadMatches
                If ColVh.Count < 1 Then
                    EventHEX.EventNo.Value = strEventNumber
                    EventHEX.HierCategory.Value = strTransmissionFileData.Substring(28, 6).PadLeft(6, "0"c)
                    EventHEX.PartCode.Value = strTransmissionFileData.Substring(34, 6).PadLeft(6, "0"c)
                End If
                EventHEX.Deleted.Value = False
                If strTransmissionFileData.Substring(40, 1) = "Y" Then EventHEX.Deleted.Value = True
                If ColVh.Count < 1 Then EventHEX.SaveIfNew()
                If ColVh.Count > 0 Then EventHEX.SaveIfExists()
            Catch ex As Exception
            End Try
        End If ' Processing TIBHUE type UE

        If boolCreateEventChange = True Then ' Create Event Change Records required
            If boolIsAMMGEventChange = False Then
                        Try
                            EventCHG = New BOEvent.cEventPriceChange(_Oasys3DB)
                            EventCHG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventCHG.EventNumber, strEventChangeNumber)
                            EventCHG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            EventCHG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventCHG.PartCode, strEventChangeSkuNumber)
                            ColEc = EventCHG.LoadMatches()
                            If ColEc.Count < 1 Then
                                EventCHG.EventNumber.Value = strEventChangeNumber
                                EventCHG.PartCode.Value = strEventChangeSkuNumber
                                EventCHG.EventPrice.Value = decEventChangePrice
                            End If
                            EventCHG.Deleted.Value = boolEventChangeDelete
                            EventCHG.StartDate.Value = EventHDR.StartDate.Value
                            EventCHG.EndDate.Value = EventHDR.EndDate.Value
                            EventCHG.Priority.Value = EventHDR.Priority.Value

                            If ColEc.Count < 1 Then
                                EventCHG.SaveIfNew()
                            Else
                                EventCHG.SaveIfExists()
                            End If
                        Catch ex As Exception
                        End Try
            End If
            If boolIsAMMGEventChange = True Then
                EventMMG.ClearLoadFilter()
                EventMMG.ClearLists()
                EventMMG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMMG.EventMixMatchID, strEventNumber)
                EventMMG.SortBy(EventMMG.EventMixMatchID.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                EventMMG.SortBy(EventMMG.PartCode.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                ColVg = EventMMG.LoadMatches()
                If ColVg.Count > 0 Then
                    For Each MMG As BOEvent.cEventMixMatch In ColVg
                        Try
                            strEventChangeSkuNumber = MMG.PartCode.Value
                            EventCHG.ClearLists()
                            EventCHG.ClearLoadFilter()
                            EventCHG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventCHG.EventNumber, strEventChangeNumber)
                            EventCHG.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            EventCHG.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventCHG.PartCode, strEventChangeSkuNumber)
                            ColEc = EventCHG.LoadMatches()
                            If ColEc.Count < 1 Then
                                EventCHG.EventNumber.Value = strEventChangeNumber
                                EventCHG.PartCode.Value = strEventChangeSkuNumber
                                EventCHG.EventPrice.Value = decEventChangePrice
                            End If
                            EventCHG.Deleted.Value = boolEventChangeDelete
                            If ColEc.Count < 1 Then EventCHG.SaveIfNew()
                            If ColEc.Count > 0 Then EventCHG.SaveIfExists()
                        Catch ex As Exception
                        End Try
                    Next
                End If
            End If
        End If ' Create Event Change Records required

        'WIX1165 - Process UM Records for all Coupon Master entries
        If strTransmissionFileData.StartsWith("UM") Then ' Processing TIBHUE type UM
            strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
            CouponNo = String.Empty & strTransmissionFileData.Substring(22, 7).PadLeft(7, "0"c)
            CouponBO.ClearLoadFilter()
            CouponBO.ClearLists()
            CouponBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponBO.CouponID, CouponNo)
            CouponBO.LoadBORecords()

            If CouponBO.Description.Value <> strTransmissionFileData.Substring(29, 20).ToUpper.Trim Then ChangesMade = True
            CouponBO.Description.Value = strTransmissionFileData.Substring(29, 20).ToUpper.Trim
            If CouponBO.StoreGenerated.Value <> (strTransmissionFileData.Substring(49, 1) = "Y") Then ChangesMade = True
            CouponBO.StoreGenerated.Value = (strTransmissionFileData.Substring(49, 1) = "Y")
            If CouponBO.SerialNo.Value <> CBool(IIf(strTransmissionFileData.Substring(50, 1).ToUpper.Trim = "Y", 1, 0)) Then ChangesMade = True
            CouponBO.SerialNo.Value = CBool(IIf(strTransmissionFileData.Substring(50, 1).ToUpper.Trim = "Y", 1, 0))
            If CouponBO.ExclusiveCoupon.Value <> (strTransmissionFileData.Substring(51, 1).ToUpper.Trim = "Y") Then ChangesMade = True
            CouponBO.ExclusiveCoupon.Value = (strTransmissionFileData.Substring(51, 1).ToUpper.Trim = "Y")
            If CouponBO.ReUsable.Value <> CBool(IIf(strTransmissionFileData.Substring(52, 1).ToUpper.Trim = "Y", 1, 0)) Then ChangesMade = True
            CouponBO.ReUsable.Value = CBool(IIf(strTransmissionFileData.Substring(52, 1).ToUpper.Trim = "Y", 1, 0))
            If CouponBO.CollectInfo.Value <> (strTransmissionFileData.Substring(53, 1).ToUpper.Trim = "Y") Then ChangesMade = True
            CouponBO.CollectInfo.Value = (strTransmissionFileData.Substring(53, 1).ToUpper.Trim = "Y")
            If CouponBO.Deleted.Value <> CBool(IIf(strTransmissionFileData.Substring(54, 1).ToUpper.Trim = "Y", 1, 0)) Then ChangesMade = True
            CouponBO.Deleted.Value = CBool(IIf(strTransmissionFileData.Substring(54, 1).ToUpper.Trim = "Y", 1, 0))
            Try
                If CouponBO.BORecords.Count = 0 Then
                    CouponBO.CouponID.Value = CouponNo
                    CouponBO.SaveIfNew()
                Else
                    If ChangesMade Then CouponBO.SaveIfExists()
                End If
            Catch
                Throw
            End Try
        End If 'Processing 'UM' - Coupon Master Entry

        'WIX1374 - Process UT Records for all Coupon Text entries
        If strTransmissionFileData.StartsWith("UT") Then ' Processing TIBHUE type UT
            strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
            CouponNo = strTransmissionFileData.Substring(22, 7).PadLeft(7, "0"c)
            CouponSeq = strTransmissionFileData.Substring(29, 2).PadLeft(2, "0"c)
            CouponTextBO.ClearLoadFilter()
            CouponTextBO.ClearLists()
            CouponTextBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponTextBO.CouponID, CouponNo)
            CouponTextBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            CouponTextBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponTextBO.SequenceNo, CouponSeq)
            CouponTextBO.LoadBORecords()

            If CouponTextBO.PrintSize.Value <> strTransmissionFileData.Substring(31, 1).ToUpper Then ChangesMade = True
            CouponTextBO.PrintSize.Value = strTransmissionFileData.Substring(31, 1).ToUpper
            If CouponTextBO.TextAlignment.Value <> strTransmissionFileData.Substring(32, 1).ToUpper Then ChangesMade = True
            CouponTextBO.TextAlignment.Value = strTransmissionFileData.Substring(32, 1).ToUpper
            If CouponTextBO.PrintText.Value <> strTransmissionFileData.Substring(33, 52).Trim Then ChangesMade = True
            CouponTextBO.PrintText.Value = strTransmissionFileData.Substring(33, 56).Trim
            If CouponTextBO.Deleted.Value <> (strTransmissionFileData.Substring(89, 1).ToUpper = "Y") Then ChangesMade = True
            CouponTextBO.Deleted.Value = (strTransmissionFileData.Substring(89, 1).ToUpper = "Y")
            Try
                If CouponTextBO.BORecords.Count <= 0 Then
                    CouponTextBO.CouponID.Value = CouponNo
                    CouponTextBO.SequenceNo.Value = CouponSeq
                    CouponTextBO.SaveIfNew()
                Else
                    If ChangesMade Then CouponTextBO.SaveIfExists()
                End If
            Catch
                Throw
            End Try
        End If

            End While ' Processing NEXT TIBHUE record
            reader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strTibhueWorkFile & " Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)

        End If ' Process TIBHUE data

        If File.Exists(strTibhueWorkFile) Then
            My.Computer.FileSystem.DeleteFile(strTibhueWorkFile)
        End If
        _NoRecInTibhue = 0
        boolNeedToRunTibhue = False
    End Sub
    Public Sub UpdateDataBaseFromTibhus()

        Const PRM_SOQ_SERVICE_PERC As Integer = 206

        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        Dim ColIm As New List(Of BOStock.cStock)
        Dim StockText As New BOStock.cStockText(_Oasys3DB)
        Dim ColIt As New List(Of BOStock.cStockText)
        Dim EanMaster As New BOStock.cBarCodes(_Oasys3DB)
        Dim ColEa As New List(Of BOStock.cBarCodes)
        Dim intTypeU5RecordsUpdated As Integer = 0
        Dim intTypeU6RecordsUpdated As Integer = 0
        Dim strCurrentEquiv As String = String.Empty
        Dim strNewEquiv As String = String.Empty
        Dim strU5Type As String = String.Empty
        Dim strU5Skun As String = String.Empty
        Dim strU5Sequence As String = String.Empty
        Dim strU5DeleteType As String = String.Empty
        Dim strU5DeleteSkun As String = String.Empty
        Dim strU5DeleteSequence As String = String.Empty
        Dim strU1StatusFlag As String = String.Empty
        Dim decSupplierPackSizeWork As Decimal = 0
        Dim decSupplierPackSizeSaved As Decimal = 0
        Dim strRemoveEanSku As String = String.Empty
        Dim strEanNumber As String = String.Empty
        Dim boolSophie As Boolean = False
        Dim boolU4NotOnFile As Boolean = False
        Dim boolU1AddRecord As Boolean = False
        Dim boolChangesCanBeMade As Boolean = False
        Dim longEanNumber As Long = 0
        Dim strWorkPrice As String = String.Empty
        Dim SoqoptionsServiceLevel As Decimal = New BOSystem.cParameter(_Oasys3DB).GetParameterDecimal(PRM_SOQ_SERVICE_PERC)

        Dim SystemOptions As New BOSystem.cSystemOptions(_Oasys3DB)
        Dim Colso As New List(Of BOSystem.cSystemOptions)

        SystemOptions.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, SystemOptions.SystemOptionsID, "01")
        Colso = SystemOptions.LoadMatches()

        If File.Exists(strTibhusWorkFile) Then
            strWorkString = "Processing of :" & strTibhusWorkFile & " Pass " & intPassNumber.ToString.PadRight(2, " "c) & " Begins : " & TimeOfDay.ToString("hh:mm:ss") & " Records to process = " & _NoRecInTibhus.ToString.PadRight(9, " "c) ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)
            reader = New StreamReader(strTibhusWorkFile, True) ' Open the file for database update
            While reader.EndOfStream = False ' Calculate Hash & Record counts
                strTransmissionFileData = reader.ReadLine '.PadRight(68, " ")
                If strTransmissionFileData.StartsWith("U4") Then
                    strTransmissionFileData = strTransmissionFileData.PadRight(370, " "c)
                    boolU4NotOnFile = False
                    StockMaster = New BOStock.cStock(_Oasys3DB) 'force new/empty Stock Master to clear down previous values
                    StockMaster.HOCheckDigit.Value = "0"
                    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTransmissionFileData.Substring(26, 6).PadLeft(6, "0"c))
                    StockMaster.Stocks = StockMaster.LoadMatches()
                    If StockMaster.Stocks.Count < 1 And strTransmissionFileData.Substring(51, 1) <> "D" Then boolU4NotOnFile = True
                    If StockMaster.Stocks.Count > 0 Then
                        Try
                            StockMaster.SpacemanDisplayFctr.Value = CInt(strTransmissionFileData.Substring(220, 5))
                            If StockMaster.DateMinLastChanged.Value = Date.MinValue.Date Then StockMaster.MinQuantity.Value = StockMaster.SpacemanDisplayFctr.Value
                            If StockMaster.DateMinLastChanged.Value <> Date.MinValue.Date And StockMaster.SpacemanDisplayFctr.Value > StockMaster.MinQuantity.Value Then
                                StockMaster.MinQuantity.Value = StockMaster.SpacemanDisplayFctr.Value
                                StockMaster.DateMinLastChanged.Value = Date.MinValue.Date
                            End If
                            If StockMaster.DateMinLastChanged.Value <> Date.MinValue.Date And StockMaster.SpacemanDisplayFctr.Value > StockMaster.MinQuantity.Value Then StockMaster.DateMinLastChanged.Value = Date.MinValue.Date
                            ' The following is equivalent to the >SET-STATUS routine in TIBHUS
                            strU1StatusFlag = strTransmissionFileData.Substring(51, 1)
                            'If strTransmissionFileData.Substring(85, 3) = "666" Then
                            '    stockmaster.
                            'End If
                            If strU1StatusFlag = "D" Then
                                If StockMaster.ItemObsolete.Value = False Then
                                    StockMaster.DateObsolete.Value = Today.Date.Date
                                    StockMaster.ItemObsolete.Value = True
                                End If
                            End If

                            If strU1StatusFlag <> "D" Then
                                StockMaster.DateObsolete.Value = Date.MinValue.Date
                                StockMaster.DateDeleted.Value = Date.MinValue.Date
                                StockMaster.ItemObsolete.Value = False
                                StockMaster.ItemDeleted.Value = False
                            End If
                            If strU1StatusFlag = "S" Then
                                Trace.WriteLine("UpdateTIBHUS:Processing 'S' Record SKU" & StockMaster.SkuNumber.Value)
                                StockMaster.RelatedItemSingle.Value = True
                                'Verify that Related Item exists
                                Dim RelItemBO As New BOStock.cRelatedItems(_Oasys3DB)
                                RelItemBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, RelItemBO.SingleItem, StockMaster.SkuNumber.Value)
                                If (RelItemBO.LoadMatches.Count = 0) Then
                                    Trace.WriteLine("UpdateTIBHUS:Processing 'S'-Create RelItm" & StockMaster.SkuNumber.Value & "/" & strTransmissionFileData.Substring(88, 6))
                                    RelItemBO.SingleItem.Value = StockMaster.SkuNumber.Value
                                    RelItemBO.BulkOrPackageItem.Value = strTransmissionFileData.Substring(88, 6)
                                    RelItemBO.NoSinglesPerPack.Value = CDec(strTransmissionFileData.Substring(174, 5))
                                    RelItemBO.SaveIfNew()
                                    'Update Bulk Record to indicate that link now exists
                                    'StockMaster.RelatedNoItems.Value += 1
                                    Dim oBulkSKU As New BOStock.cStock(_Oasys3DB)
                                    oBulkSKU.SkuNumber.Value = strTransmissionFileData.Substring(88, 6)
                                    oBulkSKU.AdjustRelatedNoItems(strTransmissionFileData.Substring(88, 6), +1)
                                Else
                                    If (RelItemBO.BulkOrPackageItem.Value = strTransmissionFileData.Substring(88, 6)) Then
                                        Trace.WriteLine("UpdateTIBHUS:Processing 'S'-RelItm Exists and Matches" & strTransmissionFileData.Substring(88, 6))
                                        RelItemBO.NoSinglesPerPack.Value = CDec(strTransmissionFileData.Substring(174, 5))
                                        RelItemBO.SaveIfNew()
                                    Else
                                        Trace.WriteLine("UpdateTIBHUS:Processing 'S'-RelItm Exists and has Changed from " & RelItemBO.BulkOrPackageItem.Value & " to " & strTransmissionFileData.Substring(88, 6))
                                        Dim oBulkSKU As New BOStock.cStock(_Oasys3DB)
                                        oBulkSKU.SkuNumber.Value = RelItemBO.BulkOrPackageItem.Value
                                        oBulkSKU.AdjustRelatedNoItems(RelItemBO.BulkOrPackageItem.Value, -1)
                                        RelItemBO.BulkOrPackageItem.Value = strTransmissionFileData.Substring(88, 6)
                                        RelItemBO.NoSinglesPerPack.Value = CDec(strTransmissionFileData.Substring(174, 5))
                                        RelItemBO.SaveIfExists()
                                        oBulkSKU = New BOStock.cStock(_Oasys3DB)
                                        oBulkSKU.SkuNumber.Value = strTransmissionFileData.Substring(88, 6)
                                        oBulkSKU.AdjustRelatedNoItems(RelItemBO.BulkOrPackageItem.Value, 1)
                                        'StockMaster.RelatedNoItems.Value += 1
                                    End If
                                End If
                            Else
                                Trace.WriteLine("UpdateTIBHUS:Processing Non 'S' Record " & StockMaster.RelatedItemSingle.Value)
                                If StockMaster.RelatedItemSingle.Value = True Then
                                    'Update Related Item as deleted
                                    Dim RelItemBO As New BOStock.cRelatedItems(_Oasys3DB)
                                    RelItemBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, RelItemBO.SingleItem, StockMaster.SkuNumber.Value)
                                    If (RelItemBO.LoadMatches.Count > 0) Then
                                        Trace.WriteLine("UpdateTIBHUS:Processing Non 'S' Record -Delete RelItm:" & strTransmissionFileData.Substring(88, 6))
                                        Dim oBulkSKU As New BOStock.cStock(_Oasys3DB)
                                        oBulkSKU.SkuNumber.Value = strTransmissionFileData.Substring(88, 6)
                                        oBulkSKU.AdjustRelatedNoItems(RelItemBO.BulkOrPackageItem.Value, -1)
                                        If strTransmissionFileData.Substring(88, 6) <> "000000" Then
                                            RelItemBO.BulkOrPackageItem.Value = strTransmissionFileData.Substring(88, 6)
                                        End If
                                        RelItemBO.NoSinglesPerPack.Value = CDec(strTransmissionFileData.Substring(174, 5))
                                        RelItemBO.Deleted.Value = True
                                        RelItemBO.SaveIfExists()
                                        'StockMaster.RelatedNoItems.Value -= 1
                                    End If
                                End If
                                StockMaster.RelatedItemSingle.Value = False
                            End If

                            StockMaster.ItemStatus.Value = strU1StatusFlag

                            If strU1StatusFlag = "R" Then
                                StockMaster.DateObsolete.Value = Date.MinValue.Date
                                StockMaster.DateDeleted.Value = Date.MinValue.Date
                                StockMaster.ItemObsolete.Value = False
                                StockMaster.ItemDeleted.Value = False
                            End If
                            'End If
                            ' END of equivalent to the >SET-STATUS routine in TIBHUS

                            ' The following is equvalent to the >GEN-CHANGE routine in TIBHUS

                            StockMaster.SupplierNo.Value = strTransmissionFileData.Substring(53, 5).PadLeft(5, "0"c)
                            StockMaster.AlternateSupplier.Value = strTransmissionFileData.Substring(229, 5).PadLeft(5, "0"c)
                            If StockMaster.AlternateSupplier.Value = "00000" Then StockMaster.AlternateSupplier.Value = StockMaster.SupplierNo.Value

                            StockMaster.SavedSupplier.Value = StockMaster.AlternateSupplier.Value
                            StockMaster.SupplierPartCode.Value = strTransmissionFileData.Substring(58, 10)
                            decSupplierPackSizeSaved = StockMaster.SupplierPackSize.Value
                            decSupplierPackSizeWork = 0

                            If strTransmissionFileData.Substring(68, 3) <> "   " Then decSupplierPackSizeWork = CDec(strTransmissionFileData.Substring(68, 3))
                            If decSupplierPackSizeWork = 0 Then decSupplierPackSizeWork = StockMaster.SupplierPackSize.Value
                            StockMaster.SupplierPackSize.Value = CInt(decSupplierPackSizeWork)
                            If StockMaster.SupplierPackSize.Value < 1 Then StockMaster.SupplierPackSize.Value = 1
                            StockMaster.SupplierUnitCode.Value = strTransmissionFileData.Substring(71, 4)
                            If strTransmissionFileData.Substring(100, 40) <> Space(40) Then StockMaster.Description.Value = strTransmissionFileData.Substring(100, 40)
                            If strTransmissionFileData.Substring(140, 20) <> Space(20) Then StockMaster.ShortDescription.Value = strTransmissionFileData.Substring(140, 20)
                            StockMaster.VATRate.Value = "1"
                            If strTransmissionFileData.Substring(167, 1) = "2" Or strTransmissionFileData.Substring(167, 1) = "3" Then StockMaster.VATRate.Value = strTransmissionFileData.Substring(167, 1)
                            StockMaster.Weight.Value = 0
                            If strTransmissionFileData.Substring(205, 7) <> Space(7) Then StockMaster.Weight.Value = CDec(strTransmissionFileData.Substring(205, 7)) / 100
                            StockMaster.ItemVolume.Value = 0
                            If strTransmissionFileData.Substring(212, 7) <> Space(7) Then StockMaster.ItemVolume.Value = CDec(strTransmissionFileData.Substring(212, 7)) / 100
                            StockMaster.DoNotOrder.Value = False
                            If strTransmissionFileData.Substring(219, 1) = "Y" Then StockMaster.DoNotOrder.Value = True
                            StockMaster.NonStockItem.Value = False
                            If strTransmissionFileData.Substring(246, 1) = "Y" Then StockMaster.NonStockItem.Value = True
                            StockMaster.PromotionalMiniMCP.Value = strTransmissionFileData.Substring(247, 1)
                            If strTransmissionFileData.Substring(255, 8) <> "  /  /  " And strTransmissionFileData.Substring(255, 8) <> Space(8) Then StockMaster.InitialOrderDate.Value = CDate(strTransmissionFileData.Substring(255, 8))
                            If strTransmissionFileData.Substring(263, 8) <> "  /  /  " And strTransmissionFileData.Substring(263, 8) <> Space(8) Then StockMaster.FinalOrderDate.Value = CDate(strTransmissionFileData.Substring(263, 8))
                            If strTransmissionFileData.Substring(271, 6) <> Space(6) Then StockMaster.PromotionalMinimum.Value = CInt(strTransmissionFileData.Substring(271, 6))
                            If strTransmissionFileData.Substring(277, 8) <> "  /  /  " And strTransmissionFileData.Substring(277, 8) <> Space(8) Then StockMaster.PromotionalMinStart.Value = CDate(strTransmissionFileData.Substring(277, 8))
                            If strTransmissionFileData.Substring(285, 8) <> "  /  /  " And strTransmissionFileData.Substring(285, 8) <> Space(8) Then StockMaster.PromotionalMinEnd.Value = CDate(strTransmissionFileData.Substring(285, 8))
                            If strTransmissionFileData.Substring(293, 1) = Space(1) And StockMaster.ItemTagType.Value <> "L" Then StockMaster.ItemTagType.Value = strTransmissionFileData.Substring(293, 1)
                            If strTransmissionFileData.Substring(293, 1) <> Space(1) Then StockMaster.ItemTagType.Value = strTransmissionFileData.Substring(293, 1)
                            StockMaster.ItemTagged.Value = True
                            If StockMaster.ItemTagType.Value = Space(1) Then StockMaster.ItemTagged.Value = False
                            If StockMaster.HierCategory.Value <> strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) Or StockMaster.HierGroup.Value <> strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c) Or StockMaster.HierSubGroup.Value <> strTransmissionFileData.Substring(306, 6).PadLeft(6, "0"c) Or StockMaster.HierStyle.Value <> strTransmissionFileData.Substring(312, 6).PadLeft(6, "0"c) Or StockMaster.HierFilter.Value <> (strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) & strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c)) Then
                                If boolSophie = False Then
                                    For Each sysopt As BOSystem.cSystemOptions In Colso
                                        Try
                                            sysopt.RecreatePHL.Value = True
                                            sysopt.SaveIfExists()
                                        Catch ex As Exception
                                        End Try
                                    Next
                                    boolSophie = True
                                End If
                            End If
                            StockMaster.HierCategory.Value = strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c)
                            StockMaster.HierGroup.Value = strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c)
                            StockMaster.HierSubGroup.Value = strTransmissionFileData.Substring(306, 6).PadLeft(6, "0"c)
                            StockMaster.HierStyle.Value = strTransmissionFileData.Substring(312, 6).PadLeft(6, "0"c)
                            StockMaster.HierFilter.Value = (strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) & strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c))
                            StockMaster.PatternSeason.Value = strTransmissionFileData.Substring(234, 4).PadLeft(4, "0"c)
                            StockMaster.PatternBankHol.Value = strTransmissionFileData.Substring(238, 4).PadLeft(4, "0"c)
                            StockMaster.PatternPromo.Value = strTransmissionFileData.Substring(242, 4).PadLeft(4, "0"c)
                            Dim decServiceLevelOverrideSaved As Decimal = StockMaster.ServiceLevelOverride.Value
                            StockMaster.ServiceLevelOverride.Value = CDec(strTransmissionFileData.Substring(248, 7))
                            If StockMaster.ServiceLevelOverride.Value = SoqoptionsServiceLevel Then StockMaster.ServiceLevelOverride.Value = 0
                            'If StockMaster.SupplierPackSize.Value <> decSupplierPackSizeSaved Or StockMaster.ServiceLevelOverride.Value <> SoqoptionsServiceLevel Then StockMaster.ForecastInd.Value = "W"
                            StockMaster.Warranty.Value = False
                            If strTransmissionFileData.Substring(326, 1) = "Y" Then StockMaster.Warranty.Value = True
                            StockMaster.OffensiveWeaponAge.Value = CInt(strTransmissionFileData.Substring(327, 2))
                            StockMaster.RetailPriceEventNo.Value = strTransmissionFileData.Substring(318, 6)
                            StockMaster.RetailPricePriority.Value = strTransmissionFileData.Substring(324, 2)
                            StockMaster.SolventAge.Value = CInt(strTransmissionFileData.Substring(329, 2))
                            StockMaster.QuarantineFlag.Value = strTransmissionFileData.Substring(331, 1)
                            StockMaster.PricingDiscrepency.Value = False
                            If strTransmissionFileData.Substring(332, 1) = "Y" Then StockMaster.PricingDiscrepency.Value = True
                            StockMaster.EquivalentPriceMult.Value = CDec(strTransmissionFileData.Substring(333, 12))
                            StockMaster.EquivalentPriceUnit.Value = strTransmissionFileData.Substring(345, 7)
                            StockMaster.SaleTypeAttribute.Value = strTransmissionFileData.Substring(352, 1)
                            If StockMaster.SaleTypeAttribute.Value <> "B" And StockMaster.SaleTypeAttribute.Value <> "P" And StockMaster.SaleTypeAttribute.Value <> "A" And StockMaster.SaleTypeAttribute.Value <> "S" And StockMaster.SaleTypeAttribute.Value <> "G" And StockMaster.SaleTypeAttribute.Value <> "R" And StockMaster.SaleTypeAttribute.Value <> "D" And StockMaster.SaleTypeAttribute.Value <> "I" And StockMaster.SaleTypeAttribute.Value <> "V" And StockMaster.SaleTypeAttribute.Value <> "T" And StockMaster.SaleTypeAttribute.Value <> "Y" And StockMaster.SaleTypeAttribute.Value <> "W" Then StockMaster.SaleTypeAttribute.Value = "O"
                            StockMaster.AutoApplyPriceChgs.Value = False
                            If strTransmissionFileData.Substring(354, 1) = "Y" Then StockMaster.AutoApplyPriceChgs.Value = True
                            StockMaster.PalletedSku.Value = False
                            If strTransmissionFileData.Substring(355, 1) = "Y" Then StockMaster.PalletedSku.Value = True
                            StockMaster.AllowAdjustments.Value = Space(1)
                            If strTransmissionFileData.Substring(356, 1) = "1" Or strTransmissionFileData.Substring(356, 1) = "2" Or strTransmissionFileData.Substring(356, 1) = "3" Or strTransmissionFileData.Substring(356, 1) = "4" Or strTransmissionFileData.Substring(356, 1) = "5" Or strTransmissionFileData.Substring(356, 1) = "6" Or strTransmissionFileData.Substring(356, 1) = "7" Then StockMaster.AllowAdjustments.Value = strTransmissionFileData.Substring(356, 1)
                            StockMaster.InsulationItem.Value = False
                            If strTransmissionFileData.Substring(357, 1) = "Y" Then StockMaster.InsulationItem.Value = True
                            StockMaster.ManagedTimberCont.Value = 0
                            If strTransmissionFileData.Substring(358, 3) <> Space(3) Then StockMaster.ManagedTimberCont.Value = CInt(strTransmissionFileData.Substring(358, 3))
                            StockMaster.ELectricalItem.Value = False
                            If strTransmissionFileData.Substring(361, 1) = "Y" Then StockMaster.ELectricalItem.Value = True
                            StockMaster.StockHeldOffSale.Value = 0
                            If strTransmissionFileData.Substring(362, 1) = "Y" Then StockMaster.StockHeldOffSale.Value = 1
                            'StockMaster.StockHeldOffSale.Value = strTransmissionFileData.Substring(362, 1)
                            StockMaster.BallastItem.Value = strTransmissionFileData.Substring(363, 1)
                            StockMaster.RecyclingSkuNumber.Value = strTransmissionFileData.Substring(364, 6).PadLeft(6, "0"c)
                            If StockMaster.CatchAll.Value = False Then
                                If strTransmissionFileData.Substring(185, 7) <> Space(7) Then StockMaster.CostPrice.Value = (CDec(strTransmissionFileData.Substring(185, 7)) / 10000)
                            End If
                            If StockMaster.RelatedItemSingle.Value = True Then StockMaster.MinQuantity.Value = 0
                            strRemoveEanSku = String.Empty
                            strEanNumber = "0000000000000000"
                            If strTransmissionFileData.Substring(192, 13) <> Space(13) Then
                                longEanNumber = CLng(strTransmissionFileData.Substring(192, 13))
                                strEanNumber = longEanNumber.ToString.PadLeft(16, "0"c)
                            End If

                            If strEanNumber <> "0000000000000000" Then
                                EanMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EanMaster.SKUNumber, StockMaster.SkuNumber.Value)
                                ColEa = EanMaster.LoadMatches()
                                For Each EANMAS As BOStock.cBarCodes In ColEa
                                    EANMAS.Delete()
                                Next
                                EanMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EanMaster.Number, strEanNumber)
                                ColEa = EanMaster.LoadMatches()
                                If ColEa.Count = 0 Then 'not found so create
                                    EanMaster.SKUNumber.Value = StockMaster.SkuNumber.Value
                                    EanMaster.Number.Value = strEanNumber
                                    EanMaster.SaveIfNew()
                                    StockMaster.EAN.Value = 1
                                End If
                                If ColEa.Count > 0 Then
                                    For Each EANMAS As BOStock.cBarCodes In ColEa
                                        If StockMaster.SkuNumber.Value <> EANMAS.SKUNumber.Value Then
                                            Try
                                                Dim EANSKU As New BOStock.cStock(_Oasys3DB)
                                                EANSKU.UpdateEANCount(EANMAS.SKUNumber.Value, -1)
                                                strRemoveEanSku = EANMAS.SKUNumber.Value
                                                EANMAS.SKUNumber.Value = StockMaster.SkuNumber.Value
                                                EANMAS.SaveIfExists()
                                                StockMaster.EAN.Value += 1
                                            Catch ex As Exception
                                            End Try
                                        End If
                                    Next
                                End If
                            End If
                            Trace.WriteLine("U4:Update STKMAS")
                            StockMaster.SaveIfExists()
                        Catch ex As Exception
                            Trace.WriteLine("Error in U4:" & ex.Message & ":" & ex.StackTrace)
                        End Try
                    End If
                End If

                If strTransmissionFileData.StartsWith("U1") Then strTransmissionFileData = strTransmissionFileData.PadRight(370, " "c)
                If strTransmissionFileData.StartsWith("U1") Or boolU4NotOnFile = True Then
                    boolU1AddRecord = False
                    boolChangesCanBeMade = False
                    StockMaster = New BOStock.cStock(_Oasys3DB)
                    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTransmissionFileData.Substring(26, 6).PadLeft(6, "0"c))
                    StockMaster.Stocks = StockMaster.LoadMatches()
                    If StockMaster.Stocks.Count > 0 Then
                        If boolDoingAStoreLoad = True Or strTransmissionFileData.Substring(51, 1) = "+" Then boolChangesCanBeMade = True
                    End If
                    If StockMaster.Stocks.Count < 1 Then
                        StockMaster.Stocks.Clear()
                        StockMaster.ClearLoadFilter()
                        StockMaster.SkuNumber.Value = strTransmissionFileData.Substring(26, 6).PadLeft(6, "0"c)
                        'StockMaster.SaveIfNew()
                        boolU1AddRecord = True
                        boolChangesCanBeMade = True
                    End If
                    If boolChangesCanBeMade = True Then
                        Try
                            StockMaster.PriorSellPrice.Value = 0
                            strWorkPrice = String.Empty & strTransmissionFileData.Substring(33, 6)
                            If strWorkPrice.StartsWith("0 ") Then strWorkPrice = String.Empty & strTransmissionFileData.Substring(35, 4).PadLeft(6, "0"c)
                            If strWorkPrice.StartsWith("00 ") Then strWorkPrice = String.Empty & strTransmissionFileData.Substring(36, 3).PadLeft(6, "0"c)
                            If strWorkPrice.StartsWith("000 ") Then strWorkPrice = String.Empty & strTransmissionFileData.Substring(37, 2).PadLeft(6, "0"c)
                            If strWorkPrice <> "000000" Then StockMaster.NormalSellPrice.Value = (CDec(strWorkPrice) / 100)
                            StockMaster.DateCreated.Value = Today.Date.Date
                            StockMaster.SpacemanDisplayFctr.Value = CInt(strTransmissionFileData.Substring(220, 5))
                            StockMaster.MinQuantity.Value = CInt(strTransmissionFileData.Substring(220, 5))
                            StockMaster.NoOfSmallLabels.Value = 1
                            ' The following is equivalent to the >SET-STATUS routine in TIBHUS
                            strU1StatusFlag = strTransmissionFileData.Substring(51, 1)
                            'If strTransmissionFileData.Substring(85, 3) = "666" Then
                            '    stockmaster.
                            'End If
                            StockMaster.DateObsolete.Value = Date.MinValue.Date
                            StockMaster.DateDeleted.Value = Date.MinValue.Date
                            If strU1StatusFlag = "D" Then
                                If StockMaster.ItemObsolete.Value = False Then
                                    StockMaster.DateObsolete.Value = Today.Date.Date
                                    StockMaster.ItemObsolete.Value = True
                                End If
                            End If

                            If strU1StatusFlag <> "D" Then
                                StockMaster.DateObsolete.Value = Date.MinValue.Date
                                StockMaster.DateDeleted.Value = Date.MinValue.Date
                                StockMaster.ItemObsolete.Value = False
                                StockMaster.ItemDeleted.Value = False

                            End If
                            If strU1StatusFlag = "S" Then
                                If StockMaster.RelatedItemSingle.Value = False Then StockMaster.RelatedItemSingle.Value = True
                            End If
                            If strU1StatusFlag <> "S" Then StockMaster.RelatedItemSingle.Value = False
                            StockMaster.ItemStatus.Value = strU1StatusFlag

                            If strU1StatusFlag = "R" Then
                                StockMaster.DateObsolete.Value = Date.MinValue.Date
                                StockMaster.DateDeleted.Value = Date.MinValue.Date
                                StockMaster.ItemObsolete.Value = False
                                StockMaster.ItemDeleted.Value = False
                            End If
                            'End If
                            ' END of equivalent to the >SET-STATUS routine in TIBHUS

                            ' The following is equvalent to the >GEN-CHANGE routine in TIBHUS

                            StockMaster.SupplierNo.Value = strTransmissionFileData.Substring(53, 5).PadLeft(5, "0"c)
                            StockMaster.AlternateSupplier.Value = strTransmissionFileData.Substring(229, 5).PadLeft(5, "0"c)
                            If StockMaster.AlternateSupplier.Value = "00000" Then StockMaster.AlternateSupplier.Value = StockMaster.SupplierNo.Value

                            StockMaster.SavedSupplier.Value = StockMaster.AlternateSupplier.Value
                            StockMaster.SupplierPartCode.Value = strTransmissionFileData.Substring(58, 10)
                            decSupplierPackSizeSaved = StockMaster.SupplierPackSize.Value
                            decSupplierPackSizeWork = 0

                            If strTransmissionFileData.Substring(68, 3) <> "   " Then decSupplierPackSizeWork = CDec(strTransmissionFileData.Substring(68, 3))
                            If decSupplierPackSizeWork = 0 Then decSupplierPackSizeWork = StockMaster.SupplierPackSize.Value
                            StockMaster.SupplierPackSize.Value = CInt(decSupplierPackSizeWork)
                            If StockMaster.SupplierPackSize.Value < 1 Then StockMaster.SupplierPackSize.Value = 1
                            StockMaster.SupplierUnitCode.Value = strTransmissionFileData.Substring(71, 4)
                            If strTransmissionFileData.Substring(100, 40) <> Space(40) Then StockMaster.Description.Value = strTransmissionFileData.Substring(100, 40)
                            If strTransmissionFileData.Substring(140, 20) <> Space(20) Then StockMaster.ShortDescription.Value = strTransmissionFileData.Substring(140, 20)
                            StockMaster.VATRate.Value = "1"
                            If strTransmissionFileData.Substring(167, 1) = "2" Or strTransmissionFileData.Substring(167, 1) = "3" Then StockMaster.VATRate.Value = strTransmissionFileData.Substring(167, 1)
                            StockMaster.Weight.Value = 0
                            If strTransmissionFileData.Substring(205, 7) <> Space(7) Then StockMaster.Weight.Value = CDec(strTransmissionFileData.Substring(205, 7)) / 100
                            StockMaster.ItemVolume.Value = 0
                            If strTransmissionFileData.Substring(212, 7) <> Space(7) Then StockMaster.ItemVolume.Value = CDec(strTransmissionFileData.Substring(212, 7)) / 100
                            StockMaster.DoNotOrder.Value = False
                            If strTransmissionFileData.Substring(219, 1) = "Y" Then StockMaster.DoNotOrder.Value = True
                            StockMaster.NonStockItem.Value = False
                            If strTransmissionFileData.Substring(246, 1) = "Y" Then StockMaster.NonStockItem.Value = True
                            StockMaster.PromotionalMiniMCP.Value = strTransmissionFileData.Substring(247, 1)
                            If strTransmissionFileData.Substring(255, 8) <> "  /  /  " And strTransmissionFileData.Substring(255, 8) <> Space(8) Then StockMaster.InitialOrderDate.Value = CDate(strTransmissionFileData.Substring(255, 8))
                            If strTransmissionFileData.Substring(263, 8) <> "  /  /  " And strTransmissionFileData.Substring(263, 8) <> Space(8) Then StockMaster.FinalOrderDate.Value = CDate(strTransmissionFileData.Substring(263, 8))
                            If strTransmissionFileData.Substring(271, 6) <> Space(6) Then StockMaster.PromotionalMinimum.Value = CInt(strTransmissionFileData.Substring(271, 6))
                            If strTransmissionFileData.Substring(277, 8) <> "  /  /  " And strTransmissionFileData.Substring(277, 8) <> Space(8) Then StockMaster.PromotionalMinStart.Value = CDate(strTransmissionFileData.Substring(277, 8))
                            If strTransmissionFileData.Substring(285, 8) <> "  /  /  " And strTransmissionFileData.Substring(285, 8) <> Space(8) Then StockMaster.PromotionalMinEnd.Value = CDate(strTransmissionFileData.Substring(285, 8))
                            If strTransmissionFileData.Substring(293, 1) = Space(1) And StockMaster.ItemTagType.Value <> "L" Then StockMaster.ItemTagType.Value = strTransmissionFileData.Substring(293, 1)
                            If strTransmissionFileData.Substring(293, 1) <> Space(1) Then StockMaster.ItemTagType.Value = strTransmissionFileData.Substring(293, 1)
                            StockMaster.ItemTagged.Value = True
                            If StockMaster.ItemTagType.Value = Space(1) Then StockMaster.ItemTagged.Value = False
                            If StockMaster.HierCategory.Value <> strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) Or StockMaster.HierGroup.Value <> strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c) Or StockMaster.HierSubGroup.Value <> strTransmissionFileData.Substring(306, 6).PadLeft(6, "0"c) Or StockMaster.HierStyle.Value <> strTransmissionFileData.Substring(312, 6).PadLeft(6, "0"c) Or StockMaster.HierFilter.Value <> (strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) & strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c)) Then
                                If boolSophie = False Then
                                    For Each sysopt As BOSystem.cSystemOptions In Colso
                                        Try
                                            sysopt.RecreatePHL.Value = True
                                            sysopt.SaveIfExists()
                                        Catch ex As Exception
                                        End Try
                                    Next
                                    boolSophie = True
                                End If
                            End If
                            StockMaster.HierCategory.Value = strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c)
                            StockMaster.HierGroup.Value = strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c)
                            StockMaster.HierSubGroup.Value = strTransmissionFileData.Substring(306, 6).PadLeft(6, "0"c)
                            StockMaster.HierStyle.Value = strTransmissionFileData.Substring(312, 6).PadLeft(6, "0"c)
                            StockMaster.HierFilter.Value = (strTransmissionFileData.Substring(294, 6).PadLeft(6, "0"c) & strTransmissionFileData.Substring(300, 6).PadLeft(6, "0"c))
                            StockMaster.PatternSeason.Value = strTransmissionFileData.Substring(234, 4).PadLeft(4, "0"c)
                            StockMaster.PatternBankHol.Value = strTransmissionFileData.Substring(238, 4).PadLeft(4, "0"c)
                            StockMaster.PatternPromo.Value = strTransmissionFileData.Substring(242, 4).PadLeft(4, "0"c)
                            Dim decServiceLevelOverrideSaved As Decimal = StockMaster.ServiceLevelOverride.Value
                            StockMaster.ServiceLevelOverride.Value = CDec(strTransmissionFileData.Substring(248, 7))
                            If StockMaster.ServiceLevelOverride.Value = SoqoptionsServiceLevel Then StockMaster.ServiceLevelOverride.Value = 0
                            'If StockMaster.SupplierPackSize.Value <> decSupplierPackSizeSaved Or StockMaster.ServiceLevelOverride.Value <> SoqoptionsServiceLevel Then StockMaster.ForecastInd.Value = "W"
                            StockMaster.Warranty.Value = False
                            If strTransmissionFileData.Substring(326, 1) = "Y" Then StockMaster.Warranty.Value = True
                            StockMaster.OffensiveWeaponAge.Value = CInt(strTransmissionFileData.Substring(327, 2))
                            StockMaster.RetailPriceEventNo.Value = strTransmissionFileData.Substring(318, 6)
                            StockMaster.RetailPricePriority.Value = strTransmissionFileData.Substring(324, 2)
                            StockMaster.SolventAge.Value = CInt(strTransmissionFileData.Substring(329, 2))
                            StockMaster.QuarantineFlag.Value = strTransmissionFileData.Substring(331, 1)
                            StockMaster.PricingDiscrepency.Value = CBool(IIf(strTransmissionFileData.Substring(332, 1) = "Y", True, False))
                            StockMaster.EquivalentPriceMult.Value = CDec(strTransmissionFileData.Substring(333, 12))
                            StockMaster.EquivalentPriceUnit.Value = strTransmissionFileData.Substring(345, 7)
                            StockMaster.SaleTypeAttribute.Value = strTransmissionFileData.Substring(352, 1)
                            If StockMaster.SaleTypeAttribute.Value <> "B" And StockMaster.SaleTypeAttribute.Value <> "P" And StockMaster.SaleTypeAttribute.Value <> "A" And StockMaster.SaleTypeAttribute.Value <> "S" And StockMaster.SaleTypeAttribute.Value <> "G" And StockMaster.SaleTypeAttribute.Value <> "R" And StockMaster.SaleTypeAttribute.Value <> "D" And StockMaster.SaleTypeAttribute.Value <> "I" And StockMaster.SaleTypeAttribute.Value <> "V" And StockMaster.SaleTypeAttribute.Value <> "T" And StockMaster.SaleTypeAttribute.Value <> "Y" And StockMaster.SaleTypeAttribute.Value <> "W" Then StockMaster.SaleTypeAttribute.Value = "O"
                            StockMaster.AutoApplyPriceChgs.Value = CBool(IIf(strTransmissionFileData.Substring(354, 1) = "Y", True, False))
                            StockMaster.PalletedSku.Value = False
                            If strTransmissionFileData.Substring(355, 1) = "Y" Then StockMaster.PalletedSku.Value = True
                            StockMaster.AllowAdjustments.Value = Space(1)
                            If strTransmissionFileData.Substring(356, 1) = "1" Or strTransmissionFileData.Substring(356, 1) = "2" Or strTransmissionFileData.Substring(356, 1) = "3" Or strTransmissionFileData.Substring(356, 1) = "4" Or strTransmissionFileData.Substring(356, 1) = "5" Or strTransmissionFileData.Substring(356, 1) = "6" Or strTransmissionFileData.Substring(356, 1) = "7" Then StockMaster.AllowAdjustments.Value = strTransmissionFileData.Substring(356, 1)
                            StockMaster.InsulationItem.Value = False
                            If strTransmissionFileData.Substring(357, 1) = "Y" Then StockMaster.InsulationItem.Value = True
                            StockMaster.ManagedTimberCont.Value = 0
                            If strTransmissionFileData.Substring(358, 3) <> Space(3) Then StockMaster.ManagedTimberCont.Value = CInt(strTransmissionFileData.Substring(358, 3))
                            StockMaster.ELectricalItem.Value = False
                            If strTransmissionFileData.Substring(361, 1) = "Y" Then StockMaster.ELectricalItem.Value = True
                            StockMaster.StockHeldOffSale.Value = 0
                            If strTransmissionFileData.Substring(362, 1) = "Y" Then StockMaster.StockHeldOffSale.Value = 1
                            'StockMaster.StockHeldOffSale.Value = strTransmissionFileData.Substring(362, 1)
                            StockMaster.BallastItem.Value = strTransmissionFileData.Substring(363, 1)
                            StockMaster.RecyclingSkuNumber.Value = strTransmissionFileData.Substring(364, 6).PadLeft(6, "0"c)
                            If StockMaster.CatchAll.Value = False Then
                                If strTransmissionFileData.Substring(185, 7) <> Space(7) Then StockMaster.CostPrice.Value = (CDec(strTransmissionFileData.Substring(185, 7)) / 10000)
                            End If
                            If StockMaster.RelatedItemSingle.Value = True Then StockMaster.MinQuantity.Value = 0
                            If (StockMaster.RelatedItemSingle.Value = True) Then

                            End If
                            strRemoveEanSku = String.Empty
                            strEanNumber = "0000000000000000"
                            If strTransmissionFileData.Substring(192, 13) <> Space(13) Then
                                longEanNumber = CLng(strTransmissionFileData.Substring(192, 13))
                                strEanNumber = longEanNumber.ToString.PadLeft(16, "0"c)
                            End If
                            If strEanNumber <> "0000000000000000" Then
                                EanMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EanMaster.SKUNumber, StockMaster.SkuNumber.Value)
                                ColEa = EanMaster.LoadMatches()
                                For Each EANMAS As BOStock.cBarCodes In ColEa
                                    EANMAS.Delete()
                                Next
                                EanMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EanMaster.Number, strEanNumber)
                                ColEa = EanMaster.LoadMatches()
                                If ColEa.Count = 0 Then
                                    Try
                                        EanMaster.SKUNumber.Value = StockMaster.SkuNumber.Value
                                        EanMaster.Number.Value = strEanNumber
                                        EanMaster.SaveIfNew()
                                    Catch ex As Exception
                                    End Try
                                    StockMaster.EAN.Value = 1
                                End If
                                If ColEa.Count > 0 Then
                                    For Each EANMAS As BOStock.cBarCodes In ColEa
                                        If StockMaster.SkuNumber.Value <> EANMAS.SKUNumber.Value Then
                                            Try
                                                strRemoveEanSku = EANMAS.SKUNumber.Value
                                                Dim EANSKU As New BOStock.cStock(_Oasys3DB)
                                                EANSKU.UpdateEANCount(EANMAS.SKUNumber.Value, -1)
                                                EANMAS.SKUNumber.Value = StockMaster.SkuNumber.Value
                                                EANMAS.SaveIfExists()
                                                StockMaster.EAN.Value += 1
                                            Catch ex As Exception
                                            End Try
                                        End If
                                    Next
                                End If
                            End If
                            If boolU1AddRecord = True Then
                                Trace.WriteLine("U1:Add STKMAS Record")
                                StockMaster.PriorSellPrice.Value = StockMaster.NormalSellPrice.Value
                                StockMaster.SaveIfNew()
                                'Create STKLOG entry for new record
                                Dim oStockLogBO As New BOStock.cStockLog(_Oasys3DB)
                                oStockLogBO.InsertStart99(Today, "HOSTU " & Format(Today, "dd/MM/yy"), 0, StockMaster.SkuNumber.Value, 0, 0, 0, 0, 0, 0, 0, 0, 0, StockMaster.NormalSellPrice.Value)
                            Else
                                Trace.WriteLine("U1:Update STKMAS Record")
                                StockMaster.SaveIfExists()
                            End If
                        Catch ex As Exception
                            Trace.WriteLine("Error in U1:" & ex.Message & ":" & ex.StackTrace)
                        End Try
                    Else
                        Trace.WriteLine("Changes cannot be made:" & CStr(IIf(boolDoingAStoreLoad, "Store Load", "Non Store Load")) & "," & strTransmissionFileData.Substring(51, 1))
                    End If
                End If
                '******* Processing TYPE U5 STARTS HERE

                If strTransmissionFileData.StartsWith("U5") Then
                    strTransmissionFileData = strTransmissionFileData.PadRight(112, " "c)
                    strU5Type = strTransmissionFileData.Substring(26, 1)
                    strU5Skun = strTransmissionFileData.Substring(28, 6)
                    strU5Sequence = strTransmissionFileData.Substring(34, 3)
                    If strU5DeleteSkun <> strU5Skun Then
                        strU5DeleteSkun = strU5Skun
                        strU5DeleteType = strU5Type
                        StockText.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockText.ItemNumber, strU5DeleteSkun)
                        StockText.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        StockText.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockText.Type, strU5DeleteType)
                        ColIt = StockText.LoadMatches()
                        If ColIt.Count > 0 Then
                            For Each textrecord As BOStock.cStockText In ColIt
                                textrecord.Delete()
                            Next
                        End If
                    End If
                    If strU5DeleteSkun = strU5Skun And strU5DeleteType <> strU5Type Then
                        strU5DeleteSkun = strU5Skun
                        strU5DeleteType = strU5Type
                        StockText.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockText.ItemNumber, strU5DeleteSkun)
                        StockText.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        StockText.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockText.Type, strU5DeleteType)
                        ColIt = StockText.LoadMatches()
                        If ColIt.Count > 0 Then
                            For Each textrecord As BOStock.cStockText In ColIt
                                textrecord.Delete()
                            Next
                        End If
                    End If
                    If strTransmissionFileData.Substring(27, 1) <> "Y" Then
                        Try
                            StockText.Type.Value = strU5Type
                            StockText.ItemNumber.Value = strU5Skun
                            StockText.Seqno.Value = strU5Sequence
                            StockText.Text.Value = strTransmissionFileData.Substring(37, 75)
                            If (StockText.Text.Value.Trim.Length > 0) Then StockText.SaveIfNew()
                            intTypeU5RecordsUpdated = intTypeU5RecordsUpdated + 1
                        Catch ex As Exception
                        End Try
                    End If
                End If
                If strTransmissionFileData.StartsWith("U6") Then
                    strTransmissionFileData = strTransmissionFileData.PadRight(68, " "c)
                    StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTransmissionFileData.Substring(22, 6))
                    StockMaster.Stocks = StockMaster.LoadMatches()
                    If StockMaster.Stocks.Count > 0 Then
                        strCurrentEquiv = String.Empty & StockMaster.ProductEquivDescr.Value.PadRight(40, " "c)
                        strNewEquiv = String.Empty & strTransmissionFileData.PadRight(40, " "c).Substring(28, 40)
                        'If StockMaster.ProductEquivDescr.Value.PadRight(40, " ") <> strTransmissionFileData.PadRight(68, " ").Substring(28, 40) Then
                        If strCurrentEquiv <> strNewEquiv Then
                            Try
                                StockMaster.ProductEquivDescr.Value = strNewEquiv
                                StockMaster.SaveIfExists()
                                intTypeU6RecordsUpdated = intTypeU6RecordsUpdated + 1
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                End If
            End While
            reader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strTibhusWorkFile & " Completed : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)

        End If
        If File.Exists(strTibhusWorkFile) Then
            My.Computer.FileSystem.DeleteFile(strTibhusWorkFile)
        End If
        _NoRecInTibhus = 0
    End Sub
    Public Sub DataHOSTUIntegrity(ByVal strTestString As String) ' Validate HOSTU data
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0
        Dim RecType As String = strTestString.Substring(0, 2)
        Dim RecTypeKnown As Boolean = False

        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(400, " "c)
        Dim intLengthTestString As Integer = strTestString.Length
        boolPassedValidation = False
        strRecordType = "D"
        intRecordOccurrence = 0
        'Locate Record Counter Position using Record Type prefix

        If (RecType = "HR") Or (RecType = "TR") Then Exit Sub
        For intX = 0 To 14
            If (RecType = arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 13

        If (RecType = "U1") Or (RecType = "U4") Then
            RecTypeKnown = True
            If intPassNumber = 1 Then
                If strTestString.Substring(51, 1) = "S" Then
                    boolNeedSecondPass = True
                Else
                    If ValidateTheData(strTestString, arrstrU1NumericValidation, arrstrU1IndicatorValidation) = True Then
                        boolPassedValidation = ValidateU1andU4Data(strTestString)
                    End If
                End If
            Else
                If strTestString.Substring(51, 1) = "S" Then
                    If ValidateTheData(strTestString, arrstrU1NumericValidation, arrstrU1IndicatorValidation) = True Then
                        boolPassedValidation = ValidateU1andU4Data(strTestString)
                    End If
                End If
            End If
        End If

        If (intPassNumber = 1) Then

            If (RecType = "U5") Then
                RecTypeKnown = True
                If ValidateTheData(strTestString, arrstrU5NumericValidation, arrstrU5IndicatorValidation) = True Then
                    boolPassedValidation = ValidateU5Data(strTestString)
                End If
            End If

            If (RecType = "U6") Then
                RecTypeKnown = True
                If ValidateTheData(strTestString, arrstrU6NumericValidation, arrstrU6IndicatorValidation) = True Then
                    boolPassedValidation = ValidateU6Data(strTestString)
                End If
            End If

        End If 'first pass

        If (RecType = "UA") Or (RecType = "UB") Or (RecType = "UC") Or (RecType = "UD") Or (RecType = "UE") Then
            RecTypeKnown = True
            boolNeedSecondPass = True
        End If

        If intPassNumber > 1 Then
            If File.Exists(strTibhusWorkFile) Then
                If strTibhusText.Length > 0 Then
                    PutSthoToDisc(strTibhusWorkFile, strTibhusText)
                    strTibhusText = String.Empty
                End If
                If _NoRecInTibhus > 0 Then UpdateDataBaseFromTibhus()

            End If
            If (RecType = "UA") Then
                If ValidateTheData(strTestString, arrstrUANumericValidation, arrstrUAIndicatorValidation) = True Then
                    boolPassedValidation = ValidateUAData(strTestString)
                End If
            End If

            If (RecType = "UB") Then
                If ValidateTheData(strTestString, arrstrUBNumericValidation, arrstrUBIndicatorValidation) = True Then
                    boolPassedValidation = ValidateUBData(strTestString)
                End If
            End If

            If (RecType = "UC") Then
                If boolNeedToRunTibhue = True Then
                    If strTibhueText.Length > 0 Then
                        PutSthoToDisc(strTibhueWorkFile, strTibhueText)
                        strTibhueText = String.Empty
                    End If
                    If _NoRecInTibhue > 0 Then UpdateDataBaseFromTibhue()
                End If
                If ValidateTheData(strTestString, arrstrUCNumericValidation, arrstrUCIndicatorValidation) = True Then
                    boolPassedValidation = ValidateUCData(strTestString)
                End If
            End If

            If (RecType = "UD") Then
                If ValidateTheData(strTestString, arrstrUDNumericValidation, arrstrUDIndicatorValidation) = True Then
                    boolPassedValidation = ValidateUDData(strTestString)
                End If
            End If

            If (RecType = "UE") Then
                If ValidateTheData(strTestString, arrstrUENumericValidation, arrstrUEIndicatorValidation) = True Then
                    boolPassedValidation = ValidateUEData(strTestString)
                End If
            End If
        End If
        If (RecType = "UM") Or (RecType = "UT") Then
            boolPassedValidation = True
            RecTypeKnown = True
        End If

        If boolPassedValidation = True Then
            ' Output data to appropriate work file - TIBHUS or TIBHUE
            If (RecType = "UA") Or (RecType = "UB") Or (RecType = "UC") Or (RecType = "UD") Or (RecType = "UE") Or (RecType = "UM") Or (RecType = "UT") Then
                RecTypeKnown = True
                If (RecType = "UA") Or (RecType = "UB") Or (RecType = "UD") Or (RecType = "UE") Or (RecType = "UM") Or (RecType = "UT") Then boolNeedToRunTibhue = True
                If strTibhueText <> String.Empty Then
                    strTibhueText = strTibhueText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strTibhueText = strTibhueText.ToString.TrimEnd(" "c) & strTestString
                _NoRecInTibhue += 1
                intSthocRecordsOut += 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
                ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                If strTibhueText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strTibhueWorkFile, strTibhueText)
                    strTibhueText = String.Empty
                End If
            End If
            If (RecType = "U1") Or (RecType = "U4") Or (RecType = "U5") Or (RecType = "U6") Then
                If strTibhusText <> String.Empty Then
                    strTibhusText = strTibhusText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strTibhusText = strTibhusText.ToString.TrimEnd(" "c) & strTestString
                _NoRecInTibhus += 1
                intSthocRecordsOut += 1
                ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
                ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                If strTibhusText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(strTibhusWorkFile, strTibhusText)
                    strTibhusText = String.Empty
                End If
            End If
        End If
        If (RecTypeKnown = False) And (intPassNumber = 1) Then
            OutputProductUpdateError(strTestString, "Wrong Record Type")
            intHeadOfficeBadType = intHeadOfficeBadType + 1
        End If
    End Sub ' Validate HOSTU data
    Public Sub DataIntegrityHostc(ByVal strTestString As String) ' Validate HOSTC data
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0

        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(400, " "c)
        Dim intLengthTestString As Integer = strTestString.Length
        boolPassedValidation = False
        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 4

        If strTestString.StartsWith("CY") Then boolPassedValidation = ValidateCYData(strTestString.PadRight(24, " "c))
        If strTestString.StartsWith("CC") Then boolPassedValidation = ValidateCCData(strTestString.PadRight(48, " "c))
        If strTestString.StartsWith("CM") Then boolPassedValidation = ValidateCMData(strTestString.PadRight(39, " "c), True)

    End Sub ' Validate HOSTC data
    Public Sub DataIntegrityHostd(ByVal strTestString As String) ' Validate HOSTD data also perform the updates

        Dim StrMyString As String = String.Empty
        Dim intX As Integer = 0
        Dim Strmas As New BOStore.cStore(_Oasys3DB)
        Dim ColOs As New List(Of BOStore.cStore)
        Dim intLengthTestString As Integer = strTestString.Length

        Dim intTestStoreNumber As Integer = 0

        strTestString = strTestString.PadRight(223, " "c)
        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        ' ------- Check gor TYPE "SM"
        boolPassedValidation = True
        If (strTestString.StartsWith("HR") = False) And (strTestString.StartsWith("TR") = False) And (strTestString.StartsWith("SM") = False) Then 'Processing type "SM" record
            OutputStoreMasterUpdateError(strTestString, "WRONG RECORD TYPE")
            boolPassedValidation = False
            Exit Sub
        End If
        If strTestString.StartsWith("SM") Then 'Processing type "SM" record
            If strTestString.Substring(22, 1) <> "D" And strTestString.Substring(22, 1) <> " " Then
                OutputStoreMasterUpdateError(strTestString, "INVALID STATUS")
                boolPassedValidation = False
            Else ' SM record has passed validation
                intTestStoreNumber = CInt(strTestString.Substring(23, 3).PadRight(3, "0"c))
                If intTestStoreNumber < 1 Then
                    OutputStoreMasterUpdateError(strTestString, "INVALID STORE")
                    boolPassedValidation = False
                Else 'Correct Store Number so continue
                    Strmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Strmas.StoreID, strTestString.Substring(23, 3).PadLeft(3, "0"c))
                    ColOs = Strmas.LoadMatches()
                    If ColOs.Count > 0 And strTestString.Substring(22, 1) = "D" Then
                        Strmas.Delete()
                        boolPassedValidation = False
                    End If
                    If boolPassedValidation = True Then ' Still Ok to update
                        Try
                            If ColOs.Count < 1 Then
                                Strmas.StoreID.Value = strTestString.Substring(23, 3).PadLeft(3, "0"c)
                            End If
                            Strmas.AddressLine1.Value = strTestString.Substring(26, 30).PadRight(30, " "c)
                            Strmas.AddressLine2.Value = strTestString.Substring(56, 30).PadRight(30, " "c)
                            Strmas.AddressLine3.Value = strTestString.Substring(86, 30).PadRight(30, " "c)
                            Strmas.AddressLine4.Value = strTestString.Substring(116, 30).PadRight(30, " "c)
                            Strmas.StoreNameOnReceipt.Value = strTestString.Substring(146, 12).PadRight(12, " "c)
                            Strmas.Telephone.Value = strTestString.Substring(158, 12).PadRight(12, " "c)
                            Strmas.Fax.Value = strTestString.Substring(170, 12).PadRight(12, " "c)
                            Strmas.ManagerName.Value = strTestString.Substring(200, 20).PadRight(20, " "c)
                            Strmas.RegionCode.Value = strTestString.Substring(220, 2).PadLeft(2, "0"c)
                            If (strTestString.Length > 223) Then Strmas.CountryCode.Value = strTestString.Substring(223, 2)
                            Strmas.IsDeleted.Value = (strTestString.Substring(222, 1) = "Y")
                            If ColOs.Count < 1 Then
                                Strmas.SaveIfNew()
                            Else
                                Strmas.SaveIfExists()
                            End If

                        Catch ex As Exception
                            Trace.WriteLine("Error occurred in DataIntegrityHostD" & ex.Message)
                        End Try
                    End If ' Still Ok to update
                End If ' SM record has passed validations
            End If
        End If 'Processing type "SM" record

    End Sub ' Validate HOSTD data - also perform the updates

    Public Sub DataIntegrityHostf(ByVal strTestString As String, ByRef LastPlan As String, ByRef LastPlangramID As Dictionary(Of String, Integer)) ' Validate HOSTF data also perform the updates

        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim NextSequenceNo As Integer = 0
        Dim intX As Integer = 0
        Dim Plangram As New BOStockLocation.cPlanGram(_Oasys3DB)
        Dim ColPg As New List(Of BOStockLocation.cPlanGram)
        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(124, " "c)
        Dim intLengthTestString As Integer = strTestString.Length

        Dim dateCheckDate As Date = Date.MinValue

        Dim lngTestNumber As Long = 9999999999
        Dim strNextFixtureSequenceNumber As String = String.Empty
        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        ' ------- Check gor TYPE "EL"
        boolPassedValidation = True
        If (strTestString.StartsWith("HR") = False) And (strTestString.StartsWith("TR") = False) And (strTestString.StartsWith("EL") = False) Then 'Processing type "EL" record
            OutputPlangramUpdateError(strTestString, "WRONG RECORD TYPE")
            boolPassedValidation = False
            Exit Sub
        End If
        If strTestString.StartsWith("EL") Then 'Processing type "EL" record
            lngTestNumber = CLng(strTestString.Substring(22, 3).PadRight(3, "0"c))
            If lngTestNumber < 1 Then
                OutputPlangramUpdateError(strTestString, "INVALID STORE NO.")
                boolPassedValidation = False
            End If
            lngTestNumber = CLng(strTestString.Substring(25, 10).PadRight(10, "0"c))
            If lngTestNumber < 1 Then
                OutputPlangramUpdateError(strTestString, "INVALID PLAN NO.")
                boolPassedValidation = False
            End If
            lngTestNumber = CLng(strTestString.Substring(75, 10).PadRight(10, "0"c))
            If lngTestNumber < 1 Then
                OutputPlangramUpdateError(strTestString, "INVALID FIXTURE NO.")
                boolPassedValidation = False
            End If
            lngTestNumber = CLng(strTestString.Substring(85, 6).PadRight(6, "0"c))
            If lngTestNumber < 1 Then
                OutputPlangramUpdateError(strTestString, "INVALID SKUN.")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then
                ' Find the next fixture sequence number
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.Store, strTestString.Substring(22, 3).PadLeft(3, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.Number, strTestString.Substring(25, 10).PadLeft(10, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.SegmentNumber, strTestString.Substring(65, 10).PadLeft(10, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.FixtureNumber, strTestString.Substring(75, 10).PadLeft(10, "0"))
                'Plangram.SortBy(Plangram.FixtureSequence.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                'ColPg = Plangram.LoadMatches(1)
                'lngTestNumber = 0
                'For Each plan As BOStockLocation.cPlanGram In ColPg
                '    If CLng(plan.FixtureSequence.Value) > lngTestNumber Then
                '        lngTestNumber = CLng(plan.FixtureSequence.Value)
                '        Exit For
                '    End If
                'Next
                'lngTestNumber += 1
                'strNextFixtureSequenceNumber = lngTestNumber.ToString.PadLeft(10, "0")
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.Store, strTestString.Substring(22, 3).PadLeft(3, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.Number, strTestString.Substring(25, 10).PadLeft(10, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.SegmentNumber, strTestString.Substring(65, 10).PadLeft(10, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.FixtureNumber, strTestString.Substring(75, 10).PadLeft(10, "0"))
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.FixtureSequence, strNextFixtureSequenceNumber)
                'Plangram.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                'Plangram.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Plangram.SKUNumber, strTestString.Substring(85, 6).PadLeft(6, "0"))
                'ColPg = Plangram.LoadMatches(1)
                Try
                    If (LastPlan <> strTestString.Substring(25, 10).PadLeft(10, "0"c)) Then LastPlangramID = New Dictionary(Of String, Integer)
                    '    If ColPg.Count < 1 Then
                    If LastPlangramID.ContainsKey(strTestString.Substring(22, 13) & strTestString.Substring(65, 20)) Then
                        NextSequenceNo = LastPlangramID.Item(strTestString.Substring(22, 13) & strTestString.Substring(65, 20)) + 1
                        LastPlangramID.Remove(strTestString.Substring(22, 13) & strTestString.Substring(65, 20))
                    Else
                        NextSequenceNo = 1
                    End If
                    LastPlangramID.Add(strTestString.Substring(22, 13) & strTestString.Substring(65, 20), NextSequenceNo)
                    LastPlan = strTestString.Substring(25, 10).PadLeft(10, "0"c)

                    Plangram.Store.Value = strTestString.Substring(22, 3).PadLeft(3, "0"c)
                    Plangram.Number.Value = strTestString.Substring(25, 10).PadLeft(10, "0"c)
                    Plangram.SegmentNumber.Value = strTestString.Substring(65, 10).PadLeft(10, "0"c)
                    Plangram.FixtureNumber.Value = strTestString.Substring(75, 10).PadLeft(10, "0"c)
                    Plangram.FixtureSequence.Value = NextSequenceNo.ToString.PadLeft(10, "0"c)
                    Plangram.SKUNumber.Value = strTestString.Substring(85, 6).PadLeft(6, "0"c)
                    'End If
                    Plangram.Name.Value = strTestString.Substring(35, 30).PadRight(30, " "c)
                    Plangram.Facings.Value = strTestString.Substring(91, 10).PadLeft(10, "0"c)
                    lngTestNumber = 0
                    If strTestString.Substring(104, 10) <> "          " Then lngTestNumber = CLng(strTestString.Substring(104, 10))
                    Plangram.Capacity.Value = CInt(lngTestNumber)
                    lngTestNumber = 0
                    If strTestString.Substring(114, 10) <> "          " Then lngTestNumber = CLng(strTestString.Substring(114, 10))
                    Plangram.Pack.Value = CInt(lngTestNumber)
                    Plangram.IsDeleted.Value = False
                    'If ColPg.Count < 1 Then
                    Plangram.SaveIfNew()
                    'Else
                    '   Plangram.SaveIfExists()
                    'End If
                Catch ex As Exception
                End Try
            End If ' EL record has passed validations
        End If 'Processing type "SM" record
    End Sub ' Validate HOSTF data - also perform the updates

    Public Sub DataIntegrityHosth(ByVal strTestString As String) ' Validate HOSTh data
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0
        Dim Hiecat As New BOHierarchy.cHierachyCategory(_Oasys3DB)
        Dim ColHa As New List(Of BOHierarchy.cHierachyCategory)
        Dim Hiegrp As New BOHierarchy.cHierachyGroup(_Oasys3DB)
        Dim ColHg As New List(Of BOHierarchy.cHierachyGroup)
        Dim Hiesgp As New BOHierarchy.cHierachySubgroup(_Oasys3DB)
        Dim ColHs As New List(Of BOHierarchy.cHierachySubgroup)
        Dim Hiesty As New BOHierarchy.cHierachyStyle(_Oasys3DB)
        Dim ColHt As New List(Of BOHierarchy.cHierachyStyle)
        Dim Hiemas As New BOHierarchy.cHierarchyMaster(_Oasys3DB)
        Dim ColHm As New List(Of BOHierarchy.cHierarchyMaster)
        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(114, " "c)
        Dim intLengthTestString As Integer = strTestString.Length
        boolPassedValidation = False
        strRecordType = "D"
        intRecordOccurrence = 0

        Trace.WriteLine("DataIntegrityHostH:" & strTestString & "*")
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 15

        strTestString.PadRight(100, " "c) 'ensure record is at least 100 characters

        If strTestString.StartsWith("HM") Then boolPassedValidation = ValidateHMData(strTestString.PadRight(24, " "c))
        If boolPassedValidation = True Then
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.Level, strTestString.Substring(23, 1))
            Hiemas.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Hiemas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiemas.MemberNumber, strTestString.Substring(24, 6).PadLeft(6, "0"c))
            ColHm = Hiemas.LoadMatches()
            If ColHm.Count < 1 Then
                Try
                    Hiemas.Level.Value = strTestString.Substring(23, 1)
                    Hiemas.MemberNumber.Value = strTestString.Substring(24, 6)
                    Hiemas.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                    Hiemas.IsDeleted.Value = False
                    If strTestString.Substring(80, 1) = "Y" Then
                        Hiemas.IsDeleted.Value = True
                    End If
                    Hiemas.MaxOverride.Value = 0
                    Hiemas.ReductionWeek1.Value = 0
                    Hiemas.ReductionWeek2.Value = 0
                    Hiemas.ReductionWeek3.Value = 0
                    Hiemas.ReductionWeek4.Value = 0
                    Hiemas.ReductionWeek5.Value = 0
                    Hiemas.ReductionWeek6.Value = 0
                    Hiemas.ReductionWeek7.Value = 0
                    Hiemas.ReductionWeek8.Value = 0
                    Hiemas.ReductionWeek9.Value = 0
                    Hiemas.ReductionWeek10.Value = 0
                    If strTestString.Substring(81, 3) > "   " And strTestString.Substring(81, 3) < "101" Then Hiemas.MaxOverride.Value = CInt(strTestString.Substring(81, 3))
                    If strTestString.Substring(84, 3) > "   " And strTestString.Substring(84, 3) < "101" Then Hiemas.ReductionWeek1.Value = CInt(strTestString.Substring(84, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek1.Value = Hiemas.ReductionWeek1.Value / 100
                    If strTestString.Substring(87, 3) > "   " And strTestString.Substring(87, 3) < "101" Then Hiemas.ReductionWeek2.Value = CInt(strTestString.Substring(87, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek2.Value = Hiemas.ReductionWeek2.Value / 100
                    If strTestString.Substring(90, 3) > "   " And strTestString.Substring(90, 3) < "101" Then Hiemas.ReductionWeek3.Value = CInt(strTestString.Substring(90, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek3.Value = Hiemas.ReductionWeek3.Value / 100
                    If strTestString.Substring(93, 3) > "   " And strTestString.Substring(93, 3) < "101" Then Hiemas.ReductionWeek4.Value = CInt(strTestString.Substring(93, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek4.Value = Hiemas.ReductionWeek4.Value / 100
                    If strTestString.Substring(96, 3) > "   " And strTestString.Substring(96, 3) < "101" Then Hiemas.ReductionWeek5.Value = CInt(strTestString.Substring(96, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek5.Value = Hiemas.ReductionWeek5.Value / 100
                    If strTestString.Substring(99, 3) > "   " And strTestString.Substring(99, 3) < "101" Then Hiemas.ReductionWeek6.Value = CInt(strTestString.Substring(99, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek6.Value = Hiemas.ReductionWeek6.Value / 100
                    If strTestString.Substring(102, 3) > "   " And strTestString.Substring(102, 3) < "101" Then Hiemas.ReductionWeek7.Value = CInt(strTestString.Substring(102, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek7.Value = Hiemas.ReductionWeek7.Value / 100
                    If strTestString.Substring(105, 3) > "   " And strTestString.Substring(105, 3) < "101" Then Hiemas.ReductionWeek8.Value = CInt(strTestString.Substring(105, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek8.Value = Hiemas.ReductionWeek8.Value / 100
                    If strTestString.Substring(108, 3) > "   " And strTestString.Substring(108, 3) < "101" Then Hiemas.ReductionWeek9.Value = CInt(strTestString.Substring(108, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek9.Value = Hiemas.ReductionWeek9.Value / 100
                    If strTestString.Substring(111, 3) > "   " And strTestString.Substring(111, 3) < "101" Then Hiemas.ReductionWeek10.Value = CInt(strTestString.Substring(111, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek10.Value = Hiemas.ReductionWeek10.Value / 100
                    Hiemas.SaveIfNew()
                Catch ex As Exception
                    Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                End Try
            End If
            If ColHm.Count > 0 Then
                Try
                    Hiemas.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                    Hiemas.IsDeleted.Value = False
                    If strTestString.Substring(80, 1) = "Y" Then
                        Hiemas.IsDeleted.Value = True
                    End If
                    Hiemas.MaxOverride.Value = 0
                    Hiemas.ReductionWeek1.Value = 0
                    Hiemas.ReductionWeek2.Value = 0
                    Hiemas.ReductionWeek3.Value = 0
                    Hiemas.ReductionWeek4.Value = 0
                    Hiemas.ReductionWeek5.Value = 0
                    Hiemas.ReductionWeek6.Value = 0
                    Hiemas.ReductionWeek7.Value = 0
                    Hiemas.ReductionWeek8.Value = 0
                    Hiemas.ReductionWeek9.Value = 0
                    Hiemas.ReductionWeek10.Value = 0
                    If strTestString.Substring(81, 3) > "   " And strTestString.Substring(81, 3) < "101" Then Hiemas.MaxOverride.Value = CInt(strTestString.Substring(81, 3))
                    If strTestString.Substring(84, 3) > "   " And strTestString.Substring(84, 3) < "101" Then Hiemas.ReductionWeek1.Value = CInt(strTestString.Substring(84, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek1.Value = Hiemas.ReductionWeek1.Value / 100
                    If strTestString.Substring(87, 3) > "   " And strTestString.Substring(87, 3) < "101" Then Hiemas.ReductionWeek2.Value = CInt(strTestString.Substring(87, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek2.Value = Hiemas.ReductionWeek2.Value / 100
                    If strTestString.Substring(90, 3) > "   " And strTestString.Substring(90, 3) < "101" Then Hiemas.ReductionWeek3.Value = CInt(strTestString.Substring(90, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek3.Value = Hiemas.ReductionWeek3.Value / 100
                    If strTestString.Substring(93, 3) > "   " And strTestString.Substring(93, 3) < "101" Then Hiemas.ReductionWeek4.Value = CInt(strTestString.Substring(93, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek4.Value = Hiemas.ReductionWeek4.Value / 100
                    If strTestString.Substring(96, 3) > "   " And strTestString.Substring(96, 3) < "101" Then Hiemas.ReductionWeek5.Value = CInt(strTestString.Substring(96, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek5.Value = Hiemas.ReductionWeek5.Value / 100
                    If strTestString.Substring(99, 3) > "   " And strTestString.Substring(99, 3) < "101" Then Hiemas.ReductionWeek6.Value = CInt(strTestString.Substring(99, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek6.Value = Hiemas.ReductionWeek6.Value / 100
                    If strTestString.Substring(102, 3) > "   " And strTestString.Substring(102, 3) < "101" Then Hiemas.ReductionWeek7.Value = CInt(strTestString.Substring(102, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek7.Value = Hiemas.ReductionWeek7.Value / 100
                    If strTestString.Substring(105, 3) > "   " And strTestString.Substring(105, 3) < "101" Then Hiemas.ReductionWeek8.Value = CInt(strTestString.Substring(105, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek8.Value = Hiemas.ReductionWeek8.Value / 100
                    If strTestString.Substring(108, 3) > "   " And strTestString.Substring(108, 3) < "101" Then Hiemas.ReductionWeek9.Value = CInt(strTestString.Substring(108, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek9.Value = Hiemas.ReductionWeek9.Value / 100
                    If strTestString.Substring(111, 3) > "   " And strTestString.Substring(111, 3) < "101" Then Hiemas.ReductionWeek10.Value = CInt(strTestString.Substring(111, 3))
                    If Hiemas.ReductionWeek1.Value > 0 Then Hiemas.ReductionWeek10.Value = Hiemas.ReductionWeek10.Value / 100

                    Hiemas.SaveIfExists()
                Catch ex As Exception
                    Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                End Try
            End If
            If strTestString.Substring(23, 1) = "5" Then
                Hiecat.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiecat.Category, strTestString.Substring(24, 6).PadLeft(6, "0"c))
                ColHa = Hiecat.LoadMatches()
                'If ColHa.Count < 1 Then

                'End If
                If ColHa.Count > 0 Then
                    Try
                        Hiecat.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                        Hiecat.Alpha.Value = strTestString.Substring(30, 50).ToUpper.PadRight(50, " "c)
                        Hiecat.SaveIfExists()
                    Catch ex As Exception
                        Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                    End Try
                End If
            End If
            If strTestString.Substring(23, 1) = "4" Then
                Hiegrp.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiegrp.Group, strTestString.Substring(24, 6).PadLeft(6, "0"c))
                ColHg = Hiegrp.LoadMatches()

                If ColHg.Count > 0 Then
                    For Each group As BOHierarchy.cHierachyGroup In ColHg
                        Try
                            Hiegrp.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                            Hiegrp.Alpha.Value = strTestString.Substring(30, 50).ToUpper.PadRight(50, " "c)
                            Hiegrp.SaveIfExists()
                        Catch ex As Exception
                            Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                        End Try
                    Next
                End If
            End If
            If strTestString.Substring(23, 1) = "3" Then
                Hiesgp.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiesgp.SubGroup, strTestString.Substring(24, 6).PadLeft(6, "0"c))
                ColHs = Hiesgp.LoadMatches()

                If ColHs.Count > 0 Then
                    For Each subgroup As BOHierarchy.cHierachySubgroup In ColHs
                        Try
                            Hiesgp.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                            Hiesgp.Alpha.Value = strTestString.Substring(30, 50).ToUpper.PadRight(50, " "c)
                            Hiesgp.SaveIfExists()
                        Catch ex As Exception
                            Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                        End Try
                    Next
                End If
            End If
            If strTestString.Substring(23, 1) = "2" Then
                Hiesty.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Hiesty.Style, strTestString.Substring(24, 6).PadLeft(6, "0"c))
                ColHt = Hiesty.LoadMatches()

                If ColHt.Count > 0 Then
                    For Each style As BOHierarchy.cHierachyStyle In ColHt
                        Try
                            Hiesty.Description.Value = strTestString.Substring(30, 50).PadRight(50, " "c)
                            Hiesty.Alpha.Value = strTestString.Substring(30, 50).ToUpper.PadRight(50, " "c)
                            Hiesty.SaveIfExists()
                        Catch ex As Exception
                            Trace.WriteLine("Error occurred in DataIntegrityHostH:" & ex.Message & "::" & ex.StackTrace)
                        End Try
                    Next
                End If
            End If
        End If
    End Sub ' Validate HOSTH data

    Public Sub DataIntegrityHpstv(ByVal strTestString As String, Optional ByVal blnPendingHPSTV As Boolean = False) ' Validate HPSTV data also perform the updates
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0
        Dim Supmas As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim ColVm As New List(Of BOPurchases.cSupplierDetail)
        Dim Supdets As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColVds As New List(Of BOPurchases.cSupplierDetail)
        Dim Supnot As New BOPurchases.cSupplierNote(_Oasys3DB)
        Dim ColVn As New List(Of BOPurchases.cSupplierNote)

        Dim strDataError As String = String.Empty
        Dim RecCode As String = strTestString.Substring(0, 2)

        strTestString = strTestString.PadRight(114, " "c)
        Dim intLengthTestString As Integer = strTestString.Length

        Dim dateCheckDate As Date = Date.MinValue
        Dim intSQQFreq As Integer = 0

        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        If (RecCode <> "HR") And (RecCode <> "TR") And (RecCode <> "VM") And (RecCode <> "VC") And (RecCode <> "VN") And _
                (RecCode <> "VD") And (RecCode <> "VR") And (RecCode <> "VQ") And (RecCode <> "VT") And (RecCode <> "VH") Then
            OutputSupplierUpdateError(strTestString, "Invalid Record Typ")
            boolPassedValidation = False
        End If

        ' ------- Check gor TYPE "VM"
        boolPassedValidation = False
        If strTestString.StartsWith("VM") Then boolPassedValidation = ValidateVMData(strTestString.PadRight(340, " "c))
        If boolPassedValidation = True Then ' VM record has passed validation
            strTestString = strTestString.PadRight(340, " "c)
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 And strTestString.Substring(22, 1) = "Y" Then
                OutputSupplierUpdateError(strTestString, "New Supp. Deleted")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still Ok to update
                If Supmas.Suppliers.Count < 1 Then
                    Try
                        Supmas.Number.Value = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                        Supmas.DeletedByHO.Value = False
                        Supmas.Name.Value = strTestString.Substring(28, 30).PadRight(30, " "c)
                        Supmas.Type.Value = strTestString.Substring(58, 2).PadRight(2, " "c)
                        Supmas.HelpLineNumber.Value = strTestString.Substring(250, 16).PadRight(16, " "c)
                        Supmas.Alpha.Value = strTestString.Substring(266, 15).ToUpper.PadRight(15, " "c)
                        Supmas.PrimaryMerchant.Value = strTestString.Substring(281, 30).PadRight(30, " "c)
                        Supmas.ReturnsDepotNumber.Value = strTestString.Substring(374, 3).PadLeft(3, "0"c)
                        Supmas.OrderDepotNumber.Value = strTestString.Substring(377, 3).PadLeft(3, "0"c)
                        Supmas.SaveIfNew()
                    Catch ex As Exception
                    End Try
                End If
                If Supmas.Suppliers.Count > 0 Then
                    Try
                        Supmas.DeletedByHO.Value = False
                        If strTestString.Substring(22, 1) = "Y" Then Supmas.DeletedByHO.Value = True
                        Supmas.Name.Value = strTestString.Substring(28, 30).PadRight(30, " "c)
                        Supmas.Type.Value = strTestString.Substring(58, 2).PadRight(2, " "c)
                        Supmas.HelpLineNumber.Value = strTestString.Substring(250, 16).PadRight(16, " "c)
                        Supmas.Alpha.Value = strTestString.Substring(266, 15).ToUpper.PadRight(15, " "c)
                        Supmas.PrimaryMerchant.Value = strTestString.Substring(281, 30).PadRight(30, " "c)
                        Supmas.ReturnsDepotNumber.Value = strTestString.Substring(374, 3).PadLeft(3, "0"c)
                        Supmas.OrderDepotNumber.Value = strTestString.Substring(377, 3).PadLeft(3, "0"c)
                        Supmas.SaveIfExists()
                    Catch ex As Exception
                    End Try
                End If
                UpdateSupplierDetails(strTestString, "S", "999")
                If Supmas.OrderDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "O", Supmas.OrderDepotNumber.Value)
                If Supmas.ReturnsDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "R", Supmas.ReturnsDepotNumber.Value)
            End If ' Still Ok to update
        End If ' VM record has passed validation
        ' ------- Check if TYPE "VC"
        boolPassedValidation = False
        If strTestString.StartsWith("VC") Then boolPassedValidation = ValidateVCData(strTestString.PadRight(62, " "c))
        strTestString = strTestString.PadRight(62, " "c)
        If boolPassedValidation = True Then ' VC record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then
                UpdateSupplierDetails(strTestString, "S", "999")
                If Supmas.OrderDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "O", Supmas.OrderDepotNumber.Value)
                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.OrderDepotNumber.Value)
                ColVds = Supdets.LoadMatches()
                If ColVds.Count > 0 Then
                    For Each detail As BOPurchases.cSupplierDetail In ColVds
                        If detail.DepotNumber.Value = "000" Then
                            OutputSupplierUpdateError(strTestString, "Zero Ordering Depot")
                            Exit For
                        End If
                        UpdateSupplierDetails(strTestString, "O", Supdets.DepotNumber.Value)
                    Next
                End If
                If Supmas.ReturnsDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "R", Supmas.ReturnsDepotNumber.Value)
            End If
        End If ' VCrecord has passed validation
        ' ------- Check gor TYPE "VN"
        boolPassedValidation = False
        If strTestString.StartsWith("VN") Then boolPassedValidation = ValidateVNData(strTestString.PadRight(109, " "c))
        strTestString = strTestString.PadRight(109, " "c)
        If boolPassedValidation = True Then ' VN record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still OK to process
                If CInt(strTestString.Substring(28, 3).PadRight(3, "0"c)) = 0 Or CInt(strTestString.Substring(31, 3).PadLeft(3, "0"c)) = 0 Then
                    OutputSupplierUpdateError(strTestString, "Supp Note Key Zero")
                    boolPassedValidation = False
                End If
                If boolPassedValidation = True Then ' Its still OK
                    If strTestString.Substring(22, 1) <> "Y" Then ' Flagged for deletion?
                        Dim tests As String = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                        Dim testt As String = strTestString.Substring(28, 3).PadLeft(3, "0"c)
                        If strDeleteSupplierNumber <> strTestString.Substring(23, 5).PadLeft(5, "0"c) Or strDeleteSupplierType <> strTestString.Substring(28, 3).PadLeft(3, "0"c) Then
                            ' Have they been deleted?
                            strDeleteSupplierNumber = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                            strDeleteSupplierType = strTestString.Substring(28, 3).PadLeft(3, "0"c)
                            Supnot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supnot.SupplierID, strDeleteSupplierNumber)
                            Supnot.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supnot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supnot.Type, strDeleteSupplierType)
                            ColVn = Supnot.LoadMatches()
                            If ColVn.Count > 0 Then ' Delete Them 
                                For Each note As BOPurchases.cSupplierNote In ColVn
                                    Supnot.Delete()
                                Next
                            End If ' Delete Them 
                        End If ' Have they been deleted?
                    End If ' Flagged for deletion?
                    Supnot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supnot.SupplierID, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                    Supnot.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Supnot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supnot.Type, strTestString.Substring(28, 3).PadLeft(3, "0"c))
                    Supnot.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Supnot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supnot.SeqNo, strTestString.Substring(31, 3).PadLeft(3, "0"c))
                    ColVn = Supnot.LoadMatches()
                    If ColVn.Count < 1 And strTestString.Substring(22, 1) = "Y" Then
                        OutputSupplierUpdateError(strTestString, "New Note Deleted")
                        boolPassedValidation = False
                    End If
                    If boolPassedValidation = True Then ' This is definitely OK
                        If ColVn.Count < 1 Then
                            Try
                                Supnot.SupplierID.Value = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                                Supnot.Type.Value = strTestString.Substring(28, 3).PadLeft(3, "0"c)
                                Supnot.SeqNo.Value = strTestString.Substring(31, 3).PadLeft(3, "0"c)
                                Supnot.Text.Value = strTestString.Substring(34, 75).PadRight(75, " "c)
                                Supnot.SaveIfNew()
                            Catch ex As Exception
                            End Try
                        End If
                        If ColVn.Count > 0 And strTestString.Substring(22, 1) <> "Y" Then
                            Try
                                Supnot.Text.Value = strTestString.Substring(34, 75).PadRight(75, " "c)
                                Supnot.SaveIfExists()
                            Catch ex As Exception
                            End Try
                        End If
                        If ColVn.Count > 0 And strTestString.Substring(22, 1) = "Y" Then
                            Supnot.Delete()
                        End If
                    End If ' This is definitely OK
                End If ' Its still OK
            End If ' Still OK to process
        End If ' VN record has passed validation
        ' ------- Check gor TYPE "VD"
        boolPassedValidation = False
        If strTestString.StartsWith("VD") Then boolPassedValidation = ValidateVDData(strTestString.PadRight(345, " "c))
        strTestString = strTestString.PadRight(345, " "c)
        If boolPassedValidation = True Then ' VD record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still OK to process
                If CInt(strTestString.Substring(28, 3).PadRight(3, "0"c)) = 0 Then ' Check for ZERO depot
                    OutputSupplierUpdateError(strTestString, "Order Depot Zeros")
                    boolPassedValidation = False
                End If ' Check for ZERO depot
                If boolPassedValidation = True Then ' Its still OK
                    UpdateSupplierDetails(strTestString, "O", strTestString.Substring(28, 3).PadLeft(3, "0"c))
                    UpdateSupplierDetails(strTestString, "S", "999")
                End If ' Its still OK
            End If ' Still OK to process
        End If ' VD record has passed validation
        ' ------- Check gor TYPE "VR"
        boolPassedValidation = False
        If strTestString.StartsWith("VR") Then boolPassedValidation = ValidateVRData(strTestString.PadRight(345, " "c))
        strTestString = strTestString.PadRight(345, " "c)
        If boolPassedValidation = True Then ' VR record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still OK to process
                If CInt(strTestString.Substring(28, 3).PadRight(3, "0"c)) = 0 Then ' Check for ZERO depot
                    OutputSupplierUpdateError(strTestString, "Order Depot Zeros")
                    boolPassedValidation = False
                End If ' Check for ZERO depot
                If boolPassedValidation = True Then ' Its still OK
                    UpdateSupplierDetails(strTestString, "R", strTestString.Substring(28, 3).PadLeft(3, "0"c))
                End If ' Its still OK
            End If ' Still OK to process
        End If ' VR record has passed validation
        ' ------- Check gor TYPE "VQ"
        boolPassedValidation = False
        If strTestString.StartsWith("VQ") Then boolPassedValidation = ValidateVQData(strTestString.PadRight(79, " "c))
        strTestString = strTestString.PadRight(79, " "c)
        If boolPassedValidation = True Then ' VQ record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still OK to process
                dateCheckDate = CDate(strTestString.Substring(28, 8))
                If dateCheckDate > dateHashDate Then
                    AddToPendingHpstv(strTestString)
                    boolPassedValidation = False
                End If
                If boolPassedValidation = True Then 'Still going
                    If dateLastHpstvVqRecordUpdated <> Date.MinValue Then ' Have already done an update
                        If dateCheckDate < dateLastHpstvVqRecordUpdated And strTestString.Substring(23, 5).PadLeft(5, "0"c) = strLastHpstvVqSupplierUpdated Then
                            ' lower date and same supplier
                            boolPassedValidation = False
                        End If ' lower date and same supplier
                        If boolPassedValidation = True Then 'Check if RESET needed
                            If dateCheckDate = dateLastHpstvVqRecordUpdated And strTestString.Substring(23, 5).PadLeft(5, "0"c) = strLastHpstvVqSupplierUpdated And strTestString.Substring(22, 1) = "Y" Then
                                boolPassedValidation = False
                                ' Reset fields from SAVED data
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "999")
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    Try
                                        Supdets.OrderMinType.Value = strSavedHeadOfficeMcpType
                                        Supdets.OrderMinValue.Value = CInt(decSavedHeadOfficeMcpValue)
                                        Supdets.OrderMinUnits.Value = CInt(decSavedHeadOfficeMcpUnits)
                                        Supdets.OrderMinWeight.Value = CInt(decSavedHeadOfficeMcpWeight)
                                        Supdets.TruckCapWeight.Value = CInt(decSavedHeadOfficeTruckWeight)
                                        Supdets.TruckCapVolume.Value = CInt(decSAvedHeadOfficeTruckVolume)
                                        Supdets.TruckCapPallets.Value = CInt(decSavedHeadOfficeTruckPallets)
                                        Supdets.SaveIfExists()
                                    Catch ex As Exception
                                    End Try
                                End If
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                                'Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                'Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, strSAVEDs.OrderDepotNumber.Value)
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    For Each depot As BOPurchases.cSupplierDetail In ColVds
                                        Try
                                            Supdets.OrderMinType.Value = strSavedOrderDepotMcpType
                                            Supdets.OrderMinValue.Value = CInt(decSavedOrderDepotMcpValue)
                                            Supdets.OrderMinUnits.Value = CInt(decSavedOrderDepotMcpUnits)
                                            Supdets.OrderMinWeight.Value = CInt(decSavedOrderDepotMcpWeight)
                                            Supdets.TruckCapWeight.Value = CInt(decSavedOrderDepotTruckWeight)
                                            Supdets.TruckCapVolume.Value = CInt(decSAvedOrderDepotTruckVolume)
                                            Supdets.TruckCapPallets.Value = CInt(decSavedOrderDepotTruckPallets)
                                            Supdets.SaveIfExists()
                                        Catch ex As Exception
                                        End Try
                                    Next
                                End If
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "R")
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.ReturnsDepotNumber.Value)
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    Try
                                        Supdets.OrderMinType.Value = strSavedReturnsDepotMcpType
                                        Supdets.OrderMinValue.Value = CInt(decSavedReturnsDepotMcpValue)
                                        Supdets.OrderMinUnits.Value = CInt(decSavedReturnsDepotMcpUnits)
                                        Supdets.OrderMinWeight.Value = CInt(decSavedReturnsDepotMcpWeight)
                                        Supdets.TruckCapWeight.Value = CInt(decSavedReturnsDepotTruckWeight)
                                        Supdets.TruckCapVolume.Value = CInt(decSAvedReturnsDepotTruckVolume)
                                        Supdets.TruckCapPallets.Value = CInt(decSavedReturnsDepotTruckPallets)
                                        Supdets.SaveIfExists()
                                    Catch ex As Exception
                                    End Try
                                End If
                                boolPassedValidation = False
                            End If
                        End If 'Check if RESET needed
                    End If ' Have already done an update
                    If boolPassedValidation = True Then ' Got a record to update
                        If dateCheckDate <> dateLastHpstvVqRecordUpdated Or strTestString.Substring(23, 5).PadLeft(5, "0"c) <> strLastHpstvVqSupplierUpdated Then
                            ' Need to save the data
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "999")
                            ColVds = Supdets.LoadMatches()
                            If ColVds.Count > 0 Then
                                strSavedHeadOfficeMcpType = Supdets.OrderMinType.Value
                                decSavedHeadOfficeMcpValue = Supdets.OrderMinValue.Value
                                decSavedHeadOfficeMcpUnits = Supdets.OrderMinUnits.Value
                                decSavedHeadOfficeMcpWeight = Supdets.OrderMinWeight.Value
                                decSavedHeadOfficeTruckWeight = Supdets.TruckCapWeight.Value
                                decSAvedHeadOfficeTruckVolume = Supdets.TruckCapVolume.Value
                                decSavedHeadOfficeTruckPallets = Supdets.TruckCapPallets.Value
                            End If
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.OrderDepotNumber.Value)
                            ColVds = Supdets.LoadMatches()
                            If ColVds.Count > 0 Then
                                strSavedOrderDepotDepotNumber = Supdets.DepotNumber.Value
                                strSavedOrderDepotMcpType = Supdets.OrderMinType.Value
                                decSavedOrderDepotMcpValue = Supdets.OrderMinValue.Value
                                decSavedOrderDepotMcpUnits = Supdets.OrderMinUnits.Value
                                decSavedOrderDepotMcpWeight = Supdets.OrderMinWeight.Value
                                decSavedOrderDepotTruckWeight = Supdets.TruckCapWeight.Value
                                decSAvedOrderDepotTruckVolume = Supdets.TruckCapVolume.Value
                                decSavedOrderDepotTruckPallets = Supdets.TruckCapPallets.Value
                            End If
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "R")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.ReturnsDepotNumber.Value)
                            ColVds = Supdets.LoadMatches()
                            If ColVds.Count > 0 Then
                                strSavedReturnsDepotMcpType = Supdets.OrderMinType.Value
                                decSavedReturnsDepotMcpValue = Supdets.OrderMinValue.Value
                                decSavedReturnsDepotMcpUnits = Supdets.OrderMinUnits.Value
                                decSavedReturnsDepotMcpWeight = Supdets.OrderMinWeight.Value
                                decSavedReturnsDepotTruckWeight = Supdets.TruckCapWeight.Value
                                decSAvedReturnsDepotTruckVolume = Supdets.TruckCapVolume.Value
                                decSavedReturnsDepotTruckPallets = Supdets.TruckCapPallets.Value
                            End If
                            dateLastHpstvVqRecordUpdated = dateCheckDate
                            strLastHpstvVqSupplierUpdated = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                        End If ' Data saved
                        UpdateSupplierDetails(strTestString, "S", "999")
                        If Supmas.OrderDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "O", Supmas.OrderDepotNumber.Value)
                        If Supmas.ReturnsDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "R", Supmas.ReturnsDepotNumber.Value)
                    End If
                End If 'Still going
            End If ' Still OK to process
        End If ' VQ record has passed validation
        ' ------- Check gor TYPE "VT"
        boolPassedValidation = False
        If strTestString.StartsWith("VT") Then boolPassedValidation = ValidateVTData(strTestString.PadRight(74, " "c))
        strTestString = strTestString.PadRight(74, " "c)
        If boolPassedValidation = True Then ' VT record has passed validation
            Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(23, 5).PadLeft(5, "0"c))
            Supmas.Suppliers = Supmas.LoadMatches()
            If Supmas.Suppliers.Count < 1 Then
                OutputSupplierUpdateError(strTestString, "Supplier NOT Found")
                boolPassedValidation = False
            End If
            If boolPassedValidation = True Then ' Still OK to process
                dateCheckDate = CDate(strTestString.Substring(28, 8))
                If dateCheckDate > dateHashDate Then
                    AddToPendingHpstv(strTestString)
                    boolPassedValidation = False
                End If
                If boolPassedValidation = True Then 'Still going
                    If dateLastHpstvVtRecordUpdated <> Date.MinValue Then ' Have already done an update
                        If dateCheckDate < dateLastHpstvVtRecordUpdated And strTestString.Substring(23, 5).PadLeft(5, "0"c) = strLastHpstvVtSupplierUpdated Then
                            ' lower date and same supplier
                            boolPassedValidation = False
                        End If ' lower date and same supplier
                        If boolPassedValidation = True Then 'Check if RESET needed
                            If dateCheckDate = dateLastHpstvVtRecordUpdated And strTestString.Substring(23, 5).PadLeft(5, "0"c) = strLastHpstvVtSupplierUpdated And strTestString.Substring(22, 1) = "Y" Then
                                boolPassedValidation = False
                                ' Reset fields from SAVED data
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "999")
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    Try
                                        Supdets.Tradanet.Value = boolSavedHeadOfficeTradanet
                                        Supdets.BBC.Value = strSavedHeadOfficeBBC
                                        Supdets.LeadTimeFixed.Value = CInt(decSavedHeadOfficeLeadTimeFixed)
                                        Supdets.SOQFrequency.Value = CInt(decSavedHeadOfficeSoqFrequency)
                                        intSQQFreq = Supdets.SOQFrequency.Value
                                        If intSQQFreq >= 64 Then
                                            intSQQFreq -= 64
                                            Supdets.ReviewDay0.Value = True
                                        Else
                                            Supdets.ReviewDay0.Value = False
                                        End If

                                        If intSQQFreq >= 32 Then
                                            intSQQFreq -= 32
                                            Supdets.ReviewDay6.Value = True
                                        Else
                                            Supdets.ReviewDay6.Value = False
                                        End If

                                        If intSQQFreq >= 16 Then
                                            intSQQFreq -= 16
                                            Supdets.ReviewDay5.Value = True
                                        Else
                                            Supdets.ReviewDay5.Value = False
                                        End If

                                        If intSQQFreq >= 8 Then
                                            intSQQFreq -= 8
                                            Supdets.ReviewDay4.Value = True
                                        Else
                                            Supdets.ReviewDay4.Value = False
                                        End If

                                        If intSQQFreq >= 4 Then
                                            intSQQFreq -= 4
                                            Supdets.ReviewDay3.Value = True
                                        Else
                                            Supdets.ReviewDay3.Value = False
                                        End If

                                        If intSQQFreq >= 2 Then
                                            intSQQFreq -= 2
                                            Supdets.ReviewDay2.Value = True
                                        Else
                                            Supdets.ReviewDay2.Value = False
                                        End If

                                        If intSQQFreq >= 1 Then
                                            intSQQFreq -= 1
                                            Supdets.ReviewDay1.Value = True
                                        Else
                                            Supdets.ReviewDay1.Value = False
                                        End If

                                        Supdets.DeliveryCheckMethod.Value = _SavedHODeliveryCheck
                                        Supdets.LeadTime0.Value = CInt(decSavedHeadOfficeLeadTime0)
                                        Supdets.LeadTime1.Value = CInt(decSavedHeadOfficeLeadTime1)
                                        Supdets.LeadTime2.Value = CInt(decSavedHeadOfficeLeadTime2)
                                        Supdets.LeadTime3.Value = CInt(decSavedHeadOfficeLeadTime3)
                                        Supdets.LeadTime4.Value = CInt(decSavedHeadOfficeLeadTime4)
                                        Supdets.LeadTime5.Value = CInt(decSavedHeadOfficeLeadTime5)
                                        Supdets.LeadTime6.Value = CInt(decSavedHeadOfficeLeadTime6)
                                        Supdets.SaveIfExists()
                                    Catch ex As Exception
                                    End Try
                                End If
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                                'Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                'Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, strSAVEDs.OrderDepotNumber.Value)
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    For Each depot As BOPurchases.cSupplierDetail In ColVds
                                        Try
                                            Supdets.Tradanet.Value = boolSavedOrderDepotTradanet
                                            Supdets.BBC.Value = strSavedOrderDepotBBC
                                            Supdets.LeadTimeFixed.Value = CInt(decSavedOrderDepotLeadTimeFixed)
                                            Supdets.SOQFrequency.Value = CInt(decSavedOrderDepotSoqFrequency)
                                            intSQQFreq = Supdets.SOQFrequency.Value
                                            If intSQQFreq >= 64 Then
                                                intSQQFreq -= 64
                                                Supdets.ReviewDay0.Value = True
                                            Else
                                                Supdets.ReviewDay0.Value = False
                                            End If

                                            If intSQQFreq >= 32 Then
                                                intSQQFreq -= 32
                                                Supdets.ReviewDay6.Value = True
                                            Else
                                                Supdets.ReviewDay6.Value = False
                                            End If

                                            If intSQQFreq >= 16 Then
                                                intSQQFreq -= 16
                                                Supdets.ReviewDay5.Value = True
                                            Else
                                                Supdets.ReviewDay5.Value = False
                                            End If

                                            If intSQQFreq >= 8 Then
                                                intSQQFreq -= 8
                                                Supdets.ReviewDay4.Value = True
                                            Else
                                                Supdets.ReviewDay4.Value = False
                                            End If

                                            If intSQQFreq >= 4 Then
                                                intSQQFreq -= 4
                                                Supdets.ReviewDay3.Value = True
                                            Else
                                                Supdets.ReviewDay3.Value = False
                                            End If

                                            If intSQQFreq >= 2 Then
                                                intSQQFreq -= 2
                                                Supdets.ReviewDay2.Value = True
                                            Else
                                                Supdets.ReviewDay2.Value = False
                                            End If

                                            If intSQQFreq >= 1 Then
                                                intSQQFreq -= 1
                                                Supdets.ReviewDay1.Value = True
                                            Else
                                                Supdets.ReviewDay1.Value = False
                                            End If

                                            Supdets.DeliveryCheckMethod.Value = strSavedOrderDepotDeliveryCheck
                                            Supdets.LeadTime0.Value = CInt(decSavedOrderDepotLeadTime0)
                                            Supdets.LeadTime1.Value = CInt(decSavedOrderDepotLeadTime1)
                                            Supdets.LeadTime2.Value = CInt(decSavedOrderDepotLeadTime2)
                                            Supdets.LeadTime3.Value = CInt(decSavedOrderDepotLeadTime3)
                                            Supdets.LeadTime4.Value = CInt(decSavedOrderDepotLeadTime4)
                                            Supdets.LeadTime5.Value = CInt(decSavedOrderDepotLeadTime5)
                                            Supdets.LeadTime6.Value = CInt(decSavedOrderDepotLeadTime6)
                                            Supdets.SaveIfExists()
                                        Catch ex As Exception
                                        End Try
                                    Next
                                End If
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "R")
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.ReturnsDepotNumber.Value)
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count > 0 Then
                                    Try
                                        Supdets.Tradanet.Value = boolSavedReturnsDepotTradanet
                                        Supdets.BBC.Value = strSavedReturnsDepotBBC
                                        Supdets.LeadTimeFixed.Value = CInt(decSavedReturnsDepotLeadTimeFixed)
                                        Supdets.SOQFrequency.Value = CInt(decSavedReturnsDepotSoqFrequency)
                                        intSQQFreq = Supdets.SOQFrequency.Value
                                        If intSQQFreq >= 64 Then
                                            intSQQFreq -= 64
                                            Supdets.ReviewDay0.Value = True
                                        Else
                                            Supdets.ReviewDay0.Value = False
                                        End If

                                        If intSQQFreq >= 32 Then
                                            intSQQFreq -= 32
                                            Supdets.ReviewDay6.Value = True
                                        Else
                                            Supdets.ReviewDay6.Value = False
                                        End If

                                        If intSQQFreq >= 16 Then
                                            intSQQFreq -= 16
                                            Supdets.ReviewDay5.Value = True
                                        Else
                                            Supdets.ReviewDay5.Value = False
                                        End If

                                        If intSQQFreq >= 8 Then
                                            intSQQFreq -= 8
                                            Supdets.ReviewDay4.Value = True
                                        Else
                                            Supdets.ReviewDay4.Value = False
                                        End If

                                        If intSQQFreq >= 4 Then
                                            intSQQFreq -= 4
                                            Supdets.ReviewDay3.Value = True
                                        Else
                                            Supdets.ReviewDay3.Value = False
                                        End If

                                        If intSQQFreq >= 2 Then
                                            intSQQFreq -= 2
                                            Supdets.ReviewDay2.Value = True
                                        Else
                                            Supdets.ReviewDay2.Value = False
                                        End If

                                        If intSQQFreq >= 1 Then
                                            intSQQFreq -= 1
                                            Supdets.ReviewDay1.Value = True
                                        Else
                                            Supdets.ReviewDay1.Value = False
                                        End If

                                        Supdets.DeliveryCheckMethod.Value = strSavedReturnsDepotDeliveryCheck
                                        Supdets.LeadTime0.Value = CInt(decSavedReturnsDepotLeadTime0)
                                        Supdets.LeadTime1.Value = CInt(decSavedReturnsDepotLeadTime1)
                                        Supdets.LeadTime2.Value = CInt(decSavedReturnsDepotLeadTime2)
                                        Supdets.LeadTime3.Value = CInt(decSavedReturnsDepotLeadTime3)
                                        Supdets.LeadTime4.Value = CInt(decSavedReturnsDepotLeadTime4)
                                        Supdets.LeadTime5.Value = CInt(decSavedReturnsDepotLeadTime5)
                                        Supdets.LeadTime6.Value = CInt(decSavedReturnsDepotLeadTime6)
                                        Supdets.SaveIfExists()
                                    Catch ex As Exception
                                    End Try
                                End If
                                boolPassedValidation = False
                            End If
                        End If 'Check if RESET needed
                    End If ' Have already done an update
                    If boolPassedValidation = True Then ' Got a record to update
                        If dateCheckDate <> dateLastHpstvVtRecordUpdated Or strTestString.Substring(23, 5).PadLeft(5, "0"c) <> strLastHpstvVtSupplierUpdated Then
                            ' Need to save the data
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "999")
                            ColVds = Supdets.LoadMatches()
                            boolSavedHeadOfficeTradanet = False
                            If ColVds.Count > 0 Then
                                boolSavedHeadOfficeTradanet = Supdets.Tradanet.Value
                                strSavedHeadOfficeBBC = Supdets.BBC.Value
                                decSavedHeadOfficeLeadTimeFixed = Supdets.LeadTimeFixed.Value
                                decSavedHeadOfficeSoqFrequency = Supdets.SOQFrequency.Value
                                _SavedHODeliveryCheck = Supdets.DeliveryCheckMethod.Value
                                decSavedHeadOfficeLeadTime0 = Supdets.LeadTime0.Value
                                decSavedHeadOfficeLeadTime1 = Supdets.LeadTime1.Value
                                decSavedHeadOfficeLeadTime2 = Supdets.LeadTime2.Value
                                decSavedHeadOfficeLeadTime3 = Supdets.LeadTime3.Value
                                decSavedHeadOfficeLeadTime4 = Supdets.LeadTime4.Value
                                decSavedHeadOfficeLeadTime5 = Supdets.LeadTime5.Value
                                decSavedHeadOfficeLeadTime6 = Supdets.LeadTime6.Value
                                intSQQFreq = Supdets.SOQFrequency.Value
                                If intSQQFreq >= 64 Then
                                    intSQQFreq -= 64
                                    Supdets.ReviewDay0.Value = True
                                Else
                                    Supdets.ReviewDay0.Value = False
                                End If

                                If intSQQFreq >= 32 Then
                                    intSQQFreq -= 32
                                    Supdets.ReviewDay6.Value = True
                                Else
                                    Supdets.ReviewDay6.Value = False
                                End If

                                If intSQQFreq >= 16 Then
                                    intSQQFreq -= 16
                                    Supdets.ReviewDay5.Value = True
                                Else
                                    Supdets.ReviewDay5.Value = False
                                End If

                                If intSQQFreq >= 8 Then
                                    intSQQFreq -= 8
                                    Supdets.ReviewDay4.Value = True
                                Else
                                    Supdets.ReviewDay4.Value = False
                                End If

                                If intSQQFreq >= 4 Then
                                    intSQQFreq -= 4
                                    Supdets.ReviewDay3.Value = True
                                Else
                                    Supdets.ReviewDay3.Value = False
                                End If

                                If intSQQFreq >= 2 Then
                                    intSQQFreq -= 2
                                    Supdets.ReviewDay2.Value = True
                                Else
                                    Supdets.ReviewDay2.Value = False
                                End If

                                If intSQQFreq >= 1 Then
                                    intSQQFreq -= 1
                                    Supdets.ReviewDay1.Value = True
                                Else
                                    Supdets.ReviewDay1.Value = False
                                End If
                                blnReviewDay0 = Supdets.ReviewDay0.Value
                                blnReviewDay1 = Supdets.ReviewDay1.Value
                                blnReviewDay2 = Supdets.ReviewDay2.Value
                                blnReviewDay3 = Supdets.ReviewDay3.Value
                                blnReviewDay4 = Supdets.ReviewDay4.Value
                                blnReviewDay5 = Supdets.ReviewDay5.Value
                                blnReviewDay6 = Supdets.ReviewDay6.Value
                            End If
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.OrderDepotNumber.Value)
                            ColVds = Supdets.LoadMatches()
                            boolSavedOrderDepotTradanet = False
                            If ColVds.Count > 0 Then
                                boolSavedOrderDepotTradanet = Supdets.Tradanet.Value
                                strSavedOrderDepotBBC = Supdets.BBC.Value
                                decSavedOrderDepotLeadTimeFixed = Supdets.LeadTimeFixed.Value
                                decSavedOrderDepotSoqFrequency = Supdets.SOQFrequency.Value
                                strSavedOrderDepotDeliveryCheck = Supdets.DeliveryCheckMethod.Value
                                decSavedOrderDepotLeadTime0 = Supdets.LeadTime0.Value
                                decSavedOrderDepotLeadTime1 = Supdets.LeadTime1.Value
                                decSavedOrderDepotLeadTime2 = Supdets.LeadTime2.Value
                                decSavedOrderDepotLeadTime3 = Supdets.LeadTime3.Value
                                decSavedOrderDepotLeadTime4 = Supdets.LeadTime4.Value
                                decSavedOrderDepotLeadTime5 = Supdets.LeadTime5.Value
                                decSavedOrderDepotLeadTime6 = Supdets.LeadTime6.Value
                                intSQQFreq = Supdets.SOQFrequency.Value
                                If intSQQFreq >= 64 Then
                                    intSQQFreq -= 64
                                    Supdets.ReviewDay0.Value = True
                                Else
                                    Supdets.ReviewDay0.Value = False
                                End If

                                If intSQQFreq >= 32 Then
                                    intSQQFreq -= 32
                                    Supdets.ReviewDay6.Value = True
                                Else
                                    Supdets.ReviewDay6.Value = False
                                End If

                                If intSQQFreq >= 16 Then
                                    intSQQFreq -= 16
                                    Supdets.ReviewDay5.Value = True
                                Else
                                    Supdets.ReviewDay5.Value = False
                                End If

                                If intSQQFreq >= 8 Then
                                    intSQQFreq -= 8
                                    Supdets.ReviewDay4.Value = True
                                Else
                                    Supdets.ReviewDay4.Value = False
                                End If

                                If intSQQFreq >= 4 Then
                                    intSQQFreq -= 4
                                    Supdets.ReviewDay3.Value = True
                                Else
                                    Supdets.ReviewDay3.Value = False
                                End If

                                If intSQQFreq >= 2 Then
                                    intSQQFreq -= 2
                                    Supdets.ReviewDay2.Value = True
                                Else
                                    Supdets.ReviewDay2.Value = False
                                End If

                                If intSQQFreq >= 1 Then
                                    intSQQFreq -= 1
                                    Supdets.ReviewDay1.Value = True
                                Else
                                    Supdets.ReviewDay1.Value = False
                                End If
                                blnReviewDay0 = Supdets.ReviewDay0.Value
                                blnReviewDay1 = Supdets.ReviewDay1.Value
                                blnReviewDay2 = Supdets.ReviewDay2.Value
                                blnReviewDay3 = Supdets.ReviewDay3.Value
                                blnReviewDay4 = Supdets.ReviewDay4.Value
                                blnReviewDay5 = Supdets.ReviewDay5.Value
                                blnReviewDay6 = Supdets.ReviewDay6.Value
                            End If
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strTestString.Substring(23, 5).PadLeft(5, "0"c))
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "R")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, Supmas.ReturnsDepotNumber.Value)
                            ColVds = Supdets.LoadMatches()
                            boolSavedReturnsDepotTradanet = False
                            If ColVds.Count > 0 Then
                                boolSavedReturnsDepotTradanet = Supdets.Tradanet.Value
                                strSavedReturnsDepotBBC = Supdets.BBC.Value
                                decSavedReturnsDepotLeadTimeFixed = Supdets.LeadTimeFixed.Value
                                decSavedReturnsDepotSoqFrequency = Supdets.SOQFrequency.Value
                                strSavedReturnsDepotDeliveryCheck = Supdets.DeliveryCheckMethod.Value
                                decSavedReturnsDepotLeadTime0 = Supdets.LeadTime0.Value
                                decSavedReturnsDepotLeadTime1 = Supdets.LeadTime1.Value
                                decSavedReturnsDepotLeadTime2 = Supdets.LeadTime2.Value
                                decSavedReturnsDepotLeadTime3 = Supdets.LeadTime3.Value
                                decSavedReturnsDepotLeadTime4 = Supdets.LeadTime4.Value
                                decSavedReturnsDepotLeadTime5 = Supdets.LeadTime5.Value
                                decSavedReturnsDepotLeadTime6 = Supdets.LeadTime6.Value
                                intSQQFreq = Supdets.SOQFrequency.Value
                                If intSQQFreq >= 64 Then
                                    intSQQFreq -= 64
                                    Supdets.ReviewDay0.Value = True
                                Else
                                    Supdets.ReviewDay0.Value = False
                                End If

                                If intSQQFreq >= 32 Then
                                    intSQQFreq -= 32
                                    Supdets.ReviewDay6.Value = True
                                Else
                                    Supdets.ReviewDay6.Value = False
                                End If

                                If intSQQFreq >= 16 Then
                                    intSQQFreq -= 16
                                    Supdets.ReviewDay5.Value = True
                                Else
                                    Supdets.ReviewDay5.Value = False
                                End If

                                If intSQQFreq >= 8 Then
                                    intSQQFreq -= 8
                                    Supdets.ReviewDay4.Value = True
                                Else
                                    Supdets.ReviewDay4.Value = False
                                End If

                                If intSQQFreq >= 4 Then
                                    intSQQFreq -= 4
                                    Supdets.ReviewDay3.Value = True
                                Else
                                    Supdets.ReviewDay3.Value = False
                                End If

                                If intSQQFreq >= 2 Then
                                    intSQQFreq -= 2
                                    Supdets.ReviewDay2.Value = True
                                Else
                                    Supdets.ReviewDay2.Value = False
                                End If

                                If intSQQFreq >= 1 Then
                                    intSQQFreq -= 1
                                    Supdets.ReviewDay1.Value = True
                                Else
                                    Supdets.ReviewDay1.Value = False
                                End If

                                blnReviewDay0 = Supdets.ReviewDay0.Value
                                blnReviewDay1 = Supdets.ReviewDay1.Value
                                blnReviewDay2 = Supdets.ReviewDay2.Value
                                blnReviewDay3 = Supdets.ReviewDay3.Value
                                blnReviewDay4 = Supdets.ReviewDay4.Value
                                blnReviewDay5 = Supdets.ReviewDay5.Value
                                blnReviewDay6 = Supdets.ReviewDay6.Value
                            End If
                            dateLastHpstvVtRecordUpdated = dateCheckDate
                            strLastHpstvVtSupplierUpdated = strTestString.Substring(23, 5).PadLeft(5, "0"c)
                        End If ' Data saved
                        UpdateSupplierDetails(strTestString, "S", "999")
                        If Supmas.OrderDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "O", Supmas.OrderDepotNumber.Value)
                        If Supmas.ReturnsDepotNumber.Value <> "000" Then UpdateSupplierDetails(strTestString, "R", Supmas.ReturnsDepotNumber.Value)
                    End If
                End If 'Still going
            End If ' Still OK to process
        End If ' VT record has passed validation
        ' ------- Check gor TYPE "VH"
        boolPassedValidation = False
        'If strTestString.StartsWith("VH") Then boolPassedValidation = ValidateVHData(strTestString.PadRight(194, " "))
        If boolPassedValidation = True Then ' VT record has passed validation
            strTestString = strTestString.PadRight(194, " "c)
        End If ' VH record has passed validation
    End Sub ' Validate HPSTV data - also perform the updates

    Public Sub DataIntegrityHpsto(ByVal strTestString As String, ByVal OBCHashValue As Decimal, ByRef HPSTOReportData As Data.DataTable) ' Validate HPSTO data also perform the updates
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0
        Dim Purhdr As New BOPurchases.cPurchaseHeader(_Oasys3DB)
        Dim ColPh As New List(Of BOPurchases.cPurchaseHeader)
        Dim Purlin As New BOPurchases.cPurchaseLine(_Oasys3DB)
        Dim ColPl As New List(Of BOPurchases.cPurchaseLine)
        Dim Supmas As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim ColVm As New List(Of BOPurchases.cSupplierMaster)
        Dim Supdets As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColVds As New List(Of BOPurchases.cSupplierDetail)
        Dim Stkmas As New BOStock.cStock(_Oasys3DB)
        Dim ColIm As New List(Of BOStock.cStock)
        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(114, " "c)
        Dim intLengthTestString As Integer = strTestString.Length

        Dim dateCheckDate As Date = Date.MinValue
        Dim intPurchaseOrderNo As Integer = 0
        Dim intHOPurchaseOrderNo As Integer = 0
        Dim boolIsAHeadOfficeOrder As Boolean = False
        Dim boolIsANewOrder As Boolean = False

        Dim strLinePONumber As String = String.Empty
        Dim strLineBBCNumber As String = String.Empty
        Dim strLineSupplierNumber As String = String.Empty
        Dim strLineSkuNumber As String = String.Empty
        Dim decLineOldOrderQuantity As Decimal = 0
        Dim decLineNewOrderQuantity As Decimal = 0
        Dim decLineReasonCode As Decimal = 0
        Dim decNewValue As Decimal = 0
        Dim intNewCartons As Integer = 0
        Dim decOldValue As Decimal = 0
        Dim intOldCartons As Integer = 0

        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        boolPassedValidation = False
        If strTestString.StartsWith("HR") Or strTestString.StartsWith("TR") Then Exit Sub

        If (strTestString.StartsWith("OC") = False) And (strTestString.StartsWith("OA") = False) Then
            OutputOrderConfirmationError(strTestString, "Invalid Rec Type", OBCHashValue)
            Exit Sub
        End If
        ' ------- Check gor TYPE "OC"
        boolOutputErrorToSthoa = True
        If strTestString.StartsWith("OC") Then boolPassedValidation = ValidateOCData(strTestString.PadRight(50, " "c), arrstrOCNumericValidation, OBCHashValue)
        If boolPassedValidation = True Then ' OC record has passed validation (Order Confirmation)
            strTestString = strTestString.PadRight(50, " "c)
            boolIsAHeadOfficeOrder = False
            intPurchaseOrderNo = CInt(strTestString.Substring(22, 6))
            intHOPurchaseOrderNo = CInt(strTestString.Substring(44, 6))
            'Purhdr.s()
            If intPurchaseOrderNo = 0 Then ' PO Number is 0 so check if HO Allocation Order
                If intHOPurchaseOrderNo = 0 Then
                    OutputOrderConfirmationError(strTestString, "Invalid P/O Number", OBCHashValue)
                    Exit Sub
                End If
                If boolPassedValidation = True Then ' The P/O number seems OK
                    Purhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Purhdr.PONumber, strTestString.Substring(44, 6).PadLeft(6, "0"c))
                    Purhdr.Orders = Purhdr.LoadMatches()
                    If Purhdr.Orders.Count > 0 Then ' Order ALREADY on file
                        OutputOrderConfirmationError(strTestString, "Dup. Allocation", OBCHashValue)
                        Exit Sub
                    End If ' Order ALREADY on file
                    If boolPassedValidation = True Then ' Creating H/O allocation order
                        Dim strSupplierFileKey As String = strTestString.Substring(28, 5).PadLeft(5, "0"c)
                        Dim dateWorkDate As Date = CDate(strTestString.Substring(33, 8))
                        Dim decWorkReason As Decimal = CDec(strTestString.Substring(42, 2))
                        Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strSupplierFileKey)
                        Supmas.Suppliers = Supmas.LoadMatches()
                        Dim strSupplierOrderDepotNumber As String = "000"
                        If Supmas.Suppliers.Count > 0 Then
                            strSupplierOrderDepotNumber = Supmas.OrderDepotNumber.Value
                        End If
                        Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strSupplierFileKey)
                        Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                        Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "000")
                        ColVds = Supdets.LoadMatches()
                        If ColVds.Count < 1 Then ' try the order depot
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strSupplierFileKey)
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "O")
                            Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, strSupplierOrderDepotNumber)
                            ColVds = Supdets.LoadMatches()
                            If ColVds.Count < 1 Then ' try the H/O depot
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.SupplierNumber, strSupplierFileKey)
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdets.DepotType, "S")
                                Supdets.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                Supdets.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Supdets.DepotNumber, "999")
                                ColVds = Supdets.LoadMatches()
                                If ColVds.Count < 1 Then
                                    Supdets.BBC.Value = " "
                                End If
                            End If ' try the H/O depot
                        End If ' try the order depot
                        Try
                            Purhdr.PONumber.Value = strTestString.Substring(44, 6).PadLeft(6, "0"c)
                            Purhdr.HONumber.Value = strTestString.Substring(44, 6).PadLeft(6, "0"c)
                            Purhdr.SupplierNumber.Value = strSupplierFileKey
                            If (Purhdr.Supplier Is Nothing) Then
                            End If
                            Purhdr.DateOrderCreated.Value = CDate(strTestString.Substring(2, 8))
                            Purhdr.DateOrderDue.Value = dateWorkDate
                            Purhdr.RaiserInitials.Value = "BBC"
                            Purhdr.SourceEntry.Value = "H"
                            Purhdr.SupplierBBC.Value = Supdets.BBC.Value
                            Purhdr.Confirmation.Value = "C"
                            Purhdr.CommBBCPrepped.Value = True
                            If (Purhdr.Lines(False) Is Nothing) Then
                            End If
                            Purhdr.SaveIfNew()
                        Catch ex As Exception
                        End Try
                        If Supmas.Suppliers.Count > 0 Then
                            Try
                                Supmas.PONumOutstanding.Value += 1
                                '                                Supmas.POValueOutstanding.Value = 1
                                Supmas.SaveIfExists()
                            Catch ex As Exception
                            End Try
                        End If
                        PrintOrderConfirmationsOCDetails(strTestString, Supmas.Name.Value, Purhdr.DateOrderCreated.Value, Purhdr.DateOrderDue.Value, boolIsAHeadOfficeOrder, HPSTOReportData)
                        _ExpectingOARecords = True
                        strExpectedPONumber = strTestString.Substring(44, 6).PadLeft(6, "0"c)
                        strExpectedSupplierNumber = strSupplierFileKey
                    End If ' Creating H/O allocation order
                End If ' The P/O number seems OK
            End If ' Could be a Head Office Allocation Order

            If intPurchaseOrderNo <> 0 Then ' Is an order confirmation
                Purhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Purhdr.PONumber, strTestString.Substring(22, 6).PadLeft(6, "0"c))
                Purhdr.Orders = Purhdr.LoadMatches()
                If Purhdr.Orders.Count < 1 Then ' Order not on file
                    OutputOrderConfirmationError(strTestString, "Invalid P/O Number", OBCHashValue)
                    Exit Sub
                End If ' Order not on file
                If boolPassedValidation = True Then ' It is on file
                    If Purhdr.Confirmation.Value > " " Then ' Confirmation Error
                        OutputOrderConfirmationError(strTestString, "P/O not Un-Confrmd", OBCHashValue)
                        Exit Sub
                    End If ' Confirmation Error
                    If boolPassedValidation = True Then ' Checking Supplier
                        Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, strTestString.Substring(28, 5).PadLeft(5, "0"c))
                        Supmas.Suppliers = Supmas.LoadMatches()
                        If Supmas.Suppliers.Count < 1 Then ' Supplier Error
                            OutputOrderConfirmationError(strTestString, "Invalid Supplier", OBCHashValue)
                            Exit Sub
                        End If ' Supplier Error
                    End If ' Checking Supplier
                    If boolPassedValidation = True Then ' Validate Confirmation Types
                        If strTestString.Substring(41, 1) <> "C" And strTestString.Substring(41, 1) <> "R" And strTestString.Substring(41, 1) <> "A" Then
                            OutputOrderConfirmationError(strTestString, "Invalid CONF Flag", OBCHashValue)
                            Exit Sub
                        End If
                    End If ' Validate Confirmation Types
                    If boolPassedValidation = True Then ' Its a valid confirmation record
                        PrintOrderConfirmationsOCDetails(strTestString, Supmas.Name.Value, Purhdr.DateOrderCreated.Value, Purhdr.DateOrderDue.Value, boolIsAHeadOfficeOrder, HPSTOReportData)
                        If Purhdr.Lines.Count > 0 Then
                            Try
                                strExpectedPONumber = strTestString.Substring(22, 6).PadLeft(6, "0"c)
                                strExpectedSupplierNumber = strTestString.Substring(28, 5).PadLeft(5, "0"c)
                                For Each poline As BOPurchases.cPurchaseLine In Purhdr.Lines
                                    poline.Confirmation.Value = strTestString.Substring(41, 1)
                                    If strTestString.Substring(41, 1) = "A" Then
                                        poline.Confirmation.Value = "C"
                                        _ExpectingOARecords = True
                                    End If
                                    poline.ReasonCode.Value = CInt(strTestString.Substring(42, 2))
                                    If strTestString.Substring(41, 1) = "R" Then
                                        Stkmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkmas.SkuNumber, poline.SkuNumber.Value)
                                        ColIm = Stkmas.LoadMatches()
                                        If ColIm.Count > 0 Then
                                            Try
                                                Stkmas.StockOnOrder.Value = Stkmas.StockOnOrder.Value - poline.OrderQty.Value
                                                If Stkmas.StockOnOrder.Value < 0 Then Stkmas.StockOnOrder.Value = 0
                                                Stkmas.ActivityToday.Value = True
                                                Stkmas.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                        End If
                                    End If
                                    poline.SaveIfExists()
                                Next
                                Purhdr.Confirmation.Value = strTestString.Substring(41, 1)
                                Purhdr.ReasonCode.Value = CInt(strTestString.Substring(42, 2))
                                If strTestString.Substring(41, 1) = "R" Then Purhdr.Deleted.Value = True
                                Purhdr.SaveIfExists()
                            Catch ex As Exception
                                Trace.WriteLine("Error updating Existing Purchase Order:" & intPurchaseOrderNo & "," & ex.Message)
                            End Try
                            If strTestString.Substring(41, 1) = "R" Then
                                Try
                                    Supmas.PONumOutstanding.Value = Supmas.PONumOutstanding.Value - 1
                                    If Supmas.PONumOutstanding.Value < 0 Then
                                        Supmas.PONumOutstanding.Value = 0
                                    End If
                                    Supmas.POValueOutstanding.Value = Supmas.POValueOutstanding.Value - Purhdr.OrderValue.Value
                                    If Supmas.POValueOutstanding.Value < 0 Then
                                        Supmas.POValueOutstanding.Value = 0
                                    End If
                                    Supmas.SaveIfExists()
                                Catch ex As Exception
                                    Trace.WriteLine("Error updating Supplier," & ex.Message)
                                End Try
                            End If
                        End If
                    End If ' Its a valid confirmation record
                End If ' It is on file
            End If ' Is an order confirmation
        End If ' OC record has passed validation  
        ' ------- Check got TYPE "OA"
        boolPassedValidation = True
        boolOutputErrorToSthoa = True
        If strTestString.StartsWith("OA") And _ExpectingOARecords = False Then
            OutputOrderConfirmationError(strTestString, "Unexpected OA Rec.", OBCHashValue)
            Exit Sub
        End If
        If (strTestString.StartsWith("OA") = False) And (_ExpectingOARecords = True) Then boolPassedValidation = False
        If (boolPassedValidation = True) And (strTestString.StartsWith("OA") = True) Then ' RECORD TYPE IS VALID
            If strTestString.StartsWith("OA") Then PrintOrderConfirmationsOADetails(strTestString, HPSTOReportData)
            If strTestString.StartsWith("OA") And _ExpectingOARecords = True Then boolPassedValidation = ValidateOAData(strTestString.PadRight(65, " "c), arrstrOANumericValidation, OBCHashValue)
            If boolPassedValidation = True Then ' OA record has passed validation
                strTestString = strTestString.PadRight(65, " "c)
                strLinePONumber = strTestString.Substring(22, 6).PadLeft(6, "0"c)
                If (strLinePONumber = "000000") Then boolIsAHeadOfficeOrder = True
                strLineBBCNumber = strTestString.Substring(28, 6).PadLeft(6, "0"c)
                strLineSupplierNumber = strTestString.Substring(34, 5).PadLeft(5, "0"c)
                strLineSkuNumber = strTestString.Substring(39, 6).PadLeft(6, "0"c)
                decLineOldOrderQuantity = CDec(strTestString.Substring(49, 6).PadLeft(6, "0"c)) * CDec(IIf(strTestString.Substring(55, 1) = " ", 1, -1))
                decLineNewOrderQuantity = CDec(strTestString.Substring(56, 6).PadLeft(6, "0"c)) * CDec(IIf(strTestString.Substring(62, 1) = " ", 1, -1))
                decLineReasonCode = CDec(strTestString.Substring(63, 2))
                If boolIsAHeadOfficeOrder = True And strLineBBCNumber <> strExpectedPONumber Then OutputOrderConfirmationError(strTestString, "Not Same P/O Numb.", OBCHashValue)
                If boolPassedValidation = True Then ' Validate OA P/O Number
                    If boolIsAHeadOfficeOrder = False And strLinePONumber <> strExpectedPONumber Then OutputOrderConfirmationError(strTestString, "Not Same P/O Numb.", OBCHashValue)
                    If boolPassedValidation = True Then ' Validate OA supplier number
                        If strLineSupplierNumber <> strExpectedSupplierNumber Then OutputOrderConfirmationError(strTestString, "Not Same Supp Numb", OBCHashValue)
                        If boolPassedValidation = True Then ' Validate OA sku number
                            Stkmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkmas.SkuNumber, strLineSkuNumber)
                            ColIm = Stkmas.LoadMatches()
                            If ColIm.Count < 1 Then OutputOrderConfirmationError(strTestString, "Invalid SKU Number", OBCHashValue)
                            If boolPassedValidation = True Then ' Validate OA new quantity
                                If decLineNewOrderQuantity < 0 Then OutputOrderConfirmationError(strTestString, "Invalid New Qty.", OBCHashValue)
                                If boolPassedValidation = True Then ' Still valid
                                    If (boolIsAHeadOfficeOrder = False) Then
                                        Purhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Purhdr.PONumber, strExpectedPONumber)
                                    Else
                                        Purhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Purhdr.HONumber, strExpectedPONumber)
                                    End If
                                    Purhdr.Orders = Purhdr.LoadMatches()
                                    If Purhdr.Orders.Count > 0 Then 'Order Exists (should)
                                        Dim PrevPOLine As BOPurchases.cPurchaseLine = Nothing
                                        Dim boolAddANewLine As Boolean = True
                                        For Each poline As BOPurchases.cPurchaseLine In Purhdr.Lines
                                            If poline.SkuNumber.Value = strLineSkuNumber Then
                                                Purlin = poline
                                                boolAddANewLine = False
                                                Exit For
                                            End If
                                        Next
                                        If boolAddANewLine = True Then
                                            Try
                                                ''Get ID of last existing Line to update PREV column on new line
                                                'For Each poline As BOPurchases.cPurchaseLine In Purhdr.Lines
                                                '    If poline.LinkNextLine.Value = 0 Then
                                                '        PrevPOLine = poline
                                                '        Purlin.LinkLastLine.Value = poline.PurchaseLineID.Value
                                                '        Exit For
                                                '    End If
                                                'Next

                                                Purlin.HeaderIdentity.Value = Purhdr.PurchaseHeaderID.Value
                                                Purlin.SkuNumber.Value = strLineSkuNumber
                                                Purlin.SKUProductCode.Value = Stkmas.SupplierPartCode.Value
                                                Purlin.OrderQty.Value = CInt(decLineNewOrderQuantity)
                                                Purlin.OrderPrice.Value = Stkmas.NormalSellPrice.Value
                                                Purlin.OrderCost.Value = Stkmas.CostPrice.Value
                                                Purlin.DateLastOrder.Value = Stkmas.LastOrdered.Value
                                                Purlin.Confirmation.Value = "A"
                                                Purlin.ReasonCode.Value = CInt(decLineReasonCode)
                                                Purlin.SaveIfNew()
                                                'If previous line then add Links to newly added line
                                                'If (PrevPOLine Is Nothing = False) Then
                                                '    PrevPOLine.LinkNextLine.Value = Purlin.PurchaseLineID.Value
                                                '    PrevPOLine.SaveIfExists()
                                                'End If
                                            Catch ex As Exception
                                            End Try
                                            decNewValue = Purlin.OrderQty.Value * Purlin.OrderPrice.Value
                                            intNewCartons = CInt(decLineNewOrderQuantity / Stkmas.SupplierPackSize.Value)
                                            Try
                                                Stkmas.StockOnOrder.Value = CInt(Stkmas.StockOnOrder.Value + decLineNewOrderQuantity)
                                                Stkmas.ActivityToday.Value = True
                                                Stkmas.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                            Try
                                                Purhdr.OrderValue.Value = Purhdr.OrderValue.Value + decNewValue
                                                Purhdr.OrderQty.Value = CInt(Purhdr.OrderQty.Value + decLineNewOrderQuantity)
                                                Purhdr.OrderQtyCartons.Value = Purhdr.OrderQtyCartons.Value + intNewCartons
                                                Purhdr.SaveIfExists()
                                                Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, Purhdr.SupplierNumber.Value)
                                                Supmas.Suppliers = Supmas.LoadMatches()
                                                Supmas.POValueOutstanding.Value = Supmas.POValueOutstanding.Value + decNewValue
                                                Supmas.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                        Else
                                            decLineOldOrderQuantity = Purlin.OrderQty.Value
                                            decNewValue = decLineNewOrderQuantity * Purlin.OrderPrice.Value
                                            intNewCartons = CInt(decLineNewOrderQuantity / Stkmas.SupplierPackSize.Value)
                                            decOldValue = decLineOldOrderQuantity * Purlin.OrderPrice.Value
                                            intOldCartons = CInt(decLineOldOrderQuantity / Stkmas.SupplierPackSize.Value)
                                            Try
                                                Purlin.OrderQty.Value = CInt(decLineNewOrderQuantity)
                                                Purlin.Confirmation.Value = "A"
                                                Purlin.ReasonCode.Value = CInt(decLineReasonCode)
                                                Purlin.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                            Try
                                                Stkmas.StockOnOrder.Value = CInt(Stkmas.StockOnOrder.Value - decLineOldOrderQuantity + decLineNewOrderQuantity)
                                                Stkmas.ActivityToday.Value = True
                                                Stkmas.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                            Try
                                                Purhdr.OrderValue.Value = Purhdr.OrderValue.Value - decOldValue + decNewValue
                                                Purhdr.OrderQty.Value = CInt(Purhdr.OrderQty.Value - decLineOldOrderQuantity + decLineNewOrderQuantity)
                                                Purhdr.OrderQtyCartons.Value = Purhdr.OrderQtyCartons.Value - intOldCartons + intNewCartons
                                                Purhdr.SaveIfExists()
                                                Supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supmas.Number, Purhdr.SupplierNumber.Value)
                                                Supmas.Suppliers = Supmas.LoadMatches()
                                                Supmas.POValueOutstanding.Value = Supmas.POValueOutstanding.Value - decOldValue + decNewValue
                                                Supmas.SaveIfExists()
                                            Catch ex As Exception
                                            End Try
                                        End If
                                    End If 'Order Exists (should)
                                End If ' Still valid
                            End If ' Validate OA new quantity
                        End If ' Validate OA sku number
                    End If ' Validate OA supplier number
                End If ' Validate OA P/O Number
            End If ' OA record has passed validation
        End If ' RECORD TYPE IS VALID
    End Sub ' Validate HPSTO data - also perform the updates

    Public Sub DataIntegrityHpsti(ByVal strTestString As String, ByRef ReportTable As Data.DataTable, ByVal OBCHashValue As Decimal) ' Validate HPSTI data also perform the updates
        Dim StrMyString As String = String.Empty
        Dim strValidValue As String = String.Empty
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim intX As Integer = 0
        Dim Isuhdr As New BOPurchases.cIssueHeader(_Oasys3DB)
        Dim ColIh As New List(Of BOPurchases.cIssueHeader)
        Dim Isulin As New BOPurchases.cIssueLine(_Oasys3DB)
        Dim ColIl As New List(Of BOPurchases.cIssueLine)
        Dim Consum As New BOPurchases.cContainerSummary(_Oasys3DB)
        Dim ColCs As New List(Of BOPurchases.cContainerSummary)
        Dim Condet As New BOPurchases.cContainerDetail(_Oasys3DB)
        Dim ColCc As New List(Of BOPurchases.cContainerDetail)
        Dim Stkmas As New BOStock.cStock(_Oasys3DB)
        Dim ColIm As New List(Of BOStock.cStock)
        Dim strDataError As String = String.Empty
        strTestString = strTestString.PadRight(114, " "c)
        Dim intLengthTestString As Integer = strTestString.Length
        Dim intIssuePONumber As Integer = 0
        Dim dateCheckDate As Date = Date.MinValue

        Dim strLinePONumber As String = String.Empty
        Dim strLineBBCNumber As String = String.Empty
        Dim strLineSupplierNumber As String = String.Empty
        Dim strLineSkuNumber As String = String.Empty
        Dim decLineOldOrderQuantity As Decimal = 0
        Dim decLineNewOrderQuantity As Decimal = 0
        Dim decLineReasonCode As Decimal = 0
        Dim decNewValue As Decimal = 0
        Dim intNewCartons As Integer = 0
        Dim decOldValue As Decimal = 0
        Dim intOldCartons As Integer = 0
        Dim strRejectionIndicator As String = String.Empty

        Dim blnItemRejected As Boolean

        strRecordType = "D"
        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(arrstrRecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        boolPassedValidation = False
        If strTestString.StartsWith("HR") Or strTestString.StartsWith("TR") Or strTestString.StartsWith("II") Or strTestString.StartsWith("ID") Or strTestString.StartsWith("CS") Or strTestString.StartsWith("CD") Then
            boolPassedValidation = True
        End If
        'Purhdr.
        If boolPassedValidation = False Then OutputOrderConfirmationError(strTestString, "Invalid Rec Type", OBCHashValue)
        ' ------- Check got TYPE "II"
        boolPassedValidation = False
        boolOutputErrorToSthoa = False
        If strTestString.StartsWith("II") Then boolPassedValidation = ValidateIIData(strTestString.PadRight(71, " "c))
        If boolPassedValidation = True Then ' II record has passed validation
            strTestString = strTestString.PadRight(71, " "c)
            Isuhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Isuhdr.IssueNumber, strTestString.Substring(22, 6).PadLeft(6, "0"c))
            ColIh = Isuhdr.LoadMatches()
            If ColIh.Count < 1 Then
                Try
                    Isuhdr.IssueNumber.Value = strTestString.Substring(22, 6).PadLeft(6, "0"c)
                    Isuhdr.IssueDate.Value = CDate(strTestString.Substring(28, 8))
                    Isuhdr.SupplierNumber.Value = strTestString.Substring(36, 5).PadLeft(5, "0"c)
                    intIssuePONumber = CInt(Val(strTestString.Substring(41, 6)))
                    Isuhdr.StorePoNumber.Value = strTestString.Substring(41, 6).PadLeft(6, "0"c)
                    If intIssuePONumber < 1 Then Isuhdr.StorePoNumber.Value = strTestString.Substring(47, 6).PadLeft(6, "0"c)
                    intIssuePONumber = CInt(Val(strTestString.Substring(53, 6)))
                    Isuhdr.StoreSoqNumber.Value = "000000"
                    If intIssuePONumber > 0 Then Isuhdr.StoreSoqNumber.Value = strTestString.Substring(53, 6).PadLeft(6, "0"c)
                    Isuhdr.SupplierBbcCode.Value = strTestString.Substring(59, 1)
                    Isuhdr.IssueValue.Value = CDec(Val(strTestString.Substring(60, 10)))
                    Isuhdr.ImportedIssue.Value = False
                    If strTestString.Substring(70, 1) = "Y" Then Isuhdr.ImportedIssue.Value = True
                    Isuhdr.SaveIfNew()
                Catch ex As Exception
                End Try
            End If
        End If ' II record has passed validation  
        ' ------- Check gor TYPE "ID"
        boolPassedValidation = False
        boolOutputErrorToSthoa = False
        If strTestString.StartsWith("ID") Then boolPassedValidation = ValidateIDData(strTestString.PadRight(61, " "c))
        If boolPassedValidation = True Then ' ID record has passed validation
            Isuhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Isuhdr.IssueNumber, strTestString.Substring(22, 6).PadLeft(6, "0"c))
            ColIh = Isuhdr.LoadMatches()
            If ColIh.Count < 1 Then
                OutputBBCIssuesError(strTestString, ("Invalid Issue Note"))
            Else
                strTestString = strTestString.PadRight(61, " "c)
                Isulin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Isulin.IssueNumber, strTestString.Substring(22, 6).PadLeft(6, "0"c))
                Isulin.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Isulin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Isulin.LineNumber, strTestString.Substring(34, 4).PadLeft(4, "0"c))
                ColIl = Isulin.LoadMatches()
                If ColIl.Count < 1 Then
                    Try
                        Isulin.IssueNumber.Value = strTestString.Substring(22, 6).PadLeft(6, "0"c)
                        Isulin.SkuNumber.Value = strTestString.Substring(28, 6).PadLeft(6, "0"c)
                        Isulin.LineNumber.Value = strTestString.Substring(34, 4).PadLeft(4, "0"c)
                        Isulin.QtyOrdered.Value = CDec(strTestString.Substring(38, 7))
                        Isulin.QtyIssued.Value = CDec(strTestString.Substring(45, 7))
                        Isulin.QtyTofollow.Value = CDec(strTestString.Substring(52, 7))
                        Isulin.AddByMaintenance.Value = False
                        If strTestString.Substring(60, 1) = "Y" Then Isulin.AddByMaintenance.Value = True
                        Isulin.SaveIfNew()
                    Catch ex As Exception
                    End Try
                End If
            End If
        End If ' ID record has passed validation  
            ' ------- Check gor TYPE "CS"
            boolPassedValidation = False
            boolOutputErrorToSthoa = False
            If strTestString.StartsWith("CS") Then boolPassedValidation = ValidateCSData(strTestString.PadRight(100, " "c))
            If boolPassedValidation = True Then ' CS record has passed validation
                strTestString = strTestString.PadRight(100, " "c)
                Consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Consum.AssemblyDepotNumber, strTestString.Substring(35, 3).PadLeft(3, "0"c))
                Consum.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Consum.Number, strTestString.Substring(39, 9).PadLeft(9, "0"c))
                ColCs = Consum.LoadMatches()
                If ColCs.Count > 0 Then OutputBBCIssuesError(strTestString, "Container Exists")
                If ColCs.Count < 1 Then ' Add New Container Summary record
                    Try
                        Consum.AssemblyDepotNumber.Value = strTestString.Substring(35, 3).PadLeft(3, "0"c)
                        Consum.Number.Value = strTestString.Substring(39, 9).PadLeft(9, "0"c)
                        Consum.DespatchDate.Value = CDate(strTestString.Substring(2, 8))
                        Consum.DespatchDepotNumber.Value = strTestString.Substring(22, 3).Trim.PadLeft(3, "0"c)
                        Consum.VehicleLoadReference.Value = strTestString.Substring(26, 9).PadRight(9, " "c)
                        Consum.Description.Value = strTestString.Substring(48, 36).PadRight(36, " "c)
                        Consum.TotalLines.Value = CDec(strTestString.Substring(84, 7))
                    Consum.ParentNumber.Value = strTestString.Substring(91, 9).Trim.PadLeft(9, "0"c)
                    Consum.DeliveryDate.Value = CDate(_ReportDate)
                        Consum.SaveIfNew()
                        Dim ConSummary As Data.DataRow = ReportTable.NewRow
                        ConSummary.Item("ContainerNo") = Consum.AssemblyDepotNumber.Value & "/" & Consum.Number.Value
                        ConSummary.Item("ContainerType") = Consum.Description.Value
                        ConSummary.Item("NoLines") = Consum.TotalLines.Value
                        ConSummary.Item("ParentContainer") = IIf(CLng(Consum.ParentNumber.Value) = 0, "", Consum.ParentNumber.Value)
                        ConSummary.Item("Received") = "N"
                        ConSummary.Item("Value") = Consum.Value.Value
                        ConSummary.Item("DeliveryDate") = Consum.DeliveryDate.Value.ToString("dd/MM/yy")
                        ReportTable.Rows.Add(ConSummary)
                    Catch ex As Exception
                    End Try
                End If ' Add New Container Summary record
                decNewValue = Consum.Value.Value
            End If ' CS record has passed validation  
            ' ------- Check For TYPE "CD
            boolPassedValidation = False
            boolOutputErrorToSthoa = False
            If strTestString.StartsWith("CD") Then boolPassedValidation = ValidateCDData(strTestString.PadRight(58, " "c))
            If boolPassedValidation = True Then ' CD record has passed validation
                strTestString = strTestString.PadRight(58, " "c)
                strRejectionIndicator = " "
                'Load existing Consignment Header
                Consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Consum.AssemblyDepotNumber, strTestString.Substring(22, 3).PadLeft(3, "0"c))
                Consum.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Consum.Number, strTestString.Substring(26, 9).PadLeft(9, "0"c))
                ColCs = Consum.LoadMatches()
                If ColCs.Count < 1 Then
                    OutputBBCIssuesError(strTestString, "No Summary Record")
                Else ' Have a SUMMARY record
                    decNewValue = Consum.Value.Value
                    Stkmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Stkmas.SkuNumber, strTestString.Substring(35, 6).PadLeft(6, "0"c))
                    Stkmas.AddLoadField(Stkmas.NormalSellPrice)
                    ColIm = Stkmas.LoadMatches()
                    If (ColIm.Count < 1) Then strRejectionIndicator = "I"
                    Condet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Condet.AssemblyDepotNumber, strTestString.Substring(22, 3).PadLeft(3, "0"c))
                    Condet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Condet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Condet.ContainerNumber, strTestString.Substring(26, 9).PadLeft(9, "0"c))
                    Condet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Condet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Condet.StorePoNumber, strTestString.Substring(48, 6).PadLeft(6, "0"c))
                    Condet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Condet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Condet.StorePoLineNumber, strTestString.Substring(54, 4).PadLeft(4, "0"c))
                    ColCc = Condet.LoadMatches()
                    If ColCc.Count > 0 Then
                        OutputBBCIssuesError(strTestString, "Detail Rec. Exists")
                    Else
                        Try ' Add a new CONDET record
                            If (strRejectionIndicator = "I") Then blnItemRejected = True
                            Condet.AssemblyDepotNumber.Value = strTestString.Substring(22, 3).PadLeft(3, "0"c)
                            Condet.ContainerNumber.Value = strTestString.Substring(26, 9).PadLeft(9, "0"c)
                            Condet.StorePoNumber.Value = strTestString.Substring(48, 6).PadLeft(6, "0"c)
                            Condet.StorePoLineNumber.Value = strTestString.Substring(54, 4).PadLeft(4, "0"c)
                            Condet.SkuNumber.Value = strTestString.Substring(35, 6).PadLeft(6, "0"c)
                            Condet.Quantity.Value = CDec(strTestString.Substring(41, 7))
                            Condet.Price.Value = Stkmas.NormalSellPrice.Value
                            decNewValue += Condet.Price.Value * Condet.Quantity.Value
                            Condet.RejectedReason.Value = strRejectionIndicator
                            Condet.SaveIfNew()
                        Catch ex As Exception
                        End Try
                    End If ' Add a new CONDET record
                End If ' Have a summary record
                'Added 16/2/09 - update header with Value and If rejected
                Consum.Value.Value = decNewValue
                If (blnItemRejected = True) Then Consum.RejectedReason.Value = "I"
                Consum.SaveIfExists()
                If (ReportTable.Rows.Count > 0) Then ReportTable.Rows(ReportTable.Rows.Count - 1).Item("Value") = decNewValue

            End If ' CS record has passed validation  
    End Sub ' Validate HPSTI data - also perform the updates

    Public Sub UpdateSupplierDetails(ByVal strteststring As String, ByVal StrDepotType As String, ByVal strDepotNumber As String)
        Dim Supdet As New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim ColvD As New List(Of BOPurchases.cSupplierDetail)
        Dim boolContinue As Boolean = True
        Dim decWorkField As Decimal = 0
        Dim intSQQFreq As Integer = 0


        Supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.SupplierNumber, strteststring.Substring(23, 5).PadLeft(5, "0"c))
        Supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.DepotType, StrDepotType)
        Supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        Supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Supdet.DepotNumber, strDepotNumber)
        ColvD = Supdet.LoadMatches()
        boolContinue = True
        If strteststring.StartsWith("VD") And StrDepotType = "S" Then ' Special "VD" insert
            boolContinue = False
            Try
                Supdet.DepotType.Value = "O"
                Supdet.DepotNumber.Value = strteststring.Substring(28, 3).PadLeft(3, "0"c)
                Supdet.AddressLine1.Value = strteststring.Substring(31, 30).PadRight(30, " "c)
                Supdet.AddressLine2.Value = strteststring.Substring(61, 30).PadRight(30, " "c)
                Supdet.AddressLine3.Value = strteststring.Substring(91, 30).PadRight(30, " "c)
                Supdet.AddressLine4.Value = strteststring.Substring(121, 30).PadRight(30, " "c)
                Supdet.AddressLine5.Value = strteststring.Substring(151, 30).PadRight(30, " "c)
                Supdet.AddressPostcode.Value = strteststring.Substring(181, 8).PadRight(8, " "c)
                Supdet.PhoneNumber1.Value = strteststring.Substring(189, 16).PadRight(16, " "c)
                Supdet.PhoneNumber2.Value = strteststring.Substring(205, 16).PadRight(16, " "c)
                Supdet.FaxNumber1.Value = strteststring.Substring(221, 16).PadRight(16, " "c)
                Supdet.FaxNumber2.Value = strteststring.Substring(237, 16).PadRight(16, " "c)
                Supdet.ContactName1.Value = strteststring.Substring(253, 30).PadRight(30, " "c)
                Supdet.DepotNotes.Value = strteststring.Substring(383, 60).PadRight(60, " "c)
                Supdet.ReturnsPolicyCode.Value = "000"
                Supdet.SaveIfNew()
            Catch ex As Exception
            End Try
        End If ' Special "VD" insert
        If boolContinue = True Then ' At this point - not creating a dummy VD record for "o"
            If ColvD.Count < 1 Then
                boolContinue = True
                If strteststring.Substring(22, 1) = "Y" And (strteststring.StartsWith("VD") Or strteststring.StartsWith("VR")) Then
                    OutputSupplierUpdateError(strteststring, "New Depot Deleted")
                    boolContinue = False
                End If
                If boolContinue = True Then ' Carry On with insert
                    Try
                        Supdet.SupplierNumber.Value = strteststring.Substring(23, 5).PadLeft(5, "0"c)
                        Supdet.DepotType.Value = StrDepotType
                        Supdet.DepotNumber.Value = strDepotNumber
                        Supdet.Deleted.Value = False
                        If strteststring.StartsWith("VM") Then ' "VM" insert
                            Supdet.Deleted.Value = False
                            If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                            Supdet.AddressLine1.Value = strteststring.Substring(60, 30).PadRight(30, " "c)
                            Supdet.AddressLine2.Value = strteststring.Substring(90, 30).PadRight(30, " "c)
                            Supdet.AddressLine3.Value = strteststring.Substring(120, 30).PadRight(30, " "c)
                            Supdet.AddressLine4.Value = strteststring.Substring(150, 30).PadRight(30, " "c)
                            Supdet.AddressLine5.Value = strteststring.Substring(180, 30).PadRight(30, " "c)
                            Supdet.AddressPostcode.Value = strteststring.Substring(210, 8).PadRight(8, " "c)
                            Supdet.PhoneNumber1.Value = strteststring.Substring(218, 16).PadRight(16, " "c)
                            Supdet.FaxNumber1.Value = strteststring.Substring(234, 16).PadRight(16, " "c)
                            Supdet.ContactName1.Value = strteststring.Substring(311, 30).PadRight(30, " "c)
                            If StrDepotType = "S" Then Supdet.ContactName2.Value = strteststring.Substring(341, 30).PadRight(30, " "c)
                            Supdet.ReturnsPolicyCode.Value = strteststring.Substring(371, 3).PadLeft(3, "0"c)
                        End If ' "VM" insert
                        If strteststring.StartsWith("VC") Then ' "VC" insert
                            Trace.WriteLine("Setting VC(I):" & strteststring.Substring(28, 32))
                            Supdet.DateOrderCloseStart.Value = CDate(strteststring.Substring(28, 8))
                            Supdet.DateOrderCloseEnd.Value = CDate(strteststring.Substring(36, 8))
                            If boolBlankStartDate1 = False And boolBlankEndDate1 = False Then
                                Supdet.DateDelCloseStart.Value = CDate(strteststring.Substring(44, 8))
                                Supdet.DateDelCloseEnd.Value = CDate(strteststring.Substring(52, 8))
                            End If
                        End If ' "VC" insert
                        If strteststring.StartsWith("VR") Then ' "VR" insert
                            Supdet.AddressLine1.Value = strteststring.Substring(31, 30).PadRight(30, " "c)
                            Supdet.AddressLine2.Value = strteststring.Substring(61, 30).PadRight(30, " "c)
                            Supdet.AddressLine3.Value = strteststring.Substring(91, 30).PadRight(30, " "c)
                            Supdet.AddressLine4.Value = strteststring.Substring(121, 30).PadRight(30, " "c)
                            Supdet.AddressLine5.Value = strteststring.Substring(151, 30).PadRight(30, " "c)
                            Supdet.AddressPostcode.Value = strteststring.Substring(181, 8).PadRight(8, " "c)
                            Supdet.PhoneNumber1.Value = strteststring.Substring(189, 16).PadRight(16, " "c)
                            Supdet.PhoneNumber2.Value = strteststring.Substring(205, 16).PadRight(16, " "c)
                            Supdet.FaxNumber1.Value = strteststring.Substring(221, 16).PadRight(16, " "c)
                            Supdet.FaxNumber2.Value = strteststring.Substring(237, 16).PadRight(16, " "c)
                            Supdet.ContactName1.Value = strteststring.Substring(253, 30).PadRight(30, " "c)
                            Supdet.DepotNotes.Value = strteststring.Substring(383, 60).PadRight(60, " "c)
                            Supdet.ReturnsMessageRef.Value = CInt(strteststring.Substring(343, 2))
                            Supdet.ReturnsPolicyCode.Value = "0"
                        End If ' "VR" INSERT 
                        If strteststring.StartsWith("VQ") Then ' "VQ" insert
                            Supdet.OrderMinType.Value = strteststring.Substring(36, 1)
                            Supdet.OrderMinValue.Value = CInt(strteststring.Substring(37, 7))
                            Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                            Supdet.OrderMinWeight.Value = CInt(strteststring.Substring(51, 7))
                            Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                            Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                            Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                            Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                            If StrDepotType = "S" Then ' Save Head Office data
                                strSavedHeadOfficeMcpType = Supdet.OrderMinType.Value
                                decSavedHeadOfficeMcpValue = Supdet.OrderMinValue.Value
                                decSavedHeadOfficeMcpUnits = Supdet.OrderMinUnits.Value
                                decSavedHeadOfficeMcpWeight = Supdet.OrderMinWeight.Value
                                decSavedHeadOfficeTruckWeight = Supdet.TruckCapWeight.Value
                                decSAvedHeadOfficeTruckVolume = Supdet.TruckCapVolume.Value
                                decSavedHeadOfficeTruckPallets = Supdet.TruckCapPallets.Value
                            End If ' Save Head Office data
                            If StrDepotType = "O" Then ' Save Ordering Depot data
                                strSavedOrderDepotMcpType = Supdet.OrderMinType.Value
                                decSavedOrderDepotMcpValue = Supdet.OrderMinValue.Value
                                decSavedOrderDepotMcpUnits = Supdet.OrderMinUnits.Value
                                decSavedOrderDepotMcpWeight = Supdet.OrderMinWeight.Value
                                decSavedOrderDepotTruckWeight = Supdet.TruckCapWeight.Value
                                decSAvedOrderDepotTruckVolume = Supdet.TruckCapVolume.Value
                                decSavedOrderDepotTruckPallets = Supdet.TruckCapPallets.Value
                            End If ' Save Ordering Depot data
                            If StrDepotType = "R" Then ' Save Returns Depot data
                                strSavedReturnsDepotMcpType = Supdet.OrderMinType.Value
                                decSavedReturnsDepotMcpValue = Supdet.OrderMinValue.Value
                                decSavedReturnsDepotMcpUnits = Supdet.OrderMinUnits.Value
                                decSavedReturnsDepotMcpWeight = Supdet.OrderMinWeight.Value
                                decSavedReturnsDepotTruckWeight = Supdet.TruckCapWeight.Value
                                decSAvedReturnsDepotTruckVolume = Supdet.TruckCapVolume.Value
                                decSavedReturnsDepotTruckPallets = Supdet.TruckCapPallets.Value
                            End If ' Save Returns Depot data
                        End If ' "VQ" INSERT
                        If strteststring.StartsWith("VT") Then ' "VT" insert
                            decWorkField = 0
                            Supdet.Tradanet.Value = False
                            If strteststring.Substring(36, 1) = "Y" Then Supdet.Tradanet.Value = True
                            Supdet.BBC.Value = strteststring.Substring(40, 1)
                            Supdet.SOQFrequency.Value = CInt(strteststring.Substring(41, 4).PadLeft(4, "0"c))
                            intSQQFreq = Supdet.SOQFrequency.Value
                            If intSQQFreq >= 64 Then
                                intSQQFreq -= 64
                                Supdet.ReviewDay0.Value = True
                            Else
                                Supdet.ReviewDay0.Value = False
                            End If

                            If intSQQFreq >= 32 Then
                                intSQQFreq -= 32
                                Supdet.ReviewDay6.Value = True
                            Else
                                Supdet.ReviewDay6.Value = False
                            End If

                            If intSQQFreq >= 16 Then
                                intSQQFreq -= 16
                                Supdet.ReviewDay5.Value = True
                            Else
                                Supdet.ReviewDay5.Value = False
                            End If

                            If intSQQFreq >= 8 Then
                                intSQQFreq -= 8
                                Supdet.ReviewDay4.Value = True
                            Else
                                Supdet.ReviewDay4.Value = False
                            End If

                            If intSQQFreq >= 4 Then
                                intSQQFreq -= 4
                                Supdet.ReviewDay3.Value = True
                            Else
                                Supdet.ReviewDay3.Value = False
                            End If

                            If intSQQFreq >= 2 Then
                                intSQQFreq -= 2
                                Supdet.ReviewDay2.Value = True
                            Else
                                Supdet.ReviewDay2.Value = False
                            End If

                            If intSQQFreq >= 1 Then
                                intSQQFreq -= 1
                                Supdet.ReviewDay1.Value = True
                            Else
                                Supdet.ReviewDay1.Value = False
                            End If

                            Supdet.LeadTime0.Value = CInt(strteststring.Substring(45, 4))
                            Supdet.LeadTime1.Value = CInt(strteststring.Substring(49, 4))
                            Supdet.LeadTime2.Value = CInt(strteststring.Substring(53, 4))
                            Supdet.LeadTime3.Value = CInt(strteststring.Substring(57, 4))
                            Supdet.LeadTime4.Value = CInt(strteststring.Substring(61, 4))
                            Supdet.LeadTime5.Value = CInt(strteststring.Substring(65, 4))
                            Supdet.LeadTime6.Value = CInt(strteststring.Substring(69, 4))
                            Supdet.DeliveryCheckMethod.Value = strteststring.Substring(73, 1)
                            If (Supdet.DeliveryCheckMethod.Value.Trim = "") Then Supdet.DeliveryCheckMethod.Value = "0"
                            Supdet.LeadTimeFixed.Value = Supdet.LeadTime0.Value + Supdet.LeadTime1.Value + Supdet.LeadTime2.Value + Supdet.LeadTime3.Value + Supdet.LeadTime4.Value + Supdet.LeadTime5.Value + Supdet.LeadTime6.Value
                            If Supdet.LeadTimeFixed.Value < 0 Then Supdet.LeadTimeFixed.Value = 0
                            If Supdet.LeadTimeFixed.Value > 0 Then Supdet.LeadTimeFixed.Value = CInt((Supdet.LeadTimeFixed.Value / 7) + 0.5)
                        End If ' "VT" insert
                        Supdet.SaveIfNew()
                    Catch ex As Exception
                    End Try
                End If ' Carry On with insert
            End If
            'If ColvD.Count > 0 And StrDepotType = "S" Then
            If ColvD.Count > 0 Then
                Try
                    Supdet.Deleted.Value = False
                    If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                    If strteststring.StartsWith("VM") Then ' "VM" update
                        Supdet.AddressLine1.Value = strteststring.Substring(60, 30).PadRight(30, " "c)
                        Supdet.AddressLine2.Value = strteststring.Substring(90, 30).PadRight(30, " "c)
                        Supdet.AddressLine3.Value = strteststring.Substring(120, 30).PadRight(30, " "c)
                        Supdet.AddressLine4.Value = strteststring.Substring(150, 30).PadRight(30, " "c)
                        Supdet.AddressLine5.Value = strteststring.Substring(180, 30).PadRight(30, " "c)
                        Supdet.AddressPostcode.Value = strteststring.Substring(210, 8).PadRight(8, " "c)
                        Supdet.PhoneNumber1.Value = strteststring.Substring(218, 16).PadRight(16, " "c)
                        Supdet.FaxNumber1.Value = strteststring.Substring(234, 16).PadRight(16, " "c)
                        Supdet.ContactName1.Value = strteststring.Substring(311, 30).PadRight(30, " "c)
                        If StrDepotType = "S" Then Supdet.ContactName2.Value = strteststring.Substring(341, 30).PadRight(30, " "c)
                        Supdet.ReturnsPolicyCode.Value = strteststring.Substring(371, 3).PadLeft(3, "0"c)
                    End If ' "VM" update
                    If strteststring.StartsWith("VC") Then ' "VC" update
                        Trace.WriteLine("Setting VC(U):" & strteststring.Substring(28, 32))
                        Supdet.Deleted.Value = False
                        If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                        Supdet.DateOrderCloseStart.Value = CDate(strteststring.Substring(28, 8))
                        Supdet.DateOrderCloseEnd.Value = CDate(strteststring.Substring(36, 8))
                        If boolBlankStartDate1 = False And boolBlankEndDate1 = False Then
                            Supdet.DateDelCloseStart.Value = CDate(strteststring.Substring(44, 8))
                            Supdet.DateDelCloseEnd.Value = CDate(strteststring.Substring(52, 8))
                        End If
                    End If ' "VC" update
                    If strteststring.StartsWith("VD") Or strteststring.StartsWith("VR") Then
                        ' "VD" or "VR" update
                        Supdet.Deleted.Value = False
                        If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                        Supdet.AddressLine1.Value = strteststring.Substring(31, 30).PadRight(30, " "c)
                        Supdet.AddressLine2.Value = strteststring.Substring(61, 30).PadRight(30, " "c)
                        Supdet.AddressLine3.Value = strteststring.Substring(91, 30).PadRight(30, " "c)
                        Supdet.AddressLine4.Value = strteststring.Substring(121, 30).PadRight(30, " "c)
                        Supdet.AddressLine5.Value = strteststring.Substring(151, 30).PadRight(30, " "c)
                        Supdet.AddressPostcode.Value = strteststring.Substring(181, 8).PadRight(8, " "c)
                        Supdet.PhoneNumber1.Value = strteststring.Substring(189, 16).PadRight(16, " "c)
                        Supdet.PhoneNumber2.Value = strteststring.Substring(205, 16).PadRight(16, " "c)
                        Supdet.FaxNumber1.Value = strteststring.Substring(221, 16).PadRight(16, " "c)
                        Supdet.FaxNumber2.Value = strteststring.Substring(237, 16).PadRight(16, " "c)
                        Supdet.ContactName1.Value = strteststring.Substring(253, 30).PadRight(30, " "c)
                        Supdet.DepotNotes.Value = strteststring.Substring(283, 60).PadRight(60, " "c)
                        If strteststring.StartsWith("VR") Then
                            Supdet.ReturnsMessageRef.Value = 0
                            Dim strTestReturnsMess As String = strteststring.Substring(343, 2)
                            Dim intTestReturnsMess As Integer = CInt(strteststring.Substring(343, 2))
                            If intTestReturnsMess > 0 Then
                                'If strTestReturnsMess <> Nothing And strTestReturnsMess <> "  " And strTestReturnsMess <> String.Empty Then
                                Supdet.ReturnsMessageRef.Value = intTestReturnsMess
                            End If
                            'If strteststring.Substring(343, 2) > "  " Or strteststring.Substring(343, 2) <> Nothing Then Supdet.ReturnsMessageRef.Value = CDec(strteststring.Substring(343, 2).PadLeft(2, "0"))
                        End If
                        Supdet.ReturnsPolicyCode.Value = "0"
                    End If ' "VD" or "VR" update
                    If strteststring.StartsWith("VQ") Then ' "VQ" update
                        Supdet.Deleted.Value = False
                        If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                        Supdet.OrderMinType.Value = strteststring.Substring(36, 1)
                        Supdet.OrderMinValue.Value = CInt(strteststring.Substring(37, 7))
                        Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                        Supdet.OrderMinWeight.Value = CInt(strteststring.Substring(51, 7))
                        Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                        Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                        Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                        Supdet.OrderMinUnits.Value = CInt(strteststring.Substring(44, 7))
                    End If ' "VQ" update
                    If strteststring.StartsWith("VT") Then ' "VT" update
                        Supdet.Deleted.Value = False
                        If strteststring.Substring(22, 1) = "Y" Then Supdet.Deleted.Value = True
                        Supdet.Tradanet.Value = False
                        If strteststring.Substring(36, 1) = "Y" Then Supdet.Tradanet.Value = True
                        Supdet.BBC.Value = strteststring.Substring(40, 1)
                        Supdet.SOQFrequency.Value = CInt(strteststring.Substring(41, 4))
                        intSQQFreq = Supdet.SOQFrequency.Value
                        If intSQQFreq >= 64 Then
                            intSQQFreq -= 64
                            Supdet.ReviewDay0.Value = True
                        Else
                            Supdet.ReviewDay0.Value = False
                        End If

                        If intSQQFreq >= 32 Then
                            intSQQFreq -= 32
                            Supdet.ReviewDay6.Value = True
                        Else
                            Supdet.ReviewDay6.Value = False
                        End If

                        If intSQQFreq >= 16 Then
                            intSQQFreq -= 16
                            Supdet.ReviewDay5.Value = True
                        Else
                            Supdet.ReviewDay5.Value = False
                        End If

                        If intSQQFreq >= 8 Then
                            intSQQFreq -= 8
                            Supdet.ReviewDay4.Value = True
                        Else
                            Supdet.ReviewDay4.Value = False
                        End If

                        If intSQQFreq >= 4 Then
                            intSQQFreq -= 4
                            Supdet.ReviewDay3.Value = True
                        Else
                            Supdet.ReviewDay3.Value = False
                        End If

                        If intSQQFreq >= 2 Then
                            intSQQFreq -= 2
                            Supdet.ReviewDay2.Value = True
                        Else
                            Supdet.ReviewDay2.Value = False
                        End If

                        If intSQQFreq >= 1 Then
                            intSQQFreq -= 1
                            Supdet.ReviewDay1.Value = True
                        Else
                            Supdet.ReviewDay1.Value = False
                        End If

                        Supdet.LeadTime0.Value = CInt(strteststring.Substring(45, 4))
                        Supdet.LeadTime1.Value = CInt(strteststring.Substring(49, 4))
                        Supdet.LeadTime2.Value = CInt(strteststring.Substring(53, 4))
                        Supdet.LeadTime3.Value = CInt(strteststring.Substring(57, 4))
                        Supdet.LeadTime4.Value = CInt(strteststring.Substring(61, 4))
                        Supdet.LeadTime5.Value = CInt(strteststring.Substring(65, 4))
                        Supdet.LeadTime6.Value = CInt(strteststring.Substring(69, 4))
                        Supdet.DeliveryCheckMethod.Value = strteststring.Substring(73, 1)
                        If (Supdet.DeliveryCheckMethod.Value.Trim = "") Then Supdet.DeliveryCheckMethod.Value = "0"
                        Supdet.LeadTimeFixed.Value = Supdet.LeadTime0.Value + Supdet.LeadTime1.Value + Supdet.LeadTime2.Value + Supdet.LeadTime3.Value + Supdet.LeadTime4.Value + Supdet.LeadTime5.Value + Supdet.LeadTime6.Value
                        If Supdet.LeadTimeFixed.Value < 0 Then Supdet.LeadTimeFixed.Value = 0
                        If Supdet.LeadTimeFixed.Value > 0 Then Supdet.LeadTimeFixed.Value = CInt((Supdet.LeadTimeFixed.Value / 7) + 0.5)
                    End If ' "VT" update
                    Supdet.SaveIfExists()
                Catch ex As Exception
                End Try
            End If
        End If ' At this point - not creating a dummy VD record for "O"
    End Sub

    Public Sub OutputProductUpdateError(ByVal strTestString As String, ByVal strDataError As String)
        If boolOutputErrorToSthoa = True Then
            SetupHash("AU", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            If strDataError = "Wrong Record Type" Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & strSthoRecordHashValue & strDataError.PadRight(18, " "c) & "    R E J E C T E D      No Changes Made"
            Else
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & strSthoRecordHashValue & strDataError.PadRight(18, " "c) & strTestString.Substring(22, 3) & strTestString.Substring(25, 1) & strTestString.Substring(26, 6) & strTestString.Substring(32, 27)
            End If
            intSthoaRecordsOut = intSthoaRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthoaRecordsOut.ToString & " - STHOA"
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        decHashValue = 1
        SetupHash("CU", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 10/09/2010
        ' Referral No : 277B
        ' Notes       : Head office bad type count being updated within this function & just after function returns
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'intHeadOfficeBadType = intHeadOfficeBadType + 1

        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub

    Public Sub OutputOrderConfirmationError(ByVal strTestString As String, ByVal strDataError As String, ByVal OBCHashValue As Decimal)
        ' Output Order Confirmation Error to STHOC
        If boolOutputErrorToSthoa = True Then
            SetupHash("OB", OBCHashValue, dateHashDate)
            'SetupHash("OB", decHashValue, dateHashDate)
            If strSthpoText <> String.Empty Then
                strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthpoText = strSthpoText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & " " & strDataError.PadRight(18, " "c).Substring(0, 18) & strTestString
            intSthpoRecordsOut = intSthpoRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthpoRecordsOut.ToString & " - STHOC"
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If strSthpoText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthpoFileName, strSthpoText)
                strSthpoText = String.Empty
            End If
        End If

        decHashValue = OBCHashValue

        SetupHash("CO", decHashValue, dateHashDate)
        If strSthocText.Trim <> String.Empty Then
            If strSthocText.Trim.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & " " & strDataError.PadRight(18, " "c) & strTestString
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) += 1
        arrintProcessedCounts(intRecordOccurrence) -= 1
        boolPassedValidation = False
    End Sub ' Output Order Confirmation Error to STHOC

    Public Sub OutputBBCIssuesError(ByVal strTestString As String, ByVal strDataError As String)
        ' Output BBC Issues Error to STHOC

        decHashValue = 1
        SetupHash("CI", decHashValue, dateHashDate)
        If (strDataError.Length > 18) Then strDataError = strDataError.Substring(0, 18)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
        boolPassedValidation = False
    End Sub ' Output BBC Issues Error to STHOC

    Public Sub OutputSupplierUpdateError(ByVal strTestString As String, ByVal strDataError As String)
        ' Supplier Update Error
        If (strDataError.Length > 18) Then strDataError = strDataError.Substring(0, 18)
        If boolOutputErrorToSthoa = True Then
            SetupHash("AV", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
            intSthoaRecordsOut = intSthoaRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthoaRecordsOut.ToString & " - STHOA"
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthpaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        decHashValue = 1
        SetupHash("CV", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub ' Supplier Update Error

    Public Sub OutputStoreMasterUpdateError(ByVal strTestString As String, ByVal strDataError As String)
        ' Supplier Update Error
        decHashValue = intHeadOfficeNumberOfRecords
        strRecordType = "D"
        If boolOutputErrorToSthoa = True Then
            SetupHash("AU", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                If strSthoaText.EndsWith(vbCrLf) = False Then strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & strSthoRecordHashValue & strDataError.PadRight(18, " "c) & strTestString.Substring(22, 1) & strTestString.Substring(23, 3)
            intSthoaRecordsOut = intSthoaRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthoaRecordsOut.ToString & " - STHOA"
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        SetupHash("CD", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub ' Supplier Update Error

    Public Sub OutputPlangramUpdateError(ByVal strTestString As String, ByVal strDataError As String)
        ' Supplier Update Error
        decHashValue = intHeadOfficeNumberOfRecords
        If (strDataError.Length > 18) Then strDataError = strDataError.Substring(0, 18)
        strRecordType = "D"
        If boolOutputErrorToSthoa = True Then
            SetupHash("AX", decHashValue, dateHashDate)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & "D" & strSthoRecordHashValue & strDataError.PadRight(18, " "c) & strTestString '.Substring(146, 22)
            intSthoaRecordsOut = intSthoaRecordsOut + 1
            ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthoaRecordsOut.ToString & " - STHOA"
            ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(strSthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If
        End If
        SetupHash("CX", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub ' Supplier Update Error

    Public Sub OutputCountCycleUpdateError(ByVal strTestString As String, ByVal strDataError As String)

        decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), dateHashDate) + 1
        strRecordType = "D"
        SetupHash("CP", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf
        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub

    Public Sub OutputHierarchyUpdateError(ByVal strTestString As String, ByVal strDataError As String)
        decHashValue = DateDiff(DateInterval.Day, CDate("1900-01-01"), dateHashDate) + 1
        strRecordType = "D"
        SetupHash("CH", decHashValue, dateHashDate)
        If strSthocText <> String.Empty Then
            If strSthocText.EndsWith(vbCrLf) = False Then strSthocText = strSthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strSthocText = strSthocText.ToString.TrimEnd(" "c) & strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strRecordType & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf

        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        If strSthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strSthocFileName, strSthocText)
            strSthocText = String.Empty
        End If
        intHeadOfficeBadType = intHeadOfficeBadType + 1
        arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
    End Sub

    Public Sub AddToPendingHpstv(ByVal strTestString As String) ' Add RECORD from HPSTV 
        If strPendingHpstvText <> String.Empty Then
            strPendingHpstvText = strPendingHpstvText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        strPendingHpstvText = strPendingHpstvText.ToString.TrimEnd(" "c) & strTestString '& vbCrLf
        If strPendingHpstvText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(strPendingHpstvFileName, strPendingHpstvText)
            strPendingHpstvText = String.Empty
        End If
    End Sub

    Public Function ValidateU1andU4Data(ByVal strTestString As String) As Boolean ' Validate U1 & U4 Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If strTestString.Substring(22, 3) <> _StoreNumber Then
            OutputProductUpdateError(strTestString, "Wrong Store Numb.".PadRight(18, " "c))
            intHeadOfficeWrongStore += 1
            Return False
        End If
        If strTestString.Substring(25, 1) <> "1" And strTestString.Substring(25, 1) <> "4" Then
            OutputProductUpdateError(strTestString, "Wrong Record Type".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If CInt(strTestString.Substring(26, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid SKU number".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.Substring(51, 1) <> " " And strTestString.Substring(51, 1) <> "D" And strTestString.Substring(51, 1) <> "R" And strTestString.Substring(51, 1) <> "S" And strTestString.Substring(51, 1) <> "X" Then
            OutputProductUpdateError(strTestString, "Invalid Status".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If CInt(strTestString.Substring(53, 5)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid Supp. No.".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If CInt(strTestString.Substring(167, 1)) > _NoOfVATRates Then
            OutputProductUpdateError(strTestString, "Invalid Vat Rate".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        strMyTestString = String.Empty & strTestString.Substring(192, 13).PadRight(13, " "c)
        Dim boolEanIsValid As Boolean = True
        If strMyTestString <> "             " Then
            boolEanIsValid = EanValid(strMyTestString)
        End If
        If boolEanIsValid = False Then
            OutputProductUpdateError(strTestString, "Invalid EAN Number".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.Substring(293, 1) <> " " And strTestString.Substring(293, 1) <> "S" And strTestString.Substring(293, 1) <> "L" And strTestString.Substring(293, 1) <> "C" And strTestString.Substring(293, 1) <> "X" Then
            OutputProductUpdateError(strTestString, "Invalid TAG Flag".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        strMyTestString = String.Empty & strTestString.Substring(234, 4).PadRight(4, " "c)
        If strMyTestString <> "    " And strMyTestString <> "0000" Then
            If strMyTestString.Substring(0, 1) <> "1" Then
                OutputProductUpdateError(strTestString, "Bad Seasonal. Patt".PadRight(18, " "c))
                intHeadOfficeBadType += 1
                Return False
            End If
        End If
        strMyTestString = String.Empty & strTestString.Substring(238, 4).PadRight(4, " "c)
        If strMyTestString <> "    " And strMyTestString <> "0000" Then
            If strMyTestString.Substring(0, 1) <> "3" Then
                OutputProductUpdateError(strTestString, "Bad Bank Hol. Patt".PadRight(18, " "c))
                intHeadOfficeBadType += 1
                Return False
            End If
        End If
        strMyTestString = String.Empty & strTestString.Substring(242, 4).PadRight(4, " "c)
        If strMyTestString <> "    " And strMyTestString <> "0000" Then
            If strMyTestString.Substring(0, 1) <> "5" Then
                OutputProductUpdateError(strTestString, "Bad Promo. Pattern".PadRight(18, " "c))
                intHeadOfficeBadType += 1
                Return False
            End If
        End If
        strMyTestString = strTestString.Substring(318, 6).PadRight(6, " "c)
        If strMyTestString = "      " Then
            OutputProductUpdateError(strTestString, "Null Price Evt.No.".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        strMyTestString = strTestString.Substring(324, 2).PadRight(2, " "c)
        If strMyTestString = "  " Then
            OutputProductUpdateError(strTestString, "Null Pric.Priority".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.Substring(331, 1) <> " " And strTestString.Substring(331, 1) <> "B" And strTestString.Substring(331, 1) <> "Q" Then
            OutputProductUpdateError(strTestString, "Invalid Quarantine".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Alan Lewis
        ' Date        : 07/10/2010
        ' Referral No : 366
        ' Notes       : Allow U4 record to have a Sale Type (SALT) of 'C'.  Part of the Think21 project (Wix1396)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If strTestString.Substring(352, 1) <> " " _
        And strTestString.Substring(352, 1) <> "B" _
        And strTestString.Substring(352, 1) <> "P" _
        And strTestString.Substring(352, 1) <> "A" _
        And strTestString.Substring(352, 1) <> "S" _
        And strTestString.Substring(352, 1) <> "G" _
        And strTestString.Substring(352, 1) <> "R" _
        And strTestString.Substring(352, 1) <> "D" _
        And strTestString.Substring(352, 1) <> "I" _
        And strTestString.Substring(352, 1) <> "O" _
        And strTestString.Substring(352, 1) <> "V" _
        And strTestString.Substring(352, 1) <> "Y" _
        And strTestString.Substring(352, 1) <> "T" _
        And strTestString.Substring(352, 1) <> "W" _
        Or (strTestString.Substring(352, 1) = "C" And Not strTestString.StartsWith("U4")) Then
            OutputProductUpdateError(strTestString, "Invalid Sale Type".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.Substring(353, 1) <> " " And strTestString.Substring(353, 1) <> "S" And strTestString.Substring(353, 1) <> "E" Then
            OutputProductUpdateError(strTestString, "Invalid Model Type".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.StartsWith("U1") Then
            intHeadOfficeNewItems = intHeadOfficeNewItems + 1
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 10/09/2010
        ' Referral No : 277B
        ' Notes       : Increase change count if type "U4" only!
        '               Like type "U1" above if bad count not increased
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'intHeadOfficeChanges = intHeadOfficeChanges + 1
        If strTestString.StartsWith("U4") Then
            intHeadOfficeChanges += 1
        End If

        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate U1 & U4 Data

    Public Function ValidateU5Data(ByVal strTestString As String) As Boolean ' Validate U5 Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If strTestString.Substring(22, 3) <> _StoreNumber Then
            OutputProductUpdateError(strTestString, "Wrong Store Numb.".PadRight(18, " "c))
            intHeadOfficeWrongStore = intHeadOfficeWrongStore + 1
            Return False
        End If
        If strTestString.Substring(25, 1) <> "5" Then
            OutputProductUpdateError(strTestString, "Wrong Record Type".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If CInt(strTestString.Substring(28, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid SKU number".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        If strTestString.Substring(26, 1) <> "T" And strTestString.Substring(26, 1) <> "G" Then
            OutputProductUpdateError(strTestString, "Invalid Info. Type".PadRight(18, " "c))
            intHeadOfficeBadType += 1
            Return False
        End If
        intHeadOfficeChanges = intHeadOfficeChanges + 1
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate U5 Data

    Public Function ValidateU6Data(ByVal strTestString As String) As Boolean ' Validate U6 Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)
        StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTestString.Substring(22, 6))
        StockMaster.Stocks = StockMaster.LoadMatches()
        If StockMaster.Stocks.Count < 1 Then
            OutputProductUpdateError(strTestString, "Invalid SKU number".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate U6 Data

    Public Function ValidateUAData(ByVal strTestString As String) As Boolean ' Validate UA Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If CInt(strTestString.Substring(22, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Inv. Mix & Match".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If CInt(strTestString.Substring(28, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid SKU number".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate UA Data

    Public Function ValidateUBData(ByVal strTestString As String) As Boolean ' Validate UB Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If CInt(strTestString.Substring(22, 6)) < 100 Then
            OutputProductUpdateError(strTestString, "Invalid Event No.".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If CInt(strTestString.Substring(68, 2)) < 1 Then
            OutputProductUpdateError(strTestString, "Inv.Event Priority".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate UB Data

    Public Function ValidateUCData(ByVal strTestString As String) As Boolean ' Validate UC Data

        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If CInt(strTestString.Substring(22, 6)) < 100 Then
            OutputProductUpdateError(strTestString, "Invalid Event No.".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If strTestString.Substring(28, 2) <> "PS" And strTestString.Substring(28, 2) <> "PM" And strTestString.Substring(28, 2) <> "TS" And strTestString.Substring(28, 2) <> "TM" And strTestString.Substring(28, 2) <> "DG" And strTestString.Substring(28, 2) <> "QS" And strTestString.Substring(28, 2) <> "QM" And strTestString.Substring(28, 2) <> "QD" And strTestString.Substring(28, 2) <> "MS" And strTestString.Substring(28, 2) <> "MM" And strTestString.Substring(28, 2) <> "HS" Then
            OutputProductUpdateError(strTestString, "UC: Bad Event Type".PadRight(18, " "c))
            Return False
        End If
        If (strTestString.Substring(28, 2) = "PS") Or (strTestString.Substring(28, 2) = "TS") And (strTestString.Substring(28, 2) = "QS") Or (strTestString.Substring(28, 2) = "MS") Then
            Dim StockMaster As New BOStock.cStock(_Oasys3DB)
            StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTestString.Substring(30, 6))
            StockMaster.Stocks = StockMaster.LoadMatches()
            If StockMaster.Stocks.Count < 1 Then
                OutputProductUpdateError(strTestString, "UC: NOF SKU Key1".PadRight(18, " "c))
                Return False
            End If
        End If

        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate UC Data

    Public Function ValidateUDData(ByVal strTestString As String) As Boolean ' Validate UD Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If CInt(strTestString.Substring(22, 6)) < 100 Then
            OutputProductUpdateError(strTestString, "Invalid Event No.".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If CInt(strTestString.Substring(28, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid Deal Group".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate UD Data

    Public Function ValidateUEData(ByVal strTestString As String) As Boolean ' Validate UE Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = True
        If CInt(strTestString.Substring(22, 6)) < 100 Then
            OutputProductUpdateError(strTestString, "Invalid Event No.".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If CInt(strTestString.Substring(28, 6)) < 1 Then
            OutputProductUpdateError(strTestString, "Invalid Category".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        If CInt(strTestString.Substring(34, 6)) = 0 Then
            OutputProductUpdateError(strTestString, "Invalid SKU number".PadRight(18, " "c))
            intHeadOfficeBadType = intHeadOfficeBadType + 1
            Return False
        End If
        arrintProcessedCounts(intRecordOccurrence) += 1
        Return True
    End Function ' Validate UE Data

    Public Function ValidateCYData(ByVal strTestString As String) As Boolean ' Validate CY Data
        Dim strMyTestString As String = String.Empty
        intCYProcessedCount = intCYProcessedCount + 1
        boolOutputErrorToSthoa = False
        intMaximumDayNumberInCycle = 0
        arrintProcessedCounts(intRecordOccurrence) += 1
        intWorkDayNumberInCycle = CInt(strTestString.Substring(22, 2))
        If intWorkDayNumberInCycle > 0 And intWorkDayNumberInCycle < 15 Then
            intMaximumDayNumberInCycle = intWorkDayNumberInCycle * 7
        End If
        If intWorkDayNumberInCycle <= 0 Or intWorkDayNumberInCycle > 14 Then
            OutputCountCycleUpdateError(strTestString, "Bad Weeks In Cycle".PadRight(18, " "c))
            intCYRejectedCount = intCYRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If

        Return True
    End Function ' Validate CY Data

    Public Function ValidateCCData(ByVal strTestString As String) As Boolean ' Validate CC Data
        Dim strMyTestString As String = String.Empty
        Dim InCategory As String = strTransmissionFileData.Substring(24, 6)
        Dim InGroup As String = strTransmissionFileData.Substring(30, 6)
        Dim InSubGroup As String = strTransmissionFileData.Substring(36, 6)
        Dim InStyle As String = strTransmissionFileData.Substring(42, 6)

        intCCProcessedCount = intCCProcessedCount + 1
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        intWorkDayNumberInCycle = CInt(strTestString.Substring(22, 2))
        '        If intWorkDayNumberInCycle > 0 And intWorkDayNumberInCycle < 15 Then
        ' intMaximumDayNumberInCycle = intWorkDayNumberInCycle * 7
        'End If

        If intWorkDayNumberInCycle <= 0 Or intWorkDayNumberInCycle > intMaximumDayNumberInCycle Then
            OutputCountCycleUpdateError(strTestString, "Bad Cycle Day Num.".PadRight(18, " "c))
            intCCRejectedCount = intCCRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If
        If (InCategory = "000000") Or (IsNumeric(InCategory) = False) Then 'And strTestString.Substring(24, 6).PadLeft(6, "0") Then
            OutputCountCycleUpdateError(strTestString, "Bad Category".PadRight(18, " "c))
            intCCRejectedCount = intCCRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If
        If ((InGroup = "000000") And ((InSubGroup > "000000") Or (InStyle > "000000"))) Or (IsNumeric(InGroup) = False) Then
            'If strTestString.Substring(30, 6).PadLeft(6, "0") = "000000" Then
            OutputCountCycleUpdateError(strTestString, "Bad Group".PadRight(18, " "c))
            intCCRejectedCount = intCCRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If
        If ((InSubGroup = "000000") And (InStyle > "000000")) Or (IsNumeric(InSubGroup) = False) Then
            'If strTestString.Substring(36, 6).PadLeft(6, "0") = "000000" Then
            OutputCountCycleUpdateError(strTestString, "Bad Sub Group".PadRight(18, " "c))
            intCCRejectedCount = intCCRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If
        If (IsNumeric(InStyle) = False) Then
            'If strTestString.Substring(42, 6).PadLeft(6, "0") = "000000" Then
            OutputCountCycleUpdateError(strTestString, "Bad Style".PadRight(18, " "c))
            intCCRejectedCount = intCCRejectedCount + 1
            'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            Return False
        End If
        Return True
    End Function ' Validate CC Data

    Public Function ValidateCMData(ByVal strTestString As String, ByVal boolOutputSthocError As Boolean) As Boolean ' Validate CM Data
        Dim strMyTestString As String = String.Empty
        Dim StockMaster As New BOStock.cStock(_Oasys3DB)

        If boolOutputSthocError = True Then
            intCMProcessedCount += 1
            arrintProcessedCounts(intRecordOccurrence) += 1
        End If

        boolOutputErrorToSthoa = False

        If (IsDate(strTestString.Substring(22, 10)) = False) Then
            If (boolOutputSthocError = True) Then
                OutputCountCycleUpdateError(strTestString, "Bad Date".PadRight(18, " "c))
                intCMRejectedCount += 1
                'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            End If
            Return False
        End If

        If strTestString.Substring(32, 6).PadLeft(6, "0"c) = "000000" Then
            If boolOutputSthocError = True Then
                OutputCountCycleUpdateError(strTestString, "Bad Sku Number".PadRight(18, " "c))
                intCMRejectedCount += 1
                'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
            End If
            Return False
        End If
        If strTestString.Substring(32, 6).PadLeft(6, "0"c) <> "000000" Then
            StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strTestString.Substring(32, 6).PadLeft(6, "0"c))
            StockMaster.Stocks = StockMaster.LoadMatches()
            If StockMaster.Stocks.Count < 1 Then
                If boolOutputSthocError = True Then
                    OutputCountCycleUpdateError(strTestString, "Bad Sku Number".PadRight(18, " "c))
                    intCMRejectedCount = intCMRejectedCount + 1
                    'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
                End If
                Return False
            End If
        End If
        If strTestString.Substring(38, 1).ToUpper <> "A" And strTestString.Substring(38, 1) <> "D" Then
            If boolOutputSthocError = True Then
                OutputCountCycleUpdateError(strTestString, "Bad Function".PadRight(18, " "c))
                'arrintRejectedCounts(intRecordOccurrence) = arrintRejectedCounts(intRecordOccurrence) + 1
                intCMRejectedCount += 1
            End If
            Return False
        End If
        Return True
    End Function ' Validate CM Data

    Public Function ValidateHMData(ByVal strTestString As String) As Boolean ' Validate HM Data
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False

        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 1) = False Then
            OutputHierarchyUpdateError(strTestString, "Bad Hierarchy Lev.".PadRight(18, " "c))
            Return False
        End If
        If strTestString.Substring(23, 1) < "1" Or strTestString.Substring(23, 1) > "5" Then
            OutputHierarchyUpdateError(strTestString, "Inv. Hierarchy Lev".PadRight(18, " "c))
            Return False
        End If
        If IsItNumeric(strTestString, 24, 6) = False Then
            OutputHierarchyUpdateError(strTestString, "Bad Hierarchy No.".PadRight(18, " "c))
            Return False
        End If
        If IsItAnIndicator(strTestString, 80) = False Then
            OutputHierarchyUpdateError(strTestString, "Bad Deletion Ind.".PadRight(18, " "c))
            Return False
        End If
        Return True
    End Function ' Validate HM Data

    Public Function ValidateCDData(ByVal strTestString As String) As Boolean ' Validate CD(Data)
        Dim strMyTestString As String = String.Empty
        Dim intTestNumeric As Integer = 0
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 22, 3) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   3 +  22"))
            Return False
        End If
        If IsItNumeric(strTestString, 26, 9) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   9 +  26"))
            Return False
        End If
        If IsItNumeric(strTestString, 35, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  35"))
            Return False
        End If
        If IsItNumeric(strTestString, 41, 7) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   7 +  41"))
            Return False
        End If
        If IsItNumeric(strTestString, 48, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  48"))
            Return False
        End If
        If IsItNumeric(strTestString, 54, 4) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   4 +  54"))
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(22, 3))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Assembly Dep.")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(26, 9))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Container")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(48, 6))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null P.O Number")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(54, 4))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null P.O Line No.")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(35, 6))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null SKU Number")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(41, 7))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "No Quantity value.")
            Return False
        End If
        Return True
    End Function ' Validate CD Data

    Public Function ValidateCSData(ByVal strTestString As String) As Boolean ' Validate CS(Data)
        Dim strMyTestString As String = String.Empty
        Dim intTestNumeric As Integer = 0
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 22, 3) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   3 +  22"))
            Return False
        End If
        If IsItNumeric(strTestString, 35, 3) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   3 +  35"))
            Return False
        End If
        If IsItNumeric(strTestString, 39, 9) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   9 +  39"))
            Return False
        End If
        If IsItNumeric(strTestString, 84, 7) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   7 +  84"))
            Return False
        End If
        If IsItNumeric(strTestString, 91, 9) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   9 +  91"))
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(35, 3))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Assembly Dep.")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(39, 9))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Container")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(22, 3))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Despatch Dep.")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(84, 7))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Zero Detail lines.")
            Return False
        End If
        Return True
    End Function ' Validate CS Data

    Public Function ValidateIDData(ByVal strTestString As String) As Boolean ' Validate II(Data)
        Dim strMyTestString As String = String.Empty
        Dim intTestNumeric As Integer = 0
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 22, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  22"))
            Return False
        End If
        If IsItNumeric(strTestString, 28, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  28"))
            Return False
        End If
        If IsItNumeric(strTestString, 34, 4) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   4 +  34"))
            Return False
        End If
        If IsItNumeric(strTestString, 38, 7) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   7 +  38"))
            Return False
        End If
        If IsItNumeric(strTestString, 45, 7) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   7 +  45"))
            Return False
        End If
        If IsItNumeric(strTestString, 52, 7) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   7 +  52"))
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(22, 6))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Invalid Issue Note")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(28, 6))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null SKU Number")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(34, 4))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Issue Line No.")
            Return False
        End If
        Return True
    End Function ' Validate ID Data

    Public Function ValidateIIData(ByVal strTestString As String) As Boolean ' Validate II(Data)
        Dim strMyTestString As String = String.Empty
        Dim intTestNumeric As Integer = 0
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 22, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  22"))
            Return False
        End If
        If IsItNumeric(strTestString, 36, 5) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   5 +  36"))
            Return False
        End If
        If IsItNumeric(strTestString, 41, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  41"))
            Return False
        End If
        If IsItNumeric(strTestString, 47, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  47"))
            Return False
        End If
        If IsItNumeric(strTestString, 53, 6) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @   6 +  53"))
            Return False
        End If
        If IsItNumeric(strTestString, 60, 10) = False Then
            OutputBBCIssuesError(strTestString, ("Invalid @  10 +  60"))
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(22, 6))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Issue Note No.")
            Return False
        End If
        If ValidDate(strTestString, 28) = False Then
            OutputBBCIssuesError(strTestString, ("Bad Issue Date"))
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(36, 5))
        If intTestNumeric < 1 Then
            OutputBBCIssuesError(strTestString, "Null Supplier No.")
            Return False
        End If
        intTestNumeric = CInt(strTestString.Substring(41, 6))
        If intTestNumeric < 1 Then
            intTestNumeric = CInt(strTestString.Substring(47, 6))
            If intTestNumeric < 1 Then
                OutputBBCIssuesError(strTestString, "No Store PO Number")
                Return False
            End If
        End If
        If strTestString.Substring(59, 1) <> " " And strTestString.Substring(59, 1) <> "C" And strTestString.Substring(59, 1) <> "D" And strTestString.Substring(59, 1) <> "W" And strTestString.Substring(59, 1) <> "I" And strTestString.Substring(59, 1) <> "A" Then
            OutputBBCIssuesError(strTestString, "Invalid BBC Code")
            Return False
        End If
        Return True
    End Function ' Validate II Data

    Public Function ValidateVMData(ByVal strTestString As String) As Boolean ' Validate VM(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If IsItNumeric(strTestString, 371, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 371"))
            Return False
        End If
        If IsItNumeric(strTestString, 374, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 374"))
            Return False
        End If
        If IsItNumeric(strTestString, 377, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 377"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VM Data

    Public Function ValidateVNData(ByVal strTestString As String) As Boolean ' Validate VN(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If IsItNumeric(strTestString, 28, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 371"))
            Return False
        End If
        If IsItNumeric(strTestString, 31, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 374"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VN Data

    Public Function ValidateVDData(ByVal strTestString As String) As Boolean ' Validate VD(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If IsItNumeric(strTestString, 28, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 371"))
            Return False
        End If
        If IsItNumeric(strTestString, 343, 2) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   2 + 343"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VD Data

    Public Function ValidateVQData(ByVal strTestString As String) As Boolean ' Validate VQ(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If ValidDate(strTestString, 28) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  28"))
            Return False
        End If
        If strTestString.Substring(36, 1) <> "N" And strTestString.Substring(36, 1) <> "M" And strTestString.Substring(36, 1) <> "C" And strTestString.Substring(36, 1) <> "U" And strTestString.Substring(36, 1) <> "E" And strTestString.Substring(36, 1) <> "T" Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  36"))
            Return False
        End If
        If IsItNumeric(strTestString, 37, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 37"))
            Return False
        End If
        If IsItNumeric(strTestString, 44, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 44"))
            Return False
        End If
        If IsItNumeric(strTestString, 51, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 51"))
            Return False
        End If
        If IsItNumeric(strTestString, 58, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 58"))
            Return False
        End If
        If IsItNumeric(strTestString, 65, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 65"))
            Return False
        End If
        If IsItNumeric(strTestString, 72, 7) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   7 + 72"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VQ Data

    Public Function ValidateVTData(ByVal strTestString As String) As Boolean ' Validate VT(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If ValidDate(strTestString, 28) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  28"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 36) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  36"))
        End If
        If IsItNumeric(strTestString, 37, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 37"))
            Return False
        End If
        If strTestString.Substring(40, 1) <> " " And strTestString.Substring(40, 1) <> "D" And strTestString.Substring(40, 1) <> "C" And strTestString.Substring(40, 1) <> "W" And strTestString.Substring(40, 1) <> "A" Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  40"))
            Return False
        End If
        If strTestString.Substring(40, 1) <> " " And CInt(strTestString.Substring(37, 3).PadLeft(3, "0"c)) < 1 Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 37"))
            Return False
        End If
        If IsItNumeric(strTestString, 41, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 41"))
            Return False
        End If
        If IsItNumeric(strTestString, 45, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 45"))
            Return False
        End If
        If IsItNumeric(strTestString, 49, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 49"))
            Return False
        End If
        If IsItNumeric(strTestString, 53, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 53"))
            Return False
        End If
        If IsItNumeric(strTestString, 57, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 57"))
            Return False
        End If
        If IsItNumeric(strTestString, 61, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 61"))
            Return False
        End If
        If IsItNumeric(strTestString, 65, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 65"))
            Return False
        End If
        If IsItNumeric(strTestString, 69, 4) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   4 + 69"))
            Return False
        End If
        If IsItNumeric(strTestString, 73, 1) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 + 73"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VT Data

    Public Function ValidateVRData(ByVal strTestString As String) As Boolean ' Validate VR(Data)
        Dim strMyTestString As String = String.Empty
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If IsItNumeric(strTestString, 28, 3) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   3 + 371"))
            Return False
        End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VR Data

    Public Function ValidateVCData(ByVal strTestString As String) As Boolean ' Validate VC(Data)
        Dim strMyTestString As String = String.Empty
        Dim dateWorkStart As Date = Date.MinValue
        Dim dateWorkEnd As Date = Date.MinValue
        boolOutputErrorToSthoa = False
        arrintProcessedCounts(intRecordOccurrence) += 1
        If IsItNumeric(strTestString, 23, 5) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   5 +  23"))
            Return False
        End If
        If ValidDate(strTestString, 28) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  28"))
            Return False
        End If
        boolBlankStartDate = boolIsBlankDate
        boolBlankEndDate = boolIsBlankDate
        If ValidDate(strTestString, 36) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  36"))
            Return False
        End If
        boolBlankEndDate = boolIsBlankDate
        If ValidDate(strTestString, 44) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  44"))
            Return False
        End If
        boolBlankStartDate1 = boolIsBlankDate
        If ValidDate(strTestString, 52) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  52"))
            Return False
        End If
        boolBlankEndDate1 = boolIsBlankDate
        'If boolBlankStartDate = False And boolBlankEndDate = False Then
        '    dateWorkStart = strTestString.Substring(28, 8)
        '     _EndDate = strTestString.Substring(36, 8)
        '    If  _EndDate <  _StartDate Then
        '        OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  28"))
        '        Return False
        '    End If
        'End If
        'If boolBlankStartDate1 = False And boolBlankEndDate1 = False Then
        '    dateWorkStart = CDate(strTestString.Substring(44, 8).PadRight(8, " "))
        '     _EndDate = CDate(strTestString.Substring(52, 8).PadRight(8, " "))
        '    If  _EndDate <  _StartDate Then
        '        OutputSupplierUpdateError(strTestString, ("Invalid @   8 +  44"))
        '        Return False
        '    End If
        'End If
        If IsItAnIndicator(strTestString, 22) = False Then
            OutputSupplierUpdateError(strTestString, ("Invalid @   1 +  22"))
        End If
        Dim intSupplierNumber As Integer = 0
        If strTestString.Substring(23, 5) <> "     " Then intSupplierNumber = CInt(strTestString.Substring(23, 5).PadLeft(5, "0"c))
        If intSupplierNumber = 0 Then OutputSupplierUpdateError(strTestString, "Supplier No Zeros")
        Return True
    End Function ' Validate VC Data

    Public Function IsItAnIndicator(ByVal strTestString As String, ByVal intStartAt As Integer) As Boolean
        If strTestString.Substring(intStartAt, 1).PadRight(1, " "c) = "Y" Or strTestString.Substring(intStartAt, 1) = "N" Or strTestString.Substring(intStartAt, 1) = " " Then
            Return True
        End If
        Return False
    End Function

    Public Function IsItNumeric(ByVal strTestString As String, ByVal intStartAt As Integer, ByVal intStringLenghth As Integer) As Boolean
        Dim intEndAt As Integer = intStartAt + (intStringLenghth - 1)
        Dim intTestCharacter As Integer = 0
        Dim intCountDecimalPoints As Integer = 0
        Dim strShowString As String = String.Empty & strTestString.Substring(intStartAt, intStringLenghth)

        boolValidatedOk = True
        If intStringLenghth = 1 Then
            If strTestString.Substring(intStartAt, 1) <> " " Then
                If strTestString.Substring(intStartAt, 1) < "0" Or strTestString.Substring(intStartAt, 1) > "9" Then
                    Return False
                End If
            End If
        End If
        If intStringLenghth > 1 Then
            For intTestCharacter = intStartAt To intEndAt
                If strTestString.Substring(intTestCharacter, 1) = "." Then
                    intCountDecimalPoints = intCountDecimalPoints + 1
                End If
            Next
            If intCountDecimalPoints < 2 Then
                If strTestString.Substring(intStartAt, 1) = "+" Or strTestString.Substring(intStartAt, 1) = "-" Then
                    intStartAt = intStartAt + 1
                End If
                If strTestString.Substring(intEndAt, 1) = "+" Or strTestString.Substring(intEndAt, 1) = "-" Then
                    intEndAt = intEndAt - 1
                End If
                For intTestCharacter = intStartAt To intEndAt
                    If strTestString.Substring(intTestCharacter, 1) <> " " And strTestString.Substring(intTestCharacter, 1) <> "." Then
                        If strTestString.Substring(intTestCharacter, 1) < "0" Or strTestString.Substring(intTestCharacter, 1) > "9" Then
                            'oolValidatedOk = False
                            Return False
                        End If
                    End If
                Next
            End If
        End If
        Return True
    End Function

    Public Function ValidateTheData(ByVal strTestString As String, ByVal arrstrNumericValidation As Array, ByVal arrstrIndicatorValidation As Array) As Boolean
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim strDataError As String = String.Empty
        Dim strMyString As String = String.Empty

        Dim intLengthTestString As Integer = strTestString.Length
        'If strTestString.StartsWith("U1") Or strTestString.StartsWith("U4") Then
        For Each strValidValue As String In arrstrNumericValidation
            If strValidValue.Length > 5 Then
                If strValidValue <> Nothing Or strValidValue <> String.Empty Or strValidValue <> " " Then
                    boolValidatedOk = True
                    intDisplacement = CInt(strValidValue.Substring(0, 3))
                    intTextLength = CInt(strValidValue.Substring(3, 3))
                    strDataError = "Invalid@" & Space(1) & intTextLength.ToString.PadLeft(3) & " +" & Space(1) & intDisplacement.ToString.PadLeft(3)
                    '                    strDataError = "Invalid @" & Space(1) & strValidValue.Substring(0, 3) & " +" & Space(1) & strValidValue.Substring(3, 3)
                    If (intDisplacement + intTextLength) > intLengthTestString Then
                        boolValidatedOk = False
                    End If
                    If boolValidatedOk = True Then
                        strMyString = strTestString.Substring(intDisplacement, intTextLength)
                        boolValidatedOk = IsItNumeric(strTestString, intDisplacement, intTextLength)
                    End If
                    If boolValidatedOk = False Then Exit For
                End If
            End If
        Next
        'End If
        If boolValidatedOk = True Then
            For Each strValidValue As String In arrstrIndicatorValidation
                If strValidValue.Length > 2 Then
                    If strValidValue <> Nothing Or strValidValue <> String.Empty Or strValidValue <> " " Then
                        boolValidatedOk = True
                        intDisplacement = CInt(strValidValue.Substring(0, 3))
                        intTextLength = 1
                        strDataError = "Invalid@" & Space(1) & strValidValue.Substring(0, 3) & " +" & Space(1) & "001"
                        If (intDisplacement + intTextLength) > intLengthTestString Then
                            boolValidatedOk = False
                        End If
                        If boolValidatedOk = True Then
                            strMyString = String.Empty & strTestString.Substring(intDisplacement, intTextLength)
                            boolValidatedOk = IsItAnIndicator(strTestString, intDisplacement)
                        End If
                        If boolValidatedOk = False Then Exit For
                    End If
                End If
            Next
        End If
        If boolValidatedOk = False Then
            OutputProductUpdateError(strTestString, strDataError)
            Return False
        End If
        Return True
    End Function

    Public Function ValidateOCData(ByVal strTestString As String, ByVal arrstrNumericValidation As Array, ByVal OBCHashValue As Decimal) As Boolean
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim strDataError As String = String.Empty
        Dim strMyString As String = String.Empty
        Dim intLengthTestString As Integer = strTestString.Length
        _ExpectingOARecords = False
        'If strTestString.StartsWith("U1") Or strTestString.StartsWith("U4") Then
        For Each strValidValue As String In arrstrNumericValidation
            If strValidValue.Length > 5 Then
                If strValidValue <> Nothing Or strValidValue <> String.Empty Or strValidValue <> " " Then
                    boolValidatedOk = True
                    intDisplacement = CInt(strValidValue.Substring(0, 3))
                    intTextLength = CInt(strValidValue.Substring(3, 3))
                    strDataError = "Invalid@" & Space(1) & strValidValue.Substring(0, 3) & " +" & Space(1) & strValidValue.Substring(3, 3)
                    If (intDisplacement + intTextLength) > intLengthTestString Then
                        boolValidatedOk = False
                    End If
                    If boolValidatedOk = True Then
                        strMyString = String.Empty & strTestString.Substring(intDisplacement, intTextLength)
                        boolValidatedOk = IsItNumeric(strTestString, intDisplacement, intTextLength)
                    End If
                    If boolValidatedOk = False Then
                        Exit For
                    End If
                End If
            End If
        Next
        If boolValidatedOk = False Then
            boolOutputErrorToSthoa = False
            OutputOrderConfirmationError(strTestString, strDataError, OBCHashValue)
            Return False
        End If

        Return True
    End Function

    Public Function ValidateOAData(ByVal strTestString As String, ByVal arrstrNumericValidation As Array, ByVal OBCHashValue As Decimal) As Boolean
        Dim intDisplacement As Integer = 0
        Dim intTextLength As Integer = 0
        Dim strDataError As String = String.Empty
        Dim strMyString As String = String.Empty
        Dim intLengthTestString As Integer = strTestString.Length

        'If strTestString.StartsWith("U1") Or strTestString.StartsWith("U4") Then
        For Each strValidValue As String In arrstrNumericValidation
            If strValidValue.Length > 5 Then
                If strValidValue <> Nothing Or strValidValue <> String.Empty Or strValidValue <> " " Then
                    boolValidatedOk = True
                    intDisplacement = CInt(strValidValue.Substring(0, 3))
                    intTextLength = CInt(strValidValue.Substring(3, 3))
                    strDataError = "Invalid@" & Space(1) & strValidValue.Substring(0, 3) & " +" & Space(1) & strValidValue.Substring(3, 3)
                    If (intDisplacement + intTextLength) > intLengthTestString Then
                        boolValidatedOk = False
                    End If
                    If boolValidatedOk = True Then
                        strMyString = String.Empty & strTestString.Substring(intDisplacement, intTextLength)
                        boolValidatedOk = IsItNumeric(strTestString, intDisplacement, intTextLength)
                    End If
                    If boolValidatedOk = False Then Exit For
                End If
            End If
        Next
        If boolValidatedOk = False Then
            boolOutputErrorToSthoa = False
            OutputOrderConfirmationError(strTestString, strDataError, OBCHashValue)
            Return False
        End If
        Return True
    End Function

    Public Function ValidDate(ByVal strTestString As String, ByVal intStartAt As Integer) As Boolean
        Dim intX As Integer = 0
        boolIsBlankDate = False
        If strTestString.StartsWith("VC") And (strTestString.Substring(intStartAt, 8) = "--/--/--" Or strTestString.Substring(intStartAt, 8).PadRight(8, " "c) = "        ") Then
            boolIsBlankDate = True
            Return True
        End If
        For intX = intStartAt To (intStartAt + 7)
            Dim mytest As String = strTestString.Substring(intX, 1)
            If strTestString.Substring(intX, 3) = "00/" Or strTestString.Substring(intX, 3) = "00." Or strTestString.Substring(intX, 3) = "00:" Then
                Return False
            End If
            If strTestString.Substring(intX, 1) <> "/" And strTestString.Substring(intX, 1) <> "." And strTestString.Substring(intX, 1) <> ":" And (strTestString.Substring(intX, 1) < "0" Or strTestString.Substring(intX, 1) > "9") Then
                Return False
            End If
            If strTestString.Substring(intX, 1) <> " " Then boolIsBlankDate = False
        Next
        'Added 4/11/09
        If (IsDate(strTestString.Substring(intStartAt, 8)) = False) Then Return False
        Return True
    End Function

    Public Function EanValid(ByVal strTestEanNumber As String) As Boolean ' test for a valid EAN(BARCODE)
        Dim longEanNumber As Long
        longEanNumber = CLng(strTestEanNumber)
        Dim intMultiplier As Integer = 3
        Dim intModulus10 As Integer = 0
        Dim intX As Integer = 0
        Dim intY As Integer = 0
        If longEanNumber < 1 Or strTestEanNumber.Length < 8 Then Return False ' Ignore if less than 8 chars or 0
        strTestEanNumber = longEanNumber.ToString.PadLeft(24, "0"c)
        For intX = 0 To 22 ' Multiply each digit by 1 or 3 depending on displacement within number
            intModulus10 = intModulus10 + CInt(CDbl(strTestEanNumber.Substring(intX, 1)) * intMultiplier)
            intMultiplier = intMultiplier - 2
            If intMultiplier < 0 Then
                intMultiplier = 3
            End If
        Next
        intY = CInt(intModulus10.ToString.PadLeft(3, "0"c).Substring(2, 1))
        intX = intY
        intY = 10 - intY
        If intY = 10 Then intY = 0
        Dim intMyCheckDigit As Integer = CInt(strTestEanNumber.Substring(23, 1))
        If (intMyCheckDigit <> intY) Then Return False
        Return True
    End Function ' test for a valid EAN(BARCODE)

    Public Sub FlagAsProcessed()
        Dim intRecordsFlagged As Integer = 0
        Dim intRecordsFlaggedThisRange As Integer = 0
        Dim intY As Integer = 0
        Dim intX As Integer = 0
        Dim intHighSku As Integer = 999999

        If boolResetSales = True Then
            strWorkString = "Resetting Activity in STKMAS Started : " & TimeOfDay.ToString("hh:mm:ss") & Space(1) & "Records Reset :" & Space(1) & intRecordsFlagged.ToString.PadLeft(6, " "c) ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)
            Dim StockMaster As New BOStock.cStock(_Oasys3DB)
            StockMaster.ResetDailyActivity()

            strWorkString = "Resetting Activity in STKMAS Completed : " & TimeOfDay.ToString("hh:mm:ss") '& Space(1) & "Records Reset :" & Space(1) & intRecordsFlagged.ToString.PadLeft(6, " ") ' & vbCrLf
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            OutputSthoLog(strWorkString)
        End If
        If boolFlagPriceChanges = True Then
            Dim Prcchg As New BOStock.cPriceChange(_Oasys3DB)
            Dim ColPc As New List(Of BOStock.cPriceChange)

            Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, Prcchg.ChangeStatus, "U")
            Prcchg.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            Prcchg.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Prcchg.CommedToHo, False)
            Prcchg.SortBy(Prcchg.PartCode.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
            Prcchg.SortBy(Prcchg.EffectiveDate.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)

            ColPc = Prcchg.LoadMatches
            If ColPc.Count > 0 Then
                intRecordsFlagged = 0
                strWorkString = "Flagging Price Changes as Commed Started   : " & TimeOfDay.ToString("hh:mm:ss") & Space(1) & "Records to Reset :" & Space(1) & ColPc.Count.ToString.PadLeft(6, " "c) ' & vbCrLf
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
                ProcessTransmissionsProgress.ProcessName.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                OutputSthoLog(strWorkString)
                For Each pricechange As BOStock.cPriceChange In ColPc
                    Try
                        pricechange.CommedToHo.Value = True
                        pricechange.SaveIfExists()
                        intRecordsFlagged = intRecordsFlagged + 1
                    Catch ex As Exception
                    End Try
                Next
                strWorkString = "Flagging Price Changes as Commed Completed : " & TimeOfDay.ToString("hh:mm:ss") & Space(1) & "Records Reset :" & Space(1) & intRecordsFlagged.ToString.PadLeft(6, " "c) ' & vbCrLf
                ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
                ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
                ProcessTransmissionsProgress.ProcessName.Text = strWorkString
                ProcessTransmissionsProgress.Show()
                OutputSthoLog(strWorkString)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Takes all files received in RTI folder and moves them into COMMS folder
    ''' removing Store Number suffix.
    ''' </summary>
    ''' <param name="FileType"></param>
    ''' <param name="StoreNo"></param>
    ''' <remarks></remarks>
    Public Function PrepareNextRTIFile(ByVal FileType As String, ByVal StoreNo As String, ByRef NextFileName As String, Optional ByVal boolTibpfv As Boolean = False, Optional ByRef strTempFileName As String = "") As Boolean

        Dim FileVersionNo As String = ""
        Dim FileStoreNo As String = ""
        Dim FileTypeLen As Integer = 0
        Dim RTIFileName As String = ""
        Dim OpenThisFile As String = ""
        Dim ImportLine As String = ""

        Dim LastSequence As String = ""
        Dim LastVersionNo As String = ""

        Dim strStoreNo As String = ""
        Dim strVersionNo As String = ""
        Dim strSequenceNo As String = ""


        Dim oTVCSTRBo As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)
        If boolTibpfv = False Then
            'If this is not the Pending file (hpstv) then do as normal HPSTV routine
            If (oTVCSTRBo.LoadControlRecord(FileType) = True) Then
                If (IsNumeric(oTVCSTRBo.PVersion.Value)) Then LastVersionNo = (CInt(oTVCSTRBo.PVersion.Value) + 1).ToString("00")
                If oTVCSTRBo.PVersion.Value = "31" Then LastVersionNo = "01"
                If (IsNumeric(oTVCSTRBo.PSequence.Value)) Then LastSequence = (CInt(oTVCSTRBo.PSequence.Value) + 1).ToString("000000")
                If oTVCSTRBo.PSequence.Value = "999999" Then LastSequence = "000001"
                RTIFileName = strFromRtiPath & "\" & FileType & StoreNo & "." & LastVersionNo
                NextFileName = RTIFileName
                If (File.Exists(RTIFileName) = True) Then
                    Trace.WriteLine("PrepareNextRTIFile-'" & RTIFileName & "'")
                    Dim TxFileReader As New StreamReader(RTIFileName, True) ' Open the file for Verification
                    If (TxFileReader.EndOfStream = False) Then ImportLine = TxFileReader.ReadLine
                    TxFileReader.Close()
                    If ImportLine.StartsWith("HR") Then
                        strStoreNo = ImportLine.Substring(2, 3)
                        strVersionNo = ImportLine.Substring(18, 2)
                        strSequenceNo = ImportLine.Substring(20, 6)
                    Else
                        oTVCSTRBo.RecordVersionRecord(FileType, LastVersionNo, "", "81")
                        Return False 'Skip file as Header Missing
                    End If
                    oTVCSTRBo.LoadVersionRecord(FileType, LastVersionNo)
                    If (LastVersionNo <> strVersionNo) Then
                        oTVCSTRBo.RecordVersionRecord(FileType, LastVersionNo, "", "85")
                        Return False
                    End If
                    If (LastSequence <> strSequenceNo) Then
                        oTVCSTRBo.RecordVersionRecord(FileType, LastVersionNo, "", "86")
                        Return False
                    End If
                    If StoreNo <> strStoreNo Then
                        oTVCSTRBo.RecordVersionRecord(FileType, LastVersionNo, "", "82")
                        Return False 'Skip file as Store Number does not match
                    End If
                    'Retrieve all files in the From RTI folder and move into processing folder
                    FileTypeLen = (strFromRtiPath & "\" & FileType).Length
                    'For each file verify File Type and for this store then move into Processing Folder
                    FileStoreNo = RTIFileName.Substring(FileTypeLen, 3)
                    If ValidateFile(RTIFileName, FileType) Then
                        'Check if file exists in COMMS folder and delete if it does
                        FileVersionNo = RTIFileName.Substring(FileTypeLen + 4, 2)
                        OpenThisFile = strTransmissionsPath & "\" & FileType & FileVersionNo
                        If File.Exists(OpenThisFile) Then
                            My.Computer.FileSystem.DeleteFile(OpenThisFile)
                        End If
                        My.Computer.FileSystem.CopyFile(RTIFileName, OpenThisFile)
                        'Once file copied then check length matches
                        Dim intOpenFileLength As Integer = CInt(My.Computer.FileSystem.GetFileInfo(RTIFileName).Length)
                        Dim intCopyToFileLength As Integer = CInt(My.Computer.FileSystem.GetFileInfo(OpenThisFile).Length)
                        If intCopyToFileLength = intOpenFileLength Then
                            My.Computer.FileSystem.DeleteFile(RTIFileName)
                        End If
                        strWorkString = "Processing Transmissions In - " & FileType & " : " & OpenThisFile & " Copied to : " & RTIFileName & " : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
                        UpdateProgress(String.Empty, String.Empty, strWorkString)
                        OutputSthoLog(strWorkString)
                        Return True
                    End If
                End If 'found file is correct file type
            Else
                Return False 'next file in sequence was not found
            End If
        Else
            'This is a pending hpstv file so we need to make a temp directory, we will add the 
            'supplier changes that are still in the future into this file which will become the 
            'latest hpstv file after processing.
            
            RTIFileName = strTempFileName & FileType
            If Directory.Exists(strTempFileName & "TEMP") = False Then
                'Temp directory does not exist, create it
                Directory.CreateDirectory(strTempFileName & "TEMP")
            End If
            strTempFileName += "TEMP\HPSTV.Tmp"

            NextFileName = RTIFileName
            If File.Exists(RTIFileName) Then
                If File.Exists(strTempFileName) Then
                    File.Delete(strTempFileName)
                End If
                My.Computer.FileSystem.CopyFile(RTIFileName, strTempFileName)
                Return True
            End If
        End If
        Return False

    End Function

    Public Function ValidateFile(ByVal ImportFileName As String, ByVal FileType As String) As Boolean

        'Next step through Processing Folder and verify matched files are valid
        Dim strStoreNo As String
        Dim strVersionNo As String
        Dim strSequenceNo As String
        Dim RecordType As String = ""
        Dim RecordTypePos As Integer
        Dim ErrorFound As Boolean
        Dim TrailerFound As Boolean = False
        Dim oTVCSTRBo As New BOStoreTransValCtl.cStoreTransValCtl(_Oasys3DB)

        Dim ActualRecCounts(15) As Integer
        Dim ActualRecTotals(15) As Decimal
        Dim ActualRecCodes(15) As String
        Dim ActualRecCodesFlag(15) As String

        Dim FNameStartPos As Integer = strTvcPath.Length
        Dim RequiredFileNameLength As Integer = strTvcPath.Length + FileType.Length + 2
        Dim ImportLine As String = ""

        For TypePos = 0 To 15 Step 1
            ActualRecCodes(TypePos) = String.Empty
        Next TypePos

        Trace.WriteLine("ValidateFile-'" & ImportFileName & "'")
        Dim TxFileReader As New StreamReader(ImportFileName, True) ' Open the file for Verification
        If (TxFileReader.EndOfStream = False) Then ImportLine = TxFileReader.ReadLine
        'Process Header - File has already been validated to contain Header as first Line
        strStoreNo = ImportLine.Substring(2, 3)
        strVersionNo = ImportLine.Substring(18, 2)
        strSequenceNo = ImportLine.Substring(20, 6)
        'Step through file and accumalate Record Counts and Totals
        While Not TxFileReader.EndOfStream
            ImportLine = TxFileReader.ReadLine
            RecordType = String.Empty
            If (ImportLine.Length >= 2) Then
                RecordType = ImportLine.Substring(0, 2).ToUpper
                UpdateProgress(ImportLine, intHeadOfficeNumberOfRecords.ToString & " - " & ImportFileName, "")
                'If not trailier then add totals
                If (RecordType <> "TR") And (RecordType.Length > 0) Then
                    'step through record types already found and identify match
                    For RecordTypePos = 1 To 15
                        If (RecordType = ActualRecCodes(RecordTypePos)) Then
                            'Update totals if already exists
                            ActualRecCounts(RecordTypePos) += 1
                            If IsNumeric(ImportLine.Substring(10, 12)) Then ActualRecTotals(RecordTypePos) += CDec(ImportLine.Substring(10, 12))
                            Exit For
                        Else
                            If (ActualRecCodes(RecordTypePos) = "") Then
                                'Create totals if Blank found - as total has not been created yet
                                ActualRecCodes(RecordTypePos) = RecordType
                                ActualRecCounts(RecordTypePos) = 1
                                If IsNumeric(ImportLine.Substring(10, 12)) Then ActualRecTotals(RecordTypePos) = CDec(ImportLine.Substring(10, 12))
                                Exit For
                            End If
                        End If
                    Next
                Else
                    TrailerFound = True
                    Dim TrailerTypeCounts(15) As Integer
                    Dim TrailerTypeTotals(15) As Decimal
                    Dim TrailerTypeCodes(15) As String
                    Dim TypePos As Integer = 0
                    Dim TrailerTypePos As Integer = 0
                    For TypePos = 0 To 15 Step 1
                        TrailerTypeCodes(TypePos) = String.Empty
                    Next TypePos
                    TypePos = 0

                    ImportLine = ImportLine & " "
                    While ((TypePos * 21) + 42) < ImportLine.Length
                        TrailerTypeCodes(TypePos) = ImportLine.Substring(32 + TypePos * 21, 2)
                        TrailerTypeCounts(TypePos) = CInt(ImportLine.Substring(34 + TypePos * 21, 7))
                        TrailerTypeTotals(TypePos) = CDec(ImportLine.Substring(41 + TypePos * 21, 12))
                        TypePos += 1
                    End While
                    ErrorFound = False
                    ActualRecCodes.CopyTo(ActualRecCodesFlag, 0)
                    For TypePos = 1 To 15 Step 1
                        If ActualRecCodesFlag(TypePos).Trim <> String.Empty Then
                            RecordType = ActualRecCodes(TypePos)
                            If (RecordType = "") Then Exit For 'validated all counts so skip to next step
                            For TrailerTypePos = 0 To 15 Step 1
                                If (RecordType = TrailerTypeCodes(TrailerTypePos)) Then
                                    If (TrailerTypeCounts(TrailerTypePos) <> ActualRecCounts(TypePos)) Then
                                        '87 Count
                                        oTVCSTRBo.RecordVersionRecord(FileType, strVersionNo, strSequenceNo, "87")
                                        TxFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
                                        Return False
                                    End If
                                    If (TrailerTypeTotals(TrailerTypePos) <> ActualRecTotals(TypePos)) Then
                                        '88 totals
                                        oTVCSTRBo.RecordVersionRecord(FileType, strVersionNo, strSequenceNo, "88")
                                        TxFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
                                        Return False
                                    End If
                                    TrailerTypeCodes(TrailerTypePos) = "" 'remove Record type so that it is flagged as processed
                                    ActualRecCodesFlag(TypePos) = ""
                                    Exit For 'Matching Trailer Totals
                                End If
                            Next
                        Else
                            Exit For
                        End If
                    Next TypePos
                    For TypePos = 0 To 15 Step 1
                        If ActualRecCodesFlag(TypePos).Trim <> String.Empty Or (TrailerTypeCodes(TypePos).Trim <> String.Empty) Then
                            '89 type not recognised
                            TxFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
                            oTVCSTRBo.RecordVersionRecord(FileType, strVersionNo, strSequenceNo, "89")
                            Return False
                        End If
                    Next TypePos
                End If 'processing Trailer record
            End If 'valid line found
        End While
        TxFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
        If TrailerFound = False Then
            oTVCSTRBo.RecordVersionRecord(FileType, strVersionNo, strSequenceNo, "81")
            Return False 'Skip file as Header Missing
        End If

        'If ok the update aseq+aver_flag=1 and ecod=''
        oTVCSTRBo.RecordVersionRecord(FileType, strVersionNo, strSequenceNo, "")
        oTVCSTRBo.RecordActualRecordCounts(FileType, strVersionNo, ActualRecCodes, ActualRecCounts, ActualRecTotals)
        Return True
        'after processing the set pseq+pver
        'Date and time - now()

    End Function

    Private Function CreateHPSTITable() As Data.DataTable

        Dim HPSTITable As New Data.DataTable

        HPSTITable.Columns.Add(New Data.DataColumn("DeliveryDate"))
        HPSTITable.Columns.Add(New Data.DataColumn("ContainerNo"))
        HPSTITable.Columns.Add(New Data.DataColumn("ContainerType"))
        HPSTITable.Columns.Add(New Data.DataColumn("NoLines", GetType(System.Int32)))
        HPSTITable.Columns.Add(New Data.DataColumn("ParentContainer"))
        HPSTITable.Columns.Add(New Data.DataColumn("Received"))
        HPSTITable.Columns.Add(New Data.DataColumn("Value", GetType(System.Decimal)))

        Return HPSTITable

    End Function

    Private Function CreateHPSTOTable() As Data.DataTable

        Dim HPSTOTable As New Data.DataTable

        HPSTOTable.Columns.Add(New Data.DataColumn("OrderNo"))
        HPSTOTable.Columns.Add(New Data.DataColumn("Supplier"))
        HPSTOTable.Columns.Add(New Data.DataColumn("OrderDate"))
        HPSTOTable.Columns.Add(New Data.DataColumn("DueDate"))
        HPSTOTable.Columns.Add(New Data.DataColumn("OrderStatus"))
        HPSTOTable.Columns.Add(New Data.DataColumn("SKU"))
        HPSTOTable.Columns.Add(New Data.DataColumn("OldQty"))
        HPSTOTable.Columns.Add(New Data.DataColumn("NewQty"))
        HPSTOTable.Columns.Add(New Data.DataColumn("Reason"))
        Return HPSTOTable

    End Function

End Module