﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing

Public Class ReportPrint
    Private _report As CompositeLink
    Private _storeIdName As String = String.Empty
    Private _title As String = String.Empty
    Private _title2 As String = String.Empty
    Private _title3 As String = String.Empty

    Public Sub New(ByVal grid As GridControl, ByVal title As String, ByVal Title2 As String, ByVal storeIdName As String, ByVal Title3 As String, ByVal PrintFooter As Boolean)
        _title = title
        _title2 = Title2
        _title3 = Title3
        _storeIdName = storeIdName

        'set up supplier copy report
        _report = New CompositeLink(New PrintingSystem)
        _report.Margins.Bottom = 70
        _report.Margins.Top = 50
        _report.Margins.Left = 50
        _report.Margins.Right = 50
        _report.Landscape = (Title3 <> String.Empty)
        AddHandler _report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea
        If (PrintFooter) Then AddHandler _report.CreateDetailFooterArea, AddressOf ReportTitle_CreateReportFooterArea

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = grid

        'populate the collection of links in the composite link in order and show preview
        _report.Links.Add(reportTitle)
        _report.Links.Add(reportGrid)

    End Sub

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw store number and name
        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _storeIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:MM/dd/yyyy HH:mm}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'draw report title
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Tahoma", 14, FontStyle.Bold)

        Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
        e.Graph.DrawString(_title, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Tahoma", 12, FontStyle.Bold)

        rec = New RectangleF(0, 30, e.Graph.ClientPageSize.Width, 30)
        e.Graph.DrawString(_title2, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        Dim PrintLines As String() = _title3.Split(CChar(vbCrLf))
        Dim LeftIndex As String = String.Empty
        Dim RightIndex As String = String.Empty
        Dim LinePos As Integer = 0
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font("Tahoma", 10, FontStyle.Regular)
        For Each Printline As String In PrintLines
            If Printline.IndexOf(vbTab) = -1 Then Printline += vbTab            
            LeftIndex += Printline.Substring(0, Printline.IndexOf(vbTab)) & Environment.NewLine
            RightIndex += Printline.Substring(Printline.IndexOf(vbTab) + 1) & Environment.NewLine
            LinePos += 1
        Next
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font("Tahoma", 10, FontStyle.Regular)
        rec = New RectangleF(0, 50, e.Graph.ClientPageSize.Width / 2 - 1, 16 * LinePos)
        e.Graph.DrawString(LeftIndex, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)
        rec = New RectangleF(e.Graph.ClientPageSize.Width / 2, 50, e.Graph.ClientPageSize.Width / 2 - 1, 16 * LinePos)
        e.Graph.DrawString(RightIndex, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub ReportTitle_CreateReportFooterArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.InnerPageFooter
        'draw report footer
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font("Tahoma", 10, FontStyle.Regular)

        Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
        e.Graph.DrawString(" Received by      : ..................................... Print Name: ....................................  Date: ............... ", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font("Tahoma", 10, FontStyle.Regular)

        rec = New RectangleF(0, 60, e.Graph.ClientPageSize.Width, 30)
        e.Graph.DrawString("Drivers Signature: ..................................... Print Name: ....................................  Date: ............... ", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Public Sub ShowPreviewDialog()

        '_report.ShowPreviewDialog()
        _report.Print(DefaultPrinterName)

    End Sub

    Public Shared Function DefaultPrinterName() As String
        Dim oPS As New System.Drawing.Printing.PrinterSettings

        Try
            DefaultPrinterName = oPS.PrinterName
        Catch ex As System.Exception
            DefaultPrinterName = ""
        Finally
            oPS = Nothing
        End Try
    End Function

End Class
