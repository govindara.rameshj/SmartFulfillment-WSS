﻿Imports System.Text
Imports System.Data
Imports BOBanking

Public Class OutputBanking
    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _sthoaFileName As String = String.Empty

    Public Sub New(ByVal sthoaFileName As String)
        _sthoaFileName = sthoaFileName
    End Sub


    Public Sub Process(ByVal periodDate As Date, doSafeClosedCheck As Boolean)

        '''' <summary>
        '''' Added to inlcude vision data to the STHOA file 
        '''' </summary>

        Dim strSTHORecTypeVision As String = String.Empty
        Dim strSTHORecDateVision As String = String.Empty
        Dim strSTHORecHashValueVision As String = String.Empty
        Dim intMaximumSthoOutputLengthVision As Integer = 0
        Dim typeADDepositDate As String = String.Empty
        Dim typeADTillNumber As String = String.Empty
        Dim typeADTransactionNumber As String = String.Empty
        Dim typeADOrderValue As String = String.Empty
        Dim typeADOrderNumber As String = String.Empty
        Dim bankQryVision As StringBuilder
        Dim bnkTableVision As DataTable
        Dim cDateVal As String = String.Empty
        Dim strSthoaTextVision As String = String.Empty
        '''' Vision changes ends
        Try
            OutputBankingProcessName("Start", True)

            'load retail options
            Dim retOptions As New BOSystem.cRetailOptions(_oasys3Db)
            retOptions.LoadRetailOptions()
            OutputBankingProcessName("RETOPT loaded", True)

            'get values
            Dim periodId As Integer = GetPeriodId(periodDate)
            periodDate = GetPeriodDate(periodId)

            'get current safe records for output to 9001 account into index 1 of safeBalances array
            'include sealed (ie unreleased and still in safe) float and banking bags
            'include floats issued but not returned from cashier/till records (dependent on accountability type)
            Dim safeBalances(20) As Decimal
            safeBalances(1) += GetSafeValue(periodId)
            safeBalances(1) += GetSealedBankingFloatValue(periodId)
            safeBalances(1) += GetFloatsInUse(retOptions.AccountabilityType.Value)


            Dim safeDate As Date = GetSafeDate(periodId, doSafeClosedCheck)


            'load system account numbers
            Dim miscInCodes(19) As String
            Dim miscOutCodes(19) As String
            Using sysAccountCodes As New BOBanking.cSystemAccountCodes(_oasys3Db)
                sysAccountCodes.Load()
                miscInCodes = sysAccountCodes.GetMiscInCodes
                miscOutCodes = sysAccountCodes.GetMiscOutCodes
            End Using

            'get released banking bag values
            Dim cashBankingValues(5) As Decimal
            Dim cashBankingSlips(5) As String
            Dim bankingValues(20) As Decimal
            Dim tenderBankingTotals As Decimal() = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            'get banking bags that have been released this period (ie collected - not returned to safe) and add to banking
            Using safeBag As New BOBanking.cSafeBags(_oasys3Db)
                For Each bag As BOBanking.cSafeBags In safeBag.GetBankingBagsForPeriod(periodId)
                    OutputBankingProgress("Checking BANKING bag : " & bag.ID.Value)

                    'add denom values to banking output
                    For Each denom As BOBanking.cSafeBagsDenoms In bag.Denoms
                        Select Case denom.TenderID.Value
                            Case 1
                                Dim DenomPos As Integer = 0
                                Dim DenomSlipPos As Integer = 0
                                Select Case (denom.ID.Value)
                                    Case 100, 50
                                        DenomPos = 4
                                        DenomSlipPos = 4
                                    Case 20
                                        DenomPos = 3
                                        DenomSlipPos = 3
                                    Case 10
                                        DenomPos = 2
                                        DenomSlipPos = 2
                                    Case 5
                                        DenomPos = 1
                                        DenomSlipPos = 1
                                End Select

                                If DenomPos > 0 Then
                                    cashBankingValues(DenomPos) += denom.Value.Value
                                    cashBankingSlips(DenomSlipPos) = denom.SlipNumber.Value
                                End If

                            Case Else
                                bankingValues(denom.TenderID.Value) += denom.Value.Value
                        End Select

                        'Fudged to force EFT and Vouchers to be logged into these Old Accounts 3/7/09 M.Milne
                        If (denom.TenderID.Value = 3) Or (denom.TenderID.Value = 8) Then
                            OutputBankingProgress("Adding tender ID Value to banking totals: " & denom.TenderID.Value & Space(1) & denom.Value.Value)
                            tenderBankingTotals(denom.TenderID.Value) += denom.Value.Value
                        End If
                    Next

                    ' Commented to enable repeated upload of Collected bags
                    ''set banking bag state and save 14/11/09 DH
                    'Select Case bag.State.Value
                    '    Case BOBanking.BagStates.ReleasedNotPrepared : bag.State.Value = BOBanking.BagStates.Released
                    '    Case BOBanking.BagStates.Sealed : bag.State.Value = BOBanking.BagStates.PreparedNotReleased
                    'End Select
                    'bag.SaveIfExists()
                Next
            End Using


            'get pickup values
            Dim floatsReturned As Decimal = GetFloatsReturned(periodId, retOptions.AccountabilityType.Value)
            Dim pickupValues() As Decimal = GetPickupValues(periodId, floatsReturned)

            Const NumberOfTenders = 13
            Dim shortsValue As Decimal = 0
            Dim discountValue As Decimal = 0
            Dim spSValue As Decimal() = New Decimal(NumberOfTenders) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Dim apValue As Decimal() = New Decimal(20) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Dim miva As Decimal() = New Decimal(20) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Dim mova As Decimal() = New Decimal(20) {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            Using cashiers As New BOBanking.cCashBalCashier(_oasys3Db)
                cashiers.LoadCashiers(periodId)

                For Each cashier As BOBanking.cCashBalCashier In cashiers.Cashiers
                    OutputBankingProgress("Checking CASHIER :" & Space(1) & cashier.CashierID.Value.ToString.PadLeft(3, "0"c))

                    discountValue += cashier.DiscountAmount.Value
                    Dim pickup As Decimal = 0
                    For Each cashierTender As BOBanking.cCashBalCashierTen In cashier.TenderTypes
                        If cashierTender.ID.Value < 21 Then
                            apValue(cashierTender.ID.Value) += cashierTender.PickUp.Value
                            If cashierTender.ID.Value <= NumberOfTenders Then
                                spSValue(cashierTender.ID.Value) += cashierTender.Amount.Value
                            Else
                                Throw New ApplicationException(String.Format("Unknown TenderType=[{0}]", cashierTender.ID.Value))
                            End If
                        Else
                            'Assume TenderID=99 - Cash Change
                            apValue(1) += cashierTender.PickUp.Value
                            spSValue(1) += cashierTender.Amount.Value
                        End If
                        pickup += cashierTender.PickUp.Value
                    Next

                    shortsValue += (cashier.GrossSalesAmount.Value - pickup + cashier.FloatReturned.Value)
                    For index As Integer = 1 To 20
                        miva(index) -= cashier.MiscIncomeValue(index).Value
                        mova(index) += cashier.MisOutValue(index).Value
                    Next
                Next
            End Using


            Dim pickupAccountNumbers As String() = {"8415", "8401", "8402", "8404", "8406", "8410", "8411", "8413", "8409", "8440", "0", "8498", "8499"}
            Dim decHashValue As Decimal
            Dim strSTHORecType As String = String.Empty
            Dim strSTHORecDate As String = String.Empty
            Dim strSTHORecHashValue As String = String.Empty
            Dim intMaximumSthoOutputLength As Integer = 0

            Dim typeA8AccountNumber As String = String.Empty
            Dim typeA8Value As String = String.Empty
            Dim typeA8Value1 As String = String.Empty

            Dim strSthoaText As String = String.Empty
            Dim intRecordsOutput As Integer = 0

            For index = 12 To 0 Step -1
                If CInt(pickupAccountNumbers(index)) <> 0 Then
                    typeA8AccountNumber = String.Empty
                    typeA8Value = String.Empty
                    FormatIntToString(CInt(pickupAccountNumbers(index)), typeA8AccountNumber, 4, "0"c)
                    FormatDecToString(spSValue(index + 1), typeA8Value, 10, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = spSValue(index + 1)

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If

                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next

            Dim intWorkInteger As Integer = 3009
            If retOptions.Store.ToString.PadLeft(3, "0"c).StartsWith("5") Then
                intWorkInteger = 3002
            End If

            typeA8AccountNumber = String.Empty
            typeA8Value = String.Empty
            typeA8Value1 = String.Empty
            FormatIntToString(intWorkInteger, typeA8AccountNumber, 4, "0"c)
            FormatDecToString(discountValue, typeA8Value, 10, " "c, "0.00")
            FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
            decHashValue = discountValue

            SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
            intRecordsOutput = intRecordsOutput + 1
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(_sthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If


            Dim arrstrActualPickupAccountNumbers As String() = {" ", "8430", "8431", "8432", "8433", "8434", "8435"}
            For index = 6 To 1 Step -1
                FormatIntToString(CInt(arrstrActualPickupAccountNumbers(index)), typeA8AccountNumber, 4, "0"c)
                FormatDecToString(pickupValues(index), typeA8Value, 10, " "c, "0.00")
                FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                decHashValue = pickupValues(index)

                SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                If strSthoaText <> String.Empty Then
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                intRecordsOutput = intRecordsOutput + 1
                If strSthoaText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(_sthoaFileName, strSthoaText)
                    strSthoaText = String.Empty
                End If
            Next


            intWorkInteger = 6509
            FormatIntToString(intWorkInteger, typeA8AccountNumber, 4, "0"c)
            FormatDecToString(shortsValue, typeA8Value, 10, " "c, "0.00")
            FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
            decHashValue = shortsValue
            SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)

            If strSthoaText <> String.Empty Then
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
            End If

            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
            intRecordsOutput = intRecordsOutput + 1
            If strSthoaText.Length > intMaximumSthoOutputLength Then
                PutSthoToDisc(_sthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If


            Dim arrstrPickupAccountNumbers As String() = New String(20) {" ", "0000", "8416", "8417", "8418", "8421", "8419", "8422", "8423", "8424", "8425", "8440", "8426", "8427", "8428", "8429", "8436", "8438", "0000", "8442", "8443"}
            Dim TenderIDAccountNo() As Integer = {0, 1, 2, 3, 0, 7, 4, 0, 0, 5, 6, 0, 0, 0, 8, 18, 9, 10, 0, 12, 13}
            For index = 20 To 1 Step -1
                If (arrstrPickupAccountNumbers(index) <> "0000") Then
                    FormatIntToString(CInt(arrstrPickupAccountNumbers(index)), typeA8AccountNumber, 4, "0"c)
                    FormatDecToString(apValue(TenderIDAccountNo(index)), typeA8Value, 10, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = apValue(TenderIDAccountNo(index))

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If

                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next

            For index = 20 To 1 Step -1
                If miva(index) <> 0 Then
                    typeA8AccountNumber = miscInCodes(index - 1).ToString.PadLeft(4, "0"c)
                    FormatDecToString(miva(index), typeA8Value, 11, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = miva(index)

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If

                If mova(index) <> 0 Then
                    mova(index) = Math.Abs(mova(index))
                    typeA8AccountNumber = miscOutCodes(index - 1).ToString.PadLeft(4, "0"c)
                    FormatDecToString(mova(index), typeA8Value, 11, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = mova(index)
                    'intCountC = intCountC + 1
                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next

            'Output Sales Values Records for Sales by Category Account "12XX"
            Dim dvTotals As New DataView
            Dim SalesValues As Decimal() = New Decimal(99) {}
            Using saleTotHeader As New BOSales.cSalesHeader(_oasys3Db)
                saleTotHeader.LoadTransactionsForFeedPeriod(periodId)
                dvTotals = saleTotHeader.Headers.LineGroupTotals

                If dvTotals IsNot Nothing Then
                    For Each drv As Data.DataRowView In dvTotals
                        SalesValues(CInt(drv("CategoryNumber").ToString)) = CDec(drv("TotalValue")) * -1
                    Next
                End If
            End Using

            Dim visionHeader As New BOVisionSales.cPVTotals(_oasys3Db)
            visionHeader.LoadValidTransactions(periodDate, True)
            dvTotals = visionHeader.LineGroupTotals
            If dvTotals IsNot Nothing Then
                For Each drv As Data.DataRowView In dvTotals
                    SalesValues(CInt(drv("CategoryNumber").ToString)) = CDec(drv("TotalValue")) * -1
                Next
            End If


            For index As Integer = 1 To 98
                If SalesValues(index) <> 0 Then
                    typeA8AccountNumber = "12" & index.ToString.PadLeft(2, "0"c)
                    FormatDecToString(SalesValues(index), typeA8Value, 11, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = SalesValues(index)

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If

                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next


            'to be banked by denominations should be done here
            Dim arrstrCashBankingAccountNumbers As String() = {" ", "8211", "8212", "8213", "8214"}
            For intI = 1 To 4
                If cashBankingValues(intI) <> 0.0 Then
                    typeA8AccountNumber = arrstrCashBankingAccountNumbers(5 - intI).ToString.PadLeft(4, "0"c)
                    FormatDecToString(cashBankingValues(intI), typeA8Value, 11, " "c, "0.00")

                    Dim dec As Decimal = CDec(cashBankingSlips(intI).ToString.PadRight(6, "0"c).Substring(0, 6)) / 100
                    FormatDecToString(dec, typeA8Value1, 11, " "c, "0.00")
                    decHashValue = cashBankingValues(intI)

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If

                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next intI


            Dim arrstrBankingAccountNumbers As String() = New String(20) {" ", "0000", "8221", "8111", "8112", "8141", "8121", "8142", "8143", "8144", "8251", "8441", "8131", "8132", "8231", "8232", "8323", "8324", "0000", "8198", "8199"}
            For index = 20 To 1 Step -1
                If CInt(arrstrBankingAccountNumbers(index)) <> 0 Then
                    FormatIntToString(CInt(arrstrBankingAccountNumbers(index)), typeA8AccountNumber, 4, "0"c)
                    FormatDecToString(bankingValues(TenderIDAccountNo(index)), typeA8Value, 10, " "c, "0.00")
                    FormatDecToString(0, typeA8Value1, 11, " "c, "0000.00")
                    decHashValue = bankingValues(TenderIDAccountNo(index))

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next index


            Dim arrstrTenderBankingTotalsAccountNumbers As String() = New String(10) {" ", "0000", "0000", "8110", "8120", "8140", "0000", "8130", "8230", "0000", "0000"}
            '            Dim arrstrTenderBankingTotalsAccountNumbers As String() = New String(10) {" ", "0000", "0000", "8110", "0000", "0000", "0000", "0000", "8230", "0000", "0000"}
            For index = 10 To 1 Step -1
                If arrstrTenderBankingTotalsAccountNumbers(index) <> "0000" Then
                    FormatIntToString(CInt(arrstrTenderBankingTotalsAccountNumbers(index)), typeA8AccountNumber, 4, "0"c)
                    FormatDecToString(tenderBankingTotals(index), typeA8Value, 10, " "c, "0.00")
                    typeA8Value1 = String.Empty
                    decHashValue = tenderBankingTotals(index)

                    SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                    If strSthoaText <> String.Empty Then
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                    End If

                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                    intRecordsOutput = intRecordsOutput + 1
                    If strSthoaText.Length > intMaximumSthoOutputLength Then
                        PutSthoToDisc(_sthoaFileName, strSthoaText)
                        strSthoaText = String.Empty
                    End If
                End If
            Next index


            For index = 1 To 20 Step 1
                typeA8AccountNumber = "90" & index.ToString.PadLeft(2, "0"c)
                FormatDecToString(safeBalances(index), typeA8Value, 11, " "c, "0.00")
                FormatDecToString(0, typeA8Value1, 11, " "c, "0.00")
                decHashValue = safeBalances(index)
                SetupHashOut("A8", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                If strSthoaText <> String.Empty Then
                    strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeA8AccountNumber & typeA8Value & typeA8Value1 '
                intRecordsOutput = intRecordsOutput + 1
                If strSthoaText.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(_sthoaFileName, strSthoaText)
                    strSthoaText = String.Empty
                End If
            Next

            If strSthoaText.Length > 0 Then
                PutSthoToDisc(_sthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If

            'Output AH Records for Sales by Category
            Using saleHeader As New BOSales.cSalesHeader(_oasys3Db)
                saleHeader.LoadTransactionsForFeedPeriod(periodId)

                Dim dv As DataView = saleHeader.Headers.CategorySales
                dv = visionHeader.CategorySales(dv)

                If dv IsNot Nothing Then
                    dv.Sort = "number"
                    Dim typeAHCatNo As String
                    For Each drv As Data.DataRowView In dv
                        typeAHCatNo = drv("number").ToString.PadLeft(6, "0"c)
                        FormatDecToString(CDec(drv("value")) * -1, typeA8Value, 11, " "c, "0.00")
                        decHashValue = CDec(drv("value")) * -1
                        SetupHashOut("AH", decHashValue, safeDate, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
                        If strSthoaText <> String.Empty Then
                            strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & vbCrLf
                        End If
                        strSthoaText = strSthoaText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & typeAHCatNo & typeA8Value
                        intRecordsOutput = intRecordsOutput + 1
                    Next
                End If
            End Using

            If strSthoaText.Length > 0 Then
                PutSthoToDisc(_sthoaFileName, strSthoaText)
                strSthoaText = String.Empty
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Dhanesh Ramachandran
            ' Date        : 22/10/2010
            ' Change      : Vision Sales
            ' Notes       : Added to inlcude vision data to the STHOA file
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            '''' <summary>
            '''' Added to inlcude vision data to the STHOA file 
            '''' </summary>

            cDateVal = periodDate.ToString().Substring(6, 4) + "-"
            cDateVal += periodDate.ToString().Substring(3, 2) + "-"
            cDateVal += periodDate.ToString().Substring(0, 2)
            bankQryVision = New StringBuilder()
            bankQryVision.Append("select dt.DATE1 , dt.TILL, dt.[TRAN], dt.TOTL,dt.DOCN   from DLTOTS dt ")
            bankQryVision.Append(" where ((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20) and (dt.DATE1 = ")
            bankQryVision.Append("'" & cDateVal & "'")
            bankQryVision.Append("))")
            bnkTableVision = _oasys3Db.ExecuteSql(bankQryVision.ToString()).Tables(0)

            For cCashIndx As Integer = 0 To bnkTableVision.Rows.Count - 1

                typeADDepositDate = bnkTableVision.Rows(cCashIndx)(0).ToString()
                typeADTillNumber = bnkTableVision.Rows(cCashIndx)(1).ToString()
                typeADTransactionNumber = bnkTableVision.Rows(cCashIndx)(2).ToString()
                typeADOrderValue = bnkTableVision.Rows(cCashIndx)(3).ToString()
                typeADOrderNumber = bnkTableVision.Rows(cCashIndx)(4).ToString()

                FormatIntToString(CInt(typeADTransactionNumber), typeADTransactionNumber, 4, "0"c)
                FormatDecToString(CDec(typeADOrderValue), typeADOrderValue, 8, " "c, "0.00")
                FormatIntToString(CInt(typeADOrderNumber), typeADOrderNumber, 6, "0"c)
                decHashValue = CDec(typeADOrderValue)

                SetupHashOut("AD", decHashValue, CDate(typeADDepositDate), strSTHORecTypeVision, strSTHORecDateVision, strSTHORecHashValueVision)

                If strSthoaTextVision <> String.Empty Then
                    strSthoaTextVision = strSthoaTextVision.ToString.TrimEnd(" "c) & vbCrLf
                End If
                strSthoaTextVision = strSthoaTextVision.ToString.TrimEnd(" "c) & strSTHORecTypeVision & strSTHORecDateVision & strSTHORecHashValueVision & typeADTillNumber & typeADTransactionNumber & typeADOrderValue & typeADOrderNumber '
                intRecordsOutput = intRecordsOutput + 1
                If strSthoaTextVision.Length > intMaximumSthoOutputLength Then
                    PutSthoToDisc(_sthoaFileName, strSthoaTextVision)
                    strSthoaTextVision = String.Empty
                End If
            Next
            If strSthoaTextVision.Length > 0 Then
                PutSthoToDisc(_sthoaFileName, strSthoaTextVision)
                strSthoaTextVision = String.Empty
            End If
            '''' Vision change ends

            OutputBankingProcessName("Ended - Records Output to " & _sthoaFileName & ": " & intRecordsOutput.ToString("#####0").PadLeft(6, " "c), True)

        Catch ex As Exception
            Trace.WriteLine(ex.Message)
            OutputSthoLog(ex.Message)
        End Try

    End Sub

    Public Function GetDefaultCurrencyId() As String

        Using sysCurrency As New BOSystemCurrency.cSystemCurrency(_oasys3Db)
            Return sysCurrency.GetDefaultCurrencyID
        End Using

    End Function

    Public Function GetDateFromUserPrompt() As Date

        Trace.WriteLine("Requesting Date for banking")
        Dim input As String = InputBox(My.Resources.PromptEnterDate, "Enter Banking Date", Today.ToString("dd/MM/yy"))
        OutputBankingProcessName("Entered Date(" & input & ")")

        'check if entered date is date value else end process
        Dim inputDate As Date
        If Date.TryParse(input, inputDate) Then
            Return inputDate
        Else
            End
        End If

    End Function

    Public Function GetPeriodId(ByVal periodDate As Date) As Integer

        Dim id As Integer = 0
        Using sysPeriod As New BOSystem.cSystemPeriods(_oasys3Db)
            sysPeriod.Load(periodDate)
            id = sysPeriod.ID.Value
        End Using

        Return id

    End Function

    Public Function GetPeriodDate(ByVal periodId As Integer) As Date

        Dim datePriod As Date
        Using sysPeriod As New BOSystem.cSystemPeriods(_oasys3Db)
            datePriod = sysPeriod.GetStartDate(periodId)
        End Using
        Return datePriod

    End Function

    Public Function GetSafeDate(ByVal periodId As Integer, doSafeClosedCheck As Boolean) As Date

        'load safe bag for this period id
        Using safe As New BOBanking.cSafe(_oasys3Db)
            safe.Load(periodId)

            'check that a safe entry exists
            Select Case safe.PeriodID.Value
                Case 0
                    OutputBankingProcessName("Safe NOT loaded", True)
                    MessageBox.Show(My.Resources.NoSafeAndExit, "Processing STHOA Banking Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    End

                Case Else
                    OutputBankingProcessName("Safe loaded", True)
            End Select

            'added 11/6/09 - check if Safe Bag has been close, else exit
            If (doSafeClosedCheck AndAlso safe.IsClosed.Value = False) Then
                OutputBankingProcessName("Safe NOT closed", True)
                MessageBox.Show("Process Banking - Pickups not completed for " & safe.PeriodDate.Value.ToString("dd/MM/yy") & Environment.NewLine & "System Exiting.", "Processing STHOA Banking Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End
            End If

            Return safe.PeriodDate.Value
        End Using

    End Function

    Public Function GetFloatsInUse(ByVal accountType As String) As Decimal

        'include floats issued but not returned from cashier/till records (dependent on accountability type)
        Select Case accountType
            Case AccountabilityTypes.Cashier
                Using cashier As New BOBanking.cCashBalCashier(_oasys3Db)
                    Return cashier.GetFloatsInUse
                End Using

            Case AccountabilityTypes.Till
                Using till As New BOBanking.cCashBalTill(_oasys3Db)
                    Return till.GetFloatsInUse
                End Using
        End Select

    End Function

    Public Function GetFloatsReturned(ByVal periodId As Integer, ByVal accountType As String) As Decimal

        'get default currency id
        Dim defaultCurrencyId As String = GetDefaultCurrencyId()
        Dim floatsIssued As Decimal = 0

        'include floats issued but not returned from cashier/till records (dependent on accountability type)
        Select Case accountType
            Case AccountabilityTypes.Cashier
                Using cashier As New BOBanking.cCashBalCashier(_oasys3Db)
                    Dim cashiers As List(Of BOBanking.cCashBalCashier) = cashier.GetCashiers(periodId)
                    For Each c As BOBanking.cCashBalCashier In cashiers.Where(Function(f) f.CurrencyID.Value = defaultCurrencyId)
                        floatsIssued += c.FloatIssued.Value
                    Next
                End Using

            Case AccountabilityTypes.Till
                Using till As New BOBanking.cCashBalTill(_oasys3Db)
                    Dim tills As List(Of BOBanking.cCashBalTill) = till.GetTills(periodId)
                    For Each t As BOBanking.cCashBalTill In tills.Where(Function(f) f.CurrencyID.Value = defaultCurrencyId)
                        floatsIssued += t.FloatIssued.Value
                    Next
                End Using
        End Select

        Return floatsIssued


    End Function

    Public Function GetSafeValue(ByVal periodId As Integer) As Decimal

        Dim value As Decimal = 0

        Using currentSafe As New cSafe(_oasys3Db)
            currentSafe.Load(periodId)
            OutputBankingProcessName("Safe loaded for period " & periodId, True)

            For Each safeDenom As BOBanking.cSafeDenoms In currentSafe.Denoms
                OutputBankingProgress("Processing : " & safeDenom.CurrencyID.Value & Space(1) & safeDenom.PeriodID.Value)
                value += safeDenom.SafeValue.Value
                value += safeDenom.ChangeValue.Value
            Next
        End Using

        Return value

    End Function

    Public Function GetSealedBankingFloatValue(ByVal periodId As Integer) As Decimal

        Dim value As Decimal = 0

        'include sealed (ie unreleased and still in safe) float and banking bags
        Using safeBagClass As New BOBanking.cSafeBags(_oasys3Db)
            Dim bags As List(Of BOBanking.cSafeBags) = safeBagClass.GetBagsSealedForPeriod(periodId, BagTypes.Float)
            'bags.AddRange(safeBagClass.GetBagsSealedForPeriod(periodId, BagTypes.Banking))

            For Each bag As BOBanking.cSafeBags In bags
                value += bag.Value.Value
            Next
        End Using

        Return value

    End Function

    Public Function GetPickupValues(ByVal periodId As Integer, ByVal floatsReturned As Decimal) As Decimal()

        'get pickup values created this period
        Dim pickupValues(7) As Decimal
        Using safebag As New BOBanking.cSafeBags(_oasys3Db)
            For Each bag As BOBanking.cSafeBags In safebag.GetBagsForPeriod(periodId, BagTypes.Pickup)
                OutputBankingProgress("Checking CASH bag : " & bag.ID.Value)

                For Each denom As BOBanking.cSafeBagsDenoms In bag.Denoms
                    If (denom.TenderID.Value = 1) Then
                        Select Case (denom.ID.Value)
                            Case (100), (50) : pickupValues(1) += denom.Value.Value
                            Case (20) : pickupValues(2) += denom.Value.Value
                            Case (10) : pickupValues(3) += denom.Value.Value
                            Case (5) : pickupValues(4) += denom.Value.Value
                            Case (1), (2) : pickupValues(5) += denom.Value.Value
                            Case Else : pickupValues(6) += denom.Value.Value
                        End Select
                    End If
                Next

                'add any related bag (ie float from pickup) to totals
                If bag.RelatedBagId.Value > 0 Then
                    Dim relatedBag As New cSafeBags(_oasys3Db)
                    relatedBag.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, relatedBag.ID, bag.RelatedBagId.Value)
                    relatedBag.LoadMatches()

                    For Each denom As BOBanking.cSafeBagsDenoms In relatedBag.Denoms
                        If (denom.TenderID.Value = 1) Then
                            Select Case (denom.ID.Value)
                                Case (100), (50) : pickupValues(1) += denom.Value.Value
                                Case (20) : pickupValues(2) += denom.Value.Value
                                Case (10) : pickupValues(3) += denom.Value.Value
                                Case (5) : pickupValues(4) += denom.Value.Value
                                Case (1), (2) : pickupValues(5) += denom.Value.Value
                                Case Else : pickupValues(6) += denom.Value.Value
                            End Select
                        End If
                    Next
                End If

            Next
        End Using

        'deduct float returned value from pickup denominations above starting at 20
        'going down denominations if they go into a negative amount
        Trace.WriteLine("Float is " & floatsReturned)
        For index As Integer = 2 To 5
            Trace.WriteLine("Float to Reduce is " & floatsReturned & " from DenomTotal:" & pickupValues(index))
            If floatsReturned > 0 Then
                Dim lessAmount As Decimal = Math.Min(pickupValues(index), floatsReturned)
                pickupValues(index) -= lessAmount
                floatsReturned -= lessAmount
            End If
        Next

        Trace.WriteLine("Float to remove from coins is " & floatsReturned)
        'remove remainder from coins (putting into negative if neccasary if float still positive
        If floatsReturned > 0 Then
            pickupValues(6) -= floatsReturned
        End If

        Return pickupValues

    End Function


    Public Sub OutputBankingProcessName(ByVal message As String, Optional ByVal clearAll As Boolean = False)

        'output starting message
        Dim sb As New StringBuilder
        sb.Append(My.Resources.ProcessBanking & Space(1))
        sb.Append(message)
        sb.Append(" : " & TimeOfDay.ToString("hh:mm:ss"))
        OutputSthoLog(sb.ToString)

        If clearAll Then
            ProcessTransmissionsProgress.ProgressTextBox.Clear()
            ProcessTransmissionsProgress.RecordCountTextBox.Clear()
        End If

        ProcessTransmissionsProgress.ProcessName.Text = sb.ToString
        ProcessTransmissionsProgress.Show()

    End Sub

    Public Sub OutputBankingProgress(ByVal message As String)

        'output starting message
        Dim sb As New StringBuilder
        sb.Append(My.Resources.ProcessBanking & Space(1))
        sb.Append(message)
        sb.Append(" : " & TimeOfDay.ToString("hh:mm:ss"))
        OutputSthoLog(sb.ToString)

        ProcessTransmissionsProgress.ProgressTextBox.Text = message & Space(1) & " : " & TimeOfDay.ToString("hh:mm:ss")
        ProcessTransmissionsProgress.Show()

    End Sub

End Class

