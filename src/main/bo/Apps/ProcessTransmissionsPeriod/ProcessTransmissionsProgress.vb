﻿Public Class ProcessTransmissionsProgress

    Private Sub ProgressTextBox_CursorChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProgressTextBox.CursorChanged
        Me.Refresh()
    End Sub

    Private Sub ProgressTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProgressTextBox.GotFocus
        Me.Refresh()
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgressTextBox.TextChanged
        Me.Refresh()
    End Sub

    Private Sub ProcessName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcessName.TextChanged
        Me.Refresh()
    End Sub

    Private Sub ProcessTransmissionsProgress_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.Refresh()
    End Sub

    Private Sub ProcessTransmissionsProgress_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Me.Refresh()
    End Sub
End Class