﻿Public Class HPSTOReport

    Dim _StoreID As String
    Dim _Title2 As String

    Public Property ReportSource() As Data.DataTable
        Get
            Return CType(GridView1.DataSource, Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            GridControl1.DataSource = value
        End Set
    End Property

    Public Property StoreID() As String
        Get
            Return _StoreID
        End Get
        Set(ByVal value As String)
            _StoreID = value
        End Set
    End Property


    Private Sub HPSTOReport_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Dim CTSPrint As New ReportPrint(GridControl1, "Order Confirmation Report", "", _StoreID, ReportIndex, False)
        CTSPrint.ShowPreviewDialog()

        Me.Hide()

    End Sub
    Public Function ReportIndex() As String

        Dim ReportText As String = String.Empty

        ReportText = ReportText & "List of Amended  / Rejected Reason Codes " & vbTab & "  " & vbCr & "  " & vbTab & "  " & vbCr
        ReportText = ReportText & "01 - Destination location nnnn does not exist for company xx" & vbTab & "02 - Raised location nnnn does not exist for company xx" & vbCr
        ReportText = ReportText & "03 - Supplier does not exist" & vbTab & "04 - Order date raised is future date" & vbCr
        ReportText = ReportText & "05 - Order Date raised is an invalid date" & vbTab & "06 - Order expected delivery date is an invalid date" & vbCr
        ReportText = ReportText & "07 - Sku does not exist for supplier" & vbTab & "08 - Sku is not unique within order" & vbCr
        ReportText = ReportText & "09 - Company Code does not exist" & vbTab & "10 - Supplier SOQ day not valid for the date raised - Referred" & vbCr
        ReportText = ReportText & "11 - Store SOQ day does not match Central maintenance -Referred" & vbTab & "12 - Supplier referral flag is set - Order Referred" & vbCr
        ReportText = ReportText & "13 - Supplier SOQ day is not valid - Referred" & vbTab & "14 - Order contains Sku flagged as deleted - Referred" & vbCr
        ReportText = ReportText & "15 - WARNING: LOC-SOQ-NR is not numeric" & vbTab & "16 - Line number is not unique" & vbCr
        ReportText = ReportText & "17 - Line number is not numeric" & vbTab & "18 - No SOQ info for supplier & store on order date" & vbCr
        ReportText = ReportText & "19 - Order confirms invalid central order" & vbTab & "20 - Tradanet flag in OP record is not Y/N" & vbCr
        ReportText = ReportText & "21 - Duplicate Store Order Number found" & vbTab & "22 - Order type invalid" & vbCr
        ReportText = ReportText & "23 - Order received outside expected hours" & vbTab & "24 - Tradanet Flag in file NOT Y/N" & vbCr
        ReportText = ReportText & "25 - Order delivery date is too early" & vbTab & "26 - SKU line record does not match Header record" & vbCr
        ReportText = ReportText & "27 - Quantity is not numeric within order" & vbTab & "28 - Order Header (OP) is invalid" & vbCr
        ReportText = ReportText & "29 - Order Line (OD) is invalid" & vbTab & "30 - File rejected, not STHPO" & vbCr
        ReportText = ReportText & "31 - Invalid Store Order Number" & vbTab & "32 - Invalid Order mode for Supp-See Tradanet Flag & order type " & vbCr
        ReportText = ReportText & "99 - Order Rejected, Corrupt / Unexpected data in Order" & vbTab & "    " & vbCr

        Return ReportText
    End Function

End Class