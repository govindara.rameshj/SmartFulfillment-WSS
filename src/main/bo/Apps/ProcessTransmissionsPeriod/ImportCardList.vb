﻿Module ImportCardList

    Public Sub ProcessProductUpdates(ByVal ImportFilePath As String) ' Process data in HOSTA files

        Dim _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        Dim strWorkString As String = String.Empty
        Dim TransmissionFileReader As StreamReader
        Dim FileData As String = String.Empty
        Dim NoOfRecords As Integer = 0

        Dim strTransmissionFileName As String = "HOSTA"
        Dim strSaveToFileName As String = ""

        Dim FileToOpen As String = ImportFilePath & "\" & strTransmissionFileName


        strWorkString = "Processing Transmissions In - " & FileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        UpdateProgress(String.Empty, String.Empty, strWorkString)

        strSaveToFileName = FileToOpen.ToString & ".old"
        'Now Process the transmission files in date sequence from the Files to process array

        If File.Exists(FileToOpen) = False Then
            Trace.WriteLine("Could not locate : " & FileToOpen)
            Exit Sub
        End If

        _oasys3Db.BeginTransaction()
        strWorkString = "Validating :" & strSaveToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
        ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
        ProcessTransmissionsProgress.ProcessName.Text = strWorkString
        ProcessTransmissionsProgress.Show()
        TransmissionFileReader = New StreamReader(FileToOpen, True) ' Open the file for calculations

        Try
            Dim CardListBO As New BOSales.cCardList(_oasys3Db)
            CardListBO.DeleteAllRecords()

            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                FileData = TransmissionFileReader.ReadLine.PadRight(250, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = FileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = NoOfRecords.ToString & " - HOSTA"
                ProcessTransmissionsProgress.Show()
                DataIntegrityHosta(FileData, _oasys3Db)
            End While

            strWorkString = "Validation of :" & strSaveToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            UpdateProgress(String.Empty, String.Empty, strWorkString)

            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
        Catch ex As Exception
            _oasys3Db.RollBackTransaction()
            Exit Sub
        End Try

        _oasys3Db.CommitTransaction()

        If File.Exists(strSaveToFileName) Then
            My.Computer.FileSystem.DeleteFile(strSaveToFileName)
        End If

        My.Computer.FileSystem.CopyFile(FileToOpen, strSaveToFileName)
        My.Computer.FileSystem.DeleteFile(FileToOpen)

    End Sub ' Process data in HOSTA files

    Public Sub DataIntegrityHosta(ByVal strTestString As String, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB) ' Validate HOSTA data

        Dim CardListBO As New BOSales.cCardList(Oasys3DB)

        strTestString = strTestString.PadRight(114, " "c)
        Trace.WriteLine("DataIntegrityHostA:" & strTestString & "*")

        Try
            CardListBO.CardNumber.Value = strTestString.Substring(0, 17)
            CardListBO.CustomerName.Value = strTestString.Substring(17, 40)
            CardListBO.AddressLine1.Value = strTestString.Substring(57, 40).PadRight(40, " "c)
            CardListBO.AddressLine2.Value = strTestString.Substring(97, 40).PadRight(40, " "c)
            CardListBO.AddressLine3.Value = strTestString.Substring(137, 40).PadRight(40, " "c)
            CardListBO.AddressLine4.Value = strTestString.Substring(177, 40).PadRight(40, " "c)
            CardListBO.PostCode.Value = strTestString.Substring(217, 10).PadRight(10, " "c)
            CardListBO.HideAddress.Value = strTestString.Substring(227, 1)
            CardListBO.IsDeleted.Value = False
            If strTestString.Substring(228, 1) = "Y" Then CardListBO.IsDeleted.Value = True
            CardListBO.SaveIfNew()
        Catch ex As Exception
        End Try

    End Sub ' Validate HOSTA data

    Private Sub UpdateProgress(ByVal ProgressText As String, ByVal RecordCount As String, ByVal ProcessName As String)

        ProcessTransmissionsProgress.ProgressTextBox.Text = ProgressText
        ProcessTransmissionsProgress.RecordCountTextBox.Text = RecordCount
        ProcessTransmissionsProgress.ProcessName.Text = ProcessName
        ProcessTransmissionsProgress.Show()

    End Sub

End Module
