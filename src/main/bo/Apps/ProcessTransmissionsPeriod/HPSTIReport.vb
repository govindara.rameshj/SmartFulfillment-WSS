﻿Public Class HPSTIReport

    Dim _StoreID As String

    Public Property ReportSource() As Data.DataTable
        Get
            Return CType(GridView1.DataSource, Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            GridControl1.DataSource = value
        End Set
    End Property

    Public Property StoreID() As String
        Get
            Return _StoreID
        End Get
        Set(ByVal value As String)
            _StoreID = value
        End Set
    End Property


    Private Sub HPSTIReport_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Dim CTSPrint As New ReportPrint(GridControl1, "Container Summary", "STORE COPY", _StoreID, "", True)
        CTSPrint.ShowPreviewDialog()

        CTSPrint = New ReportPrint(GridControl1, "Container Summary", "DRIVERS COPY", _StoreID, "", True)
        CTSPrint.ShowPreviewDialog()
        Me.Hide()

    End Sub
End Class