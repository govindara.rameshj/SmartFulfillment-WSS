﻿Imports System.Text

Module ProcessHOSTG

    Private intMaximumSthoOutputLength As Integer = 10000
    Private SthocText As String = String.Empty
    Private _RecordTypes() As String = {" ", "HG", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", " ", " ", " ", "  ", "  ", "UK"}
    Private intSthocRecordsOut As Integer = 0
    Private _RejectedCounts As Integer() = New Integer(15) {}


    Public Sub ProcessHotCardList(ByVal ImportFilePath As String, ByVal STHOCFileName As String) ' Process data in HOSTG files

        Dim Oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

        Dim strTransmissionFileName As String = "HOSTG"
        Dim strSTHORecType As String = String.Empty
        Dim strSTHORecDate As String = String.Empty
        Dim strSTHORecHashValue As String = String.Empty

        Dim strWorkString As String = String.Empty
        Dim TransmissionFileReader As StreamReader
        Dim FileData As String = String.Empty
        Dim NoOfRecords As Integer = 0

        Dim strSaveToFileName As String = ""

        Dim FileToOpen As String = ImportFilePath & "\" & strTransmissionFileName

        Dim strFromRtiPath As String = ImportFilePath & "\FROMRTI"
        Dim strTVCPath As String = String.Empty


        Dim strOpenThisFile As String = String.Empty
        Dim strCopyToFileName As String = String.Empty
        Dim Strmas As New BOStore.cStore(Oasys3Db)
        Dim Sysdates As New BOSystem.cSystemDates(Oasys3Db)
        Sysdates.AddLoadField(Sysdates.Today)
        Sysdates.AddLoadField(Sysdates.Tomorrow)
        Sysdates.AddLoadField(Sysdates.RetryMode)
        Sysdates.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Sysdates.SystemDatesID, "01")
        Sysdates.LoadMatches()

        Dim boolGotDataToProcess As Boolean = False
        Dim intI As Integer
        Dim arrIntRecordTypeCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrintRecordTypeHashes As Decimal() = New Decimal(16) {}
        Dim arrIntProcessedCounts As Integer() = New Integer(15) {}
        Dim arrIntRecordTypeTrailerCounts As Integer() = New Integer(16) {}
        Dim arrdecRecordTypeTrailerHashes As Decimal() = New Decimal(15) {}
        Dim intWhichRecordType As Integer = 0
        Dim boolRecordCountsMatch As Boolean = False
        Dim boolRecordHashesMatch As Boolean = False
        Dim boolValidatedOk As Boolean = False
        Dim arrstrTrailerHashes() As String = New String(15) {}

        Dim intVersionNumber As Integer = 0
        Dim intSequenceNumber As Integer = 0
        '        Dim strTibhpoReportText As String = String.Empty

        Dim strTransmissionFileToOpen As String = strFromRtiPath & "\" & strTransmissionFileName
        strTVCPath = ImportFilePath & "\"
        '        strTibhpoReportPath = strReportSpoolPath & "\TIBHPO.PRN"
        strWorkString = "Processing Transmissions In - " & strTransmissionFileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
        OutputHOSTGProcessName(strWorkString)
        OutputSthoLog(strWorkString)

        While MainModule.PrepareNextRTIFile(strTransmissionFileName, MainModule._StoreNumber, strOpenThisFile)

            strCopyToFileName = strTVCPath & strTransmissionFileName & strOpenThisFile.Substring(strOpenThisFile.Length - 2)

            Array.Clear(arrIntProcessedCounts, 0, 15)
            Array.Clear(_RejectedCounts, 0, 15)
            Array.Clear(arrIntRecordTypeCounts, 0, 16)
            Array.Clear(arrIntRecordTypeTrailerCounts, 0, 15)
            Array.Clear(arrdecRecordTypeHashes, 0, 16)
            Array.Clear(arrdecRecordTypeTrailerHashes, 0, 15)
            strWorkString = "Processing :" & strCopyToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss") ' & vbCrLf
            OutputSthoLog(strWorkString)
            TransmissionFileReader = New StreamReader(strCopyToFileName, True) ' Open the file for calculations
            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                NoOfRecords = NoOfRecords + 1
                FileData = TransmissionFileReader.ReadLine
                FileData = FileData.PadRight(42, " "c)
                ProcessTransmissionsProgress.ProgressTextBox.Text = FileData
                ProcessTransmissionsProgress.RecordCountTextBox.Text = NoOfRecords.ToString & " - " & strTransmissionFileName
                ProcessTransmissionsProgress.Show()
                intWhichRecordType = 0
                For intWhichRecordType = 0 To 16
                    If FileData.ToUpper.StartsWith(_RecordTypes(intWhichRecordType)) Then
                        arrIntRecordTypeCounts(intWhichRecordType) += 1
                        arrIntProcessedCounts(intWhichRecordType) += 1
                        arrdecRecordTypeHashes(intWhichRecordType) += CDec(FileData.Substring(10, 12))
                        Exit For
                    End If
                Next
                If (intWhichRecordType = 17) And (FileData.ToUpper.StartsWith("HR") = False) And (FileData.ToUpper.StartsWith("TR") = False) Then
                    arrIntRecordTypeCounts(intWhichRecordType - 1) += 1
                    If IsNumeric(FileData.Substring(10, 12)) Then arrdecRecordTypeHashes(intWhichRecordType - 1) += CDec(FileData.Substring(10, 12))
                End If
                If FileData.ToUpper.StartsWith("HR") Then
                    intVersionNumber = CInt(FileData.Substring(18, 2))
                    intSequenceNumber = CInt(FileData.Substring(20, 6))
                End If
                If FileData.ToUpper.StartsWith("TR") Then
                    Dim intTrailerLength As Integer = FileData.Length
                    intI = 32
                    While intI < (intTrailerLength - 32)
                        intWhichRecordType = 0
                        For intWhichRecordType = 1 To 15
                            If FileData.Substring(intI, 1) <> Nothing Then
                                If FileData.Substring(intI, 1).ToUpper = "U" And FileData.Substring((intI + 5), 7) > "       " Then
                                    If FileData.Substring(intI, 2).ToUpper = _RecordTypes(intWhichRecordType) Then
                                        arrIntRecordTypeTrailerCounts(intWhichRecordType) = CInt(FileData.Substring((intI + 2), 7))
                                        arrdecRecordTypeTrailerHashes(intWhichRecordType) = CDec(FileData.Substring((intI + 9), 12))
                                    End If
                                End If
                            End If
                        Next
                        intI = intI + 21
                    End While
                End If
                If (FileData.ToUpper.StartsWith("HR") = False) And (FileData.ToUpper.StartsWith("TR") = False) Then
                    DataIntegrityHOSTG(FileData, Oasys3Db, STHOCFileName, intVersionNumber, intSequenceNumber, Sysdates.Today.Value)
                End If
            End While
            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
            strWorkString = "Processing of :" & strCopyToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss") & " Records Processed : " & Space(1) & NoOfRecords.ToString.PadLeft(6, " "c)
            OutputHOSTGProgress(strWorkString)
            OutputSthoLog(strWorkString)

            boolRecordCountsMatch = True
            boolRecordHashesMatch = True
            For intWhichRecordType = 1 To 15
                If arrIntRecordTypeCounts(intWhichRecordType) <> arrIntRecordTypeTrailerCounts(intWhichRecordType) Then
                    boolRecordCountsMatch = False
                End If
                If arrdecRecordTypeHashes(intWhichRecordType) <> arrdecRecordTypeTrailerHashes(intWhichRecordType) Then
                    boolRecordHashesMatch = False
                End If
            Next
            '            decHashValue = NoOfRecords
            SetupHashOut("CG", DateDiff(DateInterval.Day, CDate("1900-01-01"), Sysdates.Today.Value) + 1, Sysdates.Today.Value, strSTHORecType, strSTHORecDate, strSTHORecHashValue)
            If SthocText <> String.Empty Then
                SthocText = SthocText.ToString.TrimEnd(" "c) & vbCrLf
            End If
            SthocText = SthocText.ToString.TrimEnd(" "c) & strSTHORecType & strSTHORecDate & strSTHORecHashValue & "S" & intVersionNumber.ToString.PadLeft(2, "0"c) & intSequenceNumber.ToString.PadLeft(6, "0"c) & Space(9)
            For intWhichRecordType = 1 To 16
                If _RecordTypes(intWhichRecordType) > "  " Then
                    If intWhichRecordType < 16 Then
                        SthocText = SthocText & _RecordTypes(intWhichRecordType) & arrIntProcessedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1) & _RejectedCounts(intWhichRecordType).ToString.PadLeft(6, " "c) & Space(1)
                    Else
                        SthocText = SthocText & _RecordTypes(intWhichRecordType) & 0.ToString.PadLeft(6, " "c) & Space(1) & 0.ToString.PadLeft(6, " "c) & Space(1)
                    End If
                Else
                    SthocText = SthocText & "  " & "      " & Space(1) & "      " & Space(1)
                End If
            Next
            If SthocText.Length > 0 Then
                PutSthoToDisc(STHOCFileName, SthocText)
                SthocText = String.Empty
            End If
            If File.Exists(strCopyToFileName) Then
                Dim strSaveFile As String = ImportFilePath & "\SAVE\" & strTransmissionFileName & intVersionNumber.ToString.PadLeft(2, "0"c)
                If File.Exists(strSaveFile) Then My.Computer.FileSystem.DeleteFile(strSaveFile)
                My.Computer.FileSystem.CopyFile(strCopyToFileName, strSaveFile)
                My.Computer.FileSystem.DeleteFile(strCopyToFileName)
            End If
            Dim oTVCControl As New BOStoreTransValCtl.cStoreTransValCtl(Oasys3Db)
            oTVCControl.RecordVersionProcessed(strTransmissionFileName, intVersionNumber.ToString("00"), intSequenceNumber.ToString("000000"))
            oTVCControl.Dispose()
        End While ' Process the files

    End Sub ' Process data in HPSTO Files

    Public Sub DataIntegrityHOSTG(ByVal strTestString As String, ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal STHOCFileName As String, ByVal VersionNumber As Integer, ByVal SequenceNo As Integer, ByVal SysToday As Date) ' Validate HOSTG data also perform the updates

        Dim intRecordOccurrence As Integer = 0
        Dim GiftHot As New BOSales.cGiftHotCardList(Oasys3DB)
        Dim ColGH As New List(Of BOSales.cGiftHotCardList)
        strTestString = strTestString.PadRight(124, " "c)
        Dim PassedValidation As Boolean

        intRecordOccurrence = 0
        For intX = 0 To 14
            If strTestString.StartsWith(_RecordTypes(intX)) Then intRecordOccurrence = intX
        Next
        If intRecordOccurrence = 0 Then intRecordOccurrence = 3
        PassedValidation = True
        If strTestString.StartsWith("HG") Then 'Processing type "HG" record
            If PassedValidation = True Then
                ' Find the next fixture sequence number
                GiftHot.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, GiftHot.SerialNumber, strTestString.Substring(23, 8).PadRight(8, " "c))
                ColGH = GiftHot.LoadMatches(1)
                Try
                    If ColGH.Count < 1 Then
                        GiftHot.SerialNumber.Value = strTestString.Substring(23, 8).PadRight(8, " "c)
                        GiftHot.RedeemedText.Value = strTestString.Substring(31, 40).PadLeft(40, " "c)
                        GiftHot.AdditionalInfo.Value = strTestString.Substring(71, 40).PadLeft(40, " "c)
                    End If
                    If ColGH.Count < 1 Then
                        GiftHot.SaveIfNew()
                    Else
                        GiftHot.SaveIfExists()
                    End If
                Catch ex As Exception
                End Try
            End If ' HG record has passed validations
        Else
            OutputGiftHostError(STHOCFileName, strTestString, "Bad Record Type", VersionNumber, SequenceNo, SysToday)
            _RejectedCounts(intRecordOccurrence) = _RejectedCounts(intRecordOccurrence) + 1
        End If 'Processing type "HG" record
    End Sub ' Validate HOSTG data - also perform the updates

    Public Sub OutputHOSTGProcessName(ByVal message As String, Optional ByVal clearAll As Boolean = False)

        'output starting message
        Dim sb As New StringBuilder
        sb.Append(My.Resources.ProcessBanking & Space(1))
        sb.Append(message)
        sb.Append(" : " & TimeOfDay.ToString("hh:mm:ss"))
        OutputSthoLog(sb.ToString)

        If clearAll Then
            ProcessTransmissionsProgress.ProgressTextBox.Clear()
            ProcessTransmissionsProgress.RecordCountTextBox.Clear()
        End If

        ProcessTransmissionsProgress.ProcessName.Text = sb.ToString
        ProcessTransmissionsProgress.Show()

    End Sub

    Public Sub OutputHOSTGProgress(ByVal message As String)

        'output starting message
        Dim sb As New StringBuilder
        sb.Append(My.Resources.ProcessBanking & Space(1))
        sb.Append(message)
        sb.Append(" : " & TimeOfDay.ToString("hh:mm:ss"))
        OutputSthoLog(sb.ToString)

        ProcessTransmissionsProgress.ProgressTextBox.Text = message & Space(1) & " : " & TimeOfDay.ToString("hh:mm:ss")
        ProcessTransmissionsProgress.Show()

    End Sub

    Public Sub OutputGiftHostError(ByVal STHOCFileName As String, ByVal strTestString As String, ByVal strDataError As String, ByVal VersionNumber As Integer, ByVal SequenceNumber As Integer, ByVal SysToday As Date)

        Dim STHOCRecType As String = String.Empty
        Dim STHOCRecDate As String = String.Empty
        Dim STHOCRecHashValue As String = String.Empty

        SetupHashOut("CG", 1, SysToday, STHOCRecType, STHOCRecDate, STHOCRecHashValue)
        If SthocText <> String.Empty Then
            SthocText = SthocText.ToString.TrimEnd(" "c) & vbCrLf
        End If
        SthocText = SthocText & STHOCRecType & STHOCRecDate & STHOCRecHashValue & "D" & VersionNumber.ToString.PadLeft(2, "0"c) & SequenceNumber.ToString.PadLeft(6, "0"c) & Space(1) & strDataError.PadRight(18, " "c) & strTestString '& vbCrLf

        intSthocRecordsOut = intSthocRecordsOut + 1
        ProcessTransmissionsProgress.RecordCountTextBox.Text = intSthocRecordsOut.ToString & " - STHOC"
        ProcessTransmissionsProgress.ProgressTextBox.Text = strTestString
        ProcessTransmissionsProgress.Show()
        If SthocText.Length > intMaximumSthoOutputLength Then
            PutSthoToDisc(STHOCFileName, SthocText)
            SthocText = String.Empty
        End If
    End Sub

End Module
