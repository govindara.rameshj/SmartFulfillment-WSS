using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using Timer = System.Windows.Forms.Timer;
using Cts.Oasys.Core;
using slf4net;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Model;

namespace Cts.CncNotifier
{
    static class Program
    {
        public static string EnableUrl;
        public static bool AuthenticationInProgress;
        public static List<string> BrowserPathList = new List<string>();
        private static IDataLayerFactory dlFactory;

        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof (Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            Logger.Info("Starting application");

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (ApplicationAlreadyRunning())
            {
                const string str = "Application is already running";
                Logger.Warn(str);
                MessageBox.Show(str, Application.ProductName);
                return -1;
            }

            int processId;
            if (args.Length < 1 || !int.TryParse(args[0], out processId))
            {
                const string str = "Application can't be run directly";
                Logger.Error(str);
                MessageBox.Show(str, Application.ProductName);
                return -2;
            }

            Process parentProcess;
            try
            {
                parentProcess = Process.GetProcessById(processId);
            }
            catch (ArgumentException ex)
            {
                Logger.Error("Parent process is not running.\nException: {0}", ex);
                return -3;
            }
            parentProcess.EnableRaisingEvents = true;
            parentProcess.Exited += parentProcess_Exited;

            var browserPath1 = Parameters.GetParameterValue<String>(4503);
            var browserPath2 = Parameters.GetParameterValue<String>(4504);
            if (string.IsNullOrEmpty(browserPath1) || string.IsNullOrEmpty(browserPath2))
            {
                const string str = "Browser Path is not specified";
                Logger.Error(str);
                MessageBox.Show(str, Application.ProductName);
                return -4;
            }

            BrowserPathList.Add(browserPath1);
            BrowserPathList.Add(browserPath2);
            BrowserPathList.Add("chrome");

            var enableUrl = Parameters.GetParameterValue<String>(4501);
            if (string.IsNullOrEmpty(enableUrl))
            {
                const string str = "Enable URL is not specified";
                Logger.Error(str);
                MessageBox.Show(str, Application.ProductName);
                return -4;
            }

            EnableUrl = enableUrl;

            int notificationPeriod = 0;
            try
            {
                notificationPeriod = Parameters.GetParameterValue<int>(4500);
            }
            catch (Exception ex)
            {
                Logger.Warn("Unable to get Notification period from DB. Exception: {0}", ex);
            }

            if (notificationPeriod <= 0)
            {
                const string str = "Notification interval is not set";
                Logger.Error(str);
                MessageBox.Show(str, Application.ProductName);
                return -5;
            }

            var clock = new Timer();
            clock.Interval = notificationPeriod * 1000;
            clock.Tick += clock_Tick;
            clock.Enabled = true;

            var resolver = new SimpleResolver();
            dlFactory = resolver.Resolve<IDataLayerFactory>();

            Application.Run();

            Logger.Info("Application is exited");
            return 0;
        }

        private static void parentProcess_Exited(object sender, EventArgs e)
        {
            Logger.Info("Closing application");
            Application.Exit();
        }

        private static void clock_Tick(object sender, EventArgs e)
        {
            // Skip check if login form is opened
            if (AuthenticationInProgress)
            {
                Logger.Info("Skip check for new alerts");
                return;
            }

            Logger.Info("Check for new alerts");
            AlertForm.RefreshMessage(dlFactory);
        }

        #region Mutex

        static Mutex _mutex;

        private static bool ApplicationAlreadyRunning()
        {
            bool createdNew;
            _mutex = new Mutex(false, "Local\\85432234-DE20-4557-AD46-B7884D85F9CA-CncNotifier", out createdNew);

            if (createdNew)
            {
                _mutex.WaitOne();
                _mutex.ReleaseMutex();
            }

            return !createdNew;
        }

        #endregion

        #region Exception Handling

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error("Critical application exception. IsTerminating: {0}", e.IsTerminating);
            
            var exception = e.ExceptionObject as Exception;
            if (exception != null)
            {
                Logger.Error("Exception: {0}", exception);
            }
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Logger.Error("{0}\nException: {1}", e.Exception.Message, e.Exception);
            Application.Exit();
        }

        #endregion
    }
}
