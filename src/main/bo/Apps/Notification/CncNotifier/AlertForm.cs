using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WSS.BO.DataLayer.Model.Entities;
using Cts.Oasys.WinForm.User;
using System.Diagnostics;
using slf4net;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Constants;

namespace Cts.CncNotifier
{
    public partial class AlertForm : Form
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(AlertForm));

        private readonly IDataLayerFactory dlFactory;
        private static AlertForm _instance;

        private IList<Alert> _alerts;
        
        private AlertForm(IDataLayerFactory dlFactory)
        {
            this.dlFactory = dlFactory;
            InitializeComponent();
        }

        private void Show(string text)
        {
            txtOrders.Text = text;
            Show();
        }

        public static void RefreshMessage(IDataLayerFactory dlFactory)
        {
            if (_instance == null)
            {
                _instance = new AlertForm(dlFactory);
                Logger.Info("Creating new alert window");
            }
            else
            {
                Logger.Info("Using current alert window");
            }

            _instance._alerts = dlFactory.Create<IAlertRepository>().GetUnauthorizedAlerts();

            if (!_instance._alerts.Any())
            {
                Logger.Info("No new alerts");

                _instance.CloseForm();
                return;
            }

            Logger.Info("{0} new alert(s)", _instance._alerts.Count);

            var sb = new StringBuilder();

            AddMessage(sb, "Hourly", _instance._alerts.Count(a => a.AlertType == AlertType.WHS_new), _instance._alerts.Count(a => a.AlertType == AlertType.WHS_cancelled));
            AddMessage(sb, "C&&C", _instance._alerts.Count(a => a.AlertType == AlertType.CNC_new), _instance._alerts.Count(a => a.AlertType == AlertType.CNC_cancelled));
            
            _instance.Show(sb.ToString().Trim());
        }

        private static void AddMessage(StringBuilder sb, string name, int newCount, int cancCount)
        {
            if (newCount + cancCount > 0)
            {
                sb.AppendFormat("{0}: ", name);
                if (newCount > 0)
                {
                    sb.AppendFormat("{0} new", newCount).Append(cancCount > 0 ? ", " : "");
                }
                if (cancCount > 0)
                {
                    sb.AppendFormat("{0} cancelled", cancCount);
                }
                sb.AppendLine();
            }
        }
        
        private void AlertForm_Load(object sender, EventArgs e)
        {
            SetSize();
            
            var startPosX = Screen.PrimaryScreen.WorkingArea.Width - Width;
            var startPosY = Screen.PrimaryScreen.WorkingArea.Height - Height;

            SetDesktopLocation(startPosX, startPosY);
        }

        private int? AuthenticateUser()
        {
            int? userId;

            Program.AuthenticationInProgress = true;
            using (var loginForm = new UserAuthorise())
            {
                userId = loginForm.GetUserId();
            }
            Program.AuthenticationInProgress = false;

            return userId;
        }

        #region Button click

        private void btnEnable_Click(object sender, EventArgs e)
        {
            Logger.Info("User clicked Enable button");
            if (ClearAllNotifications())
            {
                Logger.Info("Starting Browser: {0}", Program.EnableUrl);
                bool browserPathFound = false;
                foreach (string browserPath in Program.BrowserPathList)
                {
                    try
                    {
                        Logger.Info("Try browser path " + browserPath);
                        Process.Start(browserPath, Program.EnableUrl);
                        browserPathFound = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Incorrect browser path.\nException: {0}", ex);
                    }
                }
                if (browserPathFound == false)
                {
                    const string str = "Browser path is not correct";
                    Logger.Error(str);
                    MessageBox.Show(str, Application.ProductName);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Logger.Info("User clicked Clear button");
            ClearAllNotifications();
        }

        private bool ClearAllNotifications()
        {
            var result = false;
            Hide();

            var userId = AuthenticateUser();
            if (userId.HasValue)
            {
                Logger.Info("Authorizing alerts");
                
                int affected = dlFactory.Create<IAlertRepository>().AuthorizeAlerts(_alerts, userId.Value);

                result = true;

                Logger.Info("{0} alert(s) authorized", affected);
                if (affected < _alerts.Count)
                {
                    MessageBox.Show(string.Format("Changes are {0} authorized by another user.",
                        (affected == 0) ? "already" : "partially"), Application.ProductName);
                }
            }

            CloseForm();
            return result;
        }

        private void CloseForm()
        {
            Close();
            CloseInstance();
        }

        private static void CloseInstance()
        {
            _instance = null;
        }

        #endregion

        #region Notification behaviour

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams baseParams = base.CreateParams;

                baseParams.ExStyle |= (
                  0x08000000 /*WS_EX_NOACTIVATE*/ |
                  0x00000080 /*WS_EX_TOOLWINDOW*/ |
                  0x00000008 /*WS_EX_TOPMOST*/);

                return baseParams;
            }
        }

        #endregion

        #region Size adjustment

        private void SetSize()
        {
            int width = Screen.PrimaryScreen.WorkingArea.Width;
            if (width < 900)
            {
                SetSizeS();
            }
            else if (width < 1100)
            {
                SetSizeM();
            }
            else if (width < 1500)
            {
                SetSizeL();
            }
            else
            {
                SetSizeXl();
            }
        }

        private void SetSizeS()
        {
            SetFont(this, 9F, false);

            SetFont(txtOrders, 9.75F, true);
            SetFont(btnClear, 6F, true);
            SetFont(btnEnable, 6F, true);

            picture.Size = new Size(32, 32);
            picture.Image = Properties.Resources.alert_32;
        }

        private void SetSizeM()
        {
            // default
        }

        private void SetSizeL()
        {
            SetFont(this, 12F, false);
            
            SetFont(txtOrders, 12F, true);
            SetFont(btnClear, 8.25F, true);
            SetFont(btnEnable, 8.25F, true);

            picture.Size = new Size(48, 48);
            picture.Image = Properties.Resources.alert_48;
        }

        private void SetSizeXl()
        {
            SetFont(this, 14.25F, false);

            SetFont(txtOrders, 14.25F, true);
            SetFont(btnClear, 9F, true);
            SetFont(btnEnable, 9F, true);

            picture.Size = new Size(60, 60);
            picture.Image = Properties.Resources.alert_60;
        }

        private void SetFont(Control ctrl, float size, bool bold)
        {
            ctrl.Font = new Font("Microsoft Sans Serif", size, bold ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Point, 204);
        }

        #endregion
    }
}
