﻿using System;
using Cts.Oasys.WinForm.User;
using System.Windows.Forms;
using slf4net;

namespace Cts.CncNotifier
{
    public static class UserAuthoriseExtensions
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(UserAuthoriseExtensions));

        public static int? GetUserId(this UserAuthorise loginForm)
        {
            loginForm.Text = "Click&Collect Alert Authentication";
            loginForm.StartPosition = FormStartPosition.CenterScreen;
            loginForm.TopMost = true;

            int? userId = new int?();

            loginForm.Shown += loginForm_Shown;
            if (loginForm.ShowDialog() == DialogResult.OK)
            {
                userId = loginForm.UserId;
                Logger.Info("User logged in: {0}", userId);
            }
            else
            {
                Logger.Info("User cancelled authentication");
            }
            loginForm.Shown -= loginForm_Shown;

            return userId;
        }

        static void loginForm_Shown(object sender, EventArgs e)
        {
            var form = sender as Form;
            if (form != null)
            {
                form.Activate();
            }
        }
    }
}
