﻿
Public Class UpdPriceChgFrmEvts
    Private _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _stocks As New BOStock.cStock(_oasys3Db)
    Private _tomorrow As Date
    Private _today As Date
    Private _daysToApply As Integer
    Private _dt As DataTable
    Private _eventTable As DataTable

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub DoProcessing()
        btnStart.PerformClick()
    End Sub

    Private Sub WorkerPreWork()

        DisplayStatus("Retrieving number of days past effect date")
        Using retopt As New BOSystem.cRetailOptions(_oasys3Db)
            Dim drRetOpt As DataRow = retopt.RetailOptions
            _daysToApply = CInt(drRetOpt.Item(retopt.NoDayPstEffectDatePC.ColumnName))
        End Using

        DisplayStatus("Retrieving system date")
        Dim daysPrior As Integer
        Using systemDate As New BOSystem.cSystemDates(_oasys3Db)
            Dim sysDate As DataRow = systemDate.SystemDates()
            daysPrior = CInt(sysDate(systemDate.DaysPriorToPriceChange.ColumnName)) '+ 7
            If daysPrior < 7 Then daysPrior = 7
            _tomorrow = CDate(sysDate(systemDate.Tomorrow.ColumnName))
            _today = CDate(sysDate(systemDate.Today.ColumnName))
            Trace.WriteLine("Days Prior Using = " + daysPrior.ToString)
            Trace.WriteLine("Today Using = " + _today.ToString)
            Trace.WriteLine("Tomorrow Using = " + _tomorrow.ToString)
            Trace.WriteLine("Therefore End of Period Date = " + CStr(DateAdd(DateInterval.Day, daysPrior, _today)))
        End Using

        DisplayStatus("Perform the deletes first")
        Dim theEvents As New BOEvent.cEventPriceChange(_oasys3Db)
        theEvents.DeleteChanges()

        DisplayStatus("Retrieving all event change records")

        _dt = theEvents.GetPriceChangeEvents(_today, daysPrior)
        _eventTable = _dt.Copy

        DisplayStatus("Retrieving stock items")
        Dim newTable As DataTable = _dt.Clone
        Dim skun As String = String.Empty
        Dim strEdat As String = String.Empty
        Dim createPriceChange As New CreatePriceChange
        Dim SavePriceChangesFactoryInstance As New SavePriceChanges

        Using rowStock As BOStock.cStock = _stocks

            Dim startDate As Date = Nothing

            For Each dr As DataRow In _dt.Rows

                Dim rowSkun As String = dr("SKUN").ToString
                Dim blnStockLoaded As Boolean = _stocks.LoadStockItemForPriceChange(rowSkun)

                DisplayStatus("Checking for the SKU " & dr.Item("SKUN").ToString() & " if it exists")
                'check that this sku exists
                If blnStockLoaded Then

                    Trace.WriteLine("SKUN = " & dr.Skun)
                    Trace.WriteLine("SDAT = " & dr.Sdat)
                    Trace.WriteLine("EDAT = " & dr.Edat)
                    Trace.WriteLine("PRIO = " & dr.Prio)
                    Trace.WriteLine("NUMB = " & dr.Numb)
                    Trace.WriteLine("PRIC = " & dr.Pric)

                    If rowStock IsNot Nothing Then
                        startDate = CDate(dr("Sdat"))
                        If dr.Skun <> skun Then
                            Dim SavePriceChange As New PriceChangeDataRepository
                            SavePriceChange.DeletePriceChangesNotPrintedAlreadyWithInOurWindow(dr.Skun, DateAdd(DateInterval.Day, 1, _today), daysPrior)
                            createPriceChange.RegressAndReset(startDate, daysPrior, strEdat, skun, newTable, rowStock)
                            strEdat = ""
                        End If

                        skun = dr.Skun
                        If IsDBNull(dr("EDAT")) And strEdat = String.Empty Then
                            Dim GetData As New GetData
                            strEdat = GetData.GetMaxEndDateForSku(skun, startDate, _today, daysPrior, _eventTable)
                        Else
                            strEdat = Cts.Oasys.Core.Common.NullableToString(dr.Edat, strEdat)
                        End If

                        If dr.Edat.HasValue Then
                            Dim dteCurrentEndDate As Date = CDate(strEdat)
                            Dim dteProposedEndDate As Date = dr.Edat.Value
                            If dteProposedEndDate > dteCurrentEndDate Then
                                strEdat = dr.Edat.Value.ToString()
                            End If
                        End If
                        createPriceChange.CheckAndApplyPriceChange(startDate, daysPrior, strEdat, dr, rowStock, newTable, _today)
                    Else
                        Trace.WriteLine("MISSING SKUN IN STKMAS: cStock - Skun No: " + rowSkun.ToString + " not found.")
                    End If
                End If
            Next dr
            createPriceChange.RegressAndReset(startDate, daysPrior, strEdat, skun, newTable, rowStock)
        End Using

        Trace.WriteLine("No of rows to process = " & _dt.Rows.Count.ToString)
        If Not newTable Is Nothing Then
            SavePriceChangesFactoryInstance.SavePriceChanges(_stocks, newTable, _daysToApply, pbCompleted)
        End If
        Me.FindForm.Close()

    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        btnStart.Enabled = False
        WorkerPreWork()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.FindForm.Close()
    End Sub

End Class

