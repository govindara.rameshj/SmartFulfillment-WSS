﻿Public Class PriceChangeDataRepository

    Public Function InsertPriceChangeRecord(ByVal Skun As String, ByVal StartDate As Date, ByVal NewPrice As Decimal, ByVal Status As String, ByVal IsShelfLabelNeeded As Boolean, ByVal IsSmallLabelNeeded As Boolean, ByVal IsMediumLabelNeeded As Boolean, ByVal IsLargeLabelNeeded As Boolean, ByVal EventNumber As String, ByVal Priority As String, ByVal daysAfter As Integer) As Boolean
        Dim linesInserted As Integer = 0
        Dim applyDate As Date = DateAdd(DateInterval.Day, daysAfter, StartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_InsertPriceChangeRecord"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@StartDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Price", NewPrice)
                    .AddParameter("@Status", Status)
                    .AddParameter("@IsShelfLabel", IIf(IsShelfLabelNeeded, 1, 0))
                    .AddParameter("@AutoApplyDate", applyDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@SmallLabel", IIf(IsSmallLabelNeeded, 1, 0))
                    .AddParameter("@MediumLabel", IIf(IsMediumLabelNeeded, 1, 0))
                    .AddParameter("@LargeLabel", IIf(IsLargeLabelNeeded, 1, 0))
                    .AddParameter("@EventNumber", EventNumber)
                    .AddParameter("@Priority", Priority)
                    linesInserted += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesInserted > 0

    End Function

    Public Function ReadPriceChangeTable(ByVal Skun As String, ByVal Price As Decimal, ByVal startDate As Date) As System.Data.DataTable
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetPriceChangeRecord"
                    .AddParameter("@skun", Skun)
                    .AddParameter("@StartDate", startDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Status", "U")
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return dt

    End Function

    Public Function UpdatePriceChangeRecord(ByVal Skun As String, ByVal StartDate As Date, ByVal NewPrice As Decimal, ByVal Status As String, ByVal EventNumber As String, ByVal Priority As String, ByVal daysAfter As Integer) As Boolean
        Dim linesUpdated As Integer = 0
        Dim applyDate As Date = DateAdd(DateInterval.Day, daysAfter, StartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_UpdatePriceChangeRecord"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@Price", NewPrice)
                    .AddParameter("@Status", Status)
                    .AddParameter("@EventNumber", EventNumber)
                    .AddParameter("@Priority", Priority)
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function UpdatePriceChangeRecordResetLabel(ByVal Skun As String, ByVal StartDate As Date, ByVal NewPrice As Decimal, ByVal Status As String, ByVal EventNumber As String, ByVal Priority As String, ByVal IsShelfLabelNeeded As Boolean, ByVal IsSmallLabelNeeded As Boolean, ByVal IsMediumLabelNeeded As Boolean, ByVal IsLargeLabelNeeded As Boolean, ByVal daysAfter As Integer) As Boolean
        Dim linesUpdated As Integer = 0
        Dim applyDate As Date = DateAdd(DateInterval.Day, daysAfter, StartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_UpdatePriceChangeRecordResetLabel"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@StartDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Price", NewPrice)
                    .AddParameter("@Status", Status)
                    .AddParameter("@EventNumber", EventNumber)
                    .AddParameter("@Priority", Priority)
                    .AddParameter("@IsShelfLabel", IIf(IsShelfLabelNeeded, 1, 0))
                    .AddParameter("@SmallLabel", IIf(IsSmallLabelNeeded, 1, 0))
                    .AddParameter("@MediumLabel", IIf(IsMediumLabelNeeded, 1, 0))
                    .AddParameter("@LargeLabel", IIf(IsLargeLabelNeeded, 1, 0))
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function HasPriceChanged(ByVal Skun As String, ByVal StartDate As Date, ByVal Price As Decimal) As Boolean
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetPriceChangeRecordWithPrice"
                    .AddParameter("@skun", Skun)
                    .AddParameter("@StartDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Price", Price)
                    .AddParameter("@Status", "U")
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return (dt.Rows.Count = 0)

    End Function

    Public Function DoesThisSkuHaveAUnappledPrintedLabelAtSamePrice(ByVal Skun As String, ByVal Price As Decimal) As Boolean
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetPriceChangeRecordWithPrice"
                    .AddParameter("@skun", Skun)
                    .AddParameter("@Price", Price)
                    .AddParameter("@LabelPrinted", 1)
                    .AddParameter("@Status", "U")
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return (dt.Rows.Count > 0)

    End Function

    Public Function ReadPriceChangeTableForSku(ByVal Skun As String) As System.Data.DataTable
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetPriceChangeRecordsForSku"
                    .AddParameter("@skun", Skun)
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return dt

    End Function

    Public Function UpdatePriceChangeRecordResetLabelWithOriginalDate(ByVal Skun As String, ByVal OriginalPDAT As Date, ByVal StartDate As Date, ByVal NewPrice As Decimal, ByVal Status As String, ByVal EventNumber As String, ByVal Priority As String, ByVal IsShelfLabelNeeded As Boolean, ByVal IsSmallLabelNeeded As Boolean, ByVal IsMediumLabelNeeded As Boolean, ByVal IsLargeLabelNeeded As Boolean, ByVal daysAfter As Integer, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String) As Boolean
        Dim linesUpdated As Integer = 0
        Dim applyDate As Date = DateAdd(DateInterval.Day, daysAfter, StartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_UpdatePriceChangeRecordResetLabelWithOriginalDate"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@OriginalPDAT", OriginalPDAT.ToString("yyyy/MM/dd"))
                    .AddParameter("@OriginalEventNumber", OriginalEventNumber)
                    .AddParameter("@OriginalPrice", OriginalPric)
                    .AddParameter("@OriginalStatus", OriginalStatus)
                    .AddParameter("@StartDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Price", NewPrice)
                    .AddParameter("@Status", Status)
                    .AddParameter("@EventNumber", EventNumber)
                    .AddParameter("@Priority", Priority)
                    .AddParameter("@AutoApplyDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@IsShelfLabel", IIf(IsShelfLabelNeeded, 1, 0))
                    .AddParameter("@SmallLabel", IIf(IsSmallLabelNeeded, 1, 0))
                    .AddParameter("@MediumLabel", IIf(IsMediumLabelNeeded, 1, 0))
                    .AddParameter("@LargeLabel", IIf(IsLargeLabelNeeded, 1, 0))
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function UpdatePriceChangeRecordWithOriginalDate(ByVal Skun As String, ByVal OriginalPDAT As Date, ByVal StartDate As Date, ByVal NewPrice As Decimal, ByVal Status As String, ByVal EventNumber As String, ByVal Priority As String, ByVal daysAfter As Integer, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String) As Boolean
        Dim linesUpdated As Integer = 0
        Dim applyDate As Date = DateAdd(DateInterval.Day, daysAfter, StartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_UpdatePriceChangeRecordWithOriginalDate"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@OriginalPDAT", OriginalPDAT.ToString("yyyy/MM/dd"))
                    .AddParameter("@OriginalEventNumber", OriginalEventNumber)
                    .AddParameter("@OriginalPrice", OriginalPric)
                    .AddParameter("@OriginalStatus", OriginalStatus)
                    .AddParameter("@StartDate", StartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@Price", NewPrice)
                    .AddParameter("@Status", Status)
                    .AddParameter("@EventNumber", EventNumber)
                    .AddParameter("@Priority", Priority)
                    .AddParameter("@AutoApplyDate", StartDate.ToString("yyyy/MM/dd"))
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function DeletePriceChangesNotPrintedAlreadyWithInOurWindow(ByVal Skun As String, ByVal WindowStartDate As Date, ByVal NoOfDaysInWindow As Integer) As Integer
        Dim linesDeleted As Integer = 0
        Dim WindowEndDate As Date = DateAdd(DateInterval.Day, NoOfDaysInWindow, WindowStartDate)

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_DeletePriceChangeRecordsWithInOurWindow"
                    .AddParameter("@Skun", Skun)
                    .AddParameter("@WindowStartDate", WindowStartDate.ToString("yyyy/MM/dd"))
                    .AddParameter("@WindowEndDate", WindowEndDate.ToString("yyyy/MM/dd"))
                    linesDeleted += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesDeleted

    End Function

    Public Function GetEffectivePriceChangeEvent(ByVal skun As String, ByVal [date] As Date) As System.Data.DataRow
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_EffectiveSkuEVTCHGForDate"
                    .AddParameter("@skun", skun)
                    .AddParameter("@date", [date])
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)
        Else
            Return Nothing
        End If

    End Function

End Class
