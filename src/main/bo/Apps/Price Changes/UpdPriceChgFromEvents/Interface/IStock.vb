﻿Public Interface IStock

    Property SkuNumber() As String
    Property NormalSellPrice() As Decimal
    Property RetailPriceEventNo() As String
    Property RetailPricePriority() As String

End Interface
