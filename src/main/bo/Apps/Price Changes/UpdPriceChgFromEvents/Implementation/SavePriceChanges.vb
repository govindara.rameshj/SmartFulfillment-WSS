﻿Imports System.Text

Public Class SavePriceChanges

    Public Sub SavePriceChanges(ByVal stocks As BOStock.cStock, ByVal dt As System.Data.DataTable, ByVal daysToApply As Integer, ByRef pbCompleted As System.Windows.Forms.ProgressBar)
        Dim progressCount As Integer = 0

        Try
            pbCompleted.Maximum = dt.Rows.Count
            pbCompleted.Minimum = 0
            For Each dr As DataRow In dt.Rows
                stocks.LoadStockItem(dr.Skun)
                Dim stockItem As BOStock.cStock = stocks

                If stockItem IsNot Nothing Then
                    progressCount = progressCount + 1
                    pbCompleted.Value = progressCount
                    Trace.WriteLine("Calling Save Price Change.")
                    Dim AddPriceChange As New AddPriceChangeRecord
                    AddPriceChange.SavePriceChange(dr, stockItem, daysToApply)
                    Trace.WriteLine("Back from Save Price Change.")
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message)
        End Try

    End Sub

End Class


Public Class DataColumnToDelimitedTextConverter
    Private _Table As DataTable

    Public Function DelimitedValues(ByVal columnName As String) As String
        Dim sb As New StringBuilder
        For Each dr As DataRow In _Table.Rows
            sb.Append(dr(columnName))
            sb.Append(",")
        Next
        sb.Remove(sb.Length - 1, 1)

        Return sb.ToString
    End Function

    Public Property DataTable() As DataTable
        Get
            Return _Table
        End Get
        Set(ByVal value As DataTable)
            _Table = value
        End Set
    End Property
End Class