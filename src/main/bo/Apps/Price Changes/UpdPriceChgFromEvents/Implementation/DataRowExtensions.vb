﻿Imports System.Runtime.CompilerServices

Module DataRowExtensions

    <Extension()>
    Public Function Skun(row As DataRow) As String
        Return CStr(row!SKUN)
    End Function

    <Extension()>
    Public Function Numb(row As DataRow) As String
        Return CStr(row!NUMB)
    End Function

    <Extension()>
    Public Function Prio(row As DataRow) As String
        Return CStr(row!Prio)
    End Function

    <Extension()>
    Public Function ActiveDate(row As DataRow) As Date
        Return CDate(row!ActiveDate)
    End Function

    <Extension()>
    Public Function Sdat(row As DataRow) As Date
        Return CDate(row!SDAT)
    End Function

    <Extension()>
    Public Sub SetSdat(row As DataRow, value As Date)
        row!SDAT = value
    End Sub


    <Extension()>
    Public Function Edat(row As DataRow) As Date?
        If (IsDBNull(row!EDAT)) Then
            Return Nothing
        Else
            Return CDate(row!EDAT)
        End If
    End Function

    <Extension()>
    Public Function Idel(row As DataRow) As Boolean
        Return CBool(row!Idel)
    End Function

    <Extension()>
    Public Function Pric(row As DataRow) As Decimal
        Return CDec(row!PRIC)
    End Function

    <Extension()>
    Public Function Pdat(row As DataRow) As Date
        Return CDate(row!PDAT)
    End Function

    <Extension()>
    Public Function Evnt(row As DataRow) As Integer
        Return CInt(row!EVNT)
    End Function

    <Extension()>
    Public Function EvntStr(row As DataRow) As String
        Return CStr(row!EVNT)
    End Function

    <Extension()>
    Public Function PSTA(row As DataRow) As String
        Return CStr(row!PSTA)
    End Function

    <Extension()>
    Public Function SHEL(row As DataRow) As Boolean
        Return CBool(row!SHEL)
    End Function

End Module
