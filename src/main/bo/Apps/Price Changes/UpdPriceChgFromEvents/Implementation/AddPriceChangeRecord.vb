﻿Friend Class AddPriceChangeRecord
    Friend dtPrc As DataTable
    Friend IsShelfLabelNeeded As Boolean = False
    Friend IsSmallLabelNeeded As Boolean = False
    Friend IsMediumLabelNeeded As Boolean = False
    Friend IsLargeLabelNeeded As Boolean = False

    Friend Overridable Function SkuDateAndEventMatch(ByVal dt As DataTable, ByVal SKU As String, ByVal EventNo As String, ByVal PriceChangeDate As Date) As Boolean
        Dim MaximumPriorityEVTCHGForSkuOnEventAndDate = From row In dt _
                    Where row.Skun = SKU _
                    And row.Evnt = CInt(EventNo) _
                    And row.Pdat = PriceChangeDate

        If MaximumPriorityEVTCHGForSkuOnEventAndDate.Any() Then
            dtPrc = MaximumPriorityEVTCHGForSkuOnEventAndDate.CopyToDataTable()
        End If
        Return MaximumPriorityEVTCHGForSkuOnEventAndDate.Any()

    End Function

    Friend Overridable Function SkuAndDateMatchFound(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date) As Boolean
        Dim MaximumPriorityEVTCHGForSkuOnDate = From row In dt _
                    Where row.Skun = SKU _
                    And row.Pdat = PriceChangeDate

        If MaximumPriorityEVTCHGForSkuOnDate.Count > 0 Then
            dtPrc = MaximumPriorityEVTCHGForSkuOnDate.CopyToDataTable
        End If
        Return MaximumPriorityEVTCHGForSkuOnDate.Count() > 0

    End Function

    Friend Overridable Function SkuUnappliedAndLabelPrinted(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date, ByVal EventNo As String) As Boolean
        Dim Factory As New LabelRequired
        Return Factory.SkuUnappliedAndLabelPrinted(dt, SKU, EventNo, PriceChangeDate, dtPrc)
    End Function

    Friend Overridable Function MatchingPriceChangeFound(ByVal dtPrcchg As DataTable, ByVal Sku As String, ByVal PriceChangeDate As Date, ByVal EventNo As String, ByVal Price As Decimal) As Boolean
        Dim result As Boolean = False

        If SkuAndDateMatchFound(dtPrcchg, Sku, PriceChangeDate) Or _
            SkuDateAndEventMatch(dtPrcchg, Sku, EventNo, PriceChangeDate) Or _
            SkuUnappliedAndLabelPrinted(dtPrcchg, Sku, PriceChangeDate, EventNo) Then
            result = True
        End If

        Return result
    End Function

    Friend Overridable Sub InsertPriceChange(ByVal drEvtcgh As DataRow, ByVal DaysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository)
        SavePriceChange.InsertPriceChangeRecord(drEvtcgh.Skun, drEvtcgh.Sdat, drEvtcgh.Pric, "U", IsShelfLabelNeeded, IsSmallLabelNeeded, IsMediumLabelNeeded, IsLargeLabelNeeded, drEvtcgh.Numb, drEvtcgh.Prio, DaysAfter)
    End Sub

    Friend Overridable Sub InsertPriceChange(ByVal drEvtcgh As DataRow, ByVal DaysAfter As Integer)
        Dim SavePriceChange As New PriceChangeDataRepository
        InsertPriceChange(drEvtcgh, DaysAfter, SavePriceChange)
    End Sub

    Friend Overridable Function IsPriceTheSame(ByVal EvtcghPrice As Object, ByVal PrccghPrice As Object) As Boolean
        Dim result As Boolean = False
        If CDec(EvtcghPrice) = CDec(PrccghPrice) Then
            result = True
        End If
        Return result
    End Function

    Friend Overridable Sub UpdatePriceChangeRecordWithOriginalDate(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String)
        SavePriceChange.UpdatePriceChangeRecordWithOriginalDate(drEvtchg.Skun, dtPrc.Rows(0).Pdat, drEvtchg.Sdat, drEvtchg.Pric, "U", drEvtchg.Numb, drEvtchg.Prio, daysAfter, OriginalEventNumber, OriginalPric, OriginalStatus)
    End Sub

    Friend Overridable Sub UpdatePriceChangeRecordResetLabelWithOriginalDate(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String)
        SavePriceChange.UpdatePriceChangeRecordResetLabelWithOriginalDate(drEvtchg.Skun, dtPrc.Rows(0).Pdat, drEvtchg.Sdat, drEvtchg.Pric, "U", drEvtchg.Numb, drEvtchg.Prio, IsShelfLabelNeeded, IsSmallLabelNeeded, IsMediumLabelNeeded, IsLargeLabelNeeded, daysAfter, OriginalEventNumber, OriginalPric, OriginalStatus)
    End Sub

    Friend Overridable Sub UpdatePriceChange(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer)
        Dim SavePriceChange As New PriceChangeDataRepository

        If IsPriceTheSame(drEvtchg.Pric, dtPrc.Rows(0).Pric) Then
            UpdatePriceChangeRecordWithOriginalDate(drEvtchg, daysAfter, SavePriceChange, dtPrc.Rows(0).EvntStr, dtPrc.Rows(0).Pric, dtPrc.Rows(0).PSTA)
        Else
            UpdatePriceChangeRecordResetLabelWithOriginalDate(drEvtchg, daysAfter, SavePriceChange, dtPrc.Rows(0).EvntStr, dtPrc.Rows(0).Pric, dtPrc.Rows(0).PSTA)
        End If
    End Sub

    Friend Overridable Function PriceChangeReader(ByVal Sku As String) As DataTable
        Dim ReadPriceChange As New PriceChangeDataRepository

        Return ReadPriceChange.ReadPriceChangeTableForSku(Sku)
    End Function

    Friend Overridable Function LabelPrintedToBeSet(ByVal stockItem As cStock) As Boolean
        Dim result As Boolean = False

        If (stockItem.NonStockItem.Value = True Or _
            stockItem.AutoApplyPriceChgs.Value = True Or _
            stockItem.ItemObsolete.Value = True) AndAlso _
            ((stockItem.StockOnHand.Value + stockItem.MarkDownQuantity.Value) < 1) Then
            result = True
        End If
        Return result
    End Function

    Friend Overridable Sub SetLabelAsPrinted()
        IsSmallLabelNeeded = False
        IsMediumLabelNeeded = False
        IsLargeLabelNeeded = False
        IsShelfLabelNeeded = True
    End Sub

    Friend Overridable Sub SetLabelFromStockRecord(ByVal stockItem As BOStock.cStock)
        Dim LabelPrinted As Boolean = False

        IsShelfLabelNeeded = False
        IsSmallLabelNeeded = IsLabelNeeded(stockItem.NoOfSmallLabels.Value)
        IsMediumLabelNeeded = IsLabelNeeded(stockItem.NoOfMediumLabels.Value)
        IsLargeLabelNeeded = IsLabelNeeded(stockItem.NoOfLargeLabels.Value)

        If LabelPrintedToBeSet(stockItem) Then
            SetLabelAsPrinted()
        End If

    End Sub

    Friend Overridable Sub SavePriceChange(ByVal drEvtchg As System.Data.DataRow, ByVal stockItem As BOStock.cStock, ByVal daysAfter As Integer)
        Dim dtPrcchg As DataTable = PriceChangeReader(CStr(drEvtchg.Skun))
        SetLabelFromStockRecord(stockItem)

        If MatchingPriceChangeFound(dtPrcchg, drEvtchg.Skun, drEvtchg.Sdat, drEvtchg.Numb, drEvtchg.Pric) Then
            UpdatePriceChange(drEvtchg, daysAfter)
        Else
            InsertPriceChange(drEvtchg, daysAfter)
        End If
    End Sub

    Friend Overridable Function IsLabelNeeded(ByVal Value As Integer) As Boolean
        Dim Label As New LabelRequired
        Return Label.IsLabelRequired(Value)
    End Function

End Class
