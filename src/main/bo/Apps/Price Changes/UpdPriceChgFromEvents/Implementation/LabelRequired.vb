﻿Public Class LabelRequired

    Public Function IsLabelRequired(ByVal Value As Integer) As Boolean
        Return Value > 0
    End Function

    Public Function SkuUnappliedAndLabelPrinted(ByVal dt As System.Data.DataTable, ByVal SKU As String, ByVal EventNo As String, ByVal PriceChangeDate As Date, ByRef dtPrc As System.Data.DataTable) As Boolean
        Dim MaximumPriorityEVTCHGForSku = From row In dt _
                    Where row.Skun = SKU _
                    And row.PSTA = "U" _
                    And (row.Pdat = PriceChangeDate Or row.Evnt <= CInt(EventNo)) _
                    And row.SHEL = True
        If FoundMatchingRecord(MaximumPriorityEVTCHGForSku.Count) Then
            AddMatchingRecords(MaximumPriorityEVTCHGForSku.CopyToDataTable, dtPrc)
        End If
        Return MaximumPriorityEVTCHGForSku.Count() > 0
    End Function

    Friend Function FoundMatchingRecord(ByVal Value As Integer) As Boolean
        Return Value > 0
    End Function

    Friend Sub AddMatchingRecords(ByVal TableToCopyFrom As DataTable, ByRef TableToCopyTo As DataTable)
        TableToCopyTo = TableToCopyFrom
    End Sub
End Class
