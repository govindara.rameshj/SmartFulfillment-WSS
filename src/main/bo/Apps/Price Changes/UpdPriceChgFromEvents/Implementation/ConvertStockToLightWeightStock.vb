﻿
Imports BOStock

Public Class ConvertStockToLightWeightStock

    Public Function ConvertToLightWeightStock(ByVal RowStock As BOStock.cStock) As IStock
        Dim lightWeightStock As IStock = New Stock

        With lightWeightStock
            .NormalSellPrice = RowStock.NormalSellPrice.Value
            .RetailPriceEventNo = RowStock.RetailPriceEventNo.Value
            .RetailPricePriority = RowStock.RetailPricePriority.Value
            .SkuNumber = RowStock.SkuNumber.Value
        End With

        Return lightWeightStock

    End Function

End Class
