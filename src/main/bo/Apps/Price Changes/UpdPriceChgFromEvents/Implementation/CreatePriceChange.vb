﻿Imports System.Runtime.CompilerServices
Imports BOStock
<Assembly: InternalsVisibleTo("UpdPriceChgFrmEvts.UnitTest")> 
Public Class CreatePriceChange

    Friend _blnTidiedUpStock As Boolean
    Friend _blnRegressionRequired As Boolean
    Friend _RegressesionCheckNeeded As Boolean
    Friend _blnPriority20PastEndDate As Boolean
    Friend _blnPriorityTen As Boolean
    Friend _blnPriorityTwenty As Boolean
    Friend _today As Date
    Friend _eventEndDate As Date
    Friend _latestEndingEventEndDate As Date
    Friend _strEdat As String
    Friend _potentialPriceChange As DataRow
    Friend _endDateOfPriceChangeCreationWindow As Date
    Friend _eventStartDate As Date
    Friend _daysInFuture As Integer
    Friend _verifiedPriceChanges As System.Data.DataTable
    Friend _verifiedPriceChangeRow As DataRow
    Friend _stockRow As IStock
    Friend _blnEventFinishedToday As Boolean = False

    Private Const REGRESSION_PRIORITY As Integer = 10
    Private Const PROMOTION_PRIORITY As Integer = 20

#Region "Interface functionality"

    Public Sub CheckAndApplyPriceChange(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByRef strEdat As String, ByVal dr As System.Data.DataRow, ByVal rowStock As cStock, ByRef newTable As System.Data.DataTable, ByVal _today As Date)
        Dim ConvertStockToLightWeightStock As New ConvertStockToLightWeightStock
        Dim LightWeightStock As IStock = New Stock

        LightWeightStock = ConvertStockToLightWeightStock.ConvertToLightWeightStock(rowStock)
        CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, dr, LightWeightStock, newTable, _today)

    End Sub

    Public Sub CheckAndApplyPriceChange(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByRef strEdat As String, ByVal dr As System.Data.DataRow, ByVal rowStock As IStock, ByRef newTable As System.Data.DataTable, ByVal Today As Date)
        SetUpClassVariables(StartDate, DaysPrior, strEdat, dr, newTable, rowStock, Today)

        If NewRegressionEventNeedsToBeAdded() Then Exit Sub

        If IsPriceChangeCurrentPrice() Then Exit Sub

        If HasPriceChangeAlreadyBeenAdded(_potentialPriceChange) Then Exit Sub

        UpdatePriceChange(_eventStartDate, _eventEndDate, _strEdat, _potentialPriceChange, _stockRow, _verifiedPriceChanges, _endDateOfPriceChangeCreationWindow)
        SetRegressionRequired()
        Trace.WriteLine("13. Update, Exit.")

    End Sub

    Public Sub UpdatePriceChange(ByVal StartDate As Date, ByVal EndDate As Date, ByRef strEdat As String, ByVal _potentialPriceChange As System.Data.DataRow, ByVal rowStock As IStock, ByRef newTable As System.Data.DataTable, ByVal Ending As Date)
        Dim NewRow As DataRow = newTable.NewRow

        NewRow.ItemArray = _potentialPriceChange.ItemArray
        newTable.Rows.Add(NewRow)

    End Sub

    Friend Sub CheckForRegression(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByVal strEdat As String, ByVal Skun As String, ByVal newTable As DataTable, ByVal rowStock As IStock)
        Dim Ending As Date = DateAdd(DateInterval.Day, DaysPrior, _today)
        Dim EndDate As Date = CDate("01-01-2000")
        Dim CreatePriceChange As New CreatePriceChange

        'Need to Check if a regression event ends within our checking period, if so add a new regression event
        'If within ending date then we need to get another regression event
        For Each row As DataRow In newTable.Rows
            If row.Sdat < Ending And row.Sdat >= _today Then
                If row.Edat Is Nothing And row.Prio = "10" Then
                    If row.Skun = Skun Then
                        Dim TempDate = row.Sdat
                        StartDate = DateAdd(DateInterval.Day, 1, CDate(strEdat))
                        row.SetSdat(StartDate)
                        Dim strEndDate As String = Cts.Oasys.Core.Common.NullableToString(row.Edat, "")
                        If ThisPriceChangeAlreadyExists(newTable, row, CStr(TempDate), strEndDate) = False Then
                            CreatePriceChange.UpdatePriceChange(StartDate, EndDate, strEdat, row, rowStock, newTable, Ending)
                            row.SetSdat(TempDate)
                            Trace.WriteLine("3. Update, Exit.")
                            Exit Sub
                        End If
                    End If
                End If
            End If
        Next row
    End Sub

    Private Function ThisPriceChangeAlreadyExists(ByVal newTable As DataTable, ByVal ExistingRow As DataRow, ByVal strSDAT As String, ByVal CurrentLatestEndDateForSkun As String) As Boolean
        For Each row As DataRow In newTable.Rows
            Dim strEndDate As String = Cts.Oasys.Core.Common.NullableToString(row.Edat, "")
            If row.Skun = ExistingRow.Skun AndAlso _
                  row.Sdat.ToString("dd MMM yyyy") = CDate(strSDAT).ToString("dd MMM yyyy") AndAlso _
                  row.Prio = ExistingRow.Prio AndAlso _
                  row.Numb = ExistingRow.Numb AndAlso _
                  strEndDate = CurrentLatestEndDateForSkun AndAlso _
                  row.Pric = ExistingRow.Pric Then
                Return True
            End If
        Next
        Return False
    End Function

#End Region

#Region "Setup"

    Friend Sub SetWindowEnding()
        _endDateOfPriceChangeCreationWindow = DateAdd(DateInterval.Day, _daysInFuture, _today)
    End Sub

    Friend Sub SetDaysInFuture(ByVal DaysPrior As Integer)
        _daysInFuture = DaysPrior
    End Sub

    Friend Sub SetEventStartDate(ByVal StartDate As Date)
        _eventStartDate = StartDate
    End Sub

    Friend Sub SetNewTable(ByVal newTable As System.Data.DataTable)
        _verifiedPriceChanges = newTable
    End Sub

    Friend Sub SetLatestEndingEventEndDate(ByVal strEdat As String)
        If strEdat.Length > 0 Then
            _latestEndingEventEndDate = CDate(strEdat)
            _strEdat = strEdat
        End If
    End Sub

    Friend Sub SetEventEndDate()
        If _potentialPriceChange.Edat.HasValue Then
            _eventEndDate = _potentialPriceChange.Edat.Value
        Else
            _eventEndDate = CDate("01-01-2000")
        End If
    End Sub

    Friend Sub SetPotentialPriceChangeDataRow(ByVal dr As DataRow)
        _potentialPriceChange = dr
    End Sub

    Friend Sub SetStockRow(ByVal stockRow As IStock)
        _stockRow = stockRow
    End Sub

    Friend Sub SetToday(ByVal today As Date)
        _today = today
    End Sub

#End Region

#Region "implementation detail"

    Friend Sub SetUpClassVariables(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByVal strEdat As String, ByVal dr As System.Data.DataRow, ByRef newTable As System.Data.DataTable, ByVal rowStock As IStock, ByVal Today As Date)
        SetToday(Today)
        SetPotentialPriceChangeDataRow(dr)
        SetDaysInFuture(DaysPrior)
        SetLatestEndingEventEndDate(strEdat)
        SetWindowEnding()
        SetEventStartDate(StartDate)
        SetEventEndDate()
        SetNewTable(newTable)
        SetStockRow(rowStock)

    End Sub

    Friend Function EventIsNotActiveWithinPriceChangeCreationWindow() As Boolean
        Dim result As Boolean = False

        If EventStartDateBeforeStartOfPriceChangeCreationWindow() And EventIsNotActiveBeforeEndOfPriceChangeCreationWindow() Then
            'Priority 10 (end date is null) so already happened.
            Trace.WriteLine("8. Shouldn't carry on.")
            result = True
        End If

        Return result
    End Function

    Friend Function NoPriorityTenForThisSkuYet() As Boolean
        Return Not _blnPriorityTen
    End Function

    Friend Function EventStartDateBeforeStartOfPriceChangeCreationWindow() As Boolean
        Return _eventStartDate < _today
    End Function

    Friend Function EventEndsBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return _eventEndDate < _endDateOfPriceChangeCreationWindow
    End Function

    Friend Function EventEndsOnOrBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return _eventEndDate <= _endDateOfPriceChangeCreationWindow
    End Function

    Friend Function EventEndDateOnOrBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return _latestEndingEventEndDate <= _endDateOfPriceChangeCreationWindow
    End Function

    Friend Function EventStartDateWouldBeAfterPriceChangeCreationWindow() As Boolean
        Return _latestEndingEventEndDate = _endDateOfPriceChangeCreationWindow
    End Function

    Friend Function EventStartsOnOrBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return _eventStartDate <= _endDateOfPriceChangeCreationWindow
    End Function

    Friend Function EventIsNotActiveBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return Not EventIsActiveBeforeEndOfPriceChangeCreationWindow()
    End Function

    Friend Function EventIsActiveBeforeEndOfPriceChangeCreationWindow() As Boolean
        Return EventEndsBeforeEndOfPriceChangeCreationWindow() Or EventStartsOnOrBeforeEndOfPriceChangeCreationWindow()
    End Function

    Friend Function StockAndPotentialPriceChangePricesMatch() As Boolean
        Return _stockRow.NormalSellPrice = CDec(_potentialPriceChange("PRIC"))
    End Function

    Friend Function StockAndPotentialPriceChangeEventNumbersMatch() As Boolean
        Return _stockRow.RetailPriceEventNo = CStr(_potentialPriceChange("NUMB"))
    End Function

    Friend Function StockAndPotentialPriceChangeEventPrioritiesMatch() As Boolean
        Return _stockRow.RetailPricePriority = CStr(_potentialPriceChange("PRIO"))
    End Function

    Friend Function StockAndPotentialPriceChangesMatch() As Boolean
        Return StockAndPotentialPriceChangePricesMatch() And _
            StockAndPotentialPriceChangeEventNumbersMatch() And _
            StockAndPotentialPriceChangeEventPrioritiesMatch()
    End Function

    Friend Function PotentialPriceChangeIsARegressionPrice() As Boolean
        Return Val(_potentialPriceChange("PRIO")) = REGRESSION_PRIORITY
    End Function

    Friend Function PotentialPriceChangeIsNotARegressionPrice() As Boolean
        Return Val(_potentialPriceChange("PRIO")) > REGRESSION_PRIORITY
    End Function

    Friend Function NewRegressionEventNeedsToBeAdded() As Boolean

        Dim result As Boolean = False

        If RegressionRequired() And _
           PotentialPriceChangeIsARegressionPrice() And _
           NoPriorityTenForThisSkuYet() Then
            SetRegressionNotRequired()
            If EventEndDateOnOrBeforeEndOfPriceChangeCreationWindow() Then
                'MO'C 20/09/2010 - TFS ID 2418 Ignore the Price change if it is the one currently in STKMAS
                If StockAndPotentialPriceChangePricesMatch() And _
                   EventIsNotActiveWithinPriceChangeCreationWindow() Then
                    Trace.WriteLine("2a. Event is same as current price in STKMAS so ignore.")
                Else
                    'Check to see if the Event is on the edge of the price change window, if it is ignore it.
                    If EventStartDateWouldBeAfterPriceChangeCreationWindow() Then
                        Trace.WriteLine("2b. We only do price change events seven days in advance leave for tomorrow this one.")
                    Else
                        UpdatePriceChange(_eventStartDate, _eventEndDate, _strEdat, _potentialPriceChange, _stockRow, _verifiedPriceChanges, _endDateOfPriceChangeCreationWindow)
                        Trace.WriteLine("2c. Regression Update")
                        If (_blnPriorityTen Or _potentialPriceChange.Prio = CStr(REGRESSION_PRIORITY)) And (_eventStartDate >= _latestEndingEventEndDate) Then
                            _RegressesionCheckNeeded = False
                        Else
                            _RegressesionCheckNeeded = True
                        End If
                    End If
                End If
                result = True
            End If
        End If

        Return result

    End Function

    Friend Function IsPriceChangeCurrentPrice() As Boolean
        Dim result As Boolean = False

        'MO'C 20/09/2010 - TFS ID 2418 Ignore the Price change if it is the one currently in STKMAS
        If StockAndPotentialPriceChangesMatch() Then
            Trace.WriteLine("13a. Event is same as current price in STKMAS so ignore.")
            result = True
        End If

        Return result

    End Function

    Friend Function HasPriceChangeAlreadyBeenAdded(ByVal source As DataRow) As Boolean
        Dim myQuery = From row In _verifiedPriceChanges _
                Where row.Skun = source.Skun _
                And row.Prio = source.Prio _
                And row.Numb = source.Numb _
                And row.Pric = source.Pric _
                Select row

        Return myQuery.Any()

    End Function

    Friend Sub SetRegressionRequired()
        If PotentialPriceChangeIsNotARegressionPrice() And EventEndsOnOrBeforeEndOfPriceChangeCreationWindow() Then
            _blnRegressionRequired = True
        End If
    End Sub

    Friend Sub SetRegressionNotRequired()
        _blnRegressionRequired = False
    End Sub

    Friend Function RegressionRequired() As Boolean
        Return _blnRegressionRequired
    End Function

#End Region

    Public Sub RegressAndReset(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByVal strEdat As String, ByVal Skun As String, ByVal newTable As System.Data.DataTable, ByVal rowStock As BOStock.cStock)
        Dim ConvertStockToLightWeightStock As New ConvertStockToLightWeightStock
        Dim LightWeightStock As IStock = New Stock

        LightWeightStock = ConvertStockToLightWeightStock.ConvertToLightWeightStock(rowStock)
        If _RegressesionCheckNeeded Then
            CheckForRegression(StartDate, DaysPrior, strEdat, Skun, newTable, LightWeightStock)
        End If
        SkuChangeReset()
    End Sub

    Public Sub RegressAndReset(ByVal StartDate As Date, ByVal DaysPrior As Integer, ByVal strEdat As String, ByVal Skun As String, ByVal newTable As System.Data.DataTable, ByVal rowStock As IStock)
        If _RegressesionCheckNeeded Then
            CheckForRegression(StartDate, DaysPrior, strEdat, Skun, newTable, rowStock)
        End If
        SkuChangeReset()

    End Sub

    Friend Sub SkuChangeReset()
        _blnTidiedUpStock = False
        _blnRegressionRequired = False
        _RegressesionCheckNeeded = False
        _blnPriority20PastEndDate = False
        _blnPriorityTen = False
        _blnPriorityTwenty = False
        _blnEventFinishedToday = False
        _latestEndingEventEndDate = Nothing
        _strEdat = ""
    End Sub

End Class
