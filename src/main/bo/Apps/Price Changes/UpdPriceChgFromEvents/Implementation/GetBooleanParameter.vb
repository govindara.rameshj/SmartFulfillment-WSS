﻿Public Class GetBooleanParameter
    Implements IGetBooleanParameter

    Friend Shared _simulatedResponse As Boolean = False

    Public Function GetBoolean(ByVal ParameterID As Integer) As Boolean Implements IGetBooleanParameter.GetBoolean
        If _simulatedResponse = False Then
            Return Cts.Oasys.Core.System.Parameter.GetBoolean(ParameterID)
        Else
            Return _simulatedResponse
        End If
    End Function
End Class
