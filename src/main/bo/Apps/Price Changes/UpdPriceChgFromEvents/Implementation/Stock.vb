﻿Public Class Stock

    Implements IStock

    Private _NormalSellPrice As Decimal
    Private _RetailPriceEventNo As String
    Private _RetailPricePriority As String
    Private _SkuNumber As String

    Public Property NormalSellPrice() As Decimal Implements IStock.NormalSellPrice
        Get
            NormalSellPrice = _NormalSellPrice
        End Get
        Set(ByVal value As Decimal)
            _NormalSellPrice = value
        End Set
    End Property

    Public Property RetailPriceEventNo() As String Implements IStock.RetailPriceEventNo
        Get
            RetailPriceEventNo = _RetailPriceEventNo
        End Get
        Set(ByVal value As String)
            _RetailPriceEventNo = value
        End Set
    End Property

    Public Property RetailPricePriority() As String Implements IStock.RetailPricePriority
        Get
            RetailPricePriority = _RetailPricePriority
        End Get
        Set(ByVal value As String)
            _RetailPricePriority = value
        End Set
    End Property

    Public Property SkuNumber() As String Implements IStock.SkuNumber
        Get
            SkuNumber = _SkuNumber
        End Get
        Set(ByVal value As String)
            _SkuNumber = value
        End Set
    End Property
End Class



