﻿Public Class GetData

    Private Function GetEffectiveEVTCHGRecordsForDate(ByVal dateToCheck As Date, ByVal evtchg As DataTable) As DataTable
        Dim eventChanges = evtchg

        Dim MaximumPriorityEVTCHGForSkuOnDate = From row In eventChanges _
                    Where row.Sdat <= dateToCheck _
                    And If(row.Edat, dateToCheck) >= dateToCheck _
                    And row.Idel = False _
                    Group By skun = row.Skun _
                    Into prio = Max(CInt(row!Prio))

        Dim EffectiveEVTCHGRecordKeyValues = From evtrow In eventChanges _
            , sqRow In MaximumPriorityEVTCHGForSkuOnDate _
            Where evtrow.Skun = sqRow.skun And _
                  evtrow.Prio = CStr(sqRow.prio) _
            And evtrow.Sdat <= dateToCheck _
            And If(evtrow.Edat, dateToCheck) >= dateToCheck _
            And evtrow.Idel = False _
            Group By skun = evtrow.Skun _
            Into numb = Max(CInt(evtrow!Numb))

        Dim EffectiveEVTCHGRecordsForDate = (From evtRow In eventChanges _
                                        , PrioRow In EffectiveEVTCHGRecordKeyValues _
                                        Where evtRow.Skun = PrioRow.skun And _
                                        CInt(evtRow.Numb) = PrioRow.numb _
                                        Select evtRow)

        Dim dt As New DataTable
        Dim keys(4) As DataColumn

        dt.Columns.Add("ActiveDate", System.Type.GetType("System.DateTime"))
        keys(0) = dt.Columns.Add("SKUN", System.Type.GetType("System.String"))
        keys(1) = dt.Columns.Add("SDAT", System.Type.GetType("System.DateTime"))
        keys(2) = dt.Columns.Add("PRIO", System.Type.GetType("System.String"))
        keys(3) = dt.Columns.Add("NUMB", System.Type.GetType("System.String"))
        dt.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        dt.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        For Each row As DataRow In EffectiveEVTCHGRecordsForDate
            dt.ImportRow(row)
        Next

        Return dt
    End Function

    Public Function GetEffectiveEVTCHGRecordsForDateRange(ByRef startDate As Date, ByVal DaysPrior As Integer, ByRef evtchg As System.Data.DataTable) As System.Data.DataTable
        Dim dt As New DataTable
        Dim keys(4) As DataColumn

        '********************************************************************
        'MO'C - 21 November 2010
        'Referral 902 and 888
        'Need to add one to the StartDate as the start 
        'date held in SYSDAT:TODT is yesterdays date according to Phil T.
        'Did it here so the unit tests would apply the same logic.
        startDate = DateAdd(DateInterval.Day, 1, startDate)
        '********************************************************************

        dt.Columns.Add("ActiveDate", System.Type.GetType("System.DateTime"))
        keys(0) = dt.Columns.Add("SKUN", System.Type.GetType("System.String"))
        keys(1) = dt.Columns.Add("SDAT", System.Type.GetType("System.DateTime"))
        keys(2) = dt.Columns.Add("PRIO", System.Type.GetType("System.String"))
        keys(3) = dt.Columns.Add("NUMB", System.Type.GetType("System.String"))
        dt.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        dt.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim endDate As Date = DateAdd(DateInterval.Day, DaysPrior, startDate)
        Dim datePointer As Date = startDate

        While datePointer < endDate

            Dim dtResults As DataTable = GetEffectiveEVTCHGRecordsForDate(datePointer, evtchg)

            For Each row As DataRow In dtResults.Rows
                row!ActiveDate = datePointer
                If IsRowDifferentFromPreviousRow(row, dt) Then
                    dt.ImportRow(row)
                End If
            Next

            datePointer = DateAdd(DateInterval.Day, 1, datePointer)

        End While
        Dim query = From row In dt _
                    Order By row!Skun, row!ActiveDate _
                    Select row

        evtchg.Clear()
        Dim skun As String = ""
        For Each row As DataRow In query
            If row.Sdat < row.ActiveDate Then row.SetSdat(row.ActiveDate)
            evtchg.Rows.Add(row!Skun, row!Sdat, row!Prio, row!Numb, row!Edat, row!Pric, row!Idel)
            skun = row.Skun
        Next

        Return evtchg

    End Function

    Private Function IsRowDifferentFromPreviousRow(ByVal source As DataRow, ByVal Destination As DataTable) As Boolean
        If Destination.Rows.Count = 0 Then Return True

        Dim myQuery = (From row In Destination _
                Where row.Skun = source.Skun _
                Order By row!ActiveDate Descending _
                Select row).Take(1)

        For Each row As DataRow In myQuery
            If source.Skun = row.Skun AndAlso _
               source.Sdat = row.Sdat AndAlso _
               source.Prio = row.Prio AndAlso _
               source.Numb = row.Numb AndAlso _
               source.Edat = row.Edat AndAlso _
               source.Pric = row.Pric AndAlso _
               source.Idel = row.Idel Then
                Return False
            Else
                Return True
            End If
        Next

        Return True
    End Function

    Public Function GetMaxEndDateForSku(ByVal Sku As Object, ByVal StartDate As Date, ByVal todaysDate As Date, ByVal DaysPrior As Integer, ByRef evtchg As System.Data.DataTable) As String
        Dim eventChanges = evtchg
        Dim DateToCheck As Date = DateAdd(DateInterval.Day, DaysPrior, StartDate)
        Dim strEdat As String = String.Empty

        Dim myQuery = (From row In eventChanges _
                Where row.Skun = CStr(Sku) _
                And row.Sdat >= StartDate _
                Select row).Take(1)

        For Each row As DataRow In myQuery
            strEdat = CStr(If(row.Edat, DateAdd(DateInterval.Day, -1, todaysDate)))
        Next

        Return strEdat
    End Function
End Class
