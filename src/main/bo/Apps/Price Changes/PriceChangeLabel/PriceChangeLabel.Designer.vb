<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PriceChangeLabel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.chkLabelsDeployed = New System.Windows.Forms.CheckBox
        Me.chkUserValid = New System.Windows.Forms.CheckBox
        Me.btnUserID = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(6, 83)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'chkLabelsDeployed
        '
        Me.chkLabelsDeployed.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkLabelsDeployed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLabelsDeployed.Location = New System.Drawing.Point(6, 6)
        Me.chkLabelsDeployed.Name = "chkLabelsDeployed"
        Me.chkLabelsDeployed.Size = New System.Drawing.Size(308, 20)
        Me.chkLabelsDeployed.TabIndex = 0
        Me.chkLabelsDeployed.Text = "Have priced increase labels been deployed?"
        Me.chkLabelsDeployed.UseVisualStyleBackColor = True
        '
        'chkUserValid
        '
        Me.chkUserValid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkUserValid.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUserValid.Enabled = False
        Me.chkUserValid.Location = New System.Drawing.Point(6, 32)
        Me.chkUserValid.Name = "chkUserValid"
        Me.chkUserValid.Size = New System.Drawing.Size(308, 20)
        Me.chkUserValid.TabIndex = 4
        Me.chkUserValid.Text = "Has a valid user been entered?"
        Me.chkUserValid.UseVisualStyleBackColor = True
        Me.chkUserValid.Visible = False
        '
        'btnUserID
        '
        Me.btnUserID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUserID.Location = New System.Drawing.Point(238, 83)
        Me.btnUserID.Name = "btnUserID"
        Me.btnUserID.Size = New System.Drawing.Size(76, 39)
        Me.btnUserID.TabIndex = 5
        Me.btnUserID.Text = "F5 Enter User ID"
        Me.btnUserID.UseVisualStyleBackColor = True
        Me.btnUserID.Visible = False
        '
        'PriceChangeLabel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 134)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnUserID)
        Me.Controls.Add(Me.chkUserValid)
        Me.Controls.Add(Me.chkLabelsDeployed)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "PriceChangeLabel"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Price Change Label Confirmation"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents chkLabelsDeployed As System.Windows.Forms.CheckBox
    Friend WithEvents chkUserValid As System.Windows.Forms.CheckBox
    Friend WithEvents btnUserID As System.Windows.Forms.Button

End Class
