Public Class PriceChangeLabel
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _UserId As Integer = 0
    Private _AutoUpdate As Boolean = False

    Private Sub PriceChangeLabel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Trace.WriteLine(My.Resources.AppStart, Name)

            Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoLabelsDeployed, My.Resources.AppTitleMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If result = Windows.Forms.DialogResult.Yes Then
                chkLabelsDeployed.Checked = True

            Else
                Dim result2 As DialogResult = MessageBox.Show(My.Resources.YesNoAbort, My.Resources.AppTitleMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                If result2 = Windows.Forms.DialogResult.Yes Then
                    Me.Close()
                    End
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Resources.AppTitleMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Trace.WriteLine(ex.Message, Name)
        End Try

    End Sub

    Private Sub PriceChangeLabel_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F10 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub PriceChangeLabel_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Try
            Trace.WriteLine(My.Resources.SaveRetOptions, Name)

            Dim retOptions As New BOSystem.cRetailOptions(_Oasys3DB)
            retOptions.SetAuthorisePriceChange(_AutoUpdate, _UserId.ToString("000"))

        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Resources.AppTitleMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Trace.WriteLine(ex.Message, Name)
            e.Cancel = True
        End Try

        Trace.WriteLine(My.Resources.AppExit, Name)

    End Sub




    Private Sub chkLabelsDeployed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLabelsDeployed.CheckedChanged

        chkLabelsDeployed.Enabled = False
        chkUserValid.Visible = True
        btnUserID.Visible = True
        btnUserID.PerformClick()

    End Sub

    Private Sub chkUserValid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUserValid.CheckedChanged

        btnUserID.Enabled = False

    End Sub


    Private Sub btnUserID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUserID.Click

        Try
            Using login As New Cts.Oasys.WinForm.User.UserAuthorise(Cts.Oasys.WinForm.User.LoginLevel.Manager)
                If login.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                    _UserId = login.UserId
                    _AutoUpdate = True
                    chkUserValid.Checked = True
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Resources.AppTitleMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

End Class