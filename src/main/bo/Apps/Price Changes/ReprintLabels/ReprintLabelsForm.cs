using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Diagnostics;
using System.Drawing.Printing;
using WSS.BO.DataLayer.Database;
using Cts.Oasys.WinForm.User;
using Cts.Oasys.Core;
using Cts.Oasys.Core.Net4Replacements;
using Cts.Oasys.Core.System.User;

namespace ReprintLabels
{
    public partial class ReprintLabelsForm : Cts.Oasys.WinForm.Form
    {
        FileSystem fs = new FileSystem();
        DataAccessLayer db = new DataAccessLayer();

        private Dictionary<string, string> typeFile;
        private Dictionary<string, string> filesPaths = new Dictionary<string, string>();       

        private string pathToOpenFile = string.Empty;
        private string openLabelFileName = string.Empty;

        public ReprintLabelsForm(): base()
        {
            InitializeComponent();
        }

        public ReprintLabelsForm(int userId, int workstationId, int securityLevel, string runParameters)
            : base(userId, workstationId, securityLevel, runParameters)
        {
            InitializeComponent();            
        }

        #region Methods for form's starting

        private void SetControlsToStartValues()
        {
            const int daysForReprint = 3;

            typeFile = LoadLabelTypes();
            LoadPrinters();
            ctlDateOfPrint.MinDate = DateTime.Now.AddDays(- daysForReprint);
            ctlPriceIncreases.Checked = true;
        }

        private Dictionary<string, string> LoadLabelTypes()
        {
            Dictionary<string, string> typeFile = new Dictionary<string, string>();

            try
            {
                typeFile = fs.LoadLabelTypesFromTextFile();

                ctlLabelsType.DataSource = typeFile.Select(x => x.Key).ToList();
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show("Unable to display the list of labels types.\nError: " + ex.Message);
                return typeFile;
            }

            return typeFile;
        }

        private void LoadPrinters()
        {
            ctlPrinterName.DataSource = PrinterSettings.InstalledPrinters.Cast<string>().ToList();
        }

        #endregion

        #region Metods for getting list of files' time of creation

        private void GetFilesCreationTimes()
        {
            try
            {
                if (ValidateLabelType())
                {
                    ClearTables();

                    openLabelFileName = typeFile[ctlLabelsType.Text].ToUpper();

                    filesPaths = fs.GetFilesFromDirectory(openLabelFileName, ctlPriceIncreases.Checked, ctlDateOfPrint.Value.Date);

                    foreach (var element in filesPaths)
                    {
                        ctlDatesOfPrintTable.Rows.Add(element.Key);
                    }
                }
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                MessageBox.Show("No labels have been printed since software install.  Please print labels before using this application.");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unable to display the list of ctearion dates.\nError: " + ex.Message);
            }
        }

        private void ClearTables()
        {
            filesPaths.Clear();
            ctlDatesOfPrintTable.Rows.Clear();
            ctlLabelsTable.DataSource = null;
        }

        private bool ValidateLabelType()
        {
            bool wasChosen;

            if (ctlLabelsType.Text.Trim() == string.Empty)
            {
                errorProvider.SetError(ctlLabelsType, "Label's type wasn't chosen!");
                wasChosen = false;
            }
            else
            {
                errorProvider.SetError(ctlLabelsType, String.Empty);
                wasChosen = true;
            }

            return wasChosen;
        }

        #endregion

        #region Metods for labels' list loading

        private void LoadLabels(string time)
        {
            try
            {
                pathToOpenFile = filesPaths[time];

                List<Label> labelsListwithoutSuppliers = fs.GetLabelsListFromTextFile(filesPaths[time]);

                ctlLabelsTable.DataSource = db.FillListOfLabelsWithSuppliers(labelsListwithoutSuppliers);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unable to display the list of labels to reprint\nError: " + ex.Message);
            }
        }

        #endregion

        #region Methods for printing

        private void PrepareForPrint()
        {
            if (ctlLabelsTable.Rows.Count > 0)
            {
                if (File.Exists(pathToOpenFile))
                {
                    User user = GetUser();

                    if (user != null)
                    {
                        if (user.IsManager)
                        {
                            PrintLabels();
                        }
                        else
                        {
                            MessageBox.Show("Please get a Manager to authorise.", "User is not a Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Opened file was not found", "File was not found");
                    GetFilesCreationTimes();
                }
            }
            else
            {
                MessageBox.Show("Please choose a file with labels to print", "No labels to print");
            }
        }

        private void PrintLabels()
        {
            try
            {
                const int labelsEngineExeParameterId = 502;
                const int labelsEngineLogParameterId = 503;
                const int labelsPathPatameterId = 500;

                string appName = Parameters.GetParameterValue<string>(labelsEngineExeParameterId);
                string folderName = Path.Combine(Parameters.GetParameterValue<string>(labelsPathPatameterId), this.WorkstationId.ToString().PadLeft(3, '0'));
                string pathToEngineLogFile = Parameters.GetParameterValue<string>(labelsEngineLogParameterId);

                string pathToLabelFile = fs.CopyReprintFileToLabelFile(openLabelFileName, pathToOpenFile, folderName);

                string engineParametersLine = string.Format("{0} /PD{1} /PN\"{2}\"", pathToLabelFile.Replace(".TXT", ".SGN"), folderName, ctlPrinterName.Text);

                bool werePrinted = false;

                while (!werePrinted)
                {
                    Process proc = Process.Start(appName, engineParametersLine);

                    WaitForm form = new WaitForm(() => proc.WaitForExit(), "Failed to wait for Print EnGine completion.");
                    form.ShowDialog();

                    if (File.Exists(pathToEngineLogFile))
                    {
                        if (new FileInfo(Parameters.GetParameterValue<string>(labelsEngineLogParameterId)).Length == 0)
                        {
                            if (MessageBox.Show("Please confirm that labels have printed correctly.\nClick \"No\" to reprint labels", "Confirm Labels Printed", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                werePrinted = true;
                            }
                        }
                        else
                        {
                            string engineLogMessage = new StreamReader(Parameters.GetParameterValue<string>(labelsEngineLogParameterId)).ReadToEnd();
                            MessageBox.Show("Error occurred when calling Print EnGine:\n" + engineLogMessage + "\nCall CTS for further assistance.", "EnGine Error");

                            werePrinted = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error occurred when calling Print EnGine:\nEnGine LOG-file was not found.\nPlease, check were the labels printed correctly", "EnGine Error");

                        werePrinted = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unable to print labels\nError: " + ex.Message);
            }
        }

        private User GetUser()
        {
            UserAuthorise author = new UserAuthorise(LoginLevel.Manager);

            author.ShowDialog();

            return author.User;
        }
        
        #endregion

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrepareForPrint();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ExitForm();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            GetFilesCreationTimes();
        }

        private void ReprintLabels_Load(object sender, EventArgs e)
        {
            SetControlsToStartValues();
        }

        private void ExitForm()
        {
            FindForm().Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.F7:
                    GetFilesCreationTimes();
                    return true;
                case Keys.F9:
                    PrepareForPrint();
                    return true;
                case Keys.F10:
                    ExitForm();
                    return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void ctlDatesOfPrintTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string time = ctlDatesOfPrintTable.SelectedCells[0].Value.ToString();

            LoadLabels(time);
        }
    }
}
