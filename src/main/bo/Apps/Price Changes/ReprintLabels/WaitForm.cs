﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReprintLabels
{
    public partial class WaitForm : Form
    {
        private readonly Action action;
        private readonly string errorMessagePrefix;

        public WaitForm(Action action, string errorMessagePrefix)
        {
            InitializeComponent();
            this.action = action;
            this.errorMessagePrefix = errorMessagePrefix;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Action workThread = WorkThread;
            workThread.BeginInvoke(null, null);
        }

        private void WorkThread()
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Action showError = () => MessageBox.Show(String.Format("{0}\nError message: {1}", errorMessagePrefix, ex.Message));
                Invoke(showError);
            }
            finally
            {
                Action close = () => this.Close();
                Invoke(close);
            }
        }
    }
}
