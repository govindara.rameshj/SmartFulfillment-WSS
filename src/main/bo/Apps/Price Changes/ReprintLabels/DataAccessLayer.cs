using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Database;
using Cts.Oasys.Core.Net4Replacements;

namespace ReprintLabels
{
    public class DataAccessLayer
    {
        private Lazy<Dictionary<string, string>> suppliersDict = new Lazy<Dictionary<string, string>>(GetSuppliersDictionary);

        public List<Label> FillListOfLabelsWithSuppliers(List<Label> list)
        {
            IEnumerable<string> skusList = list.Select(x => x.SKU);

            List<Cts.Oasys.Core.Net4Replacements.Tuple<string, string>> dictionaryAsList = new List<Cts.Oasys.Core.Net4Replacements.Tuple<string, string>>();

            using (var db = new OasysDb())
            {
                const int skuQuant = 1000;

                while (skusList.Any())
                {
                    dictionaryAsList.AddRange(db.StockMasterTable
                        .Where(x => skusList.Take(skuQuant).Contains(x.SkuNumber))
                        .Select(x => new Cts.Oasys.Core.Net4Replacements.Tuple<string, string>(x.SkuNumber, x.SupplierId)));

                    skusList = skusList.Skip(skuQuant);
                }
            }

            Dictionary<string, string> skusDict = dictionaryAsList.ToDictionary(x => x.Item1, x => x.Item2);

            foreach (Label label in list)
            {
                if (skusDict.ContainsKey(label.SKU))
                {
                    if (suppliersDict.Value.ContainsKey(skusDict[label.SKU]))
                    {
                        label.Supplier = suppliersDict.Value[skusDict[label.SKU]];
                    }
                    else
                    {
                        label.Supplier = "Not Found";
                    }
                }
                else
                {
                    label.Supplier = "Not Found";
                }
            }

            return list;
        }

        private static Dictionary<string, string> GetSuppliersDictionary()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            using (var db = new OasysDb())
            {
                dict = db.SupplierMasterTable.Select(t => new { SuplierNumber = t.SupplierNumber, SuplierName = t.SupplierName }).ToDictionary(t => t.SuplierNumber, t => t.SuplierName);
            }

            return dict;
        }
    }
}
