﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReprintLabels
{
    public class Label
    {
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Planogram { get; set; }
        public string Supplier { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
    }
}
