﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using Cts.Oasys.Core;
using System.Drawing.Printing;

namespace ReprintLabels
{
    public class FileSystem
    {
        const int configFileParameterId = 910;
        const int LabelsPathParameterId = 500;
        const int labelNameIndex = 0;
        const int labelFileIndex = 3;

        const string labelsNamesFile = "labeltypes.txt";
        const string emptyPlanogramSKU = "000000";
        const string folderName = "History";
        const string increaseSuffix = "INC";
        const string decreaseSuffix = "DEC";

        public Dictionary<string, string> LoadLabelTypesFromTextFile()
        {
            Dictionary<string, string> typeFile = new Dictionary<string, string>();

            using (CsvReader csv = new CsvReader(new StreamReader(Path.Combine(Parameters.GetParameterValue<string>(configFileParameterId), labelsNamesFile)), false, '\t'))
            {
                while (csv.ReadNextRecord())
                {
                    typeFile.Add(csv[labelNameIndex], csv[labelFileIndex]);
                }
            }

            return typeFile;
        }

        public Dictionary<string, string> GetFilesFromDirectory(string fileName, bool isPriceIncreased, DateTime date)
        {
            Dictionary<string, string> filesPaths = new Dictionary<string, string>();
            
            string suffix;
            
            suffix = (isPriceIncreased) ? increaseSuffix : decreaseSuffix;

            string labelsFileName = Path.GetFileNameWithoutExtension(fileName);

            DirectoryInfo directory = new DirectoryInfo(Parameters.GetParameterValue<string>(LabelsPathParameterId) + folderName);

            var files = directory.GetFiles(String.Format("*{0}*{1}*.txt", labelsFileName, suffix)).Where(x => x.CreationTime.Date == date.Date);

            filesPaths = files.ToList().Select(file => new FileInfo(file.FullName)).ToDictionary(f => f.CreationTime.ToLongTimeString(), f => f.FullName);

            return filesPaths;
        }

        public List<Label> GetLabelsListFromTextFile(string pathToOpenFile)
        {
            List<Label> labelsListwithoutSuppliers = new List<Label>();

            using (CsvReader csv = new CsvReader(new StreamReader(pathToOpenFile), true, ','))
            {
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (csv["SKU"] != emptyPlanogramSKU)
                    {
                        var label = labelsListwithoutSuppliers.FirstOrDefault(x => x.SKU == csv["SKU"] & x.Planogram == csv["PLANGRAM"]);


                        if (label == null)
                        {
                            labelsListwithoutSuppliers.Add(
                                new Label()
                                {
                                    SKU = csv["SKU"],
                                    Description = csv["DESCRIPTION"],
                                    Planogram = csv["PLANGRAM"],
                                    Supplier = null,
                                    Price = csv["PRICE"],
                                    Quantity = 1
                                });
                        }
                        else
                        {
                            label.Quantity += 1;
                        }
                    }
                }
            }

            return labelsListwithoutSuppliers;
        }

        public string CopyReprintFileToLabelFile(string labelName, string pathToOpenFile, string folderName)
        {
            string reprintFileName = Path.GetFileName(pathToOpenFile);
            string pathToLabelFile = Path.Combine(folderName, Path.ChangeExtension(labelName, "TXT"));
            
            File.Copy(pathToOpenFile, pathToLabelFile, true);

            return pathToLabelFile;
        }
    }
}
