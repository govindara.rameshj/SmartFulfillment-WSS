﻿namespace ReprintLabels
{
    partial class ReprintLabelsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReprintLabelsForm));
            this.ctlLabelsType = new System.Windows.Forms.ComboBox();
            this.ctlDateOfPrint = new System.Windows.Forms.DateTimePicker();
            this.ctlPriceIncreases = new System.Windows.Forms.RadioButton();
            this.ctlPriceDecreases = new System.Windows.Forms.RadioButton();
            this.ctlDatesOfPrintTable = new System.Windows.Forms.DataGridView();
            this.ctlFileTimeOfCreationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctlLabelsTable = new System.Windows.Forms.DataGridView();
            this.sKUDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planogramDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceForLabels = new System.Windows.Forms.BindingSource(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblPrinter = new System.Windows.Forms.Label();
            this.ctlPrinterName = new System.Windows.Forms.ComboBox();
            this.ctlSearchCriteria = new System.Windows.Forms.GroupBox();
            this.btnProcess = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ctlDatesOfPrintTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctlLabelsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceForLabels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.ctlSearchCriteria.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctlLabelsType
            // 
            this.ctlLabelsType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ctlLabelsType.FormattingEnabled = true;
            this.ctlLabelsType.Location = new System.Drawing.Point(501, 40);
            this.ctlLabelsType.Name = "ctlLabelsType";
            this.ctlLabelsType.Size = new System.Drawing.Size(200, 26);
            this.ctlLabelsType.TabIndex = 4;
            // 
            // ctlDateOfPrint
            // 
            this.ctlDateOfPrint.Location = new System.Drawing.Point(58, 40);
            this.ctlDateOfPrint.Name = "ctlDateOfPrint";
            this.ctlDateOfPrint.Size = new System.Drawing.Size(200, 24);
            this.ctlDateOfPrint.TabIndex = 1;
            // 
            // ctlPriceIncreases
            // 
            this.ctlPriceIncreases.AutoSize = true;
            this.ctlPriceIncreases.Location = new System.Drawing.Point(317, 23);
            this.ctlPriceIncreases.Name = "ctlPriceIncreases";
            this.ctlPriceIncreases.Size = new System.Drawing.Size(98, 17);
            this.ctlPriceIncreases.TabIndex = 2;
            this.ctlPriceIncreases.TabStop = true;
            this.ctlPriceIncreases.Text = "Price Increases";
            this.ctlPriceIncreases.UseVisualStyleBackColor = true;
            // 
            // ctlPriceDecreases
            // 
            this.ctlPriceDecreases.AutoSize = true;
            this.ctlPriceDecreases.Location = new System.Drawing.Point(317, 60);
            this.ctlPriceDecreases.Name = "ctlPriceDecreases";
            this.ctlPriceDecreases.Size = new System.Drawing.Size(103, 17);
            this.ctlPriceDecreases.TabIndex = 3;
            this.ctlPriceDecreases.TabStop = true;
            this.ctlPriceDecreases.Text = "Price Decreases";
            this.ctlPriceDecreases.UseVisualStyleBackColor = true;
            // 
            // ctlDatesOfPrintTable
            // 
            this.ctlDatesOfPrintTable.AllowUserToAddRows = false;
            this.ctlDatesOfPrintTable.AllowUserToDeleteRows = false;
            this.ctlDatesOfPrintTable.AllowUserToResizeRows = false;
            this.ctlDatesOfPrintTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ctlDatesOfPrintTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ctlDatesOfPrintTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlDatesOfPrintTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ctlFileTimeOfCreationColumn});
            this.ctlDatesOfPrintTable.Location = new System.Drawing.Point(18, 138);
            this.ctlDatesOfPrintTable.Name = "ctlDatesOfPrintTable";
            this.ctlDatesOfPrintTable.ReadOnly = true;
            this.ctlDatesOfPrintTable.RowHeadersVisible = false;
            this.ctlDatesOfPrintTable.Size = new System.Drawing.Size(200, 348);
            this.ctlDatesOfPrintTable.TabIndex = 2;
            this.ctlDatesOfPrintTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDatesOfPrintTable_CellClick);
            // 
            // ctlFileTimeOfCreationColumn
            // 
            this.ctlFileTimeOfCreationColumn.HeaderText = "Time of creation";
            this.ctlFileTimeOfCreationColumn.Name = "ctlFileTimeOfCreationColumn";
            this.ctlFileTimeOfCreationColumn.ReadOnly = true;
            // 
            // ctlLabelsTable
            // 
            this.ctlLabelsTable.AllowUserToAddRows = false;
            this.ctlLabelsTable.AllowUserToDeleteRows = false;
            this.ctlLabelsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlLabelsTable.AutoGenerateColumns = false;
            this.ctlLabelsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlLabelsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sKUDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.planogramDataGridViewTextBoxColumn,
            this.supplierDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn});
            this.ctlLabelsTable.DataSource = this.bindingSourceForLabels;
            this.ctlLabelsTable.Location = new System.Drawing.Point(266, 138);
            this.ctlLabelsTable.MultiSelect = false;
            this.ctlLabelsTable.Name = "ctlLabelsTable";
            this.ctlLabelsTable.ReadOnly = true;
            this.ctlLabelsTable.RowHeadersVisible = false;
            this.ctlLabelsTable.Size = new System.Drawing.Size(1069, 348);
            this.ctlLabelsTable.TabIndex = 3;
            // 
            // sKUDataGridViewTextBoxColumn
            // 
            this.sKUDataGridViewTextBoxColumn.DataPropertyName = "SKU";
            this.sKUDataGridViewTextBoxColumn.HeaderText = "SKU";
            this.sKUDataGridViewTextBoxColumn.Name = "sKUDataGridViewTextBoxColumn";
            this.sKUDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // planogramDataGridViewTextBoxColumn
            // 
            this.planogramDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.planogramDataGridViewTextBoxColumn.DataPropertyName = "Planogram";
            this.planogramDataGridViewTextBoxColumn.HeaderText = "Planogram";
            this.planogramDataGridViewTextBoxColumn.Name = "planogramDataGridViewTextBoxColumn";
            this.planogramDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // supplierDataGridViewTextBoxColumn
            // 
            this.supplierDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.supplierDataGridViewTextBoxColumn.DataPropertyName = "Supplier";
            this.supplierDataGridViewTextBoxColumn.HeaderText = "Supplier";
            this.supplierDataGridViewTextBoxColumn.Name = "supplierDataGridViewTextBoxColumn";
            this.supplierDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Quantity";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bindingSourceForLabels
            // 
            this.bindingSourceForLabels.DataSource = typeof(ReprintLabels.Label);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExit.Location = new System.Drawing.Point(18, 521);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(132, 38);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "F10 Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1203, 521);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(132, 38);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "F9 Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(423, 531);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(51, 18);
            this.lblPrinter.TabIndex = 5;
            this.lblPrinter.Text = "Printer";
            // 
            // ctlPrinterName
            // 
            this.ctlPrinterName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctlPrinterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ctlPrinterName.FormattingEnabled = true;
            this.ctlPrinterName.Location = new System.Drawing.Point(513, 528);
            this.ctlPrinterName.Name = "ctlPrinterName";
            this.ctlPrinterName.Size = new System.Drawing.Size(397, 26);
            this.ctlPrinterName.TabIndex = 6;
            // 
            // ctlSearchCriteria
            // 
            this.ctlSearchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlSearchCriteria.Controls.Add(this.btnProcess);
            this.ctlSearchCriteria.Controls.Add(this.ctlLabelsType);
            this.ctlSearchCriteria.Controls.Add(this.ctlPriceIncreases);
            this.ctlSearchCriteria.Controls.Add(this.ctlPriceDecreases);
            this.ctlSearchCriteria.Controls.Add(this.ctlDateOfPrint);
            this.ctlSearchCriteria.Location = new System.Drawing.Point(18, 20);
            this.ctlSearchCriteria.Name = "ctlSearchCriteria";
            this.ctlSearchCriteria.Size = new System.Drawing.Size(1317, 91);
            this.ctlSearchCriteria.TabIndex = 1;
            this.ctlSearchCriteria.TabStop = false;
            this.ctlSearchCriteria.Text = "Search Criteria";
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.Location = new System.Drawing.Point(1126, 33);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(132, 38);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "F7 Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // ReprintLabelsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctlSearchCriteria);
            this.Controls.Add(this.ctlPrinterName);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.ctlLabelsTable);
            this.Controls.Add(this.ctlDatesOfPrintTable);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReprintLabelsForm";
            this.Size = new System.Drawing.Size(1354, 572);
            this.Load += new System.EventHandler(this.ReprintLabels_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ctlDatesOfPrintTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctlLabelsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceForLabels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ctlSearchCriteria.ResumeLayout(false);
            this.ctlSearchCriteria.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ctlLabelsType;
        private System.Windows.Forms.DateTimePicker ctlDateOfPrint;
        private System.Windows.Forms.RadioButton ctlPriceIncreases;
        private System.Windows.Forms.RadioButton ctlPriceDecreases;
        private System.Windows.Forms.DataGridView ctlDatesOfPrintTable;
        private System.Windows.Forms.DataGridView ctlLabelsTable;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctlFileTimeOfCreationColumn;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ComboBox ctlPrinterName;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.GroupBox ctlSearchCriteria;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.BindingSource bindingSourceForLabels;
        private System.Windows.Forms.DataGridViewTextBoxColumn sKUDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planogramDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
    }
}