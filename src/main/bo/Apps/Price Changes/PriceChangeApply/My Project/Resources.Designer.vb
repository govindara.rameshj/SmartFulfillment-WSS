﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1008
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Application Exit.
        '''</summary>
        Friend ReadOnly Property AppExit() As String
            Get
                Return ResourceManager.GetString("AppExit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Applying price changes.
        '''</summary>
        Friend ReadOnly Property ApplyPriceChanges() As String
            Get
                Return ResourceManager.GetString("ApplyPriceChanges", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Application Start.
        '''</summary>
        Friend ReadOnly Property AppStart() As String
            Get
                Return ResourceManager.GetString("AppStart", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Assigning stock items to price changes.
        '''</summary>
        Friend ReadOnly Property AssignStockPriceChanges() As String
            Get
                Return ResourceManager.GetString("AssignStockPriceChanges", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F10 Cancel.
        '''</summary>
        Friend ReadOnly Property F10Cancel() As String
            Get
                Return ResourceManager.GetString("F10Cancel", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F10 Exit.
        '''</summary>
        Friend ReadOnly Property F10Exit() As String
            Get
                Return ResourceManager.GetString("F10Exit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving price changes.
        '''</summary>
        Friend ReadOnly Property GetPriceChanges() As String
            Get
                Return ResourceManager.GetString("GetPriceChanges", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving retail options.
        '''</summary>
        Friend ReadOnly Property GetRetOptions() As String
            Get
                Return ResourceManager.GetString("GetRetOptions", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving system dates.
        '''</summary>
        Friend ReadOnly Property GetSystemDates() As String
            Get
                Return ResourceManager.GetString("GetSystemDates", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Price Changes Applied.
        '''</summary>
        Friend ReadOnly Property PriceChangesApplied() As String
            Get
                Return ResourceManager.GetString("PriceChangesApplied", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to User Cancelled.
        '''</summary>
        Friend ReadOnly Property UserCancelled() As String
            Get
                Return ResourceManager.GetString("UserCancelled", resourceCulture)
            End Get
        End Property
    End Module
End Namespace
