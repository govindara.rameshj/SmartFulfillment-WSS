Imports Cts.Oasys.Core.Tests

Public Class PriceChangeApply

    Private _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _SysDates As BOSystem.cSystemDates
    Private _RetOptions As BOSystem.cRetailOptions
    Private _BackWorker As BackgroundWorker

    ''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author       : Partha Dutta
    ' Create date  : 08/08/2011
    ' Referral No  : 120-09
    ' Description  : Select existing or new implementation of price change apply business object
    ''''''''''''''''''''''''''''''''''''''''''''''''

    'Private _PriceChange As New BOStock.cPriceChange(_Oasys3DB)
    Private _PriceChange As IPriceChange

    Private Sub PriceChangeApply_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F10 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub PriceChangeApply_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Trace.WriteLine(My.Resources.AppExit, Name)

    End Sub

    Private Sub PriceChangeApply_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Refresh()

            ''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author       : Partha Dutta
            ' Create date  : 08/08/2011
            ' Referral No  : 120-09
            ' Description  : Select existing or new implementation of price change apply business object
            ''''''''''''''''''''''''''''''''''''''''''''''''

            _PriceChange = PriceChangeFactory.FactoryGet(_Oasys3DB)


            ''''''''''''''''''''''''''''''''''''''''''''''''

            Trace.WriteLine(My.Resources.AppStart, Name)
            Trace.WriteLine(My.Resources.GetSystemDates, Name)
            _SysDates.Load()

            Trace.WriteLine(My.Resources.GetRetOptions, Name)
            _RetOptions.LoadRetailOptions()

            Trace.WriteLine(My.Resources.GetPriceChanges, Name)
            _PriceChange.Load(_SysDates.Tomorrow.Value)

            If _PriceChange.PriceChanges.Count = 0 Then
                Me.Close()

            Else
                Trace.WriteLine(My.Resources.AssignStockPriceChanges, Name)
                Dim skuNumbers As New ArrayList
                For Each change As BOStock.cPriceChange In _PriceChange.PriceChanges
                    If Not skuNumbers.Contains(change.SkuNumber.Value) Then skuNumbers.Add(change.SkuNumber.Value)
                Next

                Dim stocks As New BOStock.cStock(_Oasys3DB)
                stocks.LoadStockItems(skuNumbers)
                _PriceChange.AssignStocks(stocks.Stocks)

                Trace.WriteLine(My.Resources.ApplyPriceChanges, Name)
                lblMessage.Text = My.Resources.ApplyPriceChanges
                btnExit.Text = My.Resources.F10Cancel
                pgbProgress.Visible = True

                _BackWorker = New BackgroundWorker
                _BackWorker.WorkerReportsProgress = True
                _BackWorker.WorkerSupportsCancellation = True
                AddHandler _BackWorker.DoWork, AddressOf WorkerDoWork
                AddHandler _BackWorker.ProgressChanged, AddressOf WorkerUpdate
                AddHandler _BackWorker.RunWorkerCompleted, AddressOf WorkerFinish
                _BackWorker.RunWorkerAsync()
            End If

            'Kill the TASKCOMP file.
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Name)
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub WorkerDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

        Dim lastPercent As Integer = 0
        Dim count As Integer = _PriceChange.PriceChanges.Count

        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)

        For Each change As BOStock.cPriceChange In _PriceChange.PriceChanges
            If worker.CancellationPending Then
                e.Cancel = True

            Else
                'apply price changes
                change.ApplyPriceChange(_SysDates.Tomorrow.Value, _SysDates.Today.Value, CInt(_RetOptions.MgrAuthorisedAA.Value), _RetOptions.AutoAppPriceChgAuth.Value)

                'update progress bar if value has changed
                Dim percent As Integer = CInt(_PriceChange.PriceChanges.IndexOf(change) / count * 100)
                If percent > lastPercent Then worker.ReportProgress(percent)
                lastPercent = percent
            End If
        Next

    End Sub

    Private Sub WorkerUpdate(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)

        pgbProgress.Value = e.ProgressPercentage

    End Sub

    Private Sub WorkerFinish(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)

        If e.Cancelled Then
            Trace.WriteLine(My.Resources.UserCancelled, Name)

        ElseIf e.Error IsNot Nothing Then
            Trace.WriteLine(e.Error.Message, Name)

        Else
            Trace.WriteLine(My.Resources.PriceChangesApplied, Name)
        End If

        Me.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Select Case btnExit.Text
            Case My.Resources.F10Cancel : _BackWorker.CancelAsync()
            Case My.Resources.F10Exit : Me.Close()
        End Select

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        TestEnvironmentSetup.SetupIfTestRun()
        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        _SysDates = New BOSystem.cSystemDates(_Oasys3DB)
        _RetOptions = New BOSystem.cRetailOptions(_Oasys3DB)

    End Sub
End Class