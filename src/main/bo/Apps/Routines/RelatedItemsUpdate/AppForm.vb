﻿Imports System.Configuration.ConfigurationManager
Imports System.IO

Public Class ApplicationForm
    Private _GotPack As Boolean
    Private _RemoveSingle As Boolean '

    Private _PackPrice As Decimal ' * Saved pack item price
    Private _SinglePrice As Decimal ' * Saved single item price
    Private _MarkUp As Decimal ' * Calculated markup per bulk
    Private _SinglePackVal As Decimal ' * Single Price * Number in a pack
    Private _TodaysMovement As Integer ' * Stock movement up to today for week

    Private _ShouldDelete As Boolean '   "N"	* Indicate Delete the record NOW.                *W04
    Private _STHoTOpen As Boolean ' * Indicate STHOT has been opened		 *W06
    Private _STHoAOpen As Boolean ' * Indicate STHOT has been opened		 *W06
    Private _WorkBulkTransferQuantity As Integer ' * Working Bulk transfer quantity		 *W06
    Private _WorkSingleTransferQuantity As Integer ' * Working singles transfer quantity		 *W06
    Private _WorkBulkPrice As Decimal ' * Working Bulk Price				 *W08
    Private _WorkSinglePrice As Decimal ' * Working Single Price				 *W08

    Private _OkGo As Boolean
    Private _CommsFolder As String
    Private _TransferFileDate As Date
    Private _AutoRun As Boolean

    Private _StartStock As Integer
    Private _endStock As Integer
    Private _StartReturn As Integer
    Private _StartPrice As Decimal
    Private _StartMarkDownQuantity As Integer
    Private _StartWriteOffQuantity As Integer

    Sub New()
        InitializeComponent()

        Me.Text = String.Concat(Me.Text, " - v", FileVersion(Me.GetType))
        Dim arguments() As String = Environment.GetCommandLineArgs()
        If arguments.Length >= 2 Then
            _AutoRun = arguments(1).EndsWith("',CFC')")
        End If

    End Sub

    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        MyBase.OnShown(e)
        If _AutoRun Then OKButton.PerformClick()
    End Sub


    Private Sub PerformUpdate()

        Try
            Cursor = Cursors.WaitCursor

            Label1.Text = "Checking Single Item: "
            Label1.Refresh()
            OKButton.Enabled = False
            ExitButton.Enabled = False

            ProcessRelatedItemsUpdate()

            If _STHoTOpen Then PutControl()

            SkuLabel.Text = "Complete"
            ExitButton.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Throw
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Public Sub ProcessRelatedItemsUpdate()

        Dim itemsTable As DataTable
        Dim skuRow As DataRow

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Trace.WriteLine("RelatedItemsUpdate - Retrieving COMMS folder location.")
                    com.CommandText = "SELECT StringValue FROM Parameters WHERE ParameterId = 914"
                    _CommsFolder = com.ExecuteValue.ToString
                End Using

                Using com As New Command(con)
                    Trace.WriteLine("RelatedItemsUpdate - Retrieving System Open Date.")
                    com.CommandText = "SELECT TODT FROM SYSDAT WHERE FKey = '01'"
                    _TransferFileDate = CDate(com.ExecuteValue)
                End Using

                Using com As New Command(con)
                    Trace.WriteLine("RelatedItemsUpdate - Retrieving RELITM table.")
                    com.CommandText = "SELECT SPOS, PPOS, DELC, NSPP, QTYS, WTDM, PWKM, PTDM, PPDM, YTDM FROM RELITM ORDER BY SPOS"
                    itemsTable = com.ExecuteDataTable
                End Using
            End Using

            If Not _CommsFolder.EndsWith("\") Then _CommsFolder += "\"c


            ProgressBar1.Maximum = itemsTable.Rows.Count
            ProgressBar1.Step = 1

            For Each row As DataRow In itemsTable.Rows
                ProgressBar1.PerformStep()
                CheckingLabel.Text = CStr(row.Item("spos"))
                Application.DoEvents()

                If Not CBool(row.Item("DELC")) Then
                    ReadItems(row)

                    If _ShouldDelete Then GoTo DeleteRecord
                    If Not _OkGo Then Continue For

                    _WorkSingleTransferQuantity = 0
                    _WorkBulkTransferQuantity = 0

                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*                                                                           *
                    '*     S T O C K    U P D A T E    (TAKE MARKUP ON TRANSFER OF STOCK)        *
                    '*                                                                           *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   TRANSFER STOCK FROM BULK TO SINGLE TO COVER NEGATIVE MOVEMENT THAT      *
                    '*   RESULTS IN THE SINGLE ENDING WITH NEGATIVE STOCK HOLDING.               *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
CheckTransfer:
                    skuRow = ReadStkmas(CStr(row.Item("SPOS")))
                    SetupStklog(skuRow)

                    _TodaysMovement = 0
                    _TodaysMovement += CInt(skuRow.Item("MREQ1"))
                    _TodaysMovement += CInt(skuRow.Item("MADQ1"))
                    _TodaysMovement += CInt(skuRow.Item("MIBQ1"))
                    _TodaysMovement -= CInt(skuRow.Item("MRTQ1"))
                    _TodaysMovement -= CInt(skuRow.Item("SALU2"))

                    If _TodaysMovement >= +0 Then
                        GoTo DoSTHot
                    End If

                    If CInt(skuRow.Item("ONHA")) >= 0 Then
                        GoTo CheckStats
                    End If

                    SkuLabel.Text = CStr(row.Item("SPOS"))
                    SkuLabel.Refresh()
                    _WorkSingleTransferQuantity += CInt(row.Item("NSPP"))
                    _WorkSinglePrice = CDec(skuRow.Item("Pric"))

                    Using con As New Connection
                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Single Item, SKUN {0}, for Bulk Movement.", CStr(row.Item("SPOS"))))
                            _endStock += CInt(row.Item("NSPP"))
                            com.CommandText = "UPDATE STKMAS SET ONHA = ONHA + @singlesPerPack, MBSQ1 = MBSQ1 + @singlesPerPack, MBSV1 = MBSV1 + @singlePackValue WHERE SKUN = @skun"
                            com.AddParameter("@singlesPerPack", CInt(row.Item("NSPP")))
                            com.AddParameter("@singlePackValue", _SinglePackVal)
                            com.AddParameter("@skun", CStr(row.Item("SPOS")))
                            com.ExecuteNonQuery()
                        End Using
                    End Using


                    SinglesPerPackLabel.Text = CStr(_WorkSingleTransferQuantity)
                    SinglesPerPackLabel.Refresh()

                    OutputStklog(skuRow, row)

                    skuRow = ReadStkmas(CStr(row.Item("PPOS")))
                    SetupStklog(skuRow)

                    Using con As New Connection
                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Bulk Item, SKUN {0}, for Bulk Movement.", CStr(row.Item("PPOS"))))
                            _endStock -= 1
                            com.CommandText = "UPDATE STKMAS SET ONHA = ONHA - 1, MBSQ1 = MBSQ1 - 1, MBSV1 = MBSV1 - @packPrice WHERE SKUN = @skun"
                            com.AddParameter("@packPrice", _PackPrice)
                            com.AddParameter("@skun", row.Item("PPOS"))
                            com.ExecuteNonQuery()
                        End Using
                    End Using


                    _WorkBulkTransferQuantity -= 1
                    _WorkBulkPrice = CDec(skuRow.Item("PRIC"))

                    TransferCountLabel.Text = CStr(_WorkBulkTransferQuantity)
                    TransferCountLabel.Refresh()

                    'skuRow = ReadStkmas(CStr(row.Item("PPOS")))
                    OutputStklog(skuRow, row)

                    Using con As New Connection
                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Related Items Table, Single SKUN {0}, for Bulk Movement.", CStr(row.Item("SPOS"))))
                            com.CommandText = "UPDATE RELITM SET WTDS = WTDS + NSPP, WTDM = WTDM + @markup, PTDM = PTDM + @markup, YTDM = YTDM + @markup WHERE SPOS = @skun"
                            com.AddParameter("@markup", _MarkUp)
                            com.AddParameter("@skun", row.Item("SPOS"))
                            com.ExecuteNonQuery()
                        End Using
                    End Using

                    GoTo CheckTransfer
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*                                                                           *
                    '*     S A L E S    S T A T I S T I C S   (TO KEEP SOQ CORRECT)              *
                    '*                                                                           *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   CHECK SALES STATISTICS (MOVE STAT IN EVEN NUMBER OF SINGLES PER PACK)   *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
CheckStats:
                    If CBool(row.Item("DELC")) Then GoTo CheckDelStats

                    If CInt(row.Item("QTYS")) < CInt(row.Item("NSPP")) Then
                        GoTo DoSTHot
                        Continue For
                    Else
                        GoTo ReadupSingle
                    End If
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   S I N G L E                                                             *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
DoSingle:

                    If CInt(row.Item("QTYS")) < CInt(row.Item("NSPP")) Then
                        GoTo EndSingle
                    End If

ReadupSingle:
                    Using con As New Connection
                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Single Item, SKUN {0}, for Item Movement.", CStr(row.Item("SPOS"))))
                            com.CommandText = "UPDATE STKMAS SET US001 = US001 - @singlesPerPack WHERE SKUN = @skun"
                            com.AddParameter("@singlesPerPack", CInt(row.Item("NSPP")))
                            com.AddParameter("@skun", row.Item("SPOS"))
                            com.ExecuteNonQuery()
                        End Using

                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Bulk Item, SKUN {0}, for Item Movement.", CStr(row.Item("SPOS"))))
                            com.CommandText = "UPDATE STKMAS SET US001 = US001 + 1 WHERE SKUN = @skun"
                            com.AddParameter("@skun", row.Item("PPOS"))
                            com.ExecuteNonQuery()
                        End Using
                    End Using

EndSingle:
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   NOW, REDUCE AMOUNT SOLD BUT NOT UPDATED BY NUMBER PER PACK              *
                    '*   AND ADD TO PTD AND YTD SOLD ON THE RELITM FILE                          *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    Using con As New Connection
                        Using com As New Command(con)
                            Trace.WriteLine(String.Format("RelatedItemsUpdate - Related Items Table, Single SKUN {0}, for Sales Figures.", CStr(row.Item("SPOS"))))
                            com.CommandText = "UPDATE RELITM SET QTYS = QTYS - NSPP, PTDQ = PTDQ + NSPP, YTDQ = YTDQ + NSPP WHERE SPOS = @skun"
                            com.AddParameter("@skun", row.Item("SPOS"))
                            com.ExecuteNonQuery()
                            row.Item("QTYS") = CInt(row.Item("QTYS")) - CInt(row.Item("NSPP"))
                        End Using
                    End Using

                    GoTo CheckStats
                    GoTo DoSTHot
                Else
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   CHECK WHAT TO DO FOR A DELETED RECORD                                   *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
CheckDeleted:
                    ReadItems(row)
                    If Not _OkGo Then GoTo DeleteRecord
                    If CInt(row.Item("QTYS")) > 0 Then GoTo CheckTransfer
CheckDelStats:
                    If CInt(row.Item("QTYS")) > 0 Then GoTo DoSingle

                    If CDec(row.Item("WTDM")) = +0.0 And _
                       CDec(row.Item("PWKM")) = +0.0 And _
                       CDec(row.Item("PTDM")) = +0.0 And _
                       CDec(row.Item("PPDM")) = +0.0 And _
                       CDec(row.Item("YTDM")) = +0.0 Then
                        GoTo DeleteRecord
                    End If

                    GoTo DoSTHot

DeleteRecord:
                    DeleteRelitm(CStr(row.Item("SPOS")))
                    GoTo DoSTHot

                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    '*   Output STHOT details - if necessary                              *W06   *
                    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
DoSTHot:
                    If _WorkSingleTransferQuantity = +0 And _WorkBulkTransferQuantity = +0 Then Continue For

                    Trace.WriteLine("RelatedItemsUpdate - Opening STHOT file.")
                    If Not _STHoTOpen Then System.IO.File.Delete("STHOTST.CTL ")
                    Using fileSthot As New System.IO.StreamWriter(String.Concat(_CommsFolder, "STHOT"), True)
                        _STHoTOpen = True

                        Dim sb As New System.Text.StringBuilder("BS")                               ' df-type
                        sb.Append(_TransferFileDate.ToString("dd/MM/yy"))                           ' bs-date
                        sb.Append(_WorkSingleTransferQuantity.ToString("0.00 ;0.00-").PadLeft(12))  ' bs-hash
                        sb.Append(CInt(row.Item("SPOS")).ToString.PadLeft(6, Chr(48)))              ' bs-spos
                        sb.Append(_WorkSingleTransferQuantity.ToString("0 ;0-").PadLeft(7))         ' bs-stra
                        sb.Append(CInt(row.Item("PPOS")).ToString.PadLeft(6, Chr(48)))              ' bs-ppos
                        sb.Append(_WorkBulkTransferQuantity.ToString("0 ;0-").PadLeft(7))           ' bs-ptra
                        sb.Append(_WorkBulkPrice.ToString("0.00 ;0.00-").PadLeft(10))               ' bs-ppri
                        sb.Append(_WorkSinglePrice.ToString("0.00 ;0.00-").PadLeft(10))             ' bs-wpri

                        fileSthot.WriteLine(sb.ToString)
                    End Using

                End If
            Next

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Throw
        End Try

    End Sub

    Public Shared Sub DoUpdate()

        Using f As New RelatedItemsUpdate.ApplicationForm
            f.Show()
            Application.DoEvents()
            f.PerformUpdate()
            f.ExitButton.PerformClick()
        End Using

    End Sub


    Private Shared Function FileVersion(ByVal type As Type) As String
        Dim attributes() As Object = Reflection.Assembly.GetAssembly(type).GetCustomAttributes(GetType(Reflection.AssemblyFileVersionAttribute), False)
        If attributes.Length > 0 Then
            Return CType(attributes(0), Reflection.AssemblyFileVersionAttribute).Version
        Else
            Return String.Concat(Reflection.Assembly.GetAssembly(type).GetName.Version.ToString, "a")
        End If
    End Function


    Private Sub PutControl()
        Trace.WriteLine("RelatedItemsUpdate - Writing control record.")
        With New System.IO.StreamWriter("STHOTST.CTL", False)
            .WriteLine(Date.Today.ToString("dd/MM/yy"))
            .Close()
        End With
    End Sub

    Private Sub ReadItems(ByVal row As DataRow)
        'READ THE ITEMS TO SEE IF ON FILE (SAVE AND CALC SOME STUFF)
        _OkGo = False
        _ShouldDelete = False
        _RemoveSingle = False
        _GotPack = False

        Trace.WriteLine(String.Format("RelatedItemsUpdate - Setting up working fields for SKUN {0}.", CStr(row.Item("SPOS"))))
        If CStr(row.Item("PPOS")) = New String("0"c, 6) Then
            _ShouldDelete = True
            Exit Sub
        End If

        Dim skurow As DataRow = ReadStkmas(CStr(row.Item("SPOS")))
        If skurow Is Nothing Then
            _ShouldDelete = True
            Exit Sub
        End If

        If Not (CBool(skurow.Item("IRIS"))) Then _RemoveSingle = True
        _SinglePrice = CDec(skurow.Item("PRIC"))

        skurow = ReadStkmas(CStr(row.Item("PPOS")))
        If skurow Is Nothing Then
            CheckDeleteSingle(row)
            Exit Sub
        End If

        _GotPack = True
        _PackPrice = CDec(skurow.Item("PRIC"))
        _SinglePackVal = _SinglePrice * CDec(row.Item("NSPP"))
        _MarkUp = _SinglePackVal - _PackPrice

        If _RemoveSingle Then
            CheckDeleteSingle(row)
        End If
        _OkGo = True

    End Sub

    Private Sub CheckDeleteSingle(ByRef row As DataRow)

        DeleteRelitm(CStr(row.Item("SPOS")))

        If Not _GotPack Then Exit Sub

        ' math line below equivalent to: IRIB = Math.Max(0, IRIB - 1)
        Using con As New Connection
            Using com As New Command(con)
                Trace.WriteLine(String.Format("RelatedItemsUpdate - Updating Related Items table, Bulk SKUN {0}, for Related Items Single deletion.", CStr(row.Item("PPOS"))))
                com.CommandText = "UPDATE STKMAS SET IRIB = ((IRIB - 1) + Abs(IRIB - 1)) / 2 WHERE SKUN = @skun"
                com.AddParameter("@skun", row.Item("PPOS"))
                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub


    Private Sub SetupStklog(ByVal skuDataRow As DataRow)
        _StartStock = CInt(skuDataRow.Item("ONHA"))
        _endStock = _StartStock
        _StartReturn = CInt(skuDataRow.Item("RETQ"))
        _StartPrice = CDec(skuDataRow.Item("PRIC"))
        _StartMarkDownQuantity = CInt(skuDataRow.Item("MDNQ"))
        _StartWriteOffQuantity = CInt(skuDataRow.Item("WTFQ"))
    End Sub

    Private Function ReadStkmas(ByVal skun As String) As DataRow

        Using con As New Connection
            Using com As New Command(con)
                Trace.WriteLine(String.Format("RelatedItemsUpdate - Reading STKMAS - SKUN {0}.", skun))
                com.CommandText = "SELECT SKUN, ONHA, RETQ, IRIS, PRIC, MREQ1, MADQ1, MIBQ1, MRTQ1, SALU2, MDNQ, WTFQ FROM STKMAS WHERE SKUN = @skun"
                com.AddParameter("@skun", skun)
                Dim returnTable As DataTable = com.ExecuteDataTable
                If returnTable.Rows.Count = 0 Then Return Nothing
                Return returnTable.Rows(0)
            End Using
        End Using

    End Function

    Private Sub DeleteRelitm(ByVal skun As String)

        Using con As New Connection
            Using com As New Command(con)
                Trace.WriteLine(String.Format("RelatedItemsUpdate - Deleting Related Items Single, SKUN {0}.", skun))
                com.CommandText = "DELETE FROM RELITM WHERE SPOS = @skun"
                com.AddParameter("@skun", skun)
                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub

    Private Sub OutputStklog(ByVal skuDataRow As DataRow, ByVal relatedItemRow As DataRow)

        Using con As New Connection
            Using com As New Command(con)
                Trace.WriteLine(String.Format("RelatedItemsUpdate - Preparing STKLOG record - SKUN {0}.", CStr(relatedItemRow.Item("SPOS"))))
                com.CommandText = "INSERT INTO STKLOG (SKUN,  DAYN,  TYPE,  DATE1,  TIME,  KEYS,  EEID,  ICOM,  SSTK,  ESTK,  SRET,  ERET,  SPRI,  EPRI,  SMDN,  EMDN,  SWTF,  EWTF, SPARE)" & _
                   "VALUES(@SKUN, @DAYN, @TYPE, @DATE1, @TIME, @KEYS, @EEID, @ICOM, @SSTK, @ESTK, @SRET, @ERET, @SPRI, @EPRI, @SMDN, @EMDN, @SWTF, @EWTF, '') "

                com.AddParameter("@SKUN", skuDataRow("SKUN").ToString)
                com.AddParameter("@DAYN", CInt((Today - CDate("01/01/1900")).TotalDays + 1))
                com.AddParameter("@TYPE", "51")
                com.AddParameter("@DATE1", Date.Today)
                com.AddParameter("@TIME", Date.Now.ToString("HHmmss"))
                com.AddParameter("@KEYS", CStr(relatedItemRow.Item("SPOS")) + New String(" "c, 4) + CStr(relatedItemRow.Item("PPOS")))
                com.AddParameter("@EEID", "999")
                com.AddParameter("@ICOM", 0)
                com.AddParameter("@SSTK", _StartStock)
                com.AddParameter("@ESTK", _endStock)
                com.AddParameter("@SRET", _StartReturn)
                com.AddParameter("@ERET", CInt(skuDataRow.Item("RETQ")))
                com.AddParameter("@SPRI", _StartPrice)
                com.AddParameter("@EPRI", CDec(skuDataRow.Item("PRIC")))
                com.AddParameter("@SMDN", _StartMarkDownQuantity)
                com.AddParameter("@EMDN", CInt(skuDataRow.Item("MDNQ")))
                com.AddParameter("@SWTF", _StartWriteOffQuantity)
                com.AddParameter("@EWTF", CInt(skuDataRow.Item("WTFQ")))

                Trace.WriteLine(String.Format("RelatedItemsUpdate - Writing STKLOG record - SKUN {0}.", skuDataRow("SKUN").ToString))
                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub


    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click

        OKButton.Enabled = False
        ExitButton.Enabled = False
        Application.DoEvents()

        PerformUpdate()

        If _AutoRun Then
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
            ExitButton.PerformClick()
        End If

    End Sub

    Private Sub AbortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

End Class
