﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ApplicationForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.CheckingLabel = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.SkuLabel = New System.Windows.Forms.Label
        Me.SinglesPerPackLabel = New System.Windows.Forms.Label
        Me.TransferCountLabel = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ExitButton = New System.Windows.Forms.Button
        Me.OKButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Update Pack Items for Singles Sold?"
        '
        'CheckingLabel
        '
        Me.CheckingLabel.AutoSize = True
        Me.CheckingLabel.Location = New System.Drawing.Point(146, 9)
        Me.CheckingLabel.Name = "CheckingLabel"
        Me.CheckingLabel.Size = New System.Drawing.Size(16, 13)
        Me.CheckingLabel.TabIndex = 1
        Me.CheckingLabel.Text = "   "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Process Update for:"
        '
        'SkuLabel
        '
        Me.SkuLabel.AutoSize = True
        Me.SkuLabel.Location = New System.Drawing.Point(146, 31)
        Me.SkuLabel.Name = "SkuLabel"
        Me.SkuLabel.Size = New System.Drawing.Size(16, 13)
        Me.SkuLabel.TabIndex = 3
        Me.SkuLabel.Text = "   "
        '
        'SinglesPerPackLabel
        '
        Me.SinglesPerPackLabel.AutoSize = True
        Me.SinglesPerPackLabel.Location = New System.Drawing.Point(201, 31)
        Me.SinglesPerPackLabel.Name = "SinglesPerPackLabel"
        Me.SinglesPerPackLabel.Size = New System.Drawing.Size(13, 13)
        Me.SinglesPerPackLabel.TabIndex = 5
        Me.SinglesPerPackLabel.Text = "  "
        '
        'TransferCountLabel
        '
        Me.TransferCountLabel.AutoSize = True
        Me.TransferCountLabel.Location = New System.Drawing.Point(201, 44)
        Me.TransferCountLabel.Name = "TransferCountLabel"
        Me.TransferCountLabel.Size = New System.Drawing.Size(16, 13)
        Me.TransferCountLabel.TabIndex = 6
        Me.TransferCountLabel.Text = "   "
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 56)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(239, 23)
        Me.ProgressBar1.TabIndex = 7
        '
        'ExitButton
        '
        Me.ExitButton.Location = New System.Drawing.Point(176, 94)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(75, 23)
        Me.ExitButton.TabIndex = 4
        Me.ExitButton.Text = "E&xit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(95, 94)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 8
        Me.OKButton.Text = "&OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'ApplicationForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(263, 129)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.TransferCountLabel)
        Me.Controls.Add(Me.SinglesPerPackLabel)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SkuLabel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CheckingLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ApplicationForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Related Items Update"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CheckingLabel As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SkuLabel As System.Windows.Forms.Label
    Friend WithEvents SinglesPerPackLabel As System.Windows.Forms.Label
    Friend WithEvents TransferCountLabel As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents OKButton As System.Windows.Forms.Button

End Class
