﻿Public Class HouseKeeping
    Private Enum EditTypes
        None
        [New]
        Edit
        Delete
    End Enum
    Private Enum Criteria
        SelectGroup = 0
        Connected
        TableField
        Comparison
        ValueOrTable
        EditStatus
        Sequence
        Join
        Level
        Period
        PeriodValue
    End Enum
    Private Enum Effects
        TableField
        Action
        Value
        EditStatus
        Sequence
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _BOGroup As New BOHouseKeep.cHouseKeepGroup(_Oasys3DB)
    Private _BOTables As New BOHouseKeep.cHouseKeepEffects(_Oasys3DB)
    Private _BOCriteria As New BOHouseKeep.cHouseKeepCriteria(_Oasys3DB)
    Private _BOSchedule As New BONightSchedule.cNightSchedule(_Oasys3DB)

    Private _EditType As EditTypes = EditTypes.None
    Private _ChangesMade As Boolean = False
    Private _SelectedIndex As Integer
    Private _Grid1SelectedRow As Integer = 0

    Private DTGroup As New DataTable
    Private selectedGroup As String = String.Empty

    Private EffectsLineCount As Integer = 0
    Private EffLastLine As Integer = 0
    Private CriteriaLineCount As Integer = 0
    Private CriLastLine As Integer = 0
    Private selectedRow As Integer = 0
    Private selectedCol As Integer = 0
    Private changedRow As Integer = 0
    Private changedcol As Integer = 0
    Private changed As Boolean = False

    Private _blnActive As Boolean = False
    Private _blnValidationError As Boolean = False
    Private _blnComboChanged As Boolean = False
    Private _blnRowHeaderClick As Boolean = False
    Private _blnNewRowCreated As Boolean = False
    Private _blnUser As Boolean = False


    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)

        If _ChangesMade = True Then
            Dim Result As DialogResult = MessageBox.Show("The current changes be lost. " & vbNewLine & "Do you wish to continue", "HouseKeeping", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.No Then e.Cancel = True
        End If

    End Sub

    Protected Overrides Sub DoProcessing()

        Dim AncestorFocusedMap As InputMap = FpGridEffects.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap As InputMap = FpGridEffects.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)

        Dim AncestorFocusedMap2 As InputMap = FpGridCriteria.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap2 As InputMap = FpGridCriteria.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap2.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap2.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap2.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap2.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap2.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)

        Dim AncestorFocusedMap3 As InputMap = FpGridSched.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap3 As InputMap = FpGridSched.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap3.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap3.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap3.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap3.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap3.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)


        CboGroup.DataSource = Nothing
        CboGroup.Items.Clear()

        DTGroup = _BOGroup.ListGroups
        CboGroup.DataSource = DTGroup
        CboGroup.ValueMember = DTGroup.Columns(0).ColumnName
        CboGroup.DisplayMember = DTGroup.Columns(1).ColumnName


        If SecurityLevel <= 5 Then
            _blnUser = True
            txtBOName.ReadOnly = True
            txtClassName.ReadOnly = True
            btnAddGroup.Visible = False
            btnAddGroup.Enabled = False
        Else
            _blnUser = False
            txtBOName.ReadOnly = False
            txtClassName.ReadOnly = False
            btnAddGroup.Visible = True
            btnAddGroup.Enabled = True
        End If

        btnCancel.Visible = False
        btnCancel.Enabled = False
        btnCreate.Visible = False
        btnCreate.Enabled = False
        txtAddGroup.Visible = False
        txtAddGroup.Enabled = False
        lblBOName.Visible = False
        txtBOName.Visible = False
        lblClassName.Visible = False
        txtClassName.Visible = False
        lblGroup.Visible = True
        CboGroup.Visible = True
        CboGroup.Enabled = True
        CboGroup.Focus()

        GroupBox1.Visible = False
        btnSave.Visible = False
        btnReset.Visible = False

        _ChangesMade = False

    End Sub



    Private Sub Fillgrids()

        Try
            _ChangesMade = False

            selectedCol = -1
            selectedRow = -1
            changed = False

            Dim NitmasFound As Boolean = False
            FillSchedule(CInt(CboGroup.SelectedValue), NitmasFound)
            FillCriteria(CInt(CboGroup.SelectedValue))
            FillEffects(CInt(CboGroup.SelectedValue))

            btnSave.Visible = False
            btnReset.Visible = True
            lblBOName.Visible = True
            txtBOName.Visible = True
            lblClassName.Visible = True
            txtClassName.Visible = True
            Me.Refresh()

            If NitmasFound = False Then
                MessageBox.Show("HouseKeeping task has no NITMAS task", "HouseKeepMaintenance", MessageBoxButtons.OK)
            End If

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Name)
        End Try


    End Sub 'Fillgrids()

    Private Sub FillSchedule(ByVal id As Integer, ByRef NitmasFound As Boolean)

        Dim lstRows As List(Of BONightSchedule.cNightSchedule)
        Dim LineCount As Integer = 0
        Dim strSched As String = String.Empty

        lstRows = _BOSchedule.GetHseKeepSchedule(CStr(id))

        LineCount = 0
        FpGridSched_Sheet1.RowCount = 0

        If Not IsNothing(lstRows) Then
            NitmasFound = True
            For Each row As BONightSchedule.cNightSchedule In lstRows
                FpGridSched_Sheet1.AddRows(LineCount, 1)
                FpGridSched_Sheet1.SetText(LineCount, 0, Trim(row.SetNo.Value))
                FpGridSched_Sheet1.SetText(LineCount, 1, Trim(row.TaskNo.Value))
                FpGridSched_Sheet1.SetText(LineCount, 2, Trim(row.Description.Value))
                If row.MonthlyRun.Value = True Then
                    FpGridSched_Sheet1.SetText(LineCount, 3, "Monthly")
                ElseIf row.WeeklyRun.Value = True Then

                    strSched = "Weekly"
                    If row.DayWK1.Value = True Then
                        strSched = strSched & " Monday"
                    End If
                    If row.DayWK2.Value = True Then
                        strSched = strSched & " Tuesday"
                    End If
                    If row.DayWK3.Value = True Then
                        strSched = strSched & " Wednesday"
                    End If
                    If row.DayWK4.Value = True Then
                        strSched = strSched & " Thursday"
                    End If
                    If row.DayWK5.Value = True Then
                        strSched = strSched & " Friday"
                    End If
                    If row.DayWK6.Value = True Then
                        strSched = strSched & " Saturday"
                    End If
                    If row.DayWK7.Value = True Then
                        strSched = strSched & " Sunday"
                    End If

                    FpGridSched_Sheet1.SetText(LineCount, 3, strSched)

                End If

                LineCount = LineCount + 1
            Next
        Else
            NitmasFound = False
        End If

        If _blnUser = False Then
            btnCriteriaAdd.Visible = True
            btnCriteriaAdd.Enabled = True
            btnCriteriaDelete.Visible = True
            btnCriteriaDelete.Enabled = True
        Else
            btnCriteriaAdd.Visible = False
            btnCriteriaAdd.Enabled = False
            btnCriteriaDelete.Visible = False
            btnCriteriaDelete.Enabled = False
        End If

    End Sub ' FillSchedule()

    Private Sub FillCriteria(ByVal id As Integer)
        Dim dt As DataTable
        Dim LineCount As Integer = 0

        dt = _BOCriteria.GetCriteria(id)

        LineCount = 0
        FpGridCriteria_Sheet1.RowCount = 0
        CriteriaLineCount = 0
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    FpGridCriteria_Sheet1.AddRows(LineCount, 1)
                    CriteriaLineCount = CriteriaLineCount + 1
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.SelectGroup, Trim(row.Item("SelectGroup").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Connected, Trim(row.Item("Connected").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.TableField, Trim(row.Item("BOTableName").ToString) & ":" & Trim(row.Item("Field").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Comparison, Trim(row.Item("TestType").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.EditStatus, CStr(EditTypes.None))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Sequence, Trim(row.Item("Sequence").ToString))
                    If Trim(row.Item("TestField").ToString) > String.Empty Then
                        FpGridCriteria_Sheet1.SetText(LineCount, Criteria.ValueOrTable, Trim(row.Item("TestValueOrTable").ToString) & ":" & Trim(row.Item("Testfield").ToString))
                    Else
                        FpGridCriteria_Sheet1.SetText(LineCount, Criteria.ValueOrTable, Trim(row.Item("TestValueOrTable").ToString))
                    End If

                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Join, Trim(row.Item("WhereJoin").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Level, Trim(row.Item("AccessLevel").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.Period, Trim(row.Item("PeriodType").ToString))
                    FpGridCriteria_Sheet1.SetText(LineCount, Criteria.PeriodValue, Trim(row.Item("PeriodValue").ToString))
                    If _blnUser = False Then
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.SelectGroup).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Connected).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.TableField).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Comparison).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.ValueOrTable).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Join).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Level).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Period).Locked = False
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.PeriodValue).Locked = False
                    Else
                        If Trim(row.Item("AccessLevel").ToString) = "User" Then
                            FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Period).Locked = False
                            FpGridCriteria_Sheet1.Cells(LineCount, Criteria.PeriodValue).Locked = False
                        End If
                    End If
                    If LineCount = 0 Then
                        FpGridCriteria_Sheet1.Cells(LineCount, Criteria.Join).Locked = True
                    End If


                    LineCount = LineCount + 1
                Next
            End If
        End If

        CriLastLine = CriteriaLineCount


    End Sub 'FillCriteria()

    Private Sub FillEffects(ByVal id As Integer)

        Dim dt As DataTable = _BOTables.GetTables(id)
        Dim ColCount As Integer = 0

        EffectsLineCount = 0
        FpGridEffects_Sheet1.RowCount = 0
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    FpGridEffects_Sheet1.AddRows(EffectsLineCount, 1)
                    FpGridEffects_Sheet1.SetText(EffectsLineCount, 0, Trim(row.Item("BOTableName").ToString))
                    FpGridEffects_Sheet1.SetText(EffectsLineCount, 1, Trim(row.Item("Action").ToString))
                    FpGridEffects_Sheet1.SetText(EffectsLineCount, 2, Trim(row.Item("ActionValue").ToString))
                    FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.EditStatus, CStr(EditTypes.None))
                    FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.Sequence, row.Item("Sequence").ToString)
                    If _blnUser = False Then
                        FpGridEffects_Sheet1.Cells(EffectsLineCount, 0).Locked = False
                        FpGridEffects_Sheet1.Cells(EffectsLineCount, 1).Locked = False
                        FpGridEffects_Sheet1.Cells(EffectsLineCount, 2).Locked = False
                    End If

                    'ColCount = ColCount + 1
                    'If ColCount = 3 Then
                    '    ColCount = 0
                    EffectsLineCount = EffectsLineCount + 1
                    ' FpGridEffects_Sheet1.AddRows(EffectsLineCount, 1)
                    'End If

                Next
            End If
        End If
        EffectsLineCount = EffectsLineCount - 1
        EffLastLine = EffectsLineCount

        If _blnUser = False Then
            btnEffectsAdd.Visible = True
            btnEffectsAdd.Enabled = True
            btnEffectsDelete.Visible = True
            btnEffectsDelete.Enabled = True
            If dt.Rows.Count > 0 Then
                ' on last record ensure blank cells can be edited
                FpGridEffects_Sheet1.Cells(EffLastLine, 0).Locked = False
                FpGridEffects_Sheet1.Cells(EffLastLine, 1).Locked = False
                FpGridEffects_Sheet1.Cells(EffLastLine, 2).Locked = False
            End If
        Else
            btnEffectsAdd.Visible = False
            btnEffectsAdd.Enabled = False
            btnEffectsDelete.Visible = False
            btnEffectsDelete.Enabled = False
        End If




    End Sub 'FillEffects



    Private Sub CboGroup_SelectedIndexCommited(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CboGroup.SelectionChangeCommitted
        If _ChangesMade = False Then
            _SelectedIndex = CboGroup.SelectedIndex
            displaySelection()
        Else
            CboGroup.SelectedIndex = _SelectedIndex
        End If
    End Sub

    Private Sub displaySelection()

        Dim dr As DataRowView = CType(CboGroup.SelectedItem, DataRowView)
        txtBOName.Text = CStr(dr.Row.Item(2))
        txtClassName.Text = CStr(dr.Row.Item(3))

        selectedGroup = CStr(CboGroup.SelectedValue)
        txtGroupID.Text = CStr(CboGroup.SelectedValue)

        Fillgrids()
        GroupBox1.Visible = True

    End Sub

    Private Sub btnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGroup.Click

        btnAddGroup.Visible = False
        btnAddGroup.Enabled = False

        btnCancel.Visible = True
        btnCancel.Enabled = True

        btnCreate.Visible = True
        btnCreate.Enabled = True

        txtAddGroup.Visible = True
        txtAddGroup.Enabled = True

        CboGroup.Visible = False
        CboGroup.Enabled = False

        lblBOName.Visible = False
        lblClassName.Visible = False

        txtAddGroup.Text = String.Empty

        txtAddGroup.Focus()

    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        Dim intIndex As Integer
        If txtAddGroup.Text <> String.Empty Then
            _BOGroup.AddGroup(txtAddGroup.Text)

            DoProcessing()
            intIndex = CboGroup.FindString(txtAddGroup.Text)
            If intIndex > -1 Then
                CboGroup.SelectedIndex = intIndex
                displaySelection()
            End If
        End If

        DisplayWarning(String.Format("Ensure NITMAS task is created to call Housekeeping Task {0}", CboGroup.SelectedValue))

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        btnAddGroup.Visible = True
        btnAddGroup.Enabled = True

        btnCancel.Visible = False
        btnCancel.Enabled = False

        btnCreate.Visible = False
        btnCreate.Enabled = False

        txtAddGroup.Visible = False
        txtAddGroup.Enabled = False

        CboGroup.Visible = True
        CboGroup.Enabled = True

    End Sub



    Private Sub FpGridEffects_editmodeoff(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FpGridEffects.EditModeOff

        Dim valok As Boolean = False
        Dim intRow As Integer = 0
        Dim intCol As Integer = 0
        _blnValidationError = False

        intRow = FpGridEffects_Sheet1.ActiveRowIndex
        intCol = FpGridEffects_Sheet1.ActiveColumnIndex
        If FpGridEffects_Sheet1.GetText(FpGridEffects_Sheet1.ActiveRow.Index, 0) <> String.Empty Then
            Select Case FpGridEffects_Sheet1.GetText(FpGridEffects_Sheet1.ActiveRow.Index, Effects.Action)
                Case "Update"
                    If intCol = Effects.TableField Then
                        valok = ValidateTableField(FpGridEffects_Sheet1.GetText(intRow, intCol), CInt(selectedGroup))
                        If valok = False Then
                            DisplayWarning("Either Table or Field does not exist")
                            _blnValidationError = True
                            FpGridEffects_Sheet1.SetText(intRow, intCol, String.Empty)
                            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol)
                        Else
                            FpGridEffects_Sheet1.SetActiveCell(intRow, Effects.Value)
                            _blnValidationError = False
                        End If
                    End If
                Case "Purge"
                    If intCol = Effects.TableField Then
                        valok = ValidateTableField(FpGridEffects_Sheet1.GetText(FpGridEffects_Sheet1.ActiveRowIndex, Effects.TableField), CInt(selectedGroup))
                        If valok = True Then
                            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol + 1)
                            _blnValidationError = False
                        Else
                            DisplayWarning("Either Table or Field does not exist")
                            _blnValidationError = True
                            FpGridEffects_Sheet1.SetText(intRow, intCol, String.Empty)
                            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol)
                        End If
                    End If
                    If intCol = Effects.Value Then

                        valok = ValidateTableField(FpGridEffects_Sheet1.GetText(FpGridEffects_Sheet1.ActiveRowIndex, Effects.Value), CInt(selectedGroup))
                        If valok = True Then
                            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol + 1)
                            _blnValidationError = False
                        Else
                            DisplayWarning("Either Table or Field does not exist")
                            _blnValidationError = True
                            FpGridEffects_Sheet1.SetText(intRow, intCol, String.Empty)
                            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol)
                        End If
                    End If
                Case Else
                    DisplayWarning("Action Type must be entered")
                    _blnValidationError = True
            End Select
        End If

        If intCol = Effects.Action Then
            FpGridEffects_Sheet1.SetActiveCell(intRow, intCol + 1)
        End If

        If CDbl(FpGridEffects_Sheet1.GetText(FpGridEffects_Sheet1.ActiveRow.Index, Effects.EditStatus)) = EditTypes.None Then
            FpGridEffects_Sheet1.SetText(FpGridEffects_Sheet1.ActiveRow.Index, Effects.EditStatus, CStr(EditTypes.Edit))
        End If

        _ChangesMade = True
        SetSaveButton()

    End Sub

    Private Sub FpGridEffects_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles FpGridEffects.LeaveCell
        If _blnValidationError = True Then
            e.Cancel = True
        End If
    End Sub

    Private Sub btnEffectsDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEffectsDelete.Click

        fpEffects_deleteRow()
        _ChangesMade = True
        SetSaveButton()

    End Sub

    Private Sub btnEffectsAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEffectsAdd.Click

        EffectsLineCount = EffectsLineCount + 1
        FpGridEffects_Sheet1.AddRows(EffectsLineCount, 1)
        If EffectsLineCount - 1 >= 0 Then
            FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.Sequence, CStr(CDbl(FpGridEffects_Sheet1.GetValue(EffectsLineCount - 1, Effects.Sequence)) + 1))
        Else
            FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.Sequence, "1")
        End If
        FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.Action, "Update")
        FpGridEffects_Sheet1.Cells(EffectsLineCount, Effects.TableField).Locked = False
        FpGridEffects_Sheet1.Cells(EffectsLineCount, Effects.Action).Locked = False
        FpGridEffects_Sheet1.Cells(EffectsLineCount, Effects.Value).Locked = False
        FpGridEffects_Sheet1.SetText(EffectsLineCount, Effects.EditStatus, CStr(EditTypes.New))
        FpGridEffects_Sheet1.SetActiveCell(EffectsLineCount, Effects.TableField)
        FpGridEffects.SetViewportTopRow(0, 0, EffectsLineCount)
        EffLastLine = EffectsLineCount

        FpGridEffects.Focus()

        SetSaveButton()

    End Sub

    Private Sub fpEffects_deleteRow()

        If _blnRowHeaderClick = True Then
            If FpGridEffects.ActiveSheet.GetText(_Grid1SelectedRow, 0) <> String.Empty And FpGridEffects.ActiveSheet.ActiveRowIndex > 0 Then
                FpGridEffects.ActiveSheet.SetRowVisible(_Grid1SelectedRow, False)
                FpGridEffects.ActiveSheet.SetValue(_Grid1SelectedRow, Effects.EditStatus, EditTypes.Delete)
                EffRenumber()
            End If
        Else
            DisplayWarning("Select row header to indicate row to delete")
        End If

    End Sub

    Private Sub EffRenumber()

        Dim intCount As Integer = 0
        Dim rowCount As Integer = 0

        rowCount = 1
        For intCount = 0 To FpGridEffects_Sheet1.Rows.Count - 1
            If FpGridEffects.ActiveSheet.GetRowVisible(intCount) = True Then
                FpGridEffects.Sheets(0).Rows(intCount).Label = CStr(rowCount)
                rowCount = rowCount + 1
            End If
        Next

    End Sub

    Private Sub fpEffects_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles FpGridEffects.CellClick
        selectedCol = e.Column
        selectedRow = e.Row
        _blnRowHeaderClick = e.RowHeader
        _Grid1SelectedRow = e.Row
        _blnNewRowCreated = False
    End Sub




    Private Sub FpGridCriteria_SubEditorOpening(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SubEditorOpeningEventArgs) Handles FpGridCriteria.SubEditorOpening
        If (1 = 1) Then
            e.Cancel = True
        End If
    End Sub

    Private Sub btnCriteriaDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCriteriaDelete.Click

        If selectedRow = -1 Then
            DisplayWarning("No row selected")
        Else
            FpGridCriteria.ActiveSheet.SetRowVisible(selectedRow, False)
            FpGridCriteria_Sheet1.SetText(selectedRow, Criteria.EditStatus, CStr(EditTypes.Delete))
            selectedRow = -1
            selectedCol = -1
            _ChangesMade = True
            CriRenumber()
        End If

        SetSaveButton()

    End Sub

    Private Sub CriRenumber()
        Dim intCount As Integer = 0
        Dim rowCount As Integer = 0

        rowCount = 1
        For intCount = 0 To FpGridCriteria_Sheet1.Rows.Count - 1
            If FpGridCriteria.ActiveSheet.GetRowVisible(intCount) = True Then
                FpGridCriteria.Sheets(0).Rows(intCount).Label = CStr(rowCount)
                rowCount = rowCount + 1
            End If
        Next
    End Sub

    Private Sub btnCriteriaAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCriteriaAdd.Click

        Dim intNextSequence As Integer = _BOCriteria.GetNextSequence(CInt(selectedGroup))

        CriteriaLineCount = CriteriaLineCount + 1
        FpGridCriteria_Sheet1.AddRows(CriteriaLineCount - 1, 1)
        FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.EditStatus, "1")
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.SelectGroup).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Connected).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.TableField).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Comparison).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.ValueOrTable).Locked = False
        FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.SelectGroup, "0")
        If CriteriaLineCount - 2 >= 0 Then
            FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.Sequence, CStr(intNextSequence + 1))
        Else
            FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.Sequence, "1")
        End If
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Sequence).Column.Visible = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Sequence).Locked = True
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Join).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Level).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.Period).Locked = False
        FpGridCriteria_Sheet1.Cells(CriteriaLineCount - 1, Criteria.PeriodValue).Locked = False
        FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.PeriodValue, "0")
        FpGridCriteria_Sheet1.SetText(CriteriaLineCount - 1, Criteria.EditStatus, CStr(EditTypes.New))

        FpGridCriteria_Sheet1.SetActiveCell(CriteriaLineCount - 1, 0)
        FpGridCriteria.SetViewportTopRow(0, 0, CriteriaLineCount - 1)
        FpGridCriteria.Focus()

        CriRenumber()
        CriLastLine = CriteriaLineCount
        _ChangesMade = True

        SetSaveButton()

    End Sub

    Private Sub FpGridCriteria_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles FpGridCriteria.CellClick
        selectedRow = e.Row
        selectedCol = e.Column
        If e.Column = Criteria.Period Then
            _blnComboChanged = True
        End If
    End Sub

    Private Sub FpGridCriteria_EditChange(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.EditorNotifyEventArgs) Handles FpGridCriteria.EditChange
        changedcol = e.Column
        changedRow = e.Row
        changed = True
        _ChangesMade = True

    End Sub

    Private Sub FpGridCriteria_EditModeOff(ByVal sender As Object, ByVal e As System.EventArgs) Handles FpGridCriteria.EditModeOff
        Dim valok As Boolean = False
        _blnValidationError = False

        If changed Then
            If changedcol = Criteria.TableField Then
                valok = ValidateTableField(FpGridCriteria_Sheet1.GetText(changedRow, changedcol), CInt(selectedGroup))
                If valok = False Then
                    DisplayWarning("Either Table or Field is not valid")
                    _blnValidationError = True
                    FpGridCriteria_Sheet1.SetText(changedRow, changedcol, String.Empty)
                Else
                    FpGridCriteria_Sheet1.SetActiveCell(changedRow, changedcol + 1)
                    _ChangesMade = True
                End If
            End If
            If changedcol = Criteria.ValueOrTable Then
                valok = ValidateTableField2(FpGridCriteria_Sheet1.GetText(changedRow, changedcol), CInt(selectedGroup))
                If valok = False Then
                    DisplayWarning("Either Table or Field is not valid")
                    _blnValidationError = True
                    FpGridCriteria_Sheet1.SetText(changedRow, changedcol, String.Empty)
                Else
                    FpGridCriteria_Sheet1.SetActiveCell(changedRow, changedcol + 1)
                    _ChangesMade = True
                End If
            End If
            If CDbl(FpGridCriteria_Sheet1.GetText(changedRow, Criteria.EditStatus)) <> EditTypes.New Then
                FpGridCriteria_Sheet1.SetText(changedRow, Criteria.EditStatus, CStr(EditTypes.Edit))
            End If

        End If

        If _blnComboChanged = True Then
            If changedcol = Criteria.Period And Trim(FpGridCriteria_Sheet1.GetText(FpGridCriteria_Sheet1.ActiveRowIndex, Criteria.Period)) = String.Empty Then
                FpGridCriteria_Sheet1.SetText(changedRow, Criteria.PeriodValue, "0")

            End If
            FpGridCriteria_Sheet1.SetActiveCell(changedRow, changedcol + 1)
            _blnComboChanged = False
        End If

        SetSaveButton()
    End Sub


    Private Sub FpgridCriteria_ComboSelChange(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.EditorNotifyEventArgs) Handles FpGridCriteria.ComboSelChange

        If _blnActive = False Then
            _blnActive = True
            If CDbl(FpGridCriteria_Sheet1.GetText(e.Row, Criteria.EditStatus)) <> EditTypes.New Then
                FpGridCriteria_Sheet1.SetText(e.Row, Criteria.EditStatus, CStr(EditTypes.Edit))
            End If
            'If e.Column = Criteria.Join And e.Row = 0 And FpGridCriteria_Sheet1.GetText(e.Column, e.Row) <> String.Empty Then
            '    DisplayStatus("First Join must be blank", DisplayTypes.IsError)
            '    blnValidationError = True
            '    FpGridCriteria_Sheet1.SetValue(e.Row, Criteria.Join, String.Empty)
            '    FpGridCriteria_Sheet1.SetActiveCell(e.Row, Criteria.Join)
            'Else
            '    FpGridCriteria_Sheet1.SetActiveCell(e.Row, e.Column + 1)
            '    _blnChangeMade = True
            'End If
            'If e.Column = Criteria.Period And Trim(FpGridCriteria_Sheet1.GetText(e.Row, Criteria.Period)) = String.Empty Then
            '    FpGridCriteria_Sheet1.SetText(e.Row, Criteria.PeriodValue, 0)
            '    FpGridCriteria.Refresh()
            'End If
            ' FpGridCriteria_Sheet1.AddSelection(e.Row, 0, 1, 10)

            _ChangesMade = True
            _blnComboChanged = True
        End If
        _blnActive = False

    End Sub

    Private Sub FpGridCriteria_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles FpGridCriteria.LeaveCell
        If _blnValidationError = True Then
            e.Cancel = True
        End If
    End Sub

    Private Function ValidateTableField(ByVal tablefield As String, ByVal id As Integer) As Boolean

        Dim cell As String()
        Dim field As String()
        Dim valTable As Boolean = False
        Dim valfield As Boolean = False
        Dim pos As Integer = 0
        Dim blnValid As Boolean = False

        pos = InStr(tablefield, ":")
        If pos > 0 Then
            If tablefield <> String.Empty Then
                cell = Split(tablefield, ":")
                valTable = _BOCriteria.TableExists(cell(0))
                'table exists 
                If valTable = True Then
                    'split fields 
                    field = Split(cell(1), ";")
                    For x As Integer = 0 To UBound(field)
                        If field(x) <> String.Empty Then
                            If valTable Then
                                ' test fields 
                                valfield = _BOCriteria.FieldExists(cell(0), field(x))
                                If valfield Then
                                    blnValid = True
                                Else
                                    'if invalid dont check any more 
                                    Return False
                                End If
                            End If
                        End If
                    Next
                    Return blnValid
                Else
                    'table does not exist 
                    Return False
                End If
            Else
                'no table 
                Return False
            End If
        Else
            'validate against default table 
            valfield = _BOCriteria.TableExists(tablefield)
            If valfield Then
                Return True
            Else
                Return False
            End If
        End If
        Return blnValid

    End Function

    Private Function ValidateTableField2(ByVal tablefield As String, ByVal id As Integer) As Boolean
        Dim pos As Integer = 0
        Dim valid As Boolean = False

        ' allow blank field so that dates can be calculated using the period
        Select Case Trim(tablefield)
            Case String.Empty : Return True
            Case "0" : Return True
            Case "1" : Return True
            Case "#TODAY" : Return True
            Case "#NULL" : Return True
            Case Else
                pos = InStr(tablefield, "'")
                If pos > 0 Then
                    Return True
                Else
                    valid = ValidateTableField(tablefield, id)
                    Return valid
                End If
        End Select

    End Function

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.ParentForm.Close()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Dim Result As DialogResult
        If _ChangesMade = True Then
            Result = MessageBox.Show("The current changes will be lost. " & vbNewLine & "Do you wish to continue", "HouseKeeping", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.Yes Then
                _SelectedIndex = CboGroup.SelectedIndex
                displaySelection()
            End If 'Result = DialogResult.OK
        Else
            _SelectedIndex = CboGroup.SelectedIndex
            displaySelection()
        End If 'btnSave.Visible = True

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim SaveTablename As String = String.Empty
        Dim SaveFieldname As String = String.Empty

        Dim SaveTablename2 As String = String.Empty
        Dim SaveFieldname2 As String = String.Empty

        Dim tabledata As String()
        Dim tabledata2 As String()
        Dim pos As Integer = 0
        Dim strWork As String = String.Empty
        Dim intNextSequence As Integer = 0

        If selectedGroup <> String.Empty Then
            _BOGroup.SaveVariables(CInt(selectedGroup), txtBOName.Text, txtClassName.Text)

            EffectsSave()

            For RowNo = 0 To FpGridCriteria_Sheet1.Rows.Count - 1 Step 1
                _EditType = CType(CLng(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.EditStatus)), Global.HouseKeeping.HouseKeeping.EditTypes)
                If (_EditType <> EditTypes.None) Then
                    Select Case (FpGridCriteria_Sheet1.GetText(RowNo, Criteria.EditStatus))
                        Case CStr(EditTypes.Delete)
                            _BOCriteria.DeleteCriteria(CInt(selectedGroup), CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Sequence)))

                        Case CStr(EditTypes.Edit)
                            tabledata = Split(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.TableField), ":")
                            SaveTablename = tabledata(0)
                            SaveFieldname = tabledata(1)
                            strWork = FpGridCriteria_Sheet1.GetText(RowNo, Criteria.ValueOrTable)
                            pos = InStr(strWork, ":")
                            If pos > 0 Then
                                tabledata2 = Split(strWork, ":")
                                SaveTablename2 = tabledata2(0)
                                SaveFieldname2 = tabledata2(1)
                            Else
                                SaveTablename2 = strWork
                                SaveFieldname2 = String.Empty
                            End If
                            _BOCriteria.UpdateRecord(CInt(selectedGroup), _
                                        CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Sequence)), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Connected), _
                                        SaveTablename, SaveFieldname, _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Comparison), _
                                        SaveTablename2, SaveFieldname2, _
                                        CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.SelectGroup)), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Join), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Level), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Period), _
                                        CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.PeriodValue)) _
                                                    )
                        Case CStr(EditTypes.New)
                            tabledata = Split(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.TableField), ":")
                            SaveTablename = tabledata(0)
                            If UBound(tabledata) = 1 Then
                                SaveFieldname = tabledata(1)
                            Else
                                SaveFieldname = String.Empty
                            End If

                            strWork = FpGridCriteria_Sheet1.GetText(RowNo, Criteria.ValueOrTable)
                            pos = InStr(strWork, ":")
                            If pos > 0 Then
                                tabledata2 = Split(strWork, ":")
                                SaveTablename2 = tabledata2(0)
                                SaveFieldname2 = tabledata2(1)
                            Else
                                SaveTablename2 = strWork
                                SaveFieldname2 = String.Empty
                            End If

                            'get next sequence no 
                            intNextSequence = _BOCriteria.GetNextSequence(CInt(selectedGroup))
                            _BOCriteria.AddRecord(selectedGroup, _
                                        intNextSequence + 1, _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Connected), _
                                        SaveTablename, _
                                        SaveFieldname, _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Comparison), _
                                        SaveTablename2, _
                                        SaveFieldname2, _
                                        CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.SelectGroup)), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Join), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Level), _
                                        FpGridCriteria_Sheet1.GetText(RowNo, Criteria.Period), _
                                        CInt(FpGridCriteria_Sheet1.GetText(RowNo, Criteria.PeriodValue)) _
                                        )
                    End Select
                End If
            Next
            MessageBox.Show("Your changes have been saved", "HseKeepMaintenance", MessageBoxButtons.OK)
            _ChangesMade = False

            'rebuild the Group list to ensure that BOName and Classname are included after update 
            _SelectedIndex = CboGroup.SelectedIndex
            CboGroup.DataSource = Nothing
            CboGroup.Items.Clear()
            DTGroup = _BOGroup.ListGroups
            CboGroup.DataSource = DTGroup
            CboGroup.ValueMember = DTGroup.Columns(0).ColumnName
            CboGroup.DisplayMember = DTGroup.Columns(1).ColumnName
            CboGroup.SelectedIndex = _SelectedIndex
            displaySelection()

        End If

    End Sub

    Private Sub EffectsSave()

        Dim intNextSequence As Integer = 0
        For RowNo = 0 To FpGridEffects_Sheet1.Rows.Count - 1 Step 1
            _EditType = CType(CLng(FpGridEffects_Sheet1.GetText(RowNo, Effects.EditStatus)), Global.HouseKeeping.HouseKeeping.EditTypes)
            If (_EditType <> EditTypes.None) Then
                Select Case (FpGridEffects_Sheet1.GetText(RowNo, Effects.EditStatus))
                    Case CStr(EditTypes.Delete)
                        _BOTables.RemoveTable(CInt(selectedGroup), FpGridEffects_Sheet1.GetText(RowNo, Effects.Sequence))
                    Case CStr(EditTypes.Edit)
                        _BOTables.UpdateTable(CInt(selectedGroup), FpGridEffects_Sheet1.GetText(RowNo, Effects.TableField), CInt(FpGridEffects_Sheet1.GetText(RowNo, Effects.Sequence)), FpGridEffects_Sheet1.GetText(RowNo, Effects.Action), FpGridEffects_Sheet1.GetText(RowNo, Effects.Value))
                    Case CStr(EditTypes.Edit)
                        intNextSequence = _BOTables.GetNextSequence(CInt(selectedGroup))
                        _BOTables.AddTable(CInt(selectedGroup), FpGridEffects_Sheet1.GetText(RowNo, Effects.TableField), intNextSequence + 1, FpGridEffects_Sheet1.GetText(RowNo, Effects.Action), FpGridEffects_Sheet1.GetText(RowNo, Effects.Value))
                End Select
            End If
        Next

    End Sub

    Private Sub SetSaveButton()
        btnSave.Visible = False
        btnSave.Enabled = False
        If _ChangesMade = True And _blnValidationError = False Then
            btnSave.Visible = True
            btnSave.Enabled = True
        End If
    End Sub

    Private Sub txtClassName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtClassName.TextChanged
        _ChangesMade = True
        SetSaveButton()
    End Sub

    Private Sub txtBOName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBOName.TextChanged
        _ChangesMade = True
        SetSaveButton()
    End Sub

End Class