﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HouseKeeping
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance10 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance11 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance12 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HouseKeeping))
        Me.CboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.LblEffects = New System.Windows.Forms.Label
        Me.FpGridEffects = New FarPoint.Win.Spread.FpSpread
        Me.FpGridEffects_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.LblCriteria = New System.Windows.Forms.Label
        Me.FpGridCriteria = New FarPoint.Win.Spread.FpSpread
        Me.FpGridCriteria_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.lblSchedule = New System.Windows.Forms.Label
        Me.FpGridSched = New FarPoint.Win.Spread.FpSpread
        Me.FpGridSched_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.btnCriteriaAdd = New System.Windows.Forms.Button
        Me.btnCriteriaDelete = New System.Windows.Forms.Button
        Me.btnEffectsAdd = New System.Windows.Forms.Button
        Me.btnEffectsDelete = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtGroupID = New System.Windows.Forms.Label
        Me.lblGroupID = New System.Windows.Forms.Label
        Me.lblClassName = New System.Windows.Forms.Label
        Me.txtClassName = New System.Windows.Forms.TextBox
        Me.lblBOName = New System.Windows.Forms.Label
        Me.txtBOName = New System.Windows.Forms.TextBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnAddGroup = New System.Windows.Forms.Button
        Me.btnCreate = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.txtAddGroup = New System.Windows.Forms.TextBox
        CType(Me.FpGridEffects, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpGridEffects_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpGridCriteria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpGridCriteria_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpGridSched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpGridSched_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CboGroup
        '
        Me.CboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboGroup.FormattingEnabled = True
        Me.CboGroup.Location = New System.Drawing.Point(69, 14)
        Me.CboGroup.Name = "CboGroup"
        Me.CboGroup.Size = New System.Drawing.Size(180, 21)
        Me.CboGroup.TabIndex = 1
        '
        'lblGroup
        '
        Me.lblGroup.AutoSize = True
        Me.lblGroup.Location = New System.Drawing.Point(6, 14)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(36, 13)
        Me.lblGroup.TabIndex = 0
        Me.lblGroup.Text = "Group"
        '
        'LblEffects
        '
        Me.LblEffects.AutoSize = True
        Me.LblEffects.Location = New System.Drawing.Point(6, 269)
        Me.LblEffects.Name = "LblEffects"
        Me.LblEffects.Size = New System.Drawing.Size(40, 13)
        Me.LblEffects.TabIndex = 6
        Me.LblEffects.Text = "Effects"
        '
        'FpGridEffects
        '
        Me.FpGridEffects.About = "3.0.2004.2005"
        Me.FpGridEffects.AccessibleDescription = "FpGridEffects, Sheet1, Row 0, Column 0, "
        Me.FpGridEffects.AllowUndo = False
        Me.FpGridEffects.BackColor = System.Drawing.SystemColors.Control
        Me.FpGridEffects.EditModeReplace = True
        Me.FpGridEffects.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.FpGridEffects.Location = New System.Drawing.Point(3, 285)
        Me.FpGridEffects.Name = "FpGridEffects"
        Me.FpGridEffects.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FpGridEffects.SelectionBlockOptions = CType((FarPoint.Win.Spread.SelectionBlockOptions.Cells Or FarPoint.Win.Spread.SelectionBlockOptions.Rows), FarPoint.Win.Spread.SelectionBlockOptions)
        Me.FpGridEffects.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.FpGridEffects_Sheet1})
        Me.FpGridEffects.Size = New System.Drawing.Size(703, 83)
        Me.FpGridEffects.TabIndex = 7
        TipAppearance10.BackColor = System.Drawing.SystemColors.Info
        TipAppearance10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance10.ForeColor = System.Drawing.SystemColors.InfoText
        Me.FpGridEffects.TextTipAppearance = TipAppearance10
        Me.FpGridEffects.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'FpGridEffects_Sheet1
        '
        Me.FpGridEffects_Sheet1.Reset()
        Me.FpGridEffects_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.FpGridEffects_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.FpGridEffects_Sheet1.ColumnCount = 5
        Me.FpGridEffects_Sheet1.RowCount = 1
        Me.FpGridEffects_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "Table & Field"
        Me.FpGridEffects_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Action"
        Me.FpGridEffects_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Value"
        Me.FpGridEffects_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "EditStatus"
        Me.FpGridEffects_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "Sequence"
        Me.FpGridEffects_Sheet1.ColumnHeader.Columns.Default.Resizable = False
        Me.FpGridEffects_Sheet1.Columns.Default.Resizable = False
        Me.FpGridEffects_Sheet1.Columns.Get(0).Label = "Table & Field"
        Me.FpGridEffects_Sheet1.Columns.Get(0).Locked = True
        Me.FpGridEffects_Sheet1.Columns.Get(0).Width = 215.0!
        Me.FpGridEffects_Sheet1.Columns.Get(1).Label = "Action"
        Me.FpGridEffects_Sheet1.Columns.Get(1).Locked = True
        Me.FpGridEffects_Sheet1.Columns.Get(1).Width = 80.0!
        Me.FpGridEffects_Sheet1.Columns.Get(2).Label = "Value"
        Me.FpGridEffects_Sheet1.Columns.Get(2).Locked = True
        Me.FpGridEffects_Sheet1.Columns.Get(2).Width = 215.0!
        Me.FpGridEffects_Sheet1.Columns.Get(3).Label = "EditStatus"
        Me.FpGridEffects_Sheet1.Columns.Get(3).Visible = False
        Me.FpGridEffects_Sheet1.Columns.Get(4).Label = "Sequence"
        Me.FpGridEffects_Sheet1.Columns.Get(4).Visible = False
        Me.FpGridEffects_Sheet1.RowHeader.Columns.Default.Resizable = True
        Me.FpGridEffects_Sheet1.RowHeader.Rows.Default.Resizable = False
        Me.FpGridEffects_Sheet1.Rows.Default.Resizable = False
        Me.FpGridEffects_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.FpGridEffects_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.FpGridEffects_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'LblCriteria
        '
        Me.LblCriteria.AutoSize = True
        Me.LblCriteria.Location = New System.Drawing.Point(3, 143)
        Me.LblCriteria.Name = "LblCriteria"
        Me.LblCriteria.Size = New System.Drawing.Size(39, 13)
        Me.LblCriteria.TabIndex = 10
        Me.LblCriteria.Text = "Criteria"
        '
        'FpGridCriteria
        '
        Me.FpGridCriteria.About = "3.0.2004.2005"
        Me.FpGridCriteria.AccessibleDescription = "FpGridCriteria, Sheet1, Row 0, Column 0, "
        Me.FpGridCriteria.AllowUndo = False
        Me.FpGridCriteria.BackColor = System.Drawing.SystemColors.Control
        Me.FpGridCriteria.EditModeReplace = True
        Me.FpGridCriteria.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.FpGridCriteria.Location = New System.Drawing.Point(5, 159)
        Me.FpGridCriteria.Name = "FpGridCriteria"
        Me.FpGridCriteria.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FpGridCriteria.SelectionBlockOptions = CType((FarPoint.Win.Spread.SelectionBlockOptions.Cells Or FarPoint.Win.Spread.SelectionBlockOptions.Rows), FarPoint.Win.Spread.SelectionBlockOptions)
        Me.FpGridCriteria.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.FpGridCriteria_Sheet1})
        Me.FpGridCriteria.Size = New System.Drawing.Size(704, 94)
        Me.FpGridCriteria.TabIndex = 11
        TipAppearance11.BackColor = System.Drawing.SystemColors.Info
        TipAppearance11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance11.ForeColor = System.Drawing.SystemColors.InfoText
        Me.FpGridCriteria.TextTipAppearance = TipAppearance11
        Me.FpGridCriteria.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'FpGridCriteria_Sheet1
        '
        Me.FpGridCriteria_Sheet1.Reset()
        Me.FpGridCriteria_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.FpGridCriteria_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.FpGridCriteria_Sheet1.ColumnCount = 11
        Me.FpGridCriteria_Sheet1.RowCount = 1
        Me.FpGridCriteria_Sheet1.Cells.Get(0, 2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.FpGridCriteria_Sheet1.Cells.Get(0, 3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "Select Group"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Connected"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "       Table &        Field"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Comp"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "     Value or      Table & Field"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 4).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.Center
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "Edit Status"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Sequence"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "Join"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "Level"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "Period"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "Period Value"
        Me.FpGridCriteria_Sheet1.ColumnHeader.Columns.Default.Resizable = False
        Me.FpGridCriteria_Sheet1.ColumnHeader.Rows.Get(0).Height = 28.0!
        Me.FpGridCriteria_Sheet1.Columns.Default.Resizable = False
        Me.FpGridCriteria_Sheet1.Columns.Get(0).Label = "Select Group"
        Me.FpGridCriteria_Sheet1.Columns.Get(0).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.FpGridCriteria_Sheet1.Columns.Get(1).Label = "Connected"
        Me.FpGridCriteria_Sheet1.Columns.Get(1).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(1).Width = 100.0!
        Me.FpGridCriteria_Sheet1.Columns.Get(2).Label = "       Table &        Field"
        Me.FpGridCriteria_Sheet1.Columns.Get(2).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(2).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.General
        Me.FpGridCriteria_Sheet1.Columns.Get(2).Width = 98.0!
        Me.FpGridCriteria_Sheet1.Columns.Get(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.FpGridCriteria_Sheet1.Columns.Get(3).Label = "Comp"
        Me.FpGridCriteria_Sheet1.Columns.Get(3).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(3).Width = 70.0!
        Me.FpGridCriteria_Sheet1.Columns.Get(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.FpGridCriteria_Sheet1.Columns.Get(4).Label = "     Value or      Table & Field"
        Me.FpGridCriteria_Sheet1.Columns.Get(4).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(4).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.Center
        Me.FpGridCriteria_Sheet1.Columns.Get(4).Width = 98.0!
        Me.FpGridCriteria_Sheet1.Columns.Get(5).Label = "Edit Status"
        Me.FpGridCriteria_Sheet1.Columns.Get(5).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(5).Visible = False
        Me.FpGridCriteria_Sheet1.Columns.Get(6).Label = "Sequence"
        Me.FpGridCriteria_Sheet1.Columns.Get(6).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(6).Visible = False
        Me.FpGridCriteria_Sheet1.Columns.Get(7).Label = "Join"
        Me.FpGridCriteria_Sheet1.Columns.Get(7).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(8).Label = "Level"
        Me.FpGridCriteria_Sheet1.Columns.Get(8).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(9).Label = "Period"
        Me.FpGridCriteria_Sheet1.Columns.Get(9).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(9).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.General
        Me.FpGridCriteria_Sheet1.Columns.Get(10).Label = "Period Value"
        Me.FpGridCriteria_Sheet1.Columns.Get(10).Locked = True
        Me.FpGridCriteria_Sheet1.Columns.Get(10).VerticalAlignment = FarPoint.Win.Spread.CellVerticalAlignment.Top
        Me.FpGridCriteria_Sheet1.Columns.Get(10).Width = 40.0!
        Me.FpGridCriteria_Sheet1.RowHeader.Columns.Default.Resizable = True
        Me.FpGridCriteria_Sheet1.RowHeader.Rows.Default.Resizable = False
        Me.FpGridCriteria_Sheet1.Rows.Default.Resizable = False
        Me.FpGridCriteria_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.FpGridCriteria_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.FpGridCriteria_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'lblSchedule
        '
        Me.lblSchedule.AutoSize = True
        Me.lblSchedule.Location = New System.Drawing.Point(7, 39)
        Me.lblSchedule.Name = "lblSchedule"
        Me.lblSchedule.Size = New System.Drawing.Size(52, 13)
        Me.lblSchedule.TabIndex = 14
        Me.lblSchedule.Text = "Schedule"
        '
        'FpGridSched
        '
        Me.FpGridSched.About = "3.0.2004.2005"
        Me.FpGridSched.AccessibleDescription = "FpGridSched, Sheet1, Row 0, Column 0, "
        Me.FpGridSched.AllowUndo = False
        Me.FpGridSched.BackColor = System.Drawing.SystemColors.Control
        Me.FpGridSched.EditModeReplace = True
        Me.FpGridSched.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.FpGridSched.Location = New System.Drawing.Point(5, 55)
        Me.FpGridSched.Name = "FpGridSched"
        Me.FpGridSched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FpGridSched.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.FpGridSched_Sheet1})
        Me.FpGridSched.Size = New System.Drawing.Size(699, 85)
        Me.FpGridSched.TabIndex = 0
        TipAppearance12.BackColor = System.Drawing.SystemColors.Info
        TipAppearance12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance12.ForeColor = System.Drawing.SystemColors.InfoText
        Me.FpGridSched.TextTipAppearance = TipAppearance12
        Me.FpGridSched.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'FpGridSched_Sheet1
        '
        Me.FpGridSched_Sheet1.Reset()
        Me.FpGridSched_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.FpGridSched_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.FpGridSched_Sheet1.ColumnCount = 4
        Me.FpGridSched_Sheet1.RowCount = 1
        Me.FpGridSched_Sheet1.Cells.Get(0, 0).Locked = True
        Me.FpGridSched_Sheet1.Cells.Get(0, 1).Locked = True
        Me.FpGridSched_Sheet1.Cells.Get(0, 2).Locked = True
        Me.FpGridSched_Sheet1.Cells.Get(0, 3).Locked = True
        Me.FpGridSched_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "SetNo"
        Me.FpGridSched_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "TaskNo"
        Me.FpGridSched_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Description"
        Me.FpGridSched_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Frequency"
        Me.FpGridSched_Sheet1.ColumnHeader.Columns.Default.Resizable = False
        Me.FpGridSched_Sheet1.Columns.Default.Resizable = False
        Me.FpGridSched_Sheet1.Columns.Get(0).Label = "SetNo"
        Me.FpGridSched_Sheet1.Columns.Get(0).Locked = True
        Me.FpGridSched_Sheet1.Columns.Get(1).Label = "TaskNo"
        Me.FpGridSched_Sheet1.Columns.Get(1).Locked = True
        Me.FpGridSched_Sheet1.Columns.Get(2).Label = "Description"
        Me.FpGridSched_Sheet1.Columns.Get(2).Locked = True
        Me.FpGridSched_Sheet1.Columns.Get(2).Width = 282.0!
        Me.FpGridSched_Sheet1.Columns.Get(3).Label = "Frequency"
        Me.FpGridSched_Sheet1.Columns.Get(3).Locked = True
        Me.FpGridSched_Sheet1.Columns.Get(3).Width = 282.0!
        Me.FpGridSched_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.FpGridSched_Sheet1.RowHeader.Rows.Default.Resizable = False
        Me.FpGridSched_Sheet1.RowHeader.Visible = False
        Me.FpGridSched_Sheet1.Rows.Default.Resizable = False
        Me.FpGridSched_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'btnCriteriaAdd
        '
        Me.btnCriteriaAdd.Enabled = False
        Me.btnCriteriaAdd.Location = New System.Drawing.Point(602, 259)
        Me.btnCriteriaAdd.Name = "btnCriteriaAdd"
        Me.btnCriteriaAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnCriteriaAdd.TabIndex = 13
        Me.btnCriteriaAdd.Text = "Add"
        Me.btnCriteriaAdd.UseVisualStyleBackColor = True
        Me.btnCriteriaAdd.Visible = False
        '
        'btnCriteriaDelete
        '
        Me.btnCriteriaDelete.Enabled = False
        Me.btnCriteriaDelete.Location = New System.Drawing.Point(521, 259)
        Me.btnCriteriaDelete.Name = "btnCriteriaDelete"
        Me.btnCriteriaDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnCriteriaDelete.TabIndex = 12
        Me.btnCriteriaDelete.Text = "Delete"
        Me.btnCriteriaDelete.UseVisualStyleBackColor = True
        Me.btnCriteriaDelete.Visible = False
        '
        'btnEffectsAdd
        '
        Me.btnEffectsAdd.Enabled = False
        Me.btnEffectsAdd.Location = New System.Drawing.Point(602, 376)
        Me.btnEffectsAdd.Name = "btnEffectsAdd"
        Me.btnEffectsAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnEffectsAdd.TabIndex = 9
        Me.btnEffectsAdd.Text = "Add"
        Me.btnEffectsAdd.UseVisualStyleBackColor = True
        Me.btnEffectsAdd.Visible = False
        '
        'btnEffectsDelete
        '
        Me.btnEffectsDelete.Enabled = False
        Me.btnEffectsDelete.Location = New System.Drawing.Point(521, 376)
        Me.btnEffectsDelete.Name = "btnEffectsDelete"
        Me.btnEffectsDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnEffectsDelete.TabIndex = 8
        Me.btnEffectsDelete.Text = "Delete"
        Me.btnEffectsDelete.UseVisualStyleBackColor = True
        Me.btnEffectsDelete.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtGroupID)
        Me.GroupBox1.Controls.Add(Me.lblGroupID)
        Me.GroupBox1.Controls.Add(Me.lblClassName)
        Me.GroupBox1.Controls.Add(Me.btnCriteriaDelete)
        Me.GroupBox1.Controls.Add(Me.txtClassName)
        Me.GroupBox1.Controls.Add(Me.lblBOName)
        Me.GroupBox1.Controls.Add(Me.FpGridSched)
        Me.GroupBox1.Controls.Add(Me.txtBOName)
        Me.GroupBox1.Controls.Add(Me.btnEffectsAdd)
        Me.GroupBox1.Controls.Add(Me.lblSchedule)
        Me.GroupBox1.Controls.Add(Me.btnCriteriaAdd)
        Me.GroupBox1.Controls.Add(Me.btnEffectsDelete)
        Me.GroupBox1.Controls.Add(Me.LblCriteria)
        Me.GroupBox1.Controls.Add(Me.FpGridCriteria)
        Me.GroupBox1.Controls.Add(Me.LblEffects)
        Me.GroupBox1.Controls.Add(Me.FpGridEffects)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(720, 443)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'txtGroupID
        '
        Me.txtGroupID.AutoSize = True
        Me.txtGroupID.Location = New System.Drawing.Point(63, 13)
        Me.txtGroupID.Name = "txtGroupID"
        Me.txtGroupID.Size = New System.Drawing.Size(0, 13)
        Me.txtGroupID.TabIndex = 17
        '
        'lblGroupID
        '
        Me.lblGroupID.AutoSize = True
        Me.lblGroupID.Location = New System.Drawing.Point(7, 13)
        Me.lblGroupID.Name = "lblGroupID"
        Me.lblGroupID.Size = New System.Drawing.Size(47, 13)
        Me.lblGroupID.TabIndex = 16
        Me.lblGroupID.Text = "GroupID"
        '
        'lblClassName
        '
        Me.lblClassName.AutoSize = True
        Me.lblClassName.Location = New System.Drawing.Point(376, 13)
        Me.lblClassName.Name = "lblClassName"
        Me.lblClassName.Size = New System.Drawing.Size(63, 13)
        Me.lblClassName.TabIndex = 15
        Me.lblClassName.Text = "Class Name"
        '
        'txtClassName
        '
        Me.txtClassName.Location = New System.Drawing.Point(445, 10)
        Me.txtClassName.MaxLength = 30
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.Size = New System.Drawing.Size(215, 20)
        Me.txtClassName.TabIndex = 13
        '
        'lblBOName
        '
        Me.lblBOName.AutoSize = True
        Me.lblBOName.Location = New System.Drawing.Point(96, 13)
        Me.lblBOName.Name = "lblBOName"
        Me.lblBOName.Size = New System.Drawing.Size(53, 13)
        Me.lblBOName.TabIndex = 14
        Me.lblBOName.Text = "BO Name"
        '
        'txtBOName
        '
        Me.txtBOName.Location = New System.Drawing.Point(155, 10)
        Me.txtBOName.MaxLength = 30
        Me.txtBOName.Name = "txtBOName"
        Me.txtBOName.Size = New System.Drawing.Size(215, 20)
        Me.txtBOName.TabIndex = 12
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(648, 493)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 40)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(567, 493)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 40)
        Me.btnReset.TabIndex = 6
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(3, 493)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 40)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnAddGroup
        '
        Me.btnAddGroup.Location = New System.Drawing.Point(255, 14)
        Me.btnAddGroup.Name = "btnAddGroup"
        Me.btnAddGroup.Size = New System.Drawing.Size(75, 23)
        Me.btnAddGroup.TabIndex = 8
        Me.btnAddGroup.Text = "Add Group"
        Me.btnAddGroup.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Enabled = False
        Me.btnCreate.Location = New System.Drawing.Point(255, 14)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(75, 23)
        Me.btnCreate.TabIndex = 2
        Me.btnCreate.Text = "Create"
        Me.btnCreate.UseVisualStyleBackColor = True
        Me.btnCreate.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Enabled = False
        Me.btnCancel.Location = New System.Drawing.Point(336, 14)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'txtAddGroup
        '
        Me.txtAddGroup.Enabled = False
        Me.txtAddGroup.Location = New System.Drawing.Point(69, 14)
        Me.txtAddGroup.Name = "txtAddGroup"
        Me.txtAddGroup.Size = New System.Drawing.Size(180, 20)
        Me.txtAddGroup.TabIndex = 1
        Me.txtAddGroup.Visible = False
        '
        'HouseKeeping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txtAddGroup)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.btnAddGroup)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblGroup)
        Me.Controls.Add(Me.CboGroup)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "HouseKeeping"
        Me.Size = New System.Drawing.Size(726, 536)
        CType(Me.FpGridEffects, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpGridEffects_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpGridCriteria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpGridCriteria_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpGridSched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpGridSched_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents LblEffects As System.Windows.Forms.Label
    Friend WithEvents FpGridEffects As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpGridEffects_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents LblCriteria As System.Windows.Forms.Label
    Friend WithEvents FpGridCriteria As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpGridCriteria_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents lblSchedule As System.Windows.Forms.Label
    Friend WithEvents FpGridSched As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpGridSched_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents btnCriteriaAdd As System.Windows.Forms.Button
    Friend WithEvents btnCriteriaDelete As System.Windows.Forms.Button
    Friend WithEvents btnEffectsAdd As System.Windows.Forms.Button
    Friend WithEvents btnEffectsDelete As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnAddGroup As System.Windows.Forms.Button
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtAddGroup As System.Windows.Forms.TextBox
    Friend WithEvents txtBOName As System.Windows.Forms.TextBox
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents lblBOName As System.Windows.Forms.Label
    Friend WithEvents lblClassName As System.Windows.Forms.Label
    Friend WithEvents txtGroupID As System.Windows.Forms.Label
    Friend WithEvents lblGroupID As System.Windows.Forms.Label
End Class
