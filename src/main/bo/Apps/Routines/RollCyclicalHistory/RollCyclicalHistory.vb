﻿Imports OasysDBBO

Module MainModule

    Dim _blnRunFromNightlyRoutine As Boolean
    Dim _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Public Sub Main()

        ' get command parameters 
        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
        Next

        'get data from table 
        Select Case strCmd
            Case "NI"
                _blnRunFromNightlyRoutine = True
            Case Else
                'Left Blank intentionally
        End Select

        'Run Automatically if ran from the night routine 
        If _blnRunFromNightlyRoutine Then
            RunRollCyclicalHistory()
        Else
            frmRollCyclicalHistory.Show()
        End If

        'Program ends
        End

    End Sub

    Public Function RunRollCyclicalHistory() As Boolean

        Dim BOSystem As New BOSystem.cSystemDates(_Oasys3DB)
        Dim BOPicCtl As New BOStockTake.cPicControl(_Oasys3DB)

        Dim BOHistory As New BOStock.cCyclicalCountHistory(_Oasys3DB)
        Dim HistoryList As List(Of BOStock.cCyclicalCountHistory) = Nothing

        frmRollCyclicalHistory.btnSelect.Enabled = False
        frmRollCyclicalHistory.Cursor = Cursors.WaitCursor

        'Need to get the Store Live Date for processing 
        BOSystem.LoadMatches()
        Dim dteTodayDate As Date = BOSystem.Today.Value

        'Work out the working day code for the cycle - index into PICCTL
        Dim decWorkCode As Decimal = (BOSystem.WeekCycleNumber.Value * 7)
        decWorkCode += BOSystem.TodayDayCode.Value

        'Get the records from the PICCTL that DAYN match decWorkCode
        Dim PicCtlList As List(Of BOStockTake.cPicControl) = BOPicCtl.GetRecordsByDayNumb(CInt(decWorkCode))
        frmRollCyclicalHistory.ProgressBar1.Minimum = 0
        frmRollCyclicalHistory.ProgressBar1.Maximum = PicCtlList.Count

        For Each PicCtlItem As BOStockTake.cPicControl In PicCtlList

            'Find all Stock Items that have the same Hierarchy Category, Group, Sub Group and Style as the current PicCtl record
            Dim BOStock As New BOStock.cStock(_Oasys3DB)
            BOStock.ClearLoadFilter()
            BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.HierCategory, PicCtlItem.HieCategory.Value)

            'Only Check if GroupNumber <> "000000"
            If PicCtlItem.HieGroup.Value <> "000000" Then
                BOStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.HierGroup, PicCtlItem.HieGroup.Value)
            End If

            'Only Check if Sub Group Number <> "000000"
            If PicCtlItem.HieSubgroup.Value <> "000000" Then
                BOStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.HierSubGroup, PicCtlItem.HieSubgroup.Value)
            End If

            'Only check if style <> "000000"
            If PicCtlItem.HieStyle.Value <> "000000" Then
                BOStock.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.HierStyle, PicCtlItem.HieStyle.Value)
            End If

            Dim StockList As List(Of BOStock.cStock) = BOStock.LoadMatches()

            'Now loop through the list
            For Each StockItem As BOStock.cStock In StockList

                If StockItem.CatchAll.Value = False And StockItem.ItemDeleted.Value = False Then 'Process

                    'Get the CYHMAS (Cyclical History) for the SKu Concerned
                    BOHistory.ClearLoadFilter()
                    BOHistory.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOHistory.SKUNumber, StockItem.SkuNumber.Value)
                    HistoryList = BOHistory.LoadMatches()
                    If HistoryList.Count > 0 Then

                        If HistoryList(0).EndofPrevCycle.Value <> dteTodayDate Then

                            'Update the CYHMAS Record
                            HistoryList(0).EndofCurrCycle.Value = dteTodayDate
                            HistoryList(0).YTDSalesCurrCycle.Value = StockItem.ValueSoldThisYear.Value
                            HistoryList(0).PYrSalesCurCycle.Value = StockItem.ValueSoldLastYear.Value

                            'The History List is now rolled
                            HistoryList(0).EndofCycles3Ago.Value = HistoryList(0).EndofCycles2Ago.Value
                            HistoryList(0).AdjustValCycle3Ago.Value = HistoryList(0).AdjustValCycle2Ago.Value
                            HistoryList(0).YTDSalesCycle3Ago.Value = HistoryList(0).YTDSalesCycle2Ago.Value
                            HistoryList(0).PYrSalesCycle3Ago.Value = HistoryList(0).PYrSalesCycle2Ago.Value

                            HistoryList(0).EndofCycles2Ago.Value = HistoryList(0).EndofPrevCycle.Value
                            HistoryList(0).AdjustQtyCycle2Ago.Value = HistoryList(0).AdjustQtyPrevCycle.Value
                            HistoryList(0).AdjustValCycle2Ago.Value = HistoryList(0).AdjustValPrevCycle.Value
                            HistoryList(0).YTDSalesCycle2Ago.Value = HistoryList(0).YTDSalesPrevCycle.Value
                            HistoryList(0).PYrSalesCycle2Ago.Value = HistoryList(0).PYrSalesPrevCycle.Value

                            HistoryList(0).EndofPrevCycle.Value = HistoryList(0).EndofCurrCycle.Value
                            HistoryList(0).AdjustQtyPrevCycle.Value = HistoryList(0).AdjustQtyCurrCycle.Value
                            HistoryList(0).AdjustValPrevCycle.Value = HistoryList(0).AdjustValCurrCycle.Value
                            HistoryList(0).YTDSalesPrevCycle.Value = HistoryList(0).YTDSalesCurrCycle.Value
                            HistoryList(0).PYrSalesPrevCycle.Value = HistoryList(0).PYrSalesCurCycle.Value

                            HistoryList(0).EndofCurrCycle.Value = Nothing
                            HistoryList(0).AdjustQtyCurrCycle.Value = 0
                            HistoryList(0).AdjustValCurrCycle.Value = 0
                            HistoryList(0).YTDSalesCurrCycle.Value = 0
                            HistoryList(0).PYrSalesCurCycle.Value = 0

                            HistoryList(0).SaveIfExists()

                        End If

                    Else

                        'Sku Doesn't exist create it in CYHMAS (HISTORY)
                        BOHistory.SKUNumber.Value = StockItem.SkuNumber.Value
                        BOHistory.SaveIfNew()

                    End If

                End If

            Next

            BOStock = Nothing
            StockList = Nothing

            frmRollCyclicalHistory.ProgressBar1.Value += 1
            frmRollCyclicalHistory.Refresh()
            Application.DoEvents()
        Next PicCtlItem

        frmRollCyclicalHistory.btnSelect.Enabled = True
        frmRollCyclicalHistory.Cursor = Cursors.Default
        Return True
    End Function

End Module
