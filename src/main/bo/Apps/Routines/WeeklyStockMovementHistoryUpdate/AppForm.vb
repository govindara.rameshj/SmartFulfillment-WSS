﻿Imports System.Configuration.ConfigurationManager

Public Class ApplicationForm
    Private _DataProxy As Oasys.DataProxy
    Private _AutoRun As Boolean

    Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim arguments() As String

        Me.Text = String.Concat(Me.Text, " - v", FileVersion(Me.GetType))
        arguments = Environment.GetCommandLineArgs()
        If arguments.Length >= 2 Then
            _AutoRun = arguments(1).EndsWith(",/P=',CFC')")
        End If
    End Sub

    Public Shared Sub DoUpdate()
        Using f As New WeeklyStockMovementHistoryUpdate.ApplicationForm
            f.Show()
            Application.DoEvents()
            f.PerformUpdate()
            f.ExitButton.PerformClick()
        End Using
    End Sub

    Private Sub PerformUpdate()
        Label1.Text = "Update in progress..."
        Label1.Refresh()
        OKButton.Enabled = False
        ExitButton.Enabled = False
        Cursor = Cursors.WaitCursor
        _DataProxy = CType(Activator.CreateInstance(AppSettings("ProxyAssembly"), AppSettings("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {AppSettings("DataConnectionString")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)
        Try
            ProcessWmhUpdate()

            UpdateLabel.Text = "Processing Complete"
            UpdateLabel.Refresh()

            ExitButton.Enabled = True
        Finally
            Cursor = Cursors.Default
            If GetType(IDisposable).IsInstanceOfType(_DataProxy) Then
                DirectCast(_DataProxy, IDisposable).Dispose()
            End If
            _DataProxy = Nothing
        End Try
    End Sub

    Private Sub AbortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

    Private Shared Function FileVersion(ByVal type As Type) As String
        Dim attributes() As Object = Reflection.Assembly.GetAssembly(type).GetCustomAttributes(GetType(Reflection.AssemblyFileVersionAttribute), False)
        If attributes.Length > 0 Then
            Return CType(attributes(0), Reflection.AssemblyFileVersionAttribute).Version
        Else
            Return String.Concat(Reflection.Assembly.GetAssembly(type).GetName.Version.ToString, "a")
        End If
    End Function

    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        MyBase.OnShown(e)
        If _AutoRun Then OKButton.PerformClick()
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        OKButton.Enabled = False
        ExitButton.Enabled = False
        Application.DoEvents()

        PerformUpdate()

        If _AutoRun Then
            ExitButton.PerformClick()
        End If
    End Sub

    Private Sub ProcessWmhUpdate()
        Dim priorWeekNumber As Integer
        Dim weekNumber As Integer
        Dim weekEndingDate As Date
        Dim deleteWeek13 As Boolean

        Dim sysdatRow As DataRow
        Using cmd As New SqlClient.SqlCommand("SELECT WK131, WK132 FROM SYSDAT WHERE FKEY = '01'")
            Trace.WriteLine("WeekelyStockMovementHistoryUpdate - Retrieving period end settings.")
            sysdatRow = _DataProxy.ExecuteDataTable(cmd).Rows(0)

            weekEndingDate = CDate(sysdatRow.Item("WK131"))
            weekNumber = (weekEndingDate.DayOfYear - 1) \ 7 + 1
            priorWeekNumber = (CDate(sysdatRow.Item("WK132")).DayOfYear - 1) \ 7 + 1

            deleteWeek13 = (weekNumber = 1) And (priorWeekNumber <> 53)

            ProgressBar1.Maximum = 21
            ProgressBar1.Step = 1

            If deleteWeek13 Then
                ProgressBar1.Maximum += 1
                UpdateLabel.Text = "Removing 'Week 53' History"
                UpdateLabel.Refresh()
                Trace.WriteLine("WeekelyStockMovementHistoryUpdate - Removing week 53 history.")
                cmd.CommandText = "DELETE FROM STKWMV WHERE WKNO = 53"
                cmd.ExecuteNonQuery()
                ProgressBar1.PerformStep()
                Application.DoEvents()
            End If

            PerformSingleUpdate("Week Ending Date", cmd, "UPDATE STKWMV v SET WEDT = @weekEndingDate WHERE WKNO = @weekNumber", weekNumber, weekEndingDate)
            PerformSingleUpdate("Sales Value", cmd, "UPDATE STKWMV v SET SALV = (SELECT SALV3 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Sales Units", cmd, "UPDATE STKWMV v SET SALU = (SELECT SALU3 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Receipts Value", cmd, "UPDATE STKWMV v SET MREV = (SELECT MREV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Receipts Units", cmd, "UPDATE STKWMV v SET MREQ = (SELECT MREQ2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Adjustments Value", cmd, "UPDATE STKWMV v SET MADV = (SELECT MADV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Adjustments Quantity", cmd, "UPDATE STKWMV v SET MADQ = (SELECT MADQ2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Inter-Branch Transfer Value", cmd, "UPDATE STKWMV v SET MIBV = (SELECT MIBV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Inter-Branch Transfer Units", cmd, "UPDATE STKWMV v SET MIBQ = (SELECT MIBQ2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Returns Value", cmd, "UPDATE STKWMV v SET MRTV = (SELECT MRTV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Returns Units", cmd, "UPDATE STKWMV v SET MRTQ = (SELECT MRTQ2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Bulk-to-Single Transfers Value", cmd, "UPDATE STKWMV v SET MBSV = (SELECT MBSV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Bulk-to-Single Transfers Units", cmd, "UPDATE STKWMV v SET MBSQ = (SELECT MBSQ2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Margin Erosion Value", cmd, "UPDATE STKWMV v SET MPVV = (SELECT MPVV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Cyclical Count Value", cmd, "UPDATE STKWMV v SET MCCV = (SELECT MCCV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("DRL Adjustments Value", cmd, "UPDATE STKWMV v SET MDRV = (SELECT MDRV2 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Starting Price", cmd, "UPDATE STKWMV v SET MEPR = (SELECT MSPR1 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Starting Units", cmd, "UPDATE STKWMV v SET METQ = (SELECT MSTQ1 FROM STKMAS i WHERE i.SKUN = v.SKUN) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)
            PerformSingleUpdate("Single Markup Values", cmd, "UPDATE STKWMV v SET MARK = (SELECT PWKM FROM STKMAS INNER JOIN RELITM ON SPOS = SKUN WHERE STKMAS.SKUN = v.SKUN AND IRIS = 1) WHERE WKNO = @weekNumber", weekNumber, Date.MinValue)

            PerformSingleUpdate("Adding New Items", cmd, String.Format(String.Concat("INSERT INTO STKWMV ", _
                    "SELECT ", _
                      "SKUN, {0}, '{1}', SALV3, SALU3, MREV2, MREQ2, MADV2, MADQ2, MIBV2, MIBQ2, ", _
                      "MRTV2, MRTQ2, MBSV2, MBSQ2, MPVV2, 0, MCCV2, MDRV2, MSPR1, MSTQ1, PWKM, '' ", _
                    "FROM STKMAS ", _
                    "LEFT OUTER JOIN RELITM ON STKMAS.IRIS = 1 AND RELITM.SPOS = STKMAS.SKUN ", _
                    "WHERE SKUN NOT IN (SELECT SKUN FROM STKWMV WHERE WKNO = @weekNumber)"), weekNumber, weekEndingDate.ToString("yyyy-MM-dd") _
                ), weekNumber, Date.MinValue)

            UpdateLabel.Text = "Removing Null Values"
            UpdateLabel.Refresh()
            cmd.CommandText = "UPDATE STKWMV SET MARK = 0 WHERE MARK IS NULL"
            Trace.WriteLine("WeekelyStockMovementHistoryUpdate - Removing Null Values.")
            _DataProxy.ExecuteNonQuery(cmd)
            ProgressBar1.PerformStep()

            UpdateLabel.Text = String.Empty
            UpdateLabel.Refresh()
            Trace.WriteLine("WeekelyStockMovementHistoryUpdate - Completed.")
        End Using
    End Sub

    Private Sub PerformSingleUpdate(ByVal description As String, ByVal command As SqlClient.SqlCommand, ByVal commandText As String, ByVal weekNumber As Integer, ByVal weekEndingDate As Date)
        UpdateLabel.Text = description
        UpdateLabel.Refresh()
        command.CommandText = commandText
        command.Parameters.Add("@weekNumber", SqlDbType.Int).Value = weekNumber
        If weekEndingDate <> Date.MinValue Then
            command.Parameters.Add("@weekEndingDate", SqlDbType.DateTime).Value = weekEndingDate
        End If
        Trace.WriteLine(String.Concat("WeekelyStockMovementHistoryUpdate - Updating ", description))
        _DataProxy.ExecuteNonQuery(command)
        command.Parameters.Clear()

        ProgressBar1.PerformStep()
        Application.DoEvents()
    End Sub
End Class
