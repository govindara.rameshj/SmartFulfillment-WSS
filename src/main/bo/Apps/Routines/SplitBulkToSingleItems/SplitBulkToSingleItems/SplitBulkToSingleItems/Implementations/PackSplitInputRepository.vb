﻿Imports System.Data.SqlClient
Imports SplitBulkToSingleItems.Interfaces
Namespace Implementations
    Public Class PackSplitInputRepository
        Implements IGetStockToSplit

        Private ReadOnly _connectionString As String

        Public Sub New(ByVal connectionString As String)
            Debug.WriteLine("New PackSplitInputRepository called")
            _connectionString = connectionString
            Debug.WriteLine("New PackSplitInputRepository exiting")
        End Sub

        Public Function GetStockToSplit() As DataTable Implements IGetStockToSplit.GetStockToSplit
            Debug.WriteLine("New PackSplitInputRepository.GetStockToSplit called")
            Dim result As New DataTable

            Try
                Using conn As New SqlConnection(_connectionString)
                    Try
                        conn.Open()
                        Dim comm = conn.CreateCommand
                        comm.CommandType = CommandType.StoredProcedure
                        comm.CommandText = "usp_GetRelatedItemsRequiringAdjustments"

                        Dim reader As SqlDataReader = comm.ExecuteReader
                        result.Load(reader)
                        reader.Close()

                    Catch ex As Exception
                        Debug.WriteLine(ex.ToString())
                        Throw New Exception("Failed to retrieve Stock To Split data", ex)
                    End Try
                End Using

            Catch ex As Exception
                Debug.WriteLine(ex.ToString())
            End Try
            Debug.WriteLine("New PackSplitInputRepository.GetStockToSplit exiting")

            Return result
        End Function
    End Class
End Namespace