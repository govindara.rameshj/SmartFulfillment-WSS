﻿Namespace Structures
    Public Structure ItemAdjustment
        Private _adjustmentQuantity As Integer
        Private _startQuantity As Integer
        Private _skun As String
        Private _price As Decimal

        Sub New(ByVal skun As String, ByVal startquantity As Integer, ByVal price As Decimal)
            Me.Skun = skun
            Me.StartQuantity = startquantity
            Me.Price = price
        End Sub

        Public ReadOnly Property AdjustmentValue() As Decimal
            Get
                Return _adjustmentQuantity * _price
            End Get
        End Property
        Public ReadOnly Property EndQuantity() As Integer
            Get
                Return _startQuantity + _adjustmentQuantity
            End Get
        End Property
        Public Property Skun() As String
            Get
                Return _skun
            End Get
            Set(ByVal value As String)
                _skun = value
            End Set
        End Property

        Public Property StartQuantity() As Integer
            Get
                Return _startQuantity
            End Get
            Set(ByVal value As Integer)
                _startQuantity = value
            End Set
        End Property

        Public Property AdjustmentQuantity() As Integer
            Get
                Return _adjustmentQuantity
            End Get
            Set(ByVal value As Integer)
                _adjustmentQuantity = value
            End Set
        End Property

        Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
    End Structure
End Namespace