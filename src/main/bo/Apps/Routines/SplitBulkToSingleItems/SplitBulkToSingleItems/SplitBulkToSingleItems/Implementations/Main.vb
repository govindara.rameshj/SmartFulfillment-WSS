﻿Imports SplitBulkToSingleItems.Interfaces
Imports SplitBulkToSingleItems.Structures

Namespace Implementations
    Public Class Main
        Friend _connectionString As String
        Friend _userId As String
        Friend _environment As ISthotEnvironmentalVariables

        Friend PackAdjustment As IPackAdjuster

        Public Event AdvanceProgressBar()
        Public Event InitialiseProgressBar(ByVal Maximum As Integer)

        Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, _
                       ByVal securityLevel As Integer, ByVal RunParameters As String)
            Debug.WriteLine("New Main called")
            SetMemberVariables(userId, RunParameters)
            Debug.WriteLine("New Main exiting")
        End Sub

        Public Sub New()

        End Sub

        Private Sub SetMemberVariables(ByVal userId As Integer, ByVal RunParameters As String)
            Debug.WriteLine("SetMemberVariables called")
            _userId = userId.ToString("000")
            _connectionString = RunParameters
            _environment = GetEnvironment()
            Debug.WriteLine("SetMemberVariables exiting")
        End Sub

        Friend Overridable Function GetEnvironment() As ISthotEnvironmentalVariables
            Return New SthotEnvironmentalVariables(_connectionString)
        End Function

        Friend Overridable Function GetStockToSplit() As DataTable
            Debug.WriteLine("GetStockToSplit called")
            Dim getStock As IGetStockToSplit = New PackSplitInputRepository(_connectionString)
            Debug.WriteLine("GetStockToSplit exiting")
            Return getStock.GetStockToSplit()
        End Function

        Friend Sub DoProcessing()
            Debug.WriteLine("DoProcessing called")

            Dim data As DataTable = GetStockToSplit()
            SetProgressBar(data.Rows.Count)

            Try
                If IO.File.Exists("STHOTST.CTL") Then
                    Try
                        System.IO.File.Delete("STHOTST.CTL")
                    Catch ex As Exception
                        Debug.WriteLine(ex.InnerException)
                    End Try
                End If

            Catch ex As Exception
                Debug.WriteLine(ex.InnerException)
            End Try

            For Each dr As DataRow In data.Rows
                AdjustPack(dr)
                UpdateProgressBar()
            Next
            Debug.WriteLine("DoProcessing exiting")
        End Sub

        Private Sub UpdateProgressBar()
            RaiseEvent AdvanceProgressBar()
        End Sub

        Private Sub SetProgressBar(ByVal count As Integer)
            Debug.WriteLine("SetProgressBar called")
            RaiseEvent InitialiseProgressBar(count)
            Debug.WriteLine("SetProgressBar exiting")
        End Sub

        Friend Sub AdjustPack(ByVal dr As DataRow)
            PackAdjustment = Nothing
            Dim singleItem As New ItemAdjustment(CStr(dr.Item("SingleSkun")), CInt(dr.Item("SingleOnHand")), CDec(dr.Item("SinglePrice")))
            Dim packItem As New ItemAdjustment(CStr(dr.Item("PackSkun")), CInt(dr.Item("PackOnHand")), CDec(dr.Item("PackPrice")))

            PackAdjustment = GetPackAdjuster(dr, singleItem, packItem)

            Dim rep = GetPackSplitOutputRepository(_connectionString, _environment)

            PackAdjustment.SplitPacks(rep)
        End Sub

        Friend Overridable Function GetPackSplitOutputRepository(ByVal connectionString As String, ByVal environment As ISthotEnvironmentalVariables) As IPackSplitOutput
            Return New PackSplitOutputRepository(connectionString, environment)
        End Function

        Friend Overridable Function GetPackAdjuster(ByVal dr As DataRow, ByVal singleItem As ItemAdjustment, ByVal packItem As ItemAdjustment) As IPackAdjuster
            Return New PackAdjuster(singleItem, packItem, CInt(dr.Item("ItemPackSize")), CInt(_userId))
        End Function
    End Class

End Namespace