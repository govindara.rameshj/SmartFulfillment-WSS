﻿Imports SplitBulkToSingleItems.Interfaces
Imports System.Data.SqlClient
Imports SplitBulkToSingleItems.Structures

Namespace Implementations

    Public Class PackSplitOutputRepository

        Implements IPackSplitOutput, IDisposable
        Private Const StockLogType As String = "51"

        Private _connection As SqlClient.SqlConnection
        Private _command As SqlCommand
        Private _transaction As SqlTransaction
        Private _connectionString As String
        Private _fileLocation As String
        Private _systemDate As Date
        Private _packAdjustment As PackAdjuster
        Private _userId As String

        Private _disposedValue As Boolean = False        ' To detect redundant calls

        Public Sub New(ByVal connectionString As String, ByVal environment As ISthotEnvironmentalVariables)
            _connectionString = connectionString
            _fileLocation = environment.FileLocation()
            _systemDate = environment.SystemDate()
        End Sub

        Public Sub Save(ByVal packAdjustment As Implementations.PackAdjuster, ByVal userId As String) Implements Interfaces.IPackSplitOutput.Save
            SetMemberVariables(packAdjustment, userId)
            PrepareRepositoryConnection()
            BeginTransaction()

            Try
                SaveStock()
                SaveStockLog()
                SaveStoreToHeadOffice()
                CommitTransaction()

            Catch ex As Exception
                Try
                    RollbackTransaction()
                Catch ex2 As Exception
                    Debug.WriteLine(ex.InnerException)
                End Try
            End Try

        End Sub

        Private Sub RollbackTransaction()
            _transaction.Rollback()
        End Sub

        Private Sub CommitTransaction()
            _transaction.Commit()
        End Sub

        Private Sub BeginTransaction()
            _transaction = _connection.BeginTransaction("Adjustments for Pack Split")
            _command.Transaction = _transaction
        End Sub

        Private Sub PrepareRepositoryConnection()
            _connection = New SqlConnection(_connectionString)
            _connection.Open()
            _command = _connection.CreateCommand()
            _command.CommandType = CommandType.StoredProcedure
        End Sub

        Private Sub SetMemberVariables(ByVal packAdjustment As PackAdjuster, ByVal userId As String)
            _packAdjustment = packAdjustment
            _userId = userId
        End Sub

        Friend Sub SaveStock()
            UpdateStkmas()
            UpdateRelitm()
            AdjustStockForPackSplit(_packAdjustment.SingleItem)
            AdjustStockForPackSplit(_packAdjustment.BulkItem)
            AdjustSinglesSoldNotUpdatedYet(_packAdjustment.SingleItem.Skun)
        End Sub

        Private Sub AdjustSinglesSoldNotUpdatedYet(ByVal skun As String)
            _command.Parameters.Clear()
            _command.CommandText = "usp_AdjustSinglesSoldNotUpdatedYet"

            AddParameter("@Skun", skun, SqlDbType.VarChar)

            _command.ExecuteNonQuery()
        End Sub

        Private Sub AdjustStockForPackSplit(ByVal adjustment As ItemAdjustment)
            _command.Parameters.Clear()
            _command.CommandText = "usp_AdjustStockForPackSplit"

            AddParameter("@Skun", adjustment.Skun, SqlDbType.VarChar)
            AddParameter("@Quantity", adjustment.AdjustmentQuantity, SqlDbType.Int)
            AddParameter("@Value", adjustment.AdjustmentValue, SqlDbType.Decimal)

            _command.ExecuteNonQuery()
        End Sub

        Friend Sub UpdateStkmas()
            UpdateStkmasItem(_packAdjustment.SingleItem)
            UpdateStkmasItem(_packAdjustment.BulkItem)
        End Sub

        Private Sub UpdateStkmasItem(ByVal adjustment As ItemAdjustment)
            _command.Parameters.Clear()
            _command.CommandText = "usp_AdjustUnitsSoldInCurrentWeek"

            AddParameter("@Skun", adjustment.Skun, SqlDbType.VarChar)
            AddParameter("@Quantity", adjustment.AdjustmentQuantity, SqlDbType.Int)

            _command.ExecuteNonQuery()
        End Sub

        Private Sub UpdateRelitm()
            _command.Parameters.Clear()
            _command.CommandText = "usp_AdjustRelatedItemsMarkup"

            AddParameter("@Skun", _packAdjustment.SingleItem.Skun, SqlDbType.VarChar)
            AddParameter("@Value", _packAdjustment.SingleItem.Price - _packAdjustment.BulkItem.Price, SqlDbType.Int)

            _command.ExecuteNonQuery()
        End Sub

        Friend Sub SaveStockLog()
            SaveStockLogItem(_packAdjustment.SingleItem)
            SaveStockLogItem(_packAdjustment.BulkItem)
        End Sub


        Private Sub SaveStockLogItem(ByVal adjustment As ItemAdjustment)
            _command.Parameters.Clear()
            _command.CommandText = "usp_InsertStockLogForSku"

            AddParameter("@SkuNumber", adjustment.Skun, SqlDbType.VarChar)
            AddParameter("@DayNumber", DayNumber(), SqlDbType.Decimal)
            AddParameter("@Type", StockLogType, SqlDbType.VarChar)
            AddParameter("@AdjustmentDate", CDate(Now), SqlDbType.Date)
            AddParameter("@AdjustmentTime", CDate(Now).ToString("HHmmss"), SqlDbType.VarChar)
            AddParameter("@Keys", Keys(), SqlDbType.VarChar)
            AddParameter("@UserId", _userId, SqlDbType.VarChar)
            AddParameter("@StartingStock", adjustment.StartQuantity, SqlDbType.Int)
            AddParameter("@EndingStock", adjustment.EndQuantity, SqlDbType.Int)
            AddParameter("@StartingReturns", 0, SqlDbType.Decimal)
            AddParameter("@EndingReturns", 0, SqlDbType.Decimal)
            AddParameter("@StartingMarkdown", 0, SqlDbType.Decimal)
            AddParameter("@EndingMarkdown", 0, SqlDbType.Decimal)
            AddParameter("@StartingWriteoff", 0, SqlDbType.Decimal)
            AddParameter("@EndingWriteoff", 0, SqlDbType.Decimal)
            AddParameter("@StartingPrice", adjustment.AdjustmentValue, SqlDbType.Decimal)
            AddParameter("@EndingPrice", adjustment.AdjustmentValue, SqlDbType.Decimal)

            _command.ExecuteNonQuery()

        End Sub

        Private Function DayNumber() As Integer
            Return CInt(DateDiff(DateInterval.Day, CDate("1900 Jan 1"), Now))
        End Function

        Private Function Keys() As String
            Return _packAdjustment.SingleItem.Skun & "    " & _packAdjustment.BulkItem.Skun
        End Function

        Private Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType)
            _command.Parameters.Add(New SqlParameter(name, type)).Value = value
        End Sub

        Friend Sub SaveStoreToHeadOffice()
            Using fileSthot As New System.IO.StreamWriter(String.Concat(_fileLocation, "STHOT"), True)
                Dim sb As New System.Text.StringBuilder("BS")                               ' df-type
                sb.Append(_systemDate.ToString("dd/MM/yy"))                                                     ' bs-date
                sb.Append(_packAdjustment.SingleItem.AdjustmentQuantity.ToString("0.00 ;0.00-").PadLeft(12))    ' bs-hash
                sb.Append(_packAdjustment.SingleItem.Skun.ToString.PadLeft(6, Chr(48)))                         ' bs-spos
                sb.Append(_packAdjustment.SingleItem.AdjustmentQuantity.ToString("0 ;0-").PadLeft(7))           ' bs-stra
                sb.Append(_packAdjustment.BulkItem.Skun.PadLeft(6, Chr(48)))                                    ' bs-ppos
                sb.Append(_packAdjustment.BulkItem.AdjustmentQuantity.ToString("0 ;0-").PadLeft(7))             ' bs-ptra
                sb.Append(_packAdjustment.BulkItem.Price.ToString("0.00 ;0.00-").PadLeft(10))                   ' bs-ppri
                sb.Append(_packAdjustment.SingleItem.Price.ToString("0.00 ;0.00-").PadLeft(10))                 ' bs-wpri

                fileSthot.WriteLine(sb.ToString)
            End Using
        End Sub


        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me._disposedValue Then
                If disposing Then
                    ' TODO: free other state (managed objects).
                End If

                If Not _connection Is Nothing Then
                    _connection.Close()
                    _connection = Nothing
                End If
            End If

            Me._disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Namespace