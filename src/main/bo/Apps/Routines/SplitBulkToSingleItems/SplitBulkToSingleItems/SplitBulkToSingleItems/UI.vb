﻿Imports SplitBulkToSingleItems.Implementations
Imports Cts.Oasys.Core.Tests

Public Class UI

    Friend _connectionString As String
    Friend _workStationId As Integer
    Friend _securityLevel As Integer
    Friend _userId As Integer

    Dim WithEvents _main As Main
    Public Sub New()

        TestEnvironmentSetup.SetupIfTestRun()

        Debug.WriteLine("New UI called")
        InitializeComponent()

        Dim params() As String = Command.Split(","c)
        Debug.WriteLine(Command().ToString())
        Debug.WriteLine(params(0))
        Debug.WriteLine(params(1))
        Debug.WriteLine(params(2))
        Debug.WriteLine(params(3))

        _userId = CInt(params(0))
        _workStationId = CInt(params(1))
        _securityLevel = CInt(params(2))
        _connectionString = FormatConnectionString(params(3))

        Debug.WriteLine("New UI called")
    End Sub

    Private Function FormatConnectionString(ByVal params As String) As String
        Return params.Replace(Chr(34), "")
    End Function

    Private Sub SplitBulkToSingleItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Debug.WriteLine("SplitBulkToSingleItems_Load called")
        _main = New Main(_userId, _workStationId, _securityLevel, _connectionString)
        ShowForm()

        Try
            _main.DoProcessing()
            If IO.File.Exists("TASKCOMP") Then
                Kill("TASKCOMP")
            End If
        Catch ex As Exception
            Debug.WriteLine(ex.InnerException)
        Finally
            Close()
        End Try
        Debug.WriteLine("SplitBulkToSingleItems_Load exiting")

    End Sub

    Private Sub ShowForm()
        Me.CenterToScreen()
        Me.Show()
        Me.Refresh()
        Me.ProgressBar1.Minimum = 0
    End Sub

    Private Sub AdvanceProgressBar() Handles _main.AdvanceProgressBar
        Debug.WriteLine("AdvanceProgressBar called")
        Me.ProgressBar1.Value += 1
        Me.ProgressBar1.Refresh()
        Debug.WriteLine("AdvanceProgressBar exiting")
    End Sub

    Private Sub InitialiseProgressBar(ByVal maximum As Integer) Handles _main.InitialiseProgressBar
        Debug.WriteLine("InitialiseProgressBar called")
        Me.ProgressBar1.Maximum = maximum
        Me.ProgressBar1.Refresh()
        Debug.WriteLine("InitialiseProgressBar exiting")
    End Sub

End Class