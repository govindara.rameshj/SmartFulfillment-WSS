﻿Imports SplitBulkToSingleItems.Implementations
Imports System.Data.SqlClient

Namespace Interfaces
    Public Interface IPackSplitOutput
        Sub Save(ByVal packAdjustment As PackAdjuster, ByVal userId As String)
    End Interface

    Public Interface IPackAdjuster
        Sub SplitPacks(ByVal rep As IPackSplitOutput)
    End Interface
End Namespace
