﻿Namespace Interfaces
    Public Interface ISthotEnvironmentalVariables
        Function SystemDate() As Date
        Function FileLocation() As String
    End Interface
End Namespace