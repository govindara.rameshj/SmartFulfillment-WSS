﻿Imports SplitBulkToSingleItems.Structures
Imports SplitBulkToSingleItems.Interfaces

Namespace Implementations
    Public Class PackAdjuster
        Implements IPackAdjuster
        Friend _singleItem As ItemAdjustment
        Friend _bulkItem As ItemAdjustment
        Friend _itemsPerPack As Integer
        Friend _userId As Integer

        Public ReadOnly Property BulkItem() As ItemAdjustment
            Get
                Return _bulkItem
            End Get
        End Property

        Public ReadOnly Property SingleItem() As ItemAdjustment
            Get
                Return _singleItem
            End Get
        End Property

        Public Sub New(ByVal singleItem As ItemAdjustment, ByVal bulkItem As ItemAdjustment, ByVal itemsPerPack As Integer, ByVal userID As Integer)
            SetMemberVariables(singleItem, bulkItem, itemsPerPack, userID)
        End Sub

        Private Sub SetMemberVariables(ByVal singleItem As ItemAdjustment, ByVal bulkItem As ItemAdjustment, ByVal itemsPerPack As Integer, ByVal userID As Integer)
            _singleItem = singleItem
            _bulkItem = bulkItem
            _itemsPerPack = itemsPerPack
            _userId = userID
        End Sub

        Public Sub SplitPacks(ByVal rep As IPackSplitOutput) Implements IPackAdjuster.SplitPacks
            Dim numberOfPacksSplit As Integer = NumberOfPacks()
            SetAdjustmentQuantities(numberOfPacksSplit)
            SaveSplitPackAdjustments(rep)
        End Sub

        Private Sub SaveSplitPackAdjustments(ByVal rep As IPackSplitOutput)
            rep.Save(Me, CStr(_userId))
        End Sub

        Private Sub SetAdjustmentQuantities(ByVal numberOfPacksSplit As Integer)
            _singleItem.AdjustmentQuantity = numberOfPacksSplit * _itemsPerPack
            _bulkItem.AdjustmentQuantity -= numberOfPacksSplit
        End Sub

        Friend Function NumberOfPacks() As Integer
            Return ((Math.Abs(SingleItem.StartQuantity)) \ _itemsPerPack) + AddOneIfNeeded()
        End Function

        Private Function AddOneIfNeeded() As Integer
            Dim ret As Integer = 1
            If Math.Abs(SingleItem.StartQuantity) Mod _itemsPerPack = 0 Then ret = 0
            Return ret
        End Function
    End Class
End Namespace