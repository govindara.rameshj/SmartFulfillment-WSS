﻿Imports System.IO
Imports OasysDBBO

Public Class frmWeeklyUpdate

    Dim _blnRunFromNightlyRoutine As Boolean
    Dim _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Dim _BOLog As BOStock.cStockLog

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        'Ask the user if they want to update, if they answer No the program ends.
        If MsgBox("Weekly System Update - Continue (Y/N):", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, MsgBoxStyle), "Daily Flash Totals Creation") = vbYes Then
            RunWeeklyUpdate()
        Else
            End
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub frmRollCyclicalHistory_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyValue = Keys.F10 Then
            Debug.Print("F10 Hit")
            btnExit_Click(sender, e)
        End If

        If e.KeyValue = Keys.F7 Then
            Debug.Print("F7 Hit")
            btnSelectAll_Click(sender, e)
        End If

        If e.KeyValue = Keys.F6 Then
            Debug.Print("F6 Hit")
            btnUnselectAll_Click(sender, e)
        End If

        If e.KeyValue = Keys.F5 Then
            Debug.Print("F5 Hit")
            btnSelect_Click(sender, e)
        End If

    End Sub

    Private Function RunWeeklyUpdate() As Boolean

        Try

            btnSelect.Enabled = False
            btnSelectAll.Enabled = False
            btnUnselectAll.Enabled = False

            Cursor = Cursors.WaitCursor

            If chkJob0.Checked = True Then
                UpdateStockMasterFile()
            End If

            Me.Refresh()
            Application.DoEvents()

            If chkJob1.Checked = True Then
                UpdateRelatedItemFile()
            End If

            Me.Refresh()
            Application.DoEvents()

            If chkJob2.Checked = True Then
                RollSupplierCycleCountWeek()
            End If

            Me.Refresh()
            Application.DoEvents()

            'Kill the TASKCOMP file if it exists
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

            End

        Catch ex As Exception
            Trace.WriteLine("Error in WeeklySystemUpdate: " & ex.Message.ToString)
        End Try

    End Function

    Private Sub UpdateStockMasterFile()

        Dim BOStock As New BOStock.cStock(_Oasys3DB)
        Dim BOHistory As New BOStock.cStockHistory(_Oasys3DB)
        Dim decAdjQuantity As Decimal = 0
        Dim decEndStockValue As Decimal = 0
        Dim decStartStockValue As Decimal = 0
        Dim decAdjValue As Decimal = 0
        Dim decTotalSold As Decimal = 0
        Dim decTotalDays As Decimal = 0
        Dim decStockDays As Decimal = 0
        Dim dteWeekEndDate As Date = Nothing
        Dim BOSystem As New BOSystem.cSystemDates(_Oasys3DB)
        BOSystem.LoadMatches()
        'dteWeekEndDate = BOSystem.

        BOStock.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOStock.WeeklyUpdate, False)
        Dim StockList As List(Of BOStock.cStock) = BOStock.LoadMatches()

        ProgressBar1.Minimum = 0
        ProgressBar1.Maximum = StockList.Count
        ProgressBar1.Visible = True

        lblStatus.Text = "Updating Stock Master File."
        Me.Refresh()
        Application.DoEvents()

        For Each StockItem As BOStock.cStock In StockList

            'Only update if the Weekly Update Flag is set
            If StockItem.WeeklyUpdate.Value = False Then
 
                StockItem.ValueSoldLastWeek.Value = StockItem.ValueSoldThisWeek.Value
                StockItem.ValueSoldThisWeek.Value = 0

                StockItem.UnitsSoldLastWeek.Value = StockItem.UnitsSoldThisWeek.Value
                StockItem.UnitsSoldThisWeek.Value = 0

                StockItem.WeeklyUpdate.Value = True
                If StockItem.DateFirstStock.Value = Nothing Or StockItem.DateFirstStock.Value = CDate("1/1/1900") Then

                    StockItem.UpdateWeeklyStock()

                Else
                    GetAverageWeeklySales(StockItem, StockItem.NoWeeksUpdated.Value, CInt(BOSystem.DaysOpen.Value))
                    GetAverageFourWeeklySales(StockItem, 4, CInt(BOSystem.DaysOpen.Value))

                    StockItem.UnitsSoldCurWkPlus14.Value = StockItem.UnitsSoldCurWkPlus13.Value
                    StockItem.UnitsSoldCurWkPlus13.Value = StockItem.UnitsSoldCurWkPlus12.Value
                    StockItem.UnitsSoldCurWkPlus12.Value = StockItem.UnitsSoldCurWkPlus11.Value
                    StockItem.UnitsSoldCurWkPlus11.Value = StockItem.UnitsSoldCurWkPlus10.Value
                    StockItem.UnitsSoldCurWkPlus10.Value = StockItem.UnitsSoldCurWkPlus9.Value
                    StockItem.UnitsSoldCurWkPlus9.Value = StockItem.UnitsSoldCurWkPlus8.Value
                    StockItem.UnitsSoldCurWkPlus8.Value = StockItem.UnitsSoldCurWkPlus7.Value
                    StockItem.UnitsSoldCurWkPlus7.Value = StockItem.UnitsSoldCurWkPlus6.Value
                    StockItem.UnitsSoldCurWkPlus6.Value = StockItem.UnitsSoldCurWkPlus5.Value
                    StockItem.UnitsSoldCurWkPlus5.Value = StockItem.UnitsSoldCurWkPlus4.Value
                    StockItem.UnitsSoldCurWkPlus4.Value = StockItem.UnitsSoldCurWkPlus3.Value
                    StockItem.UnitsSoldCurWkPlus3.Value = StockItem.UnitsSoldCurWkPlus2.Value
                    StockItem.UnitsSoldCurWkPlus2.Value = StockItem.UnitsSoldCurWkPlus1.Value
                    StockItem.UnitsSoldCurWkPlus1.Value = 0

                    StockItem.DaysOutStockPeriod14.Value = StockItem.DaysOutStockPeriod13.Value
                    StockItem.DaysOutStockPeriod13.Value = StockItem.DaysOutStockPeriod12.Value
                    StockItem.DaysOutStockPeriod12.Value = StockItem.DaysOutStockPeriod11.Value
                    StockItem.DaysOutStockPeriod11.Value = StockItem.DaysOutStockPeriod10.Value
                    StockItem.DaysOutStockPeriod10.Value = StockItem.DaysOutStockPeriod9.Value
                    StockItem.DaysOutStockPeriod9.Value = StockItem.DaysOutStockPeriod8.Value
                    StockItem.DaysOutStockPeriod8.Value = StockItem.DaysOutStockPeriod7.Value
                    StockItem.DaysOutStockPeriod7.Value = StockItem.DaysOutStockPeriod6.Value
                    StockItem.DaysOutStockPeriod6.Value = StockItem.DaysOutStockPeriod5.Value
                    StockItem.DaysOutStockPeriod5.Value = StockItem.DaysOutStockPeriod4.Value
                    StockItem.DaysOutStockPeriod4.Value = StockItem.DaysOutStockPeriod3.Value
                    StockItem.DaysOutStockPeriod3.Value = StockItem.DaysOutStockPeriod2.Value
                    StockItem.DaysOutStockPeriod2.Value = StockItem.DaysOutStockPeriod1.Value
                    StockItem.DaysOutStockPeriod1.Value = 0

                    decTotalSold = 0
                    decTotalDays = 0
                    decStockDays = 0

                    StockItem.NoWeeksUpdated.Value += 1
                    If StockItem.NoWeeksUpdated.Value > 13 Then StockItem.NoWeeksUpdated.Value = 13

                    StockItem.WeeklyUpdate.Value = True
                    StockItem.ToForecast.Value = True
                    StockItem.UpdateWeeklyStock()

                End If

            End If

            ProgressBar1.Value += 1
            Me.Refresh()
            Application.DoEvents()

        Next StockItem

        ProgressBar1.Visible = True
        lblStatus.Text = ""

    End Sub

    Private Sub UpdateRelatedItemFile()

        Dim BORelItm As New BOStock.cRelatedItems(_Oasys3DB)
        Dim RelItemList As List(Of BOStock.cRelatedItems) = BORelItm.LoadMatches()

        ProgressBar1.Value = 0
        ProgressBar1.Minimum = 0
        ProgressBar1.Maximum = RelItemList.Count
        ProgressBar1.Visible = True
        lblStatus.Text = "Updating Related Items File."

        Me.Refresh()
        Application.DoEvents()

        For Each RelatedItem As BOStock.cRelatedItems In RelItemList

            'Update each record in Related Items
            RelatedItem.TransForMUPPriorWeek.Value = RelatedItem.TransForMUPThisWeek.Value
            RelatedItem.MarkUpPriorWeek.Value = RelatedItem.MarkUpThisWeek.Value
            RelatedItem.TransForMUPThisWeek.Value = 0
            RelatedItem.MarkUpThisWeek.Value = 0
            RelatedItem.SaveIfExists()

            ProgressBar1.Value += 1
            ProgressBar1.Refresh()
            Application.DoEvents()

        Next RelatedItem

    End Sub

    Private Sub RollSupplierCycleCountWeek()

        Dim BOSystem As New BOSystem.cSystemDates(_Oasys3DB)

        ProgressBar1.Value = 0
        ProgressBar1.Minimum = 0
        ProgressBar1.Maximum = 1
        ProgressBar1.Visible = True
        lblStatus.Text = "Roll Supplier Cycle Count Week."

        Me.Refresh()
        Application.DoEvents()


        BOSystem.LoadMatches()
        BOSystem.UpdateCurrentWeekNumber()

        ProgressBar1.Value = 1
        Me.Refresh()
        Application.DoEvents()

    End Sub

    Private Sub UpdateAlphaLookupLog()
        'To Do: Waiting for Answer to wether or not its required
    End Sub

    Private Sub UpdateCustomerAccounts()
        'To Do: Waiting for Answer to wether or not its required
    End Sub

    Private Sub UpdateModelMasterFile()
        'To Do: Waiting for Answer to wether or not its required
    End Sub

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click

        chkJob0.Checked = True
        chkJob0.BackColor = Color.SteelBlue
        chkJob1.Checked = True
        chkJob1.BackColor = Color.SteelBlue
        chkJob2.Checked = True
        chkJob2.BackColor = Color.SteelBlue
        btnSelect.Enabled = True

    End Sub

    Private Sub btnUnselectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnselectAll.Click

        chkJob0.Checked = False
        chkJob0.BackColor = Color.White
        chkJob1.Checked = False
        chkJob1.BackColor = Color.White
        chkJob2.Checked = False
        chkJob2.BackColor = Color.White
        btnSelect.Enabled = False

    End Sub

    Private Sub frmWeeklyUpdate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim blnRunAutomatically As Boolean = False

        ' get command parameters 
        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd = "Y" Or strCmd = "y" Then
                blnRunAutomatically = True
            End If
        Next cmd

        Me.Show()
        Me.Refresh()

        chkJob1.Text = "Update the Related Item File."
        chkJob2.Text = "Roll Hierarchy Cycle Count Week."
        btnSelect.Enabled = False

        If blnRunAutomatically = True Then
            chkJob0.Checked = True
            chkJob1.Checked = True
            chkJob2.Checked = True
            Me.Refresh()
            Application.DoEvents()
            RunWeeklyUpdate()
            End
        End If

    End Sub

    Private Sub CheckedListBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckStateofButtons()
    End Sub

    Private Sub CheckedListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckStateofButtons()
    End Sub

    Private Sub CheckedListBox1_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
        CheckStateofButtons()
    End Sub

    Private Sub CheckStateofButtons()

        If chkJob0.Checked = True Then
            btnSelect.Enabled = True
            Exit Sub
        End If

        If chkJob1.Checked = True Then
            btnSelect.Enabled = True
            Exit Sub
        End If

        If chkJob2.Checked = True Then
            btnSelect.Enabled = True
            Exit Sub
        End If

        btnSelect.Enabled = False

    End Sub

    Private Function GetAverageWeeklySales(ByRef StockItem As BOStock.cStock, ByVal intNumbWeeksUpdated As Integer, ByVal intNumberOfDaysStoreOpen As Integer) As Decimal

        Dim decDaysOutofStock As Decimal = 0
        Dim decTotalDaysSold As Decimal = 0
        Dim decTotalSold As Decimal = 0
        Dim decTotalDays As Decimal = 0
        Dim decStockDays As Decimal = 0
        Dim decUnitsSoldArray() = {StockItem.UnitsSoldCurWkPlus1.Value, _
                                   StockItem.UnitsSoldCurWkPlus2.Value, _
                                   StockItem.UnitsSoldCurWkPlus3.Value, _
                                   StockItem.UnitsSoldCurWkPlus4.Value, _
                                   StockItem.UnitsSoldCurWkPlus5.Value, _
                                   StockItem.UnitsSoldCurWkPlus6.Value, _
                                   StockItem.UnitsSoldCurWkPlus7.Value, _
                                   StockItem.UnitsSoldCurWkPlus8.Value, _
                                   StockItem.UnitsSoldCurWkPlus9.Value, _
                                   StockItem.UnitsSoldCurWkPlus10.Value, _
                                   StockItem.UnitsSoldCurWkPlus11.Value, _
                                   StockItem.UnitsSoldCurWkPlus12.Value, _
                                   StockItem.UnitsSoldCurWkPlus13.Value, _
                                   StockItem.UnitsSoldCurWkPlus14.Value}

        Dim decUnitsOutOfStockArray() = {StockItem.DaysOutStockPeriod1.Value, _
                                         StockItem.DaysOutStockPeriod2.Value, _
                                         StockItem.DaysOutStockPeriod3.Value, _
                                         StockItem.DaysOutStockPeriod4.Value, _
                                         StockItem.DaysOutStockPeriod5.Value, _
                                         StockItem.DaysOutStockPeriod6.Value, _
                                         StockItem.DaysOutStockPeriod7.Value, _
                                         StockItem.DaysOutStockPeriod8.Value, _
                                         StockItem.DaysOutStockPeriod9.Value, _
                                         StockItem.DaysOutStockPeriod10.Value, _
                                         StockItem.DaysOutStockPeriod11.Value, _
                                         StockItem.DaysOutStockPeriod12.Value, _
                                         StockItem.DaysOutStockPeriod13.Value, _
                                         StockItem.DaysOutStockPeriod14.Value}


        For intIndex As Integer = 0 To intNumbWeeksUpdated
            decTotalSold += decUnitsSoldArray(intIndex)
            decDaysOutofStock += decUnitsOutOfStockArray(intIndex)
        Next intIndex
        'StockItem.DaysOutStockPeriod1.Value = decDaysOutofStock

        decTotalDays = (intNumberOfDaysStoreOpen * StockItem.NoWeeksUpdated.Value)
        decStockDays = (intNumberOfDaysStoreOpen * StockItem.NoWeeksUpdated.Value)


        If StockItem.DaysOutStockPeriod1.Value >= 64 Then
            decStockDays -= 64
            decStockDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 32 Then
            decStockDays -= 32
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 16 Then
            decStockDays -= 16
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 8 Then
            decStockDays -= 8
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 4 Then
            decStockDays -= 4
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 2 Then
            decStockDays -= 2
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 1 Then
            decStockDays -= 1
            decTotalDays -= 1
        End If


        Dim decValue As Decimal = 0
        If decStockDays > 0 Then
            decValue = ((decTotalSold * intNumberOfDaysStoreOpen) / decStockDays)
        End If

        StockItem.AvgWeeklySales.Value = Math.Round(decValue, 2)

    End Function

    Private Function GetAverageFourWeeklySales(ByRef StockItem As BOStock.cStock, ByVal intNumbWeeksUpdated As Integer, ByVal intNumberOfDaysStoreOpen As Integer) As Decimal

        Dim decDaysOutofStock As Decimal = 0
        Dim decTotalDaysSold As Decimal = 0
        Dim decTotalSold As Decimal = 0
        Dim decTotalDays As Decimal = 0
        Dim decStockDays As Decimal = 0
        Dim decUnitsSoldArray() = {StockItem.UnitsSoldCurWkPlus1.Value, _
                                   StockItem.UnitsSoldCurWkPlus2.Value, _
                                   StockItem.UnitsSoldCurWkPlus3.Value, _
                                   StockItem.UnitsSoldCurWkPlus4.Value, _
                                   StockItem.UnitsSoldCurWkPlus5.Value, _
                                   StockItem.UnitsSoldCurWkPlus6.Value, _
                                   StockItem.UnitsSoldCurWkPlus7.Value, _
                                   StockItem.UnitsSoldCurWkPlus8.Value, _
                                   StockItem.UnitsSoldCurWkPlus9.Value, _
                                   StockItem.UnitsSoldCurWkPlus10.Value, _
                                   StockItem.UnitsSoldCurWkPlus11.Value, _
                                   StockItem.UnitsSoldCurWkPlus12.Value, _
                                   StockItem.UnitsSoldCurWkPlus13.Value, _
                                   StockItem.UnitsSoldCurWkPlus14.Value}

        Dim decUnitsOutOfStockArray() = {StockItem.DaysOutStockPeriod1.Value, _
                                         StockItem.DaysOutStockPeriod2.Value, _
                                         StockItem.DaysOutStockPeriod3.Value, _
                                         StockItem.DaysOutStockPeriod4.Value, _
                                         StockItem.DaysOutStockPeriod5.Value, _
                                         StockItem.DaysOutStockPeriod6.Value, _
                                         StockItem.DaysOutStockPeriod7.Value, _
                                         StockItem.DaysOutStockPeriod8.Value, _
                                         StockItem.DaysOutStockPeriod9.Value, _
                                         StockItem.DaysOutStockPeriod10.Value, _
                                         StockItem.DaysOutStockPeriod11.Value, _
                                         StockItem.DaysOutStockPeriod12.Value, _
                                         StockItem.DaysOutStockPeriod13.Value, _
                                         StockItem.DaysOutStockPeriod14.Value}

        For intIndex As Integer = 0 To 3
            decTotalSold += decUnitsSoldArray(intIndex)
            decDaysOutofStock += decUnitsOutOfStockArray(intIndex)
        Next intIndex
        'StockItem.DaysOutStockPeriod1.Value = 4 'decDaysOutofStock

        decTotalDays = (intNumberOfDaysStoreOpen * intNumbWeeksUpdated)
        decStockDays = (intNumberOfDaysStoreOpen * intNumbWeeksUpdated)

        If StockItem.DaysOutStockPeriod1.Value >= 64 Then
            decStockDays -= 64
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 32 Then
            decStockDays -= 32
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 16 Then
            decStockDays -= 16
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 8 Then
            decStockDays -= 8
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 4 Then
            decStockDays -= 4
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 2 Then
            decStockDays -= 2
            decTotalDays -= 1
        End If

        If StockItem.DaysOutStockPeriod1.Value >= 1 Then
            decStockDays -= 1
            decTotalDays -= 1
        End If

        Dim decValue As Decimal = 0
        If decTotalDays > 0 Then
            decValue = ((decTotalSold * intNumberOfDaysStoreOpen) / decStockDays)
        End If
        StockItem.AverageWeeklySales.Value = Math.Round(decValue, 2)

        If StockItem.NoWeeksUpdated.Value < 2 Or (decTotalDays / 2) > decStockDays Then
            StockItem.ConfidentWeeklyAvge.Value = False
        End If

    End Function

    Private Sub chkJob0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkJob0.Click
        If chkJob0.Checked Then
            chkJob0.BackColor = Color.SteelBlue
        Else
            chkJob0.BackColor = Color.White
        End If
        CheckStateofButtons()
    End Sub

    Private Sub chkJob1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkJob1.Click
        If chkJob1.Checked Then
            chkJob1.BackColor = Color.SteelBlue
        Else
            chkJob1.BackColor = Color.White
        End If
        CheckStateofButtons()
    End Sub

    Private Sub ChkJob2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkJob2.Click
        If chkJob2.Checked Then
            chkJob2.BackColor = Color.SteelBlue
        Else
            chkJob2.BackColor = Color.White
        End If
        CheckStateofButtons()
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Cts.Oasys.Core.Tests.TestEnvironmentSetup.SetupIfTestRun()

        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        _BOLog = New BOStock.cStockLog(_Oasys3DB)

    End Sub
End Class
