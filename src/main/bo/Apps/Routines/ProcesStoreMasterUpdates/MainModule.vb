﻿Imports System.IO
Imports OasysDBBO.Oasys3.DB.clsOasys3DB
Module MainModule
    ' all these fields place here so that they can be seen throughout the code
    Dim hash As Integer
    Dim didCount As Integer
    Dim ErrorMessage As String = String.Empty
    Dim line As String
    Dim writer As StreamWriter
    Dim writer2 As StreamWriter
    Dim SmRcnt As Integer
    Dim PCRcnt As Integer
    Dim ThisVersion As String
    Dim ThisSequence As String
    Dim HoRecords As Integer
    Dim HoWrongStore As Integer
    Dim HoBadType As Integer
    Dim HoNewItems As Integer
    Dim HoBadNew As Integer
    Dim HoChanges As Integer
    Dim HoBadChanges As Integer
    Dim HoClass As Integer
    Dim SDDate As Date

    Sub Main()
        Dim startKey As String
        Dim endKey As String

        Dim dl As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        Dim tvcstr As New BOStoreTransValCtl.cStoreTransValCtl(dl)
        Dim lsttsh As List(Of BOStoreTransValCtl.cStoreTransValCtl)
        Dim sd As New BOSystem.cSystemDates(dl)

        sd.AddLoadFilter(eOperator.pEquals, sd.SystemDatesID, "01")
        sd.LoadMatches()
        SDDate = sd.Today.Value

        'find first record to get version no 
        tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.FileName, "HOSTD")
        tvcstr.JoinLoadFilter(eOperator.pAnd)
        tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.VersionNo, "00")
        tvcstr.LoadMatches()
        Do While (tvcstr.PVersion.Value <> tvcstr.LastAvailVersionNo.Value)

            startKey = tvcstr.PVersion.Value
            endKey = tvcstr.LastAvailVersionNo.Value
            ' find  one in sequence
            tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.FileName, "HOSTD")
            tvcstr.JoinLoadFilter(eOperator.pAnd)
            tvcstr.AddLoadFilter(eOperator.pGreaterThanOrEquals, tvcstr.VersionNo, startKey)
            tvcstr.JoinLoadFilter(eOperator.pAnd)
            tvcstr.AddLoadFilter(eOperator.pLessThanOrEquals, tvcstr.VersionNo, endKey)
            tvcstr.JoinLoadFilter(eOperator.pAnd)
            tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.Flag, False)
            lsttsh = tvcstr.LoadMatches()
            For Each row As BOStoreTransValCtl.cStoreTransValCtl In lsttsh
                HoRecords = 0
                HoWrongStore = 0
                HoBadType = 0
                HoNewItems = 0
                HoBadNew = 0
                HoChanges = 0
                HoBadChanges = 0
                HoClass = 0
                ThisVersion = row.VersionNo.Value
                ThisSequence = row.Sequence.Value
                process(ThisVersion)
                tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.FileName, "HOSTD")
                tvcstr.JoinLoadFilter(eOperator.pAnd)
                tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.VersionNo, "00")
                tvcstr.LoadMatches()
                tvcstr.PVersion.Value = ThisVersion
                tvcstr.PSequence.Value = ThisSequence
                tvcstr.SaveIfExists()
            Next
            ' read again to check that more records have not been added
            tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.FileName, "HOSTD")
            tvcstr.JoinLoadFilter(eOperator.pAnd)
            tvcstr.AddLoadFilter(eOperator.pEquals, tvcstr.VersionNo, "00")
            tvcstr.LoadMatches()
        Loop

    End Sub
    Private Sub process(ByVal versionno As String)
        Dim sr As StreamReader
        Dim HoClass As Integer
        Dim SmPcnt As Integer
        Dim Deleteing As Boolean
        Dim adding As Boolean
        Dim didCount As Integer
        Dim HoRecords As Integer
        Dim status As String = String.Empty
        Dim OSKey As String = String.Empty
        Dim str As New BOStore.cStore

        'write data to ascii file 

        Dim filename As String = "C:\COMMS\HOSTD" & versionno & ".txt"

        If File.Exists(filename) Then
            didCount = didCount + 1
            HoRecords = HoRecords + 1
            sr = New StreamReader(filename)
            While Not sr.EndOfStream
                line = sr.ReadLine()
                If Mid(line, 1, 2) = "SM" Then
                    HoClass = HoClass + 1
                    SmPcnt = SmPcnt + 1
                    status = Mid(line, 22, 1)
                    didCount = didCount + 1
                    HoRecords = HoRecords + 1
                    If Mid(line, 23, 1) = "D" Then
                        Deleteing = True
                    Else
                        Deleteing = False
                    End If
                    If status = " " Or status = "D" Then

                    Else
                        ErrorMessage = "Invalid Status    "
                        SthoaError()
                    End If
                    OSKey = Mid(line, 23, 3)
                    If Not OSKey <> "   " Then 'Was 'If Not "   " Then' here. It is very strange, because it always fails in runtime. Changed to OSKey <> "   "
                        If Deleteing = True Then
                            str.StoreID.Value = OSKey
                            str.Delete()
                        Else
                            str.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, str.StoreID, OSKey)
                            If str.LoadMatches.Count = 0 Then
                                adding = True
                            Else
                                adding = False
                            End If
                            str.StoreID.Value = OSKey
                            str.AddressLine1.Value = Mid(line, 26, 30)
                            str.AddressLine2.Value = Mid(line, 56, 30)
                            str.AddressLine3.Value = Mid(line, 86, 30)
                            str.AddressLine4.Value = Mid(line, 116, 30)
                            str.StoreNameOnReceipt.Value = Mid(line, 146, 12)
                            str.Telephone.Value = Mid(line, 158, 12)
                            str.Fax.Value = Mid(line, 170, 12)
                            str.ManagerName.Value = Mid(line, 200, 20)
                            str.RegionCode.Value = Mid(line, 220, 2)
                            str.IsDeleted.Value = CBool(Mid(line, 222, 1))
                            If adding = True Then
                                str.SaveIfNew()
                            Else
                                str.SaveIfExists()
                            End If

                        End If
                    Else
                        ErrorMessage = "Invalid Store     "
                        SthoaError()
                    End If

                ElseIf Mid(line, 1, 2) = "HR" Then
                    If Mid(line, 32, 10) = "LOAD      " Then
                        DeleteStores()
                    End If
                ElseIf Mid(line, 1, 2) = "TR" Or Mid(line, 1, 2) = "HR" Then
                    'do nothing
                Else
                    HoBadType = HoBadType + 1
                    SthoaError()
                End If
            End While
            sr.Close()
            EndProcess()

        Else
            'error message here somehow
        End If
    End Sub
    Private Sub DeleteStores()
        Dim str As New BOStore.cStore
        Dim lststr As List(Of BOStore.cStore)
        lststr = str.LoadMatches()
        For Each row As BOStore.cStore In lststr
            row.IsDeleted.Value = True
            row.SaveIfExists()
        Next
    End Sub
    Private Sub SthoaError()
        Dim filename As String = "C:\COMMS\STHOA.txt"
        Dim filename2 As String = "C:\COMMS\STHOC.txt"
        Dim writer As StreamWriter
        Dim outText As String
        If File.Exists(filename) Then
            writer = New StreamWriter("C:\COMMS\STOHA.txt", True)
        Else
            writer = New StreamWriter("C:\COMMS\STOHA.txt", False)
        End If

        outText = String.Empty
        outText = outText & "AU"
        outText = outText & SDDate.ToString("dd/MM/yy")
        outText = outText & hash.ToString("##########0").PadLeft(12, "0"c)
        outText = outText & "D"
        outText = outText & hash.ToString("##########0").PadLeft(12, "0"c)
        outText = outText & ErrorMessage
        outText = outText & Mid(line, 22, 1)
        outText = outText & Mid(line, 23, 3)
        writer.WriteLine(outText)

        If File.Exists(filename2) Then
            writer2 = New StreamWriter("C:\COMMS\STOHC.txt", True)
        Else
            writer2 = New StreamWriter("C:\COMMS\STOHC.txt", False)
        End If
        outText = String.Empty
        outText = outText & "CD"
        outText = outText & ThisVersion
        outText = outText & hash.ToString("##########0").PadLeft(12, "0"c)
        outText = outText & "D"
        outText = outText & hash.ToString("##########0").PadLeft(12, "0"c)
        outText = outText & ErrorMessage
        outText = outText & Mid(line, 22, 1)
        outText = outText & Mid(line, 23, 3)

        SmRcnt = SmRcnt + 1
        writer2.WriteLine(outText)

        writer2.Close()
        writer.Close()

        hash = didCount
    End Sub
    Private Sub EndProcess()
        Dim blank As String = " "
        Dim filename As String = "C:\COMMS\STHOA.txt"
        Dim filename2 As String = "C:\COMMS\STHOC.txt"
        Dim writer As StreamWriter
        Dim outText As String
        If File.Exists(filename) Then
            writer = New StreamWriter("C:\COMMS\STOHA.txt", True)
        Else
            writer = New StreamWriter("C:\COMMS\STOHA.txt", False)
        End If

        outText = String.Empty
        outText = blank.PadRight(178, " "c)
        Mid(outText, 1, 2) = "AU"
        Mid(outText, 3, 8) = SDDate.ToString("dd/MM/yy")
        Mid(outText, 11, 12) = hash.ToString("##########0").PadLeft(12, "0"c)
        Mid(outText, 23, 1) = "C"
        Mid(outText, 24, 12) = hash.ToString("##########0").PadLeft(12, "0"c)
        Mid(outText, 36, 8) = HoRecords.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 44, 8) = HoWrongStore.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 51, 8) = HoBadType.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 60, 8) = HoNewItems.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 68, 8) = HoBadNew.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 76, 8) = "      0 "
        Mid(outText, 83, 8) = "      0 "
        Mid(outText, 92, 8) = "      0 "
        Mid(outText, 100, 8) = "      0 "
        Mid(outText, 108, 8) = HoChanges.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 116, 8) = HoBadChanges.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 124, 8) = "      0 "
        Mid(outText, 132, 8) = "      0 "
        Mid(outText, 140, 8) = HoClass.ToString("######0 ").PadLeft(8, "0"c)
        Mid(outText, 148, 8) = "      0 "
        Mid(outText, 156, 8) = "      0 "
        Mid(outText, 163, 8) = "      0 "
        Mid(outText, 172, 8) = "      0 "
        writer.WriteLine(outText)
        writer.Close()
    End Sub
End Module
