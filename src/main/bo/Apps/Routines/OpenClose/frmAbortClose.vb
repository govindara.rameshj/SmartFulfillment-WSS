﻿Public Class frmAbortClose

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _Cancel As Boolean = False

    Public Property Cancel() As Boolean
        Get
            Cancel = _Cancel
        End Get
        Set(ByVal value As Boolean)
            _Cancel = value
        End Set
    End Property

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
    End Sub

    Private Sub frmAbortClose_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyData = Keys.Escape Then
            MakeSureUserWantstoAbort()
        End If
    End Sub

    Private Sub frmAbortClose_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim parmtable As New BOSystem.cParameter
        Const PRM_ABORT_TIMEOUT As Long = 150001
        Dim lngTimeOut As Long = 10 * 1000
        Dim strTimeOut As String = ""

        Try

            parmtable.ConnectionString = _Oasys3DB.ConnectionString
            parmtable.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, parmtable.ParameterID, PRM_ABORT_TIMEOUT)
            parmtable.LoadMatches()

            lngTimeOut = parmtable.LongValue.Value
            If lngTimeOut > 0 Then
                lngTimeOut *= 1000
            Else
                lngTimeOut = 10000 'Default to 10 seconds
            End If
            Timer1.Interval = CInt(lngTimeOut)
            Timer1.Enabled = True

        Catch
            Trace.WriteLine("frmAbortClose failed to get timeout parameter.")
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Cancel = False
        Me.Hide()
    End Sub

    Private Sub btnAbort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbort.Click
        MakeSureUserWantstoAbort()
    End Sub

    Private Sub MakeSureUserWantstoAbort()

        If MsgBox("Are you sure you want to abort the night routine ?", MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Abort Night Routine - Confirm") = MsgBoxResult.Yes Then
            Cancel = True
            Me.Hide()
        Else
            Cancel = False
            Me.Hide()
        End If
    End Sub

End Class