﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpenClose
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOpenClose))
        Me.ssTask = New FarPoint.Win.Spread.FpSpread
        Me.ssTask_SheetView1 = New FarPoint.Win.Spread.SheetView
        Me.txtCurrentTask = New System.Windows.Forms.TextBox
        Me.lblCurrentTask = New System.Windows.Forms.Label
        Me.txtCurrentTaskDesc = New System.Windows.Forms.TextBox
        Me.lblProgress = New System.Windows.Forms.Label
        Me.PBarProgress = New System.Windows.Forms.ProgressBar
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblMode = New System.Windows.Forms.Label
        CType(Me.ssTask, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssTask_SheetView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ssTask
        '
        Me.ssTask.About = "3.0.2004.2005"
        Me.ssTask.AccessibleDescription = "FpSpread1, Sheet1, Row 0, Column 0, "
        Me.ssTask.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ssTask.BackColor = System.Drawing.SystemColors.Control
        Me.ssTask.Location = New System.Drawing.Point(6, 78)
        Me.ssTask.Name = "ssTask"
        Me.ssTask.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssTask.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssTask_SheetView1})
        Me.ssTask.Size = New System.Drawing.Size(513, 424)
        Me.ssTask.TabIndex = 0
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssTask.TextTipAppearance = TipAppearance1
        Me.ssTask.Visible = False
        '
        'ssTask_SheetView1
        '
        Me.ssTask_SheetView1.Reset()
        Me.ssTask_SheetView1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssTask_SheetView1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssTask_SheetView1.ColumnCount = 3
        Me.ssTask_SheetView1.RowCount = 0
        Me.ssTask_SheetView1.ColumnHeader.Cells.Get(0, 0).Value = "Task No."
        Me.ssTask_SheetView1.ColumnHeader.Cells.Get(0, 1).Value = "Description"
        Me.ssTask_SheetView1.ColumnHeader.Cells.Get(0, 2).Value = "Status"
        Me.ssTask_SheetView1.Columns.Get(0).Label = "Task No."
        Me.ssTask_SheetView1.Columns.Get(0).Locked = True
        Me.ssTask_SheetView1.Columns.Get(1).Label = "Description"
        Me.ssTask_SheetView1.Columns.Get(1).Locked = True
        Me.ssTask_SheetView1.Columns.Get(1).Width = 264.0!
        Me.ssTask_SheetView1.Columns.Get(2).Label = "Status"
        Me.ssTask_SheetView1.Columns.Get(2).Locked = True
        Me.ssTask_SheetView1.Columns.Get(2).Width = 78.0!
        Me.ssTask_SheetView1.RowHeader.Columns.Default.Resizable = False
        Me.ssTask_SheetView1.RowHeader.Visible = False
        Me.ssTask_SheetView1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        Me.ssTask.SetActiveViewport(0, 1, 0)
        '
        'txtCurrentTask
        '
        Me.txtCurrentTask.Location = New System.Drawing.Point(77, 23)
        Me.txtCurrentTask.Name = "txtCurrentTask"
        Me.txtCurrentTask.ReadOnly = True
        Me.txtCurrentTask.Size = New System.Drawing.Size(65, 20)
        Me.txtCurrentTask.TabIndex = 1
        Me.txtCurrentTask.Visible = False
        '
        'lblCurrentTask
        '
        Me.lblCurrentTask.Location = New System.Drawing.Point(3, 23)
        Me.lblCurrentTask.Name = "lblCurrentTask"
        Me.lblCurrentTask.Size = New System.Drawing.Size(68, 20)
        Me.lblCurrentTask.TabIndex = 2
        Me.lblCurrentTask.Text = "Current Task"
        Me.lblCurrentTask.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCurrentTask.Visible = False
        '
        'txtCurrentTaskDesc
        '
        Me.txtCurrentTaskDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCurrentTaskDesc.Location = New System.Drawing.Point(148, 23)
        Me.txtCurrentTaskDesc.Name = "txtCurrentTaskDesc"
        Me.txtCurrentTaskDesc.ReadOnly = True
        Me.txtCurrentTaskDesc.Size = New System.Drawing.Size(371, 20)
        Me.txtCurrentTaskDesc.TabIndex = 3
        Me.txtCurrentTaskDesc.Visible = False
        '
        'lblProgress
        '
        Me.lblProgress.Location = New System.Drawing.Point(3, 49)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(68, 20)
        Me.lblProgress.TabIndex = 5
        Me.lblProgress.Text = "Progress"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblProgress.Visible = False
        '
        'PBarProgress
        '
        Me.PBarProgress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PBarProgress.Location = New System.Drawing.Point(148, 49)
        Me.PBarProgress.Name = "PBarProgress"
        Me.PBarProgress.Size = New System.Drawing.Size(371, 23)
        Me.PBarProgress.TabIndex = 6
        Me.PBarProgress.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(113, 223)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(277, 59)
        Me.Panel1.TabIndex = 9
        Me.Panel1.UseWaitCursor = True
        Me.Panel1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(251, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Checking for available printers"
        Me.Label1.UseWaitCursor = True
        '
        'lblMode
        '
        Me.lblMode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMode.Location = New System.Drawing.Point(3, 3)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(516, 20)
        Me.lblMode.TabIndex = 10
        Me.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMode.Visible = False
        '
        'frmOpenClose
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblMode)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PBarProgress)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.txtCurrentTaskDesc)
        Me.Controls.Add(Me.lblCurrentTask)
        Me.Controls.Add(Me.txtCurrentTask)
        Me.Controls.Add(Me.ssTask)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmOpenClose"
        Me.Size = New System.Drawing.Size(525, 505)
        CType(Me.ssTask, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssTask_SheetView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ssTask As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssTask_SheetView1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents txtCurrentTask As System.Windows.Forms.TextBox
    Friend WithEvents lblCurrentTask As System.Windows.Forms.Label
    Friend WithEvents txtCurrentTaskDesc As System.Windows.Forms.TextBox
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents PBarProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblMode As System.Windows.Forms.Label

End Class
