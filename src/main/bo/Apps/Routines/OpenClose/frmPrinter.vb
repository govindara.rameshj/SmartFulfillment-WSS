﻿Imports System.Drawing.Printing

Public Class frmPrinter

    Private prtdoc As New PrintDocument
    Private strDefaultPrinter As String = prtdoc.PrinterSettings.PrinterName


    Private Sub frmPrinter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strDefaultPrinter As String = prtdoc.PrinterSettings.PrinterName

        'Get list of printers available
        For Each strPrinter In PrinterSettings.InstalledPrinters
            cmbPrinters.Items.Add(strPrinter)
            If CStr(strPrinter) = strDefaultPrinter Then
                cmbPrinters.SelectedIndex = cmbPrinters.Items.IndexOf(strPrinter)
            End If
            Application.DoEvents()
        Next strPrinter

    End Sub

    Public Shared Function GetDefaultSystemPrinter() As String
        Dim pd As New PrintDocument
        Return pd.PrinterSettings.PrinterName '/ Get the system default printer
    End Function

    <System.Runtime.InteropServices.DllImport("Winspool.drv")> _
    Private Shared Function SetDefaultPrinter(ByVal printerName As String) As Boolean
    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub

    Private Sub cmbPrinters_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPrinters.SelectedIndexChanged

        Dim prtdoc As New PrintDocument

        prtdoc.PrinterSettings.PrinterName = cmbPrinters.Items(cmbPrinters.SelectedIndex).ToString
        SetDefaultPrinter(prtdoc.PrinterSettings.PrinterName)

    End Sub

End Class