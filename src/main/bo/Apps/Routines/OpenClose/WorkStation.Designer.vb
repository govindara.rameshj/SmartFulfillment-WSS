﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WorkStation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WorkStation))
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.lblmessage1 = New System.Windows.Forms.Label
        Me.lblMessage2 = New System.Windows.Forms.Label
        Me.lblMessage3 = New System.Windows.Forms.Label
        Me.lblMessage4 = New System.Windows.Forms.Label
        Me.lblMessage5 = New System.Windows.Forms.Label
        Me.FpWorkstation = New FarPoint.Win.Spread.FpSpread
        Me.FpWorkstation_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnRefresh = New System.Windows.Forms.Button
        CType(Me.FpWorkstation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpWorkstation_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblmessage1
        '
        Me.lblmessage1.AutoSize = True
        Me.lblmessage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmessage1.Location = New System.Drawing.Point(21, 9)
        Me.lblmessage1.Name = "lblmessage1"
        Me.lblmessage1.Size = New System.Drawing.Size(346, 20)
        Me.lblmessage1.TabIndex = 0
        Me.lblmessage1.Text = "Please close the workstations listed below"
        '
        'lblMessage2
        '
        Me.lblMessage2.AutoSize = True
        Me.lblMessage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage2.Location = New System.Drawing.Point(22, 40)
        Me.lblMessage2.Name = "lblMessage2"
        Me.lblMessage2.Size = New System.Drawing.Size(315, 17)
        Me.lblMessage2.TabIndex = 1
        Me.lblMessage2.Text = "If it is not possible to close any of them manually "
        '
        'lblMessage3
        '
        Me.lblMessage3.AutoSize = True
        Me.lblMessage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage3.Location = New System.Drawing.Point(22, 57)
        Me.lblMessage3.Name = "lblMessage3"
        Me.lblMessage3.Size = New System.Drawing.Size(313, 17)
        Me.lblMessage3.TabIndex = 2
        Me.lblMessage3.Text = "you can scroll and select a workstation to flag as"
        '
        'lblMessage4
        '
        Me.lblMessage4.AutoSize = True
        Me.lblMessage4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage4.Location = New System.Drawing.Point(22, 74)
        Me.lblMessage4.Name = "lblMessage4"
        Me.lblMessage4.Size = New System.Drawing.Size(342, 17)
        Me.lblMessage4.TabIndex = 3
        Me.lblMessage4.Text = "closed. You will need to contact the WICKES support "
        '
        'lblMessage5
        '
        Me.lblMessage5.AutoSize = True
        Me.lblMessage5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage5.Location = New System.Drawing.Point(22, 91)
        Me.lblMessage5.Name = "lblMessage5"
        Me.lblMessage5.Size = New System.Drawing.Size(269, 17)
        Me.lblMessage5.TabIndex = 4
        Me.lblMessage5.Text = "desk for a password if you use this option"
        '
        'FpWorkstation
        '
        Me.FpWorkstation.About = "3.0.2004.2005"
        Me.FpWorkstation.AccessibleDescription = "FpWorkstation, Sheet1, Row 0, Column 0, "
        Me.FpWorkstation.BackColor = System.Drawing.SystemColors.Control
        Me.FpWorkstation.EditModePermanent = True
        Me.FpWorkstation.FocusRenderer = New FarPoint.Win.Spread.CustomFocusIndicatorRenderer(CType(resources.GetObject("FpWorkstation.FocusRenderer"), System.Drawing.Bitmap), 1)
        Me.FpWorkstation.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.FpWorkstation.Location = New System.Drawing.Point(25, 130)
        Me.FpWorkstation.Name = "FpWorkstation"
        Me.FpWorkstation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FpWorkstation.SelectionBlockOptions = FarPoint.Win.Spread.SelectionBlockOptions.Rows
        Me.FpWorkstation.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.FpWorkstation_Sheet1})
        Me.FpWorkstation.Size = New System.Drawing.Size(342, 336)
        Me.FpWorkstation.TabIndex = 5
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.FpWorkstation.TextTipAppearance = TipAppearance1
        '
        'FpWorkstation_Sheet1
        '
        Me.FpWorkstation_Sheet1.Reset()
        Me.FpWorkstation_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.FpWorkstation_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.FpWorkstation_Sheet1.ColumnCount = 2
        Me.FpWorkstation_Sheet1.RowCount = 1
        Me.FpWorkstation_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "Number"
        Me.FpWorkstation_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Description"
        Me.FpWorkstation_Sheet1.Columns.Get(0).Label = "Number"
        Me.FpWorkstation_Sheet1.Columns.Get(0).Locked = False
        Me.FpWorkstation_Sheet1.Columns.Get(1).Label = "Description"
        Me.FpWorkstation_Sheet1.Columns.Get(1).Locked = False
        Me.FpWorkstation_Sheet1.Columns.Get(1).Width = 259.0!
        Me.FpWorkstation_Sheet1.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect
        Me.FpWorkstation_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.FpWorkstation_Sheet1.RowHeader.Visible = False
        Me.FpWorkstation_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.FpWorkstation_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.FpWorkstation_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(7, 490)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(310, 490)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 39)
        Me.btnRefresh.TabIndex = 7
        Me.btnRefresh.Text = "F5 &Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'WorkStation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(397, 535)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.FpWorkstation)
        Me.Controls.Add(Me.lblMessage5)
        Me.Controls.Add(Me.lblMessage4)
        Me.Controls.Add(Me.lblMessage3)
        Me.Controls.Add(Me.lblMessage2)
        Me.Controls.Add(Me.lblmessage1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "WorkStation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "WorkStation"
        CType(Me.FpWorkstation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpWorkstation_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblmessage1 As System.Windows.Forms.Label
    Friend WithEvents lblMessage2 As System.Windows.Forms.Label
    Friend WithEvents lblMessage3 As System.Windows.Forms.Label
    Friend WithEvents lblMessage4 As System.Windows.Forms.Label
    Friend WithEvents lblMessage5 As System.Windows.Forms.Label
    Friend WithEvents FpWorkstation As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpWorkstation_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
End Class
