﻿Imports System.Text
Imports System.Reflection
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Tests
Imports Cts.Oasys.Data
Imports Cts.Oasys.Data.Command
Imports Pic.Count.Apply
Imports StockAdjustments


Public Class frmOpenClose
    Private Enum FormTypes
        None
        Open
        Close
    End Enum
    Private _FormType As FormTypes = FormTypes.None
    Private MODULE_FUNCTION As String = "Open Close"
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _SysDates As New BOSystem.cSystemDates(_Oasys3DB)

    Private _dt As DataTable
    Private _drStoreStatus As DataRow
    Private _drRetailOptions As DataRow

    Private _inRetryMode As Boolean = False
    Private _setNumber As String = String.Empty
    Private _lastTask As String = String.Empty
    Private _isLive As Boolean = False
    Private _nextOpenDate As Date = Nothing
    Private _dayCode As Decimal = 0
    Private _isFromNightlyClose As Boolean = False

    Private _isMaster As Boolean = False
    Private _testAgain As Boolean = False
    Private _intNumTasks As Integer = 0

    Private _isWeekEnd As Boolean = False
    Private _isPeriodEnd As Boolean = False
    Private _isCloseCompleted As Boolean = False
    Private _isFirstRun As Boolean = True

    Private _endProgram As Boolean = False
    Private _isFirstTime As Boolean = True
    Private _endProcess As Boolean = False

    Private _intCountPrcchg3DaysLeft As Integer = 0
    Private _intCountPrcchgAlreadyHeppened As Integer = 0

    Public _masterId As Long = -1

#Region "Error codes"
    'Error Codes 1-100 reserved for Oasys System Errors
    Public Const OASYS_ERR_CLASS_NOT_FOUND = vbObjectError + 1
    Public Const OASYS_ERR_DUPLICATE_CLASS = vbObjectError + 2
    Public Const OASYS_ERR_NULL_OBJECT_REFERENCE = vbObjectError + 3
    Public Const OASYS_ERR_ROOT_NOT_INITIALISED = vbObjectError + 4
    Public Const OASYS_ERR_INVALID_ENTERPRISE_ID = vbObjectError + 5
    Public Const OASYS_ERR_INCONSISTENT_OASYS_SETUP = vbObjectError + 6
    Public Const OASYS_ERR_INCONSISTENT_OBJECT = vbObjectError + 7
    Public Const OASYS_ERR_NOT_IMPLEMENTED = vbObjectError + 8
    Public Const OASYS_ERR_NOT_INSTALLED = vbObjectError + 9
    Public Const OASYS_ERR_BO_CLASS_INCOMPLETE = vbObjectError + 10
    Public Const OASYS_ERR_NO_DB_CONNECTION = vbObjectError + 11
    Public Const OASYS_ERR_GETFIELD_INVALID_FID = vbObjectError + 12
    Public Const OASYS_ERR_NON_UNIQUE_SELECTION = vbObjectError + 13
    Public Const OASYS_ERR_BO_METHOD_NOT_IMPLEMENTED = vbObjectError + 14
    Public Const OASYS_ERR_VIEWBO_METHOD_INVALID = vbObjectError + 15

    'Error Codes 101-199 reserved for General Oasys Errors
    Public Const OASYS_ERR_PARAMETER_NOT_KNOWN = vbObjectError + 101
    Public Const OASYS_ERR_INVALID_WORKSTATIONID = vbObjectError + 102
    Public Const OASYS_ERR_INVALID_USERID = vbObjectError + 103
    Public Const OASYS_ERR_INVALID_EDI_PATH = vbObjectError + 104
    Public Const OASYS_ERR_MISSING_SYSTEMOPTIONS = vbObjectError + 105
    Public Const OASYS_ERR_MISSING_RETAILEROPTIONS = vbObjectError + 110
    Public Const OASYS_ERR_UNABLE_To_LOG_TASK = vbObjectError + 111
    Public Const OASYS_ERR_INVALID_PARAMETER_LINE = vbObjectError + 120
    Public Const OASYS_ERR_MISSING_INVENTORY_RECORD = vbObjectError + 121
    Public Const OASYS_ERR_INVALID_PAYROLLNUMBER = vbObjectError + 122
    Public Const OASYS_ERR_NOMENUACCESS = vbObjectError + 123
    Public Const OASYS_ERR_USER_LOCKEDOUT = vbObjectError + 124
    'Block reserved for use by PCI for NITMAS conversion 130-150
    Public Const OASYS_ERR_NITMAS1 = vbObjectError + 130
    Public Const OASYS_ERR_COMMS_PATH_NOT_FOUND = vbObjectError + 131
    Public Const OASYS_ERR_INCOMPLETE_SYSTEM_UPDATE = vbObjectError + 132
    Public Const OASYS_ERR_UPDATE_FAILED = vbObjectError + 133
    Public Const OASYS_ERR_INVALID_PERIOD_END_SETUP = vbObjectError + 134
    Public Const OASYS_ERR_INSERT_FAILED = vbObjectError + 135
    Public Const OASYS_ERR_MISSING_EDI_FILE_CONTROL_RECORD = vbObjectError + 136
    Public Const OASYS_ERR_EDI_CORRUPT_TRANSMISSION_FILE = vbObjectError + 137
    Public Const OASYS_ERR_DELETE_FAILED = vbObjectError + 138
    Public Const OASYS_ERR_TRANSMISSION_CONTROL_FILE_INCONSISTENCY = vbObjectError + 139
    Public Const OASYS_ERR_STORE_NOT_OPEN = vbObjectError + 140
    Public Const OASYS_ERR_MENU_OPTIONS_FILE_NOT_FOUND = vbObjectError + 141
    Public Const OASYS_ERR_MENU_OPTIONS_FILE_EMPTY = vbObjectError + 142
    Public Const OASYS_ERR_NIGHTLY_ROUTINE_FILE_NOT_FOUND = vbObjectError + 143
    Public Const OASYS_ERR_NIGHTLY_ROUTINE_FILE_EMPTY = vbObjectError + 144
    Public Const OASYS_ERR_MISSING_TRANSMISSION_FILE = vbObjectError + 145
    Public Const OASYS_ERR_MISSING_TCFCTL_RECORD = vbObjectError + 146

    Public Const OASYS_ERR_NITMAS20 = vbObjectError + 150

    'Error Codes 200-299 reserved for Oasys Transmission Export and Import Errors
    Public Const OASYS_ERR_EDI_DETAIL_HEADER_MISSING = vbObjectError + 200
    Public Const OASYS_ERR_EDI_HEADER_MISSING As Long = 201
    Public Const OASYS_ERR_EDI_TRAILER_RECORDCOUNT_ERROR As Long = 202
    Public Const OASYS_ERR_EDI_INVALID_LINETYPE As Long = 203

    Public Const OASYS_ERR_GETFIELD_INVALID_FID_MSG As String = "Invalid Field ID passed into GetField - ID="
#End Region

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        'get form type from parameters and set control widths
        Select Case RunParameters.Trim.ToLower
            Case "open"
                _FormType = FormTypes.Open
                MODULE_FUNCTION = "Daily Open"

            Case "close"
                _FormType = FormTypes.Close
                Panel1.Left = CInt((Me.Width / 2) - (Panel1.Width / 2))
                Panel1.Top = CInt((Me.Height / 2) - (Panel1.Height / 2))
                Panel1.Visible = True
                MODULE_FUNCTION = "Daily Close"
        End Select

    End Sub

    Private Sub InitializeScreen()

        'get master workstation id and check if this workstation is the master
        Using sysOptions As New BOSystem.cSystemOptions(_Oasys3DB)
            sysOptions.Load()
            _masterId = CLng(sysOptions.MasterOutletNo.Value)
            _isMaster = (WorkstationId = _masterId)
        End Using

        'show printer dialog form if first time close called
        If _FormType = FormTypes.Close And _isFirstTime Then
            If _isMaster = True Then
                Dim Printer As frmPrinter = New frmPrinter
                Printer.ShowDialog()
                Panel1.Visible = False
                _isFirstTime = False
            End If
        End If

        'load system dates
        ReadSystemDates(True)
        ssTask_SheetView1.RowCount = 0

        'get list of night routine tasks to process
        Using BOTask As New BONightSchedule.cNightSchedule(_Oasys3DB)
            If _FormType = FormTypes.Close Then
                _dt = BOTask.ListTasks(CChar(_setNumber))
            Else
                If _lastTask <> "000" Then
                    _dt = BOTask.ListRetryTasks(_setNumber, _lastTask)
                Else
                    _SysDates.UpdateLstNiteClsDate(_nextOpenDate)
                End If
            End If
        End Using

        'load tasks into seet and set status of each
        If _isMaster = True Then
            If _dt IsNot Nothing Then
                lblMode.Visible = True
                lblCurrentTask.Visible = True
                txtCurrentTask.Visible = True
                txtCurrentTaskDesc.Visible = True
                lblProgress.Visible = True
                ssTask.Visible = True
                PBarProgress.Visible = True
                lblMode.Visible = True

                Dim rowcount As Integer = 0
                For Each row As DataRow In _dt.Rows

                    If TaskOkToRun(row) = True Then
                        ssTask_SheetView1.AddRows(rowcount, 1)
                        ssTask_SheetView1.SetText(rowcount, 0, row.Item("TASK").ToString)
                        ssTask_SheetView1.SetText(rowcount, 1, row.Item("DESCR").ToString)

                        If _lastTask = "000" Then
                            If _isCloseCompleted = False Then
                                ssTask_SheetView1.SetText(rowcount, 2, "To be run")
                            Else
                                ssTask_SheetView1.SetText(rowcount, 2, "Done")
                                ssTask.EditMode = False
                                ssTask.Refresh()
                                Application.DoEvents()
                            End If
                        Else
                            If row.Item("Task").ToString < _lastTask Then
                                ssTask_SheetView1.SetText(rowcount, 2, "Done")
                                ssTask.Refresh()
                                Application.DoEvents()
                            Else
                                ' find failed task 
                                Dim LastTaskNo As Integer
                                Dim FailedTask As String
                                LastTaskNo = CInt(_lastTask)
                                'LastTaskNo = LastTaskNo + 1
                                FailedTask = Format(LastTaskNo, "000")
                                If row.Item("Task").ToString = FailedTask Then
                                    ssTask_SheetView1.SetText(rowcount, 2, "Failed")
                                    ssTask.Refresh()
                                    Application.DoEvents()

                                Else
                                    ssTask_SheetView1.SetText(rowcount, 2, "To be run")
                                    ssTask.Refresh()
                                    Application.DoEvents()
                                End If
                            End If
                        End If

                        rowcount = rowcount + 1
                    End If
                Next row
            End If
        End If

    End Sub

    Public Function RunTask(dataRow As DataRow, taskCompleted As Boolean) As Boolean

        Dim startDate As Date = Date.Now

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Before running TASK an ascii file is written out. The last thing
        ' the TASK program should do, if it completes without error, is
        ' erase this file. If the file is still there when the task ends
        ' then this program will know that the TASK did not complete
        ' successfully.
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then
            File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
        End If

        Trace.WriteLine("Shell Out and Run Executable", Me.Name)
        taskCompleted = True

        Select Case CInt(dataRow.Item("TType"))

            Case 0, 4
                ' Run program through shell 
                If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = False Then
                    CreateFile(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
                End If

                Dim strParam As String = ""
                If Trim(CStr(dataRow.Item("PROG"))).Contains("|") Then strParam = Trim(CStr(dataRow.Item("PROG"))).Substring(Trim(CStr(dataRow.Item("PROG"))).IndexOf("|") + 1)
                Dim strProgram As String = My.Computer.FileSystem.CurrentDirectory & "\" & Trim(CStr(dataRow.Item("PROG")))
                If strProgram.Contains("|") Then strProgram = strProgram.Substring(0, strProgram.IndexOf("|"))

                If strProgram.ToLower.Contains(".exe y") Then
                    strParam = "y"
                    strProgram = strProgram.Remove(strProgram.Length - 1, 1)
                End If

                strParam += " (/U=" & UserId.ToString & ",/P=',CFC')"
                Trace.WriteLine("CLOSE CALLING - " & strProgram & " " & strParam)
                RunShell(strProgram, strParam)
                Trace.WriteLine("FINISHED CLOSE CALLING - " & strProgram & " " & strParam)
                If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then taskCompleted = False

            Case 6
                Dim names() As String = dataRow.Item("PROG").ToString.Split(","c)
                Dim assemblyName As String = names(0).Trim
                Dim className As String = names(1).Trim
                Dim appName As String = dataRow.Item("DESCR").ToString.Trim
                Dim parameters As String = dataRow.Item("JRUN").ToString.Trim & " (/P=',CFC')"
                ' Specify date
                parameters = parameters & " --date=" & startDate.ToShortDateString()

                RunDll(assemblyName, className, appName, parameters)
                taskCompleted = True

            Case 2
                RunHouseKeeping(CInt(Trim(CStr(dataRow.Item("PROG")))))
                taskCompleted = True
            Case 3 'To Do: Stream System
            Case 1
                Dim params As String = " (/P=',CFC')"
                If Not IsDBNull(dataRow.Item("JRUN")) Then params = Trim(CStr(dataRow.Item("JRUN"))) & " (/P=',CFC')"
                taskCompleted = RunDllOld(Trim(CStr(dataRow.Item("PROG"))), params)
        End Select
        Return taskCompleted
    End Function

    Private Sub GetCountPricesToPrintLabels(ByVal dDate As Date)
        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetCountPricesToPrintLabels"
                    .AddParameter("@Date", dDate, SqlDbType.Date)
                    .AddOutputParameter("@CountPRCCHNG3Days", SqlDbType.Int)
                    .AddOutputParameter("@CountPRCCHNGAlreadyHappened", SqlDbType.Int)
                    .ExecuteValue()
                    _intCountPrcchg3DaysLeft = CInt(.GetParameterValue("@CountPRCCHNG3Days"))
                    _intCountPrcchgAlreadyHeppened = CInt(.GetParameterValue("@CountPRCCHNGAlreadyHappened"))
                End With
            End Using
        End Using
    End Sub

    Protected Overrides Sub DoProcessing()

        Dim BOTask As New BONightSchedule.cNightSchedule(_Oasys3DB)
        Dim BOTaskLog As New BONightSchedule.cNightSchedLog(_Oasys3DB)
        Dim BOSysDate As New BOSystem.cSystemDates(_Oasys3DB)
        Dim BOStoreStatus As New BONightSchedule.cStoreStatus(_Oasys3DB)
        Dim drSysDate As DataRow
        Dim BORetailOptions As New BOSystem.cRetailOptions(_Oasys3DB)
        Dim recCount As Integer = 0
        Dim dt As DataTable
        Dim ProcessingError As Boolean
        Dim TaskCompleted As Boolean = False
        Dim intRun As Integer = 0
        Dim messagePrcchg3DaysLeft As String
        Dim messagePrcchgAlreadyHeppened As String
        Dim picCodeInstance As IPicCodeCalculation = (New PicCountCalculationFactory).GetImplementation

        Try
            Me.Cursor = Cursors.WaitCursor

            InitializeScreen()

            If _endProgram = True Then
                Me.Cursor = Cursors.Default
                Me.ParentForm.Close()
                Return
            End If

            GetCountPricesToPrintLabels(_SysDates.Tomorrow.Value)

            If _intCountPrcchg3DaysLeft <> 0 Or _intCountPrcchgAlreadyHeppened <> 0 Then
                messagePrcchg3DaysLeft = "Labels for " & _intCountPrcchg3DaysLeft.ToString() & " outstanding price changes not yet printed."
                messagePrcchgAlreadyHeppened = "Labels for " & _intCountPrcchgAlreadyHeppened.ToString() & " overdue price changes not yet printed."
                MessageBox.Show("WARNING" & vbCrLf & vbCrLf & messagePrcchg3DaysLeft & vbCrLf & vbCrLf & messagePrcchgAlreadyHeppened, MODULE_FUNCTION, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If

            _drRetailOptions = BORetailOptions.RetailOptions

            If _FormType = FormTypes.Open Then
                ReadSystemDates(True)
                If Not _isMaster Then
                    Call OpenSlave()
                    Me.ParentForm.Close()
                    Return
                Else
                    OpenMaster()

                    ' Has the system fallen over if so put in retry mode 
                    If _FormType = FormTypes.Open Then
                        drSysDate = BOSysDate.SystemDates
                        If CStr(drSysDate.Item(BOSysDate.NightSetNo.ColumnName)) <> "000" Then
                            If CBool(drSysDate.Item(BOSysDate.RetryMode.ColumnName)) = False Then
                                BOSysDate.SetOnRetry()
                                _inRetryMode = True
                            Else
                                MessageBox.Show(String.Format(My.Resources.Strings.OpenRetry, drSysDate.Item(BOSysDate.NightSetNo.ColumnName), AppName))
                                FindForm.Close()
                                _endProgram = True
                                Exit Sub
                            End If
                        Else

                            If Not _isFirstRun Then
                                MessageBox.Show(My.Resources.Strings.NightAlreadyRunOk, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                FindForm.Close()
                                _endProgram = True
                                Exit Sub
                            End If

                        End If
                    End If

                    If _endProgram = True Then
                        Me.Cursor = Cursors.Default
                        Me.ParentForm.Close()
                        FindForm.Close()
                        Return
                    End If

                    If _isFirstRun Then
                        ClearFlashTotals()
                    End If

                    If Not _inRetryMode And _isFirstRun Then
                        _drStoreStatus = BOStoreStatus.StoreStatus

                        Me.Cursor = Cursors.Default
                        MessageBox.Show(String.Format(My.Resources.Strings.OpenNow, CDate(_drStoreStatus.Item("DATE1")).ToString("dddd dd/MM/yyyy")), AppName)
                        Me.ParentForm.Close()
                        Return
                    End If
                End If

                InitializeComponent()
                InitializeScreen()

            Else 'If it is daily close, check if there are non authorised adjustments
                picCodeInstance.StockAdjustmentFactoryInstance = AdjustmentFactory.FactoryGet
                Dim adjDate As Date = picCodeInstance.StockAdjustmentFactoryInstance.ReadAdjustmentDate

                picCodeInstance.HhtHeaderFactoryInstance = HandHeldTerminalHeaderFactory.FactoryGet
                picCodeInstance.HhtHeaderFactoryInstance.ReadHHT(adjDate)

                Dim isAdjustmentsAuthorizingNeeded = picCodeInstance.HhtHeaderFactoryInstance.CheckAdjustmentsAuthorizingNeeded().Item1

                If isAdjustmentsAuthorizingNeeded Then
                    Dim result As DialogResult = MessageBox.Show("Adjustments have not been authorised, do you wish to continue with the close - Y/N", Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                    If result = DialogResult.No Then
                        Me.Cursor = Cursors.Default
                        Me.ParentForm.Close()
                        Return
                    End If
                End If
            End If

            If _inRetryMode Then
                lblMode.ForeColor = Color.Red
                lblMode.Text = "Retrying Nightly Routine"
            Else

                lblMode.Text = "Nightly Routine"
            End If

            Do
                _testAgain = False
                If _isMaster Then
                    If _inRetryMode Then
                        If _isLive = True Then
                            Me.Cursor = Cursors.Default
                            If MsgBox("Store Close Procedure - Retry Failed - Continue with close of this workstation?" & vbCrLf & _
                                "Please ensure that there is enough paper in the printer for nightly update reports.", _
                                vbYesNo Or vbQuestion, MODULE_FUNCTION) = vbNo Then
                                Me.ParentForm.Close()
                                Return
                            End If
                        Else
                            Me.Cursor = Cursors.Default
                            If MsgBox("Training Close - Retry Failed - Continue with close of this workstation?" & vbCrLf & _
                                "Please ensure that there is enough paper in the printer for nightly update reports.", _
                                vbYesNo Or vbQuestion, MODULE_FUNCTION) = vbNo Then
                                Me.ParentForm.Close()
                                Return
                            End If
                        End If
                    Else
                        If _isLive = True Then
                            Me.Cursor = Cursors.Default
                            If MsgBox("All workstations must be closed. Tills should be Zread before close." & vbCrLf & _
                                "" & vbCrLf & _
                                "Store Close Procedure - Continue with close of this workstation?" & vbCrLf & _
                                "Please ensure that there is enough paper in the printer for nightly update reports.", _
                                vbYesNo Or vbQuestion, MODULE_FUNCTION) = vbNo Then
                                Me.ParentForm.Close()
                                Return
                            End If
                        Else
                            Me.Cursor = Cursors.Default
                            If MsgBox("Training Close - Continue with close of this workstation?" & vbCrLf & _
                                "Please ensure that there is enough paper in the printer for nightly update reports.", _
                                vbYesNo Or vbQuestion, MODULE_FUNCTION) = vbNo Then
                                Me.ParentForm.Close()
                                Return
                            End If
                        End If
                    End If

                Else
                    Me.Cursor = Cursors.Default
                End If

                If _isMaster Then
                    If Not _inRetryMode Then
                        If CheckForOpenPC() = True Then
                            If _isFromNightlyClose = False Then
                                Dim frmWorkstation As New WorkStation
                                frmWorkstation.MasterId = _masterId
                                frmWorkstation.WorkstationId = WorkstationId
                                frmWorkstation.ShowDialog()
                                _testAgain = frmWorkstation.Retry
                                frmWorkstation.Close()
                            End If

                        End If
                        'update sysdat.nite to anything but "000" to stop menu closing down
                        BOSysDate.UpdateNightTaskNo(1)

                        If _testAgain = False Then LogOff()
                    End If

                Else
                    Me.Cursor = Cursors.Default
                    Call LogOff()
                    Me.ParentForm.Close()
                    Return
                End If

                Application.DoEvents()
            Loop Until (_testAgain = False)

            _isCloseCompleted = True

            If _FormType = FormTypes.Close Then
                dt = BOTask.ListTasks(CChar(_setNumber))
                LogOff()
            Else
                dt = BOTask.ListRetryTasks(_setNumber, _lastTask)
            End If

            If dt.Rows.Count > 0 Then
                _intNumTasks = 0

                For Each datarow As DataRow In dt.Rows
                    If TaskOkToRun(datarow) = True And CStr(datarow.Item("TASK")) >= _lastTask Then
                        _intNumTasks += 1
                    End If
                    Application.DoEvents()
                Next

                If _intNumTasks > 0 Then
                    PBarProgress.Minimum = 0
                    PBarProgress.Maximum = _intNumTasks
                    PBarProgress.Value = 0
                    intRun = 0

                    Trace.WriteLine("There will be " & _intNumTasks & " in the List of Tasks to be Processed.", Me.Name)

                    For Each datarow As DataRow In dt.Rows

                        If TaskOkToRun(datarow) = True And CStr(datarow.Item("TASK")) > _lastTask Then

                            PBarProgress.Value = PBarProgress.Value + 1

                            ' Update SYSDAT with task we're about to run
                            txtCurrentTask.Text = CStr(datarow.Item("TASK"))
                            txtCurrentTaskDesc.Text = CStr(datarow.Item("DESCR"))

                            ' Output NITLOG
                            BOTaskLog.WriteLog(_setNumber, datarow.Item("TASK").ToString.PadLeft(3, "0"c), CStr(datarow.Item("DESCR")), CStr(datarow.Item("PROG")), Format(Now, "yyyy-MM-dd"), Format(Now, "HHmmss"))
                            TaskCompleted = RunTask(datarow, TaskCompleted)

                            ReadSystemDates(False)

                            Trace.WriteLine("Check if " & My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP Exists to Determine if" & " Task Failed.", Me.Name)

                            If (TaskCompleted = False) Then
                                Trace.WriteLine(String.Format("{0} Task must have failed.", datarow.Item("TType")), Me.Name)

                                ' Update NITLOG for aborted task
                                ProcessingError = BOTaskLog.UpdateAbort(_setNumber, datarow.Item("Task").ToString.PadLeft(3, "0"c), Format(Now, "yyyy-MM-dd"), Format(Now, "HHmmss"))
                                If ProcessingError = True Then
                                    Throw New ApplicationException(OASYS_ERR_UPDATE_FAILED & Me.Name & "Error Updating Night Log Record.")
                                End If

                                If CBool(datarow.Item("ABOR")) = True Then
                                    If _isFromNightlyClose = False Then
                                        InitializeScreen()
                                        MsgBox(String.Format("ERROR   ERROR   ERROR{0}{0}The Nightly Routine was ABORTED.{0}Task {1} - {2}{0}{0}ERROR   ERROR   ERROR", vbCrLf, datarow.Item("Task"), datarow.Item("DESCR")), vbCritical, _
                                            MODULE_FUNCTION)
                                    End If
                                    _isCloseCompleted = False
                                    Exit For
                                End If

                                ssTask_SheetView1.SetText(intRun, 2, "Failed")
                                Me.Refresh()
                                Application.DoEvents()

                            Else
                                'Update the SYSDAT to indicate last task completed REGARDLESS OF Wether it failed ...
                                ProcessingError = BOSysDate.UpdateNightTaskNo(CInt(datarow.Item("Task")))
                                If ProcessingError = True Then
                                    Throw New ApplicationException(OASYS_ERR_UPDATE_FAILED & Me.Name & "Error Updating System Dates Record.")
                                End If

                                Trace.WriteLine(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP Does NOT Exists, Task Successful.", Me.Name)

                                ' Update NITLOG for ended task
                                ProcessingError = BOTaskLog.UpdateEnd(_setNumber, datarow.Item("Task").ToString.PadLeft(3, "0"c), Format(Now, "yyyy-MM-dd"), Format(Now, "HHmmss"))

                                If ProcessingError = True Then
                                    Throw New ApplicationException(OASYS_ERR_UPDATE_FAILED & Me.Name & "Error Updating Night Log Record.")
                                End If

                                ssTask_SheetView1.SetText(intRun, 2, "Done")
                                Me.Refresh()
                                Application.DoEvents()

                            End If

                            intRun += 1
                            If intRun < _intNumTasks Then
                                Dim frmAbortClose As New frmAbortClose(_Oasys3DB)
                                frmAbortClose.ShowDialog()
                                If frmAbortClose.Cancel = True Then
                                    FindForm.Close()
                                    Exit Sub
                                Else
                                    frmAbortClose = Nothing
                                End If

                            End If
                            Trace.WriteLine(String.Format("Daily Close Task {0} Ran.", datarow.Item("TASK")))
                        Else
                            Trace.WriteLine(String.Format("Daily Close Task {0} Skipped.", datarow.Item("TASK")))
                        End If

                        Application.DoEvents()
                        Me.Refresh()
                    Next datarow
                End If
            End If

            'check if close completed
            If _isCloseCompleted Then
                If Not BOSysDate.UpdateNightTaskOK() Then
                    Throw New ApplicationException(OASYS_ERR_UPDATE_FAILED & Me.Name & "Error Updating System Dates Record.")
                End If

                Me.Refresh()
                Application.DoEvents()

                Dim hostForm As Cts.Oasys.WinForm.HostForm = CType(Me.ParentForm, Cts.Oasys.WinForm.HostForm)
                hostForm.LogOutUserAfterClose = True

                If Not _inRetryMode Then
                    Call MsgBox("Close Complete.", vbInformation, MODULE_FUNCTION)
                    FindForm.Close()
                Else
                    Call MsgBox("Night Retry Procedure Completed OK.", vbInformation, MODULE_FUNCTION)
                    FindForm.Close()
                End If
            Else
                FindForm.Close()
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub CreateFile(ByVal strFullFilePath As String)

        Dim oWrite As System.IO.StreamWriter
        oWrite = File.AppendText(strFullFilePath)
        oWrite.WriteLine("1") 'write text to file
        oWrite.Close()

    End Sub

    Private Function CheckForOpenPC() As Boolean

        Dim boWKStn As BOSecurityProfile.cWorkStationConfig = New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
        Dim lstWKStn As List(Of BOSecurityProfile.cWorkStationConfig)

        CheckForOpenPC = False
        boWKStn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boWKStn.IsActive, 1)
        boWKStn.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
        boWKStn.AddLoadFilter(clsOasys3DB.eOperator.pNotEquals, boWKStn.ID, WorkstationId)
        lstWKStn = boWKStn.LoadMatches()

        If lstWKStn.Count > 0 Then
            CheckForOpenPC = True
        End If

    End Function

    Private Sub LogOff()

        If _isMaster = True Then
            Using BOStoreStatus As New BONightSchedule.cStoreStatus(_Oasys3DB)
                BOStoreStatus.LogOffAll()
            End Using

            _Oasys3DB.ExecuteSql("UPDATE SYSNID SET MAST=0 WHERE FKEY = 01")
        End If

        Using workstation As New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
            workstation.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, workstation.ID, WorkstationId)
            workstation.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            workstation.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, workstation.IsActive, True)

            If workstation.LoadMatches.Count > 0 Then
                workstation.DeActivateWKStation(WorkstationId.ToString.PadLeft(2, "0"c))
            End If
        End Using

    End Sub

    Private Function TaskOkToRun(ByRef NightTask As DataRow) As Boolean

        If RunOnDayWeek(NightTask) Then
            Select Case True
                Case IsDBNull(NightTask.Item("BEGD")) AndAlso IsDBNull(NightTask.Item("ENDD"))
                    Return CheckWeekPeriodFlag(NightTask)

                Case Not IsDBNull(NightTask.Item("BEGD")) AndAlso IsDBNull(NightTask.Item("ENDD"))
                    If _SysDates.Today.Value >= CDate(NightTask.Item("BEGD")) Then
                        Return CheckWeekPeriodFlag(NightTask)
                    End If

                Case IsDBNull(NightTask.Item("BEGD")) AndAlso Not IsDBNull(NightTask.Item("ENDD"))
                    If _SysDates.Today.Value < CDate(NightTask.Item("ENDD")) Then
                        Return CheckWeekPeriodFlag(NightTask)
                    End If

                Case Else
                    If _SysDates.Today.Value >= CDate(NightTask.Item("BEGD")) AndAlso _SysDates.Today.Value < CDate(NightTask.Item("ENDD")) Then
                        Return CheckWeekPeriodFlag(NightTask)
                    End If
            End Select
        End If
        Return False

    End Function

    Private Function CheckWeekPeriodFlag(ByRef NightTask As DataRow) As Boolean

        If CBool(NightTask.Item("WEEK")) = True Then
            'Need to check against WEND date
            If _isWeekEnd = False Then
                Return False
            End If
        End If

        If CBool(NightTask.Item("PEND")) = True Then
            'Is this a period end date
            If _isPeriodEnd = False Then
                Return False
            End If
        End If

        If CBool(NightTask.Item("LIVE")) = False Then
            If _SysDates.StoreLiveDate.Value > _SysDates.Today.Value Then
                Return False
            End If
        End If

        Return True
    End Function

    Public Function RunOnDayWeek(ByVal row As DataRow) As Boolean

        Select Case _dayCode
            Case 1 : Return CBool((row.Item("DAYS1")))
            Case 2 : Return CBool((row.Item("DAYS2")))
            Case 3 : Return CBool((row.Item("DAYS3")))
            Case 4 : Return CBool((row.Item("DAYS4")))
            Case 5 : Return CBool((row.Item("DAYS5")))
            Case 6 : Return CBool((row.Item("DAYS6")))
            Case 7 : Return CBool((row.Item("DAYS7")))
            Case Else : Return False
        End Select

    End Function

    Private Sub RunShell(ByVal cmdline As String, ByVal cdArguements As String)

        Dim procID As Integer
        Dim newProc As Diagnostics.Process

        If GlobalVars.IsTestEnvironment Then
            newProc = New Diagnostics.Process
            newProc.StartInfo.FileName = cmdline
            newProc.StartInfo.Arguments = cdArguements
            TestEnvironmentSetup.SetupSpawnedProcess(newProc.StartInfo)
            newProc.Start()
        Else
            newProc = Diagnostics.Process.Start(cmdline, cdArguements)
        End If

        procID = newProc.Id
        _endProcess = False
        newProc.EnableRaisingEvents = True
        AddHandler newProc.Exited, AddressOf Me.ProcessExited

        While _endProcess = False
            Application.DoEvents()
        End While

        Me.Refresh()
        Application.DoEvents()

    End Sub

    Friend Sub ProcessExited(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim myProcess As Process = DirectCast(sender, Process)
        _endProcess = True
        myProcess.Close()

    End Sub

    Private Sub OpenSlave()

        Dim BOStoreStatus As New BONightSchedule.cStoreStatus(_Oasys3DB)
        Dim BOWorkstationConfig As New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
        Dim Result As DialogResult = DialogResult.Ignore
        Dim actlog As BOSecurityProfile.cActivityLog = New BOSecurityProfile.cActivityLog(_Oasys3DB)
        Dim boparm As BOSystem.cParameter = New BOSystem.cParameter(_Oasys3DB)
        Dim boWKStn As BOSecurityProfile.cWorkStationConfig = New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
        Dim lstWKStn As List(Of BOSecurityProfile.cWorkStationConfig)
        Dim MethodOK As Boolean = False
        Dim DayNo As Integer

        _drStoreStatus = BOStoreStatus.StoreStatus

        If (CDate(_drStoreStatus.Item("DATE1")) <> Today) Or (CBool(_drStoreStatus.Item("MAST")) <> True) Then
            MsgBox("System not Opened - Master workstation must be opened before other workstations can be used.", MsgBoxStyle.Exclamation, MODULE_FUNCTION)
            Exit Sub
        End If

        boWKStn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boWKStn.ID, CInt(WorkstationId.ToString.PadLeft(2, "0"c)))
        boWKStn.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        boWKStn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boWKStn.IsActive, 1)
        lstWKStn = boWKStn.LoadMatches()

        If lstWKStn.Count > 0 Then
            MessageBox.Show("Workstation already Open", "OpenClose", MessageBoxButtons.OK)
            _endProgram = True
            Exit Sub
        End If

        DayNo = CDate(_drStoreStatus.Item("DATE1")).DayOfWeek
        If DayNo = 0 Then
            DayNo = 7
        End If
        Result = MessageBox.Show("System Last Opened on " & WeekdayName(DayNo) & Space(3) & Format(CDate(_drStoreStatus.Item("DATE1")), "dd/MM/yy") & vbCrLf & "Do you wish to Continue with the Open of this workstation?", "Open", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
        If Result = DialogResult.No Then
            Me.FindForm.Close()
            Return
            Exit Sub
        End If

        Dim WarehousePC As String = boparm.GetParameterString(976)
        If WorkstationId = CDbl(WarehousePC) Then

            Result = MessageBox.Show("Has the Forklift Daily Checklist been completed?", "Open", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.No Then
                FindForm.Close()
                Return
                Exit Sub
            Else
                Using login As New Cts.Oasys.WinForm.User.UserAuthorise()
                    If login.ShowDialog(Me) = DialogResult.OK Then
                        MethodOK = boWKStn.ActivateWKStation(Me.WorkstationId.ToString.PadLeft(2, "0"c))
                        If MethodOK = True Then
                            DisplayStatus(WorkstationId.ToString("000") & " workstation now opened for " & WeekdayName(Today.DayOfWeek) & Space(3) & Format(CDate(_drStoreStatus.Item("DATE1")), "dd/MM/yy") & " System file updated")
                        End If
                        actlog.RecordAccessedOption(CStr(UserId), CStr(WorkstationId), -1)
                    Else
                        FindForm.Close()
                        Exit Sub

                    End If
                End Using
            End If
        Else

            MethodOK = boWKStn.ActivateWKStation(Me.WorkstationId.ToString.PadLeft(2, "0"c))
            If MethodOK = True Then
                'ref 300
                MessageBox.Show(WorkstationId.ToString("000") & " workstation now opened for " & WeekdayName(Today.DayOfWeek + 1, False, FirstDayOfWeek.Sunday) & Space(3) & Format(CDate(_drStoreStatus.Item("DATE1")), "dd/MM/yy") & " System file updated", "OpenClose", MessageBoxButtons.OK)
            End If
        End If

    End Sub

    Private Sub OpenMaster()

        Dim BOStoreStatus As New BONightSchedule.cStoreStatus(_Oasys3DB)
        _drStoreStatus = BOStoreStatus.StoreStatus

        If CDate(_drStoreStatus.Item("DATE1")) = Today Then
            _isFirstRun = False
        End If

        Dim sb As New StringBuilder
        sb.Append("System Last opened on ")
        sb.Append(CDate(_drStoreStatus.Item("DATE1")).DayOfWeek.ToString & Space(3))
        sb.Append(Format(CDate(_drStoreStatus.Item("DATE1")), "dd/MM/yy"))

        If _isFirstRun = False Then
            sb.Append("<--- Same as Today. Cannot re-open again today.")
            MessageBox.Show(sb.ToString, AppName)
            Exit Sub
        Else
            sb.Append(Environment.NewLine)
            sb.Append("Continue with Open of the System?")
            Dim Result As DialogResult = MessageBox.Show(sb.ToString, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Result = DialogResult.No Then
                _endProgram = True
                Exit Sub
            End If
        End If

        BOStoreStatus.LogOnMaster()
        Using workstation As New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
            workstation.ActivateWKStation(WorkstationId.ToString.PadLeft(2, "0"c))
        End Using

        'process HOSTG files
        RunShell(My.Computer.FileSystem.CurrentDirectory & "\ProcessTransmissions.exe", "HOSTG")

    End Sub

    Private Sub ReadSystemDates(ByVal InitialRead As Boolean)

        _SysDates = New BOSystem.cSystemDates(_Oasys3DB)
        _SysDates.Load()

        If InitialRead = True Then
            _inRetryMode = _SysDates.RetryMode.Value ' drSystemDates.Item("RETY")
            _setNumber = _SysDates.NetSetNo.Value ' drSystemDates.Item("NSET")
            _lastTask = _SysDates.NightSetNo.Value ' drSystemDates.Item("NITE")
            _nextOpenDate = _SysDates.Tomorrow.Value ' drSystemDates.Item("TMDT")

            If Date.Now >= _SysDates.StoreLiveDate.Value Then ' CDate(drSystemDates.Item("LIVE")) Then
                _isLive = True
            Else
                _isLive = False
            End If

            If _inRetryMode OrElse _lastTask <> "000" Then
                _dayCode = _SysDates.TodayDayCode.Value
            Else
                _dayCode = _SysDates.TomorrowsDayCode.Value
            End If

            If (_dayCode = _SysDates.EndofWeekDay.Value) And (_SysDates.WeekEndandNotProcessed.Value = False) Then
                _isWeekEnd = True
            End If 'end of week so include Weekend Tasks and Roll SOQ Weeks
        End If
    End Sub

    Private Sub RunHouseKeeping(ByVal id As Integer)

        Using boGroup As New BOHouseKeep.cHouseKeepGroup(_Oasys3DB)
            boGroup.HouseKeeping(id)
        End Using

    End Sub

    Private Sub RunDll(ByVal assemblyName As String, ByVal className As String, ByVal appName As String, ByVal parameters As String)

        Dim ass As Assembly = Assembly.LoadFrom(assemblyName)
        Select Case ass.GetType(className).BaseType.ToString
            Case "Cts.Oasys.WinForm.Form"
                Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, UserId, WorkstationId, SecurityLevel, parameters, "", False, 1)
                hostForm.ShowDialog()

            Case "OasysCTS.ModuleTabBase"
                MessageBox.Show("Deprecated base, contact CTS")
        End Select

    End Sub

    Private Function RunDllOld(ByVal DllName As String, ByVal cdArguements As String) As Boolean

        Dim AppName As String = String.Empty

        Try
            AppName = New System.Text.RegularExpressions.Regex("([A-Z])").Replace(DllName.Substring(DllName.LastIndexOf("."c) + 1), " ${1}", -1, 1)

            Dim parameterDocument As StandardClasses.ParameterDocument

            parameterDocument = StandardClasses.StandardApp.StandardParametersDocument(CShort(UserId), CShort(WorkstationId), SecurityLevel)
            parameterDocument.AddItem("AppName", AppName)
            parameterDocument.AddItem("MenuID", DllName)
            parameterDocument.AddItem("ConnectionString", _Oasys3DB.ConnectionString)
            parameterDocument.AddItem("RunParameters", cdArguements)

            Dim oasysApp As New OasysCTS.App()
            oasysApp.Show(parameterDocument)

            Return True

        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
            Return False
        End Try

    End Function

    Private Sub ClearFlashTotals()

        _Oasys3DB.ExecuteSql("delete from RSTILL")
        _Oasys3DB.ExecuteSql("delete from RSFLAS")
        _Oasys3DB.ExecuteSql("delete from RSCash")

    End Sub

End Class
