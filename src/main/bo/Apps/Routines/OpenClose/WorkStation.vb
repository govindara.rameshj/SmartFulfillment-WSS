﻿Imports FarPoint.Win.Spread.CellType.GeneralCellType
Imports Cts.Oasys.WinForm.User

Public Class WorkStation

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _records As Boolean = False
    Private _Cancel As Boolean = True
    Private _MasterId As Long = 0
    Private _WorkstationId As Long = 0

    Public Property Retry() As Boolean

        Get
            Retry = _Cancel
        End Get

        Set(ByVal value As Boolean)
            _Cancel = value
        End Set

    End Property

    Public Property MasterId() As Long
        Get
            MasterId = _MasterId
        End Get
        Set(ByVal value As Long)
            _MasterId = value
        End Set
    End Property

    Public Property WorkstationId() As Long
        Get
            WorkstationId = _WorkstationId
        End Get
        Set(ByVal value As Long)
            _WorkstationId = value
        End Set
    End Property

    Private Sub WorkStation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'key mappings


        Dim AncestorFocusedMap As InputMap = FpWorkstation.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap As InputMap = FpWorkstation.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)

        Populate()

    End Sub

    Private Sub FpWorkstation_CellDoubleClick(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles FpWorkstation.CellDoubleClick

        Dim selectedcol As Integer
        Dim selectedrow As Integer

        selectedcol = FpWorkstation.ActiveSheet.ActiveColumn.Index
        selectedrow = FpWorkstation.ActiveSheet.ActiveRow.Index

        TryToDeactivateWorkstation(selectedcol, selectedrow)
    End Sub

    Private Sub fpWorkstation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FpWorkstation.KeyPress

        Dim selectedcol As Integer
        Dim selectedrow As Integer

        selectedcol = CInt(FpWorkstation.ActiveSheet.ActiveColumn.Index.ToString)
        selectedrow = CInt(FpWorkstation.ActiveSheet.ActiveRow.Index.ToString)

        e.Handled = False
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = False
            TryToDeactivateWorkstation(selectedcol, selectedrow)
        End If

    End Sub 'spdHeaderSheet_KeyPress

    Private Sub TryToDeactivateWorkstation(ByVal selectedcol As Integer, ByVal selectedrow As Integer)

        Dim frmpassword As New ServiceDeskAuthorise
        Dim intNoRows As Integer = 0

        If selectedcol = 0 Then
            Dim boWKStn As BOSecurityProfile.cWorkStationConfig = New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
            frmpassword.WorkstationID = CInt(FpWorkstation_Sheet1.GetText(selectedrow, 0))
            frmpassword.WKStnDescription = FpWorkstation_Sheet1.GetText(selectedrow, 0) & Space(3) & FpWorkstation_Sheet1.GetText(selectedrow, 1)
            frmpassword.ShowDialog()
            If frmpassword.IsAuthorized Then
                boWKStn.DeActivateWKStation(frmpassword.WorkstationID.ToString.PadLeft(2, "0"c))
            End If
            _records = False
            intNoRows = Populate()
            If intNoRows = 0 Then
                Retry = False
                Me.Close()
            End If
        End If
    End Sub

    Private Function Populate() As Integer

        Dim boWKStn As BOSecurityProfile.cWorkStationConfig = New BOSecurityProfile.cWorkStationConfig(_Oasys3DB)
        Dim lstWKStn As List(Of BOSecurityProfile.cWorkStationConfig)
        Dim row As Integer

        FpWorkstation_Sheet1.RowCount = 0
        boWKStn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boWKStn.IsActive, 1)
        boWKStn.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
        boWKStn.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pNotEquals, boWKStn.ID, CInt(WorkstationId))
        lstWKStn = boWKStn.LoadMatches()
        If lstWKStn.Count > 0 Then
            _records = True
            For Each item As BOSecurityProfile.cWorkStationConfig In lstWKStn
                If item.ID.Value <> _MasterId And item.ID.Value <> _WorkstationId Then
                    FpWorkstation_Sheet1.AddRows(row, 1)
                    FpWorkstation_Sheet1.SetText(row, 0, item.ID.Value.ToString)
                    FpWorkstation_Sheet1.SetText(row, 1, item.Description.Value)
                    row += 1
                End If
            Next
            FpWorkstation.Focus()
            FpWorkstation_Sheet1.AddSelection(0, 0, 1, 2)
        Else
            btnExit.PerformClick()
        End If

        FpWorkstation.Sheets(0).Columns(0).CanFocus = True
        FpWorkstation.Sheets(0).Columns(1).CanFocus = True

        Return lstWKStn.Count

    End Function

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = False
        Select Case e.KeyData
            Case Keys.F5
                btnRefresh.PerformClick()
                e.Handled = True

            Case Keys.F10
                btnExit.PerformClick()
                e.Handled = True
        End Select

    End Sub 'Form_KeyDown

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Dim intRows As Integer = Populate()
        If intRows = 0 Then
            Retry = False
            Me.Close()
        End If
    End Sub


    Private Sub FpWorkstation_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles FpWorkstation.CellClick

    End Sub
End Class