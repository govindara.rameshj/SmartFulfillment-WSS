﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrinter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmbPrinters = New System.Windows.Forms.ComboBox
        Me.lblPrinterInfo = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(197, 112)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(94, 30)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&Ok"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmbPrinters
        '
        Me.cmbPrinters.FormattingEnabled = True
        Me.cmbPrinters.Location = New System.Drawing.Point(34, 48)
        Me.cmbPrinters.Name = "cmbPrinters"
        Me.cmbPrinters.Size = New System.Drawing.Size(424, 21)
        Me.cmbPrinters.TabIndex = 11
        '
        'lblPrinterInfo
        '
        Me.lblPrinterInfo.AutoSize = True
        Me.lblPrinterInfo.Location = New System.Drawing.Point(33, 30)
        Me.lblPrinterInfo.Name = "lblPrinterInfo"
        Me.lblPrinterInfo.Size = New System.Drawing.Size(133, 13)
        Me.lblPrinterInfo.TabIndex = 10
        Me.lblPrinterInfo.Text = "Current printer to output to:"
        '
        'frmPrinter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 171)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbPrinters)
        Me.Controls.Add(Me.lblPrinterInfo)
        Me.Controls.Add(Me.cmdOK)
        Me.Name = "frmPrinter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Printer Selection"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmbPrinters As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrinterInfo As System.Windows.Forms.Label
End Class
