﻿Imports System.Configuration

Public Class TableSection
    Inherits ConfigurationSection

    <ConfigurationProperty("", IsDefaultCollection:=True)> _
    Public ReadOnly Property Tables() As Tables
        Get
            Return CType(Me(""), Tables)
        End Get
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

End Class


Public Class Tables
    Inherits ConfigurationElementCollection

    Public Sub New()
    End Sub

    Protected Overloads Overrides Function CreateNewElement() As ConfigurationElement
        Return New Table()
    End Function
    Protected Overrides Function GetElementKey(ByVal element As ConfigurationElement) As Object
        Return CType(element, Table).Name
    End Function
    Public Overrides ReadOnly Property CollectionType() As ConfigurationElementCollectionType
        Get
            Return ConfigurationElementCollectionType.BasicMap
        End Get
    End Property
    Protected Overrides ReadOnly Property ElementName() As String
        Get
            Return "add"
        End Get
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

End Class

Public Class Table
    Inherits ConfigurationElement

    <ConfigurationProperty("name", IsKey:=True, isRequired:=True)> Public ReadOnly Property Name() As String
        Get
            Return CStr(Me("name"))
        End Get
    End Property
    <ConfigurationProperty("keys", isRequired:=True)> Public ReadOnly Property Keys() As String
        Get
            Return CStr(Me("keys"))
        End Get
    End Property
    <ConfigurationProperty("version", isRequired:=False)> Public Property Version() As Integer
        Get
            Return CInt(Me("version"))
        End Get
        Set(ByVal value As Integer)
            Me("version") = value
        End Set
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

End Class