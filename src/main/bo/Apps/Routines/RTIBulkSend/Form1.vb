﻿Imports System.Configuration.ConfigurationManager

Public Class Form1

    Sub New()
        InitializeComponent()
        ProcessTables()
    End Sub

    Private Sub ProcessTables()

        Dim DSN As String = AppSettings("DSN")
        For Each argsString As String In My.Application.CommandLineArgs
            Dim args As String() = argsString.Split(New Char() {"="c}, 2)
            If args(0).ToLower() = "dsn" Then
                DSN = args(1)
                Exit For
            End If
        Next

        Dim XmlLocation = AppSettings("XmlLocation")
        Dim NumberRecords As Integer = CInt(AppSettings("NumberRecords"))

        Dim Connection As String = DSN
        Dim StoreID As String = GetStoreID(Connection)
        If StoreID Is Nothing Then End

        Dim config As Configuration.Configuration = OpenExeConfiguration(Configuration.ConfigurationUserLevel.None)
        Dim tableSection As TableSection = CType(config.GetSection("tables"), RTIBulkSend.TableSection)
        tableSection.SectionInformation.ForceSave = True

        For Each table As Table In tableSection.Tables
            Dim cn As New ADODB.Connection
            Dim rs As New ADODB.Recordset
            Dim dt As New DataTable
            Dim da As New OleDb.OleDbDataAdapter
            Dim sql As New StringBuilder

            Try
                sql.Append("SELECT TOP " & NumberRecords & " * ")
                sql.Append("FROM " & table.Name & " ")
                sql.Append("WHERE (RTI='S') ")
                sql.Append("ORDER BY RTI, " & table.Keys)
                Trace.WriteLine(sql.ToString, Me.Name)

                Trace.WriteLine("get records from table", Me.Name)
                cn.Open(Connection)
                rs.Open(sql.ToString, cn, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)
                da.Fill(dt, rs)


                Trace.WriteLine("close recordset", Me.Name)
                If rs.State <> 0 Then rs.Close()
                rs = Nothing


                Trace.WriteLine("check datatable for column names starting with number and rename with number on the end", Me.Name)
                For Each col As DataColumn In dt.Columns
                    Dim firstchar As String = col.ColumnName.Substring(0, 1)
                    If IsNumeric(firstchar) Then
                        col.ColumnName = col.ColumnName.Remove(0, 1)
                        col.ColumnName &= firstchar
                    End If
                Next



                Trace.WriteLine("insert storeID and date sent columns", Me.Name)
                If dt.Rows.Count > 0 Then
                    dt.TableName = table.Name
                    dt.Columns.Add("StoreID")
                    dt.Columns("StoreID").SetOrdinal(0)
                    If dt.Columns.Contains("RTI") Then dt.Columns.Remove("RTI")
                    If dt.Columns.Contains("RTICount") Then dt.Columns.Remove("RTICount")
                    If dt.Columns.Contains("SPARE") Then dt.Columns.Remove("SPARE")

                    For Each row As DataRow In dt.Rows
                        row(0) = StoreID
                    Next

                    'save to xml
                    Dim filename As String = table.Name.PadRight(20, "~"c) & "." & StoreID & "." & Format(Now, "dd.MM.yy-HH.mm.ss") & ".xml"
                    dt.WriteXml(My.Application.Info.DirectoryPath & "\" & filename)
                    File.Move(My.Application.Info.DirectoryPath & "\" & filename, XmlLocation & filename)
                End If



                Trace.WriteLine("update table as sent using table keys", Me.Name)
                For Each dr As DataRow In dt.Rows
                    sql = New StringBuilder
                    sql.Append("UPDATE " & table.Name & " SET RTI='C' WHERE ")

                    Dim keys() As String = table.Keys.Split(","c)
                    For i As Integer = 0 To (keys.Length - 1)
                        If i > 0 Then sql.Append("AND ")
                        Dim columnName As String = keys(i)
                        columnName = columnName.Replace("[", "")
                        columnName = columnName.Replace("]", "")

                        Select Case dt.Columns(columnName).DataType.ToString
                            Case "System.DateTime"
                                sql.Append(keys(i) & " = '" & Format(dr(columnName), "yyyy-MM-dd") & "' ")

                            Case "System.Int32"
                                sql.Append(keys(i) & " = " & dr(columnName).ToString & " ")

                            Case Else
                                sql.Append(keys(i) & " = '" & dr(columnName).ToString & "' ")
                        End Select
                    Next

                    Trace.WriteLine("update table", Me.Name)
                    Trace.WriteLine(sql.ToString)
                    cn.Execute(sql.ToString)
                Next

            Catch ex As Exception
                Trace.WriteLine("Error getting data for table: " & table.Name & Space(1) & ex.ToString, Me.Name)

            Finally
                If rs IsNot Nothing Then
                    If rs.State <> 0 Then rs.Close()
                    rs = Nothing
                End If
                If cn.State <> 0 Then cn.Close()
                cn = Nothing
            End Try
        Next

        End
    End Sub

    Private Function GetStoreID(ByVal Connection As String) As String

        Dim cn As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim sql As String = "SELECT STOR FROM SYSOPT WHERE FKEY='01'"
        GetStoreID = String.Empty

        Try
            cn.Mode = ADODB.ConnectModeEnum.adModeRead
            cn.Open(Connection)
            rs.Open(sql, cn)
            GetStoreID = rs.Fields("STOR").Value.ToString

            Trace.WriteLine(sql & ": " & GetStoreID, Me.Name)

        Catch ex As Exception
            Trace.WriteLine("Error getting StoreID: " & ex.ToString, Me.Name)
            Return Nothing
        Finally
            If rs.State <> 0 Then rs.Close()
            If cn.State <> 0 Then cn.Close()
            rs = Nothing
            cn = Nothing
        End Try

    End Function


End Class