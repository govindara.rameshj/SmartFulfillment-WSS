﻿Imports System.Text
Imports OasysDBBO
<Serializable()> Public Class cAdditionalPicCntItm
    Inherits CTSBOInterface
    '<CAMH>****************************************************************************************
    '* Module: cAdditionalPicCntItm
    '* Date  : 26/11/07
    '* Author: markm
    '*$Archive:
    '**********************************************************************************************
    '* Summary: Business Object to wrap the functionality of the AdditionalPicCntItm items
    '**********************************************************************************************
    '* $Author:  $
    '* $Date:  $
    '* $Revision:  $
    '* Versions:
    '</CAMH>***************************************************************************************
    Implements IDisposable

    Private BOFields As New List(Of Object)

    Private mWhereFields As List(Of DBField)
    Private mWhereJoins As List(Of Oasys3.DB.clsOasys3DB.eOperator)
    Private mLoadFields As List(Of Object)
    Private mSortFields As List(Of DBField)
    Private mAggregateFields As List(Of DBField)
    Private mOasysDBBo As New Oasys3.DB.clsOasys3DB
    Private m_oSession As Object
    Private mStatus As String = ""
    Private mConnectionString As String = String.Empty
    Const MODULE_NAME As String = "cAdditionalPicCntItm"
    Const TABLE_NAME As String = "AdditionalPicCntItm"

    'Define work fields 
    ' need to create a string like this otherwise the compiler will give each string the same refid
    Private mTransactionDate As New ColField(Of Date)("DATE1", "1900/01/01")
    Private mPartCode As New ColField(Of String)("SKUN", "", True)
    Private mDeleted As New ColField(Of Boolean)("IDEL", 0)
    Private mOriginCode As New ColField(Of String)("ORIG", "")

    ' end of work fields
#Region "Working Variables"
    
#End Region
        
#Region "Public Properties"

    Public Property TransactionDate() As ColField(Of Date)
        Get
            TransactionDate = mTransactionDate
        End Get
        Set(ByVal value As ColField(Of Date))
            mTransactionDate = value
        End Set
    End Property
    Public Property PartCode() As ColField(Of String)
        Get
            PartCode = mPartCode
        End Get
        Set(ByVal value As ColField(Of String))
            mPartCode = value
        End Set
    End Property
    Public Property Deleted() As ColField(Of Boolean)
        Get
            Deleted = mDeleted
        End Get
        Set(ByVal value As ColField(Of Boolean))
            mDeleted = value
        End Set
    End Property
    Public Property OriginCode() As ColField(Of String)
        Get
            OriginCode = mOriginCode
        End Get
        Set(ByVal value As ColField(Of String))
            mOriginCode = value
        End Set
    End Property


#End Region

#Region "Methods Generic"
    Private Sub Start()

        BOFields.Add(mTransactionDate)
        BOFields.Add(mPartCode)
        BOFields.Add(mDeleted)
        BOFields.Add(mOriginCode)

    End Sub
    Public Sub New()
        Start()
    End Sub
    Public Sub New(ByVal strConnection As String)
        ConnectionString = strConnection
        Start()
    End Sub
    Public Sub New(ByRef oasysdbbo As Oasys3.DB.clsOasys3DB)
        mOasysDBBo = oasysdbbo
        Start()
    End Sub
    Public Overrides Function BOProperties() As System.Collections.Generic.List(Of Object)
        Return BOFields

    End Function
    Public Overrides ReadOnly Property ModuleClassName() As String
        Get

            ModuleClassName = MODULE_NAME

        End Get
    End Property

    Public Overrides ReadOnly Property DebugString() As String
        Get

            DebugString = "mPartCode = " & mPartCode.Value.ToString & Space(1)

        End Get
    End Property

    Public Overrides ReadOnly Property Version() As String
        Get
            Version = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductName _
            & " " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion
        End Get
    End Property

    '<CACH>****************************************************************************************
    '* Function:  Boolean LoadFromRow()
    '**********************************************************************************************
    '* Description: Part of the interface called by the DB object to pass in a row of field and
    '*              for this object to extract the data and populate the properties of the object.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:oRow   Object. - collection of fields
    '**********************************************************************************************
    '* Returns:  Boolean
    '**********************************************************************************************
    '* History:
    '* 
    '*
    '</CACH>***************************************************************************************
    Public Overrides Function LoadFromRow(ByVal drRow As System.Data.DataRow) As Boolean

        Dim lngIndex As Long = 0

        LoadFromRow = True
        '        Try
        For Each dcColumn As Data.DataColumn In drRow.Table.Columns
            '            oField = oRow.Field(lFieldNo)
            If Not IsDBNull(drRow(dcColumn)) Then
                Select Case (dcColumn.ColumnName)
                    Case ("DATE1") : mTransactionDate.Value = drRow(dcColumn)
                    Case ("SKUN") : mPartCode.Value = drRow(dcColumn)
                    Case ("IDEL") : mDeleted.Value = drRow(dcColumn)
                    Case ("ORIG") : mOriginCode.Value = drRow(dcColumn)

                    Case Else 'invalid field ID passed in
                        LoadFromRow = False
                End Select
            End If
        Next

    End Function


    Public Overrides Function Initialise(ByRef oSession As Object) As Object

        m_oSession = oSession
        Return Me

    End Function


    Public Overrides Function SaveIfNew() As Boolean

        Return Save(Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)

    End Function

    Public Overrides Function SaveIfExists() As Boolean

        Return Save(Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)

    End Function
#Region "IsValid"
    Public Overrides Function IsValid() As Boolean

        Return True

    End Function
#End Region

    Public Overrides Function Status() As String

        Return mStatus

    End Function

    Public Overrides Function Save(ByVal eSave As Oasys3.DB.clsOasys3DB.eSqlQueryType) As Boolean
        Dim key As String
        Dim NoRecs As Integer

        '   Save all properties to the database

        Save = False
        If IsValid() Then
            mOasysDBBo.ClearAllParameters()
            mOasysDBBo.SetTableParameter(TABLE_NAME, eSave)
            If eSave = Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert Then
                SetDBFieldValues(mOasysDBBo)
                NoRecs = mOasysDBBo.Insert()
            Else
                SetDBFieldValues(mOasysDBBo)
                key = setKey(mOasysDBBo)
                mOasysDBBo.SetWhereParameter(mPartCode.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, mPartCode.Value)

                NoRecs = mOasysDBBo.Update()
            End If
        End If
        If NoRecs = -1 Then
            Save = True
        Else
            Save = False
        End If
        Return Save

    End Function

    Public Overrides Sub SetDBFieldValues(ByRef db As Oasys3.DB.clsOasys3DB)

        Dim value As New Object

        db.ClearColumnParameters()
        For Each entry As Object In BOFields
            If entry.IdentityKey = False Then
                db.SetColumnAndValueParameter(entry.ColumnName, entry.Value)
            End If
        Next

    End Sub

    Public Overrides Function setKey(ByRef db As Oasys3.DB.clsOasys3DB) As String

        Dim value As String = ""

        For Each entry As Object In BOFields
            If entry.ColumnKey = True Then
                value = entry.value
                Return value
            End If
        Next
        Return value


    End Function

    Public Overrides Function Delete() As Boolean
        Dim NoRecs As Integer
        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)
        mOasysDBBo.SetWhereParameter(mPartCode.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, mPartCode.Value)

        NoRecs = mOasysDBBo.Delete()
        If NoRecs = -1 Then
            Delete = True
        Else
            Delete = False
        End If
        Return Delete

    End Function

    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:Comparator Long. - Comparison Type - see modBO.bas
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '*In/Out:Value  Variant. - Value that Field will be compared to
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of String), ByRef Value As String) As Boolean

        Dim oField As DBField

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function 'AddLoadFilter
    Public Overloads Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As Object, ByRef Value As Object) As Boolean

        Dim oField As DBField

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function

    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:Comparator Long. - Comparison Type - see modBO.bas
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '*In/Out:Value  Variant. - Value that Field will be compared to
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Integer), ByRef Value As Integer) As Boolean

        Dim oField As DBField

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function 'AddLoadFilter
    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:Comparator Long. - Comparison Type - see modBO.bas
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '*In/Out:Value  Variant. - Value that Field will be compared to
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Decimal), ByRef Value As Decimal) As Boolean

        Dim oField As DBField

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function 'AddLoadFilter
    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:Comparator Long. - Comparison Type - see modBO.bas
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '*In/Out:Value  Variant. - Value that Field will be compared to
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Date), ByRef Value As Date) As Boolean

        Dim oField As DBField

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function 'AddLoadFilter
    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:Comparator Long. - Comparison Type - see modBO.bas
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '*In/Out:Value  Variant. - Value that Field will be compared to
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function AddLoadFilter(ByRef eOperator As Oasys3.DB.clsOasys3DB.eOperator, ByRef FieldID As ColField(Of Boolean), ByRef Value As Object) As Boolean

        Dim oField As DBField

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

        'Create field for storing Key into
        oField = GetField(FieldID)
        oField.DBValue = Value
        oField.DBOperator = eOperator
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mWhereFields Is Nothing Then mWhereFields = New List(Of DBField)
        Call mWhereFields.Add(oField)
        Return True

    End Function 'AddLoadFilter

    '<CACH>****************************************************************************************
    '* Function:  Boolean JoinLoadFilter()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up any selection criteria before
    '*              calling the Load method.  This appends the selection criteria to the
    '*              oRowSel object which is used by the Load method.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '**********************************************************************************************
    '* Returns:  Boolean - True if Valid FieldID and successful
    '**********************************************************************************************
    '* History:
    '* 
    '</CACH>***************************************************************************************
    Public Overrides Function JoinLoadFilter(ByRef DBOperator As Oasys3.DB.clsOasys3DB.eOperator) As Boolean

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddJoinLoadFilter"

        'Create field for storing Key into

        If mWhereJoins Is Nothing = True Then mWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
        mWhereJoins.Add(DBOperator)
        Return True

    End Function 'AddLoadFilter
    '<CACH>****************************************************************************************
    '* Function:  Boolean AddLoadField()
    '**********************************************************************************************
    '* Description: Part of the Interface.  Used to set-up all fields that must be returned
    '*              when using the LoadMatches method.  This appends the field to the oFieldRow
    '*              object which is used by the LoadMatches method, if it is populated.
    '**********************************************************************************************
    '* Parameters:
    '*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
    '**********************************************************************************************
    '* History:
    '* 09/10/02    mauricem
    '*             Header added.
    '</CACH>***************************************************************************************
    Public Overrides Sub AddLoadField(ByRef FieldID As Object)

        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"

        'Add field to Row selector
        If mLoadFields Is Nothing Then mLoadFields = (New List(Of Object))
        Call mLoadFields.Add(FieldID)

    End Sub 'AddLoadField
    Public Overrides Sub AddAggregateField(ByVal AggreateOperator As OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates, ByVal FieldId As Object, ByVal AliasName As String)
        Dim oField As DBField

        'FieldId.ColumnName = ColumnName
        'Create field for storing Key into
        oField = GetField(FieldId)
        oField.DBValue = AliasName
        oField.DBOperator = AggreateOperator
        If oField Is Nothing Then Exit Sub



        Const PROCEDURE_NAME As String = MODULE_NAME & ".AddAggregateField"

        'Add field to Row selector
        If mAggregateFields Is Nothing Then mAggregateFields = (New List(Of OasysDBBO.DBField))
        Call mAggregateFields.Add(oField)

    End Sub 'AddLoadField

    Public Overrides Sub ClearLoadFilter()

        mWhereFields = Nothing
        mWhereJoins = Nothing

    End Sub

    Public Overrides Sub ClearLoadField()

        mLoadFields = Nothing

    End Sub
    Public Overrides Sub ConnectionKey(ByVal key As String)

        mOasysDBBo.ConnectionKey = key
        mConnectionString = mOasysDBBo.ConnectionString

    End Sub
    Public Overrides Property ConnectionString() As String

        Get
            ConnectionString = mOasysDBBo.ConnectionString
        End Get

        Set(ByVal value As String)
            mOasysDBBo.ConnectionString = value
            mConnectionString = value
        End Set
    End Property
    Public Overrides Function SortBy(ByVal column As String, ByVal type As Oasys3.DB.clsOasys3DB.eOrderByType) As Boolean
        Dim oField As New DBField(column, "", DBField.enColumnType.enctString)

        Const PROCEDURE_NAME As String = MODULE_NAME & ".SortBy"

        'Create field for storing Key into
        oField.DBValue = column
        oField.DBOperator = type
        If oField Is Nothing Then Exit Function

        'Add field to Row selector
        If mSortFields Is Nothing Then mSortFields = New List(Of DBField)
        Call mSortFields.Add(oField)
        Return True

    End Function

    Public Function LoadMatches(Optional ByVal count As Integer = -1) As List(Of BOAdditionalPicCntItm.cAdditionalPicCntItm)

        ' Load up all properties from the database
        Dim colLines As New List(Of cAdditionalPicCntItm)
        Dim colCTSBO As List(Of CTSBOInterface) = Nothing

        colCTSBO = LoadCTSMatches()

        For Each CTSBo As CTSBOInterface In colCTSBO
            colLines.Add(CType(CTSBo, BOAdditionalPicCntItm.cAdditionalPicCntItm))
        Next

        Return colLines
        Exit Function

    End Function
    Public Overrides Function LoadCTSMatches(Optional ByVal count As Integer = -1) As List(Of CTSBOInterface)

        ' Load up all properties from the database
        Dim dsLines As DataSet
        Dim colLines As New List(Of CTSBOInterface)

        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
        If (mWhereFields Is Nothing) Then mWhereFields = New List(Of DBField)
        For Each entry As DBField In mWhereFields
            mOasysDBBo.SetWhereParameter(entry.ColumnName, entry.DBOperator, entry.DBValue)
        Next

        If (mWhereJoins Is Nothing) Then mWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
        For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In mWhereJoins
            mOasysDBBo.SetWhereJoinParameter(WhereJoin)
        Next

        If Not mLoadFields Is Nothing Then
            For Each entry As DBField In mLoadFields
                mOasysDBBo.SetColumnParameter(entry.ColumnName)
            Next
        End If

        If Not mSortFields Is Nothing Then
            For Each entry As DBField In mSortFields
                mOasysDBBo.SetOrderByParameter(entry.ColumnName, entry.DBOperator)
            Next
        End If

        Try
            dsLines = mOasysDBBo.Query()
        Catch ex As Oasys3.DB.OasysDbException
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        Catch ex As Exception
            'Throw New Oasys2MQException("SProfile check if user exists error - " + ex.ToString)
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        End Try
        If dsLines.Tables(0).Rows.Count > 0 Then
            'load the first row into me 
            Me.LoadFromRow(dsLines.Tables(0).Rows(0))

            Dim linecount As Integer = 0
            For Each drRow As Data.DataRow In dsLines.Tables(0).Rows
                Dim oLine As New cAdditionalPicCntItm
                Call oLine.LoadFromRow(drRow)
                Call colLines.Add(oLine)
                linecount = linecount + 1
                If linecount = count Then
                    Exit For
                End If
            Next

        End If
        '      Call OasysDBBo.Dispose()

        ClearLists()

        Return colLines

    End Function

    Public Overrides Function GetAggregateDataSet() As DataSet

        ' Load up all properties from the database
        Dim dsLines As DataSet
        Dim colLines As New List(Of cAdditionalPicCntItm)

        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
        If (mWhereFields Is Nothing) Then mWhereFields = New List(Of DBField)
        For Each entry As DBField In mWhereFields
            mOasysDBBo.SetWhereParameter(entry.ColumnName, entry.DBOperator, entry.DBValue)
        Next

        If (mWhereJoins Is Nothing) Then mWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
        For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In mWhereJoins
            mOasysDBBo.SetWhereJoinParameter(WhereJoin)
        Next

        If Not mAggregateFields Is Nothing Then
            For Each entry As DBField In mAggregateFields
                mOasysDBBo.SetColumnAggregate(entry.DBOperator, entry.ColumnName, entry.DBValue)
            Next
        End If

        If Not mSortFields Is Nothing Then
            For Each entry As DBField In mSortFields
                mOasysDBBo.SetOrderByParameter(entry.ColumnName, entry.DBOperator)
            Next
        End If

        Try
            dsLines = mOasysDBBo.Query()
        Catch ex As Oasys3.DB.OasysDbException
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        Catch ex As Exception
            'Throw New Oasys2MQException("SProfile check if user exists error - " + ex.ToString)
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        End Try
        'If dsLines.Tables(0).Rows.Count > 0 Then
        '    'load the first row into me 
        '    Me.LoadFromRow(dsLines.Tables(0).Rows(0))

        '    Dim linecount As Integer = 0
        '    For Each drRow As Data.DataRow In dsLines.Tables(0).Rows
        '        Dim oLine As New cmmtest
        '        Call oLine.LoadFromRow(drRow)
        '        Call colLines.Add(oLine)
        '    Next

        'End If
        '      Call OasysDBBo.Dispose()

        ClearLists()

        Return dsLines

    End Function
    Public Overrides Function GetAggregateField() As Object

        ' Load up all properties from the database
        Dim dsLines As DataSet
        Dim colLines As New List(Of cAdditionalPicCntItm)

        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
        If (mWhereFields Is Nothing) Then mWhereFields = New List(Of DBField)
        For Each entry As DBField In mWhereFields
            mOasysDBBo.SetWhereParameter(entry.ColumnName, entry.DBOperator, entry.DBValue)
        Next

        If (mWhereJoins Is Nothing) Then mWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
        For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In mWhereJoins
            mOasysDBBo.SetWhereJoinParameter(WhereJoin)
        Next

        If Not mAggregateFields Is Nothing Then
            For Each entry As DBField In mAggregateFields
                mOasysDBBo.SetColumnAggregate(entry.DBOperator, entry.ColumnName, entry.DBValue)
            Next
        End If

        If Not mSortFields Is Nothing Then
            For Each entry As DBField In mSortFields
                mOasysDBBo.SetOrderByParameter(entry.ColumnName, entry.DBOperator)
            Next
        End If

        Try
            dsLines = mOasysDBBo.Query()
        Catch ex As Oasys3.DB.OasysDbException
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        Catch ex As Exception
            'Throw New Oasys2MQException("SProfile check if user exists error - " + ex.ToString)
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        End Try

        ClearLists()

        Return dsLines.Tables(0).Rows(0).Item(0)

    End Function

    Public Overrides Sub GetSQLBase()

        ' Load up all properties from the database

        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pSelect)
        If (mWhereFields Is Nothing) Then mWhereFields = New List(Of DBField)
        For Each entry As DBField In mWhereFields
            mOasysDBBo.SetWhereParameter(entry.ColumnName, entry.DBOperator, entry.DBValue)
        Next

        If (mWhereJoins Is Nothing) Then mWhereJoins = New List(Of Oasys3.DB.clsOasys3DB.eOperator)
        For Each WhereJoin As Oasys3.DB.clsOasys3DB.eOperator In mWhereJoins
            mOasysDBBo.SetWhereJoinParameter(WhereJoin)
        Next

        If Not mLoadFields Is Nothing Then
            For Each entry As Object In mLoadFields
                mOasysDBBo.SetColumnParameter(entry.ColumnName)
            Next
        End If

        If Not mSortFields Is Nothing Then
            For Each entry As DBField In mSortFields
                mOasysDBBo.SetOrderByParameter(entry.ColumnName, entry.DBOperator)
            Next
        End If

    End Sub
    Public Overrides Function GetSQLSelectDataSet() As DataSet

        ' Load up all properties from the database
        Dim dsLines As DataSet

        GetSQLBase()

        Try
            dsLines = mOasysDBBo.Query()
        Catch ex As Oasys3.DB.OasysDbException
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        Catch ex As Exception
            'Throw New Oasys2MQException("SProfile check if user exists error - " + ex.ToString)
            Call mOasysDBBo.Dispose()
            Return Nothing
        End Try
        '      Call OasysDBBo.Dispose()

        ClearLists()

        Return dsLines

    End Function

    Public Overrides Function GetSQLSelect() As String

        GetSQLBase()

        Return mOasysDBBo.BuildSql()

    End Function

    Public Overrides Function GetTextLine() As String

        ' Load up all properties from the database
        Dim dsLines As DataSet
        Dim text As String

        GetSQLBase()

        Try
            dsLines = mOasysDBBo.Query()
        Catch ex As Oasys3.DB.OasysDbException
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        Catch ex As Exception
            'Throw New Oasys2MQException("SProfile check if user exists error - " + ex.ToString)
            Trace.WriteLine(ex.Message)
            Call mOasysDBBo.Dispose()
            Return Nothing
        End Try
        If dsLines.Tables(0).Rows.Count > 0 Then
            Me.LoadFromRow(dsLines.Tables(0).Rows(0))

            For Each item As Object In BOFields
                text = text & item.ColumnCSV().ToString
            Next

        End If
        ' remove last comma
        text = Mid(text, 1, Len(text) - 1)
        '      Call OasysDBBo.Dispose()

        ClearLists()

        Return text

    End Function

    Public Overrides Sub ClearLists()
        mWhereFields = Nothing
        mWhereJoins = Nothing
        mLoadFields = Nothing
        mSortFields = Nothing
        mAggregateFields = Nothing
    End Sub

    Public Overrides Function GetField(ByRef lFieldID As Object) As DBField

        Return New DBField(lFieldID.ColumnName, lFieldID.Value, lFieldID.ColumnType)

        Exit Function

Error_Handler:

        Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID.ColumnName & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
        Resume Next

    End Function

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free unmanaged resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region
#End Region

#Region "BusinessMethods"
    Public Function HKDeleteRecord(ByVal PartCode As String) As Boolean
        Dim NoRecs As Integer
        Dim SaveError As Boolean
        mOasysDBBo.ClearAllParameters()
        mOasysDBBo.SetTableParameter(TABLE_NAME, Oasys3.DB.clsOasys3DB.eSqlQueryType.pDelete)

        mOasysDBBo.SetWhereParameter(mPartCode.ColumnName, Oasys3.DB.clsOasys3DB.eOperator.pEquals, PartCode)

        NoRecs = mOasysDBBo.Delete()

        If NoRecs = -1 Then
            SaveError = True
        Else
            SaveError = False
        End If

        Return SaveError
    End Function

    Public Overrides Function GetProperty(ByVal FieldName As String) As Object
        ' return the property of the Field name passed in
        For intProperty As Integer = 0 To BOProperties.Count
            If BOProperties.Item(intProperty).columnName = FieldName Then
                Return BOFields.Item(intProperty)
            End If
        Next
        Return Nothing
    End Function 'ConvertOperator
#End Region


End Class
