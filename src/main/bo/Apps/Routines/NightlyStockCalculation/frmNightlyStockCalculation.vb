﻿Imports System
Imports System.IO
Imports OasysDBBO

Public Class frmNightlyStockCalculation
    Dim _blnRunFromNightlyRoutine As Boolean
    Dim _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Dim _BOSystem As BOSystem.cSystemDates
    Dim _BOStock As BOStock.cStock

    Dim _totalStockValue As Decimal = 0
    Dim _totalInstallationValue As Decimal = 0
    Dim _totalDeliveryValue As Decimal = 0
    Dim _totalOpenReturnValue As Decimal = 0
    Dim _dayValue As Integer = 0
    Dim _yesterday As Integer = 0

    Public Sub RunNightlyStockCalculation()

        Const MODULE_NAME = "RunNightlyStockCalculation"

        Dim decStockValue As Decimal = 0
        Dim blnUpdateFlag As Boolean = False
        Dim strDay As String = String.Empty
        Dim strSql As String = String.Empty
        Dim dsStock As DataSet = Nothing
        Dim dtStock As DataTable = Nothing

        'btnSelect.Enabled = False
        'btnExit.Enabled = False
        Cursor = Cursors.WaitCursor

        'Need to get the Store Live Date for processing 
        _BOSystem.LoadMatches()

        Dim dteTodayDate As Date = _BOSystem.Today.Value
        Select Case dteTodayDate.DayOfWeek
            Case DayOfWeek.Monday
                _dayValue = 1
                _yesterday = 7
                strDay = "Monday"
            Case DayOfWeek.Tuesday
                _dayValue = 2
                _yesterday = 1
                strDay = "Tuesday"
            Case DayOfWeek.Wednesday
                _dayValue = 4
                _yesterday = 2
                strDay = "Wednesday"
            Case DayOfWeek.Thursday
                _dayValue = 8
                _yesterday = 3
                strDay = "Thursday"
            Case DayOfWeek.Friday
                _dayValue = 16
                _yesterday = 4
                strDay = "Friday"
            Case DayOfWeek.Saturday
                _dayValue = 32
                _yesterday = 5
                strDay = "Saturday"
            Case DayOfWeek.Sunday
                _dayValue = 64
                _yesterday = 6
                strDay = "Sunday"
        End Select

        lblDate.Text = "Update is for " & dteTodayDate.DayOfWeek.ToString & " " & Format(dteTodayDate, "dd/MM/yy")
        Me.Refresh()
        Application.DoEvents()

        Try

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Dhanesh Ramachandran
            ' Date        : 06/12/2010
            ' Referral No : 444
            ' Notes       : The routine has been updated to roll over the sales to previous weeks.
            '
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'get all the records from
            strSql = "SELECT SKUN, ONHA, PRIC, IOBS, INON, RETV, SALT, SALV1, FLAG, DATS, SALU1, CPDO, CYDO, DO001 FROM STKMAS"
            dsStock = _Oasys3DB.ExecuteSql(strSql)
            dtStock = dsStock.Tables(0)
            Trace.WriteLine("SQL: " & strSql, MODULE_NAME & " RunNightlyStockCalculation")
            Trace.WriteLine("Rows returned: " & dtStock.Rows.Count.ToString, MODULE_NAME & " RunStockLogPurge")


            ProgressBar1.Minimum = 0
            ProgressBar1.Maximum = dtStock.Rows.Count
            Application.DoEvents()
            Refresh()


            'Process Each STKMAS record in turn
            'For Each StockItem As BOStock.cStock In StockList


            For Each rec As DataRow In dtStock.Rows

                Dim StockItem As New BOStock.cStock(_Oasys3DB)
                blnUpdateFlag = False
                If ((rec.Item("DATS").ToString().Equals(String.Empty)) AndAlso (rec.Item("ONHA") > 0)) Then

                    lblSku.Text = rec.Item("SKUN").ToString 'StockItem.SkuNumber.Value
                    Application.DoEvents()
                    Refresh()

                    StockItem.SkuNumber.Value = rec.Item("SKUN").ToString
                    StockItem.WeeklyUpdate.Value = rec.Item("FLAG")
                    StockItem.DaysOutStockPeriod.Value = rec.Item("CPDO")
                    StockItem.DaysOutOfStock.Value = rec.Item("CYDO")
                    StockItem.DateFirstStock.Value = IIf(IsDBNull(rec.Item("DATS")), Nothing, rec.Item("DATS"))
                    StockItem.DaysOutStockPeriod1.Value = rec.Item("DO001")
                    StockItem.StockOnHand.Value = rec.Item("ONHA")

                    blnUpdateFlag = False
                    If StockItem.WeeklyUpdate.Value = True Then 'Weekly Update

                        blnUpdateFlag = True
                        StockItem.WeeklyUpdate.Value = False
                    End If

                    If StockItem.DateFirstStock.Value = Nothing And StockItem.StockOnHand.Value = 0 Then
                        'Intentionally left blank to follow uptrac logic as this spec was hell
                    Else

                        If StockItem.DateFirstStock.Value = Nothing Then
                            StockItem.DateFirstStock.Value = dteTodayDate
                            blnUpdateFlag = True
                        End If

                    End If

                ElseIf (rec.Item("DATS").ToString() <> String.Empty AndAlso (rec.Item("ONHA") <> 0)) Then

                    lblSku.Text = rec.Item("SKUN").ToString 'StockItem.SkuNumber.Value
                    Application.DoEvents()
                    Refresh()

                    StockItem.SkuNumber.Value = rec.Item("SKUN").ToString
                    StockItem.WeeklyUpdate.Value = rec.Item("FLAG")
                    StockItem.DaysOutStockPeriod.Value = rec.Item("CPDO")
                    StockItem.DaysOutOfStock.Value = rec.Item("CYDO")
                    StockItem.DateFirstStock.Value = IIf(IsDBNull(rec.Item("DATS")), Nothing, rec.Item("DATS"))
                    StockItem.DaysOutStockPeriod1.Value = rec.Item("DO001")
                    StockItem.StockOnHand.Value = rec.Item("ONHA")

                    blnUpdateFlag = False
                    If StockItem.WeeklyUpdate.Value = True Then 'Weekly Update

                        blnUpdateFlag = True
                        StockItem.WeeklyUpdate.Value = False
                    End If

                    If StockItem.DateFirstStock.Value = Nothing And StockItem.StockOnHand.Value = 0 Then
                        'Intentionally left blank to follow uptrac logic as this spec was hell
                    Else

                        If StockItem.DateFirstStock.Value = Nothing Then
                            StockItem.DateFirstStock.Value = dteTodayDate
                            blnUpdateFlag = True
                        End If

                        If StockItem.StockOnHand.Value <= 0 And StockItem.ValueSoldYesterday.Value = 0 Then
                            'Were out of stock
                            StockItem.DaysOutStockPeriod.Value += 1
                            StockItem.DaysOutOfStock.Value += 1
                            StockItem.DaysOutStockPeriod1.Value += _dayValue
                            blnUpdateFlag = True
                        End If

                    End If

                End If


                'Only Update If Flag is set
                If blnUpdateFlag = True Then
                    StockItem.UpdateNightlyStock()
                End If

                ProgressBar1.Value += 1
                Application.DoEvents()
                Refresh()

                StockItem = Nothing

            Next rec

            ''Get Necessary Totals
            '_decTotalStockValue = GetTotalsWithSql("SELECT SUM(ONHA * PRIC) AS TOTAL from STKMAS WHERE ONHA > 0")
            '_decTotalOpenReturnValue = GetTotalsWithSql("SELECT SUM(RETV) as TOTAL from STKMAS WHERE RETV > 0")
            '_decTotalDeliveryValue = GetTotalsWithSql("SELECT SUM(SALV1) as TOTAL from STKMAS WHERE SALT ='D'")
            '_decTotalInstallationValue = GetTotalsWithSql("SELECT SUM(SALV1) as TOTAL from STKMAS WHERE SALT ='I'")

            ''Update Totals
            'Dim ManagerDetails As New BOPicControl.cManagerDetails(_Oasys3DB)
            'ManagerDetails.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, ManagerDetails.ID, "01")
            'ManagerDetails.LoadMatches()

            'Select Case _BOSystem.TodayDayCode.Value
            '    Case 1
            '        ManagerDetails.StockValue1.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue1.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue1.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue1.Value = _decTotalInstallationValue
            '    Case 2
            '        ManagerDetails.StockValue2.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue2.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue2.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue2.Value = _decTotalInstallationValue
            '    Case 3
            '        ManagerDetails.StockValue3.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue3.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue3.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue3.Value = _decTotalInstallationValue
            '    Case 4
            '        ManagerDetails.StockValue4.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue4.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue4.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue4.Value = _decTotalInstallationValue
            '    Case 5
            '        ManagerDetails.StockValue5.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue5.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue5.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue5.Value = _decTotalInstallationValue
            '    Case 6
            '        ManagerDetails.StockValue6.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue6.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue6.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue6.Value = _decTotalInstallationValue
            '    Case 7
            '        ManagerDetails.StockValue7.Value = _decTotalStockValue
            '        ManagerDetails.ReturnValue7.Value = _decTotalOpenReturnValue
            '        ManagerDetails.DeliveryValue7.Value = _decTotalDeliveryValue
            '        ManagerDetails.InstallationValue7.Value = _decTotalInstallationValue
            'End Select
            'ManagerDetails.SaveIfExists()


            'Kill the TASKCOMP file if it exists
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
            Cursor = Cursors.Default
            End

        Catch ex As Exception

            Trace.WriteLine(ex.Message)
            End

        End Try

    End Sub

    Private Function GetTotalsWithSql(ByVal strSQL As String) As Decimal

        Dim ds As DataSet = _Oasys3DB.ExecuteSql(strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow = dt.Rows(0)

        GetTotalsWithSql = IIf(IsDBNull(dr.Item("TOTAL")), 0, dr.Item("TOTAL"))

    End Function

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Ask the user if they want to update, if they answer No the program ends.
        If MsgBox("Nightly Stock File Calculation - Continue (Y/N):", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Daily Flash Totals Creation") = vbYes Then
            RunNightlyStockCalculation()
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub frmNightlyStockCalculation_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.Show()
        Application.DoEvents()

    End Sub

    Private Sub frmNightlyStockCalculation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyValue = Keys.F10 Then
            Debug.Print("F10 Hit")
            btnExit_Click(sender, e)
        End If

        If e.KeyValue = Keys.F5 Then
            Debug.Print("F5 Hit")
            btnSelect_Click(sender, e)
        End If

    End Sub

    Private Sub frmNightlyStockCalculation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar.ToString = Keys.F10.ToString Then
            Debug.Print("F10 Hit")
        End If
    End Sub

    Private Sub frmNightlyStockCalculation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Debug.Print("Started")
        ' get command parameters 
        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd = "Y" Or strCmd = "y" Then
                _blnRunFromNightlyRoutine = True
            End If
        Next

        Me.Show()
        Application.DoEvents()

        lblDate.Text = "Update is for "
        lblSku.Text = ""

        'Run Automatically if ran from the night routine 
        If _blnRunFromNightlyRoutine = True Then
            RunNightlyStockCalculation()
        Else
            If MsgBox("Nightly Stock File Calculation - Continue (Y/N):", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Nightly Stock File Calculation") = vbYes Then
                RunNightlyStockCalculation()
            End If
        End If

        'Program ends
        End

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Cts.Oasys.Core.Tests.TestEnvironmentSetup.SetupIfTestRun()

        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        _BOSystem = New BOSystem.cSystemDates(_Oasys3DB)
        _BOStock = New BOStock.cStock(_Oasys3DB)

    End Sub
End Class
