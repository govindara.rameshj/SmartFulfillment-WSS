﻿Imports System.IO

Public Class frmAutomaticIssueChecking

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Private strRejectCodeHeader As String = String.Empty
    Private strRejectCodeLine As String = String.Empty
    Private blnPartialReceipt As Boolean = False
    Private decTotalValue As Decimal = 0
    Private intUserId As Integer = 0

    Private Sub RunCheck()

        Dim issueline As BOPurchases.cIssueLine = New BOPurchases.cIssueLine(_Oasys3DB)
        Dim issueHeader As BOPurchases.cIssueHeader = New BOPurchases.cIssueHeader(_Oasys3DB)
        Dim lstIssueHeaders As List(Of BOPurchases.cIssueHeader)

        lstIssueHeaders = issueHeader.checkIssueHeader()
        pgBar.Maximum = lstIssueHeaders.Count
        pgBar.Value = 0
        For Each Header As BOPurchases.cIssueHeader In lstIssueHeaders

            strRejectCodeHeader = String.Empty
            strRejectCodeLine = String.Empty
            'is this an import then do supmas check
            If Header.ImportedIssue.Value = True Then

                'check supmas 
                If SupmasExists(Header.SupplierNumber.Value) = False Then
                    issueHeader.UpdateRejectCode(Header.IssueNumber.Value, "N")
                End If

                ImportCheckLine(Header.IssueNumber.Value)

            Else

                'check purhdr
                Check_Purhdr(Header.StorePoNumber.Value, Header.StoreSoqNumber.Value, Header.SupplierNumber.Value, Header.IssueNumber.Value)
                If SupmasExists(Header.SupplierNumber.Value) = False Then
                    strRejectCodeHeader = "N"
                End If
                'check conmas 
                If blnPartialReceipt = True Then
                    Check_Conmas(Header.StorePoNumber.Value)
                End If
                checkLine(Header.IssueNumber.Value)

            End If

            If strRejectCodeHeader <> String.Empty Then
                issueHeader.UpdateRejectCode(Header.IssueNumber.Value, strRejectCodeHeader)
            End If
            'If RejectCodeLine <> String.Empty Then
            '    issueline.UpdateRejectCode(Header.IssueNumber.Value, RejectCodeLine)
            'End If
            issueHeader.UpdateIssueValue(Header.IssueNumber.Value, decTotalValue)

            pgBar.Value += 1
            pgBar.Refresh()
            Me.Refresh()
            Application.DoEvents()

        Next Header

        'Kill the TASKCOMP file if it exists
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        FindForm.Close()

    End Sub 'RunCheck

    Private Function SupmasExists(ByVal Supplier As String) As Boolean

        Dim issueHeader As BOPurchases.cIssueHeader = New BOPurchases.cIssueHeader(_Oasys3DB)

        issueHeader.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, issueHeader.SupplierNumber, Supplier)
        If issueHeader.LoadMatches(1).Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function 'SupmasExists

    Private Sub ImportCheckLine(ByVal issueNum As String)

        Dim purlin As BOPurchases.cPurchaseLine = New BOPurchases.cPurchaseLine(_Oasys3DB)
        Dim stkmas As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim isslin As BOPurchases.cIssueLine = New BOPurchases.cIssueLine(_Oasys3DB)
        Dim lstIsslin As List(Of BOPurchases.cIssueLine)
        Dim decCalc As Decimal = 0

        decTotalValue = 0

        'find lines to calculate from 
        isslin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, isslin.IssueNumber, issueNum)
        lstIsslin = isslin.LoadMatches()
        If lstIsslin IsNot Nothing Then
            For Each line As BOPurchases.cIssueLine In lstIsslin
                'does the sku exist in stock master
                stkmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, stkmas.SkuNumber, line.SkuNumber.Value)
                If stkmas.LoadMatches().Count > 0 Then
                    decCalc = Decimal.Round(CDec(stkmas.NormalSellPrice.Value * line.QtyIssued.Value), 2, MidpointRounding.ToEven)
                    decTotalValue += decCalc
                Else
                    strRejectCodeLine = "I"
                    isslin.UpdateRejectCode(issueNum, line.LineNumber.Value, strRejectCodeLine)
                    If strRejectCodeHeader = String.Empty Then
                        strRejectCodeHeader = "I"
                    End If
                End If
            Next
        End If
    End Sub 'ImportCheckLine

    Private Sub Check_Purhdr(ByVal strPurchaseOrderNum As String, ByVal strStoreSOQCtrlNum As String, ByVal strSupplierNum As String, ByVal strIssueNum As String)

        Dim purhdr As BOPurchases.cPurchaseHeader = New BOPurchases.cPurchaseHeader(_Oasys3DB)
        Dim supmas As BOPurchases.cSupplierMaster = New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim supdet As BOPurchases.cSupplierDetail = New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim isshdr As BOPurchases.cIssueHeader = New BOPurchases.cIssueHeader(_Oasys3DB)

        blnPartialReceipt = False
        purhdr.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, purhdr.PONumber, strPurchaseOrderNum)
        If purhdr.LoadMatches().Count <= 0 Then

            strRejectCodeHeader = "B"
            strRejectCodeLine = "B"

        Else

            If purhdr.ReceivedPartial.Value = True Then
                blnPartialReceipt = True
            End If

            If purhdr.PurchaseHeaderID.Value = 0 Then

                strRejectCodeHeader = "A"
                strRejectCodeLine = "A"

            Else

                If purhdr.ReceivedComplete.Value = True Then

                    strRejectCodeHeader = "C"
                    strRejectCodeLine = "C"
                    Exit Sub

                Else

                    If purhdr.Deleted.Value = True Then

                        strRejectCodeHeader = "D"
                        strRejectCodeLine = "D"
                        Exit Sub

                    Else

                        If purhdr.IssueFileValidated.Value = True Then

                            strRejectCodeHeader = "O"
                            strRejectCodeLine = "O"
                            Exit Sub

                        Else

                            If purhdr.SOQNumber.Value <> strStoreSOQCtrlNum Then

                                strRejectCodeHeader = "E"
                                strRejectCodeLine = "E"
                                Exit Sub

                            Else

                                If purhdr.SupplierNumber.Value = strSupplierNum Then

                                    purhdr.UpdateIssueFileUpdated(strPurchaseOrderNum)

                                Else

                                    'check for alternative supplier number
                                    supmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supmas.Number, purhdr.SupplierNumber.Value)
                                    If supmas.LoadMatches().Count <= 0 Then

                                        'no record 
                                        strRejectCodeHeader = "N"
                                        strRejectCodeLine = "N"
                                        Exit Sub

                                    Else
                                        ' try and find detail record 
                                        supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.SupplierNumber, supmas.Number.Value)
                                        supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                        supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.DepotType, "O")
                                        supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                        supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.DepotNumber, supmas.OrderDepotNumber.Value)
                                        If supdet.LoadMatches().Count > 0 Then
                                            If supdet.BBC.Value <> "A" Then
                                                strRejectCodeHeader = "N"
                                                strRejectCodeLine = "N"
                                                Exit Sub
                                            End If
                                            isshdr.UpdateSupplierNumber(strIssueNum, purhdr.SupplierNumber.Value)
                                            purhdr.UpdateIssueFileUpdated(purhdr.PONumber.Value)
                                        Else
                                            'Failed to get a record now try with different parameters ...
                                            supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.SupplierNumber, supmas.Number.Value)
                                            supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                            supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.DepotType, "S")
                                            supdet.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                                            supdet.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, supdet.DepotNumber, "999")
                                            If supdet.LoadMatches().Count > 0 Then
                                                If supdet.BBC.Value <> "A" Then
                                                    strRejectCodeHeader = "N"
                                                    strRejectCodeLine = "N"
                                                    Exit Sub
                                                End If
                                                isshdr.UpdateSupplierNumber(strIssueNum, purhdr.SupplierNumber.Value)
                                                purhdr.UpdateIssueFileUpdated(purhdr.PONumber.Value)
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub 'Check_Purhdr

    Private Sub Check_Conmas(ByVal Num As String)

        Dim conmas As BOPurchases.cConsignMaster = New BOPurchases.cConsignMaster(_Oasys3DB)

        conmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, conmas.PONumber, Num)
        If conmas.LoadMatches().Count = 0 Then
            strRejectCodeHeader = "G"
            strRejectCodeLine = "G"
        Else
            If conmas.Number.Value = "000000" Then
                strRejectCodeHeader = "F"
                strRejectCodeLine = "F"
            End If

            If Num < conmas.BBCIssueNumber.Value Then
                strRejectCodeHeader = "H"
                strRejectCodeLine = "H"
            End If
        End If
    End Sub 'Check_Conmas

    Private Sub checkLine(ByVal issueNum As String)

        Dim purlin As BOPurchases.cPurchaseLine = New BOPurchases.cPurchaseLine(_Oasys3DB)
        Dim stkmas As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim isshdr As BOPurchases.cIssueHeader = New BOPurchases.cIssueHeader(_Oasys3DB)
        Dim isslin As BOPurchases.cIssueLine = New BOPurchases.cIssueLine(_Oasys3DB)
        Dim lstIsslin As List(Of BOPurchases.cIssueLine)
        Dim decCalc As Decimal = 0

        decTotalValue = 0

        'find lines to calculate from 
        isslin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, isslin.IssueNumber, issueNum)
        isslin.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        isslin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, isslin.AddByMaintenance, False)
        lstIsslin = isslin.LoadMatches()
        If lstIsslin IsNot Nothing Then
            For Each line As BOPurchases.cIssueLine In lstIsslin
                'does the sku exist in the purchase line 
                purlin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, purlin.SKUNumber, line.SkuNumber.Value)
                purlin.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                purlin.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, purlin.DeleteFlag, False)
                If purlin.LoadCTSMatches().Count > 0 Then
                    'accumilate total 
                    decCalc = Decimal.Round(CDec(purlin.OrderPrice.Value * line.QtyIssued.Value), 2, MidpointRounding.ToEven)
                    decTotalValue += decCalc
                Else
                    'does the sku exist in stock master
                    stkmas.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, stkmas.SkuNumber, line.SkuNumber.Value)
                    If stkmas.LoadMatches().Count > 0 Then
                        If purlin.Delete = False Then
                            decCalc = Decimal.Round(CDec(stkmas.NormalSellPrice.Value * line.QtyIssued.Value), 2, MidpointRounding.ToEven)
                            decTotalValue += decCalc
                        Else
                            strRejectCodeLine = "K"
                            isslin.UpdateRejectCode(issueNum.ToString, line.LineNumber.Value, strRejectCodeLine)
                        End If
                    Else
                        strRejectCodeLine = "B"
                        isslin.UpdateRejectCode(issueNum, line.LineNumber.Value, strRejectCodeLine)
                        If strRejectCodeHeader = String.Empty Then
                            strRejectCodeHeader = "B"
                        End If
                    End If
                End If
            Next line
        End If
    End Sub

    Private Sub frmAutomaticIssueChecking_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        Me.Show()
        Me.Refresh()
        Application.DoEvents()

    End Sub 'CheckLine 

    Private Sub frmAutomaticIssueChecking_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' get command parameters 
        Dim strCmd As String = String.Empty
        Dim strListOfCmd() = {String.Empty}
        Dim intPos As Integer = 0
        Dim blnRunAutomatically As Boolean = False

        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd.Contains("/U=") Then
                intPos = strCmd.IndexOf("/U=") + 3
                intUserId = CInt(Val(strCmd.Substring(intPos, 3)))
            End If
            If strCmd = "y" Or strCmd = "Y" Then
                blnRunAutomatically = True
            End If
        Next

        Me.Show()
        Me.Refresh()

        If blnRunAutomatically Then
            RunCheck()
        Else
            'Run Automatically ...
            If MsgBox("Run Automatic Issue Check (Y/N):", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Automatic Issue Checking") = vbYes Then
                RunCheck()
            End If
        End If

        End

    End Sub
End Class