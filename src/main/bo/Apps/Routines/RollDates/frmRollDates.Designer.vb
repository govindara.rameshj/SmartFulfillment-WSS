﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRollDates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblNextTrardingDay = New System.Windows.Forms.Label
        Me.lblNightRoutineDay = New System.Windows.Forms.Label
        Me.lblLastSystemUpdate = New System.Windows.Forms.Label
        Me.cmdContinue = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblNextTrardingDay
        '
        Me.lblNextTrardingDay.AutoSize = True
        Me.lblNextTrardingDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNextTrardingDay.Location = New System.Drawing.Point(12, 67)
        Me.lblNextTrardingDay.Name = "lblNextTrardingDay"
        Me.lblNextTrardingDay.Size = New System.Drawing.Size(134, 16)
        Me.lblNextTrardingDay.TabIndex = 9
        Me.lblNextTrardingDay.Text = "Next trading day - "
        Me.lblNextTrardingDay.Visible = False
        '
        'lblNightRoutineDay
        '
        Me.lblNightRoutineDay.AutoSize = True
        Me.lblNightRoutineDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNightRoutineDay.Location = New System.Drawing.Point(12, 37)
        Me.lblNightRoutineDay.Name = "lblNightRoutineDay"
        Me.lblNightRoutineDay.Size = New System.Drawing.Size(192, 16)
        Me.lblNightRoutineDay.TabIndex = 8
        Me.lblNightRoutineDay.Text = "Nightly routine date set to: "
        Me.lblNightRoutineDay.Visible = False
        '
        'lblLastSystemUpdate
        '
        Me.lblLastSystemUpdate.AutoSize = True
        Me.lblLastSystemUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastSystemUpdate.Location = New System.Drawing.Point(12, 9)
        Me.lblLastSystemUpdate.Name = "lblLastSystemUpdate"
        Me.lblLastSystemUpdate.Size = New System.Drawing.Size(237, 16)
        Me.lblLastSystemUpdate.TabIndex = 7
        Me.lblLastSystemUpdate.Text = "Last system date update was on: "
        Me.lblLastSystemUpdate.Visible = False
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(147, 101)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(83, 23)
        Me.cmdContinue.TabIndex = 6
        Me.cmdContinue.Text = "&Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        Me.cmdContinue.Visible = False
        '
        'frmRollDates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(394, 147)
        Me.Controls.Add(Me.lblNextTrardingDay)
        Me.Controls.Add(Me.lblNightRoutineDay)
        Me.Controls.Add(Me.lblLastSystemUpdate)
        Me.Controls.Add(Me.cmdContinue)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRollDates"
        Me.Opacity = 0
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Roll Dates"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblNextTrardingDay As System.Windows.Forms.Label
    Friend WithEvents lblNightRoutineDay As System.Windows.Forms.Label
    Friend WithEvents lblLastSystemUpdate As System.Windows.Forms.Label
    Friend WithEvents cmdContinue As System.Windows.Forms.Button

End Class
