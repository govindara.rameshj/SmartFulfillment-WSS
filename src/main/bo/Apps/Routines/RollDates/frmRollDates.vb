﻿Imports System.Windows.Forms
Imports System.IO
Imports Cts.Oasys.Core.System
Imports System.Runtime.InteropServices
Imports System.Text

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author      : Dhanesh Ramachandran
' Date        : 28/06/2011
' Referral No : 567
' Notes       : Modifed to Provide mechanism for Roll Dates program to close automatically after a specified time
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Class frmRollDates

    Private _currentMinutesToShowFor As Long = -1
    Private _message As String
    Private WithEvents tmrCloseMessage As System.Windows.Forms.Timer
    Private WithEvents thrMessageBox As Threading.Thread
    ' constant for destroying the messagebox
    Const WM_NCDESTROY As Integer = &H82 ' 130

    ' For destroying the messagebox
    <DllImport("user32.dll", SetLastError:=True)> _
    Private Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As UInt32, _
                                                                           ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    End Function

    Private Sub RollDates_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim intPos As Integer
        Dim silently As Boolean = Command.Contains("/Y")

        If Command.Contains("/P=',CFC'") Then
            RunRollDates(silently)
        ElseIf Command.Contains("/T=") Then
            intPos = Command.IndexOf("/T=") + 3
            RunRollDates(silently, CInt(Command.Substring(intPos)))
        End If
    End Sub

    Private Sub RunRollDates(ByVal Silently As Boolean, Optional ByVal TimeOutMinutes As Integer = 10)

        Try
            Using sysDates As Dates.Date = Dates.GetDates
                sysDates.RollDates()

                lblLastSystemUpdate.Text += Format(sysDates.Today, "dd/MM/yy")
                lblNightRoutineDay.Text += WeekdayName(sysDates.TodayDayNumber)
                lblNextTrardingDay.Text += Format(sysDates.Tomorrow, "dd/MM/yy") + " " + WeekdayName(sysDates.TomorrowDayNumber)

                SystemOption.UpdateLastReportDate(sysDates.Today)
            End Using

            _message = lblLastSystemUpdate.Text + vbCrLf + vbCrLf + _
                           lblNightRoutineDay.Text + vbCrLf + vbCrLf + _
                           lblNextTrardingDay.Text + vbCrLf + vbCrLf + _
                           "Nightly update of SYSTEM DATE - Continue : "

            If Not Silently Then
                If (TimeOutMinutes <= 0) Then


                    MessageBox.Show(lblLastSystemUpdate.Text + vbCrLf + vbCrLf + _
                       lblNightRoutineDay.Text + vbCrLf + vbCrLf + _
                       lblNextTrardingDay.Text + vbCrLf + vbCrLf + _
                       "Nightly update of SYSTEM DATE - Continue : ", "Roll Dates", MessageBoxButtons.OK)


                    FindForm.Close()
                Else
                    ShowMessage(TimeOutMinutes)
                End If
            Else
                FindForm.Close()
            End If

            'Kill the TASKCOMP file if it exists
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        Catch ex As Exception
            Trace.WriteLine(ex.Message)
            Throw
        End Try
    End Sub
    Private Function ShowMessage(ByVal MinutesToShowFor As Long) As Boolean

        If _currentMinutesToShowFor < 0 Then
            _currentMinutesToShowFor = MinutesToShowFor
            tmrCloseMessage = New System.Windows.Forms.Timer
            With tmrCloseMessage
                .Interval = CInt(MinutesToShowFor * 60000)
                .Enabled = True
            End With

            thrMessageBox = New System.Threading.Thread(AddressOf StartMessageBoxThread)
            With thrMessageBox
                .IsBackground = True
                .Start()
            End With
        End If
    End Function
    Private Sub tmrCloseMessage_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrCloseMessage.Tick

        CloseMessageBox("Roll Dates")
        _currentMinutesToShowFor = -1
    End Sub

    Private Sub StartMessageBoxThread()

        ' standard Messagebox with results 
        Dim result As DialogResult = MessageBox.Show(_message, "Roll Dates", MessageBoxButtons.OK)

        'dispose the timeout timer
        DisposeTimeoutTimer()
        ' dispose current options
        _currentMinutesToShowFor = -1

        CloseMessageBox("Roll Dates")
    End Sub
    ' method close messagebox
    Public Sub CloseMessageBox(ByVal Title As String)

        ' disopose timeout timer
        DisposeTimeoutTimer()
        ' kill window
        ' find window handle
        Dim handle As IntPtr = FindWindow(Nothing, Title)
        ' send destroy message
        SendMessage(handle, WM_NCDESTROY, CType(0I, IntPtr), CType(0I, IntPtr))
        ' kill dialog thread
        ' check whether exists
        If thrMessageBox IsNot Nothing Then
            ' abort the thread
            thrMessageBox.Abort()
            thrMessageBox = Nothing
        End If
        FindForm.Close()
    End Sub
    ' method dispose timeout timer 
    Private Sub DisposeTimeoutTimer()

        If tmrCloseMessage IsNot Nothing Then
            tmrCloseMessage.Dispose()
        End If
        tmrCloseMessage = Nothing
    End Sub
    Private Sub cmdContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdContinue.Click
        Try
            RunRollDates(False)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Cts.Oasys.Core.Tests.TestEnvironmentSetup.SetupIfTestRun()

    End Sub
End Class
