﻿Imports System.IO

Public Class frmStockLogPurge

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Const MODULE_NAME As String = "StockLogPurge"

    Private lngTimer As Long = 0

    Public intUserId As Integer
    Public intWorkstationId As Integer
    Public intSecurityLevel As Integer

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub frmStockLogPurge_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.Show()
        Me.Refresh()
        Application.DoEvents()
    End Sub

    Private Sub frmStockLogPurge_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F10 Then
            End
        End If
    End Sub

    Private Sub frmStockLogPurge_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' get command parameters 
        Dim strCmd As String = String.Empty
        Dim strListOfCmd() = {String.Empty}
        Dim intPos As Integer = 0
        Dim blnCalledFromNight As Boolean = False

        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd.Contains("/U=") Then
                intPos = strCmd.IndexOf("/U=") + 3
                intUserId = CInt(Val(strCmd.Substring(intPos, 3)))
            End If
            If strCmd.Contains("Y") Then
                blnCalledFromNight = True
            End If
        Next

        Me.Show()
        Me.Refresh()

        If blnCalledFromNight Then
            RunStockLogPurge()
        Else
            'Run Automatically ...
            If MsgBox("Delete OLD Daily Stock Movement Records?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Stock Log Purge") = vbYes Then
                RunStockLogPurge()
            End If
        End If

        'Kill the TASKCOMP file if it exists
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        End

    End Sub

    Private Sub RunStockLogPurge()

        Dim boSysDates As BOSystem.cSystemDates = New BOSystem.cSystemDates(_Oasys3DB)
        Dim boretailOptions As BOSystem.cRetailOptions = New BOSystem.cRetailOptions(_Oasys3DB)
        Dim boStkLog As BOStock.cStockLog = New BOStock.cStockLog(_Oasys3DB)
        Dim boLog As BOStock.cStockLog = New BOStock.cStockLog(_Oasys3DB)
        Dim boStock As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim dteEndDate As Date
        Dim intHorizion As Integer
        Dim drRetailOptions As DataRow
        Dim blnErrorFlag As Boolean = False
        Dim intCount As Integer = 0
        Dim intLastPercent As Integer = 0
        Dim intTempPercent As Integer = 0
        Dim dteRecDate As Date
        Dim strOldSku As String = String.Empty
        Dim strSKUNumber As String = String.Empty
        Dim intStockOnHandEnd As Integer = 0
        Dim intStockReturnsEnd As Integer = 0
        Dim intStockMarkDownEnd As Integer = 0
        Dim intStockWriteOffEnd As Integer = 0
        Dim decPriceEnd As Decimal = 0
        Dim intUserId As Integer = 0
        Dim strSql As String = String.Empty
        Dim dsStock As DataSet = Nothing
        Dim dtStock As DataTable = Nothing
        Dim dsStockLog As DataSet = Nothing
        Dim dtStockLog As DataTable = Nothing
        Dim timeToStop As Date = DateAdd("h", 6.0, Now())

        lblInfo.Text = "Get Records from Stock Log"
        FindForm.Refresh()

        ' find the date to delete from 

        'start with today from system date file 
        drRetailOptions = boSysDates.SystemDates
        dteEndDate = CDate(drRetailOptions.Item(boSysDates.Today.ColumnName))

        ' get the number of days to keep records 
        drRetailOptions = boretailOptions.RetailOptions
        intHorizion = CInt(drRetailOptions.Item(boretailOptions.AccoutingPerToKeep.ColumnName))

        ' calculate the date
        dteEndDate = DateAdd(DateInterval.Day, -intHorizion, dteEndDate)

        ' record date 
        dteRecDate = DateAdd(DateInterval.Day, -1, dteEndDate)

        ' now we have a list of records to process delete the records before or equal to horizion date 
        blnErrorFlag = MarkForDeletetion(dteEndDate)

        'get all the records from
        strSql = "SELECT SKUN, ONHA, RETQ, PRIC, MDNQ, WTFQ FROM STKMAS ORDER BY SKUN"
        dsStock = _Oasys3DB.ExecuteSql(strSql)
        dtStock = dsStock.Tables(0)
        Trace.WriteLine("SQL: " & strSql, MODULE_NAME & " RunStockLogPurge")
        Trace.WriteLine("Rows returned: " & dtStock.Rows.Count.ToString, MODULE_NAME & " RunStockLogPurge")
        pbCompleted.Refresh()


        'Do this at the end now to keep data integrity if the user aborts
        lblInfo.Text = "Writing Stock Log records"

        FindForm.Refresh()

        For Each rec As DataRow In dtStock.Rows

            intCount = intCount + 1
            intTempPercent = (intCount * 100) \ dtStock.Rows.Count
            If intTempPercent <> intLastPercent Then
                intLastPercent = intTempPercent
                pbCompleted.Increment(intLastPercent - pbCompleted.Value)
                pbCompleted.Refresh()
            End If

            Application.DoEvents()

            strSKUNumber = rec.Item("SKUN").ToString

            strSql = "SELECT SKUN, ESTK, ERET, EMDN, EWTF, EPRI, DAYN FROM STKLOG WHERE SKUN ='" & strSKUNumber & "' and DATE1 <= '" & Format(dteEndDate, "yyyy-MM-dd") & "' ORDER BY SKUN ASC , DATE1 DESC , TIME DESC "
            dsStockLog = _Oasys3DB.ExecuteSql(strSql)
            dtStockLog = dsStockLog.Tables(0)
            If dtStockLog.Rows.Count > 0 Then
                intStockOnHandEnd = CInt(dtStockLog.Rows(0).Item("ESTK").ToString)
                intStockReturnsEnd = CInt(dtStockLog.Rows(0).Item("ERET").ToString)
                intStockMarkDownEnd = CInt(dtStockLog.Rows(0).Item("EMDN").ToString)
                intStockWriteOffEnd = CInt(dtStockLog.Rows(0).Item("EWTF").ToString)
                decPriceEnd = CDec(dtStockLog.Rows(0).Item("EPRI").ToString)
            Else
                intStockOnHandEnd = CInt(rec.Item("ONHA").ToString)
                intStockReturnsEnd = CInt(rec.Item("RETQ").ToString)
                intStockMarkDownEnd = CInt(rec.Item("MDNQ").ToString)
                intStockWriteOffEnd = CInt(rec.Item("WTFQ").ToString)
                decPriceEnd = CDec(rec.Item("PRIC").ToString)
            End If

            lblInfo.Text = "Processing Stock Item - " & strSKUNumber & "."
            lblInfo.Refresh()
            Application.DoEvents()


            ' write 99
            boStkLog.InsertStart99(dteRecDate, "StockLogPurge " & Format(Date.Now, "dd/MM/yyyy"), intUserId, strSKUNumber, intStockOnHandEnd, _
                               intStockOnHandEnd, intStockReturnsEnd, intStockReturnsEnd, _
                               intStockMarkDownEnd, intStockMarkDownEnd, intStockWriteOffEnd, _
                               intStockWriteOffEnd, decPriceEnd, decPriceEnd)
            Me.Refresh()

            If intCount Mod 1000 = 0 Then
                'Test every 1000 records if its time to stop
                If Now() > timeToStop Then
                    Trace.WriteLine("Times up, process stopped after 6 hours.", MODULE_NAME & " RunStockLogPurge")
                    End
                End If
            End If

        Next rec

        FindForm.Close()

        blnErrorFlag = DeleteMarkedRecords()

    End Sub

    Private Function MarkForDeletetion(ByVal dteEndDate As Date) As Boolean

        Dim strSql As String = "UPDATE STKLOG set DAYN = -1 WHERE DATE1 <= '" & Format(dteEndDate, "yyyy-MM-dd") & "'"
        Dim ds As DataSet = Nothing
        Dim dt As DataTable = Nothing

        ds = _Oasys3DB.ExecuteSql(strSql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function DeleteMarkedRecords() As Boolean

        Dim strSql As String = "DELETE FROM STKLOG WHERE DAYN = -1 "
        Dim ds As DataSet = Nothing
        Dim dt As DataTable = Nothing

        ds = _Oasys3DB.ExecuteSql(strSql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub RunStockLogPurgeOld()

        Dim boSysDates As BOSystem.cSystemDates = New BOSystem.cSystemDates(_Oasys3DB)
        Dim boretailOptions As BOSystem.cRetailOptions = New BOSystem.cRetailOptions(_Oasys3DB)
        Dim boStkLog As BOStock.cStockLog = New BOStock.cStockLog(_Oasys3DB)
        Dim boLog As BOStock.cStockLog = New BOStock.cStockLog(_Oasys3DB)
        Dim lstLOG As List(Of BOStock.cStockLog)
        Dim boStock As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim lstStock As List(Of BOStock.cStock)
        Dim dteEndDate As Date
        Dim intHorizion As Integer
        Dim drRetailOptions As DataRow
        Dim blnErrorFlag As Boolean = False
        Dim intCount As Integer = 0
        Dim intLastPercent As Integer = 0
        Dim intTempPercent As Integer = 0
        Dim dteRecDate As Date
        Dim strOldSku As String = String.Empty
        Dim strSKUNumber As String = String.Empty
        Dim intStockOnHandEnd As Integer = 0
        Dim intStockReturnsEnd As Integer = 0
        Dim intStockMarkDownEnd As Integer = 0
        Dim intStockWriteOffEnd As Integer = 0
        Dim decPriceEnd As Decimal = 0
        Dim intUserId As Integer = 0
        Dim strSql As String = String.Empty
        Dim dsStockLog As DataSet = Nothing
        Dim dtStockLog As DataTable = Nothing
        Dim timeToStop As Date = DateAdd("h", 6.0, Now())

        lblInfo.Text = "Get Records from Stock Log"
        FindForm.Refresh()

        ' find the date to delete from 

        'start with today from system date file 
        drRetailOptions = boSysDates.SystemDates
        dteEndDate = CDate(drRetailOptions.Item(boSysDates.Today.ColumnName))

        ' get the number of days to keep records 
        drRetailOptions = boretailOptions.RetailOptions
        intHorizion = CInt(drRetailOptions.Item(boretailOptions.AccoutingPerToKeep.ColumnName))

        ' calculate the date
        dteEndDate = DateAdd(DateInterval.Day, -intHorizion, dteEndDate)

        ' record date 
        dteRecDate = DateAdd(DateInterval.Day, -1, dteEndDate)

        'get all the records from
        strSql = "SELECT SKUN, ESTK, ERET, EMDN, EWTF, EPRI, DAYN FROM STKLOG WHERE DATE1 <= '" & Format(dteEndDate, "yyyy-MM-dd") & "' ORDER BY SKUN ASC , DATE1 DESC , TIME DESC "
        dsStockLog = _Oasys3DB.ExecuteSql(strSql)
        dtStockLog = dsStockLog.Tables(0)
        Trace.WriteLine("SQL: " & strSql, MODULE_NAME & " RunStockLogPurge")
        Trace.WriteLine("Rows returned: " & dtStockLog.Rows.Count.ToString, MODULE_NAME & " RunStockLogPurge")
        pbCompleted.Refresh()

        ' now we have a list of records to process delete the records before or equal to horizion date 
        'blnErrorFlag = boStkLog.Purge(dteEndDate)
        'Do this at the end now to keep data integrity if the user aborts
        lblInfo.Text = "Writing Stock Log records"

        FindForm.Refresh()

        For Each rec As DataRow In dtStockLog.Rows

            intCount = intCount + 1
            intTempPercent = (intCount * 100) \ dtStockLog.Rows.Count
            If intTempPercent <> intLastPercent Then
                intLastPercent = intTempPercent
                pbCompleted.Increment(intLastPercent - pbCompleted.Value)
                pbCompleted.Refresh()
            End If

            Application.DoEvents()

            strSKUNumber = rec.Item("SKUN").ToString
            intStockOnHandEnd = CInt(rec.Item("ESTK").ToString)
            intStockReturnsEnd = CInt(rec.Item("ERET").ToString)
            intStockMarkDownEnd = CInt(rec.Item("EMDN").ToString)
            intStockWriteOffEnd = CInt(rec.Item("EWTF").ToString)
            decPriceEnd = CDec(rec.Item("EPRI").ToString)

            lblInfo.Text = "Processing Stock Item - " & strSKUNumber & "."
            lblInfo.Refresh()
            Application.DoEvents()

            If strOldSku <> strSKUNumber Then
                'See if there is any further records in stklog after purge
                boLog.AddLoadField(boLog.SkuNumber)
                boLog.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boLog.SkuNumber, strSKUNumber)
                boLog.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                boLog.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, boLog.LogDayNumber, CInt(rec.Item("DAYN").ToString))
                lstLOG = boLog.LoadMatches
                If lstLOG.Count = 0 Then
                    boStock.AddLoadField(boStock.SkuNumber)
                    boStock.AddLoadField(boStock.StockOnHand)
                    boStock.AddLoadField(boStock.UnitsInOpenReturns)
                    boStock.AddLoadField(boStock.NormalSellPrice)
                    boStock.AddLoadField(boStock.MarkDownQuantity)
                    boStock.AddLoadField(boStock.WriteOffQuantity)
                    boStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, boStock.SkuNumber, strSKUNumber)
                    boStock.SortBy(boStock.SkuNumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Ascending)
                    lstStock = boStock.LoadMatches

                    'Use Stock Master Value for 99 record.
                    If lstStock.Count > 0 Then
                        intStockOnHandEnd = lstStock(0).StockOnHand.Value
                        intStockReturnsEnd = lstStock(0).UnitsInOpenReturns.Value
                        intStockMarkDownEnd = lstStock(0).MarkDownQuantity.Value
                        intStockWriteOffEnd = lstStock(0).WriteOffQuantity.Value
                        decPriceEnd = lstStock(0).NormalSellPrice.Value
                    End If

                End If

                ' write 99
                boStkLog.InsertStart99(dteRecDate, "StockLogPurge " & Format(Date.Now, "dd/MM/yyyy"), intUserId, strSKUNumber, intStockOnHandEnd, _
                                   intStockOnHandEnd, intStockReturnsEnd, intStockReturnsEnd, _
                                   intStockMarkDownEnd, intStockMarkDownEnd, intStockWriteOffEnd, _
                                   intStockWriteOffEnd, decPriceEnd, decPriceEnd)
                Me.Refresh()
                strOldSku = strSKUNumber

            End If

            If intCount Mod 1000 = 0 Then
                'Test every 1000 records if its time to stop
                If Now() > timeToStop Then
                    Trace.WriteLine("Times up, process stopped after 6 hours.", MODULE_NAME & " RunStockLogPurge")
                    End
                End If
            End If

        Next rec

        FindForm.Close()

        blnErrorFlag = boStkLog.Purge(dteEndDate)

    End Sub

End Class