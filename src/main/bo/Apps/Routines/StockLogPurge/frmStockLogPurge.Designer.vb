﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockLogPurge
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblPercentageComplete = New System.Windows.Forms.Label
        Me.pbCompleted = New System.Windows.Forms.ProgressBar
        Me.btnExit = New System.Windows.Forms.Button
        Me.lblInfo = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblPercentageComplete
        '
        Me.lblPercentageComplete.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercentageComplete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentageComplete.Location = New System.Drawing.Point(14, 43)
        Me.lblPercentageComplete.Name = "lblPercentageComplete"
        Me.lblPercentageComplete.Size = New System.Drawing.Size(394, 13)
        Me.lblPercentageComplete.TabIndex = 3
        Me.lblPercentageComplete.Text = "Deleting OLD Stock Movements Audit Files"
        Me.lblPercentageComplete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pbCompleted
        '
        Me.pbCompleted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbCompleted.Location = New System.Drawing.Point(17, 59)
        Me.pbCompleted.Name = "pbCompleted"
        Me.pbCompleted.Size = New System.Drawing.Size(394, 32)
        Me.pbCompleted.Step = 1
        Me.pbCompleted.TabIndex = 2
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(17, 154)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 6
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblInfo
        '
        Me.lblInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.Location = New System.Drawing.Point(14, 106)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(397, 21)
        Me.lblInfo.TabIndex = 7
        Me.lblInfo.Text = "Information"
        '
        'frmStockLogPurge
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 205)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblPercentageComplete)
        Me.Controls.Add(Me.pbCompleted)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmStockLogPurge"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Log Purge"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblPercentageComplete As System.Windows.Forms.Label
    Friend WithEvents pbCompleted As System.Windows.Forms.ProgressBar
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblInfo As System.Windows.Forms.Label
End Class
