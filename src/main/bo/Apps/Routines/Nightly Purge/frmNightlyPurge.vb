﻿Imports System.IO
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core.Tests

Public Class frmNightlyPurge

    Private _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Const MODULE_NAME As String = "LabelRequestPurge"

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub
    Private Sub frmNightlyPurge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' get command parameters 
        Dim strCmd As String = String.Empty
        Dim strListOfCmd() = {String.Empty}
        Dim intPos As Integer = 0
        Dim blnCalledFromNight As Boolean = False

        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd.Contains("Y") Then
                blnCalledFromNight = True
            End If
        Next

        Me.Show()
        Me.Refresh()
        If blnCalledFromNight Then
            RunPrintLabelPurge()
        Else
            'Run Automatically ...
            If MsgBox("Purge Records ?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Nightly Purge") = vbYes Then
                RunPrintLabelPurge()
            End If
        End If

        'Kill the TASKCOMP file if it exists
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        End
    End Sub

    Private Sub frmNightlyPurge_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Me.Show()
        Me.Refresh()
        Application.DoEvents()
    End Sub

    Private Sub RunPrintLabelPurge()

        Try
            lblInformation.Text = "Getting Records from TMPFIL to Purge"
            pbCompleted.Value = 50
            lblInformation.Text = String.Concat(_Oasys3DB.ExecuteSql("SELECT COUNT(*) FROM TMPFIL WHERE FKEY LIKE '%PCPLRQ%'").Tables(0).Rows(0)(0).ToString(), " Records have been sucessfully Purged")
            _Oasys3DB.ExecuteSql("DELETE FROM TMPFIL WHERE FKEY LIKE '%PCPLRQ%'")

            ExecuteSql(My.Resources.ProcISUHDR)
            ExecuteSql(My.Resources.ProcSTKMAS)
            pbCompleted.Value = 100
            Me.Refresh()
            FindForm.Close()
        Catch ex As Exception
            ex.Message.ToString()
        End Try
    End Sub

    Private Sub frmNightlyPurge_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F10 Then
            End
        End If
    End Sub

    Private Sub ExecuteSql(ByVal storedProc As String)
        Using con As New Connection
            con.StartTransaction()
            Try
                Using com As Command = con.NewCommand(storedProc)
                    com.ExecuteNonQuery()
                    con.CommitTransaction()
                End Using
            Catch ex As Exception
                con.RollbackTransaction()
            End Try
        End Using
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        TestEnvironmentSetup.SetupIfTestRun()
        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    End Sub
End Class
