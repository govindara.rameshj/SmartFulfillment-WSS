﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PeriodYearEndUpdate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbSelection = New System.Windows.Forms.GroupBox
        Me.btnRun = New System.Windows.Forms.Button
        Me.txtRelatedItems = New System.Windows.Forms.TextBox
        Me.lblRelatedItems = New System.Windows.Forms.Label
        Me.txtStock = New System.Windows.Forms.TextBox
        Me.lblStock = New System.Windows.Forms.Label
        Me.txtSupplier = New System.Windows.Forms.TextBox
        Me.lblSupplier = New System.Windows.Forms.Label
        Me.TxtUpdate = New System.Windows.Forms.TextBox
        Me.lblUpdate = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.lblTask = New System.Windows.Forms.Label
        Me.gbSelection.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbSelection
        '
        Me.gbSelection.Controls.Add(Me.btnRun)
        Me.gbSelection.Controls.Add(Me.txtRelatedItems)
        Me.gbSelection.Controls.Add(Me.lblRelatedItems)
        Me.gbSelection.Controls.Add(Me.txtStock)
        Me.gbSelection.Controls.Add(Me.lblStock)
        Me.gbSelection.Controls.Add(Me.txtSupplier)
        Me.gbSelection.Controls.Add(Me.lblSupplier)
        Me.gbSelection.Location = New System.Drawing.Point(44, 66)
        Me.gbSelection.Name = "gbSelection"
        Me.gbSelection.Size = New System.Drawing.Size(473, 169)
        Me.gbSelection.TabIndex = 5
        Me.gbSelection.TabStop = False
        Me.gbSelection.Visible = False
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(195, 132)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 5
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'txtRelatedItems
        '
        Me.txtRelatedItems.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRelatedItems.Location = New System.Drawing.Point(368, 76)
        Me.txtRelatedItems.MaxLength = 1
        Me.txtRelatedItems.Name = "txtRelatedItems"
        Me.txtRelatedItems.Size = New System.Drawing.Size(24, 20)
        Me.txtRelatedItems.TabIndex = 3
        '
        'lblRelatedItems
        '
        Me.lblRelatedItems.AutoSize = True
        Me.lblRelatedItems.Location = New System.Drawing.Point(96, 83)
        Me.lblRelatedItems.Name = "lblRelatedItems"
        Me.lblRelatedItems.Size = New System.Drawing.Size(139, 13)
        Me.lblRelatedItems.TabIndex = 4
        Me.lblRelatedItems.Text = "Update Related Items (Y/N)"
        '
        'txtStock
        '
        Me.txtStock.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStock.Location = New System.Drawing.Point(368, 50)
        Me.txtStock.MaxLength = 1
        Me.txtStock.Name = "txtStock"
        Me.txtStock.Size = New System.Drawing.Size(24, 20)
        Me.txtStock.TabIndex = 2
        '
        'lblStock
        '
        Me.lblStock.AutoSize = True
        Me.lblStock.Location = New System.Drawing.Point(96, 57)
        Me.lblStock.Name = "lblStock"
        Me.lblStock.Size = New System.Drawing.Size(174, 13)
        Me.lblStock.TabIndex = 2
        Me.lblStock.Text = "Update the Stock Master File (Y/N)"
        '
        'txtSupplier
        '
        Me.txtSupplier.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSupplier.Location = New System.Drawing.Point(368, 24)
        Me.txtSupplier.MaxLength = 1
        Me.txtSupplier.Name = "txtSupplier"
        Me.txtSupplier.Size = New System.Drawing.Size(24, 20)
        Me.txtSupplier.TabIndex = 1
        '
        'lblSupplier
        '
        Me.lblSupplier.AutoSize = True
        Me.lblSupplier.Location = New System.Drawing.Point(96, 31)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(146, 13)
        Me.lblSupplier.TabIndex = 0
        Me.lblSupplier.Text = "Update the Supplier file (Y/N)"
        '
        'TxtUpdate
        '
        Me.TxtUpdate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtUpdate.Location = New System.Drawing.Point(410, 20)
        Me.TxtUpdate.MaxLength = 1
        Me.TxtUpdate.Name = "TxtUpdate"
        Me.TxtUpdate.Size = New System.Drawing.Size(24, 20)
        Me.TxtUpdate.TabIndex = 4
        '
        'lblUpdate
        '
        Me.lblUpdate.AutoSize = True
        Me.lblUpdate.Location = New System.Drawing.Point(138, 20)
        Me.lblUpdate.Name = "lblUpdate"
        Me.lblUpdate.Size = New System.Drawing.Size(201, 13)
        Me.lblUpdate.TabIndex = 3
        Me.lblUpdate.Text = "Period system file update - Continue(Y/N)"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(-1, 266)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(558, 14)
        Me.ProgressBar1.TabIndex = 6
        '
        'lblTask
        '
        Me.lblTask.AutoSize = True
        Me.lblTask.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTask.Location = New System.Drawing.Point(41, 243)
        Me.lblTask.Name = "lblTask"
        Me.lblTask.Size = New System.Drawing.Size(111, 15)
        Me.lblTask.TabIndex = 7
        Me.lblTask.Text = "Task Running ..."
        '
        'PeriodYearEndUpdate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(559, 279)
        Me.Controls.Add(Me.lblTask)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.gbSelection)
        Me.Controls.Add(Me.TxtUpdate)
        Me.Controls.Add(Me.lblUpdate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PeriodYearEndUpdate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Period Year End Update"
        Me.gbSelection.ResumeLayout(False)
        Me.gbSelection.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbSelection As System.Windows.Forms.GroupBox
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents txtRelatedItems As System.Windows.Forms.TextBox
    Friend WithEvents lblRelatedItems As System.Windows.Forms.Label
    Friend WithEvents txtStock As System.Windows.Forms.TextBox
    Friend WithEvents lblStock As System.Windows.Forms.Label
    Friend WithEvents txtSupplier As System.Windows.Forms.TextBox
    Friend WithEvents lblSupplier As System.Windows.Forms.Label
    Friend WithEvents TxtUpdate As System.Windows.Forms.TextBox
    Friend WithEvents lblUpdate As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblTask As System.Windows.Forms.Label
End Class
