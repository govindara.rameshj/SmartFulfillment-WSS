﻿Imports System.Windows.Forms
Imports System.IO

Public Class PeriodYearEndUpdate

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _blnBatchRun As Boolean = False
    Private _intUserId As Integer = 0

    Private Sub TxtUpdate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtUpdate.KeyDown

        If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Then
            'use this character 
        Else
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub TxtUpdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtUpdate.TextChanged
        If _blnBatchRun = False Then
            If TxtUpdate.Text = "Y" Then
                gbSelection.Enabled = True
                gbSelection.Visible = True
                txtSupplier.Focus()
            Else
                FindForm.Close()
            End If
        End If
    End Sub

    Private Sub TxtSupplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupplier.KeyDown

        If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 13 Then
            'use this character
            If e.KeyValue = 13 Then
                txtStock.Focus()
            End If
        Else
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub TxtStock_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtStock.KeyDown
        If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 13 Then
            'use this character
            If e.KeyValue = 13 Then
                txtRelatedItems.Focus()
            End If
        Else
            e.SuppressKeyPress = True
        End If
    End Sub

    'Private Sub TxtRelatedItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRelatedItems.KeyDown
    '    If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 13 Then
    '        'use this character 
    '        If e.KeyValue = 13 Then
    '            txtStockAdjustments.Focus()
    '        End If
    '    Else
    '        e.SuppressKeyPress = True
    '    End If
    'End Sub

    Private Sub TxtStockAdjustments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 13 Then
            'use this character 
            If e.KeyValue = 13 Then
                btnRun.Focus()
            End If
        Else
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click
        Me.Cursor = Cursors.WaitCursor
        If txtRelatedItems.Text = "N" And txtStock.Text = "N" And txtSupplier.Text = "N" Then
            'nothing to be done therefore exit 
            'Kill the TASKCOMP file if it exists
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

            FindForm.Close()
        Else
            RunUpdate(txtRelatedItems.Text, txtStock.Text, txtSupplier.Text)
        End If
 
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub RunUpdate(ByVal RelatedItems As String, ByVal Stock As String, ByVal Supplier As String)

        Dim systemDates As New BOSystem.cSystemDates(_Oasys3DB)
        Dim boSuppliers As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim boStock As New BOStock.cStock(_Oasys3DB)
        Dim boStockHistory As New BOStock.cStockHistory(_Oasys3DB)
        Dim boRelatedItems As New BOStock.cRelatedItems(_Oasys3DB)
        Dim boStockAdjust As New BOStock.cStockAdjust(_Oasys3DB)
        Dim drSystemDates As DataRow
        Dim blnYearEnd As Boolean = False
        Dim dteDeletionHorizion As Date
        Dim blnError As Boolean = False

        ProgressBar1.Maximum = 4
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True

        lblTask.Text = "Processing Dates."
        lblTask.Refresh()
        Application.DoEvents()

        ' get system dates

        drSystemDates = systemDates.SystemDates
        If drSystemDates IsNot Nothing Then

            ' is this year end 
            If CBool(drSystemDates.Item(systemDates.CurrentEndIsYearEnd.ColumnName)) Then
                blnYearEnd = True
            Else
                blnYearEnd = False
            End If

            ' calculate deletion horizion
            dteDeletionHorizion = CDate(drSystemDates.Item(systemDates.Today.ColumnName))
            dteDeletionHorizion = DateAdd(DateInterval.Day, -366, dteDeletionHorizion)

            ProgressBar1.Value += 1
            ProgressBar1.Refresh()
            Application.DoEvents()

            _Oasys3DB.BeginTransaction()

            ' process suppliers 
            If Supplier = "Y" Then

                lblTask.Text = "Processing Suppliers."
                lblTask.Refresh()
                Application.DoEvents()

                blnError = boSuppliers.ProcessPeriodEnd()
                If blnError = False And blnYearEnd = True Then
                    blnError = boSuppliers.ProcessYearEnd()
                End If
            End If

            ProgressBar1.Value += 1
            ProgressBar1.Refresh()
            Application.DoEvents()

            ' only continue if there are no errors process stock 
            If blnError = False And Stock = "Y" Then

                lblTask.Text = "Processing Stock."
                lblTask.Refresh()
                Application.DoEvents()

                blnError = boStock.ProcessPeriodEnd()
                If blnError = False Then
                    blnError = boStockHistory.ProcessPeriodEnd()
                End If
                'continue if there are no errors and year end do yearend processing  
                If blnError = False And blnYearEnd = True Then
                    blnError = boStock.ProcessYearEnd()
                End If
            End If

            ProgressBar1.Value += 1
            ProgressBar1.Refresh()
            Application.DoEvents()

            ' only continue if there are no errors  then process related  
            If blnError = False And RelatedItems = "Y" Then
                lblTask.Text = "Processing Related Items."
                lblTask.Refresh()
                Application.DoEvents()

                blnError = boRelatedItems.ProcessPeriodEnd()
                'continue if there are no errors and year end do yearend processing  
                If blnError = False And blnYearEnd = True Then
                    blnError = boRelatedItems.ProcessYearEnd()
                End If
            End If

            ' if there are no errors ' process stockAdjustments
            'If blnError = False And StockAjustments = "Y" Then '
            '    blnError = boStockAdjust.DeleteOldStockAdj(dteDeletionHorizion)
            'End If
            'only commit if there are no errors 
            If blnError = False Then
                _Oasys3DB.CommitTransaction()
            Else
                _Oasys3DB.RollBackTransaction()
            End If
        End If

        lblTask.Text = "Finished."
        lblTask.Refresh()
        Application.DoEvents()

        ProgressBar1.Value = 4
        ProgressBar1.Refresh()
        Application.DoEvents()

        'Kill the TASKCOMP file if it exists
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

        FindForm.Close()

    End Sub

    Private Sub PeriodYearEndUpdate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strCmd As String = String.Empty
        Dim strListOfCmd() = {String.Empty}
        Dim intPos As Integer = 0

        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd.Contains("/U=") Then
                intPos = strCmd.IndexOf("/U=") + 3
                _intUserId = CInt(Val(strCmd.Substring(intPos, 3)))
            End If
            If strCmd = "Y" Or strCmd = "y" Then
                _blnBatchRun = True
            End If
        Next

        Me.Show()
        Me.Refresh()

        If _blnBatchRun = True Then

            TxtUpdate.Text = "Y"

            txtSupplier.Text = "Y"

            txtStock.Text = "Y"

            txtRelatedItems.Text = "Y"

            gbSelection.Visible = True

            btnRun.Visible = False

            TxtUpdate.Focus()

            RunUpdate(txtRelatedItems.Text, txtStock.Text, txtSupplier.Text)

        End If

        TxtUpdate.Focus()

    End Sub

End Class