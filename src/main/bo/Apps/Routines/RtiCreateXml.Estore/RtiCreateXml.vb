﻿''' <summary>
''' Allows user to create xml files for tables in RtiTables for transmission by RTI. 
''' Can be run manually or as an overnight process.
''' </summary>
''' <remarks>
''' ================================================================================================
''' Command Line Parameters: OVERNIGHT       - runs process from today to sysdat.todt
''' Command Line Parameters: 'date'          - runs process from 'date' to sysdat.todt
''' Command Line Parameters: 'date1','date2' - runs process from 'date1' to 'date2'
''' ================================================================================================
''' $Archive:  $
''' $Author:  $ 
''' $Date:  $ 
''' $Revision:  $
''' 
''' 
''' Date        Version     Author      Comments 
''' ==========  ==========  ==========  ==========
''' 20/02/2009  1.0.0.0     Darryl      Initial creation
''' 09/03/2009  2.0.0.0     Darryl      Allows entry of second date for historical data
'''  
''' 
''' 
''' </remarks>
Public Class RtiCreateXml
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private WithEvents _RtiHeader As BORti.cRtiHeader

    Sub New()
        InitializeComponent()

        Trace.WriteLine(My.Resources.AppStart, Name)

        'set max value of dtp
        Dim sysDat As New BOSystem.cSystemDates(_Oasys3DB)
        sysDat.Load()
        dtpStart.MaxDate = sysDat.Today.Value
        dtpEnd.MaxDate = sysDat.Today.Value
        dtpEnd.Value = sysDat.Today.Value

        'see if any command line args - if date then use that as startdate
        If My.Application.CommandLineArgs.Count > 0 Then
            Select Case My.Application.CommandLineArgs.Count
                Case 1
                    Dim arg As Date = Nothing
                    If Date.TryParse(My.Application.CommandLineArgs(0), arg) Then
                        DoProcessing(arg)
                    Else
                        DoProcessing()
                    End If
                Case 2
                    Dim arg1 As Date = Nothing
                    Dim arg2 As Date = Nothing
                    If Date.TryParse(My.Application.CommandLineArgs(0), arg1) Then
                        If Date.TryParse(My.Application.CommandLineArgs(1), arg2) Then
                            DoProcessing(arg1, arg2)
                        Else
                            DoProcessing(arg1)
                        End If
                    Else
                        DoProcessing()
                    End If
            End Select

            End
        End If

    End Sub

    Private Sub RtiCreateXml_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Trace.WriteLine(My.Resources.AppExit, Name)
    End Sub

    Private Sub RtiCreateXml_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnProcess.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub DoProcessing(Optional ByVal startDate As Date = Nothing, Optional ByVal endDate As Date = Nothing)

        Try
            Cursor = Cursors.WaitCursor

            If startDate = Nothing Then startDate = dtpStart.Value.Date
            If endDate = Nothing Then endDate = dtpEnd.Value.Date

            'get store id and xml location
            UpdateProcess(My.Resources.GetStoreID)
            Dim retOpt As New BOSystem.cRetailOptions(_Oasys3DB)
            retOpt.LoadRetailOptions()
            Dim storeID As String = retOpt.Store.Value
            Dim XmlLocation = ConfigurationManager.AppSettings("XmlLocation")


            'get all tables to create xml from
            UpdateProcess(My.Resources.GetRtiTables)
            _RtiHeader = New BORti.cRtiHeader(_Oasys3DB)
            _RtiHeader.LoadActiveRtiTables()

            'reset progress bar
            pgb.Value = 0
            pgb.Visible = True


            'create xmls for all tables
            For Each table As BORti.cRtiHeader In _RtiHeader.RtiHeaders
                pgb.Value = CInt(_RtiHeader.RtiHeaders.IndexOf(table) / _RtiHeader.RtiHeaders.Count * 100)
                Refresh()
                table.CreateXml(startDate, endDate, storeID, XmlLocation)
            Next

            'reset all
            pgb.Value = 0
            pgb.Visible = False
            UpdateProcess(My.Resources.ProcessComplete)

        Catch ex As Exception
            lbl.Text = ex.Message
            Trace.WriteLine(ex.Message, Name)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    ''' <summary>
    ''' Displays given message on the form
    ''' </summary>
    ''' <param name="Message"></param>
    ''' <remarks></remarks>
    Private Sub UpdateProcess(ByVal Message As String) Handles _RtiHeader.UpdateProcess

        lbl.Text = Message
        Refresh()

    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        DoProcessing()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

End Class