﻿Imports Cts.Oasys.Core.Tests

''' <summary>
''' Formerly PTBUPD - Daily Flash Totals Creation
''' </summary>
''' <remarks></remarks>
Public Class frmNightlyRetailUpdates : Implements IProgressCallback

    Private Sub frmNightlyRetailUpdates_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Show()
        Refresh()
        Application.DoEvents()
    End Sub

    Private Sub frmNightlyRetailUpdates_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim blnRunAutomatically = False

        'get command parameters 
        Dim strCmd As String
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd = "Y" Or strCmd = "y" Then
                blnRunAutomatically = True
            End If
        Next

        Show()
        Refresh()

        If blnRunAutomatically = True Then
            RunNightlyRetailUpdates()
        Else
            'Run Automatically ...
            If MsgBox("Update Sales Statistics (Y/N):", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Nightly Retail Update") = vbYes Then
                RunNightlyRetailUpdates()
            End If
        End If
        End

    End Sub

    Private Sub RunNightlyRetailUpdates()
        Try
            Dim nightlyRetailUpdateLogic As New NightlyRetailUpdateLogic(Me)
            Cursor = Cursors.WaitCursor
            Refresh()
            nightlyRetailUpdateLogic.RunNightlyRetailUpdates()
            Cursor = Cursors.Default
            btnExit.Enabled = True
            Refresh()
        Catch ex As Exception
            Trace.WriteLine("RunNightlyRetailUpdates: " & ex.Message.ToString)
            End
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        TestEnvironmentSetup.SetupIfTestRun()
    End Sub

    Public Sub SetupProgressBar(ByVal salesCount As Integer) Implements IProgressCallback.SetupProgressBar
        ProgressBar1.Maximum = salesCount
        ProgressBar1.Minimum = 0
        ProgressBar1.Value = 0
        Refresh()
    End Sub

    Public Sub IncrementProgressBar() Implements IProgressCallback.IncrementProgressBar
        ProgressBar1.Value += 1
        ProgressBar1.Refresh()
        Ping()
    End Sub

    Public Sub UpdateStats(tranNumber As String, tranDate As Date, tillId As String, skuNumber As String) Implements IProgressCallback.UpdateStats
        lblTransaction.Text = tranNumber
        lblTranDate.Text = CStr(tranDate)
        lblTranTill.Text = tillId
        lblTranSkun.Text = skuNumber
        Ping()
    End Sub

    Public Sub Ping() Implements IProgressCallback.Ping
        Refresh()
        Application.DoEvents()
    End Sub
End Class
