﻿Public Interface IProgressCallback
    Sub SetupProgressBar(ByVal salesCount As Integer)
    Sub IncrementProgressBar()
    Sub Ping()
    Sub UpdateStats(ByVal tranNumber As String, ByVal tranDate As Date, ByVal tillId As String, ByVal skuNumber As String)
End Interface
