﻿Public Class NightlyRetailUpdateLogic
    Private _oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Private ReadOnly _progressCallback As IProgressCallback
    Private _intNumLinesReversed As Integer = 0


    Public Sub New(ByVal progressCallback As IProgressCallback)
        _oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        _progressCallback = progressCallback
    End Sub

    Public Sub RunNightlyRetailUpdates()

        'check system live date
        Dim sysDates As New BOSystem.cSystemDates(_oasys3DB)
        sysDates.Load()

        Dim dteLiveDate As Date = sysDates.StoreLiveDate.Value
        If dteLiveDate = Nothing Or IsDBNull(dteLiveDate) Then
            Trace.WriteLine("Store Live Date was Invalid. NightlyRetailUpdates")
            Throw New Exception
        End If

        Dim BOSale As New BOSales.cSalesHeader(_oasys3DB)
        BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOSale.DailyUpdateProc, False)
        BOSale.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, BOSale.TransDate, dteLiveDate)

        Dim SalesList As List(Of BOSales.cSalesHeader) = BOSale.LoadMatches

        _progressCallback.SetupProgressBar(SalesList.Count)

        'Read in the Sales that are unprocessed for the current Store Live date
        For Each Sale As BOSales.cSalesHeader In SalesList

            If sysDates.RetryMode.Value = False Then

                _intNumLinesReversed = 0 'Reset number of lines reversed for this transaction

                'No need to do this CW ...
                'If Sale.TrainingMode.Value = False Then ' Were not in training mode then we need to update Cashier Performance 
                '    'Update Cashier Performance Values
                '    UpdateCashierPerformanceSale(Sale)
                'End If

                'IF DLTOTS:TCOD (TransactionCode) = "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"
                Select Case Sale.TransactionCode.Value

                    Case "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"

                        Sale.DailyUpdateProc.Value = True

                    Case Else

                        'Any other transaction code (Refunds) is If the transacion is Parked and were not in retry mode
                        Sale.DailyUpdateProc.Value = True
                        If Sale.TransParked.Value = True Then
                            UpdateParkedRefunds(Sale)
                        End If

                End Select

                UpdateDLTOTS(Sale)

            End If

            _progressCallback.IncrementProgressBar()

        Next Sale

        'Kill the TASKCOMP file.
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
    End Sub

    Private Function UpdateStockLogRecord(ByVal StockItem As BOStock.cStock, ByVal LogItem As BOStock.cStockLog, ByVal StockLog As BOStock.cStockLog, ByVal intCashierID As Integer) As Boolean

        If (StockLog.StockOnHandEnd.Value <> StockItem.StockOnHand.Value) Or _
           (StockLog.StockReturnsEnd.Value <> StockItem.UnitsInOpenReturns.Value) Or _
           (StockLog.StockMarkdownEnd.Value <> StockItem.MarkDownQuantity.Value) Or _
           (StockLog.StockWriteoffEnd.Value <> StockItem.WriteOffQuantity.Value) Then

            LogItem.StockOnHandStart.Value = StockLog.StockOnHandEnd.Value
            LogItem.StockReturnsStart.Value = StockLog.StockReturnsEnd.Value
            LogItem.PriceStart.Value = LogItem.PriceEnd.Value
            LogItem.StockMarkdownStart.Value = LogItem.StockMarkdownEnd.Value
            LogItem.StockWriteoffStart.Value = LogItem.StockWriteoffEnd.Value

            LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
            LogItem.StockReturnsEnd.Value = StockItem.UnitsInOpenReturns.Value
            LogItem.PriceEnd.Value = StockItem.NormalSellPrice.Value
            LogItem.StockMarkdownEnd.Value = StockItem.MarkDownQuantity.Value
            LogItem.StockWriteoffEnd.Value = StockItem.WriteOffQuantity.Value
            LogItem.LogKey.Value = "System Adjustment 91"
            LogItem.LogType.Value = "91"

            'Update the stock log record.
            LogItem.InsertLog(LogItem.LogType.Value, LogItem.LogKey.Value, intCashierID, StockItem.SkuNumber.Value, LogItem.StockOnHandStart.Value, LogItem.StockOnHandEnd.Value, LogItem.StockReturnsStart.Value, LogItem.StockReturnsEnd.Value, LogItem.StockMarkdownStart.Value, LogItem.StockMarkdownEnd.Value, LogItem.StockWriteoffStart.Value, LogItem.StockWriteoffEnd.Value, LogItem.PriceStart.Value, LogItem.PriceEnd.Value)

        End If

        LogItem.SkuNumber.Value = StockItem.SkuNumber.Value
        LogItem.StockOnHandStart.Value = StockItem.StockOnHand.Value
        LogItem.StockReturnsStart.Value = StockItem.UnitsInOpenReturns.Value
        LogItem.PriceStart.Value = StockItem.NormalSellPrice.Value
        LogItem.StockMarkdownStart.Value = StockItem.MarkDownQuantity.Value
        LogItem.StockWriteoffStart.Value = StockItem.WriteOffQuantity.Value

        LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
        LogItem.PriceEnd.Value = StockItem.NormalSellPrice.Value
        LogItem.StockMarkdownEnd.Value = StockItem.MarkDownQuantity.Value
        LogItem.StockWriteoffEnd.Value = StockItem.WriteOffQuantity.Value
        LogItem.LogType.Value = "70"

        '*** End of Stock Log Setup
        Return True

    End Function 'UpdateStockLogRecord

    Private Function GetExtendedPrice(ByVal Line As BOSales.cSalesLine) As Decimal

        Return (Line.ExtendedValue.Value - _
                Line.QtyBreakMarginAmount.Value - _
                Line.DealGroupMarginAmt.Value - _
                Line.MultiBuyMarginAmount.Value - _
                Line.HierarchyMarginAmt.Value - _
                Line.EmpSaleSecMarginAmt.Value)

    End Function 'GetExtendedPrice

    Private Function UpdateParkedRefunds(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_oasys3DB)
        Dim BOPeriod As New BOSystem.cSystemPeriods(_oasys3DB)

        Try

            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransNo, Sale.TransactionNo.Value)
            SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransDate, Sale.TransDate.Value)
            SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TillID, Sale.TillID.Value)
            Dim SaleList As List(Of BOSales.cSalesLine) = SaleLine.LoadMatches
            Dim intSequenceNum As Integer = 0

            For Each Line As BOSales.cSalesLine In SaleList

                'Ignore Line reversals
                If Line.LineReversed.Value = False Then

                    'If Quantity Sold is less than 0 calculate the adjustment quantity
                    If Line.QuantitySold.Value < 0 Then

                        'Calculate the adjustment
                        Dim lngAdjustmentQuantity = 0 - Line.QuantitySold.Value

                        'If the adjustment quantity is greater than 9999 ignore as probably a dodgy record
                        If lngAdjustmentQuantity < 9999 Then

                            'Amend the STKMAS:ONHA value by the adjustment quantity
                            Dim StockItem As New BOStock.cStock(_oasys3DB)
                            StockItem.AddLoadField(StockItem.SkuNumber)
                            StockItem.AddLoadField(StockItem.StockOnHand)
                            StockItem.AddLoadField(StockItem.NormalSellPrice)
                            StockItem.AddLoadField(StockItem.CostPrice)
                            StockItem.AddLoadField(StockItem.UnitsInOpenReturns)
                            StockItem.AddLoadField(StockItem.MarkDownQuantity)
                            StockItem.AddLoadField(StockItem.WriteOffQuantity)
                            StockItem.AddLoadField(StockItem.CatchAll)
                            StockItem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockItem.SkuNumber, Line.SkuNumber.Value)
                            StockItem.LoadMatches()

                            'Calculate the extended price.
                            Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)

                            Dim BOUser As New BOSecurityProfile.cSystemUsers(_oasys3DB)
                            BOUser.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOUser.EmployeeCode, Sale.CashierID.Value)
                            BOUser.LoadMatches()

                            '*** Add a Stock Adjustment record for Parked refunds. ***
                            Dim StockAdj As New BOStock.cStockAdjust(_oasys3DB)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.SkuNumber, Line.SkuNumber.Value)
                            StockAdj.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.Code, "08")
                            StockAdj.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.DateCreated, CDate(Format(Now, "yyyy-MM-dd")))
                            StockAdj.SortBy(StockAdj.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                            Dim StkAdjList As List(Of BOStock.cStockAdjust) = StockAdj.LoadMatches()
                            If StkAdjList.Count > 0 Then
                                intSequenceNum = CInt(Val(StkAdjList(0).Sequence.Value) + 1)
                            End If

                            'Update stats
                            _progressCallback.UpdateStats(Sale.TransactionNo.Value, Sale.TransDate.Value, Sale.TillID.Value, Line.SkuNumber.Value)

                            StockAdj.SkuNumber.Value = Line.SkuNumber.Value
                            StockAdj.DateCreated.Value = Now.Date
                            StockAdj.Code.Value = "08"
                            StockAdj.Sequence.Value = Format(intSequenceNum, "00")
                            StockAdj.EmployeeID.Value = Sale.CashierID.Value
                            StockAdj.StartStock.Value = StockItem.StockOnHand.Value
                            StockAdj.QtyAdjusted.Value = lngAdjustmentQuantity
                            StockAdj.Price.Value = StockItem.NormalSellPrice.Value
                            StockAdj.Cost.Value = StockItem.CostPrice.Value
                            StockAdj.CodeType.Value = "N"
                            StockAdj.Comment.Value = "Parked Refund Adj."
                            StockAdj.RTI.Value = "N"
                            StockAdj.PeriodID.Value = BOPeriod.GetCurrentPeriodID
                            StockAdj.TransferValue.Value = 0D
                            StockAdj.TransferSKU.Value = "000000"

                            'Now save the StockAdj record away.
                            StockAdj.UpdateDailyFlashTotalsAdjustment()
                            '*** End of Updating the Stock Adjustment ***

                            'Now Setup the Stock Log record ...
                            Dim StockLog As New BOStock.cStockLog(_oasys3DB)

                            StockLog.LogDate.Value = Now.Date
                            StockLog.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), StockLog.LogDate.Value))
                            StockLog.LogDayNumber.Value += 1

                            StockLog.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockLog.SkuNumber, Line.SkuNumber.Value)
                            StockLog.SortBy(StockLog.StockLogID.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                            StockLog.LoadMatches()
                            Dim LogItem As New BOStock.cStockLog(_oasys3DB)

                            'Dim LogItem As BOStock.cStockLog = StockLog.GetLatestLogRecordForPartCode(Line.SkuNumber.ToString).Item(1)
                            UpdateStockLogRecord(StockItem, LogItem, StockLog, CInt(Sale.CashierID.Value))

                            'Now update the STKMAS record with the updated on hand value
                            If StockItem.CatchAll.Value = True Then
                                StockItem.StockOnHand.Value = 0
                                StockItem.CostPrice.Value -= decExtendedPrice
                            Else
                                StockItem.StockOnHand.Value += CInt(lngAdjustmentQuantity)
                            End If

                            'StockItem.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
                            Dim blnResult = StockItem.UpdateDailyFlashTotalsStockFile()
                            LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
                            LogItem.LogKey.Value = Sale.TransDate.Value & "  " & Sale.TillID.Value & "  " & Sale.TransactionNo.Value & "      " & Line.SequenceNo.Value.ToString '& " " & Sale.CashierID.Value

                            'Update the stock log record.
                            StockLog.InsertLog(LogItem.LogType.Value, LogItem.LogKey.Value, CInt(Sale.CashierID.Value), Line.SkuNumber.Value, LogItem.StockOnHandStart.Value, LogItem.StockOnHandEnd.Value, LogItem.StockReturnsStart.Value, LogItem.StockReturnsEnd.Value, LogItem.StockMarkdownStart.Value, LogItem.StockMarkdownEnd.Value, LogItem.StockWriteoffStart.Value, LogItem.StockWriteoffEnd.Value, LogItem.PriceStart.Value, LogItem.PriceEnd.Value)

                        End If

                    End If

                End If

            Next Line

            Return True

        Catch ex As Exception
            Trace.WriteLine("Error in UpdateParkedRefunds. " & ex.Message.ToString)
            Return False
        End Try

    End Function 'UpdateParkedRefunds

    Private Function UpdateDLTOTS(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_oasys3DB)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransNo, Sale.TransactionNo.Value)
        SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransDate, Sale.TransDate.Value)
        SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TillID, Sale.TillID.Value)
        Dim SaleList As List(Of BOSales.cSalesLine) = SaleLine.LoadMatches

        For Each Line As BOSales.cSalesLine In SaleList

            'Ignore Line reversals
            If Line.LineReversed.Value = False Then

                'Update stats
                If Sale.Voided.Value = False And Sale.TrainingMode.Value = False Then

                    _progressCallback.UpdateStats(Sale.TransactionNo.Value, Sale.TransDate.Value, Sale.TillID.Value, Line.SkuNumber.Value)

                    If Line.SkuNumber.Value <> "" Then
                        'Calculate the extended price.
                        Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)
                        UpdateStockMasterStatistics(Line, decExtendedPrice, CInt(Sale.CashierID.Value))
                    End If

                End If

            Else
                _intNumLinesReversed += 1
            End If

        Next Line

        Sale.UpdateProcessedFlag(Sale.DailyUpdateProc.Value)
        Return True

    End Function

    Private Function UpdateStockMasterStatistics(ByVal Line As BOSales.cSalesLine, ByVal decExtendedPrice As Decimal, ByVal intUserId As Integer) As Boolean

        Dim StockItem As New BOStock.cStock(_oasys3DB)
        StockItem.AddLoadField(StockItem.SkuNumber)
        StockItem.AddLoadField(StockItem.UnitsSoldCurWkPlus1)
        StockItem.AddLoadField(StockItem.UnitsSoldYesterday)
        StockItem.AddLoadField(StockItem.UnitsSoldThisWeek)
        StockItem.AddLoadField(StockItem.UnitsSoldThisPeriod)
        StockItem.AddLoadField(StockItem.UnitsSoldThisYear)
        StockItem.AddLoadField(StockItem.LastSold)
        StockItem.AddLoadField(StockItem.ActivityToday)
        StockItem.AddLoadField(StockItem.NormalSellPrice)
        StockItem.AddLoadField(StockItem.StockOnHand)
        StockItem.AddLoadField(StockItem.CatchAll)
        StockItem.AddLoadField(StockItem.ValueSoldYesterday)
        StockItem.AddLoadField(StockItem.ValueSoldThisWeek)
        StockItem.AddLoadField(StockItem.ValueSoldThisPeriod)
        StockItem.AddLoadField(StockItem.ValueSoldThisYear)
        StockItem.AddLoadField(StockItem.DateLastSoldExcludingRefund)
        StockItem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockItem.SkuNumber, Line.SkuNumber.Value)
        StockItem.LoadMatches()

        If Line.LineReversed.Value = False Then
            'Implemented during US17409 special for new column in STKMAS
            'We decided not to use DSOL column, and create new DateLastSold column because the first one includes refund lines
            If StockItem.DateLastSoldExcludingRefund.Value < Line.TransDate.Value And Line.QuantitySold.Value > 0 Then
                StockItem.DateLastSoldExcludingRefund.Value = Line.TransDate.Value
            End If

            If Line.MarkDownStock.Value = False Then
                StockItem.UnitsSoldCurWkPlus1.Value += CInt(Line.QuantitySold.Value)
                StockItem.UnitsSoldYesterday.Value += CInt(Line.QuantitySold.Value)
                StockItem.UnitsSoldThisWeek.Value += CInt(Line.QuantitySold.Value)
                StockItem.UnitsSoldThisPeriod.Value += CInt(Line.QuantitySold.Value)
                StockItem.UnitsSoldThisYear.Value += CInt(Line.QuantitySold.Value)

                StockItem.ValueSoldYesterday.Value += decExtendedPrice
                StockItem.ValueSoldThisWeek.Value += decExtendedPrice
                StockItem.ValueSoldThisPeriod.Value += decExtendedPrice
                StockItem.ValueSoldThisYear.Value += decExtendedPrice

                'If its a related Item we need to update the Single Sold Value
                If Line.RelatedItems.Value = True Then
                    Dim relItem As New BOStock.cRelatedItems(_oasys3DB)
                    relItem.LoadRelatedItems(Line.SkuNumber.Value)

                    For Each item As BOStock.cRelatedItems In relItem.Items
                        item.SinglesSold.Value += Line.QuantitySold.Value
                        item.UpdateItemsSold(item.SingleItem.Value, item.BulkOrPackageItem.Value)
                    Next item

                End If

                'Update the stock last date sold if the date is less than the Line transaction date
                If StockItem.LastSold.Value < Line.TransDate.Value Then
                    Dim RetailOptions As New BOSystem.cRetailOptions(_oasys3DB)
                    RetailOptions.LoadMatches()
                    StockItem.LastSold.Value = RetailOptions.LastReformatDate.Value
                End If

                StockItem.ActivityToday.Value = True
            End If

            StockItem.UpdateStockMasterStatistics()
        End If

        _progressCallback.Ping()

        Return True

    End Function
End Class
