﻿Public Class StubProgressCallback : Implements IProgressCallback

    Public Sub SetupProgressBar(salesCount As Integer) Implements IProgressCallback.SetupProgressBar
    End Sub

    Public Sub IncrementProgressBar() Implements IProgressCallback.IncrementProgressBar
    End Sub

    Public Sub Ping() Implements IProgressCallback.Ping
    End Sub

    Public Sub UpdateStats(tranNumber As String, tranDate As Date, tillId As String, skuNumber As String) Implements IProgressCallback.UpdateStats
    End Sub
End Class
