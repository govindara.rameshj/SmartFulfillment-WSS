﻿Imports System.IO
Imports OasysDBBO

Public Class frmDailyFlashTotalsCreation

    'Formerly PTBUPD - Daily Flash Totals Creation
    '
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _SkuNumber As String = ""

    Private _blnRunFromNightlyRoutine As Boolean = False

    Private _BOSystem As New BOSystem.cSystemDates(_Oasys3DB)
    Private _BOUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
    Private _BOStock As New BOStock.cStock(_Oasys3DB)
    Private _BOSales As New BOSales.cSalesHeader(_Oasys3DB)
    Private _intNumLinesReversed As Integer = 0

    Public Sub RunDailyFlashTotalsCreation()

        Try
            Me.Show()
            Me.Refresh()
            Application.DoEvents()

            'btnExit.Enabled = False
            Cursor = Cursors.WaitCursor
            Refresh()

            'Need to get the Store Live Date for processing 
            _BOSystem = New BOSystem.cSystemDates(_Oasys3DB)
            _BOSystem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, _BOSystem.SystemDatesID, "01")
            Dim SystemList As List(Of BOSystem.cSystemDates) = _BOSystem.LoadMatches

            Dim dteLiveDate As Date = SystemList(0).StoreLiveDate.Value
            If dteLiveDate = Nothing Or dteLiveDate = CDate("01/01/1900") Or IsDBNull(dteLiveDate) Then
                Trace.WriteLine("Store Live Date was Invalid. DailyFlashTotalsCreation")
                Throw New Exception
            End If

            Dim BOSale As New BOSales.cSalesHeader(_Oasys3DB)
            BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOSale.DailyUpdateProc, False)
            BOSale.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, BOSale.TransDate, dteLiveDate)
            Dim SalesList As List(Of BOSales.cSalesHeader) = BOSale.LoadMatches
            ProgressBar1.Maximum = SalesList.Count
            ProgressBar1.Minimum = 0
            ProgressBar1.Value = 0
            Refresh()

            'Read in the Sales that are unprocessed for the current Store Live date
            For Each Sale As BOSales.cSalesHeader In SalesList
                If _BOSystem.RetryMode.Value = False Then
                    _intNumLinesReversed = 0 'Reset number of lines reversed for this transaction

                    'No need to do this CW ...
                    'If Sale.TrainingMode.Value = False Then ' Were not in training mode then we need to update Cashier Performance 
                    '    'Update Cashier Performance Values
                    '    UpdateCashierPerformanceSale(Sale)
                    'End If

                    'IF DLTOTS:TCOD (TransactionCode) = "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"
                    Select Case Sale.TransactionCode.Value
                        Case "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"

                            If Sale.TransParked.Value = False Then
                                Sale.DailyUpdateProc.Value = True
                            End If

                        Case Else

                            'Any other transaction code (Refunds) is If the transacion is Parked and were not in retry mode
                            Sale.DailyUpdateProc.Value = True
                            If Sale.TransParked.Value = True Then
                                UpdateParkedRefunds(Sale)
                            End If

                    End Select

                    'If Sale.TrainingMode.Value = False And Sale.Voided.Value = False Then
                    UpdateDLTOTS(Sale)
                    'End If

                End If

                ProgressBar1.Value += 1
                ProgressBar1.Refresh()
                Refresh()
                Application.DoEvents()
            Next Sale

            btnExit.Enabled = True

            'Kill the TASKCOMP file.
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

            Cursor = Cursors.Default

        Catch ex As Exception
            Trace.WriteLine("RunDailyFlashTotalsCreation: " & ex.Message.ToString)
            End
        End Try

    End Sub

    Private Function UpdateStockLogRecord(ByVal StockItem As BOStock.cStock, ByVal LogItem As BOStock.cStockLog, ByVal StockLog As BOStock.cStockLog, ByVal intCashierID As Integer) As Boolean

        If (StockLog.StockOnHandEnd.Value <> StockItem.StockOnHand.Value) Or _
           (StockLog.StockReturnsEnd.Value <> StockItem.UnitsInOpenReturns.Value) Or _
           (StockLog.StockMarkdownEnd.Value <> StockItem.MarkDownQuantity.Value) Or _
           (StockLog.StockWriteoffEnd.Value <> StockItem.WriteOffQuantity.Value) Then

            LogItem.StockOnHandStart.Value = StockLog.StockOnHandEnd.Value
            LogItem.StockReturnsStart.Value = StockLog.StockReturnsEnd.Value
            LogItem.PriceStart.Value = LogItem.PriceEnd.Value
            LogItem.StockMarkdownStart.Value = LogItem.StockMarkdownEnd.Value
            LogItem.StockWriteoffStart.Value = LogItem.StockWriteoffEnd.Value

            LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
            LogItem.StockReturnsEnd.Value = StockItem.UnitsInOpenReturns.Value
            LogItem.PriceEnd.Value = StockItem.NormalSellPrice.Value
            LogItem.StockMarkdownEnd.Value = StockItem.MarkDownQuantity.Value
            LogItem.StockWriteoffEnd.Value = StockItem.WriteOffQuantity.Value
            LogItem.LogKey.Value = "System Adjustment 91"
            LogItem.LogType.Value = "91"

            'Update the stock log record.
            LogItem.InsertLog(LogItem.LogType.Value, LogItem.LogKey.Value, intCashierID, StockItem.SkuNumber.Value, LogItem.StockOnHandStart.Value, LogItem.StockOnHandEnd.Value, LogItem.StockReturnsStart.Value, LogItem.StockReturnsEnd.Value, LogItem.StockMarkdownStart.Value, LogItem.StockMarkdownEnd.Value, LogItem.StockWriteoffStart.Value, LogItem.StockWriteoffEnd.Value, LogItem.PriceStart.Value, LogItem.PriceEnd.Value)

        End If

        LogItem.SkuNumber.Value = StockItem.SkuNumber.Value
        LogItem.StockOnHandStart.Value = StockItem.StockOnHand.Value
        LogItem.StockReturnsStart.Value = StockItem.UnitsInOpenReturns.Value
        LogItem.PriceStart.Value = StockItem.NormalSellPrice.Value
        LogItem.StockMarkdownStart.Value = StockItem.MarkDownQuantity.Value
        LogItem.StockWriteoffStart.Value = StockItem.WriteOffQuantity.Value

        LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
        LogItem.PriceEnd.Value = StockItem.NormalSellPrice.Value
        LogItem.StockMarkdownEnd.Value = StockItem.MarkDownQuantity.Value
        LogItem.StockWriteoffEnd.Value = StockItem.WriteOffQuantity.Value
        LogItem.LogType.Value = "70"

        '*** End of Stock Log Setup
        Return True

    End Function 'UpdateStockLogRecord

    Private Function GetExtendedPrice(ByVal Line As BOSales.cSalesLine) As Decimal

        Return (Line.ExtendedValue.Value - _
                Line.QtyBreakMarginAmount.Value - _
                Line.DealGroupMarginAmt.Value - _
                Line.MultiBuyMarginAmount.Value - _
                Line.HierarchyMarginAmt.Value - _
                Line.EmpSaleSecMarginAmt.Value)

    End Function 'GetExtendedPrice

    Private Function UpdateParkedRefunds(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_Oasys3DB)
        Try

            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransNo, Sale.TransactionNo.Value)
            SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransDate, Sale.TransDate.Value)
            SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TillID, Sale.TillID.Value)
            Dim SaleList As List(Of BOSales.cSalesLine) = SaleLine.LoadMatches
            Dim intSequenceNum As Integer = 0

            For Each Line As BOSales.cSalesLine In SaleList

                'Ignore Line reversals
                If Line.LineReversed.Value = False Then

                    'If Quantity Sold is less than 0 calculate the adjustment quantity
                    If Line.QuantitySold.Value < 0 Then

                        'Calculate the adjustment
                        Dim lngAdjustmentQuantity = 0 - Line.QuantitySold.Value

                        'If the adjustment quantity is greater than 9999 ignore as probably a dodgy record
                        If lngAdjustmentQuantity < 9999 Then

                            'Amend the STKMAS:ONHA value by the adjustment quantity
                            Dim StockItem As New BOStock.cStock(_Oasys3DB)
                            StockItem.AddLoadField(StockItem.SkuNumber)
                            StockItem.AddLoadField(StockItem.StockOnHand)
                            StockItem.AddLoadField(StockItem.NormalSellPrice)
                            StockItem.AddLoadField(StockItem.CostPrice)
                            StockItem.AddLoadField(StockItem.UnitsInOpenReturns)
                            StockItem.AddLoadField(StockItem.MarkDownQuantity)
                            StockItem.AddLoadField(StockItem.WriteOffQuantity)
                            StockItem.AddLoadField(StockItem.CatchAll)
                            StockItem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockItem.SkuNumber, Line.SkuNumber.Value)
                            StockItem.LoadMatches()

                            'Calculate the extended price.
                            Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)

                            Dim BOUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
                            BOUser.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOUser.EmployeeCode, Sale.CashierID.Value)
                            BOUser.LoadMatches()

                            '*** Add a Stock Adjustment record for Parked refunds. ***
                            Dim StockAdj As New BOStock.cStockAdjust(_Oasys3DB)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.SkuNumber, Line.SkuNumber.Value)
                            StockAdj.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.Code, "08")
                            StockAdj.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            StockAdj.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockAdj.DateCreated, Today)
                            StockAdj.SortBy(StockAdj.Sequence.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                            Dim StkAdjList As List(Of BOStock.cStockAdjust) = StockAdj.LoadMatches()
                            If StkAdjList.Count > 0 Then
                                intSequenceNum = CInt(Val(StkAdjList(0).Sequence.Value) + 1)
                            End If

                            'Update stats
                            lblTransaction.Text = Sale.TransactionNo.Value
                            lblTranDate.Text = CStr(Sale.TransDate.Value)
                            lblTranTill.Text = Sale.TillID.Value
                            lblTranSkun.Text = Line.SkuNumber.Value
                            Me.Refresh()
                            Application.DoEvents()

                            StockAdj.SkuNumber.Value = Line.SkuNumber.Value
                            StockAdj.DateCreated.Value = Now.Date
                            StockAdj.Code.Value = "08"
                            StockAdj.Sequence.Value = Format(intSequenceNum, "00")
                            StockAdj.EmployeeID.Value = BOUser.Initials.Value
                            StockAdj.StartStock.Value = StockItem.StockOnHand.Value
                            StockAdj.QtyAdjusted.Value = lngAdjustmentQuantity
                            StockAdj.Price.Value = StockItem.NormalSellPrice.Value
                            StockAdj.Cost.Value = StockItem.CostPrice.Value
                            StockAdj.CodeType.Value = "N"
                            StockAdj.Comment.Value = "Parked Refund Adj."
                            StockAdj.RTI.Value = "N"

                            'Now save the StockAdj record away.
                            StockAdj.UpdateDailyFlashTotalsAdjustment()
                            '*** End of Updating the Stock Adjustment ***

                            'Now Setup the Stock Log record ...
                            Dim StockLog As New BOStock.cStockLog(_Oasys3DB)

                            StockLog.LogDate.Value = Now.Date
                            StockLog.LogDayNumber.Value = CInt(DateDiff(DateInterval.Day, CDate("01/01/1900"), StockLog.LogDate.Value))
                            StockLog.LogDayNumber.Value += 1

                            StockLog.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockLog.SkuNumber, Line.SkuNumber.Value)
                            StockLog.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                            StockLog.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockLog.LogDayNumber, StockLog.LogDayNumber.Value)
                            StockLog.SortBy(StockLog.StockLogID.ColumnName, Oasys3.DB.clsOasys3DB.eOrderByType.Descending)
                            StockLog.LoadMatches()
                            Dim LogItem As New BOStock.cStockLog(_Oasys3DB)

                            'Dim LogItem As BOStock.cStockLog = StockLog.GetLatestLogRecordForPartCode(Line.SkuNumber.ToString).Item(1)
                            UpdateStockLogRecord(StockItem, LogItem, StockLog, CInt(Sale.CashierID.Value))

                            'Now update the STKMAS record with the updated on hand value
                            If StockItem.CatchAll.Value = True Then
                                StockItem.StockOnHand.Value = 0
                                StockItem.CostPrice.Value -= decExtendedPrice
                            Else
                                StockItem.StockOnHand.Value = CInt(StockItem.StockOnHand.Value + lngAdjustmentQuantity)
                            End If
                            'StockItem.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
                            Dim blnResult = StockItem.UpdateDailyFlashTotalsStockFile()
                            LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
                            LogItem.LogKey.Value = Sale.TransDate.Value & "  " & Sale.TillID.Value & "  " & Sale.TransactionNo.Value & "      " & Line.SequenceNo.Value.ToString '& " " & Sale.CashierID.Value

                            'Update the stock log record.
                            StockLog.InsertLog(LogItem.LogType.Value, LogItem.LogKey.Value, CInt(Sale.CashierID.Value), Line.SkuNumber.Value, LogItem.StockOnHandStart.Value, LogItem.StockOnHandEnd.Value, LogItem.StockReturnsStart.Value, LogItem.StockReturnsEnd.Value, LogItem.StockMarkdownStart.Value, LogItem.StockMarkdownEnd.Value, LogItem.StockWriteoffStart.Value, LogItem.StockWriteoffEnd.Value, LogItem.PriceStart.Value, LogItem.PriceEnd.Value)

                        End If

                    End If

                End If

            Next Line

            Return True

        Catch ex As Exception

            Trace.WriteLine("Error in UpdateParkedRefunds. " & ex.Message.ToString)
            Return False
        End Try

    End Function 'UpdateParkedRefunds

    Private Function UpdateDLTOTS(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_Oasys3DB)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransNo, Sale.TransactionNo.Value)
        SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TransDate, Sale.TransDate.Value)
        SaleLine.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        SaleLine.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, SaleLine.TillID, Sale.TillID.Value)
        Dim SaleList As List(Of BOSales.cSalesLine) = SaleLine.LoadMatches
        Try

            For Each Line As BOSales.cSalesLine In SaleList

                'Ignore Line reversals
                If Line.LineReversed.Value = False Then

                    'Update stats
                    lblTransaction.Text = Sale.TransactionNo.Value
                    lblTranDate.Text = CStr(Sale.TransDate.Value)
                    lblTranTill.Text = Sale.TillID.Value
                    lblTranSkun.Text = Line.SkuNumber.Value

                    Me.Refresh()
                    Application.DoEvents()

                    If Line.SkuNumber.Value <> "" Then
                        'Calculate the extended price.
                        Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)
                        UpdateStockMasterStatistics(Line, decExtendedPrice, CInt(Sale.CashierID.Value))
                    End If
                Else
                    _intNumLinesReversed += 1
                End If

            Next Line

            'Sale.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
            If SaleList.Count > 0 Then
                Dim strSQL As String = "UPDATE DLTOTS SET #PROC# = 1 WHERE #TRAN# ='" & Sale.TransactionNo.Value & "' AND #DATE1# = '" & Format(Sale.TransDate.Value, "yyyy-MM-dd") & "' AND #TILL#='" & Sale.TillID.Value & "' "
                strSQL = strSQL.Replace("#", Chr(34))
                _Oasys3DB.ExecuteSql(strSQL)

                'Sale.SaveIfExists()
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Trace.WriteLine("RunDailyFlashTotalsCreation: " & ex.Message.ToString)
        End Try

    End Function

    Private Function UpdateStockMasterStatistics(ByVal Line As BOSales.cSalesLine, ByVal decExtendedPrice As Decimal, ByVal intUserId As Integer) As Boolean

        Dim StockItem As New BOStock.cStock(_Oasys3DB)

        StockItem.AddLoadField(StockItem.SkuNumber)
        StockItem.AddLoadField(StockItem.UnitsSoldYesterday)
        StockItem.AddLoadField(StockItem.UnitsSoldThisWeek)
        StockItem.AddLoadField(StockItem.UnitsSoldThisPeriod)
        StockItem.AddLoadField(StockItem.UnitsSoldThisYear)
        StockItem.AddLoadField(StockItem.LastSold)
        StockItem.AddLoadField(StockItem.ActivityToday)
        StockItem.AddLoadField(StockItem.NormalSellPrice)
        StockItem.AddLoadField(StockItem.StockOnHand)
        StockItem.AddLoadField(StockItem.CatchAll)
        StockItem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockItem.SkuNumber, Line.SkuNumber.Value)
        StockItem.LoadMatches()

        If Line.LineReversed.Value = False And Line.MarkDownStock.Value = False Then

            StockItem.UnitsSoldYesterday.Value = CInt(StockItem.UnitsSoldYesterday.Value + Line.QuantitySold.Value)
            StockItem.UnitsSoldThisWeek.Value = CInt(StockItem.UnitsSoldThisWeek.Value + Line.QuantitySold.Value)
            StockItem.UnitsSoldThisPeriod.Value = CInt(StockItem.UnitsSoldThisPeriod.Value + Line.QuantitySold.Value)
            StockItem.UnitsSoldThisYear.Value = CInt(StockItem.UnitsSoldThisYear.Value + Line.QuantitySold.Value)

            StockItem.ValueSoldYesterday.Value += decExtendedPrice
            StockItem.ValueSoldThisWeek.Value += decExtendedPrice
            StockItem.ValueSoldThisPeriod.Value += decExtendedPrice
            StockItem.ValueSoldThisYear.Value += decExtendedPrice

            If StockItem.NormalSellPrice.Value <> Line.ActualSellPrice.Value Then

                Dim StockHistory As New BOStock.cStockHistory(_Oasys3DB)
                StockHistory.AddLoadField(StockHistory.SKUNumber)
                StockHistory.AddLoadField(StockHistory.PriceViol1)
                StockHistory.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockHistory.SKUNumber, Line.SkuNumber.Value)
                StockHistory.LoadMatches()

                Dim decPriceViolation = ((StockItem.NormalSellPrice.Value * Line.QuantitySold.Value) - Line.ExtendedValue.Value)
                StockHistory.PriceViol1.Value += decPriceViolation
                StockHistory.PriceViol1.Value += Line.QtyBreakMarginAmount.Value
                StockHistory.PriceViol1.Value += Line.DealGroupMarginAmt.Value
                StockHistory.PriceViol1.Value += Line.MultiBuyMarginAmount.Value
                StockHistory.PriceViol1.Value += Line.HierarchyMarginAmt.Value

                StockHistory.SaveIfExists()

            End If

            'If its a related Item we need to update the Single Sold Value
            If Line.RelatedItems.Value = True Then
                Dim RelatedItem As New BOStock.cRelatedItems(_Oasys3DB)
                RelatedItem.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, RelatedItem.SingleItem, Line.SkuNumber.Value)
                Dim items As List(Of BOStock.cRelatedItems) = RelatedItem.LoadMatches
                For Each item As BOStock.cRelatedItems In items
                    'item.GetRelatedItem(Line.SkuNumber.Value)
                    item.SinglesSold.Value += Line.QuantitySold.Value
                    item.SaveIfExists()
                Next item

            End If

            'Update the stock last date sold if the date is less than the Line transaction date
            If StockItem.LastSold.Value < Line.TransDate.Value Then
                Dim RetailOptions As New BOSystem.cRetailOptions(_Oasys3DB)
                RetailOptions.LoadMatches()
                StockItem.LastSold.Value = RetailOptions.LastReformatDate.Value
                StockItem.ActivityToday.Value = True
            End If

            StockItem.SaveIfExists()
        End If

        Application.DoEvents()
        Me.Refresh()

        Return True

    End Function

    Public Function UpdateCashierPerformanceSale(ByVal sale As BOSales.cSalesHeader) As Boolean

        'To Do: Update Cashier Performance NOT NEEDED CW 

        'Need to get record from CashBalCashier
        '_BOControl.getPropertyIdFromTransactionDate(sale.TransDate.Value)
        '_BOCashier.GetCashierPerformanceRecord(sale.CashierID.Value, _BOControl.PeriodID.Value)

        ''Get cashier name from CASMAS
        '_BOUser.GetUser(CInt(sale.CashierID.Value))
        'If _BOUser.Name.Value = "" Then
        '    _BOUser.Name.Value = "Cashier " & sale.CashierID.Value & " missing."
        'End If

        'If sale.Voided.Value = False Then 'If the transaction is not voided

        '    'Update Depending on the trnsaction type 
        '    Select Case sale.TransactionCode.ToString

        '        Case "SA", "RF", "M+", "M-"
        '            _BOCashier.NumTransactions.Value += 1

        '        Case "SC", "RC", "C+", "C-"
        '            _BOCashier.NumCorrections.Value += 1

        '        Case "OD"
        '            _BOCashier.NumOpenDrawer.Value += 1

        '        Case Else
        '            Debug.Print("Shouldn't get this Transaction code =  " & sale.TransactionCode.ToString) 't Get this 

        '    End Select

        'Else    'If the transaction is voided
        '    _BOCashier.NumVoids.Value += 1
        'End If

        ''Save the Record back to cashier performance
        'If _BOCashier.PeriodID.Value > 0 Then
        '    _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        'Else
        '    _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        'End If

        Return True

    End Function

    Public Function UpdateCashierPerformanceLine(ByVal Sale As BOSales.cSalesHeader, ByVal Line As BOSales.cSalesLine) As Boolean

        'To Do: Update Cashier Performance Line

        ''Need to get record from CashBalCashier
        '_BOControl.getPropertyIdFromTransactionDate(Sale.TransDate.Value)
        '_BOCashier.GetCashierPerformanceRecord(Sale.CashierID.Value, _BOControl.PeriodID.Value)

        ''Get cashier name from CASMAS
        '_BOUser.GetUser(CInt(Sale.CashierID.Value))
        'If _BOUser.Name.Value = "" Then
        '    _BOUser.Name.Value = "Cashier " & Sale.CashierID.Value & " missing."
        'End If

        'If Line.LineReversed.Value = True And _intNumLinesReversed = 2 Then

        'End If

        'If Sale.Voided.Value = False Then 'If the transaction is not voided

        '    'Update Depending on the trnsaction type 
        '    Select Case Sale.TransactionCode.ToString

        '        Case "SA", "RF", "M+", "M-"
        '            _BOCashier.NumTransactions.Value += 1

        '        Case "SC", "RC", "C+", "C-"
        '            _BOCashier.NumCorrections.Value += 1

        '        Case "OD"
        '            _BOCashier.NumOpenDrawer.Value += 1

        '        Case Else
        '            Debug.Print("Shouldn't get this Transaction code =  " & Sale.TransactionCode.ToString) 't Get this 

        '    End Select

        'Else    'If the transaction is voided
        '    _BOCashier.NumVoids.Value += 1
        'End If

        ''Save the Record back to cashier performance
        'If _BOCashier.PeriodID.Value > 0 Then
        '    _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        'Else
        '    _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        'End If

        Return True

    End Function

    Private Sub Start(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Ask the user if they want to update, if they answer No the program ends.
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub frmDailyFlashTotalsCreation_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.Show()
        Me.Refresh()
        Application.DoEvents()
    End Sub

    Private Sub frmDailyFlashTotalsCreation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim blnRunAutomatically As Boolean = False

        ' get command parameters 
        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
            If strCmd = "Y" Or strCmd = "y" Then
                blnRunAutomatically = True
            End If
        Next

        Me.Show()
        Me.Refresh()

        If blnRunAutomatically = True Then
            RunDailyFlashTotalsCreation()
        Else
            'Run Automatically ...
            If MsgBox("Update Sales Statistics (Y/N):", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, MsgBoxStyle), "Daily Flash Totals Creation") = vbYes Then
                RunDailyFlashTotalsCreation()
            End If
        End If
        End

    End Sub

End Class
