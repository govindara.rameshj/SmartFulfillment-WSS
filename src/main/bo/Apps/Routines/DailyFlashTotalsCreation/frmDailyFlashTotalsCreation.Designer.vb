﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDailyFlashTotalsCreation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTransaction = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblTill = New System.Windows.Forms.Label
        Me.lblTranDate = New System.Windows.Forms.Label
        Me.lblTranTill = New System.Windows.Forms.Label
        Me.lblSku = New System.Windows.Forms.Label
        Me.lblTranSkun = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(12, 167)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 5
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 116)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(420, 35)
        Me.ProgressBar1.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(139, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Checking Transaction: "
        '
        'lblTransaction
        '
        Me.lblTransaction.AutoSize = True
        Me.lblTransaction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransaction.Location = New System.Drawing.Point(157, 21)
        Me.lblTransaction.Name = "lblTransaction"
        Me.lblTransaction.Size = New System.Drawing.Size(0, 13)
        Me.lblTransaction.TabIndex = 9
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(12, 45)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(49, 13)
        Me.lblDate.TabIndex = 10
        Me.lblDate.Text = "Dated: "
        '
        'lblTill
        '
        Me.lblTill.AutoSize = True
        Me.lblTill.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTill.Location = New System.Drawing.Point(12, 67)
        Me.lblTill.Name = "lblTill"
        Me.lblTill.Size = New System.Drawing.Size(50, 13)
        Me.lblTill.TabIndex = 11
        Me.lblTill.Text = "For Till:"
        '
        'lblTranDate
        '
        Me.lblTranDate.AutoSize = True
        Me.lblTranDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranDate.Location = New System.Drawing.Point(60, 45)
        Me.lblTranDate.Name = "lblTranDate"
        Me.lblTranDate.Size = New System.Drawing.Size(0, 13)
        Me.lblTranDate.TabIndex = 12
        '
        'lblTranTill
        '
        Me.lblTranTill.AutoSize = True
        Me.lblTranTill.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranTill.Location = New System.Drawing.Point(60, 67)
        Me.lblTranTill.Name = "lblTranTill"
        Me.lblTranTill.Size = New System.Drawing.Size(0, 13)
        Me.lblTranTill.TabIndex = 13
        '
        'lblSku
        '
        Me.lblSku.AutoSize = True
        Me.lblSku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSku.Location = New System.Drawing.Point(12, 89)
        Me.lblSku.Name = "lblSku"
        Me.lblSku.Size = New System.Drawing.Size(33, 13)
        Me.lblSku.TabIndex = 14
        Me.lblSku.Text = "Sku:"
        '
        'lblTranSkun
        '
        Me.lblTranSkun.AutoSize = True
        Me.lblTranSkun.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranSkun.Location = New System.Drawing.Point(60, 89)
        Me.lblTranSkun.Name = "lblTranSkun"
        Me.lblTranSkun.Size = New System.Drawing.Size(0, 13)
        Me.lblTranSkun.TabIndex = 15
        '
        'frmDailyFlashTotalsCreation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(444, 218)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTranSkun)
        Me.Controls.Add(Me.lblSku)
        Me.Controls.Add(Me.lblTranTill)
        Me.Controls.Add(Me.lblTranDate)
        Me.Controls.Add(Me.lblTill)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.lblTransaction)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "frmDailyFlashTotalsCreation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Daily Flash Totals Creation"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTransaction As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblTill As System.Windows.Forms.Label
    Friend WithEvents lblTranDate As System.Windows.Forms.Label
    Friend WithEvents lblTranTill As System.Windows.Forms.Label
    Friend WithEvents lblSku As System.Windows.Forms.Label
    Friend WithEvents lblTranSkun As System.Windows.Forms.Label

End Class
