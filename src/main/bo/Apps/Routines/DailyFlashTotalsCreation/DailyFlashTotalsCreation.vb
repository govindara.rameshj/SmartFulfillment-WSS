﻿Imports System.IO
Imports OasysDBBO

Module MainModule

    Dim _blnRunFromNightlyRoutine As Boolean
    Dim _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Dim _BOSales As New BOSales.cSalesHeader(_Oasys3DB)
    Dim _BOSystem As New BOSystem.cSystemDates(_Oasys3DB)
    Dim _BOCashier As New BOBanking.cCashBalCashier(_Oasys3DB)
    Dim _BOControl As New BOBanking.cCashBalControl(_Oasys3DB)
    Dim _BOUser As New BOSystemUsers.cSystemUsers(_Oasys3DB)
    Dim _BOStock As New BOStock.cStock(_Oasys3DB)
    Dim _intNumLinesReversed As Integer = 0

    Private Sub Main()

        ' get command parameters 
        Dim strCmd As String = String.Empty
        For Each cmd As String In My.Application.CommandLineArgs
            strCmd = cmd
        Next

        'get data from table 
        Select Case strCmd
            Case "NI"
                _blnRunFromNightlyRoutine = True
            Case Else
                'Left Blank intentionally
        End Select

        'Run Automatically if ran from the night routine ...
        If _blnRunFromNightlyRoutine Then
            RunDailyFlashTotalsCreation()
        Else
            frmDailyFlashTotalsCreation.Show()
        End If

        'Program ends
        End

    End Sub

    Public Sub RunDailyFlashTotalsCreation()

        'Need to get the Store Live Date for processing 
        _BOSystem.LoadMatches()

        Dim dteLiveDate As Date = _BOSystem.StoreLiveDate.Value
        Dim BOSale As New BOSales.cSalesHeader(_Oasys3DB)
        BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOSale.DailyUpdateProc, False)
        BOSale.JoinLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        BOSale.AddLoadFilter(Oasys3.DB.clsOasys3DB.eOperator.pLessThan, BOSale.TransDate, dteLiveDate)
        Dim SalesList As List(Of BOSales.cSalesHeader) = BOSale.LoadMatches

        'Read in the Sales that are unprocessed for the current Store Live date
        For Each Sale As BOSales.cSalesHeader In SalesList

            _intNumLinesReversed = 0 'Reset number of lines reversed for this transaction

            If Sale.TrainingMode.Value = False Then ' Were not in training mode then we need to update Cashier Performance 
                'Update Cashier Performance Values
                UpdateCashierPerformanceSale(Sale)
            End If

            'IF DLTOTS:TCOD (TransactionCode) = "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"
            Select Case Sale.TransactionCode.Value
                Case "CO", "CC", "M+", "M-", "OD", "RL", "C+", "C-", "ZR", "XR"

                    If Sale.TransParked.Value = False Or (Sale.TransParked.Value = False And _BOSystem.RetryMode.Value = False) Then
                        Sale.DailyUpdateProc.Value = True
                        UpdateDLTOTS(Sale)
                    End If

                Case Else
                    'Any other transaction code (Refunds) is If the transacion is Parked and were not in retry mode
                    If Sale.TransParked.Value = True And _BOSystem.RetryMode.Value = False Then
                        UpdateParkedRefunds(Sale)
                    End If
                    UpdateDLTOTS(Sale)

            End Select

        Next Sale

    End Sub

    Private Function UpdateStockLogRecord(ByVal StockItem As BOStock.cStock, ByVal LogItem As BOStock.cStockLog, ByRef StockLog As BOStock.cStockLog) As Boolean

        If (LogItem.StockOnHandEnd.Value <> StockItem.StockOnHand.Value) Or _
           (LogItem.StockReturnsEnd.Value <> StockItem.UnitsInOpenReturns.Value) Or _
           (LogItem.StockMarkdownEnd.Value <> StockItem.MarkDownQuantity.Value) Or _
           (LogItem.StockWriteoffEnd.Value <> StockItem.WriteOffQuantity.Value) Then

            LogItem.StockOnHandStart.Value = StockLog.StockOnHandEnd.Value
            LogItem.StockReturnsStart.Value = StockLog.StockOnHandEnd.Value
            LogItem.PriceStart.Value = LogItem.PriceEnd.Value
            LogItem.StockMarkdownStart.Value = LogItem.StockMarkdownEnd.Value
            LogItem.StockWriteoffStart.Value = LogItem.StockWriteoffEnd.Value

            LogItem.StockOnHandEnd.Value = StockItem.StockOnHand.Value
            LogItem.PriceEnd.Value = StockItem.CostPrice.Value
            LogItem.StockMarkdownEnd.Value = StockItem.MarkDownQuantity.Value
            LogItem.StockWriteoffEnd.Value = StockItem.WriteOffQuantity.Value
            LogItem.LogType.Value = "91"

        Else

            LogItem.LogType.Value = "70"

        End If

        '*** End of Stock Log Setup
        Return True

    End Function 'UpdateStockLogRecord

    Private Function GetExtendedPrice(ByVal Line As BOSales.cSalesLine) As Decimal

        Return (Line.ExtendedValue.Value - _
                Line.QtyBreakMarginAmount.Value - _
                Line.DealGroupMarginAmt.Value - _
                Line.MultiBuyMarginAmount.Value - _
                Line.HierarchyMarginAmt.Value - _
                Line.EmpSaleSecMarginAmt.Value)

    End Function 'GetExtendedPrice

    Private Function UpdateParkedRefunds(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_Oasys3DB)
        Dim intSequenceNum As Integer = 0

        For Each Line As BOSales.cSalesLine In SaleLine.GetLinesForTransaction(Sale.TransactionNo.Value)

            'Ignore Line reversals
            If Line.LineReversed.Value = False Then

                'If Quantity Sold is less than 0 calculate the adjustment quantity
                If Line.QuantitySold.Value < 0 Then

                    'Calculate the adjustment
                    Dim lngAdjustmentQuantity = 0 - Line.QuantitySold.Value

                    'If the adjustment quantity is greater than 9999 ignore as probably a dodgy record
                    If lngAdjustmentQuantity < 9999 Then

                        'Amend the STKMAS:ONHA value by the adjustment quantity
                        Dim Stock As New BOStock.cStock(_Oasys3DB)
                        Dim StockItem As BOStock.cStock = Stock.Stock(Line.PartCode.Value)

                        'Calculate the extended price.
                        Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)

                        '*** Add a Stock Adjustment record for Parked refunds. ***
                        Dim StockAdj As New BOStock.cStockAdjust(_Oasys3DB)
                        StockAdj.SkuNumber.Value = Line.PartCode.Value
                        StockAdj.DateCreated.Value = Now.Date
                        StockAdj.Code.Value = "08"
                        StockAdj.Sequence.Value = Format("00", intSequenceNum)
                        StockAdj.EmployeeID.Value = _BOUser.Initials.Value
                        StockAdj.StartStock.Value = StockItem.StockOnHand.Value
                        StockAdj.QtyAdjusted.Value = lngAdjustmentQuantity
                        StockAdj.Price.Value = StockItem.NormalSellPrice.Value
                        StockAdj.Cost.Value = StockItem.CostPrice.Value
                        StockAdj.CodeType.Value = "N"
                        StockAdj.Comment.Value = "Parked Refund Adj."
                        StockAdj.RTI.Value = "N"

                        'Now save the StockAdj record away.
                        StockAdj.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
                        '*** End of Updating the Stock Adjustment ***

                        'Now Setup the Stock Log record ...
                        Dim StockLog As New BOStock.cStockLog(_Oasys3DB)
                        Dim LogItem As BOStock.cStockLog = StockLog.GetLatestLogRecordForPartCode(Line.PartCode.ToString).Item(1)
                        UpdateStockLogRecord(StockItem, LogItem, StockLog)

                        'Now update the STKMAS record with the updated on hAnd value
                        If StockItem.CatchAll.Value = True Then
                            StockItem.StockOnHand.Value = 0
                            StockItem.CostPrice.Value -= decExtendedPrice
                        Else
                            StockItem.StockOnHand.Value += lngAdjustmentQuantity
                        End If
                        StockItem.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
                        '*** stkmas has been updated

                        'Update the stock log record.
                        StockLog.InsertLog(LogItem.LogType.Value, LogItem.LogKey.Value, Sale.CashierID.Value, Line.PartCode.Value, LogItem.StockOnHandStart.Value, LogItem.StockOnHandEnd.Value, LogItem.StockReturnsStart.Value, LogItem.StockReturnsEnd.Value, LogItem.StockMarkdownStart.Value, LogItem.StockMarkdownEnd.Value, LogItem.StockWriteoffStart.Value, LogItem.StockWriteoffEnd.Value, LogItem.PriceStart.Value, LogItem.PriceEnd.Value)
                    End If

                End If

            End If

        Next Line

        Return True

    End Function 'UpdateParkedRefunds

    Private Function UpdateDLTOTS(ByVal Sale As BOSales.cSalesHeader) As Boolean

        Dim SaleLine As New BOSales.cSalesLine(_Oasys3DB)

        For Each Line As BOSales.cSalesLine In SaleLine.GetLinesForTransaction(Sale.TransactionNo.Value)

            'Ignore Line reversals
            If Line.LineReversed.Value = False Then
                If Line.PartCode.Value <> "" Then
                    'Calculate the extended price.
                    Dim decExtendedPrice As Decimal = GetExtendedPrice(Line)
                    UpdateStockMasterStatistics(Line, decExtendedPrice)
                Else
                    UpdateCashierStatistics(Sale, Line)
                End If
            Else
                _intNumLinesReversed += 1
            End If

        Next Line

        Sale.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Return True

    End Function

    Private Function UpdateCashierStatistics(ByVal Sale As BOSales.cSalesHeader, ByVal Line As BOSales.cSalesLine) As Boolean

        _BOControl.getPropertyIdFromTransactionDate(Sale.TransDate.Value)
        _BOCashier.GetCashierPerformanceRecord(Sale.CashierID.Value, _BOControl.PeriodID.Value)

        If Sale.TransactionCode.Value = "RC" Then
            '_BOCashier.


        End If
        Return True
    End Function 'UpdateCashierStatistics

    Private Function UpdateStockMasterStatistics(ByVal Line As BOSales.cSalesLine, ByVal decExtendedPrice As Decimal) As Boolean

        Dim StockItem As BOStock.cStock = _BOStock.Stock(Line.PartCode.Value)

        If Line.LineReversed.Value = False And Line.MarkDownStock.Value = False Then

            StockItem.UnitsSoldYesterday.Value += Line.QuantitySold.Value
            StockItem.UnitsSoldThisWeek.Value += Line.QuantitySold.Value
            StockItem.UnitsSoldThisPeriod.Value += Line.QuantitySold.Value
            StockItem.UnitsSoldThisYear.Value += Line.QuantitySold.Value

            StockItem.ValueSoldYesterday.Value += decExtendedPrice
            StockItem.ValueSoldThisWeek.Value += decExtendedPrice
            StockItem.ValueSoldThisPeriod.Value += decExtendedPrice
            StockItem.ValueSoldThisYear.Value += decExtendedPrice

            If StockItem.NormalSellPrice.Value <> Line.ActualSellPrice.Value Then
                'To Do: Can't Find STKMAS:MPVV
            End If

            'If its a related Item we need to update the Single Sold Value
            If Line.RelatedItems.Value = True Then
                Dim RelatedItem As New BOStock.cRelatedItems(_Oasys3DB)
                RelatedItem.GetRelatedItem(Line.PartCode.Value)
                RelatedItem.SinglesSold.Value += Line.QuantitySold.Value
                RelatedItem.SaveIfExists()
            End If

            'Update the stock last date sold if the date is less than the Line transaction date
            If StockItem.LastSold.Value < Line.TransDate.Value Then
                Dim RetailOptions As New BOSystem.cRetailOptions(_Oasys3DB)
                RetailOptions.LoadMatches()
                StockItem.LastSold.Value = RetailOptions.LastReformatDate.Value
                StockItem.ActivityToday.Value = "Y"
            End If

            StockItem.SaveIfExists()

        End If

        Return True

    End Function

    Public Function UpdateCashierPerformanceSale(ByVal sale As BOSales.cSalesHeader) As Boolean

        'Need to get record from CashBalCashier
        _BOControl.getPropertyIdFromTransactionDate(sale.TransDate.Value)
        _BOCashier.GetCashierPerformanceRecord(sale.CashierID.Value, _BOControl.PeriodID.Value)

        'Get cashier name from CASMAS
        _BOUser.GetUser(CInt(sale.CashierID.Value))
        If _BOUser.Name.Value = "" Then
            _BOUser.Name.Value = "Cashier " & sale.CashierID.Value & " missing."
        End If

        If sale.Voided.Value = False Then 'If the transaction is not voided

            'Update Depending on the trnsaction type 
            Select Case sale.TransactionCode.ToString

                Case "SA", "RF", "M+", "M-"
                    _BOCashier.NumTransactions.Value += 1

                Case "SC", "RC", "C+", "C-"
                    _BOCashier.NumCorrections.Value += 1

                Case "OD"
                    _BOCashier.NumOpenDrawer.Value += 1

                Case Else
                    Debug.Print("Shouldn't get this Transaction code =  " & sale.TransactionCode.ToString) 't Get this 

            End Select

        Else    'If the transaction is voided
            _BOCashier.NumVoids.Value += 1
        End If

        'Save the Record back to cashier performance
        If _BOCashier.PeriodID.Value > 0 Then
            _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Else
            _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        End If

        Return True

    End Function

    Public Function UpdateCashierPerformanceLine(ByVal Sale As BOSales.cSalesHeader, ByVal Line As BOSales.cSalesLine) As Boolean

        'Need to get record from CashBalCashier
        _BOControl.getPropertyIdFromTransactionDate(Sale.TransDate.Value)
        _BOCashier.GetCashierPerformanceRecord(Sale.CashierID.Value, _BOControl.PeriodID.Value)

        'Get cashier name from CASMAS
        _BOUser.GetUser(CInt(Sale.CashierID.Value))
        If _BOUser.Name.Value = "" Then
            _BOUser.Name.Value = "Cashier " & Sale.CashierID.Value & " missing."
        End If

        If Line.LineReversed.Value = True And _intNumLinesReversed = 2 Then

        End If

        If Sale.Voided.Value = False Then 'If the transaction is not voided

            'Update Depending on the trnsaction type 
            Select Case Sale.TransactionCode.ToString

                Case "SA", "RF", "M+", "M-"
                    _BOCashier.NumTransactions.Value += 1

                Case "SC", "RC", "C+", "C-"
                    _BOCashier.NumCorrections.Value += 1

                Case "OD"
                    _BOCashier.NumOpenDrawer.Value += 1

                Case Else
                    Debug.Print("Shouldn't get this Transaction code =  " & Sale.TransactionCode.ToString) 't Get this 

            End Select

        Else    'If the transaction is voided
            _BOCashier.NumVoids.Value += 1
        End If

        'Save the Record back to cashier performance
        If _BOCashier.PeriodID.Value > 0 Then
            _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pUpdate)
        Else
            _BOCashier.Save(OasysDBBO.Oasys3.DB.clsOasys3DB.eSqlQueryType.pInsert)
        End If

        Return True

    End Function

End Module
