﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RefreshEventEnquiry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RefreshEventEnquiry))
        Me.lblProcessingComplete = New System.Windows.Forms.Label
        Me.pbCompleted = New System.Windows.Forms.ProgressBar
        Me.SuspendLayout()
        '
        'lblProcessingComplete
        '
        Me.lblProcessingComplete.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProcessingComplete.Location = New System.Drawing.Point(12, 9)
        Me.lblProcessingComplete.Name = "lblProcessingComplete"
        Me.lblProcessingComplete.Size = New System.Drawing.Size(335, 13)
        Me.lblProcessingComplete.TabIndex = 0
        Me.lblProcessingComplete.Text = "Percentage Complete"
        Me.lblProcessingComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblProcessingComplete.UseMnemonic = False
        '
        'pbCompleted
        '
        Me.pbCompleted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbCompleted.Location = New System.Drawing.Point(12, 25)
        Me.pbCompleted.Name = "pbCompleted"
        Me.pbCompleted.Size = New System.Drawing.Size(335, 23)
        Me.pbCompleted.TabIndex = 1
        '
        'RefreshEventEnquiry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(359, 62)
        Me.Controls.Add(Me.pbCompleted)
        Me.Controls.Add(Me.lblProcessingComplete)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RefreshEventEnquiry"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblProcessingComplete As System.Windows.Forms.Label
    Friend WithEvents pbCompleted As System.Windows.Forms.ProgressBar
End Class
