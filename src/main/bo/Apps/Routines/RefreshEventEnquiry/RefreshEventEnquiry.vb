﻿Imports Cts.Oasys.Core.System
Imports System.IO

Public Class RefreshEventEnquiry
    Private _Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _processRunning As Boolean

    Public Sub New()
        InitializeComponent()

        Cts.Oasys.Core.Tests.TestEnvironmentSetup.SetupIfTestRun()

        _Oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        pbCompleted.Increment(0)
    End Sub

    Private Sub RefreshEventEnquiry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            If Command.Contains("/P=',CFC'") Then
                RunProcessing()
            End If
        End If
    End Sub

    Private Sub RefreshEventEnquiry_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Shown

        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            'Make sure its not already started from nightly routine
            If Not _processRunning Then
                RunProcessing()
            End If
        Else
            RunProcessing()
        End If
    End Sub

    Private Sub RunProcessing()

        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            RunProcessingNew()
            'Kill the TASKCOMP file if it exists
            If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then
                File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")
            End If
        Else
            RunProcessingOriginal()
        End If
    End Sub

    Private Sub RunProcessingOriginal()
        Dim EvtEnq As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)
        Dim EvtHdr As BOEvent.cEventHeader = New BOEvent.cEventHeader(_Oasys3DB)
        Dim EvtDealGroup As BOEvent.cEventDealGroup = New BOEvent.cEventDealGroup(_Oasys3DB)
        Dim EvtMMGroup As BOEvent.cEventMixMatch = New BOEvent.cEventMixMatch(_Oasys3DB)
        Dim Evt As BOEvent.cEvent = New BOEvent.cEvent(_Oasys3DB)
        Dim LstEvtHdr As List(Of BOEvent.cEventHeader)
        Dim LstEvtMas As List(Of BOEvent.cEvent)
        Dim LstEvtDealGroup As List(Of BOEvent.cEventDealGroup)
        Dim LstEvtMMGroup As List(Of BOEvent.cEventMixMatch)
        Dim intCount As Integer = 0
        Dim intLastPercent As Integer = 0

        'Delete all existing records
        EvtEnq.DeleteAll()

        'get the active event headers
        LstEvtHdr = EvtHdr.GetEventHeaders(Date.Now)
        If LstEvtHdr IsNot Nothing Then
            For Each header As BOEvent.cEventHeader In LstEvtHdr
                intCount = intCount + 1
                If intCount / LstEvtHdr.Count <> intLastPercent Then
                    intLastPercent = CInt((intCount / LstEvtHdr.Count) * 100)
                    pbCompleted.Increment(intLastPercent - pbCompleted.Value)
                    FindForm.Refresh()
                End If
                ' read the active master records 
                LstEvtMas = Evt.GetEventMaster(header.EventNoID.Value)
                If LstEvtMas IsNot Nothing Then
                    'ProcessMaster
                    For Each Master As BOEvent.cEvent In LstEvtMas
                        Select Case Master.EventType.Value
                            ' if mix and match that is 'QM','TM' or 'MM' 
                            Case "QM", "TM", "MM"
                                'get mix and match group
                                LstEvtMMGroup = EvtMMGroup.GetMixMatchGroup(Master.EventKey1.Value)
                                ProcessMandMOriginal(Master, LstEvtMMGroup)
                            Case "DG"
                                'get deal group
                                ' iF dg then use key find active records on EVTDLG
                                LstEvtDealGroup = EvtDealGroup.GetDealGroup(Master.EventKey2.Value)
                                '   if dealSKU  that is type = 'S' then output One entry 
                                '   if dealM&M (mix and match then) that is tupe = M then output one entry for every sku
                                ProcessDealGroupOriginal(Master, EvtDealGroup)
                            Case "TS", "QS", "MS"
                                ProcessEventOriginal(Master)
                            Case ("HS")
                                'get hierachy
                                ProcessHierarchyOriginal(Master)
                            Case Else
                                ' ignore 
                        End Select
                    Next Master
                End If
            Next header
        End If
        FindForm.Close()
    End Sub

    Private Sub RunProcessingNew()
        Dim EnquiryEvent As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)

        Dim HeaderEvent As BOEvent.cEventHeader = New BOEvent.cEventHeader(_Oasys3DB)
        Dim HeaderEventList As List(Of BOEvent.cEventHeader)

        Dim MasterEvent As BOEvent.cEvent = New BOEvent.cEvent(_Oasys3DB)
        Dim MasterEventList As List(Of BOEvent.cEvent)

        Dim intCount As Integer
        Dim intLastPercent As Integer


        EnquiryEvent.DeleteAll()

        HeaderEventList = HeaderEvent.GetEventHeaders(Date.Now)
        If HeaderEventList IsNot Nothing Then
            intCount = 0
            intLastPercent = 0
            For Each Header As BOEvent.cEventHeader In HeaderEventList
                'update progress bar
                intCount = intCount + 1
                If intCount / HeaderEventList.Count <> intLastPercent Then
                    intLastPercent = CInt((intCount / HeaderEventList.Count) * 100)
                    pbCompleted.Increment(intLastPercent - pbCompleted.Value)
                    FindForm.Refresh()
                End If

                MasterEventList = MasterEvent.GetEventMaster(Header.EventNo.Value)
                If MasterEventList IsNot Nothing Then
                    For Each Master As BOEvent.cEvent In MasterEventList
                        Select Case Master.EventType.Value
                            Case "QM", "TM", "MM"
                                ProcessMixAndMatch(Master)
                            Case "DG"
                                ProcessDealGroup(Master)
                            Case "TS", "QS", "MS"
                                ProcessEvent(Master)
                            Case "HS"
                                ProcessHierarchy(Master)
                            Case Else
                                ' ignore
                        End Select
                    Next Master
                End If
            Next Header
        End If
        FindForm.Close()
    End Sub

    Private Sub ProcessMandMOriginal(ByVal Master As BOEvent.cEvent, ByVal lstEvtMMGroup As List(Of BOEvent.cEventMixMatch))
        Dim EvtEnq As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)
        Dim strDatc As String = String.Empty
        Dim decDat1 As Decimal = 0
        Dim decDat2 As Decimal = 0
        Dim strDTyp As String = String.Empty
        Dim decDat3 As Decimal = 0

        decDat3 = Master.SpecialPrice.Value
        strDTyp = String.Empty
        If Master.EventType.Value = "MM" Then
            decDat1 = Master.BuyQuantity.Value
            decDat2 = Master.GetQuantity.Value
            If Master.DiscountAmount.Value > 0 Then
                strDTyp = "V"
                decDat3 = Master.DiscountAmount.Value
            End If
            If Master.PercentageDiscount.Value > 0 Then
                strDTyp = "P"
                decDat3 = Master.PercentageDiscount.Value
            End If
        End If
        If Master.EventType.Value = "QM" Then
            decDat1 = CDec(Master.EventKey1.Value)
            strDatc = Master.EventKey1.Value.PadLeft(6, "0"c)
        End If
        If Master.EventType.Value <> "QM" Then
            strDatc = decDat1.ToString.PadLeft(8, "0"c)
        End If
        For Each MMGroup As BOEvent.cEventMixMatch In lstEvtMMGroup
            EvtEnq.AddRecord(Master.Priority.Value, Master.EventNo.Value, String.Empty, Master.EventType.Value, Master.EventKey1.Value, strDatc, MMGroup.PartCode.Value, decDat1, decDat2, strDTyp, decDat3)
        Next
    End Sub

    Private Sub ProcessMixAndMatch(ByVal Master As BOEvent.cEvent)
        Dim MixAndMatchEvent As BOEvent.cEventMixMatch = New BOEvent.cEventMixMatch(_Oasys3DB)
        Dim MixAndMatchEventList As List(Of BOEvent.cEventMixMatch)
        Dim EnquiryEvent As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)
        Dim BuyQuantity As Decimal
        Dim GetQuantity As Decimal
        Dim Price As Decimal
        Dim DiscountType As String

        BuyQuantity = 0
        GetQuantity = 0
        Price = 0
        DiscountType = String.Empty
        Price = Master.SpecialPrice.Value

        If Master.EventType.Value = "MM" Then
            BuyQuantity = Master.BuyQuantity.Value
            GetQuantity = Master.GetQuantity.Value

            If Master.DiscountAmount.Value > 0 Then
                DiscountType = "V"
                Price = Master.DiscountAmount.Value
            End If
            If Master.DiscountPercentage.Value > 0 Then
                DiscountType = "P"
                Price = Master.DiscountPercentage.Value * 10
            End If
        End If

        If Master.EventType.Value = "QM" Then
            BuyQuantity = CDec(Master.EventKey2.Value)               'mix & match buy quantity
        End If

        MixAndMatchEventList = MixAndMatchEvent.GetMixMatchGroup(Master.EventKey1.Value)
        For Each MM As BOEvent.cEventMixMatch In MixAndMatchEventList
            EnquiryEvent.AddRecord(Master.Priority.Value, Master.EventNo.Value, String.Empty, Master.EventType.Value, MM.MixAndMatchGroupNumber.Value, _
                                   BuyQuantity.ToString.PadLeft(8, "0"c), MM.SkuNumber.Value, BuyQuantity, GetQuantity, DiscountType, Price)
        Next
    End Sub

    Private Sub ProcessDealGroupOriginal(ByVal Master As BOEvent.cEvent, ByVal DealGroup As BOEvent.cEventDealGroup)
        Dim EvtEnq As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)
        Dim EvtMMGroup As BOEvent.cEventMixMatch = New BOEvent.cEventMixMatch(_Oasys3DB)
        Dim LstEvtMMGroup As List(Of BOEvent.cEventMixMatch)
        Dim strMmhs As String = String.Empty
        Dim strEtyp As String = String.Empty
        Dim strSKU As String = String.Empty
        Dim strDatc As String = String.Empty
        Dim decDat1 As Decimal = 0
        Dim decDat2 As Decimal = 0
        Dim strDTyp As String = String.Empty
        Dim decDat3 As Decimal = 0

        strDTyp = String.Empty
        decDat1 = DealGroup.Quantity.Value
        decDat2 = 0
        decDat3 = Master.SpecialPrice.Value
        strDatc = decDat1.ToString.PadLeft(8, "0"c)
        If DealGroup.DealType.Value = "S" Then
            strEtyp = "DS"
            strMmhs = "000000"
            strSKU = DealGroup.ItemKey.Value
            EvtEnq.AddRecord(Master.Priority.Value, Master.EventNo.Value, DealGroup.DealGroupNo.Value, strEtyp, strMmhs, strDatc, strSKU, decDat1, decDat2, strDTyp, decDat3)
        End If
        If DealGroup.DealType.Value = "M" Then
            strEtyp = "DM"
            LstEvtMMGroup = EvtMMGroup.GetMixMatchGroup(DealGroup.ItemKey.Value)
            If LstEvtMMGroup IsNot Nothing Then
                For Each MMgroup As BOEvent.cEventMixMatch In LstEvtMMGroup
                    EvtEnq.AddRecord(Master.Priority.Value, Master.EventNo.Value, DealGroup.DealGroupNo.Value, strEtyp, MMgroup.EventMixMatchID.Value, strDatc, MMgroup.PartCode.Value, decDat1, decDat2, strDTyp, decDat3)
                Next
            End If
        End If
    End Sub

    Private Sub ProcessDealGroup(ByRef Master As BOEvent.cEvent)
        Dim DealGroupEvent As New BOEvent.cEventDealGroup(_Oasys3DB)
        Dim DealGroupEventList As List(Of BOEvent.cEventDealGroup)

        Dim MixAndMatchEvent As BOEvent.cEventMixMatch = New BOEvent.cEventMixMatch(_Oasys3DB)
        Dim MixAndMatchEventList As List(Of BOEvent.cEventMixMatch)

        Dim EnquiryEvent As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)

        DealGroupEventList = DealGroupEvent.GetDealGroup(Master.EventNo.Value, Master.EventKey2.Value)
        If DealGroupEventList IsNot Nothing Then
            For Each DG As BOEvent.cEventDealGroup In DealGroupEventList
                If DG.DealType.Value = "M" Then
                    MixAndMatchEventList = MixAndMatchEvent.GetMixMatchGroup(DG.ItemKey.Value)
                    If MixAndMatchEventList IsNot Nothing Then
                        For Each MM As BOEvent.cEventMixMatch In MixAndMatchEventList
                            EnquiryEvent.AddRecord(Master.Priority.Value, DG.EventNo.Value, DG.DealGroupNo.Value, _
                                                   "DM", MM.MixAndMatchGroupNumber.Value, DG.Quantity.Value.ToString.PadLeft(8, "0"c), _
                                                   MM.SkuNumber.Value, DG.Quantity.Value, 0, String.Empty, Master.SpecialPrice.Value)
                        Next
                    End If
                Else
                    EnquiryEvent.AddRecord(Master.Priority.Value, DG.EventNo.Value, DG.DealGroupNo.Value, _
                                           "DS", "000000", DG.Quantity.Value.ToString.PadLeft(8, "0"c), _
                                            DG.ItemKey.Value, DG.Quantity.Value, 0, String.Empty, Master.SpecialPrice.Value)
                End If
            Next
        End If
    End Sub

    Public Sub ProcessEventOriginal(ByVal Master As BOEvent.cEvent)
        Dim EvtEnq As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)

        Dim strMmhs As String = String.Empty
        Dim strDatc As String = String.Empty
        Dim decDat1 As Decimal = 0
        Dim decDat2 As Decimal = 0
        Dim strDTyp As String = String.Empty
        Dim decDat3 As Decimal = 0
        Dim strSKU As String = String.Empty

        decDat3 = Master.SpecialPrice.Value
        strDTyp = String.Empty
        strMmhs = "000000"
        strSKU = Master.EventKey1.Value


        If Master.EventType.Value = "MS" Then
            decDat1 = Master.BuyQuantity.Value
            decDat2 = Master.GetQuantity.Value
            If Master.DiscountAmount.Value > 0 Then
                strDTyp = "V"
                decDat3 = Master.DiscountAmount.Value
            End If
            If Master.PercentageDiscount.Value > 0 Then
                strDTyp = "P"
                decDat3 = Master.PercentageDiscount.Value
            End If
        End If
        If Master.EventType.Value = "QS" Then
            decDat1 = CDec(Master.EventKey2.Value.PadLeft(6, "0"c))
        End If
        strDatc = decDat1.ToString.PadLeft(8, "0"c)
        EvtEnq.AddRecord(Master.Priority.Value, Master.EventNo.Value, String.Empty, Master.EventType.Value, strMmhs, strDatc, strSKU, decDat1, decDat2, strDTyp, decDat3)
    End Sub

    Public Sub ProcessEvent(ByRef Master As BOEvent.cEvent)
        Dim EnquiryEvent As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)

        Dim BuyQuantity As Decimal
        Dim GetQuantity As Decimal
        Dim DiscountType As String
        Dim Price As Decimal

        BuyQuantity = 0
        GetQuantity = 0
        DiscountType = String.Empty
        Price = Master.SpecialPrice.Value

        If Master.EventType.Value = "MS" Then
            BuyQuantity = Master.BuyQuantity.Value
            GetQuantity = Master.GetQuantity.Value

            If Master.DiscountAmount.Value > 0 Then
                DiscountType = "V"
                Price = Master.DiscountAmount.Value
            End If
            If Master.DiscountPercentage.Value > 0 Then
                DiscountType = "P"
                Price = Master.DiscountPercentage.Value * 10
            End If
        End If
        If Master.EventType.Value = "QS" Then BuyQuantity = CDec(Master.EventKey2.Value.PadLeft(6, "0"c))

        EnquiryEvent.AddRecord(Master.Priority.Value, Master.EventNo.Value, String.Empty, Master.EventType.Value, "000000", _
                               BuyQuantity.ToString.PadLeft(8, "0"c), Master.EventKey1.Value, BuyQuantity, GetQuantity, DiscountType, Price)
    End Sub

    Public Sub ProcessHierarchyOriginal(ByVal Master As BOEvent.cEvent)
        Dim EvtEnq As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)
        Dim Evtexcl As BOEvent.cEventHierarchyExcl = New BOEvent.cEventHierarchyExcl(_Oasys3DB)
        Dim stk As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim lststk As List(Of BOStock.cStock)
        Dim strMmhs As String = String.Empty
        Dim strDatc As String = String.Empty
        Dim decDat1 As Decimal = 0
        Dim decDat2 As Decimal = 0
        Dim strDTyp As String = String.Empty
        Dim decDat3 As Decimal = 0
        Dim strSKU As String = String.Empty
        Dim strDlgn As String = String.Empty
        Dim strSKUExcl() As String = {}
        Dim dtskuExcl As DataTable
        Dim count As Integer

        'set up default values 
        decDat3 = Master.SpecialPrice.Value
        strDTyp = String.Empty
        strMmhs = Master.EventKey1.Value
        strDlgn = Master.EventKey2.Value
        decDat1 = CDec(Master.EventKey2.Value)
        decDat2 = Master.SpecialPrice.Value
        If Master.DiscountAmount.Value > 0 Then
            strDTyp = "V"
            decDat3 = Master.DiscountAmount.Value
        End If
        If Master.PercentageDiscount.Value > 0 Then
            strDTyp = "P"
            decDat3 = Master.PercentageDiscount.Value
        End If
        strDatc = decDat1.ToString
        ' get all excluded strings for this heirachy 
        dtskuExcl = Evtexcl.ExcludeOriginal(Master.EventKey1.Value)
        'build array of skus 
        count = 0
        If Not dtskuExcl Is Nothing Then
            ReDim strSKUExcl(dtskuExcl.Rows.Count - 1)
            For Each rec As DataRow In dtskuExcl.Rows
                strSKUExcl(count) = CStr(rec.Item("ITEM"))
                count = count + 1
            Next
        End If
        'get all skus for this Heirachy
        lststk = stk.GetHierachy(Master.EventKey1.Value, strSKUExcl)
        If Not lststk Is Nothing Then
            For Each item As BOStock.cStock In lststk
                EvtEnq.AddRecord(Master.Priority.Value, Master.EventNo.Value, strDlgn, Master.EventType.Value, strMmhs, strDatc, item.SkuNumber.Value, decDat1, decDat2, strDTyp, decDat3)
            Next
        End If
    End Sub

    Public Sub ProcessHierarchy(ByVal Master As BOEvent.cEvent)
        Dim EnquiryEvent As BOEvent.cEventEnquiry = New BOEvent.cEventEnquiry(_Oasys3DB)

        Dim StockExclusion As BOEvent.cEventHierarchyExcl = New BOEvent.cEventHierarchyExcl(_Oasys3DB)
        Dim StockExclusionList As List(Of BOEvent.cEventHierarchyExcl)

        Dim Stock As BOStock.cStock = New BOStock.cStock(_Oasys3DB)
        Dim StockList As List(Of BOStock.cStock)

        Dim DiscountType As String
        Dim Price As Decimal

        Dim ExcludedSkuArray() As String = {}
        Dim intIndex As Integer

        'set up default values
        DiscountType = String.Empty
        Price = Master.SpecialPrice.Value

        If Master.DiscountAmount.Value > 0 Then
            DiscountType = "V"
            Price = Master.DiscountAmount.Value
        End If
        If Master.DiscountPercentage.Value > 0 Then
            DiscountType = "P"
            Price = Master.DiscountPercentage.Value * 10
        End If
        'array with excluded SKUs
        StockExclusionList = StockExclusion.Exclude(Master.EventNo.Value)
        intIndex = 0
        If StockExclusionList IsNot Nothing Then
            ReDim ExcludedSkuArray(StockExclusionList.Count - 1)

            For Each ExcludedSKU As BOEvent.cEventHierarchyExcl In StockExclusionList
                ExcludedSkuArray(intIndex) = ExcludedSKU.SkuNumber.Value
                intIndex += 1
            Next
        End If
        'get SKUs for this heirachy
        StockList = Stock.GetHierachy(Master.EventKey1.Value, ExcludedSkuArray)
        If StockList IsNot Nothing Then
            For Each SKU As BOStock.cStock In StockList
                EnquiryEvent.AddRecord(Master.Priority.Value, Master.EventNo.Value, Master.EventKey2.Value, Master.EventType.Value, _
                                       Master.EventKey1.Value, String.Empty, SKU.SkuNumber.Value, 0, _
                                       Master.SpecialPrice.Value, DiscountType, Price)
            Next
        End If
    End Sub
End Class