﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports Cts.Oasys.Data
Imports StockDetailsExtract.StockDetails

<TestClass()> Public Class StockDetailsExtract_Tests

    Private testContextInstance As TestContext

    Private stockdetailsextractInstance As New StockDetailsExtract
    Private Shared stkadj As New DataTable("STKADJ")
    Private Shared stkadjRow As DataRow = stkadj.NewRow
    Private xmlFilePathParameter As String = "6002"
    Private xmlFileNameParameter As String = "6003"
    Private stockdetailsRepositoryInstance As New StockDetailsRepository



    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        stkadj.Columns.Add("SKUN", System.Type.GetType("System.String"))
        stkadj.Columns.Add("QUAN", System.Type.GetType("System.String"))
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub StockDetailsXMLFileCreated()

        Dim status As Boolean

        stockdetailsextractInstance.StockXmlFileOutputPath = "C:\Wix"
        stockdetailsextractInstance.XmlFileName = "MyTest1.xml"
        stockdetailsextractInstance.StockDataTable = PopulateDataTable()
        StockDetailsExtractFactory.FactorySet(stockdetailsextractInstance)
        StockDetailsExtractFactory.FactoryGet.ExtractStockDetails()
        If System.IO.File.Exists("C:\Wix\MyTest1.xml") Then
            status = True
        End If
        Assert.IsTrue(status)
    End Sub

    <TestMethod()> Public Sub StockDetailsXmlFilePathCheck()

        Dim xmlFilePathExpected As String
        Dim xmlFilePathActual As String

        StockDetailsRepositoryFactory.FactorySet(stockdetailsRepositoryInstance)
        xmlFilePathExpected = GetDetails(xmlFilePathParameter).Rows(0)(0).ToString()
        xmlFilePathActual = StockDetailsRepositoryFactory.FactoryGet.GetXMLFileOutputPath(xmlFilePathParameter).Rows(0)(0).ToString()
        Assert.AreEqual(xmlFilePathExpected, xmlFilePathActual)

    End Sub

    <TestMethod()> Public Sub StockDetailsXmlFileNameCheck()

        Dim xmlFileNameExpected As String
        Dim xmlFileNameActual As String

        StockDetailsRepositoryFactory.FactorySet(stockdetailsRepositoryInstance)
        xmlFileNameExpected = GetDetails(xmlFileNameParameter).Rows(0)(0).ToString()
        xmlFileNameActual = StockDetailsRepositoryFactory.FactoryGet.GetXMLFileName(xmlFileNameParameter).Rows(0)(0).ToString()
        Assert.AreEqual(xmlFileNameExpected, xmlFileNameActual)

    End Sub


    Private Function PopulateDataTable() As DataTable
        stkadjRow("SKUN") = "100803"
        stkadjRow("QUAN") = 10
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("SKUN") = "100804"
        stkadjRow("QUAN") = 50
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("SKUN") = "100805"
        stkadjRow("QUAN") = 40
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("SKUN") = "100806"
        stkadjRow("QUAN") = 30
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("SKUN") = "100807"
        stkadjRow("QUAN") = 20
        stkadj.Rows.Add(stkadjRow)

        Return stkadj

    End Function

    Private Function GetDetails(ByVal parameterid As String) As DataTable

        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "SELECT StringValue FROM PARAMETERS WHERE PARAMETERID = " & parameterid
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt
            End Using
        End Using
        Return Nothing
    End Function

End Class
