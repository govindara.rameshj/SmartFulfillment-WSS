﻿Public Class HPSTIReport

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        _oasys3Db = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        InitializeStoreInfo()
    End Sub

    Private _storeNumber As String
    Private _storeName As String
    ReadOnly _oasys3Db As OasysDBBO.Oasys3.DB.clsOasys3DB

    Private Sub InitializeStoreInfo()
        Dim retopt As New BOSystem.cRetailOptions(_oasys3Db)
        retopt.AddLoadField(retopt.Store)
        retopt.AddLoadField(retopt.StoreName)
        retopt.AddLoadField(retopt.NoVATrates)
        retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, retopt.RetailOptionsID, "01")
        retopt.LoadMatches()
        _storeNumber = retopt.Store.Value.ToString.PadLeft(3, "0"c)
        _storeName = retopt.StoreName.Value.PadRight(30, " "c)
    End Sub

    Protected Overrides Sub DoProcessing()
        Hide()
        Dim hpstiReportData = CreateHpstiTable()
        FillReportData(hpstiReportData)

        If (hpstiReportData.Rows.Count > 0) Then
            GridControl1.DataSource = hpstiReportData
            ShowPreviewForm()
            FindForm.Close()
        Else
            MsgBox("Report can't be built up until the Process IW is executed", MsgBoxStyle.Exclamation, "Unable to create Container Summary report")
            FindForm.Close()
        End If
    End Sub

    Private Sub ShowPreviewForm()
        Dim Retopt As New BOSystem.cRetailOptions(_oasys3Db)
        Retopt.AddLoadField(Retopt.Store)
        Retopt.AddLoadField(Retopt.StoreName)
        Retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Retopt.RetailOptionsID, "01")
        Retopt.LoadMatches()
        Dim ctsPrint As New ReportPrint(GridControl1, "Container Summary", "STORE COPY", _storeNumber & "-" & _storeName, "", True)
        ctsPrint.ShowPreviewDialog()

        ctsPrint = New ReportPrint(GridControl1, "Container Summary", "DRIVERS COPY", _storeNumber & "-" & _storeName, "", True)
        ctsPrint.ShowPreviewDialog()
    End Sub

    Private Function CreateHpstiTable() As DataTable
        Dim hpstiTable As New DataTable
        hpstiTable.Columns.Add(New DataColumn("DeliveryDate"))
        hpstiTable.Columns.Add(New DataColumn("ContainerNo"))
        hpstiTable.Columns.Add(New DataColumn("ContainerType"))
        hpstiTable.Columns.Add(New DataColumn("NoLines", GetType(System.Int32)))
        hpstiTable.Columns.Add(New DataColumn("ParentContainer"))
        hpstiTable.Columns.Add(New DataColumn("Received"))
        hpstiTable.Columns.Add(New DataColumn("Value", GetType(System.Decimal)))
        Return hpstiTable
    End Function

    Private Sub FillReportData(ByRef hpstiReportData As DataTable)
        Dim consum As New BOPurchases.cContainerSummary(_oasys3Db)
        consum.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, consum.IsLastProcessed, True)
        Dim matches = consum.LoadMatches()
        For Each match As BOPurchases.cContainerSummary In matches
            Dim conSummary As DataRow = hpstiReportData.NewRow
            conSummary.Item("ContainerNo") = match.AssemblyDepotNumber.Value & "/" & match.Number.Value
            conSummary.Item("ContainerType") = match.Description.Value
            conSummary.Item("NoLines") = match.TotalLines.Value
            conSummary.Item("ParentContainer") = IIf(CLng(match.ParentNumber.Value) = 0, "", match.ParentNumber.Value)
            conSummary.Item("Received") = "N"
            conSummary.Item("Value") = match.Value.Value
            conSummary.Item("DeliveryDate") = match.DeliveryDate.Value.ToString("dd/MM/yy")
            hpstiReportData.Rows.Add(conSummary)
        Next
    End Sub
End Class