﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HPSTIReport
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.DelDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ContainerNo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ContainerType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.NoLines = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ParentContainer = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Received = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Value = New DevExpress.XtraGrid.Columns.GridColumn
        Me.cmdPrint = New System.Windows.Forms.Button
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(12, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(724, 200)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.AppearancePrint.FooterPanel.BackColor = System.Drawing.Color.White
        Me.GridView1.AppearancePrint.FooterPanel.Options.UseBackColor = True
        Me.GridView1.AppearancePrint.GroupFooter.BackColor = System.Drawing.Color.White
        Me.GridView1.AppearancePrint.GroupFooter.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.DelDate, Me.ContainerNo, Me.ContainerType, Me.NoLines, Me.ParentContainer, Me.Received, Me.Value})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowQuickHideColumns = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsPrint.UsePrintStyles = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'DelDate
        '
        Me.DelDate.Caption = "Delivery Date"
        Me.DelDate.FieldName = "DeliveryDate"
        Me.DelDate.MaxWidth = 80
        Me.DelDate.MinWidth = 80
        Me.DelDate.Name = "DelDate"
        Me.DelDate.Visible = True
        Me.DelDate.VisibleIndex = 0
        Me.DelDate.Width = 80
        '
        'ContainerNo
        '
        Me.ContainerNo.Caption = "Container No"
        Me.ContainerNo.FieldName = "ContainerNo"
        Me.ContainerNo.MaxWidth = 90
        Me.ContainerNo.MinWidth = 90
        Me.ContainerNo.Name = "ContainerNo"
        Me.ContainerNo.Visible = True
        Me.ContainerNo.VisibleIndex = 1
        Me.ContainerNo.Width = 90
        '
        'ContainerType
        '
        Me.ContainerType.Caption = "Container Type"
        Me.ContainerType.FieldName = "ContainerType"
        Me.ContainerType.MaxWidth = 200
        Me.ContainerType.MinWidth = 200
        Me.ContainerType.Name = "ContainerType"
        Me.ContainerType.Visible = True
        Me.ContainerType.VisibleIndex = 2
        Me.ContainerType.Width = 200
        '
        'NoLines
        '
        Me.NoLines.Caption = "No of Lines"
        Me.NoLines.FieldName = "NoLines"
        Me.NoLines.MaxWidth = 75
        Me.NoLines.MinWidth = 75
        Me.NoLines.Name = "NoLines"
        Me.NoLines.Visible = True
        Me.NoLines.VisibleIndex = 3
        '
        'ParentContainer
        '
        Me.ParentContainer.Caption = "Parent Container"
        Me.ParentContainer.FieldName = "ParentContainer"
        Me.ParentContainer.MaxWidth = 100
        Me.ParentContainer.MinWidth = 100
        Me.ParentContainer.Name = "ParentContainer"
        Me.ParentContainer.Visible = True
        Me.ParentContainer.VisibleIndex = 4
        Me.ParentContainer.Width = 100
        '
        'Received
        '
        Me.Received.AppearanceCell.Options.UseTextOptions = True
        Me.Received.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Received.Caption = "Received"
        Me.Received.FieldName = "Received"
        Me.Received.MaxWidth = 60
        Me.Received.MinWidth = 60
        Me.Received.Name = "Received"
        Me.Received.Visible = True
        Me.Received.VisibleIndex = 5
        Me.Received.Width = 60
        '
        'Value
        '
        Me.Value.Caption = "Value"
        Me.Value.DisplayFormat.FormatString = "c"
        Me.Value.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Value.FieldName = "Value"
        Me.Value.MaxWidth = 70
        Me.Value.MinWidth = 70
        Me.Value.Name = "Value"
        Me.Value.Visible = True
        Me.Value.VisibleIndex = 6
        Me.Value.Width = 70
        '
        'cmdPrint
        '
        Me.cmdPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdPrint.Location = New System.Drawing.Point(661, 218)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(75, 23)
        Me.cmdPrint.TabIndex = 1
        Me.cmdPrint.Text = "F9-Print"
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'HPSTIReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(742, 246)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "HPSTIReport"
        Me.Text = "HPSTIReport"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DelDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ContainerNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ContainerType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NoLines As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ParentContainer As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Received As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Value As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
End Class
