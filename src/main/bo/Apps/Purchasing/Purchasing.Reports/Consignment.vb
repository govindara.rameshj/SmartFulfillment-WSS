﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Text
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns
Imports System.Linq.Expressions
Imports Purchasing.Core

Public Class Consignment
    Private _report As CompositeLink
    Private _storeIdName As String = String.Empty
    Private _order As Order
    Private _consignment As Core.Consignment

    Public Sub New(ByVal storeIdName As String, ByVal order As Order)

        _storeIdName = storeIdName
        _order = order
        _consignment = Core.Consignment.GetConsignment(CStr(_order.ConsignNumber))

        'set up supplier copy report
        _report = New CompositeLink(New PrintingSystem)
        _report.Margins.Bottom = 70
        _report.Margins.Top = 50
        _report.Margins.Left = 50
        _report.Margins.Right = 50

        'create title link and add all report handlers
        Dim title As New Link
        AddHandler title.CreateDetailArea, AddressOf Title_CreateReportDetailArea
        AddHandler _report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        'create grid
        Dim grid As New GridControl
        Dim view As New GridView
        grid.MainView = view
        grid.Name = "grid"
        grid.ViewCollection.Add(view)
        grid.Parent = New Form

        view.GridControl = grid
        view.Name = "view"
        view.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        view.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        view.Appearance.HeaderPanel.Options.UseFont = True
        view.Appearance.HeaderPanel.Options.UseTextOptions = True
        view.OptionsBehavior.Editable = False
        view.OptionsDetail.ShowDetailTabs = False
        view.OptionsView.ColumnAutoWidth = False

        'set data source and sort out columns
        grid.DataSource = order.Lines

        For Each col As GridColumn In view.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As OrderLine) f.Id)
                    col.Caption = "ID"
                    col.MinWidth = 50
                    col.MaxWidth = 50
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                Case GetPropertyName(Function(f As OrderLine) f.SkuNumber)
                    col.Caption = "SKU"
                    col.MinWidth = 80
                    col.MaxWidth = 80
                Case GetPropertyName(Function(f As OrderLine) f.SkuDescription)
                    col.Caption = "Description"
                Case GetPropertyName(Function(f As OrderLine) f.SkuProductCode)
                    col.MinWidth = 100
                    col.MaxWidth = 100
                Case GetPropertyName(Function(f As OrderLine) f.Price)
                    col.MaxWidth = 80
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = ChrW(163) & "#0.00"
                Case GetPropertyName(Function(f As OrderLine) f.OrderQty)
                    col.MaxWidth = 80
                Case Else : col.Visible = False
            End Select
        Next
        view.BestFitColumns()

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = grid

        'populate the collection of links in the composite link in order
        _report.Links.Add(title)
        _report.Links.Add(reportGrid)

    End Sub

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw store number and name
        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _storeIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:MM/dd/yyyy HH:mm}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub

    Private Sub Title_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'set variables
        Dim sectionWidth As Integer = CInt(e.Graph.ClientPageSize.Width)
        Dim fontType As String = "Tahoma"
        Dim fontSize As Integer = 16
        Dim titleHeight As Integer = 30
        Dim headerHeight As Integer = 100

        'draw report title
        Dim rec As RectangleF = New RectangleF(0, 0, sectionWidth, titleHeight)
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font(fontType, fontSize, FontStyle.Bold)
        e.Graph.DrawString("Consignment Report", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        'reset variables
        fontSize = 10
        sectionWidth = CInt(CLng(e.Graph.ClientPageSize.Width) \ 6)

        'draw near side header info titles
        Dim sb As New StringBuilder
        sb.Append("Consignment:" & Environment.NewLine)
        sb.Append("PO Number:" & Environment.NewLine)
        sb.Append("SOQ Number:" & Environment.NewLine)
        sb.Append("Supplier:" & Environment.NewLine)
        sb.Append("Delivery Notes:")

        rec = New RectangleF(0, titleHeight, sectionWidth, headerHeight)
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font(fontType, fontSize, FontStyle.Bold)
        e.Graph.DrawString(sb.ToString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        'draw near side info
        sb = New StringBuilder
        sb.Append(_order.ConsignNumber & Environment.NewLine)
        sb.Append(_order.PoNumber & Environment.NewLine)
        sb.Append(_order.SoqNumber & Environment.NewLine)
        sb.Append(_order.SupplierNumber & Space(1) & _order.SupplierName & Environment.NewLine)
        If _consignment IsNot Nothing Then sb.Append(_consignment.DeliveryNotesString)

        rec = New RectangleF(sectionWidth, titleHeight, sectionWidth * 3, headerHeight)
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font(fontType, fontSize, FontStyle.Regular)
        e.Graph.DrawString(sb.ToString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        'draw far side header info titles
        sb = New StringBuilder
        sb.Append("Employee ID:" & Environment.NewLine)
        sb.Append("Pallets In:" & Environment.NewLine)
        sb.Append("Pallets Out:")

        rec = New RectangleF(sectionWidth * 4, titleHeight, sectionWidth, headerHeight)
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font(fontType, fontSize, FontStyle.Bold)
        e.Graph.DrawString(sb.ToString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

        'draw far side info
        sb = New StringBuilder
        If _consignment IsNot Nothing Then
            sb.Append(_consignment.EmployeeId & Environment.NewLine)
            sb.Append(_consignment.PalletsIn & Environment.NewLine)
            sb.Append(_consignment.PalletsOut)
        End If

        rec = New RectangleF(sectionWidth * 5, 30, sectionWidth, headerHeight)
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near)
        e.Graph.Font = New Font(fontType, fontSize, FontStyle.Regular)
        e.Graph.DrawString(sb.ToString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

    End Sub


    Public Sub ShowPreviewDialog()

        _report.ShowPreviewDialog()

    End Sub

    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'properyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

End Class
