﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports Purchasing.Core
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns
Imports System.Linq.Expressions
Imports DevExpress.XtraGrid.Views.BandedGrid
Imports DevExpress.XtraGrid.Views.Base
Imports System.Windows.Forms
Imports System.Text
Imports Cts.Oasys.Core.System

Public Class ReceiptReport
    Implements IDisposable
    Private _report As CompositeLink
    Private _headerMain As String = "Purchase Orders"
    Private _headerSub As String = "DRL Print"
    Private _storeIdName As String = String.Empty
    Private _userIdName As String = String.Empty
    Private _workstationId As Integer
    Private _receipt As Receipt


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    Public Sub New(ByVal receipt As Receipt, ByVal userId As Integer, ByVal workstationId As Integer)
        _storeIdName = Store.GetIdName
        _receipt = receipt
        _userIdName = User.GetUserIdName(userId, IncludeExternalIds:=True)
        _workstationId = workstationId
        Initialise()
    End Sub

    Private Sub Initialise()

        'set up supplier copy report
        _report = New CompositeLink(New PrintingSystem)
        _report.Margins.Bottom = 70
        _report.Margins.Top = 70
        _report.Margins.Left = 50
        _report.Margins.Right = 50
        _report.Landscape = False
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler _report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea
        _report.Links.Add(reportTitle)

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = CreateGrid()
        _report.Links.Add(reportGrid)

    End Sub

    Private Function CreateGrid() As GridControl

        'create grid
        Dim grid As New GridControl
        Dim view As New GridView
        grid.MainView = view
        grid.Name = "grid"
        grid.ViewCollection.Add(view)
        grid.Parent = New Form
        view.GridControl = grid
        view.Name = "view"
        view.AppearancePrint.HeaderPanel.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        view.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        view.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        view.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        view.OptionsPrint.UsePrintStyles = True
        view.ColumnPanelRowHeight = 30

        'set data source and sort out columns
        grid.DataSource = _receipt.Lines
        grid.ForceInitialize()

        For Each col As GridColumn In view.Columns
            col.Visible = True

            Select Case col.FieldName
                Case GetPropertyName(Function(f As ReceiptLine) f.Sequence)
                Case GetPropertyName(Function(f As ReceiptLine) f.SkuNumber)
                Case GetPropertyName(Function(f As ReceiptLine) f.SkuDescription)
                Case GetPropertyName(Function(f As ReceiptLine) f.OrderPrice)
                Case GetPropertyName(Function(f As ReceiptLine) f.OrderQty)
                Case GetPropertyName(Function(f As ReceiptLine) f.ReceivedPrice)
                Case GetPropertyName(Function(f As ReceiptLine) f.ReceivedQty)
                Case GetPropertyName(Function(f As ReceiptLine) f.ReturnQty)
                Case GetPropertyName(Function(f As ReceiptLine) f.ReasonCode)
                Case Else : col.Visible = False
            End Select
        Next

        view.BestFitColumns()
        Return grid

    End Function



    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(_headerMain, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(_headerSub, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        tb = e.Graph.DrawString("Date: " & Now.Date.ToShortDateString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & _userIdName & Space(2) & "WS: " & _workstationId & Space(2) & "Time: {0:HH:mm}")
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'set variables
        Dim x As Integer = 0
        Dim h As Integer = 70

        'order type header column
        Dim sb As New StringBuilder
        sb.Append("PO Number:" & Environment.NewLine)
        sb.Append("Supplier:" & Environment.NewLine)
        sb.Append("DRL Number:" & Environment.NewLine)
        sb.Append("Consignment:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 90, h, StringAlignment.Near, BorderSide.None, True)

        'order type info column
        sb = New StringBuilder
        If _receipt.PoNumber.HasValue Then sb.Append(_receipt.PoNumber)
        sb.Append(Environment.NewLine)
        sb.Append(_receipt.PoSupplierNumber & Space(1) & _receipt.PoSupplierName.Trim & Environment.NewLine)
        sb.Append(_receipt.Number & Environment.NewLine)
        If _receipt.PoConsignNumber.HasValue Then sb.Append(_receipt.PoConsignNumber)
        DrawTextRectangle(e.Graph, sb.ToString, x, 200, h, StringAlignment.Near, BorderSide.None)

        'soq control header column
        sb = New StringBuilder
        sb.Append("Order Date:" & Environment.NewLine)
        sb.Append("Received Date:" & Environment.NewLine)
        sb.Append("Delivery Notes:" & Environment.NewLine)
        sb.Append("Comment:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 100, h, StringAlignment.Near, BorderSide.None, True)

        'soq control info column
        sb = New StringBuilder
        sb.Append(_receipt.PoOrderDate & Environment.NewLine)
        sb.Append(_receipt.DateReceipt & Environment.NewLine)
        sb.Append(_receipt.PoDeliveryNoteString & Environment.NewLine)
        sb.Append(_receipt.Comments)
        DrawTextRectangle(e.Graph, sb.ToString, x, 100, h, StringAlignment.Near, BorderSide.None)

        'units/value label
        sb = New StringBuilder
        sb.Append("Units Rec/Ord:" & Environment.NewLine)
        sb.Append("Value Rec/Ord:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 100, h, StringAlignment.Far, BorderSide.None, True)

        'units
        sb = New StringBuilder
        sb.Append(_receipt.Lines.ReceivedQty.ToString("n0") & Environment.NewLine)
        sb.Append(_receipt.Lines.OrderedQty.ToString("n0"))
        DrawTextRectangle(e.Graph, sb.ToString, x, 50, h, StringAlignment.Far, BorderSide.None)

        'value
        sb = New StringBuilder
        sb.Append(_receipt.Lines.ReceivedValue.ToString("c2") & Environment.NewLine)
        sb.Append(_receipt.Lines.OrderedValue.ToString("c2"))
        DrawTextRectangle(e.Graph, sb.ToString, x, 80, h, StringAlignment.Far, BorderSide.None)


    End Sub

    Private Sub DrawTextRectangle(ByRef graph As BrickGraphics, ByVal text As String, ByRef x As Integer, ByVal width As Integer, ByVal height As Integer, ByVal align As StringAlignment, ByVal sides As DevExpress.XtraPrinting.BorderSide, Optional ByVal bold As Boolean = False)

        Dim rec As New RectangleF(x, 0, width, height)
        graph.StringFormat = New BrickStringFormat(align)
        If bold Then
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        Else
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Regular)
        End If
        graph.DrawString(text, Color.Black, rec, sides)
        x += width

    End Sub



    Public Sub ShowPreviewDialog()

        _report.ShowPreviewDialog()

    End Sub

    Public Sub Print()
        _report.PrintingSystem.Print()
    End Sub

    Public Sub CreateDocument()
        _report.CreateDocument()
    End Sub


    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'properyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _report.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
