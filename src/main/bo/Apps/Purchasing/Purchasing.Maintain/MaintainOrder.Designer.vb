<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MaintainOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblValueReceived = New System.Windows.Forms.Label()
        Me.lblValueOrdered = New System.Windows.Forms.Label()
        Me.lblUnitsReceived = New System.Windows.Forms.Label()
        Me.lblUnitsOrdered = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblPO = New System.Windows.Forms.Label()
        Me.lblDateOrder = New System.Windows.Forms.Label()
        Me.lblDRL = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblDateReceived = New System.Windows.Forms.Label()
        Me.lblConsign = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.lblSupplier = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.xgridLines = New DevExpress.XtraGrid.GridControl()
        Me.xviewLines = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lblDeliveryNotes = New System.Windows.Forms.Label()
        Me.btnDeliveryNotes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnUpdate = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStockEnquiry = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.xgridLines, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewLines, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblValueReceived
        '
        Me.lblValueReceived.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblValueReceived.Location = New System.Drawing.Point(804, 29)
        Me.lblValueReceived.Name = "lblValueReceived"
        Me.lblValueReceived.Size = New System.Drawing.Size(70, 20)
        Me.lblValueReceived.TabIndex = 116
        Me.lblValueReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblValueOrdered
        '
        Me.lblValueOrdered.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblValueOrdered.Location = New System.Drawing.Point(880, 29)
        Me.lblValueOrdered.Name = "lblValueOrdered"
        Me.lblValueOrdered.Size = New System.Drawing.Size(56, 20)
        Me.lblValueOrdered.TabIndex = 115
        Me.lblValueOrdered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUnitsReceived
        '
        Me.lblUnitsReceived.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUnitsReceived.Location = New System.Drawing.Point(804, 9)
        Me.lblUnitsReceived.Name = "lblUnitsReceived"
        Me.lblUnitsReceived.Size = New System.Drawing.Size(70, 20)
        Me.lblUnitsReceived.TabIndex = 114
        Me.lblUnitsReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUnitsOrdered
        '
        Me.lblUnitsOrdered.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUnitsOrdered.Location = New System.Drawing.Point(880, 9)
        Me.lblUnitsOrdered.Name = "lblUnitsOrdered"
        Me.lblUnitsOrdered.Size = New System.Drawing.Size(56, 20)
        Me.lblUnitsOrdered.TabIndex = 113
        Me.lblUnitsOrdered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(625, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(173, 20)
        Me.Label13.TabIndex = 110
        Me.Label13.Text = "Value Received / Ordered"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(625, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(173, 20)
        Me.Label14.TabIndex = 109
        Me.Label14.Text = "Units Received / Ordered"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(7, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 20)
        Me.Label16.TabIndex = 106
        Me.Label16.Text = "PO Number"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(311, 29)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(98, 20)
        Me.Label18.TabIndex = 103
        Me.Label18.Text = "Received Date"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPO
        '
        Me.lblPO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPO.Location = New System.Drawing.Point(95, 9)
        Me.lblPO.Name = "lblPO"
        Me.lblPO.Size = New System.Drawing.Size(210, 20)
        Me.lblPO.TabIndex = 102
        Me.lblPO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateOrder
        '
        Me.lblDateOrder.Location = New System.Drawing.Point(415, 9)
        Me.lblDateOrder.Name = "lblDateOrder"
        Me.lblDateOrder.Size = New System.Drawing.Size(181, 18)
        Me.lblDateOrder.TabIndex = 101
        Me.lblDateOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDRL
        '
        Me.lblDRL.Location = New System.Drawing.Point(95, 49)
        Me.lblDRL.Name = "lblDRL"
        Me.lblDRL.Size = New System.Drawing.Size(210, 20)
        Me.lblDRL.TabIndex = 100
        Me.lblDRL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(311, 9)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(98, 20)
        Me.Label23.TabIndex = 98
        Me.Label23.Text = "Order Date"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(7, 49)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(82, 20)
        Me.Label24.TabIndex = 97
        Me.Label24.Text = "DRL Number"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateReceived
        '
        Me.lblDateReceived.Location = New System.Drawing.Point(415, 29)
        Me.lblDateReceived.Name = "lblDateReceived"
        Me.lblDateReceived.Size = New System.Drawing.Size(181, 18)
        Me.lblDateReceived.TabIndex = 119
        Me.lblDateReceived.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblConsign
        '
        Me.lblConsign.Location = New System.Drawing.Point(95, 69)
        Me.lblConsign.Name = "lblConsign"
        Me.lblConsign.Size = New System.Drawing.Size(210, 20)
        Me.lblConsign.TabIndex = 123
        Me.lblConsign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 122
        Me.Label3.Text = "Consignment"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(311, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 120
        Me.Label1.Text = "Delivery Notes"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(311, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 20)
        Me.Label2.TabIndex = 125
        Me.Label2.Text = "Comment"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComment
        '
        Me.txtComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComment.Location = New System.Drawing.Point(415, 69)
        Me.txtComment.MaxLength = 20
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(181, 20)
        Me.txtComment.TabIndex = 0
        '
        'lblSupplier
        '
        Me.lblSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupplier.Location = New System.Drawing.Point(95, 29)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(210, 20)
        Me.lblSupplier.TabIndex = 99
        Me.lblSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(82, 20)
        Me.Label25.TabIndex = 95
        Me.Label25.Text = "Supplier"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'xgridLines
        '
        Me.xgridLines.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridLines.Location = New System.Drawing.Point(10, 96)
        Me.xgridLines.MainView = Me.xviewLines
        Me.xgridLines.Name = "xgridLines"
        Me.xgridLines.Size = New System.Drawing.Size(926, 382)
        Me.xgridLines.TabIndex = 127
        Me.xgridLines.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewLines, Me.GridView2})
        '
        'xviewLines
        '
        Me.xviewLines.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewLines.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewLines.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewLines.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewLines.GridControl = Me.xgridLines
        Me.xviewLines.Name = "xviewLines"
        Me.xviewLines.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.xviewLines.OptionsView.ShowGroupPanel = False
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.xgridLines
        Me.GridView2.Name = "GridView2"
        '
        'lblDeliveryNotes
        '
        Me.lblDeliveryNotes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeliveryNotes.Location = New System.Drawing.Point(415, 50)
        Me.lblDeliveryNotes.Name = "lblDeliveryNotes"
        Me.lblDeliveryNotes.Size = New System.Drawing.Size(426, 18)
        Me.lblDeliveryNotes.TabIndex = 128
        Me.lblDeliveryNotes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDeliveryNotes
        '
        Me.btnDeliveryNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeliveryNotes.Appearance.Options.UseTextOptions = True
        Me.btnDeliveryNotes.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnDeliveryNotes.Location = New System.Drawing.Point(847, 52)
        Me.btnDeliveryNotes.Name = "btnDeliveryNotes"
        Me.btnDeliveryNotes.Size = New System.Drawing.Size(89, 39)
        Me.btnDeliveryNotes.TabIndex = 129
        Me.btnDeliveryNotes.Text = "F4 Maintain Delivery Notes"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(859, 484)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(77, 39)
        Me.btnCancel.TabIndex = 130
        Me.btnCancel.Text = "F10 Cancel"
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUpdate.Enabled = False
        Me.btnUpdate.Location = New System.Drawing.Point(93, 484)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(77, 39)
        Me.btnUpdate.TabIndex = 131
        Me.btnUpdate.Text = "F5 Update"
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Appearance.Options.UseTextOptions = True
        Me.btnStockEnquiry.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.btnStockEnquiry.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnStockEnquiry.Location = New System.Drawing.Point(10, 484)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(77, 39)
        Me.btnStockEnquiry.TabIndex = 132
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        '
        'MaintainOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 535)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDeliveryNotes)
        Me.Controls.Add(Me.lblDeliveryNotes)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.lblConsign)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDateReceived)
        Me.Controls.Add(Me.lblValueReceived)
        Me.Controls.Add(Me.lblValueOrdered)
        Me.Controls.Add(Me.lblUnitsReceived)
        Me.Controls.Add(Me.lblUnitsOrdered)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.lblPO)
        Me.Controls.Add(Me.lblDateOrder)
        Me.Controls.Add(Me.lblDRL)
        Me.Controls.Add(Me.lblSupplier)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.xgridLines)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(956, 543)
        Me.Name = "MaintainOrder"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Maintain DRL"
        CType(Me.xgridLines, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewLines, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblValueReceived As System.Windows.Forms.Label
    Friend WithEvents lblValueOrdered As System.Windows.Forms.Label
    Friend WithEvents lblUnitsReceived As System.Windows.Forms.Label
    Friend WithEvents lblUnitsOrdered As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblPO As System.Windows.Forms.Label
    Friend WithEvents lblDateOrder As System.Windows.Forms.Label
    Friend WithEvents lblDRL As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblDateReceived As System.Windows.Forms.Label
    Friend WithEvents lblConsign As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents lblSupplier As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents xgridLines As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewLines As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lblDeliveryNotes As System.Windows.Forms.Label
    Friend WithEvents btnDeliveryNotes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStockEnquiry As DevExpress.XtraEditors.SimpleButton
End Class
