Imports Cts.Oasys.Core
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports Purchasing.Core
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns

Public Class Maintain

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnMaintain.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            DisplayStatus(My.Resources.Strings.GetReceipts)
            LoadReceipts()
            LoadReceiptLines()

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub


    Private Sub LoadReceipts()

        xgridMaintain.DataSource = Purchasing.Core.Receipt.GetMaintablePurchaseReceipts

        'set up column widths
        For Each col As GridColumn In xviewMaintain.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Receipt) f.Number)
                Case GetPropertyName(Function(f As Receipt) f.PoNumber) : col.Caption = My.Resources.ColumnHeaders.PoNumber
                Case GetPropertyName(Function(f As Receipt) f.PoConsignNumber) : col.Caption = My.Resources.ColumnHeaders.PoConsign
                Case GetPropertyName(Function(f As Receipt) f.PoSupplierNumber) : col.Caption = My.Resources.ColumnHeaders.Supplier
                Case GetPropertyName(Function(f As Receipt) f.PoSupplierName) : col.Caption = My.Resources.ColumnHeaders.Name
                Case GetPropertyName(Function(f As Receipt) f.PoOrderDate)
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.Caption = My.Resources.ColumnHeaders.DateOrdered
                Case GetPropertyName(Function(f As Receipt) f.DateReceipt)
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.Caption = My.Resources.ColumnHeaders.DateReceived
                Case GetPropertyName(Function(f As Receipt) f.Value)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    col.MinWidth = 60
                Case Else : col.Visible = False
            End Select
        Next

        xviewMaintain.BestFitColumns()
        xviewMaintain.Focus()

    End Sub

    Private Sub LoadReceiptLines()

        Dim view As New GridView(xgridMaintain)
        xgridMaintain.LevelTree.Nodes.Add(GetPropertyName(Function(f As Receipt) f.Lines), view)

        Dim skuNumber As String = GetPropertyName(Function(f As ReceiptLine) f.SkuNumber)
        view.Columns.AddField(skuNumber).VisibleIndex = 0
        view.Columns(skuNumber).Caption = My.Resources.ColumnHeaders.SkuNumber
        view.Columns(skuNumber).MinWidth = 60
        view.Columns(skuNumber).MaxWidth = 60

        Dim skuDescription As String = GetPropertyName(Function(f As ReceiptLine) f.SkuDescription)
        view.Columns.AddField(skuDescription).VisibleIndex = 1
        view.Columns(skuDescription).Caption = My.Resources.ColumnHeaders.Description
        view.Columns(skuDescription).MinWidth = 120

        Dim productCode As String = GetPropertyName(Function(f As ReceiptLine) f.SkuProductCode)
        view.Columns.AddField(productCode).VisibleIndex = 2
        view.Columns(productCode).Caption = My.Resources.ColumnHeaders.ProductCode
        view.Columns(productCode).MinWidth = 100
        view.Columns(productCode).MaxWidth = 100

        Dim packSize As String = GetPropertyName(Function(f As ReceiptLine) f.SkuPack)
        view.Columns.AddField(packSize).VisibleIndex = 3
        view.Columns(packSize).Caption = My.Resources.ColumnHeaders.PackSize
        view.Columns(packSize).MinWidth = 80
        view.Columns(packSize).MaxWidth = 80

        Dim orderPrice As String = GetPropertyName(Function(f As ReceiptLine) f.OrderPrice)
        view.Columns.AddField(orderPrice).VisibleIndex = 5
        view.Columns(orderPrice).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        view.Columns(orderPrice).DisplayFormat.FormatString = "c2"
        view.Columns(orderPrice).MinWidth = 80

        Dim orderQty As String = GetPropertyName(Function(f As ReceiptLine) f.OrderQty)
        view.Columns.AddField(orderQty).VisibleIndex = 6
        view.Columns(orderQty).MinWidth = 80

        Dim receivedQty As String = GetPropertyName(Function(f As ReceiptLine) f.ReceivedQty)
        view.Columns.AddField(receivedQty).VisibleIndex = 7
        view.Columns(receivedQty).MinWidth = 80

        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.Appearance.HeaderPanel.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        view.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub


    Private Sub btnMaintain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMaintain.Click

        'get receipt from datasource
        Dim receipts = CType(xgridMaintain.DataSource, BindingList(Of Receipt))
        Dim receipt As Receipt = receipts(xviewMaintain.GetDataSourceRowIndex(xviewMaintain.GetSelectedRows(0)))

        Using order As New MaintainOrder(receipt, UserId)
            order.ShowDialog(Me)
            RefreshGrid()
        End Using

    End Sub

    Private Sub RefreshGrid()
        xgridMaintain.RefreshDataSource()
        Dim focusedRowHandle = xviewMaintain.FocusedRowHandle
        If (xviewMaintain.GetMasterRowExpanded(focusedRowHandle)) Then
            xviewMaintain.SetMasterRowExpanded(xviewMaintain.FocusedRowHandle, False)
            xviewMaintain.SetMasterRowExpanded(xviewMaintain.FocusedRowHandle, True)
        End If
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        'get receipt from datasource
        Dim receipts = CType(xgridMaintain.DataSource, BindingList(Of Receipt))
        Dim receipt As Receipt = receipts(xviewMaintain.GetDataSourceRowIndex(xviewMaintain.GetSelectedRows(0)))

        Dim report As New Purchasing.Reports.ReceiptReport(receipt, UserId, WorkstationId)
        report.ShowPreviewDialog()

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class
