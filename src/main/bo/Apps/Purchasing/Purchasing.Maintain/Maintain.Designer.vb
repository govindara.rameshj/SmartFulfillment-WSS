<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Maintain
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Maintain))
        Me.xgridMaintain = New DevExpress.XtraGrid.GridControl
        Me.xviewMaintain = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton
        Me.btnMaintain = New DevExpress.XtraEditors.SimpleButton
        Me.btnPrint = New DevExpress.XtraEditors.SimpleButton
        CType(Me.xgridMaintain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewMaintain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xgridMaintain
        '
        Me.xgridMaintain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridMaintain.Location = New System.Drawing.Point(3, 3)
        Me.xgridMaintain.MainView = Me.xviewMaintain
        Me.xgridMaintain.Name = "xgridMaintain"
        Me.xgridMaintain.Size = New System.Drawing.Size(794, 499)
        Me.xgridMaintain.TabIndex = 4
        Me.xgridMaintain.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewMaintain})
        '
        'xviewMaintain
        '
        Me.xviewMaintain.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewMaintain.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewMaintain.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewMaintain.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewMaintain.GridControl = Me.xgridMaintain
        Me.xviewMaintain.Name = "xviewMaintain"
        Me.xviewMaintain.OptionsBehavior.Editable = False
        Me.xviewMaintain.OptionsDetail.ShowDetailTabs = False
        Me.xviewMaintain.OptionsSelection.EnableAppearanceFocusedCell = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(722, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "F12 Exit"
        '
        'btnMaintain
        '
        Me.btnMaintain.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMaintain.Location = New System.Drawing.Point(3, 508)
        Me.btnMaintain.Name = "btnMaintain"
        Me.btnMaintain.Size = New System.Drawing.Size(75, 39)
        Me.btnMaintain.TabIndex = 6
        Me.btnMaintain.Text = "F5 Maintain"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(641, 508)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 39)
        Me.btnPrint.TabIndex = 7
        Me.btnPrint.Text = "F9 Print"
        '
        'Maintain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnMaintain)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.xgridMaintain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Maintain"
        Me.Size = New System.Drawing.Size(800, 550)
        CType(Me.xgridMaintain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewMaintain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xgridMaintain As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewMaintain As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMaintain As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrint As DevExpress.XtraEditors.SimpleButton

End Class
