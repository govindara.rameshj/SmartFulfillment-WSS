Imports Cts.Oasys.Core
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports System.ComponentModel
Imports Purchasing.Core
Imports System.Data
Imports System.Reflection

Public Class MaintainOrder
    Private _linkToReceipt As Purchasing.Core.Receipt
    Private _userId As Integer
    Private _supplierStocks As StockCollection = Nothing
    Private _receiptTrackable As ReceiptTrackable
    Private _prevRowHandle As Integer = GridControl.InvalidRowHandle
    Private _columnOrders(6) As String

    Sub New(ByRef receipt As Purchasing.Core.Receipt, ByVal userId As Integer)
        InitializeComponent()
        _linkToReceipt = receipt
        _receiptTrackable = New ReceiptTrackable(receipt)
        _userId = userId

        _columnOrders(0) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)
        _columnOrders(1) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuDescription)
        _columnOrders(2) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuProductCode)
        _columnOrders(3) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuPack)
        _columnOrders(4) = GetPropertyName(Function(f As ReceiptLineEditable) f.OrderPrice)
        _columnOrders(5) = GetPropertyName(Function(f As ReceiptLineEditable) f.OrderQty)
        _columnOrders(6) = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
    End Sub

    Private Sub FormKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10
                xviewLines.CloseEditor()
                xviewLines.CancelUpdateCurrentRow()
                btnCancel.PerformClick()
            Case Keys.F5 : btnUpdate.PerformClick()
            Case Keys.F4 : btnDeliveryNotes.PerformClick()
            Case Keys.F2 : btnStockEnquiry.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub FormLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor

            PopulateLabels()
            PopulateTotals()
            LoadLines()

            If _receiptTrackable.PoSupplierNumber IsNot Nothing Then
                _supplierStocks = Cts.Oasys.Core.Stock.Stock.GetStocksBySupplier(_receiptTrackable.PoSupplierNumber)
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub PopulateLabels()

        lblPO.Text = _receiptTrackable.PoNumber.ToString()
        lblSupplier.Text = _receiptTrackable.PoSupplierNumber & Space(1) & _receiptTrackable.PoSupplierName
        lblDRL.Text = _receiptTrackable.Number

        Dim poConsingNumber = _receiptTrackable.PoConsignNumber
        If poConsingNumber <> 0 Then
            lblConsign.Text = CStr(poConsingNumber)
        End If

        lblDateOrder.Text = Format(_receiptTrackable.PoOrderDate, "dd/MM/yy dddd")
        lblDateReceived.Text = Format(_receiptTrackable.DateReceipt, "dd/MM/yy dddd")
        If _receiptTrackable.Comments Is Nothing Then
            txtComment.Text = String.Empty
        Else
            txtComment.Text = _receiptTrackable.Comments.Trim
        End If

        lblDeliveryNotes.Text = _receiptTrackable.PoDeliveryNoteString

    End Sub

    Private Sub TxtComment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComment.TextChanged
        Dim tb As TextBox = CType(sender, TextBox)
        _receiptTrackable.Comments = tb.Text
    End Sub

    Private Sub PopulateTotals()

        Dim orderQty As Integer
        Dim orderValue As Decimal
        Dim receivedQty As Integer
        Dim receivedValue As Decimal

        _receiptTrackable.LinesTotal(orderQty, orderValue, receivedQty, receivedValue)

        lblUnitsOrdered.Text = orderQty.ToString("n0")
        lblValueOrdered.Text = orderValue.ToString("c2")
        lblUnitsReceived.Text = receivedQty.ToString("n0")
        lblValueReceived.Text = receivedValue.ToString("c2")

    End Sub

    Private Sub LoadLines()

        xgridLines.DataSource = _receiptTrackable.Lines

        'set up column widths
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In xviewLines.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)
                    col.Caption = My.Resources.ColumnHeaders.SkuNumber
                    'col.VisibleIndex = 0
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuDescription)
                    col.Caption = My.Resources.ColumnHeaders.Description
                    col.OptionsColumn.AllowFocus = False
                    'col.VisibleIndex = 1
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuProductCode)
                    col.Caption = My.Resources.ColumnHeaders.ProductCode
                    col.OptionsColumn.AllowFocus = False
                    'col.VisibleIndex = 2
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuPack)
                    col.Caption = My.Resources.ColumnHeaders.PackSize
                    col.OptionsColumn.AllowFocus = False
                    'col.VisibleIndex = 3
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.OrderQty)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "n0"
                    col.OptionsColumn.AllowFocus = False
                    'col.VisibleIndex = 5
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "n0"
                    'col.VisibleIndex = 6
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.OrderPrice)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    col.OptionsColumn.AllowFocus = False
                    'col.VisibleIndex = 4
                Case Else : col.Visible = False
            End Select
        Next

        ' See details in https://www.devexpress.com/Support/Center/Question/Details/Q294902
        For counter = 0 To _columnOrders.Length - 1 Step 1
            SetColumnOrder(_columnOrders(counter), counter)
        Next

        xviewLines.BestFitColumns()
        xviewLines.Focus()

        AddHandler _receiptTrackable.DataChanged, AddressOf ReceiptChanged
    End Sub

    Private Sub SetColumnOrder(ByVal columnName As String, ByVal visibleIndex As Integer)
        Dim col As DevExpress.XtraGrid.Columns.GridColumn = xviewLines.Columns.ColumnByFieldName(columnName)
        col.VisibleIndex = visibleIndex
    End Sub

    Private Sub ReceiptChanged(ByVal sender As Object, ByVal e As DataChangedEventArgs)
        btnUpdate.Enabled = e.HasModifications
    End Sub

    Private Sub xviewLines_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles xviewLines.ValidatingEditor

        'get view in question
        Dim view = CType(sender, GridView)

        'check if reset button has been pressed
        If Me.ActiveControl.Name = btnCancel.Name Then
            view.CancelUpdateCurrentRow()
            Exit Sub
        End If

        Dim receivedQty As String = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
        Dim sku As String = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)

        Select Case view.FocusedColumn.FieldName
            Case sku
                Dim skuNumber As String = CStr(e.Value)
                If skuNumber.Length <= 6 Then
                    skuNumber.PadLeft(6, "0"c)

                    'check that sku not already in receipt lines collection
                    If _receiptTrackable.IsLineStockExist(skuNumber) Then
                        e.ErrorText = My.Resources.Strings.ItemAlreadyExists
                        e.Valid = False
                        Exit Select
                    End If

                    'find stock item in collection and populate columns (only for new item)
                    Dim stockFound As Boolean = False
                    For Each s As Cts.Oasys.Core.Stock.Stock In _supplierStocks
                        If s.SkuNumber = skuNumber Then
                            stockFound = True
                            e.Value = s.SkuNumber

                            Dim rl As ReceiptLineEditable = CType(view.GetRow(GridControl.NewItemRowHandle), ReceiptLineEditable)
                            If rl IsNot Nothing Then
                                rl.SkuDescription = s.Description.Trim
                                rl.SkuProductCode = s.ProductCode.Trim
                                rl.SkuPack = s.PackSize
                                rl.OrderPrice = s.Price
                                rl.ReceivedPrice = s.Price
                                rl.Sequence = Nothing
                            End If

                        End If
                    Next

                    If Not stockFound Then
                        e.ErrorText = My.Resources.Strings.ItemNotForSupplier
                        e.Valid = False
                    End If

                Else
                    e.ErrorText = My.Resources.Strings.EnterValidSku
                    e.Valid = False
                End If

            Case receivedQty
                If Not IsDBNull(e.Value) Then
                    'validate qty entered
                    Dim intValue As Integer = CInt(e.Value)
                    If intValue < 0 Then
                        e.ErrorText = My.Resources.Strings.QtyCannotBeNegative
                        e.Valid = False
                    End If

                    view.SetRowCellValue(xviewLines.FocusedRowHandle, receivedQty, intValue)
                    view.UpdateCurrentRow()
                Else
                    e.Valid = False
                End If
        End Select

        'if an error exists then select all text for easy editing
        If e.Valid = False Then
            view.ActiveEditor.SelectAll()
        Else
            view.CloseEditor()
            If view.FocusedColumn.FieldName <> sku Then
                PopulateTotals()
            End If
        End If

    End Sub

    Private Sub xviewLines_ShowingEditor(ByVal sender As Object, ByVal e As CancelEventArgs) Handles xviewLines.ShowingEditor

        Dim view = CType(sender, GridView)
        If view.FocusedRowHandle <> GridControl.NewItemRowHandle Then
            If view.FocusedColumn.FieldName = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber) Then
                e.Cancel = True
            End If
        Else
            If view.FocusedColumn.FieldName = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty) Then
                Dim rl As ReceiptLineEditable = CType(view.GetRow(GridControl.NewItemRowHandle), ReceiptLineEditable)
                If rl Is Nothing Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub


    Private Sub btnDeliveryNotes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeliveryNotes.Click

        Using delNotes As New DeliveryNotes.DeliveryNotes
            delNotes.DeliveryNote1 = _receiptTrackable.PoDeliveryNote1
            delNotes.DeliveryNote2 = _receiptTrackable.PoDeliveryNote2
            delNotes.DeliveryNote3 = _receiptTrackable.PoDeliveryNote3
            delNotes.DeliveryNote4 = _receiptTrackable.PoDeliveryNote4
            delNotes.DeliveryNote5 = _receiptTrackable.PoDeliveryNote5
            delNotes.DeliveryNote6 = _receiptTrackable.PoDeliveryNote6
            delNotes.DeliveryNote7 = _receiptTrackable.PoDeliveryNote7
            delNotes.DeliveryNote8 = _receiptTrackable.PoDeliveryNote8
            delNotes.DeliveryNote9 = _receiptTrackable.PoDeliveryNote9

            If delNotes.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                _receiptTrackable.PoDeliveryNote1 = delNotes.DeliveryNote1
                _receiptTrackable.PoDeliveryNote2 = delNotes.DeliveryNote2
                _receiptTrackable.PoDeliveryNote3 = delNotes.DeliveryNote3
                _receiptTrackable.PoDeliveryNote4 = delNotes.DeliveryNote4
                _receiptTrackable.PoDeliveryNote5 = delNotes.DeliveryNote5
                _receiptTrackable.PoDeliveryNote6 = delNotes.DeliveryNote6
                _receiptTrackable.PoDeliveryNote7 = delNotes.DeliveryNote7
                _receiptTrackable.PoDeliveryNote8 = delNotes.DeliveryNote8
                _receiptTrackable.PoDeliveryNote9 = delNotes.DeliveryNote9
                lblDeliveryNotes.Text = _receiptTrackable.PoDeliveryNoteString
            End If
        End Using

    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(0, 0, 0, String.Empty)
            lookup.ShowAccept()
            lookup.SetSupplier(_receiptTrackable.PoSupplierNumber)

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Try
                        xviewLines.FocusedRowHandle = GridControl.NewItemRowHandle
                        xviewLines.FocusedColumn = xviewLines.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber))
                        xviewLines.ShowEditor()
                        xviewLines.ActiveEditor.EditValue = lookup.SkuNumbers(0)
                        xviewLines.ValidateEditor()
                    Catch ex As DevExpress.Utils.HideException
                    End Try
                End If
            End Using

        End Using

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Try
            Cursor = Cursors.WaitCursor
            _linkToReceipt.Update(_userId, _receiptTrackable)
            DialogResult = Windows.Forms.DialogResult.OK
            FindForm.Close()
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        FindForm.Close()
    End Sub

    Private Sub xviewLines_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles xviewLines.KeyDown
        If (e.KeyCode = Keys.Escape) AndAlso (xviewLines.ActiveEditor IsNot Nothing) Then
            xviewLines.CancelUpdateCurrentRow()
            xviewLines.FocusedRowHandle = _prevRowHandle
        End If
    End Sub

    Private Sub xviewLines_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles xviewLines.FocusedRowChanged
        If (e.FocusedRowHandle = e.PrevFocusedRowHandle) Then
            _prevRowHandle = e.PrevFocusedRowHandle
        End If
    End Sub

    Private Sub xgridLines_Leave(sender As System.Object, e As System.EventArgs) Handles xgridLines.Leave
        If xviewLines.FocusedRowHandle = GridControl.NewItemRowHandle Then
            xviewLines.CancelUpdateCurrentRow()
            xviewLines.FocusedRowHandle = _prevRowHandle
        End If
    End Sub

    Private Sub MaintainOrder_MouseClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseClick
        xviewLines.ValidateEditor()
    End Sub
End Class