Imports System.Windows.Forms
Imports Cts.Oasys.Data



Public Class Clear

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)

        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    ' Referral 343A
    ' Alan Lewis
    ' 10/11/2010
    ' Added standard F5 - 'Do main task' and F12 'Exit' button hot key access
    Private Sub Clear_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnMaintain.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Private Sub Clear_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        btnMaintain.Text = My.Resources.GapWalkClearButton
        Me.ParentForm.MaximizeBox = False
        Me.ParentForm.MinimizeBox = False
    End Sub

    ' Referral 343A
    ' Alan Lewis
    ' 10/11/2010
    ' Added standard F12 'Exit' button
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        FindForm.Close()
    End Sub

    Private Sub btnMaintain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMaintain.Click

        If Me.YesNoWarning(My.Resources.MessageBoxMessage) = DialogResult.Yes Then
            'clear down gap table
            Using con As New Connection
                Using com As New Command(con, My.Resources.StoredProcedureGapWalkClear)
                    With com
                        .AddParameter("Today", DateTime.Today, System.Data.SqlDbType.Date)
                    End With
                    com.ExecuteNonQuery()
                End Using
            End Using

            Me.FriendlyMessage(My.Resources.MessageBoxSuccess)
            ' Referral 343A
            ' Alan Lewis
            ' 10/11/2010
            ' Close after doing requested maintenance
            FindForm.Close()
        End If
    End Sub
End Class