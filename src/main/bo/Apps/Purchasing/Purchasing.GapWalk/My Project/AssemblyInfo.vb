﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Purchasing.GapWalk")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Travis Perkins PLC")> 
<Assembly: AssemblyProduct("Purchasing.GapWalk")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " Travis Perkins PLC 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("94438c9f-6484-4c7a-b0d3-b8b90cb462d0")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.1")> 
<Assembly: AssemblyFileVersion("3.0.0.1")> 
