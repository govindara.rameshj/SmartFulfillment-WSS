Public Class Reminder
    Private _PalletsIn As Integer = 1
    Private _PalletsOut As Integer = 0

    Public ReadOnly Property PalletsIn() As Integer
        Get
            Return _PalletsIn
        End Get
    End Property
    Public ReadOnly Property PalletsOut() As Integer
        Get
            Return _PalletsOut
        End Get
    End Property


    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub 'New

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub 'Form_KeyDown

    Private Sub AreAllAnswered()

        If chkStamped.Checked And chkSignature.Checked And chkDocuments.Checked _
        And txtPalletsIn.Text.Length > 0 And txtPalletsOut.Text.Length > 0 Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

    End Sub 'AreAllAnswered


    Private Sub txtPalletsOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPalletsOut.KeyPress, txtPalletsIn.KeyPress

        If Char.IsNumber(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

    End Sub 'Pallets_Text_Check

    Private Sub txtPalletsOut_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPalletsOut.Leave, txtPalletsIn.Leave

        Try
            Dim value As Integer = CInt(txtPalletsOut.Text)
            Select Case value
                Case Is < 0, Is > 99
                    txtPalletsOut.Clear()
                    btnSave.Enabled = False
                    Exit Sub
                Case Else
                    AreAllAnswered()

            End Select

        Catch ex As Exception
            txtPalletsOut.Clear()
            btnSave.Enabled = False
        End Try

    End Sub 'txtPalletsOut_Leave

    Private Sub Check_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStamped.CheckedChanged, chkDocuments.CheckedChanged, chkSignature.CheckedChanged

        Dim check As CheckBox = CType(sender, CheckBox)
        If check.Checked = True Then
            AreAllAnswered()
        Else
            btnSave.Enabled = False
        End If

    End Sub 'Check_CheckedChanged



    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub 'btnExit_Click

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        _PalletsIn = CInt(txtPalletsIn.Text)
        _PalletsOut = CInt(txtPalletsOut.Text)
        DialogResult = Windows.Forms.DialogResult.OK
    End Sub 'btnSave_Click


  
End Class