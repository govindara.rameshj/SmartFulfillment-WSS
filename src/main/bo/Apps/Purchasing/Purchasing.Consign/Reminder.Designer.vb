<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reminder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Reminder))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkStamped = New System.Windows.Forms.CheckBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPalletsIn = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtPalletsOut = New System.Windows.Forms.TextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label10 = New System.Windows.Forms.Label
        Me.chkSignature = New System.Windows.Forms.CheckBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Label11 = New System.Windows.Forms.Label
        Me.chkDocuments = New System.Windows.Forms.CheckBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(664, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "REMINDER"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label2.Location = New System.Drawing.Point(8, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(664, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Ensure that the Pallet Control details are completed accurately prior to signing " & _
            "the delivery notes"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(664, 24)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "FAILURE TO DO SO CAN AND WILL LEAD TO STOCKLOSS FOR YOUR STORE"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(664, 24)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "You must answer these questions"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.chkStamped)
        Me.Panel1.Location = New System.Drawing.Point(8, 112)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(664, 56)
        Me.Panel1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(296, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(360, 40)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "As per section 3.15 of the store procedures guide, all copies of the delivery not" & _
            "es should be stamped with the ""Goods Received"" stamp."
        '
        'chkStamped
        '
        Me.chkStamped.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkStamped.Location = New System.Drawing.Point(16, 8)
        Me.chkStamped.Name = "chkStamped"
        Me.chkStamped.Size = New System.Drawing.Size(264, 40)
        Me.chkStamped.TabIndex = 0
        Me.chkStamped.Text = "Have you stamped the delivery notes?"
        Me.chkStamped.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkStamped.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.txtPalletsIn)
        Me.Panel2.Location = New System.Drawing.Point(8, 176)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(664, 64)
        Me.Panel2.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(296, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(360, 48)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = resources.GetString("Label7.Text")
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(64, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(216, 32)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "How many pallets have been delivered?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(0-99)"
        '
        'txtPalletsIn
        '
        Me.txtPalletsIn.Location = New System.Drawing.Point(16, 16)
        Me.txtPalletsIn.MaxLength = 2
        Me.txtPalletsIn.Name = "txtPalletsIn"
        Me.txtPalletsIn.Size = New System.Drawing.Size(40, 20)
        Me.txtPalletsIn.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.txtPalletsOut)
        Me.Panel3.Location = New System.Drawing.Point(9, 248)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(664, 102)
        Me.Panel3.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(296, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(360, 88)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = resources.GetString("Label8.Text")
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(64, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(216, 32)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "How many pallets are being returned?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(0-99)"
        '
        'txtPalletsOut
        '
        Me.txtPalletsOut.Location = New System.Drawing.Point(16, 16)
        Me.txtPalletsOut.MaxLength = 2
        Me.txtPalletsOut.Name = "txtPalletsOut"
        Me.txtPalletsOut.Size = New System.Drawing.Size(40, 20)
        Me.txtPalletsOut.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Controls.Add(Me.chkSignature)
        Me.Panel4.Location = New System.Drawing.Point(9, 360)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(664, 56)
        Me.Panel4.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(296, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(360, 40)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "It is imperative that the driver's signature is obtained on the delivery notes.  " & _
            "Failure to do so will lead to stock loss for your store."
        '
        'chkSignature
        '
        Me.chkSignature.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkSignature.Location = New System.Drawing.Point(16, 8)
        Me.chkSignature.Name = "chkSignature"
        Me.chkSignature.Size = New System.Drawing.Size(264, 40)
        Me.chkSignature.TabIndex = 0
        Me.chkSignature.Text = "Have you obtained the driver's signature?"
        Me.chkSignature.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkSignature.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label11)
        Me.Panel5.Controls.Add(Me.chkDocuments)
        Me.Panel5.Location = New System.Drawing.Point(9, 424)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(664, 56)
        Me.Panel5.TabIndex = 4
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(296, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(360, 40)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "If you are unsure, call a senior colleague to confirm that all documentation has " & _
            "been completed correctly prior to allowing the driver to leave."
        '
        'chkDocuments
        '
        Me.chkDocuments.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkDocuments.Location = New System.Drawing.Point(16, 8)
        Me.chkDocuments.Name = "chkDocuments"
        Me.chkDocuments.Size = New System.Drawing.Size(264, 40)
        Me.chkDocuments.TabIndex = 0
        Me.chkDocuments.Text = "Are you certain all documentation has been completed correctly?"
        Me.chkDocuments.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkDocuments.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(8, 488)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(596, 488)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 39)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "F5 Accept"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Reminder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 536)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Reminder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Receiving Reminder"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkStamped As System.Windows.Forms.CheckBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPalletsIn As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPalletsOut As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkSignature As System.Windows.Forms.CheckBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents chkDocuments As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
End Class
