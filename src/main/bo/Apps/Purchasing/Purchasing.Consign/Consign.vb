Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports Purchasing.Core
Imports System.ComponentModel
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System

Public Class Consign
    Private _storeIdName As String = String.Empty

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        dteEnd.EditValue = Now.Date
        dteEnd.Properties.MaxValue = Now.Date
        dteStart.EditValue = Now.Date
        dteStart.Properties.MaxValue = Now.Date
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3 : btnSelect.PerformClick()
            Case Keys.F5 : btnConsign.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.Strings.GettingOrders)
            LoadOrdersNotConsigned()

            btnConsign.Enabled = (xviewOrders.RowCount > 0)
            btnPrint.Enabled = False
            _storeIdName = Store.GetIdName

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub

    ''' <summary>
    ''' Load Unconsigned Purchase Orders
    ''' </summary>
    ''' <history>
    ''' Author      : Partha Dutta
    ''' Date        : 14/10/2010
    ''' Referral No : 432
    ''' Notes       : Menu : Purchase Orders -> Consign Purchase Orders
    '''               Grids for both consigned and unconsigned purchases tyo display a "TIME" column
    ''' </history>
    ''' <remarks></remarks>
    Private Sub LoadOrdersNotConsigned()

        'get orders to bind to grid
        Dim orders As BindingList(Of Purchasing.Core.Order) = Order.GetOrdersNotConsigned
        xgridOrders.DataSource = orders

        'set up column widths
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In xviewOrders.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Order) f.PoNumber) : col.Caption = My.Resources.ColumnHeaders.PoNumber
                Case GetPropertyName(Function(f As Order) f.ReleaseNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierName)
                Case GetPropertyName(Function(f As Order) f.DateCreated) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.DateDue) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.Cartons) : col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Value)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Units) : col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Time) : col.MinWidth = 60
                Case Else : col.Visible = False
            End Select
        Next

        LoadOrderLines(xgridOrders)

        xviewOrders.BestFitColumns()
        xviewOrders.Focus()

    End Sub

    ''' <summary>
    ''' Load Consigned Purchase Orders
    ''' </summary>
    ''' <history>
    ''' Author      : Partha Dutta
    ''' Date        : 14/10/2010
    ''' Referral No : 432
    ''' Notes       : Menu : Purchase Orders -> Consign Purchase Orders
    '''               Grids for both consigned and unconsigned purchases tyo display a "TIME" column
    ''' </history>
    ''' <remarks></remarks>
    Private Sub LoadOrdersConsigned(ByVal startDate As Date, ByVal endDate As Date)

        'get orders to bind to grid
        Dim orders As BindingList(Of Purchasing.Core.Order) = Purchasing.Core.Order.GetOrdersConsigned(startDate, endDate)
        xgridConsigned.DataSource = orders

        'set up column widths
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In xviewConsigned.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Order) f.PoNumber) : col.Caption = My.Resources.ColumnHeaders.PoNumber
                Case GetPropertyName(Function(f As Order) f.ReleaseNumber)
                Case GetPropertyName(Function(f As Order) f.ConsignNumber)
                Case GetPropertyName(Function(f As Order) f.SoqNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierName)
                Case GetPropertyName(Function(f As Order) f.DateCreated) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.DateDue) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.Cartons) : col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Value)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Units) : col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Time) : col.MinWidth = 60
                Case Else : col.Visible = False
            End Select
        Next

        LoadOrderLines(xgridConsigned)

        xviewConsigned.BestFitColumns()
        xviewConsigned.Focus()

    End Sub

    Private Sub LoadOrderLines(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As Order) f.Lines), view)

        Dim id As String = GetPropertyName(Function(f As OrderLine) f.Id)
        view.Columns.AddField(id).VisibleIndex = 0
        view.Columns(id).MinWidth = 60
        view.Columns(id).MaxWidth = 60

        Dim skuNumber As String = GetPropertyName(Function(f As OrderLine) f.SkuNumber)
        view.Columns.AddField(skuNumber).VisibleIndex = 1
        view.Columns(skuNumber).MinWidth = 120
        view.Columns(skuNumber).MaxWidth = 120

        view.Columns.AddField(GetPropertyName(Function(f As OrderLine) f.SkuDescription)).VisibleIndex = 2

        Dim productCode As String = GetPropertyName(Function(f As OrderLine) f.SkuProductCode)
        view.Columns.AddField(productCode).VisibleIndex = 3
        view.Columns(productCode).MinWidth = 120
        view.Columns(productCode).MaxWidth = 120

        Dim price As String = GetPropertyName(Function(f As OrderLine) f.Price)
        view.Columns.AddField(price).VisibleIndex = 5
        view.Columns(price).MaxWidth = 80
        view.Columns(price).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        view.Columns(price).DisplayFormat.FormatString = "c2"

        Dim orderQty As String = GetPropertyName(Function(f As OrderLine) f.OrderQty)
        view.Columns.AddField(orderQty).VisibleIndex = 6
        view.Columns(orderQty).MaxWidth = 80

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False

    End Sub

    Private Sub xtabControl_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles xtabControl.SelectedPageChanged

        Select Case xtabControl.SelectedTabPage.Name
            Case xtabOrders.Name
                btnConsign.Enabled = (xviewOrders.RowCount > 0)
                btnPrint.Enabled = False

            Case xtabConsigned.Name
                btnConsign.Enabled = False
                btnPrint.Enabled = (xviewConsigned.RowCount > 0)
        End Select

    End Sub



    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.Strings.GettingConsignedOrders)
            LoadOrdersConsigned(CDate(dteStart.EditValue), CDate(dteEnd.EditValue))
            btnPrint.Enabled = (xviewConsigned.RowCount > 0)

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub

    Private Sub btnConsign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsign.Click

        Dim orders As BindingList(Of Purchasing.Core.Order) = CType(xgridOrders.DataSource, Global.System.ComponentModel.BindingList(Of Global.Purchasing.Core.Order))
        Dim order As Purchasing.Core.Order = orders(xviewOrders.GetDataSourceRowIndex(xviewOrders.GetSelectedRows(0)))

        'enter delivery notes
        Dim deliveryNotes(8) As String
        Using notesEntry As New DeliveryNotes.DeliveryNotes
            Dim deliveryResult As DialogResult = notesEntry.ShowDialog(Me)
            If deliveryResult <> DialogResult.OK Then
                xviewOrders.Focus()
                Exit Sub
            End If

            deliveryNotes = notesEntry.DeliveryNotes
        End Using

        'perform pallet check
        Dim palletsIn As Integer = 0
        Dim palletsOut As Integer = 0
        If order.PalletCheck Then
            Using reminder As New Reminder
                Dim reminderResult As DialogResult = reminder.ShowDialog(Me)
                Me.Focus()
                If reminderResult <> DialogResult.OK Then
                    xviewOrders.Focus()
                    Exit Sub
                End If
                palletsIn = reminder.PalletsIn
                palletsOut = reminder.PalletsOut
            End Using
        End If


        Try
            'assign values
            DisplayStatus(My.Resources.Strings.SavingConsignment)
            Cursor = Cursors.WaitCursor

            order.ConsignOrder(UserId, palletsIn, palletsOut, deliveryNotes)

            'check if want to print
            Dim result As DialogResult = MessageBox.Show(String.Format(My.Resources.Strings.YesNoConsignmentSaved, order.ConsignNumber), Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If result = DialogResult.Yes Then
                Dim report As New Purchasing.Reports.Consignment(_storeIdName, order)
                report.ShowPreviewDialog()
            End If

            'remove from orders and check if any left
            orders.Remove(order)
            xviewOrders.RefreshData()
            If xviewOrders.RowCount = 0 Then btnConsign.Enabled = False

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim orders = CType(xgridConsigned.DataSource, Global.System.ComponentModel.BindingList(Of Global.Purchasing.Core.Order))
        Dim order As Purchasing.Core.Order = orders(xviewConsigned.GetDataSourceRowIndex(xviewConsigned.GetSelectedRows(0)))

        Dim report As New Purchasing.Reports.Consignment(_storeIdName, order)
        report.ShowPreviewDialog()

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class