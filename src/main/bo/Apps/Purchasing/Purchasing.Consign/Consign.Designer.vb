<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Consign
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtabControl = New DevExpress.XtraTab.XtraTabControl
        Me.xtabOrders = New DevExpress.XtraTab.XtraTabPage
        Me.xgridOrders = New DevExpress.XtraGrid.GridControl
        Me.xviewOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.xtabConsigned = New DevExpress.XtraTab.XtraTabPage
        Me.btnSelect = New DevExpress.XtraEditors.SimpleButton
        Me.dteEnd = New DevExpress.XtraEditors.DateEdit
        Me.lblEnd = New DevExpress.XtraEditors.LabelControl
        Me.dteStart = New DevExpress.XtraEditors.DateEdit
        Me.lblStart = New DevExpress.XtraEditors.LabelControl
        Me.xgridConsigned = New DevExpress.XtraGrid.GridControl
        Me.xviewConsigned = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnConsign = New DevExpress.XtraEditors.SimpleButton
        Me.btnPrint = New DevExpress.XtraEditors.SimpleButton
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton
        CType(Me.xtabControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtabControl.SuspendLayout()
        Me.xtabOrders.SuspendLayout()
        CType(Me.xgridOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtabConsigned.SuspendLayout()
        CType(Me.dteEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgridConsigned, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewConsigned, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtabControl
        '
        Me.xtabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xtabControl.Location = New System.Drawing.Point(3, 3)
        Me.xtabControl.Name = "xtabControl"
        Me.xtabControl.SelectedTabPage = Me.xtabOrders
        Me.xtabControl.Size = New System.Drawing.Size(894, 449)
        Me.xtabControl.TabIndex = 0
        Me.xtabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtabOrders, Me.xtabConsigned})
        '
        'xtabOrders
        '
        Me.xtabOrders.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xtabOrders.Controls.Add(Me.xgridOrders)
        Me.xtabOrders.Name = "xtabOrders"
        Me.xtabOrders.Size = New System.Drawing.Size(885, 418)
        Me.xtabOrders.Text = "Orders To Consign"
        '
        'xgridOrders
        '
        Me.xgridOrders.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xgridOrders.Location = New System.Drawing.Point(0, 0)
        Me.xgridOrders.MainView = Me.xviewOrders
        Me.xgridOrders.Name = "xgridOrders"
        Me.xgridOrders.Size = New System.Drawing.Size(885, 418)
        Me.xgridOrders.TabIndex = 0
        Me.xgridOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewOrders})
        '
        'xviewOrders
        '
        Me.xviewOrders.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewOrders.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black
        Me.xviewOrders.Appearance.FocusedRow.Options.UseBackColor = True
        Me.xviewOrders.Appearance.FocusedRow.Options.UseForeColor = True
        Me.xviewOrders.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewOrders.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewOrders.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewOrders.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewOrders.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black
        Me.xviewOrders.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.xviewOrders.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.xviewOrders.Appearance.SelectedRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewOrders.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black
        Me.xviewOrders.Appearance.SelectedRow.Options.UseBackColor = True
        Me.xviewOrders.Appearance.SelectedRow.Options.UseForeColor = True
        Me.xviewOrders.GridControl = Me.xgridOrders
        Me.xviewOrders.Name = "xviewOrders"
        Me.xviewOrders.OptionsBehavior.Editable = False
        Me.xviewOrders.OptionsDetail.ShowDetailTabs = False
        Me.xviewOrders.OptionsView.ColumnAutoWidth = False
        '
        'xtabConsigned
        '
        Me.xtabConsigned.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xtabConsigned.Controls.Add(Me.btnSelect)
        Me.xtabConsigned.Controls.Add(Me.dteEnd)
        Me.xtabConsigned.Controls.Add(Me.lblEnd)
        Me.xtabConsigned.Controls.Add(Me.dteStart)
        Me.xtabConsigned.Controls.Add(Me.lblStart)
        Me.xtabConsigned.Controls.Add(Me.xgridConsigned)
        Me.xtabConsigned.Name = "xtabConsigned"
        Me.xtabConsigned.Size = New System.Drawing.Size(885, 418)
        Me.xtabConsigned.Text = "Consigned Orders"
        '
        'btnSelect
        '
        Me.btnSelect.Location = New System.Drawing.Point(207, 6)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(76, 39)
        Me.btnSelect.TabIndex = 5
        Me.btnSelect.Text = "F3 Select"
        '
        'dteEnd
        '
        Me.dteEnd.EditValue = Nothing
        Me.dteEnd.Location = New System.Drawing.Point(84, 29)
        Me.dteEnd.Name = "dteEnd"
        Me.dteEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteEnd.Size = New System.Drawing.Size(100, 20)
        Me.dteEnd.TabIndex = 3
        '
        'lblEnd
        '
        Me.lblEnd.Location = New System.Drawing.Point(2, 32)
        Me.lblEnd.Name = "lblEnd"
        Me.lblEnd.Size = New System.Drawing.Size(44, 13)
        Me.lblEnd.TabIndex = 2
        Me.lblEnd.Text = "&End Date"
        '
        'dteStart
        '
        Me.dteStart.EditValue = Nothing
        Me.dteStart.Location = New System.Drawing.Point(84, 3)
        Me.dteStart.Name = "dteStart"
        Me.dteStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteStart.Size = New System.Drawing.Size(100, 20)
        Me.dteStart.TabIndex = 1
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(2, 6)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(50, 13)
        Me.lblStart.TabIndex = 0
        Me.lblStart.Text = "&Start Date"
        '
        'xgridConsigned
        '
        Me.xgridConsigned.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridConsigned.Location = New System.Drawing.Point(0, 55)
        Me.xgridConsigned.MainView = Me.xviewConsigned
        Me.xgridConsigned.Name = "xgridConsigned"
        Me.xgridConsigned.Size = New System.Drawing.Size(885, 363)
        Me.xgridConsigned.TabIndex = 4
        Me.xgridConsigned.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewConsigned})
        '
        'xviewConsigned
        '
        Me.xviewConsigned.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewConsigned.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black
        Me.xviewConsigned.Appearance.FocusedRow.Options.UseBackColor = True
        Me.xviewConsigned.Appearance.FocusedRow.Options.UseForeColor = True
        Me.xviewConsigned.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewConsigned.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewConsigned.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewConsigned.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewConsigned.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewConsigned.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewConsigned.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black
        Me.xviewConsigned.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.xviewConsigned.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.xviewConsigned.Appearance.SelectedRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.xviewConsigned.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black
        Me.xviewConsigned.Appearance.SelectedRow.Options.UseBackColor = True
        Me.xviewConsigned.Appearance.SelectedRow.Options.UseForeColor = True
        Me.xviewConsigned.GridControl = Me.xgridConsigned
        Me.xviewConsigned.Name = "xviewConsigned"
        Me.xviewConsigned.OptionsBehavior.Editable = False
        Me.xviewConsigned.OptionsDetail.ShowDetailTabs = False
        Me.xviewConsigned.OptionsPrint.ExpandAllDetails = True
        Me.xviewConsigned.OptionsPrint.PrintDetails = True
        Me.xviewConsigned.OptionsPrint.PrintSelectedRowsOnly = True
        Me.xviewConsigned.OptionsView.ColumnAutoWidth = False
        '
        'btnConsign
        '
        Me.btnConsign.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnConsign.Location = New System.Drawing.Point(3, 458)
        Me.btnConsign.Name = "btnConsign"
        Me.btnConsign.Size = New System.Drawing.Size(76, 39)
        Me.btnConsign.TabIndex = 6
        Me.btnConsign.Text = "F5 Consign"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(739, 458)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 7
        Me.btnPrint.Text = "F9 Print"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(821, 458)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "F12 Exit"
        '
        'Consign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnConsign)
        Me.Controls.Add(Me.xtabControl)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "Consign"
        Me.Size = New System.Drawing.Size(900, 500)
        CType(Me.xtabControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtabControl.ResumeLayout(False)
        Me.xtabOrders.ResumeLayout(False)
        CType(Me.xgridOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtabConsigned.ResumeLayout(False)
        Me.xtabConsigned.PerformLayout()
        CType(Me.dteEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgridConsigned, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewConsigned, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xtabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtabOrders As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtabConsigned As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xgridOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents xgridConsigned As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewConsigned As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents dteEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblEnd As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dteStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblStart As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSelect As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnConsign As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton

End Class
