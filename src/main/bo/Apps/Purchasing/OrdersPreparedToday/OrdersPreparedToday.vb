﻿Public Class OrdersPreparedToday
    Inherits OasysCTS.ModuleTabBase
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _PrintNotPrinted As Boolean = False
    Private _Printset As New FarPoint.Win.Spread.PrintInfo()
    Private _IsInitialising As Boolean = False

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByRef ConnectionString As String, ByVal RunParameters As String, ByRef StatusMessage As ToolStripLabel, ByRef StatusProgress As ToolStripProgressBar)
        MyBase.New(userId, workstationId, securityLevel, ConnectionString, RunParameters, StatusMessage, StatusProgress)
        InitializeComponent()
    End Sub

    Protected Overrides Sub DoProcessing()
        RunUpdate()
    End Sub


    Private Sub RunUpdate()
        Dim supplier As BOPurchases.cSupplierMaster = New BOPurchases.cSupplierMaster(_Oasys3DB)
        Dim SupplierDetails As BOPurchases.cSupplierDetail = New BOPurchases.cSupplierDetail(_Oasys3DB)
        Dim SupplierNotes As BOPurchases.cSupplierNote = New BOPurchases.cSupplierNote(_Oasys3DB)
        Dim lstSupplier As List(Of BOPurchases.cSupplierMaster)
        Dim lstDetails As List(Of BOPurchases.cSupplierDetail)
        Dim lstNotes As List(Of BOPurchases.cSupplierNote)
        Dim system As BOSystem.cSystemDates = New BOSystem.cSystemDates(_Oasys3DB)
        Dim BOretopt As BOSystem.cRetailOptions = New BOSystem.cRetailOptions(_Oasys3DB)
        Dim retopt As DataRow
        Dim Current As DataRow
        Dim DirectDayNum As Integer = 0
        Dim BBCDayNum As Integer = 0

        Dim PrintPONum As String = String.Empty
        Dim PrintSupplierNum As String = String.Empty
        Dim PrintSupplierName As String = String.Empty
        Dim PrintSupplierType As String = String.Empty
        Dim PrintHQSequence As String = String.Empty
        Dim PrintMesssage As String = String.Empty

        Dim rowCount As Integer = 0
        Dim tempString As String = String.Empty

        'Calculate the date to use 
        retopt = BOretopt.RetailOptions()

        Current = system.SystemDates
        If CInt(Current.Item(system.TodayDayCode.ColumnName)) = CInt(Current.Item(system.DaysOpen.ColumnName)) Then
            DirectDayNum = 1
            BBCDayNum = 1
        Else
            DirectDayNum = CInt(Current.Item(system.TomorrowsDayCode.ColumnName))
            BBCDayNum = CInt(Current.Item(system.TomorrowsDayCode.ColumnName))
        End If

        BBCDayNum = BBCDayNum + 1

        If BBCDayNum > CInt(Current.Item(system.DaysOpen.ColumnName)) Then

            BBCDayNum = CInt(Current.Item(system.DaysOpen.ColumnName))

        End If
        'Set up the form header 
        _Printset.Header = "Orders prepared for :  " & Format(CDate(Current.Item(system.Tomorrow.ColumnName)), "dd/MM/yy") & " for Store: " & CStr(retopt.Item(BOretopt.Store.ColumnName)) & " - " & CStr(retopt.Item(BOretopt.StoreName.ColumnName)) & _
                                          vbCrLf & vbCrLf & "P/O No" & Space(7) & "Supp" & Space(9) & "Supplier Name" & Space(33) & "Supplier Type" & Space(17) & "H/0 Sequence                   /rPage /p of /pc" & vbCrLf & vbCrLf

        FpOrder_Sheet1.AddRows(rowCount, 1)

        lstSupplier = supplier.GetActiveRecords()

        pbProgress.Maximum = lstSupplier.Count
        pbProgress.Value = 0
        pbProgress.Visible = True

        For Each BoSupplier As BOPurchases.cSupplierMaster In lstSupplier
            Debug.WriteLine("Start Transaction")
            ' get the detail records 
            ' Debug.WriteLine("before lstdetails")
            lstDetails = SupplierDetails.GetDetailRecords(BoSupplier.Number.Value)
            ' Debug.WriteLine("after lstdetails ")
            For Each detail As BOPurchases.cSupplierDetail In lstDetails
                'Debug.WriteLine("Start Detail")
                'Creat mesage 
                PrintMesssage = String.Empty
                If detail.SOQFrequency.Value = 0 Then
                    PrintMesssage = " - Order not Due to be Ordered Today"
                Else
                    If Trim(detail.BBC.Value) = String.Empty Then
                        'direct
                        If detail.SOQFrequency.Value <> DirectDayNum Then
                            PrintMesssage = " - Order not Due to be Ordered Today"
                        End If
                    Else
                        'bbc 
                        If detail.SOQFrequency.Value <> BBCDayNum Then
                            PrintMesssage = " - Order not Due to be Ordered Today"
                        End If
                    End If

                End If
                'Debug.WriteLine("before lstNotes ")
                lstNotes = SupplierNotes.GetSuppliersType999(detail.SupplierNumber.Value)
                'Debug.WriteLine("after lstNotes ")

                For Each Note As BOPurchases.cSupplierNote In lstNotes
                    '   Debug.WriteLine("start Note ")
                    'format the not data
                    If Mid(Note.Text.Value, 1, 6) = "000000" Then
                        PrintPONum = "* NULL *"
                    Else
                        PrintPONum = Mid(Note.Text.Value, 1, 6)
                    End If
                    PrintSupplierNum = BoSupplier.Number.Value
                    PrintSupplierName = BoSupplier.Name.Value

                    Select Case detail.BBC.Value
                        Case " "
                            PrintSupplierType = "Direct"
                        Case "C"
                            PrintSupplierType = "BBC Consolidated"
                        Case "D"
                            PrintSupplierType = "BBC Discreet"
                        Case "W"
                            PrintSupplierType = "Warehouse"
                        Case "A"
                            PrintSupplierType = "Alternative"
                    End Select
                    'put the data into the Grid
                    PrintHQSequence = Mid(Note.Text.Value, 14, 2) & "-" & Mid(Note.Text.Value, 16, 6)

                    FpOrder_Sheet1.AddRows(rowCount, 1)
                    FpOrder_Sheet1.SetText(rowCount, 0, PrintPONum)
                    FpOrder_Sheet1.SetText(rowCount, 1, PrintSupplierNum)
                    FpOrder_Sheet1.SetText(rowCount, 2, PrintSupplierName)
                    FpOrder_Sheet1.SetText(rowCount, 3, PrintSupplierType)
                    FpOrder_Sheet1.SetText(rowCount, 4, PrintHQSequence)
                    FpOrder_Sheet1.SetText(rowCount, 5, PrintMesssage)

                    rowCount = rowCount + 1

                    '  Debug.WriteLine("End Notes ")

                    Application.DoEvents()

                Next
                Application.DoEvents()
                ' Debug.WriteLine("End Details ")
            Next
            pbProgress.Value += 1
            pbProgress.Refresh()
            Application.DoEvents()
            Debug.WriteLine("End Transaction ")
        Next

        pbProgress.Visible = False

        txtPrint.Text = "N"
        txtPrint.Focus()

    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
        Loop
        FindForm.Close()
    End Sub

    Private Sub FpOrder_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles FpOrder.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub txtPrint_TextChanged(ByVal sender As System.Object, ByVal e As Windows.Forms.KeyEventArgs) Handles txtPrint.KeyDown

        If e.KeyValue = 89 Or e.KeyValue = 78 Or e.KeyValue = 8 Or e.KeyValue = 16 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 27 Then
            'use this character 
        Else
            e.SuppressKeyPress = True
        End If

        If e.KeyValue = Keys.Enter Then
            If txtPrint.Text = "Y" Then
                DisplayStatus("Printing - Please Wait")
                Panel1.Visible = True
                Me.Refresh()
                Application.DoEvents()
                Print()
                Panel1.Visible = False
                Me.Refresh()
                Application.DoEvents()
            End If
            FindForm.Close()
        End If

    End Sub

    Private Sub Print()

        _Printset.ShowGrid = False
        _Printset.ShowBorder = False
        _Printset.ShowPrintDialog = False
        _Printset.Orientation = FarPoint.Win.Spread.PrintOrientation.Landscape
        _Printset.ShowColumnHeaders = False

        FpOrder_Sheet1.PrintInfo = _Printset


        FpOrder.PrintSheet(0)
        'wait until printing finished
        Finish()
    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles FpOrder.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        '  spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub

    'Private Sub cboPrinters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    If blnInitialising = False Then
    '        Dim prtdoc As New PrintDocument
    '        prtdoc.PrinterSettings.PrinterName = cboPrinters.Items(cboPrinters.SelectedIndex).ToString
    '        SetDefaultPrinter(prtdoc.PrinterSettings.PrinterName)
    '    End If

    'End Sub


    'Public Shared Function SetDefaultPrinter(ByVal PrinterName As String) As Boolean
    '    Dim search As System.Management.ManagementObjectSearcher
    '    Dim results As System.Management.ManagementObjectCollection
    '    Dim printer As System.Management.ManagementObject
    '    Dim args(1) As Object
    '    Dim blnResult As Boolean = False

    '    search = New System.Management.ManagementObjectSearcher("select * from win32_printer")
    '    results = search.Get()

    '    'Retrieves current default Printer
    '    Dim p As New System.Windows.Forms.PrintDialog
    '    Dim currentDefault As String
    '    currentDefault = p.PrinterSettings.PrinterName

    '    'Sets new default Printer
    '    For Each printer In results
    '        Debug.Print(printer("Name"))
    '        If printer("Name") = PrinterName Then
    '            printer.InvokeMethod("SetDefaultPrinter", args(0))
    '            blnResult = True
    '        End If
    '        Application.DoEvents()
    '    Next

    '    Return blnResult

    'End Function

    Private Sub OrdersPreparedToday_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

End Class

