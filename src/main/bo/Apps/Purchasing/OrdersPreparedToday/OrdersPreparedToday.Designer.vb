﻿Imports System.Drawing.Printing

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrdersPreparedToday
    Inherits OasysCTS.ModuleTabBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TextCellType1 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim TextCellType2 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim TextCellType3 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim TextCellType4 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim TextCellType5 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim TextCellType6 As FarPoint.Win.Spread.CellType.TextCellType = New FarPoint.Win.Spread.CellType.TextCellType
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OrdersPreparedToday))
        Me.FpOrder = New FarPoint.Win.Spread.FpSpread
        Me.FpOrder_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.lblPrint = New System.Windows.Forms.Label
        Me.txtPrint = New System.Windows.Forms.TextBox
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.FpOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FpOrder_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FpOrder
        '
        Me.FpOrder.About = "3.0.2004.2005"
        Me.FpOrder.AccessibleDescription = "FpOrder, Sheet1, Row 0, Column 0, "
        Me.FpOrder.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FpOrder.BackColor = System.Drawing.SystemColors.Control
        Me.FpOrder.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        Me.FpOrder.Location = New System.Drawing.Point(12, 12)
        Me.FpOrder.Name = "FpOrder"
        Me.FpOrder.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FpOrder.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.FpOrder_Sheet1})
        Me.FpOrder.Size = New System.Drawing.Size(706, 259)
        Me.FpOrder.TabIndex = 0
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.FpOrder.TextTipAppearance = TipAppearance1
        Me.FpOrder.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'FpOrder_Sheet1
        '
        Me.FpOrder_Sheet1.Reset()
        Me.FpOrder_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.FpOrder_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.FpOrder_Sheet1.ColumnCount = 6
        Me.FpOrder_Sheet1.RowCount = 1
        Me.FpOrder_Sheet1.RowHeader.ColumnCount = 0
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "P/O No"
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Supp"
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Supplier Name"
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Supplier Type"
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "H/O Sequence"
        Me.FpOrder_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "  "
        Me.FpOrder_Sheet1.Columns.Get(0).CellType = TextCellType1
        Me.FpOrder_Sheet1.Columns.Get(0).Label = "P/O No"
        Me.FpOrder_Sheet1.Columns.Get(0).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(0).Resizable = False
        Me.FpOrder_Sheet1.Columns.Get(1).CellType = TextCellType2
        Me.FpOrder_Sheet1.Columns.Get(1).Label = "Supp"
        Me.FpOrder_Sheet1.Columns.Get(1).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(1).Resizable = False
        Me.FpOrder_Sheet1.Columns.Get(2).CellType = TextCellType3
        Me.FpOrder_Sheet1.Columns.Get(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.FpOrder_Sheet1.Columns.Get(2).Label = "Supplier Name"
        Me.FpOrder_Sheet1.Columns.Get(2).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(2).Resizable = False
        Me.FpOrder_Sheet1.Columns.Get(2).Width = 160.0!
        Me.FpOrder_Sheet1.Columns.Get(3).CellType = TextCellType4
        Me.FpOrder_Sheet1.Columns.Get(3).Label = "Supplier Type"
        Me.FpOrder_Sheet1.Columns.Get(3).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(3).Resizable = False
        Me.FpOrder_Sheet1.Columns.Get(3).Width = 75.0!
        Me.FpOrder_Sheet1.Columns.Get(4).CellType = TextCellType5
        Me.FpOrder_Sheet1.Columns.Get(4).Label = "H/O Sequence"
        Me.FpOrder_Sheet1.Columns.Get(4).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(4).Resizable = False
        Me.FpOrder_Sheet1.Columns.Get(4).Width = 85.0!
        Me.FpOrder_Sheet1.Columns.Get(5).CellType = TextCellType6
        Me.FpOrder_Sheet1.Columns.Get(5).Label = "  "
        Me.FpOrder_Sheet1.Columns.Get(5).Locked = True
        Me.FpOrder_Sheet1.Columns.Get(5).Width = 238.0!
        Me.FpOrder_Sheet1.PrintInfo.Margin.Left = 50
        Me.FpOrder_Sheet1.PrintInfo.Orientation = FarPoint.Win.Spread.PrintOrientation.Landscape
        Me.FpOrder_Sheet1.PrintInfo.ShowBorder = False
        Me.FpOrder_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.FpOrder_Sheet1.RowHeader.HorizontalGridLine = New FarPoint.Win.Spread.GridLine
        Me.FpOrder_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'lblPrint
        '
        Me.lblPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblPrint.AutoSize = True
        Me.lblPrint.Location = New System.Drawing.Point(482, 298)
        Me.lblPrint.Name = "lblPrint"
        Me.lblPrint.Size = New System.Drawing.Size(61, 13)
        Me.lblPrint.TabIndex = 1
        Me.lblPrint.Text = "Print report:"
        '
        'txtPrint
        '
        Me.txtPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPrint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrint.Location = New System.Drawing.Point(549, 291)
        Me.txtPrint.MaxLength = 1
        Me.txtPrint.Name = "txtPrint"
        Me.txtPrint.Size = New System.Drawing.Size(27, 20)
        Me.txtPrint.TabIndex = 2
        '
        'pbProgress
        '
        Me.pbProgress.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pbProgress.Location = New System.Drawing.Point(0, 325)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(746, 16)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 5
        Me.pbProgress.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(246, 141)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(255, 59)
        Me.Panel1.TabIndex = 8
        Me.Panel1.UseWaitCursor = True
        Me.Panel1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Printing - Please Wait"
        Me.Label1.UseWaitCursor = True
        '
        'OrdersPreparedToday
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pbProgress)
        Me.Controls.Add(Me.txtPrint)
        Me.Controls.Add(Me.lblPrint)
        Me.Controls.Add(Me.FpOrder)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "OrdersPreparedToday"
        Me.Size = New System.Drawing.Size(746, 341)
        CType(Me.FpOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FpOrder_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FpOrder As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpOrder_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents lblPrint As System.Windows.Forms.Label
    Friend WithEvents txtPrint As System.Windows.Forms.TextBox
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
