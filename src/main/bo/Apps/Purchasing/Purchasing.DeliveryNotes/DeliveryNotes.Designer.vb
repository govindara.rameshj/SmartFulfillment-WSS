﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeliveryNotes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtNote1 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote2 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote3 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote4 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote5 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote6 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote7 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote8 = New DevExpress.XtraEditors.TextEdit
        Me.txtNote9 = New DevExpress.XtraEditors.TextEdit
        CType(Me.txtNote1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Delivery Note 1*"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Delivery Note 2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 139)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Delivery Note 6"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Delivery Note 5"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Delivery Note 4"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Delivery Note 3"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 165)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Delivery Note 7"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 191)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Delivery Note 8"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 217)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Delivery Note 9"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(13, 249)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(154, 12)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "* The first delivery note is mandatory"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(150, 289)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(12, 289)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 39)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtNote1
        '
        Me.txtNote1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote1.EditValue = ""
        Me.txtNote1.EnterMoveNextControl = True
        Me.txtNote1.Location = New System.Drawing.Point(125, 6)
        Me.txtNote1.Name = "txtNote1"
        Me.txtNote1.Properties.MaxLength = 10
        Me.txtNote1.Size = New System.Drawing.Size(100, 20)
        Me.txtNote1.TabIndex = 0
        '
        'txtNote2
        '
        Me.txtNote2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote2.EditValue = ""
        Me.txtNote2.EnterMoveNextControl = True
        Me.txtNote2.Location = New System.Drawing.Point(125, 32)
        Me.txtNote2.Name = "txtNote2"
        Me.txtNote2.Properties.MaxLength = 10
        Me.txtNote2.Size = New System.Drawing.Size(100, 20)
        Me.txtNote2.TabIndex = 1
        '
        'txtNote3
        '
        Me.txtNote3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote3.EditValue = ""
        Me.txtNote3.EnterMoveNextControl = True
        Me.txtNote3.Location = New System.Drawing.Point(125, 58)
        Me.txtNote3.Name = "txtNote3"
        Me.txtNote3.Properties.MaxLength = 10
        Me.txtNote3.Size = New System.Drawing.Size(100, 20)
        Me.txtNote3.TabIndex = 2
        '
        'txtNote4
        '
        Me.txtNote4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote4.EditValue = ""
        Me.txtNote4.EnterMoveNextControl = True
        Me.txtNote4.Location = New System.Drawing.Point(125, 84)
        Me.txtNote4.Name = "txtNote4"
        Me.txtNote4.Properties.MaxLength = 10
        Me.txtNote4.Size = New System.Drawing.Size(100, 20)
        Me.txtNote4.TabIndex = 3
        '
        'txtNote5
        '
        Me.txtNote5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote5.EditValue = ""
        Me.txtNote5.EnterMoveNextControl = True
        Me.txtNote5.Location = New System.Drawing.Point(125, 110)
        Me.txtNote5.Name = "txtNote5"
        Me.txtNote5.Properties.MaxLength = 10
        Me.txtNote5.Size = New System.Drawing.Size(100, 20)
        Me.txtNote5.TabIndex = 4
        '
        'txtNote6
        '
        Me.txtNote6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote6.EditValue = ""
        Me.txtNote6.EnterMoveNextControl = True
        Me.txtNote6.Location = New System.Drawing.Point(125, 136)
        Me.txtNote6.Name = "txtNote6"
        Me.txtNote6.Properties.MaxLength = 10
        Me.txtNote6.Size = New System.Drawing.Size(100, 20)
        Me.txtNote6.TabIndex = 5
        '
        'txtNote7
        '
        Me.txtNote7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote7.EditValue = ""
        Me.txtNote7.EnterMoveNextControl = True
        Me.txtNote7.Location = New System.Drawing.Point(125, 162)
        Me.txtNote7.Name = "txtNote7"
        Me.txtNote7.Properties.MaxLength = 10
        Me.txtNote7.Size = New System.Drawing.Size(100, 20)
        Me.txtNote7.TabIndex = 6
        '
        'txtNote8
        '
        Me.txtNote8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote8.EditValue = ""
        Me.txtNote8.EnterMoveNextControl = True
        Me.txtNote8.Location = New System.Drawing.Point(125, 188)
        Me.txtNote8.Name = "txtNote8"
        Me.txtNote8.Properties.MaxLength = 10
        Me.txtNote8.Size = New System.Drawing.Size(100, 20)
        Me.txtNote8.TabIndex = 7
        '
        'txtNote9
        '
        Me.txtNote9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote9.EditValue = ""
        Me.txtNote9.EnterMoveNextControl = True
        Me.txtNote9.Location = New System.Drawing.Point(125, 214)
        Me.txtNote9.Name = "txtNote9"
        Me.txtNote9.Properties.MaxLength = 10
        Me.txtNote9.Size = New System.Drawing.Size(100, 20)
        Me.txtNote9.TabIndex = 8
        '
        'DeliveryNotes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(237, 340)
        Me.Controls.Add(Me.txtNote9)
        Me.Controls.Add(Me.txtNote8)
        Me.Controls.Add(Me.txtNote7)
        Me.Controls.Add(Me.txtNote6)
        Me.Controls.Add(Me.txtNote5)
        Me.Controls.Add(Me.txtNote4)
        Me.Controls.Add(Me.txtNote3)
        Me.Controls.Add(Me.txtNote2)
        Me.Controls.Add(Me.txtNote1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.Name = "DeliveryNotes"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enter Delivery Notes"
        CType(Me.txtNote1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtNote1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNote9 As DevExpress.XtraEditors.TextEdit
End Class
