﻿Imports System.Collections
Imports System.Windows.Forms

Public Class DeliveryNotes

    Public Property DeliveryNote1() As String
        Get
            Return txtNote1.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote1.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote2() As String
        Get
            Return txtNote2.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote2.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote3() As String
        Get
            Return txtNote3.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote3.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote4() As String
        Get
            Return txtNote4.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote4.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote5() As String
        Get
            Return txtNote5.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote5.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote6() As String
        Get
            Return txtNote6.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote6.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote7() As String
        Get
            Return txtNote7.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote7.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote8() As String
        Get
            Return txtNote8.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote8.EditValue = value.Trim
        End Set
    End Property
    Public Property DeliveryNote9() As String
        Get
            Return txtNote9.EditValue.ToString.Trim
        End Get
        Set(ByVal value As String)
            txtNote9.EditValue = value.Trim
        End Set
    End Property

    Public Function DeliveryNotes() As String()

        Dim al As New List(Of String)
        al.Add(CStr(txtNote1.EditValue))
        If txtNote2.EditValue IsNot Nothing Then al.Add(CStr(txtNote2.EditValue))
        If txtNote3.EditValue IsNot Nothing Then al.Add(CStr(txtNote3.EditValue))
        If txtNote4.EditValue IsNot Nothing Then al.Add(CStr(txtNote4.EditValue))
        If txtNote5.EditValue IsNot Nothing Then al.Add(CStr(txtNote5.EditValue))
        If txtNote6.EditValue IsNot Nothing Then al.Add(CStr(txtNote6.EditValue))
        If txtNote7.EditValue IsNot Nothing Then al.Add(CStr(txtNote7.EditValue))
        If txtNote8.EditValue IsNot Nothing Then al.Add(CStr(txtNote8.EditValue))
        If txtNote9.EditValue IsNot Nothing Then al.Add(CStr(txtNote9.EditValue))

        Return al.ToArray()

    End Function


    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub DeliveryNotes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyData
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub txtNote1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNote1.Leave
        btnSave.Enabled = (txtNote1.Text.Trim.Length > 0)
    End Sub



    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub

End Class