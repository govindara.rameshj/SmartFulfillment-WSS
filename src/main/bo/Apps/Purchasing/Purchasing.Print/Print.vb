Public Class Print
    Private Enum col
        PONumber
        ConsignNumber
        SupplierNumber
        SupplierName
        OrderDate
        OrderDateDue
        OrderCartons
        OrderValue
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _Supplier As BOPurchases.cSupplierMaster
    Private _StoreAddress As String = String.Empty
    'Private _DatesSelected As Boolean = False

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnRetrieve.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            GetStoreAddress()
            GetSuppliers()
            spdOrder_Initialise()

            dtpFrom.Value = Now.Date.AddDays(-Now.Day + 1)
            '_DatesSelected = True

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub



    Private Sub GetStoreAddress()

        Dim RetailOptions As New BOSystem.cRetailOptions(_Oasys3DB)
        RetailOptions.AddLoadField(RetailOptions.StoreName)
        RetailOptions.AddLoadField(RetailOptions.AddressLine1)
        RetailOptions.AddLoadField(RetailOptions.AddressLine2)
        RetailOptions.AddLoadField(RetailOptions.AddressLine3)
        RetailOptions.LoadMatches()

        Dim arrStore As New List(Of String)
        Dim sbStore As New StringBuilder
        arrStore.Add(RetailOptions.StoreName.Value)
        arrStore.Add(RetailOptions.AddressLine1.Value)
        arrStore.Add(RetailOptions.AddressLine2.Value)
        arrStore.Add(RetailOptions.AddressLine3.Value)
        For Each str As String In arrStore
            If str.ToString.Trim.Length > 0 Then sbStore.Append(str & Environment.NewLine)
        Next

        _StoreAddress = sbStore.ToString

    End Sub

    Private Sub GetSuppliers()

        _Supplier = New BOPurchases.cSupplierMaster(_Oasys3DB)
        _Supplier.AddLoadField(_Supplier.Number)
        _Supplier.AddLoadField(_Supplier.Name)
        _Supplier.Suppliers = _Supplier.LoadMatches

        'Populate supplier combo box
        Dim dt As New DataTable
        dt.Columns.Add("number", GetType(String))
        dt.Columns.Add("name", GetType(String))
        dt.Columns.Add("display", GetType(String))
        dt.Rows.Add("00000", "", "---All---")

        For Each supplier As BOPurchases.cSupplierMaster In _Supplier.Suppliers
            dt.Rows.Add(supplier.Number.Value, supplier.Name.Value, supplier.Number.Value & Space(1) & supplier.Name.Value)
        Next

        Dim dv As DataView = New DataView(dt, "", "name", DataViewRowState.CurrentRows)
        cmbSupplier.ValueMember = "number"
        cmbSupplier.DisplayMember = "display"
        cmbSupplier.DataSource = dv

    End Sub

    'Private Function GetOrders() As List(Of BOPurchases.cPurchaseHeader)

    '    Dim Order As New BOPurchases.cPurchaseHeader(_Oasys3DB)
    '    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.Deleted, False)
    '    Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Order.DateOrderCreated, dtpFrom.Value)
    '    Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Order.DateOrderCreated, dtpTo.Value)

    '    If cmbSupplier.SelectedValue.ToString <> "00000" Then
    '        Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
    '        Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.SupplierNumber, cmbSupplier.SelectedValue.ToString)
    '    End If

    '    Order.Orders = Order.LoadMatches
    '    If Order.Orders.Count > 0 Then
    '        Return Order.Orders
    '    End If

    '    MessageBox.Show("No purchase orders within this time period", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '    Return Nothing

    'End Function


    Private Sub spdOrder_Initialise()

        Dim sheet As New SheetView
        sheet.SelectionUnit = Model.SelectionUnit.Row
        sheet.SelectionPolicy = Model.SelectionPolicy.Single
        sheet.RowCount = 0
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.SetColumnAllowAutoSort(-1, True)

        'default properties
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.DefaultStyle.Locked = True
        sheet.ColumnHeader.Rows(0).HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.ColumnHeader.Rows(0).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

        'make F keys available in spread
        Dim im As InputMap = spdOrder.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdOrder.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        'columns types
        Dim typeInt As New CellType.NumberCellType
        typeInt.DecimalPlaces = 0

        'column headers
        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
        sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        sheet.ColumnHeader.Cells(0, col.PONumber, 0, col.SupplierName).HorizontalAlignment = CellHorizontalAlignment.Left

        'columns
        sheet.Columns(col.PONumber).Label = "PO Number"
        sheet.Columns(col.PONumber, col.SupplierName).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(col.ConsignNumber).Label = "Consignment"
        sheet.Columns(col.SupplierNumber).Label = "Supplier"
        sheet.Columns(col.SupplierName).Label = "Name"
        sheet.Columns(col.OrderDate).Label = "Order Date"
        sheet.Columns(col.OrderDate, col.OrderDateDue).CellType = New CellType.DateTimeCellType
        sheet.Columns(col.OrderDateDue).Label = "Due Date"
        sheet.Columns(col.OrderCartons).Label = "No. Cartons"
        sheet.Columns(col.OrderCartons).CellType = typeInt
        sheet.Columns(col.OrderValue).Label = "Value"
        sheet.Columns(col.OrderValue).CellType = New CellType.NumberCellType

        spdOrder.Sheets.Add(sheet)

    End Sub

    Private Sub spdOrder_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles spdOrder.KeyPress

        e.Handled = True
        If e.KeyChar = ChrW(Keys.Enter) Then
            btnPrint.PerformClick()
        End If

    End Sub

    Private Sub spdOrder_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdOrder.Resize

        Dim spd As FpSpread = CType(sender, FpSpread)
        If spd.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spd.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spd.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spd.Width
        Dim dataAreaHeight As Single = spd.Height

        If spd.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spd.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spd.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If

            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spd.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spd.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth

            'get columns to resize and new widths in arraylist
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spd.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub

    Private Sub spdOrder_Changed()

        Dim sheet As SheetView = spdOrder.ActiveSheet
        If sheet.RowCount = 0 Then
            MessageBox.Show("No purchase orders within selected parameters", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim firstRow As Integer = -1
            For rowIndex As Integer = sheet.RowCount - 1 To 0 Step -1
                If sheet.Rows(rowIndex).Visible Then firstRow = rowIndex
            Next

            If firstRow = -1 Then
                btnPrint.Enabled = False
            Else
                btnPrint.Enabled = True
                sheet.SetActiveCell(firstRow, 0)
                sheet.AddSelection(firstRow, 0, 1, [Enum].GetValues(GetType(col)).Length)
            End If
        End If

    End Sub

    Private Sub AddOrderToSpread(ByVal order As BOPurchases.cPurchaseHeader)

        'check whether order already on sheet
        Dim sheet As SheetView = spdOrder.ActiveSheet
        If sheet.RowCount > 0 Then
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If sheet.GetValue(rowIndex, col.PONumber).ToString = order.PONumber.Value Then Exit Sub
            Next
        End If

        sheet.AddRows(sheet.RowCount, 1)
        sheet.Rows(sheet.RowCount - 1).Tag = order
        sheet.SetValue(sheet.RowCount - 1, col.PONumber, order.PONumber.Value)
        If order.ConsignNumber.Value <> "000000" Then sheet.SetValue(sheet.RowCount - 1, col.ConsignNumber, order.ConsignNumber.Value)
        sheet.SetValue(sheet.RowCount - 1, col.SupplierNumber, order.SupplierNumber.Value)
        sheet.SetValue(sheet.RowCount - 1, col.SupplierName, order.Supplier.Name.Value)
        sheet.SetValue(sheet.RowCount - 1, col.OrderDate, order.DateOrderCreated.Value)
        sheet.SetValue(sheet.RowCount - 1, col.OrderDateDue, order.DateOrderDue.Value)
        sheet.SetValue(sheet.RowCount - 1, col.OrderCartons, order.OrderQtyCartons.Value)
        sheet.SetValue(sheet.RowCount - 1, col.OrderValue, order.OrderValue.Value)

        If sheet.RowCount > 0 Then
            'select first row
            spdOrder.Visible = True
            btnPrint.Enabled = True
            btnReset.Enabled = True

            sheet.SetActiveCell(0, 0)
            sheet.AddSelection(0, 0, 1, [Enum].GetValues(GetType(col)).Length)
            spdOrder_Resize(spdOrder, New EventArgs)
            spdOrder.Focus()
        End If

    End Sub



    Private Sub txtPO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPO.KeyPress

        DisplayStatus()

        e.Handled = True
        If e.KeyChar = ChrW(Keys.Enter) Then
            btnRetrieve.PerformClick()
            'Select Case txtPO.Text.Trim.Length
            '    Case Is <= 6
            '        Dim Order As New BOPurchases.cPurchaseHeader(_Oasys3DB)
            '        Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.Deleted, False)
            '        Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
            '        Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.PONumber, txtPO.Text.Trim.PadLeft(6, "0"c))
            '        If Order.LoadMatches.Count > 0 Then
            '            Dim print As New PrintOrder(Order, _StoreAddress)
            '            print.ShowDialog()
            '            print.Dispose()
            '        Else
            '            MessageBox.Show("No purchase order for this number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '        End If

            '    Case Else
            '        DisplayWarning("Please enter a valid PO number")
            'End Select
        End If
        If Char.IsControl(e.KeyChar) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub

    Private Sub txtSupplier_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSupplier.TextChanged

        Dim dv As DataView = CType(cmbSupplier.DataSource, DataView)
        dv.RowFilter = Nothing

        Select Case txtSupplier.Text.Trim.Length
            Case 0
                cmbSupplier.SelectedIndex = 0
            Case Is >= 3
                If IsNumeric(txtSupplier.Text.Trim) Then
                    dv.RowFilter = "number like '%" & txtSupplier.Text.Trim & "%'"
                Else
                    dv.RowFilter = "name like '%" & txtSupplier.Text.Trim & "%'"
                End If
            Case Else
                cmbSupplier.DroppedDown = False
        End Select

    End Sub

    Private Sub txtSupplier_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtSupplier.PreviewKeyDown

        If e.KeyCode = Keys.Tab And Not e.Shift Then
            cmbSupplier_KeyDown(sender, New KeyEventArgs(Keys.Down))
        End If

    End Sub

    Private Sub txtSupplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupplier.KeyDown

        If e.KeyCode = Keys.Tab Then
            cmbSupplier.Focus()
            cmbSupplier_KeyDown(sender, New KeyEventArgs(Keys.Down))
        End If

    End Sub

    Private Sub txtSupplier_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupplier.KeyPress

        Select Case e.KeyChar
            Case ChrW(Keys.Enter)
                Dim dv As DataView = CType(cmbSupplier.DataSource, DataView)
                If dv.Count = 1 Then
                    cmbSupplier_KeyPress(sender, e)
                ElseIf dv.Count > 1 Then
                    cmbSupplier.Focus()
                    cmbSupplier_KeyDown(sender, New KeyEventArgs(Keys.Down))
                End If
        End Select

    End Sub

    Private Sub cmbSupplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbSupplier.KeyDown

        e.Handled = False
        Select Case e.KeyData
            Case Keys.Down, Keys.Up
                If cmbSupplier.DroppedDown = False Then
                    cmbSupplier.DroppedDown = True
                    e.Handled = True
                End If
        End Select

    End Sub

    Private Sub cmbSupplier_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbSupplier.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) AndAlso cmbSupplier.SelectedValue IsNot Nothing Then
            btnRetrieve.PerformClick()
        End If

        'Try
        '    If e.KeyChar = ChrW(Keys.Enter) Then
        '        If cmbSupplier.SelectedValue IsNot Nothing Then

        '            Cursor = Cursors.WaitCursor

        '            Dim orders As List(Of BOPurchases.cPurchaseHeader) = GetOrders()
        '            Dim sheet As SheetView = spdOrder.ActiveSheet
        '            sheet.Rows.Remove(0, sheet.RowCount)

        '            If orders IsNot Nothing Then
        '                For Each order As BOPurchases.cPurchaseHeader In orders
        '                    AddOrderToSpread(order)
        '                Next
        '            End If

        '        End If
        '    End If

        'Finally
        '    Cursor = Cursors.Default
        'End Try

    End Sub

    Private Sub cmbSupplier_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSupplier.SelectionChangeCommitted
        cmbSupplier_KeyPress(sender, New KeyPressEventArgs(ChrW(Keys.Enter)))
    End Sub

    Private Sub dtp_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged, dtpTo.ValueChanged

        If dtpFrom.Value > dtpTo.Value Then dtpTo.Value = dtpFrom.Value
        If dtpTo.Value < dtpFrom.Value Then dtpFrom.Value = dtpTo.Value
        'If _DatesSelected Then cmbSupplier_KeyPress(sender, New KeyPressEventArgs(ChrW(Keys.Enter)))

    End Sub



    Private Sub btnRetrieve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click

        Try
            Cursor = Cursors.WaitCursor

            Dim Order As New BOPurchases.cPurchaseHeader(_Oasys3DB)
            Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.Deleted, False)

            Select Case txtPO.Text.Trim.Length
                Case 3, 4, 5, 6
                    Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.PONumber, txtPO.Text.Trim)
                Case Else
                    Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, Order.DateOrderCreated, dtpFrom.Value)
                    Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                    Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, Order.DateOrderCreated, dtpTo.Value)
            End Select

            If cmbSupplier.SelectedValue.ToString <> "00000" Then
                Order.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                Order.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, Order.SupplierNumber, cmbSupplier.SelectedValue.ToString)
            End If

            Order.SortBy(Order.PONumber.ColumnName, OasysDBBO.Oasys3.DB.clsOasys3DB.eOrderByType.Descending)

            Dim sheet As SheetView = spdOrder.ActiveSheet
            sheet.Rows.Remove(0, sheet.RowCount)

            Dim orders As List(Of BOPurchases.cPurchaseHeader) = Order.LoadMatches
            If orders.Count = 0 Then
                MessageBox.Show("No purchase orders returned", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                For Each orderItem As BOPurchases.cPurchaseHeader In orders
                    AddOrderToSpread(orderItem)
                Next
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim sheet As SheetView = spdOrder.ActiveSheet
        Dim order As BOPurchases.cPurchaseHeader = CType(sheet.Rows(sheet.ActiveRowIndex).Tag, BOPurchases.cPurchaseHeader)

        'load print window for order
        Dim print As New PrintOrder(order, _StoreAddress)
        print.ShowDialog()
        print.Dispose()

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        'clear sheet
        Dim sheet As SheetView = spdOrder.ActiveSheet
        sheet.Rows.Remove(0, sheet.RowCount)
        spdOrder.Visible = False
        btnPrint.Enabled = False
        btnReset.Enabled = False

        'clear textboxes
        txtSupplier.Clear()
        txtPO.Clear()
        txtPO.Focus()

        'reset dates
        '_DatesSelected = False
        dtpTo.Value = Now.Date
        dtpFrom.Value = Now.Date.AddDays(-Now.Day + 1)
        '_DatesSelected = True

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.FindForm.Close()
    End Sub

End Class
