Public Class PrintOrder
    Private Enum col
        SkuNumber = 0
        Description
        ProductCode
        Pack
        OrderQty
        Price
        Value
    End Enum
    Private Enum HeaderRow
        DateOrder = 0
        DateDue
        SoqNumber
        Space
        Address
        ColumnHeader
    End Enum
    Private _Order As BOPurchases.cPurchaseHeader
    Private _StoreAddress As String = String.Empty
    Private _SupplierAddress As String = String.Empty


    Sub New(ByVal Order As BOPurchases.cPurchaseHeader, ByVal StoreAddress As String)
        InitializeComponent()
        _Order = Order
        _StoreAddress = StoreAddress
        Text = "Purchase Order Number: " & Order.PONumber.Value
    End Sub 'New

    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Cursor = Cursors.WaitCursor
        Refresh()

        If Not GetSupplierAddress() Then Exit Sub
        If Not SheetSetup() Then Exit Sub
        If Not SheetAddItems() Then Exit Sub

        Cursor = Cursors.Default
        btnPrint.Focus()

    End Sub 'Form_Shown

    Private Sub FormKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub 'FormKeyDown


    Private Function GetSupplierAddress() As Boolean

        Try
            Dim arr As New List(Of String)
            Dim sb As New StringBuilder
            arr.Add(_Order.Supplier.Number.Value & Space(1) & _Order.Supplier.Name.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressLine1.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressLine2.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressLine3.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressLine4.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressLine5.Value)
            arr.Add(_Order.Supplier.ReturnDetails.AddressPostcode.Value)
            For Each str As String In arr
                If str.ToString.Trim.Length > 0 Then sb.Append(str & Environment.NewLine)
            Next

            _SupplierAddress = sb.ToString
            Return True

        Catch ex As Exception
            MessageBox.Show("Problem getting supplier address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Cursor = Cursors.Default
            Return False
        End Try

    End Function 'GetSupplierAddress

    Private Function SheetSetup() As Boolean

        Try
            Dim sheet As New SheetView
            sheet.RowCount = 0
            sheet.RowHeader.ColumnCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
            sheet.ColumnHeader.RowCount = [Enum].GetValues(GetType(HeaderRow)).Length
            sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
            sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
            sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
            sheet.DefaultStyle.Locked = True
            sheet.DefaultStyle.CellType = New CellType.TextCellType
            sheet.VerticalGridLine = New GridLine(GridLineType.None)
            sheet.HorizontalGridLine = New GridLine(GridLineType.None)

            'Print info stuff.
            sheet.PrintInfo.Header = "/c/fz""14""Purchase Order Print"
            sheet.PrintInfo.Header &= "/n/c/fz""10""PO Number: " & _Order.PONumber.Value
            sheet.PrintInfo.Header &= "/n "
            sheet.PrintInfo.Footer = "/lPrinted: " & Now
            sheet.PrintInfo.Footer &= "/cPage /p of /pc"
            sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            sheet.PrintInfo.Orientation = PrintOrientation.Landscape
            sheet.PrintInfo.Margin.Left = 50
            sheet.PrintInfo.Margin.Right = 50
            sheet.PrintInfo.ShowBorder = False
            sheet.PrintInfo.UseSmartPrint = True
            sheet.PrintInfo.ShowPrintDialog = True

            'Column types
            Dim typeMultiline = New CellType.TextCellType
            typeMultiline.Multiline = True
            Dim exp As New RegularExpressions.Regex(Environment.NewLine)

            'column headers
            sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeader.DefaultStyle.Border = New EmptyBorder
            sheet.ColumnHeader.DefaultStyle.CellType = typeMultiline
            sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.ColumnHeader.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Top

            sheet.ColumnHeader.Cells(HeaderRow.DateOrder, col.SkuNumber).Value = "Order Date"
            sheet.ColumnHeader.Cells(HeaderRow.DateDue, col.SkuNumber).Value = "Due Date"
            sheet.ColumnHeader.Cells(HeaderRow.SoqNumber, col.SkuNumber).Value = "SOQ Number"
            sheet.ColumnHeader.Cells(HeaderRow.Address, col.SkuNumber).Value = "To"
            sheet.ColumnHeader.Columns(col.SkuNumber).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)

            sheet.ColumnHeader.Cells(HeaderRow.DateOrder, col.Description).Value = FormatDateTime(_Order.DateOrderCreated.Value, DateFormat.ShortDate)
            sheet.ColumnHeader.Cells(HeaderRow.DateDue, col.Description).Value = FormatDateTime(_Order.DateOrderDue.Value, DateFormat.ShortDate)
            If _Order.SOQNumber.Value <> "000000" Then sheet.ColumnHeader.Cells(HeaderRow.SoqNumber, col.Description).Value = _Order.SOQNumber.Value
            sheet.ColumnHeader.Cells(HeaderRow.Address, col.Description).Value = _SupplierAddress

            sheet.ColumnHeader.Cells(HeaderRow.Address, col.ProductCode).Value = "From"
            sheet.ColumnHeader.Columns(col.ProductCode).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)

            sheet.ColumnHeader.Cells(HeaderRow.Address, col.Pack).Value = _StoreAddress
            sheet.ColumnHeader.Cells(HeaderRow.Address, col.Pack).ColumnSpan = 4
            sheet.ColumnHeader.Rows(HeaderRow.Address).Height = sheet.ColumnHeader.Rows(HeaderRow.Address).Height * Math.Max(exp.Matches(_SupplierAddress).Count, exp.Matches(_StoreAddress).Count)

            sheet.ColumnHeader.Rows(HeaderRow.ColumnHeader).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            sheet.ColumnHeader.Rows(HeaderRow.ColumnHeader).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.SkuNumber).Value = "SKU Number"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.Description).Value = "Description"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.ProductCode).Value = "Code"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.Pack).Value = "Pack"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.OrderQty).Value = "Order Qty"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.Price).Value = "Price"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.Value).Value = "Value"
            sheet.ColumnHeader.Cells(HeaderRow.ColumnHeader, col.Pack, HeaderRow.ColumnHeader, col.Value).HorizontalAlignment = CellHorizontalAlignment.Right

            'columns
            sheet.Columns(col.SkuNumber).Width = 80
            sheet.Columns(col.SkuNumber).Resizable = False
            sheet.Columns(col.SkuNumber, col.ProductCode).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.Columns(col.ProductCode).Width = 80
            sheet.Columns(col.ProductCode).Resizable = False
            sheet.Columns(col.Pack, col.Value).Width = 80
            sheet.Columns(col.Pack, col.Value).Resizable = False
            sheet.Columns(col.Price, col.Value).CellType = New CellType.NumberCellType

            spdPrint.Sheets.Add(sheet)
            Return True

        Catch ex As Exception
            MessageBox.Show("Problem setting up sheet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Cursor = Cursors.Default
            Return False
        End Try

    End Function 'SheetSetup

    Private Function SheetAddItems() As Boolean

        Try
            Dim sheet As SheetView = spdPrint.ActiveSheet

            For Each line As BOPurchases.cPurchaseLine In _Order.Lines
                sheet.AddRows(sheet.RowCount, 1)
                sheet.SetValue(sheet.RowCount - 1, col.SkuNumber, line.SKUNumber.Value)
                sheet.SetValue(sheet.RowCount - 1, col.Description, line.Stock.Description.Value)
                sheet.SetValue(sheet.RowCount - 1, col.ProductCode, line.SKUProductCode.Value)
                sheet.SetValue(sheet.RowCount - 1, col.Pack, line.Stock.SupplierPackSize.Value)
                sheet.SetValue(sheet.RowCount - 1, col.OrderQty, line.OrderQty.Value)
                sheet.SetValue(sheet.RowCount - 1, col.Price, line.OrderPrice.Value)
                sheet.SetFormula(sheet.RowCount - 1, col.Value, "RC[-2] * RC[-1]")
            Next

            If sheet.RowCount > 0 Then
                'add totals row
                sheet.AddRows(sheet.RowCount, 1)
                sheet.SetValue(sheet.RowCount - 1, col.SkuNumber, "TOTALS")
                sheet.SetFormula(sheet.RowCount - 1, col.OrderQty, "SUM(R1C:R" & sheet.RowCount - 1 & "C)")
                sheet.SetFormula(sheet.RowCount - 1, col.Value, "SUM(R1C:R" & sheet.RowCount - 1 & "C)")
                sheet.Rows(sheet.RowCount - 1).Border = New LineBorder(Color.Black, 1, False, True, False, True)
                sheet.Rows(sheet.RowCount - 1).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, FontStyle.Bold)
                spdPrint.Visible = True
                SheetResize()
            End If
            Return True

        Catch ex As Exception
            MessageBox.Show("Problem adding lines to sheet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Cursor = Cursors.Default
            Return False
        End Try

    End Function 'SheetAddItems

    Private Sub SheetResize() Handles spdPrint.Resize

        Dim spread As FpSpread = spdPrint
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'Perform resize for active sheet of spread and initialise variables.
        Dim sheet As SheetView = spread.ActiveSheet
        Dim sngDataAreaWidth As Single = 0
        Dim sngDataAreaHeight As Single = 0
        Dim intBorderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim intBorderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim sngRowHeadersWidth As Single = 0
        Dim sngColHeadersHeight As Single = 0
        Dim intScrollbarWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim intScrollbarHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim intTabStripHeight As Integer = 10
        Dim intNumResizableCols As Integer = sheet.ColumnCount
        Dim sngColsWidth As Single = 0

        'Get row header width.
        For Each header As Column In sheet.RowHeader.Columns
            If header.Visible Then sngRowHeadersWidth += header.Width
        Next header

        'Get column header height.
        For Each header As Row In sheet.ColumnHeader.Rows
            If header.Visible Then sngColHeadersHeight += header.Height
        Next header

        'Get data area height
        sngDataAreaHeight = spread.Height - sngColHeadersHeight - intScrollbarHeight
        If spread.TabStripPolicy = TabStripPolicy.Always Then sngDataAreaHeight -= intTabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then sngDataAreaHeight -= (intBorderHeight * 2)

        'If number rows * row heights greater than available area then display scrollbar
        If sheet.RowCount * sheet.Rows.Default.Height > sngDataAreaHeight Then
            spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
        End If

        'Get data area width and number of resizable columns from sheet.
        sngDataAreaWidth = spread.Width - sngRowHeadersWidth
        For Each column As Column In sheet.Columns
            If column.Visible Then
                If column.Resizable Then 'Set width to show all data.
                    column.Width = column.GetPreferredWidth
                    sngColsWidth += column.Width
                Else
                    intNumResizableCols -= 1
                    sngDataAreaWidth -= column.Width
                End If
            Else
                intNumResizableCols -= 1
            End If
        Next column

        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then sngDataAreaWidth -= (intBorderWidth * 2)
        If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sngDataAreaWidth -= intScrollbarWidth

        'If total width of colums greater than available area then show scrollbar else
        'increase resizable columns to fill up grey area of spread.
        If sngColsWidth > sngDataAreaWidth Then
            spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
        Else
            Dim intColIncrease As Integer = CInt((sngDataAreaWidth - sngColsWidth) / intNumResizableCols)
            For Each column As Column In sheet.Columns
                If column.Visible And column.Resizable Then column.Width += intColIncrease
            Next column
        End If

    End Sub 'SheetResize



    Private Sub PrintSheet(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        spdPrint.PrintSheet(spdPrint.ActiveSheet)

    End Sub 'PrintSheet

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub 'btnExit_Click


End Class