﻿Imports System.Data

Public Class SupplierTable
    Inherits DataTable
    Public Shared ReadOnly Name As String = "Name"
    Public Shared ReadOnly Number As String = "Number"
    Public Shared ReadOnly Stocks As String = "Stocks"
    Public Shared ReadOnly Progress As String = "Progress"
    Public Shared ReadOnly Status As String = "Status"

    Public Sub New()
        MyBase.New()
        Me.Columns.Add(Number, GetType(String))
        Me.Columns.Add(Name, GetType(String))
        Me.Columns.Add(Stocks, GetType(Integer))
        Me.Columns.Add(Progress, GetType(Integer))
        Me.Columns.Add(Status, GetType(String))
        Me.PrimaryKey = New DataColumn() {Me.Columns(Number)}
    End Sub

End Class