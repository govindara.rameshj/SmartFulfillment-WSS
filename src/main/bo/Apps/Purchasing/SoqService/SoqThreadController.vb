﻿Friend Class SoqThreadController
    Private _soqThreads As New List(Of SoqThread)
    Private _thread As Threading.Thread
    Private _abort As Boolean

    Public Function Completed() As Boolean
        Return (_soqThreads.Count = 0)
    End Function

    Public Sub New()
    End Sub

    Public Sub Add(ByVal soq As SoqThread)
        AddHandler soq.Completed, AddressOf SoqCompleted
        _soqThreads.Add(soq)
    End Sub

    Public Sub Start()
        _thread = New Threading.Thread(AddressOf DoWork)
        _thread.Start()
    End Sub

    Private Sub DoWork()

        '        For Each soq As SoqThread In _soqThreads
        '           soq.Start()
        '        Next
        ' Number of SoqThreads in _soqThreads may change during exection, so use a counter & while loop instead
        Dim i As Integer = 0

        Do While i < _soqThreads.Count - 1
            _soqThreads.Item(i).Start()
            i += 1
        Loop
    End Sub

    Public Sub Abort()
        _abort = True
        _thread.Abort()
        'For Each soq As SoqThread In _soqThreads
        '    soq.Abort()
        'Next
        ' Number of SoqThreads in _soqThreads may change during exection, so use a counter & while loop instead
        Dim i As Integer = 0

        Do While i < _soqThreads.Count - 1
            _soqThreads.Item(i).Abort()
            i += 1
        Loop
    End Sub

    Private Sub SoqCompleted(ByVal soq As SoqThread)

        SyncLock Me
            If _abort Then Exit Sub

            If _soqThreads.Count > 0 Then
                If _soqThreads.Exists(Function(s As SoqThread) s.ThreadState = Threading.ThreadState.Unstarted) Then
                    _soqThreads.Find(Function(s As SoqThread) s.ThreadState = Threading.ThreadState.Unstarted).Start()
                End If
            End If

            _soqThreads.Remove(soq)
        End SyncLock

    End Sub

End Class