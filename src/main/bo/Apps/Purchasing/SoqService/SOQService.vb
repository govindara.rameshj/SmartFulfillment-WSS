Imports System.Data
Imports DevExpress.XtraGrid
Imports DevExpress.XtraEditors.Repository
Imports System.Threading
Imports Purchasing.Core
Imports System.ComponentModel

Public Class SoqService
    Private _soqThreadController As New SoqThreadController
    Private _suppliers As BindingList(Of Supplier)
    Private _numberToProcess As Integer = 10
    Private _exitWhenProcessed As Boolean
    Delegate Sub SoqStartedDelegate(ByVal sup As Supplier)
    Delegate Sub SoqProgressDelegate(ByVal sup As Supplier, ByVal progress As Integer)
    Delegate Sub SoqCompletedDelegate(ByVal sup As Supplier, ByVal status As Soq.CompletedStatus)
    Delegate Sub SoqCancelledDelegate(ByVal sup As Supplier)
    Delegate Sub ApplicationThreadExceptionDelegate(ByVal sender As Object, ByVal e As ThreadExceptionEventArgs)

    ''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author       : Partha Dutta
    ' Create date  : 02/09/2011
    ' Referral No  : Back Office 3.8 - RF0856
    ' Description  : Multi-threading not working when executable called via "Daily Close"; threads crashing
    '                Due to time pressures have modified it to a non multi-threaded application
    '
    '                Have also added code to indicate that this process has completed to the "Daily Close" process 
    ''''''''''''''''''''''''''''''''''''''''''''''''

    Private WithEvents _SOQ As Soq

    Private Sub SOQService_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddGlobalErrorHandler()

        'set control labels
        btnExit.Text = My.Resources.F12Exit
        btnSoq.Text = My.Resources.F5Soq

        'see if need to run through command line parameter input
        If My.Application.CommandLineArgs.Count > 0 Then
            _exitWhenProcessed = True
            For Each text As String In My.Application.CommandLineArgs
                AddSuppliers(text)
                AddSuppliersToGrid()

                'RF0856 : non multi-threaded version
                AlternativeStart()

            Next

            'indciate that process has completed
            If _exitWhenProcessed Then

                'Kill the TASKCOMP file.
                If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP") = True Then File.Delete(My.Computer.FileSystem.CurrentDirectory & "\TASKCOMP")

                Close()

            End If

        End If

    End Sub

    Private Sub SOQService_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnSoq.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub AddGlobalErrorHandler()
        AddHandler Application.ThreadException, New ThreadExceptionEventHandler(AddressOf ApplicationThreadException)
    End Sub

    Private Sub ApplicationThreadException(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New ApplicationThreadExceptionDelegate(AddressOf ApplicationThreadException), New Object() {sender, e})
        Else
            'trace log and write to event log
            Trace.WriteLine(e.Exception.ToString, Me.Name)
            EventLog.WriteEntry("Application", e.Exception.ToString, EventLogEntryType.Error)

            'display exception message to user in dialog box
            MessageBox.Show(e.Exception.Message, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub


    Private Sub AddSuppliers(ByVal text As String)

        _suppliers = New BindingList(Of Supplier)

        Select Case True
            Case text.Length = 0 OrElse text.ToUpper.StartsWith("ALL")
                _suppliers = Supplier.GetAllForOrder

            Case Else
                For Each supplierNumber As String In text.Split(","c)
                    Dim sup As Supplier = Supplier.GetByNumber(supplierNumber.Trim.PadLeft(5, "0"c))
                    If sup IsNot Nothing Then _suppliers.Add(sup)
                Next
        End Select

    End Sub

    Private Sub AddSuppliersToGrid()

        Try
            Cursor = Cursors.WaitCursor

            Dim dt As DataTable = New SupplierTable
            For Each sup As Supplier In _suppliers
                Dim dr As DataRow = dt.NewRow
                dr(SupplierTable.Number) = sup.Number
                dr(SupplierTable.Name) = sup.Name
                dr(SupplierTable.Stocks) = sup.Stocks.Count
                dr(SupplierTable.Progress) = 0
                dr(SupplierTable.Status) = ""
                dt.Rows.Add(dr)
            Next

            'add progress bar to grid and bind
            Dim riProgress As New RepositoryItemProgressBar
            gridSupplier.RepositoryItems.Add(riProgress)
            gridSupplier.DataSource = dt

            For Each col As Columns.GridColumn In viewSupplier.Columns
                Select Case col.FieldName
                    Case SupplierTable.Number : col.MaxWidth = 60
                    Case SupplierTable.Name : col.MinWidth = 150
                    Case SupplierTable.Stocks : col.MaxWidth = 60
                    Case SupplierTable.Progress : col.ColumnEdit = riProgress
                    Case SupplierTable.Status : col.MinWidth = 200
                End Select
            Next

            Me.Refresh()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub SoqControllerStart()

        btnExit.Text = My.Resources.F12Cancel
        btnExit.Refresh()

        'create new thread for each supplier and add to controller
        _soqThreadController = New SoqThreadController

        For index As Integer = 0 To Math.Min(_suppliers.Count, _numberToProcess) - 1
            Dim soq As New SoqThread(_suppliers(index))
            soq.startDelegate = AddressOf SoqStarted
            soq.progressDelegate = AddressOf SoqProgress
            soq.endDelegate = AddressOf SoqCompleted
            soq.cancelledDelegate = AddressOf SoqCancelled
            _soqThreadController.Add(soq)
        Next

        _soqThreadController.Start()

    End Sub



    Private Sub SoqStarted(ByVal sup As Supplier)

        If gridSupplier.InvokeRequired Then
            gridSupplier.Invoke(New SoqStartedDelegate(AddressOf SoqStarted), New Object() {sup})
        Else
            'find supplier in table
            Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
            Dim dr As DataRow = dt.Rows.Find(sup.Number)
            dr(SupplierTable.Progress) = 0
            dr(SupplierTable.Status) = My.Resources.InProgress
        End If

    End Sub

    Private Sub SoqProgress(ByVal sup As Supplier, ByVal progress As Integer)

        If gridSupplier.InvokeRequired Then
            gridSupplier.Invoke(New SoqProgressDelegate(AddressOf SoqProgress), New Object() {sup, progress})
        Else
            'find supplier in table
            Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
            Dim dr As DataRow = dt.Rows.Find(sup.Number)
            dr(SupplierTable.Progress) = progress
        End If

    End Sub

    Private Sub SoqCompleted(ByVal sup As Supplier, ByVal status As Soq.CompletedStatus)

        If gridSupplier.InvokeRequired Then
            gridSupplier.Invoke(New SoqCompletedDelegate(AddressOf SoqCompleted), New Object() {sup, status})
        Else
            'find supplier in table
            Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
            Dim dr As DataRow = dt.Rows.Find(sup.Number)
            dr(SupplierTable.Progress) = 100
            Select Case status
                Case Soq.CompletedStatus.Cancelled : dr(SupplierTable.Status) = My.Resources.CompletedCancelled
                Case Soq.CompletedStatus.Error : dr(SupplierTable.Status) = My.Resources.CompletedError
                Case Soq.CompletedStatus.NoStocks : dr(SupplierTable.Status) = My.Resources.CompletedNoStocks
                Case Soq.CompletedStatus.Successful : dr(SupplierTable.Status) = My.Resources.CompletedSuccessful
                Case Soq.CompletedStatus.Deleted : dr(SupplierTable.Status) = My.Resources.CompletedDeleted
            End Select

            'remove supplier from collection
            _suppliers.Remove(sup)

            If _soqThreadController.Completed Then
                'check if any more suppliers to do
                If _suppliers.Count > 0 Then
                    SoqControllerStart()

                Else
                    btnExit.Text = My.Resources.F12Exit
                    If _exitWhenProcessed Then Close()

                    ''check that all entered supplier numbers are valid
                    'If _supplierNumbers.Count > 0 Then
                    '    Dim dt As DataTable = gridSupplier.DataSource
                    '    For Each number As String In _supplierNumbers
                    '        'try and find number in table and add new row if not there
                    '        Dim dr As DataRow = dt.Rows.Find(number)
                    '        If dr Is Nothing Then
                    '            Dim newRow As DataRow = dt.NewRow
                    '            newRow(SupplierTable.Number) = number
                    '            newRow(SupplierTable.Status) = My.Resources.CompletedInvalid
                    '            dt.Rows.Add(newRow)
                    '        End If
                    '    Next
                    'End If

                End If

            End If
        End If

    End Sub

    Private Sub SoqCancelled(ByVal sup As Supplier)

        If gridSupplier.InvokeRequired Then
            gridSupplier.Invoke(New SoqCancelledDelegate(AddressOf SoqCancelled), New Object() {sup})
        Else
            'find supplier in table
            Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
            Dim dr As DataRow = dt.Rows.Find(sup.Number)
            dr(SupplierTable.Status) = My.Resources.CompletedCancelled
        End If

    End Sub



    Private Sub txtSuppliers_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSuppliers.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            btnSoq.PerformClick()
        End If

    End Sub

    Private Sub btnSoq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSoq.Click

        If txtSuppliers.Text.Length = 0 Then
            soqError.SetError(txtSuppliers, My.Resources.supplierEmpty)
            soqError.SetIconAlignment(txtSuppliers, ErrorIconAlignment.MiddleLeft)
        Else
            soqError.SetError(txtSuppliers, "")
            AddSuppliers(txtSuppliers.Text)
            AddSuppliersToGrid()

            'RF0856 : non multi-threaded version
            AlternativeStart()

        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Select Case btnExit.Text
            Case My.Resources.F12Cancel
                btnExit.Text = My.Resources.F12Exit
                _soqThreadController.Abort()

            Case Else
                Close()
        End Select

    End Sub

#Region "Non Multi-Threaded Code"

    Private Sub AlternativeStart()

        For index As Integer = 0 To _suppliers.Count - 1

            Application.DoEvents()

            _SOQ = New Soq()
            _SOQ.PerformInitialisation(_suppliers(index))

            Application.DoEvents()

        Next

    End Sub

    Private Sub _SOQ_SoqStarted(ByVal sup As Purchasing.Core.Supplier) Handles _SOQ.SoqStarted

        Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
        Dim dr As DataRow = dt.Rows.Find(sup.Number)

        dr(SupplierTable.Progress) = 0
        Application.DoEvents()

    End Sub

    Private Sub _SOQ_SoqProgress(ByVal sup As Purchasing.Core.Supplier, ByVal progress As Integer) Handles _SOQ.SoqProgress

        Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
        Dim dr As DataRow = dt.Rows.Find(sup.Number)

        dr(SupplierTable.Progress) = progress
        Application.DoEvents()

    End Sub

    Private Sub _SOQ_SoqCompleted(ByVal sup As Purchasing.Core.Supplier, ByVal status As Purchasing.Core.Soq.CompletedStatus) Handles _SOQ.SoqCompleted

        Dim dt As DataTable = CType(gridSupplier.DataSource, DataTable)
        Dim dr As DataRow = dt.Rows.Find(sup.Number)

        dr(SupplierTable.Progress) = 100
        Select Case status
            Case Soq.CompletedStatus.Cancelled : dr(SupplierTable.Status) = My.Resources.CompletedCancelled
            Case Soq.CompletedStatus.Error : dr(SupplierTable.Status) = My.Resources.CompletedError
            Case Soq.CompletedStatus.NoStocks : dr(SupplierTable.Status) = My.Resources.CompletedNoStocks
            Case Soq.CompletedStatus.Successful : dr(SupplierTable.Status) = My.Resources.CompletedSuccessful
            Case Soq.CompletedStatus.Deleted : dr(SupplierTable.Status) = My.Resources.CompletedDeleted
        End Select
        Application.DoEvents()

    End Sub

#End Region

End Class