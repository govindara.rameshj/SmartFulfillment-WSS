﻿Imports SoqService
Imports System.Threading
Imports Purchasing.Core

Friend Class SoqThread
    Private WithEvents _soq As Soq
    Private _thread As Threading.Thread = Nothing
    Private _supplier As Supplier
    Public startDelegate As SoqStartedDelegate
    Public progressDelegate As SoqProgressDelegate
    Public endDelegate As SoqCompletedDelegate
    Public cancelledDelegate As SoqCancelledDelegate
    Public Event Completed(ByVal soq As SoqThread)

    Public ReadOnly Property ThreadState() As Threading.ThreadState
        Get
            If _thread Is Nothing Then Return Threading.ThreadState.Unstarted
            Return _thread.ThreadState
        End Get
    End Property

    Public Sub New(ByRef sup As Supplier)
        _supplier = sup
    End Sub

    Public Sub Start()
        _thread = New Threading.Thread(AddressOf DoWork)
        _thread.Start()
    End Sub

    Public Sub Abort()

        Try
            If _thread IsNot Nothing AndAlso _thread.IsAlive Then
                Select Case _thread.ThreadState
                    Case Threading.ThreadState.Running
                        _thread.Abort()
                        SoqCancelled(_supplier)
                End Select
            End If

        Catch ex As ThreadAbortException
            SoqCancelled(_supplier)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DoWork()
        'instantiate new soq class and attach event handlers
        _soq = New Soq()
        AddHandler _soq.SoqStarted, AddressOf SoqStarted
        AddHandler _soq.SoqProgress, AddressOf SoqProgress
        AddHandler _soq.SoqCompleted, AddressOf SoqCompleted
        _soq.PerformInitialisation(_supplier)
    End Sub

    Private Sub SoqStarted(ByVal sup As Supplier)
        startDelegate(sup)
    End Sub

    Private Sub SoqProgress(ByVal sup As Supplier, ByVal progress As Integer)
        progressDelegate(sup, progress)
    End Sub

    Private Sub SoqCompleted(ByVal sup As Supplier, ByVal status As Soq.CompletedStatus)

        RaiseEvent Completed(Me)
        Select Case _thread.ThreadState
            Case Threading.ThreadState.Aborted : endDelegate(sup, Soq.CompletedStatus.Cancelled)
            Case Else : endDelegate(sup, status)
        End Select

    End Sub

    Private Sub SoqCancelled(ByVal sup As Supplier)
        cancelledDelegate(sup)
    End Sub

End Class