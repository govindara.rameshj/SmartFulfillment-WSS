<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SoqService
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnSoq = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.txtSuppliers = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.gridSupplier = New DevExpress.XtraGrid.GridControl
        Me.viewSupplier = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.soqError = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.gridSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.viewSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soqError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSoq
        '
        Me.btnSoq.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSoq.Location = New System.Drawing.Point(12, 328)
        Me.btnSoq.Name = "btnSoq"
        Me.btnSoq.Size = New System.Drawing.Size(76, 39)
        Me.btnSoq.TabIndex = 1
        Me.btnSoq.Text = "Soq"
        Me.btnSoq.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(512, 328)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtSuppliers
        '
        Me.txtSuppliers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSuppliers.Location = New System.Drawing.Point(11, 28)
        Me.txtSuppliers.Name = "txtSuppliers"
        Me.txtSuppliers.Size = New System.Drawing.Size(577, 20)
        Me.txtSuppliers.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(8, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(580, 20)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Enter comma separated supplier numbers or 'ALL' for all suppliers"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gridSupplier
        '
        Me.gridSupplier.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridSupplier.Location = New System.Drawing.Point(11, 54)
        Me.gridSupplier.MainView = Me.viewSupplier
        Me.gridSupplier.Name = "gridSupplier"
        Me.gridSupplier.Size = New System.Drawing.Size(577, 264)
        Me.gridSupplier.TabIndex = 9
        Me.gridSupplier.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.viewSupplier})
        '
        'viewSupplier
        '
        Me.viewSupplier.GridControl = Me.gridSupplier
        Me.viewSupplier.Name = "viewSupplier"
        Me.viewSupplier.OptionsBehavior.Editable = False
        Me.viewSupplier.OptionsView.ShowGroupPanel = False
        '
        'soqError
        '
        Me.soqError.ContainerControl = Me
        '
        'SoqService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 379)
        Me.ControlBox = False
        Me.Controls.Add(Me.gridSupplier)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtSuppliers)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSoq)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(462, 195)
        Me.Name = "SoqService"
        Me.ShowIcon = False
        Me.Text = "SOQ Service"
        CType(Me.gridSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.viewSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soqError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSoq As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtSuppliers As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gridSupplier As DevExpress.XtraGrid.GridControl
    Friend WithEvents viewSupplier As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents soqError As System.Windows.Forms.ErrorProvider

End Class
