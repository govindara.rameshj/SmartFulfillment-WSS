Public Class ProcessIW

    Private _BusinessLockIW As IBusinessLockIW
    Private _LockID As Integer

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)

        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

    End Sub

    Protected Overrides Sub Form_Closed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs)

        RemoveLock()

        MyBase.Form_Closed(sender, e)

    End Sub

    Protected Overrides Sub DoProcessing()

        If CreateLock() = False Then

            FindForm.Close()
            Exit Sub

        End If

        Using proc As New Process
            proc.StartInfo.FileName = Application.StartupPath & "\ProcessTransmissions.exe"
            proc.StartInfo.Arguments = "HPSTI"
            proc.Start()
            proc.WaitForExit()
        End Using

        FindForm.Close()

    End Sub

#Region "Private Procedures And Functions"

    Private Function CreateLock() As Boolean

        _BusinessLockIW = (New BusinessLockIWFactory).GetImplementation

        _LockID = _BusinessLockIW.CreateLock(MyBase.WorkstationId, Now, "ProcessTransmissions.exe", "HPSTI")

        If _LockID = -1 Then

            MessageBox.Show("This program is already running on another machine", "Menu Process Already Running", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Return False

        End If

        Return True

    End Function

    Private Sub RemoveLock()

        If _LockID > 0 Then _BusinessLockIW.RemoveLock(_LockID)

        'application exit runs "close event" one or more times for this form
        'exact number depends on number of time this form is loaded
        _LockID = 0

    End Sub

#End Region

End Class