﻿Imports BLToolkit.EditableObjects

Public MustInherit Class ReceiptLineEditable
    Inherits EditableObject(Of ReceiptLineEditable)

    Public MustOverride Property ReceiptNumber() As String
    Public MustOverride Property Sequence() As String
    Public MustOverride Property SkuNumber() As String
    Public MustOverride Property SkuDescription() As String
    Public MustOverride Property SkuProductCode() As String
    Public MustOverride Property SkuPack() As Integer
    Public MustOverride Property OrderLineId() As Integer
    Public MustOverride Property OrderPrice() As Decimal
    Public MustOverride Property OrderQty() As Integer
    Public MustOverride Property ReceivedQty() As Integer
    Public MustOverride Property ReceivedPrice() As Decimal
    Public MustOverride Property ReturnQty() As Integer
    Public MustOverride Property ReasonCode() As String
    Public MustOverride Property ReceivedOriginal() As Integer

End Class
