﻿Imports System.Data.SqlClient
Imports System.Text
Imports Oasys.Core

<CLSCompliant(True)> Public Class Container
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _assemblyDepotNumber As String
    Private _number As String
    Private _dateDespatch As Date
    Private _description As String
    Private _despatchDepotNumber As String
    Private _vehicleLoadReference As String
    Private _numberLines As Integer
    Private _parentNumber As String
    Private _isReceived As Boolean
    Private _printRequired As Boolean
    Private _dateDelivery As Date
    Private _value As Decimal
    Private _automaticallyPrinted As Boolean
    Private _rejectedReason As String

    Private _lines As ContainerLineCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("AssemblyDepotNumber")> Public Property AssemblyDepotNumber() As String
        Get
            Return _assemblyDepotNumber
        End Get
        Private Set(ByVal value As String)
            _assemblyDepotNumber = value
        End Set
    End Property
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("DateDespatch")> Public Property DateDespatch() As Date
        Get
            Return _dateDespatch
        End Get
        Private Set(ByVal value As Date)
            _dateDespatch = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("DespatchDepotNumber")> Public Property DespatchDepotNumber() As String
        Get
            Return _despatchDepotNumber
        End Get
        Private Set(ByVal value As String)
            _despatchDepotNumber = value
        End Set
    End Property
    <ColumnMapping("VehicleLoadReference")> Public Property VehicleLoadReference() As String
        Get
            Return _vehicleLoadReference
        End Get
        Private Set(ByVal value As String)
            _vehicleLoadReference = value
        End Set
    End Property
    <ColumnMapping("NumberLines")> Public Property NumberLines() As Integer
        Get
            Return _numberLines
        End Get
        Private Set(ByVal value As Integer)
            _numberLines = value
        End Set
    End Property
    <ColumnMapping("ParentNumber")> Public Property ParentNumber() As String
        Get
            Return _parentNumber
        End Get
        Private Set(ByVal value As String)
            _parentNumber = value
        End Set
    End Property
    <ColumnMapping("IsReceived")> Public Property IsReceived() As Boolean
        Get
            Return _isReceived
        End Get
        Private Set(ByVal value As Boolean)
            _isReceived = value
        End Set
    End Property
    <ColumnMapping("PrintRequired")> Public Property PrintRequired() As Boolean
        Get
            Return _printRequired
        End Get
        Private Set(ByVal value As Boolean)
            _printRequired = value
        End Set
    End Property
    <ColumnMapping("DateDelivery")> Public Property DateDelivery() As Date
        Get
            Return _dateDelivery
        End Get
        Private Set(ByVal value As Date)
            _dateDelivery = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Private Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
    <ColumnMapping("AutomaticallyPrinted")> Public Property AutomaticallyPrinted() As Boolean
        Get
            Return _automaticallyPrinted
        End Get
        Private Set(ByVal value As Boolean)
            _automaticallyPrinted = value
        End Set
    End Property
    <ColumnMapping("RejectedReason")> Public Property RejectedReason() As String
        Get
            Return _rejectedReason
        End Get
        Private Set(ByVal value As String)
            _rejectedReason = value
        End Set
    End Property

    Public ReadOnly Property Lines() As ContainerLineCollection
        Get
            If _lines Is Nothing Then _lines = New ContainerLineCollection(Me)
            Return _lines
        End Get
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region


    Public Shared Function GetByDateRange(ByVal startDate As Date, ByVal endDate As Date) As ContainerCollection

        Dim col As New ContainerCollection
        Dim dt As DataTable = DataOperations.ContainerGetByDateRange(startDate, endDate)

        For Each dr As DataRow In dt.Rows
            col.Add(New Container(dr))
        Next

        Return col

    End Function

End Class
