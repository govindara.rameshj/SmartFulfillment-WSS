﻿Public Class DataChangedEventArgs
    Inherits EventArgs

    Private _hasModification As Boolean

    Public Property HasModifications() As Boolean
        Get
            Return _hasModification
        End Get
        Private Set(ByVal value As Boolean)
            _hasModification = value
        End Set
    End Property

    Public Sub New(hasMofication As Boolean)
        _hasModification = hasMofication
    End Sub

End Class
