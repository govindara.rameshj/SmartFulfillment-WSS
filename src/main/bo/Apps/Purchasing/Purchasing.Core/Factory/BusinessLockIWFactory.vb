﻿Public Class BusinessLockIWFactory
    Inherits RequirementSwitchFactory(Of IBusinessLockIW)

    Private Const _RequirementSwitchRF1029 As Integer = -1029

    Public Overrides Function ImplementationA() As IBusinessLockIW

        Return New EnabledBusinessLockIW

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchRF1029)

    End Function

    Public Overrides Function ImplementationB() As IBusinessLockIW

        Return New DisabledBusinessLockIW

    End Function

End Class