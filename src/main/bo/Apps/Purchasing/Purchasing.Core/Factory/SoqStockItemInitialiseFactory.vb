﻿Public Class SoqStockItemInitialiseFactory
    Inherits BaseFactory(Of ISoqStockItemInitialise)

    Public Overrides Function Implementation() As ISoqStockItemInitialise

        Return New StockItemDemandPatternNew

    End Function

End Class
