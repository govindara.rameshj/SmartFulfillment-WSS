﻿Public Class SoqConstRepositoryFactory

    Private Shared m_FactoryMember As ISoqConstants

    Public Shared Function FactoryGet() As ISoqRepository

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New SoqConstantRepository         'live implementation
        Else
            Return m_FactoryMember                     'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ISoqRepository)

        m_FactoryMember = obj

    End Sub


End Class
