﻿Imports Oasys.Core

<CLSCompliant(True)> Public Class ReceiptLine
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _receiptNumber As String
    Private _sequence As String
    Private _skuNumber As String
    Private _skuDescription As String
    Private _skuProductCode As String
    Private _skuPack As Integer
    Private _orderLineId As Integer
    Private _orderPrice As Decimal
    Private _orderQty As Integer
    Private _receivedQty As Integer
    Private _receivedPrice As Decimal
    Private _returnQty As Integer
    Private _reasonCode As String
    Private _receivedOriginal As Integer
#End Region

#Region "Properties"
    <ColumnMapping("ReceiptNumber")> Public Property ReceiptNumber() As String
        Get
            Return _receiptNumber
        End Get
        Friend Set(ByVal value As String)
            _receiptNumber = value
        End Set
    End Property
    <ColumnMapping("Sequence")> Public Property Sequence() As String
        Get
            Return _sequence
        End Get
        Friend Set(ByVal value As String)
            _sequence = value
        End Set
    End Property
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
    <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
        Get
            Return _skuDescription
        End Get
        Private Set(ByVal value As String)
            _skuDescription = value
        End Set
    End Property
    <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
        Get
            Return _skuProductCode
        End Get
        Private Set(ByVal value As String)
            _skuProductCode = value
        End Set
    End Property
    <ColumnMapping("SkuPack")> Public Property SkuPack() As Integer
        Get
            Return _skuPack
        End Get
        Private Set(ByVal value As Integer)
            _skuPack = value
        End Set
    End Property
    <ColumnMapping("OrderLineId")> Public Property OrderLineId() As Integer
        Get
            Return _orderLineId
        End Get
        Private Set(ByVal value As Integer)
            _orderLineId = value
        End Set
    End Property
    <ColumnMapping("OrderPrice")> Public Property OrderPrice() As Decimal
        Get
            Return _orderPrice
        End Get
        Private Set(ByVal value As Decimal)
            _orderPrice = value
        End Set
    End Property
    <ColumnMapping("OrderQty")> Public Property OrderQty() As Integer
        Get
            Return _orderQty
        End Get
        Private Set(ByVal value As Integer)
            _orderQty = value
        End Set
    End Property
    <ColumnMapping("ReceivedQty")> Public Property ReceivedQty() As Integer
        Get
            Return _receivedQty
        End Get
        Set(ByVal value As Integer)
            _receivedQty = value
        End Set
    End Property
    <ColumnMapping("ReceivedPrice")> Public Property ReceivedPrice() As Decimal
        Get
            Return _receivedPrice
        End Get
        Set(ByVal value As Decimal)
            _receivedPrice = value
        End Set
    End Property
    <ColumnMapping("ReturnQty")> Public Property ReturnQty() As Integer
        Get
            Return _returnQty
        End Get
        Private Set(ByVal value As Integer)
            _returnQty = value
        End Set
    End Property
    <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
        Get
            Return _reasonCode
        End Get
        Set(ByVal value As String)
            _reasonCode = value
        End Set
    End Property
    Public ReadOnly Property ReceivedOriginal() As Integer
        Get
            Return _receivedOriginal
        End Get
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
        _receivedOriginal = _receivedQty
    End Sub

    Friend Sub New(ByVal orderLine As OrderLine)
        MyBase.New()
        _skuNumber = orderLine.SkuNumber
        _skuDescription = orderLine.SkuDescription
        _skuProductCode = orderLine.SkuProductCode
        _skuPack = orderLine.SkuPackSize
        _orderLineId = orderLine.Id
        _orderPrice = orderLine.Price
        _orderQty = orderLine.OrderQty
        _receivedQty = orderLine.OrderQty
    End Sub
#End Region


    Public Sub SetSkuValues(ByVal description As String, ByVal productCode As String, ByVal pack As Integer, ByVal orderPrice As Decimal)
        _skuDescription = description.Trim
        _skuProductCode = productCode.Trim
        _skuPack = pack
        _orderPrice = orderPrice
    End Sub

End Class