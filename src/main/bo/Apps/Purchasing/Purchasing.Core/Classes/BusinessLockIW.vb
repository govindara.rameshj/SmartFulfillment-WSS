﻿Public Class BusinessLockIW

    Protected Overridable Function ExecuteCreateLock(ByVal WorkStationID As Integer, _
                                         ByVal CurrentDateAndTime As Date, _
                                         ByVal AssemblyName As String, _
                                         ByVal ParameterValues As String) As Integer

        Dim BL As IBusinessLock

        BL = New BusinessLock

        Return BL.CreateLock(WorkStationID, CurrentDateAndTime, AssemblyName, ParameterValues)

    End Function

    Protected Overridable Sub ExecuteRemoveLock(ByVal RecordID As Integer)

        Dim BL As IBusinessLock

        BL = New BusinessLock

        BL.RemoveLock(RecordID)

    End Sub

End Class