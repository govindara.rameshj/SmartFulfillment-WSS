﻿Public Class DisabledBusinessLockIW
    Inherits BusinessLockIW
    Implements IBusinessLockIW

    Public Function CreateLock(ByVal WorkStationID As Integer, _
                               ByVal CurrentDateAndTime As Date, _
                               ByVal AssemblyName As String, _
                               ByVal ParameterValues As String) As Integer Implements IBusinessLockIW.CreateLock

        'do nothing

    End Function

    Public Sub RemoveLock(ByVal RecordID As Integer) Implements IBusinessLockIW.RemoveLock

        'do nothing

    End Sub

End Class