﻿Public Class StockItemDemandPatternNew
    Implements ISoqStockItemInitialise

    Private Const DemandPatternNew As String = "new"
    Private Const DemandPatternSuperceded As String = "superceded"
    Private Const DemandPatternObsolete As String = "obsolete"
    Private Const DemandPatternBlank As String = ""
    Private Const DemandPatternTrendDown As String = "trenddown"

    Private Shared Function SoqCheckStockItemDemandPatternNewOrSupercededOrObsolete(ByVal stockItem As Stock) As Boolean

        With stockItem
            If String.Compare(.DemandPattern.ToLower, DemandPatternNew) = 0 _
                            Or String.Compare(.DemandPattern.ToLower, DemandPatternSuperceded) = 0 _
                            Or String.Compare(.DemandPattern.ToLower, DemandPatternBlank) = 0 _
                            Or String.Compare(.DemandPattern.ToLower, DemandPatternObsolete) = 0 _
                            Or (String.Compare(.DemandPattern.ToLower, DemandPatternTrendDown) = 0 _
                                And .PeriodDemand = 0) Then
                Return True
            End If
        End With
        Return False
    End Function
    Public Function SoqGetStockItemDemandPattern(ByVal stockItem As Cts.Oasys.Core.Stock.Stock) As Boolean Implements ISoqStockItemInitialise.SoqGetStockItemDemandPattern
        Return SoqCheckStockItemDemandPatternNewOrSupercededOrObsolete(stockItem)
    End Function

    Public Function SoqUpdateOrderLevel() As Boolean Implements ISoqStockItemInitialise.SoqUpdateOrderLevel
        Return True
    End Function
End Class
