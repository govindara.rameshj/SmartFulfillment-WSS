﻿Public Class EnabledBusinessLockIW
    Inherits BusinessLockIW
    Implements IBusinessLockIW

    Public Function CreateLock(ByVal WorkStationID As Integer, _
                               ByVal CurrentDateAndTime As Date, _
                               ByVal AssemblyName As String, _
                               ByVal ParameterValues As String) As Integer Implements IBusinessLockIW.CreateLock

        'Dim BL As IBusinessLock
        'BL = New BusinessLock
        'Return BL.CreateLock(WorkStationID, CurrentDateAndTime, AssemblyName, ParameterValues)

        Return Me.ExecuteCreateLock(WorkStationID, CurrentDateAndTime, AssemblyName, ParameterValues)

    End Function

    Public Sub RemoveLock(ByVal RecordID As Integer) Implements IBusinessLockIW.RemoveLock

        'Dim BL As IBusinessLock
        'BL = New BusinessLock
        'BL.RemoveLock(RecordID)

        Me.ExecuteRemoveLock(RecordID)

    End Sub

End Class