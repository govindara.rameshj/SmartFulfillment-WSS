﻿Public Class SoqConstantsNew
    Implements ISoqConstants

    Public ReadOnly Property ZeroDemandLumpyCheck() As Dictionary(Of Integer, Integer) Implements ISoqConstants.ZeroDemandLumpyCheck  'S36

        Get
            ZeroDemandLumpyCheck = New Dictionary(Of Integer, Integer)
            ' Referral 856
            ' Add these 0 values for keys 1-3, which mimics the UpTrac behaviour in this situation
            ZeroDemandLumpyCheck.Add(1, 0)
            ZeroDemandLumpyCheck.Add(2, 0)
            ZeroDemandLumpyCheck.Add(3, 0)
            ' End of Referral 856
            ZeroDemandLumpyCheck.Add(4, 3)
            ZeroDemandLumpyCheck.Add(5, 3)
            ZeroDemandLumpyCheck.Add(6, 4)
            ZeroDemandLumpyCheck.Add(7, 4)
            ZeroDemandLumpyCheck.Add(8, 5)
            ZeroDemandLumpyCheck.Add(9, 5)
            ZeroDemandLumpyCheck.Add(10, 6)
            ZeroDemandLumpyCheck.Add(11, 6)
            ZeroDemandLumpyCheck.Add(12, 7)
            ZeroDemandLumpyCheck.Add(13, 7)
        End Get
    End Property

    Public ReadOnly Property SlowLumpyCheck() As Dictionary(Of Integer, Integer) Implements ISoqConstants.SlowLumpyCheck    'S37

        Get
            SlowLumpyCheck = New Dictionary(Of Integer, Integer)
            ' Referral 856
            ' Add these 0 values for keys 1-3, which mimics the UpTrac behavoiur in this situation
            SlowLumpyCheck.Add(0, 0)
            SlowLumpyCheck.Add(1, 0)
            SlowLumpyCheck.Add(2, 0)
            SlowLumpyCheck.Add(3, 0)
            ' End of Referral 856
            SlowLumpyCheck.Add(4, 1)
            SlowLumpyCheck.Add(5, 1)
            SlowLumpyCheck.Add(6, 2)
            SlowLumpyCheck.Add(7, 2)
            SlowLumpyCheck.Add(8, 3)
            SlowLumpyCheck.Add(9, 3)
            SlowLumpyCheck.Add(10, 4)
            SlowLumpyCheck.Add(11, 4)
            SlowLumpyCheck.Add(12, 5)
            SlowLumpyCheck.Add(13, 5)
        End Get
    End Property
End Class
