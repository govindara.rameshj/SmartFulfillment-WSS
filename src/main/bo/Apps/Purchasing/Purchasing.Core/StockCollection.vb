﻿Imports System.ComponentModel

<EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> <CLSCompliant(True)> Public Class StockCollection
    Inherits BindingList(Of Stock.Core.Stock)
    Private _supplier As Supplier

    Friend Sub New(ByRef sup As Supplier)
        MyBase.New()
        _supplier = sup
        LoadStocks()
    End Sub


    Private Sub LoadStocks()
        For Each s As Stock.Core.Stock In Stock.Core.Stock.GetStocksForSupplier(_supplier.Number)
            Me.Items.Add(s)
        Next
    End Sub


    ''' <summary>
    ''' Calculates order value, units and weight currently on order
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="units"></param>
    ''' <param name="weight"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOrder(ByRef value As Decimal, ByRef units As Integer, ByRef weight As Decimal)

        'reset values
        value = 0
        units = 0
        weight = 0

        'get initial values
        For Each stockItem As Stock.Core.Stock In Me.Items
            value += (stockItem.OrderQty * stockItem.Price)
            units += (stockItem.OrderQty)
            weight += (stockItem.OrderQty * stockItem.Weight)
        Next

    End Sub

    ''' <summary>
    ''' Calculates order value, units, weight and cartons currently on order
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="units"></param>
    ''' <param name="weight"></param>
    ''' <param name="cartons"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOrder(ByRef value As Decimal, ByRef units As Integer, ByRef weight As Decimal, ByRef cartons As Integer)

        'reset values
        value = 0
        units = 0
        weight = 0
        cartons = 0

        'get initial values
        For Each stockItem As Stock.Core.Stock In Me.Items
            value += (stockItem.OrderQty * stockItem.Price)
            units += (stockItem.OrderQty)
            weight += (stockItem.OrderQty * stockItem.Weight)
            cartons += (stockItem.OrderQty * stockItem.PackSize)
        Next

    End Sub

    ''' <summary>
    ''' Calculates units and value of all stock items where stock on hand is greater than capacity
    ''' </summary>
    ''' <param name="units"></param>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOverShelfCapacity(ByRef units As Integer, ByRef value As Decimal)

        'reset values
        units = 0
        value = 0

        For Each stockItem As Stock.Core.Stock In Me.Items
            Dim over As Integer = stockItem.OnHandQty - stockItem.Capacity
            If over > 0 Then
                units += over
                value += (over * stockItem.Price)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Calculates count and value of all stock items where stock on hand is greater than capacity
    '''  taking on order qty into account
    ''' </summary>
    ''' <param name="count"></param>
    ''' <param name="value"></param>
    ''' <param name="soqUnits"></param>
    ''' <param name="soqValue"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOverShelfCapacity(ByRef count As Integer, ByRef value As Decimal, ByRef soqUnits As Integer, ByRef soqValue As Decimal)

        For Each stockItem As Stock.Core.Stock In Me.Items
            Dim over As Integer = stockItem.OnOrderQty + stockItem.OnHandQty - stockItem.Capacity
            If over > 0 Then
                count += 1
                value += (over * stockItem.Price)
            End If

            If stockItem.OrderLevel > stockItem.Capacity Then
                soqUnits += (stockItem.OrderLevel - stockItem.Capacity)
                soqValue += ((stockItem.OrderLevel - stockItem.Capacity) * stockItem.Price)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Sets order qty to soq qty for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub OrderQtySetSoq()

        For Each stockItem As Stock.Core.Stock In Me.Items
            stockItem.OrderQty = stockItem.OrderLevel
        Next

    End Sub

    ''' <summary>
    ''' Sets order qty to zero for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub OrderQtySetZero()

        For Each stockItem As Stock.Core.Stock In Me.Items
            stockItem.OrderQty = 0
        Next

    End Sub

    ''' <summary>
    ''' Returns whether all order qtys are zero
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AllOrderQtysZero() As Boolean

        For Each stockItem As Stock.Core.Stock In Me.Items
            If stockItem.OrderQty <> 0 Then
                Return False
            End If
        Next
        Return True

    End Function

End Class