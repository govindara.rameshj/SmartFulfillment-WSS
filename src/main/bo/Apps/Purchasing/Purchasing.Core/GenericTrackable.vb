﻿Imports System.Reflection

Public Class GenericTrackable

    Public Event DataChanged As EventHandler(Of DataChangedEventArgs)

    Protected Sub OnDataChanged(ByVal hasChanges As Boolean)
        RaiseEvent DataChanged(Me, New DataChangedEventArgs(hasChanges))
    End Sub

End Class
