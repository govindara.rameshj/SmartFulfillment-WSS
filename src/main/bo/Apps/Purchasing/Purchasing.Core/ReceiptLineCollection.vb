﻿Imports System.ComponentModel
Imports System.Reflection

<CLSCompliant(True)> Public Class ReceiptLineCollection
    Inherits BindingList(Of ReceiptLine)
    Private _receipt As Receipt

    Friend Sub New(ByRef receipt As Receipt)
        MyBase.New()
        _receipt = receipt
        LoadLines()
    End Sub

    Friend Sub New(ByRef receipt As Receipt, ByVal order As Order)
        MyBase.New()
        _receipt = receipt
        LoadLines(order)
    End Sub

    Friend Sub New(ByRef receipt As Receipt, ByVal receiptTrackable As ReceiptTrackable)
        MyBase.New()
        _receipt = receipt
        UpdateLines(receiptTrackable)
    End Sub


    Private Sub LoadLines()

        Dim dt As DataTable = DataOperations.ReceiptGetLines(_receipt.Number)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New ReceiptLine(dr))
        Next

    End Sub

    Private Sub LoadLines(ByVal order As Order)

        For Each line As OrderLine In order.Lines
            Me.Items.Add(New ReceiptLine(line))
        Next

    End Sub

    Private Sub UpdateLines(receiptTrackable As ReceiptTrackable)
        If receiptTrackable.Lines IsNot Nothing Then
            For Each line As ReceiptLineEditable In receiptTrackable.Lines
                If (line.SkuNumber.Trim <> String.Empty) Then
                    Me.Items.Add(CreateReceiptLine(line))
                End If
            Next
        End If
    End Sub

    Private Function CreateReceiptLine(ByVal source As ReceiptLineEditable) As ReceiptLine
        Dim receipt = New ReceiptLine()

        Dim sourceProperties = source.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)
        Dim targetPropeties = receipt.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)

        For Each sourceProperty As PropertyInfo In sourceProperties
            Dim sourcePropertyName = sourceProperty.Name
            Dim targetProperty = targetPropeties.FirstOrDefault(Function(prop) prop.Name = sourcePropertyName)
            If targetProperty IsNot Nothing AndAlso targetProperty.CanWrite Then
                Dim sourcePropertyValue = sourceProperty.GetValue(source, Nothing)
                If (sourceProperty.PropertyType Is GetType(String) AndAlso sourcePropertyValue IsNot Nothing) Then
                    sourcePropertyValue = sourcePropertyValue.ToString().Trim()
                End If

                targetProperty.SetValue(receipt, sourcePropertyValue, Nothing)
            End If
        Next

        Dim receivedOriginalProperty = sourceProperties.FirstOrDefault(Function(item) item.Name = "ReceivedOriginal")
        If receivedOriginalProperty IsNot Nothing Then
            Dim receivedOriginalField = receipt.GetType().GetField("_receivedOriginal", BindingFlags.NonPublic Or BindingFlags.Instance)
            receivedOriginalField.SetValue(receipt, receivedOriginalProperty.GetValue(source, Nothing))
        End If

        CreateReceiptLine = receipt
    End Function


    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As ReceiptLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Clears all received qtys for all lines in collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearReceiveQty()
        For Each line As ReceiptLine In Me.Items
            line.ReceivedQty = 0
        Next
    End Sub


    ''' <summary>
    ''' Calculates receipt order qtys and values
    ''' </summary>
    ''' <param name="orderqty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal)

        orderQty = 0
        orderValue = 0

        For Each line As ReceiptLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.OrderPrice)
        Next

    End Sub

    ''' <summary>
    ''' Calculates receipt order/received qtys and values
    ''' </summary>
    ''' <param name="orderQty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal, ByRef receivedQty As Integer, ByRef receivedValue As Decimal)

        orderQty = 0
        orderValue = 0
        receivedQty = 0
        receivedValue = 0

        For Each line As ReceiptLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.OrderPrice)
            receivedQty += line.ReceivedQty
            receivedValue += (line.ReceivedQty * line.ReceivedPrice)
        Next

    End Sub

    ''' <summary>
    ''' Returns next sequence number
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function NextSequenceNumber() As String

        Dim sequence As Integer = CInt(Me.Items.Max(Function(f As ReceiptLine) f.Sequence))
        Return (sequence + 1).ToString("0000")

    End Function

    Public Function OrderedQty() As Integer

        Dim qty As Integer = 0
        For Each line As ReceiptLine In Me.Items
            qty += line.OrderQty
        Next

        Return qty

    End Function

    Public Function OrderedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As ReceiptLine In Me.Items
            value += (line.OrderQty * line.OrderPrice)
        Next

        Return value

    End Function

    Public Function ReceivedQty() As Integer

        Dim qty As Integer = 0
        For Each line As ReceiptLine In Me.Items
            qty += line.ReceivedQty
        Next

        Return qty

    End Function

    Public Function ReceivedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As ReceiptLine In Me.Items
            value += (line.ReceivedQty * line.ReceivedPrice)
        Next

        Return value

    End Function

End Class