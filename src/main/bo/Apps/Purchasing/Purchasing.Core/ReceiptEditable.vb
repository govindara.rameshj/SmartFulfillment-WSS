﻿Imports BLToolkit.EditableObjects

Public MustInherit Class ReceiptEditable
    Inherits EditableObject(Of ReceiptEditable)

    Public MustOverride Property Number() As String
    Public MustOverride Property PoId() As Integer
    Public MustOverride Property PoNumber() As Nullable(Of Integer)
    Public MustOverride Property PoConsignNumber() As Nullable(Of Integer)
    Public MustOverride Property PoSupplierNumber() As String
    Public MustOverride Property PoSupplierName() As String
    Public MustOverride Property PoSupplierBbc() As Boolean
    Public MustOverride Property PoSoqNumber() As Nullable(Of Integer)
    Public MustOverride Property PoReleaseNumber() As Integer
    Public MustOverride Property PoOrderDate() As Date
    Public MustOverride Property DateReceipt() As Date
    Public MustOverride Property EmployeeId() As Integer
    Public MustOverride Property Value() As Decimal
    Public MustOverride Property Comments() As String
    Public MustOverride Property PoDeliveryNote1() As String
    Public MustOverride Property PoDeliveryNote2() As String
    Public MustOverride Property PoDeliveryNote3() As String
    Public MustOverride Property PoDeliveryNote4() As String
    Public MustOverride Property PoDeliveryNote5() As String
    Public MustOverride Property PoDeliveryNote6() As String
    Public MustOverride Property PoDeliveryNote7() As String
    Public MustOverride Property PoDeliveryNote8() As String
    Public MustOverride Property PoDeliveryNote9() As String

End Class
