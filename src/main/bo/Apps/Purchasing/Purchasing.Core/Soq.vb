﻿<CLSCompliant(True)> Public Class Soq
    Public Event SoqStarted(ByVal sup As Supplier)
    Public Event SoqProgress(ByVal sup As Supplier, ByVal progress As Integer)
    Public Event SoqCompleted(ByVal sup As Supplier, ByVal status As CompletedStatus)

    Private _isInGUIMode As Boolean

    Public Sub PerformInitialisation(ByRef sup As Supplier)

        Dim daysOpen As Integer = Store.GetDaysOpen
        Dim soqStockItemInitialiseFactoryInstance As ISoqStockItemInitialise

        'enumerate over suppliers loaded and perform soq
        TraceDebug(sup.ToString, "Performing SOQ")
        Dim stockcount As Integer = sup.Stocks.Count
        Dim doWork As Boolean = True

        RaiseEvent SoqStarted(sup)

        'check if deleted
        If sup.IsDeleted Then
            RaiseEvent SoqCompleted(sup, CompletedStatus.Deleted)
            doWork = False
        End If

        'check has any stock items
        If stockcount = 0 Then
            RaiseEvent SoqCompleted(sup, CompletedStatus.NoStocks)
            doWork = False
        End If

        soqStockItemInitialiseFactoryInstance = (New SoqStockItemInitialiseFactory).GetImplementation

        'perform initialisation for each stock item if can do work
        If doWork Then
            For Each stockItem As Stock In sup.Stocks
                RaiseEvent SoqProgress(sup, CInt(sup.Stocks.IndexOf(stockItem) / stockcount * 100))

                'check if need to do forecasting
                If stockItem.ToForecast Then
                    TraceDebug(stockItem.ToString, "Item set to forecast")

                    'check if item is out of stock and whether current period sales are less than period demand
                    If stockItem.DaysOutStock1 > 0 AndAlso stockItem.UnitSales1 < stockItem.PeriodDemand Then
                        TraceDebug(stockItem.ToString, "Out of stock and current sales < period demand")
                        stockItem.IsInitialised = False
                        Continue For
                    End If

                    'get initialisation periods
                    Dim soqPeriods As SoqPeriodCollection = GetInitialisationPeriods(stockItem)

                    If Not DemandPatternReclassified(stockItem, soqPeriods) AndAlso stockItem.IsInitialised Then
                        If Not ExceptionalFliersFound(stockItem) Then
                            ForecastParameters(stockItem)
                        End If
                    End If
                    If soqStockItemInitialiseFactoryInstance.SoqGetStockItemDemandPattern(stockItem) Then
                        stockItem.IsInitialised = False
                    End If
                End If

                'perform initialisation routine
                If stockItem.IsInitialised = False Then
                    stockItem.DemandPattern = String.Empty
                    stockItem.PeriodDemand = 0
                    stockItem.PeriodTrend = 0
                    stockItem.ErrorForecast = 0
                    stockItem.ErrorSmoothed = 0
                    stockItem.ValueAnnualUsage = String.Empty
                    stockItem.BufferConversion = 0
                    stockItem.BufferStock = 0
                    stockItem.OrderLevel = 0

                    'get initialisation periods
                    Dim soqPeriods As New SoqPeriodCollection
                    Do
                        soqPeriods = GetInitialisationPeriods(stockItem)
                    Loop While Not DemandPatternInitialised(stockItem, soqPeriods)

                    'initialise value annual usage
                    Dim value As Decimal = stockItem.PeriodDemand * 52 * stockItem.Price

                    stockItem.ValueAnnualUsage = SoqConstants.AnnualUsage(CInt(value))



                    'initialise buffer conversion
                    Select Case stockItem.DemandPattern
                        Case DemandPatterns.[New], _
                        DemandPatterns.Fast, DemandPatterns.Erratic, _
                        DemandPatterns.TrendDown, DemandPatterns.TrendUp

                            InitialiseBufferConversion(stockItem, sup.DaysLeadFixed(daysOpen), sup.DaysReviewFixed)
                    End Select
                End If
            Next

            If soqStockItemInitialiseFactoryInstance.SoqUpdateOrderLevel Then
                SoqUpdateOrderLine(sup)
            End If
            'update supplier stock items
            SupplierUpdateSoqInit(sup)
        End If

        RaiseEvent SoqCompleted(sup, CompletedStatus.Successful)

    End Sub


    Private Function GetInitialisationPeriods(ByRef stockItem As Stock) As SoqPeriodCollection

        Dim soqPeriods As New SoqPeriodCollection
        TraceDebug(stockItem.ToString, "Initialising periods")

        'get number of weeks in stock
        'if there is a date first stocked then set values
        Dim days As Integer = 0
        Dim weeks As Integer = 0
        If stockItem.DateFirstStocked.HasValue Then
            days = CInt(DateDiff(DateInterval.Day, stockItem.DateFirstStocked.Value, Now.Date))
            weeks = CInt(Math.Abs(Math.Floor(days / 7)))
        End If

        Select Case True
            Case weeks <= SoqConstants.InitNewPeriods
                TraceDebug(stockItem.ToString, "In stock less than InitNewPeriods - cannot initialise")
                stockItem.DemandPattern = DemandPatterns.[New]

            Case weeks < SoqConstants.InitNewLimit
                'use last soq.InitNewPeriods periods
                TraceDebug(stockItem.ToString, "In stock less than new stock limit")
                stockItem.DemandPattern = DemandPatterns.[New]
                For index As Integer = 2 To SoqConstants.InitNewPeriods + 1
                    soqPeriods.Add(New SoqPeriod(index, CInt(stockItem.UnitSales(index)), stockItem.FlierPeriod(index - 1), stockItem.DaysOutStock(index)))
                Next

            Case (SoqConstants.InitNewLimit <= weeks) AndAlso (weeks < SoqConstants.InitNewLimit + SoqConstants.InitPostLimit)
                'add all but first NumPeriodsLimitPostNew periods but do not include fliers
                TraceDebug(stockItem.ToString, "In stock between lower and upper limits")
                For index As Integer = 2 To (weeks - SoqConstants.InitPostLimit + 1)
                    If stockItem.FlierPeriod(index - 1).Trim.Length = 0 Then
                        soqPeriods.Add(New SoqPeriod(index, CInt(stockItem.UnitSales(index)), stockItem.FlierPeriod(index - 1), stockItem.DaysOutStock(index)))
                    End If
                Next

            Case Else
                'add all periods but do not include fliers
                TraceDebug(stockItem.ToString, "In stock above upper limit")
                For index As Integer = 2 To 13
                    If stockItem.FlierPeriod(index - 1).Trim.Length = 0 Then
                        soqPeriods.Add(New SoqPeriod(index, CInt(stockItem.UnitSales(index)), stockItem.FlierPeriod(index - 1), stockItem.DaysOutStock(index)))
                    End If
                Next
        End Select

        'calculate average period demand for selected periods where there are no days out of stock
        'but only for not new periods
        If stockItem.DemandPattern <> DemandPatterns.[New] Then
            Dim sales As New List(Of Decimal)
            For Each period As SoqPeriod In soqPeriods.Where(Function(p) p.OutOfStock = False)
                sales.Add(period.Sales)
            Next

            'remove any out of stock periods that are less then the average sales calculated above
            If sales.Count > 0 Then
                Dim average As Decimal = sales.Average
                soqPeriods.RemoveOutOfStockLessThanAverage(average)
            End If
        End If

        'do trace output
        For Each period As SoqPeriod In soqPeriods
            TraceDebug(stockItem.ToString, "Period Used: " & period.Index)
        Next

        Return soqPeriods

    End Function

    Private Function DemandPatternReclassified(ByRef stockItem As Stock, ByRef soqPeriods As SoqPeriodCollection) As Boolean

        'perform demand pattern classification checks
        Select Case True
            Case soqPeriods.CountZeroSales = soqPeriods.Count
                TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to obsolete")
                stockItem.DemandPattern = DemandPatterns.Obsolete
                stockItem.IsInitialised = False

            Case soqPeriods.CountZeroSalesConsecutive = SoqConstants.NumPeriodsConsecutiveZero
                TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to superceded")
                stockItem.DemandPattern = DemandPatterns.Superceded
                stockItem.IsInitialised = False

            Case Else
                Select Case stockItem.DemandPattern
                    Case DemandPatterns.Normal, DemandPatterns.Erratic, DemandPatterns.Fast, DemandPatterns.TrendDown, DemandPatterns.TrendUp

                        ' Referral 856
                        If soqPeriods.CountZeroSales > (New SoqConstantsFactory).GetImplementation.ZeroDemandLumpyCheck(soqPeriods.Count) Then

                            stockItem.IsInitialised = False

                            If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                                TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to slow")
                                stockItem.DemandPattern = DemandPatterns.Slow
                            Else
                                TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to lumpy")
                                stockItem.DemandPattern = DemandPatterns.Lumpy
                            End If
                        End If

                    Case DemandPatterns.Slow, DemandPatterns.Lumpy
                        stockItem.IsInitialised = False

                        Select Case soqPeriods.CountZeroSales
                            ' Referral 856
                            Case Is <= (New SoqConstantsFactory).GetImplementation.SlowLumpyCheck(soqPeriods.Count)

                                TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to normal")
                                stockItem.DemandPattern = DemandPatterns.Normal

                            Case Else
                                Select Case stockItem.DemandPattern
                                    Case DemandPatterns.Slow
                                        If soqPeriods.AverageSales >= SoqConstants.SlowAveDemandCheck Then
                                            TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to lumpy")
                                            stockItem.DemandPattern = DemandPatterns.Lumpy
                                        End If

                                    Case DemandPatterns.Lumpy
                                        If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                                            TraceDebug(stockItem.ToString, "Reclassified " & stockItem.DemandPattern & " to slow")
                                            stockItem.DemandPattern = DemandPatterns.Slow
                                        End If
                                End Select
                        End Select
                End Select
        End Select

        Return (stockItem.IsInitialised = False)

    End Function

    Private Function ExceptionalFliersFound(ByRef stockItem As Stock) As Boolean

        TraceDebug(stockItem.ToString, "Performing exceptional flier check")

        'get adjuster for this stock item and week
        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 7))
        Dim weights As Store.SaleWeightCollection = Store.SaleWeight.GetActiveWeights(stockItem.SaleWeightBank)
        weights.AddActiveWeights(stockItem.SaleWeightPromo)
        weights.AddActiveWeights(stockItem.SaleWeightSeason)
        Dim adjuster As Decimal = weights.Adjuster(week)

        Dim F1 As Double = (stockItem.PeriodDemand + stockItem.PeriodTrend) * adjuster
        Dim periodCheck As Double = stockItem.UnitSales1 - F1
        Dim absLimit As Double = Math.Abs(SoqConstants.InputDemandSupressor * stockItem.ErrorForecast)

        TraceDebug(stockItem.ToString, "Current week = " & week)
        TraceDebug(stockItem.ToString, "Week adjuster = " & adjuster)
        TraceDebug(stockItem.ToString, "Period(1) (demand+trend)*adjuster = " & F1)
        TraceDebug(stockItem.ToString, "Period(1) absolute period check = " & periodCheck)
        TraceDebug(stockItem.ToString, "Absolute flier limit = " & absLimit)


        If periodCheck < absLimit Then
            TraceDebug(stockItem.ToString, "Flier found for period 1")
            stockItem.FlierPeriod1 = "F"
            stockItem.IsInitialised = False

            ' Check to see if second consecutive flier
            If stockItem.FlierPeriod2 = "F" Then
                TraceDebug(stockItem.ToString, "Flier found for period 2")
                Dim average1 As Double = stockItem.UnitSales1 / weights.Adjuster(week + 1)
                Dim average2 As Double = stockItem.UnitSales2 / weights.Adjuster(week + 2)
                stockItem.PeriodDemand = CDec((average1 + average2) / 2)
                stockItem.PeriodTrend = 0
                stockItem.ErrorSmoothed = 0

                Select Case stockItem.PeriodDemand
                    Case Is <= 3 : stockItem.ErrorForecast = SoqConstants.StdDevMultiplier(stockItem.PeriodDemand) * stockItem.PeriodDemand
                    Case Is <= 10 : stockItem.ErrorForecast = SoqConstants.StdDevMultiplier(stockItem.PeriodDemand) * stockItem.PeriodDemand
                    Case Else : stockItem.ErrorForecast = SoqConstants.StdDevMultiplier(stockItem.PeriodDemand) * stockItem.PeriodDemand

                End Select
            End If

            TraceDebug(stockItem.ToString, "Stock is not to be forecasted")
            Return True
        End If

        ' Check whether item has 2 or more non-consecutive fliers in last 12 periods and if so then reset all flier indicators and reclassify item
        Dim numberNonConsecutiveFliers As Integer = 0
        For period As Integer = 3 To 12
            If stockItem.FlierPeriod(period) = "F" And stockItem.FlierPeriod(period - 1) <> "F" Then numberNonConsecutiveFliers += 1
        Next

        If numberNonConsecutiveFliers >= 2 Then
            TraceDebug(stockItem.ToString, numberNonConsecutiveFliers & " non consecutive fliers found")
            TraceDebug(stockItem.ToString, "Stock is to be reclassified")
            stockItem.IsInitialised = False
            For period As Integer = 1 To 12
                stockItem.FlierPeriod(period) = ""
            Next
            Return True
        End If

        Return False

    End Function

    Private Sub ForecastParameters(ByRef stockItem As Stock)

        TraceDebug(stockItem.ToString, "Performing forecasting")

        'check if flier for this period
        If stockItem.FlierPeriod1 = "F" Then
            TraceDebug(stockItem.ToString, "Flier found thus forecasting stopped")
            Exit Sub
        End If

        'get adjuster for this stock item and week
        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 7))
        Dim weights As Store.SaleWeightCollection = Store.SaleWeight.GetActiveWeights(stockItem.SaleWeightBank)
        weights.AddActiveWeights(stockItem.SaleWeightPromo)
        weights.AddActiveWeights(stockItem.SaleWeightSeason)
        Dim weekAdjuster As Decimal = weights.Adjuster(week)

        Dim lastDemand As Double = stockItem.PeriodDemand - (1 - SoqConstants.SmoothingConstant) / SoqConstants.SmoothingConstant * stockItem.PeriodTrend     'Ei-1
        Dim adjustedDemand As Double = Math.Max(CType(stockItem.UnitSales1, Double) / weekAdjuster, 0)                                                'Ri
        Dim estimateDemand As Double = lastDemand + SoqConstants.SmoothingConstant * (adjustedDemand - lastDemand)                'Ei
        Dim actualTrend As Double = SoqConstants.SmoothingConstant * (adjustedDemand - lastDemand)                                 'Gi
        Dim estimateError As Double = stockItem.UnitSales1 / weekAdjuster - Math.Abs(stockItem.PeriodDemand + stockItem.PeriodTrend)

        ' Calculate new parameters for item PeriodDemand/trend and smoothed error
        Dim newTrend As Double = stockItem.PeriodTrend + SoqConstants.SmoothingConstant * (actualTrend - stockItem.PeriodTrend)
        Dim newDemand As Double = estimateDemand + (1 - SoqConstants.SmoothingConstant) / SoqConstants.SmoothingConstant * newTrend
        Dim newErrorSmoothed As Double = stockItem.ErrorSmoothed + SoqConstants.SmoothingConstant * (estimateError - stockItem.ErrorSmoothed)

        ' Calculate error forecast
        Dim errMAD As Double = stockItem.ErrorForecast * 0.8 + SoqConstants.SmoothingConstant * (Math.Abs(estimateError) - stockItem.ErrorForecast * 0.8)
        Dim newErrorForecast As Double = errMAD / 0.8
        Dim TrackingSignal As Double = newErrorSmoothed / errMAD


        'output calculated values
        TraceDebug(stockItem.ToString, "lastDemand       = " & lastDemand)
        TraceDebug(stockItem.ToString, "adjustedDemand   = " & adjustedDemand)
        TraceDebug(stockItem.ToString, "estimateDemand   = " & estimateDemand)
        TraceDebug(stockItem.ToString, "actualTrend      = " & actualTrend)
        TraceDebug(stockItem.ToString, "estimateError    = " & estimateError)
        TraceDebug(stockItem.ToString, "newTrend         = " & newTrend)
        TraceDebug(stockItem.ToString, "newDemand        = " & newDemand)
        TraceDebug(stockItem.ToString, "newErrorSmoothed = " & newErrorSmoothed)
        TraceDebug(stockItem.ToString, "errorMad         = " & errMAD)
        TraceDebug(stockItem.ToString, "newErrorForecast = " & newErrorForecast)
        TraceDebug(stockItem.ToString, "signal           = " & TrackingSignal)

        ' Check tracking signal
        If Math.Abs(TrackingSignal) > Math.Abs(SoqConstants.TrackingSignalLimit) Then
            stockItem.IsInitialised = False
            TraceDebug(stockItem.ToString, "Reclassify item")
        Else
            Select Case stockItem.DemandPattern
                Case DemandPatterns.Fast, DemandPatterns.TrendDown, DemandPatterns.TrendUp
                    If newErrorForecast / newDemand > SoqConstants.StdDevMax Then
                        stockItem.IsInitialised = False
                        TraceDebug(stockItem.ToString, "Reclassify item")
                    Else
                        TraceDebug(stockItem.ToString, "Setting values")
                        stockItem.PeriodDemand = CDec(newDemand)
                        stockItem.PeriodTrend = CDec(newTrend)
                        stockItem.ErrorForecast = CDec(newErrorForecast)
                        stockItem.ErrorSmoothed = CDec(newErrorSmoothed)
                    End If

                Case DemandPatterns.Erratic
                    If newErrorForecast / newDemand < (SoqConstants.StdDevMax - 0.2) Then
                        stockItem.IsInitialised = False
                        TraceDebug(stockItem.ToString, "Reclassify item")
                    Else
                        TraceDebug(stockItem.ToString, "Setting values")
                        stockItem.PeriodDemand = CDec(newDemand)
                        stockItem.PeriodTrend = CDec(newTrend)
                        stockItem.ErrorForecast = CDec(newErrorForecast)
                        stockItem.ErrorSmoothed = CDec(newErrorSmoothed)
                    End If
            End Select

        End If

    End Sub



    Private Function DemandPatternInitialised(ByRef stockItem As Stock, ByRef soqPeriods As SoqPeriodCollection) As Boolean

        TraceDebug(stockItem.ToString, "Initialising demand pattern")

        'if already classed as new then set values
        If stockItem.DemandPattern = DemandPatterns.[New] Then
            TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            stockItem.PeriodDemand = soqPeriods.AverageSales
            stockItem.PeriodTrend = 0
            stockItem.ErrorForecast = stockItem.PeriodDemand * SoqConstants.StdDevMultiplier(stockItem.PeriodDemand)
            stockItem.ErrorSmoothed = 0
            Return True
        End If

        'check if number periods is zero and use all periods if true
        If soqPeriods.Count = 0 Then
            'get rbar as avreage of all periods
            Dim rbar As Decimal = 0
            For index As Integer = 2 To 13
                rbar += CDec(stockItem.UnitSales(index))
            Next
            rbar /= 12

            stockItem.DemandPattern = DemandPatterns.Fast
            stockItem.PeriodDemand = rbar
            TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            Return DemandPatternInitialiseFastErratic(stockItem, soqPeriods)
        End If

        'get adjuster for this stock item and week
        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 7))
        Dim weights As Store.SaleWeightCollection = Store.SaleWeight.GetActiveWeights(stockItem.SaleWeightBank)
        weights.AddActiveWeights(stockItem.SaleWeightPromo)
        weights.AddActiveWeights(stockItem.SaleWeightSeason)

        'get period adjusters for periods
        For Each period As SoqPeriod In soqPeriods
            period.Adjuster = weights.Adjuster(period.WeekNumber)
        Next


        'check that not all periods have zero sales
        If soqPeriods.CountZeroSales = soqPeriods.Count Then
            stockItem.DemandPattern = DemandPatterns.Obsolete
            TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            stockItem.PeriodDemand = 0
            stockItem.PeriodTrend = 0
            stockItem.ErrorForecast = 0
            stockItem.ErrorSmoothed = 0
            Return True
        End If

        'check for fast stock items
        If soqPeriods.Count < 4 Then
            stockItem.DemandPattern = DemandPatterns.Fast
            stockItem.PeriodDemand = soqPeriods.AverageSales
            TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            Return DemandPatternInitialiseFastErratic(stockItem, soqPeriods)
        End If

        'check for superceded stock items
        If soqPeriods.CountZeroSalesConsecutive >= SoqConstants.NumPeriodsConsecutiveZero Then
            stockItem.DemandPattern = DemandPatterns.Superceded
            TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            stockItem.PeriodDemand = 0
            stockItem.PeriodTrend = 0
            stockItem.ErrorForecast = 0
            stockItem.ErrorSmoothed = 0
            Return True
        End If

        'check for lumpy/slow items

        ' Referral 856
        If soqPeriods.CountZeroSales >= (New SoqConstantsFactory).GetImplementation.ZeroDemandLumpyCheck(soqPeriods.Count) Then

            If soqPeriods.AverageSales < SoqConstants.SlowAveDemandCheck Then
                stockItem.DemandPattern = DemandPatterns.Slow
                TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            Else
                stockItem.DemandPattern = DemandPatterns.Lumpy
                TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
            End If

            'get last Soq.NumPeriodsAveSlowLumpy periods
            soqPeriods = New SoqPeriodCollection
            For index As Integer = 2 To SoqConstants.NumPeriodsAveSlowLumpy + 1
                soqPeriods.Add(New SoqPeriod(index, CInt(stockItem.UnitSales(index)), stockItem.FlierPeriod(index), stockItem.DaysOutStock(index)))
                TraceDebug(stockItem.ToString, "Period Used: " & index - 1)
            Next

            'get period adjusters for periods
            For Each period As SoqPeriod In soqPeriods
                period.Adjuster = weights.Adjuster(period.WeekNumber)
            Next

            stockItem.PeriodDemand = soqPeriods.AverageSalesAdjusted
            stockItem.PeriodTrend = 0
            stockItem.ErrorForecast = 0
            stockItem.ErrorSmoothed = 0
            Return True
        End If

        Return DemandPatternInitialiseNormal(stockItem, soqPeriods)

    End Function

    Private Function DemandPatternInitialiseNormal(ByRef stock As Stock, ByRef soqPeriods As SoqPeriodCollection) As Boolean

        'calculate required values
        Dim sdSales As Decimal = soqPeriods.StdDevSales
        Dim sdPeriods As Decimal = soqPeriods.StdDevPeriods
        Dim correlation As Decimal = soqPeriods.CorrelationCoeff(sdSales, sdPeriods)
        Dim trendSig As Decimal = SoqConstants.TrendSignificance(soqPeriods.Count)

        'output trace
        TraceDebug(stock.ToString, "Initialising demand pattern - normal")
        TraceDebug(stock.ToString, "Standard dev sales (sigmaR) = " & sdSales)
        TraceDebug(stock.ToString, "Standard dev periods (sigmaP) = " & sdPeriods)
        TraceDebug(stock.ToString, "Correlation (r) = " & correlation)
        TraceDebug(stock.ToString, "Soq.TrendSignificance(" & soqPeriods.Count & ") = " & trendSig)

        'test for significance of trend
        If Math.Abs(correlation) > trendSig Then
            Return DemandPatternInitialiseTrend(stock, soqPeriods)
        Else
            Return DemandPatternInitialiseFastErratic(stock, soqPeriods)
        End If

    End Function

    Private Function DemandPatternInitialiseFastErratic(ByRef stock As Stock, ByRef soqPeriods As SoqPeriodCollection) As Boolean

        Dim adjustedAverage As Decimal = 0
        Dim sdSales As Decimal = 0
        Dim sdPeriods As Decimal = 0

        Select Case soqPeriods.Count
            Case Is < 4
                TraceDebug(stock.ToString, "Initialising demand pattern - already set to fast")
                adjustedAverage = stock.PeriodDemand
                sdSales = stock.PeriodDemand * SoqConstants.StdDevMultiplier(stock.PeriodDemand)

            Case Else
                TraceDebug(stock.ToString, "Initialising demand pattern - fast erratic")
                adjustedAverage = soqPeriods.AverageSalesAdjusted
                sdSales = soqPeriods.StdDevSales
                sdPeriods = soqPeriods.StdDevPeriods
        End Select

        'output trace
        TraceDebug(stock.ToString, "Soq adjusted average (Rbar) = " & adjustedAverage)
        TraceDebug(stock.ToString, "Standard dev sales (sigmaR) = " & sdSales)
        TraceDebug(stock.ToString, "Standard dev periods (sigmaP) = " & sdPeriods)


        'perform flier elimination process if more equal to 4 periods

        If soqPeriods.Count >= 4 Then
            Dim lowLimit As Decimal = adjustedAverage - (SoqConstants.FlierEliminator(soqPeriods.Count) * sdSales)
            Dim highLimit As Decimal = adjustedAverage + (SoqConstants.FlierEliminator(soqPeriods.Count) * sdSales)

            TraceDebug(stock.ToString, "Performing flier elimination")
            TraceDebug(stock.ToString, "Flier eliminator low limit = " & lowLimit)
            TraceDebug(stock.ToString, "Flier eliminator high limit = " & highLimit)

            For Each period As SoqPeriod In soqPeriods
                If period.Sales < lowLimit OrElse period.Sales > highLimit Then
                    TraceDebug(stock.ToString, "Flier found at period " & period.Index)
                    stock.FlierPeriod(period.Index) = "F"
                    Return False
                End If
            Next

            TraceDebug(stock.ToString, "Flier elimination - none found")

            If adjustedAverage <> 0 AndAlso (sdSales / adjustedAverage) < SoqConstants.ErracticLimitVariance Then
                stock.DemandPattern = DemandPatterns.Fast
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            Else
                stock.DemandPattern = DemandPatterns.Erratic
                TraceDebug(stock.ToString, "Demand pattern set to " & stock.DemandPattern)
            End If
        End If

        'set initial parameters in table
        stock.PeriodDemand = adjustedAverage
        stock.PeriodTrend = 0
        stock.ErrorForecast = Math.Max(sdSales, adjustedAverage * SoqConstants.StdDevMin)
        stock.ErrorSmoothed = 0
        Return True

    End Function

    Private Function DemandPatternInitialiseTrend(ByRef stockItem As Stock, ByRef soqPeriods As SoqPeriodCollection) As Boolean

        'calculate required values
        Dim sdSales As Decimal = soqPeriods.StdDevSales
        Dim sdPeriods As Decimal = soqPeriods.StdDevPeriods
        Dim correlation As Decimal = soqPeriods.CorrelationCoeff(sdSales, sdPeriods)
        Dim adjustedAverage As Decimal = soqPeriods.AverageSalesAdjusted
        Dim b As Decimal = Math.Abs(correlation * sdSales / sdPeriods)

        'output trace
        TraceDebug(stockItem.ToString, "Initialising demand pattern - trend")
        TraceDebug(stockItem.ToString, "Standard dev sales (sigmaR)= " & sdSales)
        TraceDebug(stockItem.ToString, "Standard dev periods (sigmaP)= " & sdPeriods)
        TraceDebug(stockItem.ToString, "Soq correlation (r)= " & correlation)
        TraceDebug(stockItem.ToString, "Soq adjusted average (Rbar)= " & adjustedAverage)
        TraceDebug(stockItem.ToString, "Estimate Trend (b)= " & b)

        'set trend up values
        stockItem.DemandPattern = DemandPatterns.TrendUp
        stockItem.PeriodDemand = adjustedAverage + (soqPeriods.Count - soqPeriods.AverageIndeces) * b
        stockItem.PeriodTrend = 0
        stockItem.ErrorForecast = CDec(sdSales * Math.Sqrt((1 - Math.Pow(correlation, 2)) * (soqPeriods.Count - 1) / (soqPeriods.Count - 2)))
        stockItem.ErrorSmoothed = 0

        'limit error forecast if too low
        If stockItem.ErrorForecast < (adjustedAverage * SoqConstants.StdDevMin) Then
            stockItem.ErrorForecast = adjustedAverage * SoqConstants.StdDevMin
        End If

        'if negative then set trend down
        If correlation > 0 Then
            stockItem.DemandPattern = DemandPatterns.TrendDown

            'if trend down is allowed then check for trend down using last 4 periods
            If SoqConstants.UseNegativeTrend Then
                soqPeriods = New SoqPeriodCollection
                For index As Integer = 2 To 5
                    soqPeriods.Add(New SoqPeriod(index, CInt(stockItem.UnitSales(index)), stockItem.FlierPeriod(index), stockItem.DaysOutStock(index)))
                Next

                stockItem.PeriodDemand = soqPeriods.AverageSales
                stockItem.PeriodTrend = 0
                stockItem.ErrorForecast = stockItem.PeriodDemand * SoqConstants.StdDevMultiplier(stockItem.PeriodDemand)
                stockItem.ErrorSmoothed = 0
            End If
        End If

        TraceDebug(stockItem.ToString, "Demand pattern set to " & stockItem.DemandPattern)
        Return True

    End Function

    Private Sub InitialiseBufferConversion(ByRef stockItem As Stock, ByVal fixedLead As Decimal, ByVal fixedReview As Decimal)

        'initialise buffer conversion constant for item based on supplier fixed lead and review time
        TraceDebug(stockItem.ToString, "Forecasting Buffer")
        Dim q As Double = Math.Max(Math.Max(stockItem.PeriodDemand * fixedReview, stockItem.PackSize), stockItem.MinimumQty)
        Dim root As Double = Math.Sqrt(fixedLead + fixedReview)
        Dim sigma As Double = stockItem.ErrorForecast

        'output trace
        TraceDebug(stockItem.ToString, "q = " & q)
        TraceDebug(stockItem.ToString, "root = " & root)
        TraceDebug(stockItem.ToString, "sigma = " & sigma)

        ' Set bounds for sigma if erratic
        If stockItem.DemandPattern <> DemandPatterns.Erratic Then
            If sigma < stockItem.PeriodDemand * SoqConstants.StdDevMin Then sigma = stockItem.PeriodDemand * SoqConstants.StdDevMin
            If sigma > stockItem.PeriodDemand * SoqConstants.StdDevMax Then sigma = stockItem.PeriodDemand * SoqConstants.StdDevMax
            TraceDebug(stockItem.ToString, "sigma (bounded) = " & sigma)
        Else
            sigma = stockItem.PeriodDemand * SoqConstants.ErracticLimitVariance
            TraceDebug(stockItem.ToString, "sigma (erratic) = " & sigma)
        End If

        'get service level from stock defaulting to system level if zero
        Dim serviceLevel As Decimal = stockItem.ServiceLevelOverride
        If serviceLevel = 0 Then serviceLevel = SoqConstants.TargetServicePercent(0)

        'check for zero denominator
        Dim z As Double = 0
        Dim ek As Double = 3.1
        If sigma * root <> 0 Then
            z = q / (sigma * root)
            ek = z * (1 - serviceLevel / 100)
        End If

        'set buffer
        stockItem.BufferConversion = CDec(SoqConstants.ServiceFunction(CDec(ek)) * root)

        'output trace
        TraceDebug(stockItem.ToString, "z = " & z)
        TraceDebug(stockItem.ToString, "ek = " & ek)
        TraceDebug(stockItem.ToString, "buffer = " & stockItem.BufferConversion)

    End Sub

    Public Sub SoqInventory(ByRef sup As Supplier)

        _isInGUIMode = True

        SoqUpdateOrderLine_Implementation(sup)

    End Sub


    Private Sub SoqUpdateOrderLine(ByRef sup As Supplier)

        _isInGUIMode = False

        SoqUpdateOrderLine_Implementation(sup)

    End Sub

    Private Sub SoqUpdateOrderLine_Implementation(ByRef sup As Supplier)

        Try
            sup.SoqDate = Now.Date
            sup.SoqNumber = Nothing
            sup.SoqOrdered = False

            'check if deleted
            If sup.IsDeleted Then
                RaiseEvent SoqCompleted(sup, CompletedStatus.Deleted)
                Exit Sub
            End If

            'get all stock items for this supplier
            Dim daysOpen As Integer = Store.GetDaysOpen
            Dim supLeadReview As Integer = sup.DaysLeadVariable()
            Dim supLeadReviewFixed As Decimal = sup.DaysLeadReviewFixed(daysOpen)
            Dim orderDue As Date = sup.DateOrderDue(daysOpen)
            Dim orderNextDue As Date = sup.DateNextOrderDue(daysOpen)
            Dim stockcount As Integer = sup.Stocks.Count

            For Each stockItem As Stock In sup.Stocks
                If _isInGUIMode Then
                    RaiseEvent SoqProgress(sup, CInt(sup.Stocks.IndexOf(stockItem) / stockcount * 100))
                End If

                'exit if stock obsolete or superceded or non-stock item
                stockItem.OrderLevel = 0
                If stockItem.DemandPattern = DemandPatterns.Obsolete Then Continue For
                If stockItem.DemandPattern = DemandPatterns.Superceded Then Continue For
                If stockItem.IsNonStock Then Continue For

                'initialise variables
                Dim a As Double = 0
                Dim b As Double = 0
                Dim c As Double = 0
                Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 53))
                Dim leadReview As Integer = supLeadReview
                Dim leadReviewFixed As Decimal = supLeadReviewFixed

                Dim bias As New List(Of Decimal)
                Dim biasCheck As Decimal = 0
                For dayWeek As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
                    bias.Add(stockItem.SalesBias(CType(dayWeek, DayOfWeek)))
                    biasCheck += stockItem.SalesBias(CType(dayWeek, DayOfWeek))
                Next
                If biasCheck <> 1 Then
                    bias.Clear()
                    For dayWeek As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
                        bias.Add(SoqConstants.SalesBias(dayWeek))
                    Next
                End If

                'Loop over 'a' days
                Dim day As Integer = 0
                Do While (Now.AddDays(day).DayOfWeek + 1) < daysOpen
                    a += bias(day)
                    day += 1
                    leadReview -= 1
                    If leadReview = 0 Then Exit Do
                Loop

                'get period adjuster, working forecast total, sum Wi and Sum Squares Wi
                Dim weights As Store.SaleWeightCollection = Store.SaleWeight.GetActiveWeights(stockItem.SaleWeightBank)
                weights.AddActiveWeights(stockItem.SaleWeightPromo)
                weights.AddActiveWeights(stockItem.SaleWeightSeason)
                Dim Wi As Double = weights.Adjuster(week)
                Dim FLT As Double = stockItem.PeriodDemand * a * Wi
                Dim SumW As Double = a * Wi
                Dim SumWW As Double = a * Wi * Wi

                'Loop over 'b' weeks
                Do While leadReview >= daysOpen
                    week += 1 : If week > 53 Then week = 0
                    Wi = weights.Adjuster(week)
                    leadReview -= daysOpen
                    FLT += (stockItem.PeriodDemand + week * stockItem.PeriodTrend) * Wi
                    SumW += Wi
                    SumWW += Wi * Wi
                Loop

                'do 'c' days if any left
                If leadReview > 0 Then
                    For i As Integer = 0 To leadReview
                        c += bias(i)
                    Next
                    week += 1 : If week > 53 Then week = 0
                    Wi = weights.Adjuster(week)
                    FLT = FLT + c * (stockItem.PeriodDemand + week * stockItem.PeriodTrend) * Wi
                    SumW = SumW + c * Wi
                    SumWW = SumWW + c * Wi * Wi
                End If

                ' Calculate buffer stock level whilst setting bounds for sigma
                If stockItem.DemandPattern <> DemandPatterns.Lumpy Then
                    Dim sigma As Double = stockItem.ErrorForecast
                    If sigma < stockItem.PeriodDemand * SoqConstants.StdDevMin Then sigma = stockItem.PeriodDemand * SoqConstants.StdDevMin
                    If sigma > stockItem.PeriodDemand * SoqConstants.StdDevMax Then sigma = stockItem.PeriodDemand * SoqConstants.StdDevMax
                    stockItem.BufferStock = CInt(Math.Round(stockItem.BufferConversion * Math.Sqrt(SumWW) * sigma / Math.Sqrt(leadReviewFixed)))
                End If

                'calculate stock loss
                Dim Stockloss As Decimal = leadReviewFixed * stockItem.StockLossPerWeek

                'calculate order SOQ value 
                stockItem.OrderLevel = CInt(FLT + stockItem.BufferStock + stockItem.MinimumStockQty(orderDue, orderNextDue) - stockItem.OnHandQty - stockItem.OnOrderQty + Stockloss)

                'validate SOQ value to be maximum and pack size
                If stockItem.OrderLevel <= 0 Then
                    stockItem.OrderLevel = 0
                Else
                    stockItem.OrderLevel = CInt(Math.Ceiling(stockItem.OrderLevel))
                    Dim pack As Integer = CInt(Math.Ceiling(stockItem.OrderLevel / stockItem.PackSize))
                    stockItem.OrderLevel = pack * stockItem.PackSize
                End If

            Next

            'update supplier and stock items
            If _isInGUIMode Then
                SupplierUpdateSoqInventory(sup)
                RaiseEvent SoqCompleted(sup, CompletedStatus.Successful)
            End If

        Catch ex As Exception
            RaiseEvent SoqCompleted(sup, CompletedStatus.Error)
        End Try

    End Sub


    Private Sub TraceDebug(ByVal section1 As String, Optional ByVal section2 As String = "")

        Dim sb As New StringBuilder(section1)
        If section2.Length > 0 Then sb.Append(" - " & section2)
        Trace.WriteLine(sb.ToString, Me.GetType.ToString)

    End Sub

    Public Enum CompletedStatus
        Successful
        Cancelled
        NoStocks
        [Error]
        Deleted
    End Enum

    Public Enum State
        Expired
        No
        Yes
        Ordered
    End Enum

End Class