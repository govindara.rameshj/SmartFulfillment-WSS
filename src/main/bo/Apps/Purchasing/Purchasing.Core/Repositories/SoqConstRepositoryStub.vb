﻿Public Class SoqConstRepositoryStub
    Implements ISoqRepository

#Region "Stub Code"

    Private _Requirement_RF0856_Enabled As System.Nullable(Of Boolean)

    Public Sub SetRequirementEnabled()

        _Requirement_RF0856_Enabled = True

    End Sub

    Public Sub SetRequirementDisabled()

        _Requirement_RF0856_Enabled = False

    End Sub
#End Region

#Region "Interface"

    Public Function Requirement_RF0861_Enabled() As Boolean? Implements ISoqRepository.Requirement_RF0856_Enabled

        Return _Requirement_RF0856_Enabled

    End Function

#End Region
End Class
