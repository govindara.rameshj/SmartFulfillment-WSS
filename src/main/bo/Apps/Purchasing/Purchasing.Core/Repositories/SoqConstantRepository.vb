﻿Public Class SoqConstantRepository
    Implements ISoqRepository

    Public Function Requirement_RF0856_Enabled() As Boolean? Implements ISoqRepository.Requirement_RF0856_Enabled

        Return Cts.Oasys.Core.System.Parameter.GetBoolean(-856)

    End Function

End Class
