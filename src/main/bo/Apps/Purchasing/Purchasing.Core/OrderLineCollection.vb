﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class OrderLineCollection
    Inherits BindingList(Of OrderLine)
    Private _order As Order

    Friend Sub New(ByRef order As Order)
        MyBase.New()
        _order = order
        LoadLines()
    End Sub

    Private Sub LoadLines()

        Dim dt As DataTable = DataOperations.OrderGetLines(_order.Id)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New OrderLine(dr))
        Next

    End Sub


    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As OrderLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Clears all order qtys for lines in collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearOrderQty()
        For Each line As OrderLine In Me.Items
            line.OrderQty = 0
        Next
    End Sub

    ''' <summary>
    ''' Calculates receipt order qtys and values
    ''' </summary>
    ''' <param name="orderqty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal)

        orderQty = 0
        orderValue = 0

        For Each line As OrderLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.Price)
        Next

    End Sub


    Public Function OrderedQty() As Integer

        Dim qty As Integer = 0
        For Each line As OrderLine In Me.Items
            qty += line.OrderQty
        Next

        Return qty

    End Function

    Public Function OrderedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As OrderLine In Me.Items
            value += (line.OrderQty * line.Price)
        Next

        Return value

    End Function

End Class