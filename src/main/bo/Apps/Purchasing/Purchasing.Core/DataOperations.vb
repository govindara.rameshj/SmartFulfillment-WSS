﻿Imports Cts.Oasys.Core.Stock
Imports Oasys.Data

Friend Module DataOperations


    Friend Function ReceiptGetMaintainableOrders() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ReceiptGetMaintainableOrders)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function ReceiptGetLines(ByVal receiptNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ReceiptGetLines)
                com.AddParameter(My.Resources.Parameters.ReceiptNumber, receiptNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function ReceiptInsert(ByRef receipt As Receipt) As String

        Dim number As String = String.Empty

        Using con As New Connection
            Try
                con.StartTransaction()

                Using com As New Command(con, My.Resources.Procedures.ReceiptOrderInsert)
                    com.AddParameter(My.Resources.Parameters.EmployeeId, receipt.EmployeeId)
                    com.AddParameter(My.Resources.Parameters.Comments, receipt.Comments)
                    com.AddParameter(My.Resources.Parameters.Value, receipt.Value)
                    com.AddParameter(My.Resources.Parameters.Quantity, receipt.Lines.ReceivedQty)
                    com.AddParameter(My.Resources.Parameters.PoNumber, receipt.PoNumber, SqlDbType.Int)
                    com.AddParameter(My.Resources.Parameters.PoId, receipt.PoId)
                    com.AddParameter(My.Resources.Parameters.PoConsignNumber, receipt.PoConsignNumber, SqlDbType.Int)
                    com.AddParameter(My.Resources.Parameters.PoSupplierNumber, receipt.PoSupplierNumber)
                    com.AddParameter(My.Resources.Parameters.PoSupplierBbc, receipt.PoSupplierBbc)
                    com.AddParameter(My.Resources.Parameters.PoOrderDate, receipt.PoOrderDate)
                    com.AddParameter(My.Resources.Parameters.PoSoqNumber, receipt.PoSoqNumber, SqlDbType.Int)
                    com.AddParameter(My.Resources.Parameters.PoReleaseNumber, receipt.PoReleaseNumber)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote1, receipt.PoDeliveryNote1)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote2, receipt.PoDeliveryNote2)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote3, receipt.PoDeliveryNote3)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote4, receipt.PoDeliveryNote4)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote5, receipt.PoDeliveryNote5)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote6, receipt.PoDeliveryNote6)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote7, receipt.PoDeliveryNote7)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote8, receipt.PoDeliveryNote8)
                    com.AddParameter(My.Resources.Parameters.PoDeliveryNote9, receipt.PoDeliveryNote9)
                    com.AddParameter(My.Resources.Parameters.Number, number, SqlDbType.Char, 6,ParameterDirection.Output)
                    com.ExecuteNonQuery()

                    number = CStr(com.GetParameterValue(My.Resources.Parameters.Number))
                End Using

                'insert all lines
                For Each line As ReceiptLine In receipt.Lines
                    line.ReceiptNumber = number
                    line.Sequence = receipt.Lines.NextSequenceNumber

                    Using com As New Command(con, My.Resources.Procedures.ReceiptLineInsert)
                        com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                        com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                        com.AddParameter(My.Resources.Parameters.OrderPrice, line.OrderPrice)
                        com.AddParameter(My.Resources.Parameters.ReceivedQty, line.ReceivedQty)
                        com.AddParameter(My.Resources.Parameters.ReceivedPrice, line.ReceivedPrice)
                        com.AddParameter(My.Resources.Parameters.OrderLineId, line.OrderLineId)
                        com.AddParameter("@StockLogType", "72")
                        com.AddParameter("@StockLogKey", receipt.PoId & Space(1) & line.OrderLineId & Space(1) & line.ReceiptNumber & Space(1) & line.Sequence)
                        com.AddParameter(My.Resources.Parameters.UserId, receipt.EmployeeId)
                        com.ExecuteNonQuery()
                    End Using
                Next

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try

        End Using

        Return number

    End Function

    Friend Sub ReceiptMaintain(ByRef receipt As Receipt, ByVal userId As Integer)

        Using con As New Connection
            con.StartTransaction()

            Try
                'update receipt header first
                Using com As New Command(con, My.Resources.Procedures.ReceiptOrderUpdate)
                    com.AddParameter(My.Resources.Parameters.Number, receipt.Number)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote1, receipt.PoDeliveryNote1)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote2, receipt.PoDeliveryNote2)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote3, receipt.PoDeliveryNote3)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote4, receipt.PoDeliveryNote4)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote5, receipt.PoDeliveryNote5)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote6, receipt.PoDeliveryNote6)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote7, receipt.PoDeliveryNote7)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote8, receipt.PoDeliveryNote8)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote9, receipt.PoDeliveryNote9)
                    com.AddParameter(My.Resources.Parameters.Value, receipt.Value)
                    com.AddParameter(My.Resources.Parameters.Comments, receipt.Comments)
                    com.ExecuteNonQuery()
                End Using

                'check status of each line add or update accordingly
                For Each line As ReceiptLine In receipt.Lines
                    If line.Sequence Is Nothing Then
                        line.ReceiptNumber = receipt.Number
                        line.Sequence = receipt.Lines.NextSequenceNumber
                        Using com As New Command(con, My.Resources.Procedures.ReceiptLineInsert)
                            com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                            com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                            com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                            com.AddParameter(My.Resources.Parameters.OrderPrice, line.OrderPrice)
                            com.AddParameter(My.Resources.Parameters.ReceivedQty, line.ReceivedQty)
                            com.AddParameter(My.Resources.Parameters.ReceivedPrice, line.ReceivedPrice)
                            com.AddParameter(My.Resources.Parameters.OrderLineId, line.OrderLineId)
                            com.AddParameter("@StockLogType", "73")
                            com.AddParameter("@StockLogKey", line.ReceiptNumber & Space(1) & line.Sequence)
                            com.AddParameter(My.Resources.Parameters.UserId, userId)
                            com.ExecuteNonQuery()
                        End Using

                    Else
                        If line.ReceivedQty <> line.ReceivedOriginal Then
                            Using com As New Command(con, My.Resources.Procedures.ReceiptLineUpdate)
                                com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                                com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                                com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                                com.AddParameter(My.Resources.Parameters.ReceivedChange, line.ReceivedQty - line.ReceivedOriginal)
                                com.AddParameter(My.Resources.Parameters.UserId, userId)
                                com.ExecuteNonQuery()
                            End Using
                        End If
                    End If
                Next

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try
        End Using

    End Sub


    Friend Function ReturnGetNonReleased() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ReturnGetNonReleased)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function ReturnGetLines(ByVal returnId As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ReturnGetLines)
                com.AddParameter(My.Resources.Parameters.ReturnId, returnId)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function



    Friend Function SupplierGetAll() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SupplierGetAll)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function SupplierGetAllForOrder(ByVal type As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SupplierGetAllForOrder)
                com.AddParameter(My.Resources.Parameters.Type, type)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function SupplierGetOrdering(ByVal supplierNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumber)
                com.AddParameter(My.Resources.Parameters.Type, "O")
                com.AddParameter(My.Resources.Parameters.Number, supplierNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function SupplierGetHeadOffice(ByVal supplierNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumber)
                com.AddParameter(My.Resources.Parameters.Type, "S")
                com.AddParameter(My.Resources.Parameters.Number, supplierNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function SupplierGetByNumbers(ByVal supplierNumbers As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumbers)
                com.AddParameter(My.Resources.Parameters.Numbers, supplierNumbers)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Sub SupplierUpdateSoqInit(ByVal sup As Supplier)

        Using con As New Connection
            con.StartTransaction()

            Try
                'update all stock items for supplier
                For Each stockItem As Stock In sup.Stocks
                    Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInit)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.DemandPattern, stockItem.DemandPattern)
                        com.AddParameter(My.Resources.Parameters.PeriodDemand, stockItem.PeriodDemand)
                        com.AddParameter(My.Resources.Parameters.PeriodTrend, stockItem.PeriodTrend)
                        com.AddParameter(My.Resources.Parameters.ErrorForecast, stockItem.ErrorForecast)
                        com.AddParameter(My.Resources.Parameters.ErrorSmoothed, stockItem.ErrorSmoothed)
                        com.AddParameter(My.Resources.Parameters.BufferConversion, stockItem.BufferConversion)
                        com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                        com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                        com.AddParameter(My.Resources.Parameters.ValueAnnualUsage, stockItem.ValueAnnualUsage)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod1, stockItem.FlierPeriod1)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod2, stockItem.FlierPeriod2)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod3, stockItem.FlierPeriod3)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod4, stockItem.FlierPeriod4)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod5, stockItem.FlierPeriod5)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod6, stockItem.FlierPeriod6)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod7, stockItem.FlierPeriod7)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod8, stockItem.FlierPeriod8)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod9, stockItem.FlierPeriod9)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod10, stockItem.FlierPeriod10)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod11, stockItem.FlierPeriod11)
                        com.AddParameter(My.Resources.Parameters.FlierPeriod12, stockItem.FlierPeriod12)
                        com.ExecuteNonQuery()
                    End Using
                Next

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try
        End Using

    End Sub

    Friend Sub SupplierUpdateSoqInventory(ByRef sup As Supplier)

        Using con As New Connection
            con.StartTransaction()

            Try
                Dim suggested As Boolean = False

                'update all stock items for supplier
                For Each stockItem As Stock In sup.Stocks
                    If stockItem.OrderLevel > 0 Then suggested = True

                    Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInventory)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                        com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                        com.ExecuteNonQuery()
                    End Using
                Next

                'update supplier
                Using com As New Command(con, My.Resources.Procedures.SupplierUpdateSoqInventory)
                    com.AddParameter(My.Resources.Parameters.Number, sup.Number)
                    com.AddParameter(My.Resources.Parameters.Suggested, suggested)
                    com.AddParameter(My.Resources.Parameters.SoqDate, sup.SoqDate)
                    com.AddParameter(My.Resources.Parameters.SoqOrdered, sup.SoqOrdered)
                    com.AddParameter(My.Resources.Parameters.SoqNumber, sup.SoqNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                    com.ExecuteNonQuery()

                    sup.SoqNumber = CType(com.GetParameterValue(My.Resources.Parameters.SoqNumber), Integer?)
                End Using

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try
        End Using

    End Sub

    Friend Sub SupplierUpdateSoqReset(ByRef sup As Supplier)

        Using con As New Connection
            con.StartTransaction()

            Try
                'update all stock items for supplier
                For Each stockItem As Stock In sup.Stocks
                    stockItem.BufferStock = 0
                    stockItem.OrderLevel = 0

                    Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInventory)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                        com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                        com.ExecuteNonQuery()
                    End Using
                Next

                'update supplier
                Using com As New Command(con, My.Resources.Procedures.SupplierUpdateSoqInventory)
                    sup.SoqDate = Now.Date
                    sup.SoqOrdered = False
                    sup.SoqNumber = Nothing

                    com.AddParameter(My.Resources.Parameters.Number, sup.Number)
                    com.AddParameter(My.Resources.Parameters.Suggested, False)
                    com.AddParameter(My.Resources.Parameters.SoqDate, sup.SoqDate)
                    com.AddParameter(My.Resources.Parameters.SoqOrdered, sup.SoqOrdered)
                    com.AddParameter(My.Resources.Parameters.SoqNumber, sup.SoqNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                    com.ExecuteNonQuery()
                End Using

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try
        End Using

    End Sub


    Friend Function OrdersGetNotConsigned() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoGetNotConsigned)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function OrdersGetConsignedByDateRange(ByVal startDate As Date, ByVal endDate As Date) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoGetConsigned)
                com.AddParameter(My.Resources.Parameters.StartDate, startDate)
                com.AddParameter(My.Resources.Parameters.EndDate, endDate)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function OrdersGetOutstanding() As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoGetOutstanding)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function OrderGetLines(ByVal orderId As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoGetLines)
                com.AddParameter(My.Resources.Parameters.OrderId, orderId)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function OrderConsign(ByVal orderId As Integer, ByVal consign As Consignment) As String

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoConsign)
                com.AddParameter(My.Resources.Parameters.OrderId, orderId)
                com.AddParameter(My.Resources.Parameters.PoNumber, consign.PoNumber)
                com.AddParameter(My.Resources.Parameters.ReleaseNumber, consign.ReleaseNumber)
                com.AddParameter(My.Resources.Parameters.SupplierNumber, consign.SupplierNumber)
                com.AddParameter(My.Resources.Parameters.EmployeeId, consign.EmployeeId)
                com.AddParameter(My.Resources.Parameters.PalletsIn, consign.PalletsIn)
                com.AddParameter(My.Resources.Parameters.PalletsOut, consign.PalletsOut)
                com.AddParameter(My.Resources.Parameters.DeliveryNote1, consign.DeliveryNote1)
                com.AddParameter(My.Resources.Parameters.DeliveryNote2, consign.DeliveryNote2)
                com.AddParameter(My.Resources.Parameters.DeliveryNote3, consign.DeliveryNote3)
                com.AddParameter(My.Resources.Parameters.DeliveryNote4, consign.DeliveryNote4)
                com.AddParameter(My.Resources.Parameters.DeliveryNote5, consign.DeliveryNote5)
                com.AddParameter(My.Resources.Parameters.DeliveryNote6, consign.DeliveryNote6)
                com.AddParameter(My.Resources.Parameters.DeliveryNote7, consign.DeliveryNote7)
                com.AddParameter(My.Resources.Parameters.DeliveryNote8, consign.DeliveryNote8)
                com.AddParameter(My.Resources.Parameters.DeliveryNote9, consign.DeliveryNote9)
                com.AddParameter(My.Resources.Parameters.Number, consign.Number, SqlDbType.Int, 4, ParameterDirection.Output)
                com.ExecuteNonQuery()

                Return CStr(com.GetParameterValue(My.Resources.Parameters.Number))
            End Using
        End Using

    End Function

    Friend Sub OrderInsertNew(ByVal po As Order)

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.PoInsertNewOrder)
                com.AddParameter(My.Resources.Parameters.SupplierNumber, po.SupplierNumber)
                com.AddParameter(My.Resources.Parameters.SupplierBbc, po.SupplierBbc)
                com.AddParameter(My.Resources.Parameters.SupplierTradanet, po.SupplierTradanet)
                com.AddParameter(My.Resources.Parameters.SoqNumber, po.SoqNumber, SqlDbType.Int)
                com.AddParameter(My.Resources.Parameters.EmployeeId, po.EmployeeId)
                com.AddParameter(My.Resources.Parameters.Id, po.Id, SqlDbType.Int, 10, ParameterDirection.Output)
                com.AddParameter(My.Resources.Parameters.Number, po.PoNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                com.AddParameter(My.Resources.Parameters.SourceEntry, po.SourceEntry, SqlDbType.Char, 1, ParameterDirection.Output)
                com.AddParameter(My.Resources.Parameters.IsDeleted, po.IsDeleted, SqlDbType.Bit, 4, ParameterDirection.Output)
                com.AddParameter(My.Resources.Parameters.DateCreated, po.DateCreated, SqlDbType.Date, 10, ParameterDirection.Output)
                com.ExecuteNonQuery()

                po.Id = CInt(com.GetParameterValue(My.Resources.Parameters.Id))
                po.PoNumber = CInt(com.GetParameterValue(My.Resources.Parameters.Number))
                po.SourceEntry = CStr(com.GetParameterValue(My.Resources.Parameters.SourceEntry))
                po.IsDeleted = CBool(com.GetParameterValue(My.Resources.Parameters.IsDeleted))
                po.DateCreated = CDate(com.GetParameterValue(My.Resources.Parameters.DateCreated))
            End Using
        End Using

    End Sub

    Friend Sub OrderUpdate(ByVal po As Order)

        'initialise order header values
        po.IsDeleted = False
        po.IsCommed = False
        po.Cartons = 0
        po.Value = 0
        po.Units = 0

        Using con As New Connection
            Try
                con.StartTransaction()

                'insert all lines (and update stock as ordered)
                For Each line As OrderLine In po.Lines
                    line.OrderId = po.Id
                    po.Cartons = CInt(po.Cartons + (line.OrderQty / line.SkuPackSize))
                    po.Units += line.OrderQty
                    po.Value += (line.OrderQty * line.Price)

                    Using com As New Command(con, My.Resources.Procedures.PoInsertLine)
                        com.AddParameter(My.Resources.Parameters.OrderId, line.OrderId)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.SkuProductCode, line.SkuProductCode)
                        com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                        com.AddParameter(My.Resources.Parameters.Price, line.Price)
                        com.AddParameter(My.Resources.Parameters.Cost, line.Cost)
                        com.ExecuteNonQuery()
                    End Using
                Next

                'update header and supplier as ordered (and reset all stock order levels)
                Using com As New Command(con, My.Resources.Procedures.PoUpdate)
                    com.AddParameter(My.Resources.Parameters.Id, po.Id)
                    com.AddParameter(My.Resources.Parameters.IsDeleted, po.IsDeleted)
                    com.AddParameter(My.Resources.Parameters.DateDue, po.DateDue)
                    com.AddParameter(My.Resources.Parameters.Cartons, po.Cartons)
                    com.AddParameter(My.Resources.Parameters.Value, po.Value)
                    com.AddParameter(My.Resources.Parameters.Units, po.Units)
                    com.AddParameter(My.Resources.Parameters.IsCommed, po.IsCommed)
                    com.ExecuteNonQuery()
                End Using

                con.CommitTransaction()

            Catch ex As Exception
                con.RollbackTransaction()
                Throw
            End Try

        End Using

    End Sub


    Friend Function GetConsignment(ByVal number As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ConsignmentGetByNumber)
                com.AddParameter(My.Resources.Parameters.Number, number)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function ContainerGetByDateRange(ByVal startDate As Date, ByVal endDate As Date) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ContainerGetByDateRange)
                com.AddParameter(My.Resources.Parameters.StartDate, startDate)
                com.AddParameter(My.Resources.Parameters.EndDate, endDate)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function ContainerGetLines(ByVal assemblyDepotNumber As String, ByVal containerNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.ContainerGetLines)
                com.AddParameter(My.Resources.Parameters.AssemblyDepotNumber, assemblyDepotNumber)
                com.AddParameter(My.Resources.Parameters.ContainerNumber, containerNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

End Module
