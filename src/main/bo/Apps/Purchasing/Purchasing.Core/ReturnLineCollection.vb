﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class ReturnLineCollection
    Inherits BindingList(Of ReturnLine)

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal returnId As Integer)
        MyBase.New()
        LoadLines(returnId)
    End Sub


    Private Sub LoadLines(ByVal returnId As Integer)

        Dim dt As DataTable = DataOperations.ReturnGetLines(CStr(returnId))
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New ReturnLine(dr))
        Next

    End Sub


    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As ReturnLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Calculates return  qtys and values
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef qty As Integer, ByRef value As Decimal)

        qty = 0
        value = 0

        For Each line As ReturnLine In Me.Items
            qty += line.Qty
            value += (line.Qty * line.Price)
        Next

    End Sub


    Public Function Qty() As Integer

        Dim total As Integer = 0
        For Each line As ReturnLine In Me.Items
            total += line.Qty
        Next

        Return total

    End Function

    Public Function Value() As Decimal

        Dim total As Decimal = 0
        For Each line As ReturnLine In Me.Items
            total += (line.Qty * line.Price)
        Next

        Return total

    End Function

End Class