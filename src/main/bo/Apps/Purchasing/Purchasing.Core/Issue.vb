﻿Imports System.Data.SqlClient
Imports Oasys.Core
Imports System.ComponentModel

Public Class Issue
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As Integer
    Private _dateCreated As Date
    Private _supplierNumber As String
    Private _supplierName As String
    Private _supplierBbc As String
    Private _poNumber As Integer
    Private _receiptNumber As Nullable(Of Integer)
    Private _soqNumber As Nullable(Of Integer)
    Private _value As Decimal
    Private _pricingDocNumber As Nullable(Of Integer)
    Private _isImported As Boolean
    Private _isReceived As Boolean
    Private _isDeleted As Boolean
    Private _rejectReason As String
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As Integer
        Get
            Return _number
        End Get
        Friend Set(ByVal value As Integer)
            _number = value
        End Set
    End Property
    <ColumnMapping("DateCreated")> Public Property DateCreated() As Date
        Get
            Return _dateCreated
        End Get
        Friend Set(ByVal value As Date)
            _dateCreated = value
        End Set
    End Property
    <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
        Get
            If _supplierNumber Is Nothing Then _supplierNumber = String.Empty
            Return _supplierNumber
        End Get
        Private Set(ByVal value As String)
            _supplierNumber = value
        End Set
    End Property
    <ColumnMapping("SupplierName")> Public Property SupplierName() As String
        Get
            Return _supplierName
        End Get
        Private Set(ByVal value As String)
            _supplierName = value
        End Set
    End Property
    <ColumnMapping("SupplierBbc")> Public Property SupplierBbc() As String
        Get
            Return _supplierBbc
        End Get
        Private Set(ByVal value As String)
            _supplierBbc = value
        End Set
    End Property
    <ColumnMapping("PoNumber")> Public Property PoNumber() As Integer
        Get
            Return _poNumber
        End Get
        Friend Set(ByVal value As Integer)
            _poNumber = value
        End Set
    End Property
    <ColumnMapping("ReceiptNumber")> Public Property ReceiptNumber() As Nullable(Of Integer)
        Get
            Return _receiptNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _receiptNumber = value
        End Set
    End Property
    <ColumnMapping("SoqNumber")> Public Property SoqNumber() As Nullable(Of Integer)
        Get
            Return _soqNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _soqNumber = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Friend Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
    <ColumnMapping("PricingDocNumber")> Public Property PricingDocNumber() As Nullable(Of Integer)
        Get
            Return _pricingDocNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _pricingDocNumber = value
        End Set
    End Property
    <ColumnMapping("IsImported")> Public Property IsImported() As Boolean
        Get
            Return _isImported
        End Get
        Set(ByVal value As Boolean)
            _isImported = value
        End Set
    End Property
    <ColumnMapping("IsReceived")> Public Property IsReceived() As Boolean
        Get
            Return _isReceived
        End Get
        Private Set(ByVal value As Boolean)
            _isReceived = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Friend Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("RejectReason")> Public Property RejectReason() As String
        Get
            Return _rejectReason
        End Get
        Private Set(ByVal value As String)
            _rejectReason = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region

End Class
