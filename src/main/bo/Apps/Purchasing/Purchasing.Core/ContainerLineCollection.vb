﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class ContainerLineCollection
    Inherits BindingList(Of ContainerLine)
    Private _container As Container

    Friend Sub New(ByRef container As Container)
        MyBase.New()
        _container = container
        LoadLines()
    End Sub

    Private Sub LoadLines()

        Dim dt As DataTable = DataOperations.ContainerGetLines(_container.AssemblyDepotNumber, _container.Number)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New ContainerLine(dr))
        Next

    End Sub


    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As ContainerLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Returns total qty in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Qty() As Integer

        Dim q As Integer = 0
        For Each line As ContainerLine In Me.Items
            q += line.Qty
        Next

        Return q

    End Function

    ''' <summary>
    ''' Returns total value in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Value() As Decimal

        Dim val As Decimal = 0
        For Each line As ContainerLine In Me.Items
            val += (line.Qty * line.Price)
        Next

        Return val

    End Function

End Class