﻿Public Interface IBusinessLockIW

    Function CreateLock(ByVal WorkStationID As Integer, _
                        ByVal CurrentDateAndTime As DateTime, _
                        ByVal AssemblyName As String, _
                        ByVal ParameterValues As String) As Integer

    Sub RemoveLock(ByVal RecordID As Integer)

End Interface