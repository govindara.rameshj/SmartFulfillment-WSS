﻿Public Interface ISoqConstants
    ReadOnly Property SlowLumpyCheck() As Dictionary(Of Integer, Integer)
    ReadOnly Property ZeroDemandLumpyCheck() As Dictionary(Of Integer, Integer)
End Interface
