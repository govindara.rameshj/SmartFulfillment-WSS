﻿Imports Oasys.Core
Imports System.ComponentModel
Imports System.Text
Imports System.Reflection

Public Class Receipt
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _poId As Integer
    Private _poNumber As Nullable(Of Integer)
    Private _poConsignNumber As Nullable(Of Integer)
    Private _poSupplierNumber As String
    Private _poSupplierName As String
    Private _poSupplierBbc As Boolean
    Private _poSoqNumber As Nullable(Of Integer)
    Private _poReleaseNumber As Integer
    Private _poOrderDate As Date
    Private _dateReceipt As Date
    Private _employeeId As Integer
    Private _value As Decimal
    Private _comments As String
    Private _poDeliveryNote1 As String
    Private _poDeliveryNote2 As String
    Private _poDeliveryNote3 As String
    Private _poDeliveryNote4 As String
    Private _poDeliveryNote5 As String
    Private _poDeliveryNote6 As String
    Private _poDeliveryNote7 As String
    Private _poDeliveryNote8 As String
    Private _poDeliveryNote9 As String
    Private _lines As ReceiptLineCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("PoId")> Public Property PoId() As Integer
        Get
            Return _poId
        End Get
        Private Set(ByVal value As Integer)
            _poId = value
        End Set
    End Property
    <ColumnMapping("PoNumber")> Public Property PoNumber() As Nullable(Of Integer)
        Get
            Return _poNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poNumber = value
        End Set
    End Property
    <ColumnMapping("PoConsignNumber")> Public Property PoConsignNumber() As Nullable(Of Integer)
        Get
            Return _poConsignNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poConsignNumber = value
        End Set
    End Property
    <ColumnMapping("PoSupplierNumber")> Public Property PoSupplierNumber() As String
        Get
            Return _poSupplierNumber
        End Get
        Private Set(ByVal value As String)
            _poSupplierNumber = value
        End Set
    End Property
    <ColumnMapping("PoSupplierName")> Public Property PoSupplierName() As String
        Get
            Return _poSupplierName
        End Get
        Private Set(ByVal value As String)
            _poSupplierName = value
        End Set
    End Property
    <ColumnMapping("PoSupplierBbc")> Public Property PoSupplierBbc() As Boolean
        Get
            Return _poSupplierBbc
        End Get
        Private Set(ByVal value As Boolean)
            _poSupplierBbc = value
        End Set
    End Property
    <ColumnMapping("PoSoqNumber")> Public Property PoSoqNumber() As Nullable(Of Integer)
        Get
            Return _poSoqNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poSoqNumber = value
        End Set
    End Property
    <ColumnMapping("PoReleaseNumber")> Public Property PoReleaseNumber() As Integer
        Get
            Return _poReleaseNumber
        End Get
        Private Set(ByVal value As Integer)
            _poReleaseNumber = value
        End Set
    End Property
    <ColumnMapping("PoOrderDate")> Public Property PoOrderDate() As Date
        Get
            Return _poOrderDate
        End Get
        Private Set(ByVal value As Date)
            _poOrderDate = value
        End Set
    End Property
    <ColumnMapping("DateReceipt")> Public Property DateReceipt() As Date
        Get
            Return _dateReceipt
        End Get
        Private Set(ByVal value As Date)
            _dateReceipt = value
        End Set
    End Property
    <ColumnMapping("EmployeeId")> Public Property EmployeeId() As Integer
        Get
            Return _employeeId
        End Get
        Private Set(ByVal value As Integer)
            _employeeId = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Private Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal value As String)
            _comments = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote1")> Public Property PoDeliveryNote1() As String
        Get
            If _poDeliveryNote1 Is Nothing Then _poDeliveryNote1 = String.Empty
            Return _poDeliveryNote1
        End Get
        Set(ByVal value As String)
            _poDeliveryNote1 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote2")> Public Property PoDeliveryNote2() As String
        Get
            If _poDeliveryNote2 Is Nothing Then _poDeliveryNote2 = String.Empty
            Return _poDeliveryNote2
        End Get
        Set(ByVal value As String)
            _poDeliveryNote2 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote3")> Public Property PoDeliveryNote3() As String
        Get
            If _poDeliveryNote3 Is Nothing Then _poDeliveryNote3 = String.Empty
            Return _poDeliveryNote3
        End Get
        Set(ByVal value As String)
            _poDeliveryNote3 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote4")> Public Property PoDeliveryNote4() As String
        Get
            If _poDeliveryNote4 Is Nothing Then _poDeliveryNote4 = String.Empty
            Return _poDeliveryNote4
        End Get
        Set(ByVal value As String)
            _poDeliveryNote4 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote5")> Public Property PoDeliveryNote5() As String
        Get
            If _poDeliveryNote5 Is Nothing Then _poDeliveryNote5 = String.Empty
            Return _poDeliveryNote5
        End Get
        Set(ByVal value As String)
            _poDeliveryNote5 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote6")> Public Property PoDeliveryNote6() As String
        Get
            If _poDeliveryNote6 Is Nothing Then _poDeliveryNote6 = String.Empty
            Return _poDeliveryNote6
        End Get
        Set(ByVal value As String)
            _poDeliveryNote6 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote7")> Public Property PoDeliveryNote7() As String
        Get
            If _poDeliveryNote7 Is Nothing Then _poDeliveryNote7 = String.Empty
            Return _poDeliveryNote7
        End Get
        Set(ByVal value As String)
            _poDeliveryNote7 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote8")> Public Property PoDeliveryNote8() As String
        Get
            If _poDeliveryNote8 Is Nothing Then _poDeliveryNote8 = String.Empty
            Return _poDeliveryNote8
        End Get
        Set(ByVal value As String)
            _poDeliveryNote8 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote9")> Public Property PoDeliveryNote9() As String
        Get
            If _poDeliveryNote9 Is Nothing Then _poDeliveryNote9 = String.Empty
            Return _poDeliveryNote9
        End Get
        Set(ByVal value As String)
            _poDeliveryNote9 = value
        End Set
    End Property

    Public ReadOnly Property PoDeliveryNoteString() As String
        Get
            If _poDeliveryNote1.Trim.Length = 0 Then
                Return String.Empty
            Else
                Dim sb As New StringBuilder(_poDeliveryNote1.Trim)
                If _poDeliveryNote2.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote2.Trim)
                If _poDeliveryNote3.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote3.Trim)
                If _poDeliveryNote4.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote4.Trim)
                If _poDeliveryNote5.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote5.Trim)
                If _poDeliveryNote6.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote6.Trim)
                If _poDeliveryNote7.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote7.Trim)
                If _poDeliveryNote8.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote8.Trim)
                If _poDeliveryNote9.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote9.Trim)
                Return sb.ToString
            End If
        End Get
    End Property
    Public ReadOnly Property Lines() As ReceiptLineCollection
        Get
            If _lines Is Nothing Then _lines = New ReceiptLineCollection(Me)
            Return _lines
        End Get
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Sub New(ByVal order As Order, ByVal userId As Integer)
        MyBase.New()
        _poNumber = order.PoNumber
        _poId = order.Id
        _poConsignNumber = order.ConsignNumber
        _poSupplierNumber = order.SupplierNumber
        _poSupplierName = order.SupplierName
        _poSupplierBbc = order.IsSupplierBbc
        _poOrderDate = order.DateCreated
        _poSoqNumber = order.SoqNumber
        _dateReceipt = Now.Date
        _employeeId = userId
        _lines = New ReceiptLineCollection(Me, order)
    End Sub

#End Region

    Public Sub Update(ByVal userId As Integer, ByRef receiptTrackable As ReceiptTrackable)

        UpdateReceiptPropertiesOnly(receiptTrackable)
        _lines = New ReceiptLineCollection(Me, receiptTrackable)
        Update(userId)

    End Sub

    Private Sub UpdateReceiptPropertiesOnly(ByRef receiptTrackable As ReceiptTrackable)

        Dim propInfos = Me.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance).Where(Function(prop) Attribute.IsDefined(prop, GetType(ColumnMappingAttribute)))
        Dim sourcePropeties = receiptTrackable.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)

        For Each propInfo As PropertyInfo In propInfos
            Dim propInfoName = propInfo.Name
            Dim sourcePropety = sourcePropeties.FirstOrDefault(Function(prop) prop.Name = propInfoName)
            If sourcePropety IsNot Nothing AndAlso sourcePropety.CanWrite Then
                Dim sourcePropetyValue = sourcePropety.GetValue(receiptTrackable, Nothing)
                propInfo.SetValue(Me, sourcePropetyValue, Nothing)
            End If
        Next

    End Sub

    Public Sub Update(ByVal userId As Integer)

        _value = Lines.ReceivedValue()
        DataOperations.ReceiptMaintain(Me, userId)

    End Sub


    Public Sub Insert(ByRef receiptTrackable As ReceiptTrackable)

        UpdateReceiptPropertiesOnly(receiptTrackable)
        _lines = New ReceiptLineCollection(Me, receiptTrackable)
        _number = DataOperations.ReceiptInsert(Me)
    End Sub

    Public Sub Insert()

        _value = Lines.ReceivedValue()
        _number = DataOperations.ReceiptInsert(Me)

    End Sub


    Public Shared Function GetMaintablePurchaseReceipts() As BindingList(Of Receipt)

        Dim receipts As New BindingList(Of Receipt)

        Dim dt As DataTable = DataOperations.ReceiptGetMaintainableOrders
        For Each dr As DataRow In dt.Rows
            receipts.Add(New Receipt(dr))
        Next

        Return receipts

    End Function

End Class
