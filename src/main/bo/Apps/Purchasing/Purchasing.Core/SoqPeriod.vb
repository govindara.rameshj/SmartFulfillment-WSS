﻿<CLSCompliant(True)> Public Class SoqPeriod
    Public Index As Integer = 0
    Public Selected As Boolean = False
    Public Sales As Integer = 0
    Public Flier As String = String.Empty
    Public OutOfStock As Boolean = False
    Public Adjuster As Decimal = 1

    Public Sub New(ByVal PeriodIndex As Integer, ByVal PeriodSales As Integer, ByVal PeriodFlier As String, ByVal DaysOutStock As Integer)
        Selected = True
        Index = PeriodIndex - 1
        Sales = Math.Max(0, PeriodSales)
        Flier = PeriodFlier
        OutOfStock = (DaysOutStock > 0)
    End Sub

    Public Function WeekNumber() As Integer

        Return CInt(Math.Floor(Now.DayOfYear / 7)) - Index Mod 53

    End Function

    Public Function AdjustedSales() As Decimal
        Return Sales / Adjuster
    End Function

End Class