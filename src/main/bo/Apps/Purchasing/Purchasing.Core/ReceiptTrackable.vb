﻿Imports System.Reflection
Imports Oasys.Core
Imports System.ComponentModel
Imports BLToolkit.EditableObjects
Imports System.Collections.Specialized

Public Class ReceiptTrackable
    Inherits GenericTrackable

#Region "Fields"

    Private receiptEditable As ReceiptEditable
    Private linesEditable As EditableArrayList

#End Region

#Region "Properites"
    Public Property PoSupplierNumber() As String
        Get
            Return receiptEditable.PoSupplierNumber
        End Get
        Private Set(ByVal value As String)
            receiptEditable.PoSupplierNumber = value
        End Set
    End Property

    Public Property PoNumber() As Nullable(Of Integer)
        Get
            Return receiptEditable.PoNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            receiptEditable.PoNumber = value
        End Set
    End Property

    Public Property PoSupplierName() As String
        Get
            Return receiptEditable.PoSupplierName
        End Get
        Private Set(ByVal value As String)
            receiptEditable.PoSupplierName = value
        End Set
    End Property

    Public Property Number() As String
        Get
            Return receiptEditable.Number
        End Get
        Private Set(ByVal value As String)
            receiptEditable.Number = value
        End Set
    End Property

    Public Property Comments() As String
        Get
            Return receiptEditable.Comments
        End Get
        Set(ByVal value As String)
            receiptEditable.Comments = value
        End Set
    End Property

    Public Property PoConsignNumber() As Nullable(Of Integer)
        Get
            Return receiptEditable.PoConsignNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            receiptEditable.PoConsignNumber = value
        End Set
    End Property

    Public Property PoOrderDate() As DateTime
        Get
            Return receiptEditable.PoOrderDate
        End Get
        Private Set(ByVal value As DateTime)
            receiptEditable.PoOrderDate = value
        End Set
    End Property

    Public Property DateReceipt() As DateTime
        Get
            Return receiptEditable.DateReceipt
        End Get
        Set(ByVal value As DateTime)
            receiptEditable.DateReceipt = value
        End Set
    End Property

    Public Property PoDeliveryNote1() As String
        Get
            Return receiptEditable.PoDeliveryNote1
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote1 = value
        End Set
    End Property

    Public Property PoDeliveryNote2() As String
        Get
            Return receiptEditable.PoDeliveryNote2
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote2 = value
        End Set
    End Property

    Public Property PoDeliveryNote3() As String
        Get
            Return receiptEditable.PoDeliveryNote3
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote3 = value
        End Set
    End Property

    Public Property PoDeliveryNote4() As String
        Get
            Return receiptEditable.PoDeliveryNote4
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote4 = value
        End Set
    End Property

    Public Property PoDeliveryNote5() As String
        Get
            Return receiptEditable.PoDeliveryNote5
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote5 = value
        End Set
    End Property

    Public Property PoDeliveryNote6() As String
        Get
            Return receiptEditable.PoDeliveryNote6
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote6 = value
        End Set
    End Property

    Public Property PoDeliveryNote7() As String
        Get
            Return receiptEditable.PoDeliveryNote7
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote7 = value
        End Set
    End Property

    Public Property PoDeliveryNote8() As String
        Get
            Return receiptEditable.PoDeliveryNote8
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote8 = value
        End Set
    End Property

    Public Property PoDeliveryNote9() As String
        Get
            Return receiptEditable.PoDeliveryNote9
        End Get
        Set(ByVal value As String)
            receiptEditable.PoDeliveryNote9 = value
        End Set
    End Property

    Public ReadOnly Property PoDeliveryNoteString() As String
        Get
            If PoDeliveryNote1 Is Nothing Then
                Return String.Empty
            ElseIf PoDeliveryNote1.Trim.Length = 0 Then
                Return String.Empty
            Else
                Dim sb As New StringBuilder(PoDeliveryNote1.Trim)
                If PoDeliveryNote2.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote2.Trim)
                If PoDeliveryNote3.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote3.Trim)
                If PoDeliveryNote4.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote4.Trim)
                If PoDeliveryNote5.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote5.Trim)
                If PoDeliveryNote6.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote6.Trim)
                If PoDeliveryNote7.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote7.Trim)
                If PoDeliveryNote8.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote8.Trim)
                If PoDeliveryNote9.Trim.Length > 0 Then sb.Append(", " & PoDeliveryNote9.Trim)
                Return sb.ToString
            End If
        End Get
    End Property

    Public ReadOnly Property Lines() As EditableArrayList
        Get
            Return linesEditable
        End Get
    End Property

#End Region

#Region "Constructors"
    Public Sub New(ByVal receipt As Receipt)
        MyBase.New()

        receiptEditable = CreateReceiptEditable(receipt)

        linesEditable = New EditableArrayList(GetType(ReceiptLineEditable))
        For Each line In receipt.Lines
            Dim lineEditable = CreateReceiptLineEditable(line)
            AddHandler lineEditable.PropertyChanged, AddressOf PropertyChanged
            linesEditable.Add(lineEditable)
        Next

        receiptEditable.AcceptChanges()
        linesEditable.AcceptChanges()

        AddHandler Me.receiptEditable.PropertyChanged, AddressOf PropertyChanged
        AddHandler Me.linesEditable.CollectionChanged, AddressOf LinesChanged
    End Sub
#End Region

#Region "Public Methods"

    ''' <summary>
    ''' Calculates receipt order/received qtys and values
    ''' </summary>
    ''' <param name="orderQty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub LinesTotal(ByRef orderQty As Integer, ByRef orderValue As Decimal, ByRef receivedQty As Integer, ByRef receivedValue As Decimal)

        orderQty = 0
        orderValue = 0
        receivedQty = 0
        receivedValue = 0

        For Each line As ReceiptLineEditable In Lines
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.OrderPrice)
            receivedQty += line.ReceivedQty
            receivedValue += (line.ReceivedQty * line.ReceivedPrice)
        Next

    End Sub

    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsLineStockExist(ByVal skuNumber As String) As Boolean
        Dim result = False
        For Each line As ReceiptLineEditable In Lines
            If (line.SkuNumber = skuNumber) Then
                result = True
                Exit For
            End If
        Next
        Return result
    End Function

    ''' <summary>
    ''' Clears all received qtys for all lines in collection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearReceiveQty()
        For Each line As ReceiptLineEditable In Me.Lines
            line.ReceivedQty = 0
        Next
    End Sub

#End Region

#Region "Private Methods"

    Private Function CreateReceiptEditable(ByVal source As Receipt) As ReceiptEditable
        Dim receiptEtitable = receiptEditable.CreateInstance()

        Dim sourceProperties As IEnumerable(Of PropertyInfo) = source.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance).Where(Function(prop) Attribute.IsDefined(prop, GetType(ColumnMappingAttribute)))
        Dim targetPropeties As IEnumerable(Of PropertyInfo) = receiptEtitable.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)

        For Each sourceProperty As PropertyInfo In sourceProperties
            Dim sourcePropertyName = sourceProperty.Name
            Dim targetProperty = targetPropeties.FirstOrDefault(Function(prop) prop.Name = sourcePropertyName)
            If targetProperty IsNot Nothing AndAlso targetProperty.CanWrite Then
                Dim sourcePropertyValue = sourceProperty.GetValue(source, Nothing)
                If (sourceProperty.PropertyType Is GetType(String)) Then
                    If sourcePropertyValue IsNot Nothing Then
                        sourcePropertyValue = sourcePropertyValue.ToString().Trim()
                    Else
                        sourcePropertyValue = String.Empty
                    End If
                End If

                targetProperty.SetValue(receiptEtitable, sourcePropertyValue, Nothing)
            End If
        Next

        CreateReceiptEditable = receiptEtitable
    End Function

    Private Function CreateReceiptLineEditable(ByVal source As ReceiptLine) As ReceiptLineEditable
        Dim receiptEtitable = ReceiptLineEditable.CreateInstance()

        Dim sourceProperties As IEnumerable(Of PropertyInfo) = source.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)
        Dim targetPropeties As IEnumerable(Of PropertyInfo) = receiptEtitable.GetType.GetProperties(BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)

        For Each sourceProperty As PropertyInfo In sourceProperties
            Dim sourcePropertyName = sourceProperty.Name
            Dim targetProperty = targetPropeties.FirstOrDefault(Function(prop) prop.Name = sourcePropertyName)
            If targetProperty IsNot Nothing AndAlso targetProperty.CanWrite Then
                Dim sourcePropertyValue = sourceProperty.GetValue(source, Nothing)
                If (sourceProperty.PropertyType Is GetType(String) AndAlso sourcePropertyValue IsNot Nothing) Then
                    sourcePropertyValue = sourcePropertyValue.ToString().Trim()
                End If

                targetProperty.SetValue(receiptEtitable, sourcePropertyValue, Nothing)
            End If
        Next

        CreateReceiptLineEditable = receiptEtitable
    End Function

    Private Sub PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        OnDataChanged(HasChangesInReceipt() OrElse HasChangesInLines())
    End Sub

    Private Sub LinesChanged(sender As Object, e As EventArgs)
        OnDataChanged(HasChangesInReceipt() OrElse HasChangesInLines())
    End Sub

    Private Function HasChangesInReceipt() As Boolean
        HasChangesInReceipt = receiptEditable.IsDirty
    End Function

    Private Function HasChangesInLines() As Boolean
        Dim result = linesEditable.IsDirty
        If (result = False) Then
            For Each line As ReceiptLineEditable In Lines
                If (line.IsDirty) Then
                    result = True
                    Exit For
                End If
            Next
        End If
        HasChangesInLines = result
    End Function

#End Region

End Class
