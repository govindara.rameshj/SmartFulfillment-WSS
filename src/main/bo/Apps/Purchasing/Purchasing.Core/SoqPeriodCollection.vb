﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class SoqPeriodCollection
    Inherits BindingList(Of SoqPeriod)

    Friend Sub New()
        MyBase.New()
    End Sub


    Friend Function AverageSales() As Decimal

        If Me.Items.Count = 0 Then Return 0

        Dim total As Integer = 0
        For Each period As SoqPeriod In Me.Items
            If period.Sales > 0 Then total += period.Sales
        Next
        Return CDec(total / Me.Items.Count)

    End Function

    Friend Function AverageIndeces() As Decimal

        If Me.Items.Count = 0 Then Return 0

        Dim total As Integer = 0
        For Each period As SoqPeriod In Me.Items
            total += period.Index
        Next
        Return CDec(total / Me.Items.Count)

    End Function

    Friend Function AverageSalesAdjusted() As Decimal

        If Me.Items.Count = 0 Then Return 0

        Dim total As Decimal = 0
        For Each period As SoqPeriod In Me.Items
            If period.Sales > 0 Then total += period.AdjustedSales
        Next
        Return total / Me.Items.Count

    End Function

    Friend Function CountZeroSales() As Integer

        Dim count As Integer = 0
        For Each period As SoqPeriod In Me.Items.Where(Function(p) p.Sales = 0)
            count += 1
        Next
        Return count

    End Function

    Friend Function CountZeroSalesConsecutive() As Integer

        Dim count As Integer = 0
        For Each period As SoqPeriod In Me.Items
            If period.Sales = 0 Then
                count += 1
            Else
                Exit For
            End If
        Next
        Return count

    End Function


    Friend Function StdDevSales() As Decimal

        If Me.Items.Count <= 1 Then Return 0
        Dim sumR As Double = 0
        Dim sumRR As Double = 0

        For Each period As SoqPeriod In Me.Items
            sumR += period.AdjustedSales
            sumRR += Math.Pow(period.AdjustedSales, 2)
        Next

        sumR = Math.Pow(sumR, 2)
        sumR /= Me.Items.Count

        Dim stdDev As Double = sumRR - sumR
        stdDev /= (Me.Items.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function

    Friend Function StdDevPeriods() As Decimal

        If Me.Items.Count <= 1 Then Return 0
        Dim sumP As Double = 0
        Dim sumPP As Double = 0

        For Each period As SoqPeriod In Me.Items
            sumP += period.Index
            sumPP += Math.Pow(period.Index, 2)
        Next

        Dim stdDev As Double = sumPP - (Math.Pow(sumP, 2) / Me.Items.Count)
        stdDev /= (Me.Items.Count - 1)
        Return CDec(Math.Sqrt(stdDev))

    End Function

    Friend Function CorrelationCoeff(ByVal StdDevPeriodSales As Decimal, ByVal StdDevPeriodPeriods As Decimal) As Decimal

        If Me.Items.Count <= 1 Then Return 0
        If StdDevPeriodSales * StdDevPeriodPeriods * (Me.Items.Count - 1) = 0 Then Return 0

        Dim sumR As Double = 0
        Dim sumP As Double = 0
        Dim sumRP As Double = 0

        For Each period As SoqPeriod In Me.Items
            sumR += period.AdjustedSales
            sumP += period.Index
            sumRP += (period.Index * period.Sales)
        Next

        Dim stdDev As Double = sumRP - (sumR * sumP / Me.Items.Count)
        stdDev /= (StdDevPeriodSales * StdDevPeriodPeriods * (Me.Items.Count - 1))
        Return CDec(Math.Min(stdDev, 1))

    End Function

    Friend Sub RemoveOutOfStockLessThanAverage(ByVal average As Decimal)

        For index As Integer = Me.Items.Count - 1 To 0 Step -1
            If Me.Item(index).OutOfStock AndAlso Me.Item(index).Sales < average Then
                Me.Items.RemoveAt(index)
            End If
        Next

    End Sub


End Class