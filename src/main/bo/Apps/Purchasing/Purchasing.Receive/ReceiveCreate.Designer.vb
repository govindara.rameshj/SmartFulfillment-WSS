<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReceiveCreate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblValueReceived = New System.Windows.Forms.Label()
        Me.lblValueOrdered = New System.Windows.Forms.Label()
        Me.lblUnitsReceived = New System.Windows.Forms.Label()
        Me.lblUnitsOrdered = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblPoNumber = New System.Windows.Forms.Label()
        Me.lblDateOrder = New System.Windows.Forms.Label()
        Me.lblSoqNumber = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblConsign = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.lblSupplier = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.uxLinesGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxLinesView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lblDeliveryNotes = New System.Windows.Forms.Label()
        Me.uxDeliveryNotesButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxUpdateButton = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTelephone = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblDateDue = New System.Windows.Forms.Label()
        Me.uxAddItemButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxClearAllButton = New DevExpress.XtraEditors.SimpleButton()
        Me.lblDateReceived = New System.Windows.Forms.Label()
        CType(Me.uxLinesGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxLinesView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblValueReceived
        '
        Me.lblValueReceived.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblValueReceived.Location = New System.Drawing.Point(804, 29)
        Me.lblValueReceived.Name = "lblValueReceived"
        Me.lblValueReceived.Size = New System.Drawing.Size(70, 20)
        Me.lblValueReceived.TabIndex = 25
        Me.lblValueReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblValueOrdered
        '
        Me.lblValueOrdered.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblValueOrdered.Location = New System.Drawing.Point(880, 29)
        Me.lblValueOrdered.Name = "lblValueOrdered"
        Me.lblValueOrdered.Size = New System.Drawing.Size(56, 20)
        Me.lblValueOrdered.TabIndex = 26
        Me.lblValueOrdered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUnitsReceived
        '
        Me.lblUnitsReceived.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUnitsReceived.Location = New System.Drawing.Point(804, 9)
        Me.lblUnitsReceived.Name = "lblUnitsReceived"
        Me.lblUnitsReceived.Size = New System.Drawing.Size(70, 20)
        Me.lblUnitsReceived.TabIndex = 22
        Me.lblUnitsReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUnitsOrdered
        '
        Me.lblUnitsOrdered.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUnitsOrdered.Location = New System.Drawing.Point(880, 9)
        Me.lblUnitsOrdered.Name = "lblUnitsOrdered"
        Me.lblUnitsOrdered.Size = New System.Drawing.Size(56, 20)
        Me.lblUnitsOrdered.TabIndex = 23
        Me.lblUnitsOrdered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(625, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(173, 20)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Value Received / Ordered"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(625, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(173, 20)
        Me.Label14.TabIndex = 21
        Me.Label14.Text = "Units Received / Ordered"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(7, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 20)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "PO Number"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(311, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(98, 20)
        Me.Label18.TabIndex = 14
        Me.Label18.Text = "&Received Date"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPoNumber
        '
        Me.lblPoNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPoNumber.Location = New System.Drawing.Point(95, 9)
        Me.lblPoNumber.Name = "lblPoNumber"
        Me.lblPoNumber.Size = New System.Drawing.Size(210, 20)
        Me.lblPoNumber.TabIndex = 1
        Me.lblPoNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateOrder
        '
        Me.lblDateOrder.Location = New System.Drawing.Point(415, 9)
        Me.lblDateOrder.Name = "lblDateOrder"
        Me.lblDateOrder.Size = New System.Drawing.Size(181, 18)
        Me.lblDateOrder.TabIndex = 11
        Me.lblDateOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSoqNumber
        '
        Me.lblSoqNumber.Location = New System.Drawing.Point(95, 68)
        Me.lblSoqNumber.Name = "lblSoqNumber"
        Me.lblSoqNumber.Size = New System.Drawing.Size(210, 20)
        Me.lblSoqNumber.TabIndex = 7
        Me.lblSoqNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(311, 9)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(98, 20)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Order Date"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(7, 68)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(82, 20)
        Me.Label24.TabIndex = 6
        Me.Label24.Text = "SOQ Number"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblConsign
        '
        Me.lblConsign.Location = New System.Drawing.Point(95, 88)
        Me.lblConsign.Name = "lblConsign"
        Me.lblConsign.Size = New System.Drawing.Size(210, 20)
        Me.lblConsign.TabIndex = 9
        Me.lblConsign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Consignment"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(311, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Delivery Notes"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(311, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 20)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "&Comment"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComment
        '
        Me.txtComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComment.Location = New System.Drawing.Point(415, 88)
        Me.txtComment.MaxLength = 20
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(181, 20)
        Me.txtComment.TabIndex = 19
        '
        'lblSupplier
        '
        Me.lblSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupplier.Location = New System.Drawing.Point(95, 29)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(210, 20)
        Me.lblSupplier.TabIndex = 3
        Me.lblSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(82, 20)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Supplier"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxLinesGrid
        '
        Me.uxLinesGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxLinesGrid.Location = New System.Drawing.Point(10, 114)
        Me.uxLinesGrid.MainView = Me.uxLinesView
        Me.uxLinesGrid.Name = "uxLinesGrid"
        Me.uxLinesGrid.Size = New System.Drawing.Size(926, 364)
        Me.uxLinesGrid.TabIndex = 27
        Me.uxLinesGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxLinesView, Me.GridView2})
        '
        'uxLinesView
        '
        Me.uxLinesView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxLinesView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxLinesView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxLinesView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxLinesView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxLinesView.GridControl = Me.uxLinesGrid
        Me.uxLinesView.Name = "uxLinesView"
        Me.uxLinesView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.uxLinesGrid
        Me.GridView2.Name = "GridView2"
        '
        'lblDeliveryNotes
        '
        Me.lblDeliveryNotes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeliveryNotes.Location = New System.Drawing.Point(415, 69)
        Me.lblDeliveryNotes.Name = "lblDeliveryNotes"
        Me.lblDeliveryNotes.Size = New System.Drawing.Size(426, 18)
        Me.lblDeliveryNotes.TabIndex = 17
        Me.lblDeliveryNotes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxDeliveryNotesButton
        '
        Me.uxDeliveryNotesButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryNotesButton.Appearance.Options.UseTextOptions = True
        Me.uxDeliveryNotesButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDeliveryNotesButton.Location = New System.Drawing.Point(847, 69)
        Me.uxDeliveryNotesButton.Name = "uxDeliveryNotesButton"
        Me.uxDeliveryNotesButton.Size = New System.Drawing.Size(89, 39)
        Me.uxDeliveryNotesButton.TabIndex = 20
        Me.uxDeliveryNotesButton.Text = "F4 Maintain Delivery Notes"
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.Location = New System.Drawing.Point(859, 484)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(77, 39)
        Me.uxCancelButton.TabIndex = 29
        Me.uxCancelButton.Text = "F10 Cancel"
        '
        'uxUpdateButton
        '
        Me.uxUpdateButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxUpdateButton.Enabled = False
        Me.uxUpdateButton.Location = New System.Drawing.Point(176, 484)
        Me.uxUpdateButton.Name = "uxUpdateButton"
        Me.uxUpdateButton.Size = New System.Drawing.Size(77, 39)
        Me.uxUpdateButton.TabIndex = 28
        Me.uxUpdateButton.Text = "F5 Update"
        '
        'lblTelephone
        '
        Me.lblTelephone.Location = New System.Drawing.Point(95, 49)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(210, 20)
        Me.lblTelephone.TabIndex = 5
        Me.lblTelephone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 20)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Telephone"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(311, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 20)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Due Date"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateDue
        '
        Me.lblDateDue.Location = New System.Drawing.Point(415, 29)
        Me.lblDateDue.Name = "lblDateDue"
        Me.lblDateDue.Size = New System.Drawing.Size(181, 18)
        Me.lblDateDue.TabIndex = 13
        Me.lblDateDue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxAddItemButton
        '
        Me.uxAddItemButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxAddItemButton.Appearance.Options.UseTextOptions = True
        Me.uxAddItemButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAddItemButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAddItemButton.Location = New System.Drawing.Point(10, 484)
        Me.uxAddItemButton.Name = "uxAddItemButton"
        Me.uxAddItemButton.Size = New System.Drawing.Size(77, 39)
        Me.uxAddItemButton.TabIndex = 30
        Me.uxAddItemButton.Text = "F2 Add Item"
        '
        'uxClearAllButton
        '
        Me.uxClearAllButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxClearAllButton.Location = New System.Drawing.Point(93, 484)
        Me.uxClearAllButton.Name = "uxClearAllButton"
        Me.uxClearAllButton.Size = New System.Drawing.Size(77, 39)
        Me.uxClearAllButton.TabIndex = 31
        Me.uxClearAllButton.Text = "F3 Clear All"
        '
        'lblDateReceived
        '
        Me.lblDateReceived.Location = New System.Drawing.Point(415, 49)
        Me.lblDateReceived.Name = "lblDateReceived"
        Me.lblDateReceived.Size = New System.Drawing.Size(181, 18)
        Me.lblDateReceived.TabIndex = 32
        Me.lblDateReceived.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ReceiveCreate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 535)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblDateReceived)
        Me.Controls.Add(Me.uxClearAllButton)
        Me.Controls.Add(Me.uxAddItemButton)
        Me.Controls.Add(Me.lblDateDue)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblTelephone)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.uxUpdateButton)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxDeliveryNotesButton)
        Me.Controls.Add(Me.lblDeliveryNotes)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.lblConsign)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblValueReceived)
        Me.Controls.Add(Me.lblValueOrdered)
        Me.Controls.Add(Me.lblUnitsReceived)
        Me.Controls.Add(Me.lblUnitsOrdered)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.lblPoNumber)
        Me.Controls.Add(Me.lblDateOrder)
        Me.Controls.Add(Me.lblSoqNumber)
        Me.Controls.Add(Me.lblSupplier)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.uxLinesGrid)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(956, 543)
        Me.Name = "ReceiveCreate"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Receive Order"
        CType(Me.uxLinesGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxLinesView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblValueReceived As System.Windows.Forms.Label
    Friend WithEvents lblValueOrdered As System.Windows.Forms.Label
    Friend WithEvents lblUnitsReceived As System.Windows.Forms.Label
    Friend WithEvents lblUnitsOrdered As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblPoNumber As System.Windows.Forms.Label
    Friend WithEvents lblDateOrder As System.Windows.Forms.Label
    Friend WithEvents lblSoqNumber As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblConsign As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents lblSupplier As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents uxLinesGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxLinesView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lblDeliveryNotes As System.Windows.Forms.Label
    Friend WithEvents uxDeliveryNotesButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxUpdateButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTelephone As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblDateDue As System.Windows.Forms.Label
    Friend WithEvents uxAddItemButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxClearAllButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblDateReceived As System.Windows.Forms.Label
End Class
