Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Stock
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors.Repository
Imports System.ComponentModel
Imports Purchasing.Core

Public Class ReceiveCreate
    Private _linkToReceipt As Receipt
    Private _order As Order
    Private _supplier As Supplier
    Private _receiptTrackable As ReceiptTrackable
    Private _prevRowHandle As Integer = GridControl.InvalidRowHandle
    Private _columnOrders(6) As String

    Sub New(ByRef order As Order, ByVal userId As Integer)
        InitializeComponent()
        _order = order
        _linkToReceipt = New Receipt(order, userId)
        _supplier = Supplier.GetByNumber(order.SupplierNumber)
        _receiptTrackable = New ReceiptTrackable(_linkToReceipt)

        _columnOrders(0) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)
        _columnOrders(1) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuDescription)
        _columnOrders(2) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuProductCode)
        _columnOrders(3) = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuPack)
        _columnOrders(4) = GetPropertyName(Function(f As ReceiptLineEditable) f.OrderPrice)
        _columnOrders(5) = GetPropertyName(Function(f As ReceiptLineEditable) f.OrderQty)
        _columnOrders(6) = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
    End Sub

    Private Sub FormKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F10
                uxLinesView.CloseEditor()
                uxLinesView.CancelUpdateCurrentRow()
                uxCancelButton.PerformClick()
            Case Keys.F5 : uxUpdateButton.PerformClick()
            Case Keys.F4 : uxDeliveryNotesButton.PerformClick()
            Case Keys.F2 : uxAddItemButton.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub FormLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor
            PopulateLabels()
            PopulateTotals()
            PopulateGrid()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub PopulateLabels()

        lblPoNumber.Text = CStr(_order.PoNumber)
        lblSupplier.Text = _order.SupplierNumber & Space(1) & _order.SupplierName
        lblTelephone.Text = _supplier.PhoneNumber1
        If _order.SoqNumber.HasValue Then lblSoqNumber.Text = CStr(_order.SoqNumber.Value)



        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 23/09/2010
        ' Referral No : 411
        ' Notes       : Consignment number can be "null" or "zero"
        '               Check for "zero" not being done
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'if consigned then load consignment and delivery notes for it
        If _order.ConsignNumber.HasValue Then
            '"zero" check
            If _order.ConsignNumber.Value <> 0 Then

                lblConsign.Text = CStr(_order.ConsignNumber.Value)
                Dim consign As Consignment = Purchasing.Core.Consignment.GetConsignment(CStr(_order.ConsignNumber.Value))
                _receiptTrackable.PoDeliveryNote1 = consign.DeliveryNote1
                _receiptTrackable.PoDeliveryNote2 = consign.DeliveryNote2
                _receiptTrackable.PoDeliveryNote3 = consign.DeliveryNote3
                _receiptTrackable.PoDeliveryNote4 = consign.DeliveryNote4
                _receiptTrackable.PoDeliveryNote5 = consign.DeliveryNote5
                _receiptTrackable.PoDeliveryNote6 = consign.DeliveryNote6
                _receiptTrackable.PoDeliveryNote7 = consign.DeliveryNote7
                _receiptTrackable.PoDeliveryNote8 = consign.DeliveryNote8
                _receiptTrackable.PoDeliveryNote9 = consign.DeliveryNote9

                lblDeliveryNotes.Text = _receiptTrackable.PoDeliveryNoteString
                uxUpdateButton.Enabled = False

            End If

        End If

        lblDateOrder.Text = Format(_order.DateCreated, "dd/MM/yy dddd")
        lblDateDue.Text = Format(_order.DateDue, "dd/MM/yy dddd")
        lblDateReceived.Text = Format(_receiptTrackable.DateReceipt, "dd/MM/yy dddd")

        If _receiptTrackable.Comments Is Nothing Then
            txtComment.Text = String.Empty
        Else
            txtComment.Text = _receiptTrackable.Comments.Trim
        End If

    End Sub

    Private Sub TxtComment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComment.TextChanged
        Dim tb As TextBox = CType(sender, TextBox)
        _receiptTrackable.Comments = tb.Text
    End Sub

    Private Sub PopulateTotals()

        Dim orderQty As Integer
        Dim orderValue As Decimal
        Dim receivedQty As Integer
        Dim receivedValue As Decimal

        _receiptTrackable.LinesTotal(orderQty, orderValue, receivedQty, receivedValue)

        lblUnitsOrdered.Text = orderQty.ToString("n0")
        lblValueOrdered.Text = orderValue.ToString("c2")
        lblUnitsReceived.Text = receivedQty.ToString("n0")
        lblValueReceived.Text = receivedValue.ToString("c2")

    End Sub

    Private Sub PopulateGrid()

        uxLinesGrid.DataSource = _receiptTrackable.Lines

        'set up column widths
        For Each col As GridColumn In uxLinesView.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)
                    col.Caption = My.Resources.ColumnHeaders.SkuNumber
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuDescription)
                    col.Caption = My.Resources.ColumnHeaders.Description
                    col.OptionsColumn.AllowFocus = False
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuProductCode)
                    col.Caption = My.Resources.ColumnHeaders.ProductCode
                    col.OptionsColumn.AllowFocus = False
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.SkuPack)
                    col.Caption = My.Resources.ColumnHeaders.Pack
                    col.OptionsColumn.AllowFocus = False
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.OrderQty)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "#0"
                    col.OptionsColumn.AllowFocus = False
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "#0"
                Case GetPropertyName(Function(f As ReceiptLineEditable) f.OrderPrice)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = ChrW(163) & "#0.00"
                    col.OptionsColumn.AllowFocus = False
                Case Else : col.Visible = False
            End Select
        Next

        ' See details in https://www.devexpress.com/Support/Center/Question/Details/Q294902
        For counter = 0 To _columnOrders.Length - 1 Step 1
            SetColumnOrder(_columnOrders(counter), counter)
        Next

        uxLinesView.BestFitColumns()
        uxLinesView.Focus()

        AddHandler _receiptTrackable.DataChanged, AddressOf ReceiptChanged
    End Sub

    Private Sub SetColumnOrder(ByVal columnName As String, ByVal visibleIndex As Integer)
        Dim col As DevExpress.XtraGrid.Columns.GridColumn = uxLinesView.Columns.ColumnByFieldName(columnName)
        col.VisibleIndex = visibleIndex
    End Sub

    Private Sub ReceiptChanged(ByVal sender As Object, ByVal e As DataChangedEventArgs)
        uxUpdateButton.Enabled = e.HasModifications
    End Sub

    Private Sub xviewLines_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles uxLinesView.ValidatingEditor

        'get view in question
        Dim view As GridView = CType(sender, GridView)

        'check if reset button has been pressed
        If Me.ActiveControl.Name = uxCancelButton.Name Then
            view.CancelUpdateCurrentRow()
            Exit Sub
        End If

        'if no value then cancel and exit
        If e.Value Is Nothing Then
            view.CancelUpdateCurrentRow()
            Exit Sub
        End If

        Dim receivedQty As String = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty)
        Dim sku As String = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber)

        Select Case view.FocusedColumn.FieldName
            Case sku
                Dim skuNumber As String = CStr(e.Value)
                If skuNumber.Length > 6 Then
                    e.ErrorText = My.Resources.Strings.NotValidSku
                    e.Valid = False
                    Exit Select
                Else
                    'pad out to 6 chars
                    skuNumber.PadLeft(6, "0"c)

                    'check that sku not already in receipt lines collection
                    If _receiptTrackable.IsLineStockExist(skuNumber) Then
                        e.ErrorText = My.Resources.Strings.ItemAlreadyOnOrder
                        e.Valid = False
                        Exit Select
                    End If

                    'find stock item in collection and populate columns (only for new item)
                    ' Referral 293
                    ' Alan Lewis
                    ' 10/11/2010
                    ' Stored Procedure StockGetStocksBySupplier (and hence the contents of StockCollection) has
                    ' been changed to include IsNonOrder flagged items, which is needed here. So need to use
                    ' a function that does not strip the 'non order's out.
                    Dim stockItem As Cts.Oasys.Core.Stock.Stock = _supplier.StocksIncludeNonOrder.Stock(skuNumber)
                    If stockItem Is Nothing Then
                        e.ErrorText = My.Resources.Strings.ItemNotForSupplier
                        e.Valid = False
                        Exit Select
                    Else
                        e.Value = stockItem.SkuNumber
                        Dim rl As ReceiptLineEditable = CType(view.GetRow(GridControl.NewItemRowHandle), ReceiptLineEditable)
                        rl.SkuDescription = stockItem.Description.Trim
                        rl.SkuProductCode = stockItem.ProductCode.Trim
                        rl.SkuPack = stockItem.PackSize
                        rl.OrderPrice = stockItem.Price
                        rl.ReceivedPrice = stockItem.Price
                        rl.Sequence = Nothing
                    End If

                End If

            Case receivedQty
                If Not IsDBNull(e.Value) Then
                    'validate qty entered
                    Dim intValue As Integer = CInt(e.Value)
                    If intValue < 0 Then
                        e.ErrorText = My.Resources.Strings.QtyCannotBeNegative
                        e.Valid = False
                    End If

                    view.SetRowCellValue(view.FocusedRowHandle, receivedQty, intValue)
                    view.UpdateCurrentRow()
                Else
                    e.Valid = False
                End If

        End Select

        'if an error exists then select all text for easy editing
        If e.Valid = False Then
            view.ActiveEditor.SelectAll()
        Else
            view.CloseEditor()
            If view.FocusedColumn.FieldName <> sku Then
                PopulateTotals()
            End If
        End If

    End Sub

    Private Sub xviewLines_ShowingEditor(ByVal sender As Object, ByVal e As CancelEventArgs) Handles uxLinesView.ShowingEditor

        Dim view As GridView = CType(sender, GridView)
        If view.FocusedRowHandle <> GridControl.NewItemRowHandle Then
            If view.FocusedColumn.FieldName = GetPropertyName(Function(f As ReceiptLineEditable) f.SkuNumber) Then
                e.Cancel = True
            End If
        Else
            If view.FocusedColumn.FieldName = GetPropertyName(Function(f As ReceiptLineEditable) f.ReceivedQty) Then
                Dim rl As ReceiptLineEditable = CType(view.GetRow(GridControl.NewItemRowHandle), ReceiptLineEditable)
                If rl Is Nothing Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub uxDeliveryNotesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeliveryNotesButton.Click

        Using delNotes As New DeliveryNotes.DeliveryNotes
            delNotes.DeliveryNote1 = _receiptTrackable.PoDeliveryNote1
            delNotes.DeliveryNote2 = _receiptTrackable.PoDeliveryNote2
            delNotes.DeliveryNote3 = _receiptTrackable.PoDeliveryNote3
            delNotes.DeliveryNote4 = _receiptTrackable.PoDeliveryNote4
            delNotes.DeliveryNote5 = _receiptTrackable.PoDeliveryNote5
            delNotes.DeliveryNote6 = _receiptTrackable.PoDeliveryNote6
            delNotes.DeliveryNote7 = _receiptTrackable.PoDeliveryNote7
            delNotes.DeliveryNote8 = _receiptTrackable.PoDeliveryNote8
            delNotes.DeliveryNote9 = _receiptTrackable.PoDeliveryNote9

            If delNotes.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                _receiptTrackable.PoDeliveryNote1 = delNotes.DeliveryNote1
                _receiptTrackable.PoDeliveryNote2 = delNotes.DeliveryNote2
                _receiptTrackable.PoDeliveryNote3 = delNotes.DeliveryNote3
                _receiptTrackable.PoDeliveryNote4 = delNotes.DeliveryNote4
                _receiptTrackable.PoDeliveryNote5 = delNotes.DeliveryNote5
                _receiptTrackable.PoDeliveryNote6 = delNotes.DeliveryNote6
                _receiptTrackable.PoDeliveryNote7 = delNotes.DeliveryNote7
                _receiptTrackable.PoDeliveryNote8 = delNotes.DeliveryNote8
                _receiptTrackable.PoDeliveryNote9 = delNotes.DeliveryNote9
            End If
        End Using

        lblDeliveryNotes.Text = _receiptTrackable.PoDeliveryNoteString

    End Sub

    Private Sub uxAddItemButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAddItemButton.Click

        Using lookup As New Stock.Enquiry.Enquiry(0, 0, 0, String.Empty)
            lookup.ShowAccept()
            lookup.SetSupplier(_receiptTrackable.PoSupplierNumber)

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Try
                        uxLinesView.FocusedRowHandle = GridControl.NewItemRowHandle
                        uxLinesView.FocusedColumn = uxLinesView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber))
                        uxLinesView.ShowEditor()
                        uxLinesView.ActiveEditor.EditValue = lookup.SkuNumbers(0)
                        uxLinesView.ValidateEditor()
                    Catch ex As DevExpress.Utils.HideException
                    End Try
                End If
            End Using

        End Using

    End Sub

    Private Sub uxClearAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxClearAllButton.Click
        _receiptTrackable.ClearReceiveQty()
        uxLinesGrid.RefreshDataSource()
    End Sub

    Private Sub uxUpdateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxUpdateButton.Click

        Try
            Cursor = Cursors.WaitCursor

            If (lblDeliveryNotes.Text.Trim.Length > 0) Then
                'add received price
                For Each line As ReceiptLineEditable In _receiptTrackable.Lines
                    Dim skuNumber As String = line.SkuNumber
                    ' Referral 293
                    ' Alan Lewis
                    ' 10/11/2010
                    ' Stored Procedure StockGetStocksBySupplier (and hence the contents of StockCollection) has
                    ' been changed to include IsNonOrder flagged items, which is needed here. So need to use
                    ' a function that does not strip the 'non order's out.
                    For Each s As Cts.Oasys.Core.Stock.Stock In _supplier.StocksIncludeNonOrder
                        If s.SkuNumber = skuNumber Then
                            line.ReceivedPrice = s.Price
                            Exit For
                        End If
                    Next
                Next

                _linkToReceipt.Insert(_receiptTrackable)

                MessageBox.Show(My.Resources.Strings.SavedOkAndDrl & _linkToReceipt.Number, Me.Text)

                DialogResult = Windows.Forms.DialogResult.OK
                FindForm.Close()
            Else
                MessageBox.Show(My.Resources.Strings.FirstDeliveryNoteIsMandatory, Me.Text)
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        FindForm.Close()
    End Sub

    Private Sub xviewLines_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles uxLinesView.KeyDown
        If (e.KeyCode = Keys.Escape) AndAlso (uxLinesView.ActiveEditor IsNot Nothing) Then
            uxLinesView.CancelUpdateCurrentRow()
            uxLinesView.FocusedRowHandle = _prevRowHandle
        End If
    End Sub

    Private Sub xviewLines_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles uxLinesView.FocusedRowChanged
        If (e.FocusedRowHandle = e.PrevFocusedRowHandle) Then
            _prevRowHandle = e.PrevFocusedRowHandle
        End If
    End Sub

    Private Sub xgridLines_Leave(sender As System.Object, e As System.EventArgs) Handles uxLinesGrid.Leave
        If uxLinesView.FocusedRowHandle = GridControl.NewItemRowHandle Then
            uxLinesView.CancelUpdateCurrentRow()
            uxLinesView.FocusedRowHandle = _prevRowHandle
        End If
    End Sub

    Private Sub ReceiveCreate_MouseClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseClick
        uxLinesView.ValidateEditor()
    End Sub
End Class