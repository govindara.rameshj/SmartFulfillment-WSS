Imports Cts.Oasys.Core
Imports System.ComponentModel
Imports Purchasing.Core
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid

Public Class Receive

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnReceive.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.Strings.GetOrders)
            LoadOrders()

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub


    Private Sub LoadOrders()

        'get orders to bind to grid
        Dim orders As BindingList(Of Order) = Order.GetOrdersOutstanding
        xgridOrders.DataSource = orders

        'set up column widths
        For Each col As GridColumn In xviewOrders.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Order) f.PoNumber) : col.Caption = "PO Number"
                Case GetPropertyName(Function(f As Order) f.ConsignNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierNumber)
                Case GetPropertyName(Function(f As Order) f.SupplierName)
                Case GetPropertyName(Function(f As Order) f.DateCreated) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.DateDue) : col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                Case GetPropertyName(Function(f As Order) f.Cartons) : col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Value)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = ChrW(163) & "#.00"
                    col.MinWidth = 60
                Case GetPropertyName(Function(f As Order) f.Units) : col.MinWidth = 60
                Case Else : col.Visible = False
            End Select
        Next

        LoadOrderLines(xgridOrders)

        xviewOrders.BestFitColumns()
        xviewOrders.Focus()

    End Sub

    Private Sub LoadOrderLines(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As Order) f.Lines), view)

        Dim id As String = GetPropertyName(Function(f As OrderLine) f.Id)
        view.Columns.AddField(id).VisibleIndex = 0
        view.Columns(id).MinWidth = 60
        view.Columns(id).MaxWidth = 60

        Dim skuNumber As String = GetPropertyName(Function(f As OrderLine) f.SkuNumber)
        view.Columns.AddField(skuNumber).VisibleIndex = 1
        view.Columns(skuNumber).MinWidth = 120
        view.Columns(skuNumber).MaxWidth = 120

        view.Columns.AddField(GetPropertyName(Function(f As OrderLine) f.SkuDescription)).VisibleIndex = 2

        Dim productCode As String = GetPropertyName(Function(f As OrderLine) f.SkuProductCode)
        view.Columns.AddField(productCode).VisibleIndex = 3
        view.Columns(productCode).MinWidth = 120
        view.Columns(productCode).MaxWidth = 120

        Dim price As String = GetPropertyName(Function(f As OrderLine) f.Price)
        view.Columns.AddField(price).VisibleIndex = 5
        view.Columns(price).MaxWidth = 80
        view.Columns(price).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        view.Columns(price).DisplayFormat.FormatString = ChrW(163) & "#.00"

        Dim orderQty As String = GetPropertyName(Function(f As OrderLine) f.OrderQty)
        view.Columns.AddField(orderQty).VisibleIndex = 6
        view.Columns(orderQty).MaxWidth = 80

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False

    End Sub


    Private Sub btnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceive.Click

        'get receipt from datasource
        Dim orders As BindingList(Of Order) = CType(xgridOrders.DataSource, Global.System.ComponentModel.BindingList(Of Global.Purchasing.Core.Order))
        Dim order As Order = orders(xviewOrders.GetDataSourceRowIndex(xviewOrders.GetSelectedRows(0)))

        Using orderForm As New ReceiveCreate(order, UserId)
            If orderForm.ShowDialog(Me) = DialogResult.OK Then
                orders.Remove(order)
            End If
        End Using

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.FindForm.Close()
    End Sub

End Class
