<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Receive
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Receive))
        Me.xgridOrders = New DevExpress.XtraGrid.GridControl
        Me.xviewOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnReceive = New DevExpress.XtraEditors.SimpleButton
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton
        CType(Me.xgridOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xgridOrders
        '
        Me.xgridOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridOrders.Location = New System.Drawing.Point(3, 3)
        Me.xgridOrders.MainView = Me.xviewOrders
        Me.xgridOrders.Name = "xgridOrders"
        Me.xgridOrders.Size = New System.Drawing.Size(894, 499)
        Me.xgridOrders.TabIndex = 0
        Me.xgridOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewOrders})
        '
        'xviewOrders
        '
        Me.xviewOrders.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewOrders.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewOrders.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewOrders.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewOrders.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewOrders.GridControl = Me.xgridOrders
        Me.xviewOrders.Name = "xviewOrders"
        Me.xviewOrders.OptionsBehavior.Editable = False
        Me.xviewOrders.OptionsDetail.ShowDetailTabs = False
        '
        'btnReceive
        '
        Me.btnReceive.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReceive.Location = New System.Drawing.Point(3, 508)
        Me.btnReceive.Name = "btnReceive"
        Me.btnReceive.Size = New System.Drawing.Size(75, 39)
        Me.btnReceive.TabIndex = 1
        Me.btnReceive.Text = "F5 Receive"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(822, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F12 Exit"
        '
        'Receive
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnReceive)
        Me.Controls.Add(Me.xgridOrders)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Receive"
        Me.Size = New System.Drawing.Size(900, 550)
        CType(Me.xgridOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xgridOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnReceive As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton

End Class
