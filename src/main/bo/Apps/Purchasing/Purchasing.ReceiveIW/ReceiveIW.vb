Public Class ReceiveIW

    Private Enum colIssue
        Number = 0
        Received
        [Date]
        PoNumber
        PoType
        Imported
        Value
        SupplierNumber
        SupplierName
        DrlNumber
        Deleted
        QtyOrder
        QtyIssue
        QtyFollow
        Status
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _Issues As BOPurchases.cIssueHeader

    Private _BusinessLockIW As IBusinessLockIW
    Private _LockID As Integer

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        btnDelete.Text = My.Resources.F6DeleteIssue
        btnDeletePos.Enabled = True
        dtpFrom.EditValue = Now.Date
        dtpTo.EditValue = Now.Date
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnReceive.PerformClick()
            Case Keys.F6 : btnDelete.PerformClick()
            Case Keys.F7 : btnImport.PerformClick()
            Case Keys.F8 : btnDeletePos.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub Form_Closed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs)

        RemoveLock()

        MyBase.Form_Closed(sender, e)

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            If CreateLock() = False Then

                FindForm.Close()
                Exit Sub

            End If

            spdIssue_Initialise()
            _Issues = New BOPurchases.cIssueHeader(_Oasys3DB)
            GetSuppliers()

            AddIssuesToSpread()


        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

#Region "Tab Events"

    Private Sub tabcReceive_SelectedPageChanging(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangingEventArgs) Handles tabcReceive.SelectedPageChanging

        DisplayStatus()

        Select Case e.Page.Name
            Case tabpIssue.Name
                btnReceive.Visible = True
                btnDelete.Visible = True
                btnImport.Visible = True
                btnDeletePos.Visible = True
                chkRejected.Visible = True
                If spdIssue.Sheets.Count > 0 Then
                    btnReceive.Enabled = (spdIssue.ActiveSheet.RowCount > 0)
                End If

            Case tabpContainer.Name
                btnReceive.Visible = False
                btnDelete.Visible = False
                btnImport.Visible = False
                btnDeletePos.Visible = False
                chkRejected.Visible = False
                btnPrint.Enabled = (xviewContainer.RowCount > 0)
        End Select

    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub chkFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRejected.CheckedChanged

        DisplayStatus()
        Dim sheet As SheetView = spdIssue.ActiveSheet
        If sheet.RowCount = 0 Then Exit Sub

        If chkRejected.Checked Then
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If chkToday.Checked Then
                    sheet.Rows(rowIndex).Visible = (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length > 0) Or CDate(sheet.Cells(rowIndex, colIssue.Date).Value) = DateTime.Today.AddDays(-1)
                Else
                    sheet.Rows(rowIndex).Visible = (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length > 0)
                End If
            Next
        Else
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If chkToday.Checked Then
                    sheet.Rows(rowIndex).Visible = (CDate(sheet.Cells(rowIndex, colIssue.Date).Value) = DateTime.Today.AddDays(-1)) And sheet.Rows(rowIndex).Visible = (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length = 0)
                Else
                    sheet.Rows(rowIndex).Visible = True
                End If
            Next
        End If

    End Sub

    Private Sub chkToday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkToday.CheckedChanged

        DisplayStatus()
        Dim sheet As SheetView = spdIssue.ActiveSheet
        If sheet.RowCount = 0 Then Exit Sub

        If chkToday.Checked Then
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If chkRejected.Checked Then
                    sheet.Rows(rowIndex).Visible = CDate(sheet.Cells(rowIndex, colIssue.Date).Value) = DateTime.Today.AddDays(-1) Or (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length > 0)
                Else
                    sheet.Rows(rowIndex).Visible = (CDate(sheet.Cells(rowIndex, colIssue.Date).Value) = DateTime.Today.AddDays(-1)) And sheet.Rows(rowIndex).Visible = (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length = 0)
                End If

            Next
        Else
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If chkRejected.Checked Then
                    sheet.Rows(rowIndex).Visible = (sheet.Cells(rowIndex, colIssue.Status).Value.ToString.Length > 0)
                Else
                    sheet.Rows(rowIndex).Visible = True
                End If
            Next
        End If

    End Sub

#End Region

#Region "Spread Events"

    Private Sub spdIssue_Initialise()

        'make F keys available in spread
        Dim im As InputMap = spdIssue.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdIssue.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        'create both sheets and set default properties for both
        Dim sheet As New SheetView
        sheet.SelectionUnit = Model.SelectionUnit.Row
        sheet.SelectionPolicy = Model.SelectionPolicy.Single
        sheet.RowCount = 0
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnHeader.RowCount = 1
        sheet.ColumnHeaderVisible = False

        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True
        sheet.ColumnHeader.Rows(0).HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.ColumnHeader.Rows(0).Border = New LineBorder(System.Drawing.Color.Black, 1, False, False, False, True)

        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(System.Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.BackColor = System.Drawing.Color.Transparent
        sheet.ColumnHeader.DefaultStyle.Font = New System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 8.5, System.Drawing.FontStyle.Bold)
        sheet.ColumnHeader.Rows(0).HorizontalAlignment = CellHorizontalAlignment.Center

        'sheet issues
        sheet.ColumnCount = [Enum].GetValues(GetType(colIssue)).Length
        sheet.Columns(colIssue.Number).Label = "Issue Number"
        sheet.Columns(colIssue.Number).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.Number).AllowAutoSort = True
        sheet.Columns(colIssue.PoNumber).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.PoNumber).AllowAutoSort = True
        sheet.Columns(colIssue.Date).Label = "Issue Date"
        sheet.Columns(colIssue.Date).CellType = New CellType.DateTimeCellType
        sheet.Columns(colIssue.SupplierNumber).Label = "Supplier"
        sheet.Columns(colIssue.SupplierName).Label = "Name"
        sheet.Columns(colIssue.SupplierName).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.PoNumber).Label = "PO Number"
        sheet.Columns(colIssue.Value).Label = "Value"
        sheet.Columns(colIssue.Deleted).CellType = New CellType.CheckBoxCellType
        sheet.Columns(colIssue.Deleted).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(colIssue.Deleted).Width = 70
        sheet.Columns(colIssue.Deleted).Resizable = False
        sheet.Columns(colIssue.Imported).CellType = New CellType.CheckBoxCellType
        sheet.Columns(colIssue.Imported).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(colIssue.Imported).Width = 70
        sheet.Columns(colIssue.Imported).Resizable = False
        sheet.Columns(colIssue.Received).CellType = New CellType.CheckBoxCellType
        sheet.Columns(colIssue.Received).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(colIssue.Received).Width = 70
        sheet.Columns(colIssue.Received).Resizable = False
        sheet.Columns(colIssue.DrlNumber).Label = "DRL Number"
        sheet.Columns(colIssue.DrlNumber).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.DrlNumber).AllowAutoSort = True
        sheet.Columns(colIssue.Deleted).Label = "Deleted"
        sheet.Columns(colIssue.Received).Label = "Received"
        sheet.Columns(colIssue.Imported).Label = "Imported"
        sheet.Columns(colIssue.Status).Label = "Status"
        sheet.Columns(colIssue.Status).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.PoType).Label = "PO Type"
        sheet.Columns(colIssue.PoType).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(colIssue.QtyOrder).Label = "Ordered"
        sheet.Columns(colIssue.QtyIssue).Label = "Issued"
        sheet.Columns(colIssue.QtyFollow).Label = "To Follow"

        'Print info stuff.
        sheet.PrintInfo.Header = "/c/fz""14""Issue Status Report"
        sheet.PrintInfo.Header &= "/n "
        sheet.PrintInfo.Footer = "/lPrinted: " & Now
        sheet.PrintInfo.Footer &= "/cPage /p of /pc"
        sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        sheet.PrintInfo.Orientation = Spread.PrintOrientation.Landscape
        sheet.PrintInfo.Margin.Left = 40
        sheet.PrintInfo.Margin.Right = 40
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.ShowPrintDialog = True
        sheet.PrintInfo.UseSmartPrint = True

        'add sheets to spread
        spdIssue.Sheets.Add(sheet)

    End Sub

    Private Sub spdIssue_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles spdIssue.Resize

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Const tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth

            'get columns to resize and new widths in arraylist
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If

        Next

    End Sub

    Private Sub spdIssue_SelectionChanged() Handles spdIssue.SelectionChanged

        DisplayStatus()

        Dim sheet As SheetView = spdIssue.ActiveSheet
        If sheet.Cells(sheet.ActiveRowIndex, colIssue.Status).Value.ToString.Trim.Length = 0 Then
            btnDelete.Enabled = False
            btnImport.Enabled = False
            Exit Sub
        End If

        If CBool(sheet.Cells(sheet.ActiveRowIndex, colIssue.Deleted).Value) Then
            btnDelete.Text = My.Resources.F6UndeleteIssue
            btnDelete.Enabled = True
            btnImport.Enabled = False
            Exit Sub
        End If

        If CBool(sheet.Cells(sheet.ActiveRowIndex, colIssue.Imported).Value) Then
            btnDelete.Text = My.Resources.F6DeleteIssue
            btnDelete.Enabled = True
            btnImport.Enabled = False
        Else
            btnDelete.Text = My.Resources.F6DeleteIssue
            btnDelete.Enabled = True
            btnImport.Enabled = True
        End If

    End Sub

#End Region

#Region "Button Events"

    Private Sub btnRetrieve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click

        Try
            Cursor = Cursors.WaitCursor

            LoadContainers()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnDeletePos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePos.Click

        Try
            Dim result As DialogResult = MessageBox.Show("Are you sure you would like to delete all late warehouse orders?", "Confirmation Required", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
            If result <> DialogResult.Yes Then Exit Sub

            DisplayStatus("Deleting late warehouse orders")
            Cursor = Cursors.WaitCursor

            Dim order As New BOPurchases.cPurchaseHeader(_Oasys3DB)
            order.DeleteOldPos()

            DisplayStatus()
            MessageBox.Show("All late warehouse orders deleted", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click

        DisplayStatus()

        'get issue and update as import and update sheet
        Dim sheet As SheetView = spdIssue.ActiveSheet
        Dim issue As BOPurchases.cIssueHeader = _Issues.Header(CStr(sheet.Cells(sheet.ActiveRowIndex, colIssue.Number).Value))

        If issue.Import Then
            sheet.Cells(sheet.ActiveRowIndex, colIssue.Imported).Value = True
            btnImport.Enabled = False
        Else
            DisplayWarning(My.Resources.Strings.PoNotReceived)
        End If

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        DisplayStatus()
        Select Case btnDelete.Text
            Case My.Resources.F6UndeleteIssue
                'get issue and update as import and update sheet
                Dim sheet As SheetView = spdIssue.ActiveSheet
                Dim issue As BOPurchases.cIssueHeader = _Issues.Header(CStr(sheet.Cells(sheet.ActiveRowIndex, colIssue.Number).Value))

                issue.DeleteIssue.Value = False
                If issue.SaveIfExists Then
                    sheet.Cells(sheet.ActiveRowIndex, colIssue.Deleted).Value = False
                    btnDelete.Text = My.Resources.F6DeleteIssue
                End If

            Case My.Resources.F6DeleteIssue
                'get issue and update as import and update sheet
                Dim sheet As SheetView = spdIssue.ActiveSheet
                Dim issue As BOPurchases.cIssueHeader = _Issues.Header(CStr(sheet.Cells(sheet.ActiveRowIndex, colIssue.Number).Value))

                'need password to proceed
                If AuthorisationCheck() Then
                    issue.DeleteIssue.Value = True
                    If issue.SaveIfExists Then
                        sheet.Cells(sheet.ActiveRowIndex, colIssue.Deleted).Value = True
                        btnDelete.Text = My.Resources.F6UndeleteIssue
                    End If
                End If

        End Select

    End Sub

    Private Sub btnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceive.Click

        Try
            'check that sheet has rows
            Dim sheet As SheetView = spdIssue.ActiveSheet
            If sheet.RowCount = 0 Then Exit Sub

            DisplayStatus("Receiving all issues")
            Cursor = Cursors.WaitCursor

            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If CBool(sheet.Cells(rowIndex, colIssue.Received).Value) Then Continue For
                If CBool(sheet.Cells(rowIndex, colIssue.Deleted).Value) Then Continue For

                'receive issue and update sheet
                Dim issue As BOPurchases.cIssueHeader = _Issues.Header(CStr(sheet.Cells(rowIndex, colIssue.Number).Value))
                issue.UpdateIssueReceived(UserId)
                sheet.Cells(rowIndex, colIssue.Received).Value = issue.ReceivedIssue.Value
                sheet.Cells(rowIndex, colIssue.Status).Value = issue.RejectionString
                sheet.Cells(rowIndex, colIssue.DrlNumber).Value = issue.DrlNumber.Value
            Next

            MessageBox.Show("Receiving complete", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        'check which sheet is active
        Select Case tabcReceive.SelectedTabPage.Name
            Case tabpIssue.Name
                spdIssue.PrintSheet(spdIssue.ActiveSheet)

            Case tabpContainer.Name
                'get container from datasource
                Dim containers As ContainerCollection = CType(xgridContainer.DataSource, ContainerCollection)
                Dim con As Container = containers(xviewContainer.GetDataSourceRowIndex(xviewContainer.GetSelectedRows(0)))
                Using conPrint As New ContainerPrint(con, UserId, WorkstationId)
                    conPrint.ShowPreviewDialog()
                End Using

        End Select

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function CreateLock() As Boolean

        _BusinessLockIW = (New BusinessLockIWFactory).GetImplementation

        _LockID = _BusinessLockIW.CreateLock(MyBase.WorkstationId, Now, "ReceiveIW", String.Empty)

        If _LockID = -1 Then

            MessageBox.Show("This program is already running on another machine", "Menu Process Already Running", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Return False

        End If

        Return True

    End Function

    Private Sub RemoveLock()

        If _LockID > 0 Then _BusinessLockIW.RemoveLock(_LockID)

        'application exit runs "close event" one or more times for this form
        'exact number depends on number of time this form is loaded
        _LockID = 0

    End Sub

    Private Sub AddIssuesToSpread()

        Dim sheet As SheetView = spdIssue.ActiveSheet
        For Each issue As BOPurchases.cIssueHeader In _Issues.Headers
            sheet.AddRows(sheet.RowCount, 1)
            sheet.Cells(sheet.RowCount - 1, colIssue.Number).Value = issue.IssueNumber.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.Date).Value = issue.IssueDate.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.SupplierNumber).Value = issue.SupplierNumber.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.SupplierName).Value = issue.Supplier.Name.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.PoNumber).Value = issue.StorePoNumber.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.PoType).Value = issue.PoTypeString
            sheet.Cells(sheet.RowCount - 1, colIssue.Value).Value = issue.IssueValue.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.Deleted).Value = issue.DeleteIssue.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.Received).Value = issue.ReceivedIssue.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.Imported).Value = issue.ImportedIssue.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.Status).Value = issue.RejectionString
            sheet.Cells(sheet.RowCount - 1, colIssue.DrlNumber).Value = issue.DrlNumber.Value
            sheet.Cells(sheet.RowCount - 1, colIssue.QtyOrder).Value = issue.QtyOrdered
            sheet.Cells(sheet.RowCount - 1, colIssue.QtyIssue).Value = issue.QtyIssued
            sheet.Cells(sheet.RowCount - 1, colIssue.QtyFollow).Value = issue.QtyFollow
        Next

        If sheet.RowCount > 0 Then
            sheet.ColumnHeaderVisible = True
            sheet.SetActiveCell(0, 0)
            sheet.AddSelection(0, 0, 1, [Enum].GetValues(GetType(colIssue)).Length)

            btnReceive.Enabled = True
            btnDelete.Text = My.Resources.F6DeleteIssue
            btnDelete.Visible = True
            btnImport.Visible = True
            btnDeletePos.Visible = True

            spdIssue_SelectionChanged()
            spdIssue_Resize(spdIssue, New EventArgs)

            Me.chkToday.Checked = True
            Me.chkRejected.Checked = True

            Refresh()
            spdIssue.Focus()
        End If

    End Sub

    Private Function AuthorisationCheck() As Boolean

        'Check that for authorisation
        Using auth As New Authorisation.StoreForm(Authorisation.AuthTypes.Issue)
            If auth.ShowDialog(Me) = DialogResult.OK Then
                Return True
            End If
        End Using
        Return False

    End Function

    Private Sub GetSuppliers()

        'Get all supplier numbers in orders
        Dim SupplierNumbers As New Collections.ArrayList
        For Each issue As BOPurchases.cIssueHeader In _Issues.Headers
            If Not SupplierNumbers.Contains(issue.SupplierNumber.Value) Then SupplierNumbers.Add(issue.SupplierNumber.Value)
        Next
        If SupplierNumbers.Count = 0 Then Exit Sub

        Dim Supplier As New BOPurchases.cSupplierMaster(_Oasys3DB)
        Supplier.AddLoadField(Supplier.Number)
        Supplier.AddLoadField(Supplier.Name)
        Supplier.AddLoadField(Supplier.OrderDepotNumber)
        Supplier.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pIn, Supplier.Number, SupplierNumbers)
        Supplier.Suppliers = Supplier.LoadMatches

        'assign suppliers to orders
        For Each issue As BOPurchases.cIssueHeader In _Issues.Headers
            For Each sup As BOPurchases.cSupplierMaster In Supplier.Suppliers
                If sup.Number.Value = issue.SupplierNumber.Value Then
                    issue.Supplier = sup
                    Exit For
                End If
            Next
        Next

    End Sub

    Private Sub LoadContainers()

        Dim containers As Purchasing.Core.ContainerCollection = Purchasing.Core.Container.GetByDateRange(CDate(dtpFrom.EditValue), CDate(dtpTo.EditValue))
        xgridContainer.DataSource = containers

        'set up column widths
        For Each col As GridColumn In xviewContainer.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Container) f.AssemblyDepotNumber) : col.Caption = My.Resources.ColumnHeaders.AssemblyDepot
                Case GetPropertyName(Function(f As Container) f.Number)
                Case GetPropertyName(Function(f As Container) f.DateDespatch)
                Case GetPropertyName(Function(f As Container) f.Description)
                Case GetPropertyName(Function(f As Container) f.DespatchDepotNumber) : col.Caption = My.Resources.ColumnHeaders.DespatchDepot
                Case GetPropertyName(Function(f As Container) f.VehicleLoadReference) : col.Caption = My.Resources.ColumnHeaders.VehicleLoadRef
                Case GetPropertyName(Function(f As Container) f.NumberLines)
                Case GetPropertyName(Function(f As Container) f.ParentNumber) : col.Caption = My.Resources.ColumnHeaders.ParentNumber
                Case GetPropertyName(Function(f As Container) f.IsReceived) : col.Caption = My.Resources.ColumnHeaders.Received
                Case GetPropertyName(Function(f As Container) f.DateDelivery)
                Case GetPropertyName(Function(f As Container) f.Value)
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = ChrW(163) & "#.00"
                    col.MinWidth = 60
                Case GetPropertyName(Function(f As Container) f.RejectedReason)
                Case Else : col.Visible = False
            End Select
        Next

        LoadContainerLines(xgridContainer)

        xviewContainer.BestFitColumns()
        xviewContainer.Focus()
        btnPrint.Enabled = (xviewContainer.RowCount > 0)

    End Sub

    Private Sub LoadContainerLines(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As Container) f.Lines), view)

        Dim skuNumber As String = GetPropertyName(Function(f As ContainerLine) f.SkuNumber)
        view.Columns.AddField(skuNumber).VisibleIndex = 0
        view.Columns(skuNumber).Caption = My.Resources.ColumnHeaders.SkuNumber
        view.Columns(skuNumber).MinWidth = 120
        view.Columns(skuNumber).MaxWidth = 120

        Dim description As String = GetPropertyName(Function(f As ContainerLine) f.SkuDescription)
        view.Columns.AddField(description).VisibleIndex = 1
        view.Columns(description).Caption = My.Resources.ColumnHeaders.Description

        Dim productCode As String = GetPropertyName(Function(f As ContainerLine) f.SkuProductCode)
        view.Columns.AddField(productCode).VisibleIndex = 2
        view.Columns(productCode).Caption = My.Resources.ColumnHeaders.ProductCode
        view.Columns(productCode).MinWidth = 120
        view.Columns(productCode).MaxWidth = 120

        Dim packsize As String = GetPropertyName(Function(f As ContainerLine) f.SkuPackSize)
        view.Columns.AddField(packsize).VisibleIndex = 3
        view.Columns(packsize).Caption = My.Resources.ColumnHeaders.PackSize
        view.Columns(packsize).MinWidth = 60
        view.Columns(packsize).MaxWidth = 60

        Dim price As String = GetPropertyName(Function(f As ContainerLine) f.Price)
        view.Columns.AddField(price).VisibleIndex = 4
        view.Columns(price).MaxWidth = 80
        view.Columns(price).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        view.Columns(price).DisplayFormat.FormatString = ChrW(163) & "#.00"

        Dim qty As String = GetPropertyName(Function(f As ContainerLine) f.Qty)
        view.Columns.AddField(qty).VisibleIndex = 5
        view.Columns(qty).Caption = My.Resources.ColumnHeaders.Qty
        view.Columns(qty).MaxWidth = 80

        Dim poNumber As String = GetPropertyName(Function(f As ContainerLine) f.StorePoNumber)
        view.Columns.AddField(poNumber).VisibleIndex = 6
        view.Columns(poNumber).Caption = My.Resources.ColumnHeaders.PoNumber

        Dim poLine As String = GetPropertyName(Function(f As ContainerLine) f.StorePoLineNumber)
        view.Columns.AddField(poLine).VisibleIndex = 7
        view.Columns(poLine).Caption = My.Resources.ColumnHeaders.PoLine

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False

    End Sub

#End Region

End Class