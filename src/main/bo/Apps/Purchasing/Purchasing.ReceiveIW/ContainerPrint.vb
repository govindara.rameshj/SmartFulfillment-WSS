﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports Purchasing.Core
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns
Imports System.Linq.Expressions
Imports DevExpress.XtraGrid.Views.BandedGrid
Imports DevExpress.XtraGrid.Views.Base
Imports Cts.Oasys.Core.System

Public Class ContainerPrint
    Implements IDisposable
    Private _report As CompositeLink
    Private _storeIdName As String = String.Empty
    Private _userIdName As String = String.Empty
    Private _workstationId As Integer
    Private _container As Container

    Public ReadOnly Property Report() As CompositeLink
        Get
            Return _report
        End Get
    End Property


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub New(ByVal container As Container, ByVal userId As Integer, ByVal workstationId As Integer)
        _container = container
        _storeIdName = Store.GetIdName
        _userIdName = User.GetUserIdName(userId, IncludeExternalIds:=True)
        _workstationId = workstationId
        Initialise()
    End Sub

    Private Sub Initialise()

        'set up supplier copy report
        _report = New CompositeLink(New PrintingSystem)
        _report.Margins.Bottom = 70
        _report.Margins.Top = 70
        _report.Margins.Left = 50
        _report.Margins.Right = 50
        _report.Landscape = True
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler _report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea
        _report.Links.Add(reportTitle)

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = CreateGrid()
        _report.Links.Add(reportGrid)

    End Sub

    Private Function CreateGrid() As GridControl

        'create grid
        Dim grid As New GridControl
        Dim view As New GridView
        grid.MainView = view
        grid.Name = "grid"
        grid.ViewCollection.Add(view)
        grid.Parent = New Form
        view.GridControl = grid
        view.Name = "view"

        'set data source and sort out columns
        grid.DataSource = _container.Lines

        For Each col As GridColumn In view.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As ContainerLine) f.SkuNumber) : col.Caption = My.Resources.ColumnHeaders.SkuNumber
                Case GetPropertyName(Function(f As ContainerLine) f.SkuDescription) : col.Caption = My.Resources.ColumnHeaders.Description
                Case GetPropertyName(Function(f As ContainerLine) f.SkuProductCode) : col.Caption = My.Resources.ColumnHeaders.ProductCode
                Case GetPropertyName(Function(f As ContainerLine) f.SkuPackSize) : col.Caption = My.Resources.ColumnHeaders.PackSize
                Case GetPropertyName(Function(f As ContainerLine) f.Price)
                Case GetPropertyName(Function(f As ContainerLine) f.Qty) : col.Caption = My.Resources.ColumnHeaders.Qty
                Case GetPropertyName(Function(f As ContainerLine) f.StorePoNumber) : col.Caption = My.Resources.ColumnHeaders.PoNumber
                Case GetPropertyName(Function(f As ContainerLine) f.StorePoLineNumber) : col.Caption = My.Resources.ColumnHeaders.PoLine
                Case Else : col.Visible = False
            End Select
        Next

        view.BestFitColumns()
        Return grid

    End Function


    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(My.Resources.Strings.ReportHeader, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(My.Resources.Strings.ReportSubHeader, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        tb = e.Graph.DrawString("Date: " & Now.Date.ToShortDateString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & _userIdName & Space(2) & "WS: " & _workstationId & Space(2) & "Time: {0:HH:mm}")
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'set variables
        Dim x As Integer = 0
        Dim h As Integer = 40

        'container header column
        Dim sb As New StringBuilder
        sb.Append("Container Number:" & Environment.NewLine)
        sb.Append("Load Reference:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 120, h, StringAlignment.Near, BorderSide.None, True)

        'container details
        sb = New StringBuilder
        sb.Append(_container.Number & Environment.NewLine)
        sb.Append(_container.VehicleLoadReference)
        DrawTextRectangle(e.Graph, sb.ToString, x, 220, h, StringAlignment.Near, BorderSide.None)

        'value and date labels
        sb = New StringBuilder
        sb.Append("Delivery Date:" & Environment.NewLine)
        sb.Append("Container Value:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 120, h, StringAlignment.Near, BorderSide.None, True)

        'value and date
        sb = New StringBuilder
        sb.Append(_container.DateDelivery & Environment.NewLine)
        sb.Append(_container.Value)
        DrawTextRectangle(e.Graph, sb.ToString, x, 100, h, StringAlignment.Far, BorderSide.None)

    End Sub


    Private Sub DrawTextRectangle(ByRef graph As BrickGraphics, ByVal text As String, ByRef x As Integer, ByVal width As Integer, ByVal height As Integer, ByVal align As StringAlignment, ByVal sides As DevExpress.XtraPrinting.BorderSide, Optional ByVal bold As Boolean = False)

        Dim rec As New RectangleF(x, 0, width, height)
        graph.StringFormat = New BrickStringFormat(align)
        If bold Then
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        Else
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Regular)
        End If
        graph.DrawString(text, Color.Black, rec, sides)
        x += width

    End Sub



    Public Sub ShowPreviewDialog()

        _report.ShowPreviewDialog()

    End Sub

    Public Sub Print()
        _report.PrintingSystem.Print()
    End Sub

    Public Sub CreateDocument()
        _report.CreateDocument()
    End Sub


    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'properyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _report.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
