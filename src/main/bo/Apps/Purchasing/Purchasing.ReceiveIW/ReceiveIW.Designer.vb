<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReceiveIW
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReceiveIW))
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnReceive = New System.Windows.Forms.Button
        Me.spdIssue = New FarPoint.Win.Spread.FpSpread
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.chkRejected = New System.Windows.Forms.CheckBox
        Me.btnDeletePos = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.tabcReceive = New DevExpress.XtraTab.XtraTabControl
        Me.tabpIssue = New DevExpress.XtraTab.XtraTabPage
        Me.chkToday = New System.Windows.Forms.CheckBox
        Me.tabpContainer = New DevExpress.XtraTab.XtraTabPage
        Me.xgridContainer = New DevExpress.XtraGrid.GridControl
        Me.xviewContainer = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnRetrieve = New System.Windows.Forms.Button
        Me.dtpTo = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.dtpFrom = New DevExpress.XtraEditors.DateEdit
        CType(Me.spdIssue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabcReceive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabcReceive.SuspendLayout()
        Me.tabpIssue.SuspendLayout()
        Me.tabpContainer.SuspendLayout()
        CType(Me.xgridContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpTo.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpFrom.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        resources.ApplyResources(Me.btnExit, "btnExit")
        Me.btnExit.Name = "btnExit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReceive
        '
        resources.ApplyResources(Me.btnReceive, "btnReceive")
        Me.btnReceive.Name = "btnReceive"
        Me.btnReceive.UseVisualStyleBackColor = True
        '
        'spdIssue
        '
        Me.spdIssue.About = "3.0.2004.2005"
        resources.ApplyResources(Me.spdIssue, "spdIssue")
        Me.spdIssue.HorizontalScrollBarHeight = 15
        Me.spdIssue.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdIssue.Name = "spdIssue"
        Me.spdIssue.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdIssue.TextTipAppearance = TipAppearance1
        Me.spdIssue.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdIssue.VerticalScrollBarWidth = 15
        Me.spdIssue.ActiveSheetIndex = -1
        '
        'btnDelete
        '
        resources.ApplyResources(Me.btnDelete, "btnDelete")
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        resources.ApplyResources(Me.btnImport, "btnImport")
        Me.btnImport.Name = "btnImport"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'chkRejected
        '
        resources.ApplyResources(Me.chkRejected, "chkRejected")
        Me.chkRejected.Name = "chkRejected"
        Me.chkRejected.UseVisualStyleBackColor = True
        '
        'btnDeletePos
        '
        resources.ApplyResources(Me.btnDeletePos, "btnDeletePos")
        Me.btnDeletePos.Name = "btnDeletePos"
        Me.btnDeletePos.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        resources.ApplyResources(Me.btnPrint, "btnPrint")
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'tabcReceive
        '
        resources.ApplyResources(Me.tabcReceive, "tabcReceive")
        Me.tabcReceive.Name = "tabcReceive"
        Me.tabcReceive.SelectedTabPage = Me.tabpIssue
        Me.tabcReceive.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabpIssue, Me.tabpContainer})
        '
        'tabpIssue
        '
        Me.tabpIssue.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpIssue.Controls.Add(Me.chkToday)
        Me.tabpIssue.Controls.Add(Me.spdIssue)
        Me.tabpIssue.Controls.Add(Me.chkRejected)
        Me.tabpIssue.Name = "tabpIssue"
        resources.ApplyResources(Me.tabpIssue, "tabpIssue")
        '
        'chkToday
        '
        resources.ApplyResources(Me.chkToday, "chkToday")
        Me.chkToday.Name = "chkToday"
        Me.chkToday.UseVisualStyleBackColor = True
        '
        'tabpContainer
        '
        Me.tabpContainer.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpContainer.Controls.Add(Me.xgridContainer)
        Me.tabpContainer.Controls.Add(Me.btnRetrieve)
        Me.tabpContainer.Controls.Add(Me.dtpTo)
        Me.tabpContainer.Controls.Add(Me.LabelControl2)
        Me.tabpContainer.Controls.Add(Me.LabelControl1)
        Me.tabpContainer.Controls.Add(Me.dtpFrom)
        Me.tabpContainer.Name = "tabpContainer"
        resources.ApplyResources(Me.tabpContainer, "tabpContainer")
        '
        'xgridContainer
        '
        resources.ApplyResources(Me.xgridContainer, "xgridContainer")
        Me.xgridContainer.MainView = Me.xviewContainer
        Me.xgridContainer.Name = "xgridContainer"
        Me.xgridContainer.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewContainer})
        '
        'xviewContainer
        '
        Me.xviewContainer.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewContainer.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewContainer.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewContainer.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.xviewContainer.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewContainer.ColumnPanelRowHeight = 35
        Me.xviewContainer.GridControl = Me.xgridContainer
        Me.xviewContainer.Name = "xviewContainer"
        Me.xviewContainer.OptionsBehavior.Editable = False
        Me.xviewContainer.OptionsDetail.ShowDetailTabs = False
        Me.xviewContainer.OptionsSelection.EnableAppearanceFocusedCell = False
        '
        'btnRetrieve
        '
        resources.ApplyResources(Me.btnRetrieve, "btnRetrieve")
        Me.btnRetrieve.Name = "btnRetrieve"
        Me.btnRetrieve.UseVisualStyleBackColor = True
        '
        'dtpTo
        '
        Me.dtpTo.EditValue = Nothing
        resources.ApplyResources(Me.dtpTo, "dtpTo")
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(CType(resources.GetObject("dtpTo.Properties.Buttons"), DevExpress.XtraEditors.Controls.ButtonPredefines))})
        Me.dtpTo.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'LabelControl2
        '
        resources.ApplyResources(Me.LabelControl2, "LabelControl2")
        Me.LabelControl2.Name = "LabelControl2"
        '
        'LabelControl1
        '
        resources.ApplyResources(Me.LabelControl1, "LabelControl1")
        Me.LabelControl1.Name = "LabelControl1"
        '
        'dtpFrom
        '
        Me.dtpFrom.EditValue = Nothing
        resources.ApplyResources(Me.dtpFrom, "dtpFrom")
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(CType(resources.GetObject("dtpFrom.Properties.Buttons"), DevExpress.XtraEditors.Controls.ButtonPredefines))})
        Me.dtpFrom.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'ReceiveIW
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tabcReceive)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnDeletePos)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnReceive)
        Me.Controls.Add(Me.btnExit)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ReceiveIW"
        CType(Me.spdIssue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabcReceive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabcReceive.ResumeLayout(False)
        Me.tabpIssue.ResumeLayout(False)
        Me.tabpContainer.ResumeLayout(False)
        Me.tabpContainer.PerformLayout()
        CType(Me.xgridContainer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewContainer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpTo.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpFrom.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReceive As System.Windows.Forms.Button
    Friend WithEvents spdIssue As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents chkRejected As System.Windows.Forms.CheckBox
    Friend WithEvents btnDeletePos As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents tabcReceive As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabpIssue As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabpContainer As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents chkToday As System.Windows.Forms.CheckBox
    Friend WithEvents dtpTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtpFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btnRetrieve As System.Windows.Forms.Button
    Friend WithEvents xgridContainer As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewContainer As DevExpress.XtraGrid.Views.Grid.GridView

End Class
