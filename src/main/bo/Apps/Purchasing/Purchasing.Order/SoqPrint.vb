﻿Imports DevExpress.XtraPrinting

Public Class SoqPrint
    Implements IDisposable
    Private _report As CompositeLink
    Private _headerMain As String = "Purchase Orders"
    Private _headerSub As String = "SOQ"
    Private _storeIdName As String = String.Empty
    Private _userIdName As String = String.Empty
    Private _workstationId As Integer
    Private _supplier As Supplier

    Public ReadOnly Property Report() As CompositeLink
        Get
            Return _report
        End Get
    End Property

    Public Sub New(ByVal supplier As Supplier, ByVal storeIdName As String, ByVal userIdName As String, ByVal workstationId As Integer)
        _storeIdName = storeIdName
        _supplier = supplier
        _userIdName = userIdName
        _workstationId = workstationId
        Initialise()
    End Sub

    Private Sub Initialise()

        'set up supplier copy report
        _report = New CompositeLink(New PrintingSystem)
        _report.Margins.Bottom = 70
        _report.Margins.Top = 70
        _report.Margins.Left = 50
        _report.Margins.Right = 50
        _report.Landscape = True
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler _report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea
        _report.Links.Add(reportTitle)

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = CreateGrid()
        _report.Links.Add(reportGrid)

        Dim reportFooter As New Link
        AddHandler reportFooter.CreateDetailArea, AddressOf ReportFooter_CreateReportDetailArea
        _report.Links.Add(reportFooter)

    End Sub

    Private Function CreateGrid() As GridControl

        'create grid
        Dim grid As New GridControl
        Dim view As New BandedGridView
        grid.MainView = view
        grid.Name = "grid"
        grid.ViewCollection.Add(view)
        grid.Parent = New Form
        view.GridControl = grid
        view.Name = "view"

        'set data source and sort out columns
        grid.DataSource = _supplier.Stocks
        view.Bands.Clear()

        Dim bandSku As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.SkuNumber)
        Dim bandDescription As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.Description)
        Dim bandSales As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.SalesWeeks)
        Dim bandSalesAve As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.SalesAverage)
        Dim bandOnHand As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.OnHand)
        Dim bandOnOrder As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.OnOrder)
        Dim bandSoq As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.SoqQty)
        Dim bandShelfCap As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.ShelfCap)
        Dim bandPack As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.Pack)
        Dim bandIndicator As GridBand = view.Bands.AddBand(Space(1))
        Dim bandSku2 As GridBand = view.Bands.AddBand(My.Resources.ColumnHeaders.SkuNumber)

        view.AppearancePrint.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        view.AppearancePrint.BandPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        view.AppearancePrint.BandPanel.Options.UseFont = True
        view.AppearancePrint.BandPanel.Options.UseTextOptions = True
        view.OptionsView.ColumnAutoWidth = False
        view.OptionsPrint.PrintHeader = False
        view.OptionsPrint.UsePrintStyles = True
        AddHandler view.CustomUnboundColumnData, AddressOf view_CustomUnboundColumnData

        'add unbound columns
        Dim colIndicator As GridColumn = view.Columns.AddField(Unbounds.Indicator)
        colIndicator.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Dim colSku2 As GridColumn = view.Columns.AddField(Unbounds.Sku)
        colSku2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far

        grid.ForceInitialize()

        For Each col As GridColumn In view.Columns
            col.Visible = True

            Select Case col.FieldName
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber) : bandSku.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Description) : bandDescription.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales4)
                    bandSales.Columns.Add(CType(col, BandedGridColumn))
                    bandSales.Columns(col.Name).ColVIndex = 0
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales3)
                    bandSales.Columns.Add(CType(col, BandedGridColumn))
                    bandSales.Columns(col.Name).ColVIndex = 1
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales2)
                    bandSales.Columns.Add(CType(col, BandedGridColumn))
                    bandSales.Columns(col.Name).ColVIndex = 2
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales1)
                    bandSales.Columns.Add(CType(col, BandedGridColumn))
                    bandSales.Columns(col.Name).ColVIndex = 3
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PeriodDemand) : bandSalesAve.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty) : bandOnHand.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnOrderQty) : bandOnOrder.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderLevel) : bandSoq.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Capacity) : bandShelfCap.Columns.Add(CType(col, BandedGridColumn))
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PackSize) : bandPack.Columns.Add(CType(col, BandedGridColumn))
                Case Unbounds.Indicator
                    bandIndicator.Columns.Add(CType(col, BandedGridColumn))
                    col.UnboundType = DevExpress.Data.UnboundColumnType.String
                Case Unbounds.Sku
                    bandSku2.Columns.Add(CType(col, BandedGridColumn))
                    col.UnboundType = DevExpress.Data.UnboundColumnType.String
                Case Else : col.Visible = False
            End Select
        Next

        For Each col As GridColumn In view.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales4)
                    bandSales.Columns(col.Name).ColVIndex = 0
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales3)
                    bandSales.Columns(col.Name).ColVIndex = 1
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales2)
                    bandSales.Columns(col.Name).ColVIndex = 2
                Case GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales1)
                    bandSales.Columns(col.Name).ColVIndex = 3
            End Select
        Next
        
        view.BestFitColumns()
        bandSku.Width = 25
        bandSales.Width = 120
        bandSalesAve.Width = 35
        bandOnHand.Width = 35
        bandOnOrder.Width = 35
        bandSoq.Width = 35
        bandShelfCap.Width = 35
        bandPack.Width = 30
        bandIndicator.Width = 10
        bandSku2.Width = 25

        Return grid

    End Function

    Private Sub view_CustomUnboundColumnData(ByVal sender As Object, ByVal e As CustomColumnDataEventArgs)

        If e.RowHandle = GridControl.InvalidRowHandle Then Exit Sub
        Dim view As BandedGridView = CType(sender, BandedGridView)

        Select Case e.Column.FieldName
            Case Unbounds.Sku : e.Value = view.GetRowCellValue(e.ListSourceRowIndex, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber))
            Case Unbounds.Indicator
                Dim onHand As Integer = CInt(view.GetRowCellValue(e.ListSourceRowIndex, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty)))
                Dim onOrder As Integer = CInt(view.GetRowCellValue(e.ListSourceRowIndex, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnOrderQty)))
                Dim orderLevel As Integer = CInt(view.GetRowCellValue(e.ListSourceRowIndex, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderLevel)))
                Dim capacity As Integer = CInt(view.GetRowCellValue(e.ListSourceRowIndex, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Capacity)))

                Dim over As Integer = onOrder + onHand - capacity
                If over > 0 Then
                    e.Value = "~"
                End If

                If orderLevel > capacity Then
                    If over > 0 Then
                        e.Value = "~**"
                    Else
                        e.Value = "**"
                    End If
                End If
        End Select

    End Sub

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(_headerMain, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(_headerSub, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        tb = e.Graph.DrawString("Date: " & Now.Date.ToShortDateString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & _userIdName & Space(2) & "WS: " & _workstationId & Space(2) & "Time: {0:HH:mm}")
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'set variables
        Dim x As Integer = 0
        Dim h As Integer = 50

        'order type header column
        Dim sb As New StringBuilder
        sb.Append("Order Type:" & Environment.NewLine)
        sb.Append("Supplier:" & Environment.NewLine)
        sb.Append("Telephone:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 80, h, StringAlignment.Near, BorderSide.None, True)

        'order type info column
        sb = New StringBuilder
        sb.Append(_supplier.SupplierTypeString & Environment.NewLine)
        sb.Append(_supplier.Number & Space(1) & _supplier.Name.Trim & Environment.NewLine)
        sb.Append(_supplier.PhoneNumber1)
        DrawTextRectangle(e.Graph, sb.ToString, x, 220, h, StringAlignment.Near, BorderSide.None)

        'soq control header column
        sb = New StringBuilder
        sb.Append("SOQ Control:" & Environment.NewLine)
        sb.Append("MCP:" & Environment.NewLine)
        sb.Append("Order Date:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 80, h, StringAlignment.Near, BorderSide.None, True)

        'soq control info column
        sb = New StringBuilder
        If _supplier.SoqNumber.HasValue Then sb.Append(_supplier.SoqNumber)
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.MinimumOrder.ConstraintString & Environment.NewLine)
        If _supplier.SoqDate.HasValue Then sb.Append(_supplier.SoqDate.Value.ToShortDateString)
        DrawTextRectangle(e.Graph, sb.ToString, x, 200, h, StringAlignment.Near, BorderSide.None)

        'review days
        sb = New StringBuilder
        sb.Append(Environment.NewLine)
        sb.Append("Review Days:" & Environment.NewLine)
        sb.Append("Lead Time:")
        DrawTextRectangle(e.Graph, sb.ToString, x, 100, h, StringAlignment.Far, BorderSide.None, True)

        'review sun
        sb = New StringBuilder
        sb.Append("Sun" & Environment.NewLine)
        If _supplier.ReviewDay0 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime0)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review mon
        sb = New StringBuilder
        sb.Append("Mon" & Environment.NewLine)
        If _supplier.ReviewDay1 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime1)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review tue
        sb = New StringBuilder
        sb.Append("Tue" & Environment.NewLine)
        If _supplier.ReviewDay2 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime2)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review wed
        sb = New StringBuilder
        sb.Append("Wed" & Environment.NewLine)
        If _supplier.ReviewDay3 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime3)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review thu
        sb = New StringBuilder
        sb.Append("Thu" & Environment.NewLine)
        If _supplier.ReviewDay4 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime4)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review fri
        sb = New StringBuilder
        sb.Append("Fri" & Environment.NewLine)
        If _supplier.ReviewDay5 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime5)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

        'review sun
        sb = New StringBuilder
        sb.Append("Sat" & Environment.NewLine)
        If _supplier.ReviewDay6 Then sb.Append("x")
        sb.Append(Environment.NewLine)
        sb.Append(_supplier.LeadTime6)
        DrawTextRectangle(e.Graph, sb.ToString, x, 40, h, StringAlignment.Far, BorderSide.None)

    End Sub

    Private Sub ReportFooter_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        Dim overCount As Integer = 0
        Dim overValue As Decimal = 0
        Dim SoqUnits As Integer = 0
        Dim SoqValue As Decimal = 0
        _supplier.Stocks.TotalsOverShelfCapacity(overCount, overValue, SoqUnits, SoqValue)

        'set variables
        Dim x As Integer = 0
        Dim h As Integer = 30

        'first column
        Dim sb As New StringBuilder
        sb.Append(My.Resources.Strings.PrintNumberSkusAboveCapacity & Environment.NewLine)
        sb.Append(My.Resources.Strings.PrintSoqAboveCapacity)
        DrawTextRectangle(e.Graph, sb.ToString, x, 350, h, StringAlignment.Near, BorderSide.Top Or BorderSide.Bottom Or BorderSide.Left, True)

        'second column
        sb = New StringBuilder
        sb.Append(overCount & Environment.NewLine)
        sb.Append(SoqUnits)
        DrawTextRectangle(e.Graph, sb.ToString, x, 80, h, StringAlignment.Far, BorderSide.Top Or BorderSide.Bottom Or BorderSide.Right)

        'third column
        sb = New StringBuilder
        sb.Append(My.Resources.Strings.PrintTotalValueAboveCapacity & Environment.NewLine)
        sb.Append(My.Resources.Strings.PrintSoqValueAboveCapacity)
        DrawTextRectangle(e.Graph, sb.ToString, x, 350, h, StringAlignment.Far, BorderSide.Top Or BorderSide.Bottom Or BorderSide.Left, True)

        'fourth column
        sb = New StringBuilder
        sb.Append(overValue.ToString("n2") & Environment.NewLine)
        sb.Append(SoqValue.ToString("n2"))
        DrawTextRectangle(e.Graph, sb.ToString, x, 80, h, StringAlignment.Far, BorderSide.Top Or BorderSide.Bottom Or BorderSide.Right)

    End Sub

    Private Sub DrawTextRectangle(ByRef graph As BrickGraphics, ByVal text As String, ByRef x As Integer, ByVal width As Integer, ByVal height As Integer, ByVal align As StringAlignment, ByVal sides As DevExpress.XtraPrinting.BorderSide, Optional ByVal bold As Boolean = False)

        Dim rec As New RectangleF(x, 0, width, height)
        graph.StringFormat = New BrickStringFormat(align)
        If bold Then
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        Else
            graph.Font = New Font("Tahoma", 8.25, FontStyle.Regular)
        End If
        graph.DrawString(text, Color.Black, rec, sides)
        x += width

    End Sub



    Public Sub ShowPreviewDialog()

        _report.ShowPreviewDialog()

    End Sub

    Public Sub Print()
        _report.PrintingSystem.Print()
    End Sub

    Public Sub CreateDocument()
        _report.CreateDocument()
    End Sub

    Public Shared Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _report.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class