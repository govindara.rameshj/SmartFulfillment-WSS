<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrderCreate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblSupplier = New System.Windows.Forms.Label
        Me.lblSoqNumber = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblOrderType = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnSoq = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblMinOrder = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTelephone = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblOrderValue = New System.Windows.Forms.Label
        Me.lblOrderUnits = New System.Windows.Forms.Label
        Me.lblOverUnits = New System.Windows.Forms.Label
        Me.lblOverValue = New System.Windows.Forms.Label
        Me.lblWeight = New System.Windows.Forms.Label
        Me.lblCartons = New System.Windows.Forms.Label
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.grpSupplier = New System.Windows.Forms.GroupBox
        Me.grpTotals = New System.Windows.Forms.GroupBox
        Me.grpDetails = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblPoNumber = New System.Windows.Forms.Label
        Me.uxDateDuePicker = New DevExpress.XtraEditors.DateEdit
        Me.lblDateDue = New System.Windows.Forms.Label
        Me.uxStockGrid = New DevExpress.XtraGrid.GridControl
        Me.uxStockView = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnResetQtys = New System.Windows.Forms.Button
        Me.uxSkuSearchGroup = New System.Windows.Forms.GroupBox
        Me.uxSkuSearchButton = New System.Windows.Forms.Button
        Me.uxSkuSearchText = New System.Windows.Forms.TextBox
        Me.uxSkuSearchLabel = New System.Windows.Forms.Label
        Me.btnStockEnquiry = New System.Windows.Forms.Button
        Me.StatusStrip1.SuspendLayout()
        Me.grpSupplier.SuspendLayout()
        Me.grpTotals.SuspendLayout()
        Me.grpDetails.SuspendLayout()
        CType(Me.uxDateDuePicker.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDateDuePicker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStockGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStockView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxSkuSearchGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Supplier"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "SOQ Number"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSupplier
        '
        Me.lblSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupplier.Location = New System.Drawing.Point(110, 36)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(228, 20)
        Me.lblSupplier.TabIndex = 3
        Me.lblSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSoqNumber
        '
        Me.lblSoqNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSoqNumber.Location = New System.Drawing.Point(94, 36)
        Me.lblSoqNumber.Name = "lblSoqNumber"
        Me.lblSoqNumber.Size = New System.Drawing.Size(199, 20)
        Me.lblSoqNumber.TabIndex = 3
        Me.lblSoqNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 20)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "&Due Date"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOrderType
        '
        Me.lblOrderType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOrderType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderType.Location = New System.Drawing.Point(110, 16)
        Me.lblOrderType.Name = "lblOrderType"
        Me.lblOrderType.Size = New System.Drawing.Size(228, 20)
        Me.lblOrderType.TabIndex = 1
        Me.lblOrderType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(911, 526)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F10 Cancel"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPrint.Location = New System.Drawing.Point(829, 526)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "F9 Print SOQ"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnSoq
        '
        Me.btnSoq.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSoq.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSoq.Location = New System.Drawing.Point(8, 526)
        Me.btnSoq.Name = "btnSoq"
        Me.btnSoq.Size = New System.Drawing.Size(76, 39)
        Me.btnSoq.TabIndex = 1
        Me.btnSoq.Text = "F3 Accept SOQs"
        Me.btnSoq.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSave.Location = New System.Drawing.Point(172, 526)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 39)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblMinOrder
        '
        Me.lblMinOrder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMinOrder.Location = New System.Drawing.Point(94, 56)
        Me.lblMinOrder.Name = "lblMinOrder"
        Me.lblMinOrder.Size = New System.Drawing.Size(199, 20)
        Me.lblMinOrder.TabIndex = 5
        Me.lblMinOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(6, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(82, 20)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "MCP"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(98, 20)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Order Type"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 20)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Telephone"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelephone
        '
        Me.lblTelephone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTelephone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelephone.Location = New System.Drawing.Point(110, 56)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(228, 20)
        Me.lblTelephone.TabIndex = 5
        Me.lblTelephone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(175, 20)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Units/Value Of Order"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(175, 20)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Units/Value Over Shelf Cap"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(175, 20)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Order Weight (Kg)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 76)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(175, 20)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Order Number of Cartons"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOrderValue
        '
        Me.lblOrderValue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOrderValue.Location = New System.Drawing.Point(248, 16)
        Me.lblOrderValue.Name = "lblOrderValue"
        Me.lblOrderValue.Size = New System.Drawing.Size(70, 20)
        Me.lblOrderValue.TabIndex = 2
        Me.lblOrderValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrderUnits
        '
        Me.lblOrderUnits.Location = New System.Drawing.Point(187, 16)
        Me.lblOrderUnits.Name = "lblOrderUnits"
        Me.lblOrderUnits.Size = New System.Drawing.Size(55, 20)
        Me.lblOrderUnits.TabIndex = 1
        Me.lblOrderUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOverUnits
        '
        Me.lblOverUnits.Location = New System.Drawing.Point(187, 36)
        Me.lblOverUnits.Name = "lblOverUnits"
        Me.lblOverUnits.Size = New System.Drawing.Size(55, 20)
        Me.lblOverUnits.TabIndex = 4
        Me.lblOverUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOverValue
        '
        Me.lblOverValue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOverValue.Location = New System.Drawing.Point(248, 36)
        Me.lblOverValue.Name = "lblOverValue"
        Me.lblOverValue.Size = New System.Drawing.Size(70, 20)
        Me.lblOverValue.TabIndex = 5
        Me.lblOverValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWeight
        '
        Me.lblWeight.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWeight.Location = New System.Drawing.Point(187, 56)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(131, 20)
        Me.lblWeight.TabIndex = 7
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCartons
        '
        Me.lblCartons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCartons.Location = New System.Drawing.Point(187, 76)
        Me.lblCartons.Name = "lblCartons"
        Me.lblCartons.Size = New System.Drawing.Size(131, 20)
        Me.lblCartons.TabIndex = 9
        Me.lblCartons.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 568)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(996, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(981, 17)
        Me.StatusLabel.Spring = True
        Me.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpSupplier
        '
        Me.grpSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSupplier.Controls.Add(Me.Label8)
        Me.grpSupplier.Controls.Add(Me.Label1)
        Me.grpSupplier.Controls.Add(Me.lblSupplier)
        Me.grpSupplier.Controls.Add(Me.lblOrderType)
        Me.grpSupplier.Controls.Add(Me.Label5)
        Me.grpSupplier.Controls.Add(Me.lblTelephone)
        Me.grpSupplier.Location = New System.Drawing.Point(8, 8)
        Me.grpSupplier.Name = "grpSupplier"
        Me.grpSupplier.Size = New System.Drawing.Size(344, 103)
        Me.grpSupplier.TabIndex = 5
        Me.grpSupplier.TabStop = False
        Me.grpSupplier.Text = "Supplier Details"
        '
        'grpTotals
        '
        Me.grpTotals.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpTotals.Controls.Add(Me.Label6)
        Me.grpTotals.Controls.Add(Me.lblCartons)
        Me.grpTotals.Controls.Add(Me.Label7)
        Me.grpTotals.Controls.Add(Me.lblWeight)
        Me.grpTotals.Controls.Add(Me.Label9)
        Me.grpTotals.Controls.Add(Me.lblOverUnits)
        Me.grpTotals.Controls.Add(Me.Label10)
        Me.grpTotals.Controls.Add(Me.lblOverValue)
        Me.grpTotals.Controls.Add(Me.lblOrderValue)
        Me.grpTotals.Controls.Add(Me.lblOrderUnits)
        Me.grpTotals.Location = New System.Drawing.Point(663, 8)
        Me.grpTotals.Name = "grpTotals"
        Me.grpTotals.Size = New System.Drawing.Size(324, 103)
        Me.grpTotals.TabIndex = 7
        Me.grpTotals.TabStop = False
        Me.grpTotals.Text = "Order Totals"
        '
        'grpDetails
        '
        Me.grpDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDetails.Controls.Add(Me.Label3)
        Me.grpDetails.Controls.Add(Me.lblPoNumber)
        Me.grpDetails.Controls.Add(Me.Label4)
        Me.grpDetails.Controls.Add(Me.Label2)
        Me.grpDetails.Controls.Add(Me.lblSoqNumber)
        Me.grpDetails.Controls.Add(Me.Label14)
        Me.grpDetails.Controls.Add(Me.lblMinOrder)
        Me.grpDetails.Controls.Add(Me.uxDateDuePicker)
        Me.grpDetails.Controls.Add(Me.lblDateDue)
        Me.grpDetails.Location = New System.Drawing.Point(358, 8)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(299, 103)
        Me.grpDetails.TabIndex = 6
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Order Details"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "PO Number"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPoNumber
        '
        Me.lblPoNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPoNumber.Location = New System.Drawing.Point(94, 16)
        Me.lblPoNumber.Name = "lblPoNumber"
        Me.lblPoNumber.Size = New System.Drawing.Size(199, 20)
        Me.lblPoNumber.TabIndex = 1
        Me.lblPoNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxDateDuePicker
        '
        Me.uxDateDuePicker.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDateDuePicker.EditValue = New Date(2009, 5, 12, 10, 4, 0, 266)
        Me.uxDateDuePicker.Location = New System.Drawing.Point(97, 76)
        Me.uxDateDuePicker.Name = "uxDateDuePicker"
        Me.uxDateDuePicker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.uxDateDuePicker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxDateDuePicker.Properties.NullDate = ""
        Me.uxDateDuePicker.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.uxDateDuePicker.Size = New System.Drawing.Size(110, 20)
        Me.uxDateDuePicker.TabIndex = 7
        '
        'lblDateDue
        '
        Me.lblDateDue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDateDue.Location = New System.Drawing.Point(94, 76)
        Me.lblDateDue.Name = "lblDateDue"
        Me.lblDateDue.Size = New System.Drawing.Size(199, 20)
        Me.lblDateDue.TabIndex = 8
        Me.lblDateDue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxStockGrid
        '
        Me.uxStockGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxStockGrid.Location = New System.Drawing.Point(8, 166)
        Me.uxStockGrid.MainView = Me.uxStockView
        Me.uxStockGrid.Name = "uxStockGrid"
        Me.uxStockGrid.Size = New System.Drawing.Size(979, 354)
        Me.uxStockGrid.TabIndex = 0
        Me.uxStockGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxStockView, Me.GridView2})
        '
        'uxStockView
        '
        Me.uxStockView.Appearance.BandPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxStockView.Appearance.BandPanel.Options.UseFont = True
        Me.uxStockView.Appearance.BandPanel.Options.UseTextOptions = True
        Me.uxStockView.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxStockView.Appearance.BandPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxStockView.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxStockView.BandPanelRowHeight = 35
        Me.uxStockView.GridControl = Me.uxStockGrid
        Me.uxStockView.Name = "uxStockView"
        Me.uxStockView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp
        Me.uxStockView.OptionsView.ShowDetailButtons = False
        Me.uxStockView.OptionsView.ShowGroupPanel = False
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.uxStockGrid
        Me.GridView2.Name = "GridView2"
        '
        'btnResetQtys
        '
        Me.btnResetQtys.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnResetQtys.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnResetQtys.Location = New System.Drawing.Point(90, 526)
        Me.btnResetQtys.Name = "btnResetQtys"
        Me.btnResetQtys.Size = New System.Drawing.Size(76, 39)
        Me.btnResetQtys.TabIndex = 9
        Me.btnResetQtys.Text = "F4 Reset Order Qtys"
        Me.btnResetQtys.UseVisualStyleBackColor = True
        '
        'uxSkuSearchGroup
        '
        Me.uxSkuSearchGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSkuSearchGroup.Controls.Add(Me.uxSkuSearchButton)
        Me.uxSkuSearchGroup.Controls.Add(Me.uxSkuSearchText)
        Me.uxSkuSearchGroup.Controls.Add(Me.uxSkuSearchLabel)
        Me.uxSkuSearchGroup.Location = New System.Drawing.Point(8, 117)
        Me.uxSkuSearchGroup.Name = "uxSkuSearchGroup"
        Me.uxSkuSearchGroup.Size = New System.Drawing.Size(344, 43)
        Me.uxSkuSearchGroup.TabIndex = 10
        Me.uxSkuSearchGroup.TabStop = False
        Me.uxSkuSearchGroup.Text = "Search for SKU in Order"
        '
        'uxSkuSearchButton
        '
        Me.uxSkuSearchButton.Location = New System.Drawing.Point(263, 14)
        Me.uxSkuSearchButton.Name = "uxSkuSearchButton"
        Me.uxSkuSearchButton.Size = New System.Drawing.Size(75, 23)
        Me.uxSkuSearchButton.TabIndex = 11
        Me.uxSkuSearchButton.Text = "Search"
        Me.uxSkuSearchButton.UseVisualStyleBackColor = True
        '
        'uxSkuSearchText
        '
        Me.uxSkuSearchText.Location = New System.Drawing.Point(113, 16)
        Me.uxSkuSearchText.MaxLength = 6
        Me.uxSkuSearchText.Name = "uxSkuSearchText"
        Me.uxSkuSearchText.Size = New System.Drawing.Size(75, 20)
        Me.uxSkuSearchText.TabIndex = 1
        '
        'uxSkuSearchLabel
        '
        Me.uxSkuSearchLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxSkuSearchLabel.Location = New System.Drawing.Point(6, 16)
        Me.uxSkuSearchLabel.Name = "uxSkuSearchLabel"
        Me.uxSkuSearchLabel.Size = New System.Drawing.Size(98, 20)
        Me.uxSkuSearchLabel.TabIndex = 0
        Me.uxSkuSearchLabel.Text = "SKU Number"
        Me.uxSkuSearchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnStockEnquiry.Location = New System.Drawing.Point(254, 526)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(76, 39)
        Me.btnStockEnquiry.TabIndex = 11
        Me.btnStockEnquiry.Text = "F8 Stock Enquiry"
        Me.btnStockEnquiry.UseVisualStyleBackColor = True
        '
        'OrderCreate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(996, 590)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.uxSkuSearchGroup)
        Me.Controls.Add(Me.btnResetQtys)
        Me.Controls.Add(Me.uxStockGrid)
        Me.Controls.Add(Me.grpDetails)
        Me.Controls.Add(Me.grpTotals)
        Me.Controls.Add(Me.grpSupplier)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnSoq)
        Me.Controls.Add(Me.btnSave)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(956, 543)
        Me.Name = "OrderCreate"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Create Order"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.grpSupplier.ResumeLayout(False)
        Me.grpTotals.ResumeLayout(False)
        Me.grpDetails.ResumeLayout(False)
        CType(Me.uxDateDuePicker.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDateDuePicker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStockGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStockView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxSkuSearchGroup.ResumeLayout(False)
        Me.uxSkuSearchGroup.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblSupplier As System.Windows.Forms.Label
    Friend WithEvents lblSoqNumber As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblOrderType As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnSoq As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblMinOrder As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTelephone As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblOrderValue As System.Windows.Forms.Label
    Friend WithEvents lblOrderUnits As System.Windows.Forms.Label
    Friend WithEvents lblOverUnits As System.Windows.Forms.Label
    Friend WithEvents lblOverValue As System.Windows.Forms.Label
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents lblCartons As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents grpSupplier As System.Windows.Forms.GroupBox
    Friend WithEvents grpTotals As System.Windows.Forms.GroupBox
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Friend WithEvents lblDateDue As System.Windows.Forms.Label
    Friend WithEvents uxDateDuePicker As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPoNumber As System.Windows.Forms.Label
    Friend WithEvents uxStockGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnResetQtys As System.Windows.Forms.Button
    Friend WithEvents uxSkuSearchGroup As System.Windows.Forms.GroupBox
    Friend WithEvents uxSkuSearchButton As System.Windows.Forms.Button
    Friend WithEvents uxSkuSearchText As System.Windows.Forms.TextBox
    Friend WithEvents uxSkuSearchLabel As System.Windows.Forms.Label
    Friend WithEvents uxStockView As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents btnStockEnquiry As System.Windows.Forms.Button
End Class
