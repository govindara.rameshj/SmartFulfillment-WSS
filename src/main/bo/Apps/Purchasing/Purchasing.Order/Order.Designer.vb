<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Order
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Order))
        Me.btnOrder = New System.Windows.Forms.Button
        Me.btnAuto = New System.Windows.Forms.Button
        Me.btnSoqPrint = New System.Windows.Forms.Button
        Me.btnSoq = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.grpManual = New System.Windows.Forms.GroupBox
        Me.lueSupplier = New DevExpress.XtraEditors.LookUpEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkDirect = New System.Windows.Forms.CheckBox
        Me.chkWarehouse = New System.Windows.Forms.CheckBox
        Me.grpType = New System.Windows.Forms.GroupBox
        Me.chkSchedule = New System.Windows.Forms.CheckBox
        Me.btnReset = New System.Windows.Forms.Button
        Me.pnlButtons = New System.Windows.Forms.Panel
        Me.btnStockEnquiry = New System.Windows.Forms.Button
        Me.btnSelectAll = New System.Windows.Forms.Button
        Me.xgridSupplier = New DevExpress.XtraGrid.GridControl
        Me.xviewSupplier = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grpManual.SuspendLayout()
        CType(Me.lueSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpType.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        CType(Me.xgridSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOrder
        '
        Me.btnOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnOrder.Location = New System.Drawing.Point(164, 0)
        Me.btnOrder.Name = "btnOrder"
        Me.btnOrder.Size = New System.Drawing.Size(76, 39)
        Me.btnOrder.TabIndex = 2
        Me.btnOrder.Text = "F6 Create Order"
        Me.btnOrder.UseVisualStyleBackColor = True
        '
        'btnAuto
        '
        Me.btnAuto.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAuto.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAuto.Location = New System.Drawing.Point(246, 0)
        Me.btnAuto.Name = "btnAuto"
        Me.btnAuto.Size = New System.Drawing.Size(76, 39)
        Me.btnAuto.TabIndex = 3
        Me.btnAuto.Tag = "Auto"
        Me.btnAuto.Text = "F7 Auto Order"
        Me.btnAuto.UseVisualStyleBackColor = True
        '
        'btnSoqPrint
        '
        Me.btnSoqPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSoqPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSoqPrint.Location = New System.Drawing.Point(654, 0)
        Me.btnSoqPrint.Name = "btnSoqPrint"
        Me.btnSoqPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnSoqPrint.TabIndex = 4
        Me.btnSoqPrint.Text = "F9 Print SOQ"
        Me.btnSoqPrint.UseVisualStyleBackColor = True
        '
        'btnSoq
        '
        Me.btnSoq.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSoq.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSoq.Location = New System.Drawing.Point(82, 0)
        Me.btnSoq.Name = "btnSoq"
        Me.btnSoq.Size = New System.Drawing.Size(76, 39)
        Me.btnSoq.TabIndex = 1
        Me.btnSoq.Text = "F5 Calculate SOQ"
        Me.btnSoq.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(821, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'grpManual
        '
        Me.grpManual.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpManual.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpManual.Controls.Add(Me.lueSupplier)
        Me.grpManual.Controls.Add(Me.Label2)
        Me.grpManual.Location = New System.Drawing.Point(224, 3)
        Me.grpManual.Name = "grpManual"
        Me.grpManual.Size = New System.Drawing.Size(673, 81)
        Me.grpManual.TabIndex = 1
        Me.grpManual.TabStop = False
        Me.grpManual.Text = "Manual Selection"
        '
        'lueSupplier
        '
        Me.lueSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lueSupplier.Location = New System.Drawing.Point(82, 19)
        Me.lueSupplier.Name = "lueSupplier"
        Me.lueSupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSupplier.Properties.ImmediatePopup = True
        Me.lueSupplier.Properties.NullText = ""
        Me.lueSupplier.Properties.PopupWidth = 250
        Me.lueSupplier.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.lueSupplier.Size = New System.Drawing.Size(585, 20)
        Me.lueSupplier.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(6, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "&Supplier"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDirect
        '
        Me.chkDirect.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDirect.Location = New System.Drawing.Point(6, 16)
        Me.chkDirect.Margin = New System.Windows.Forms.Padding(0)
        Me.chkDirect.Name = "chkDirect"
        Me.chkDirect.Size = New System.Drawing.Size(206, 20)
        Me.chkDirect.TabIndex = 0
        Me.chkDirect.Text = "&Direct Suppliers"
        Me.chkDirect.UseVisualStyleBackColor = True
        '
        'chkWarehouse
        '
        Me.chkWarehouse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkWarehouse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkWarehouse.Location = New System.Drawing.Point(6, 36)
        Me.chkWarehouse.Margin = New System.Windows.Forms.Padding(0)
        Me.chkWarehouse.Name = "chkWarehouse"
        Me.chkWarehouse.Size = New System.Drawing.Size(206, 20)
        Me.chkWarehouse.TabIndex = 1
        Me.chkWarehouse.Text = "&Warehouse Suppliers"
        Me.chkWarehouse.UseVisualStyleBackColor = True
        '
        'grpType
        '
        Me.grpType.Controls.Add(Me.chkSchedule)
        Me.grpType.Controls.Add(Me.chkWarehouse)
        Me.grpType.Controls.Add(Me.chkDirect)
        Me.grpType.Location = New System.Drawing.Point(3, 3)
        Me.grpType.Name = "grpType"
        Me.grpType.Size = New System.Drawing.Size(215, 81)
        Me.grpType.TabIndex = 0
        Me.grpType.TabStop = False
        Me.grpType.Text = "Supplier Type"
        '
        'chkSchedule
        '
        Me.chkSchedule.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSchedule.Checked = True
        Me.chkSchedule.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSchedule.Location = New System.Drawing.Point(6, 56)
        Me.chkSchedule.Margin = New System.Windows.Forms.Padding(0)
        Me.chkSchedule.Name = "chkSchedule"
        Me.chkSchedule.Size = New System.Drawing.Size(206, 20)
        Me.chkSchedule.TabIndex = 2
        Me.chkSchedule.Text = "&Only Show Scheduled Suppliers"
        Me.chkSchedule.UseVisualStyleBackColor = True
        Me.chkSchedule.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnReset.Location = New System.Drawing.Point(736, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 5
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'pnlButtons
        '
        Me.pnlButtons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlButtons.Controls.Add(Me.btnStockEnquiry)
        Me.pnlButtons.Controls.Add(Me.btnSelectAll)
        Me.pnlButtons.Controls.Add(Me.btnSoqPrint)
        Me.pnlButtons.Controls.Add(Me.btnOrder)
        Me.pnlButtons.Controls.Add(Me.btnSoq)
        Me.pnlButtons.Controls.Add(Me.btnReset)
        Me.pnlButtons.Controls.Add(Me.btnAuto)
        Me.pnlButtons.Location = New System.Drawing.Point(3, 508)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(812, 39)
        Me.pnlButtons.TabIndex = 3
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnStockEnquiry.Location = New System.Drawing.Point(328, 0)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(76, 39)
        Me.btnStockEnquiry.TabIndex = 6
        Me.btnStockEnquiry.Tag = "Auto"
        Me.btnStockEnquiry.Text = "F8 Stock Enquiry"
        Me.btnStockEnquiry.UseVisualStyleBackColor = True
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSelectAll.Location = New System.Drawing.Point(0, 0)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(76, 39)
        Me.btnSelectAll.TabIndex = 0
        Me.btnSelectAll.Text = "F4 Select All"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        '
        'xgridSupplier
        '
        Me.xgridSupplier.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridSupplier.Location = New System.Drawing.Point(3, 90)
        Me.xgridSupplier.MainView = Me.xviewSupplier
        Me.xgridSupplier.Name = "xgridSupplier"
        Me.xgridSupplier.Size = New System.Drawing.Size(894, 412)
        Me.xgridSupplier.TabIndex = 2
        Me.xgridSupplier.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewSupplier})
        '
        'xviewSupplier
        '
        Me.xviewSupplier.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewSupplier.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewSupplier.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewSupplier.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewSupplier.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.xviewSupplier.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewSupplier.GridControl = Me.xgridSupplier
        Me.xviewSupplier.Name = "xviewSupplier"
        Me.xviewSupplier.OptionsBehavior.Editable = False
        Me.xviewSupplier.OptionsDetail.EnableMasterViewMode = False
        Me.xviewSupplier.OptionsDetail.ShowDetailTabs = False
        Me.xviewSupplier.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.xviewSupplier.OptionsSelection.MultiSelect = True
        '
        'Order
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.xgridSupplier)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.grpType)
        Me.Controls.Add(Me.grpManual)
        Me.Controls.Add(Me.btnExit)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "Order"
        Me.Size = New System.Drawing.Size(900, 550)
        Me.grpManual.ResumeLayout(False)
        CType(Me.lueSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpType.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        CType(Me.xgridSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOrder As System.Windows.Forms.Button
    Friend WithEvents btnAuto As System.Windows.Forms.Button
    Friend WithEvents btnSoqPrint As System.Windows.Forms.Button
    Friend WithEvents btnSoq As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents grpManual As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkDirect As System.Windows.Forms.CheckBox
    Friend WithEvents chkWarehouse As System.Windows.Forms.CheckBox
    Friend WithEvents grpType As System.Windows.Forms.GroupBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents lueSupplier As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents chkSchedule As System.Windows.Forms.CheckBox
    Friend WithEvents xgridSupplier As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewSupplier As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents btnStockEnquiry As System.Windows.Forms.Button

End Class
