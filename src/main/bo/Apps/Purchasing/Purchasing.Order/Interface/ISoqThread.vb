﻿Imports Purchasing.Core

Public Interface ISoqThread
    Function ProcessSoq(ByRef suppliers As BindingList(Of Supplier), ByVal postAction As Integer, ByRef soqProgress As Object, ByRef soqCompleted As Object _
                          , ByRef soqCancelled As Object, ByRef numOfItems As Integer, ByRef supplierGrid As DevExpress.XtraGrid.GridControl, ByRef supplierGridView As DevExpress.XtraGrid.Views.Grid.GridView) As Object

    Property StopSingleThread() As Boolean

End Interface
