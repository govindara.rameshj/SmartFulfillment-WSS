﻿Imports TpWickes.Library

Public Class SoqThreadFactory
    Inherits RequirementSwitchFactory(Of ISoqThread)

    Private Const _RequirementSwitch As Integer = -961
    Dim RequirementSwitch As TpWickes.Library.IRequirementRepository

    Public Overrides Function ImplementationA() As ISoqThread
        Return New SoqSingleThreadedNew
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        RequirementSwitch = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(_RequirementSwitch)
    End Function

    Public Overrides Function ImplementationB() As ISoqThread
        Return New SoqMulitiThreadedOld
    End Function
End Class
