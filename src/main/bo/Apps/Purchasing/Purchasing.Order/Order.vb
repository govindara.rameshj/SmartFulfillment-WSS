Imports Purchasing.Core
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System

Public Class Order
    Delegate Sub SoqProgressDelegate(ByVal sup As Supplier, ByVal progress As Integer)
    Delegate Sub SoqCompletedDelegate(ByVal sup As Supplier, ByVal status As Soq.CompletedStatus)
    Delegate Sub SoqCancelledDelegate(ByVal sup As Supplier)
    Private _soqThreadController As SoqThreadController
    Private _suppliers As New BindingList(Of Supplier)
    Private _suppliersSoq As BindingList(Of Supplier)
    Private _daysOpen As Integer
    Private _numberToProcess As Integer = 10
    Private soqThreadProcessInstance As ISoqThread = (New SoqThreadFactory).GetImplementation

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        btnExit.Text = My.Resources.Strings.F12Exit
        btnSoq.Text = My.Resources.Strings.F5CalculateSOQ
        btnAuto.Text = My.Resources.Strings.F6AutoOrder

        'check whether to show auto order button (parameter 207)
        btnAuto.Visible = Parameter.GetBoolean(207)

    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F4 : btnSelectAll.PerformClick()
            Case Keys.F5 : btnSoq.PerformClick()
            Case Keys.F6 : btnOrder.PerformClick()
            Case Keys.F7 : btnAuto.PerformClick()
            Case Keys.F8 : btnStockEnquiry.PerformClick()
            Case Keys.F9 : btnSoqPrint.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            _daysOpen = Store.GetDaysOpen

            'check if from night routine
            If RunParameters.Trim.Length > 0 Then
                'get suppliers requested and show in grid
                Dim parameters() As String = RunParameters.Split(" "c)
                LoadSuppliers(parameters(0))
                chkDirect.Checked = True
                chkWarehouse.Checked = True
                chkSchedule.Checked = False

                'assign suppliers to soq list
                _suppliersSoq = New BindingList(Of Supplier)
                For Each sup As Supplier In _suppliers
                    If sup.SoqStatus = Soq.State.Ordered Then Continue For
                    _suppliersSoq.Add(sup)
                Next

                'exit if no suppliers added
                If _suppliersSoq.Count = 0 Then
                    FindForm.Close()
                    Exit Sub
                End If

                'instantiate thread controller
                If parameters.GetUpperBound(0) > 1 Then
                    Select Case parameters(1).ToLower
                        Case "autoorder" : SoqStartController(SoqThreadController.Action.OrderAndExit)
                        Case "print" : SoqStartController(SoqThreadController.Action.PrintAndExit)
                        Case Else : SoqStartController(SoqThreadController.Action.SoqAndExit)
                    End Select
                Else
                    SoqStartController(SoqThreadController.Action.SoqAndExit)
                End If

                Exit Sub
            End If

            DisplayProgress(0, My.Resources.Strings.GetSuppliers)
            LoadSuppliers()

            DisplayProgress(50, My.Resources.Strings.InitialiseControls)
            lueSuppliers_Initialise()

            DisplayProgress(100)
            DisplayProgress()

            lueSupplier.Focus()
            'lueSupplier.ShowPopup()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub LoadSuppliers(Optional ByVal parameter As String = "")

        Select Case parameter.ToLower
            Case "all", "" : _suppliers = Purchasing.Core.Supplier.GetAllForOrder("A")
            Case "direct" : _suppliers = Purchasing.Core.Supplier.GetAllForOrder("D")
            Case "warehouse" : _suppliers = Purchasing.Core.Supplier.GetAllForOrder("W")
            Case Else

                Dim tempSuppliers As BindingList(Of Supplier) = Supplier.GetAllForOrder("A")
                For Each number As String In parameter.Split(","c)
                    For Each sup As Supplier In tempSuppliers
                        If sup.Number = number Then
                            _suppliers.Add(sup)
                            Continue For
                        End If
                    Next
                Next
        End Select

        'set up datasource and add unbound columns
        xgridSupplier.DataSource = _suppliers
        Dim colDateDue As GridColumn = xviewSupplier.Columns.AddField(Unbounds.DateDue)

        'add progress bar to grid and bind
        Dim riProgress As New RepositoryItemProgressBar
        xgridSupplier.RepositoryItems.Add(riProgress)

        'set up columns
        For Each col As GridColumn In xviewSupplier.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Supplier) f.Number)
                    col.VisibleIndex = 0
                Case GetPropertyName(Function(f As Supplier) f.Name)
                    col.VisibleIndex = 1
                Case GetPropertyName(Function(f As Supplier) f.SupplierTypeString)
                    col.VisibleIndex = 2
                    col.Caption = My.Resources.ColumnHeaders.SoqStatus
                Case GetPropertyName(Function(f As Supplier) f.Progress)
                    col.VisibleIndex = 3
                    col.ColumnEdit = riProgress
                    col.Visible = False
                Case GetPropertyName(Function(f As Supplier) f.SoqStatusString)
                    col.VisibleIndex = 4
                    col.Caption = My.Resources.ColumnHeaders.SoqStatus
                Case GetPropertyName(Function(f As Supplier) f.SoqNumber)
                    col.VisibleIndex = 5
                    col.Caption = My.Resources.ColumnHeaders.SoqNumber
                Case GetPropertyName(Function(f As Supplier) f.SoqDate)
                    col.VisibleIndex = 6
                    col.Caption = My.Resources.ColumnHeaders.SoqDate
                Case Unbounds.DateDue
                    col.VisibleIndex = 7
                    col.UnboundType = DevExpress.Data.UnboundColumnType.DateTime
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                    col.DisplayFormat.FormatString = "dd/MM/yyyy"
                Case GetPropertyName(Function(f As Supplier) f.DateLastOrdered)
                    col.VisibleIndex = 8
                Case Else
                    col.Visible = False
            End Select
        Next

        xviewSupplier.BestFitColumns()
        CheckedChanged()

    End Sub

    Private Sub ApplyFilter(ByVal filter As String)

        'apply type filter
        xviewSupplier.ActiveFilterString = filter
        xviewSupplier.RefreshData()
        If xviewSupplier.RowCount > 0 Then
            xviewSupplier.SelectRow(0)
            xviewSupplier.Focus()
            pnlButtons.Visible = True
        Else
            pnlButtons.Visible = False
        End If

    End Sub

    Private Sub xviewSupplier_CustomUnboundColumnData(ByVal sender As Object, ByVal e As CustomColumnDataEventArgs) Handles xviewSupplier.CustomUnboundColumnData

        If e.RowHandle = GridControl.InvalidRowHandle Then Exit Sub
        Dim sup As Supplier = _suppliers(e.ListSourceRowIndex)

        Select Case e.Column.FieldName
            Case Unbounds.DateDue : e.Value = sup.DateOrderDue(_daysOpen)
        End Select

    End Sub

    ''' <summary>
    ''' Filter data based on user choice
    ''' </summary>
    ''' <history>
    ''' <updated author="Partha Dutta" date="31/08/2010">
    ''' Referral 271B
    ''' 
    ''' Filtering code rewritten to fix issues in this referral
    ''' Existing procedure commented out and kept for reference
    ''' </updated>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub CheckedChanged() Handles chkDirect.CheckedChanged, chkWarehouse.CheckedChanged, chkSchedule.CheckedChanged
        Dim strbFilter As New StringBuilder()
        Dim strbDirectFilter As New StringBuilder()
        Dim strbWarehouseFilter As New StringBuilder()

        Try
            Cursor = Cursors.WaitCursor

            DisplayStatus()
            xviewSupplier.ClearColumnsFilter()


            'manual selection enabled/disabled
            If chkDirect.Checked = True Or chkWarehouse.Checked = True Then
                grpManual.Visible = False
                chkSchedule.Visible = True
            Else
                grpManual.Visible = True
                chkSchedule.Visible = False
            End If

            'direct suppliers
            If chkDirect.Checked = True Then
                strbDirectFilter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] = ''")
                'only show scheduled suppliers
                If chkSchedule.Checked Then
                    strbDirectFilter.Append(" AND [")
                    Select Case Now.DayOfWeek
                        Case DayOfWeek.Sunday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay0))
                        Case DayOfWeek.Monday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay1))
                        Case DayOfWeek.Tuesday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay2))
                        Case DayOfWeek.Wednesday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay3))
                        Case DayOfWeek.Thursday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay4))
                        Case DayOfWeek.Friday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay5))
                        Case DayOfWeek.Saturday : strbDirectFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay6))
                    End Select
                    strbDirectFilter.Append("] = True ")
                End If
            End If

            'warehouse suppliers
            If chkWarehouse.Checked = True Then
                strbWarehouseFilter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] <> ''")
                'only show scheduled suppliers
                If chkSchedule.Checked Then
                    strbWarehouseFilter.Append(" AND [")
                    Select Case Now.DayOfWeek
                        Case DayOfWeek.Sunday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay1))
                        Case DayOfWeek.Monday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay2))
                        Case DayOfWeek.Tuesday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay3))
                        Case DayOfWeek.Wednesday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay4))
                        Case DayOfWeek.Thursday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay5))
                        Case DayOfWeek.Friday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay6))
                        Case DayOfWeek.Saturday : strbWarehouseFilter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay0))
                    End Select
                    strbWarehouseFilter.Append("] = True ")
                End If
            End If

            If strbDirectFilter.Length <> 0 And strbWarehouseFilter.Length <> 0 Then
                'both filters selected
                strbFilter.Append("(")
                strbFilter.Append(strbDirectFilter.ToString)
                strbFilter.Append(") Or (")
                strbFilter.Append(strbWarehouseFilter.ToString)
                strbFilter.Append(")")
            Else
                'either or no filters selected
                If strbDirectFilter.Length <> 0 Then
                    strbFilter = strbDirectFilter
                End If
                If strbWarehouseFilter.Length <> 0 Then

                    strbFilter = strbWarehouseFilter
                End If
                'no filter selected
                If strbDirectFilter.Length = 0 And strbWarehouseFilter.Length = 0 Then
                    strbFilter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] is null")
                End If
            End If

            ApplyFilter(strbFilter.ToString)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    'Private Sub CheckedChanged() Handles chkDirect.CheckedChanged, chkWarehouse.CheckedChanged, chkSchedule.CheckedChanged

    '    Try
    '        DisplayStatus()
    '        Cursor = Cursors.WaitCursor

    '        xviewSupplier.ClearColumnsFilter()
    '        Dim filter As New StringBuilder()

    '        'check if direct checked
    '        Dim directWarehouse As Boolean = False
    '        If chkDirect.Checked Then
    '            filter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] = '' ")
    '            directWarehouse = True
    '        End If

    '        'check if warehouse checked
    '        If chkWarehouse.Checked Then
    '            If filter.Length > 0 Then filter.Append(" OR ")
    '            filter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] <> '' ")
    '            directWarehouse = True
    '        End If


    '        'check if schedule checked
    '        If directWarehouse AndAlso chkSchedule.Checked Then
    '            filter.Append("AND [")
    '            Select Case Now.DayOfWeek
    '                Case DayOfWeek.Sunday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay1))
    '                Case DayOfWeek.Monday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay2))
    '                Case DayOfWeek.Tuesday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay3))
    '                Case DayOfWeek.Wednesday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay4))
    '                Case DayOfWeek.Thursday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay5))
    '                Case DayOfWeek.Friday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay6))
    '                Case DayOfWeek.Saturday : filter.Append(GetPropertyName(Function(f As Supplier) f.ReviewDay0))
    '            End Select
    '            filter.Append("] = true ")
    '        End If

    '        'if neither direct or warehouse then enable manual selection
    '        chkSchedule.Visible = directWarehouse
    '        grpManual.Visible = Not directWarehouse

    '        'check if filter is set to anything
    '        If filter.Length = 0 Then
    '            filter.Append("[" & GetPropertyName(Function(f As Supplier) f.BbcNumber) & "] is null")
    '        End If

    '        ApplyFilter(filter.ToString)

    '    Finally
    '        Cursor = Cursors.Default
    '    End Try

    'End Sub

    Private Sub lueSuppliers_Initialise()

        lueSupplier.Properties.DataSource = Supplier.GetAll
        lueSupplier.Properties.ValueMember = GetPropertyName(Function(f As Supplier) f.Number)
        lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Name)

        'set up columns
        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Number)))
        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Name)))
        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Alpha)))
        lueSupplier.Properties.AutoSearchColumnIndex = 0
        lueSupplier.Properties.SortColumnIndex = 1
        lueSupplier.Properties.BestFit()

    End Sub

    Private Sub lueSuppliers_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lueSupplier.KeyPress

        Select Case True
            Case e.KeyChar = ChrW(Keys.Enter)
                If lueSupplier.EditValue Is Nothing Then Exit Sub

                'add supplier number to filter
                Dim filter As New StringBuilder(xviewSupplier.ActiveFilterString)
                If filter.Length > 0 Then filter.Append(" OR ")
                filter.Append(String.Format("[{0}] = '{1}'", GetPropertyName(Function(f As Supplier) f.Number), lueSupplier.EditValue))

                ApplyFilter(filter.ToString)

            Case IsNumeric(e.KeyChar)
                lueSupplier.Properties.AutoSearchColumnIndex = 0
                lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Number)
            Case Else
                lueSupplier.Properties.AutoSearchColumnIndex = 1
                lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Name)
        End Select

    End Sub



    Private Sub SoqProgress(ByVal sup As Supplier, ByVal progress As Integer)

        If xgridSupplier.InvokeRequired Then
            xgridSupplier.Invoke(New SoqProgressDelegate(AddressOf SoqProgress), New Object() {sup, progress})
        Else
            sup.Progress = progress

            'find row handle and update progress
            Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
            For rowHandle As Integer = 0 To xviewSupplier.DataRowCount - 1
                If CStr(xviewSupplier.GetRowCellValue(rowHandle, colNumber)) = sup.Number Then
                    xviewSupplier.RefreshRow(rowHandle)
                    Exit Sub
                End If
            Next
        End If

    End Sub

    Private Sub SoqCompleted(ByVal sup As Supplier, ByVal status As Soq.CompletedStatus)

        If xgridSupplier.InvokeRequired Then
            xgridSupplier.Invoke(New SoqCompletedDelegate(AddressOf SoqCompleted), New Object() {sup, status})
        Else
            sup.Progress = 100

            'find row handle and update progress
            Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
            For rowHandle As Integer = 0 To xviewSupplier.DataRowCount - 1
                If CStr(xviewSupplier.GetRowCellValue(rowHandle, colNumber)) = sup.Number Then
                    xviewSupplier.RefreshRow(rowHandle)
                    Exit For
                End If
            Next

            'check if need to order
            Select Case _soqThreadController.PostAction
                Case SoqThreadController.Action.OpenOrder : OpenOrder(sup)
                Case SoqThreadController.Action.Order : AutoOrder(sup)
                Case SoqThreadController.Action.OrderAndExit : AutoOrder(sup)
            End Select

            'remove from soq collection
            _suppliersSoq.Remove(sup)

            If _soqThreadController.Completed Then


                'check if anymore suppliers to process
                If _suppliersSoq.Count = 0 Then
                    btnExit.Text = My.Resources.Strings.F12Exit
                    btnSoq.Enabled = True
                    btnOrder.Enabled = True
                    btnAuto.Enabled = True
                    xviewSupplier.Columns(GetPropertyName(Function(f As Supplier) f.Progress)).Visible = False

                    Select Case _soqThreadController.PostAction
                        Case SoqThreadController.Action.OrderAndExit : FindForm.Close()
                        Case SoqThreadController.Action.SoqAndExit : FindForm.Close()
                        Case SoqThreadController.Action.PrintAndExit
                            PrintSoqs(_suppliers, True)
                            FindForm.Close()
                    End Select

                Else
                    SoqStartController(_soqThreadController.PostAction)
                End If

            End If
        End If

    End Sub

    Private Sub SoqCancelled(ByVal sup As Supplier)

        If xgridSupplier.InvokeRequired Then
            xgridSupplier.Invoke(New SoqCancelledDelegate(AddressOf SoqCancelled), New Object() {sup})
        Else
            SoqCompleted(sup, Soq.CompletedStatus.Cancelled)
        End If

    End Sub
    Private Sub SoqStartController(ByVal postAction As SoqThreadController.Action)

        'if no suppliers then exit
        If _suppliersSoq.Count = 0 Then Exit Sub

        btnExit.Text = My.Resources.Strings.F12Cancel
        btnExit.Focus()
        btnSoq.Enabled = False
        btnAuto.Enabled = False
        btnOrder.Enabled = False
        xviewSupplier.Columns(GetPropertyName(Function(f As Supplier) f.Progress)).Visible = True

        'create new thread for each supplier and add to controller
        _soqThreadController = New SoqThreadController(postAction)

        For index As Integer = 0 To Math.Min(_suppliersSoq.Count, _numberToProcess) - 1
            Dim soq As New SoqThread(_suppliersSoq(index))
            soq.progressDelegate = AddressOf SoqProgress
            soq.endDelegate = AddressOf SoqCompleted
            soq.cancelledDelegate = AddressOf SoqCancelled
            _soqThreadController.Add(soq)
        Next

        _soqThreadController.Start()

    End Sub

    Private Sub SoqStartControllerSoqButtonClick(ByVal postAction As SoqThreadController.Action)

        'if no suppliers then exit

        Dim _suppliersSoqList As New BindingList(Of Supplier)
        If _suppliersSoq.Count = 0 Then Exit Sub

        btnExit.Text = My.Resources.Strings.F12Cancel
        btnExit.Focus()
        btnSoq.Enabled = False
        btnAuto.Enabled = False
        btnOrder.Enabled = False
        xviewSupplier.Columns(GetPropertyName(Function(f As Supplier) f.Progress)).Visible = True

        With soqThreadProcessInstance
            _soqThreadController = CType(.ProcessSoq(_suppliersSoq, postAction, SoqProgressItems, SoqCompletedItems, SoqCancelledItems, SoqNumberOfItems, SupplierGrid, SupplierGridView), SoqThreadController)


            If .GetType.FullName = My.Resources.SoqSingleThreaded Then
                SoqSingleThreadButtonEnable(_suppliersSoq)
            End If
        End With

    End Sub


    Private Sub OpenOrder(ByVal sup As Supplier)

        Using CreateOrder As New OrderCreate(sup, UserId, WorkstationId, _daysOpen)
            If CreateOrder.ShowDialog() = DialogResult.OK Then
                'find row handle and update progress
                Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
                For rowHandle As Integer = 0 To xviewSupplier.DataRowCount - 1
                    If CStr(xviewSupplier.GetRowCellValue(rowHandle, colNumber)) = sup.Number Then
                        xviewSupplier.RefreshData()
                        Exit Sub
                    End If
                Next
            End If
        End Using

    End Sub

    Private Sub AutoOrder(ByVal sup As Supplier)

        Using CreateOrder As New OrderCreate(sup, UserId, WorkstationId, _daysOpen)
            If CreateOrder.OrderSave(True) Then
                'find row handle and update progress
                Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
                For rowHandle As Integer = 0 To xviewSupplier.DataRowCount - 1
                    If CStr(xviewSupplier.GetRowCellValue(rowHandle, colNumber)) = sup.Number Then
                        xviewSupplier.RefreshRow(rowHandle)
                        Exit Sub
                    End If
                Next
            End If
        End Using

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Sub PrintSoqs(ByVal suppliers As BindingList(Of Supplier), Optional ByVal straightToPrinter As Boolean = False)

        'check any suppliers in list
        If suppliers.Count = 0 Then Exit Sub

        Dim storeIdName As String = Store.GetIdName
        Dim userIdName As String = User.GetUserIdName(UserId, IncludeExternalIds:=True)

        Dim reports As New List(Of SoqPrint)
        For Each sup As Supplier In suppliers
            reports.Add(New SoqPrint(sup, storeIdName, userIdName, WorkstationId))
        Next

        Dim firstReport As SoqPrint = reports(0)

        'add other reports pages to first report
        If reports.Count > 1 Then
            firstReport.Report.CreateDocument()
            For index As Integer = 1 To reports.Count - 1
                reports(index).Report.CreateDocument()
                firstReport.Report.PrintingSystem.Pages.AddRange(reports(index).Report.PrintingSystem.Pages)
            Next
        End If

        If straightToPrinter Then
            firstReport.Print()
        Else
            firstReport.ShowPreviewDialog()
        End If

    End Sub



    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click

        'check whether to select/deselect
        If xviewSupplier.SelectedRowsCount = xviewSupplier.RowCount Then
            xviewSupplier.ClearSelection()
            xviewSupplier.SelectRow(0)
        Else
            xviewSupplier.SelectRows(0, xviewSupplier.RowCount - 1)
        End If

    End Sub

    Private Sub btnSOQPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSoqPrint.Click

        Dim suppliers As New BindingList(Of Supplier)
        For Each rowHandle As Integer In xviewSupplier.GetSelectedRows
            Dim sup As Supplier = _suppliers(xviewSupplier.GetDataSourceRowIndex(rowHandle))
            suppliers.Add(sup)
        Next

        PrintSoqs(suppliers)

    End Sub

    Private Sub btnSoq_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAuto.Click, btnSoq.Click

        'reset all supplier progress fields
        For Each sup As Supplier In _suppliers
            sup.Progress = 0
        Next

        'add selected valid suppliers to soq collection ready to start threads
        _suppliersSoq = New BindingList(Of Supplier)
        For Each rowHandle As Integer In xviewSupplier.GetSelectedRows
            Dim sup As Supplier = _suppliers(xviewSupplier.GetDataSourceRowIndex(rowHandle))
            'If sup.SoqStatus = Soq.State.Ordered Then Continue For
            _suppliersSoq.Add(sup)
        Next

        'check if auto order or not
        Select Case CType(sender, Button).Name
            Case btnAuto.Name : SoqStartController(SoqThreadController.Action.Order)
            Case btnSoq.Name : SoqStartControllerSoqButtonClick(SoqThreadController.Action.None)
        End Select

    End Sub

    Private Sub btnOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOrder.Click

        Dim sup As Supplier = _suppliers(xviewSupplier.GetDataSourceRowIndex(xviewSupplier.FocusedRowHandle))

        'check whether SOQ out of date
        If sup.SoqDate < Now.Date Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoCalculateSoq, Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                'reset supplier progress and add to soq controller
                sup.Progress = 0
                _suppliersSoq = New BindingList(Of Supplier)
                _suppliersSoq.Add(sup)
                SoqStartController(SoqThreadController.Action.OpenOrder)

            Else
                'reset soq instead
                sup.SoqReset()
                OpenOrder(sup)
            End If

        Else
            OpenOrder(sup)

        End If

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        lueSupplier.EditValue = Nothing
        chkDirect.Checked = False
        chkWarehouse.Checked = False
        CheckedChanged()
        DisplayStatus()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Select Case btnExit.Text
            Case My.Resources.Strings.F12Cancel
                btnExit.Text = My.Resources.Strings.F12Exit
                btnSoq.Enabled = True
                btnAuto.Enabled = True
                btnOrder.Enabled = True
                If soqThreadProcessInstance.GetType.FullName = My.Resources.SoqSingleThreaded Then
                    soqThreadProcessInstance.StopSingleThread = True
                Else
                    _soqThreadController.Abort()
                End If
                xviewSupplier.Columns(GetPropertyName(Function(f As Supplier) f.Progress)).Visible = False

            Case Else
                FindForm.Close()
        End Select

    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click
        Dim assemblyName As String = "Stock.Enquiry.dll"
        Dim className As String = "Stock.Enquiry.Enquiry"
        Dim appName As String = "Stock Item Enquiry"
        Dim parameters As String = " (/P=',CFC')"

        Trace.WriteLine("Launching Stock Enquiry")
        '_workstationId = "01"

        Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, 0, 1, 9, parameters, "", False)
        'Dim hostForm As New Oasys.Windows.HostForm(assemblyName, className, appName, 0, 1, 9, parameters, "", False)
        hostForm.ShowDialog()
    End Sub
    Public ReadOnly Property SoqProgressItems() As SoqProgressDelegate
        Get
            Return AddressOf SoqProgress
        End Get
    End Property
    Public ReadOnly Property SoqCompletedItems() As SoqCompletedDelegate
        Get
            Return AddressOf SoqCompleted
        End Get
    End Property
    Public ReadOnly Property SoqCancelledItems() As SoqCancelledDelegate
        Get
            Return AddressOf SoqCancelled
        End Get
    End Property
    Public ReadOnly Property SoqNumberOfItems() As Integer
        Get
            Return _numberToProcess
        End Get
    End Property
    Public ReadOnly Property SupplierGrid() As DevExpress.XtraGrid.GridControl
        Get
            Return xgridSupplier
        End Get
    End Property
    Public ReadOnly Property SupplierGridView() As DevExpress.XtraGrid.Views.Grid.GridView
        Get
            Return xviewSupplier
        End Get
    End Property

    Private Sub SoqSingleThreadButtonEnable(ByVal sup As BindingList(Of Supplier))

        btnExit.Text = My.Resources.Strings.F12Exit
        btnSoq.Enabled = True
        btnOrder.Enabled = True
        btnAuto.Enabled = True
        xviewSupplier.Columns(GetPropertyName(Function(f As Supplier) f.Progress)).Visible = False

        Select Case _soqThreadController.PostAction
            Case SoqThreadController.Action.OrderAndExit : FindForm.Close()
            Case SoqThreadController.Action.SoqAndExit : FindForm.Close()
            Case SoqThreadController.Action.PrintAndExit
                PrintSoqs(_suppliers, True)
                FindForm.Close()
        End Select

    End Sub

End Class

Friend Structure Unbounds
    Private _value As String
    Public Shared ReadOnly DateDue As String = "Date Due"
    Public Shared ReadOnly Sku As String = "SKU"
    Public Shared ReadOnly Indicator As String = "Indicator"
End Structure