﻿Friend Class SoqThreadController
    Public Enum Action
        None
        Order
        OpenOrder
        OrderAndExit
        PrintAndExit
        SoqAndExit
    End Enum
    Private _postAction As Action = Action.None
    Private _soqThreads As New List(Of SoqThread)
    Private _thread As Threading.Thread
    Private _maxThreads As Integer = 10
    Private _abort As Boolean

    Public ReadOnly Property PostAction() As Action
        Get
            Return _postAction
        End Get
    End Property

    Public Sub New(ByVal action As Action)
        _postAction = action
    End Sub

    Public Sub Add(ByVal soq As SoqThread)
        AddHandler soq.Completed, AddressOf SoqCompleted
        _soqThreads.Add(soq)
    End Sub

    Public Sub Start()
        _thread = New Threading.Thread(AddressOf DoWork)
        _thread.Start()
    End Sub

    Private Sub DoWork()

        For index As Integer = 0 To Math.Min(_soqThreads.Count - 1, _maxThreads - 1)
            _soqThreads(index).Start()
        Next

    End Sub

    Public Sub Abort()
        _abort = True
        _thread.Abort()
        For Each soq As SoqThread In _soqThreads
            soq.Abort()
        Next
    End Sub

    Private Sub SoqCompleted(ByVal soq As SoqThread)

        SyncLock Me
            If _abort Then Exit Sub

            If _soqThreads.Count > 0 Then
                If _soqThreads.Exists(Function(s As SoqThread) s.ThreadState = Threading.ThreadState.Unstarted) Then
                    _soqThreads.Find(Function(s As SoqThread) s.ThreadState = Threading.ThreadState.Unstarted).Start()
                End If
            End If

            _soqThreads.Remove(soq)

        End SyncLock

    End Sub

    Public Function Count() As Integer
        Return _soqThreads.Count
    End Function

    Public Function Completed() As Boolean
        Return (_soqThreads.Count = 0)
    End Function

End Class