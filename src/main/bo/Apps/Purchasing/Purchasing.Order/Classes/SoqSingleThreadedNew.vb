﻿Imports Purchasing.Core
Imports Cts.Oasys.Core
Imports Purchasing.Order.Order
Imports System.data

Public Class SoqSingleThreadedNew
    Implements ISoqThread


    Private _soqThreadController As SoqThreadController
    Private WithEvents _soq As Soq
    Private _supplierGrid As DataTable
    Private _supplierGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Private _supplier As BindingList(Of Supplier)
    Private _stopProcess As Boolean

    Public Property StopSingleThread() As Boolean Implements ISoqThread.StopSingleThread
        Get
            Return _stopProcess
        End Get
        Set(ByVal value As Boolean)
            _stopProcess = value
        End Set
    End Property

    Private Sub ProcessSoqStart(ByRef suppliers As BindingList(Of Supplier), ByVal postAction As Integer, ByRef supplierGridView As DevExpress.XtraGrid.Views.Grid.GridView)
        _soqThreadController = New SoqThreadController(CType(postAction, SoqThreadController.Action))
        StopSingleThread = False
        For index As Integer = 0 To suppliers.Count - 1
            If StopSingleThread = False Then
                _soq = New Soq
                _supplier = suppliers
                _supplierGridView = supplierGridView
                _soq.SoqInventory(suppliers(index))

                Application.DoEvents()
            End If
        Next
    End Sub
    Friend Function ProcessSoq(ByRef suppliers As BindingList(Of Supplier), ByVal postAction As Integer, ByRef soqProgress As Object, ByRef soqCompleted As Object _
                       , ByRef soqCancelled As Object, ByRef numOfItems As Integer, ByRef supplierGrid As DevExpress.XtraGrid.GridControl, ByRef supplierGridView As DevExpress.XtraGrid.Views.Grid.GridView) As Object Implements ISoqThread.ProcessSoq

        ProcessSoqStart(suppliers, postAction, supplierGridView)
        Return _soqThreadController
    End Function

    Private Sub _SOQ_SoqStarted(ByVal sup As Purchasing.Core.Supplier) Handles _soq.SoqStarted

        sup.Progress = 0
        Application.DoEvents()

    End Sub

    Private Sub _SOQ_SoqProgress(ByVal sup As Purchasing.Core.Supplier, ByVal progress As Integer) Handles _soq.SoqProgress

        sup.Progress = progress
        Dim colProgress As String = GetPropertyName(Function(f As Supplier) f.Progress)
        Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
        For rowHandle As Integer = 0 To _supplierGridView.DataRowCount - 1
            If _supplierGridView.GetRowCellValue(rowHandle, colNumber) Is sup.Number Then
                _supplierGridView.SetRowCellValue(rowHandle, colProgress, progress)
                _supplierGridView.RefreshRow(rowHandle)
            End If
        Next
        Application.DoEvents()

    End Sub

    Private Sub _SOQ_SoqCompleted(ByVal sup As Purchasing.Core.Supplier, ByVal status As Purchasing.Core.Soq.CompletedStatus) Handles _soq.SoqCompleted

        sup.Progress = 100

        Dim colProgress As String = GetPropertyName(Function(f As Supplier) f.SoqStatus)
        Dim colNumber As String = GetPropertyName(Function(f As Supplier) f.Number)
        For rowHandle As Integer = 0 To _supplierGridView.DataRowCount - 1
            If _supplierGridView.GetRowCellValue(rowHandle, colNumber) Is sup.Number Then
                _supplierGridView.SetRowCellValue(rowHandle, colProgress, 100)
                _supplierGridView.RefreshRow(rowHandle)
            End If
        Next

        Application.DoEvents()

    End Sub

End Class
