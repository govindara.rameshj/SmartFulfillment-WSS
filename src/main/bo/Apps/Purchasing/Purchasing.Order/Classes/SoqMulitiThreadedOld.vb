﻿Imports Purchasing.Core

Public Class SoqMulitiThreadedOld
    Implements ISoqThread


    Private _stopProcess As Boolean
    Private _soqThreadController As SoqThreadController

    Private Sub ProcessSoqStart(ByRef suppliers As BindingList(Of Supplier), ByVal postAction As Integer, ByRef soqProgress As Object, ByRef soqCompleted As Object, ByRef soqCancelled As Object, ByRef numOfItems As Integer)
        _soqThreadController = New SoqThreadController(CType(postAction, SoqThreadController.Action))

        For index As Integer = 0 To Math.Min(suppliers.Count, numOfItems) - 1
            Dim soq As New SoqThread(suppliers(index))
            With soq
                .progressDelegate = CType(soqProgress, Order.SoqProgressDelegate)
                .endDelegate = CType(soqCompleted, Order.SoqCompletedDelegate)
                .cancelledDelegate = CType(soqCancelled, Order.SoqCancelledDelegate)
                _soqThreadController.Add(soq)
            End With
        Next

        _soqThreadController.Start()
    End Sub
    Friend Function ProcessSoq(ByRef suppliers As BindingList(Of Supplier), ByVal postAction As Integer, ByRef soqProgress As Object, ByRef soqCompleted As Object _
                              , ByRef soqCancelled As Object, ByRef numOfItems As Integer, ByRef supplierGrid As DevExpress.XtraGrid.GridControl, ByRef supplierGridView As DevExpress.XtraGrid.Views.Grid.GridView) As Object Implements ISoqThread.ProcessSoq
        ProcessSoqStart(suppliers, postAction, soqProgress, soqCompleted, soqCancelled, numOfItems)
        Return _soqThreadController

    End Function

    Public Property StopSingleThread() As Boolean Implements ISoqThread.StopSingleThread
        Get
            Return _stopProcess
        End Get
        Set(ByVal value As Boolean)
            _stopProcess = value
        End Set
    End Property
End Class
