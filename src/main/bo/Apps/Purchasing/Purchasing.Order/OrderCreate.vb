Public Class OrderCreate
    Private Enum OrderQtyWrites
        None
        Zero
        OrderQty
    End Enum

    Private _supplier As Supplier
    Private _workstationId As Integer
    Private _order As Purchasing.Core.Order
    Private _maxLineQty As Integer = 0
    Private _daysOpen As Integer = 7
    Private _savedOk As Boolean = False

    Sub New(ByVal sup As Supplier, ByVal userID As Integer, ByVal workstationId As Integer, ByVal daysOpen As Integer)
        InitializeComponent()
        _supplier = sup
        _workstationId = workstationId
        _daysOpen = daysOpen
        _order = New Purchasing.Core.Order(sup, userID)

        AddHandler uxStockView.CellValueChanged, AddressOf view_CellValueChanged
        AddHandler uxStockView.ValidatingEditor, AddressOf view_ValidatingEditor
        AddHandler uxStockView.RowStyle, AddressOf view_RowStyle
        AddHandler uxStockView.RowCellStyle, AddressOf view_RowCellStyle

    End Sub

    Private Sub DisplayStatus(Optional ByVal text As String = Nothing, Optional ByVal popup As Boolean = True)

        If text = Nothing Then
            StatusLabel.Text = ""
            StatusLabel.ToolTipText = ""
        Else
            If popup Then
                MessageBox.Show(text, Me.FindForm.Text)
            Else
                StatusLabel.Text = text
                StatusLabel.ToolTipText = text
            End If
        End If
        StatusStrip1.Refresh()

    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If Not _savedOk Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoChangesLostOnExit, Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If result <> Windows.Forms.DialogResult.Yes Then e.Cancel = True
        End If

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyValue
            Case Keys.F10 : btnExit.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F8 : btnStockEnquiry.PerformClick()
            Case Keys.F3 : btnSoq.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'populate fields
        lblSupplier.Text = _supplier.Number & Space(1) & _supplier.Name
        lblOrderType.Text = _supplier.OrderTypeString()
        lblTelephone.Text = _supplier.PhoneNumber1
        lblPoNumber.Text = CStr(_order.PoNumber)
        uxDateDuePicker.EditValue = _supplier.DateOrderDue(_daysOpen)
        uxDateDuePicker.Properties.MinValue = Now.Date
        lblDateDue.Text = CDate(uxDateDuePicker.EditValue).ToLongDateString
        If _order.SoqNumber.HasValue Then lblSoqNumber.Text = CStr(_order.SoqNumber.Value)
        lblMinOrder.Text = _supplier.MinimumOrder.ConstraintString

        'disable due date picker if not direct supplier
        Select Case _supplier.BbcNumber
            Case ""
                lblDateDue.Visible = False
                uxDateDuePicker.Visible = True
            Case Else
                lblDateDue.Visible = True
                uxDateDuePicker.Visible = False
        End Select

        _maxLineQty = Parameter.GetInteger(200)

        PopulateGrid()
        PopulateOverOrderValues()
        PopulateOrderValues()

        'check if need to set order qtys to soq (parameter 204)
        btnSoq.Text = My.Resources.Strings.F3AcceptSoqs
        If Parameter.GetBoolean(204) Then
            SoqOrder()
        End If

        If _supplier.DateLastOrdered.HasValue AndAlso _supplier.DateLastOrdered.Value >= Now.Date Then
            MessageBox.Show(My.Resources.Strings.WarnOrderAlreadyPlaced, Me.FindForm.Text)
        End If

        uxStockView.FocusedColumn = uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty))

    End Sub


    Private Sub PopulateGrid()

        uxStockGrid.DataSource = _supplier.Stocks

        Dim bandSku As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.SkuNumber)
        Dim bandDescription As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.Description)
        Dim bandSales As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.SalesWeeks)
        Dim bandSalesAve As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.SalesAverage)
        Dim bandOnHand As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.OnHand)
        Dim bandOnOrder As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.OnOrder)
        Dim bandSoq As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.SoqQty)
        Dim bandOrder As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.OrderQty)
        Dim bandShelfCap As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.ShelfCap)
        Dim bandPack As GridBand = uxStockView.Bands.AddBand(My.Resources.ColumnHeaders.Pack)

        For Each col As GridColumn In uxStockView.Columns
            col.VisibleIndex = -1
            col.OptionsColumn.AllowEdit = False
            col.OptionsColumn.AllowFocus = False
            col.Caption = " "
        Next

        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber), bandSku)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Description), bandDescription)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales4), bandSales)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales3), bandSales)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales2), bandSales)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales1), bandSales)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PeriodDemand), bandSalesAve)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty), bandOnHand)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnOrderQty), bandOnOrder)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderLevel), bandSoq)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty), bandOrder)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Capacity), bandShelfCap)
        GridViewSetColumn(uxStockView, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PackSize), bandPack)

        Dim ri As New RepositoryItemTextEdit
        ri.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        ri.Mask.EditMask = "n0"
        ri.NullText = "0"
        ri.Mask.UseMaskAsDisplayFormat = True
        AddHandler ri.Spin, AddressOf repositoryItem_Spin
        uxStockGrid.RepositoryItems.Add(ri)

        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty)).ColumnEdit = ri
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty)).OptionsColumn.AllowEdit = True
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty)).OptionsColumn.AllowFocus = True
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty)).AppearanceCell.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderLevel)).AppearanceCell.Font = New Font("Tahoma", 8.25, FontStyle.Bold)

        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales4)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales3)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales2)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.UnitSales1)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.Capacity)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        uxStockView.Columns(GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PackSize)).OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        'uxStockView.OptionsView.ShowColumnHeaders = False
        uxStockView.BestFitColumns()

        bandSku.Width = 40
        bandDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        bandSales.Width = 120
        bandSalesAve.Width = 40
        bandOnHand.Width = 40
        bandOnOrder.Width = 40
        bandSoq.Width = 40
        bandOrder.Width = 40
        bandShelfCap.Width = 40
        bandPack.Width = 30

    End Sub

    Private Sub GridViewSetColumn(ByVal view As GridView, ByVal columnName As String, ByRef band As GridBand)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        band.Columns.Add(CType(view.Columns(columnName), BandedGridColumn))
    End Sub


    Private Sub PopulateOverOrderValues()

        Dim units As Integer
        Dim value As Decimal
        _supplier.Stocks.TotalsOverShelfCapacity(units, value)

        lblOverUnits.Text = CStr(units)
        lblOverValue.Text = CStr(value)

    End Sub

    Private Sub PopulateOrderValues()

        Dim units As Integer = 0
        Dim value As Decimal = 0
        Dim weight As Decimal = 0
        Dim cartons As Integer = 0
        _supplier.Stocks.TotalsOrder(value, units, weight, cartons)

        lblOrderUnits.Text = CStr(units)
        lblOrderValue.Text = CStr(value)
        lblWeight.Text = CStr(weight)
        lblCartons.Text = CStr(cartons)

    End Sub


    Private Sub view_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs)
        PopulateOrderValues()
    End Sub

    Private Sub view_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs)

        'get view in question
        Dim view As GridView = CType(sender, GridView)

        'check if exit button has been pressed
        If Me.ActiveControl.Name = btnExit.Name Then
            view.CancelUpdateCurrentRow()
            Exit Sub
        End If

        'if no value then cancel and exit
        If e.Value Is Nothing Then
            view.CancelUpdateCurrentRow()
            Exit Sub
        End If

        'only order qty is editable so check that this is order column
        If view.FocusedColumn.FieldName <> GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OrderQty) Then
            Exit Sub
            view.CancelUpdateCurrentRow()
        End If

        'check entered value
        Select Case CDbl(e.Value)
            Case 0
            Case Is < 0 : e.ErrorText = My.Resources.Strings.WarnQtyNotNegative
            Case Is > _maxLineQty : e.ErrorText = My.Resources.Strings.WarnQtyGreaterThan & _maxLineQty
            Case Else
                'check for pack size
                Dim pack As Integer = CInt(view.GetRowCellValue(view.FocusedRowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.PackSize)))
                If CInt(e.Value) Mod pack <> 0 Then
                    Dim result As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoNotMulitple, Me.FindForm.Text, MessageBoxButtons.YesNo)
                    If result = DialogResult.Yes Then
                        e.Value = CInt(Math.Ceiling(CDbl(e.Value) / pack)) * pack
                        e.Valid = True
                    End If
                End If
        End Select
        If e.ErrorText <> String.Empty Then
            If MessageBox.Show("WARNING: - Quantity entered is high." + vbCrLf + vbCrLf + "Are you sure?", Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                e.Value = 0
            End If
            e.Valid = True
        End If
    End Sub

    Private Sub view_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)

        Dim View As GridView = CType(sender, GridView)

        If (e.RowHandle >= 0) Then
            Dim isNonStock As Boolean = CBool(View.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.IsNonStock)))
            If isNonStock Then
                e.Appearance.BackColor = Color.LightGray
                e.Appearance.BackColor2 = Color.DarkGray
            End If

            Dim qty As Integer = CInt(View.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty)))
            If qty < 0 Then
                e.Appearance.BackColor = Color.Salmon
                e.Appearance.BackColor2 = Color.Red
            End If
        End If

    End Sub

    Private Sub view_RowCellStyle(ByVal sender As Object, ByVal e As RowCellStyleEventArgs)

        Dim View As GridView = CType(sender, GridView)

        If e.Column.FieldName = GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty) Then
            Dim qty As Integer = CInt(View.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.OnHandQty)))
            Dim isNonStock As Boolean = CBool(View.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.IsNonStock)))

            If Not isNonStock AndAlso qty <= 0 Then
                e.Appearance.BackColor = Color.LightSalmon
                e.Appearance.BackColor2 = Color.Red
            End If
        End If

    End Sub



    Private Sub uxDateDuePicker_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDateDuePicker.EditValueChanged
        lblDateDue.Text = CDate(uxDateDuePicker.EditValue).ToLongDateString
        lblDateDue.Visible = True
        uxDateDuePicker.Visible = False
    End Sub

    Private Sub repositoryItem_Spin(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.SpinEventArgs)
        uxStockView.CloseEditor()
        e.Handled = True
    End Sub


    Private Sub SoqOrder()

        Select Case btnSoq.Text
            Case My.Resources.Strings.F3AcceptSoqs
                btnSoq.Text = My.Resources.Strings.F3RejectSoqs
                _supplier.Stocks.OrderQtySetSoq()

            Case My.Resources.Strings.F3RejectSoqs
                btnSoq.Text = My.Resources.Strings.F3AcceptSoqs
                _supplier.Stocks.OrderQtySetZero()
        End Select
        uxStockView.RefreshData()

    End Sub

    Friend Function OrderSave(Optional ByVal AutoOrder As Boolean = False) As Boolean

        Try
            'check if any items ordered
            If _supplier.Stocks.AllZeroOrderQty Then
                If AutoOrder Then Return True
                DisplayStatus(My.Resources.Strings.NoItemsOrdered)
                Return False
            End If

            DisplayStatus(My.Resources.Strings.SavingOrder)
            Cursor = Cursors.WaitCursor

            'if auto order then automatically increase to mcp level
            If AutoOrder AndAlso Not _supplier.MinimumOrder.Reached Then
                _supplier.MinimumOrder.IncreaseOrderToLevel()
                uxStockView.RefreshData()
            End If
            _order.DateDue = CType(uxDateDuePicker.EditValue, Date?)

            'loop through sheet and add item to order if order qty > 0
            For Each stockItem As Cts.Oasys.Core.Stock.Stock In _supplier.Stocks
                If stockItem.OrderQty > 0 Then
                    Dim line As New OrderLine(stockItem)
                    line.OrderQty = stockItem.OrderQty
                    _order.Lines.Add(line)
                End If
            Next

            'Save order and update supplier
            _order.UpdateOrder(_supplier)

            If Not AutoOrder Then
                DisplayStatus()
                MessageBox.Show(String.Format(My.Resources.Strings.PoNumber, _order.PoNumber), Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            Return True

        Finally
            Cursor = Cursors.Default
        End Try

    End Function


    Private Sub uxSkuSearchText_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles uxSkuSearchText.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            SearchForSku()
        End If
    End Sub

    Public Sub SearchForSku() Handles uxSkuSearchButton.Click

        If uxSkuSearchText.Text Is Nothing Then Exit Sub
        Dim skuNumber As String = uxSkuSearchText.Text.Trim.PadLeft(6, "0"c)

        For rowHandle As Integer = 0 To uxStockView.RowCount - 1
            If CStr(uxStockView.GetRowCellValue(rowHandle, GetPropertyName(Function(f As Cts.Oasys.Core.Stock.Stock) f.SkuNumber))) = skuNumber Then
                uxStockView.SelectRow(rowHandle)
                uxStockView.FocusedRowHandle = rowHandle
                Exit Sub
            End If
        Next

    End Sub



    Private Sub btnSoq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSoq.Click

        Dim response As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoAllOrderChangesWillBeLost, btnSoq.Text.Substring(3), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If response = Windows.Forms.DialogResult.Yes Then
            SoqOrder()
        End If

    End Sub

    Private Sub btnResetQtys_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetQtys.Click

        Dim response As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoAllOrderChangesWillBeLost, Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If response = Windows.Forms.DialogResult.Yes Then
            _supplier.Stocks.OrderQtySetZero()
            uxStockGrid.DataSource = _supplier.Stocks
            uxStockGrid.Refresh()
            btnSoq.Text = My.Resources.Strings.F3AcceptSoqs
        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If (Not _supplier.MinimumOrder.Reached()) Then
            Dim response As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoOrderBelowMin, Me.Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3)
            Select Case response
                Case Windows.Forms.DialogResult.Cancel
                    Exit Sub
                Case Windows.Forms.DialogResult.Yes
                    Select Case _supplier.MinimumOrder.IncreaseOrderToLevel
                        Case SupplierMinimumOrderInfo.Result.AllStocksZeroDemand
                            MessageBox.Show(My.Resources.Strings.StocksAllZeroDemand, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                        Case SupplierMinimumOrderInfo.Result.Successful
                            PopulateOrderValues()
                            uxStockView.RefreshData()
                            MessageBox.Show(My.Resources.Strings.OrderIncreased)
                    End Select

                    Exit Sub
            End Select
        End If

        If OrderSave() Then
            _savedOk = True
            DialogResult = Windows.Forms.DialogResult.OK
            btnExit.PerformClick()
        End If

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim storeIdName As String = Store.GetIdName
        Dim userIdName As String = User.GetUserIdName(_order.EmployeeId, IncludeExternalIds:=True)

        Using rep As New SoqPrint(_supplier, storeIdName, userIdName, _workstationId)
            rep.ShowPreviewDialog()
        End Using

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click
        Dim assemblyName As String = "Stock.Enquiry.dll" '"UpdPriceChgFrmEvts.dll"
        Dim className As String = "Stock.Enquiry.Enquiry" '"UpdPriceChgFrmEvts.UpdPriceChgFrmEvts"
        Dim appName As String = "Stock Item Enquiry" '"Process Events"
        Dim parameters As String = " (/P=',CFC')"

        Trace.WriteLine("Launching Stock Enquiry")
        _workstationId = 1

        Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, 0, 1, 9, parameters, "", False)
        hostForm.ShowDialog()

    End Sub

End Class