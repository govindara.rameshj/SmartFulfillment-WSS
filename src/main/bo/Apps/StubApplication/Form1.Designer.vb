﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLaunch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLaunch))
        Me.btnLaunch = New System.Windows.Forms.Button
        Me.txtAssemblyName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtClassName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSecurityLevel = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtWorkStationId = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtUserId = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtParameters = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'btnLaunch
        '
        Me.btnLaunch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLaunch.Location = New System.Drawing.Point(120, 246)
        Me.btnLaunch.Name = "btnLaunch"
        Me.btnLaunch.Size = New System.Drawing.Size(135, 29)
        Me.btnLaunch.TabIndex = 14
        Me.btnLaunch.Text = "&Launch"
        Me.btnLaunch.UseVisualStyleBackColor = True
        '
        'txtAssemblyName
        '
        Me.txtAssemblyName.Location = New System.Drawing.Point(20, 28)
        Me.txtAssemblyName.Name = "txtAssemblyName"
        Me.txtAssemblyName.Size = New System.Drawing.Size(334, 20)
        Me.txtAssemblyName.TabIndex = 1
        Me.txtAssemblyName.Text = "NewBanking.Form.dll"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Enter Assembly Name:"
        '
        'txtClassName
        '
        Me.txtClassName.Location = New System.Drawing.Point(20, 73)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.Size = New System.Drawing.Size(334, 20)
        Me.txtClassName.TabIndex = 3
        Me.txtClassName.Text = "NewBanking.Form.Main"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Enter Class Name:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Enter Description:"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(20, 120)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(334, 20)
        Me.txtDescription.TabIndex = 5
        Me.txtDescription.Tag = "Enter Stock Adjustment"
        Me.txtDescription.Text = "Entry of Banking"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(238, 158)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Security Level:"
        '
        'txtSecurityLevel
        '
        Me.txtSecurityLevel.Location = New System.Drawing.Point(321, 155)
        Me.txtSecurityLevel.Name = "txtSecurityLevel"
        Me.txtSecurityLevel.Size = New System.Drawing.Size(33, 20)
        Me.txtSecurityLevel.TabIndex = 11
        Me.txtSecurityLevel.Tag = "Enter Stock Adjustment"
        Me.txtSecurityLevel.Text = "9"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(108, 158)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Work Station Id:"
        '
        'txtWorkStationId
        '
        Me.txtWorkStationId.Location = New System.Drawing.Point(198, 155)
        Me.txtWorkStationId.Name = "txtWorkStationId"
        Me.txtWorkStationId.Size = New System.Drawing.Size(33, 20)
        Me.txtWorkStationId.TabIndex = 9
        Me.txtWorkStationId.Text = "1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "User Id:"
        '
        'txtUserId
        '
        Me.txtUserId.Location = New System.Drawing.Point(67, 155)
        Me.txtUserId.Name = "txtUserId"
        Me.txtUserId.Size = New System.Drawing.Size(33, 20)
        Me.txtUserId.TabIndex = 7
        Me.txtUserId.Text = "1"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 191)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Enter Parameters:"
        '
        'txtParameters
        '
        Me.txtParameters.Location = New System.Drawing.Point(20, 205)
        Me.txtParameters.Name = "txtParameters"
        Me.txtParameters.Size = New System.Drawing.Size(334, 20)
        Me.txtParameters.TabIndex = 13
        Me.txtParameters.Tag = "Enter Stock Adjustment"
        '
        'frmLaunch
        '
        Me.AcceptButton = Me.btnLaunch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 287)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtParameters)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtSecurityLevel)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtWorkStationId)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtUserId)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtClassName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAssemblyName)
        Me.Controls.Add(Me.btnLaunch)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLaunch"
        Me.Text = "Launch Application "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLaunch As System.Windows.Forms.Button
    Friend WithEvents txtAssemblyName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSecurityLevel As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtWorkStationId As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtUserId As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtParameters As System.Windows.Forms.TextBox

End Class
