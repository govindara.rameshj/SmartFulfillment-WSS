﻿Public Class frmLaunch

    Private Sub btnLaunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLaunch.Click
        Dim hostForm As New Cts.Oasys.WinForm.HostForm(txtAssemblyName.Text, _
                                                       txtClassName.Text, _
                                                       txtDescription.Text, _
                                                       Integer.Parse(txtUserId.Text), _
                                                       Integer.Parse(txtWorkStationId.Text), _
                                                       Integer.Parse(txtSecurityLevel.Text), _
                                                       txtParameters.Text, "", False, 1)

        If hostForm IsNot Nothing Then
            hostForm.Show()
        Else
            'Failed to load
            MessageBox.Show("Failed to load application.", "Failed to Load", MessageBoxButtons.OK)
        End If

    End Sub

    Private Sub txtAssemblyName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssemblyName.TextChanged

    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged

    End Sub

    Private Sub txtClassName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtClassName.TextChanged

    End Sub
End Class