﻿Public Class ExtraSubTotalsRepository

    Friend Overridable Function ReadTotalDeliveriesToday(ByVal Today As Date) As Integer

        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_GetNumberofDeliveriesToday"
                com.AddParameter("@DateInterestedIn", Today, SqlDbType.Date)
                dt = com.ExecuteDataTable()

            End Using

        End Using

        Return CInt(dt.Rows(0).Item(0))

    End Function

End Class
