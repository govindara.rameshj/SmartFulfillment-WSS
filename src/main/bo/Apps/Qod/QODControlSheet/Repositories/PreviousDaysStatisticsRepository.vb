﻿Public Class PreviousDaysStatisticsRepository

    Friend Overridable Function ReadPreviousDaysStatistics(ByVal PrevDay As Date) As DataTable

        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_GetDeliveryStatusLogInfo"
                com.AddParameter("@DateInterestedIn", PrevDay, SqlDbType.Date)
                dt = com.ExecuteDataTable()

            End Using

        End Using

        Return dt

    End Function

End Class
