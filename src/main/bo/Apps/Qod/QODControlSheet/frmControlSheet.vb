﻿Public Class frmControlSheet

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Private Sub frmControlSheet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F5
                btnPrint.PerformClick()
            Case Keys.F12
                btnExit.PerformClick()
        End Select
    End Sub

    Private Sub frmControlSheet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        DateEdit1.DateTime = Now
        Me.Show()
        Me.Refresh()

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim SqlConnect As New SqlConnection
        Dim DS As New csDataSet
        Dim CorhdrTA As New csDataSetTableAdapters.CORHDRTableAdapter

        Dim Store As New BOSystem.cSystemOptions(_Oasys3DB)
        Dim ControlSheet As New XtraControlSheet
        Dim s As String = My.Settings.WixMasterConnectionString.ToString()
        Dim strDate As String = DateEdit1.DateTime.ToString("dd/MM/yyyy")

        btnPrint.Enabled = False
        Me.Cursor = Cursors.WaitCursor
        Store.AddLoadFilter(clsOasys3DB.eOperator.pEquals, Store.SystemOptionsID, "01")
        Store.LoadMatches()

        SqlConnect.ConnectionString = Cts.Oasys.Data.GetConnectionString()
        SqlConnect.Open()
        CorhdrTA.ExplicitSetConnectionPropertyOnly = SqlConnect

        Dim Data As IControlSheetReport
        Data = (New ControlSheetReportFactory).GetImplementation
        Data.GetData(CorhdrTA, DS.CORHDR, strDate)

        Dim PreviousDaysStatistics As IControlSheetReport = (New PreviousDaysStatisticsFactory).GetImplementation
        PreviousDaysStatistics.LoadPreviousDaysStatistics(ControlSheet, DateEdit1.DateTime)

        Dim ExtraSubTotals As IExtraSubTotals = (New ExtraSubTotalsFactory).GetImplementation
        ExtraSubTotals.GetDeliverysForToday(ControlSheet, DateEdit1.DateTime)

        If DS.CORHDR.Rows.Count > 0 Then
            ControlSheet.XrStoreNum.Text = Store.StoreNo.Value
            ControlSheet.XrStorName.Text = Store.StoreName.Value
            ControlSheet.XrDataDate.Text = DS.CORHDR.Item(0).DELD.ToString("dd/MM/yyyy")
            ControlSheet.XrPrintedOn.Text = Format(Now, "dd/MM/yyyy")
            ControlSheet.DataSource = DS
            ControlSheet.ShowPreview()
        Else
            ExtraSubTotals.ShowEmptyReport(ControlSheet, DateEdit1.DateTime, Store)
        End If

        'Close explicit connection object
        If IsNothing(SqlConnect) = False Then
            SqlConnect.Close()
            SqlConnect.Dispose()
        End If

        Me.Cursor = Cursors.Default
        btnPrint.Enabled = True

    End Sub

#Region "Private Functions And Procedures"

    Private Function LoadConnectionFromConfig() As String

        Try
            Dim doc As XmlDocument = New XmlDocument()

            Trace.WriteLine("Get Connection String", Me.GetType.ToString)
            LoadConnectionFromConfig = String.Empty
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory & "Resources\Connection.xml")
            Trace.WriteLine("Get Connection String: retrieving from " & doc.Value, Me.GetType.ToString)

            'search for sql connection string
            For Each node As XmlNode In doc.SelectNodes("configuration/connectionStrings/string")
                If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                    LoadConnectionFromConfig = node.Attributes.GetNamedItem("connectionString").Value

                    Trace.WriteLine("Get Connection String: (Sql Server) " & LoadConnectionFromConfig, Me.GetType.ToString)
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(ex.Message, ex)
        End Try

    End Function

#End Region

End Class