﻿Public Interface IControlSheetReport
    Sub GetData(ByRef TA As csDataSetTableAdapters.CORHDRTableAdapter, ByRef DT As csDataSet.CORHDRDataTable, ByVal SheetData As String)
    Sub LoadPreviousDaysStatistics(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date)
End Interface
