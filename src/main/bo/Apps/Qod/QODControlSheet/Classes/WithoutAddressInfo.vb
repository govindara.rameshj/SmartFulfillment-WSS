﻿Public Class WithoutAddressInfo
    Implements IControlSheetReport

    Public Sub GetData(ByRef TA As csDataSetTableAdapters.CORHDRTableAdapter, ByRef DT As csDataSet.CORHDRDataTable, ByVal SheetData As String) Implements IControlSheetReport.GetData
        TA.FillWithoutAddressInfo(DT, SheetData)
    End Sub

    Public Sub LoadPreviousDaysStatistics(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date) Implements IControlSheetReport.LoadPreviousDaysStatistics
        'Do Nothing
    End Sub
End Class