﻿Public Class ExtraSubTotals

    Implements IExtraSubTotals

    Public Sub GetDeliverysForToday(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date) Implements IExtraSubTotals.GetDeliverysForToday
        Dim AwaitingAction As Integer = (CInt(ControlSheet.XrDespatchedTotal.Text) - CInt(ControlSheet.XrFailedTotal.Text) - CInt(ControlSheet.XrCompletedTotal.Text))
        ControlSheet.XrAwaitingActionTotal.Text = AwaitingAction.ToString
        ControlSheet.XrDeliveredTodayTotal.Text = GetTotalDeliverysForToday(DateChosen)
    End Sub

    Friend Function GetTotalDeliverysForToday(ByVal DateChosen As Date) As String
        Dim Repository As New ExtraSubTotalsRepository
        Dim TotalDeliveries As Integer = Repository.ReadTotalDeliveriesToday(DateChosen)
        Return TotalDeliveries.ToString
    End Function

    Public Sub ShowEmptyReport(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date, ByVal Store As BOSystem.cSystemOptions) Implements IExtraSubTotals.ShowEmptyReport
        ControlSheet.XrStoreNum.Text = Store.StoreNo.Value
        ControlSheet.XrStorName.Text = Store.StoreName.Value
        ControlSheet.XrDataDate.Text = Format(DateChosen, "dd/MM/yyyy")
        ControlSheet.XrPrintedOn.Text = Format(Now, "dd/MM/yyyy")
        ControlSheet.DataSource = Nothing
        ControlSheet.ShowPreview()
    End Sub
End Class
