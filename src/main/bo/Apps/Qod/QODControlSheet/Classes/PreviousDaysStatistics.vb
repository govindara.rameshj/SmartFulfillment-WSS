﻿Public Class PreviousDaysStatistics
    Implements IControlSheetReport


    Public Sub GetData(ByRef TA As csDataSetTableAdapters.CORHDRTableAdapter, ByRef DT As csDataSet.CORHDRDataTable, ByVal SheetData As String) Implements IControlSheetReport.GetData
        TA.Fill(DT, SheetData)
    End Sub

    Public Sub LoadPreviousDaysStatistics(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date) Implements IControlSheetReport.LoadPreviousDaysStatistics
        ControlSheet.Bands.Item("ReportFooter").Visible = True
        ControlSheet.XrPreviousDayHeader.Text = "Previous day " + DateAdd(DateInterval.Day, -1, DateChosen).ToString("dd-MM-yyyy") + " delivery statistics."
        Dim dt As DataTable = GetPreviousDaysStatistics(DateAdd(DateInterval.Day, -1, DateChosen))
        For Each row As DataRow In dt.Rows
            Select Case CInt(row.Item("NewStatus"))
                Case 700
                    ControlSheet.XrDespatchedTotal.Text = row.Item("Total").ToString
                    ControlSheet.XrDespatchedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
                Case 800
                    ControlSheet.XrFailedTotal.Text = row.Item("Total").ToString
                    ControlSheet.XrFailedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
                Case 900
                    ControlSheet.XrCompletedTotal.Text = row.Item("Total").ToString
                    ControlSheet.XrCompletedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            End Select
        Next
    End Sub

    Friend Function GetPreviousDaysStatistics(ByVal PreviousDay As Date) As System.Data.DataTable
        Dim Repository As New PreviousDaysStatisticsRepository
        Return Repository.ReadPreviousDaysStatistics(PreviousDay)
    End Function

End Class
