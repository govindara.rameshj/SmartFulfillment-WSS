﻿Public Class ExtraSubTotalsSwitchedOff

    Implements IExtraSubTotals

    Public Sub GetDeliverysForToday(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date) Implements IExtraSubTotals.GetDeliverysForToday
        ControlSheet.XrAwaitingAction.Visible = False
        ControlSheet.XrAwaitingActionTotal.Visible = False
        ControlSheet.XrDeliveredToday.Visible = False
        ControlSheet.XrDeliveredTodayTotal.Visible = False
    End Sub

    Public Sub ShowEmptyReport(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date, ByVal Store As BOSystem.cSystemOptions) Implements IExtraSubTotals.ShowEmptyReport
        MessageBox.Show("No Deliveries found for " & DateChosen.ToString("dd/MM/yyyy"), "No Deliveries Today", MessageBoxButtons.OK)
    End Sub
End Class
