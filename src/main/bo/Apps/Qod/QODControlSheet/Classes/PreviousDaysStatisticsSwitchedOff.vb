﻿Public Class PreviousDaysStatisticsSwitchedOff

    Implements IControlSheetReport

    Public Sub GetData(ByRef TA As csDataSetTableAdapters.CORHDRTableAdapter, ByRef DT As csDataSet.CORHDRDataTable, ByVal SheetData As String) Implements IControlSheetReport.GetData
        TA.Fill(DT, SheetData)
    End Sub

    Public Sub LoadPreviousDaysStatistics(ByRef ControlSheet As XtraControlSheet, ByVal DateChosen As Date) Implements IControlSheetReport.LoadPreviousDaysStatistics
        ControlSheet.Bands.Item("ReportFooter").Visible = True
        ControlSheet.XrPreviousDayHeader.Visible = False
        ControlSheet.XrDespatchedTotal.Visible = False
        ControlSheet.XrDespatched.Visible = False
        ControlSheet.XrFailedTotal.Visible = False
        ControlSheet.XrFailed.Visible = False
        ControlSheet.XrCompletedTotal.Visible = False
        ControlSheet.XrCompleted.Visible = False
    End Sub
End Class
