﻿<Assembly: InternalsVisibleTo("QODControlSheet.UnitTest")> 

Public Class ExtraSubTotalsFactory

    Inherits RequirementSwitchFactory(Of IExtraSubTotals)

    Public Overrides Function ImplementationA() As IExtraSubTotals

        Return New ExtraSubTotals

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = Enabled()

    End Function

    Public Overrides Function ImplementationB() As IExtraSubTotals

        Return New ExtraSubTotalsSwitchedOff

    End Function

#Region "Private Functions & Procedures"

    Friend Overridable Function Enabled() As Boolean
        Dim x As New PreviousDaysStatisticsFactory
        If x.Enabled Then 'Only check switch if parent story is active
            Return Cts.Oasys.Core.System.Parameter.GetBoolean(-2201001)
        Else
            Return False
        End If

    End Function

#End Region


End Class
