﻿<Assembly: InternalsVisibleTo("QODControlSheet.UnitTest")> 

Public Class PreviousDaysStatisticsFactory

    Inherits RequirementSwitchFactory(Of IControlSheetReport)

    Public Overrides Function ImplementationA() As IControlSheetReport

        Return New PreviousDaysStatistics

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = Enabled()

    End Function

    Public Overrides Function ImplementationB() As IControlSheetReport

        Return New PreviousDaysStatisticsSwitchedOff

    End Function

#Region "Private Functions & Procedures"

    Friend Overridable Function Enabled() As Boolean

        Return Cts.Oasys.Core.System.Parameter.GetBoolean(-22010)

    End Function

#End Region

End Class
