<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class XtraControlSheet
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrTitle = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreNum = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStorName = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDataDate = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPrintedOn = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.CsDataSet1 = New QODControlSheet.csDataSet
        Me.CORHDRTableAdapter = New QODControlSheet.csDataSetTableAdapters.CORHDRTableAdapter
        Me.CsDataSet2 = New QODControlSheet.csDataSet
        Me.OrderTotal = New DevExpress.XtraReports.UI.CalculatedField
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.XrDeliveredTodayTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAwaitingActionTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDeliveredToday = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAwaitingAction = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCompletedTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrFailedTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDespatchedTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCompleted = New DevExpress.XtraReports.UI.XRLabel
        Me.XrFailed = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDespatched = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPreviousDayHeader = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.CsDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CsDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel30, Me.XrLabel29, Me.XrLabel28, Me.XrLabel26, Me.XrLabel22, Me.XrLabel19, Me.XrLabel18, Me.XrLabel15, Me.XrLabel17, Me.XrLabel16, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLine2, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3})
        Me.Detail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Detail.Height = 150
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseForeColor = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel30
        '
        Me.XrLabel30.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel30.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel30.CanGrow = False
        Me.XrLabel30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel30.Location = New System.Drawing.Point(665, 67)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel30.StylePriority.UseBorderColor = False
        Me.XrLabel30.StylePriority.UseBorders = False
        Me.XrLabel30.StylePriority.UseForeColor = False
        Me.XrLabel30.Text = "XrLabel19"
        '
        'XrLabel29
        '
        Me.XrLabel29.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel29.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel29.CanGrow = False
        Me.XrLabel29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel29.Location = New System.Drawing.Point(597, 67)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel29.StylePriority.UseBorderColor = False
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.StylePriority.UseForeColor = False
        Me.XrLabel29.Text = "XrLabel19"
        '
        'XrLabel28
        '
        Me.XrLabel28.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel28.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel28.CanGrow = False
        Me.XrLabel28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel28.Location = New System.Drawing.Point(529, 67)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel28.StylePriority.UseBorderColor = False
        Me.XrLabel28.StylePriority.UseBorders = False
        Me.XrLabel28.StylePriority.UseForeColor = False
        Me.XrLabel28.Text = "XrLabel19"
        '
        'XrLabel26
        '
        Me.XrLabel26.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel26.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel26.Location = New System.Drawing.Point(461, 67)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel26.StylePriority.UseBorderColor = False
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.StylePriority.UseForeColor = False
        Me.XrLabel26.Text = "XrLabel19"
        '
        'XrLabel22
        '
        Me.XrLabel22.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel22.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel22.CanGrow = False
        Me.XrLabel22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel22.Location = New System.Drawing.Point(393, 67)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel22.StylePriority.UseBorderColor = False
        Me.XrLabel22.StylePriority.UseBorders = False
        Me.XrLabel22.StylePriority.UseForeColor = False
        Me.XrLabel22.Text = "XrLabel19"
        '
        'XrLabel19
        '
        Me.XrLabel19.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrLabel19.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel19.CanGrow = False
        Me.XrLabel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.XrLabel19.Location = New System.Drawing.Point(325, 67)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.Size = New System.Drawing.Size(33, 25)
        Me.XrLabel19.StylePriority.UseBorderColor = False
        Me.XrLabel19.StylePriority.UseBorders = False
        Me.XrLabel19.StylePriority.UseForeColor = False
        Me.XrLabel19.Text = "XrLabel19"
        '
        'XrLabel18
        '
        Me.XrLabel18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.OrderTotal", ": {0:" & ChrW(163) & "0.00}")})
        Me.XrLabel18.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel18.Location = New System.Drawing.Point(142, 68)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.Text = "XrLabel18"
        '
        'XrLabel15
        '
        Me.XrLabel15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.DCST", ": {0:" & ChrW(163) & "0.00}")})
        Me.XrLabel15.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel15.Location = New System.Drawing.Point(142, 85)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.Text = "XrLabel15"
        '
        'XrLabel17
        '
        Me.XrLabel17.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.VOLU", ": {0:#0} m3")})
        Me.XrLabel17.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel17.Location = New System.Drawing.Point(142, 119)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.Text = "XrLabel17"
        '
        'XrLabel16
        '
        Me.XrLabel16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.WGHT", ": {0:#0} Kg")})
        Me.XrLabel16.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel16.Location = New System.Drawing.Point(142, 102)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.Text = "XrLabel16"
        '
        'XrLabel14
        '
        Me.XrLabel14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.PHON", ": {0}")})
        Me.XrLabel14.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel14.Location = New System.Drawing.Point(142, 51)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.Text = "XrLabel14"
        '
        'XrLabel13
        '
        Me.XrLabel13.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.POST", ": {0}")})
        Me.XrLabel13.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel13.Location = New System.Drawing.Point(142, 34)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.Text = "XrLabel13"
        '
        'XrLabel12
        '
        Me.XrLabel12.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.NAME", ": {0}")})
        Me.XrLabel12.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel12.Location = New System.Drawing.Point(142, 17)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.Text = "XrLabel12"
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.NUMB", ": {0:#0}")})
        Me.XrLabel11.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel11.Location = New System.Drawing.Point(142, 0)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.Text = "XrLabel11"
        '
        'XrLine2
        '
        Me.XrLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine2.Location = New System.Drawing.Point(8, 142)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.Size = New System.Drawing.Size(717, 8)
        Me.XrLine2.StylePriority.UseForeColor = False
        '
        'XrLabel10
        '
        Me.XrLabel10.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel10.Location = New System.Drawing.Point(8, 119)
        Me.XrLabel10.Multiline = True
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.Text = "Order Volume" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.Location = New System.Drawing.Point(8, 102)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.Text = "Order Weight"
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.Location = New System.Drawing.Point(8, 85)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.Text = "Delivery Cost"
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.Location = New System.Drawing.Point(8, 68)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "Order Total"
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.Location = New System.Drawing.Point(8, 51)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "Customer Contact"
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.Location = New System.Drawing.Point(8, 34)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "Customer Post Code"
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel4.Location = New System.Drawing.Point(8, 17)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = "Customer Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.Location = New System.Drawing.Point(8, 0)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.Size = New System.Drawing.Size(125, 17)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "Order Number / Rev"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTitle, Me.XrStoreNum, Me.XrStorName, Me.XrLabel1, Me.XrDataDate, Me.XrLabel2, Me.XrPrintedOn, Me.XrLine1, Me.XrLabel20, Me.XrLabel21, Me.XrLabel23, Me.XrLabel24, Me.XrLabel25, Me.XrLabel27})
        Me.PageHeader.Height = 125
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseTextAlignment = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrTitle
        '
        Me.XrTitle.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTitle.Location = New System.Drawing.Point(8, 0)
        Me.XrTitle.Name = "XrTitle"
        Me.XrTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTitle.Size = New System.Drawing.Size(258, 33)
        Me.XrTitle.StylePriority.UseFont = False
        Me.XrTitle.StylePriority.UseTextAlignment = False
        Me.XrTitle.Text = "Customer Control Sheet"
        Me.XrTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreNum
        '
        Me.XrStoreNum.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStoreNum.Location = New System.Drawing.Point(8, 42)
        Me.XrStoreNum.Name = "XrStoreNum"
        Me.XrStoreNum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreNum.Size = New System.Drawing.Size(42, 17)
        Me.XrStoreNum.StylePriority.UseFont = False
        Me.XrStoreNum.StylePriority.UseTextAlignment = False
        Me.XrStoreNum.Text = "XrStoreNum"
        Me.XrStoreNum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStorName
        '
        Me.XrStorName.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStorName.Location = New System.Drawing.Point(58, 42)
        Me.XrStorName.Name = "XrStorName"
        Me.XrStorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStorName.Size = New System.Drawing.Size(192, 17)
        Me.XrStorName.StylePriority.UseFont = False
        Me.XrStorName.StylePriority.UseTextAlignment = False
        Me.XrStorName.Text = "XrStorName"
        Me.XrStorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.Location = New System.Drawing.Point(8, 62)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Data Date:"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDataDate
        '
        Me.XrDataDate.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrDataDate.Location = New System.Drawing.Point(75, 62)
        Me.XrDataDate.Name = "XrDataDate"
        Me.XrDataDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDataDate.Size = New System.Drawing.Size(108, 17)
        Me.XrDataDate.StylePriority.UseFont = False
        Me.XrDataDate.StylePriority.UseTextAlignment = False
        Me.XrDataDate.Text = "XrDataDate"
        Me.XrDataDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.Location = New System.Drawing.Point(8, 83)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "Printed On:"
        '
        'XrPrintedOn
        '
        Me.XrPrintedOn.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPrintedOn.Location = New System.Drawing.Point(75, 83)
        Me.XrPrintedOn.Name = "XrPrintedOn"
        Me.XrPrintedOn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPrintedOn.Size = New System.Drawing.Size(108, 17)
        Me.XrPrintedOn.StylePriority.UseFont = False
        Me.XrPrintedOn.StylePriority.UseTextAlignment = False
        Me.XrPrintedOn.Text = "XrDataDate"
        Me.XrPrintedOn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLine1
        '
        Me.XrLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine1.Location = New System.Drawing.Point(8, 117)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.Size = New System.Drawing.Size(717, 8)
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrLabel20
        '
        Me.XrLabel20.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel20.Location = New System.Drawing.Point(442, 83)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.Size = New System.Drawing.Size(58, 33)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Drivers" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Signature"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel21
        '
        Me.XrLabel21.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel21.Location = New System.Drawing.Point(508, 83)
        Me.XrLabel21.Multiline = True
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.Size = New System.Drawing.Size(58, 33)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "Customers" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Signature"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel23
        '
        Me.XrLabel23.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel23.Location = New System.Drawing.Point(658, 83)
        Me.XrLabel23.Multiline = True
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.Size = New System.Drawing.Size(67, 33)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "System" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Despatched"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel24
        '
        Me.XrLabel24.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel24.Location = New System.Drawing.Point(375, 83)
        Me.XrLabel24.Multiline = True
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.Size = New System.Drawing.Size(58, 33)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "Pick" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Signature"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel25
        '
        Me.XrLabel25.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel25.Location = New System.Drawing.Point(575, 83)
        Me.XrLabel25.Multiline = True
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.Size = New System.Drawing.Size(75, 33)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "Store Check" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Signature"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel27
        '
        Me.XrLabel27.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel27.Location = New System.Drawing.Point(317, 83)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.Size = New System.Drawing.Size(50, 33)
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.Text = "Version" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Correct"
        '
        'PageFooter
        '
        Me.PageFooter.Height = 20
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'CsDataSet1
        '
        Me.CsDataSet1.DataSetName = "csDataSet"
        Me.CsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CORHDRTableAdapter
        '
        Me.CORHDRTableAdapter.ClearBeforeFill = True
        '
        'CsDataSet2
        '
        Me.CsDataSet2.DataSetName = "csDataSet"
        Me.CsDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'OrderTotal
        '
        Me.OrderTotal.DataMember = "CORHDR"
        Me.OrderTotal.DataSource = Me.CsDataSet2
        Me.OrderTotal.Expression = "[MVST] + [DCST]"
        Me.OrderTotal.Name = "OrderTotal"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine3, Me.XrDeliveredTodayTotal, Me.XrAwaitingActionTotal, Me.XrDeliveredToday, Me.XrAwaitingAction, Me.XrCompletedTotal, Me.XrFailedTotal, Me.XrDespatchedTotal, Me.XrCompleted, Me.XrFailed, Me.XrDespatched, Me.XrPreviousDayHeader})
        Me.ReportFooter.Height = 177
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLine3
        '
        Me.XrLine3.Location = New System.Drawing.Point(0, 133)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.Size = New System.Drawing.Size(725, 9)
        '
        'XrDeliveredTodayTotal
        '
        Me.XrDeliveredTodayTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrDeliveredTodayTotal.Location = New System.Drawing.Point(292, 150)
        Me.XrDeliveredTodayTotal.Name = "XrDeliveredTodayTotal"
        Me.XrDeliveredTodayTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDeliveredTodayTotal.Size = New System.Drawing.Size(83, 18)
        Me.XrDeliveredTodayTotal.StylePriority.UseFont = False
        Me.XrDeliveredTodayTotal.StylePriority.UseTextAlignment = False
        Me.XrDeliveredTodayTotal.Text = "0"
        Me.XrDeliveredTodayTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrAwaitingActionTotal
        '
        Me.XrAwaitingActionTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrAwaitingActionTotal.Location = New System.Drawing.Point(292, 108)
        Me.XrAwaitingActionTotal.Name = "XrAwaitingActionTotal"
        Me.XrAwaitingActionTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrAwaitingActionTotal.Size = New System.Drawing.Size(83, 17)
        Me.XrAwaitingActionTotal.StylePriority.UseFont = False
        Me.XrAwaitingActionTotal.StylePriority.UseTextAlignment = False
        Me.XrAwaitingActionTotal.Text = "0"
        Me.XrAwaitingActionTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrDeliveredToday
        '
        Me.XrDeliveredToday.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrDeliveredToday.Location = New System.Drawing.Point(8, 150)
        Me.XrDeliveredToday.Multiline = True
        Me.XrDeliveredToday.Name = "XrDeliveredToday"
        Me.XrDeliveredToday.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDeliveredToday.Size = New System.Drawing.Size(275, 18)
        Me.XrDeliveredToday.StylePriority.UseFont = False
        Me.XrDeliveredToday.StylePriority.UseTextAlignment = False
        Me.XrDeliveredToday.Text = "Orders to be Delivered Today:"
        Me.XrDeliveredToday.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrAwaitingAction
        '
        Me.XrAwaitingAction.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrAwaitingAction.Location = New System.Drawing.Point(8, 108)
        Me.XrAwaitingAction.Multiline = True
        Me.XrAwaitingAction.Name = "XrAwaitingAction"
        Me.XrAwaitingAction.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrAwaitingAction.Size = New System.Drawing.Size(275, 18)
        Me.XrAwaitingAction.StylePriority.UseFont = False
        Me.XrAwaitingAction.StylePriority.UseTextAlignment = False
        Me.XrAwaitingAction.Text = "Orders Awaiting Confirm/Fail Delivery:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrAwaitingAction.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrCompletedTotal
        '
        Me.XrCompletedTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrCompletedTotal.Location = New System.Drawing.Point(292, 83)
        Me.XrCompletedTotal.Name = "XrCompletedTotal"
        Me.XrCompletedTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCompletedTotal.Size = New System.Drawing.Size(83, 18)
        Me.XrCompletedTotal.StylePriority.UseFont = False
        Me.XrCompletedTotal.StylePriority.UseTextAlignment = False
        Me.XrCompletedTotal.Text = "0"
        Me.XrCompletedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrFailedTotal
        '
        Me.XrFailedTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrFailedTotal.Location = New System.Drawing.Point(292, 58)
        Me.XrFailedTotal.Name = "XrFailedTotal"
        Me.XrFailedTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrFailedTotal.Size = New System.Drawing.Size(83, 18)
        Me.XrFailedTotal.StylePriority.UseFont = False
        Me.XrFailedTotal.StylePriority.UseTextAlignment = False
        Me.XrFailedTotal.Text = "0"
        Me.XrFailedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrDespatchedTotal
        '
        Me.XrDespatchedTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrDespatchedTotal.Location = New System.Drawing.Point(292, 33)
        Me.XrDespatchedTotal.Name = "XrDespatchedTotal"
        Me.XrDespatchedTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDespatchedTotal.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
        Me.XrDespatchedTotal.Size = New System.Drawing.Size(83, 18)
        Me.XrDespatchedTotal.StylePriority.UseFont = False
        Me.XrDespatchedTotal.StylePriority.UseTextAlignment = False
        Me.XrDespatchedTotal.Text = "0"
        Me.XrDespatchedTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrCompleted
        '
        Me.XrCompleted.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrCompleted.Location = New System.Drawing.Point(8, 83)
        Me.XrCompleted.Name = "XrCompleted"
        Me.XrCompleted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCompleted.Size = New System.Drawing.Size(275, 18)
        Me.XrCompleted.StylePriority.UseFont = False
        Me.XrCompleted.StylePriority.UseTextAlignment = False
        Me.XrCompleted.Text = "Orders Confirmed Delivery:"
        Me.XrCompleted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrFailed
        '
        Me.XrFailed.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrFailed.Location = New System.Drawing.Point(8, 58)
        Me.XrFailed.Multiline = True
        Me.XrFailed.Name = "XrFailed"
        Me.XrFailed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrFailed.Size = New System.Drawing.Size(283, 18)
        Me.XrFailed.StylePriority.UseFont = False
        Me.XrFailed.StylePriority.UseTextAlignment = False
        Me.XrFailed.Text = "Orders Failed Delivery:"
        Me.XrFailed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDespatched
        '
        Me.XrDespatched.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrDespatched.Location = New System.Drawing.Point(8, 33)
        Me.XrDespatched.Name = "XrDespatched"
        Me.XrDespatched.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDespatched.Size = New System.Drawing.Size(283, 18)
        Me.XrDespatched.StylePriority.UseFont = False
        Me.XrDespatched.StylePriority.UseTextAlignment = False
        Me.XrDespatched.Text = "Orders Despatched:"
        Me.XrDespatched.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrPreviousDayHeader
        '
        Me.XrPreviousDayHeader.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrPreviousDayHeader.Location = New System.Drawing.Point(8, 0)
        Me.XrPreviousDayHeader.Multiline = True
        Me.XrPreviousDayHeader.Name = "XrPreviousDayHeader"
        Me.XrPreviousDayHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPreviousDayHeader.Size = New System.Drawing.Size(308, 25)
        Me.XrPreviousDayHeader.StylePriority.UseFont = False
        Me.XrPreviousDayHeader.Text = "Previous day dd-mm-yyyy delivery statistics:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XtraControlSheet
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.OrderTotal})
        Me.DataAdapter = Me.CORHDRTableAdapter
        Me.DataMember = "CORHDR"
        Me.DataSource = Me.CsDataSet2
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "9.1"
        Me.Watermark.ForeColor = System.Drawing.Color.LightGray
        Me.Watermark.Text = "COPY"
        CType(Me.CsDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CsDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrTitle As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPrintedOn As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDataDate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStorName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreNum As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents CsDataSet1 As QODControlSheet.csDataSet
    Friend WithEvents CORHDRTableAdapter As QODControlSheet.csDataSetTableAdapters.CORHDRTableAdapter
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents CsDataSet2 As QODControlSheet.csDataSet
    Friend WithEvents OrderTotal As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrDespatched As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPreviousDayHeader As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrFailedTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDespatchedTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCompleted As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrFailed As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCompletedTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAwaitingAction As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDeliveredToday As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAwaitingActionTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDeliveredTodayTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
End Class
