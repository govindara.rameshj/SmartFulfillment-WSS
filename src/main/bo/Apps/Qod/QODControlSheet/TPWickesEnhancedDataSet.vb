﻿Namespace TPWickes

    Public MustInherit Class TPWickesEnhancedDataAdaptor
        Inherits System.ComponentModel.Component

        Private ReadOnly Property Adapter() As SqlDataAdapter
            Get
                Return DirectCast([GetType]().GetProperty("Adapter", BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(Me, Nothing), SqlDataAdapter)
            End Get
        End Property

        Private ReadOnly Property CommandCollection() As SqlCommand()
            Get
                Return DirectCast([GetType]().GetProperty("CommandCollection", BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(Me, Nothing), SqlCommand())
            End Get
        End Property

        Public WriteOnly Property ExplicitSetConnectionPropertyOnly() As SqlConnection
            Set(ByVal value As SqlConnection)
                If Adapter.InsertCommand IsNot Nothing Then Adapter.InsertCommand.Connection = value
                If Adapter.UpdateCommand IsNot Nothing Then Adapter.UpdateCommand.Connection = value
                If Adapter.DeleteCommand IsNot Nothing Then Adapter.DeleteCommand.Connection = value

                If CommandCollection IsNot Nothing Then
                    For Each command As SqlCommand In CommandCollection
                        command.Connection = value
                    Next
                End If
            End Set
        End Property

        Public WriteOnly Property ExplicitSetConnectionAndTransactionProperties() As SqlTransaction
            Set(ByVal value As SqlTransaction)
                If Adapter.InsertCommand IsNot Nothing Then
                    Adapter.InsertCommand.Transaction = value
                    Adapter.InsertCommand.Connection = value.Connection
                End If

                If Adapter.UpdateCommand IsNot Nothing Then
                    Adapter.UpdateCommand.Transaction = value
                    Adapter.UpdateCommand.Connection = value.Connection
                End If

                If Adapter.DeleteCommand IsNot Nothing Then
                    Adapter.DeleteCommand.Transaction = value
                    Adapter.DeleteCommand.Connection = value.Connection
                End If

                If CommandCollection IsNot Nothing Then
                    For Each command As SqlCommand In CommandCollection
                        command.Transaction = value
                        command.Connection = value.Connection
                    Next
                End If
            End Set
        End Property

    End Class

End Namespace


