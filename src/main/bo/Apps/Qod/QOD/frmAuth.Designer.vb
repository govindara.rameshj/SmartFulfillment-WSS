﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuth
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAuth = New System.Windows.Forms.Button
        Me.lblManager = New System.Windows.Forms.Label
        Me.lblAuth = New System.Windows.Forms.Label
        Me.txtId = New System.Windows.Forms.TextBox
        Me.txtAuth = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(253, 149)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(83, 42)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAuth
        '
        Me.btnAuth.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAuth.Location = New System.Drawing.Point(164, 149)
        Me.btnAuth.Name = "btnAuth"
        Me.btnAuth.Size = New System.Drawing.Size(83, 42)
        Me.btnAuth.TabIndex = 3
        Me.btnAuth.Text = "F2 Auth"
        Me.btnAuth.UseVisualStyleBackColor = True
        '
        'lblManager
        '
        Me.lblManager.AutoSize = True
        Me.lblManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblManager.Location = New System.Drawing.Point(30, 40)
        Me.lblManager.Name = "lblManager"
        Me.lblManager.Size = New System.Drawing.Size(127, 24)
        Me.lblManager.TabIndex = 2
        Me.lblManager.Text = "Managers ID"
        '
        'lblAuth
        '
        Me.lblAuth.AutoSize = True
        Me.lblAuth.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuth.Location = New System.Drawing.Point(30, 93)
        Me.lblAuth.Name = "lblAuth"
        Me.lblAuth.Size = New System.Drawing.Size(188, 24)
        Me.lblAuth.TabIndex = 3
        Me.lblAuth.Text = "Authorisation Code"
        Me.lblAuth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtId.Location = New System.Drawing.Point(236, 35)
        Me.txtId.MaxLength = 3
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(100, 29)
        Me.txtId.TabIndex = 1
        '
        'txtAuth
        '
        Me.txtAuth.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAuth.Location = New System.Drawing.Point(236, 90)
        Me.txtAuth.MaxLength = 5
        Me.txtAuth.Name = "txtAuth"
        Me.txtAuth.Size = New System.Drawing.Size(100, 29)
        Me.txtAuth.TabIndex = 2
        '
        'frmAuth
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(371, 203)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtAuth)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.lblAuth)
        Me.Controls.Add(Me.lblManager)
        Me.Controls.Add(Me.btnAuth)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "frmAuth"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "QOD - Despatch Error Managers Authorisation"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnAuth As System.Windows.Forms.Button
    Friend WithEvents lblManager As System.Windows.Forms.Label
    Friend WithEvents lblAuth As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents txtAuth As System.Windows.Forms.TextBox
End Class
