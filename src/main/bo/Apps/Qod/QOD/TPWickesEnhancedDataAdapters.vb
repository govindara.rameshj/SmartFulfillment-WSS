﻿Imports System.Data.SqlClient

Namespace dsEnquiryTableAdapters

    Partial Public Class CORHDRTableAdapter

        Public Overloads WriteOnly Property ExplicitSetConnectionPropertyOnly() As SqlConnection
            Set(ByVal value As SqlConnection)
                For Each sqlComm As SqlCommand In Me.CommandCollection
                    sqlComm.Connection = value
                Next
            End Set
        End Property

        Public Overloads WriteOnly Property ExplicitSetConnectionAndTransactionProperties() As SqlTransaction
            Set(ByVal value As SqlTransaction)
                For Each sqlComm As SqlCommand In Me.CommandCollection
                    sqlComm.Transaction = value
                    sqlComm.Connection = value.Connection
                Next
            End Set
        End Property

    End Class

End Namespace

Namespace dsOrdersTableAdapters

    Partial Public Class CORHDRAdapter

        Public Overloads WriteOnly Property ExplicitSetConnectionPropertyOnly() As SqlConnection
            Set(ByVal value As SqlConnection)
                For Each sqlComm As SqlCommand In Me.CommandCollection
                    sqlComm.Connection = value
                Next
            End Set
        End Property

        Public Overloads WriteOnly Property ExplicitSetConnectionAndTransactionProperties() As SqlTransaction
            Set(ByVal value As SqlTransaction)
                For Each sqlComm As SqlCommand In Me.CommandCollection
                    sqlComm.Transaction = value
                    sqlComm.Connection = value.Connection
                Next
            End Set
        End Property

    End Class

End Namespace