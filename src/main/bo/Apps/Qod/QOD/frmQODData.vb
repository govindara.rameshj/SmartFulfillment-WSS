﻿Imports System.Xml
Imports System.Text
Imports System.Drawing
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting.Drawing
Imports DevExpress.XtraReports.UI

Public Class frmQODData
    Private _oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _orders As New BOSalesOrders.cOrderHeader(_oasys3DB)
    Private _orderNumber As String = String.Empty
    Private _oleDBConnectionString As String = String.Empty
    Private _isLoaded As Boolean = False
    Private _storeId As String = ""


    Private Sub frmQOD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Using BOSystem As New BOSystem.cRetailOptions(_oasys3DB)
            BOSystem.LoadRetailOptions()
            Dim storeId As Integer = CInt(BOSystem.Store.Value)
            If storeId < 1000 Then storeId += 8000
            _storeId = storeId.ToString("0000")
        End Using

        Load_GridOrders(False)

        Me.Show()
        Me.Refresh()
        _oleDBConnectionString = GetDataSourceForOLEDB()
        _isLoaded = True

    End Sub

    Private Sub Load_GridOrders(ByVal specific As Boolean)

        Dim strSQL As String
        If specific Then
            GridcOrders.DataSource = Nothing
            strSQL = String.Format("QodGetOrder @orderNumber = '{0}'", _orderNumber)
        Else
            strSQL = String.Format("QodGetOrdersForDeliverySource @deliveryStoreID = '{0}', @selectDelivery = {1}", _storeId, IIf(radDel.Checked, "1", "0"))
        End If

        Dim ds As DataSet = _oasys3DB.ExecuteSql(strSQL)
        GridcOrders.DataSource = ds.Tables(0)

        GridvOrders.Bands.Clear()

        Dim bandMain As GridBand = GridvOrders.Bands.AddBand("Order Information")
        bandMain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandCustomer As GridBand = GridvOrders.Bands.AddBand("Customer")
        bandCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandTran As GridBand = GridvOrders.Bands.AddBand("Transaction")
        bandTran.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        For Each col As BandedGridColumn In GridvOrders.Columns
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Select Case col.AbsoluteIndex 'col.FieldName
                Case 0 '_Orders.Column(cOrderHeader.ColumnNames.NUMB)
                    col.Caption = "Order No:"
                    col.BestFit()
                    bandMain.Columns.Add(col)
                Case 1 '_Orders.Column(cOrderHeader.ColumnNames.DATE1)
                    col.Caption = "Date"
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                    col.DisplayFormat.FormatString = "dd-MM-yyyy"
                    col.BestFit()
                    bandMain.Columns.Add(col)
                Case 2 '_Orders.Column(cOrderHeader.ColumnNames.DELC)
                    col.Caption = "Confirmed"
                    bandMain.Columns.Add(col)
                Case 3 '_Orders.Column(cOrderHeader.ColumnNames.DELD)
                    col.Caption = If(radDel.Checked, "Del Date", "Col Date")
                    bandMain.Columns.Add(col)
                Case 4 '_Orders.Column(cOrderHeader.ColumnNames.CUST)
                    col.Caption = "No:"
                    bandCustomer.Columns.Add(col)
                Case 5 '_Orders.Column(cOrderHeader.ColumnNames.NAME)
                    col.Caption = "Name"
                    col.BestFit()
                    bandCustomer.Columns.Add(col)
                Case 6 '_Orders.Column(cOrderHeader.ColumnNames.ADDR1)
                    col.Caption = "Address"
                    bandCustomer.Columns.Add(col)
                Case 7 '_Orders.Column(cOrderHeader.ColumnNames.ADDR2)
                    col.Caption = " "
                    bandCustomer.Columns.Add(col)
                Case 8 '_Orders.Column(cOrderHeader.ColumnNames.ADDR3)
                    col.Caption = " "
                    bandCustomer.Columns.Add(col)
                Case 9 '_Orders.Column(cOrderHeader.ColumnNames.ADDR4)
                    col.Caption = " "
                    bandCustomer.Columns.Add(col)
                Case 10 '_Orders.Column(cOrderHeader.ColumnNames.POST)
                    col.Caption = "Post Code"
                    col.BestFit()
                    bandCustomer.Columns.Add(col)
                Case 11 '_Orders.Column(cOrderHeader.ColumnNames.PHON)
                    col.Caption = "Tel"
                    col.BestFit()
                    bandCustomer.Columns.Add(col)
                Case 12 '_Orders.Column(cOrderHeader.ColumnNames.DELI)
                    col.Caption = If(radDel.Checked, "Del", "Col")
                    bandTran.Columns.Add(col)
                Case 13 '_Orders.Column(cOrderHeader.ColumnNames.STIL)
                    col.Caption = "Till"
                    bandTran.Columns.Add(col)
                Case 14 '_Orders.Column(cOrderHeader.ColumnNames.SDAT)
                    col.Caption = "Date"
                    col.DisplayFormat.FormatString = "dd-MM-yyyy"
                    bandTran.Columns.Add(col)
                Case 15 '_Orders.Column(cOrderHeader.ColumnNames.STRN)
                    col.Caption = "No:"
                    bandTran.Columns.Add(col)
                Case 16 '_Orders.Column(cOrderHeader.ColumnNames.DeliveryStatus)
                    col.Caption = "Status:"
                    bandTran.Columns.Add(col)
                    col.Visible = True
                Case 17 '_Orders.Column(cOrderHeader.ColumnNames.DeliverySource)
                    col.Caption = "Del Srce:"
                    bandTran.Columns.Add(col)
                    col.Visible = True
            End Select
        Next

        If GridvOrders.RowCount > 0 Then
            btnEnquiry.Enabled = True
            btnPrintDelNote.Enabled = True
            btnConfirm.Enabled = True
            btnChangeDate.Enabled = True
        Else
            btnEnquiry.Enabled = False
            btnPrintDelNote.Enabled = False
            btnConfirm.Enabled = False
            btnChangeDate.Enabled = False
        End If

        GridcOrders.Visible = True
        GridvOrders.BestFitColumns()

        GridvOrders.OptionsBehavior.Editable = False
        bandCustomer.Visible = False

    End Sub

    Private Sub txtOrder_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOrder.KeyDown

        If e.KeyCode = Keys.Return Then
            _orderNumber = txtOrder.Text.PadLeft(6, "0"c)
            Load_GridOrders(True)
            Exit Sub
        End If

        Select Case e.KeyCode
            Case Keys.F12
                btnExit.PerformClick()
            Case Keys.F2
                btnConfirm.PerformClick()
            Case Keys.F3
                btnChangeDate.PerformClick()
            Case Keys.F4
                btnEnquiry.PerformClick()
            Case Keys.F5
                btnPrintDelNote.PerformClick()
        End Select

    End Sub

    Private Sub txtOrder_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOrder.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub


    Private Sub GridvOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridvOrders.DoubleClick
        btnPrintDelNote.PerformClick()
    End Sub

    Private Sub chkShowDespatchedOrders_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (_isLoaded) Then Load_GridOrders(False)
    End Sub

    Private Sub radDel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDel.CheckedChanged
        If (_isLoaded) Then Load_GridOrders(False)
    End Sub

    Private Sub radCol_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCol.CheckedChanged
        If (_isLoaded) Then Load_GridOrders(False)
    End Sub


    Private Sub btnPrintDelNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintDelNote.Click

        Dim SqlConnect As New SqlConnection

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim row As System.Data.DataRow = Me.GridvOrders.GetDataRow(Me.GridvOrders.FocusedRowHandle)
            Dim strOrderNum As String = CStr(row("NUMB"))
            Dim CellValue As String = CStr(row("NUMB"))
            Dim DelStat As Integer = CInt(row("DeliveryStatus"))

            If txtOrder.Text = "" Then
                _orderNumber = CellValue
            End If

            Dim ds As New dsOrders
            Dim CorhdrTA As New dsOrdersTableAdapters.CORHDRAdapter

            ds.EnforceConstraints = False
            'use info in .\resourecs\connection.xml to connect to database
            'explicitly define connection object and replace dataset's design-time version
            SqlConnect.ConnectionString = Cts.Oasys.Data.GetConnectionString()  ' LoadConnectionFromConfig()
            SqlConnect.Open()
            CorhdrTA.ExplicitSetConnectionPropertyOnly = SqlConnect
            CorhdrTA.Fill(ds.CORHDR, strOrderNum, _storeId)

            Application.DoEvents()

            If DelStat = 9999 OrElse (DelStat >= 300 And DelStat <= 999) Then
                If ds.CORHDR.Rows.Count > 0 Then
                    If ds.CORHDR.Item(0).PRNT = True Then
                        If MessageBox.Show("This " & CStr(IIf(radCol.Checked = True, "Collection Note", "Delivery Note")) & " has already been printed. Do you want to re-print it.", "Printing.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If

                    Dim DelNote As New XtraDelNoteRep
                    If ds.CORHDR.Item(0).CustomerAddress1.ToString = "Customer line 1" Then
                        DelNote.XrAddr1.Visible = True
                        DelNote.XrCustAddr1.Visible = False
                        DelNote.XrAddr1.Top = DelNote.XrCustAddr1.Top
                        DelNote.XrAddr1.Left = DelNote.XrCustAddr1.Left
                        DelNote.XrAddr1.Width = DelNote.XrCustAddr1.Width
                    Else
                        DelNote.XrAddr1.Visible = False
                        DelNote.XrCustAddr1.Visible = True
                    End If

                    If ds.CORHDR.Item(0).CustomerAddress2 = "Customer line 2" Then
                        DelNote.XrAddr2.Visible = True
                        DelNote.XrCustAddr2.Visible = False
                        DelNote.XrAddr2.Top = DelNote.XrCustAddr2.Top
                        DelNote.XrAddr2.Left = DelNote.XrCustAddr2.Left
                        DelNote.XrAddr2.Width = DelNote.XrCustAddr2.Width
                    Else
                        DelNote.XrAddr2.Visible = False
                        DelNote.XrCustAddr2.Visible = True
                    End If

                    If ds.CORHDR.Item(0).CustomerAddress3 = "Customer Town" Then
                        DelNote.XrAddr3.Visible = True
                        DelNote.XrCustAddr3.Visible = False
                        DelNote.XrAddr3.Top = DelNote.XrCustAddr3.Top
                        DelNote.XrAddr3.Left = DelNote.XrCustAddr3.Left
                        DelNote.XrAddr3.Width = DelNote.XrCustAddr3.Width
                        DelNote.XrCustPostCode.Visible = False
                    Else
                        DelNote.XrAddr3.Visible = False
                        DelNote.XrCustAddr3.Visible = True
                    End If

                    If ds.CORHDR.Item(0).CustomerAddress4 = "Customer line 4" Then
                        DelNote.XrAddr4.Visible = True
                        DelNote.XrCustAddr4.Visible = False
                        DelNote.XrAddr4.Top = DelNote.XrCustAddr4.Top
                        DelNote.XrAddr4.Left = DelNote.XrCustAddr4.Left
                        DelNote.XrAddr4.Width = DelNote.XrCustAddr4.Width
                    Else
                        DelNote.XrAddr4.Visible = False
                        DelNote.XrCustAddr4.Visible = True
                    End If

                    Using store As New BOStore.cStore(_oasys3DB)
                        store.AddLoadFilter(clsOasys3DB.eOperator.pEquals, store.StoreID, ds.CORHDR.Item(0).SellingStoreId.ToString)
                        store.LoadMatches()
                        If (store.AddressLine1.Value = "") Then
                            store.AddLoadFilter(clsOasys3DB.eOperator.pEquals, store.StoreID, ds.CORHDR.Item(0).SellingStoreId.ToString.Substring(1))
                            store.LoadMatches()
                        End If

                        DelNote.XrStoreName.Text = store.AddressLine1.Value
                        DelNote.XrStoreAddr1.Text = store.AddressLine2.Value
                        DelNote.XrStoreAdd2.Text = store.AddressLine3.Value
                        DelNote.XrStoreAddr3.Text = store.AddressLine4.Value
                        DelNote.XrStorePhone.Text = store.Telephone.Value
                        DelNote.XrStoreFax.Text = store.Fax.Value
                    End Using

                    DelNote.XrOrderNum.Text = strOrderNum
                    DelNote.XrDatePrinted.Text = Format(Now, "dd/MM/yyyy")
                    DelNote.XrTotalCost.Text = ChrW(163) + Format(ds.CORHDR.Item(0).MVST, "#0.00")
                    DelNote.XrDayName.Text = ds.CORHDR.Item(0).DELD.ToString("dddd")

                    DelNote.XrDelCol.Text = CStr(IIf(radCol.Checked = True, "Collection Note", "Delivery Note"))
                    DelNote.XrDelColNew.Text = CStr(IIf(radCol.Checked = True, "Collection Date:", "Delivery Date:"))

                    Using BoOrders As New BOSalesOrders.cOrderHeader(_oasys3DB)
                        If BoOrders.LoadAnOrder(_orderNumber) Then
                            DelNote.XrDispatchDate.Text = BoOrders.DeliveryDate.Value.ToShortDateString
                        End If
                    End Using

                    Using deliveryInstructions As New BOSalesOrders.cOrderText(_oasys3DB)
                        deliveryInstructions.AddLoadFilter(clsOasys3DB.eOperator.pEquals, deliveryInstructions.OrderNo, strOrderNum)
                        DelNote.XrDelIns.Text = String.Empty
                        Dim listOfIns As List(Of BOSalesOrders.cOrderText) = deliveryInstructions.LoadMatches()
                        For Each Instruction As BOSalesOrders.cOrderText In listOfIns
                            DelNote.XrDelIns.Text += " " & Instruction.Text.Value
                        Next
                    End Using

                    DelNote.DataSource = ds
                    If ds.CORHDR.Item(0).PRNT = True Then
                        DelNote.Watermark.Text = "REPRINT " & ds.CORHDR.Item(0).RPRN
                        DelNote.Watermark.TextDirection = DirectionMode.ForwardDiagonal
                        DelNote.Watermark.Font = New Font(DelNote.Watermark.Font.FontFamily, 40)
                        DelNote.Watermark.ForeColor = Color.GhostWhite
                        DelNote.Watermark.TextTransparency = 150
                        DelNote.Watermark.ShowBehind = True
                        DelNote.Watermark.PageRange = "1-5"
                    End If
                    DelNote.XrSellingStoreOrder.Text = ds.CORHDR.Item(0).SellingStoreOrderId.ToString.PadLeft(6, "0"c)

                    DelNote.Print()
                    _orders.Oasys3DB.ExecuteSql("UPDATE CORHDR SET PRNT=1, RPRN=RPRN+1 WHERE NUMB = '" & strOrderNum & "'")

                    If (DelStat >= 300 And DelStat <= 499) Then
                        _orders.Oasys3DB.ExecuteSql("UPDATE CORHDR4 SET DeliveryStatus=500 WHERE NUMB = '" & strOrderNum & "'")
                        _orders.Oasys3DB.ExecuteSql("UPDATE CORLIN SET DeliveryStatus=500 WHERE NUMB = '" & strOrderNum & "' AND DeliveryStatus <> 999")
                        Load_GridOrders(False)
                    End If

                Else
                    MessageBox.Show("No items to pick for this order for this store.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Else
                MessageBox.Show("Order has not been confirmed by OM. Unable to print picking note. Status required = 300 - 399.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Trace.WriteLine("QOD.EXE Deliver Note Print Error: " & ex.Message)
        Finally
            'close explicit connection object
            If IsNothing(SqlConnect) = False Then
                SqlConnect.Close()
                SqlConnect.Dispose()
            End If

            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnquiry.Click
        Dim SqlConnect As New SqlConnection

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim row As System.Data.DataRow = Me.GridvOrders.GetDataRow(Me.GridvOrders.FocusedRowHandle)
            Dim strOrderNum As String = CStr(row("NUMB"))

            Dim ds As New dsEnquiry
            Dim CorhdrTA As New dsEnquiryTableAdapters.CORHDRTableAdapter

            ds.EnforceConstraints = False
            'use info in .\resourecs\connection.xml to connect to database
            'explicitly define connection object and replace dataset's design-time version
            SqlConnect.ConnectionString = Cts.Oasys.Data.GetConnectionString()  ' LoadConnectionFromConfig()
            SqlConnect.Open()
            CorhdrTA.ExplicitSetConnectionPropertyOnly = SqlConnect
            CorhdrTA.Fill(ds.CORHDR, strOrderNum)

            If ds.CORHDR.Rows.Count > 0 Then
                Dim Enquiry As New XtraEnquiry

                If ds.CORHDR.Item(0).CustomerAddress1.ToString = "Customer line 1" Then
                    Enquiry.XrAddr1.Visible = True
                    If ds.CORHDR.Item(0).ADDR1 = "Delivery line 1" Then
                        Enquiry.XrAddr1.Visible = False
                    End If
                    Enquiry.XrCustAddr1.Visible = False
                    Enquiry.XrAddr1.Top = Enquiry.XrCustAddr1.Top
                    Enquiry.XrAddr1.Left = Enquiry.XrCustAddr1.Left
                    Enquiry.XrAddr1.Width = Enquiry.XrCustAddr1.Width
                Else
                    Enquiry.XrAddr1.Visible = False
                    Enquiry.XrCustAddr1.Visible = True
                End If

                If ds.CORHDR.Item(0).CustomerAddress2 = "Customer line 2" Then
                    Enquiry.XrAddr2.Visible = True
                    If ds.CORHDR.Item(0).ADDR2 = "Delivery line 2" Then
                        Enquiry.XrAddr2.Visible = False
                    End If
                    Enquiry.XrCustAddr2.Visible = False
                    Enquiry.XrAddr2.Top = Enquiry.XrCustAddr2.Top
                    Enquiry.XrAddr2.Left = Enquiry.XrCustAddr2.Left
                    Enquiry.XrAddr2.Width = Enquiry.XrCustAddr2.Width
                Else
                    Enquiry.XrAddr2.Visible = False
                    Enquiry.XrCustAddr2.Visible = True
                End If

                If ds.CORHDR.Item(0).CustomerAddress3 = "Customer Town" Then
                    Enquiry.XrAddr3.Visible = True
                    If ds.CORHDR.Item(0).ADDR3 = "Delivery line 3" Then
                        Enquiry.XrAddr3.Visible = False
                    End If
                    Enquiry.XrCustAddr3.Visible = False
                    Enquiry.XrAddr3.Top = Enquiry.XrCustAddr3.Top
                    Enquiry.XrAddr3.Left = Enquiry.XrCustAddr3.Left
                    Enquiry.XrAddr3.Width = Enquiry.XrCustAddr3.Width
                    Enquiry.XrCustPostCode.Visible = False
                Else
                    Enquiry.XrAddr3.Visible = False
                    Enquiry.XrCustAddr3.Visible = True
                End If

                If ds.CORHDR.Item(0).CustomerAddress4 = "Customer line 4" Then
                    Enquiry.XrAddr4.Visible = True
                    If ds.CORHDR.Item(0).ADDR4 = "Delivery line 4" Then
                        Enquiry.XrAddr4.Visible = False
                    End If
                    Enquiry.XrCustAddr4.Visible = False
                    Enquiry.XrAddr4.Top = Enquiry.XrCustAddr4.Top
                    Enquiry.XrAddr4.Left = Enquiry.XrCustAddr4.Left
                    Enquiry.XrAddr4.Width = Enquiry.XrCustAddr4.Width
                Else
                    Enquiry.XrAddr4.Visible = False
                    Enquiry.XrCustAddr4.Visible = True
                End If

                If ds.CORHDR.Item(0).DELC = True Then
                    Enquiry.XrStatus.Text = "Despatched"
                    Enquiry.XrStatus.BackColor = Color.Red
                ElseIf ds.CORHDR.Item(0).DELC = False Then
                    Enquiry.XrStatus.Text = "Sold"
                    Enquiry.XrStatus.BackColor = Color.Green
                End If

                If Val(ds.CORHDR.Item(0).AMDT) > 0 Then
                    Enquiry.XrStatus.Text = "Ammended"
                    Enquiry.XrStatus.BackColor = Color.Yellow
                End If

                Enquiry.XrDelDay.Text = ds.CORHDR.Item(0).DELD.ToString("dddd")
                Dim Corhdr4 As New BOSalesOrders.cOrderHeaderOther(_oasys3DB)
                Corhdr4.AddLoadFilter(clsOasys3DB.eOperator.pEquals, Corhdr4.OrderNo, strOrderNum)
                Corhdr4.LoadCTSMatches()

                Enquiry.XrCashier.Text = Corhdr4.OrderTakerID.Value & " - " & GetEmployeeName(Corhdr4.OrderTakerID.Value)
                Enquiry.XrOrderNum.Text = strOrderNum
                Enquiry.XrSellingOrderNo.Text = ds.CORHDR.Item(0).SellingStoreOrderId.ToString.PadLeft(6, "0"c)
                If ds.CORHDR.Item(0).DELI = True Then
                    Enquiry.XrDelCol.Text = "Delivery Date:"
                Else
                    Enquiry.XrDelCol.Text = "Collection Date:"
                End If

                Enquiry.DataSource = ds
                Enquiry.ShowPreview()
            Else
                MessageBox.Show("No items to pick for this order for this store.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Finally
            'close explicit connection object
            If IsNothing(SqlConnect) = False Then
                SqlConnect.Close()
                SqlConnect.Dispose()
            End If

            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click

        Dim row As System.Data.DataRow = Me.GridvOrders.GetDataRow(Me.GridvOrders.FocusedRowHandle)
        Dim CellValue As String = CStr(row("NUMB"))
        Dim DelStat As Integer = CInt(row("DeliveryStatus"))
        Dim BoOrders As New BOSalesOrders.cOrderHeader(_oasys3DB)

        If txtOrder.Text = "" Then
            _orderNumber = CellValue
        End If

        If DelStat = 9999 Or (DelStat >= 500 And DelStat <= 899) Then

            If BoOrders.LoadAnOrder(_orderNumber) Then
                If BoOrders.DeliveryConfirmed.Value Then
                    Dim ErrorMade As New frmError
                    ErrorMade.ShowDialog(Me)
                    If ErrorMade._AuthorisedId = "" Then
                        'Not Manager Authorised Can't Contine
                        Exit Sub
                    Else
                        'Mark as manager Authorised
                        BoOrders._OrdHeader4.ManagerID.Value = ErrorMade._AuthorisedId
                    End If
                End If
                BoOrders.DeliveryConfirmed.Value = True
                'BoOrders.DeliveryDate.Value = Format(Now, "yyyy-MM-dd")
                If BoOrders.SaveIfExists Then
                    BoOrders._OrdHeader4.DeliveryDate.Value = Today
                    BoOrders._OrdHeader4.DespatchDate.Value = Today
                    BoOrders._OrdHeader4.CustomerName.Value = BoOrders.CustomerName.Value
                    BoOrders._OrdHeader4.CustomerNo.Value = BoOrders.CustomerNo.Value
                    BoOrders._OrdHeader4.SaveIfExists()
                    MessageBox.Show(_orderNumber & " Confirmed " & CStr(IIf(radCol.Checked, "Collection.", "Delivery.")), "Confirmed " & CStr(IIf(radCol.Checked, "Collection.", "Delivery.")), MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtOrder.Focus()
                    If DelStat <> 9999 Then
                        _orders.Oasys3DB.ExecuteSql("UPDATE CORHDR4 SET DeliveryStatus=900 WHERE NUMB = '" & CellValue.ToString & "'")
                        _orders.Oasys3DB.ExecuteSql("UPDATE CORLIN SET DeliveryStatus=900 WHERE NUMB = '" & CellValue.ToString & "' AND DeliveryStatus <> 999")
                    End If
                    Load_GridOrders(False)
                End If
            End If
        Else
            MessageBox.Show("Unable to dispatch - Picking Notes not printed. Status required = 500 - 899.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub


    Private Function GetEmployeeName(ByVal strEmpNum As String) As String
        Using systemUser As New BOSecurityProfile.cSystemUsers(_oasys3DB)
            systemUser.AddLoadFilter(clsOasys3DB.eOperator.pEquals, systemUser.EmployeeCode, strEmpNum)
            systemUser.LoadMatches()
            Return systemUser.Name.Value
        End Using
    End Function

    Private Function GetDataSourceForOLEDB() As String
        Return My.Settings.WixMasterConnectionString
    End Function

    Private Function LoadConnectionFromConfig() As String

        Try
            Dim doc As XmlDocument = New XmlDocument()

            Trace.WriteLine("Get Connection String", Me.GetType.ToString)
            LoadConnectionFromConfig = String.Empty
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory & "Resources\Connection.xml")
            Trace.WriteLine("Get Connection String: retrieving from " & doc.Value, Me.GetType.ToString)

            'search for sql connection string
            For Each node As XmlNode In doc.SelectNodes("configuration/connectionStrings/String")
                If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                    LoadConnectionFromConfig = node.Attributes.GetNamedItem("connectionString").Value

                    Trace.WriteLine("Get Connection String: (Sql Server) " & LoadConnectionFromConfig, Me.GetType.ToString)
                End If
            Next

        Catch ex As Exception
            Throw New OasysDbException(ex.Message, ex)
        End Try

    End Function

End Class