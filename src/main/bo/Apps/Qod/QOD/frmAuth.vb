﻿Public Class frmAuth

    Public _AuthorisedId As String = String.Empty

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Private Sub frmAuth_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        txtId.Focus()
    End Sub

    Private Sub frmAuth_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F12
                btnExit.PerformClick()
            Case Keys.F2
                btnAuth.PerformClick()
        End Select
    End Sub

    Private Sub frmAuth_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        _AuthorisedId = ""
        Me.Hide()
    End Sub

    Private Sub btnAuth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuth.Click
        'Check Auth and Id match in system users
        Dim BoUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
        Dim strMsg As String = String.Empty

        If txtAuth.Text = "" Then
            strMsg = "You must enter a valid autorisation code."
            Call MessageBox.Show(strMsg, "No Authorisation Code", MessageBoxButtons.OK)
            txtAuth.Focus()
        End If

        If txtId.Text = "" Then
            strMsg = "You must enter a manager Id."
            Call MessageBox.Show(strMsg, "No Id Entered", MessageBoxButtons.OK)
            txtId.Focus()
        End If

        If BoUser.CheckAuthorisationPWD(CInt(txtId.Text), txtAuth.Text) = False Then
            strMsg = "Invalid User Id or Authorisation Number."
            Call MessageBox.Show(strMsg, "Invalid Authorisation", MessageBoxButtons.OK)
            txtAuth.Focus()
        Else
            _AuthorisedId = txtId.Text
            Me.Hide()
        End If
    End Sub

    Private Sub txtId_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtId.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtId.Text = txtId.Text.PadLeft(3, "0"c)
            SendKeys.Send("{TAB}")
            Exit Sub
        End If
    End Sub

    Private Sub txtId_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtId.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtId_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtId.TextChanged

    End Sub

    Private Sub txtAuth_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAuth.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If

    End Sub

    Private Sub txtAuth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAuth.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.Handled = True
        End Select

    End Sub
End Class