﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQODData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Me.lblInfo = New System.Windows.Forms.Label
        Me.txtOrder = New System.Windows.Forms.TextBox
        Me.btnConfirm = New System.Windows.Forms.Button
        Me.btnChangeDate = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.GridcOrders = New DevExpress.XtraGrid.GridControl
        Me.GridvOrders = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.radDel = New System.Windows.Forms.RadioButton
        Me.radCol = New System.Windows.Forms.RadioButton
        Me.btnPrintDelNote = New System.Windows.Forms.Button
        Me.btnEnquiry = New System.Windows.Forms.Button
        CType(Me.GridcOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridvOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.Location = New System.Drawing.Point(12, 24)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(174, 20)
        Me.lblInfo.TabIndex = 1
        Me.lblInfo.Text = "Type Order Number: "
        '
        'txtOrder
        '
        Me.txtOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrder.Location = New System.Drawing.Point(180, 21)
        Me.txtOrder.MaxLength = 6
        Me.txtOrder.Name = "txtOrder"
        Me.txtOrder.Size = New System.Drawing.Size(99, 26)
        Me.txtOrder.TabIndex = 1
        '
        'btnConfirm
        '
        Me.btnConfirm.Location = New System.Drawing.Point(526, 21)
        Me.btnConfirm.Name = "btnConfirm"
        Me.btnConfirm.Size = New System.Drawing.Size(113, 26)
        Me.btnConfirm.TabIndex = 4
        Me.btnConfirm.Text = "F2 Despatch Order"
        Me.btnConfirm.UseVisualStyleBackColor = True
        '
        'btnChangeDate
        '
        Me.btnChangeDate.Location = New System.Drawing.Point(646, 21)
        Me.btnChangeDate.Name = "btnChangeDate"
        Me.btnChangeDate.Size = New System.Drawing.Size(113, 26)
        Me.btnChangeDate.TabIndex = 5
        Me.btnChangeDate.Text = "F3 Change Date"
        Me.btnChangeDate.UseVisualStyleBackColor = True
        Me.btnChangeDate.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(835, 341)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(96, 31)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "F12 E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GridcOrders
        '
        Me.GridcOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridcOrders.Location = New System.Drawing.Point(16, 53)
        Me.GridcOrders.MainView = Me.GridvOrders
        Me.GridcOrders.Name = "GridcOrders"
        Me.GridcOrders.Size = New System.Drawing.Size(915, 282)
        Me.GridcOrders.TabIndex = 6
        Me.GridcOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridvOrders, Me.GridView1})
        '
        'GridvOrders
        '
        Me.GridvOrders.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        StyleFormatCondition1.Value1 = New Date(2009, 10, 1, 0, 0, 0, 0)
        Me.GridvOrders.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridvOrders.GridControl = Me.GridcOrders
        Me.GridvOrders.Name = "GridvOrders"
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Name = "GridBand1"
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridcOrders
        Me.GridView1.Name = "GridView1"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(285, 21)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Size = New System.Drawing.Size(235, 26)
        Me.RadioGroup1.TabIndex = 8
        '
        'radDel
        '
        Me.radDel.AutoSize = True
        Me.radDel.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.radDel.Checked = True
        Me.radDel.Location = New System.Drawing.Point(293, 26)
        Me.radDel.Name = "radDel"
        Me.radDel.Size = New System.Drawing.Size(101, 17)
        Me.radDel.TabIndex = 2
        Me.radDel.TabStop = True
        Me.radDel.Text = "Show Deliveries"
        Me.radDel.UseVisualStyleBackColor = False
        '
        'radCol
        '
        Me.radCol.AutoSize = True
        Me.radCol.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.radCol.Location = New System.Drawing.Point(405, 26)
        Me.radCol.Name = "radCol"
        Me.radCol.Size = New System.Drawing.Size(106, 17)
        Me.radCol.TabIndex = 3
        Me.radCol.Text = "Show Collections"
        Me.radCol.UseVisualStyleBackColor = False
        '
        'btnPrintDelNote
        '
        Me.btnPrintDelNote.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintDelNote.Location = New System.Drawing.Point(118, 341)
        Me.btnPrintDelNote.Name = "btnPrintDelNote"
        Me.btnPrintDelNote.Size = New System.Drawing.Size(96, 31)
        Me.btnPrintDelNote.TabIndex = 9
        Me.btnPrintDelNote.Text = "F5 Delivery Note"
        Me.btnPrintDelNote.UseVisualStyleBackColor = True
        '
        'btnEnquiry
        '
        Me.btnEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEnquiry.Location = New System.Drawing.Point(16, 341)
        Me.btnEnquiry.Name = "btnEnquiry"
        Me.btnEnquiry.Size = New System.Drawing.Size(96, 31)
        Me.btnEnquiry.TabIndex = 10
        Me.btnEnquiry.Text = "F4 Enquiry"
        Me.btnEnquiry.UseVisualStyleBackColor = True
        '
        'frmQODData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(943, 384)
        Me.Controls.Add(Me.btnEnquiry)
        Me.Controls.Add(Me.btnPrintDelNote)
        Me.Controls.Add(Me.radCol)
        Me.Controls.Add(Me.radDel)
        Me.Controls.Add(Me.RadioGroup1)
        Me.Controls.Add(Me.GridcOrders)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnChangeDate)
        Me.Controls.Add(Me.btnConfirm)
        Me.Controls.Add(Me.txtOrder)
        Me.Controls.Add(Me.lblInfo)
        Me.Name = "frmQODData"
        Me.ShowIcon = False
        Me.Text = "QOD Despatch Control"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GridcOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridvOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents txtOrder As System.Windows.Forms.TextBox
    Friend WithEvents btnConfirm As System.Windows.Forms.Button
    Friend WithEvents btnChangeDate As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents GridcOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridvOrders As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents radDel As System.Windows.Forms.RadioButton
    Friend WithEvents radCol As System.Windows.Forms.RadioButton
    Friend WithEvents btnPrintDelNote As System.Windows.Forms.Button
    Friend WithEvents btnEnquiry As System.Windows.Forms.Button
End Class
