﻿Public Class frmError

    Public _AuthorisedId As String = String.Empty

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        _AuthorisedId = ""
        Me.Hide()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        _AuthorisedId = ""
        Me.Hide()
    End Sub

    Private Sub btnAuth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuth.Click

        Me.Hide()

        Dim frmAuthorised As New frmAuth
        frmAuthorised.ShowDialog()
        If frmAuthorised._AuthorisedId <> "" Then
            _AuthorisedId = frmAuthorised._AuthorisedId
        Else
            Me.Show()
        End If
        frmAuthorised = Nothing

    End Sub

    Private Sub frmError_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case Keys.KeyCode
            Case Keys.F12
                btnExit.PerformClick()
            Case Keys.F2
                btnOk.PerformClick()
            Case Keys.F11
                btnAuth.PerformClick()
        End Select
    End Sub

 End Class