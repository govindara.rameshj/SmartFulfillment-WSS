﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangeDelivery
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnConfirm = New System.Windows.Forms.Button
        Me.lblCustomer = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.lblCustomerNameTxt = New DevExpress.XtraEditors.LabelControl
        Me.lblAddresstxt = New DevExpress.XtraEditors.LabelControl
        Me.lblPostCodeTxt = New DevExpress.XtraEditors.LabelControl
        Me.lblCurrentDelDatetxt = New DevExpress.XtraEditors.LabelControl
        Me.lblTelNoTxt = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.lblTelNo = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.dteDelDate = New DevExpress.XtraEditors.DateEdit
        CType(Me.dteDelDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDelDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(446, 380)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(90, 37)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnConfirm
        '
        Me.btnConfirm.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConfirm.Location = New System.Drawing.Point(350, 380)
        Me.btnConfirm.Name = "btnConfirm"
        Me.btnConfirm.Size = New System.Drawing.Size(90, 37)
        Me.btnConfirm.TabIndex = 1
        Me.btnConfirm.Text = "F3 Confirm"
        Me.btnConfirm.UseVisualStyleBackColor = True
        '
        'lblCustomer
        '
        Me.lblCustomer.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomer.Appearance.Options.UseFont = True
        Me.lblCustomer.Location = New System.Drawing.Point(31, 35)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(153, 23)
        Me.lblCustomer.TabIndex = 2
        Me.lblCustomer.Text = "Customer Name"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(31, 79)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(174, 23)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Customer Address"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(31, 230)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(185, 23)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Customer Postcode"
        '
        'lblCustomerNameTxt
        '
        Me.lblCustomerNameTxt.Appearance.BackColor = System.Drawing.Color.White
        Me.lblCustomerNameTxt.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomerNameTxt.Appearance.Options.UseBackColor = True
        Me.lblCustomerNameTxt.Appearance.Options.UseFont = True
        Me.lblCustomerNameTxt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblCustomerNameTxt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.lblCustomerNameTxt.Location = New System.Drawing.Point(245, 35)
        Me.lblCustomerNameTxt.Name = "lblCustomerNameTxt"
        Me.lblCustomerNameTxt.Size = New System.Drawing.Size(291, 23)
        Me.lblCustomerNameTxt.TabIndex = 5
        '
        'lblAddresstxt
        '
        Me.lblAddresstxt.Appearance.BackColor = System.Drawing.Color.White
        Me.lblAddresstxt.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddresstxt.Appearance.Options.UseBackColor = True
        Me.lblAddresstxt.Appearance.Options.UseFont = True
        Me.lblAddresstxt.Appearance.Options.UseTextOptions = True
        Me.lblAddresstxt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAddresstxt.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.lblAddresstxt.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lblAddresstxt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblAddresstxt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.lblAddresstxt.Location = New System.Drawing.Point(245, 79)
        Me.lblAddresstxt.Name = "lblAddresstxt"
        Me.lblAddresstxt.Size = New System.Drawing.Size(291, 141)
        Me.lblAddresstxt.TabIndex = 6
        '
        'lblPostCodeTxt
        '
        Me.lblPostCodeTxt.Appearance.BackColor = System.Drawing.Color.White
        Me.lblPostCodeTxt.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCodeTxt.Appearance.Options.UseBackColor = True
        Me.lblPostCodeTxt.Appearance.Options.UseFont = True
        Me.lblPostCodeTxt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblPostCodeTxt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.lblPostCodeTxt.Location = New System.Drawing.Point(245, 230)
        Me.lblPostCodeTxt.Name = "lblPostCodeTxt"
        Me.lblPostCodeTxt.Size = New System.Drawing.Size(144, 23)
        Me.lblPostCodeTxt.TabIndex = 7
        '
        'lblCurrentDelDatetxt
        '
        Me.lblCurrentDelDatetxt.Appearance.BackColor = System.Drawing.Color.White
        Me.lblCurrentDelDatetxt.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentDelDatetxt.Appearance.Options.UseBackColor = True
        Me.lblCurrentDelDatetxt.Appearance.Options.UseFont = True
        Me.lblCurrentDelDatetxt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblCurrentDelDatetxt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.lblCurrentDelDatetxt.Location = New System.Drawing.Point(245, 299)
        Me.lblCurrentDelDatetxt.Name = "lblCurrentDelDatetxt"
        Me.lblCurrentDelDatetxt.Size = New System.Drawing.Size(146, 23)
        Me.lblCurrentDelDatetxt.TabIndex = 8
        '
        'lblTelNoTxt
        '
        Me.lblTelNoTxt.Appearance.BackColor = System.Drawing.Color.White
        Me.lblTelNoTxt.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNoTxt.Appearance.Options.UseBackColor = True
        Me.lblTelNoTxt.Appearance.Options.UseFont = True
        Me.lblTelNoTxt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblTelNoTxt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.lblTelNoTxt.Location = New System.Drawing.Point(245, 259)
        Me.lblTelNoTxt.Name = "lblTelNoTxt"
        Me.lblTelNoTxt.Size = New System.Drawing.Size(291, 23)
        Me.lblTelNoTxt.TabIndex = 9
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(31, 299)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(206, 23)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "Current Delivery Date"
        '
        'lblTelNo
        '
        Me.lblTelNo.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Appearance.Options.UseFont = True
        Me.lblTelNo.Location = New System.Drawing.Point(31, 259)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(197, 23)
        Me.lblTelNo.TabIndex = 11
        Me.lblTelNo.Text = "Customer Telephone"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(31, 331)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(177, 23)
        Me.LabelControl9.TabIndex = 13
        Me.LabelControl9.Text = "New Delivery Date"
        '
        'dteDelDate
        '
        Me.dteDelDate.EditValue = Nothing
        Me.dteDelDate.Location = New System.Drawing.Point(245, 328)
        Me.dteDelDate.Name = "dteDelDate"
        Me.dteDelDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteDelDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteDelDate.Properties.Appearance.Options.UseFont = True
        Me.dteDelDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDelDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteDelDate.Size = New System.Drawing.Size(146, 29)
        Me.dteDelDate.TabIndex = 0
        '
        'frmChangeDelivery
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(548, 429)
        Me.ControlBox = False
        Me.Controls.Add(Me.dteDelDate)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.lblTelNo)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.lblTelNoTxt)
        Me.Controls.Add(Me.lblCurrentDelDatetxt)
        Me.Controls.Add(Me.lblPostCodeTxt)
        Me.Controls.Add(Me.lblAddresstxt)
        Me.Controls.Add(Me.lblCustomerNameTxt)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.lblCustomer)
        Me.Controls.Add(Me.btnConfirm)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "frmChangeDelivery"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "QOD - Change Delivery / Collection Date"
        CType(Me.dteDelDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDelDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnConfirm As System.Windows.Forms.Button
    Friend WithEvents lblCustomer As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCustomerNameTxt As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblAddresstxt As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPostCodeTxt As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCurrentDelDatetxt As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTelNoTxt As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTelNo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dteDelDate As DevExpress.XtraEditors.DateEdit
End Class
