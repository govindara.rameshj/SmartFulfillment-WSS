﻿Public Class frmQOD
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _Orders As New BOSalesOrders.cOrderHeader(_Oasys3DB)

    Private Sub frmQOD_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    End Sub

    Private Sub frmQOD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_GridOrders()
        Me.Show()
        Me.Refresh()
    End Sub

    Private Sub Load_GridOrders()

        GridcOrders.DataSource = _Orders.GetOrdersTable(chkShowDespatchedOrders.Checked)
        GridvOrders.Bands.Clear()

        Dim bandMain As GridBand = GridvOrders.Bands.AddBand("Orders")
        bandMain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandCustomer As GridBand = GridvOrders.Bands.AddBand("Customer")
        bandCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandTran As GridBand = GridvOrders.Bands.AddBand("Transaction")
        bandTran.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        For Each col As DevExpress.XtraGrid.Columns.GridColumn In GridvOrders.Columns
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Select Case col.FieldName
                Case _Orders.Column(cOrderHeader.ColumnNames.OrderNumber)
                    col.Caption = "Order No:"
                    col.BestFit()
                    bandMain.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.OrderDate)
                    col.Caption = "Date"
                    col.BestFit()
                    bandMain.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.CustomerNumber)
                    col.Caption = "Customer No:"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.CustomerName)
                    col.Caption = "Customer Name"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.DeliveryAddress1)
                    col.Caption = "Address1"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.DeliveryAddress2)
                    col.Caption = "Address2"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.DeliveryAddress3)
                    col.Caption = "Address3"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.DeliveryAddress4)
                    col.Caption = "Address4"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.PostCode)
                    col.Caption = "Pst Code"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.TelephoneNumber)
                    col.Caption = "Tel"
                    bandCustomer.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.TillID)
                    col.Caption = "Till Id"
                    bandTran.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.TranDate)
                    col.Caption = "Tran Date"
                    bandTran.Columns.Add(col)
                Case _Orders.Column(cOrderHeader.ColumnNames.TranNumber)
                    col.Caption = "Tran No:"
                    bandTran.Columns.Add(col)
            End Select
        Next col

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub
End Class
