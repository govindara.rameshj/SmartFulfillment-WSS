<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class XtraDelNoteRep
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDelNoteRep))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.DsOrders4 = New QOD.dsOrders
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrCustPostCode = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCustAddr4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCustAddr3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCustAddr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrCustAddr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDelColNew = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDayName = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDispatchDate = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreFax = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStorePhone = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreAddr3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreAdd2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreAddr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrStoreName = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDatePrinted = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox
        Me.XrOrderNum = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDelCol = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSellingStoreId = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSellingStoreOrder = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.XrDelIns = New DevExpress.XtraReports.UI.XRLabel
        Me.XrTotalCost = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.DateFormat = New DevExpress.XtraReports.UI.FormattingRule
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.QtyToTake = New DevExpress.XtraReports.UI.CalculatedField
        Me.QtyMinusRefund = New DevExpress.XtraReports.UI.CalculatedField
        Me.XrAddr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAddr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAddr3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAddr4 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.DsOrders4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel15, Me.XrLabel3, Me.XrLabel24, Me.XrLabel2, Me.XrLabel4, Me.XrLabel6})
        Me.Detail.Height = 20
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel15
        '
        Me.XrLabel15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.Price", "")})
        Me.XrLabel15.Location = New System.Drawing.Point(467, 0)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.Size = New System.Drawing.Size(75, 17)
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "XrLabel15"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'DsOrders4
        '
        Me.DsOrders4.DataSetName = "dsOrders"
        Me.DsOrders4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CALC", "{0:#0}")})
        Me.XrLabel3.Location = New System.Drawing.Point(392, 0)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "XrLabel3"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel24
        '
        Me.XrLabel24.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.VOLU", "{0:#0.0} m3 ")})
        Me.XrLabel24.Location = New System.Drawing.Point(625, 0)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.Size = New System.Drawing.Size(83, 15)
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "XrLabel24"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.SKUN", "")})
        Me.XrLabel2.Location = New System.Drawing.Point(8, 0)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.Size = New System.Drawing.Size(67, 15)
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "XrLabel15"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.DESCR", "")})
        Me.XrLabel4.Location = New System.Drawing.Point(83, 0)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.Size = New System.Drawing.Size(300, 15)
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "XrLabel21"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.WGHT", "{0:#0} Kg")})
        Me.XrLabel6.Location = New System.Drawing.Point(550, 0)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.Size = New System.Drawing.Size(75, 15)
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "XrLabel23"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel22, Me.XrPanel2, Me.XrLabel16, Me.XrLabel17, Me.XrLabel18, Me.XrLabel19, Me.XrLabel20, Me.XrLine1, Me.XrPictureBox1, Me.XrOrderNum, Me.XrDelCol, Me.XrLabel1, Me.XrLabel13, Me.XrLabel9, Me.XrSellingStoreId, Me.XrSellingStoreOrder})
        Me.PageHeader.Height = 434
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel1
        '
        Me.XrPanel1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrAddr4, Me.XrAddr3, Me.XrAddr2, Me.XrAddr1, Me.XrCustPostCode, Me.XrCustAddr4, Me.XrCustAddr3, Me.XrCustAddr2, Me.XrCustAddr1, Me.XrLabel33, Me.XrLabel34, Me.XrLabel35, Me.XrLabel36, Me.XrLabel37, Me.XrDelColNew, Me.XrLabel39, Me.XrLabel41, Me.XrLabel46, Me.XrLabel47, Me.XrLabel48, Me.XrLabel49, Me.XrLabel50, Me.XrDayName, Me.XrLabel5, Me.XrDispatchDate, Me.XrLabel21, Me.XrLabel23, Me.XrLabel38, Me.XrLabel51})
        Me.XrPanel1.Location = New System.Drawing.Point(8, 100)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.Size = New System.Drawing.Size(333, 292)
        Me.XrPanel1.StylePriority.UseBorderColor = False
        Me.XrPanel1.StylePriority.UseBorders = False
        '
        'XrCustPostCode
        '
        Me.XrCustPostCode.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrCustPostCode.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerPostcode", "")})
        Me.XrCustPostCode.Location = New System.Drawing.Point(117, 93)
        Me.XrCustPostCode.Name = "XrCustPostCode"
        Me.XrCustPostCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCustPostCode.Size = New System.Drawing.Size(208, 17)
        Me.XrCustPostCode.StylePriority.UseBorders = False
        Me.XrCustPostCode.Text = "XrCustPostCode"
        '
        'XrCustAddr4
        '
        Me.XrCustAddr4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrCustAddr4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerAddress4", "")})
        Me.XrCustAddr4.Location = New System.Drawing.Point(117, 76)
        Me.XrCustAddr4.Name = "XrCustAddr4"
        Me.XrCustAddr4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCustAddr4.Size = New System.Drawing.Size(208, 17)
        Me.XrCustAddr4.StylePriority.UseBorders = False
        Me.XrCustAddr4.Text = "XrCustAddr4"
        '
        'XrCustAddr3
        '
        Me.XrCustAddr3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrCustAddr3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerAddress3", "")})
        Me.XrCustAddr3.Location = New System.Drawing.Point(117, 59)
        Me.XrCustAddr3.Name = "XrCustAddr3"
        Me.XrCustAddr3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCustAddr3.Size = New System.Drawing.Size(208, 17)
        Me.XrCustAddr3.StylePriority.UseBorders = False
        Me.XrCustAddr3.Text = "XrCustAddr3"
        '
        'XrCustAddr2
        '
        Me.XrCustAddr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrCustAddr2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerAddress2", "")})
        Me.XrCustAddr2.Location = New System.Drawing.Point(117, 42)
        Me.XrCustAddr2.Name = "XrCustAddr2"
        Me.XrCustAddr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCustAddr2.Size = New System.Drawing.Size(208, 17)
        Me.XrCustAddr2.StylePriority.UseBorders = False
        Me.XrCustAddr2.Text = "XrCustAddr2"
        '
        'XrCustAddr1
        '
        Me.XrCustAddr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrCustAddr1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerAddress1", "")})
        Me.XrCustAddr1.Location = New System.Drawing.Point(117, 25)
        Me.XrCustAddr1.Name = "XrCustAddr1"
        Me.XrCustAddr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrCustAddr1.Size = New System.Drawing.Size(208, 17)
        Me.XrCustAddr1.StylePriority.UseBorders = False
        Me.XrCustAddr1.Text = "XrCustAddr1"
        '
        'XrLabel33
        '
        Me.XrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel33.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.Size = New System.Drawing.Size(101, 17)
        Me.XrLabel33.StylePriority.UseBorders = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.Text = "Customer Name:"
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel34
        '
        Me.XrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel34.Location = New System.Drawing.Point(9, 25)
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel34.StylePriority.UseBorders = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "Address:"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel35
        '
        Me.XrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel35.Location = New System.Drawing.Point(9, 133)
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel35.StylePriority.UseBorders = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "Telephone:"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel36
        '
        Me.XrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel36.Location = New System.Drawing.Point(8, 150)
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel36.StylePriority.UseBorders = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.Text = "Mobile:"
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel37
        '
        Me.XrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel37.Location = New System.Drawing.Point(8, 216)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel37.StylePriority.UseBorders = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.Text = "Order Date:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDelColNew
        '
        Me.XrDelColNew.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDelColNew.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrDelColNew.Location = New System.Drawing.Point(9, 250)
        Me.XrDelColNew.Name = "XrDelColNew"
        Me.XrDelColNew.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDelColNew.Size = New System.Drawing.Size(100, 17)
        Me.XrDelColNew.StylePriority.UseBorders = False
        Me.XrDelColNew.StylePriority.UseFont = False
        Me.XrDelColNew.StylePriority.UseTextAlignment = False
        Me.XrDelColNew.Text = "Delivery Date:"
        Me.XrDelColNew.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel39
        '
        Me.XrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel39.Location = New System.Drawing.Point(8, 267)
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel39.StylePriority.UseBorders = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "Order Value:"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel41
        '
        Me.XrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel41.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.NAME", "")})
        Me.XrLabel41.Location = New System.Drawing.Point(117, 8)
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.Size = New System.Drawing.Size(208, 17)
        Me.XrLabel41.StylePriority.UseBorders = False
        Me.XrLabel41.StylePriority.UseTextAlignment = False
        Me.XrLabel41.Text = "XrLabel41"
        Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel46
        '
        Me.XrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel46.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.PHON", "")})
        Me.XrLabel46.Location = New System.Drawing.Point(117, 133)
        Me.XrLabel46.Name = "XrLabel46"
        Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel46.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel46.StylePriority.UseBorders = False
        Me.XrLabel46.StylePriority.UseTextAlignment = False
        Me.XrLabel46.Text = "XrLabel46"
        Me.XrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel47
        '
        Me.XrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel47.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.MOBP", "")})
        Me.XrLabel47.Location = New System.Drawing.Point(117, 150)
        Me.XrLabel47.Name = "XrLabel47"
        Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel47.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel47.StylePriority.UseBorders = False
        Me.XrLabel47.StylePriority.UseTextAlignment = False
        Me.XrLabel47.Text = "XrLabel47"
        Me.XrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel48
        '
        Me.XrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel48.Location = New System.Drawing.Point(117, 216)
        Me.XrLabel48.Name = "XrLabel48"
        Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel48.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel48.StylePriority.UseBorders = False
        Me.XrLabel48.StylePriority.UseTextAlignment = False
        Me.XrLabel48.Text = "[CORHDR.DATE1!dd/MM/yyyy]"
        Me.XrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel49
        '
        Me.XrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel49.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel49.Location = New System.Drawing.Point(117, 250)
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel49.StylePriority.UseBorders = False
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.StylePriority.UseTextAlignment = False
        Me.XrLabel49.Text = "[CORHDR.DELD!dd/MM/yyyy]"
        Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel50
        '
        Me.XrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel50.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.MVST", "{0:" & ChrW(163) & "0.00}")})
        Me.XrLabel50.Location = New System.Drawing.Point(117, 267)
        Me.XrLabel50.Name = "XrLabel50"
        Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel50.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel50.StylePriority.UseBorders = False
        Me.XrLabel50.StylePriority.UseTextAlignment = False
        Me.XrLabel50.Text = "XrLabel50"
        Me.XrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDayName
        '
        Me.XrDayName.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDayName.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrDayName.Location = New System.Drawing.Point(225, 250)
        Me.XrDayName.Name = "XrDayName"
        Me.XrDayName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDayName.Size = New System.Drawing.Size(100, 17)
        Me.XrDayName.StylePriority.UseBorders = False
        Me.XrDayName.StylePriority.UseFont = False
        Me.XrDayName.StylePriority.UseTextAlignment = False
        Me.XrDayName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel5.Location = New System.Drawing.Point(8, 233)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Dispatch Date:"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDispatchDate
        '
        Me.XrDispatchDate.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDispatchDate.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.XrDispatchDate.Location = New System.Drawing.Point(117, 233)
        Me.XrDispatchDate.Name = "XrDispatchDate"
        Me.XrDispatchDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDispatchDate.Size = New System.Drawing.Size(100, 17)
        Me.XrDispatchDate.StylePriority.UseBorders = False
        Me.XrDispatchDate.StylePriority.UseFont = False
        Me.XrDispatchDate.StylePriority.UseTextAlignment = False
        Me.XrDispatchDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel21
        '
        Me.XrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel21.Location = New System.Drawing.Point(8, 167)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel21.StylePriority.UseBorders = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "Work Phone:"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel23
        '
        Me.XrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel23.Location = New System.Drawing.Point(9, 184)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel23.StylePriority.UseBorders = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "Email:"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel38
        '
        Me.XrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel38.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.CustomerEmail", "")})
        Me.XrLabel38.Location = New System.Drawing.Point(117, 184)
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.Size = New System.Drawing.Size(193, 17)
        Me.XrLabel38.StylePriority.UseBorders = False
        Me.XrLabel38.Text = "XrLabel38"
        '
        'XrLabel51
        '
        Me.XrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel51.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.PhoneNumberWork", "")})
        Me.XrLabel51.Location = New System.Drawing.Point(117, 167)
        Me.XrLabel51.Name = "XrLabel51"
        Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel51.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel51.StylePriority.UseBorders = False
        Me.XrLabel51.Text = "XrLabel51"
        '
        'XrLabel22
        '
        Me.XrLabel22.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel22.Location = New System.Drawing.Point(467, 400)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.Size = New System.Drawing.Size(75, 17)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.Text = "Price"
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPanel2
        '
        Me.XrPanel2.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel7, Me.XrStoreFax, Me.XrStorePhone, Me.XrStoreAddr3, Me.XrStoreAdd2, Me.XrStoreAddr1, Me.XrStoreName, Me.XrDatePrinted, Me.XrLabel40, Me.XrLabel14, Me.XrLabel12, Me.XrLabel11})
        Me.XrPanel2.Location = New System.Drawing.Point(367, 100)
        Me.XrPanel2.Name = "XrPanel2"
        Me.XrPanel2.Size = New System.Drawing.Size(333, 292)
        Me.XrPanel2.StylePriority.UseBorderColor = False
        Me.XrPanel2.StylePriority.UseBorders = False
        '
        'XrLabel8
        '
        Me.XrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.RPRN", "{0:#0}")})
        Me.XrLabel8.Location = New System.Drawing.Point(108, 233)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel8.StylePriority.UseBorders = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "XrLabel8"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel7.Location = New System.Drawing.Point(8, 233)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel7.StylePriority.UseBorders = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Reprint:"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreFax
        '
        Me.XrStoreFax.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStoreFax.Location = New System.Drawing.Point(108, 133)
        Me.XrStoreFax.Name = "XrStoreFax"
        Me.XrStoreFax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreFax.Size = New System.Drawing.Size(100, 25)
        Me.XrStoreFax.StylePriority.UseBorders = False
        Me.XrStoreFax.StylePriority.UseTextAlignment = False
        Me.XrStoreFax.Text = "0123456789"
        Me.XrStoreFax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStorePhone
        '
        Me.XrStorePhone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStorePhone.Location = New System.Drawing.Point(108, 108)
        Me.XrStorePhone.Name = "XrStorePhone"
        Me.XrStorePhone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStorePhone.Size = New System.Drawing.Size(100, 25)
        Me.XrStorePhone.StylePriority.UseBorders = False
        Me.XrStorePhone.StylePriority.UseTextAlignment = False
        Me.XrStorePhone.Text = "0123456789"
        Me.XrStorePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreAddr3
        '
        Me.XrStoreAddr3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStoreAddr3.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStoreAddr3.Location = New System.Drawing.Point(8, 83)
        Me.XrStoreAddr3.Name = "XrStoreAddr3"
        Me.XrStoreAddr3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreAddr3.Size = New System.Drawing.Size(317, 25)
        Me.XrStoreAddr3.StylePriority.UseBorders = False
        Me.XrStoreAddr3.StylePriority.UseFont = False
        Me.XrStoreAddr3.StylePriority.UseTextAlignment = False
        Me.XrStoreAddr3.Text = "Hampshire"
        Me.XrStoreAddr3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreAdd2
        '
        Me.XrStoreAdd2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStoreAdd2.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStoreAdd2.Location = New System.Drawing.Point(8, 58)
        Me.XrStoreAdd2.Name = "XrStoreAdd2"
        Me.XrStoreAdd2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreAdd2.Size = New System.Drawing.Size(317, 25)
        Me.XrStoreAdd2.StylePriority.UseBorders = False
        Me.XrStoreAdd2.StylePriority.UseFont = False
        Me.XrStoreAdd2.StylePriority.UseTextAlignment = False
        Me.XrStoreAdd2.Text = "Farnborough"
        Me.XrStoreAdd2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreAddr1
        '
        Me.XrStoreAddr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStoreAddr1.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStoreAddr1.Location = New System.Drawing.Point(8, 33)
        Me.XrStoreAddr1.Name = "XrStoreAddr1"
        Me.XrStoreAddr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreAddr1.Size = New System.Drawing.Size(317, 25)
        Me.XrStoreAddr1.StylePriority.UseBorders = False
        Me.XrStoreAddr1.StylePriority.UseFont = False
        Me.XrStoreAddr1.StylePriority.UseTextAlignment = False
        Me.XrStoreAddr1.Text = "13b Invincible Road"
        Me.XrStoreAddr1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrStoreName
        '
        Me.XrStoreName.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrStoreName.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrStoreName.Location = New System.Drawing.Point(8, 8)
        Me.XrStoreName.Name = "XrStoreName"
        Me.XrStoreName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrStoreName.Size = New System.Drawing.Size(317, 25)
        Me.XrStoreName.StylePriority.UseBorders = False
        Me.XrStoreName.StylePriority.UseFont = False
        Me.XrStoreName.StylePriority.UseTextAlignment = False
        Me.XrStoreName.Text = "Wickes Farnborough"
        Me.XrStoreName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDatePrinted
        '
        Me.XrDatePrinted.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDatePrinted.Location = New System.Drawing.Point(108, 208)
        Me.XrDatePrinted.Name = "XrDatePrinted"
        Me.XrDatePrinted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDatePrinted.Size = New System.Drawing.Size(100, 25)
        Me.XrDatePrinted.StylePriority.UseBorders = False
        Me.XrDatePrinted.StylePriority.UseTextAlignment = False
        Me.XrDatePrinted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel40
        '
        Me.XrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel40.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel40.Location = New System.Drawing.Point(8, 258)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.Size = New System.Drawing.Size(317, 25)
        Me.XrLabel40.StylePriority.UseBorders = False
        Me.XrLabel40.StylePriority.UseFont = False
        Me.XrLabel40.StylePriority.UseTextAlignment = False
        Me.XrLabel40.Text = "Store Copy"
        Me.XrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel14.Location = New System.Drawing.Point(8, 208)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Date Printed:"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel12.Location = New System.Drawing.Point(8, 133)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Store Fax:"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel11.Location = New System.Drawing.Point(8, 108)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "Store Telephone:"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.Location = New System.Drawing.Point(8, 400)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "SKU #"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel17
        '
        Me.XrLabel17.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel17.Location = New System.Drawing.Point(83, 400)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.Size = New System.Drawing.Size(300, 17)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "Description"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel18
        '
        Me.XrLabel18.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel18.Location = New System.Drawing.Point(392, 400)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "Quantity"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel19
        '
        Me.XrLabel19.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel19.Location = New System.Drawing.Point(550, 400)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.Size = New System.Drawing.Size(75, 17)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Weight"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel20
        '
        Me.XrLabel20.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel20.Location = New System.Drawing.Point(625, 400)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.Size = New System.Drawing.Size(83, 17)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Volume"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLine1
        '
        Me.XrLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine1.Location = New System.Drawing.Point(8, 417)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.Size = New System.Drawing.Size(700, 17)
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.Location = New System.Drawing.Point(442, 0)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.Size = New System.Drawing.Size(258, 92)
        '
        'XrOrderNum
        '
        Me.XrOrderNum.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrOrderNum.Location = New System.Drawing.Point(250, 42)
        Me.XrOrderNum.Name = "XrOrderNum"
        Me.XrOrderNum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrOrderNum.Size = New System.Drawing.Size(108, 33)
        Me.XrOrderNum.StylePriority.UseFont = False
        Me.XrOrderNum.Text = "XrOrderNum"
        '
        'XrDelCol
        '
        Me.XrDelCol.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrDelCol.Location = New System.Drawing.Point(8, 75)
        Me.XrDelCol.Name = "XrDelCol"
        Me.XrDelCol.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDelCol.Size = New System.Drawing.Size(333, 25)
        Me.XrDelCol.StylePriority.UseFont = False
        Me.XrDelCol.Text = "Delivery Note"
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.Size = New System.Drawing.Size(158, 33)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "Customer Order:"
        '
        'XrLabel13
        '
        Me.XrLabel13.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.Location = New System.Drawing.Point(8, 42)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.Size = New System.Drawing.Size(158, 33)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Delivery Id:"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel9
        '
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.DeliverySource", "")})
        Me.XrLabel9.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel9.Location = New System.Drawing.Point(167, 42)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.Size = New System.Drawing.Size(83, 33)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "XrLabel9"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrSellingStoreId
        '
        Me.XrSellingStoreId.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.SellingStoreId", "")})
        Me.XrSellingStoreId.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrSellingStoreId.Location = New System.Drawing.Point(167, 8)
        Me.XrSellingStoreId.Name = "XrSellingStoreId"
        Me.XrSellingStoreId.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrSellingStoreId.Size = New System.Drawing.Size(83, 33)
        Me.XrSellingStoreId.StylePriority.UseFont = False
        Me.XrSellingStoreId.StylePriority.UseTextAlignment = False
        Me.XrSellingStoreId.Text = "XrSellingStoreId"
        Me.XrSellingStoreId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrSellingStoreOrder
        '
        Me.XrSellingStoreOrder.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrSellingStoreOrder.Location = New System.Drawing.Point(250, 8)
        Me.XrSellingStoreOrder.Name = "XrSellingStoreOrder"
        Me.XrSellingStoreOrder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrSellingStoreOrder.Size = New System.Drawing.Size(108, 33)
        Me.XrSellingStoreOrder.StylePriority.UseFont = False
        Me.XrSellingStoreOrder.StylePriority.UseTextAlignment = False
        Me.XrSellingStoreOrder.Text = "Delivery Id:"
        Me.XrSellingStoreOrder.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrDelIns, Me.XrTotalCost, Me.XrLine6, Me.XrLabel32, Me.XrLabel31, Me.XrLabel30, Me.XrPanel3})
        Me.PageFooter.Height = 292
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrDelIns
        '
        Me.XrDelIns.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDelIns.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrDelIns.Location = New System.Drawing.Point(8, 17)
        Me.XrDelIns.Name = "XrDelIns"
        Me.XrDelIns.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDelIns.Size = New System.Drawing.Size(692, 42)
        Me.XrDelIns.StylePriority.UseBorders = False
        Me.XrDelIns.StylePriority.UseFont = False
        Me.XrDelIns.StylePriority.UseTextAlignment = False
        Me.XrDelIns.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrTotalCost
        '
        Me.XrTotalCost.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrTotalCost.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrTotalCost.Location = New System.Drawing.Point(533, 108)
        Me.XrTotalCost.Name = "XrTotalCost"
        Me.XrTotalCost.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTotalCost.Size = New System.Drawing.Size(83, 25)
        Me.XrTotalCost.StylePriority.UseBorders = False
        Me.XrTotalCost.StylePriority.UseFont = False
        Me.XrTotalCost.StylePriority.UseTextAlignment = False
        Me.XrTotalCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine6
        '
        Me.XrLine6.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine6.Location = New System.Drawing.Point(8, 0)
        Me.XrLine6.Name = "XrLine6"
        Me.XrLine6.Size = New System.Drawing.Size(700, 8)
        Me.XrLine6.StylePriority.UseForeColor = False
        '
        'XrLabel32
        '
        Me.XrLabel32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CORHDR.DCST", "{0:" & ChrW(163) & "0.00}")})
        Me.XrLabel32.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel32.Location = New System.Drawing.Point(533, 75)
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.Size = New System.Drawing.Size(83, 25)
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.StylePriority.UseTextAlignment = False
        Me.XrLabel32.Text = "XrLabel32"
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel31
        '
        Me.XrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel31.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel31.Location = New System.Drawing.Point(408, 108)
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.Size = New System.Drawing.Size(83, 25)
        Me.XrLabel31.StylePriority.UseBorders = False
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "Order Total:"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel30
        '
        Me.XrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel30.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel30.Location = New System.Drawing.Point(408, 75)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel30.StylePriority.UseBorders = False
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "Delivery Charge:"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrPanel3
        '
        Me.XrPanel3.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel25, Me.XrLabel26, Me.XrLine2, Me.XrLabel27, Me.XrLine3, Me.XrLabel28, Me.XrLine4, Me.XrLabel29, Me.XrLine5})
        Me.XrPanel3.Location = New System.Drawing.Point(8, 67)
        Me.XrPanel3.Name = "XrPanel3"
        Me.XrPanel3.Size = New System.Drawing.Size(300, 225)
        Me.XrPanel3.StylePriority.UseBorderColor = False
        Me.XrPanel3.StylePriority.UseBorders = False
        '
        'XrLabel25
        '
        Me.XrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel25.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel25.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.Size = New System.Drawing.Size(242, 25)
        Me.XrLabel25.StylePriority.UseBorders = False
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.Text = "Delivery Service Control"
        '
        'XrLabel26
        '
        Me.XrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel26.Location = New System.Drawing.Point(8, 42)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.Text = "Drivers Signature" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XrLine2
        '
        Me.XrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine2.Location = New System.Drawing.Point(117, 58)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.Size = New System.Drawing.Size(175, 25)
        Me.XrLine2.StylePriority.UseBorders = False
        Me.XrLine2.StylePriority.UseForeColor = False
        '
        'XrLabel27
        '
        Me.XrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel27.Location = New System.Drawing.Point(8, 83)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel27.StylePriority.UseBorders = False
        Me.XrLabel27.Text = "Store Pick Signature"
        '
        'XrLine3
        '
        Me.XrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine3.Location = New System.Drawing.Point(117, 109)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.Size = New System.Drawing.Size(175, 17)
        Me.XrLine3.StylePriority.UseBorders = False
        Me.XrLine3.StylePriority.UseForeColor = False
        '
        'XrLabel28
        '
        Me.XrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel28.Location = New System.Drawing.Point(8, 133)
        Me.XrLabel28.Multiline = True
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel28.StylePriority.UseBorders = False
        Me.XrLabel28.Text = "Store Check Signature"
        '
        'XrLine4
        '
        Me.XrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine4.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine4.Location = New System.Drawing.Point(117, 158)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.Size = New System.Drawing.Size(175, 17)
        Me.XrLine4.StylePriority.UseBorders = False
        Me.XrLine4.StylePriority.UseForeColor = False
        '
        'XrLabel29
        '
        Me.XrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel29.Location = New System.Drawing.Point(9, 175)
        Me.XrLabel29.Multiline = True
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.Text = "Customers Signature"
        '
        'XrLine5
        '
        Me.XrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine5.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine5.Location = New System.Drawing.Point(117, 200)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.Size = New System.Drawing.Size(175, 17)
        Me.XrLine5.StylePriority.UseBorders = False
        Me.XrLine5.StylePriority.UseForeColor = False
        '
        'DateFormat
        '
        Me.DateFormat.Name = "DateFormat"
        '
        'XrLabel10
        '
        Me.XrLabel10.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel10.Location = New System.Drawing.Point(533, 456)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "Quantity Ordered"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'QtyToTake
        '
        Me.QtyToTake.DataMember = "CORHDR"
        Me.QtyToTake.DataSource = Me.DsOrders4
        Me.QtyToTake.DisplayName = "QTYTOTAKE"
        Me.QtyToTake.Expression = "[QTYO]-[QTYT]+ [QTYR] "
        Me.QtyToTake.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
        Me.QtyToTake.Name = "QtyToTake"
        '
        'QtyMinusRefund
        '
        Me.QtyMinusRefund.DataMember = "CORHDR"
        Me.QtyMinusRefund.DataSource = Me.DsOrders4
        Me.QtyMinusRefund.Expression = "[QTYO]+[QTYR]"
        Me.QtyMinusRefund.FieldType = DevExpress.XtraReports.UI.FieldType.Int32
        Me.QtyMinusRefund.Name = "QtyMinusRefund"
        '
        'XrAddr1
        '
        Me.XrAddr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrAddr1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.ADDR1", "")})
        Me.XrAddr1.Location = New System.Drawing.Point(8, 50)
        Me.XrAddr1.Name = "XrAddr1"
        Me.XrAddr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrAddr1.Size = New System.Drawing.Size(100, 17)
        Me.XrAddr1.StylePriority.UseBorders = False
        Me.XrAddr1.Text = "XrAddr1"
        Me.XrAddr1.Visible = False
        '
        'XrAddr2
        '
        Me.XrAddr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrAddr2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.ADDR2", "")})
        Me.XrAddr2.Location = New System.Drawing.Point(8, 67)
        Me.XrAddr2.Name = "XrAddr2"
        Me.XrAddr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrAddr2.Size = New System.Drawing.Size(100, 17)
        Me.XrAddr2.StylePriority.UseBorders = False
        Me.XrAddr2.Text = "XrAddr2"
        Me.XrAddr2.Visible = False
        '
        'XrAddr3
        '
        Me.XrAddr3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrAddr3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.ADDR3", "")})
        Me.XrAddr3.Location = New System.Drawing.Point(8, 84)
        Me.XrAddr3.Name = "XrAddr3"
        Me.XrAddr3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrAddr3.Size = New System.Drawing.Size(100, 17)
        Me.XrAddr3.StylePriority.UseBorders = False
        Me.XrAddr3.Text = "XrAddr3"
        Me.XrAddr3.Visible = False
        '
        'XrAddr4
        '
        Me.XrAddr4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrAddr4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.DsOrders4, "CORHDR.ADDR4", "")})
        Me.XrAddr4.Location = New System.Drawing.Point(8, 101)
        Me.XrAddr4.Name = "XrAddr4"
        Me.XrAddr4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrAddr4.Size = New System.Drawing.Size(100, 17)
        Me.XrAddr4.StylePriority.UseBorders = False
        Me.XrAddr4.Text = "XrAddr4"
        Me.XrAddr4.Visible = False
        '
        'XtraDelNoteRep
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.QtyToTake, Me.QtyMinusRefund})
        Me.DataAdapter = Me.DsOrders4
        Me.DataSource = Me.DsOrders4.CORHDR
        Me.DataSourceSchema = resources.GetString("$this.DataSourceSchema")
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.DateFormat})
        Me.Margins = New System.Drawing.Printing.Margins(60, 55, 35, 49)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Version = "9.1"
        CType(Me.DsOrders4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDelCol As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTotalCost As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDatePrinted As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStorePhone As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreAddr3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreAdd2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreAddr1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrStoreFax As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DsOrders4 As QOD.dsOrders
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents DateFormat As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents XrOrderNum As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents QtyToTake As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents QtyMinusRefund As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDelIns As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSellingStoreId As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSellingStoreOrder As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDelColNew As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel46 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel47 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel48 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel49 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel50 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDayName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDispatchDate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel51 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCustPostCode As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCustAddr4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCustAddr3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCustAddr2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCustAddr1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAddr3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAddr2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAddr1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAddr4 As DevExpress.XtraReports.UI.XRLabel
End Class
