﻿Public Class frmChangeDelivery

    Public strCustomerName As String = String.Empty
    Public strAddress As String = String.Empty
    Public strPostCode As String = String.Empty
    Public strTelNo As String = String.Empty
    Public strCurDelDate As String = String.Empty
    Public dteNewDelDate As Date = Nothing

    Private Sub frmChangeDelivery_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dteDelDate.Properties.MinValue = DateAdd(DateInterval.Day, 1, Now)
        dteDelDate.DateTime = DateAdd(DateInterval.Day, 1, Now)

        lblCustomerNameTxt.Text = strCustomerName
        lblAddresstxt.Text = strAddress
        lblPostCodeTxt.Text = strPostCode
        lblTelNoTxt.Text = strTelNo
        lblCurrentDelDatetxt.Text = strCurDelDate

    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        dteNewDelDate = dteDelDate.DateTime
        Me.Hide()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        dteNewDelDate = Nothing
        Me.Hide()
    End Sub


End Class