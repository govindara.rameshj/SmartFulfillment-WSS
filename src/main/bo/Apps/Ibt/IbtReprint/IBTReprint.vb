﻿Imports System.Threading.Thread
Imports IBTCore

Public Class IbtReprint
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private DRLno As String = String.Empty
    Private drldate As String = String.Empty
    Private drlstore As String = String.Empty
    Private drlcomment As String = String.Empty
    Private drlvalue As String = String.Empty
    Private CurStore As Integer = 0
    Private CurStoreName As String = String.Empty
    Private StoreName As String = String.Empty
    Private toStore As Integer = 0
    Private toStoreName As String = String.Empty
    Private rowSelected As Boolean = False
    Private totalvalue As Decimal = 0
    Private blnError As Boolean = False
    Private _strUser As String = String.Empty
    Private _AddressLineFactory As IIbtStoreName

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        Dim BOUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
        _strUser = BOUser.GetUser(userId).Initials.Value.Trim
    End Sub

    Protected Overrides Sub DoProcessing()

        'key mappings
        _AddressLineFactory = (New IbtStoreNameFactory).GetImplementation
        Dim AncestorFocusedMap As InputMap = ssGrid1.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap As InputMap = ssGrid1.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.Home, Keys.None), SpreadActions.MoveToFirstRow)
        FocusedMap.Put(New Keystroke(Keys.End, Keys.None), SpreadActions.MoveToLastItem)
        pnlWaitMsg.Visible = True
        Me.Refresh()
        Application.DoEvents()
        loaddata()
        pnlWaitMsg.Visible = False
        rowSelected = False
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3 : btnReset.PerformClick()
            Case Keys.F4 : btnPrint.PerformClick()
            Case Keys.F10 : BtnExit.PerformClick()
            Case Else : e.Handled = False : MyBase.Form_KeyDown(sender, e)
        End Select

    End Sub

    Private Sub BtnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExit.Click
        FindForm.Close()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        DisplayStatus()
        loaddata()
        rowSelected = False
    End Sub

    Private Sub loaddata()

        Dim BODRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)
        Dim lstHeader As List(Of BOPurchases.cDrlHeader)
        Dim boRO As New BOSystem.cRetailOptions(_Oasys3DB)
        'Dim storeNo As Integer
        boRO.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boRO.RetailOptionsID, "01")
        boRO.LoadMatches()

        CurStore = CInt(boRO.Store.Value)
        CurStoreName = Trim(boRO.StoreName.Value)

        BODRLHeader.ClearLoadFilter()
        BODRLHeader.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BODRLHeader.Type, "2")
        BODRLHeader.SortBy(BODRLHeader.Number.ColumnName, clsOasys3DB.eOrderByType.Descending)
        lstHeader = BODRLHeader.LoadMatches()

        ssGrid1_Sheet1.RowCount = 0
        ssGrid1_Sheet1.AddRows(0, 1)
        For Each item As BOPurchases.cDrlHeader In lstHeader

            ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 0, item.Number.Value)
            ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 1, item.AssignedDate.Value.ToShortDateString)
            ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 2, item.ISTStoreNumber.Value)
            ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 3, item.Comment.Value)
            If item.ISTOutputReq.Value = True Then
                ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 4, "Yes")
            Else
                ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 4, "No")
            End If

            ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 5, item.Value.Value)

            ssGrid1_Sheet1.AddRows(ssGrid1.ActiveSheet.RowCount, 1)
            ssGrid1_Sheet1.SetActiveCell(ssGrid1.ActiveSheet.RowCount - 1, 0)
        Next
        ssGrid1_Sheet1.RemoveRows(ssGrid1.ActiveSheet.RowCount - 1, 1)
        ssGrid1.Focus()

        ssGrid1_Sheet1.ActiveRowIndex = 0
        ssGrid1_Sheet1.SetActiveCell(0, 0)
        SendKeys.Send("{END}")
        SendKeys.Send("{HOME}")
        btnPrint.Visible = False

    End Sub

    Private Sub ssgrid1_SelectionChanged(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles ssGrid1.SelectionChanged
        rowSelected = True
        btnPrint.Visible = True
    End Sub

    Private Sub printReport()

        Dim tempString As String
        Dim bostore As New BOStore.cStore(_Oasys3DB)
        Dim gnrlcell As New FarPoint.Win.Spread.CellType.GeneralCellType
        Dim textcell As New FarPoint.Win.Spread.CellType.TextCellType
        Dim numbCell As New FarPoint.Win.Spread.CellType.NumberCellType

        'place on grand totals

        getdetails(DRLno)

        If blnError = False Then

            ' place in headers 
            bostore.ClearLoadFilter()
            bostore.AddLoadFilter(clsOasys3DB.eOperator.pEquals, bostore.StoreID, drlstore)
            bostore.LoadCTSMatches()

            StoreName = _AddressLineFactory.ReturnAddress(bostore) 'bostore.AddressLine1.Value

            ssPrintGrid_Sheet1.PrintInfo.Header = "/cI.B.T. File Copy (Sending Store)   ++ Re-Print ++ "
            ssPrintGrid_Sheet1.PrintInfo.Footer = "/lPrinted: " & Now
            ssPrintGrid_Sheet1.PrintInfo.Footer &= "/cPage /p of /pc"
            ssPrintGrid_Sheet1.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            ssPrintGrid_Sheet1.PrintInfo.RepeatRowStart = 1
            ssPrintGrid_Sheet1.PrintInfo.RepeatRowEnd = 19

            ssPrintGrid_Sheet1.AddRows(0, 18)
            Dim acell As FarPoint.Win.Spread.Cell
            acell = ssPrintGrid.ActiveSheet.Cells(0, 0)
            acell.HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center

            tempString = "DRL Number : "
            ssPrintGrid_Sheet1.SetText(3, 0, tempString)

            tempString = DRLno & Space(10) & "To Store : " & drlstore & Space(2) & StoreName & Space(35 - Len(StoreName)) & "By:- " & _strUser
            ssPrintGrid_Sheet1.SetText(3, 1, tempString)
            ssPrintGrid.ActiveSheet.AddSpanCell(3, 1, 1, 5)

            tempString = "From Store : "
            ssPrintGrid_Sheet1.SetText(5, 0, tempString)

            tempString = CurStore.ToString("##0").PadLeft(3, "0"c) & Space(1) & CurStoreName
            ssPrintGrid_Sheet1.SetText(5, 1, tempString)
            ssPrintGrid.ActiveSheet.AddSpanCell(5, 1, 1, 5)

            tempString = "IBT Date : "
            ssPrintGrid_Sheet1.SetText(7, 0, tempString)

            tempString = drldate
            ssPrintGrid_Sheet1.Cells(7, 1).HorizontalAlignment = CellHorizontalAlignment.Left
            ssPrintGrid_Sheet1.SetText(7, 1, tempString)
            ssPrintGrid.ActiveSheet.AddSpanCell(7, 1, 1, 5)

            tempString = "Comment : "
            ssPrintGrid_Sheet1.SetText(9, 0, tempString)

            tempString = drlcomment
            ssPrintGrid_Sheet1.Cells(9, 1).HorizontalAlignment = CellHorizontalAlignment.Left
            textcell.Multiline = True
            textcell.WordWrap = True
            ssPrintGrid_Sheet1.Cells(9, 1).CellType = textcell
            ssPrintGrid_Sheet1.SetText(9, 1, tempString)
            ssPrintGrid.ActiveSheet.AddSpanCell(9, 1, 2, 5)

            ssPrintGrid_Sheet1.Cells(11, 1).CellType = gnrlcell
            ssPrintGrid_Sheet1.SetText(11, 0, "DRL Value :")
            ssPrintGrid_Sheet1.Cells(11, 1).HorizontalAlignment = CellHorizontalAlignment.Left
            ssPrintGrid_Sheet1.Cells(11, 1).CellType = numbCell
            ssPrintGrid_Sheet1.SetText(11, 1, CStr(totalvalue))

            tempString = "Collected by :"
            ssPrintGrid_Sheet1.SetText(13, 0, tempString)

            ssPrintGrid.ActiveSheet.Cells(13, 1, 13, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

            tempString = "(Print)" & Space(13) & " ____/____/________ (Date)"
            ssPrintGrid_Sheet1.SetText(13, 2, tempString)
            ssPrintGrid.ActiveSheet.AddSpanCell(13, 2, 1, 5)

            ssPrintGrid.ActiveSheet.Cells(15, 1, 15, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            tempString = "(Sign)"
            ssPrintGrid_Sheet1.SetText(15, 2, tempString)

            ssPrintGrid.ActiveSheet.Cells(16, 0, 16, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            ssPrintGrid_Sheet1.SetText(17, 0, "SKU Number")
            ssPrintGrid_Sheet1.SetText(17, 1, "Description")
            ssPrintGrid_Sheet1.SetText(17, 2, "Sell")
            ssPrintGrid_Sheet1.Cells(17, 2).HorizontalAlignment = CellHorizontalAlignment.Right
            ssPrintGrid_Sheet1.SetText(17, 3, "Quantity")
            ssPrintGrid_Sheet1.Cells(17, 3).HorizontalAlignment = CellHorizontalAlignment.Right
            ssPrintGrid_Sheet1.Cells(17, 4).CellType = gnrlcell
            ssPrintGrid_Sheet1.Cells(17, 4).HorizontalAlignment = CellHorizontalAlignment.Right
            ssPrintGrid_Sheet1.SetText(17, 4, "Price")
            ssPrintGrid_Sheet1.Cells(17, 5).HorizontalAlignment = CellHorizontalAlignment.Right
            ssPrintGrid_Sheet1.Cells(17, 5).CellType = gnrlcell
            ssPrintGrid_Sheet1.SetText(17, 5, "Value")
            ssPrintGrid.ActiveSheet.Cells(17, 0, 17, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

            ssPrintGrid.PrintSheet(0)

            SecondPrint()

            DisplayStatus("Report of DRL Number - " & DRLno & " has printed")
        End If

    End Sub

    Private Sub getdetails(ByVal drlno As String)

        Dim BODRLDetail As New BOPurchases.cDrlDetail(_Oasys3DB)
        Dim lstDetail As List(Of BOPurchases.cDrlDetail)
        Dim BODRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)
        Dim Stock As New BOStock.cStock(_Oasys3DB)
        Dim RecordCount As Integer = 0
        Dim qty As Integer = 0
        Dim price As Decimal = 0
        Dim value As Decimal = 0
        Dim gnrlcell As New FarPoint.Win.Spread.CellType.GeneralCellType

        blnError = False

        RecordCount = 0
        totalvalue = 0

        BODRLDetail.ClearLoadFilter()
        BODRLDetail.AddLoadFilter(clsOasys3DB.eOperator.pEquals, BODRLDetail.DrlNumber, drlno)
        lstDetail = BODRLDetail.LoadMatches()

        For Each item As BOPurchases.cDrlDetail In lstDetail

            ssPrintGrid_Sheet1.AddRows(RecordCount, 1)
            ssPrintGrid_Sheet1.SetText(RecordCount, 0, item.SkuNumber.Value)
            ssPrintGrid_Sheet1.SetText(RecordCount, 3, CStr(item.IbtInOutQty.Value))
            ssPrintGrid_Sheet1.SetText(RecordCount, 4, CStr(item.SkuPrice.Value))
            ssPrintGrid.ActiveSheet.AddSpanCell(RecordCount, 1, 1, 2)

            qty = CInt(item.IbtInOutQty.Value)
            price = item.SkuPrice.Value
            value = qty * price
            ssPrintGrid_Sheet1.SetText(RecordCount, 5, CStr(value))
            totalvalue += value

            Stock.AddLoadFilter(clsOasys3DB.eOperator.pEquals, Stock.SkuNumber, item.SkuNumber.Value)
            Stock.LoadMatches()

            ssPrintGrid_Sheet1.SetText(RecordCount, 1, Stock.Description.Value)
            RecordCount += 1
        Next item

        If RecordCount > 0 Then
            RecordCount = RecordCount - 1
            ssPrintGrid.ActiveSheet.Cells(RecordCount, 4, RecordCount, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            RecordCount += 1
            ssPrintGrid_Sheet1.AddRows(RecordCount, 1)
            ssPrintGrid_Sheet1.Cells(RecordCount, 4).CellType = gnrlcell
            ssPrintGrid_Sheet1.SetText(RecordCount, 4, "Grand Total")
            ssPrintGrid_Sheet1.SetText(RecordCount, 5, CStr(totalvalue))
            ssPrintGrid.ActiveSheet.Cells(RecordCount, 4, RecordCount, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            If Trim(ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 4)) = "Yes" Then
                BODRLHeader.DRLN0Printed(drlno)
                loaddata()
                rowSelected = False
            End If
        Else
            blnError = True
            DisplayStatus("Detail records were not found")
        End If

    End Sub

    Private Sub SecondPrint()

        Dim printset As New FarPoint.Win.Spread.PrintInfo()

        printset.Centering = FarPoint.Win.Spread.Centering.Horizontal
        printset.ShowRowHeaders = False
        printset.ShowGrid = False
        printset.ShowBorder = False

        ssPrintGrid2_Sheet1 = ssPrintGrid_Sheet1.Clone()
        ssPrintGrid2_Sheet1.PrintInfo = printset
        ssPrintGrid2_Sheet1.PrintInfo.Header = "/cI.B.T. File Copy (Receiving Store)   ++ Re-Print ++"
        ssPrintGrid2_Sheet1.PrintInfo.Margin.Left = 50
        ssPrintGrid2_Sheet1.PrintInfo.Margin.Top = 50
        ssPrintGrid2_Sheet1.PrintInfo.Footer = "/lPrinted: " & Now
        ssPrintGrid2_Sheet1.PrintInfo.Footer &= "/cPage /p of /pc"
        ssPrintGrid2_Sheet1.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        ssPrintGrid2_Sheet1.PrintInfo.RepeatRowStart = 1
        ssPrintGrid2_Sheet1.PrintInfo.RepeatRowEnd = 17

        ssPrintGrid2.Sheets(0) = ssPrintGrid2_Sheet1

        ' Print the sheet.

        ssPrintGrid2.PrintSheet(0)

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        If rowSelected = True Then

            pnlWaitMsg.Visible = True
            Me.Refresh()
            Application.DoEvents()

            ssPrintGrid_Sheet1.RowCount = 0
            DRLno = ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 0)
            drldate = ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 1)
            drlstore = ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 2)
            drlcomment = ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 3)
            drlvalue = ssGrid1_Sheet1.GetText(ssGrid1.ActiveSheet.ActiveRow.Index, 5)
            printReport()

            pnlWaitMsg.Visible = False
            Me.Refresh()
            Application.DoEvents()

        End If

    End Sub

    Private Sub SheetResize() Handles ssGrid1.Resize

        If ssGrid1.Sheets.Count = 0 Then Exit Sub
        ssGrid1.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        ssGrid1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never

        Dim sheet As FarPoint.Win.Spread.SheetView = ssGrid1.ActiveSheet
        Dim dataAreaWidth As Single = 0
        Dim dataAreaHeight As Single = 0
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim RowHeadersWidth As Single = 0
        Dim columnHeadersHeight As Single = 0
        Dim scrollbarWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollbarHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim columnsResizable As Integer = sheet.ColumnCount
        Dim columnsWidth As Single = 0

        'get rowheader width
        For Each header As FarPoint.Win.Spread.Column In sheet.RowHeader.Columns
            If header.Visible Then RowHeadersWidth += header.Width
        Next header

        'get column header height
        For Each header As FarPoint.Win.Spread.Row In sheet.ColumnHeader.Rows
            If header.Visible Then columnHeadersHeight += header.Height
        Next header

        'get area height
        dataAreaHeight = ssGrid1.Height - columnHeadersHeight - scrollbarHeight
        If ssGrid1.TabStripPolicy = FarPoint.Win.Spread.TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If ssGrid1.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then dataAreaHeight -= borderHeight
        If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then
            ssGrid1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Always
        End If

        'get area width
        'loop through columns and get number of resizable columns
        dataAreaWidth = ssGrid1.Width - RowHeadersWidth
        For Each column As FarPoint.Win.Spread.Column In sheet.Columns
            If column.Visible Then
                If column.Resizable Then
                    column.Width = column.GetPreferredWidth
                    columnsWidth += column.Width
                Else
                    columnsResizable -= 1
                    dataAreaWidth -= column.Width
                End If
            Else
                columnsResizable -= 1
            End If
        Next

        If ssGrid1.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then dataAreaWidth -= (borderWidth * 2)
        If ssGrid1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Always Then dataAreaWidth -= scrollbarWidth
        If columnsWidth > dataAreaWidth Then
            ssGrid1.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Always
        Else
            'increase width of columns
            Dim columnWidthIncrease As Integer = CInt((dataAreaWidth - columnsWidth) / columnsResizable)
            For Each column As FarPoint.Win.Spread.Column In sheet.Columns
                If column.Visible And column.Resizable Then
                    column.Width += columnWidthIncrease
                End If
            Next column
        End If

    End Sub

End Class