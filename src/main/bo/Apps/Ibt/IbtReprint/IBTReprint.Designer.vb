﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IbtReprint
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim NumberCellType1 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim CurrencyCellType1 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IbtReprint))
        Me.GBSpread = New System.Windows.Forms.GroupBox()
        Me.pnlWaitMsg = New System.Windows.Forms.Panel()
        Me.lblPleaseWait = New System.Windows.Forms.Label()
        Me.ssGrid1 = New FarPoint.Win.Spread.FpSpread()
        Me.ssGrid1_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.ssPrintGrid = New FarPoint.Win.Spread.FpSpread()
        Me.ssPrintGrid_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.ssPrintGrid2 = New FarPoint.Win.Spread.FpSpread()
        Me.ssPrintGrid2_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.GBSpread.SuspendLayout()
        Me.pnlWaitMsg.SuspendLayout()
        CType(Me.ssGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssGrid1_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid2_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GBSpread
        '
        Me.GBSpread.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBSpread.Controls.Add(Me.pnlWaitMsg)
        Me.GBSpread.Controls.Add(Me.ssGrid1)
        Me.GBSpread.Location = New System.Drawing.Point(4, 4)
        Me.GBSpread.Name = "GBSpread"
        Me.GBSpread.Size = New System.Drawing.Size(588, 440)
        Me.GBSpread.TabIndex = 0
        Me.GBSpread.TabStop = False
        Me.GBSpread.Text = "Select IBT out to print"
        '
        'pnlWaitMsg
        '
        Me.pnlWaitMsg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlWaitMsg.BackColor = System.Drawing.Color.White
        Me.pnlWaitMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWaitMsg.Controls.Add(Me.lblPleaseWait)
        Me.pnlWaitMsg.Location = New System.Drawing.Point(180, 10)
        Me.pnlWaitMsg.Name = "pnlWaitMsg"
        Me.pnlWaitMsg.Size = New System.Drawing.Size(228, 68)
        Me.pnlWaitMsg.TabIndex = 14
        Me.pnlWaitMsg.UseWaitCursor = True
        Me.pnlWaitMsg.Visible = False
        '
        'lblPleaseWait
        '
        Me.lblPleaseWait.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPleaseWait.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPleaseWait.Location = New System.Drawing.Point(15, 22)
        Me.lblPleaseWait.Name = "lblPleaseWait"
        Me.lblPleaseWait.Size = New System.Drawing.Size(194, 24)
        Me.lblPleaseWait.TabIndex = 0
        Me.lblPleaseWait.Text = "Please Wait ..."
        Me.lblPleaseWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPleaseWait.UseWaitCursor = True
        '
        'ssGrid1
        '
        Me.ssGrid1.About = "3.0.2004.2005"
        Me.ssGrid1.AccessibleDescription = "ssGrid1, Sheet1, Row 0, Column 0, "
        Me.ssGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ssGrid1.BackColor = System.Drawing.SystemColors.Control
        Me.ssGrid1.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.ssGrid1.Location = New System.Drawing.Point(9, 20)
        Me.ssGrid1.Name = "ssGrid1"
        Me.ssGrid1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssGrid1.SelectionBlockOptions = FarPoint.Win.Spread.SelectionBlockOptions.Rows
        Me.ssGrid1.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssGrid1_Sheet1})
        Me.ssGrid1.Size = New System.Drawing.Size(569, 414)
        Me.ssGrid1.TabIndex = 0
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssGrid1.TextTipAppearance = TipAppearance1
        Me.ssGrid1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'ssGrid1_Sheet1
        '
        Me.ssGrid1_Sheet1.Reset()
        Me.ssGrid1_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssGrid1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssGrid1_Sheet1.ColumnCount = 6
        Me.ssGrid1_Sheet1.RowCount = 100
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "DRL. No"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Date"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Store"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Comment"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "Needs to be Printed"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "totalvalue"
        Me.ssGrid1_Sheet1.Columns.Get(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.ssGrid1_Sheet1.Columns.Get(0).Label = "DRL. No"
        Me.ssGrid1_Sheet1.Columns.Get(0).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(0).Resizable = False
        Me.ssGrid1_Sheet1.Columns.Get(0).Width = 131.0!
        Me.ssGrid1_Sheet1.Columns.Get(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.ssGrid1_Sheet1.Columns.Get(1).Label = "Date"
        Me.ssGrid1_Sheet1.Columns.Get(1).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(1).Resizable = False
        Me.ssGrid1_Sheet1.Columns.Get(1).Width = 140.0!
        Me.ssGrid1_Sheet1.Columns.Get(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.ssGrid1_Sheet1.Columns.Get(2).Label = "Store"
        Me.ssGrid1_Sheet1.Columns.Get(2).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(2).Resizable = False
        Me.ssGrid1_Sheet1.Columns.Get(2).Width = 88.0!
        Me.ssGrid1_Sheet1.Columns.Get(3).Label = "Comment"
        Me.ssGrid1_Sheet1.Columns.Get(3).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(3).Width = 200.0!
        Me.ssGrid1_Sheet1.Columns.Get(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Me.ssGrid1_Sheet1.Columns.Get(4).Label = "Needs to be Printed"
        Me.ssGrid1_Sheet1.Columns.Get(4).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(4).Resizable = False
        Me.ssGrid1_Sheet1.Columns.Get(4).Width = 106.0!
        Me.ssGrid1_Sheet1.Columns.Get(5).Label = "totalvalue"
        Me.ssGrid1_Sheet1.Columns.Get(5).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(5).Visible = False
        Me.ssGrid1_Sheet1.RestrictRows = True
        Me.ssGrid1_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssGrid1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'BtnExit
        '
        Me.BtnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnExit.Location = New System.Drawing.Point(3, 450)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(75, 40)
        Me.BtnExit.TabIndex = 1
        Me.BtnExit.Text = "F10 Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(84, 450)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 40)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "F3 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'ssPrintGrid
        '
        Me.ssPrintGrid.About = "3.0.2004.2005"
        Me.ssPrintGrid.AccessibleDescription = "ssPrintGrid, Sheet1, Row 0, Column 0, "
        Me.ssPrintGrid.BackColor = System.Drawing.SystemColors.Control
        Me.ssPrintGrid.Location = New System.Drawing.Point(13, 476)
        Me.ssPrintGrid.Name = "ssPrintGrid"
        Me.ssPrintGrid.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssPrintGrid.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssPrintGrid_Sheet1})
        Me.ssPrintGrid.Size = New System.Drawing.Size(200, 100)
        Me.ssPrintGrid.TabIndex = 3
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssPrintGrid.TextTipAppearance = TipAppearance2
        Me.ssPrintGrid.Visible = False
        '
        'ssPrintGrid_Sheet1
        '
        Me.ssPrintGrid_Sheet1.Reset()
        Me.ssPrintGrid_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssPrintGrid_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssPrintGrid_Sheet1.ColumnCount = 7
        Me.ssPrintGrid_Sheet1.ColumnHeader.RowCount = 0
        Me.ssPrintGrid_Sheet1.RowCount = 8
        Me.ssPrintGrid_Sheet1.RowHeader.ColumnCount = 0
        Me.ssPrintGrid_Sheet1.Columns.Get(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.ssPrintGrid_Sheet1.Columns.Get(0).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(0).Width = 100.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(1).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(1).Width = 200.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(2).Width = 140.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(3).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(4).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(4).Width = 67.0!
        NumberCellType1.DecimalPlaces = 2
        Me.ssPrintGrid_Sheet1.Columns.Get(5).CellType = NumberCellType1
        Me.ssPrintGrid_Sheet1.Columns.Get(5).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(5).Width = 75.0!
        CurrencyCellType1.DecimalPlaces = 2
        Me.ssPrintGrid_Sheet1.Columns.Get(6).CellType = CurrencyCellType1
        Me.ssPrintGrid_Sheet1.Columns.Get(6).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(6).Width = 80.0!
        Me.ssPrintGrid_Sheet1.HorizontalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssPrintGrid_Sheet1.PrintInfo.Margin.Left = 50
        Me.ssPrintGrid_Sheet1.PrintInfo.Margin.Top = 50
        Me.ssPrintGrid_Sheet1.PrintInfo.ShowBorder = False
        Me.ssPrintGrid_Sheet1.PrintInfo.ShowGrid = False
        Me.ssPrintGrid_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssPrintGrid_Sheet1.RowHeader.Visible = False
        Me.ssPrintGrid_Sheet1.VerticalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssPrintGrid_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'ssPrintGrid2
        '
        Me.ssPrintGrid2.About = "3.0.2004.2005"
        Me.ssPrintGrid2.AccessibleDescription = "ssPrintGrid2, Sheet1, Row 0, Column 0, "
        Me.ssPrintGrid2.BackColor = System.Drawing.SystemColors.Control
        Me.ssPrintGrid2.Location = New System.Drawing.Point(341, 476)
        Me.ssPrintGrid2.Name = "ssPrintGrid2"
        Me.ssPrintGrid2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssPrintGrid2.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssPrintGrid2_Sheet1})
        Me.ssPrintGrid2.Size = New System.Drawing.Size(200, 100)
        Me.ssPrintGrid2.TabIndex = 4
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssPrintGrid2.TextTipAppearance = TipAppearance3
        Me.ssPrintGrid2.Visible = False
        '
        'ssPrintGrid2_Sheet1
        '
        Me.ssPrintGrid2_Sheet1.Reset()
        Me.ssPrintGrid2_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssPrintGrid2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssPrintGrid2_Sheet1.PrintInfo.Margin.Left = 50
        Me.ssPrintGrid2_Sheet1.PrintInfo.Margin.Top = 50
        Me.ssPrintGrid2_Sheet1.PrintInfo.ShowBorder = False
        Me.ssPrintGrid2_Sheet1.PrintInfo.ShowGrid = False
        Me.ssPrintGrid2_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssPrintGrid2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(165, 450)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 40)
        Me.btnPrint.TabIndex = 5
        Me.btnPrint.Text = "F4 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'IbtReprint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.ssPrintGrid2)
        Me.Controls.Add(Me.ssPrintGrid)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.GBSpread)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "IbtReprint"
        Me.Size = New System.Drawing.Size(604, 493)
        Me.GBSpread.ResumeLayout(False)
        Me.pnlWaitMsg.ResumeLayout(False)
        CType(Me.ssGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssGrid1_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid2_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GBSpread As System.Windows.Forms.GroupBox
    Friend WithEvents BtnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents FpSpread1 As FarPoint.Win.Spread.FpSpread
    Friend WithEvents FpSpread1_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents ssGrid1 As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssGrid1_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents ssPrintGrid As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssPrintGrid_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents ssPrintGrid2 As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssPrintGrid2_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents pnlWaitMsg As System.Windows.Forms.Panel
    Friend WithEvents lblPleaseWait As System.Windows.Forms.Label
End Class
