﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IbtIn
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IbtIn))
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim TipAppearance4 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim NumberCellType5 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType6 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType7 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType8 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim CurrencyCellType2 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Me.ssConsignment = New FarPoint.Win.Spread.FpSpread()
        Me.ssConsignment_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.ssGrid2 = New FarPoint.Win.Spread.FpSpread()
        Me.ssGrid2_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblSelection = New System.Windows.Forms.Label()
        Me.lblConsignmentNo = New System.Windows.Forms.Label()
        Me.lblSelectedStore = New System.Windows.Forms.Label()
        Me.txtSelectedStore = New System.Windows.Forms.Label()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.TxtComment = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pnlWaitMsg = New System.Windows.Forms.Panel()
        Me.lblPleaseWait = New System.Windows.Forms.Label()
        Me.lblIBTtotal = New System.Windows.Forms.Label()
        Me.txtIBTtotal = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnStockEnquiry = New System.Windows.Forms.Button()
        Me.pbSaveProgress = New System.Windows.Forms.ProgressBar()
        Me.btnDelCons = New System.Windows.Forms.Button()
        CType(Me.ssConsignment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssConsignment_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssGrid2_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.pnlWaitMsg.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ssConsignment
        '
        Me.ssConsignment.About = "3.0.2004.2005"
        Me.ssConsignment.AccessibleDescription = "ssConsignment, Sheet1, Row 0, Column 0, "
        Me.ssConsignment.BackColor = System.Drawing.SystemColors.Control
        Me.ssConsignment.FocusRenderer = New FarPoint.Win.Spread.CustomFocusIndicatorRenderer(CType(resources.GetObject("ssConsignment.FocusRenderer"), System.Drawing.Bitmap), 1)
        Me.ssConsignment.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        Me.ssConsignment.Location = New System.Drawing.Point(6, 18)
        Me.ssConsignment.Name = "ssConsignment"
        Me.ssConsignment.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssConsignment.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssConsignment_Sheet1})
        Me.ssConsignment.Size = New System.Drawing.Size(436, 167)
        Me.ssConsignment.TabIndex = 0
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssConsignment.TextTipAppearance = TipAppearance3
        Me.ssConsignment.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'ssConsignment_Sheet1
        '
        Me.ssConsignment_Sheet1.Reset()
        Me.ssConsignment_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssConsignment_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssConsignment_Sheet1.ColumnCount = 4
        Me.ssConsignment_Sheet1.RowCount = 1
        Me.ssConsignment_Sheet1.RowHeader.ColumnCount = 0
        Me.ssConsignment_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "Consignment No"
        Me.ssConsignment_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "DRL No"
        Me.ssConsignment_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Store"
        Me.ssConsignment_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Store Name"
        Me.ssConsignment_Sheet1.Columns.Get(0).AllowAutoSort = True
        Me.ssConsignment_Sheet1.Columns.Get(0).Label = "Consignment No"
        Me.ssConsignment_Sheet1.Columns.Get(0).Locked = True
        Me.ssConsignment_Sheet1.Columns.Get(0).ShowSortIndicator = False
        Me.ssConsignment_Sheet1.Columns.Get(0).Width = 110.0!
        Me.ssConsignment_Sheet1.Columns.Get(1).AllowAutoSort = True
        Me.ssConsignment_Sheet1.Columns.Get(1).Label = "DRL No"
        Me.ssConsignment_Sheet1.Columns.Get(1).Locked = True
        Me.ssConsignment_Sheet1.Columns.Get(1).ShowSortIndicator = False
        Me.ssConsignment_Sheet1.Columns.Get(1).Width = 70.0!
        Me.ssConsignment_Sheet1.Columns.Get(2).AllowAutoSort = True
        Me.ssConsignment_Sheet1.Columns.Get(2).Label = "Store"
        Me.ssConsignment_Sheet1.Columns.Get(2).ShowSortIndicator = False
        Me.ssConsignment_Sheet1.Columns.Get(2).Width = 50.0!
        Me.ssConsignment_Sheet1.Columns.Get(3).AllowAutoSort = True
        Me.ssConsignment_Sheet1.Columns.Get(3).Label = "Store Name"
        Me.ssConsignment_Sheet1.Columns.Get(3).Locked = True
        Me.ssConsignment_Sheet1.Columns.Get(3).ShowSortIndicator = False
        Me.ssConsignment_Sheet1.Columns.Get(3).Width = 190.0!
        Me.ssConsignment_Sheet1.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect
        Me.ssConsignment_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssConsignment_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.ssConsignment_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.ssConsignment_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'ssGrid2
        '
        Me.ssGrid2.About = "3.0.2004.2005"
        Me.ssGrid2.AccessibleDescription = "ssGrid2, Sheet1, Row 0, Column 0, "
        Me.ssGrid2.AllowUndo = False
        Me.ssGrid2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ssGrid2.BackColor = System.Drawing.SystemColors.Control
        Me.ssGrid2.EditModeReplace = True
        Me.ssGrid2.FocusRenderer = New FarPoint.Win.Spread.CustomFocusIndicatorRenderer(CType(resources.GetObject("ssGrid2.FocusRenderer"), System.Drawing.Bitmap), 1)
        Me.ssGrid2.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        Me.ssGrid2.Location = New System.Drawing.Point(6, 19)
        Me.ssGrid2.Name = "ssGrid2"
        Me.ssGrid2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssGrid2.SelectionBlockOptions = CType((FarPoint.Win.Spread.SelectionBlockOptions.Cells Or FarPoint.Win.Spread.SelectionBlockOptions.Rows), FarPoint.Win.Spread.SelectionBlockOptions)
        Me.ssGrid2.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssGrid2_Sheet1})
        Me.ssGrid2.Size = New System.Drawing.Size(729, 214)
        Me.ssGrid2.TabIndex = 9
        TipAppearance4.BackColor = System.Drawing.SystemColors.Info
        TipAppearance4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance4.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssGrid2.TextTipAppearance = TipAppearance4
        Me.ssGrid2.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'ssGrid2_Sheet1
        '
        Me.ssGrid2_Sheet1.Reset()
        Me.ssGrid2_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssGrid2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssGrid2_Sheet1.ColumnCount = 8
        Me.ssGrid2_Sheet1.RowCount = 1
        Me.ssGrid2_Sheet1.ColumnHeader.AutoText = FarPoint.Win.Spread.HeaderAutoText.Numbers
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "SKU Number"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Quantity"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Space"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "EditCode"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "Description"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "Price"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Value"
        Me.ssGrid2_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "value"
        Me.ssGrid2_Sheet1.ColumnHeader.Columns.Default.Resizable = False
        Me.ssGrid2_Sheet1.Columns.Default.Resizable = False
        NumberCellType5.DecimalPlaces = 0
        NumberCellType5.MaximumValue = 999999.0R
        NumberCellType5.MinimumValue = 0.0R
        Me.ssGrid2_Sheet1.Columns.Get(0).CellType = NumberCellType5
        Me.ssGrid2_Sheet1.Columns.Get(0).Label = "SKU Number"
        Me.ssGrid2_Sheet1.Columns.Get(0).Width = 90.0!
        NumberCellType6.DecimalPlaces = 0
        NumberCellType6.MaximumValue = 9999.0R
        NumberCellType6.MinimumValue = -9999.0R
        Me.ssGrid2_Sheet1.Columns.Get(1).CellType = NumberCellType6
        Me.ssGrid2_Sheet1.Columns.Get(1).Label = "Quantity"
        Me.ssGrid2_Sheet1.Columns.Get(1).Width = 56.0!
        Me.ssGrid2_Sheet1.Columns.Get(2).Label = "Space"
        Me.ssGrid2_Sheet1.Columns.Get(2).Locked = False
        Me.ssGrid2_Sheet1.Columns.Get(2).Visible = False
        Me.ssGrid2_Sheet1.Columns.Get(2).Width = 14.0!
        Me.ssGrid2_Sheet1.Columns.Get(3).Label = "EditCode"
        Me.ssGrid2_Sheet1.Columns.Get(3).Locked = True
        Me.ssGrid2_Sheet1.Columns.Get(3).Visible = False
        Me.ssGrid2_Sheet1.Columns.Get(4).Label = "Description"
        Me.ssGrid2_Sheet1.Columns.Get(4).Locked = True
        Me.ssGrid2_Sheet1.Columns.Get(4).Width = 300.0!
        Me.ssGrid2_Sheet1.Columns.Get(5).CellType = NumberCellType7
        Me.ssGrid2_Sheet1.Columns.Get(5).Label = "Price"
        Me.ssGrid2_Sheet1.Columns.Get(5).Locked = True
        Me.ssGrid2_Sheet1.Columns.Get(5).Width = 80.0!
        Me.ssGrid2_Sheet1.Columns.Get(6).CellType = NumberCellType8
        Me.ssGrid2_Sheet1.Columns.Get(6).Label = "Value"
        Me.ssGrid2_Sheet1.Columns.Get(6).Locked = True
        Me.ssGrid2_Sheet1.Columns.Get(6).Width = 80.0!
        CurrencyCellType2.DecimalPlaces = 2
        Me.ssGrid2_Sheet1.Columns.Get(7).CellType = CurrencyCellType2
        Me.ssGrid2_Sheet1.Columns.Get(7).Label = "value"
        Me.ssGrid2_Sheet1.Columns.Get(7).Locked = True
        Me.ssGrid2_Sheet1.Columns.Get(7).Visible = False
        Me.ssGrid2_Sheet1.RowHeader.Columns.Default.Resizable = True
        Me.ssGrid2_Sheet1.RowHeader.Columns.Default.VisualStyles = FarPoint.Win.VisualStyles.Off
        Me.ssGrid2_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault"
        Me.ssGrid2_Sheet1.RowHeader.DefaultStyle.VisualStyles = FarPoint.Win.VisualStyles.Off
        Me.ssGrid2_Sheet1.RowHeader.Rows.Default.Resizable = False
        Me.ssGrid2_Sheet1.RowHeader.Rows.Default.VisualStyles = FarPoint.Win.VisualStyles.Off
        Me.ssGrid2_Sheet1.Rows.Default.Resizable = False
        Me.ssGrid2_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.ssGrid2_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.ssGrid2_Sheet1.StartingRowNumber = 0
        Me.ssGrid2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(9, 439)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 39)
        Me.btnSave.TabIndex = 12
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblSelection
        '
        Me.lblSelection.Location = New System.Drawing.Point(448, 18)
        Me.lblSelection.Name = "lblSelection"
        Me.lblSelection.Size = New System.Drawing.Size(87, 18)
        Me.lblSelection.TabIndex = 3
        Me.lblSelection.Text = "Consignment"
        Me.lblSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSelection.Visible = False
        '
        'lblConsignmentNo
        '
        Me.lblConsignmentNo.Location = New System.Drawing.Point(541, 18)
        Me.lblConsignmentNo.Name = "lblConsignmentNo"
        Me.lblConsignmentNo.Size = New System.Drawing.Size(130, 22)
        Me.lblConsignmentNo.TabIndex = 4
        '
        'lblSelectedStore
        '
        Me.lblSelectedStore.Location = New System.Drawing.Point(448, 46)
        Me.lblSelectedStore.Name = "lblSelectedStore"
        Me.lblSelectedStore.Size = New System.Drawing.Size(86, 19)
        Me.lblSelectedStore.TabIndex = 5
        Me.lblSelectedStore.Text = "Selected Store"
        Me.lblSelectedStore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSelectedStore.Visible = False
        '
        'txtSelectedStore
        '
        Me.txtSelectedStore.Location = New System.Drawing.Point(541, 46)
        Me.txtSelectedStore.Name = "txtSelectedStore"
        Me.txtSelectedStore.Size = New System.Drawing.Size(127, 23)
        Me.txtSelectedStore.TabIndex = 6
        '
        'lblComment
        '
        Me.lblComment.Location = New System.Drawing.Point(449, 74)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(59, 21)
        Me.lblComment.TabIndex = 7
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxtComment
        '
        Me.TxtComment.Location = New System.Drawing.Point(451, 98)
        Me.TxtComment.MaxLength = 75
        Me.TxtComment.Name = "TxtComment"
        Me.TxtComment.Size = New System.Drawing.Size(284, 20)
        Me.TxtComment.TabIndex = 8
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(668, 439)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(586, 439)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 11
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.pnlWaitMsg)
        Me.GroupBox1.Controls.Add(Me.lblIBTtotal)
        Me.GroupBox1.Controls.Add(Me.txtIBTtotal)
        Me.GroupBox1.Controls.Add(Me.ssConsignment)
        Me.GroupBox1.Controls.Add(Me.lblSelection)
        Me.GroupBox1.Controls.Add(Me.lblConsignmentNo)
        Me.GroupBox1.Controls.Add(Me.TxtComment)
        Me.GroupBox1.Controls.Add(Me.lblSelectedStore)
        Me.GroupBox1.Controls.Add(Me.lblComment)
        Me.GroupBox1.Controls.Add(Me.txtSelectedStore)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(741, 185)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Consignment"
        '
        'pnlWaitMsg
        '
        Me.pnlWaitMsg.BackColor = System.Drawing.Color.White
        Me.pnlWaitMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWaitMsg.Controls.Add(Me.lblPleaseWait)
        Me.pnlWaitMsg.Location = New System.Drawing.Point(210, 19)
        Me.pnlWaitMsg.Name = "pnlWaitMsg"
        Me.pnlWaitMsg.Size = New System.Drawing.Size(320, 68)
        Me.pnlWaitMsg.TabIndex = 2
        Me.pnlWaitMsg.Visible = False
        '
        'lblPleaseWait
        '
        Me.lblPleaseWait.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPleaseWait.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPleaseWait.Location = New System.Drawing.Point(15, 19)
        Me.lblPleaseWait.Name = "lblPleaseWait"
        Me.lblPleaseWait.Size = New System.Drawing.Size(286, 24)
        Me.lblPleaseWait.TabIndex = 0
        Me.lblPleaseWait.Text = "Please Wait ..."
        Me.lblPleaseWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblIBTtotal
        '
        Me.lblIBTtotal.AutoSize = True
        Me.lblIBTtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIBTtotal.Location = New System.Drawing.Point(449, 140)
        Me.lblIBTtotal.Name = "lblIBTtotal"
        Me.lblIBTtotal.Size = New System.Drawing.Size(60, 13)
        Me.lblIBTtotal.TabIndex = 11
        Me.lblIBTtotal.Text = "IBT Total"
        '
        'txtIBTtotal
        '
        Me.txtIBTtotal.AutoSize = True
        Me.txtIBTtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIBTtotal.Location = New System.Drawing.Point(612, 140)
        Me.txtIBTtotal.Name = "txtIBTtotal"
        Me.txtIBTtotal.Size = New System.Drawing.Size(0, 13)
        Me.txtIBTtotal.TabIndex = 10
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.ssGrid2)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 194)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(741, 239)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Enter Items"
        Me.GroupBox2.Visible = False
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Location = New System.Drawing.Point(91, 439)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(94, 39)
        Me.btnStockEnquiry.TabIndex = 13
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        Me.btnStockEnquiry.UseVisualStyleBackColor = True
        Me.btnStockEnquiry.Visible = False
        '
        'pbSaveProgress
        '
        Me.pbSaveProgress.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbSaveProgress.Location = New System.Drawing.Point(223, 447)
        Me.pbSaveProgress.Name = "pbSaveProgress"
        Me.pbSaveProgress.Size = New System.Drawing.Size(223, 23)
        Me.pbSaveProgress.TabIndex = 14
        '
        'btnDelCons
        '
        Me.btnDelCons.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelCons.Location = New System.Drawing.Point(486, 439)
        Me.btnDelCons.Name = "btnDelCons"
        Me.btnDelCons.Size = New System.Drawing.Size(94, 39)
        Me.btnDelCons.TabIndex = 15
        Me.btnDelCons.Text = "F6 Delete Consignment"
        Me.btnDelCons.UseVisualStyleBackColor = True
        '
        'IbtIn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnDelCons)
        Me.Controls.Add(Me.pbSaveProgress)
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "IbtIn"
        Me.Size = New System.Drawing.Size(747, 481)
        CType(Me.ssConsignment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssConsignment_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssGrid2_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlWaitMsg.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ssConsignment As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssConsignment_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents ssGrid2 As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssGrid2_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblSelection As System.Windows.Forms.Label
    Friend WithEvents lblConsignmentNo As System.Windows.Forms.Label
    Friend WithEvents lblSelectedStore As System.Windows.Forms.Label
    Friend WithEvents txtSelectedStore As System.Windows.Forms.Label
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents TxtComment As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnStockEnquiry As System.Windows.Forms.Button
    Friend WithEvents txtIBTtotal As System.Windows.Forms.Label
    Friend WithEvents pbSaveProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents pnlWaitMsg As System.Windows.Forms.Panel
    Friend WithEvents lblPleaseWait As System.Windows.Forms.Label
    Friend WithEvents lblIBTtotal As System.Windows.Forms.Label
    Friend WithEvents btnDelCons As System.Windows.Forms.Button

End Class
