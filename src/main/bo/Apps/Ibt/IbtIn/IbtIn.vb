﻿Imports OasysDBBO.Oasys3.DB
Imports System.Data
Imports Cts.Oasys.Core

Public Class IbtIn

    Const EDIT_NONE = 0
    Const EDIT_NEW = 1
    Const EDIT_EDIT = 2
    Const EDIT_DEL = 3

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private WithEvents _DRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)
    Private validStore As New Dictionary(Of String, String)
    Private invalidStore As New Dictionary(Of String, String)
    Private _blnLeaveSSConsignment As Boolean = False

    Private selectedrow As Integer = 0
    Private selectedcol As Integer = 0
    Private selected2row As Integer = 0
    Private selected2col As Integer = 0
    Private grid2selectedrow As Integer = 0
    Private sequenceno As Integer = 0
    Private totalvalue As Decimal = 0
    Private selectedNumb As Integer = 0
    Private validationError As Boolean = False
    Private rowcreated As Boolean = False
    Private rowheaderclick As Boolean = False
    Private rowSelected As Boolean = False
    Private _blnWarningMessages As Boolean = False
    Private _blnRowSelected As Boolean = False
    Private _blnLeaveCell As Boolean = False
    Private _blnEditModeOff As Boolean = False
    Private _blnConsignmentCheck As Boolean = False

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub DoProcessing()

        Using BoSystemDate As New BOSystem.cSystemDates(_Oasys3DB)
            BoSystemDate.LoadMatches()
            If BoSystemDate.NightSetNo.Value < "400" And BoSystemDate.NightSetNo.Value <> "000" Then
                MsgBox("Night Incomplete - Please Check", MsgBoxStyle.OkOnly, "IBT In")
                FindForm.Close()
            Else
                BuildLists()
                loadConsigments()
            End If
        End Using

    End Sub

    Protected Overrides Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        If lblSelection.Visible = True Then
            If MessageBox.Show("Do you wish to exit?", Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
                e.Cancel = True
            End If
        Else
            MyBase.Form_Closing(sender, e)
        End If
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnStockEnquiry.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F6 : btnDelCons.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Keys.Delete : ssgrid2_deleterow()
            Case Else : e.Handled = False : MyBase.Form_KeyDown(sender, e)
        End Select
    End Sub

    Private Sub IBTIn_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ssGrid2.ActiveSheet.Columns(6).CellType = New FarPoint.Win.Spread.CellType.NumberCellType
        ssGrid2.ActiveSheet.Columns(6).Visible = True
    End Sub


    Private Sub loadConsigments()

        Dim boCon As New BOPurchases.cConsignMaster(_Oasys3DB)
        Dim lstCon As List(Of BOPurchases.cConsignMaster)
        Dim CurRow As Integer
        Dim Description As String

        'key mappings
        Dim AncestorFocusedMap As InputMap = ssConsignment.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap As InputMap = ssConsignment.GetInputMap(InputMapMode.WhenFocused)

        ssConsignment.Enabled = True
        AncestorFocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)

        Dim AncestorFocusedMap2 As InputMap = ssGrid2.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap2 As InputMap = ssGrid2.GetInputMap(InputMapMode.WhenFocused)

        AncestorFocusedMap2.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap2.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap2.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap2.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap2.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)
        FocusedMap2.Put(New Keystroke(Keys.Control, Keys.None), SpreadActions.StopEditing)

        TxtComment.Text = String.Empty
        txtSelectedStore.Text = String.Empty
        lblConsignmentNo.Text = String.Empty

        'Switch Off save button
        btnSave.Visible = False

        txtIBTtotal.Text = String.Empty
        lblSelection.Visible = False
        lblSelectedStore.Visible = False
        GroupBox2.Visible = False
        btnStockEnquiry.Visible = False
        rowSelected = False

        boCon.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boCon.IBTIndicator, True)
        boCon.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
        boCon.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boCon.Done, False)
        boCon.SortBy(boCon.PONumber.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        lstCon = boCon.LoadMatches()

        If lstCon.Count > 0 Then

            ssConsignment_Sheet1.RowCount = 0

            For Each row As BOPurchases.cConsignMaster In lstCon

                CurRow = ssConsignment_Sheet1.RowCount

                Description = IsvalidStore(row.IBTStoreNumber.Value)
                If Description > String.Empty Then
                    ssConsignment_Sheet1.AddRows(CurRow, 1)
                    ssConsignment_Sheet1.SetValue(CurRow, 0, row.Number.Value)
                    ssConsignment_Sheet1.SetValue(CurRow, 1, row.PONumber.Value)
                    ssConsignment_Sheet1.SetValue(CurRow, 2, row.IBTStoreNumber.Value)
                    ssConsignment_Sheet1.SetValue(CurRow, 3, Description.Trim)
                End If
            Next row

            ssConsignment_Sheet1.SetActiveCell(0, 0)

            ssGrid2_Sheet1.RowCount = 0
            ssGrid2_Sheet1.AddRows(0, 1)
            ssGrid2_Sheet1.SetText(0, 3, CStr(EDIT_NONE))
            ssGrid2_Sheet1.SetFormula(0, 5, "SUM(G2:G2)")
            ssGrid2.ActiveSheet.SetRowVisible(0, False)
            ssGrid2_Sheet1.AddRows(1, 1)
            ssGrid2_Sheet1.SetValue(1, 3, EDIT_NONE)
            ssGrid2_Sheet1.SetActiveCell(1, 0)

        Else

            MessageBox.Show("No Available Consignments To Select", "IBTIn", MessageBoxButtons.OK)
            btnExit.PerformClick()
            Exit Sub

        End If

        _blnLeaveSSConsignment = True
        DisplayStatus()
        ssConsignment.Focus()
        ssConsignment_Sheet1.AddSelection(0, 0, 1, 5)
        rowSelected = True
        _blnLeaveSSConsignment = False
        pbSaveProgress.Visible = False
        _blnWarningMessages = True

    End Sub

    Private Sub ssConsignment_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles ssConsignment.CellClick
        selectedrow = e.Row
        selectedcol = e.Column
    End Sub

    Private Sub ssConsignment_celldoubleclick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles ssConsignment.CellDoubleClick
        If e.ColumnHeader = False Then
            _blnRowSelected = True
            DisplaySelectedItem()
        End If
    End Sub

    Private Sub ssConsignment_keypress(ByVal sender As System.Object, ByVal e As Windows.Forms.KeyPressEventArgs) Handles ssConsignment.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then DisplaySelectedItem()
    End Sub

    Private Sub DisplaySelectedItem()

        DisplayStatus()

        If rowSelected = True Then
            lblConsignmentNo.Text = ssConsignment_Sheet1.GetText(ssConsignment_Sheet1.ActiveRow.Index, 0)
            txtSelectedStore.Text = ssConsignment_Sheet1.GetText(ssConsignment_Sheet1.ActiveRow.Index, 2)
            selectedNumb = CInt(ssConsignment_Sheet1.GetText(ssConsignment_Sheet1.ActiveRow.Index, 1))
            lblSelection.Visible = True
            lblSelectedStore.Visible = True
            _blnLeaveSSConsignment = True
            TxtComment.Focus()
        End If

    End Sub

    Private Sub ssConsignment_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles ssConsignment.SelectionChanged
        If e.Range IsNot Nothing Then selectedrow = e.Range.Row
        rowSelected = True
    End Sub

    Private Sub Save(ByVal partno As String, ByVal qty As Integer)

        Using Stock As New BOStock.cStock(_Oasys3DB)
            Stock.AddLoadFilter(clsOasys3DB.eOperator.pEquals, Stock.SkuNumber, partno)
            Stock.LoadMatches()
            Dim spric As Decimal = Stock.NormalSellPrice.Value

            sequenceno += 1
            totalvalue += (Stock.NormalSellPrice.Value * qty)

            'automatically add detail to collection if not already existing
            _DRLHeader.addItemToCollection(partno, sequenceno.ToString("###0").PadLeft(4, "0"c), qty * -1, spric)
        End Using

    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(UserId, WorkstationId, SecurityLevel, String.Empty)

            lookup.ShowAccept()

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Dim boPart As New BOStock.cStock(_Oasys3DB)

                    ssGrid2_Sheet1.SetText(ssGrid2_Sheet1.ActiveRow.Index, 0, CStr(lookup.SkuNumbers(0)))
                    ssGrid2_Sheet1.SetText(ssGrid2_Sheet1.ActiveRow.Index, 3, CStr(EDIT_NEW))
                    boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, ssGrid2_Sheet1.GetText(ssGrid2_Sheet1.ActiveRow.Index, 0))

                    If boPart.LoadMatches.Count > 0 Then
                        ssGrid2_Sheet1.SetValue(ssGrid2_Sheet1.ActiveRow.Index, 4, boPart.Description.Value)
                        ssGrid2_Sheet1.SetValue(ssGrid2_Sheet1.ActiveRow.Index, 5, boPart.NormalSellPrice.Value)
                        ssGrid2_Sheet1.SetActiveCell(ssGrid2_Sheet1.ActiveRow.Index, 1)
                    End If

                End If
            End Using

        End Using
        ssGrid2.Focus()

    End Sub

    Private Sub btnDelCons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelCons.Click
        Dim boCon As New BOPurchases.cConsignMaster(_Oasys3DB)
        Dim Result As DialogResult
        Dim strConNumber As String = ssConsignment_Sheet1.GetText(ssConsignment_Sheet1.ActiveRow.Index, 0)

        Result = MessageBox.Show("Are you sure you want to delete the consignment " & strConNumber & " ?", "IBT In", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If Result = DialogResult.Yes Then
            _Oasys3DB.ExecuteSql("Update CONMAS SET DONE=1 WHERE NUMB = " & strConNumber.ToString)
            loadConsigments()
        End If 'Result = DialogResult.OK
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim lngValue As Long
        Dim validationError As Boolean = False
        Dim returnError As Boolean
        Dim number As Integer

        Me.Cursor = Cursors.WaitCursor
        pnlWaitMsg.Visible = True

        Me.Refresh()
        Application.DoEvents()

        _blnWarningMessages = False
        For value As Integer = 0 To ssGrid2_Sheet1.Rows.Count - 1
            lngValue = CInt(ssGrid2_Sheet1.GetText(value, 3))
            If (lngValue <> EDIT_NONE) Then
                Select Case (lngValue)
                    Case (EDIT_NEW)
                        returnError = validatePartNo(value)
                        If returnError = True Then
                            validationError = True
                            Exit For
                        End If
                        returnError = validateQty(value)
                        If returnError = True Then
                            validationError = True
                            Exit For
                        End If
                End Select
            End If
        Next

        If validationError = False Then
            _DRLHeader.ClearItemCollection()
            totalvalue = 0
            sequenceno = 0

            For value As Integer = 0 To ssGrid2_Sheet1.Rows.Count - 1
                lngValue = Integer.Parse(ssGrid2_Sheet1.GetText(value, 3))
                If (lngValue <> EDIT_NONE) Then

                    Select Case (lngValue)
                        Case (EDIT_NEW)
                            If ssGrid2_Sheet1.GetText(value, 0) <> String.Empty Then
                                Save(ssGrid2_Sheet1.GetText(value, 0), CInt(Val(ssGrid2_Sheet1.GetText(value, 1))))
                            End If
                    End Select
                End If

            Next value

            pbSaveProgress.Visible = True

            _DRLHeader.SaveIBTin(TxtComment.Text, txtSelectedStore.Text, UserId, lblConsignmentNo.Text, selectedNumb, totalvalue)

            number = CInt(_DRLHeader.Number.Value)
            pnlWaitMsg.Visible = False
            MessageBox.Show("DRL No " & number.ToString("#####0").PadLeft(6, "0"c) & " has been created", "IBTIn", MessageBoxButtons.OK)

            loadConsigments()
        End If

        pnlWaitMsg.Visible = False
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        If lblSelection.Visible = True Then
            If MessageBox.Show("Do you wish to continue resetting?", Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
                Exit Sub
            End If
        End If
        loadConsigments()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        _blnLeaveSSConsignment = True
        Me.FindForm.Close()
    End Sub



    Private Sub ssGrid2_EditModeOff(ByVal sender As Object, ByVal e As System.EventArgs) Handles ssGrid2.EditModeOff

        Dim returnError As Boolean

        _blnEditModeOff = True
        selectedcol = ssGrid2.ActiveSheet.ActiveColumn.Index
        selectedrow = ssGrid2.ActiveSheet.ActiveRow.Index
        rowcreated = False

        DisplayStatus()

        validationError = False

        If ssGrid2_Sheet1.GetText(selectedrow, 0) = String.Empty And selectedrow = ssGrid2_Sheet1.Rows.Count - 1 Then
            ' do nothing 
        Else

            'validate input 
            Select Case selectedcol
                Case Is = 0
                    returnError = validatePartNo(selectedrow)
                Case Is = 1
                    returnError = CBool(validateQty(selectedrow))
            End Select

            'Set Total 
            ssGrid2_Sheet1.SetFormula(0, 6, "SUM(G2:G" & (ssGrid2_Sheet1.Rows.Count - 1).ToString & ")")
            txtIBTtotal.Text = ssGrid2_Sheet1.GetText(0, 6)

            'Position focus 
            If returnError = False Then
                If rowcreated = False Then
                    ssGrid2.ActiveSheet.SetActiveCell(selectedrow, selectedcol + 1)
                Else
                    ssGrid2.ActiveSheet.SetActiveCell(selectedrow + 1, 0)
                    Renumber()
                End If

            Else

                If rowcreated = False Then
                    ssGrid2.ActiveSheet.SetActiveCell(selectedrow, selectedcol)
                Else
                    ssGrid2.ActiveSheet.SetActiveCell(selectedrow + 1, 0)
                    rowcreated = False
                    Renumber()
                End If

                If ssGrid2.ActiveSheet.RowCount = 0 Then
                    btnSave.Visible = False
                End If

            End If
        End If
        _blnEditModeOff = False
    End Sub

    Private Sub ssGrid2_SubEditorOpening(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SubEditorOpeningEventArgs) Handles ssGrid2.SubEditorOpening
        If (1 = 1) Then
            e.Cancel = True
            ssGrid2_Sheet1.SetActiveCell(selectedrow + 1, selectedcol)
        End If
    End Sub

    Private Sub ssGrid2_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles ssGrid2.LeaveCell

        Dim returnerror As Boolean = False
        _blnLeaveCell = True

        If validationError = True Then
            e.NewRow = selectedrow
            e.NewColumn = selectedcol
            ssGrid2_Sheet1.SetActiveCell(selectedrow, selectedcol)
            validationError = False
            Return
        End If

        selectedrow = e.Row
        If CDbl(Trim(ssGrid2_Sheet1.GetText(selectedrow, 3))) <> EDIT_NONE And e.Column = 1 Then
            If Trim(ssGrid2_Sheet1.GetText(selectedrow, 1)) = String.Empty Then
                If _blnEditModeOff = False Then
                    If validationError = False Then
                        returnerror = CBool(validateQty(selectedrow))
                        If returnerror = True Then
                            validationError = True
                        End If
                    End If
                End If
            End If

        End If

        If Trim(ssGrid2_Sheet1.GetText(e.NewRow, 0)) = "" And e.Column = 2 Then
            e.NewColumn = 0
            ssGrid2_Sheet1.SetActiveCell(e.NewRow, e.NewColumn)
        End If

        If rowcreated = True Then

            If Trim(ssGrid2_Sheet1.GetText(selectedrow, 1)) = String.Empty Then
                'position focus
                e.NewRow = selectedrow
                e.NewColumn = 0
                ssGrid2.ActiveSheet.SetActiveCell(selectedrow + 1, 0)
                rowcreated = False
            End If

        End If

        _blnLeaveCell = False

    End Sub

    Private Sub ssgrid2_deleterow()

        DisplayStatus()
        If rowheaderclick = True Then

            If CLng(ssGrid2.ActiveSheet.GetValue(grid2selectedrow, 3)) <> EDIT_NONE And ssGrid2.ActiveSheet.GetText(grid2selectedrow, 1) <> String.Empty Then
                ssGrid2.ActiveSheet.SetRowVisible(grid2selectedrow, False)
                ssGrid2.ActiveSheet.SetValue(grid2selectedrow, 0, "000000")
                ssGrid2.ActiveSheet.SetValue(grid2selectedrow, 3, EDIT_DEL)
                ssGrid2.ActiveSheet.SetValue(grid2selectedrow, 6, 0)
                txtIBTtotal.Text = ssGrid2_Sheet1.GetText(0, 6)
                ssGrid2_Sheet1.SetActiveCell(grid2selectedrow + 1, 0)
            End If

        Else

            DisplayWarning("Select row header to indicate row to delete")

        End If

        Renumber()
    End Sub



    Private Sub Renumber()
        Dim intCount As Integer = 0
        Dim rowCount As Integer = 0

        rowCount = 1
        For intCount = 0 To ssGrid2_Sheet1.Rows.Count - 1
            If ssGrid2.ActiveSheet.GetRowVisible(intCount) = True Then
                ssGrid2.Sheets(0).Rows(intCount).Label = CStr(rowCount)
                rowCount = rowCount + 1
            End If
        Next intCount

        If rowCount <= 2 Then
            btnSave.Visible = False
        End If

    End Sub

    Private Function validatePartNo(ByVal selected3row As Integer) As Boolean

        Dim boPart As New BOStock.cStock(_Oasys3DB)
        Dim SkuNo As String
        Dim skuvalue As Integer

        DisplayStatus()
        SkuNo = Trim(ssGrid2_Sheet1.GetText(selected3row, 0))
        If Len(SkuNo) <= 6 Then
            If IsNumeric(SkuNo) Then
                skuvalue = CInt(SkuNo)
                SkuNo = skuvalue.ToString.PadLeft(6, "0"c)
                ssGrid2_Sheet1.SetValue(selected3row, 0, SkuNo)
                boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, ssGrid2_Sheet1.GetText(selected3row, 0))
                If boPart.LoadMatches.Count < 1 Then
                    ssGrid2_Sheet1.SetValue(selected3row, 0, String.Empty)
                    DisplayWarning("SKU does not exist")
                    validationError = True
                Else
                    If boPart.CatchAll.Value = True Then
                        DisplayWarning("Invalid Item Type (Dummy).")
                        validationError = True
                        ssGrid2_Sheet1.SetValue(selected3row, 0, String.Empty)
                    Else
                        ssGrid2_Sheet1.SetValue(selected3row, 3, EDIT_NEW)
                        ssGrid2_Sheet1.SetValue(selected3row, 4, boPart.Description.Value)
                        ssGrid2_Sheet1.SetValue(selected3row, 5, boPart.NormalSellPrice.Value)
                        validationError = False
                    End If

                End If

                For count As Integer = 1 To ssGrid2_Sheet1.RowCount - 1
                    If count <> selected3row And CDbl(ssGrid2_Sheet1.GetText(count, 3)) = EDIT_NEW Then
                        If ssGrid2_Sheet1.GetText(count, 0) = ssGrid2_Sheet1.GetText(selected3row, 0) Then
                            DisplayWarning("Duplicate SKU entered - Maintain")
                            ssGrid2_Sheet1.SetValue(selected3row, 4, String.Empty)
                            ssGrid2_Sheet1.SetValue(selected3row, 5, String.Empty)
                            ssGrid2_Sheet1.SetValue(selected3row, 0, String.Empty)
                            validationError = True
                        End If
                    End If
                Next count
            Else

                DisplayWarning("Sku No can not contain alphabetic characters")
                validationError = True
            End If

        Else

            DisplayWarning("Sku No can not be longer than 6 characters")
            validationError = True

        End If

        validatePartNo = validationError

    End Function

    Private Function validateQty(ByVal selected3row As Integer) As Boolean
        Const PARAM_MAX_QTY As Integer = 460

        Dim newrow As Integer
        Dim boParm As New BOSystem.cParameter(_Oasys3DB)
        Dim MaxQuantity As Integer = boParm.GetParameterInteger(PARAM_MAX_QTY)

        validationError = False
        rowcreated = False

        If Val(ssGrid2_Sheet1.GetText(selected3row, 1)) > MaxQuantity Then

            If _blnWarningMessages = True Then
                'DisplayStatus("Max Quantity exceeded", DisplayTypes.IsWarning)
                If MsgBox("Quantity seems high - Are you sure ?", MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "IBT In") = MsgBoxResult.No Then
                    ssGrid2_Sheet1.SetValue(selected3row, 1, String.Empty)
                    validationError = True
                End If
            End If

            If validationError = False Then
                Dim maxVal As Long = maxQty(CDec(ssGrid2_Sheet1.GetValue(selected3row, 5)))
                If CLng(ssGrid2_Sheet1.GetValue(selected3row, 1)) > maxVal Then

                    DisplayWarning("Maximum value has exceeded " & ChrW(163) & maxVal.ToString)
                    validationError = True
                    ssGrid2_Sheet1.SetText(selected3row, 1, String.Empty)

                Else

                    ssGrid2_Sheet1.SetValue(selected3row, 6, CInt(ssGrid2_Sheet1.GetValue(selected3row, 1)) * CDec(ssGrid2_Sheet1.GetValue(selected3row, 5)))

                End If

            End If

        Else

            If Val(ssGrid2_Sheet1.GetText(selected3row, 1)) <= 0 Then

                ssGrid2_Sheet1.SetValue(selected3row, 1, String.Empty)
                'ssGrid2.EditMode = True
                DisplayWarning("Quantity cannot be zero or negative")
                validationError = True

            Else

                If CInt(ssGrid2_Sheet1.GetValue(selected3row, 1)) > maxQty(CDec(ssGrid2_Sheet1.GetValue(selected3row, 5))) Then
                    DisplayWarning("Maximum value has exceeded 100,000")
                    validationError = True
                    ssGrid2_Sheet1.SetValue(selected3row, 1, String.Empty)
                Else
                    ssGrid2_Sheet1.SetValue(selected3row, 6, CInt(ssGrid2_Sheet1.GetValue(selected3row, 1)) * CDec(ssGrid2_Sheet1.GetValue(selected3row, 5)))
                End If

            End If

        End If

        If validationError = False Then

            If lblConsignmentNo.Text <> String.Empty And txtSelectedStore.Text <> String.Empty Then
                btnSave.Visible = True
            End If

            If Val(ssGrid2_Sheet1.GetText(selected3row, 1)) > 0 And selected3row = ssGrid2_Sheet1.RowCount - 1 Then
                newrow = selected3row + 1
                ssGrid2_Sheet1.AddRows(newrow, 1)
                ssGrid2_Sheet1.SetText(newrow, 3, CStr(EDIT_NONE))
                rowcreated = True
            End If

        End If

        validateQty = validationError

    End Function

    Public Function maxQty(ByVal Price As Decimal) As Long
        Const PARAM_MAX_VAL As Integer = 461
        Using boParm As New BOSystem.cParameter(_Oasys3DB)
            maxQty = boParm.GetParameterInteger(PARAM_MAX_VAL)
        End Using
        'If Price <> 0 Then
        '    maxQty = 100000 / Price
        'End If
    End Function

    Private Shared Function IbtStoreListSQL() As String
        Dim StoreListFactory As New IBTCore.IbtStoreListSqlFactory
        Dim StoreList As IBTCore.IIbtStoreListSQLStatement
        StoreList = StoreListFactory.GetImplementation()
        Return StoreList.GetSQL
    End Function

    Private Sub BuildLists()

        Using retOptions As New BOSystem.cRetailOptions(_Oasys3DB)
            retOptions.LoadRetailOptions()
            Dim ds As DataSet = _Oasys3DB.ExecuteSql(IbtStoreListSQL())
            For intIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim strNumb = CStr(ds.Tables(0).Rows(intIndex).Item(0))
                Dim strCountry = CStr(ds.Tables(0).Rows(intIndex).Item(1))
                Dim strAddress = CStr(ds.Tables(0).Rows(intIndex).Item(2))

                If strCountry.Trim = retOptions.CountryCode.Value.Trim Then
                    validStore.Add(strNumb, strAddress)
                Else
                    invalidStore.Add(strNumb, strAddress)
                End If
            Next

        End Using

    End Sub

    Private Function IsvalidStore(ByVal store As String) As String

        If validStore.ContainsKey(store) Then
            IsvalidStore = validStore.Item(store)
        Else
            If invalidStore.ContainsKey(store) Then
                IsvalidStore = String.Empty
            Else
                IsvalidStore = "Store not found in Store Master"
            End If
        End If

    End Function

    Private Sub TxtComment_leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtComment.Leave

        If ActiveControl IsNot Nothing Then
            If ActiveControl.Text <> "F10 Exit" Then

                If _blnLeaveSSConsignment = False Then
                    ConsignmentCheck()
                Else
                    If _blnRowSelected = True Then
                        TxtComment.Focus()
                    End If '_blnRowSelected = True
                End If ' _blnLeaveSSConsignment = False

            End If
        End If

        _blnRowSelected = False
        _blnLeaveSSConsignment = False

    End Sub

    Private Sub TxtComment_keydown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtComment.KeyDown

        If e.KeyValue = Keys.Enter Or e.KeyValue = Keys.Tab Then
            _blnLeaveSSConsignment = False
            ConsignmentCheck()
            ssConsignment.Enabled = False
        End If

    End Sub

    Private Sub ConsignmentCheck()

        If _blnConsignmentCheck = False Then
            _blnConsignmentCheck = True

            If lblConsignmentNo.Text > String.Empty Then
                GroupBox2.Visible = True
                btnStockEnquiry.Visible = True
                ssGrid2_Sheet1.SetActiveCell(1, 0)
                ssGrid2.Focus()
            Else
                DisplayWarning("Consignment has not been selected")
                ssConsignment.Focus()
            End If

            _blnConsignmentCheck = False
        End If

    End Sub

    Private Sub ssGrid2_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles ssGrid2.CellClick
        rowheaderclick = e.RowHeader
        grid2selectedrow = e.Row
    End Sub



    Private Sub updateProgressBar(ByVal intPercentageDone As Integer) Handles _DRLHeader.PercentageDone
        pbSaveProgress.Increment(intPercentageDone - pbSaveProgress.Value)
    End Sub


End Class
