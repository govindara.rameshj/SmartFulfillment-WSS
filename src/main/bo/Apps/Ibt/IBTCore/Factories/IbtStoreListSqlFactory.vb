﻿
Public Class IbtStoreListSqlFactory
    Inherits TpWickes.Library.RequirementSwitchFactory(Of IIbtStoreListSQLStatement)

    Friend _requirementSwitchParameterId As Integer = -22035

    Sub New()
        MyBase.SetSwitchParameterID(_requirementSwitchParameterId)
    End Sub

    Public Overrides Function ImplementationA() As IIbtStoreListSQLStatement
        Return New IBTStoreListSQLIncludingStoreName
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IIbtStoreListSQLStatement
        Return New IBTStoreListSQLExcludingStoreName
    End Function
End Class
