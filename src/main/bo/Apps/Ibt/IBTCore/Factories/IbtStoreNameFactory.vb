﻿Imports TpWickes.Library

Public Class IbtStoreNameFactory
    Inherits RequirementSwitchFactory(Of IIbtStoreName)

    Private _requirementSwitchParameterId As Integer = -22035

    Sub New()
        MyBase.SetSwitchParameterID(_requirementSwitchParameterId)
    End Sub

    Public Overrides Function ImplementationA() As IIbtStoreName
        Return New IbtStoreName
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IIbtStoreName
        Return New IbtStoreNameOldWay
    End Function
End Class
