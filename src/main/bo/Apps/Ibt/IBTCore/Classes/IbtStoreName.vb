﻿Public Class IbtStoreName

    Implements IIbtStoreName

    Public Function ReturnAddress(ByVal Store As cStore) As String Implements IIbtStoreName.ReturnAddress

        Return Store.StoreNameOnReceipt.Value

    End Function

End Class