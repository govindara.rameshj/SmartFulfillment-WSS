﻿Imports OasysDBBO.Oasys3.DB
Imports System.Data
Imports IBTCore

Public Class IbtOut
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private WithEvents _DRLHeader As New BOPurchases.cDrlHeader(_Oasys3DB)

    Private selectedrow As Integer = 0
    Private selectedcol As Integer = 0
    Private sequenceno As Integer = 0
    Private CurStore As Integer = 0
    Private totalvalue As Decimal = 0
    Private validationError As Boolean = False
    Private generatePassword As Boolean = False
    Private _strStoreName As String = String.Empty
    Private KeyText As String = String.Empty
    Private NewRowCreated As Boolean = False
    Private lstStore As List(Of BOStore.cStore)
    Private boStore As New BOStore.cStore(_Oasys3DB)
    Private boRO As New BOSystem.cRetailOptions(_Oasys3DB)
    Private BOsysnos As New BOSystem.cSystemNumbers(_Oasys3DB)
    Private boPart As New BOStock.cStock(_Oasys3DB)
    Private boParm As New BOSystem.cParameter(_Oasys3DB)
    Private _strCountryCode As String = String.Empty

    Private validationRun As Boolean = True
    Private rowheaderclick As Boolean = False
    Private rowSelected As Boolean = False

    Private _Grid1SelectedRow As Integer = 0
    Private _blnPrintCancelled As Boolean = False
    Private _blnEnterPressed As Boolean = False
    Private _strStoreNumber As String = String.Empty
    Private _PrinterName As String = String.Empty
    Private _blnEditModeOff As Boolean = False
    Private _blnLeaveCell As Boolean = False
    Private _blncboStoreFocus As Boolean = False
    Private _lstInternalWarehouse As New List(Of String)
    Private _strUser As String = String.Empty
    Private _blnAlreadyValidating As Boolean = False
    Private _IbtAddressDisplayFactory As IIbtStoreName

    Const EDIT_NONE As Long = 0
    Const EDIT_NEW As Long = 1
    Const EDIT_EDIT As Long = 2
    Const EDIT_DEL As Long = 3
    Const START_ROW_COUNT = 12

    Private wasClosed As Boolean

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        Dim BOUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
        _strUser = BOUser.GetUser(userId).Initials.Value.Trim

        wasClosed = False
    End Sub

    Protected Overrides Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)

        Dim Result As DialogResult = DialogResult.Ignore

        If (wasClosed = False) Then
            If txtFromStore.Text > String.Empty Then
                Result = MessageBox.Show("Do you wish to exit?", "IBT Out", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                If Result = DialogResult.Yes Then
                    'write blank header
                    _DRLHeader.SaveIBTOut(String.Empty, String.Empty, 0, UserId, txtDrlno.Text, False)
                    MyBase.Form_Closing(sender, e)
                    wasClosed = True
                Else
                    e.Cancel = True
                End If 'Result = DialogResult.OK
            Else
                MyBase.Form_Closing(sender, e)
            End If 'btnSave.Visible = True
        End If

    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnStockEnquiry.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick() : e.Handled = False
            Case Keys.Delete
                If ssGrid1.Focused = True Then
                    ssgrid1_deleterow()
                Else
                    e.Handled = False : MyBase.Form_KeyDown(sender, e)
                End If

            Case Else : e.Handled = False : MyBase.Form_KeyDown(sender, e)
        End Select

        If e.Handled = True Then
            DisplayStatus()
        End If

    End Sub

    Protected Overrides Sub DoProcessing()

        Dim BoSystemDate As New BOSystem.cSystemDates(_Oasys3DB)

        BoSystemDate.LoadMatches()

        GroupBox2.Visible = False

        If BoSystemDate.NightSetNo.Value < "400" And BoSystemDate.NightSetNo.Value <> "000" Then
            MsgBox("Night Incomplete - Please Check", MsgBoxStyle.OkOnly, "IBT In")
            FindForm.Close()
        End If

        Dim storeNo As Integer = 0
        Dim strInternalWH As String = String.Empty
        Dim strWarehouse() As String

        boRO.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boRO.RetailOptionsID, "01")
        boRO.LoadMatches()

        CurStore = CInt(boRO.Store.Value)

        Dim myDataSet As DataSet = boRO.Oasys3DB.ExecuteSql("Select CountryCode From STRMAS WHERE NUMB = '" & CurStore.ToString("##0").PadLeft(3, "0"c) & "'")

        If myDataSet.Tables(0).Rows.Count > 0 Then
            _strCountryCode = CStr(myDataSet.Tables(0).Rows(0).Item(0))
        Else
            _strCountryCode = "UK"
        End If

        _IbtAddressDisplayFactory = (New IbtStoreNameFactory).GetImplementation

        boStore.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boStore.CountryCode, _strCountryCode)
        boStore.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
        boStore.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boStore.IsDeleted, False)
        boStore.SortBy(boStore.StoreID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        lstStore = boStore.LoadMatches()

        For Each row As BOStore.cStore In lstStore
            If row.StoreID.Value <> boRO.Store.Value Then
                storeNo = CInt(row.StoreID.Value)
                cboStore.Items.Add(storeNo.ToString("##0").PadLeft(3, "0"c) & Space(1) & _IbtAddressDisplayFactory.ReturnAddress(row)) 'row.AddressLine1.Value)
            End If
        Next

        'get internal warehouse 
        strInternalWH = boParm.GetParameterString(405)
        strWarehouse = Split(strInternalWH, ",")
        For x As Integer = 0 To UBound(strWarehouse)
            _lstInternalWarehouse.Add(strWarehouse(x))
        Next

        '   cboStore.SelectedIndex = 0
        txtFromStore.Text = String.Empty
        'key mappings
        Dim AncestorFocusedMap As InputMap = ssGrid1.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        Dim FocusedMap As InputMap = ssGrid1.GetInputMap(InputMapMode.WhenFocused)
        AncestorFocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        FocusedMap.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.StopEditing)
        FocusedMap.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.StopEditing)

        rbIncludeInSales.Checked = True
        ssPrintGrid_Sheet1.PrintInfo.ShowPrintDialog = False
        pbSaveProgress.Visible = False
        ssGrid1_Sheet1.SetText(0, 3, CStr(EDIT_NONE))
        ssGrid1_Sheet1.SetFormula(0, 5, "SUM(G2:G2)")
        ssGrid1.ActiveSheet.SetRowVisible(0, False)
        ssGrid1_Sheet1.AddRows(1, 1)
        ssGrid1_Sheet1.SetText(1, 3, CStr(EDIT_NONE))
        ssGrid1_Sheet1.SetActiveCell(1, 0)
        GroupBox2.Visible = False
        btnStockEnquiry.Visible = False

        txtFromStore.Focus()

    End Sub


    Private Sub cboStore_SelectedIndexCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStore.SelectionChangeCommitted
        If _blncboStoreFocus = False Then
            ProcessStore()
        Else
            _blncboStoreFocus = False
        End If
    End Sub

    Private Sub cboStore_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboStore.KeyDown
        If (e.KeyData = Keys.Down Or e.KeyData = Keys.Up) And cboStore.DroppedDown = False Then
            cboStore.DroppedDown = True
        End If
    End Sub

    Private Sub ProcessStore()
        Dim position As Integer = InStr(cboStore.Text, " ")
        If position <> 0 Then
            txtFromStore.Text = Mid(cboStore.Text, 1, position)
            _strStoreName = Mid(cboStore.Text, position)
            ValidateStore()
        End If
        If validationError = False Then
            validationRun = False
            validationRun = True
        End If
    End Sub

    Private Sub txtFromStore_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFromStore.KeyDown

        If e.KeyValue >= 48 And e.KeyValue <= 57 Or e.KeyValue >= 96 And e.KeyValue <= 105 Or e.KeyValue = 8 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Then 'process keys 0-9
            'txtFromStore.Text = txtFromStore.Text & ChrW(e.KeyValue)
        Else
            e.SuppressKeyPress = True
        End If

        If e.KeyValue = Keys.Enter Then

            _blnEnterPressed = True

            testStore()

            If txtFromStore.Text = String.Empty And txtFromStore.Focused = True And validationError = False Then
                validationRun = False
                cboStore.Focus()
                validationRun = True
            End If

        End If

        _blnEnterPressed = False

    End Sub 'txtFromStore_KeyDown

    Private Sub txtFromStore_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFromStore.Leave
        If ActiveControl IsNot Nothing Then
            If txtFromStore.Text = String.Empty And ActiveControl.Name = "cboStore" Or ActiveControl.Name = "btnReset" Or ActiveControl.Name = "btnExit" Then
                validationRun = False
            End If
        Else
            validationRun = False
        End If

        If _blnEnterPressed = True Then
            validationRun = False
        End If

        If validationRun = True Then
            testStore()
        Else
            If _blnEnterPressed = False Then
                cboStore.SelectedIndex = -1
                cboStore.Focus()
            End If
        End If

        validationRun = True

        _blnEnterPressed = False

    End Sub 'txtFromStore_Leave

    Private Sub testStore()

        Dim cborow As Integer = 0
        Dim storeno As Integer = 0

        If txtFromStore.Text > String.Empty Then
            storeno = CInt(Trim(txtFromStore.Text))
            txtFromStore.Text = storeno.ToString("##0").PadLeft(3, "0"c)
            cborow = cboStore.FindString(txtFromStore.Text)
            If cborow > -1 Then
                cboStore.SelectedIndex = -1
                cboStore.SelectedIndex = cborow
            End If
        End If

        ValidateStore()

        If validationError = False Then
            If cboStore.SelectedIndex > -1 Then
                rbIncludeInSales.Focus()
            End If
        End If

    End Sub ' testStore()

    Private Sub ssGrid1_SubEditorOpening(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SubEditorOpeningEventArgs) Handles ssGrid1.SubEditorOpening
        If (1 = 1) Then
            e.Cancel = True
            ssGrid1_Sheet1.SetActiveCell(selectedrow + 1, selectedcol)
        End If
    End Sub

    Private Sub ssGrid1_EditModeOff(ByVal sender As Object, ByVal e As System.EventArgs) Handles ssGrid1.EditModeOff

        Dim returnError As Boolean

        _blnEditModeOff = True
        selectedcol = ssGrid1.ActiveSheet.ActiveColumn.Index
        selectedrow = ssGrid1.ActiveSheet.ActiveRow.Index
        NewRowCreated = False

        DisplayStatus()

        validationError = False

        If ssGrid1_Sheet1.GetText(selectedrow, 0) = String.Empty And selectedrow = ssGrid1_Sheet1.Rows.Count - 1 Then
            ' do nothing 
        Else

            'validate input 
            Select Case selectedcol
                Case Is = 0
                    returnError = validatePartNo(selectedrow, selectedcol)
                Case Is = 1
                    If _blnAlreadyValidating = False Then
                        _blnAlreadyValidating = True
                        returnError = validateQty(selectedrow, selectedcol)
                    End If
            End Select

            'Set Total 
            ssGrid1_Sheet1.SetFormula(0, 6, "SUM(G2:G" & (ssGrid1_Sheet1.Rows.Count - 1).ToString & ")")
            txtIBTTotal.Text = ssGrid1_Sheet1.GetText(0, 6)

            'Position focus 
            If returnError = False Then
                If NewRowCreated = False Then
                    ssGrid1.ActiveSheet.SetActiveCell(selectedrow, selectedcol + 1)
                Else
                    ssGrid1.ActiveSheet.SetActiveCell(selectedrow + 1, 0)
                    Renumber()
                End If

            Else

                ssGrid1_Sheet1.SetValue(selectedrow, selectedcol, String.Empty)
                ssGrid1.ActiveSheet.SetActiveCell(selectedrow, selectedcol)

            End If

        End If

        CheckSaveButtonStatus()

        _blnEditModeOff = False

    End Sub 'ssGrid1_KeyPress

    Private Sub ssGrid1_LeaveCell(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.LeaveCellEventArgs) Handles ssGrid1.LeaveCell

        Dim returnerror As Boolean = False
        _blnLeaveCell = True

        If validationError = True Then
            e.NewRow = selectedrow
            e.NewColumn = selectedcol
            ssGrid1_Sheet1.SetActiveCell(selectedrow, selectedcol)
            validationError = False
            Return
        End If

        selectedrow = e.Row
        If CDbl(Trim(ssGrid1_Sheet1.GetText(selectedrow, 3))) <> EDIT_NONE And e.Column = 1 Then
            If Trim(ssGrid1_Sheet1.GetText(selectedrow, 1)) = String.Empty Then
                If _blnEditModeOff = False Then
                    If validationError = False Then
                        _blnAlreadyValidating = True
                        returnerror = validateQty(selectedrow, e.Column)
                        If returnerror = True Then
                            e.NewRow = e.Row
                            ssGrid1_Sheet1.SetActiveCell(e.Row, e.Column)
                            validationError = True
                        End If
                    End If
                End If
            End If
        End If

        If Trim(ssGrid1_Sheet1.GetText(e.NewRow, 0)) = "" And e.Column = 2 And validationError = False Then
            e.NewColumn = 0
            ssGrid1_Sheet1.SetActiveCell(e.NewRow, e.NewColumn)
        End If

        If NewRowCreated = True Then

            If Trim(ssGrid1_Sheet1.GetText(selectedrow, 1)) = String.Empty Then
                'position focus
                e.NewRow = selectedrow
                e.NewColumn = 0
                ssGrid1.ActiveSheet.SetActiveCell(selectedrow + 1, 0)
                NewRowCreated = False
            End If

        End If

        CheckSaveButtonStatus()

        _blnLeaveCell = False
    End Sub 'ssGrid2_LeaveCell

    Private Sub ssGrid1_CellClick(ByVal sender As System.Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs) Handles ssGrid1.CellClick
        selectedcol = e.Column
        selectedrow = e.Row
        rowheaderclick = e.RowHeader
        _Grid1SelectedRow = e.Row
        NewRowCreated = False
    End Sub

    Private Sub ssgrid1_deleterow()
        Dim lngRowToDelete As Integer = ssGrid1_Sheet1.ActiveRow.Index
        DisplayStatus()

        If lngRowToDelete > 0 Then
            If ssGrid1_Sheet1.Cells(lngRowToDelete, 0).Text <> "" Then
                If MessageBox.Show("Are you sure you want to delete line " & lngRowToDelete.ToString, "Delete Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then

                    ssGrid1.ActiveSheet.SetRowVisible(lngRowToDelete, False)
                    ssGrid1.ActiveSheet.SetValue(lngRowToDelete, 0, "000000")
                    ssGrid1.ActiveSheet.SetValue(lngRowToDelete, 3, EDIT_DEL)
                    ssGrid1.ActiveSheet.SetValue(lngRowToDelete, 6, 0)
                    txtIBTTotal.Text = ssGrid1_Sheet1.GetText(0, 6)
                    ssGrid1_Sheet1.SetActiveCell(lngRowToDelete + 1, 0)

                End If
            End If
        End If

        Renumber()
        CheckSaveButtonStatus()
    End Sub 'ssgrid2_deleterow()

    Private Sub Renumber()
        Dim intCount As Integer = 0
        Dim rowCount As Integer = 0

        rowCount = 1
        For intCount = 0 To ssGrid1_Sheet1.Rows.Count - 1
            If ssGrid1.ActiveSheet.GetRowVisible(intCount) = True Then
                ssGrid1.Sheets(0).Rows(intCount).Label = CStr(rowCount)
                rowCount = rowCount + 1
            End If
        Next
    End Sub 'Renumber()

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim lngValue As Long = 0
        Dim validationError As Boolean = False
        Dim returnError As Boolean = False
        Dim intDrlNumber As Integer = 0
        Dim strPrintMessage As String = String.Empty
        Dim blnIncludeInSales As Boolean = False
        DisplayStatus()

        ' validated spreadsheet so no errors with f5
        btnSave.Visible = False

        For i As Integer = 1 To ssGrid1_Sheet1.RowCount - 1
            If Val(ssGrid1_Sheet1.GetText(i, 0)) <> 0 Then
                If Val(ssGrid1_Sheet1.GetText(i, 1).Trim) = 0 Then
                    MsgBox("Unable to save as some quantities are equal to zero or blank.", MsgBoxStyle.OkOnly, "IBtOut")
                    btnSave.Visible = True
                    Me.Cursor = Cursors.Default
                    _blnAlreadyValidating = True
                    ssGrid1.Focus()
                    ssGrid1_Sheet1.SetActiveCell(i, 1)
                    _blnAlreadyValidating = False
                    Exit Sub
                End If
            End If
        Next i

        pnlWaitMsg.Visible = True
        Me.Cursor = Cursors.WaitCursor

        Me.Refresh()
        Application.DoEvents()

        Trace.WriteLine("btnSave_Click")

        sequenceno = 0
        totalvalue = 0
        ssPrintGrid_Sheet1.RowCount = 0

        _DRLHeader.ClearItemCollection()

        For value As Integer = 0 To ssGrid1_Sheet1.Rows.Count - 1
            lngValue = Integer.Parse(ssGrid1_Sheet1.GetText(value, 3))

            If (lngValue <> EDIT_NONE) Then
                Select Case (lngValue)
                    Case (EDIT_NEW)
                        If Trim(ssGrid1_Sheet1.GetText(value, 0)) <> String.Empty Then
                            Save(ssGrid1_Sheet1.GetText(value, 0), CInt(Val(ssGrid1_Sheet1.GetText(value, 1))))
                        End If
                    Case (EDIT_EDIT)
                        'Should not happen

                    Case (EDIT_DEL)

                End Select
            End If

        Next value

        Trace.WriteLine("Records Written to collection")

        pbSaveProgress.Visible = True

        If rbIncludeInSales.Checked = True Then
            blnIncludeInSales = True
        Else
            blnIncludeInSales = False
        End If

        _DRLHeader.SaveIBTOut(TxtComment.Text, txtFromStore.Text, totalvalue, UserId, txtDrlno.Text, blnIncludeInSales)
        Trace.WriteLine("Records written to database")

        PrintFileCopy()

        Trace.WriteLine("Documents printed")

        intDrlNumber = CInt(txtDrlno.Text)

        If _blnPrintCancelled = True Then
            strPrintMessage = " has been saved"
        Else
            strPrintMessage = " has been printed"
        End If

        pnlWaitMsg.Visible = False

        MessageBox.Show("DRL No " & intDrlNumber.ToString("#####0").PadLeft(6, "0"c) & strPrintMessage, "IBTs Out", MessageBoxButtons.OK)
        txtFromStore.Text = String.Empty

        'ensure that the collection is clear to deal with reset Ibtouts
        _DRLHeader.ClearItemCollection()

        btnReset.PerformClick()

        pnlWaitMsg.Visible = False
        Me.Cursor = Cursors.Default

        btnSave.Visible = False
        Me.Refresh()
        Application.DoEvents()

    End Sub 'btnSave_Click

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(UserId, WorkstationId, SecurityLevel, String.Empty)

            lookup.ShowAccept()

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Dim boPart As New BOStock.cStock(_Oasys3DB)

                    ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 0, CStr(lookup.SkuNumbers(0)))
                    boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, ssGrid1_Sheet1.GetText(ssGrid1_Sheet1.ActiveRow.Index, 0))

                    If boPart.LoadMatches.Count > 0 Then
                        ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 4, boPart.Description.Value)
                        ssGrid1_Sheet1.SetValue(ssGrid1_Sheet1.ActiveRow.Index, 5, boPart.NormalSellPrice.Value)
                        ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 3, CStr(EDIT_NEW))
                        ssGrid1_Sheet1.SetActiveCell(ssGrid1_Sheet1.ActiveRow.Index, 1)
                    End If

                    CheckForDuplicate(ssGrid1_Sheet1.ActiveRow.Index, 0)

                End If
            End Using

        End Using
        ssGrid1.Focus()

    End Sub ' btnStockEnquiry_Click

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Dim Result As DialogResult = DialogResult.Ignore
        If txtFromStore.Text > String.Empty Then
            Result = MessageBox.Show("Do you wish to continue resetting?", "IBT Out", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.Yes Then
                'write blank header
                _DRLHeader.SaveIBTOut(String.Empty, String.Empty, 0, UserId, txtDrlno.Text, False)
                Reset()
            End If 'Result = DialogResult.OK
        Else
            Reset()
        End If 'btnSave.Visible = True
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Sub Save(ByVal partno As String, ByVal qty As Integer)

        Dim printrow As Integer = 0
        Dim currcell As New FarPoint.Win.Spread.CellType.NumberCellType
        currcell.DecimalPlaces = 2

        sequenceno += 1

        boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, partno)
        boPart.AddLoadField(boPart.NormalSellPrice)
        boPart.AddLoadField(boPart.Description)
        boPart.LoadMatches()

        'add details to the collection 
        _DRLHeader.addItemToCollection(partno, sequenceno.ToString("###0").PadLeft(4, "0"c), qty, boPart.NormalSellPrice.Value)

        totalvalue += (boPart.NormalSellPrice.Value * qty)

        printrow = ssPrintGrid_Sheet1.RowCount - 1
        If printrow = -1 Then
            printrow = 0
        End If

        ssPrintGrid_Sheet1.AddRows(printrow, 1)
        ssPrintGrid_Sheet1.SetValue(printrow, 0, partno)
        ssPrintGrid_Sheet1.SetValue(printrow, 1, boPart.Description.Value)
        ssPrintGrid.ActiveSheet.AddSpanCell(printrow, 1, 1, 2)
        ssPrintGrid_Sheet1.SetValue(printrow, 3, qty)
        ssPrintGrid_Sheet1.Cells(printrow, 4).CellType = currcell
        ssPrintGrid_Sheet1.SetValue(printrow, 4, boPart.NormalSellPrice.Value)
        ssPrintGrid_Sheet1.Cells(printrow, 5).CellType = currcell
        ssPrintGrid_Sheet1.SetValue(printrow, 5, boPart.NormalSellPrice.Value * qty)

    End Sub 'Save

    Private Sub PrintFileCopy()

        Dim printrow As Integer = 0
        Dim tempString As String = String.Empty
        Dim pd As New PrintDialog
        Dim reply As DialogResult

        'place on grand totals

        Dim gnrlcell As New FarPoint.Win.Spread.CellType.GeneralCellType
        Dim textcell As New FarPoint.Win.Spread.CellType.TextCellType
        Dim numbCell As New FarPoint.Win.Spread.CellType.NumberCellType

        printrow = ssPrintGrid_Sheet1.RowCount
        ssPrintGrid_Sheet1.Cells(printrow - 1, 4, printrow - 1, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        ssPrintGrid_Sheet1.AddRows(printrow, 1)
        ssPrintGrid_Sheet1.Cells(printrow, 4).CellType = gnrlcell
        ssPrintGrid_Sheet1.Cells(printrow, 4).HorizontalAlignment = CellHorizontalAlignment.Right
        ssPrintGrid_Sheet1.SetText(printrow, 4, "Grand Total")
        ssPrintGrid_Sheet1.Cells(printrow, 5).CellType = numbCell
        ssPrintGrid_Sheet1.SetText(printrow, 5, CStr(totalvalue))
        ssPrintGrid.ActiveSheet.Cells(printrow, 4, printrow, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

        ' place in headers 

        ssPrintGrid_Sheet1.PrintInfo.Header = "/cI.B.T. File Copy (Sending Store)"
        ssPrintGrid_Sheet1.PrintInfo.Footer = "/lPrinted: " & Now
        ssPrintGrid_Sheet1.PrintInfo.Footer &= "/cPage /p of /pc"
        ssPrintGrid_Sheet1.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        ssPrintGrid_Sheet1.PrintInfo.RepeatRowStart = 1
        ssPrintGrid_Sheet1.PrintInfo.RepeatRowEnd = 19

        ssPrintGrid_Sheet1.AddRows(0, 18)
        Dim acell As FarPoint.Win.Spread.Cell
        acell = ssPrintGrid.ActiveSheet.Cells(0, 0)
        acell.HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

        tempString = "DRL Number : "
        ssPrintGrid_Sheet1.SetText(3, 0, tempString)

        tempString = txtDrlno.Text & Space(10) & "To Store : " & cboStore.SelectedItem.ToString & Space(35 - Len(cboStore.SelectedItem.ToString)) & "By:- " & _strUser
        ssPrintGrid_Sheet1.SetText(3, 1, tempString)
        ssPrintGrid.ActiveSheet.AddSpanCell(3, 1, 1, 5)

        tempString = "From Store : "
        ssPrintGrid_Sheet1.SetText(5, 0, tempString)

        tempString = CurStore.ToString("##0").PadLeft(3, "0"c) & Space(1) & boRO.StoreName.Value
        ssPrintGrid_Sheet1.SetText(5, 1, tempString)
        ssPrintGrid.ActiveSheet.AddSpanCell(5, 1, 1, 5)

        tempString = "IBT Date : "
        ssPrintGrid_Sheet1.SetText(7, 0, tempString)

        tempString = Date.Today.ToShortDateString
        ssPrintGrid_Sheet1.Cells(7, 1).HorizontalAlignment = CellHorizontalAlignment.Left
        ssPrintGrid_Sheet1.SetText(7, 1, tempString)
        ssPrintGrid.ActiveSheet.AddSpanCell(7, 1, 1, 5)

        tempString = "Comment : "
        ssPrintGrid_Sheet1.SetText(9, 0, tempString)

        tempString = TxtComment.Text
        ssPrintGrid_Sheet1.Cells(9, 1).HorizontalAlignment = CellHorizontalAlignment.Left
        textcell.Multiline = True
        textcell.WordWrap = True
        ssPrintGrid_Sheet1.Cells(9, 1).CellType = textcell
        ssPrintGrid_Sheet1.SetText(9, 1, tempString)
        ssPrintGrid.ActiveSheet.AddSpanCell(9, 1, 2, 5)

        ssPrintGrid_Sheet1.Cells(11, 1).CellType = gnrlcell
        ssPrintGrid_Sheet1.SetText(11, 0, "DRL Value :")
        ssPrintGrid_Sheet1.Cells(11, 1).HorizontalAlignment = CellHorizontalAlignment.Left
        ssPrintGrid_Sheet1.Cells(11, 1).CellType = numbCell
        ssPrintGrid_Sheet1.SetText(11, 1, CStr(totalvalue))

        tempString = "Collected by :"
        ssPrintGrid_Sheet1.SetText(13, 0, tempString)

        ssPrintGrid.ActiveSheet.Cells(13, 1, 13, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

        tempString = "(Print)" & Space(13) & " ____/____/________ (Date)"
        ssPrintGrid_Sheet1.SetText(13, 2, tempString)
        ssPrintGrid.ActiveSheet.AddSpanCell(13, 2, 1, 5)

        ssPrintGrid.ActiveSheet.Cells(15, 1, 15, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        tempString = "(Sign)"
        ssPrintGrid_Sheet1.SetText(15, 2, tempString)

        ssPrintGrid.ActiveSheet.Cells(16, 0, 16, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        ssPrintGrid_Sheet1.SetText(17, 0, "SKU Number")
        ssPrintGrid_Sheet1.SetText(17, 1, "Description")
        ssPrintGrid_Sheet1.SetText(17, 2, "")
        ssPrintGrid_Sheet1.Cells(17, 2).HorizontalAlignment = CellHorizontalAlignment.Right
        ssPrintGrid_Sheet1.Cells(17, 3).CellType = gnrlcell
        ssPrintGrid_Sheet1.SetText(17, 3, "Quantity")
        ssPrintGrid_Sheet1.Cells(17, 3).HorizontalAlignment = CellHorizontalAlignment.Right
        ssPrintGrid_Sheet1.Cells(17, 4).CellType = gnrlcell
        ssPrintGrid_Sheet1.SetText(17, 4, "Price")
        ssPrintGrid_Sheet1.Cells(17, 4).HorizontalAlignment = CellHorizontalAlignment.Right
        ssPrintGrid_Sheet1.Cells(17, 5).CellType = gnrlcell
        ssPrintGrid_Sheet1.SetText(17, 5, "Value")
        ssPrintGrid_Sheet1.Cells(17, 5).HorizontalAlignment = CellHorizontalAlignment.Right
        ssPrintGrid.ActiveSheet.Cells(17, 0, 17, 5).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        ssPrintGrid.ActiveSheet.AddColumnHeaderSpanCell(1, 1, 17, 5)

        reply = pd.ShowDialog
        If reply = DialogResult.OK Then
            _PrinterName = pd.PrinterSettings.PrinterName
            ssPrintGrid_Sheet1.PrintInfo.Printer = _PrinterName
            ssPrintGrid.PrintSheet(0)
            SecondPrint()
            ssSecondPrint.PrintSheet(0)
            _blnPrintCancelled = False
            _DRLHeader.DRLN0Printed(txtDrlno.Text)
        Else
            _blnPrintCancelled = True
        End If

    End Sub  'PrintFileCopy()

    Private Sub SecondPrint()
        Dim printset As New FarPoint.Win.Spread.PrintInfo()

        printset.ShowGrid = False
        printset.ShowBorder = False
        printset.ShowPrintDialog = False
        printset.Printer = _PrinterName
        printset.Header = "/cI.B.T. File Copy (Receiving Store)"
        printset.Footer = "/lPrinted: " & Now
        printset.Footer &= "/cPage /p of /pc"
        printset.Footer &= "/rVersion " & Application.ProductVersion
        printset.Margin.Left = 50
        printset.RepeatRowStart = 1
        printset.RepeatRowEnd = 17

        ssSecondPrint_Sheet1 = ssPrintGrid_Sheet1.Clone()
        ssSecondPrint_Sheet1.PrintInfo = printset

        ssSecondPrint.Sheets(0) = ssSecondPrint_Sheet1

    End Sub 'SecondPrint()

    Private Sub ValidateStore()

        If _blnAlreadyValidating = True Then Exit Sub

        Dim boStr As New BOStore.cStore(_Oasys3DB)
        Dim storeNo As Integer = 0
        Dim KeysUsed As DialogResult
        Dim intIndex As Integer = 0
        Dim strStoreSelectedCountryCode As String = String.Empty

        generatePassword = False
        validationError = False
        DisplayStatus()

        If txtFromStore.Text <> String.Empty Then

            If txtDrlno.Text = String.Empty Then
                txtDrlno.Text = BOsysnos.GetDRLNumber().ToString("#####0").PadLeft(6, "0"c)
            End If

            If Len(Trim(txtFromStore.Text)) < 3 Then
                storeNo = CInt(txtFromStore.Text)
                txtFromStore.Text = storeNo.ToString.PadLeft(3, "0"c)
            End If

            boStr.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boStr.StoreID, txtFromStore.Text)
            Dim myDataSet As DataSet = boRO.Oasys3DB.ExecuteSql("Select CountryCode From STRMAS WHERE NUMB = '" & txtFromStore.Text & "'")

            If myDataSet.Tables(0).Rows.Count > 0 Then
                strStoreSelectedCountryCode = CStr(myDataSet.Tables(0).Rows(0).Item(0))
            End If

            If boStr.LoadMatches.Count > 0 Then

                KeyText = boStr.StoreID.Value & Space(1) & _IbtAddressDisplayFactory.ReturnAddress(boStr) 'boStr.AddressLine1.Value

                If boStr.IsDeleted.Value = True Then
                    DisplayWarning("Deleted Store. Please key a non deleted store")
                    validationError = True
                Else

                    storeNo = CInt(txtFromStore.Text)

                    If storeNo = CurStore Then

                        DisplayWarning("Resident Store. Please key another store")
                        validationError = True

                    Else

                        If strStoreSelectedCountryCode.Trim <> _strCountryCode.Trim Then
                            DisplayWarning("Store has a different country code. Please use another store")
                            validationError = True
                        Else
                            If InternalWarehouse(CStr(storeNo)) Then
                                DDCCancelPassword()
                            Else
                                Select Case storeNo
                                    Case 167
                                        DisplayWarning("IBT Out creation is not permitted to store 167 - HDC return transfers should be raised to store 166 only")
                                        validationError = True
                                        'Case 514, 524, 534
                                        '    DDCCancelPassword()

                                    Case 166, 173
                                        IBTtoHDCPassword()

                                    Case 183
                                        DisplayWarning("You must enter the LAST SIX DIGITS of the Retail Development Order number to IBT to this department")

                                End Select

                            End If

                        End If

                    End If

                End If

            Else

                DisplayWarning("Please key a valid Store number ")
                validationError = True
                txtFromStore.Focus()

            End If

        Else

            DisplayWarning("Store has not been entered ")
            cboStore.SelectedIndex = -1
            validationError = True
            txtFromStore.Focus()

        End If

        If validationError = False And ssGrid1.Visible = True Then

            KeysUsed = MessageBox.Show("Would you like to change the store number ", "IBTs Out", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)

            If KeysUsed = DialogResult.Cancel Then
                txtFromStore.Text = _strStoreNumber
                intIndex = cboStore.FindString(_strStoreNumber)
                cboStore.SelectedIndex = intIndex
            End If

        End If

        _strStoreNumber = txtFromStore.Text

    End Sub 'ValidateStore()

    Private Sub DDCCancelPassword()

        Dim reply As String = String.Empty


        generatePassword = True

        Do

            Dim frmPassword As New frmPassword

            frmPassword.myInputBox("Contact IW Administration for Password, 5 Chars Only :", txtDrlno.Text, CurStore.ToString)
            frmPassword.ShowDialog()
            reply = frmPassword.Password

            If Len(reply) = 0 Then
                validationError = True
                txtFromStore.Focus()
                btnReset.PerformClick()
                Exit Do
            End If

            frmPassword = Nothing

        Loop While CheckPassWord(reply, 2, CStr(CurStore), txtDrlno.Text) = True

    End Sub 'DDCCancelPassword()

    Private Sub IBTtoHDCPassword()

        Dim reply As String = String.Empty

        generatePassword = True

        Do

            Dim frmPassword As New frmPassword

            frmPassword.myInputBox("Contact Conservatory Administation for Password, 5 Chars Only:", txtDrlno.Text, CurStore.ToString)
            frmPassword.ShowDialog()
            reply = frmPassword.Password
            If Len(reply) = 0 Then
                validationError = True
                txtFromStore.Focus()
                btnReset.PerformClick()
                Exit Do
            End If

            frmPassword = Nothing

        Loop While CheckPassWord(reply, 2, CStr(CurStore), txtDrlno.Text) = True

    End Sub 'IBTtoHDCPassword()

    Private Function CheckPassWord(ByVal Password As String, ByVal Type As Integer, ByVal Store As String, ByVal Number As String) As Boolean

        Dim CalcPassword As String = String.Empty
        Dim tempPassword As String = String.Empty
        Dim IntStore As Integer = 0
        Dim IntNumber As Integer = 0
        Dim WorkPassword As Integer = 0
        Dim intPassword As Integer = 0


        If IsNumeric(Password) Then

            If Password.Length < 5 Then
                intPassword = CInt(Password)
                Password = intPassword.ToString.PadLeft(5, "0"c)
            End If

            IntStore = CInt(Store)
            IntNumber = CInt(Number)
            WorkPassword = IntStore * 12345


            ' add the year to the password
            WorkPassword = WorkPassword + Year(Date.Now)
            ' multipy by the month 
            WorkPassword = WorkPassword * Month(Date.Now)
            ' sub the day from the password
            WorkPassword = WorkPassword - Microsoft.VisualBasic.DateAndTime.Day(Date.Now)
            ' sub the store number
            WorkPassword = WorkPassword - IntStore
            ' add the Number
            WorkPassword = WorkPassword + CInt(Number)

            Select Case Type
                Case 1
                    'do nothing 
                Case 2
                    ' sub the store number
                    WorkPassword = WorkPassword - IntStore
                    ' add the Number
                    WorkPassword = WorkPassword + CInt(Number)
            End Select

            'convert to a string 
            tempPassword = WorkPassword.ToString.PadLeft(10, "0"c)
            CalcPassword = Mid(tempPassword, 6, 5)

            If Password = CalcPassword Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If

    End Function 'CheckPassWord

    Private Sub TxtComment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtComment.KeyDown

        DisplayStatus()

        If e.KeyValue = Keys.Enter Then

            'ssGrid1.Focus()
            If Trim(txtFromStore.Text) = "183" Then
                validateComment()
                If validationError = True Then
                    TxtComment.Focus()
                Else
                    checkStore()
                End If
            Else
                checkStore()
            End If

        End If

    End Sub 'TxtComment_KeyDown

    Private Sub TxtComment_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtComment.Leave
        If ActiveControl IsNot Nothing And ActiveControl.Name <> "cboStore" And ActiveControl.Name <> "btnReset" And ActiveControl.Name <> "txtFromStore" Then
            DisplayStatus()
            If Trim(txtFromStore.Text) = "183" Then
                validateComment()
                If validationError = True Then
                    TxtComment.Focus()
                Else
                    checkStore()
                End If
            Else
                checkStore()
            End If
        End If
    End Sub 'TxtComment_Leave

    Private Sub checkStore()

        If AllowLines() = True Then

            GroupBox2.Visible = True
            ssGrid1.Focus()
            ssGrid1_Sheet1.SetActiveCell(ssGrid1_Sheet1.Rows.Count - 1, 0)
            btnStockEnquiry.Visible = True

        Else

            DisplayWarning("Store has not been entered")
            cboStore.SelectedIndex = -1
            validationError = True
            txtFromStore.Focus()
            btnStockEnquiry.Visible = False

        End If

    End Sub

    Private Sub validateComment()

        validationError = False
        DisplayStatus()

        If Not IsNumeric(Trim(TxtComment.Text)) Then
            DisplayWarning("Comment should be numeric")
            validationError = True
        Else
            If Len(Trim(TxtComment.Text)) <> 6 Then
                DisplayWarning("Comment should be six characters long")
                validationError = True
            End If
        End If
    End Sub 'validateComment()

    Private Function validatePartNo(ByVal row As Integer, ByVal col As Integer) As Boolean

        Dim curRow As Integer = 0

        If AllowLines() = True Then

            If Trim(ssGrid1_Sheet1.GetText(selectedrow, 0)) > String.Empty Then
                'btnSave.Visible = False
                boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, ssGrid1_Sheet1.GetText(selectedrow, 0))
                boPart.AddLoadField(boPart.Description)
                boPart.AddLoadField(boPart.NormalSellPrice)
                boPart.AddLoadField(boPart.SupplierNo)
                boPart.AddLoadField(boPart.CatchAll)

                If boPart.LoadMatches.Count < 1 Then

                    DisplayWarning("SKU does not exist")
                    validationError = True
                    ssGrid1_Sheet1.SetValue(row, 0, String.Empty)
                    ssGrid1_Sheet1.SetActiveCell(row, col)

                Else

                    If boPart.CatchAll.Value = True Then
                        DisplayWarning("Invalid Item Type (Dummy).")
                        validationError = True
                        ssGrid1_Sheet1.SetValue(row, 0, String.Empty)
                        ssGrid1_Sheet1.SetActiveCell(row, col)
                    Else
                        ssGrid1_Sheet1.SetActiveCell(row, col)
                        ssGrid1_Sheet1.SetText(row, 4, boPart.Description.Value)
                        ssGrid1_Sheet1.SetText(row, 5, CStr(boPart.NormalSellPrice.Value))
                        ssGrid1_Sheet1.SetText(row, 3, CStr(EDIT_NEW))
                    End If

                End If

                If validationError = False Then

                    CheckForDuplicate(row, col)

                    ' find warehouse for this partnumber 
                    Dim boSupp As New BOPurchases.cSupplierMaster(_Oasys3DB)
                    boSupp.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boSupp.Number, boPart.SupplierNo.Value)
                    boSupp.LoadMatches()

                    '  If Trim(txtFromStore.Text) = "514" Or Trim(txtFromStore.Text) = "524" Or Trim(txtFromStore.Text) = "534" Then
                    If InternalWarehouse(Trim(txtFromStore.Text)) Then

                        If boSupp.BBCSiteNumber.Value <> txtFromStore.Text Then
                            DisplayWarning("Selected item not supplied by this warehouse - Please check ")
                            ssGrid1_Sheet1.SetActiveCell(row, col)
                            ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 0, "")
                            ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 1, "")
                            ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 4, "")
                            ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 5, "")
                        End If

                    End If

                Else

                    If ssGrid1_Sheet1.GetText(selectedrow, 1) > String.Empty Then
                        DisplayWarning("SKU cannot be blank")
                        validationError = True
                        ssGrid1_Sheet1.SetActiveCell(row, col)
                    End If

                End If
            End If

        Else

            DisplayWarning("Store has not been entered")
            validationError = True

        End If

        validatePartNo = validationError

    End Function  'validatePartNo

    Private Sub CheckForDuplicate(ByVal row As Integer, ByVal col As Integer)

        Dim curRow As Integer = 0

        For count As Integer = 0 To ssGrid1_Sheet1.RowCount - 1

            If count <> row Then

                If ssGrid1_Sheet1.GetText(count, 0) = ssGrid1_Sheet1.GetText(row, 0) Then

                    DisplayWarning("Duplicate SKU entered - Maintain")
                    validationError = True
                    ssGrid1_Sheet1.SetActiveCell(row, 0)
                    ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 0, "")
                    ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 1, "")
                    ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 4, "")
                    ssGrid1_Sheet1.SetText(ssGrid1_Sheet1.ActiveRow.Index, 5, "")

                End If

            End If

        Next count

    End Sub

    Private Function validateQty(ByVal row As Integer, ByVal col As Integer) As Boolean

        Dim newRow As Integer = 0

        If AllowLines() = True Then

            If Trim(ssGrid1_Sheet1.GetText(selectedrow, 1)) > String.Empty Then

                Dim MaxQuantity As Integer = boParm.GetParameterInteger(410)
                Dim remainder As Integer

                If Val(ssGrid1_Sheet1.GetText(selectedrow, 1)) <= 0 Then

                    ssGrid1_Sheet1.SetValue(selectedrow, 1, String.Empty)
                    ssGrid1_Sheet1.SetActiveCell(selectedcol, 1)
                    ssGrid1.EditMode = True
                    DisplayWarning("Quantity cannot be zero or negative")
                    validationError = True

                Else

                    boPart.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boPart.SkuNumber, ssGrid1_Sheet1.GetText(selectedrow, 0))
                    boPart.AddLoadField(boPart.Description)
                    boPart.AddLoadField(boPart.StockOnHand)
                    boPart.AddLoadField(boPart.SupplierPackSize)

                    If boPart.LoadMatches.Count > 0 Then

                        If Val(ssGrid1_Sheet1.GetText(selectedrow, 1)) > boPart.StockOnHand.Value Then

                            'DisplayStatus("Quantity is greater than on hand stock", DisplayTypes.IsWarning)

                            If MsgBox("Quantity is greater than on hand stock. Are you sure ?", MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "IBtOut") = MsgBoxResult.No Then
                                ssGrid1_Sheet1.SetActiveCell(row, col)
                                validationError = True
                            End If

                        Else

                            If Val(ssGrid1_Sheet1.GetText(selectedrow, 1)) > MaxQuantity Then

                                'DisplayStatus("Max Quantity exceeded", DisplayTypes.IsWarning)

                                If MsgBox("Quantity seems high. Are you sure ?", MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "IBtOut") = MsgBoxResult.No Then
                                    ssGrid1_Sheet1.SetActiveCell(row, col)
                                    validationError = True
                                End If

                            End If

                        End If

                    Else

                        DisplayWarning("SKU was not found")
                        validationError = True
                        selectedcol = 0

                    End If

                End If

                'If Trim(txtFromStore.Text) = "514" Or Trim(txtFromStore.Text) = "524" Or Trim(txtFromStore.Text) = "534" Then
                If InternalWarehouse(Trim(txtFromStore.Text)) = True Then

                    Dim qty As Integer = CInt(Val(ssGrid1_Sheet1.GetText(selectedrow, 1)))

                    remainder = qty Mod boPart.SupplierPackSize.Value

                    If remainder > 0 Then
                        DisplayWarning("Quantity not a multiple of Pack Size")
                        validationError = True
                        ssGrid1_Sheet1.SetActiveCell(row, col)

                    Else

                        'btnSave.Visible = True
                        ssGrid1_Sheet1.SetActiveCell(row, col)

                        If Val(ssGrid1_Sheet1.GetText(selectedrow, 1)) > 0 Then

                            If selectedrow = ssGrid1_Sheet1.RowCount - 1 Then

                                newRow = selectedrow + 1
                                ssGrid1_Sheet1.AddRows(newRow, 1)
                                ssGrid1_Sheet1.SetText(newRow, 3, CStr(EDIT_NONE))
                                ssGrid1_Sheet1.SetActiveCell(newRow, 0)
                                NewRowCreated = True
                                Renumber()

                            End If

                        End If

                    End If

                Else

                    'btnSave.Visible = True
                    If Val(ssGrid1_Sheet1.GetText(selectedrow, 1)) > 0 And ssGrid1_Sheet1.GetText(selectedrow, 0) <> String.Empty Then

                        If selectedrow = ssGrid1_Sheet1.RowCount - 1 Then

                            newRow = selectedrow + 1
                            ssGrid1_Sheet1.AddRows(newRow, 1)
                            ssGrid1_Sheet1.SetText(newRow, 3, CStr(EDIT_NONE))
                            ssGrid1_Sheet1.SetActiveCell(newRow, 0)
                            NewRowCreated = True
                            Renumber()

                        End If

                    End If

                End If

            Else

                If ssGrid1_Sheet1.GetText(selectedrow, 0) <> String.Empty Then
                    ssGrid1_Sheet1.SetValue(selectedrow, 1, String.Empty)
                    ssGrid1_Sheet1.SetActiveCell(selectedcol, 1)
                    ssGrid1.EditMode = True
                    DisplayWarning("Quantity cannot be zero or negative")
                    validationError = True
                End If

            End If

        Else

            DisplayWarning("Store and comment have not been entered")
            validationError = True

        End If

        If validationError = False Then
            Dim value As Double
            value = CInt(ssGrid1_Sheet1.GetValue(selectedrow, 1)) * CDec(ssGrid1_Sheet1.GetValue(selectedrow, 5))
            ssGrid1_Sheet1.SetValue(selectedrow, 6, value)
        End If

        _blnAlreadyValidating = False

        validateQty = validationError

    End Function 'validateQty

    Private Sub Reset()

        Dim storeNo As Integer

        txtFromStore.Text = String.Empty
        TxtComment.Text = String.Empty
        txtDrlno.Text = String.Empty
        txtFromStore.Text = String.Empty
        _strStoreName = String.Empty
        _strStoreNumber = String.Empty

        rbIncludeInSales.Checked = True
        ssGrid1_Sheet1.Rows.Count = 0
        ssGrid1_Sheet1.AddRows(0, 1)
        ssGrid1_Sheet1.AddRows(1, 1)
        ssGrid1_Sheet1.SetText(0, 3, CStr(EDIT_NONE))
        ssGrid1_Sheet1.SetFormula(0, 5, "SUM(G2:G2)")
        ssGrid1.ActiveSheet.SetRowVisible(0, False)
        ssGrid1_Sheet1.SetText(1, 3, CStr(EDIT_NEW))
        ssGrid1_Sheet1.SetActiveCell(1, 0)
        DisplayStatus()

        ' do this as selectedIndex = -1 will cause an error 
        cboStore.Items.Clear()
        For Each row As BOStore.cStore In lstStore
            If row.StoreID.Value <> boRO.Store.Value Then
                storeNo = CInt(row.StoreID.Value)
                cboStore.Items.Add(storeNo.ToString("##0").PadLeft(3, "0"c) & Space(1) & _IbtAddressDisplayFactory.ReturnAddress(row)) 'row.AddressLine1.Value)
            End If
        Next row

        ssGrid1_Sheet1.SetFormula(0, 6, "SUM(G2:G" & (ssGrid1_Sheet1.Rows.Count - 1).ToString & ")")
        txtIBTTotal.Text = ssGrid1_Sheet1.GetText(0, 6)

        pbSaveProgress.Visible = False
        btnStockEnquiry.Visible = False
        GroupBox2.Visible = False
        cboStore.SelectedIndex = -1
        txtFromStore.Focus()

    End Sub ' btnReset_Click

    Private Function AllowLines() As Boolean
        If txtFromStore.Text <> String.Empty Then
            Return True
        End If
        Return False
    End Function 'AllowLines()

    Private Sub updateProgressBar(ByVal intPercentageDone As Integer) Handles _DRLHeader.PercentageDone
        pbSaveProgress.Increment(intPercentageDone - pbSaveProgress.Value)
    End Sub

    Private Function InternalWarehouse(ByVal Warehouse As String) As Boolean
        If _lstInternalWarehouse.Contains(Warehouse) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ssGrid1_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs) Handles ssGrid1.SelectionChanged
        selectedcol = ssGrid1_Sheet1.ActiveColumnIndex
        selectedrow = ssGrid1_Sheet1.ActiveRowIndex
        rowheaderclick = False
        _Grid1SelectedRow = ssGrid1_Sheet1.ActiveRowIndex
        NewRowCreated = False
    End Sub

    Private Sub CheckSaveButtonStatus()

        btnSave.Visible = False
        For i As Integer = 1 To ssGrid1_Sheet1.RowCount - 1
            If Val(ssGrid1_Sheet1.GetText(i, 1).Trim) > 0 Then
                btnSave.Visible = True
                Exit For
            End If
        Next i
    End Sub

    Private Sub rbIncludeInSales_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rbIncludeInSales.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtComment.Focus()
        End If
    End Sub

    Private Sub rbExcludeFromSales_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rbExcludeFromSales.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtComment.Focus()
        End If
    End Sub
End Class