﻿'<CAMH>
'**********************************************************************************************
'*  Module : frmPassword.vb
'*  Date   : 12/03/09
'*  Author : MikeO
'*  $Archive: \Projects\OasysHW\VB.NET2008\Apps\IbtOut\frmPassword.vb $
'*
'**********************************************************************************************
'*  Summary:
'*  Allows the entry of a five digit password, to be checked by the calling program.
'*
'**********************************************************************************************
'*  $Author: MikeO $ $Date: 12/03/09 12:00 $ $Revision: 0 $
'*  Version number is manually updated in the AssemblyInfo.vb file in this project.
'*
'*  Date     Vers.      Author      Comments
'*  ======== =======    ========    =============================
'*  12/03/08 1.0.0.0    MikeO       Initial application creation.
'*
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program:
'*
'* strStore     - Store Number
'*
'* strDrlNo     - Drl No
'*
'* strMessage   - Message for the user
'*
'**********************************************************************************************
'</CAMH>

Public Class frmPassword

    Private _Cancel As Boolean = False
    Private _Password As String = String.Empty

    Public Property Cancel() As Boolean
        Get
            Cancel = _Cancel
        End Get
        Set(ByVal value As Boolean)
            _Cancel = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Password = _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Private Sub frmPassword_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        txtPassword.Focus()
    End Sub

    Private Sub frmPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyCode
            Case Keys.D0 To Keys.D9 'Numeric Keys allowed
                e.Handled = False
            Case Keys.NumPad0 To Keys.NumPad9 'Numeric Keys allowed
                e.Handled = False
            Case Keys.Back 'Backspace key allowed
                e.Handled = False
            Case Keys.Enter
                btnOK_Click(sender, e)
            Case Keys.Escape
                btnCancel_Click(sender, e)
            Case Else 'Nothing else allowed suppress
                e.SuppressKeyPress = True
        End Select

    End Sub

    Private Sub frmPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Show()
        Application.DoEvents()
    End Sub

    Public Sub myInputBox(ByVal strMessage As String, ByVal strDRLNo As String, ByVal strStoreNo As String)

        'Populate the form with user supplied data

        lblDRLNo.Text = "DRL No: " & strDRLNo
        lblStore.Text = "Store No: " & strStoreNo
        lblInfo.Text = strMessage

        _Password = String.Empty
        txtPassword.Text = String.Empty
        txtPassword.Focus()

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        _Password = txtPassword.Text
        _Cancel = False
        Me.Hide()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        _Password = String.Empty
        _Cancel = True
        Me.Hide()
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged

    End Sub
End Class