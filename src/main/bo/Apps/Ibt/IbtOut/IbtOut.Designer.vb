﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IbtOut
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IbtOut))
        Dim TipAppearance4 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim NumberCellType7 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType8 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType9 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim NumberCellType10 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim CurrencyCellType6 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Dim TipAppearance5 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim NumberCellType11 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim CurrencyCellType7 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Dim CurrencyCellType8 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Dim TipAppearance6 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim NumberCellType12 As FarPoint.Win.Spread.CellType.NumberCellType = New FarPoint.Win.Spread.CellType.NumberCellType()
        Dim CurrencyCellType9 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Dim CurrencyCellType10 As FarPoint.Win.Spread.CellType.CurrencyCellType = New FarPoint.Win.Spread.CellType.CurrencyCellType()
        Me.LblFromStore = New System.Windows.Forms.Label()
        Me.txtFromStore = New System.Windows.Forms.TextBox()
        Me.cboStore = New System.Windows.Forms.ComboBox()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.TxtComment = New System.Windows.Forms.TextBox()
        Me.ssGrid1 = New FarPoint.Win.Spread.FpSpread()
        Me.ssGrid1_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtDrlno = New System.Windows.Forms.Label()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.ssPrintGrid = New FarPoint.Win.Spread.FpSpread()
        Me.ssPrintGrid_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.lblDrlno = New System.Windows.Forms.Label()
        Me.lblIBTotal = New System.Windows.Forms.Label()
        Me.txtIBTTotal = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbExcludeFromSales = New System.Windows.Forms.RadioButton()
        Me.rbIncludeInSales = New System.Windows.Forms.RadioButton()
        Me.pnlWaitMsg = New System.Windows.Forms.Panel()
        Me.lblPleaseWait = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ssSecondPrint = New FarPoint.Win.Spread.FpSpread()
        Me.ssSecondPrint_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.pbSaveProgress = New System.Windows.Forms.ProgressBar()
        Me.btnStockEnquiry = New System.Windows.Forms.Button()
        CType(Me.ssGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssGrid1_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssPrintGrid_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.pnlWaitMsg.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ssSecondPrint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ssSecondPrint_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblFromStore
        '
        Me.LblFromStore.Location = New System.Drawing.Point(6, 19)
        Me.LblFromStore.Name = "LblFromStore"
        Me.LblFromStore.Size = New System.Drawing.Size(70, 20)
        Me.LblFromStore.TabIndex = 0
        Me.LblFromStore.Text = "To Store"
        Me.LblFromStore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblFromStore.UseCompatibleTextRendering = True
        '
        'txtFromStore
        '
        Me.txtFromStore.Location = New System.Drawing.Point(84, 18)
        Me.txtFromStore.MaxLength = 3
        Me.txtFromStore.Name = "txtFromStore"
        Me.txtFromStore.Size = New System.Drawing.Size(75, 20)
        Me.txtFromStore.TabIndex = 1
        '
        'cboStore
        '
        Me.cboStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStore.FormattingEnabled = True
        Me.cboStore.Location = New System.Drawing.Point(165, 18)
        Me.cboStore.Name = "cboStore"
        Me.cboStore.Size = New System.Drawing.Size(336, 21)
        Me.cboStore.TabIndex = 2
        '
        'lblComment
        '
        Me.lblComment.Location = New System.Drawing.Point(6, 45)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(70, 20)
        Me.lblComment.TabIndex = 5
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxtComment
        '
        Me.TxtComment.Location = New System.Drawing.Point(83, 45)
        Me.TxtComment.MaxLength = 75
        Me.TxtComment.Name = "TxtComment"
        Me.TxtComment.Size = New System.Drawing.Size(417, 20)
        Me.TxtComment.TabIndex = 5
        '
        'ssGrid1
        '
        Me.ssGrid1.About = "3.0.2004.2005"
        Me.ssGrid1.AccessibleDescription = "ssGrid1, Sheet1, Row 0, Column 0, "
        Me.ssGrid1.AllowUndo = False
        Me.ssGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ssGrid1.BackColor = System.Drawing.SystemColors.Control
        Me.ssGrid1.EditModeReplace = True
        Me.ssGrid1.FocusRenderer = New FarPoint.Win.Spread.CustomFocusIndicatorRenderer(CType(resources.GetObject("ssGrid1.FocusRenderer"), System.Drawing.Bitmap), 1)
        Me.ssGrid1.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        Me.ssGrid1.Location = New System.Drawing.Point(8, 19)
        Me.ssGrid1.Name = "ssGrid1"
        Me.ssGrid1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssGrid1.SelectionBlockOptions = CType((FarPoint.Win.Spread.SelectionBlockOptions.Cells Or FarPoint.Win.Spread.SelectionBlockOptions.Rows), FarPoint.Win.Spread.SelectionBlockOptions)
        Me.ssGrid1.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssGrid1_Sheet1})
        Me.ssGrid1.Size = New System.Drawing.Size(726, 301)
        Me.ssGrid1.TabIndex = 7
        TipAppearance4.BackColor = System.Drawing.SystemColors.Info
        TipAppearance4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance4.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssGrid1.TextTipAppearance = TipAppearance4
        Me.ssGrid1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.AsNeeded
        '
        'ssGrid1_Sheet1
        '
        Me.ssGrid1_Sheet1.Reset()
        Me.ssGrid1_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssGrid1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssGrid1_Sheet1.ColumnCount = 8
        Me.ssGrid1_Sheet1.RowCount = 1
        Me.ssGrid1_Sheet1.Cells.Get(0, 7).Value = 0
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "SKU Number"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "Quantity"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Space"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "EditCoder"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "Description"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "Price"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Value"
        Me.ssGrid1_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "Total"
        Me.ssGrid1_Sheet1.ColumnHeader.Columns.Default.Resizable = False
        Me.ssGrid1_Sheet1.Columns.Default.Resizable = False
        NumberCellType7.DecimalPlaces = 0
        NumberCellType7.MaximumValue = 999999.0R
        NumberCellType7.MinimumValue = 0.0R
        Me.ssGrid1_Sheet1.Columns.Get(0).CellType = NumberCellType7
        Me.ssGrid1_Sheet1.Columns.Get(0).Label = "SKU Number"
        Me.ssGrid1_Sheet1.Columns.Get(0).Width = 86.0!
        NumberCellType8.DecimalPlaces = 0
        NumberCellType8.MaximumValue = 9999.0R
        NumberCellType8.MinimumValue = -9999.0R
        Me.ssGrid1_Sheet1.Columns.Get(1).CellType = NumberCellType8
        Me.ssGrid1_Sheet1.Columns.Get(1).Label = "Quantity"
        Me.ssGrid1_Sheet1.Columns.Get(1).Width = 69.0!
        Me.ssGrid1_Sheet1.Columns.Get(2).Label = "Space"
        Me.ssGrid1_Sheet1.Columns.Get(2).Visible = False
        Me.ssGrid1_Sheet1.Columns.Get(2).Width = 20.0!
        Me.ssGrid1_Sheet1.Columns.Get(3).Label = "EditCoder"
        Me.ssGrid1_Sheet1.Columns.Get(3).Visible = False
        Me.ssGrid1_Sheet1.Columns.Get(4).Label = "Description"
        Me.ssGrid1_Sheet1.Columns.Get(4).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(4).Width = 300.0!
        Me.ssGrid1_Sheet1.Columns.Get(5).CellType = NumberCellType9
        Me.ssGrid1_Sheet1.Columns.Get(5).Label = "Price"
        Me.ssGrid1_Sheet1.Columns.Get(5).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(5).Width = 80.0!
        Me.ssGrid1_Sheet1.Columns.Get(6).CellType = NumberCellType10
        Me.ssGrid1_Sheet1.Columns.Get(6).Label = "Value"
        Me.ssGrid1_Sheet1.Columns.Get(6).Locked = True
        Me.ssGrid1_Sheet1.Columns.Get(6).Width = 80.0!
        Me.ssGrid1_Sheet1.Columns.Get(7).CellType = CurrencyCellType6
        Me.ssGrid1_Sheet1.Columns.Get(7).Label = "Total"
        Me.ssGrid1_Sheet1.Columns.Get(7).Visible = False
        Me.ssGrid1_Sheet1.RowHeader.Columns.Default.Resizable = True
        Me.ssGrid1_Sheet1.RowHeader.Rows.Default.Resizable = False
        Me.ssGrid1_Sheet1.Rows.Default.Resizable = False
        Me.ssGrid1_Sheet1.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.[Single]
        Me.ssGrid1_Sheet1.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row
        Me.ssGrid1_Sheet1.StartingRowNumber = 0
        Me.ssGrid1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(4, 437)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 39)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "F5 Save"
        Me.btnSave.UseVisualStyleBackColor = True
        Me.btnSave.Visible = False
        '
        'txtDrlno
        '
        Me.txtDrlno.Location = New System.Drawing.Point(86, 68)
        Me.txtDrlno.MinimumSize = New System.Drawing.Size(40, 20)
        Me.txtDrlno.Name = "txtDrlno"
        Me.txtDrlno.Size = New System.Drawing.Size(75, 20)
        Me.txtDrlno.TabIndex = 8
        Me.txtDrlno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'ssPrintGrid
        '
        Me.ssPrintGrid.About = "3.0.2004.2005"
        Me.ssPrintGrid.AccessibleDescription = "ssPrintGrid, Sheet1, Row 0, Column 0, "
        Me.ssPrintGrid.BackColor = System.Drawing.SystemColors.Control
        Me.ssPrintGrid.Location = New System.Drawing.Point(22, 72)
        Me.ssPrintGrid.Name = "ssPrintGrid"
        Me.ssPrintGrid.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssPrintGrid.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssPrintGrid_Sheet1})
        Me.ssPrintGrid.Size = New System.Drawing.Size(200, 100)
        Me.ssPrintGrid.TabIndex = 10
        TipAppearance5.BackColor = System.Drawing.SystemColors.Info
        TipAppearance5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance5.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssPrintGrid.TextTipAppearance = TipAppearance5
        Me.ssPrintGrid.Visible = False
        '
        'ssPrintGrid_Sheet1
        '
        Me.ssPrintGrid_Sheet1.Reset()
        Me.ssPrintGrid_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssPrintGrid_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssPrintGrid_Sheet1.ColumnCount = 7
        Me.ssPrintGrid_Sheet1.ColumnHeader.RowCount = 0
        Me.ssPrintGrid_Sheet1.RowCount = 13
        Me.ssPrintGrid_Sheet1.RowHeader.ColumnCount = 0
        Me.ssPrintGrid_Sheet1.ColumnHeader.Visible = False
        Me.ssPrintGrid_Sheet1.Columns.Get(0).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(0).Width = 100.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(1).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(1).Width = 200.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(2).Width = 140.0!
        Me.ssPrintGrid_Sheet1.Columns.Get(3).Locked = True
        NumberCellType11.DecimalPlaces = 0
        Me.ssPrintGrid_Sheet1.Columns.Get(4).CellType = NumberCellType11
        Me.ssPrintGrid_Sheet1.Columns.Get(4).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(4).Width = 67.0!
        CurrencyCellType7.DecimalPlaces = 2
        Me.ssPrintGrid_Sheet1.Columns.Get(5).CellType = CurrencyCellType7
        Me.ssPrintGrid_Sheet1.Columns.Get(5).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(5).Width = 75.0!
        CurrencyCellType8.DecimalPlaces = 2
        Me.ssPrintGrid_Sheet1.Columns.Get(6).CellType = CurrencyCellType8
        Me.ssPrintGrid_Sheet1.Columns.Get(6).Locked = True
        Me.ssPrintGrid_Sheet1.Columns.Get(6).Width = 80.0!
        Me.ssPrintGrid_Sheet1.HorizontalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssPrintGrid_Sheet1.PrintInfo.Header = ""
        Me.ssPrintGrid_Sheet1.PrintInfo.Margin.Left = 50
        Me.ssPrintGrid_Sheet1.PrintInfo.ShowBorder = False
        Me.ssPrintGrid_Sheet1.PrintInfo.ShowPrintDialog = True
        Me.ssPrintGrid_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssPrintGrid_Sheet1.RowHeader.Visible = False
        Me.ssPrintGrid_Sheet1.VerticalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssPrintGrid_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'lblDrlno
        '
        Me.lblDrlno.Location = New System.Drawing.Point(6, 68)
        Me.lblDrlno.Name = "lblDrlno"
        Me.lblDrlno.Size = New System.Drawing.Size(69, 20)
        Me.lblDrlno.TabIndex = 7
        Me.lblDrlno.Text = "DRL No"
        Me.lblDrlno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIBTotal
        '
        Me.lblIBTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIBTotal.Location = New System.Drawing.Point(510, 68)
        Me.lblIBTotal.Name = "lblIBTotal"
        Me.lblIBTotal.Size = New System.Drawing.Size(68, 20)
        Me.lblIBTotal.TabIndex = 3
        Me.lblIBTotal.Text = "IBT Total"
        Me.lblIBTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIBTTotal
        '
        Me.txtIBTTotal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIBTTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIBTTotal.Location = New System.Drawing.Point(593, 68)
        Me.txtIBTTotal.Name = "txtIBTTotal"
        Me.txtIBTTotal.Size = New System.Drawing.Size(134, 20)
        Me.txtIBTTotal.TabIndex = 4
        Me.txtIBTTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(662, 437)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(580, 437)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 3
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.pnlWaitMsg)
        Me.GroupBox1.Controls.Add(Me.rbExcludeFromSales)
        Me.GroupBox1.Controls.Add(Me.rbIncludeInSales)
        Me.GroupBox1.Controls.Add(Me.cboStore)
        Me.GroupBox1.Controls.Add(Me.LblFromStore)
        Me.GroupBox1.Controls.Add(Me.txtFromStore)
        Me.GroupBox1.Controls.Add(Me.txtIBTTotal)
        Me.GroupBox1.Controls.Add(Me.lblComment)
        Me.GroupBox1.Controls.Add(Me.lblIBTotal)
        Me.GroupBox1.Controls.Add(Me.TxtComment)
        Me.GroupBox1.Controls.Add(Me.lblDrlno)
        Me.GroupBox1.Controls.Add(Me.txtDrlno)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(733, 96)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Store"
        '
        'rbExcludeFromSales
        '
        Me.rbExcludeFromSales.AutoSize = True
        Me.rbExcludeFromSales.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.rbExcludeFromSales.FlatAppearance.BorderSize = 2
        Me.rbExcludeFromSales.Location = New System.Drawing.Point(513, 47)
        Me.rbExcludeFromSales.Name = "rbExcludeFromSales"
        Me.rbExcludeFromSales.Size = New System.Drawing.Size(133, 17)
        Me.rbExcludeFromSales.TabIndex = 4
        Me.rbExcludeFromSales.TabStop = True
        Me.rbExcludeFromSales.Text = "Exclude IBT from sales"
        Me.rbExcludeFromSales.UseVisualStyleBackColor = True
        '
        'rbIncludeInSales
        '
        Me.rbIncludeInSales.AutoSize = True
        Me.rbIncludeInSales.Checked = True
        Me.rbIncludeInSales.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.rbIncludeInSales.FlatAppearance.BorderSize = 2
        Me.rbIncludeInSales.Location = New System.Drawing.Point(513, 24)
        Me.rbIncludeInSales.Name = "rbIncludeInSales"
        Me.rbIncludeInSales.Size = New System.Drawing.Size(118, 17)
        Me.rbIncludeInSales.TabIndex = 3
        Me.rbIncludeInSales.TabStop = True
        Me.rbIncludeInSales.Text = "Include IBT in sales"
        Me.rbIncludeInSales.UseVisualStyleBackColor = True
        '
        'pnlWaitMsg
        '
        Me.pnlWaitMsg.BackColor = System.Drawing.Color.White
        Me.pnlWaitMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWaitMsg.Controls.Add(Me.lblPleaseWait)
        Me.pnlWaitMsg.Location = New System.Drawing.Point(199, 9)
        Me.pnlWaitMsg.Name = "pnlWaitMsg"
        Me.pnlWaitMsg.Size = New System.Drawing.Size(335, 68)
        Me.pnlWaitMsg.TabIndex = 13
        Me.pnlWaitMsg.Visible = False
        '
        'lblPleaseWait
        '
        Me.lblPleaseWait.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPleaseWait.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPleaseWait.Location = New System.Drawing.Point(15, 22)
        Me.lblPleaseWait.Name = "lblPleaseWait"
        Me.lblPleaseWait.Size = New System.Drawing.Size(301, 24)
        Me.lblPleaseWait.TabIndex = 0
        Me.lblPleaseWait.Text = "Please Wait ..."
        Me.lblPleaseWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.ssSecondPrint)
        Me.GroupBox2.Controls.Add(Me.ssPrintGrid)
        Me.GroupBox2.Controls.Add(Me.ssGrid1)
        Me.GroupBox2.Location = New System.Drawing.Point(4, 105)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(734, 326)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Select SKU Number and Quantity"
        Me.GroupBox2.Visible = False
        '
        'ssSecondPrint
        '
        Me.ssSecondPrint.About = "3.0.2004.2005"
        Me.ssSecondPrint.AccessibleDescription = "ssSecondPrint, Sheet1, Row 0, Column 0, "
        Me.ssSecondPrint.BackColor = System.Drawing.SystemColors.Control
        Me.ssSecondPrint.Location = New System.Drawing.Point(320, 72)
        Me.ssSecondPrint.Name = "ssSecondPrint"
        Me.ssSecondPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ssSecondPrint.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ssSecondPrint_Sheet1})
        Me.ssSecondPrint.Size = New System.Drawing.Size(200, 100)
        Me.ssSecondPrint.TabIndex = 11
        TipAppearance6.BackColor = System.Drawing.SystemColors.Info
        TipAppearance6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance6.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ssSecondPrint.TextTipAppearance = TipAppearance6
        Me.ssSecondPrint.Visible = False
        '
        'ssSecondPrint_Sheet1
        '
        Me.ssSecondPrint_Sheet1.Reset()
        Me.ssSecondPrint_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ssSecondPrint_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ssSecondPrint_Sheet1.ColumnCount = 7
        Me.ssSecondPrint_Sheet1.ColumnHeader.RowCount = 0
        Me.ssSecondPrint_Sheet1.RowCount = 13
        Me.ssSecondPrint_Sheet1.RowHeader.ColumnCount = 0
        Me.ssSecondPrint_Sheet1.ColumnHeader.Visible = False
        Me.ssSecondPrint_Sheet1.Columns.Get(0).Locked = True
        Me.ssSecondPrint_Sheet1.Columns.Get(0).Width = 100.0!
        Me.ssSecondPrint_Sheet1.Columns.Get(1).Locked = True
        Me.ssSecondPrint_Sheet1.Columns.Get(1).Width = 200.0!
        Me.ssSecondPrint_Sheet1.Columns.Get(2).Width = 140.0!
        Me.ssSecondPrint_Sheet1.Columns.Get(3).Locked = True
        NumberCellType12.DecimalPlaces = 0
        Me.ssSecondPrint_Sheet1.Columns.Get(4).CellType = NumberCellType12
        Me.ssSecondPrint_Sheet1.Columns.Get(4).Locked = True
        Me.ssSecondPrint_Sheet1.Columns.Get(4).Width = 67.0!
        CurrencyCellType9.DecimalPlaces = 2
        Me.ssSecondPrint_Sheet1.Columns.Get(5).CellType = CurrencyCellType9
        Me.ssSecondPrint_Sheet1.Columns.Get(5).Locked = True
        Me.ssSecondPrint_Sheet1.Columns.Get(5).Width = 75.0!
        CurrencyCellType10.DecimalPlaces = 2
        Me.ssSecondPrint_Sheet1.Columns.Get(6).CellType = CurrencyCellType10
        Me.ssSecondPrint_Sheet1.Columns.Get(6).Locked = True
        Me.ssSecondPrint_Sheet1.Columns.Get(6).Width = 80.0!
        Me.ssSecondPrint_Sheet1.HorizontalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssSecondPrint_Sheet1.PrintInfo.Header = ""
        Me.ssSecondPrint_Sheet1.PrintInfo.Margin.Left = 50
        Me.ssSecondPrint_Sheet1.PrintInfo.ShowBorder = False
        Me.ssSecondPrint_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ssSecondPrint_Sheet1.RowHeader.Visible = False
        Me.ssSecondPrint_Sheet1.VerticalGridLine = New FarPoint.Win.Spread.GridLine(FarPoint.Win.Spread.GridLineType.None)
        Me.ssSecondPrint_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        '
        'pbSaveProgress
        '
        Me.pbSaveProgress.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbSaveProgress.Location = New System.Drawing.Point(184, 446)
        Me.pbSaveProgress.Name = "pbSaveProgress"
        Me.pbSaveProgress.Size = New System.Drawing.Size(353, 23)
        Me.pbSaveProgress.TabIndex = 12
        Me.pbSaveProgress.Visible = False
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Location = New System.Drawing.Point(86, 437)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(76, 39)
        Me.btnStockEnquiry.TabIndex = 4
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        Me.btnStockEnquiry.UseVisualStyleBackColor = True
        Me.btnStockEnquiry.Visible = False
        '
        'IbtOut
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pbSaveProgress)
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "IbtOut"
        Me.Size = New System.Drawing.Size(741, 479)
        CType(Me.ssGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssGrid1_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssPrintGrid_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlWaitMsg.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ssSecondPrint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ssSecondPrint_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblFromStore As System.Windows.Forms.Label
    Friend WithEvents txtFromStore As System.Windows.Forms.TextBox
    Friend WithEvents cboStore As System.Windows.Forms.ComboBox
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents TxtComment As System.Windows.Forms.TextBox
    Friend WithEvents ssGrid1 As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssGrid1_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtDrlno As System.Windows.Forms.Label
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents ssPrintGrid As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssPrintGrid_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents lblDrlno As System.Windows.Forms.Label
    Friend WithEvents lblIBTotal As System.Windows.Forms.Label
    Friend WithEvents txtIBTTotal As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnStockEnquiry As System.Windows.Forms.Button
    Friend WithEvents ssSecondPrint As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ssSecondPrint_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents pbSaveProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents rbExcludeFromSales As System.Windows.Forms.RadioButton
    Friend WithEvents rbIncludeInSales As System.Windows.Forms.RadioButton
    Friend WithEvents pnlWaitMsg As System.Windows.Forms.Panel
    Friend WithEvents lblPleaseWait As System.Windows.Forms.Label
End Class
