﻿Imports TpWickes.Library

Public Class SaveIBTFactory
    Inherits RequirementSwitchFactory(Of ISaveIBT)

    Friend _requirementSwitchParameterId As Integer = -1032

    Public Overrides Function ImplementationA() As ISaveIBT
        Return New SaveIBT
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As ISaveIBT

        Return New SaveIBTOldWay
    End Function
End Class
