﻿Imports System.Data
Imports TpWickes.Library
Imports IBTCore

Public Class IbtConsignment
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _BoParm As New BOSystem.cParameter(_Oasys3DB)
    Private _RetOptions As New BOSystem.cRetailOptions(_Oasys3DB)
    Private _Saving As Boolean = False
    Private _Leaving As Boolean = False
    Private _ChangeStore As Boolean = False
    Private _validationError As Boolean = False
    Private _KeyText As String = String.Empty
    Private _CurStore As String = String.Empty
    Private _StoreNumber As String = String.Empty
    Private _SaveKeyText As String = String.Empty
    Private _LeaveStore As Boolean = False
    Private _intNoStoreDigits As Integer = 3
    Private _blnHadError As Boolean = False
    Private _AddressLineFactory As IIbtStoreName

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub ' New

    Protected Overrides Sub DoProcessing()

        Dim boStore As New BOStore.cStore(_Oasys3DB)
        Dim lstStore As List(Of BOStore.cStore)
        Dim storeNo As Integer = 0

        _RetOptions.AddLoadFilter(clsOasys3DB.eOperator.pEquals, _RetOptions.RetailOptionsID, "01")
        _RetOptions.LoadMatches()

        _intNoStoreDigits = _BoParm.GetParameterInteger(147)
        _CurStore = _RetOptions.Store.Value

        boStore.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boStore.CountryCode, _RetOptions.CountryCode.Value)
        boStore.SortBy(boStore.StoreID.ColumnName, clsOasys3DB.eOrderByType.Ascending)
        lstStore = boStore.LoadMatches()
        _AddressLineFactory = (New IbtStoreNameFactory).GetImplementation

        For Each row As BOStore.cStore In lstStore
            storeNo = CInt(row.StoreID.Value)
            If storeNo <> CInt(_CurStore) Then
                If _intNoStoreDigits = 4 Then
                    If _BoParm.GetParameterString(148) = "UK" Then
                        cboStore.Items.Add("8" & storeNo.ToString("##0").PadLeft(3, "0"c) & Space(1) & _AddressLineFactory.ReturnAddress(row)) 'row.AddressLine1.Value)
                    Else
                        cboStore.Items.Add("5" & storeNo.ToString("##0").PadLeft(3, "0"c) & Space(1) & _AddressLineFactory.ReturnAddress(row)) 'row.AddressLine1.Value)
                    End If
                Else
                    cboStore.Items.Add(storeNo.ToString("##0").PadLeft(3, "0"c) & Space(1) & _AddressLineFactory.ReturnAddress(row)) 'row.AddressLine1.Value)
                End If
            End If
        Next

        cboStore.SelectedIndex = -1
        TxtStore.Text = String.Empty
        txtDRLNo.SelectionStart = 1
        BtnAccept.Visible = False
        BtnAccept.Enabled = False
        TxtStore.Focus()

    End Sub 'DoProcessing

    Protected Overrides Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        Dim Result As DialogResult = DialogResult.Ignore
        If BtnAccept.Visible = True Then
            Result = MessageBox.Show("The current consignment will be lost " & vbNewLine & "Do you wish to exit ?", "IBT Consignment", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.Yes Then
                MyBase.Form_Closing(sender, e)
            Else
                e.Cancel = True
            End If 'Result = DialogResult.OK
        Else
            MyBase.Form_Closing(sender, e)
        End If 'btnSave.Visible = True
    End Sub 'Form_Clos

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5
                BtnAccept.PerformClick()
                Exit Sub
            Case Keys.F11
                DisplayStatus()
                ResetCheck()
                Exit Sub
            Case Keys.F12
                btnExit.PerformClick()
                Exit Sub
            Case Else
                e.Handled = False
                MyBase.Form_KeyDown(sender, e)
        End Select

    End Sub

    Private Sub cboStore_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboStore.KeyDown

        If (e.KeyData = Keys.Down Or e.KeyData = Keys.Up) And cboStore.DroppedDown = False Then
            cboStore.DroppedDown = True
        End If

    End Sub

    Private Sub cboStore_SelectedIndexCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStore.SelectionChangeCommitted

        Dim KeysUsed As DialogResult
        Dim intIndex As Integer

        If _validationError = False Then
            DisplayStatus()
        End If

        _StoreNumber = TxtStore.Text
        _SaveKeyText = _KeyText
        If _ChangeStore = False Then
            If cboStore.Text > String.Empty Then
                Dim position As Integer = 0
                position = InStr(cboStore.Text, " ")
                TxtStore.Text = Mid(cboStore.Text, 1, position)
                If TxtStore.Text <> _StoreNumber And _StoreNumber <> String.Empty Then
                    KeysUsed = MessageBox.Show("Would you like to change the store number ", "IBT Consignment", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                    If KeysUsed = DialogResult.Cancel Then
                        TxtStore.Text = _StoreNumber
                        intIndex = cboStore.FindString(_StoreNumber)
                        cboStore.SelectedIndex = intIndex
                        _KeyText = _SaveKeyText
                    Else
                        BtnAccept.Visible = False
                    End If
                End If
                txtDRLNo.Focus()
            End If 'cboStore.Text > String.Empty
        End If '_blnChangeStore = False
        _StoreNumber = TxtStore.Text

    End Sub 'cboStore_SelectedIndexChanged

    Private Sub BtnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAccept.Click

        Dim BOUser As New BOSecurityProfile.cSystemUsers(_Oasys3DB)
        Dim strStore As String

        Try

            _Saving = True
            If DRLNoCheck(txtDRLNo.Text) = True Then
                'MsgBox("This DRLNo has already been used")
                'DisplayStatus("This DRLNo has already been used", DisplayTypes.None)
            Else
                Dim con As New BOPurchases.cConsignMaster(_Oasys3DB)
                BOUser.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, BOUser.ID, UserId)
                If BOUser.LoadMatches.Count < 1 Then
                    con.EmployeeID.Value = String.Empty
                Else
                    con.EmployeeID.Value = BOUser.EmployeeCode.Value
                End If

                con.PONumber.Value = txtDRLNo.Text
                con.POReleaseNumber.Value = 0
                con.DateConsigned.Value = Now.Date
                If TxtStore.Text.Trim.Length > 3 And _intNoStoreDigits = 3 Then
                    strStore = TxtStore.Text.Substring(TxtStore.Text.Length - 3)
                    TxtStore.Text = strStore
                End If

                con.IBTStoreNumber.Value = TxtStore.Text
                con.IBTIndicator.Value = True
                If con.SaveIfNew Then
                    BtnAccept.Visible = False
                    BtnAccept.Enabled = False
                    MessageBox.Show("Consignment No " & con.Number.Value & " has been saved   ", "IBTConsignment", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    txtDRLNo.Text = String.Empty
                    TxtStore.Text = String.Empty
                    cboStore.SelectedIndex = -1
                    TxtStore.Focus()
                End If

            End If
            _Saving = False
            _blnHadError = False

        Catch ex As Exception
            MessageBox.Show("Cannot insert a duplicate record.", "Duplicate Record", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub 'BtnAccept_Click

    Private Sub TxtStore_Keydown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtStore.KeyDown
        If e.KeyValue >= 48 And e.KeyValue <= 57 Or e.KeyValue >= 96 And e.KeyValue <= 105 Or e.KeyValue = 8 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Then 'process keys 0-9
            'txtFromStore.Text = txtFromStore.Text & ChrW(e.KeyValue)
        Else
            e.SuppressKeyPress = True
        End If
        If e.KeyValue = Keys.Enter Then

            If TxtStore.Text <> String.Empty Then
                Dim storeno As Integer = CInt(Trim(TxtStore.Text))
                TxtStore.Text = storeno.ToString("##0").PadLeft(3, "0"c)
                ValidateStore()
                If _validationError = False Then
                    Dim cborow As Integer = cboStore.FindString(TxtStore.Text)
                    If cborow > -1 Then
                        cboStore.SelectedIndex = -1
                        cboStore.SelectedIndex = cborow
                    End If
                    _ChangeStore = True
                    txtDRLNo.Focus()
                End If
            Else
                TxtStore.Text = String.Empty
                DisplayWarning("Please key a valid Store number")
                'MessageBox.Show("Please key a valid Store number")
                _validationError = True
                TxtStore.Focus()
            End If
        End If
    End Sub 'TxtStore_Keydown

    Private Sub TxtStore_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtStore.Leave

        If ActiveControl IsNot Nothing AndAlso ActiveControl.Name = "TxtDRLNo" Then
            _LeaveStore = True
        End If

        If _LeaveStore = False Then
            _LeaveStore = True
            If TxtStore.Text <> String.Empty Then
                ValidateStore()
            Else
                _KeyText = String.Empty
            End If
            _ChangeStore = True
            cboStore.SelectedIndex = -1
            _ChangeStore = False

        End If
        _LeaveStore = False

    End Sub 'TxtStore_Leave

    Private Sub ValidateStore()

        Dim boStr As New BOStore.cStore(_Oasys3DB)
        Dim storeNo As Integer = 0
        Dim KeysUsed As DialogResult

        _validationError = False
        DisplayStatus()
        _SaveKeyText = _KeyText
        _KeyText = String.Empty
        If TxtStore.Text <> String.Empty Then
            If _intNoStoreDigits = 4 Then
                If TxtStore.Text.Length < 4 Then
                    storeNo = CInt(Val(TxtStore.Text))
                    If _BoParm.GetParameterString(148) = "UK" Then
                        storeNo += 8000
                    Else
                        storeNo += 5000
                    End If
                    TxtStore.Text = storeNo.ToString
                End If
            Else
                If Len(Trim(TxtStore.Text)) < 3 Then
                    TxtStore.Text = storeNo.ToString.PadLeft(3, "0"c)
                End If
                storeNo = CInt(TxtStore.Text)
            End If
            boStr.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boStr.StoreID, TxtStore.Text)

            Dim myDataSet As DataSet = boStr.Oasys3DB.ExecuteSql("Select CountryCode From STRMAS WHERE NUMB = '" & TxtStore.Text & "'")

            If myDataSet.Tables(0).Rows.Count > 0 Then

                If _RetOptions.CountryCode.Value.Trim = myDataSet.Tables(0).Rows(0).Item(0).ToString.Trim Then
                    _KeyText = boStr.StoreID.Value & Space(1) & _AddressLineFactory.ReturnAddress(boStr) 'boStr.AddressLine1.Value

                    If boStr.IsDeleted.Value = True Then
                        DisplayWarning("Deleted Store. Please key a non deleted store")
                        'MessageBox.Show("Deleted Store. Please key a non deleted store")
                        _validationError = True
                    Else
                        storeNo = CInt(TxtStore.Text)
                        If storeNo = CInt(_CurStore) Then
                            DisplayWarning("Resident Store. Please key another store")
                            'MessageBox.Show("Resident Store. Please key another store")
                            _validationError = True
                            TxtStore.Focus()
                        End If
                    End If
                Else
                    TxtStore.Text = String.Empty
                    DisplayWarning("Please key a valid Store number for your country")
                    _validationError = True
                    TxtStore.Focus()
                End If

            Else
                TxtStore.Text = String.Empty
                cboStore.SelectedIndex = -1
                DisplayWarning("Please key a valid Store number")
                _validationError = True
                TxtStore.Focus()
            End If
        End If

        If _validationError = True Then
            BtnAccept.Visible = False
            BtnAccept.Enabled = False
            TxtStore.Focus()
        Else
            txtDRLNo.SelectionStart = 0
            txtDRLNo.Focus()
            'If TxtDRLNo.Text <> String.Empty Then
            '    BtnAccept.Visible = True
            '    BtnAccept.Enabled = True
            'End If
        End If
        If _validationError = False And BtnAccept.Visible = True And TxtStore.Text <> _StoreNumber And TxtStore.Text <> String.Empty Then
            KeysUsed = MessageBox.Show("Would you like to change the store number ", "IBT Consignment", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
            If KeysUsed = DialogResult.Cancel Then
                TxtStore.Text = _StoreNumber
                _KeyText = _SaveKeyText
            Else
                BtnAccept.Visible = False
            End If
        End If
        _StoreNumber = TxtStore.Text

    End Sub ' ValidateStore()

    Private Function DRLNoCheck(ByVal DRLNo As String) As Boolean

        Dim boCon As New BOPurchases.cConsignMaster(_Oasys3DB)
        Dim DRLvalue As Integer = 0

        DisplayStatus()
        _validationError = False
        BtnAccept.Visible = False
        BtnAccept.Enabled = False
        If IsNumeric(DRLNo) Then
            If Len(Trim(DRLNo)) <= 6 Then
                DRLvalue = CInt(DRLNo)
                If DRLvalue = 0 Then
                    DisplayWarning("DRL Number cannot be zero")
                    _validationError = True
                    txtDRLNo.Focus()
                Else
                    DRLNo = DRLvalue.ToString.PadLeft(6, "0"c)
                    txtDRLNo.Text = DRLNo
                    boCon.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boCon.PONumber, DRLNo)
                    boCon.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                    boCon.AddLoadFilter(clsOasys3DB.eOperator.pEquals, boCon.IBTStoreNumber, Trim(TxtStore.Text))
                    If boCon.LoadMatches.Count <> 0 Then
                        If _blnHadError = False Then
                            DisplayWarning("This IBT In consignment has already been recorded.")
                        Else
                            Return False
                        End If
                        _blnHadError = True
                        _validationError = True
                        txtDRLNo.Clear()
                        txtDRLNo.Focus()
                        Return True
                    Else
                        If TxtStore.Text <> String.Empty And _Saving = False Then
                            BtnAccept.Visible = True
                            BtnAccept.Enabled = True
                        End If 'TxtStore.Text <> String.Empty
                        Return False
                    End If 'boCon.LoadMatches.Count <> 0
                End If 'DRLvalue = 0
            Else
                DisplayWarning("DRL Number cannot be longer than 6 characters")
                _validationError = True
                txtDRLNo.Focus()
                Return True
            End If 'Len < 6

        Else
            DisplayWarning("DRL Number is not numeric")
            _validationError = True
            SendKeys.Send("{HOME}")
            txtDRLNo.Focus()
            Return True
        End If 'IsNumeric
    End Function

    Private Sub TxtDRLNo_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDRLNo.Leave
        If _Leaving = False Then
            BtnAccept.Visible = False
            If txtDRLNo.Text > String.Empty Then
                If TxtStore.Text > String.Empty Then
                    If DRLNoCheck(txtDRLNo.Text) = True Then
                        ResetCheck()
                    Else
                        _Leaving = True
                        'BtnAccept.Focus()
                    End If 'DRLNoCheck(TxtDRLNo.Text) = True
                Else
                    DisplayWarning("Store number can not be blank")
                    TxtStore.Focus()
                End If 'TxtStore.Text > String.Empty
            End If 'TxtDRLNo.Text <> String.Empty
        End If ' _blnLeaving = False
        _Leaving = False
    End Sub 'TxtDRLNo_Leave

    Private Sub TxtDRLNo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDRLNo.KeyDown
        'process keys 0-9                           and the cursor keys                                                        and enter          and numeric key pad  
        If e.KeyValue >= 48 And e.KeyValue <= 57 Or e.KeyValue = 8 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Or e.KeyValue = 13 Or e.KeyValue >= 96 And e.KeyValue <= 105 Then
            'txtFromStore.Text = txtFromStore.Text & ChrW(e.KeyValue)
            _blnHadError = False
        Else
            e.SuppressKeyPress = True
        End If

        If e.KeyValue = Keys.Enter Then
            _Leaving = False
            If DRLNoCheck(txtDRLNo.Text) = True Then
                'DisplayStatus("This DRLNo has already been used", DisplayTypes.None)
            Else
                If _validationError = False Then
                    DisplayStatus()
                    If TxtStore.Text > String.Empty Then
                        BtnAccept.Visible = True
                        BtnAccept.Enabled = True
                        _Leaving = True
                        BtnAccept.Focus()
                    Else
                        DisplayWarning("Store Number is blank")
                    End If 'TxtStore.Text > String.Empty
                End If ' validationError = False
            End If 'DRLNoCheck(TxtDRLNo.Text)
        End If 'e.KeyValue = Keys.Enter
    End Sub  'TxtDRLNo_KeyDown

    Private Sub ResetCheck()
        Dim Result As DialogResult = DialogResult.Ignore
        If BtnAccept.Visible = True Then
            Result = MessageBox.Show("The current consignment will be lost " & vbNewLine & "Do you wish to continue resetting ?", "IBT Consignment", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.Yes Then
                Reset()

            End If 'Result = DialogResult.OK
        Else
            Reset()
        End If 'btnSave.Visible = True
    End Sub

    Private Sub Reset()
        _blnHadError = False
        cboStore.SelectedIndex = -1
        TxtStore.Text = String.Empty
        txtDRLNo.Text = String.Empty
        BtnAccept.Visible = False
        BtnAccept.Enabled = False
        TxtStore.Focus()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        DisplayStatus()
        ResetCheck()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub


    Private Sub cboStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStore.SelectedIndexChanged

    End Sub

    Private Sub btnExit_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Enter

    End Sub

    Private Sub btnExit_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.GotFocus

    End Sub

    Private Sub txtDRLNo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDRLNo.LostFocus
    End Sub

    Private Sub txtDRLNo_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDRLNo.MouseLeave

    End Sub

End Class
