﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IbtConsignment
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IbtConsignment))
        Me.LblFromStore = New System.Windows.Forms.Label
        Me.TxtStore = New System.Windows.Forms.TextBox
        Me.cboStore = New System.Windows.Forms.ComboBox
        Me.lblDrlno = New System.Windows.Forms.Label
        Me.BtnAccept = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtDRLNo = New System.Windows.Forms.TextBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblFromStore
        '
        Me.LblFromStore.AutoSize = True
        Me.LblFromStore.Location = New System.Drawing.Point(6, 25)
        Me.LblFromStore.Name = "LblFromStore"
        Me.LblFromStore.Size = New System.Drawing.Size(58, 13)
        Me.LblFromStore.TabIndex = 0
        Me.LblFromStore.Text = "From Store"
        '
        'TxtStore
        '
        Me.TxtStore.Location = New System.Drawing.Point(82, 22)
        Me.TxtStore.MaxLength = 3
        Me.TxtStore.Name = "TxtStore"
        Me.TxtStore.Size = New System.Drawing.Size(100, 20)
        Me.TxtStore.TabIndex = 1
        '
        'cboStore
        '
        Me.cboStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStore.FormattingEnabled = True
        Me.cboStore.Location = New System.Drawing.Point(188, 22)
        Me.cboStore.Name = "cboStore"
        Me.cboStore.Size = New System.Drawing.Size(188, 21)
        Me.cboStore.TabIndex = 2
        '
        'lblDrlno
        '
        Me.lblDrlno.AutoSize = True
        Me.lblDrlno.Location = New System.Drawing.Point(6, 51)
        Me.lblDrlno.Name = "lblDrlno"
        Me.lblDrlno.Size = New System.Drawing.Size(69, 13)
        Me.lblDrlno.TabIndex = 3
        Me.lblDrlno.Text = "DRL Number"
        '
        'BtnAccept
        '
        Me.BtnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnAccept.Location = New System.Drawing.Point(3, 441)
        Me.BtnAccept.Name = "BtnAccept"
        Me.BtnAccept.Size = New System.Drawing.Size(76, 39)
        Me.BtnAccept.TabIndex = 5
        Me.BtnAccept.Text = "F5 Accept"
        Me.BtnAccept.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(666, 441)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(584, 441)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 7
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtDRLNo)
        Me.GroupBox1.Controls.Add(Me.LblFromStore)
        Me.GroupBox1.Controls.Add(Me.TxtStore)
        Me.GroupBox1.Controls.Add(Me.lblDrlno)
        Me.GroupBox1.Controls.Add(Me.cboStore)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(739, 80)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enter Required Information"
        '
        'txtDRLNo
        '
        Me.txtDRLNo.Location = New System.Drawing.Point(81, 51)
        Me.txtDRLNo.MaxLength = 6
        Me.txtDRLNo.Name = "txtDRLNo"
        Me.txtDRLNo.Size = New System.Drawing.Size(100, 20)
        Me.txtDRLNo.TabIndex = 7
        '
        'IbtConsignment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.BtnAccept)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "IbtConsignment"
        Me.Size = New System.Drawing.Size(745, 483)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblFromStore As System.Windows.Forms.Label
    Friend WithEvents TxtStore As System.Windows.Forms.TextBox
    Friend WithEvents cboStore As System.Windows.Forms.ComboBox
    Friend WithEvents lblDrlno As System.Windows.Forms.Label
    Friend WithEvents BtnAccept As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDRLNo As System.Windows.Forms.TextBox
End Class
