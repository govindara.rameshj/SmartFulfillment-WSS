﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Runtime.CompilerServices
Imports System.String

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("BarCodeGenerator")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Travis Perkins PLC")> 
<Assembly: AssemblyProduct("BarCodeGenerator")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " Travis Perkins PLC 2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("cf982358-e39e-41e4-9b2c-e88ec4ded677")> 

<Assembly: InternalsVisibleTo("BarCodeGenerator.UnitTest")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
