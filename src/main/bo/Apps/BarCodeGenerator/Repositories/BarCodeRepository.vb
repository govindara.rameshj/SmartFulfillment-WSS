﻿Imports Cts.Oasys.Data

Public Class BarCodeRepository

    Public Function GetRecordsNotInEanmas() As System.Data.DataTable

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetEanmasNonExistingRecords
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function

    Public Sub InsertEanmasRecords(ByVal skuNumber As String, ByVal barcode As String)

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.UpdateEanmas
                com.AddParameter("@SkuNumber", skuNumber, SqlDbType.Char)
                com.AddParameter("@BarCode", barcode, SqlDbType.Char)
                com.ExecuteNonQuery()

            End Using
        End Using
    End Sub

End Class
