﻿Public Class BarCodeEanmas
    Implements IBarCodeEanmas

    Friend _skuNumber As String
    Friend _barcode As String


    Public Property SkuNumber() As String Implements IBarCodeEanmas.SkuNumber
        Get
            Return _skuNumber

        End Get
        Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property

    Public Property BarCode() As String Implements IBarCodeEanmas.BarCode
        Get
            Return _barcode

        End Get
        Set(ByVal value As String)
            _barcode = value
        End Set
    End Property

    Public Sub New(ByVal skuNumber As String, _
         ByVal barCode As String)

        Me.SkuNumber = skuNumber
        Me.BarCode = barCode
    End Sub


End Class
