﻿Imports System.IO

Public Class BarCodeGenerator
    Implements IBarCodeGenerator

    Private Const barcodeStartString As String = "000000002"
    Private _eanmasTable As DataTable
    Private _barcodeRepositoryInstance As BarCodeRepository = New BarCodeRepository


    Public Property EanmasDataTable() As DataTable Implements IBarCodeGenerator.EanmasDataTable
        Get
            Return _eanmasTable
        End Get
        Set(ByVal value As DataTable)
            _eanmasTable = value
        End Set
    End Property


    Friend Function GetOddPositionInNumberTotal(ByVal InputValue As String) As Integer

        Dim i As Integer
        Dim TotalOdd As Integer
        InputValue = Trim(InputValue)

        'get sum of odd number positions in the number
        For i = 1 To Len(InputValue) Step 2
            TotalOdd = TotalOdd + CInt(Mid(InputValue, i, 1))
        Next i
        Return TotalOdd

    End Function

    Friend Function GetNumberMultipliedByThree(ByVal InputValue As Integer) As Integer
        Return 3 * InputValue
    End Function

    Friend Function GetSumOfOddNumberTotalAndEvenNumberTotal(ByVal oddNumberTotal As Integer, ByVal evenNumberTotal As Integer) As Integer
        Return oddNumberTotal + evenNumberTotal
    End Function

    Friend Function CheckDigitCalculation(ByVal InputValue As Integer) As Integer
        Return (10 - CInt(IIf(CInt(Right(CStr(InputValue), 1)) = 0, 10, Right(CStr(InputValue), 1))))
    End Function

    Friend Function GetEvenPositionInNumberTotal(ByVal InputValue As String) As Integer
        Dim i As Integer
        Dim TotalEven As Integer
        InputValue = Trim(InputValue)

        'get sum of even number positions in the number
        i = 0
        For i = 2 To Len(InputValue) Step 2
            TotalEven = TotalEven + CInt(Mid(InputValue, i, 1))
        Next i

        Return TotalEven

    End Function

    Friend Function GetCheckDigit(ByVal InputValue As String) As Integer
        Return CheckDigitCalculation(GetSumOfOddNumberTotalAndEvenNumberTotal _
                                    (GetNumberMultipliedByThree(GetOddPositionInNumberTotal(InputValue)), _
                                     GetEvenPositionInNumberTotal(InputValue)))
    End Function

    Friend Function GetBarcode(ByVal skuNumber As String) As String

        Return barcodeStartString + skuNumber + CStr(GetCheckDigit("2" + skuNumber))

    End Function

    Friend Overridable Sub DeleteTaskCompFileIfExists()
        'Kill the TASKCOMP file if it exists
        With My.Computer.FileSystem
            If File.Exists(.CurrentDirectory & "\TASKCOMP") = True Then
                File.Delete(.CurrentDirectory & "\TASKCOMP")
            End If
        End With
    End Sub

    Public Sub GenerateBarCode() Implements IBarCodeGenerator.GenerateBarCode
        Try
            Trace.WriteLine("GenerateBarCode : Started")
            Console.WriteLine("GenerateBarCode : Started")
            LoadDataTobeInsertedtoEanmas()
            CreateBarCodeAndSaveData()
        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
        End Try
    End Sub

    Friend Overridable Sub LoadDataTobeInsertedtoEanmas()
        If EanmasDataTable Is Nothing Then
            EanmasDataTable = _barcodeRepositoryInstance.GetRecordsNotInEanmas
        End If
    End Sub

    Friend Overridable Sub CreateBarCodeAndSaveData()
        Try
            Dim eanMasInstance As BarCodeEanmas() = GetEanmasData()

            If Not eanMasInstance Is Nothing Then

                For Each record In eanMasInstance
                    Trace.WriteLine("GenerateBarCode : Processing Sku : " & record.SkuNumber)
                    Console.WriteLine("GenerateBarCode : Processing Sku : " & record.SkuNumber)
                    record.BarCode = GetBarcode(record.SkuNumber)
                    Trace.WriteLine("GenerateBarCode : BarCode for Sku : " & record.SkuNumber & " :  " & record.BarCode)
                    Console.WriteLine("GenerateBarCode : BarCode for Sku : " & record.SkuNumber & " :  " & record.BarCode)
                    _barcodeRepositoryInstance.InsertEanmasRecords(record.SkuNumber.Trim, record.BarCode.Trim)
                Next

                Trace.WriteLine("GenerateBarCode : Total Number of Skus Processed : " & eanMasInstance.Count)
                Console.WriteLine("GenerateBarCode : Total Number of Skus Processed : " & eanMasInstance.Count)
            Else
                Trace.WriteLine("GenerateBarCode : No Records to be Processed")
                Console.WriteLine("GenerateBarCode : No Records to be Processed")
            End If
        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
        End Try

    End Sub

    Friend Overridable Function GetEanmasData() As BarCodeEanmas()
        If Not EanmasDataTable Is Nothing Then
            Dim eanMasInstance(EanmasDataTable.Rows.Count - 1) As BarCodeEanmas
            For i = 0 To EanmasDataTable.Rows.Count - 1
                eanMasInstance(i) = New BarCodeEanmas(CStr(EanmasDataTable.Rows(i)(0)), CStr(EanmasDataTable.Rows(i)(1)))
            Next
            Return eanMasInstance
        End If
        Return Nothing
    End Function

End Class
