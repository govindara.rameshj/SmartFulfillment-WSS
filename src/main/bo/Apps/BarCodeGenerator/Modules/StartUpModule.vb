﻿Imports System.IO
Imports Cts.Oasys.Core.Tests

Module StartUpModule

    Private Const extractDetailsStarted As String = "Generating Barcodes for Missing Skus In EANMAS Started"
    Private Const underline As String = "======================================================================"
    Private Const extractDetailsCompleted As String = "Generating Barcodes for Missing Skus In EANMAS Completed"
    Private barcodeInstance As BarCodeGenerator = New BarCodeGenerator

    Sub Main()

        TestEnvironmentSetup.SetupIfTestRun()

        Console.WriteLine(extractDetailsStarted)
        Console.WriteLine(underline)
        barcodeInstance.GenerateBarCode()
        Console.WriteLine(extractDetailsCompleted)
        barcodeInstance.DeleteTaskCompFileIfExists()
    End Sub

End Module
