﻿Module StartUpModule

    Private Const extractDetailsStarted As String = "Stock Details Extract Started"
    Private Const underline As String = "============================="
    Private Const extractDetailsCompleted As String = "Stock Details Extract Completed"


    Sub Main()
        Console.WriteLine(extractDetailsStarted)
        Console.WriteLine(underline)
        StockDetailsExtractFactory.FactoryGet.ExtractStockDetails()
        Console.WriteLine(extractDetailsCompleted)
    End Sub

End Module
