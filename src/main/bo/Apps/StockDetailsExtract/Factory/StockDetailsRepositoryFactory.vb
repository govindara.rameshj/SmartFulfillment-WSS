﻿Public Class StockDetailsRepositoryFactory

    Private Shared m_FactoryMember As IStockDetailsRepository

    Public Shared Function FactoryGet() As IStockDetailsRepository

        If m_FactoryMember Is Nothing Then

            Return New StockDetailsRepository                'live implementation

        Else

            Return m_FactoryMember                        'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockDetailsRepository)

        m_FactoryMember = obj

    End Sub
End Class
