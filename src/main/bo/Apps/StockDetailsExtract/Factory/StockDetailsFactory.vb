﻿Public Class StockDetailsFactory

    Private Shared m_FactoryMember As IStockDetails

    Public Shared Function FactoryGet(ByVal skuNumber As String, ByVal quantity As Integer) As IStockDetails

        If m_FactoryMember Is Nothing Then

            Return New StockDetails(skuNumber, quantity)               'live implementation

        Else

            Return m_FactoryMember                        'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockDetails)

        m_FactoryMember = obj

    End Sub

End Class
