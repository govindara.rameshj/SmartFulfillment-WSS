﻿Public Class StockDetailsExtractFactory

    Private Shared m_FactoryMember As IStockDetailsExtract

    Public Shared Function FactoryGet() As IStockDetailsExtract

        If m_FactoryMember Is Nothing Then

            Return New StockDetailsExtract                 'live implementation

        Else

            Return m_FactoryMember                        'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockDetailsExtract)

        m_FactoryMember = obj

    End Sub

End Class
