﻿Public Interface IStockDetailsRepository

    Function GetStockDetails() As System.Data.DataTable
    Function GetXMLFileOutputPath(ByVal parameterId As String) As System.Data.DataTable
    Function GetXMLFileName(ByVal parameterId As String) As System.Data.DataTable

End Interface
