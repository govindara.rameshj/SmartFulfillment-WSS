﻿
Public Interface IStockDetailsExtract

    Sub ExtractStockDetails()
    Sub WriteXml()
    Sub ValidateXMLPath()
    Function GetStockDetails() As StockDetails()

End Interface
