﻿Public Interface IStockDetails

    Property SkuNumber() As String
    Property Quantity() As Integer

End Interface
