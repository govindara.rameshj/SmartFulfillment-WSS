﻿Imports Cts.Oasys.Data

Public Class StockDetailsRepository
    Implements IStockDetailsRepository


    Public Function GetStockDetails() As System.Data.DataTable Implements IStockDetailsRepository.GetStockDetails

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetStockDetails
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetXMLFileOutputPath(ByVal parameterId As String) As System.Data.DataTable Implements IStockDetailsRepository.GetXMLFileOutputPath
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetXMLFileValue
                com.AddParameter("@ParameterId", parameterId, SqlDbType.Char)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetXMLFileName(ByVal parameterId As String) As System.Data.DataTable Implements IStockDetailsRepository.GetXMLFileName
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetXMLFileValue
                com.AddParameter("@ParameterId", parameterId, SqlDbType.Char)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function
End Class
