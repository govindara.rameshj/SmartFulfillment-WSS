﻿
Public Class StockDetails
    Implements IStockDetails

    Friend _skuNumber As String
    Friend _quantity As Integer


    Public Property SkuNumber() As String Implements IStockDetails.SkuNumber
        Get
            Return _skuNumber

        End Get
        Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property

    Public Property Quantity() As Integer Implements IStockDetails.Quantity
        Get
            Return _quantity

        End Get
        Set(ByVal value As Integer)
            _quantity = value
        End Set
    End Property

    Public Sub New(ByVal skuNumber As String, _
         ByVal quantity As Integer)

        Me.SkuNumber = skuNumber
        Me.Quantity = quantity
    End Sub

End Class




