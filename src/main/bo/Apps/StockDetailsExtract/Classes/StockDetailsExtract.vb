﻿Imports System.Xml
Imports System.Math


Public Class StockDetailsExtract
    Implements IStockDetailsExtract

    Friend _stockDataTable As DataTable
    Friend _stockXmlFileOutputPath As String
    Friend _xmlFileName As String
    Friend xmlFileOutputParameter As String = "6002"
    Friend xmlFileNameParameter As String = "6003"

    Public Property StockDataTable() As DataTable
        Get
            Return _stockDataTable
        End Get
        Set(ByVal value As DataTable)
            _stockDataTable = value
        End Set
    End Property

    Public Property XmlFileName() As String
        Get
            Return _xmlFileName
        End Get
        Set(ByVal value As String)
            _xmlFileName = value
        End Set
    End Property
    Public Property StockXmlFileOutputPath() As String
        Get
            Return _stockXmlFileOutputPath
        End Get
        Set(ByVal value As String)
            _stockXmlFileOutputPath = value
        End Set
    End Property

    Private Sub CheckXmlFileOutputPath()
        If StockXmlFileOutputPath Is Nothing Then
            StockXmlFileOutputPath = StockDetailsRepositoryFactory.FactoryGet.GetXMLFileOutputPath(xmlFileOutputParameter).Rows(0)(0).ToString()
        End If
    End Sub
    Private Sub CheckXMLFileName()
        If XmlFileName Is Nothing Then
            XmlFileName = StockDetailsRepositoryFactory.FactoryGet.GetXMLFileOutputPath(xmlFileNameParameter).Rows(0)(0).ToString
        End If
    End Sub
    Private Sub CheckStockDetails()
        If StockDataTable Is Nothing Then
            StockDataTable = StockDetailsRepositoryFactory.FactoryGet.GetStockDetails
        End If
    End Sub
    Public Sub ExtractStockDetails() Implements IStockDetailsExtract.ExtractStockDetails
        Try
            Trace.WriteLine("Extract Stock Details : Started")
            CheckXmlFileOutputPath()
            Trace.WriteLine("XMLFileOutputPath : " & StockXmlFileOutputPath)
            Console.WriteLine("XMLFileOutputPath : " & StockXmlFileOutputPath)
            CheckXMLOutputPath()
            CheckXMLFileName()
            Trace.WriteLine("XMLFileName : " & XmlFileName)
            Console.WriteLine("XMLFileName : " & XmlFileName)
            CheckStockDetails()
            WriteXml()

        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
        End Try
    End Sub

    Private Shared Sub CheckStockQuantity(ByVal writer As XmlWriter, ByVal stock As StockDetails)
        If stock.Quantity < 0 Then
            writer.WriteElementString("QTY", Abs(stock.Quantity).ToString.PadLeft(6, "0"c) + "-")
        Else
            writer.WriteElementString("QTY", stock.Quantity.ToString.PadLeft(6, "0"c))
        End If
    End Sub
    Private Shared Sub WriteStockDetailsToXML(ByVal stkInstance As StockDetails(), ByVal writer As XmlWriter)
        Dim stock As StockDetails
        For Each stock In stkInstance
            Trace.WriteLine("Writing SkuNumber" & Space(2) & stock.SkuNumber _
            & " with quantity" & Space(2) & stock.Quantity & " to the XML file")
            Console.WriteLine("Writing SkuNumber" & Space(2) & stock.SkuNumber _
            & " with quantity" & Space(2) & stock.Quantity & " to the XML file")
            writer.WriteStartElement("ITEM")
            writer.WriteElementString("SKUN", stock.SkuNumber)
            CheckStockQuantity(writer, stock)
            writer.WriteEndElement()
        Next
    End Sub
    Private Sub CreateXmlWriter(ByVal stkInstance As StockDetails(), ByVal settings As XmlWriterSettings)
        Using writer As XmlWriter = XmlWriter.Create(StockXmlFileOutputPath + XmlFileName, settings)
            ' Begin writing.
            writer.WriteStartDocument()
            writer.WriteStartElement("WEBSTOCK") ' Root.

            WriteStockDetailsToXML(stkInstance, writer)
            ' End document.
            writer.WriteEndElement()
            writer.WriteEndDocument()
        End Using
    End Sub
    Public Function GetStockDetails() As StockDetails() Implements IStockDetailsExtract.GetStockDetails
        Trace.WriteLine("Writing Stock Details to an XML File : Started")
        Console.WriteLine("Writing Stock Details to an XML File : Started")
        Dim stkInstance(StockDataTable.Rows.Count - 1) As StockDetails

        For i = 0 To StockDataTable.Rows.Count - 1
            stkInstance(i) = New StockDetails(CStr(StockDataTable.Rows(i)(0)), CInt(StockDataTable.Rows(i)(1)))
        Next
        Return stkInstance
    End Function
    Public Sub WriteXml() Implements IStockDetailsExtract.WriteXml

        Try
            Dim stkInstance As StockDetails() = GetStockDetails()
            ' Create XmlWriterSettings.
            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.Indent = True
            ' Create XmlWriter.
            CreateXmlWriter(stkInstance, settings)
            Trace.WriteLine("Writing Stock Details to an XML File : Completed")
        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
        End Try

    End Sub

    Public Sub CheckXMLOutputPath()
        Try
            Trace.WriteLine("Checking XML Output Path : Started")
            ValidateXMLPath()
            ValidateIfDirectoryExists()
            Trace.WriteLine("Checking XML Output Path : Completed")
        Catch ex As Exception
            Trace.WriteLine(ex.ToString)
        End Try

    End Sub

    Public Sub ValidateXMLPath() Implements IStockDetailsExtract.ValidateXMLPath
        If Not StockXmlFileOutputPath.EndsWith("\") Then
            StockXmlFileOutputPath = StockXmlFileOutputPath + "\"
        End If
    End Sub

    Private Sub ValidateIfDirectoryExists()
        If Not IO.Directory.Exists(StockXmlFileOutputPath) Then
            IO.Directory.CreateDirectory(StockXmlFileOutputPath)
        End If
    End Sub
End Class