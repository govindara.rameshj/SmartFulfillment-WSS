﻿Public Class HOForm

    Sub New()
        InitializeComponent()
    End Sub

    Private Sub HOForm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Dim dt As New DataTable
        dt.Columns.Add("number")
        dt.Columns.Add("name")

        For Each authType As Authorisation.AuthTypes In [Enum].GetValues(GetType(Authorisation.AuthTypes))
            dt.Rows.Add(authType, authType.ToString)
        Next

        cmbType.ValueMember = "number"
        cmbType.DisplayMember = "name"
        cmbType.DataSource = dt

    End Sub

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged

        If cmbType.SelectedIndex > 0 Then
            Dim auth As New Authorisation.HOForm(CType(cmbType.SelectedIndex, Authorisation.AuthTypes))
            auth.ShowDialog(Me)
            auth.Dispose()
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub


End Class
