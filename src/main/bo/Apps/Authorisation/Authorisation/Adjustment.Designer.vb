﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Adjustment
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtContainer = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPO = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtQty = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDepot = New System.Windows.Forms.TextBox
        Me.txtStore = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSku = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblComment = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(3, 79)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(140, 20)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Container Number"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContainer
        '
        Me.txtContainer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtContainer.Location = New System.Drawing.Point(149, 81)
        Me.txtContainer.MaxLength = 9
        Me.txtContainer.Name = "txtContainer"
        Me.txtContainer.Size = New System.Drawing.Size(120, 20)
        Me.txtContainer.TabIndex = 3
        Me.txtContainer.Tag = "Container Number"
        Me.txtContainer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(3, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(140, 20)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Purchase Order"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPO
        '
        Me.txtPO.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPO.Location = New System.Drawing.Point(149, 107)
        Me.txtPO.MaxLength = 6
        Me.txtPO.Name = "txtPO"
        Me.txtPO.Size = New System.Drawing.Size(120, 20)
        Me.txtPO.TabIndex = 4
        Me.txtPO.Tag = "Purchase Order"
        Me.txtPO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(3, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(140, 20)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Quantity Queried"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtQty
        '
        Me.txtQty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQty.Location = New System.Drawing.Point(149, 55)
        Me.txtQty.MaxLength = 7
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(120, 20)
        Me.txtQty.TabIndex = 2
        Me.txtQty.Tag = "Quantity"
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(3, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Assembly Depot Number"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepot
        '
        Me.txtDepot.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDepot.Location = New System.Drawing.Point(149, 133)
        Me.txtDepot.MaxLength = 3
        Me.txtDepot.Name = "txtDepot"
        Me.txtDepot.Size = New System.Drawing.Size(120, 20)
        Me.txtDepot.TabIndex = 5
        Me.txtDepot.Tag = "Depot Number"
        Me.txtDepot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStore
        '
        Me.txtStore.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStore.Location = New System.Drawing.Point(149, 3)
        Me.txtStore.MaxLength = 4
        Me.txtStore.Name = "txtStore"
        Me.txtStore.Size = New System.Drawing.Size(120, 20)
        Me.txtStore.TabIndex = 0
        Me.txtStore.Tag = "Store Number"
        Me.txtStore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 20)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Sku Number"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSku
        '
        Me.txtSku.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSku.Location = New System.Drawing.Point(149, 29)
        Me.txtSku.MaxLength = 6
        Me.txtSku.Name = "txtSku"
        Me.txtSku.Size = New System.Drawing.Size(120, 20)
        Me.txtSku.TabIndex = 1
        Me.txtSku.Tag = "SKU Number"
        Me.txtSku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Store Number"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblComment
        '
        Me.lblComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComment.Location = New System.Drawing.Point(6, 156)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(263, 135)
        Me.lblComment.TabIndex = 27
        '
        'Adjustment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblComment)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtContainer)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtPO)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDepot)
        Me.Controls.Add(Me.txtStore)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSku)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Adjustment"
        Me.Size = New System.Drawing.Size(272, 291)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Public WithEvents txtContainer As System.Windows.Forms.TextBox
    Public WithEvents txtPO As System.Windows.Forms.TextBox
    Public WithEvents txtQty As System.Windows.Forms.TextBox
    Public WithEvents txtDepot As System.Windows.Forms.TextBox
    Public WithEvents txtStore As System.Windows.Forms.TextBox
    Public WithEvents txtSku As System.Windows.Forms.TextBox

End Class
