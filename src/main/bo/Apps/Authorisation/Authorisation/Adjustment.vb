﻿Public Class Adjustment
    Public Event GetPassword()


    Public Sub New()
        InitializeComponent()
    End Sub

    Public Function Password() As String

        ' Verify have no zero values enetered 
        For Each ctl As Control In Me.Controls
            If TypeOf ctl Is TextBox Then
                If CType(ctl, TextBox).Text.Length = 0 Then
                    lblComment.Text = CStr(CType(ctl, TextBox).Tag) & " must have a valid entry"
                    CType(ctl, TextBox).Focus()
                    Return String.Empty
                End If
            End If
        Next

        'This Check needs to be in place as the Password Generator only deals with a three digit store number.
        If txtStore.Text.Length > 3 Then
            txtStore.Text = txtStore.Text.Substring(1)
        End If

        'Password calculation starts here     
        Dim Workpass As Double = CDbl(txtStore.Text) * 12345
        For count As Integer = 1 To 5
            Workpass += CDbl(txtPO.Text)
            Workpass -= CDbl(txtStore.Text)
        Next
        Workpass += (CDbl(txtSku.Text) * 4) - 1
        Workpass += CDbl(txtContainer.Text)
        Workpass -= CDbl(txtDepot.Text)
        Workpass += CDbl(txtQty.Text)
        Workpass -= CDbl(txtSku.Text)

        Dim Workfield2 As Double = Math.Abs((CDbl(txtQty.Text) * CDbl(txtSku.Text)) + Workpass)

        'gets last 6 digits of number and reverses
        Dim lastSixDigits As String = Workfield2.ToString("000000")
        lastSixDigits = lastSixDigits.Substring(lastSixDigits.Length - 6, 6)
        Dim chars() As Char = lastSixDigits.ToCharArray
        Array.Reverse(chars)
        lastSixDigits = chars

        Dim Workfield3 As Double = CDbl(lastSixDigits.Substring(1, 1) & lastSixDigits.Substring(3, 1) & lastSixDigits.Substring(5, 1) & lastSixDigits.Substring(2, 1))
        Workfield3 = Workfield3 * CDbl(txtQty.Text)
        Workfield3 = (Workfield3 * 4) + 1

        Dim numb1 As Integer = CInt(lastSixDigits.Substring(0, 1))
        Dim numb2 As Integer = CInt(lastSixDigits.Substring(1, 1))
        Dim numb3 As Integer = CInt(lastSixDigits.Substring(2, 1))

        Workfield2 = CDbl(lastSixDigits)
        If numb1 <> 0 Then Workfield2 *= numb1
        If numb2 <> 0 Then Workfield2 *= numb2
        If numb3 <> 0 Then Workfield2 *= numb3
        Workfield2 *= CDbl(txtDepot.Text)
        Workfield2 += Workpass

        Workpass = Workfield2 + Workfield3

        Password = Workpass.ToString("00000")
        Return password.Substring(password.Length - 5, 5)

    End Function

    Private Sub txtbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSku.KeyPress, txtStore.KeyPress, txtDepot.KeyPress, txtPO.KeyPress, txtContainer.KeyPress, txtQty.KeyPress

        lblComment.Text = ""

        If (e.KeyChar = ChrW(Keys.Enter)) Then

            'check that text can be converted to decimal
            Dim senderTextBox As TextBox = CType(sender, TextBox)
            If Not IsNumeric(senderTextBox.Text) Then
                lblComment.Text = String.Format("{0} must be a numeric value", senderTextBox.Tag)
                senderTextBox.Focus()
                e.Handled = False
                Exit Sub
            End If

            'we know that is numeric so test that is positive value
            If CInt(senderTextBox.Text) <= 0 Then
                lblComment.Text = String.Format("{0} must be a positive number", senderTextBox.Tag)
                senderTextBox.Focus()
                e.Handled = False
                Exit Sub
            End If


            'check whether all textboxes have values
            Dim nextCtl As Control = GetNextControl(senderTextBox, True)

            Do
                'check if textbox and empty string
                If TypeOf nextCtl Is TextBox Then
                    If CType(nextCtl, TextBox).Text = String.Empty Then
                        nextCtl.Focus()
                        Exit Sub
                    End If
                End If
                nextCtl = GetNextControl(nextCtl, True)

            Loop Until nextCtl Is senderTextBox

            'If here then all boxes are valid so raise password event on calling form
            RaiseEvent GetPassword()
            Exit Sub
        End If

        e.Handled = True
        If (e.KeyChar = "-"c) Then e.Handled = False
        If Char.IsControl(e.KeyChar) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub

End Class
