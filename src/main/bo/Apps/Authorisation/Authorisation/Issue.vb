﻿Public Class Issue
    Public Event GetPassword()

    Public Function Password() As String

        ' Verify have no zero values enetered 
        For Each ctl As Control In Me.Controls
            If TypeOf ctl Is TextBox Then
                If CType(ctl, TextBox).Text.Length = 0 Then
                    lblComment.Text = String.Format("{0} must have a valid entry", CType(ctl, TextBox).Tag)
                    CType(ctl, TextBox).Focus()
                    Return String.Empty
                End If
            End If
        Next

        'Password calculation starts here     
        Dim Workpass As Double = CDbl(txtStore.Text) * 12345
        For count As Integer = 1 To 4
            Workpass += CDbl(txtPO.Text)
            Workpass -= CDbl(txtStore.Text)
        Next

        Password = Workpass.ToString("00000")
        Return password.Substring(password.Length - 5, 5)

    End Function

    Private Sub txtbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStore.KeyPress, txtPO.KeyPress

        lblComment.Text = ""

        If (e.KeyChar = ChrW(Keys.Enter)) Then

            'check that text can be converted to decimal
            Dim senderTextBox As TextBox = CType(sender, TextBox)
            If Not IsNumeric(senderTextBox.Text) Then
                lblComment.Text = String.Format("{0} must be a numeric value", senderTextBox.Tag)
                senderTextBox.Focus()
                e.Handled = False
                Exit Sub
            End If

            'we know that is numeric so test that is positive value
            If CInt(senderTextBox.Text) <= 0 Then
                lblComment.Text = String.Format("{0} must be a positive number", senderTextBox.Tag)
                senderTextBox.Focus()
                e.Handled = False
                Exit Sub
            End If

            Dim tabIndex As Integer = senderTextBox.TabIndex
            For Each ctl As Control In Me.Controls
                If TypeOf ctl Is TextBox Then
                    If ctl.TabIndex = tabIndex + 1 Then
                        ctl.Focus()
                        e.Handled = True
                        Exit Sub
                    End If
                End If
            Next

            'check whether all textboxes have values
            Dim nextCtl As Control = GetNextControl(senderTextBox, True)

            Do
                'check if textbox and empty string
                If TypeOf nextCtl Is TextBox Then
                    If CType(nextCtl, TextBox).Text = String.Empty Then
                        nextCtl.Focus()
                        Exit Sub
                    End If
                End If
                nextCtl = GetNextControl(nextCtl, True)

            Loop Until nextCtl Is senderTextBox

            'If here then all boxes are valid so raise password event on calling form
            RaiseEvent GetPassword()
            Exit Sub
        End If

        e.Handled = True
        If (e.KeyChar = "-"c) Then e.Handled = False
        If Char.IsControl(e.KeyChar) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub


End Class
