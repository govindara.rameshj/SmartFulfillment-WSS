﻿Public Class HOForm
    Private WithEvents _Adjustment As Adjustment = Nothing
    Private WithEvents _Issue As Issue = Nothing

    Public Sub New(ByVal Type As AuthTypes)
        InitializeComponent()

        Select Case Type
            Case AuthTypes.None : Exit Sub
            Case AuthTypes.Adjustment
                _Adjustment = New Adjustment
                AddHandler _Adjustment.GetPassword, AddressOf GetPassword
                Text = _Adjustment.Name
                grpParameters.Controls.Add(_Adjustment)
                _Adjustment.Dock = DockStyle.Fill

            Case AuthTypes.Issue
                _Issue = New Issue
                Text = _Issue.Name
                grpParameters.Controls.Add(_Issue)
                _Issue.Dock = DockStyle.Fill
        End Select

    End Sub

    Private Sub StoreForm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        If grpParameters.Controls.Count > 0 Then
            grpParameters.Controls(0).Focus()
        End If

    End Sub


    Private Sub GetPassword()

        Select Case True
            Case _Adjustment IsNot Nothing
                lblPassword.Text = _Adjustment.Password
                If lblPassword.Text.Length > 0 Then lblPasswordLabel.Visible = True

            Case _Issue IsNot Nothing
                lblPassword.Text = _Issue.Password
                If lblPassword.Text.Length > 0 Then lblPasswordLabel.Visible = True
        End Select

    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        GetPassword()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

End Class