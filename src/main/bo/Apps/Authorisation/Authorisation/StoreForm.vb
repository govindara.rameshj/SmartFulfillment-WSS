﻿Public Class StoreForm
    Private WithEvents _Adjustment As Adjustment = Nothing
    Private WithEvents _Issue As Issue = Nothing

    Public Sub New(ByVal Type As AuthTypes)
        InitializeComponent()

        Select Case Type
            Case AuthTypes.None : Exit Sub
            Case AuthTypes.Adjustment
                _Adjustment = New Adjustment
                AddHandler _Adjustment.GetPassword, AddressOf GetPassword
                Text = _Adjustment.Name
                grpParameters.Controls.Add(_Adjustment)
                _Adjustment.Dock = DockStyle.Fill

            Case AuthTypes.Issue
                _Issue = New Issue
                AddHandler _Issue.GetPassword, AddressOf GetPassword
                Text = _Issue.Name
                grpParameters.Controls.Add(_Issue)
                _Issue.Dock = DockStyle.Fill
                txtPassword.Enabled = True

        End Select

    End Sub

    Public Sub New(ByRef AdjustForm As Adjustment, ByVal PassRequired As Boolean)
        InitializeComponent()

        _Adjustment = AdjustForm
        AddHandler _Adjustment.GetPassword, AddressOf GetPassword

        Text = AdjustForm.Name
        grpParameters.Controls.Add(AdjustForm)
        AdjustForm.Dock = DockStyle.Fill
        pnlPassword.Enabled = PassRequired

    End Sub


    Private Sub StoreForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If lblPassword.Text = My.Resources.PasswordAccepted Then
            DialogResult = Windows.Forms.DialogResult.OK
        Else
            DialogResult = Windows.Forms.DialogResult.Cancel
        End If

    End Sub

    Private Sub StoreForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyData
            Case Keys.F10 : btnExit.PerformClick()
        End Select
    End Sub

    Private Sub StoreForm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        'force focus to next control
        Select Case True
            Case _Adjustment IsNot Nothing : _Adjustment.txtContainer.Focus()
            Case _Issue IsNot Nothing : _Issue.Focus()
        End Select

    End Sub

    Private Sub GetPassword()

        Try
            'check if pnlPassword enabled so make visible
            If pnlPassword.Enabled And pnlPassword.Visible = False Then
                pnlPassword.Visible = True
                txtPassword.Focus()
                Exit Sub
            End If

            If pnlPassword.Visible And txtPassword.Text = String.Empty Then
                txtPassword.Focus()
                Exit Sub
            End If

            Dim password As String = String.Empty
            Select Case True
                Case _Adjustment IsNot Nothing : password = _Adjustment.Password
                Case _Issue IsNot Nothing
                    Dim ctl As Issue = CType(grpParameters.Controls(0), Issue)
                    password = ctl.Password
            End Select

            'check if password non empty
            If password = String.Empty Then
                lblPassword.Text = My.Resources.PasswordRejected
                lblPassword.ForeColor = Color.Red
                Exit Sub
            End If

            'if password not required then accept
            If Not pnlPassword.Visible Then
                lblPassword.Text = My.Resources.PasswordAccepted
                lblPassword.ForeColor = Color.Black
                Close()
                Exit Sub
            End If

            'check if password correct
            If txtPassword.Text = password Then
                lblPassword.Text = My.Resources.PasswordAccepted
                lblPassword.ForeColor = Color.Black
                Close()
            Else
                lblPassword.Text = My.Resources.PasswordRejected
                lblPassword.ForeColor = Color.Red
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            GetPassword()
            e.Handled = True
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

End Class

Public Enum AuthTypes
    None = 0
    Adjustment
    Issue
End Enum