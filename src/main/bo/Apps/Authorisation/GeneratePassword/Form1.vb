﻿Public Class frmServiceDeskPassword

    Private _SD_Number As String = String.Empty
    Private _SD_Password As String = String.Empty

    Private Sub TxtKey_KeyDown(ByVal sender As System.Object, ByVal e As Windows.Forms.KeyEventArgs) Handles TxtKey.KeyDown
        If e.KeyValue >= 48 And e.KeyValue <= 57 Or e.KeyValue >= 96 And e.KeyValue <= 105 Or e.KeyValue = 8 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Then 'process keys 0-9
            'txtFromStore.Text = txtFromStore.Text & ChrW(e.KeyValue)
        Else
            e.SuppressKeyPress = True
        End If
        If e.KeyValue = Keys.Enter Then
            If TxtKey.Text > String.Empty Then
                'calculatePassWord(TxtKey.Text)
                GetServiceDeskPassword()
            End If
        End If
    End Sub

    Public Function GetServiceDeskPassword() As Boolean

        Dim wkRandom As Long = 0
        Dim wkDouble As Double = 0
        Dim lngNumber As Long = 0

        On Error GoTo 0

        If IsNumeric(TxtKey.Text) Then
            wkRandom = CLng(Val(TxtKey.Text.ToString))
            _SD_Number = Format(wkRandom, "0000#")

            wkDouble = CLng(98765432)
            wkDouble = Fix(CInt(wkDouble - wkRandom))

            wkDouble = Fix((wkDouble * 119))
            wkDouble = Fix((wkDouble / 19))
            wkDouble = Fix((wkDouble * 256))
            wkDouble = Fix((wkDouble / 27))
            wkDouble = Fix((wkDouble * 5656))
            wkDouble = Fix((wkDouble / 321))
            wkDouble = Fix((wkDouble + wkRandom))

            _SD_Password = Format(wkDouble)
            _SD_Password = _SD_Password.Substring(_SD_Password.Length - 5)
            _SD_Password = Format(CLng(_SD_Password), "0000#")

            txtPassword.Text = _SD_Password

            Return True
        Else

            MessageBox.Show("Invalid key entered retype.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

    End Function

    Private Function calculatePassWord(ByVal number As Integer) As Boolean

        Dim CalcPassword As String = String.Empty
        Dim tempPassword As String = String.Empty
        Dim IntStore As Integer = 0
        Dim WorkPassword As Integer = 0
        Dim intPassword As Integer = 0

        IntStore = 500 - Date.Now.DayOfYear
        WorkPassword = IntStore * 12345

        ' add the year to the password
        WorkPassword = WorkPassword + Year(Date.Now)
        ' multipy by the month 
        WorkPassword = WorkPassword * Month(Date.Now)
        ' sub the day from the password
        WorkPassword = WorkPassword - Microsoft.VisualBasic.DateAndTime.Day(Date.Now)
        ' sub the store number
        WorkPassword = WorkPassword - IntStore
        ' add the Number
        WorkPassword = WorkPassword + number

        ' sub the store number
        WorkPassword = WorkPassword - IntStore
        ' add the Number
        WorkPassword = WorkPassword + number

        'convert to a string 
        tempPassword = WorkPassword.ToString.PadLeft(10, "0"c)
        txtPassword.Text = Mid(tempPassword, 6, 5)

    End Function 'CheckPassWord

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        GetServiceDeskPassword()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub frmServiceDeskPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

End Class
