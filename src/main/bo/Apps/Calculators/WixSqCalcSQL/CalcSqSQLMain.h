//---------------------------------------------------------------------------

#ifndef CalcSqSQLMainH
#define CalcSqSQLMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TADOConnection *ADOConnection1;
        TADOTable *ADOTable1;
        TDBGrid *DBGrid1;
        TDataSource *DataSource1;
        TADOQuery *ADOQuery1;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TDBText *DBText1;
        TDBText *DBText2;
        TDBText *DBText3;
        TShape *Shape1;
        TButton *Button2;
        TEdit *Output;
        TLabel *Label8;
        TShape *Shape3;
        TShape *Shape4;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TEdit *Length;
        TEdit *Width;
        TButton *Button5;
        TStaticText *StaticText1;
        TStaticText *StaticText2;
        TStaticText *StaticText3;
        TButton *Button6;
        TStaticText *StaticText5;
        TEdit *Volume;
        TButton *Button7;
        TStaticText *StaticText6;
        TMainMenu *MainMenu1;
        TMenuItem *Main1;
        TMenuItem *Calculate1;
        TMenuItem *ClearFields1;
        TMenuItem *Exit1;
        TMenuItem *RunApp11;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TBevel *Bevel3;
        TMenuItem *CalculateProduct1;
        TImage *Image1;
        TStringField *ADOQuery1SKU;
        TStringField *ADOQuery1Description;
        TFloatField *ADOQuery1Coverage;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall DBGrid1Enter(TObject *Sender);
        void __fastcall DBGrid1KeyPress(TObject *Sender, char &Key);
        void __fastcall WidthKeyPress(TObject *Sender, char &Key);
        void __fastcall LengthKeyPress(TObject *Sender, char &Key);
        void __fastcall AreaKeyPress(TObject *Sender, char &Key);
        void __fastcall DepthKeyPress(TObject *Sender, char &Key);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Calculate1Click(TObject *Sender);
        void __fastcall CalculateProduct1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall ClearFields1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
