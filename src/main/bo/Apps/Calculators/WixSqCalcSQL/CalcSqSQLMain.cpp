//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcSqSQLMain.h"
#include "inifiles.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
DataSource1->DataSet = ADOQuery1;
DBGrid1->Update();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
if ((Width->Text == "")|(Length->Text == "")|(Volume->Text == ""))
        {
        ShowMessage("No Values entered for Length, Width or Area");
        }
        else
        {
        if (Width->Text =="")
                {
                double I1 = Volume->Text.ToDouble();
                double I2 = DBText3->Field->Text.ToDouble();
                double O1 = I1 / I2;
                Output->Text = Output->Text.sprintf("%.1f", O1);
                Button5->SetFocus();
                }
                else
                {
                double I1 = Width->Text.ToDouble();
                double I2 = Length->Text.ToDouble();
                double I4 = DBText3->Field->Text.ToDouble();
                double O1 = I1 * I2;
                double O2 = O1 / I4;
                Volume->Text = Volume->Text.sprintf("%.2f", O1);
                Output->Text = Output->Text.sprintf("%.1f", O2);
                Button5->SetFocus();
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormActivate(TObject *Sender)
{
   TIniFile *ini;
   ini = new TIniFile( ChangeFileExt( Application->ExeName, ".INI" ) );
   ADOConnection1->ConnectionString = ini->ReadString("Application", "Connection", "");
   delete ini;

   ADOConnection1->Connected = true;
   //ADOTable1->Active = true;
   ADOQuery1->Active = true;

   Width->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
Volume->Text = "";
Length->Text = "";
Width->Text = "";
Output->Text = "";
Length->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
ADOTable1->Active = false;
ADOConnection1->Connected = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DBGrid1Enter(TObject *Sender)
{
Width->SetFocus();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DBGrid1KeyPress(TObject *Sender, char &Key)
{
if (Key == VK_RETURN)
        {
        Button2->SetFocus();
        }
        else
        {
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::WidthKeyPress(TObject *Sender, char &Key)
{
if (Key == VK_RETURN)
        {
        Length->SetFocus();
        }
        else
        {
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LengthKeyPress(TObject *Sender, char &Key)
{
if (Key == VK_RETURN)
        {
        Button7->SetFocus();
        }
        else
        {
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::AreaKeyPress(TObject *Sender, char &Key)
{
if (Key == VK_RETURN)
        {
        Button2->SetFocus();
        }
        else
        {
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DepthKeyPress(TObject *Sender, char &Key)
{
if (Key == VK_RETURN)
        {
        Button7->SetFocus();
        }
        else
        {
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
                double I1 = Width->Text.ToDouble();
                double I2 = Length->Text.ToDouble();
                double O1 = I1 * I2;
                Volume->Text = Volume->Text.sprintf("%.2f", O1);
                DBGrid1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
Close();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
Volume->Text = "";
Length->Text = "";
Width->Text = "";
Output->Text = "";
Length->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Calculate1Click(TObject *Sender)
{
                double I1 = Width->Text.ToDouble();
                double I2 = Length->Text.ToDouble();
                double O1 = I1 * I2;
                Volume->Text = Volume->Text.sprintf("%.2f", O1);
                DBGrid1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CalculateProduct1Click(TObject *Sender)
{
if ((Width->Text == "")|(Length->Text == "")|(Volume->Text == ""))
        {
        ShowMessage("No Values entered for Length, Width or Depth");
        }
        else
        {
        if (Width->Text =="")
                {
                double I1 = Volume->Text.ToDouble();
                double I2 = DBText3->Field->Text.ToDouble();
                double O1 = I1 / I2;
                Output->Text = Output->Text.sprintf("%.1f", O1);
                Button5->SetFocus();
                }
                else
                {
                double I1 = Width->Text.ToDouble();
                double I2 = Length->Text.ToDouble();
                double I4 = DBText3->Field->Text.ToDouble();
                double O1 = I1 * I2;
                double O2 = O1 / I4;
                Volume->Text = Volume->Text.sprintf("%.2f", O1);
                Output->Text = Output->Text.sprintf("%.1f", O2);
                Button5->SetFocus();
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Exit1Click(TObject *Sender)
{
Close();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ClearFields1Click(TObject *Sender)
{
Volume->Text = "";
Length->Text = "";
Width->Text = "";
Output->Text = "";
Length->SetFocus();
}
//---------------------------------------------------------------------------

