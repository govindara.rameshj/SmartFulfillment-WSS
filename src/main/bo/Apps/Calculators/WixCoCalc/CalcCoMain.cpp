//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcCoMain.h"
#include "math.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCC_Calc_Main *CC_Calc_Main;
//---------------------------------------------------------------------------
__fastcall TCC_Calc_Main::TCC_Calc_Main(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TCC_Calc_Main::Button1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TCC_Calc_Main::Image1Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 2.54;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image2Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.3937;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image3Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.3048;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image4Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 3.281;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image5Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 10.76;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image6Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.0929;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image7Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.8361;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image8Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 1.196;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlue;
Label4->Font->Color = clBlue;
Label1->Font->Color = clBlue;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image2MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlue;
Label3->Font->Color = clBlue;
Label12->Font->Color = clBlue;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image3MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlue;
Label5->Font->Color = clBlue;
Label9->Font->Color = clBlue;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image4MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlue;
Label6->Font->Color = clBlue;
Label7->Font->Color = clBlue;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image5MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlue;
Label15->Font->Color = clBlue;
Label17->Font->Color = clBlue;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image6MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlue;
Label16->Font->Color = clBlue;
Label14->Font->Color = clBlue;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image7MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlue;
Label23->Font->Color = clBlue;
Label19->Font->Color = clBlue;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Image8MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlue;
Label24->Font->Color = clBlue;
Label22->Font->Color = clBlue;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::FormMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TCC_Calc_Main::Button2Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TCC_Calc_Main::Clear1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";        
}
//---------------------------------------------------------------------------

void __fastcall TCC_Calc_Main::Exit1Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

