//---------------------------------------------------------------------------

#ifndef CalcCoMainH
#define CalcCoMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TCC_Calc_Main : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TButton *Button1;
        TLabel *Label2;
        TLabel *Label1;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TImage *Image1;
        TImage *Image2;
        TImage *Image3;
        TImage *Image4;
        TImage *Image5;
        TImage *Image6;
        TImage *Image7;
        TImage *Image8;
        TStaticText *StaticText1;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TStaticText *StaticText2;
        TStaticText *StaticText3;
        TButton *Button2;
        TImage *Image9;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Clear1;
        TMenuItem *Exit1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Image1Click(TObject *Sender);
        void __fastcall Image2Click(TObject *Sender);
        void __fastcall Image3Click(TObject *Sender);
        void __fastcall Image4Click(TObject *Sender);
        void __fastcall Image5Click(TObject *Sender);
        void __fastcall Image6Click(TObject *Sender);
        void __fastcall Image7Click(TObject *Sender);
        void __fastcall Image8Click(TObject *Sender);
        void __fastcall Image1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image2MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image3MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image4MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image5MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image6MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image7MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image8MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Clear1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TCC_Calc_Main(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCC_Calc_Main *CC_Calc_Main;
//---------------------------------------------------------------------------
#endif
