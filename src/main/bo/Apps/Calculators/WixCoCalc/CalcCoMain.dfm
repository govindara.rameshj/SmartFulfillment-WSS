object CC_Calc_Main: TCC_Calc_Main
  Left = 533
  Top = 139
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'WC - CC Module'
  ClientHeight = 564
  ClientWidth = 452
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnMouseMove = FormMouseMove
  PixelsPerInch = 96
  TextHeight = 13
  object Image9: TImage
    Left = 184
    Top = 104
    Width = 81
    Height = 65
    Picture.Data = {
      0A544A504547496D616765A0150000FFD8FFE000104A46494600010101006000
      600000FFE1006645786966000049492A000800000004001A010500010000003E
      0000001B01050001000000460000002801030001000000020000003101020010
      0000004E00000000000000600000000100000060000000010000005061696E74
      2E4E45542076332E333600FFDB00430001010101010101010101010101010101
      0101010101010101010101010101010101010101010101010101010101010101
      01010101010101010101010101010101FFDB0043010101010101010101010101
      0101010101010101010101010101010101010101010101010101010101010101
      010101010101010101010101010101010101010101FFC000110800C800C80301
      2200021101031101FFC4001F0000010501010101010100000000000000000102
      030405060708090A0BFFC400B5100002010303020403050504040000017D0102
      0300041105122131410613516107227114328191A1082342B1C11552D1F02433
      627282090A161718191A25262728292A3435363738393A434445464748494A53
      5455565758595A636465666768696A737475767778797A838485868788898A92
      939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7
      C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FA
      FFC4001F0100030101010101010101010000000000000102030405060708090A
      0BFFC400B5110002010204040304070504040001027700010203110405213106
      1241510761711322328108144291A1B1C109233352F0156272D10A162434E125
      F11718191A262728292A35363738393A434445464748494A535455565758595A
      636465666768696A737475767778797A82838485868788898A92939495969798
      999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4
      D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002
      110311003F00FEFE28A28A0028A28A0028A28A0028A28A0028A2BE7EF8C9FB55
      7ECF1FB3E6A7A368DF1A7E2CF857E1D6A9E21B1B8D4F45B2F10CF750CDA8D85A
      DC0B5B8BAB716F6B700C515C1113162A77F0011CD4CE70A7172A938C22AD794E
      4A3157765772692BBD16BB8D2727649B7D926DFDC8FA068AF863FE1E5FFB07FF
      00D1CE7C36FF00C0CD4BFF0095B47FC3CBFF0060FF00FA39CF86DFF819A97FF2
      B6B1FADE13FE8270FF00F83A9FFF002457B3A9FF003EE7FF0080CBFC8FB9E8AF
      863FE1E5FF00B07FFD1CE7C36FFC0CD4BFF95B47FC3CBFF60FFF00A39CF86DFF
      00819A97FF002B68FADE13FE8270FF00F83A9FFF00241ECEA7FCFB9FFE032FF2
      3EE7A2BC5FE107ED13F04FE3EDA5F5F7C1CF88DE1EF88167A692B7D71A0CD712
      A5AB0758C897CFB78083BD957807935ED15B46709C54A128CE2F69464A517E8D
      369FDE4B4D3B34D35BA6ACD7C98514515420A28A2800A28A2800A28A2800A28A
      2800A28A2800A28A2800A28A2800A28A2800AFE54BFE0E12FF0092CBFB3D7FD9
      32F13FFEA5495FD56D7F2A5FF07097FC965FD9EBFEC99789FF00F52A4AF1F3EF
      F91657FF00151FFD3D03A709FC78FA4BFF004967F3D9451457E7E7AE14514500
      7F537FF040AFF911BE24FF00D7793FF4E16F5FD12D7F3B5FF040AFF911BE24FF
      00D7793FF4E16F5FD12D7E89937FC8BB0FFE17F99E3627F8F53D57FE92828A28
      AF50C028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028
      A28A002BF952FF008384BFE4B2FECF5FF64CBC4FFF00A95257F55B5FCA97FC1C
      25FF002597F67AFF00B265E27FFD4A92BC7CFBFE4595FF00C547FF004F40E9C2
      7F1E3E92FF00D259FCF6514515F9F9EB851451401FD4DFFC102BFE446F893FF5
      DE4FFD385BD7F44B5FCED7FC102BFE446F893FF5DE4FFD385BD7F44B5FA264DF
      F22EC3FF0085FE678D89FE3D4F55FF00A4A0A28A2BD4300A28A2800A28A2800A
      28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AFE54BFE0E12FF92C
      BFB3D7FD932F13FF00EA5495FD56D7F2A5FF0007097FC965FD9EBFEC99789FFF
      0052A4AF1F3EFF0091657FF151FF00D3D03A709FC78FA4BFF4967F3D9451457E
      7E7AE145145007F537FF00040AFF00911BE24FFD7793FF004E16F5FD12D7F3B5
      FF00040AFF00911BE24FFD7793FF004E16F5FD12D7E89937FC8BB0FF00E17F99
      E3627F8F53D57FE92828A28AF50C028A28A0028A28A0028A28A0028A28A0028A
      28A0028A28A0028A2BC3BF694F067C47F1F7C0DF891E19F83FE35D5FE1EFC53B
      AF0EDCDE7C3FF14E8B771595D5A78AB4A68F54D274FB89E7496DD34BD7AE2D17
      41D5E49A1945BE9BA9DCDD4282E6081D26727184A4A2E6E3172508DB9A4D2BF2
      C6F65CCF657695DABB5B8D2BB4AF6BB4AEF65E6FD0F71A2BF82ED47FE0A35FB7
      F691A85FE93AAFED25F15B4ED4F4BBCBAD3B52D3EF2EACEDEF2C6FECA77B6BCB
      3BBB7974C5960B9B6B88A482786455922951D1D4329029FF00C3CAFF006EFF00
      FA39EF89BFF81FA7FF00F2BABE7BFD65C22DF0F895DF4A5F3FF979EBFD3D3B3E
      A353F9E1FF00937F91FDEED7F2A5FF0007097FC965FD9EBFEC99789FFF0052A4
      AFCCFF00F8795FEDDFFF00473DF137FF0003F4FF00FE5757CFFF0018FF00683F
      8D3FB416A7A36B3F1A3E22F88BE22EA9E1EB19F4CD16F7C453C13CFA7585D5C0
      BAB8B5B7682DE00B14B7004CC1831DFC820715C19967787C6E0EA61E9D2AD09C
      DD36A53E4E55C938CDDF966DEA934B4EDF2D6861674AA29B945A49E8AF7D55BA
      A478E514515F3277051451401FD4DFFC102BFE446F893FF5DE4FFD385BD7F44B
      5FE777F08BF6A3FDA0BE02DA5ED8FC1DF8ADE2AF87D69A9316BE83C3F716D025
      CB17590997CEB69C9CBAAB70472057B38FF82957EDDE381FB4F7C4DFFC1869FF
      00FCAEAFA9C067B86C26169509D1AF2953566E0A9F2BD7A5E69FDE91C157093A
      95253528A526AC9DEFB25D1791FDEED15FC117FC3CAFF6EFFF00A39EF89BFF00
      81FA7FFF002BABBEF857FB717FC1467E31FC48F047C2CF047ED23F13B50F15F8
      FBC4BA4F85F44B737F64215BCD56EE3B6FB5DE489A639B7D3B4F89E4BFD4EF19
      4C765A7DB5CDDCA56285D876C78930B2928C70F89729351492A4DB6DA4925ED3
      56DBB2EFA77D337829A4DB9C124AEDFBDA25F23FB9FA2B98F04E817FE15F0778
      5BC33AAF88B56F17EA9A0787F48D2353F166BB22C9ACF89B51D3EC20B6BED7F5
      464C44B7DABDD472EA173140A96F0CB70D0DBC71C091A2F4F5F429B6936ACDA4
      DAD1D9F5575A3B6DA68710514514C028A28A0028A28A0028A28A0028A28A0028
      A28A00FE3ABFE0B6BFB297FC298FDA26DFE37F85F4DFB3F807F681FB6EB1A8FD
      9A2DB69A47C52D3443FF00097DAC9B01587FE128867B3F174124F2096FF55BDF
      1408231069CDB7F152BFBFFF00DBBBF663B0FDADBF666F885F098C36DFF0959B
      31E29F86DA85C18D0699F10FC3B15C5CF87DBED127C9696DAC2C979E17D56E8A
      B98345D7B52923532AC647F019A8E9D7FA46A17FA4EAB6773A76A7A5DE5D69DA
      969F790C96F79637F653BDB5E59DDDBCAAB2C1736D7114904F0C8AB2452A3A3A
      865207C0E7B82FAAE31D482B52C4DEA46CB48D4BFEF61DBE26A697453496C7AD
      84ABCF4F95BF7A164FCE3F65FE9F2F329D14515E29D414514500145145001451
      450015FD21FF00C105FF00652FED1D6BC65FB5D78B74DCDA681F6FF875F097ED
      517126B57B6D1FFC275E2AB4DE15C7F67E95736FE13D3EF21325BDC36B3E2BB3
      7DB71A7FCBFCFD7C2BF86BE2AF8C7F123C11F0B3C1164750F15F8FBC4BA4F85F
      44B73BC42B79AADDC76DF6BBC9115CDBE9DA7C4F25FEA778CA63B2D3EDAE6EE5
      2B142EC3FD09FE037C1BF0AFECFBF077E1DFC19F05C413C3FF000FBC3563A15B
      DC189219F55BE40D73AD6BF7B1C64C6BA8F88B5AB8D435CD4B61F2CDFEA17063
      0136A8FA0E1FC17B7C4BC4CD5E9E1ACE37DA55A5F07FE00AF37DA5C9DCE3C655
      E582A69FBD3DFCA2B7FBDE9E973D6E8A28AFB83CB0A28A2800A28A2800A28A28
      00A28A2800A28A2800A28A2800AFE3ABFE0B6BFB297FC298FDA26DFE37F85F4D
      FB3F807F681FB6EB1A8FD9A2DB69A47C52D3443FF097DAC9B01587FE128867B3
      F174124F2096FF0055BDF1408231069CDB7FB15AF8FBF6EEFD98EC3F6B6FD99B
      E217C2630DB7FC2566CC78A7E1B6A1706341A67C43F0EC57173E1F6FB449F25A
      5B6B0B25E785F55BA2AE60D175ED4A48D4CAB191E6E6B83FAEE0EA538ABD587E
      F68F7E78A7EEFF00DBF1BC3B5DA6F636A153D9548CBECBF765E8FAFC9D9FC8FE
      0028AB9A8E9D7FA46A17FA4EAB6773A76A7A5DE5D69DA969F790C96F79637F65
      3BDB5E59DDDBCAAB2C1736D7114904F0C8AB2452A3A3A8652053AFCE76DCF682
      8A28A0028A28A0028A2BBEF857F0D7C55F18FE247823E167822C8EA1E2BF1F78
      9749F0BE896E778856F355BB8EDBED779222B9B7D3B4F89E4BFD4EF194C765A7
      DB5CDDCA56285D838C5CA4A314DCA4D46296ADB6EC925DDBD1036926DE892BB7
      D923FA05FF00820BFECA5FDA3AD78CBF6BAF16E9B9B4D03EDFF0EBE12FDAA2E2
      4D6AF6DA3FF84EBC5569BC2B8FECFD2AE6DFC27A7DE4264B7B86D67C5766FB6E
      34FF0097FA7DAF24F80DF06FC2BFB3EFC1DF877F067C17104F0FFC3EF0D58E85
      6F70624867D56F9035CEB5AFDEC71931AEA3E22D6AE350D7352D87CB37FA85C1
      8C04DAA3D6EBF4ACBF08B0584A5415B9D2E6AAD7DAAB2D66EFD52D2317FCB147
      895AA3AB5253E8DDA2BB456DFE6FCDB0A28A2BB4C828A28A0028A28A0028A28A
      0028A28A0028A28A0028A28A0028A28A00FE3ABFE0B6BFB297FC298FDA26DFE3
      7F85F4DFB3F807F681FB6EB1A8FD9A2DB69A47C52D3443FF00097DAC9B01587F
      E128867B3F174124F2096FF55BDF1408231069CDB7F152BFBFFF00DBBBF663B0
      FDADBF666F885F098C36DFF0959B31E29F86DA85C18D0699F10FC3B15C5CF87D
      BED127C9696DAC2C979E17D56E8AB98345D7B52923532AC647F019A8E9D7FA46
      A17FA4EAB6773A76A7A5DE5D69DA969F790C96F79637F653BDB5E59DDDBCAAB2
      C1736D7114904F0C8AB2452A3A3A865207C0E7B82FAAE31D482B52C4DEA46CB4
      8D4BFEF61DBE26A697453496C7AD84ABCF4F95BF7A164FCE3F65FE9F2F329D14
      515E29D41451450015FD21FF00C105FF00652FED1D6BC65FB5D78B74DCDA681F
      6FF875F097ED517126B57B6D1FFC275E2AB4DE15C7F67E95736FE13D3EF21325
      BDC36B3E2BB37DB71A7FCBFCFD7C2BF86BE2AF8C7F123C11F0B3C1164750F15F
      8FBC4BA4F85F44B73BC42B79AADDC76DF6BBC9115CDBE9DA7C4F25FEA778CA63
      B2D3EDAE6EE52B142EC3FD09FE037C1BF0AFECFBF077E1DFC19F05C413C3FF00
      0FBC3563A15BDC189219F55BE40D73AD6BF7B1C64C6BA8F88B5AB8D435CD4B61
      F2CDFEA170630136A8FA0E1FC17B7C4BC4CD5E9E1ACE37DA55A5F07FE00AF37D
      A5C9DCE3C655E582A69FBD3DFCA2B7FBDE9E973D6E8A28AFB83CB0A28A2800A2
      8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AFE3ABFE0B
      6BFB297FC298FDA26DFE37F85F4DFB3F807F681FB6EB1A8FD9A2DB69A47C52D3
      443FF097DAC9B01587FE128867B3F174124F2096FF0055BDF1408231069CDB7F
      B15AF8FBF6EEFD98EC3F6B6FD99BE217C2630DB7FC2566CC78A7E1B6A1706341
      A67C43F0EC57173E1F6FB449F25A5B6B0B25E785F55BA2AE60D175ED4A48D4CA
      B191E6E6B83FAEE0EA538ABD587EF68F7E78A7EEFF00DBF1BC3B5DA6F636A153
      D9548CBECBF765E8FAFC9D9FC8FE0028AB9A8E9D7FA46A17FA4EAB6773A76A7A
      5DE5D69DA969F790C96F79637F653BDB5E59DDDBCAAB2C1736D7114904F0C8AB
      2452A3A3A8652053AFCE76DCF6828A2BBEF857F0D7C55F18FE247823E167822C
      8EA1E2BF1F789749F0BE896E778856F355BB8EDBED779222B9B7D3B4F89E4BFD
      4EF194C765A7DB5CDDCA56285D838C5CA4A314DCA4D46296ADB6EC925DDBD103
      6926DE892BB7D923FA05FF00820BFECA5FDA3AD78CBF6BAF16E9B9B4D03EDFF0
      EBE12FDAA2E24D6AF6DA3FF84EBC5569BC2B8FECFD2AE6DFC27A7DE4264B7B86
      D67C5766FB6E34FF0097FA7DAF24F80DF06FC2BFB3EFC1DF877F067C17104F0F
      FC3EF0D58E856F70624867D56F9035CEB5AFDEC71931AEA3E22D6AE350D7352D
      87CB37FA85C18C04DAA3D6EBF4ACBF08B0584A5415B9D2E6AAD7DAAB2D66EFD5
      2D2317FCB147895AA3AB5253E8DDA2BB456DFE6FCDB0A28A2BB4C828A28A0028
      A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A00FE
      3ABFE0B6BFB297FC298FDA26DFE37F85F4DFB3F807F681FB6EB1A8FD9A2DB69A
      47C52D3443FF00097DAC9B01587FE128867B3F174124F2096FF55BDF14082310
      69CDB7F152BFBFFF00DBBBF663B0FDADBF666F885F098C36DFF0959B31E29F86
      DA85C18D0699F10FC3B15C5CF87DBED127C9696DAC2C979E17D56E8AB98345D7
      B52923532AC647F019A8E9D7FA46A17FA4EAB6773A76A7A5DE5D69DA969F790C
      96F79637F653BDB5E59DDDBCAAB2C1736D7114904F0C8AB2452A3A3A865207C0
      E7B82FAAE31D482B52C4DEA46CB48D4BFEF61DBE26A697453496C7AD84ABCF4F
      95BF7A164FCE3F65FE9F2F329D7F487FF0417FD94BFB475AF197ED75E2DD3736
      9A07DBFE1D7C25FB545C49AD5EDB47FF0009D78AAD378571FD9FA55CDBF84F4F
      BC84C96F70DACF8AECDF6DC69FF2FF003F5F0AFE1AF8ABE31FC48F047C2CF045
      91D43C57E3EF12E93E17D12DCEF10ADE6AB771DB7DAEF2445736FA769F13C97F
      A9DE3298ECB4FB6B9BB94AC50BB0FF00427F80DF06FC2BFB3EFC1DF877F067C1
      7104F0FF00C3EF0D58E856F70624867D56F9035CEB5AFDEC71931AEA3E22D6AE
      350D7352D87CB37FA85C18C04DAA36E1FC17B7C4BC4CD5E9E1ACE37DA55A5F07
      FE00AF37DA5C9DC9C655E582A69FBD3DFCA2B7FBDE9E973D6E8A28AFB83CB0A2
      8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A2
      8A2800A28A2800AFE3ABFE0B6BFB297FC298FDA26DFE37F85F4DFB3F807F681F
      B6EB1A8FD9A2DB69A47C52D3443FF097DAC9B01587FE128867B3F174124F2096
      FF0055BDF1408231069CDB7FB15AF91FF6E1FD9734AFDAFF00F672F1B7C1CB99
      AC34FF0011DCA5B788BE1EEBFA8A48D6DE1EF1EE85E6CBA25F4EF0C5713C1617
      F14D7DE1ED6A6B7B6B9B98F42D6B536B5B796E442B5E6E6B82FAEE0EA538A4EA
      C3F7945E9F1C53F76FDA716E1BA5769BD8DA854F655232FB2FDD97A3EBF2767F
      23F0EBFE082FFB297F68EB5E32FDAEBC5BA6E6D340FB7FC3AF84BF6A8B8935AB
      DB68FF00E13AF155A6F0AE3FB3F4AB9B7F09E9F790992DEE1B59F15D9BEDB8D3
      FE5FE9F6BC93E037C1BF0AFECFBF077E1DFC19F05C413C3FF0FBC3563A15BDC1
      89219F55BE40D73AD6BF7B1C64C6BA8F88B5AB8D435CD4B61F2CDFEA17063013
      6A8F5BAD72FC22C161295056E74B9AAB5F6AACB59BBF54B48C5FF2C50AB54756
      A4A7D1BB45768ADBFCDF9B614514576990514514005145140051451400514514
      0051451400514514005145140051451400514514005145140051451400514514
      0051451400514514005145140051451400514514005145140051451400514514
      0051451400514514005145140051451400514514005145140051451400514514
      00514514005145140051451401FFD9}
    Stretch = True
  end
  object Label2: TLabel
    Left = 104
    Top = 216
    Width = 106
    Height = 26
    Caption = 'Centimetres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 248
    Top = 184
    Width = 106
    Height = 26
    Caption = 'Centimetres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 216
    Top = 216
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 216
    Top = 184
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 216
    Top = 248
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 216
    Top = 280
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 248
    Top = 280
    Width = 39
    Height = 26
    Caption = 'Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 104
    Top = 248
    Width = 39
    Height = 26
    Caption = 'Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 248
    Top = 248
    Width = 62
    Height = 26
    Caption = 'Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 104
    Top = 280
    Width = 62
    Height = 26
    Caption = 'Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 104
    Top = 184
    Width = 56
    Height = 26
    Caption = 'Inches'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 248
    Top = 216
    Width = 56
    Height = 26
    Caption = 'Inches'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 104
    Top = 312
    Width = 94
    Height = 26
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 248
    Top = 344
    Width = 94
    Height = 26
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 216
    Top = 312
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label16: TLabel
    Left = 216
    Top = 344
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label17: TLabel
    Left = 248
    Top = 312
    Width = 71
    Height = 26
    Caption = 'Sq. Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label18: TLabel
    Left = 104
    Top = 344
    Width = 71
    Height = 26
    Caption = 'Sq. Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label19: TLabel
    Left = 248
    Top = 376
    Width = 94
    Height = 26
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 104
    Top = 408
    Width = 94
    Height = 26
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 104
    Top = 376
    Width = 79
    Height = 26
    Caption = 'Sq. Yards'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label22: TLabel
    Left = 248
    Top = 408
    Width = 79
    Height = 26
    Caption = 'Sq. Yards'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 216
    Top = 376
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label24: TLabel
    Left = 216
    Top = 408
    Width = 10
    Height = 26
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image1: TImage
    Left = 96
    Top = 184
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image1Click
    OnMouseMove = Image1MouseMove
  end
  object Image2: TImage
    Left = 96
    Top = 216
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image2Click
    OnMouseMove = Image2MouseMove
  end
  object Image3: TImage
    Left = 96
    Top = 248
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image3Click
    OnMouseMove = Image3MouseMove
  end
  object Image4: TImage
    Left = 96
    Top = 280
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image4Click
    OnMouseMove = Image4MouseMove
  end
  object Image5: TImage
    Left = 96
    Top = 312
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image5Click
    OnMouseMove = Image5MouseMove
  end
  object Image6: TImage
    Left = 96
    Top = 344
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image6Click
    OnMouseMove = Image6MouseMove
  end
  object Image7: TImage
    Left = 96
    Top = 376
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image7Click
    OnMouseMove = Image7MouseMove
  end
  object Image8: TImage
    Left = 96
    Top = 408
    Width = 265
    Height = 25
    Cursor = crHandPoint
    OnClick = Image8Click
    OnMouseMove = Image8MouseMove
  end
  object Bevel1: TBevel
    Left = 10
    Top = 32
    Width = 431
    Height = 11
    Shape = bsBottomLine
  end
  object Bevel2: TBevel
    Left = 10
    Top = 496
    Width = 431
    Height = 11
    Shape = bsBottomLine
  end
  object Button1: TButton
    Left = 8
    Top = 520
    Width = 97
    Height = 33
    Caption = 'F3 Clear Fields'
    TabOrder = 2
    OnClick = Button1Click
  end
  object StaticText1: TStaticText
    Left = 162
    Top = 52
    Width = 127
    Height = 23
    Alignment = taCenter
    Caption = 'Measurement'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 3
  end
  object StaticText2: TStaticText
    Left = 202
    Top = 444
    Width = 50
    Height = 23
    Caption = 'Result'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 4
  end
  object StaticText3: TStaticText
    Left = 8
    Top = 8
    Width = 226
    Height = 26
    Caption = 'Calculate : Conversions'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object Button2: TButton
    Left = 352
    Top = 520
    Width = 89
    Height = 33
    Caption = 'F10 Exit'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 160
    Top = 72
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object Edit2: TEdit
    Left = 160
    Top = 464
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object MainMenu1: TMainMenu
    Left = 416
    object File1: TMenuItem
      Caption = 'File'
      Visible = False
      object Clear1: TMenuItem
        Caption = 'Clear'
        ShortCut = 114
        OnClick = Clear1Click
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        ShortCut = 121
        OnClick = Exit1Click
      end
    end
  end
end
