//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcPrMain.h"
#include "math.h"
#include "shellapi.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPC_Calc_Main *PC_Calc_Main;
//---------------------------------------------------------------------------
__fastcall TPC_Calc_Main::TPC_Calc_Main(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Button1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Image1Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 2.54;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image2Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.3937;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image3Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.3048;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image4Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 3.281;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image5Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 10.76;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image6Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.0929;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image7Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 0.8361;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image8Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double O1 = I1 * 1.196;
Edit2->Text = Edit2->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlue;
Label4->Font->Color = clBlue;
Label1->Font->Color = clBlue;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image2MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlue;
Label3->Font->Color = clBlue;
Label12->Font->Color = clBlue;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image3MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlue;
Label5->Font->Color = clBlue;
Label9->Font->Color = clBlue;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image4MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlue;
Label6->Font->Color = clBlue;
Label7->Font->Color = clBlue;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image5MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlue;
Label15->Font->Color = clBlue;
Label17->Font->Color = clBlue;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image6MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlue;
Label16->Font->Color = clBlue;
Label14->Font->Color = clBlue;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image7MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlue;
Label23->Font->Color = clBlue;
Label19->Font->Color = clBlue;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Image8MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlue;
Label24->Font->Color = clBlue;
Label22->Font->Color = clBlue;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::FormMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
//Inches To Centimetres
Label11->Font->Color = clBlack;
Label4->Font->Color = clBlack;
Label1->Font->Color = clBlack;
//Centimetres To Inches
Label2->Font->Color = clBlack;
Label3->Font->Color = clBlack;
Label12->Font->Color = clBlack;
//Feet To Inches
Label8->Font->Color = clBlack;
Label5->Font->Color = clBlack;
Label9->Font->Color = clBlack;
//Metres To Feet
Label10->Font->Color = clBlack;
Label6->Font->Color = clBlack;
Label7->Font->Color = clBlack;
//Sq Metres To Sq Feet
Label13->Font->Color = clBlack;
Label15->Font->Color = clBlack;
Label17->Font->Color = clBlack;
//Sq Feet To Sq Metres
Label18->Font->Color = clBlack;
Label16->Font->Color = clBlack;
Label14->Font->Color = clBlack;
//Sq Yards To Sq Metres
Label21->Font->Color = clBlack;
Label23->Font->Color = clBlack;
Label19->Font->Color = clBlack;
//Sq Metres To Sq Yards
Label20->Font->Color = clBlack;
Label24->Font->Color = clBlack;
Label22->Font->Color = clBlack;
}
//---------------------------------------------------------------------------
void __fastcall TPC_Calc_Main::Button2Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Clear1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Exit1Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Button3Click(TObject *Sender)
{
// Check ZERO Fields
// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "")
        {
        Edit3->Text = "0";
        }
        else
        {
        }
}
{
if (Edit4->Text == "")
        {
        Edit4->Text = "0";
        }
        else
        {
        }
}
{
if (Edit5->Text == "")
        {
        Edit5->Text = "0";
        }
        else
        {
        }
}
{
if (Edit6->Text == "")
        {
        Edit6->Text = "0";
        }
        else
        {
        }
}
{
if (Edit7->Text == "")
        {
        Edit7->Text = "0";
        }
        else
        {
        }
}
{
if (Edit8->Text == "")
        {
        Edit8->Text = "0";
        }
        else
        {
        }
}
{
if (Edit9->Text == "")
        {
        Edit9->Text = "0";
        }
        else
        {
        }
}
{
if (Edit10->Text == "")
        {
        Edit10->Text = "0";
        }
        else
        {
        }
}
{
if (Edit11->Text == "")
        {
        Edit11->Text = "0";
        }
        else
        {
        }
}
{
if (Edit12->Text == "")
        {
        Edit12->Text = "0";
        }
        else
        {
        }
}
{
if (Edit13->Text == "")
        {
        Edit13->Text = "0";
        }
        else
        {
        }
}
{
if (Edit14->Text == "")
        {
        Edit14->Text = "0";
        }
        else
        {
        }
}
{
if (Edit15->Text == "")
        {
        Edit15->Text = "0";
        }
        else
        {
        }
}
{
if (Edit16->Text == "")
        {
        Edit16->Text = "0";
        }
        else
        {
        }
}
{
if (Edit17->Text == "")
        {
        Edit17->Text = "0";
        }
        else
        {
        }
}
{
if (Edit18->Text == "")
        {
        Edit18->Text = "0";
        }
        else
        {
        }
}
{
if (Edit19->Text == "")
        {
        Edit19->Text = "0";
        }
        else
        {
        }
}
{
if (Edit20->Text == "")
        {
        Edit20->Text = "0";
        }
        else
        {
        }
}
{
if (Edit23->Text == "")
        {
        Edit23->Text = "0";
        }
        else
        {
        }
}
{
if (Edit24->Text == "")
        {
        Edit24->Text = "0";
        }
        else
        {
        }
}
{
if (Edit25->Text == "")
        {
        Edit25->Text = "0";
        }
        else
        {
        }
}
{
if (Edit26->Text == "")
        {
        Edit26->Text = "0";
        }
        else
        {
        }
}
{
if (Edit27->Text == "")
        {
        Edit27->Text = "0";
        }
        else
        {
        }
}          {
if (Edit28->Text == "")
        {
        Edit28->Text = "0";
        }
        else
        {
        }
}
{
if (Edit21->Text == "")
        {
        Edit21->Text = "0";
        }
        else
        {
        }
}
{
if (Edit29->Text == "")
        {
        Edit29->Text = "0";
        }
        else
        {
        }
}
{
if (Edit22->Text == "")
        {
        Edit22->Text = "0";
        }
        else
        {
        }
}
// End ZERO Check process

// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "0")
        {
        Edit28->Text = (Edit5->Text);
        Edit29->Text = (Edit5->Text);
        }
        else
        {
        double I1 = Edit3->Text.ToDouble();
        double I2 = Edit4->Text.ToDouble();
        double O1 = I1 * I2;
        Edit28->Text = Edit28->Text.sprintf("%.2f", O1);
        Edit29->Text = Edit29->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Start Room Detail Calculations
// R1
{
if (Edit6->Text == "0")
        {
        Edit23->Text = (Edit8->Text);
        }
        else
        {
        double I1 = Edit6->Text.ToDouble();
        double I2 = Edit7->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit23->Text = Edit23->Text.sprintf("%.2f", O1);
        }
}
// R2
{
if (Edit9->Text == "0")
        {
        Edit24->Text = (Edit11->Text);
        }
        else
        {
        double I1 = Edit9->Text.ToDouble();
        double I2 = Edit10->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit24->Text = Edit24->Text.sprintf("%.2f", O1);
        }
}
// R3
{
if (Edit12->Text == "0")
        {
        Edit25->Text = (Edit14->Text);
        }
        else
        {
        double I1 = Edit12->Text.ToDouble();
        double I2 = Edit13->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit25->Text = Edit25->Text.sprintf("%.2f", O1);
        }
}
// R4
{
if (Edit15->Text == "0")
        {
        Edit26->Text = (Edit17->Text);
        }
        else
        {
        double I1 = Edit15->Text.ToDouble();
        double I2 = Edit16->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit26->Text = Edit26->Text.sprintf("%.2f", O1);
        }
}
// R5
{
if (Edit18->Text == "0")
        {
        Edit27->Text = (Edit20->Text);
        }
        else
        {
        double I1 = Edit18->Text.ToDouble();
        double I2 = Edit19->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit27->Text = Edit27->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Calculation Total Room Size
{
        double I1 = Edit23->Text.ToDouble();
        double I2 = Edit24->Text.ToDouble();
        double I3 = Edit25->Text.ToDouble();
        double I4 = Edit26->Text.ToDouble();
        double I5 = Edit27->Text.ToDouble();
        double O1 = (I1 + I2 + I3 + I4 + I5);
        Edit21->Text = Edit21->Text.sprintf("%.2f", O1);
}
// End of Process

// Calculation Product QTY
{
        double I1 = Edit21->Text.ToDouble();
        double I2 = Edit29->Text.ToDouble();
        double O1 = (I1 / I2);
        Edit22->Text = Edit22->Text.sprintf("%.2f", O1);
}
// End of Process
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit3KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit4->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit4KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit5->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit5KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit6->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit7KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit8->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit8KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit9->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit10KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit11->SetFocus();
	}
}
//---------------------------------------------------------------------------


void __fastcall TPC_Calc_Main::Edit9KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit10->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit11KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit12->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit12KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit13->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit13KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit14->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit14KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit15->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit15KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit16->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit16KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit17->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit17KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit18->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit18KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit19->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit19KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit20->SetFocus();
	}
}
//---------------------------------------------------------------------------


void __fastcall TPC_Calc_Main::Edit20KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        // Check ZERO Fields
// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "")
        {
        Edit3->Text = "0";
        }
        else
        {
        }
}
{
if (Edit4->Text == "")
        {
        Edit4->Text = "0";
        }
        else
        {
        }
}
{
if (Edit5->Text == "")
        {
        Edit5->Text = "0";
        }
        else
        {
        }
}
{
if (Edit6->Text == "")
        {
        Edit6->Text = "0";
        }
        else
        {
        }
}
{
if (Edit7->Text == "")
        {
        Edit7->Text = "0";
        }
        else
        {
        }
}
{
if (Edit8->Text == "")
        {
        Edit8->Text = "0";
        }
        else
        {
        }
}
{
if (Edit9->Text == "")
        {
        Edit9->Text = "0";
        }
        else
        {
        }
}
{
if (Edit10->Text == "")
        {
        Edit10->Text = "0";
        }
        else
        {
        }
}
{
if (Edit11->Text == "")
        {
        Edit11->Text = "0";
        }
        else
        {
        }
}
{
if (Edit12->Text == "")
        {
        Edit12->Text = "0";
        }
        else
        {
        }
}
{
if (Edit13->Text == "")
        {
        Edit13->Text = "0";
        }
        else
        {
        }
}
{
if (Edit14->Text == "")
        {
        Edit14->Text = "0";
        }
        else
        {
        }
}
{
if (Edit15->Text == "")
        {
        Edit15->Text = "0";
        }
        else
        {
        }
}
{
if (Edit16->Text == "")
        {
        Edit16->Text = "0";
        }
        else
        {
        }
}
{
if (Edit17->Text == "")
        {
        Edit17->Text = "0";
        }
        else
        {
        }
}
{
if (Edit18->Text == "")
        {
        Edit18->Text = "0";
        }
        else
        {
        }
}
{
if (Edit19->Text == "")
        {
        Edit19->Text = "0";
        }
        else
        {
        }
}
{
if (Edit20->Text == "")
        {
        Edit20->Text = "0";
        }
        else
        {
        }
}
{
if (Edit23->Text == "")
        {
        Edit23->Text = "0";
        }
        else
        {
        }
}
{
if (Edit24->Text == "")
        {
        Edit24->Text = "0";
        }
        else
        {
        }
}
{
if (Edit25->Text == "")
        {
        Edit25->Text = "0";
        }
        else
        {
        }
}
{
if (Edit26->Text == "")
        {
        Edit26->Text = "0";
        }
        else
        {
        }
}
{
if (Edit27->Text == "")
        {
        Edit27->Text = "0";
        }
        else
        {
        }
}          {
if (Edit28->Text == "")
        {
        Edit28->Text = "0";
        }
        else
        {
        }
}
{
if (Edit21->Text == "")
        {
        Edit21->Text = "0";
        }
        else
        {
        }
}
{
if (Edit29->Text == "")
        {
        Edit29->Text = "0";
        }
        else
        {
        }
}
{
if (Edit22->Text == "")
        {
        Edit22->Text = "0";
        }
        else
        {
        }
}
// End ZERO Check process

// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "0")
        {
        Edit28->Text = (Edit5->Text);
        Edit29->Text = (Edit5->Text);
        }
        else
        {
        double I1 = Edit3->Text.ToDouble();
        double I2 = Edit4->Text.ToDouble();
        double O1 = I1 * I2;
        Edit28->Text = Edit28->Text.sprintf("%.2f", O1);
        Edit29->Text = Edit29->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Start Room Detail Calculations
// R1
{
if (Edit6->Text == "0")
        {
        Edit23->Text = (Edit8->Text);
        }
        else
        {
        double I1 = Edit6->Text.ToDouble();
        double I2 = Edit7->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit23->Text = Edit23->Text.sprintf("%.2f", O1);
        }
}
// R2
{
if (Edit9->Text == "0")
        {
        Edit24->Text = (Edit11->Text);
        }
        else
        {
        double I1 = Edit9->Text.ToDouble();
        double I2 = Edit10->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit24->Text = Edit24->Text.sprintf("%.2f", O1);
        }
}
// R3
{
if (Edit12->Text == "0")
        {
        Edit25->Text = (Edit14->Text);
        }
        else
        {
        double I1 = Edit12->Text.ToDouble();
        double I2 = Edit13->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit25->Text = Edit25->Text.sprintf("%.2f", O1);
        }
}
// R4
{
if (Edit15->Text == "0")
        {
        Edit26->Text = (Edit17->Text);
        }
        else
        {
        double I1 = Edit15->Text.ToDouble();
        double I2 = Edit16->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit26->Text = Edit26->Text.sprintf("%.2f", O1);
        }
}
// R5
{
if (Edit18->Text == "0")
        {
        Edit27->Text = (Edit20->Text);
        }
        else
        {
        double I1 = Edit18->Text.ToDouble();
        double I2 = Edit19->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit27->Text = Edit27->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Calculation Total Room Size
{
        double I1 = Edit23->Text.ToDouble();
        double I2 = Edit24->Text.ToDouble();
        double I3 = Edit25->Text.ToDouble();
        double I4 = Edit26->Text.ToDouble();
        double I5 = Edit27->Text.ToDouble();
        double O1 = (I1 + I2 + I3 + I4 + I5);
        Edit21->Text = Edit21->Text.sprintf("%.2f", O1);
}
// End of Process

// Calculation Product QTY
{
        double I1 = Edit21->Text.ToDouble();
        double I2 = Edit29->Text.ToDouble();
        double O1 = (I1 / I2);
        Edit22->Text = Edit22->Text.sprintf("%.2f", O1);
}
// End of Process
	}
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Clear21Click(TObject *Sender)
{
Edit3->Text = "";
Edit4->Text = "";
Edit5->Text = "";
Edit28->Text = "";
Edit6->Text = "";
Edit7->Text = "";
Edit8->Text = "";
Edit23->Text = "";
Edit9->Text = "";
Edit10->Text = "";
Edit11->Text = "";
Edit24->Text = "";
Edit12->Text = "";
Edit13->Text = "";
Edit14->Text = "";
Edit25->Text = "";
Edit15->Text = "";
Edit16->Text = "";
Edit17->Text = "";
Edit26->Text = "";
Edit18->Text = "";
Edit19->Text = "";
Edit20->Text = "";
Edit27->Text = "";
Edit21->Text = "";
Edit29->Text = "";
Edit22->Text = "";
Edit3->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Calculate1Click(TObject *Sender)
{
// Check ZERO Fields
// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "")
        {
        Edit3->Text = "0";
        }
        else
        {
        }
}
{
if (Edit4->Text == "")
        {
        Edit4->Text = "0";
        }
        else
        {
        }
}
{
if (Edit5->Text == "")
        {
        Edit5->Text = "0";
        }
        else
        {
        }
}
{
if (Edit6->Text == "")
        {
        Edit6->Text = "0";
        }
        else
        {
        }
}
{
if (Edit7->Text == "")
        {
        Edit7->Text = "0";
        }
        else
        {
        }
}
{
if (Edit8->Text == "")
        {
        Edit8->Text = "0";
        }
        else
        {
        }
}
{
if (Edit9->Text == "")
        {
        Edit9->Text = "0";
        }
        else
        {
        }
}
{
if (Edit10->Text == "")
        {
        Edit10->Text = "0";
        }
        else
        {
        }
}
{
if (Edit11->Text == "")
        {
        Edit11->Text = "0";
        }
        else
        {
        }
}
{
if (Edit12->Text == "")
        {
        Edit12->Text = "0";
        }
        else
        {
        }
}
{
if (Edit13->Text == "")
        {
        Edit13->Text = "0";
        }
        else
        {
        }
}
{
if (Edit14->Text == "")
        {
        Edit14->Text = "0";
        }
        else
        {
        }
}
{
if (Edit15->Text == "")
        {
        Edit15->Text = "0";
        }
        else
        {
        }
}
{
if (Edit16->Text == "")
        {
        Edit16->Text = "0";
        }
        else
        {
        }
}
{
if (Edit17->Text == "")
        {
        Edit17->Text = "0";
        }
        else
        {
        }
}
{
if (Edit18->Text == "")
        {
        Edit18->Text = "0";
        }
        else
        {
        }
}
{
if (Edit19->Text == "")
        {
        Edit19->Text = "0";
        }
        else
        {
        }
}
{
if (Edit20->Text == "")
        {
        Edit20->Text = "0";
        }
        else
        {
        }
}
{
if (Edit23->Text == "")
        {
        Edit23->Text = "0";
        }
        else
        {
        }
}
{
if (Edit24->Text == "")
        {
        Edit24->Text = "0";
        }
        else
        {
        }
}
{
if (Edit25->Text == "")
        {
        Edit25->Text = "0";
        }
        else
        {
        }
}
{
if (Edit26->Text == "")
        {
        Edit26->Text = "0";
        }
        else
        {
        }
}
{
if (Edit27->Text == "")
        {
        Edit27->Text = "0";
        }
        else
        {
        }
}          {
if (Edit28->Text == "")
        {
        Edit28->Text = "0";
        }
        else
        {
        }
}
{
if (Edit21->Text == "")
        {
        Edit21->Text = "0";
        }
        else
        {
        }
}
{
if (Edit29->Text == "")
        {
        Edit29->Text = "0";
        }
        else
        {
        }
}
{
if (Edit22->Text == "")
        {
        Edit22->Text = "0";
        }
        else
        {
        }
}
// End ZERO Check process

// Start Product Size and Coverage Calculations
{
if (Edit3->Text == "0")
        {
        Edit28->Text = (Edit5->Text);
        Edit29->Text = (Edit5->Text);
        }
        else
        {
        double I1 = Edit3->Text.ToDouble();
        double I2 = Edit4->Text.ToDouble();
        double O1 = I1 * I2;
        Edit28->Text = Edit28->Text.sprintf("%.2f", O1);
        Edit29->Text = Edit29->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Start Room Detail Calculations
// R1
{
if (Edit6->Text == "0")
        {
        Edit23->Text = (Edit8->Text);
        }
        else
        {
        double I1 = Edit6->Text.ToDouble();
        double I2 = Edit7->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit23->Text = Edit23->Text.sprintf("%.2f", O1);
        }
}
// R2
{
if (Edit9->Text == "0")
        {
        Edit24->Text = (Edit11->Text);
        }
        else
        {
        double I1 = Edit9->Text.ToDouble();
        double I2 = Edit10->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit24->Text = Edit24->Text.sprintf("%.2f", O1);
        }
}
// R3
{
if (Edit12->Text == "0")
        {
        Edit25->Text = (Edit14->Text);
        }
        else
        {
        double I1 = Edit12->Text.ToDouble();
        double I2 = Edit13->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit25->Text = Edit25->Text.sprintf("%.2f", O1);
        }
}
// R4
{
if (Edit15->Text == "0")
        {
        Edit26->Text = (Edit17->Text);
        }
        else
        {
        double I1 = Edit15->Text.ToDouble();
        double I2 = Edit16->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit26->Text = Edit26->Text.sprintf("%.2f", O1);
        }
}
// R5
{
if (Edit18->Text == "0")
        {
        Edit27->Text = (Edit20->Text);
        }
        else
        {
        double I1 = Edit18->Text.ToDouble();
        double I2 = Edit19->Text.ToDouble();
        double O1 = (I1 * I2);
        Edit27->Text = Edit27->Text.sprintf("%.2f", O1);
        }
}
// End of Process

// Calculation Total Room Size
{
        double I1 = Edit23->Text.ToDouble();
        double I2 = Edit24->Text.ToDouble();
        double I3 = Edit25->Text.ToDouble();
        double I4 = Edit26->Text.ToDouble();
        double I5 = Edit27->Text.ToDouble();
        double O1 = (I1 + I2 + I3 + I4 + I5);
        Edit21->Text = Edit21->Text.sprintf("%.2f", O1);
}
// End of Process

// Calculation Product QTY
{
        double I1 = Edit21->Text.ToDouble();
        double I2 = Edit29->Text.ToDouble();
        double O1 = (I1 / I2);
        Edit22->Text = Edit22->Text.sprintf("%.2f", O1);
}
// End of Process
}
//---------------------------------------------------------------------------

void __fastcall TPC_Calc_Main::Edit6KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit7->SetFocus();
	}
}
//---------------------------------------------------------------------------

