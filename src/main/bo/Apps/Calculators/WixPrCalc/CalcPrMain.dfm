object PC_Calc_Main: TPC_Calc_Main
  Left = 233
  Top = 61
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'WC - PC Module'
  ClientHeight = 668
  ClientWidth = 854
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnMouseMove = FormMouseMove
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 400
    Top = 80
    Width = 82
    Height = 19
    Caption = 'Centimetres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 520
    Top = 56
    Width = 82
    Height = 19
    Caption = 'Centimetres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 496
    Top = 80
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 496
    Top = 56
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 496
    Top = 104
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 496
    Top = 128
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 520
    Top = 128
    Width = 29
    Height = 19
    Caption = 'Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 400
    Top = 104
    Width = 29
    Height = 19
    Caption = 'Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 520
    Top = 104
    Width = 48
    Height = 19
    Caption = 'Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 400
    Top = 128
    Width = 48
    Height = 19
    Caption = 'Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 400
    Top = 56
    Width = 43
    Height = 19
    Caption = 'Inches'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 520
    Top = 80
    Width = 43
    Height = 19
    Caption = 'Inches'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 624
    Top = 56
    Width = 73
    Height = 19
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 744
    Top = 80
    Width = 73
    Height = 19
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 712
    Top = 56
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label16: TLabel
    Left = 712
    Top = 80
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label17: TLabel
    Left = 744
    Top = 56
    Width = 54
    Height = 19
    Caption = 'Sq. Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label18: TLabel
    Left = 624
    Top = 80
    Width = 54
    Height = 19
    Caption = 'Sq. Feet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label19: TLabel
    Left = 744
    Top = 104
    Width = 73
    Height = 19
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 624
    Top = 128
    Width = 73
    Height = 19
    Caption = 'Sq. Metres'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 624
    Top = 104
    Width = 62
    Height = 19
    Caption = 'Sq. Yards'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label22: TLabel
    Left = 744
    Top = 128
    Width = 62
    Height = 19
    Caption = 'Sq. Yards'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 712
    Top = 104
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label24: TLabel
    Left = 712
    Top = 128
    Width = 8
    Height = 19
    Caption = '>'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image1: TImage
    Left = 400
    Top = 56
    Width = 209
    Height = 17
    Cursor = crHandPoint
    OnClick = Image1Click
    OnMouseMove = Image1MouseMove
  end
  object Image2: TImage
    Left = 400
    Top = 80
    Width = 209
    Height = 17
    Cursor = crHandPoint
    OnClick = Image2Click
    OnMouseMove = Image2MouseMove
  end
  object Image3: TImage
    Left = 400
    Top = 104
    Width = 209
    Height = 17
    Cursor = crHandPoint
    OnClick = Image3Click
    OnMouseMove = Image3MouseMove
  end
  object Image4: TImage
    Left = 400
    Top = 128
    Width = 209
    Height = 17
    Cursor = crHandPoint
    OnClick = Image4Click
    OnMouseMove = Image4MouseMove
  end
  object Image5: TImage
    Left = 624
    Top = 56
    Width = 201
    Height = 17
    Cursor = crHandPoint
    OnClick = Image5Click
    OnMouseMove = Image5MouseMove
  end
  object Image6: TImage
    Left = 624
    Top = 80
    Width = 201
    Height = 17
    Cursor = crHandPoint
    OnClick = Image6Click
    OnMouseMove = Image6MouseMove
  end
  object Image7: TImage
    Left = 624
    Top = 104
    Width = 201
    Height = 17
    Cursor = crHandPoint
    OnClick = Image7Click
    OnMouseMove = Image7MouseMove
  end
  object Image8: TImage
    Left = 624
    Top = 128
    Width = 201
    Height = 17
    Cursor = crHandPoint
    OnClick = Image8Click
    OnMouseMove = Image8MouseMove
  end
  object Bevel1: TBevel
    Left = 8
    Top = 32
    Width = 833
    Height = 11
    Shape = bsBottomLine
  end
  object Bevel2: TBevel
    Left = 104
    Top = 600
    Width = 737
    Height = 11
    Shape = bsBottomLine
  end
  object Label25: TLabel
    Left = 112
    Top = 168
    Width = 237
    Height = 23
    Caption = 'Product Size or Pack Coverage'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel3: TBevel
    Left = 104
    Top = 248
    Width = 737
    Height = 11
    Shape = bsBottomLine
  end
  object Label26: TLabel
    Left = 392
    Top = 224
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label27: TLabel
    Left = 112
    Top = 264
    Width = 106
    Height = 23
    Caption = 'Room Details'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel4: TBevel
    Left = 104
    Top = 504
    Width = 737
    Height = 11
    Shape = bsBottomLine
  end
  object Label28: TLabel
    Left = 392
    Top = 320
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label29: TLabel
    Left = 392
    Top = 360
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label30: TLabel
    Left = 392
    Top = 400
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label31: TLabel
    Left = 392
    Top = 440
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label32: TLabel
    Left = 392
    Top = 480
    Width = 15
    Height = 19
    Caption = 'or'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label33: TLabel
    Left = 112
    Top = 520
    Width = 117
    Height = 23
    Caption = 'Project Details'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel5: TBevel
    Left = 104
    Top = 152
    Width = 737
    Height = 11
    Shape = bsBottomLine
  end
  object Label34: TLabel
    Left = 112
    Top = 48
    Width = 206
    Height = 23
    Caption = 'Measurement Conversion'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 224
    Top = 624
    Width = 161
    Height = 33
    Caption = 'F3 Clear Fields (Conversions)'
    TabOrder = 21
    OnClick = Button1Click
  end
  object StaticText1: TStaticText
    Left = 114
    Top = 100
    Width = 95
    Height = 23
    Alignment = taCenter
    Caption = 'Measurement'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 24
  end
  object StaticText2: TStaticText
    Left = 290
    Top = 100
    Width = 44
    Height = 23
    Caption = 'Result'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 25
  end
  object StaticText3: TStaticText
    Left = 8
    Top = 8
    Width = 173
    Height = 26
    Caption = 'Calculate : Project'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 26
  end
  object Button2: TButton
    Left = 752
    Top = 624
    Width = 89
    Height = 33
    Caption = 'F10 Exit'
    TabOrder = 22
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 112
    Top = 120
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 23
  end
  object Edit2: TEdit
    Left = 248
    Top = 120
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object StaticText4: TStaticText
    Left = 114
    Top = 196
    Width = 48
    Height = 23
    Alignment = taCenter
    Caption = 'Length'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 27
  end
  object Edit3: TEdit
    Left = 112
    Top = 216
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnKeyPress = Edit3KeyPress
  end
  object StaticText5: TStaticText
    Left = 250
    Top = 196
    Width = 43
    Height = 23
    Alignment = taCenter
    Caption = 'Width'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 28
  end
  object Edit4: TEdit
    Left = 248
    Top = 216
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnKeyPress = Edit4KeyPress
  end
  object StaticText6: TStaticText
    Left = 426
    Top = 196
    Width = 64
    Height = 23
    Alignment = taCenter
    Caption = 'Coverage'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 29
  end
  object Edit5: TEdit
    Left = 424
    Top = 216
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnKeyPress = Edit5KeyPress
  end
  object Button3: TButton
    Left = 112
    Top = 624
    Width = 105
    Height = 33
    Caption = 'F2 Calculate'
    TabOrder = 19
    OnClick = Button3Click
  end
  object StaticText7: TStaticText
    Left = 114
    Top = 292
    Width = 48
    Height = 23
    Alignment = taCenter
    Caption = 'Length'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 30
  end
  object Edit6: TEdit
    Left = 112
    Top = 312
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnKeyPress = Edit6KeyPress
  end
  object StaticText8: TStaticText
    Left = 250
    Top = 292
    Width = 43
    Height = 23
    Alignment = taCenter
    Caption = 'Width'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 31
  end
  object Edit7: TEdit
    Left = 248
    Top = 312
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnKeyPress = Edit7KeyPress
  end
  object StaticText9: TStaticText
    Left = 426
    Top = 292
    Width = 64
    Height = 23
    Alignment = taCenter
    Caption = 'Coverage'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 32
  end
  object Edit8: TEdit
    Left = 424
    Top = 312
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnKeyPress = Edit8KeyPress
  end
  object Edit9: TEdit
    Left = 112
    Top = 352
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnKeyPress = Edit9KeyPress
  end
  object Edit10: TEdit
    Left = 248
    Top = 352
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnKeyPress = Edit10KeyPress
  end
  object Edit11: TEdit
    Left = 424
    Top = 352
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnKeyPress = Edit11KeyPress
  end
  object Edit12: TEdit
    Left = 112
    Top = 392
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    OnKeyPress = Edit12KeyPress
  end
  object Edit13: TEdit
    Left = 248
    Top = 392
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    OnKeyPress = Edit13KeyPress
  end
  object Edit14: TEdit
    Left = 424
    Top = 392
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnKeyPress = Edit14KeyPress
  end
  object Edit15: TEdit
    Left = 112
    Top = 432
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    OnKeyPress = Edit15KeyPress
  end
  object Edit16: TEdit
    Left = 248
    Top = 432
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    OnKeyPress = Edit16KeyPress
  end
  object Edit17: TEdit
    Left = 424
    Top = 432
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    OnKeyPress = Edit17KeyPress
  end
  object Edit18: TEdit
    Left = 112
    Top = 472
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    OnKeyPress = Edit18KeyPress
  end
  object Edit19: TEdit
    Left = 248
    Top = 472
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 17
    OnKeyPress = Edit19KeyPress
  end
  object Edit20: TEdit
    Left = 424
    Top = 472
    Width = 129
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 18
    OnKeyPress = Edit20KeyPress
  end
  object StaticText10: TStaticText
    Left = 114
    Top = 548
    Width = 108
    Height = 23
    Alignment = taCenter
    Caption = 'Total Room Size'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 33
  end
  object Edit21: TEdit
    Left = 112
    Top = 568
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 34
  end
  object StaticText11: TStaticText
    Left = 386
    Top = 548
    Width = 109
    Height = 23
    Alignment = taCenter
    Caption = 'Qty Product Req'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 35
  end
  object Edit22: TEdit
    Left = 384
    Top = 568
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 36
  end
  object StaticText12: TStaticText
    Left = 586
    Top = 292
    Width = 71
    Height = 23
    Alignment = taCenter
    Caption = 'Room Size'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 37
  end
  object Edit23: TEdit
    Left = 584
    Top = 312
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 38
  end
  object Edit24: TEdit
    Left = 584
    Top = 352
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 39
  end
  object Edit25: TEdit
    Left = 584
    Top = 392
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 40
  end
  object Edit26: TEdit
    Left = 584
    Top = 432
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 41
  end
  object Edit27: TEdit
    Left = 584
    Top = 472
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 42
  end
  object StaticText13: TStaticText
    Left = 586
    Top = 196
    Width = 117
    Height = 23
    Alignment = taCenter
    Caption = 'Product Coverage'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 43
  end
  object Edit28: TEdit
    Left = 584
    Top = 216
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 44
  end
  object StaticText14: TStaticText
    Left = 250
    Top = 548
    Width = 117
    Height = 23
    Alignment = taCenter
    Caption = 'Product Coverage'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 45
  end
  object Edit29: TEdit
    Left = 248
    Top = 568
    Width = 129
    Height = 32
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 46
  end
  object Button4: TButton
    Left = 392
    Top = 624
    Width = 153
    Height = 33
    Caption = 'F4 Clear Fields (Details)'
    TabOrder = 20
    OnClick = Button1Click
  end
  object MainMenu1: TMainMenu
    Left = 416
    object File1: TMenuItem
      Caption = 'File'
      Visible = False
      object Clear1: TMenuItem
        Caption = 'Clear'
        ShortCut = 114
        OnClick = Clear1Click
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        ShortCut = 121
        OnClick = Exit1Click
      end
      object Clear21: TMenuItem
        Caption = 'Clear2'
        ShortCut = 115
        OnClick = Clear21Click
      end
      object Calculate1: TMenuItem
        Caption = 'Calculate'
        ShortCut = 113
        OnClick = Calculate1Click
      end
    end
  end
end
