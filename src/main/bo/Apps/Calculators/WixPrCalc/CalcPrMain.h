//---------------------------------------------------------------------------

#ifndef CalcPrMainH
#define CalcPrMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TPC_Calc_Main : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TButton *Button1;
        TLabel *Label2;
        TLabel *Label1;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TImage *Image1;
        TImage *Image2;
        TImage *Image3;
        TImage *Image4;
        TImage *Image5;
        TImage *Image6;
        TImage *Image7;
        TImage *Image8;
        TStaticText *StaticText1;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TStaticText *StaticText2;
        TStaticText *StaticText3;
        TButton *Button2;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Clear1;
        TMenuItem *Exit1;
        TStaticText *StaticText4;
        TEdit *Edit3;
        TStaticText *StaticText5;
        TEdit *Edit4;
        TLabel *Label25;
        TBevel *Bevel3;
        TStaticText *StaticText6;
        TEdit *Edit5;
        TLabel *Label26;
        TButton *Button3;
        TStaticText *StaticText7;
        TEdit *Edit6;
        TStaticText *StaticText8;
        TEdit *Edit7;
        TLabel *Label27;
        TBevel *Bevel4;
        TStaticText *StaticText9;
        TEdit *Edit8;
        TLabel *Label28;
        TEdit *Edit9;
        TEdit *Edit10;
        TEdit *Edit11;
        TLabel *Label29;
        TEdit *Edit12;
        TEdit *Edit13;
        TEdit *Edit14;
        TLabel *Label30;
        TEdit *Edit15;
        TEdit *Edit16;
        TEdit *Edit17;
        TLabel *Label31;
        TEdit *Edit18;
        TEdit *Edit19;
        TEdit *Edit20;
        TLabel *Label32;
        TLabel *Label33;
        TStaticText *StaticText10;
        TEdit *Edit21;
        TStaticText *StaticText11;
        TEdit *Edit22;
        TBevel *Bevel5;
        TLabel *Label34;
        TStaticText *StaticText12;
        TEdit *Edit23;
        TEdit *Edit24;
        TEdit *Edit25;
        TEdit *Edit26;
        TEdit *Edit27;
        TStaticText *StaticText13;
        TEdit *Edit28;
        TStaticText *StaticText14;
        TEdit *Edit29;
        TButton *Button4;
        TMenuItem *Clear21;
        TMenuItem *Calculate1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Image1Click(TObject *Sender);
        void __fastcall Image2Click(TObject *Sender);
        void __fastcall Image3Click(TObject *Sender);
        void __fastcall Image4Click(TObject *Sender);
        void __fastcall Image5Click(TObject *Sender);
        void __fastcall Image6Click(TObject *Sender);
        void __fastcall Image7Click(TObject *Sender);
        void __fastcall Image8Click(TObject *Sender);
        void __fastcall Image1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image2MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image3MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image4MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image5MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image6MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image7MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image8MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Clear1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Edit3KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit4KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit5KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit7KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit8KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit10KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit9KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit11KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit12KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit13KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit14KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit15KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit16KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit17KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit18KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit19KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit20KeyPress(TObject *Sender, char &Key);
        void __fastcall Clear21Click(TObject *Sender);
        void __fastcall Calculate1Click(TObject *Sender);
        void __fastcall Edit6KeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TPC_Calc_Main(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPC_Calc_Main *PC_Calc_Main;
//---------------------------------------------------------------------------
#endif
