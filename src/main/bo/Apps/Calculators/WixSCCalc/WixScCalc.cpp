//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USEFORM("CalcScMain.cpp", Calculator);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "WC-SC Module (Kevan)";
                 Application->CreateForm(__classid(TCalculator), &Calculator);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
