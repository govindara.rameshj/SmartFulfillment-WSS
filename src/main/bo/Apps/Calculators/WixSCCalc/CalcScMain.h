//�2001 Roy E. Bourquin.  All Rights Reserved.

//---------------------------------------------------------------------------
#ifndef CalcScMainH
#define CalcScMainH
//---------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Mask.hpp>
#include <stdio.h>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TCalculator : public TForm
{
__published:	// IDE-managed Components
	TMemo *Display;
	TButton *Button10;
        TImage *Image1;
        TImage *Image2;
        TImage *Image3;
        TImage *Image4;
        TImage *Image5;
        TImage *Image6;
        TImage *Image7;
        TImage *Image8;
        TImage *Image9;
        TImage *Image10;
        TImage *Image11;
        TImage *Image12;
        TImage *Image13;
        TImage *Image14;
        TImage *Image15;
        TImage *Image16;
        TImage *Image17;
        TImage *Image18;
        TImage *Image19;
        TButton *Button1;
        TShape *Shape1;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Exit1;
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button0Click(TObject *Sender);
	void __fastcall DivideClick(TObject *Sender);
	void __fastcall MultiplyClick(TObject *Sender);
	void __fastcall SubtractClick(TObject *Sender);
	void __fastcall AddClick(TObject *Sender);
	void __fastcall EqualClick(TObject *Sender);
	void __fastcall DecimalClick(TObject *Sender);
	void __fastcall CEClick(TObject *Sender);
	void __fastcall ClearDisplayClick(TObject *Sender);
	void __fastcall PlusMinusClick(TObject *Sender);
	void __fastcall GotKey(TObject *Sender, char &Key);
	void __fastcall AcceptKeys(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
        void __fastcall Image12Click(TObject *Sender);
        void __fastcall Image13Click(TObject *Sender);
        void __fastcall Image14Click(TObject *Sender);
        void __fastcall Image19Click(TObject *Sender);
        void __fastcall Image11Click(TObject *Sender);
        void __fastcall Image10Click(TObject *Sender);
        void __fastcall Image9Click(TObject *Sender);
        void __fastcall Image15Click(TObject *Sender);
        void __fastcall Image8Click(TObject *Sender);
        void __fastcall Image7Click(TObject *Sender);
        void __fastcall Image6Click(TObject *Sender);
        void __fastcall Image16Click(TObject *Sender);
        void __fastcall Image5Click(TObject *Sender);
        void __fastcall Image4Click(TObject *Sender);
        void __fastcall Image3Click(TObject *Sender);
        void __fastcall Image17Click(TObject *Sender);
        void __fastcall Image1Click(TObject *Sender);
        void __fastcall Image2Click(TObject *Sender);
        void __fastcall Image18Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	char OptionSelect[2];
    char gs_DisplayValue[15];
    char gs_Result[40];
    char ShowValue[40];
    bool gb_DisplayValue;
    bool gb_Decimal;
    bool gb_ClearDisplay;
    bool gb_NeedClear;
    void UpdateDisplay(char *ls_button);
    void DoCalc(char *ls_DisplayValue, char *ls_Result, char *ls_OptionValue);
    __fastcall TCalculator(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern TCalculator *Calculator;
//---------------------------------------------------------------------------
#endif
