//�2009 Kevan Madelin

//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "CalcScMain.h"
//---------------------------------------------------------------------------
#pragma resource "*.dfm"
TCalculator *Calculator;
//---------------------------------------------------------------------------
__fastcall TCalculator::TCalculator(TComponent* Owner)
	: TForm(Owner)
{
	strcpy(OptionSelect, "");
    gb_DisplayValue = false;
    gb_Decimal = false;
    gb_ClearDisplay = true;
    strcpy(gs_DisplayValue, "");
    strcpy(gs_Result, "0");
    gb_NeedClear = false;
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button7Click(TObject *Sender)
{
	UpdateDisplay("7");	
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button8Click(TObject *Sender)
{
	UpdateDisplay("8");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button9Click(TObject *Sender)
{
	UpdateDisplay("9");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button4Click(TObject *Sender)
{
	UpdateDisplay("4");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button5Click(TObject *Sender)
{
	UpdateDisplay("5");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button6Click(TObject *Sender)
{
	UpdateDisplay("6");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button1Click(TObject *Sender)
{
	Application->Terminate();		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button2Click(TObject *Sender)
{
	UpdateDisplay("2");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button3Click(TObject *Sender)
{
	UpdateDisplay("3");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Button0Click(TObject *Sender)
{
	UpdateDisplay("0");		
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::DivideClick(TObject *Sender)
{
	UpdateDisplay("/");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::MultiplyClick(TObject *Sender)
{
	UpdateDisplay("*");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::SubtractClick(TObject *Sender)
{
	UpdateDisplay("-");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::AddClick(TObject *Sender)
{
	UpdateDisplay("+");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::EqualClick(TObject *Sender)
{
	UpdateDisplay("=");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::DecimalClick(TObject *Sender)
{
	UpdateDisplay(".");
}
//---------------------------------------------------------------------------

void TCalculator::UpdateDisplay(char *ls_button)
{
	FocusControl(Button10);
    if (gb_NeedClear && ls_button[0] != 'E') {
    	MessageBeep(-1);
        Display->Text = "Error: Press CE";
        return;
    }
    switch (ls_button[0]) {
    	case '+' :
        	if (gb_ClearDisplay == true) {
            	strcpy(OptionSelect, "+");
                return;
            }
            DoCalc(gs_DisplayValue, gs_Result, OptionSelect);
            strcpy(OptionSelect, "+");
			gb_ClearDisplay = true;
            break;
        case '-' :
        	if (gb_ClearDisplay == true) {
            	strcpy(OptionSelect, "-");
                return;
            }
            DoCalc(gs_DisplayValue, gs_Result, OptionSelect);
            strcpy(OptionSelect, "-");
			gb_ClearDisplay = true;
            break;
        case '/' :
        	if (gb_ClearDisplay == true) {
            	strcpy(OptionSelect, "/");
                return;
            }
            DoCalc(gs_DisplayValue, gs_Result, OptionSelect);
            strcpy(OptionSelect, "/");
			gb_ClearDisplay = true;
            break;
        case '*' :
        	if (gb_ClearDisplay == true) {
            	strcpy(OptionSelect, "*");
                return;
            }
            DoCalc(gs_DisplayValue, gs_Result, OptionSelect);
            strcpy(OptionSelect, "*");
			gb_ClearDisplay = true;
            break;
        case '=' :
        	if (gb_ClearDisplay == true) return;
            DoCalc(gs_DisplayValue, gs_Result, OptionSelect);
            strcpy(OptionSelect, "");
    		gb_DisplayValue = false;
    		gb_Decimal = false;
    		break;
        case '0' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "0.";
                strcpy(gs_DisplayValue, "");
                if (strcmp(OptionSelect, "/") == 0) Application->MessageBox("Can't divide by zero.", "Calculator Warning:", MB_OK);
            }
            else {
            	strcat(gs_DisplayValue, "0");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '1' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "1.";
                strcpy(gs_DisplayValue, "1");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "1");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
            break;
        case '2' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "2.";
                strcpy(gs_DisplayValue, "2");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "2");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '3' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "3.";
                strcpy(gs_DisplayValue, "3");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "3");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '4' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "4.";
                strcpy(gs_DisplayValue, "4");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "4");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '5' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "5.";
                strcpy(gs_DisplayValue, "5");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "5");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '6' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "6.";
                strcpy(gs_DisplayValue, "6");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "6");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '7' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "7.";
                strcpy(gs_DisplayValue, "7");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "7");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '8' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "8.";
                strcpy(gs_DisplayValue, "8");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "8");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case '9' :
        	if (strlen(gs_DisplayValue) >= 13 && !gb_ClearDisplay) {
            	MessageBeep(-1);
            	return;
            }
            if (gb_ClearDisplay == true) {
            	Display->Text = "9.";
                strcpy(gs_DisplayValue, "9");
                gb_ClearDisplay = false;
            }
            else {
            	strcat(gs_DisplayValue, "9");
                strcpy(ShowValue, gs_DisplayValue);
                if (gb_Decimal == false) strcat(ShowValue, ".");
                Display->Text = ShowValue;
            }
        	break;
        case 'C' :
  			FocusControl(Button10);
    		gb_DisplayValue = false;
    		gb_Decimal = false;
    		gb_ClearDisplay = true;
    		strcpy(gs_DisplayValue, "");
    		Display->Text = "0.";
	       	break;
       case 'E' :
        	FocusControl(Button10);
    		strcpy(OptionSelect, "");
    		gb_DisplayValue = false;
    		gb_Decimal = false;
    		gb_ClearDisplay = true;
    		strcpy(gs_DisplayValue, "");
    		strcpy(gs_Result, "0");
    		Display->Text = "0.";
    		gb_NeedClear = false;
    		break;
        case '.' :
        	if (gb_ClearDisplay == true) {
            	Display->Text = "0.";
                strcpy(gs_DisplayValue, ".");
                gb_ClearDisplay = false;
            }
            else if (!gb_Decimal) {
            	strcat(gs_DisplayValue, ".");
                strcpy(ShowValue, gs_DisplayValue);
                Display->Text = ShowValue;
            }
            gb_Decimal = true;
        	break;
        default :
        	break;
    }

}

void TCalculator::DoCalc(char *ls_DisplayValue, char *ls_Result, char *ls_OptionValue)
{
	double ld_Result;
    bool lb_Decimal = false;
    bool lb_TrailingZeros = true;
    unsigned int lui_i;

    gb_Decimal = false;
    gb_ClearDisplay = true;
    if (strcmp(ls_OptionValue, "") == 0) {
    	strcpy (ls_Result, ls_DisplayValue);
        return;
    }
    switch (ls_OptionValue[0]) {
    	case '/' :
        	if (strcmp(ls_DisplayValue, "") == 0) {
            	gb_NeedClear = true;
                return;
            }
            if (atof(ls_DisplayValue) == 0) {
            	gb_NeedClear = true;
                return;
            }
            ld_Result = atof(ls_Result)/atof(ls_DisplayValue);
            if (ld_Result >= 10000000000000) sprintf(ls_DisplayValue, "%.12e", ld_Result);
            else sprintf(ls_DisplayValue, "%.14f", ld_Result);
            break;
        case '*' :
        	ld_Result = atof(ls_Result)*atof(ls_DisplayValue);
            if (ld_Result >= 10000000000000) sprintf(ls_DisplayValue, "%.12e", ld_Result);
            else sprintf(ls_DisplayValue, "%.14f", ld_Result);
            break;
        case '-' :
         	ld_Result = atof(ls_Result)-atof(ls_DisplayValue);
            if (ld_Result >= 10000000000000) sprintf(ls_DisplayValue, "%.12e", ld_Result);
            else sprintf(ls_DisplayValue, "%.12f", ld_Result);
            break;
        case '+' :
         	ld_Result = atof(ls_Result)+atof(ls_DisplayValue);
            if (ld_Result >= 10000000000000) sprintf(ls_DisplayValue, "%.12e", ld_Result);
            else sprintf(ls_DisplayValue, "%.12f", ld_Result);
            break;
        case '=' :
        	return;
        default :
        	break;
    }
    strcpy(ShowValue, ls_DisplayValue);
    // remove trailing zeros
    for (lui_i = strlen(ShowValue)-1; lui_i > 1; lui_i--) {
    	if (ShowValue[lui_i] != '0') lb_TrailingZeros = false;
        if (ShowValue[lui_i] == '0' && lb_TrailingZeros) ShowValue[lui_i] = '\0';
    }
    // shorten remainder.
    if (strlen(ShowValue) >= 14) {
    	for (lui_i = strlen(ShowValue)-1; lui_i >= 12; lui_i--) {
    		ShowValue[lui_i] = '\0';
    	}
    }
    for (lui_i = 0; lui_i < strlen(ShowValue); lui_i++) if (ShowValue[lui_i] == '.') lb_Decimal = true;
    strcpy(gs_DisplayValue, ShowValue);
    if (!lb_Decimal) strcat(ShowValue, ".");
    Display->Text = ShowValue;
    strcpy(ls_Result, ShowValue);
    if (strlen(ShowValue) >=13) gb_NeedClear = true;
}

void __fastcall TCalculator::CEClick(TObject *Sender)
{
	UpdateDisplay("E");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::ClearDisplayClick(TObject *Sender)
{
	UpdateDisplay("C");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::PlusMinusClick(TObject *Sender)
{
	FocusControl(Button10);
    strcpy(gs_Result, "-1");
    DoCalc(gs_DisplayValue, gs_Result, "*");
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::GotKey(TObject *Sender, char &Key)
{
    switch (Key) {
    	case '0' :
        	UpdateDisplay("0");
            break;
        case '1' :
        	UpdateDisplay("1");
            break;
    	case '2' :
        	UpdateDisplay("2");
            break;
    	case '3' :
        	UpdateDisplay("3");
            break;
    	case '4' :
        	UpdateDisplay("4");
            break;
    	case '5' :
        	UpdateDisplay("5");
            break;
    	case '6' :
        	UpdateDisplay("6");
            break;
    	case '7' :
        	UpdateDisplay("7");
            break;
    	case '8' :
        	UpdateDisplay("8");
            break;
    	case '9' :
        	UpdateDisplay("9");
            break;
    	case '+' :
        	UpdateDisplay("+");
            break;
    	case '-' :
        	UpdateDisplay("-");
            break;
    	case '*' :
        	UpdateDisplay("*");
            break;
    	case '/' :
        	UpdateDisplay("/");
            break;
    	case '=' :
        	UpdateDisplay("=");
            break;
        case '.' :
        	UpdateDisplay(".");
            break;
    	case 'c' :
        	UpdateDisplay("C");
            break;
        case 'e' :
        	UpdateDisplay("E");
            break;
    }
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::AcceptKeys(TObject *Sender)
{
	FocusControl(Button10);
    	
}


void __fastcall TCalculator::Button10Click(TObject *Sender)
{
	UpdateDisplay("=");	
}
//---------------------------------------------------------------------------
void __fastcall TCalculator::Image12Click(TObject *Sender)
{
UpdateDisplay("0");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image13Click(TObject *Sender)
{
	FocusControl(Button10);
    strcpy(gs_Result, "-1");
    DoCalc(gs_DisplayValue, gs_Result, "*");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image14Click(TObject *Sender)
{
UpdateDisplay(".");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image19Click(TObject *Sender)
{
UpdateDisplay("=");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image11Click(TObject *Sender)
{
UpdateDisplay("1");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image10Click(TObject *Sender)
{
UpdateDisplay("2");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image9Click(TObject *Sender)
{
UpdateDisplay("3");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image15Click(TObject *Sender)
{
UpdateDisplay("+");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image8Click(TObject *Sender)
{
UpdateDisplay("4");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image7Click(TObject *Sender)
{
UpdateDisplay("5");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image6Click(TObject *Sender)
{
UpdateDisplay("6");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image16Click(TObject *Sender)
{
UpdateDisplay("-");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image5Click(TObject *Sender)
{
UpdateDisplay("7");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image4Click(TObject *Sender)
{
UpdateDisplay("8");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image3Click(TObject *Sender)
{
UpdateDisplay("9");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image17Click(TObject *Sender)
{
UpdateDisplay("*");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image1Click(TObject *Sender)
{
// Clear Everything - E
UpdateDisplay("E");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image2Click(TObject *Sender)
{
// Clear Entry - C
UpdateDisplay("C");
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Image18Click(TObject *Sender)
{
UpdateDisplay("/");        
}
//---------------------------------------------------------------------------

void __fastcall TCalculator::Exit1Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

