//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcVolMain.h"
#include "math.h"
#include "shellapi.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TVC_Calc_Main *VC_Calc_Main;
//---------------------------------------------------------------------------
__fastcall TVC_Calc_Main::TVC_Calc_Main(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Button1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
Edit3->Text = "";
Edit4->Text = "";
Edit1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Button2Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------





void __fastcall TVC_Calc_Main::Button3Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double I2 = Edit2->Text.ToDouble();
double I3 = Edit3->Text.ToDouble();
double O1 = I1 * I2 * I3;
Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------


void __fastcall TVC_Calc_Main::Exit1Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::ClearFields1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
Edit3->Text = "";
Edit4->Text = "";
Edit1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Calculate1Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double I2 = Edit2->Text.ToDouble();
double I3 = Edit3->Text.ToDouble();
double O1 = I1 * I2 * I3;
Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Edit1KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit2->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Edit2KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit3->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Edit3KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        double I1 = Edit1->Text.ToDouble();
        double I2 = Edit2->Text.ToDouble();
        double I3 = Edit3->Text.ToDouble();
        double O1 = I1 * I2 * I3;
        Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
        Button1->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::RunApp11Click(TObject *Sender)
{
ShellExecute(0, "open", "Explorer.exe", "", "", 1);
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label27Click(TObject *Sender)
{
Edit3->Text = "0.025";
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label28Click(TObject *Sender)
{
Edit3->Text = "0.05";
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label29Click(TObject *Sender)
{
Edit3->Text = "0.075";        
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label30Click(TObject *Sender)
{
Edit3->Text = "0.1";        
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label31Click(TObject *Sender)
{
Edit3->Text = "0.125";        
}
//---------------------------------------------------------------------------

void __fastcall TVC_Calc_Main::Label32Click(TObject *Sender)
{
Edit3->Text = "0.15";
}
//---------------------------------------------------------------------------

