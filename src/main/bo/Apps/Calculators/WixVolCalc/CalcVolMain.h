//---------------------------------------------------------------------------

#ifndef CalcVolMainH
#define CalcVolMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TVC_Calc_Main : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TButton *Button1;
        TStaticText *StaticText1;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TStaticText *StaticText2;
        TStaticText *StaticText3;
        TButton *Button2;
        TStaticText *StaticText4;
        TEdit *Edit3;
        TStaticText *StaticText5;
        TEdit *Edit4;
        TLabel *Label27;
        TLabel *Label28;
        TLabel *Label29;
        TLabel *Label30;
        TLabel *Label31;
        TLabel *Label32;
        TButton *Button3;
        TStaticText *StaticText6;
        TMainMenu *MainMenu1;
        TMenuItem *Main1;
        TMenuItem *Calculate1;
        TMenuItem *ClearFields1;
        TMenuItem *Exit1;
        TMenuItem *RunApp11;
        TImage *Image1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall ClearFields1Click(TObject *Sender);
        void __fastcall Calculate1Click(TObject *Sender);
        void __fastcall Edit1KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit2KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit3KeyPress(TObject *Sender, char &Key);
        void __fastcall RunApp11Click(TObject *Sender);
        void __fastcall Label27Click(TObject *Sender);
        void __fastcall Label28Click(TObject *Sender);
        void __fastcall Label29Click(TObject *Sender);
        void __fastcall Label30Click(TObject *Sender);
        void __fastcall Label31Click(TObject *Sender);
        void __fastcall Label32Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TVC_Calc_Main(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TVC_Calc_Main *VC_Calc_Main;
//---------------------------------------------------------------------------
#endif
