//---------------------------------------------------------------------------

#ifndef CalcSqMainH
#define CalcSqMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TSQ_Calc_Main : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TButton *Button1;
        TStaticText *StaticText1;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TStaticText *StaticText2;
        TStaticText *StaticText3;
        TButton *Button2;
        TStaticText *StaticText5;
        TEdit *Edit4;
        TButton *Button3;
        TStaticText *StaticText6;
        TMainMenu *MainMenu1;
        TMenuItem *Main1;
        TMenuItem *Calculate1;
        TMenuItem *ClearFields1;
        TMenuItem *Exit1;
        TImage *Image1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Image10Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall ClearFields1Click(TObject *Sender);
        void __fastcall Calculate1Click(TObject *Sender);
        void __fastcall Edit1KeyPress(TObject *Sender, char &Key);
        void __fastcall Edit2KeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TSQ_Calc_Main(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSQ_Calc_Main *SQ_Calc_Main;
//---------------------------------------------------------------------------
#endif
