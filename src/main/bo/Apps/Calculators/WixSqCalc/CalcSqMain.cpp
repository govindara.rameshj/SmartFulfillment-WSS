//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcSqMain.h"
#include "math.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSQ_Calc_Main *SQ_Calc_Main;
//---------------------------------------------------------------------------
__fastcall TSQ_Calc_Main::TSQ_Calc_Main(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::Button1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
Edit4->Text = "";
Edit1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::Button2Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------


void __fastcall TSQ_Calc_Main::Image10Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double I2 = Edit2->Text.ToDouble();
double O1 = I1 * I2;
//Edit4->Text = O1;
Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
        Button1->SetFocus();
}
//---------------------------------------------------------------------------



void __fastcall TSQ_Calc_Main::Button3Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double I2 = Edit2->Text.ToDouble();
double O1 = I1 * I2;
//Edit4->Text = O1;
Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
Button1->SetFocus();
}
//---------------------------------------------------------------------------


void __fastcall TSQ_Calc_Main::Exit1Click(TObject *Sender)
{
Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::ClearFields1Click(TObject *Sender)
{
Edit1->Text = "";
Edit2->Text = "";
Edit4->Text = "";
Edit1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::Calculate1Click(TObject *Sender)
{
double I1 = Edit1->Text.ToDouble();
double I2 = Edit2->Text.ToDouble();
double O1 = I1 * I2;
//Edit4->Text = O1;
Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
Button1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::Edit1KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        Edit2->SetFocus();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSQ_Calc_Main::Edit2KeyPress(TObject *Sender, char &Key)
{
if( Key == VK_RETURN )
	{
        double I1 = Edit1->Text.ToDouble();
        double I2 = Edit2->Text.ToDouble();
        double O1 = I1 * I2;
        //Edit4->Text = O1;
        Edit4->Text = Edit4->Text.sprintf("%.2f", O1);
        Button1->SetFocus();
	}
}
//---------------------------------------------------------------------------



