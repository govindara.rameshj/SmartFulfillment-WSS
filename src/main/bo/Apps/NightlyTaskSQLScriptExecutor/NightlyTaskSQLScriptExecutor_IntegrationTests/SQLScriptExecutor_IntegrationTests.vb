﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SQLScriptExecutor_IntegrationTests

    Private testContextInstance As TestContext
    Private _successScenario As Boolean

    Friend _sourceScriptFileName As String = "..\Resources\script.sql"
    Friend _destinationScriptFileName As String = "c:\temp\script.sql"
    Friend _scriptOutputFileName As String = "c:\temp\log\20120501235900 script.log"
    Friend _taskCompletionFileName As String = "TASKCOMP"
    Friend _logFolderCreated As Boolean

    'Friend _server As String = "-S S144426\SQLExpress"
    Friend _server As String = "(local)"
    Friend _database As String = "Oasys"
    Friend _nightlyRoutineAppendedParamter As String = "(/U=888,/P=',CFC')"


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub SQLScriptExecutor_SuccessfulScriptExecuted_TASKCOMPdeleted()
        Dim Executor As New TestSQLScriptExecutor_OverrideDecorateConsoleInCaseNoConsoleInTest
        Dim parm As New List(Of String)
        Dim CompletedTaskFileExists As Boolean = True

        _successScenario = True
        SetUp()
        SetUpParameters(parm)

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Executor.Execute(genericParam)
        CompletedTaskFileExists = IO.File.Exists(_taskCompletionFileName)
        TearDown()

        Assert.IsFalse(CompletedTaskFileExists)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_FailedScriptExecuted_TASKCOMPremains()
        Dim Executor As New TestSQLScriptExecutor_OverrideDecorateConsoleInCaseNoConsoleInTest
        Dim parm As New List(Of String)
        Dim CompletedTaskFileExists As Boolean = False

        _successScenario = False
        SetUp()
        SetUpParameters(parm)

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Executor.Execute(genericParam)
        CompletedTaskFileExists = IO.File.Exists(_taskCompletionFileName)
        TearDown()

        Assert.IsTrue(CompletedTaskFileExists)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectConnectionCredentials_TASKCOMPremains()
        Dim Executor As New TestSQLScriptExecutor_OverrideDecorateConsoleInCaseNoConsoleInTest
        Dim parm As New List(Of String)
        Dim CompletedTaskFileExists As Boolean = False

        _successScenario = False
        SetUp()
        SetUpParameters(parm)

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Executor.Execute(genericParam)
        CompletedTaskFileExists = IO.File.Exists(_taskCompletionFileName)
        TearDown()

        Assert.IsTrue(CompletedTaskFileExists)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ScriptFileDoesNotExistButPathAndLogPathDoes_LogfileCreated()
        Dim Executor As TestSQLScriptExecutor_UseTestScriptOutputRepository
        Dim CorrectScriptFileName As String = _destinationScriptFileName
        Dim CorrectLogFileName As String = _scriptOutputFileName
        Dim parm As New List(Of String)
        Dim LogFileExists As Boolean = False

        _successScenario = True
        _destinationScriptFileName = _destinationScriptFileName.Replace("script", "noscript")
        _scriptOutputFileName = _scriptOutputFileName.Replace("script", "noscript")

        CreateTaskCompFile()
        _logFolderCreated = CreateLogFolder()
        SetUpParameters(parm)

        Executor = New TestSQLScriptExecutor_UseTestScriptOutputRepository(_scriptOutputFileName)
        Executor.Execute(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm))

        LogFileExists = IO.File.Exists(_scriptOutputFileName)
        TearDown()
        _destinationScriptFileName = CorrectScriptFileName
        _scriptOutputFileName = CorrectLogFileName

        Assert.IsTrue(LogFileExists)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ScriptFileDoesNotExistButPathAndLogPathDoes_LogfileCreatedWithScriptNotFoundMessage()
        Dim Executor As TestSQLScriptExecutor_UseTestScriptOutputRepository
        Dim CorrectScriptFileName As String = _destinationScriptFileName
        Dim CorrectLogFileName As String = _scriptOutputFileName
        Dim parm As New List(Of String)
        Dim LogFileWithCorrectContentExists As Boolean = False

        _successScenario = True
        _destinationScriptFileName = _destinationScriptFileName.Replace("script", "noscript")
        _scriptOutputFileName = _scriptOutputFileName.Replace("script", "noscript")

        CreateTaskCompFile()
        _logFolderCreated = CreateLogFolder()
        SetUpParameters(parm)

        Executor = New TestSQLScriptExecutor_UseTestScriptOutputRepository(_scriptOutputFileName)
        Executor.Execute(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm))

        If IO.File.Exists(_scriptOutputFileName) Then
            Dim sr As New IO.StreamReader(_scriptOutputFileName)
            Dim content As String
            Dim LogFileRepository As New ScriptOutputRepository

            content = sr.ReadToEnd
            sr.Close()
            LogFileWithCorrectContentExists = content.Contains(LogFileRepository.GetScriptFileNotFoundErrorMessage(_destinationScriptFileName))
        End If

        TearDown()
        _destinationScriptFileName = CorrectScriptFileName
        _scriptOutputFileName = CorrectLogFileName

        Assert.IsTrue(LogFileWithCorrectContentExists)
    End Sub

#Region "Helper methods"

    Friend Sub SetUpParameters(ByRef parm As List(Of String))

        With parm
            .Add("-S " & _server)
            .Add("-D " & _database)
            .Add("-F " & _destinationScriptFileName)
            .Add(_nightlyRoutineAppendedParamter)
        End With
    End Sub

    Friend Sub SetUp()

        ArrangeFilesForTest()
    End Sub

    Friend Sub TearDown()

        RemoveTestFiles()
    End Sub

    Private Sub ArrangeFilesForTest()

        'IO.File.Copy(_sourceScriptFileName, _destinationScriptFileName, True)
        CreateTaskCompFile()
        CreateScriptFile()
        _logFolderCreated = CreateLogFolder()
    End Sub

    Private Sub RemoveTestFiles()

        If IO.File.Exists(_taskCompletionFileName) Then
            IO.File.Delete(_taskCompletionFileName)
        End If
        If IO.File.Exists(_destinationScriptFileName) Then
            IO.File.Delete(_destinationScriptFileName)
        End If
        If IO.File.Exists(_scriptOutputFileName) Then
            IO.File.Delete(_scriptOutputFileName)
        End If
        If _logFolderCreated And IO.Directory.Exists(LogPath) Then
            IO.Directory.Delete(LogPath, True)
        End If
    End Sub

    Private Sub CreateScriptFile()
        Dim fs As New FileStream(_destinationScriptFileName, FileMode.Create, FileAccess.Write)
        Dim sw As StreamWriter
        sw = New StreamWriter(fs)

        Try

            With sw
                .WriteLine(GetSelectStatement())
                .WriteLine("")
                .WriteLine("if @@ERROR = 0 AND @@ROWCOUNT = 1")
                .WriteLine("   Print '****Scheduled Script Completed Successfully****'")
                .WriteLine("Else")
                .WriteLine("   Print '****Failure****'")
                .Close()
            End With
            fs.Close()

        Finally
            If Not sw Is Nothing Then
                sw.Close()
            End If
            If Not fs Is Nothing Then
                fs.Close()
            End If
        End Try
    End Sub

    Private Sub CreateTaskCompFile()
        Dim fs As New FileStream(_taskCompletionFileName, FileMode.Create, FileAccess.Write)
        Dim sw As StreamWriter

        sw = New StreamWriter(fs)
        Try

            With sw
                .WriteLine("Left intentionally blank")
                .Close()
            End With
            fs.Close()
        Finally
            If Not sw Is Nothing Then
                sw.Close()
            End If
            If Not fs Is Nothing Then
                fs.Close()
            End If
        End Try
    End Sub

    Private Function CreateLogFolder() As Boolean

        If Not FileIO.FileSystem.DirectoryExists(LogPath) Then
            FileIO.FileSystem.CreateDirectory(LogPath)
            CreateLogFolder = FileIO.FileSystem.DirectoryExists(LogPath)
        End If
    End Function

    Friend Overridable Function GetSelectStatement() As String

        If _successScenario = True Then
            Return "select COUNT(*) from RETOPT"
        Else
            Return "select COUNT(*) from REXXTOPT"
        End If
    End Function

    Private Function LogPath() As String

        Return Path.GetDirectoryName(_scriptOutputFileName)
    End Function
#End Region

#Region "Test Scenario classes"

    Private Class TestSQLScriptExecutor_OverrideDecorateConsoleInCaseNoConsoleInTest
        Inherits SQLScriptExecutor

        Friend Overrides Sub DecorateConsole()

            ' Deliberately empty
        End Sub
    End Class

    Private Class TestSQLScriptExecutor_AnScriptOutputRepository_WithConstructorOverload_ToSetLogFileFileName
        Inherits ScriptOutputRepository

        Private _logFileName As String

        Public Sub New(ByVal LogFileName As String)

            _logFileName = LogFileName
        End Sub

        Friend Overrides Function ScriptOutputFileName(ByVal ScriptInputFileName As String) As String

            _scriptOutputFileName = _logFileName

            Return _scriptOutputFileName
        End Function
    End Class

    Private Class TestSQLScriptExecutor_UseTestScriptOutputRepository
        Inherits TestSQLScriptExecutor_OverrideDecorateConsoleInCaseNoConsoleInTest

        Private _logFileName As String

        Public Sub New(ByVal LogFileName As String)

            _logFileName = LogFileName
        End Sub

        Friend Overrides Function GetLogFileRepository() As ScriptOutputRepository

            Return New TestSQLScriptExecutor_AnScriptOutputRepository_WithConstructorOverload_ToSetLogFileFileName(_logFileName)
        End Function
    End Class
#End Region
End Class
