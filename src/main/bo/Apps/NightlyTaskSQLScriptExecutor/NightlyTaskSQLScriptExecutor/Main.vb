﻿Module Main

    Sub Main()
        Cts.Oasys.Core.Tests.TestEnvironmentSetup.SetupIfTestRun()

        Dim Executor As New SQLScriptExecutor

        Executor.Execute(My.Application.CommandLineArgs)
    End Sub
End Module
