﻿Public Class SQLScriptExecutor

    Private _commandBuilder As CommandBuilder
    Private Const SuccessKey As String = "Scheduled Script Completed Successfully"
    Private Const NITMASFailureIndicatorFile As String = "TASKCOMP"
    Private Const SqlCmdExe As String = "sqlcmd.exe"

    Private Const SwitchPrefixServer As String = "-S"
    Private Const SwitchPrefixDatabase As String = "-D"
    Private Const SwitchPrefixFile As String = "-F"
    Private Const SwitchPrefixNightlyTask As String = "(/U="

    Private _endProcess As Boolean = False

    Friend ReadOnly Property CommandBuilder() As CommandBuilder
        Get
            Return _commandBuilder
        End Get
    End Property

    Friend Sub CleanUp()

        If ScriptExecutedSuccessfully() Then
            Debug.Print("SQL Script Executed Successfully")
            DeleteNITMASTaskFailureFile()
        Else
            Debug.Print("Failed")
        End If
    End Sub

    Public Sub Execute(ByVal commandParameters As System.Collections.ObjectModel.ReadOnlyCollection(Of String))
        Dim ConsolidatedParameters As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        ConsolidatedParameters = ConsolidateParameters(commandParameters)
        ValidateParameters(ConsolidatedParameters)
        PopulateBuilder(ConsolidatedParameters)
        DecorateConsole()
        ExecuteSQLScript(SqlCmdExe, _commandBuilder.CommandToExecute.Replace(SqlCmdExe & " ", ""))
        CleanUp()
    End Sub

    Friend Sub PopulateBuilder(ByVal commandLineParameters As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If _commandBuilder Is Nothing Then
            _commandBuilder = GetNewCommandBuilder()
        End If
        With _commandBuilder
            For Each param As String In commandLineParameters
                If ParamIsServerSwitch(param) Then
                    .Server = param.Replace(SwitchPrefixServer, "").Trim
                ElseIf ParamIsDatabaseSwitch(param) Then
                    .Database = param.Replace(SwitchPrefixDatabase, "").Trim
                ElseIf ParamIsFileSwitch(param) Then
                    .ScriptInputFile = param.Replace(SwitchPrefixFile, "").Trim
                End If
            Next
        End With
    End Sub

    Friend Overridable Function GetNewCommandBuilder() As CommandBuilder

        Return New CommandBuilder
    End Function

    Friend ReadOnly Property ScriptExecutedSuccessfully() As Boolean
        Get
            Return GetFileContent().ToUpper.Contains(SuccessKey.ToUpper)
        End Get
    End Property

    Friend Overridable Function GetFileContent() As String
        Dim Repository As New ScriptOutputRepository

        Return Repository.GetFileContent(_commandBuilder.ScriptOutputFile)
    End Function

    Friend Overridable Sub DecorateConsole()

        If CommandBuilder IsNot Nothing Then
            Try
                Console.Title = "Running script " & CommandBuilder.ScriptInputFileNameOnly
            Catch ex As Exception
                ' Might not be a Console - if doing unit/integration tests
            End Try
        End If
    End Sub

    Friend Overridable Sub ExecuteSQLScript(ByVal cmdline As String, ByVal cdArguements As String)

        Try
            RunShell(cmdline, cdArguements)
        Catch e As TimeoutException
            Debug.Print(e.ToString)
        Catch ex As SqlClient.SqlException
            Debug.Print(ex.ToString)
        End Try
    End Sub

    Private Sub RunShell(ByVal cmdline As String, ByVal cdArguments As String)
        Dim newProc As Diagnostics.Process = Nothing
        Dim ProcessStarted As DateTime = Date.MinValue

        Try
            _endProcess = False
            newProc = New Process
            With newProc
                With .StartInfo
                    .FileName = cmdline
                    .Arguments = cdArguments
                    .WindowStyle = ProcessWindowStyle.Hidden
                End With
                .EnableRaisingEvents = True
                AddHandler .Exited, AddressOf Me.ProcessExited
                .Start()
                ProcessStarted = Now
                Console.WriteLine("Processing script " & CommandBuilder.ScriptInputFileNameOnly & ".")
                Console.WriteLine("Please wait.")
            End With

            Dim DoneDotForThisMod5 As Boolean = False

            While _endProcess = False
                Dim CheckTime As DateTime = Now

                If DateDiff(DateInterval.Second, ProcessStarted, CheckTime) Mod 5 = 0 Then
                    If Not DoneDotForThisMod5 Then
                        Console.Write(".")
                        DoneDotForThisMod5 = True
                    End If
                Else
                    DoneDotForThisMod5 = False
                End If
            End While
            Console.WriteLine()
            Console.WriteLine("Completed runnning script " & CommandBuilder.ScriptInputFileNameOnly & ".")
        Catch ex As Exception
            _endProcess = True
        Finally
            If newProc IsNot Nothing Then
                newProc.Close()
            End If
        End Try
    End Sub

    Friend Sub ProcessExited(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myProcess As Process = DirectCast(sender, Process)

        _endProcess = True
        myProcess.Close()
    End Sub

    Friend Sub DeleteNITMASTaskFailureFile()

        If IO.File.Exists(NITMASFailureIndicatorFile) Then
            IO.File.Delete(NITMASFailureIndicatorFile)
            Debug.Print(NITMASFailureIndicatorFile & " file deleted")
        Else
            Debug.Print(NITMASFailureIndicatorFile & " file not found")
        End If
    End Sub

    Friend Function ProgramUsageHelpText() As String
        Dim sb As New StringBuilder
        Dim out As New ScriptOutputRepository
        Dim padding As String = "     "
        Dim assemblyName As String = System.Reflection.Assembly.GetExecutingAssembly.GetName.Name().ToString

        With sb
            .AppendLine()
            .AppendLine()
            .AppendLine(assemblyName)
            .AppendLine()
            .AppendLine("Usage:")
            .AppendLine(assemblyName & ".exe [[-S<SQL Server Instance Name>] [-D<Initial Database>]] -F<SQL Script PathAndFilename>")
            .AppendLine()
            .AppendLine("Assumptions:")
            .AppendLine("SQL Script will PRINT 'Scheduled Script Completed Successfully' when the script is deemed entirely successful.")
            .AppendLine("An output log file will be created which has the same name as the script file and;")
            .AppendLine(padding & "Is saved to a sub folder (of the script path) called " & out._subFolder & " and this sub folder must already exist.")
            .AppendLine(padding & "Is prefixed with a date and time stamp in the format " & out._dateTimeFileNamePrefixFormat)
            .AppendLine(padding & "Has the file extension " & out._fileNameExtension)
        End With

        Return sb.ToString
    End Function

    Friend Function ConsolidateParameters(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        Dim ConsolidatedParams As New List(Of String)
        Dim NextParam As String = ""
        Dim ConcatenateWithNextParam As Boolean = False

        For Each param As String In params
            If Not ConcatenateWithNextParam Then
                NextParam = param
                If ParamIsServerSwitch(param) AndAlso String.Compare(param.Trim, SwitchPrefixServer.Trim, True) = 0 Then
                    ConcatenateWithNextParam = True
                ElseIf ParamIsDatabaseSwitch(param) AndAlso String.Compare(param.Trim, SwitchPrefixDatabase.Trim, True) = 0 Then
                    ConcatenateWithNextParam = True
                ElseIf ParamIsFileSwitch(param) AndAlso String.Compare(param.Trim, SwitchPrefixFile.Trim, True) = 0 Then
                    ConcatenateWithNextParam = True
                End If
            Else
                NextParam &= " " & param
                ConcatenateWithNextParam = False
            End If
            If Not ConcatenateWithNextParam Then
                ConsolidatedParams.Add(NextParam)
            End If
        Next

        Return New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(ConsolidatedParams)
    End Function

    Friend Sub ValidateParameters(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If IncorrectNumberOfParametersPassed(params) _
        Or InvalidSwitchInParams(params) _
        Or Not ParamsIncludeAllNonOptionalSwitches(params) _
        Or ScriptFilePathOrLogPathNotExist(params) Then
            DisplayConsoleHelpAndEnd()
        Else
            If ScriptFileNotExist(params) Then
                WriteScriptFileNotExistErrorToLogFileAndEnd(params)
            End If
        End If
    End Sub

    Friend Overridable Function IncorrectNumberOfParametersPassed(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

        If param IsNot Nothing Then
            With param
                Return .Count < 2 Or .Count > 4
            End With
        End If
    End Function

    Friend Overridable Function InvalidSwitchInParams(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean
        Dim ServerSwitchFound As Boolean = False
        Dim PotentialServerSwitchParam As String = ""

        If params Is Nothing Then
            InvalidSwitchInParams = True
        Else
            For Each param As String In params
                If Not ParamIsServerSwitch(param) _
                And Not ParamIsFileSwitch(param) _
                And Not ParamIsNightlyTaskSwitch(param) _
                And Not ParamIsDatabaseSwitch(param) Then
                    InvalidSwitchInParams = True
                    Exit For
                End If
            Next
        End If
    End Function

    Friend Overridable Function ParamsIncludeAllNonOptionalSwitches(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean
        Dim FoundFileSwitch As Boolean = False

        If params IsNot Nothing Then
            For Each param As String In params
                If ParamIsFileSwitch(param) Then
                    FoundFileSwitch = True
                    Exit For
                End If
            Next
        End If

        Return FoundFileSwitch
    End Function

    Friend Overridable Function ScriptFilePathOrLogPathNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean
        Dim FileSwitch As String = ""
        Dim ScriptFilePath As String = ""
        Dim ScriptFilePathDoesNotExist As Boolean = False
        Dim LogFilePath As String = ""
        Dim LogPathDoesNotExist As Boolean = False

        If params IsNot Nothing Then
            For Each param As String In params
                If ParamIsFileSwitch(param) Then
                    FileSwitch = param
                    Exit For
                End If
            Next
        End If
        If FileSwitch.Length > 0 Then
            FileSwitch = FileSwitch.Replace(SwitchPrefixFile, "").Trim
            ScriptFilePath = Path.GetDirectoryName(FileSwitch)
            LogFilePath = Path.GetDirectoryName((New ScriptOutputRepository).ScriptOutputFileName(FileSwitch))
            ScriptFilePathDoesNotExist = Not Directory.Exists(ScriptFilePath)
            LogPathDoesNotExist = Not Directory.Exists(LogFilePath)
        End If

        Return ScriptFilePathDoesNotExist Or LogPathDoesNotExist
    End Function

    Friend Overridable Function ScriptFileNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean
        Dim FileSwitch As String = ""
        Dim ScriptFileDoesNotExist As Boolean = False

        If params IsNot Nothing Then
            For Each param As String In params
                If ParamIsFileSwitch(param) Then
                    FileSwitch = param
                    Exit For
                End If
            Next
        End If
        If FileSwitch.Length > 0 Then
            FileSwitch = FileSwitch.Replace(SwitchPrefixFile, "").Trim
            ScriptFileDoesNotExist = Not File.Exists(FileSwitch)
        End If

        Return ScriptFileDoesNotExist
    End Function

    Friend Function ParamIsServerSwitch(ByVal param As String) As Boolean

        Return param.StartsWith(SwitchPrefixServer, StringComparison.CurrentCultureIgnoreCase)
    End Function

    Friend Function ParamIsDatabaseSwitch(ByVal param As String) As Boolean

        Return param.StartsWith(SwitchPrefixDatabase, StringComparison.CurrentCultureIgnoreCase)
    End Function

    Friend Function ParamIsFileSwitch(ByVal param As String) As Boolean

        Return param.StartsWith(SwitchPrefixFile, StringComparison.CurrentCultureIgnoreCase)
    End Function

    Friend Function ParamIsNightlyTaskSwitch(ByVal param As String) As Boolean

        Return param.StartsWith(SwitchPrefixNightlyTask, StringComparison.CurrentCultureIgnoreCase)
    End Function

    Friend Overridable Sub DisplayConsoleHelpAndEnd()

        Try
            Console.WriteLine(ProgramUsageHelpText)
        Finally
            End
        End Try
    End Sub

    Friend Overridable Sub WriteScriptFileNotExistErrorToLogFileAndEnd(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String))
        Dim FileSwitch As String = ""

        If params IsNot Nothing Then
            For Each param As String In params
                If ParamIsFileSwitch(param) Then
                    FileSwitch = param
                    Exit For
                End If
            Next
        End If
        If FileSwitch.Length > 0 Then
            Dim LogfileRepository As ScriptOutputRepository = GetLogFileRepository()

            FileSwitch = FileSwitch.Replace(SwitchPrefixFile, "").Trim
            LogfileRepository.WriteScriptFileNotFoundErrorToLog(FileSwitch)
        End If
    End Sub

    Friend Overridable Function GetLogFileRepository() As ScriptOutputRepository

        Return New ScriptOutputRepository
    End Function
End Class