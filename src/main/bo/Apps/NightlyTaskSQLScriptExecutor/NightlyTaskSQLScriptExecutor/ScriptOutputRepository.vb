﻿Public Class ScriptOutputRepository

    Friend _subFolder As String = "\log"
    Friend _fileNameExtension As String = ".log"
    Friend _scriptOutputFileName As String = ""
    Friend _dateTimeFileNamePrefixFormat As String = "yyyyMMddHHmmss "

    Friend Overridable Function GetFileContent(ByVal ScriptOutputFileName As String) As String

        Dim ret As String = ""

        If File.Exists(ScriptOutputFileName) Then
            Dim sr As New IO.StreamReader(ScriptOutputFileName)
            ret = sr.ReadToEnd
            sr.Close()
        End If

        Return ret
    End Function

    Private Sub SetScriptOutputFileName(ByVal ScriptInputFileName As String)
        Dim sb As New StringBuilder

        With sb
            .Append(Path.GetDirectoryName(ScriptInputFileName))
            If .Length = 0 Then
                .Append(".")
            End If
            .Append(_subFolder)
            .Append("\")
            .Append(FileNamePrefix)
            .Append(Path.GetFileNameWithoutExtension(ScriptInputFileName))
            .Append(_fileNameExtension)
        End With

        _scriptOutputFileName = sb.ToString
    End Sub

    Friend Overridable Function ScriptOutputFileName(ByVal ScriptInputFileName As String) As String

        If _scriptOutputFileName.Length = 0 Then
            SetScriptOutputFileName(ScriptInputFileName)
        End If

        Return _scriptOutputFileName
    End Function

    Friend Function FileNamePrefix() As String

        Return Format(CurrentDateTime(), _dateTimeFileNamePrefixFormat)
    End Function

    Friend Overridable Function CurrentDateTime() As DateTime

        Return Now
    End Function

    Friend Overridable Sub WriteScriptFileNotFoundErrorToLog(ByVal ScriptInputFileName As String)

        ScriptOutputFileName(ScriptInputFileName)

        Dim LogFile As New IO.StreamWriter(_scriptOutputFileName)

        With LogFile
            .WriteLine(GetScriptFileNotFoundErrorMessage(ScriptInputFileName))
            .Flush()
            .Close()
        End With
    End Sub

    Friend Overridable Function GetScriptFileNotFoundErrorMessage(ByVal ScriptInputFileName As String) As String

        Return "Failure: Script file '" & ScriptInputFileName & "' does not exist."
    End Function
End Class
