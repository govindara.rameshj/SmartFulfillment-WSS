﻿Public Class CommandBuilder

    Private _database As String
    Private _scriptInputFile As String
    Private _server As String
    Friend _ScriptOutputRepository As New ScriptOutputRepository

    Public Sub New()

        InitialiseDefaultServerAndDefaultDatabase()
    End Sub

    Public ReadOnly Property CommandToExecute() As String
        Get
            Const SqlCommandExecutableName As String = "sqlcmd.exe"
            Const EscapedDoubleQuote As String = """"

            Dim sb As New StringBuilder

            With sb
                .Append(SqlCommandExecutableName)
                .Append(Space(1))
                .Append("-S")
                .Append(Space(1))
                .Append(_server)
                .Append(Space(1))
                .Append("-d")
                .Append(Space(1))
                .Append(_database)
                .Append(Space(1))
                .Append("-E")
                .Append(Space(1))
                .Append("-i")
                .Append(Space(1))
                .Append(EscapedDoubleQuote)
                .Append(_scriptInputFile)
                .Append(EscapedDoubleQuote)
                .Append(Space(1))
                .Append("-o")
                .Append(Space(1))
                .Append(EscapedDoubleQuote)
                .Append(ScriptOutputFile)
                .Append(EscapedDoubleQuote)
            End With

            Return sb.ToString
        End Get
    End Property

    Public Property Database() As String
        Get
            Return _database
        End Get
        Set(ByVal value As String)
            _database = value
        End Set
    End Property

    Public Property ScriptInputFile() As String
        Get
            Return _scriptInputFile
        End Get
        Set(ByVal value As String)
            _scriptInputFile = value
        End Set
    End Property

    Public ReadOnly Property ScriptOutputFile() As String
        Get
            Return _ScriptOutputRepository.ScriptOutputFileName(_scriptInputFile)
        End Get
    End Property

    Public ReadOnly Property ScriptInputFileNameOnly() As String
        Get
            Return IO.Path.GetFileName(_scriptInputFile)
        End Get
    End Property

    Public Property Server() As String
        Get
            Return _server
        End Get
        Set(ByVal value As String)
            _server = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder

        With sb
            .AppendLine(Me.CommandToExecute)
            .AppendLine(Me.Database)
            .AppendLine(Me.ScriptInputFile)
            .AppendLine(Me.ScriptOutputFile)
            .AppendLine(Me.Server)
        End With

        Return sb.ToString
    End Function

    Public Function ToStringExcludingOutputFileTime() As String
        Dim sb As New StringBuilder

        With sb
            .AppendLine(RemoveTimeFromDateTimeInString(Me.CommandToExecute))
            .AppendLine(Me.Database)
            .AppendLine(Me.ScriptInputFile)
            .AppendLine(RemoveTimeFromDateTimeInString(Me.ScriptOutputFile))
            .AppendLine(Me.Server)
        End With

        Return sb.ToString
    End Function

    Friend Overridable Sub InitialiseDefaultServerAndDefaultDatabase()
        Dim DataSourcePrefix As String = "Data Source="
        Dim InitialCatalogPrefix As String = "Initial Catalog="
        Dim Delimiter As Char = ";"c
        Dim ConnectionString As String = GetConnectionString()
        Dim ConnectionStringParameters() As String = ConnectionString.Split(Delimiter)

        For Each ConnStringParam As String In ConnectionStringParameters
            With ConnStringParam
                If .StartsWith(DataSourcePrefix) Then
                    _server = .Substring(.IndexOf(DataSourcePrefix) + DataSourcePrefix.Length)
                End If
                If .StartsWith(InitialCatalogPrefix) Then
                    _database = .Substring(.IndexOf(InitialCatalogPrefix) + InitialCatalogPrefix.Length)
                End If
            End With
        Next
    End Sub

    Friend Overridable Function GetConnectionString() As String

        Return Cts.Oasys.Data.GetConnectionString()
    End Function

    Private Function RemoveTimeFromDateTimeInString(ByVal ToRemoveFrom As String) As String
        Dim BeenRemoved As String = ""
        Dim CurrentDate As String = _ScriptOutputRepository.CurrentDateTime.ToString("yyyyMMdd")

        With ToRemoveFrom
            BeenRemoved = .Substring(0, .IndexOf(CurrentDate))
            BeenRemoved &= CurrentDate & " "
            With .Substring(.IndexOf(CurrentDate))
                BeenRemoved &= .Substring(.IndexOf(" ") + 1)
            End With
        End With

        Return BeenRemoved
    End Function
End Class
