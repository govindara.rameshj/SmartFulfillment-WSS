﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1022
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class Strings
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Menu.Strings", GetType(Strings).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Application Exit.
        '''</summary>
        Friend Shared ReadOnly Property AppExit() As String
            Get
                Return ResourceManager.GetString("AppExit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Application Start.
        '''</summary>
        Friend Shared ReadOnly Property AppStart() As String
            Get
                Return ResourceManager.GetString("AppStart", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Wickes Back Office.
        '''</summary>
        Friend Shared ReadOnly Property AppTitle() As String
            Get
                Return ResourceManager.GetString("AppTitle", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving master menu config.
        '''</summary>
        Friend Shared ReadOnly Property GetMasterMenu() As String
            Get
                Return ResourceManager.GetString("GetMasterMenu", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving workstation information.
        '''</summary>
        Friend Shared ReadOnly Property GetWorkstation() As String
            Get
                Return ResourceManager.GetString("GetWorkstation", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Heart Beat....
        '''</summary>
        Friend Shared ReadOnly Property HeartBeat() As String
            Get
                Return ResourceManager.GetString("HeartBeat", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Your work station has been closed as Master is not available.
        '''You will now be logged out and have to re-start the application.
        '''</summary>
        Friend Shared ReadOnly Property MasterNotAvailableAndExit() As String
            Get
                Return ResourceManager.GetString("MasterNotAvailableAndExit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Close All Windows.
        '''</summary>
        Friend Shared ReadOnly Property mnu_CloseAll() As String
            Get
                Return ResourceManager.GetString("mnu_CloseAll", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Exit.
        '''</summary>
        Friend Shared ReadOnly Property mnu_Exit() As String
            Get
                Return ResourceManager.GetString("mnu_Exit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to File.
        '''</summary>
        Friend Shared ReadOnly Property mnu_File() As String
            Get
                Return ResourceManager.GetString("mnu_File", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Log In.
        '''</summary>
        Friend Shared ReadOnly Property mnu_Login() As String
            Get
                Return ResourceManager.GetString("mnu_Login", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Log Out.
        '''</summary>
        Friend Shared ReadOnly Property mnu_Logout() As String
            Get
                Return ResourceManager.GetString("mnu_Logout", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to &amp;Cascade.
        '''</summary>
        Friend Shared ReadOnly Property mnu_TileCascade() As String
            Get
                Return ResourceManager.GetString("mnu_TileCascade", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Tile &amp;Horizontal.
        '''</summary>
        Friend Shared ReadOnly Property mnu_TileHorizontal() As String
            Get
                Return ResourceManager.GetString("mnu_TileHorizontal", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Tile &amp;Vertical.
        '''</summary>
        Friend Shared ReadOnly Property mnu_TileVertical() As String
            Get
                Return ResourceManager.GetString("mnu_TileVertical", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Window.
        '''</summary>
        Friend Shared ReadOnly Property mnu_Window() As String
            Get
                Return ResourceManager.GetString("mnu_Window", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Sorry cannot run a second copy of the application.
        '''</summary>
        Friend Shared ReadOnly Property MultipleCopies() As String
            Get
                Return ResourceManager.GetString("MultipleCopies", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to User logged out.
        '''</summary>
        Friend Shared ReadOnly Property UserLogout() As String
            Get
                Return ResourceManager.GetString("UserLogout", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Work Station is Closed.
        '''You will now be logged out and have to re-start the application.
        '''</summary>
        Friend Shared ReadOnly Property WorkstationClosedAndExit() As String
            Get
                Return ResourceManager.GetString("WorkstationClosedAndExit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Please log out before ending the program
        '''Would you like to log out and close now?.
        '''</summary>
        Friend Shared ReadOnly Property YesNoLogoutAndClose() As String
            Get
                Return ResourceManager.GetString("YesNoLogoutAndClose", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
