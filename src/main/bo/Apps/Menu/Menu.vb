Imports Cts.Oasys
Imports Cts.Oasys.Core.System
Imports Cts.Oasys.Core.System.User
Imports Cts.Oasys.WinForm
Imports System.Threading
Imports System.Reflection
Imports Cts.Oasys.Core.Locking

Public Class Menu
    Private _storeIdName As String
    Private _workstationMasterId As Integer
    Private _workStationId As Integer
    Private _workstationMenus As MenuOption.ItemCollection
    Private _user As User = Nothing
    Private _userMenuProfiles As MenuOption.ProfileCollection = Nothing

    Private _cncNotifierProcess As Process

    Public Sub New()
        InitializeComponent()
        AddHandler Application.ThreadException, New ThreadExceptionEventHandler(AddressOf ApplicationThreadException)
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf CurrentDomain_UnhandledException
    End Sub

    Private Sub ApplicationThreadException(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
        'trace log and write to event log and display exception message to user in dialog box
        Trace.WriteLine(e.Exception.ToString, Me.Name)
        EventLog.WriteEntry("Application", e.Exception.ToString, EventLogEntryType.Error)
        MessageBox.Show(e.Exception.Message, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Private Sub CurrentDomain_UnhandledException(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        Dim ex As Exception = CType(e.ExceptionObject, Exception)

        'trace log and write to event log and display exception message to user in dialog box
        Dim stackTrace As String = ex.ToString()
        If stackTrace.Contains("FarPoint.Win.Spread.FpSpread") AndAlso
        ex.Message.Equals("The RPC server is unavailable", StringComparison.InvariantCultureIgnoreCase) Then
            Trace.WriteLine(stackTrace, Me.Name)
            EventLog.WriteEntry("Application", stackTrace, EventLogEntryType.Error)
            MessageBox.Show(String.Format("Printing failed with the error: {0}. The application will be closed. Please contact administrator.", ex.Message), Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error)
            RemoveHandler Me.FormClosing, AddressOf Menu_FormClosing
            Application.Exit()
        End If
    End Sub

    Private Sub Menu_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        If _user IsNot Nothing Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.Strings.YesNoLogoutAndClose, "Menu", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result <> Windows.Forms.DialogResult.Yes Then
                e.Cancel = True
                Exit Sub
            End If
        End If
        Trace.WriteLine(My.Resources.Strings.AppExit, Name)

    End Sub

    Private Sub Menu_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        'set focus to menu and first item
        If MenuStrip1.Items.Count > 0 Then
            MenuStrip1.Items(0).Select()
            MenuStrip1.Focus()
        End If

        If tmrIsClosed.Enabled = False And _user IsNot Nothing Then
            tmrIsClosed.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' Author      : Partha Dutta
    ''' Date        : 15/10/2010
    ''' Referral No : 439
    ''' Notes       : Prevent multiple copies of the application to run, alternative version to ref 201B
    ''' </history>
    ''' <history>
    ''' Author      : Partha Dutta
    ''' Date        : 24/08/2010
    ''' Referral No : 201B
    ''' Notes       : Prevent multiple copies of the application to run
    ''' </history>
    ''' <remarks></remarks>
    Private Sub Menu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Trace.WriteLine(My.Resources.Strings.AppStart, Name)
            Cursor = Cursors.WaitCursor

            _storeIdName = SystemOption.GetIdName
            _workstationMasterId = SystemOption.GetMasterWorkstationId
            _workStationId = CInt(My.Computer.Registry.GetValue(My.Settings.registry, "WorkstationID", "20"))
            Me.Text = My.Resources.Strings.AppTitle & Space(1) & _storeIdName

            'get workstation profile id and menus allowed
            Dim profileId As Integer = Workstation.GetProfileId(_workStationId)
            Using profiles As MenuOption.ProfileCollection = MenuOption.Profile.GetProfiles(profileId)
                _workstationMenus = MenuOption.Item.GetMenus()
                _workstationMenus.Union(profiles)
            End Using

            'add file, master menus for workstation (non visible) and window menu
            InitialiseMenuFile()
            Me.Show()

            'check for daily open and enable timer
            CheckForDailyOpen()
            tmrIsClosed.Enabled = True

            'ref 439
            If Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName).Count > 1 Then
                TimerUniqueCopy.Enabled = True
            Else
                If CncNotificationEnabled() Then InitCncNotifierProcess()

                'show user login
                LoginClick(Nothing, EventArgs.Empty)
            End If
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Function CncNotificationEnabled() As Boolean
        Try
            Dim exludeWorkstationStr As String = Parameter.GetString(4502)
            If exludeWorkstationStr Is Nothing Then exludeWorkstationStr = ""

            If exludeWorkstationStr = "*" Then
                Trace.WriteLine("CnC notifications are disabled on all workstations")
                Return False
            End If

            Dim separator As Char() = {","c}
            Dim workstationList As String() = exludeWorkstationStr.Split(separator, StringSplitOptions.RemoveEmptyEntries)

            For Each num As String In workstationList
                Dim w As Integer
                If Integer.TryParse(num, w) AndAlso w = _workStationId Then
                    Trace.WriteLine("CnC notifications are disabled on current workstation")
                    Return False
                End If
            Next
            Trace.WriteLine("CnC notifications are enabled on current workstation")
            Return True
        Catch ex As Exception
            Trace.WriteLine("Error in CncNotificationEnabled: " & ex.ToString())
            Return False
        End Try
    End Function

    Private Sub InitCncNotifierProcess()
        Try
            Trace.WriteLine("Starting CncNotifier process")

            If Not _cncNotifierProcess Is Nothing Then
                ' Unsubscribe to let process object be disposed
                _cncNotifierProcess.EnableRaisingEvents = False
                RemoveHandler _cncNotifierProcess.Exited, AddressOf Me.CncNotifierProcess_Exited
            End If

            _cncNotifierProcess = New Process()
            _cncNotifierProcess.StartInfo.FileName = Path.Combine(Application.StartupPath, "Cts.CncNotifier.exe")
            _cncNotifierProcess.StartInfo.Arguments = Process.GetCurrentProcess().Id.ToString()

            AddHandler _cncNotifierProcess.Exited, AddressOf Me.CncNotifierProcess_Exited
            _cncNotifierProcess.EnableRaisingEvents = True
            _cncNotifierProcess.Start()

            Trace.WriteLine("CncNotifier process is started")
        Catch ex As Exception
            Trace.WriteLine("Error starting CncNotifier process: " & ex.ToString())
        End Try
    End Sub

    Private Sub CncNotifierProcess_Exited(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ' Do not restart process if is disabled or configuration isn't enough/correct
            If _cncNotifierProcess.ExitCode >= 0 Then InitCncNotifierProcess()
        Catch ex As Exception
            Trace.WriteLine("Error restarting CncNotifier process: " & ex.ToString())
        End Try
    End Sub


    Private Sub TimerUniqueCopy_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerUniqueCopy.Tick
        TimerUniqueCopy.Enabled = False
        MessageBox.Show(My.Resources.Strings.MultipleCopies)
        Me.Close()
    End Sub

    Private Sub InitialiseMenuFile()

        Dim fileItem As New ToolStripMenuItem
        fileItem.Text = My.Resources.Strings.mnu_File
        fileItem.Name = My.Resources.Strings.mnu_File
        Me.MainMenuStrip.Items.Add(fileItem)

        Dim loginItem As New ToolStripMenuItem
        loginItem.Text = My.Resources.Strings.mnu_Login
        loginItem.Name = My.Resources.Strings.mnu_Login
        loginItem.Image = New System.Drawing.Bitmap(My.Resources.logon)
        loginItem.ImageScaling = ToolStripItemImageScaling.None
        AddHandler loginItem.Click, AddressOf LoginClick

        fileItem.DropDownItems.Add(loginItem)
        fileItem.DropDownItems.Add("-")
        fileItem.DropDownItems.Add(My.Resources.Strings.mnu_Exit, Nothing, AddressOf Exit_Click)

    End Sub

    Private Sub InitialiseMenuMasters()

        If _userMenuProfiles Is Nothing Then
            Exit Sub
        End If

        Dim menus As MenuOption.ItemCollection = _workstationMenus.FindMasterMenus(0)
        For Each menuItem As MenuOption.Item In menus
            'check whether to add menu to strip
            For Each profile As MenuOption.Profile In _userMenuProfiles
                If profile.MenuId = menuItem.Id AndAlso profile.AccessAllowed Then
                    'create menu strip item
                    Dim stripItem As New ToolStripMenuItem
                    stripItem.Text = menuItem.Description
                    stripItem.Tag = menuItem.Id

                    'add menu and any sub menus 
                    Me.MainMenuStrip.Items.Add(stripItem)
                    InitialiseMenuSubMenus(stripItem, menuItem.Id)

                    'add click handler if no sub menus
                    If stripItem.DropDownItems.Count = 0 Then
                        AddHandler stripItem.Click, AddressOf MenuClick
                    End If
                End If
            Next
        Next

        'add window menu item
        Dim windowItem As New ToolStripMenuItem
        windowItem.Text = My.Resources.Strings.mnu_Window
        MenuStrip1.Items.Add(windowItem)
        MenuStrip1.MdiWindowListItem = windowItem

        windowItem.DropDownItems.Add(My.Resources.Strings.mnu_CloseAll, Nothing, AddressOf WindowCloseAll_Click)
        windowItem.DropDownItems.Add("-")
        windowItem.DropDownItems.Add(My.Resources.Strings.mnu_TileCascade, Nothing, AddressOf WindowCascade_Click)
        windowItem.DropDownItems.Add(My.Resources.Strings.mnu_TileHorizontal, Nothing, AddressOf WindowTileHoriz_Click)
        windowItem.DropDownItems.Add(My.Resources.Strings.mnu_TileVertical, Nothing, AddressOf WindowTileVert_Click)

    End Sub

    Private Sub InitialiseMenuSubMenus(ByRef stripItem As ToolStripMenuItem, ByVal menuId As Integer)

        If _userMenuProfiles Is Nothing Then
            Exit Sub
        End If

        Dim menus As MenuOption.ItemCollection = _workstationMenus.FindMasterMenus(menuId)

        For Each menuItem As MenuOption.Item In menus
            'check whether to make strip menu visible
            For Each profile As MenuOption.Profile In _userMenuProfiles
                If profile.MenuId = menuItem.Id AndAlso profile.AccessAllowed Then
                    'create tool strip item and add to strip
                    Dim item = CType(stripItem.DropDownItems.Add(menuItem.Description), ToolStripMenuItem)
                    item.ImageAlign = ContentAlignment.MiddleLeft
                    item.ImageScaling = ToolStripItemImageScaling.None
                    item.Tag = menuItem.Id
                    If menuItem.ImagePath IsNot Nothing AndAlso menuItem.ImagePath.Length > 0 Then
                        item.Image = New System.Drawing.Bitmap(menuItem.ImagePath)
                    Else
                        item.Image = New Drawing.Bitmap(My.Resources.logo)
                    End If

                    'add any sub menus and add click handler if no sub menus
                    InitialiseMenuSubMenus(item, menuItem.Id)
                    If item.DropDownItems.Count = 0 Then
                        AddHandler item.Click, AddressOf MenuClick
                    End If

                    Exit For
                End If
            Next
        Next

    End Sub

    Private Sub CheckForDailyOpen()

        'Is Workstation Open or Closed
        Dim isActive As Boolean = Workstation.GetActive(_workStationId)
        If isActive Then
            Me.Text &= Space(5) & "Workstation is Open"

        Else
            'Need to force Daily Open Program to run, find the Daily Open menu config record (14010)
            Me.Text &= Space(5) & "Workstation is Closed"
            For Each menuItem As MenuOption.Item In _workstationMenus
                If menuItem.Id = My.Settings.OpenCloseId Then
                    LoadApplication(menuItem, True)
                    Exit Sub
                End If
            Next

            Throw New Exception("Daily Open not found")
        End If

    End Sub

    Private Sub tmrIsClosed_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrIsClosed.Tick

        tmrIsClosed.Enabled = False
        Trace.WriteLine(My.Resources.Strings.HeartBeat, Name)

        'get workstation detail
        Dim workstationOpen As Boolean = Workstation.GetActive(_workStationId)

        'check if workstation open
        If workstationOpen Then
            Me.Text = Me.Text.Replace("Closed", "Open")
            Me.Refresh()
            Application.DoEvents()

        Else
            Me.Text = Me.Text.Replace("Open", "Closed")
            Me.Refresh()
            Application.DoEvents()

            'workstation is closed so check if night routine is running (exiting if so)
            If _workStationId = _workstationMasterId Then
                Dim nightlyTask As String = Dates.GetNightlyTask
                If (nightlyTask Is Nothing) OrElse (nightlyTask <> "000") Then
                    Exit Sub
                End If

                'Make sure that SYSNID:MAST = false
                Core.System.SystemOption.SetMasterOpen(False)
            End If

            MessageBox.Show(My.Resources.Strings.WorkstationClosedAndExit, "Auto Close", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            _user = Nothing
            Me.Close()
            Exit Sub
        End If

        'check if master open
        Dim masterOpen As Boolean = Core.System.SystemOption.GetMasterOpen
        If Not masterOpen AndAlso _workStationId <> _workstationMasterId Then

            'force daily open to run
            For Each menuItem As MenuOption.Item In _workstationMenus
                If menuItem.Id = My.Settings.OpenCloseId Then
                    LoadApplication(menuItem, True)

                    'log out and close application
                    MessageBox.Show(My.Resources.Strings.MasterNotAvailableAndExit, "Auto Close", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    _user = Nothing
                    Me.Close()
                    Exit Sub
                End If
            Next
        End If

        tmrIsClosed.Enabled = True

    End Sub

    Private Sub LoginClick(ender As Object, e As System.EventArgs)

        'get login menu item
        Dim loginMenu = CType(MenuStrip1.Items(My.Resources.Strings.mnu_File), ToolStripMenuItem)
        Dim login = CType(loginMenu.DropDownItems(My.Resources.Strings.mnu_Login), ToolStripMenuItem)

        'check whether to log in or out
        Select Case login.Text
            Case My.Resources.Strings.mnu_Login
                Using author As New WinForm.User.UserAuthorise()
                    If author.ShowDialog = Windows.Forms.DialogResult.OK Then
                        _user = author.User
                        login.Text = My.Resources.Strings.mnu_Logout
                        Me.Text &= Space(5) & _user.ToString

                        'record user logging in
                        ActivityLog.RecordUserLogIn(_user.Id, _workStationId)

                        'get menu configs allowed for this profile id along with security level
                        _userMenuProfiles = MenuOption.Profile.GetProfiles(_user.ProfileId)
                        InitialiseMenuMasters()
                    End If
                End Using

            Case My.Resources.Strings.mnu_Logout
                login.Text = My.Resources.Strings.mnu_Login
                Me.Text = My.Resources.Strings.AppTitle & Space(1) & _storeIdName

                'record user logging out
                ActivityLog.RecordUserLogOut(CStr(_user.Id), CStr(_workStationId), False)
                _user = Nothing
                _userMenuProfiles = Nothing

                'remove all menu items except file
                For index As Integer = Me.MainMenuStrip.Items.Count - 1 To 0 Step -1
                    If index > 0 Then Me.MainMenuStrip.Items.RemoveAt(index)
                Next
        End Select

    End Sub

    Private Sub MenuClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim menuId As Integer = CInt(CType(sender, ToolStripMenuItem).Tag)
        Dim menuItem As MenuOption.Item = _workstationMenus.Find(menuId)
        If menuItem IsNot Nothing Then
            LoadApplication(menuItem)
        End If
    End Sub

    Private Sub LoadApplication(ByVal menuItem As MenuOption.Item, Optional ByVal anonymousUser As Boolean = False)

        'check for anonymous user
        Dim userId As Integer = 0
        Dim securityLevel As Integer = 9
        If Not anonymousUser Then
            If _user Is Nothing Then Throw New Exception("No user is logged or information cannot be found")
            userId = _user.Id

            'get menu item and security level for this menu item and check they exist
            Using profile As MenuOption.Profile = _userMenuProfiles.Find(menuItem.Id, _user.ProfileId)
                If profile Is Nothing Then Throw New Exception("No menu profile available for this menu item and user")
                securityLevel = profile.SecurityLevel
            End Using
        End If

        'check menu item assembly is not nothing and load
        If (menuItem.AssemblyName Is Nothing) OrElse (menuItem.AssemblyName.Length = 0) Then
            Throw New Exception("No assembly name specified for this menu item")
        End If

        'check menu load type
        Try
            Select Case CType(menuItem.LoadType, MenuOption.LoadType)
                Case MenuOption.LoadType.Process
                    Dim parameters As String = menuItem.Parameters & Space(1)
                    Dim params() As String = parameters.Split("|"c)
                    For Each param As String In params
                        Dim newProcess As Process = Process.Start(menuItem.AssemblyName, param)
                        If menuItem.IsModal Then newProcess.WaitForExit()
                    Next

                Case Else
                    'check class name is not nothing
                    If (menuItem.ClassName Is Nothing) OrElse (menuItem.ClassName.Length = 0) Then
                        Throw New Exception("No class name specified for this menu item")
                    End If

                    'get assembly and class to load
                    Dim ass As Assembly = Assembly.LoadFrom(menuItem.AssemblyName)

                    If ass IsNot Nothing Then
                        Dim cls As Type = ass.GetType(menuItem.ClassName)

                        If cls IsNot Nothing Then
                            Dim params As String = String.Empty

                            If menuItem.Parameters IsNot Nothing Then
                                params = menuItem.Parameters
                            End If

                            Select Case cls.BaseType.ToString
                                Case "Cts.Oasys.WinForm.Form"
                                    Dim hostForm As New Cts.Oasys.WinForm.HostForm(menuItem.AssemblyName, menuItem.ClassName, _
                                                    menuItem.Description, userId, _workStationId, securityLevel, _
                                                    params, menuItem.ImagePath, menuItem.IsMaximised, 1)

                                    If hostForm IsNot Nothing Then
                                        'get activity log id needed to update log when app is closed
                                        hostForm.Tag = ActivityLog.RecordAppStarted(menuItem.Id, userId, _workStationId)
                                        AddHandler hostForm.FormClosed, AddressOf WindowClosed

                                        If menuItem.IsModal Then
                                            hostForm.Owner = Me
                                            hostForm.ShowDialog()
                                        Else
                                            hostForm.MdiParent = Me
                                            hostForm.Show()
                                        End If
                                    Else
                                        MessageBox.Show("Failed creating report for " & menuItem.Description & ".  Please contact CTS", "Cannot create report", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                                    End If

                                Case "OasysCTS.ModuleTabBase"
                                    MessageBox.Show("Deprecated base, contact CTS")
                            End Select
                        Else
                            MessageBox.Show("Assembly " & menuItem.AssemblyName & " does not contain class " & menuItem.ClassName & ".  Please contact CTS", "Cannot create report", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                        End If
                    Else
                        MessageBox.Show("Cannot find assembly " & menuItem.AssemblyName & ".  Please contact WSSDT", "Cannot create report", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                    End If
            End Select
        Catch exc As TargetInvocationException
            If TypeOf exc.InnerException Is Cts.Oasys.Core.Locking.LockException Then
            Else
                Throw exc
            End If
        End Try
    End Sub

    Private Sub WindowClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs)

        Select Case True
            Case TypeOf sender Is Cts.Oasys.WinForm.HostForm
                Dim hostForm = CType(sender, WinForm.HostForm)
                ActivityLog.RecordAppEnded(CInt(hostForm.Tag))

                'Log out user if needed
                If hostForm.LogOutUserAfterClose Then
                    'get login menu item
                    Dim loginMenu = CType(MenuStrip1.Items(My.Resources.Strings.mnu_File), ToolStripMenuItem)
                    Dim login = CType(loginMenu.DropDownItems(My.Resources.Strings.mnu_Login), ToolStripMenuItem)

                    If login.Text = My.Resources.Strings.mnu_Logout Then
                        Call LoginClick(Nothing, EventArgs.Empty)
                    End If
                End If
        End Select

    End Sub

    Private Sub WindowCloseAll_Click(sender As Object, e As System.EventArgs)
        For Each child As Windows.Forms.Form In Me.MdiChildren
            child.Close()
        Next
    End Sub

    Private Sub WindowCascade_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub WindowTileHoriz_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub WindowTileVert_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub Exit_Click(ender As Object, e As System.EventArgs)
        Me.Close()
    End Sub

End Class