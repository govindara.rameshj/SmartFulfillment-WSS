﻿Imports System.Reflection

Public Class frmSecurityModel_Test

    Private _allMenus As MenuOption.ItemCollection
    Private _allUsers As UserCollection
    Private _allWorkStations As Workstation.WorkstationCollection
    Private _user As User = Nothing
    Private _going As Boolean

    Private Sub butGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butGo.Click

        If cboWorkStation.SelectedItem IsNot Nothing AndAlso TypeOf cboWorkStation.SelectedItem Is Workstation.Workstation Then
            Dim WkSt As Workstation.Workstation = CType(cboWorkStation.SelectedItem, Workstation.Workstation)

            If cboUser.SelectedItem IsNot Nothing AndAlso TypeOf cboUser.SelectedItem Is User Then
                Dim Usr As User = CType(cboUser.SelectedItem, User)

                _going = True
                txtResults.Text = ""
                RunTest(WkSt, Usr)
                _going = False
            End If
        End If
    End Sub

    Private Sub cboWorkStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboWorkStation.SelectedIndexChanged

        SetupUI()
    End Sub

    Private Sub cboUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUser.SelectedIndexChanged

        SetupUI()
    End Sub

    Private Sub frmSecurityModel_Test_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If _going Then
            MsgBox("Cannot close whilst runnning the automated security check", MsgBoxStyle.Exclamation, "Not closing")
        End If
        e.Cancel = _going
    End Sub

    Private Sub frmSecurityModel_Test_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor
            _allUsers = Cts.Oasys.Core.System.User.GetUsers()
            _allMenus = MenuOption.Item.GetMenus()
            _allWorkStations = Workstation.GetWorkstations()
            SetupUI()
            Me.Show()
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    'Private Sub optAutoSecurityCheck_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAutoSecurityCheck.CheckedChanged

    '    SetupUI()
    'End Sub

    'Private Sub optManualVisual_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optManualVisual.CheckedChanged

    '    SetupUI()
    'End Sub

    Private Sub SetupUI()

        With cboWorkStation
            If .Items.Count = 0 Then
                ' Specify (WorkStation) method to provide text to to display in dropdown list,
                ' so can keep whole object in list for later use
                .DisplayMember = "IdDescription"
                If _allWorkStations IsNot Nothing Then
                    For Each WkSt As Workstation.Workstation In _allWorkStations
                        .Items.Add(WkSt)
                    Next
                End If
            End If
        End With
        With cboUser
            If .Items.Count = 0 Then
                ' Specify (User) method to provide text to to display in dropdown list
                ' so can keep whole object in list for later use
                .DisplayMember = "IdName"
                If _allUsers IsNot Nothing Then
                    For Each Usr As User In _allUsers
                        .Items.Add(Usr)
                    Next
                End If
            End If
        End With
        butGo.Enabled = cboWorkStation.SelectedItem IsNot Nothing And cboUser.SelectedItem IsNot Nothing
    End Sub

    Private Function LoadApplication(ByVal menuItem As MenuOption.Item, ByVal TestWorkStation As Workstation.Workstation, ByVal TestUser As User) As String
        'check for anonymous user
        Dim userId As Integer = 0
        Dim securityLevel As Integer = 9

        LoadApplication = ""
        userId = TestUser.Id

        'get menu item and security level for this menu item and check they exist
        Using profile As MenuOption.Profile = MenuOption.Profile.GetProfiles(TestUser.ProfileId).Find(menuItem.Id, TestUser.ProfileId)
            If profile Is Nothing Then
                LoadApplication = "Load Application failure: No menu profile available for this menu item and user"
            Else
                securityLevel = profile.SecurityLevel
                'check menu item assembly is not nothing and load
                If (menuItem.AssemblyName Is Nothing) OrElse (menuItem.AssemblyName.Length = 0) Then
                    If menuItem.LoadType <> MenuOption.LoadType.MenuGroup Then
                        LoadApplication = "Load Application failure: No assembly name specified for this menu item"
                    End If
                Else
                    'check menu load type
                    Select Case CType(menuItem.LoadType, MenuOption.LoadType)
                        Case MenuOption.LoadType.Process
                            If optAutoSecurityCheck.Checked Then
                                LoadApplication = "Load Application Inconclusive: Cannot run security check on 'Process Type' of application '" & menuItem.AssemblyName & "'"
                            Else
                                Dim parameters As String = menuItem.Parameters & Space(1)
                                Dim params() As String = parameters.Split("|"c)

                                For Each param As String In params
                                    Try
                                        Dim newProcess As Process = Process.Start(menuItem.AssemblyName, param)

                                        If menuItem.IsModal Then
                                            newProcess.WaitForExit()
                                        End If
                                        LoadApplication = "Load Application Manual Test completed."
                                    Catch ex As Exception
                                        LoadApplication = String.Format("Load Application Manual Test failure: Problem loading 'Process Type' application '{0}' with parameters ({1}).  {2}", menuItem.AssemblyName, IIf(param.Length = 0, "none", param), ex.Message)
                                    End Try
                                Next
                            End If
                        Case Else
                            'check class name is not nothing
                            If (menuItem.ClassName Is Nothing) OrElse (menuItem.ClassName.Length = 0) Then
                                LoadApplication = "Load Application failure: No class name specified for this menu item"
                            Else
                                Try
                                    'get assembly and class to load
                                    Dim ass As Assembly = Assembly.LoadFrom(menuItem.AssemblyName)

                                    If ass IsNot Nothing Then
                                        Dim cls As Type = ass.GetType(menuItem.ClassName)

                                        If cls IsNot Nothing Then
                                            Dim params As String = String.Empty

                                            If menuItem.Parameters IsNot Nothing Then
                                                params = menuItem.Parameters
                                            End If

                                            Select Case cls.BaseType.ToString
                                                Case "Cts.Oasys.WinForm.Form"
                                                    Dim hostForm As New Cts.Oasys.WinForm.HostForm(menuItem.AssemblyName, menuItem.ClassName, _
                                                                    menuItem.Description, userId, TestWorkStation.Id, securityLevel, _
                                                                    params, menuItem.ImagePath, menuItem.IsMaximised, 1)

                                                    If hostForm IsNot Nothing Then
                                                        'get activity log id needed to update log when app is closed
                                                        hostForm.Tag = ActivityLog.RecordAppStarted(menuItem.Id, userId, TestWorkStation.Id)
                                                        AddHandler hostForm.FormClosed, AddressOf WindowClosed

                                                        If optAutoSecurityCheck.Checked Then
                                                            Dim Message As String = "No security test run"

                                                            Select Case hostForm.DoSecurityCheck(Message)
                                                                Case Cts.Oasys.WinForm.Form.SecurityCheckResult.HasNoTest
                                                                    LoadApplication = "Load Application Automated Security Check success: Application '" & menuItem.Description & "' does not have any security checks (" & Message & ")"
                                                                Case Cts.Oasys.WinForm.Form.SecurityCheckResult.Success
                                                                    LoadApplication = "Load Application Automated Security Check success: Application '" & menuItem.Description & "' passed the security checks (" & Message & ")"
                                                                Case Cts.Oasys.WinForm.Form.SecurityCheckResult.Failure
                                                                    LoadApplication = "Load Application Automated Security Check failure: Application '" & menuItem.Description & "' failed the security checks (" & Message & ")"
                                                                Case Else
                                                                    LoadApplication = "Load Application Automated Security Check inconclusive: No security test run for application '" & menuItem.Description & "' (" & Message & ")"
                                                            End Select
                                                        Else
                                                            If menuItem.IsModal Then
                                                                hostForm.Owner = Me
                                                                hostForm.ShowDialog()
                                                            Else
                                                                Me.IsMdiContainer = True
                                                                hostForm.MdiParent = Me
                                                                hostForm.Show()
                                                            End If
                                                            LoadApplication = "Load Application Manual Test completed."
                                                        End If
                                                    Else
                                                        LoadApplication = "Load Application failure: Failed creating report for " & menuItem.Description & ".  Please contact WSS Development Team"
                                                    End If
                                                Case "OasysCTS.ModuleTabBase"
                                                    LoadApplication = "Load Application failure: Deprecated base, contact WSS Development Team"
                                            End Select
                                        Else
                                            LoadApplication = "Load Application failure: Assembly " & menuItem.AssemblyName & " does not contain class " & menuItem.ClassName & ".  Please contact WSS Development Team"
                                        End If
                                    Else
                                        LoadApplication = "Load Application failure: Cannot find assembly " & menuItem.AssemblyName & ".  Please contact WSS Development Team"
                                    End If
                                Catch ex As Exception
                                    LoadApplication = "Load Application failure: " & ex.Message
                                End Try
                            End If
                    End Select
                End If
            End If
        End Using
    End Function

    'Private Sub RunTests()

    '    If _allWorkStations IsNot Nothing Then
    '        If _allUsers IsNot Nothing Then

    '            For Each TestWorkStation As Workstation.Workstation In _allWorkStations
    '                For Each TestUser As User In _allUsers
    '                    RunTest(TestWorkStation, TestUser)
    '                Next
    '            Next
    '        Else
    '            txtResults.Text = "Failed.  No users available to test."
    '        End If
    '    Else
    '        txtResults.Text = "Failed.  No workstations available to test."
    '    End If
    'End Sub

    Private Sub RunTest(ByVal TestWorkStation As Workstation.Workstation, ByVal TestUser As User)
        Dim MasterMenus As MenuOption.ItemCollection = _allMenus.FindMasterMenus(0)

        txtResults.Text &= vbNewLine & "WorkStation: " & TestWorkStation.IdDescription & vbTab & " User: " & TestUser.IdName
        For Each MasterMenu As MenuOption.Item In MasterMenus
            txtResults.Text &= vbNewLine & vbTab & MasterMenu.Description
            For Each profile As MenuOption.Profile In MenuOption.Profile.GetProfiles(TestUser.ProfileId)
                If profile.MenuId = MasterMenu.Id Then
                    txtResults.Text &= String.Format("{0}{1}{1}Access {2}allowed.", vbNewLine, vbTab, IIf(profile.AccessAllowed, "", "not "))
                    txtResults.Refresh()
                    CheckSubMenus(TestWorkStation, TestUser, MasterMenu.Id, 1)
                End If
            Next
        Next
        MsgBox(String.Format("{0} checks finished.", IIf(optManualVisual.Checked, "Manual", "Automated")), MsgBoxStyle.Information, "Test Completed")
    End Sub

    Private Sub CheckSubMenus(ByVal TestWorkStation As Workstation.Workstation, ByVal TestUser As User, ByVal MenuId As Integer, ByVal Depth As Integer)

        If _allUsers IsNot Nothing Then
            Dim menus As MenuOption.ItemCollection = _allMenus.FindMasterMenus(MenuId)

            For Each menuItem As MenuOption.Item In menus
                For Each profile As MenuOption.Profile In MenuOption.Profile.GetProfiles(TestUser.ProfileId)
                    If profile.MenuId = menuItem.Id AndAlso profile.AccessAllowed Then
                        'check whether to make strip menu visible
                        If profile.MenuId = menuItem.Id Then
                            txtResults.Text &= vbNewLine & New String(CChar(vbTab), Depth + 2) & menuItem.Description
                            txtResults.Text &= vbNewLine & New String(CChar(vbTab), Depth + 3) & String.Format("Access {0}allowed.", IIf(profile.AccessAllowed, "", "not "))
                            txtResults.Text &= vbNewLine & New String(CChar(vbTab), Depth + 3) & LoadApplication(menuItem, TestWorkStation, TestUser)
                            txtResults.Refresh()
                        End If
                        ' check any sub menus
                        CheckSubMenus(TestWorkStation, TestUser, menuItem.Id, Depth + 1)
                        Exit For
                    End If
                Next
            Next
        Else
            txtResults.Text = "Failed.  No users available to test."
        End If
    End Sub

    Private Sub WindowClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs)

        Me.IsMdiContainer = False
        If TypeOf sender Is Cts.Oasys.WinForm.HostForm Then
            Dim hostForm As Cts.Oasys.WinForm.HostForm = CType(sender, Cts.Oasys.WinForm.HostForm)

            ' Record the closure and any error
        End If
    End Sub
End Class
