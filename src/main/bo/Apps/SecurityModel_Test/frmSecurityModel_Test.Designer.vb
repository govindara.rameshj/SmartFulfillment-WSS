﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSecurityModel_Test
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtResults = New System.Windows.Forms.TextBox
        Me.optAutoSecurityCheck = New System.Windows.Forms.RadioButton
        Me.optManualVisual = New System.Windows.Forms.RadioButton
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblProfile = New System.Windows.Forms.Label
        Me.cboWorkStation = New System.Windows.Forms.ComboBox
        Me.lblWorkstation = New System.Windows.Forms.Label
        Me.butGo = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtResults
        '
        Me.txtResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResults.Location = New System.Drawing.Point(12, 129)
        Me.txtResults.Multiline = True
        Me.txtResults.Name = "txtResults"
        Me.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtResults.Size = New System.Drawing.Size(680, 399)
        Me.txtResults.TabIndex = 5
        Me.txtResults.WordWrap = False
        '
        'optAutoSecurityCheck
        '
        Me.optAutoSecurityCheck.AutoSize = True
        Me.optAutoSecurityCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.optAutoSecurityCheck.Location = New System.Drawing.Point(80, 95)
        Me.optAutoSecurityCheck.Name = "optAutoSecurityCheck"
        Me.optAutoSecurityCheck.Size = New System.Drawing.Size(151, 17)
        Me.optAutoSecurityCheck.TabIndex = 0
        Me.optAutoSecurityCheck.Text = "Automated Security Check"
        Me.optAutoSecurityCheck.UseVisualStyleBackColor = True
        '
        'optManualVisual
        '
        Me.optManualVisual.AutoSize = True
        Me.optManualVisual.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.optManualVisual.Checked = True
        Me.optManualVisual.Location = New System.Drawing.Point(96, 72)
        Me.optManualVisual.Name = "optManualVisual"
        Me.optManualVisual.Size = New System.Drawing.Size(135, 17)
        Me.optManualVisual.TabIndex = 1
        Me.optManualVisual.TabStop = True
        Me.optManualVisual.Text = "Manual Security Check"
        Me.optManualVisual.UseVisualStyleBackColor = True
        '
        'cboUser
        '
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(85, 35)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(146, 21)
        Me.cboUser.TabIndex = 3
        '
        'lblProfile
        '
        Me.lblProfile.AutoSize = True
        Me.lblProfile.Location = New System.Drawing.Point(50, 38)
        Me.lblProfile.Name = "lblProfile"
        Me.lblProfile.Size = New System.Drawing.Size(29, 13)
        Me.lblProfile.TabIndex = 2
        Me.lblProfile.Text = "User"
        '
        'cboWorkStation
        '
        Me.cboWorkStation.FormattingEnabled = True
        Me.cboWorkStation.Location = New System.Drawing.Point(85, 8)
        Me.cboWorkStation.Name = "cboWorkStation"
        Me.cboWorkStation.Size = New System.Drawing.Size(146, 21)
        Me.cboWorkStation.TabIndex = 1
        '
        'lblWorkstation
        '
        Me.lblWorkstation.AutoSize = True
        Me.lblWorkstation.Location = New System.Drawing.Point(15, 11)
        Me.lblWorkstation.Name = "lblWorkstation"
        Me.lblWorkstation.Size = New System.Drawing.Size(64, 13)
        Me.lblWorkstation.TabIndex = 0
        Me.lblWorkstation.Text = "Workstation"
        '
        'butGo
        '
        Me.butGo.Location = New System.Drawing.Point(249, 89)
        Me.butGo.Name = "butGo"
        Me.butGo.Size = New System.Drawing.Size(75, 23)
        Me.butGo.TabIndex = 4
        Me.butGo.Text = "Go"
        Me.butGo.UseVisualStyleBackColor = True
        '
        'frmSecurityModel_Test
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 539)
        Me.Controls.Add(Me.cboUser)
        Me.Controls.Add(Me.lblProfile)
        Me.Controls.Add(Me.butGo)
        Me.Controls.Add(Me.cboWorkStation)
        Me.Controls.Add(Me.lblWorkstation)
        Me.Controls.Add(Me.optManualVisual)
        Me.Controls.Add(Me.optAutoSecurityCheck)
        Me.Controls.Add(Me.txtResults)
        Me.Name = "frmSecurityModel_Test"
        Me.Text = "Security Model Test"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtResults As System.Windows.Forms.TextBox
    Friend WithEvents optAutoSecurityCheck As System.Windows.Forms.RadioButton
    Friend WithEvents optManualVisual As System.Windows.Forms.RadioButton
    Friend WithEvents cboWorkStation As System.Windows.Forms.ComboBox
    Friend WithEvents lblWorkstation As System.Windows.Forms.Label
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents lblProfile As System.Windows.Forms.Label
    Friend WithEvents butGo As System.Windows.Forms.Button

End Class
