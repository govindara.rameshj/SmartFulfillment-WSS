using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cts.Oasys.Core.Net4Replacements;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.RemoteServices.ProxyService.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;
using WSS.BO.RemoteServices.UserManagementService.Client;

namespace ExternalRequestMonitor
{
    public class RemoteServicesFactory : IRemoteServicesFactory
    {
        public Lazy<ProxyServiceClient> ProxyServiceClient;
        public Lazy<UserManagementServiceClient> UserManagementServiceClient;

        public RemoteServicesFactory(Settings settings)
        {
            ProxyServiceClient = new Lazy<ProxyServiceClient>(() => new ProxyServiceClient(settings.ApiAddress));
            UserManagementServiceClient = new Lazy<UserManagementServiceClient>(() => new UserManagementServiceClient(settings.UserManagmentServiceUrl));
        }

        public IAddressLookupService GetAddressLookupService()
        {
            return ProxyServiceClient.Value;
        }

        public ICapacityService GetCapacityService()
        {
            return ProxyServiceClient.Value;
        }

        public IDeliveryNotificationService GetDeliveryNotificationService()
        {
            return ProxyServiceClient.Value;
        }

        public IUserManagementService GetUserManagementService()
        {
            return UserManagementServiceClient.Value;
        }
    }
}
