using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;
using WSS.BO.RemoteServices.UserManagementService.Client;

namespace ExternalRequestMonitor
{
    public interface IRemoteServicesFactory
    {
        IAddressLookupService GetAddressLookupService();
        ICapacityService GetCapacityService();
        IDeliveryNotificationService GetDeliveryNotificationService();
        IUserManagementService GetUserManagementService();
    }
}
