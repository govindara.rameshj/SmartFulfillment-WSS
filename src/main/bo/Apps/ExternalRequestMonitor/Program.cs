using System;
using NLog;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Model;
using slf4net;
using WSS.BO.DataLayer.Model.Repositories;

namespace ExternalRequestMonitor
{
    public class Program
    {
        private static NLog.Logger logger = null;

        public static void Main()
        {
            try
            {
                logger = LogManager.GetLogger("ExternalRequestMonitor");
                logger.Trace("Application started");

                InitLogging();

                var resolver = new SimpleResolver();
                var dataLayerFactory = resolver.Resolve<IDataLayerFactory>();
                var remoteService = new RemoteServicesFactory(dataLayerFactory.Create<IDictionariesRepository>().GetSettings());
                var monitor = new ExternalRequestMonitor(dataLayerFactory, remoteService);
                monitor.GetPendingRequests().ForEach(r => monitor.ProcessRequest(r));
            }
            catch (Exception exc)
            {
                if (logger != null)
                    logger.Fatal("Unhandled exception: ", exc);
                Environment.ExitCode = 1;
            }
        }

        private static void InitLogging()
        {
            var factory = new slf4net.NLog.NLogLoggerFactory();
            var resolver = new SimpleFactoryResolver(factory);
            LoggerFactory.SetFactoryResolver(resolver);
        }
    }

    public class SimpleFactoryResolver : IFactoryResolver
    {
        private ILoggerFactory _factory;

        public SimpleFactoryResolver(ILoggerFactory factory)
        {
            _factory = factory;
        }

        public ILoggerFactory GetFactory()
        {
            return _factory;
        }
    }
}
