using System;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;
using slf4net;
using WSS.BO.RemoteServices.Common.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;
using WSS.BO.RemoteServices.UserManagementService.Client;

namespace ExternalRequestMonitor
{
    public class ExternalRequestMonitor
    {
        private readonly string requestTemplate = "Request (Id={0}){1}";
        private readonly string requestTemplateWithInfo = "Request (Id={0}){1}{2}";

        private readonly ILogger logger;
        private readonly IExternalRequestRepository repo;
        private readonly IDataLayerFactory datalayerFactory;
        private readonly IRemoteServicesFactory remoteServicesFactory;
        private readonly Settings settings;

        public ExternalRequestMonitor(IDataLayerFactory dlFactory, IRemoteServicesFactory rsFactory)
        {
            logger = LoggerFactory.GetLogger(typeof(ExternalRequestMonitor));
            datalayerFactory = dlFactory;
            remoteServicesFactory = rsFactory;
            repo = dlFactory.Create<IExternalRequestRepository>();
            settings = dlFactory.Create<IDictionariesRepository>().GetSettings();
        }

        #region General methods

        public List<ExternalRequest> GetPendingRequests()
        {
            logger.Info("Getting pending requests");
            var requests = repo.GetRequestsForProcessing(settings.RetryPeriodMinutes);
            logger.Info(string.Format("{0} request(s) returned", requests.Count));

            return requests;
        }

        public void ProcessRequest(ExternalRequest externalRequest)
        {
            // Do not catch errors from DB, let processing to continue on the next task run.

            var success = false;

            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": processing started"));
            try
            {
                SendRequest(externalRequest);
                success = true;
            }
            catch (BaseServiceClientException e)
            {
                var status = (e.Severity > 0 || externalRequest.RetriesCount >= settings.MaxRetriesNumber - 1) ? ExternalRequestStatus.Failed : ExternalRequestStatus.Retry;
                if (status == ExternalRequestStatus.Failed)
                {
                    logger.Error(string.Format(requestTemplate, externalRequest.Id, ": failed with error: ") + e.Message);
                }
                else
                {
                    logger.Warn(string.Format(requestTemplate, externalRequest.Id, ": failed with error: ") + e.Message);
                }
                ProcessRequestExecution(externalRequest, status, e.Message);
            }
            catch (Exception e)
            {
                logger.Error(string.Format(requestTemplate, externalRequest.Id, ": failed with unexpected error: ") + e.Message);
                ProcessRequestExecution(externalRequest, ExternalRequestStatus.Failed, e.Message);
            }

            if (success)
            {
                logger.Info(string.Format(requestTemplate, externalRequest.Id, ": successfully processed"));
                ProcessRequestExecution(externalRequest, ExternalRequestStatus.Processed, null);
            }
        }

        private void ProcessRequestExecution(ExternalRequest externalRequest, int status, string message)
        {
            repo.UpdateRequest(externalRequest.Id, status, message);

            if (externalRequest.Type == ExternalRequestType.InsertUser || externalRequest.Type == ExternalRequestType.UpdateUser || externalRequest.Type == ExternalRequestType.DeleteUser)
            {
                var requestDetails = GetExternalRequestDetails<UserManagmentRequestDetails>(externalRequest);
                var repoUser = datalayerFactory.Create<IUserRepository>();
                switch (status)
                {
                    case ExternalRequestStatus.Processed:
                    {
                        repoUser.UpdateSynchronizedWhen(Convert.ToInt32(requestDetails.Id));
                        break;
                    }
                    case ExternalRequestStatus.Failed:
                    {
                        repoUser.UpdateSynchronizationFailedWhen(Convert.ToInt32(requestDetails.Id));
                        break;
                    }
                    case ExternalRequestStatus.Retry:
                    {
                        break;
                    }
                    default:
                    {
                        throw new ApplicationException("Unsupported external requst type.");
                    }
                }
            }
        }

        private void SendRequest(ExternalRequest request)
        {
            switch (request.Type)
            {
                case ExternalRequestType.Allocate:
                    {
                        var requestDetails = GetExternalRequestDetails<UpdateCapacityRequestDetails>(request);
                        SendAllocationRequest(request, FillAllocationDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.Deallocate:
                    {
                        var requestDetails = GetExternalRequestDetails<UpdateCapacityRequestDetails>(request);
                        SendDeallocationRequest(request, FillAllocationDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.AllocateDeallocate:
                    {
                        var requestDetails = GetExternalRequestDetails<UpdateCapacityRequestDetailsExtended>(request);
                        SendAllocationRequest(request, FillAllocationDetails(requestDetails));
                        SendDeallocationRequest(request, FillAllocationDetailsForOldDate(requestDetails));
                    }
                    break;
                case ExternalRequestType.NewConsignment:
                    {
                        var requestDetails = GetExternalRequestDetails<ConsignmentRequestDetailsExtended>(request);
                        SendNewConsignmentRequest(request, FillConsignmentDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.UpdateConsignment:
                    {
                        var requestDetails = GetExternalRequestDetails<ConsignmentRequestDetailsExtended>(request);
                        SendUpdateConsignmentRequest(request, FillConsignmentDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.CancelConsignment:
                    {
                        var requestDetails = GetExternalRequestDetails<ConsignmentRequestDetails>(request);
                        SendCancelConsignmentRequest(request, FillConsignmentDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.InsertUser:
                    {
                        var requestDetails = GetExternalRequestDetails<UserManagmentRequestDetails>(request);
                        SendInsertUserRequest(request, FillUserManagementDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.UpdateUser:
                    {
                        var requestDetails = GetExternalRequestDetails<UserManagmentRequestDetails>(request);
                        SendUpdateUserRequest(request, FillUserManagementDetails(requestDetails));
                    }
                    break;
                case ExternalRequestType.DeleteUser:
                    {
                        var requestDetails = GetExternalRequestDetails<UserManagmentRequestDetails>(request);
                        SendDeleteUserRequest(request, FillUserManagementDetails(requestDetails));
                    }
                    break;
                default:
                    throw new ApplicationException(String.Format("Unknown ExternalRequestType=[{0}]", request.Type));
            }
        }

        private T GetExternalRequestDetails<T>(ExternalRequest request) where T : IExternalRequestDetails
        {
            var requestDetails = request.GetDetails<T>();
            logger.Info(string.Format(requestTemplateWithInfo, request.Id, " details: ", requestDetails.GetDescription()));

            return requestDetails;
        }

        private void SendAllocationRequest(ExternalRequest externalRequest, AllocationDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate Allocate request"));
            remoteServicesFactory.GetCapacityService().AllocateCapacity(details);
        }

        private void SendDeallocationRequest(ExternalRequest externalRequest, AllocationDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate Deallocate request"));
            remoteServicesFactory.GetCapacityService().DeallocateCapacity(details);
        }

        private void SendNewConsignmentRequest(ExternalRequest externalRequest, ConsignmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate NewConsignment request"));
            remoteServicesFactory.GetDeliveryNotificationService().NewConsignment(details);
        }

        private void SendUpdateConsignmentRequest(ExternalRequest externalRequest, ConsignmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate UpdateConsignment request"));
            remoteServicesFactory.GetDeliveryNotificationService().UpdateConsignment(details);
        }

        private void SendCancelConsignmentRequest(ExternalRequest externalRequest, ConsignmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate CancelConsignment request"));
            remoteServicesFactory.GetDeliveryNotificationService().CancelConsignment(details);
        }

        private void SendInsertUserRequest(ExternalRequest externalRequest, UserManagmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate InsertUser request"));
            remoteServicesFactory.GetUserManagementService().NewUser(details);
        }

        private void SendUpdateUserRequest(ExternalRequest externalRequest, UserManagmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate UpdateUser request"));
            remoteServicesFactory.GetUserManagementService().UpdateUser(details);
        }

        private void SendDeleteUserRequest(ExternalRequest externalRequest, UserManagmentDetails details)
        {
            logger.Info(string.Format(requestTemplate, externalRequest.Id, ": generate DeleteUser request"));
            remoteServicesFactory.GetUserManagementService().UpdateUser(details);
        }

        #endregion

        #region Helpers

        private AllocationDetails FillAllocationDetails(UpdateCapacityRequestDetails requestDetails)
        {
            return new AllocationDetails()
            {
                FulfillerId = requestDetails.FulfillerId,
                Date = requestDetails.DeliveryDate,
                SlotId = requestDetails.SlotId
            };
        }

        private AllocationDetails FillAllocationDetailsForOldDate(UpdateCapacityRequestDetailsExtended requestDetails)
        {
            return new AllocationDetails()
            {
                FulfillerId = requestDetails.FulfillerId,
                Date = requestDetails.OldDeliveryDate,
                SlotId = requestDetails.OldSlotId
            };
        }

        private ConsignmentDetails FillConsignmentDetails(ConsignmentRequestDetails requestDetails)
        {
            var details = new ConsignmentDetails()
            {
                ConsignmentNumber = requestDetails.ConsignmentNumber,
                FulfillerNumber = requestDetails.FulfillerNumber
            };
            var detailsExtended = requestDetails as ConsignmentRequestDetailsExtended;
            if (detailsExtended != null)
            {
                details.FulfillerName = detailsExtended.FulfillerName;
                details.DeliveryDate = detailsExtended.DeliveryDate;
                details.CustomerMobilePhone = detailsExtended.CustomerMobilePhone;
                details.CustomerContactPhone = detailsExtended.CustomerContactPhone;
                details.CustomerHomePhone = detailsExtended.CustomerHomePhone;
                details.CustomerWorkPhone = detailsExtended.CustomerWorkPhone;
                details.CustomerName = detailsExtended.CustomerName;
                details.PostCode = detailsExtended.PostCode;
                details.AddressLine1 = detailsExtended.AddressLine1;
                details.AddressLine2 = detailsExtended.AddressLine2;
                details.AddressLine3 = detailsExtended.AddressLine3;
                details.AddressLine4 = detailsExtended.AddressLine4;
                details.CustomerOrderNumber = detailsExtended.CustomerOrderNumber;
                details.OriginatingStoreNumber = detailsExtended.OriginatingStoreNumber;
                details.OriginatingStoreName = detailsExtended.OriginatingStoreName;
            }
            return details;
        }

        private UserManagmentDetails FillUserManagementDetails(UserManagmentRequestDetails requestDetails)
        {
            return new UserManagmentDetails()
            {
                Id = requestDetails.Id,
                Code = requestDetails.Code,
                Name = requestDetails.Name,
                IsDeleted = requestDetails.IsDeleted,
                SecurityProfileId = requestDetails.SecurityProfileId,
                StoreId = requestDetails.StoreId,
                PasswordHash = requestDetails.PasswordHash
            };
        }

        #endregion
    }
}
    
