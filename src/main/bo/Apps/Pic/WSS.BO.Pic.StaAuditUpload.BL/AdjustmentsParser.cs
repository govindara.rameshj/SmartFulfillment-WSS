﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace WSS.BO.Pic.StaAuditUpload.BL
{
    public class AdjustmentsParser
    {
        public List<Adjustment> Parse(string fileName)
        {
            List<Adjustment> adjustments;
            var deserializer = new XmlSerializer(typeof(List<Adjustment>), CreateRootAttribute());
            
            using (StreamReader reader = new StreamReader(fileName))
            {
                adjustments = (List<Adjustment>)deserializer.Deserialize(reader);
            }

            return adjustments;
        }

        private XmlRootAttribute CreateRootAttribute()
        {
            var root = new XmlRootAttribute();
            root.ElementName = "Adjustments";
            root.IsNullable = true;
            return root;
        }
    }
}
