using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Constants;
using Cts.Oasys.Core.Helpers;
using slf4net;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.BO.Pic.StaAuditUpload.BL
{
    public class AdjustmentsProcessor
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(AdjustmentsProcessor));

        private IDataLayerFactory dataLayerFactory;
        private DateTime currentDate;
        private List<Adjustment> processedAdjustmentSkuNumbers;

        private string normalStockAdjustmentType;
        private string markdownStockAdjustmentType;

        public AdjustmentsProcessor(IDataLayerFactory dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
        }

        public void Process(List<Adjustment> adjustments, Action<int, int, int> reportProgress)
        {
            currentDate = DateTime.Now;

            int processedAdjustmentsCount = 0;
            int errorsCount = 0;

            CheckStockAdjustmentCodesExist();

            processedAdjustmentSkuNumbers = new List<Adjustment>();

            foreach (var adjustment in adjustments)
            {
                try
                {
                    Logger.Info("Adjustments processing for SKU {0} was started", adjustment.Sku);

                    ProcessAdjustment(adjustment);

                    processedAdjustmentSkuNumbers.Add(adjustment);

                    processedAdjustmentsCount++;

                    Logger.Info("Adjustments processing for SKU {0} was successfully finished", adjustment.Sku);
                }
                catch (Exception exception)
                {
                    Logger.Error("Error was caugth during ajustment processing: {0}", exception.Message);

                    errorsCount++;

                    Logger.Info("Adjustments processing for SKU {0} failed", adjustment.Sku);
                }

                reportProgress(adjustments.Count(), processedAdjustmentsCount, errorsCount);
            }
        }

        private void ProcessAdjustment(Adjustment adjustment)
        {
            using (var unitOfWork = dataLayerFactory.CreateUnitOfWork())
            {
                var stockRepo = unitOfWork.Create<StockRepository>();

                var stkmas = GetStockMasterIfExists(adjustment.Sku, stockRepo);

                if (adjustment.StandardStockAdjustment != 0)
                {
                    ProcessAdjustmentQuantity(adjustment.Sku, adjustment.StandardStockAdjustment, true, stockRepo, stkmas);
                }

                if (adjustment.MarkdownStockAdjustment != 0)
                {
                    ProcessAdjustmentQuantity(adjustment.Sku, adjustment.MarkdownStockAdjustment, false, stockRepo, stkmas);
                }

                unitOfWork.Commit();
            }
        }

        private void ProcessAdjustmentQuantity(string skuNumber, decimal quantity, bool isNormalStock, StockRepository stockRepo, StockMaster stkmas)
        {
            if (!CheckAdjustmentExists(stockRepo, skuNumber, isNormalStock))
            {
                var stockAdjustment = CreateStockAdjustment(skuNumber, quantity, isNormalStock, stkmas);
                stockRepo.InsertIntoStockAdjustment(stockAdjustment);

                var stockUpdate = CreateStockUpdate(skuNumber, quantity, isNormalStock, stkmas);
                stockRepo.ApplyStockUpdate(stockUpdate);
            }
        }

        private StockAdjustmentFile CreateStockAdjustment(string skuNumber, decimal quantity, bool isNormalStock, StockMaster stkmas)
        {
            string stockAdjustmentCodeNumber = GetStockAdjustmentCodeNumber(isNormalStock);

            return new StockAdjustmentFile()
            {
                AdjustmentAuthoriser = "IT",
                AdjustmentComment = "PIC - Audit Count",
                Code = stockAdjustmentCodeNumber,
                Date = currentDate.Date,
                IsStaAuditUpload = true,
                MovedOrWriteOff = isNormalStock ? StockAdjustmentMovedOrWriteOffType.NormalStockType : StockAdjustmentMovedOrWriteOffType.MovedStockType,
                Quantity = quantity,
                Price = stkmas.Price,
                RtiFlag = RtiStatus.ToBeSent,
                SequenceNumber = GetAdjustmentSequenceNumber(skuNumber, isNormalStock),
                SkuNumber = skuNumber,
                StartingStockOnHand = stkmas.StockOnHand,
                Type = isNormalStock ? normalStockAdjustmentType : markdownStockAdjustmentType         
            };
        }

        private StockUpdate CreateStockUpdate(string skuNumber, decimal quantity, bool isNormalStock, StockMaster stkmas)
        {
            return new StockUpdate()
            {
                SkuNumber = skuNumber,
                CashierNumber = null,
                DateOfMovement = currentDate.Date,
                TimeOfMovement = ConversionUtils.TimeToOasysString(currentDate),
                DayNumber = ConversionUtils.GetDaysSinceStartOf1900(currentDate.Date),
                TypeOfMovement = GetTypeOfStockMovement(isNormalStock, quantity),
                MovementKeys = string.Format("{0}  {1}  {2}  {3}", currentDate.Date.ToString("yyyy/MM/dd"), GetStockAdjustmentCodeNumber(isNormalStock), skuNumber, GetAdjustmentSequenceNumber(skuNumber, isNormalStock)),
                StockOnHandDelta = isNormalStock ? quantity : 0,
                MarkdownStockDelta = isNormalStock ? 0 : quantity
            };
        }

        private bool CheckAdjustmentExists(StockRepository stockRepo, string skuNumber, bool isNormalStock)
        {
            return stockRepo.CheckStockAdjustmentExists(skuNumber, GetAdjustmentSequenceNumber(skuNumber, isNormalStock), GetStockAdjustmentCodeNumber(isNormalStock), currentDate.Date, true);
        }

        private string GetStockAdjustmentCodeNumber(bool isNormalStock)
        {
            return isNormalStock ? StockAdjustmentCodeNumber.NormalAdjustment : StockAdjustmentCodeNumber.Damages;
        }

        private string GetTypeOfStockMovement(bool isNormalStock, decimal quantity)
        {
            if (isNormalStock)
            {
                if (quantity > 0)
                {
                    return TypeOfMovement.NormalStockAdjustmentWithPositiveAdjustmentQuantity;
                }
                else
                {
                    return TypeOfMovement.NormalStockAdjustmentWithNegativeAdjustmentQuantity;
                }
            }
            else
            {
                if (quantity > 0)
                {
                    return TypeOfMovement.MarkdownStockAdjustmentWithPositiveAdjustmentQuantity;
                }
                else
                {
                    return TypeOfMovement.MarkdownStockAdjustmentWithNegativeAdjustmentQuantity;
                }
            }
        }

        private string GetAdjustmentSequenceNumber(string skuNumber, bool isNormalStock)
        {
            if (isNormalStock)
            {
                return processedAdjustmentSkuNumbers.Where(x => x.Sku == skuNumber && x.StandardStockAdjustment != 0).Count().ToString().PadLeft(2, '0');
            }
            else
            {
                return processedAdjustmentSkuNumbers.Where(x => x.Sku == skuNumber && x.MarkdownStockAdjustment != 0).Count().ToString().PadLeft(2, '0');
            }
        }

        private StockMaster GetStockMasterIfExists(string skuNumber, StockRepository stockRepo)
        {
            var stkmas = stockRepo.GetStockMaster(skuNumber);

            if (stkmas != null)
            {
                return stkmas;
            }
            else
            {
                throw new NullReferenceException(string.Format("SKU {0} not found in DB", skuNumber));
            }
        }

        private void CheckStockAdjustmentCodesExist()
        {
            // TODO refactor this!
            var normalStockAdjustmentCode = GetStockAdjustmentCode(StockAdjustmentCodeNumber.NormalAdjustment);

            if (normalStockAdjustmentCode != null)
            {
                normalStockAdjustmentType = normalStockAdjustmentCode.Type;
            }
            else
            {
                throw new ApplicationException(string.Format("Information About Stock Adjustment Code \"{0}\" for Normal Stock not found in DB", StockAdjustmentCodeNumber.NormalAdjustment));
            }

            var markdownStockAdjustmentCode = GetStockAdjustmentCode(StockAdjustmentCodeNumber.Damages);

            if (markdownStockAdjustmentCode != null)
            {
                markdownStockAdjustmentType = markdownStockAdjustmentCode.Type;
            }
            else
            {
                throw new ApplicationException(string.Format("Information About Stock Adjustment Code \"{0}\" for Demages not found in DB", StockAdjustmentCodeNumber.Damages));
            }
        }

        private StockAdjustmentCode GetStockAdjustmentCode(string codeNumber)
        {
            var stockRepo = dataLayerFactory.Create<StockRepository>();

            return stockRepo.SelectStockAdjustmentCode(codeNumber);
        }
    }
}
