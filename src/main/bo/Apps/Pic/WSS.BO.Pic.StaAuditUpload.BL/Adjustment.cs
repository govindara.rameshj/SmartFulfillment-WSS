﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace WSS.BO.Pic.StaAuditUpload.BL
{
    public class Adjustment
    {
        [XmlElement(ElementName = "SKU")]
        public string Sku {get; set;}
        [XmlElement(ElementName = "SSA")]
        public int StandardStockAdjustment { get; set; }
        [XmlElement(ElementName = "MSA")]
        public int MarkdownStockAdjustment { get; set; }
    }
}
