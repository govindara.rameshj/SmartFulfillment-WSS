﻿Public Class PicCountCalculationFactory
    Inherits BaseFactory(Of IPicCodeCalculation)

    Public Overrides Function Implementation() As IPicCodeCalculation

        Return New PicCountCalculation

    End Function

End Class