﻿Public Class StockVarianceFormulaFactory
    Inherits BaseFactory(Of IStockVarianceFormula)

    Public Overrides Function Implementation() As IStockVarianceFormula

        Return New StockVarianceFormulaExcludePresold

    End Function

End Class