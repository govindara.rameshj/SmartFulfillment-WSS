<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Apply
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Apply))
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAccept = New System.Windows.Forms.Button
        Me.spdItems = New FarPoint.Win.Spread.FpSpread
        Me.cgpHeader = New CtsControls.CollapsibleGroupBox
        Me.lblPreSold = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblSold = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblCount = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblStart = New System.Windows.Forms.Label
        Me.lblVarValue = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblVarUnits = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.rdoVariances = New System.Windows.Forms.RadioButton
        Me.rdoNonCounted = New System.Windows.Forms.RadioButton
        Me.rdoAll = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.lblItemsCounted = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblItems = New System.Windows.Forms.Label
        Me.lblLabelled = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblAdjusted = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.btnPrint = New System.Windows.Forms.Button
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cgpHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(821, 558)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAccept.Enabled = False
        Me.btnAccept.Location = New System.Drawing.Point(3, 558)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 2
        Me.btnAccept.Text = "F5"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'spdItems
        '
        Me.spdItems.About = "3.0.2004.2005"
        Me.spdItems.AccessibleDescription = "spdOrder, Count Items, Row 0, Column 0, "
        Me.spdItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdItems.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.HorizontalScrollBarHeight = 15
        Me.spdItems.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.Location = New System.Drawing.Point(163, 3)
        Me.spdItems.Name = "spdItems"
        Me.spdItems.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.Size = New System.Drawing.Size(734, 549)
        Me.spdItems.TabIndex = 1
        Me.spdItems.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        Me.spdItems.TabStripPolicy = FarPoint.Win.Spread.TabStripPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdItems.TextTipAppearance = TipAppearance1
        Me.spdItems.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.VerticalScrollBarWidth = 15
        Me.spdItems.ActiveSheetIndex = -1
        '
        'cgpHeader
        '
        Me.cgpHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cgpHeader.Controls.Add(Me.lblPreSold)
        Me.cgpHeader.Controls.Add(Me.Label7)
        Me.cgpHeader.Controls.Add(Me.lblSold)
        Me.cgpHeader.Controls.Add(Me.Label15)
        Me.cgpHeader.Controls.Add(Me.Label3)
        Me.cgpHeader.Controls.Add(Me.lblCount)
        Me.cgpHeader.Controls.Add(Me.Label6)
        Me.cgpHeader.Controls.Add(Me.lblStart)
        Me.cgpHeader.Controls.Add(Me.lblVarValue)
        Me.cgpHeader.Controls.Add(Me.Label9)
        Me.cgpHeader.Controls.Add(Me.Label11)
        Me.cgpHeader.Controls.Add(Me.lblVarUnits)
        Me.cgpHeader.Controls.Add(Me.Label13)
        Me.cgpHeader.Controls.Add(Me.rdoVariances)
        Me.cgpHeader.Controls.Add(Me.rdoNonCounted)
        Me.cgpHeader.Controls.Add(Me.rdoAll)
        Me.cgpHeader.Controls.Add(Me.Label5)
        Me.cgpHeader.Controls.Add(Me.Label19)
        Me.cgpHeader.Controls.Add(Me.lblItemsCounted)
        Me.cgpHeader.Controls.Add(Me.Label2)
        Me.cgpHeader.Controls.Add(Me.lblItems)
        Me.cgpHeader.Controls.Add(Me.lblLabelled)
        Me.cgpHeader.Controls.Add(Me.Label10)
        Me.cgpHeader.Controls.Add(Me.Label1)
        Me.cgpHeader.Controls.Add(Me.lblAdjusted)
        Me.cgpHeader.Controls.Add(Me.Label18)
        Me.cgpHeader.Location = New System.Drawing.Point(3, 3)
        Me.cgpHeader.MinHeight = 18
        Me.cgpHeader.MinWidth = 18
        Me.cgpHeader.Name = "cgpHeader"
        Me.cgpHeader.Size = New System.Drawing.Size(154, 549)
        Me.cgpHeader.Style = CtsControls.CollapsibleGroupBox.Styles.Width
        Me.cgpHeader.TabIndex = 0
        Me.cgpHeader.TabStop = False
        Me.cgpHeader.Text = "Details"
        '
        'lblPreSold
        '
        Me.lblPreSold.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPreSold.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPreSold.Location = New System.Drawing.Point(97, 159)
        Me.lblPreSold.Name = "lblPreSold"
        Me.lblPreSold.Size = New System.Drawing.Size(49, 20)
        Me.lblPreSold.TabIndex = 51
        Me.lblPreSold.Text = "0"
        Me.lblPreSold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(6, 159)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 20)
        Me.Label7.TabIndex = 50
        Me.Label7.Text = "Pre Sold Stock"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSold
        '
        Me.lblSold.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSold.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSold.Location = New System.Drawing.Point(97, 219)
        Me.lblSold.Name = "lblSold"
        Me.lblSold.Size = New System.Drawing.Size(49, 20)
        Me.lblSold.TabIndex = 49
        Me.lblSold.Text = "0"
        Me.lblSold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(6, 219)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(85, 20)
        Me.Label15.TabIndex = 48
        Me.Label15.Text = "Stock Sold"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 137)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(142, 20)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Stock Items"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCount
        '
        Me.lblCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCount.Location = New System.Drawing.Point(97, 199)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(49, 20)
        Me.lblCount.TabIndex = 42
        Me.lblCount.Text = "0"
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(6, 199)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 20)
        Me.Label6.TabIndex = 40
        Me.Label6.Text = "Stock Counted"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStart
        '
        Me.lblStart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStart.Location = New System.Drawing.Point(97, 179)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(49, 20)
        Me.lblStart.TabIndex = 41
        Me.lblStart.Text = "0"
        Me.lblStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVarValue
        '
        Me.lblVarValue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVarValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarValue.Location = New System.Drawing.Point(97, 259)
        Me.lblVarValue.Name = "lblVarValue"
        Me.lblVarValue.Size = New System.Drawing.Size(49, 20)
        Me.lblVarValue.TabIndex = 47
        Me.lblVarValue.Text = "0"
        Me.lblVarValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(6, 259)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 20)
        Me.Label9.TabIndex = 46
        Me.Label9.Text = "Variance Value"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(6, 179)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 20)
        Me.Label11.TabIndex = 39
        Me.Label11.Text = "Starting Stock"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVarUnits
        '
        Me.lblVarUnits.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVarUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarUnits.Location = New System.Drawing.Point(97, 239)
        Me.lblVarUnits.Name = "lblVarUnits"
        Me.lblVarUnits.Size = New System.Drawing.Size(49, 20)
        Me.lblVarUnits.TabIndex = 44
        Me.lblVarUnits.Text = "0"
        Me.lblVarUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(6, 239)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 20)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = "Variance"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdoVariances
        '
        Me.rdoVariances.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoVariances.Location = New System.Drawing.Point(9, 375)
        Me.rdoVariances.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoVariances.Name = "rdoVariances"
        Me.rdoVariances.Size = New System.Drawing.Size(137, 29)
        Me.rdoVariances.TabIndex = 38
        Me.rdoVariances.Text = "Variance Items"
        Me.rdoVariances.UseVisualStyleBackColor = True
        '
        'rdoNonCounted
        '
        Me.rdoNonCounted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoNonCounted.Location = New System.Drawing.Point(9, 352)
        Me.rdoNonCounted.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.rdoNonCounted.Name = "rdoNonCounted"
        Me.rdoNonCounted.Size = New System.Drawing.Size(137, 29)
        Me.rdoNonCounted.TabIndex = 37
        Me.rdoNonCounted.Text = "Non Counted Items"
        Me.rdoNonCounted.UseVisualStyleBackColor = True
        '
        'rdoAll
        '
        Me.rdoAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoAll.Location = New System.Drawing.Point(9, 329)
        Me.rdoAll.Name = "rdoAll"
        Me.rdoAll.Size = New System.Drawing.Size(137, 29)
        Me.rdoAll.TabIndex = 36
        Me.rdoAll.Text = "All Items"
        Me.rdoAll.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 306)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(140, 29)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Filters"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(6, 16)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(142, 20)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "Totals"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblItemsCounted
        '
        Me.lblItemsCounted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblItemsCounted.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemsCounted.Location = New System.Drawing.Point(97, 56)
        Me.lblItemsCounted.Name = "lblItemsCounted"
        Me.lblItemsCounted.Size = New System.Drawing.Size(49, 20)
        Me.lblItemsCounted.TabIndex = 6
        Me.lblItemsCounted.Text = "0"
        Me.lblItemsCounted.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(6, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Items Counted"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblItems
        '
        Me.lblItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblItems.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItems.Location = New System.Drawing.Point(97, 36)
        Me.lblItems.Name = "lblItems"
        Me.lblItems.Size = New System.Drawing.Size(49, 20)
        Me.lblItems.TabIndex = 5
        Me.lblItems.Text = "0"
        Me.lblItems.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLabelled
        '
        Me.lblLabelled.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLabelled.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLabelled.Location = New System.Drawing.Point(97, 96)
        Me.lblLabelled.Name = "lblLabelled"
        Me.lblLabelled.Size = New System.Drawing.Size(49, 20)
        Me.lblLabelled.TabIndex = 34
        Me.lblLabelled.Text = "0"
        Me.lblLabelled.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(6, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 20)
        Me.Label10.TabIndex = 33
        Me.Label10.Text = "Items Label OK"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(6, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Items"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAdjusted
        '
        Me.lblAdjusted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAdjusted.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdjusted.Location = New System.Drawing.Point(97, 76)
        Me.lblAdjusted.Name = "lblAdjusted"
        Me.lblAdjusted.Size = New System.Drawing.Size(49, 20)
        Me.lblAdjusted.TabIndex = 26
        Me.lblAdjusted.Text = "0"
        Me.lblAdjusted.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(6, 76)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(85, 20)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "Items Adjusted"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPrint.Location = New System.Drawing.Point(81, 558)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'Apply
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.cgpHeader)
        Me.Controls.Add(Me.spdItems)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnExit)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Apply"
        Me.Size = New System.Drawing.Size(900, 600)
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cgpHeader.ResumeLayout(False)
        Me.cgpHeader.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents spdItems As FarPoint.Win.Spread.FpSpread
    Friend WithEvents cgpHeader As CtsControls.CollapsibleGroupBox
    Friend WithEvents lblItemsCounted As System.Windows.Forms.Label
    Friend WithEvents lblItems As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblAdjusted As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblLabelled As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents rdoVariances As System.Windows.Forms.RadioButton
    Friend WithEvents rdoNonCounted As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAll As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSold As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents lblVarValue As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblVarUnits As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblPreSold As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label

End Class
