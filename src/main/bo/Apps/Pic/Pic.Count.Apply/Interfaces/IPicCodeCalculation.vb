﻿Public Interface IPicCodeCalculation

    Sub PicCountCode2Calculation(ByRef stockDetail As HandHeldTerminalDetail, Optional ByVal code53Trigerred As Boolean = False)
    Sub PicCountCode53Calculation(ByRef stockDetail As HandHeldTerminalDetail)
    Property OHVAR() As Integer
    Property MDVAR() As Integer
    Property SecurityLevel() As Integer
    Property Code2SequenceCounter() As Integer
    Property SecondUserId() As Integer
    Property Comment() As String
    Property ErrorFlag() As Boolean
    Property UpdateFlag() As Boolean
    Property Code53TrigerredFlag() As Boolean
    Property StockAdjustmentFactoryInstance() As IAdjustment
    Property StockFactoryInstance() As IStock
    Property StockLogFactoryInstance() As IStockLog
    Property SaCodeFactoryInstance() As ISaCode
    Property ConnectionInstance() As Connection
    Property HhtHeaderFactoryInstance() As IHandHeldTerminalHeader
End Interface
