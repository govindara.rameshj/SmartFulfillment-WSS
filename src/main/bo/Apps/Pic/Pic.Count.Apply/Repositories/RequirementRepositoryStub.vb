﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Pic.Count.Apply.UnitTest")> 
Public Class RequirementRepositoryStub
    Implements IRequirementRepository

#Region "Stub Code"

    Private _RequirementEnabled As Nullable(Of Boolean)

    Friend Sub ConfigureStub(ByVal RequirementEnabled As Nullable(Of Boolean))

        _RequirementEnabled = RequirementEnabled
    End Sub
#End Region

#Region "Interface"

    Public Function RequirementEnabled(ByVal RequirementID As Integer) As Boolean? Implements IRequirementRepository.RequirementEnabled

        'ignore parameters
        Return _RequirementEnabled
    End Function

#End Region
End Class