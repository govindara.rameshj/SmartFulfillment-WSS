﻿Public Class StockVarianceFormulaExcludePresold
    Implements IStockVarianceFormula

    Public ReadOnly Property Formula() As String Implements IStockVarianceFormula.Formula
        Get
            Return "RC[-1]+RC[-2]-RC[-3]"
        End Get
    End Property
End Class
