﻿Public Class PicCountCalculation
    Implements IPicCodeCalculation



    Private mintFirstUserID As Integer
    Private m_Ohvar As Integer
    Private m_Mdvar As Integer
    Private m_SecondUserId As Integer
    Private m_StrComment As String
    Private m_HasErrorOccured As Boolean
    Private m_Update As Boolean
    Private m_objStockAdjustment As IAdjustment
    Private m_objStockLog As IStockLog
    Private m_objStock As IStock
    Private m_objSaCode As ISaCode
    Private m_objHhtDetails As IHandHeldTerminalHeader
    Private m_Connection As Connection
    Private m_Code2SequenceCount As Integer
    Private m_SecurityLevel As Integer
    Private m_Code53Trigerred As Boolean

#Region "Properties"

    Public Property OHVAR() As Integer Implements IPicCodeCalculation.OHVAR
        Get
            Return m_Ohvar
        End Get
        Set(ByVal value As Integer)
            m_Ohvar = value
        End Set
    End Property
    Public Property SecurityLevel() As Integer Implements IPicCodeCalculation.SecurityLevel
        Get
            Return m_SecurityLevel
        End Get
        Set(ByVal value As Integer)
            m_SecurityLevel = value
        End Set
    End Property
    Public Property MDVAR() As Integer Implements IPicCodeCalculation.MDVAR
        Get
            Return m_Mdvar
        End Get
        Set(ByVal value As Integer)
            m_Mdvar = value
        End Set
    End Property
    Public Property Code2SequenceCounter() As Integer Implements IPicCodeCalculation.Code2SequenceCounter
        Get
            Return m_Code2SequenceCount
        End Get
        Set(ByVal value As Integer)
            m_Code2SequenceCount = value
        End Set
    End Property
    Public Property SecondUserId() As Integer Implements IPicCodeCalculation.SecondUserId
        Get
            Return m_SecondUserId
        End Get
        Set(ByVal value As Integer)
            m_SecondUserId = value
        End Set
    End Property
    Public Property Comment() As String Implements IPicCodeCalculation.Comment
        Get
            Return m_StrComment
        End Get
        Set(ByVal value As String)
            m_StrComment = value
        End Set
    End Property
    Public Property ErrorFlag() As Boolean Implements IPicCodeCalculation.ErrorFlag
        Get
            Return m_HasErrorOccured
        End Get
        Set(ByVal value As Boolean)
            m_HasErrorOccured = value
        End Set
    End Property
    Public Property UpdateFlag() As Boolean Implements IPicCodeCalculation.UpdateFlag
        Get
            Return m_Update
        End Get
        Set(ByVal value As Boolean)
            m_Update = value
        End Set
    End Property
    Public Property StockAdjustmentFactoryInstance() As IAdjustment Implements IPicCodeCalculation.StockAdjustmentFactoryInstance
        Get
            Return m_objStockAdjustment
        End Get
        Set(ByVal value As IAdjustment)
            m_objStockAdjustment = value
        End Set
    End Property
    Public Property StockFactoryInstance() As IStock Implements IPicCodeCalculation.StockFactoryInstance
        Get
            Return m_objStock
        End Get
        Set(ByVal value As IStock)
            m_objStock = value
        End Set
    End Property
    Public Property StockLogFactoryInstance() As IStockLog Implements IPicCodeCalculation.StockLogFactoryInstance
        Get
            Return m_objStockLog
        End Get
        Set(ByVal value As IStockLog)
            m_objStockLog = value
        End Set
    End Property
    Public Property HhtHeaderFactoryInstance() As IHandHeldTerminalHeader Implements IPicCodeCalculation.HhtHeaderFactoryInstance
        Get
            Return m_objHhtDetails
        End Get
        Set(ByVal value As IHandHeldTerminalHeader)
            m_objHhtDetails = value
        End Set
    End Property
    Public Property SaCodeFactoryInstance() As ISaCode Implements IPicCodeCalculation.SaCodeFactoryInstance
        Get
            Return m_objSaCode
        End Get
        Set(ByVal value As ISaCode)
            m_objSaCode = value
        End Set
    End Property
    Public Property ConnectionInstance() As Connection Implements IPicCodeCalculation.ConnectionInstance
        Get
            Return m_Connection
        End Get
        Set(ByVal value As Connection)
            m_Connection = value
        End Set
    End Property
    Public Property Code53TrigerredFlag() As Boolean Implements IPicCodeCalculation.Code53TrigerredFlag
        Get
            Return m_Code53Trigerred
        End Get
        Set(ByVal value As Boolean)
            m_Code53Trigerred = value
        End Set
    End Property
#End Region

#Region "Private Procedures & Functions"

    Friend Sub UpdateStockValues(ByRef stockDetail As HandHeldTerminalDetail)
        If OHVAR = 0 Or Code53TrigerredFlag Then
            StockLogFactoryInstance.SKUN = stockDetail.ProductCode
            StockLogFactoryInstance.EndingStock_ESTK = StockFactoryInstance.OnHand_ONHA
            StockFactoryInstance.OnHand_ONHA += MDVAR 'StockFactoryInstance.OnHand_ONHA - stockDetail.NormalOnHandStock
            StockFactoryInstance.CycleDate_ADAT1 = HhtHeaderFactoryInstance.HHTDate
            StockFactoryInstance.AdjustmentQty_AQ021 += MDVAR
            StockFactoryInstance.AdjustmentValue_AV021 += (MDVAR * StockFactoryInstance.Price_PRIC)
        Else
            StockFactoryInstance.OnHand_ONHA += OHVAR 'StockFactoryInstance.OnHand_ONHA - stockDetail.NormalOnHandStock
            StockFactoryInstance.CycleDate_ADAT1 = HhtHeaderFactoryInstance.HHTDate
            StockFactoryInstance.AdjustmentQty_AQ021 += OHVAR
            StockFactoryInstance.AdjustmentValue_AV021 += (OHVAR * StockFactoryInstance.Price_PRIC)

        End If
    End Sub
    Friend Sub CheckCode2Ajustment(ByRef shouldReturn As Boolean)
        shouldReturn = False
        If SaCodeFactoryInstance.Load(Me.SecurityLevel, "02") = False Then
            Trace.WriteLine("Stock Adjustment Code 02 Not Found.........")
            ErrorFlag = True
            shouldReturn = True : Exit Sub
        End If
    End Sub
    Friend Sub SetCode2StockLog(ByRef stockDetail As HandHeldTerminalDetail)
        StockLogFactoryInstance.SKUN = stockDetail.ProductCode
        StockLogFactoryInstance.EndingStock_ESTK = StockFactoryInstance.OnHand_ONHA
    End Sub
    Friend Sub CheckStock(ByRef stockDetail As HandHeldTerminalDetail, ByRef shouldReturn As Boolean)
        shouldReturn = False
        If StockFactoryInstance.ReadSKU(ConnectionInstance, stockDetail.ProductCode) = False Then
            Trace.WriteLine("Sku " & stockDetail.ProductCode.ToString & " Not Found.........")
            ErrorFlag = True
            shouldReturn = True : Exit Sub
        End If
    End Sub
    Friend Sub UpdateStockAdjustmentAndStockLog(ByRef stockDetail As HandHeldTerminalDetail)
        Try
            ConnectionInstance.StartTransaction()
            StockAdjustmentFactoryInstance.SaveAdjustment(ConnectionInstance, GetAdjustmentQuantity, HhtHeaderFactoryInstance.HHTDate, stockDetail.ProductCode, String.Empty, Comment, _
                                              SaCodeFactoryInstance, StockFactoryInstance, StockLogFactoryInstance, SecondUserId, , , , , True)

            'new stock log entry
            StockLogFactoryInstance.SaveStockLog(ConnectionInstance, GetAdjustmentQuantity, SaCodeFactoryInstance, StockFactoryInstance, StockAdjustmentFactoryInstance, SecondUserId)

            ConnectionInstance.CommitTransaction()
            UpdateFlag = True

        Catch ex As Exception
            'No action as per Uptrac, don't put in a existing record
            ConnectionInstance.RollbackTransaction()
            ErrorFlag = True
        End Try
    End Sub

    Friend Function GetAdjustmentQuantity() As Integer
        If OHVAR = 0 Or Code53TrigerredFlag Then
            Return MDVAR
        Else
            Return OHVAR
        End If
    End Function


    Friend Sub SetCode53()
        SaCodeFactoryInstance = SACodeFactory.FactoryGet
        SaCodeFactoryInstance.Load(Me.SecurityLevel, "53")
        SaCodeFactoryInstance.MarkdownWriteoff = "M"
    End Sub
    Friend Sub SetStockAndAdjustmentAndStockLog()
        StockFactoryInstance.MarkdownQty_MDNQ += MDVAR 'StockFactoryInstance.MarkdownQty_MDNQ - objDetail.TotalMarkDownCount

        StockAdjustmentFactoryInstance = AdjustmentFactory.FactoryGet
        StockLogFactoryInstance = StockLogFactory.FactoryGet
    End Sub
    Friend Sub SetStockLog(ByRef stockDetail As HandHeldTerminalDetail)
        If OHVAR = 0 Then
            StockLogFactoryInstance.SKUN = stockDetail.ProductCode
            StockLogFactoryInstance.EndingStock_ESTK = StockFactoryInstance.OnHand_ONHA
        End If
    End Sub
    Friend Sub UpdateStock()
        StockFactoryInstance.OnHand_ONHA += -MDVAR
    End Sub
    Friend Sub UpdateStockAdjustmentAndStockLogForCode53(ByRef stockDetail As HandHeldTerminalDetail)
        Try
            ConnectionInstance.StartTransaction()
            'new stock adjustment
            StockAdjustmentFactoryInstance.SaveAdjustment(ConnectionInstance, -MDVAR, HhtHeaderFactoryInstance.HHTDate, stockDetail.ProductCode, String.Empty, Comment, _
              SaCodeFactoryInstance, StockFactoryInstance, StockLogFactoryInstance, SecondUserId, , , , , True, True)

            'new stock log entry
            StockLogFactoryInstance.SaveStockLog(ConnectionInstance, -MDVAR, SaCodeFactoryInstance, StockFactoryInstance, StockAdjustmentFactoryInstance, SecondUserId)
            ConnectionInstance.CommitTransaction()
            UpdateFlag = True

        Catch ex As Exception
            'No action as per Uptrac, don't put in a existing record
            ConnectionInstance.RollbackTransaction()
            ErrorFlag = True
        End Try

    End Sub
#End Region

#Region "Public Procedures and Functions"
    Public Sub PicCountCode2Calculation(ByRef stockDetail As HandHeldTerminalDetail, Optional ByVal code53Trigerred As Boolean = False) Implements IPicCodeCalculation.PicCountCode2Calculation

        Dim checkCode2ExistFlag As Boolean
        Dim checkStockExistFlag As Boolean

        SaCodeFactoryInstance.MarkdownWriteoff = String.Empty
        Code53TrigerredFlag = code53Trigerred


        CheckCode2Ajustment(checkCode2ExistFlag)
        If checkCode2ExistFlag Then
            Return
        End If

        'load stock
        CheckStock(stockDetail, checkStockExistFlag)
        If checkStockExistFlag Then
            Return
        End If


        SetCode2StockLog(stockDetail)
        UpdateStockValues(stockDetail)

        'read last stock log entry for product code
        StockLogFactoryInstance.ReadStockLog(stockDetail.ProductCode)

        UpdateStockAdjustmentAndStockLog(stockDetail)

    End Sub


    Public Sub PicCountCode53Calculation(ByRef stockDetail As HandHeldTerminalDetail) Implements IPicCodeCalculation.PicCountCode53Calculation

        Dim checkStockFlag As Boolean

        StockFactoryInstance.Update(ConnectionInstance)
        SetCode53()

        'load stock
        CheckStock(stockDetail, checkStockFlag)
        If checkStockFlag Then
            Return
        End If

        SetStockAndAdjustmentAndStockLog()
        'read last stock log entry for product code
        StockLogFactoryInstance.ReadStockLog(stockDetail.ProductCode)
        'picCode2Instance.StockFactoryInstance.OnHand_ONHA = -(picCode2Instance.OHVAR)

        SetStockLog(stockDetail)
        UpdateStock()
        'read last stock log entry for product code
        StockLogFactoryInstance.ReadStockLog(stockDetail.ProductCode)
        UpdateStockAdjustmentAndStockLogForCode53(stockDetail)

    End Sub

#End Region

End Class
