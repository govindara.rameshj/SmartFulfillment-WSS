Imports Cts.Oasys.Core.Locking

Public Class Apply


    Private Enum col
        Counted
        SkuNumber
        Description
        Supplier
        PartCode
        Price
        PreSold
        StockStart
        StockCount
        StockSold
        StockVar
        StockVarValue
        MarkdownStart
        MarkdownCount
        MarkdownVar
        Adjusted
        Labelled
    End Enum

    Private m_Header As IHandHeldTerminalHeader
    Private mintFirstUserID As Integer
    Private m_Ohvar As Integer
    Private m_Mdvar As Integer
    Private m_SecondUserId As Integer
    Private m_StrComment As String
    Private m_HasErrorOccured As Boolean
    Private m_Update As Boolean
    Private m_objStockAdjustment As IAdjustment
    Private m_objStockLog As IStockLog
    Private m_objStock As IStock
    Private m_objSaCode As ISaCode
    Private m_Connection As Connection
    Private m_Code2SequenceCount As Integer
    Private picCodeInstance As IPicCodeCalculation = (New PicCountCalculationFactory).GetImplementation

    Private Const LockEntityId As String = "Pic.Count.Apply"
    Private formLock As EntityLock

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)

        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        While True
            Try
                formLock = New EntityLock(LockEntityId, userId.ToString())
                Exit While
            Catch exc As LockException
                If MessageBox.Show(String.Format("This form is already opened by {0} at {1}. Are you sure you wish to continue as this could result in the duplication of adjustments?", exc.OwnerId, exc.LockTime), Application.ProductName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    EntityLock.RemoveLock(LockEntityId)
                Else
                    Throw exc
                End If
            End Try
        End While


        btnAccept.Text = My.Resources.F5AcceptCount

        'store logged on user, other forms not been sub-classed from Cts.Oasys.WinForm.Form, do not have access to "userId"
        mintFirstUserID = userId

        picCodeInstance.SecurityLevel = securityLevel


    End Sub

    Public Sub TestLaunch()

        DoProcessing()
        PicApplyProcess()
    End Sub


    'Protected Overrides Sub DoProcessing()
    Protected Overrides Sub DoProcessing()
        Try
            Cursor = Cursors.WaitCursor

            'm_Header = HandHeldTerminalHeaderFactory.FactoryGet

            picCodeInstance.HhtHeaderFactoryInstance = HandHeldTerminalHeaderFactory.FactoryGet

            AddHandler picCodeInstance.HhtHeaderFactoryInstance.UpdateProgess, AddressOf DisplayProgress
            AddHandler picCodeInstance.HhtHeaderFactoryInstance.ClearProgress, AddressOf DisplayProgress

            'Read Date from SYSDAT
            'Dim StockAdjust As IAdjustment = AdjustmentFactory.FactoryGet
            picCodeInstance.StockAdjustmentFactoryInstance = AdjustmentFactory.FactoryGet
            Dim AdjDate As Date = picCodeInstance.StockAdjustmentFactoryInstance.ReadAdjustmentDate


            'load header
            picCodeInstance.HhtHeaderFactoryInstance.ReadHHT(AdjDate)

            'load details
            DisplayProgress(40, My.Resources.InitGetDetails)
            cgpHeader.Text = My.Resources.DetailsFor & picCodeInstance.HhtHeaderFactoryInstance.HHTDate.ToShortDateString

            DisplayProgress(80, My.Resources.InitConfigSpreads)
            spdItems_Initialise()
            spdItems_AddItems()

            Dim statusWithMessage = picCodeInstance.HhtHeaderFactoryInstance.CheckAdjustmentsAuthorizingNeeded()
            Dim isAdjustmentsAuthorizingNeeded = statusWithMessage.Item1
            Dim errorMessage = statusWithMessage.Item2

            If isAdjustmentsAuthorizingNeeded Then
                btnAccept.Enabled = True
            Else
                MessageBox.Show(errorMessage, Me.FindForm.Text)
            End If

            rdoAll.Checked = True
        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try
    End Sub

#Region "Form Events"

    Protected Overrides Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        cgpHeader.Collapse()

        AddHandler FindForm.FormClosed, AddressOf OnFormClosed
    End Sub

    Private Sub OnFormClosed(ByVal sender As Object, ByVal e As EventArgs)
        formLock.Dispose()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

#End Region

#Region "Spread Events"

    Private Sub spdItems_Initialise()

        'make F keys available in spread
        Dim im As InputMap = spdItems.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdItems.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim sheet As New SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.SelectionUnit = Model.SelectionUnit.Row
        sheet.SelectionPolicy = Model.SelectionPolicy.Single
        sheet.RowCount = 0
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 2
        sheet.ColumnHeaderVisible = True

        'default properties
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True

        'columns types
        Dim typeInt As New CellType.NumberCellType
        typeInt.DecimalPlaces = 0

        'column headers
        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)

        sheet.ColumnHeader.Rows(0, 1).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.Rows(1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.Cells(1, col.SkuNumber, 1, col.Description).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.ColumnHeader.Cells(0, col.PreSold).Value = "Stock"
        sheet.ColumnHeader.Cells(0, col.PreSold).ColumnSpan = 6
        sheet.ColumnHeader.Cells(0, col.MarkdownStart).Value = "Markdowns"
        sheet.ColumnHeader.Cells(0, col.MarkdownStart).ColumnSpan = 3

        'columns
        sheet.Columns(col.Counted).Label = "Counted"
        sheet.Columns(col.Counted).CellType = New CellType.CheckBoxCellType
        sheet.Columns(col.Counted).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(col.Counted).Width = 60
        sheet.Columns(col.Counted).Resizable = False
        sheet.Columns(col.SkuNumber, col.Supplier).AllowAutoSort = True
        sheet.Columns(col.SkuNumber, col.PartCode).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(col.SkuNumber).Label = "SKU"
        sheet.Columns(col.Description).Label = "Description"
        sheet.Columns(col.Description).Width = 90
        sheet.Columns(col.Supplier).Label = "Supplier"
        sheet.Columns(col.Supplier).Width = 70
        sheet.Columns(col.PartCode).Label = "Part Code"
        sheet.Columns(col.Price).Label = "Price"
        sheet.Columns(col.Price).CellType = New CellType.NumberCellType
        sheet.Columns(col.PreSold).CellType = typeInt
        sheet.Columns(col.PreSold).Label = "Pre Sold"
        sheet.Columns(col.StockStart, col.StockVar).CellType = typeInt
        sheet.Columns(col.StockStart).Label = "Start"
        sheet.Columns(col.StockCount).Label = "Count"
        sheet.Columns(col.StockSold).Label = "Sold"
        sheet.Columns(col.StockVar).Label = "Variance"
        ' RF 898
        sheet.Columns(col.StockVar).Formula = (New StockVarianceFormulaFactory).GetImplementation.Formula
        ' End of RF 898
        sheet.Columns(col.StockVarValue).Formula = "RC[-6]*RC[-1]"
        sheet.Columns(col.StockVarValue).Label = "Value"
        sheet.Columns(col.StockVarValue).CellType = New CellType.NumberCellType

        sheet.Columns(col.MarkdownStart, col.MarkdownVar).CellType = typeInt
        sheet.Columns(col.MarkdownStart).Label = "Start"
        sheet.Columns(col.MarkdownCount).Label = "Count"
        sheet.Columns(col.MarkdownVar).Label = "Variance"
        sheet.Columns(col.MarkdownVar).Formula = "RC[-1]-RC[-2]"

        sheet.Columns(col.Adjusted).Label = "Adjusted"
        sheet.Columns(col.Adjusted, col.Labelled).CellType = New CellType.CheckBoxCellType
        sheet.Columns(col.Adjusted, col.Labelled).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(col.Labelled).Label = "Labelled"

        spdItems.Sheets.Add(sheet)
        AddHandler spdItems.Resize, AddressOf spd_Resize

        'set up print sheet
        Dim print As SheetView = sheet.Clone
        print.Visible = False
        print.ColumnHeader.RowCount = 0
        print.ColumnHeader.RowCount = 5

        'print info
        print.PrintInfo.Header = "/c/fz""14""PIC Sample Check Worksheet"
        'print.PrintInfo.Header &= "/n/c/fz""10""Data for " & m_Header.HHTDate.ToShortDateString
        print.PrintInfo.Header &= "/n/c/fz""10""Data for " & picCodeInstance.HhtHeaderFactoryInstance.HHTDate.ToShortDateString
        print.PrintInfo.Header &= "/n "
        print.PrintInfo.Footer = "/lPrinted: " & Now
        print.PrintInfo.Footer &= "/cPage /p of /pc"
        print.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
        print.PrintInfo.Margin.Left = 50
        print.PrintInfo.Margin.Right = 50
        print.PrintInfo.ShowBorder = False
        print.PrintInfo.ShowPrintDialog = True
        print.PrintInfo.UseMax = False
        print.PrintInfo.Orientation = PrintOrientation.Portrait

        'column headers
        print.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        print.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        print.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        print.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        print.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType

        print.ColumnHeader.Rows(0, 3).HorizontalAlignment = CellHorizontalAlignment.Left
        print.ColumnHeader.Rows(4).HorizontalAlignment = CellHorizontalAlignment.Center
        print.ColumnHeader.Rows(4).VerticalAlignment = CellVerticalAlignment.Bottom
        print.ColumnHeader.Rows(4).Border = New LineBorder(Color.Black, 1, False, False, False, True)
        print.ColumnHeader.Rows(4).Height = 40

        print.ColumnHeader.Cells(0, col.SkuNumber).Value = "Counter"
        print.ColumnHeader.Cells(1, col.SkuNumber).Value = "Time Start"
        print.ColumnHeader.Cells(2, col.SkuNumber).Value = "Time End"
        print.ColumnHeader.Cells(3, col.SkuNumber).Value = "Date End"
        print.ColumnHeader.Cells(0, col.Description, 3, col.Description).Border = New LineBorder(Color.Black, 1, False, False, False, True)

        print.ColumnHeader.Cells(0, col.Price).Value = "All Warehouse Checked"
        print.ColumnHeader.Cells(0, col.Price).ColumnSpan = 4
        print.ColumnHeader.Cells(1, col.Price).Value = "Delivery Collection Checked"
        print.ColumnHeader.Cells(1, col.Price).ColumnSpan = 4
        print.ColumnHeader.Cells(2, col.Price).Value = "Sales Checked"
        print.ColumnHeader.Cells(2, col.Price).ColumnSpan = 4
        print.ColumnHeader.Cells(3, col.Price).Value = "Outpostings Checked"
        print.ColumnHeader.Cells(3, col.Price).ColumnSpan = 4
        print.ColumnHeader.Cells(0, col.Labelled, 3, col.Labelled).CellType = New CellType.CheckBoxCellType
        print.ColumnHeader.Cells(0, col.Labelled, 3, col.Labelled).HorizontalAlignment = CellHorizontalAlignment.Center

        'columns
        print.Columns(col.Counted).Visible = False
        print.Columns(col.Adjusted).Visible = False
        print.Columns(col.SkuNumber, col.PartCode).HorizontalAlignment = CellHorizontalAlignment.Left
        print.Columns(col.SkuNumber).Label = "SKU"
        print.Columns(col.SkuNumber).Width = 60
        print.Columns(col.Description).Label = "Description"
        print.Columns(col.Description).Width = 250
        print.Columns(col.Supplier).Label = "Supplier"
        print.Columns(col.Supplier).Width = 60
        print.Columns(col.PartCode).Label = "Part Code"
        print.Columns(col.PartCode).Width = 80
        print.Columns(col.Price).Label = "Price"
        print.Columns(col.PreSold).Label = "Pre Sold"
        print.Columns(col.StockStart).Label = "Start"
        print.Columns(col.StockCount).Label = "Count"
        print.Columns(col.StockSold).Label = "Sold"
        print.Columns(col.StockVar).Visible = False
        print.Columns(col.StockVarValue).Visible = False
        print.Columns(col.Price, col.Labelled).Width = 55
        print.Columns(col.Labelled).Label = "Labelled"
        print.Columns.Default.Resizable = False

        spdItems.Sheets.Add(print)

    End Sub

    Private Sub spdItems_AddItems()
        Dim Sheet As SheetView = spdItems.ActiveSheet

        For Each objDetail As HandHeldTerminalDetail In picCodeInstance.HhtHeaderFactoryInstance.Details
            Sheet.Rows.Add(Sheet.RowCount, 1)
            Sheet.Cells(Sheet.RowCount - 1, col.SkuNumber).Value = objDetail.ProductCode

            Sheet.Cells(Sheet.RowCount - 1, col.Description).Value = objDetail.ProductDescription
            Sheet.Cells(Sheet.RowCount - 1, col.Supplier).Value = objDetail.SupplierNumber
            Sheet.Cells(Sheet.RowCount - 1, col.PartCode).Value = objDetail.SupplierPartCode
            Sheet.Cells(Sheet.RowCount - 1, col.Price).Value = objDetail.NormalSellingPrice
            Sheet.Cells(Sheet.RowCount - 1, col.PreSold).Value = objDetail.TotalPreSoldStock
            Sheet.Cells(Sheet.RowCount - 1, col.StockStart).Value = objDetail.NormalOnHandStock
            Sheet.Cells(Sheet.RowCount - 1, col.StockSold).Value = objDetail.StockSold
            Sheet.Cells(Sheet.RowCount - 1, col.StockCount).Value = (objDetail.TotalCount - objDetail.TotalMarkDownCount)
            'Sheet.Cells(Sheet.RowCount - 1, col.StockVarValue).Value = objDetail.NormalSellingPrice * (objDetail.

            Sheet.Cells(Sheet.RowCount - 1, col.Adjusted).Value = objDetail.AdjustmentApplied
            Sheet.Cells(Sheet.RowCount - 1, col.Labelled).Value = objDetail.LabelDetailCorrect
            If CType(Sheet.Cells(Sheet.RowCount - 1, col.StockVar).Value, Integer) <> 0 Then Sheet.Rows(Sheet.RowCount - 1).ForeColor = Color.Blue

            Sheet.Cells(Sheet.RowCount - 1, col.MarkdownStart).Value = objDetail.MarkDownStock
            Sheet.Cells(Sheet.RowCount - 1, col.MarkdownCount).Value = objDetail.TotalMarkDownCount
            If CType(Sheet.Cells(Sheet.RowCount - 1, col.MarkdownVar).Value, Integer) <> 0 Then Sheet.Rows(Sheet.RowCount - 1).ForeColor = Color.Blue
        Next

        If Sheet.RowCount > 0 Then
            Sheet.ColumnHeaderVisible = True
            spd_Resize(spdItems, New EventArgs)
        End If

        spdItems_SheetTotals()

    End Sub

    Private Sub spdItems_AutoSortingColumn(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.AutoSortingColumnEventArgs) Handles spdItems.AutoSortingColumn

        e.Sheet.ClearSelection()

    End Sub

    Private Sub spdItems_SheetTotals()

        Dim Sheet As SheetView = spdItems.ActiveSheet
        Dim Presold As Integer = 0
        Dim Counted As Integer = 0
        Dim Adjusted As Integer = 0
        Dim LabelOK As Integer = 0
        Dim Start As Integer = 0
        Dim Count As Integer = 0
        Dim Sold As Integer = 0
        Dim Units As Integer = 0
        Dim Value As Decimal = 0

        If Sheet.RowCount > 0 Then
            For rowIndex As Integer = 0 To Sheet.RowCount - 1
                If CType(Sheet.Cells(rowIndex, col.Counted).Value, Boolean) Then Counted += 1
                If CType(Sheet.Cells(rowIndex, col.Adjusted).Value, Boolean) Then Adjusted += 1
                If CType(Sheet.Cells(rowIndex, col.Labelled).Value, Boolean) Then LabelOK += 1

                Presold += CType(Sheet.Cells(rowIndex, col.PreSold).Value, Integer)
                Start += CType(Sheet.Cells(rowIndex, col.StockStart).Value, Integer)
                Count += CType(Sheet.Cells(rowIndex, col.StockCount).Value, Integer)
                Sold += CType(Sheet.Cells(rowIndex, col.StockSold).Value, Integer)
                Units += CType(Sheet.Cells(rowIndex, col.StockVar).Value, Integer)
                Value += CType(Sheet.Cells(rowIndex, col.StockVarValue).Value, Decimal)
            Next
        End If

        lblItems.Text = picCodeInstance.HhtHeaderFactoryInstance.Details.Count.ToString
        lblItemsCounted.Text = Counted.ToString
        lblAdjusted.Text = Adjusted.ToString
        lblLabelled.Text = LabelOK.ToString
        lblPreSold.Text = Presold.ToString
        lblStart.Text = Start.ToString
        lblCount.Text = Count.ToString
        lblSold.Text = Sold.ToString
        lblVarUnits.Text = Units.ToString
        lblVarValue.Text = Value.ToString

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs)

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next

            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CType(((dataAreaWidth - colsTotal) / CType(colsResizable, Single)), Integer)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub

#End Region

#Region "Button Events"

    Public Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        PicApplyProcess()
        FindForm.Close()
    End Sub

    Private Sub PicApplyProcessCalculation(ByVal intSecondUserID As Integer, ByVal blnUpdate As Boolean, ByVal blnHasErrorOccurred As Boolean)

        Try

            Cursor = Cursors.WaitCursor

            Dim Connect As IConnecxion = ConnexionFactory.FactoryGet
            picCodeInstance.ConnectionInstance = Connect.Connecxion()
            Try

                DisplayProgress(0, "Applying PIC Count")
                Dim NumberOfRecords As Integer = GetTotalRecords()
                Dim count As Integer = 0
                For Each objDetail As HandHeldTerminalDetail In picCodeInstance.HhtHeaderFactoryInstance.Details
                    blnUpdate = False
                    picCodeInstance.Code2SequenceCounter = 0

                    picCodeInstance.StockAdjustmentFactoryInstance = AdjustmentFactory.FactoryGet
                    picCodeInstance.StockLogFactoryInstance = StockLogFactory.FactoryGet
                    picCodeInstance.StockFactoryInstance = StockFactory.FactoryGet
                    picCodeInstance.SaCodeFactoryInstance = SACodeFactory.FactoryGet

                    If objDetail.AdjustmentApplied = True Then Continue For

                    If objDetail.StockExist = False Then Continue For

                    picCodeInstance.OHVAR = (objDetail.TotalCount - objDetail.TotalMarkDownCount) - (objDetail.NormalOnHandStock)
                    picCodeInstance.MDVAR = objDetail.TotalMarkDownCount - objDetail.MarkDownStock
                    'If OHVAR + MDVAR = 0 Then Continue For
                    'Dim strComment As String = "PIC - Primary Count"
                    picCodeInstance.Comment = "PIC - Primary Count"

                    If objDetail.Original = "R" Then
                        picCodeInstance.Comment = "PIC - Refund Count"
                    End If

                    If picCodeInstance.OHVAR <> 0 Then
                        picCodeInstance.PicCountCode2Calculation(objDetail)
                    End If

                    If picCodeInstance.MDVAR <> 0 Then
                        picCodeInstance.PicCountCode53Calculation(objDetail)
                    End If

                    If picCodeInstance.SaCodeFactoryInstance.CodeNumber = "53" Then
                        picCodeInstance.StockFactoryInstance.Update(picCodeInstance.ConnectionInstance)
                        picCodeInstance.PicCountCode2Calculation(objDetail, True)
                    End If '

                    If picCodeInstance.UpdateFlag Then
                        picCodeInstance.StockFactoryInstance.Update(picCodeInstance.ConnectionInstance)
                    End If

                    'adjustment applied
                    objDetail.Update(picCodeInstance.HhtHeaderFactoryInstance.HHTDate, objDetail.ProductCode, True)

                    count += 1
                    DisplayProgress(CInt(count / NumberOfRecords * 100), "Applying PIC Count")

                Next
            Catch ex As Exception
                'Con.RollbackTransaction()
            Finally
                picCodeInstance.ConnectionInstance.Dispose()
            End Try

            If blnHasErrorOccurred = False Then
                'update header: authorisation id, adjustment applied
                picCodeInstance.HhtHeaderFactoryInstance.Update(picCodeInstance.HhtHeaderFactoryInstance.HHTDate, New System.Nullable(Of Integer), New System.Nullable(Of Integer), True, _
                                intSecondUserID.ToString.PadLeft(3, CType("0", Char)), New System.Nullable(Of Boolean))

            End If

            'complete transaction - TO BE DONE

        Catch ex As Exception

            MessageBox.Show(ex.Message, Me.FindForm.Text)

        Finally

            Cursor = Cursors.Default

        End Try

        DisplayProgress(100, "Applied PIC Count")
        If blnHasErrorOccurred Then
            MessageBox.Show("PIC counts have not all been applied. Please contact IT support.", "PIC Count Not All Applied", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            If MessageBox.Show("PIC Counts Have Been Applied", "Question", MessageBoxButtons.OK, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then Exit Sub
        End If
    End Sub
    Private Sub PicApplyProcess()
        Dim blnUpdate As Boolean = False
        Dim blnHasErrorOccurred As Boolean = False

        If MessageBox.Show("Apply all PIC Counts Y/N", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

        'second verification: manager
        If AuthoriseCodeSecurity(True, mintFirstUserID, picCodeInstance.SecondUserId) = False Then Exit Sub

        PicApplyProcessCalculation(picCodeInstance.SecondUserId, picCodeInstance.UpdateFlag, picCodeInstance.ErrorFlag)

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim sheet As SheetView = spdItems.Sheets(0)
        Dim print As SheetView = spdItems.Sheets(1)

        'remove any existing rows
        If print.RowCount > 0 Then
            print.Rows.Remove(0, print.RowCount)
        End If

        'loop through spdItems and copy any visible rows to report sheet
        For rowIndex As Integer = 0 To sheet.RowCount - 1
            If sheet.Rows(rowIndex).Visible Then
                print.Rows.Add(print.RowCount, 1)
                print.Cells(print.RowCount - 1, col.SkuNumber).Value = sheet.Cells(rowIndex, col.SkuNumber).Value
                print.Cells(print.RowCount - 1, col.Description).Value = sheet.Cells(rowIndex, col.Description).Value
                print.Cells(print.RowCount - 1, col.Supplier).Value = sheet.Cells(rowIndex, col.Supplier).Value
                print.Cells(print.RowCount - 1, col.PartCode).Value = sheet.Cells(rowIndex, col.PartCode).Value
                print.Cells(print.RowCount - 1, col.Price).Value = sheet.Cells(rowIndex, col.Price).Value
                print.Cells(print.RowCount - 1, col.StockStart).Value = sheet.Cells(rowIndex, col.StockStart).Value
                print.Cells(print.RowCount - 1, col.StockSold).Value = sheet.Cells(rowIndex, col.StockSold).Value
                print.Cells(print.RowCount - 1, col.StockCount).Value = sheet.Cells(rowIndex, col.StockCount).Value
            End If
        Next

        spdItems.PrintSheet(print)

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Function GetTotalRecords() As Integer
        Dim Count As Integer = 0
        For Each objDetail As HandHeldTerminalDetail In picCodeInstance.HhtHeaderFactoryInstance.Details
            Count += 1
        Next
        Return Count
    End Function

#End Region

#Region "Other Events"

    Private Sub rdo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAll.CheckedChanged, rdoNonCounted.CheckedChanged, rdoVariances.CheckedChanged

        Dim sheet As SheetView = spdItems.ActiveSheet
        If sheet.RowCount = 0 Then Exit Sub

        If rdoAll.Checked Then
            sheet.Rows(0, sheet.RowCount - 1).Visible = True

        ElseIf rdoNonCounted.Checked Then
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                sheet.Rows(rowIndex).Visible = Not CType(sheet.Cells(rowIndex, col.Counted).Value, Boolean)
            Next

        Else
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                If CType(sheet.Cells(rowIndex, col.StockVar).Value, Integer) <> 0 Then
                    sheet.Rows(rowIndex).Visible = True
                Else
                    sheet.Rows(rowIndex).Visible = False
                End If
            Next

        End If

    End Sub


#End Region

End Class