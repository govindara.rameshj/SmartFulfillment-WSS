using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using WSS.BO.DataLayer.Model;
using System.Xml.Serialization;
using WSS.BO.Pic.StaAuditUpload.BL;
using System.IO;
using System.Threading;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;
using Cts.Oasys.Core.System.User;
using Cts.Oasys.WinForm.User;
using WSS.BO.Pic.StaAuditUpload.Host.Enums;
using slf4net;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public partial class StaAuditUploadForm : Form
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(StaAuditUploadForm));

        private AsyncExecuter asyncExecuter;
        private FileOpener fileOpener;
        private ModeAccessor modeAccessor;
        private List<Adjustment> adjustments;
        private FileUploadState fileUploadState;
        private Authorizer authorizer;

        public StaAuditUploadForm(IDataLayerFactory repositoriesFactory)
        {
            InitializeComponent();
            Init(repositoriesFactory);
        }

        private void Init(IDataLayerFactory repositoriesFactory)
        {
            var settings = new Settings(repositoriesFactory.Create<IDictionariesRepository>());

            var fileNameParser = new FileNameParser(settings.DateExtractor, settings.DateFormat);

            fileUploadState = new FileUploadState(repositoriesFactory, fileNameParser);

            this.fileOpener = new FileOpener(settings.FolderPath, settings.FileMask, fileNameParser);

            authorizer = new Authorizer();

            this.asyncExecuter = new AsyncExecuter(repositoriesFactory);
            this.asyncExecuter.Completed += Completed;
            this.asyncExecuter.UpdateProgress += progress.UpdateProgress;

            this.buttons.LoadAction += LoadButtonAction;
            this.buttons.ActionAction += ActionButtonAction;
            this.buttons.ExitAction += ExitButtonAction;

            // All logic form mode related was moved into FormMode
            // If you need add some new change ui logic realted on form mode
            // please use this class.
            this.modeAccessor = new ModeAccessor(Modes.Ready);
            this.modeAccessor.ModeChanged += (from, to, haveErrors) => buttons.UpdateState(to);
            this.modeAccessor.ModeChanged += (from, to, haveErrors) => status.UpdateStatus(from, to);
            this.modeAccessor.ModeChanged += (from, to, haveErrors) => UpdateState(to, haveErrors);

            this.FormClosing += (sender, e) => MainFormClosing(e);
        }

        private void UpdateState(Modes to, bool haveErrors)
        {
            switch (to)
            {
                case Modes.Loaded:
                    progress.Init(adjustments.Count);
                    break;
                case Modes.Finished:
                    fileUploadState.MarkAsProcessed(
                        status.FileNameOnly,
                        progress.TotalAdjustments,
                        !haveErrors);
                    break;
                case Modes.InAction:
                    break;
                default:
                    progress.Init(0);
                    break;
            }
        }

        private Modes LoadAdjustments()
        {
            adjustments = new AdjustmentsParser().Parse(status.FullFileName);

            if (adjustments != null && adjustments.Any())
            {
                Logger.Info("Adjustments was Loaded from file {0}, records = {1}", status.FileNameOnly, adjustments.Count);
                return Modes.Loaded;
            }

            return Modes.NotLoadded;
        }

        private void LoadButtonAction(object sender, EventArgs e)
        {
            try
            {
                if (!fileOpener.IsFolderAccessible)
                {
                    var message = string.Format("Application can't open folder: {0}", fileOpener.FolderPath);
                    MessageBox.Show(message, "Load", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Logger.Error(message);
                    return;
                }

                if (fileOpener.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                this.Refresh();
                status.FullFileName = fileOpener.SelectedFile;

                if (fileUploadState.IsProcessed(status.FileNameOnly))
                {
                    modeAccessor.Set(Modes.AlreadyProcessed);
                    Logger.Info("Audit file {0} was already processed", status.FileNameOnly);
                }
                else
                {
                    modeAccessor.Set(LoadAdjustments());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ExitButtonAction(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ActionButtonAction(object sender, EventArgs e)
        {
            try
            {
                var result = authorizer.Authorize();
                switch (result)
                {
                    case AuthorizerResults.NotAuthorized:
                        Logger.Info("Not Authorized");
                        break;
                    case AuthorizerResults.OK:
                        Logger.Info("Authorization was successful");
                        StartApplyAdjustment();
                        break;
                    case AuthorizerResults.FailUserIsNotManager:
                        MessageBox.Show("Please get a Manager to authorise.", "User is not a Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Logger.Info("Manager authorisation was failed");
                        break;
                    case AuthorizerResults.FailServiceDeskAuthorisation:
                        MessageBox.Show("Please pass Service Desk authorisation.", "Service desk authorisation is needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Logger.Info("Service desk authorisation was failed");
                        break;
                    default:
                        Logger.Warn("Unknown AuthorizerResults");
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 

        private void StartApplyAdjustment()
        {
            Debug.Assert(modeAccessor.Value == Modes.Loaded);

            Logger.Info("Adjustments applying was started.");
            modeAccessor.Set(Modes.InAction);
            asyncExecuter.Execute(adjustments);
        }

        private void MainFormClosing(FormClosingEventArgs e)
        {
            if (modeAccessor.Value == Modes.InAction)
            {
                e.Cancel = true;
                MessageBox.Show("Application closing is not possible, while audit processing is in progress.");
            }
        }
        
        private void Completed(bool haveErrors, Exception ex)
        {
            if (ex != null)
            {
                Logger.Error(ex.ToString());
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                modeAccessor.Set(Modes.FatalError, haveErrors);
                this.Close();
            }
            modeAccessor.Set(Modes.Finished, haveErrors);
            Logger.Info("Adjustments applying was finished.");
        }
    }
}
