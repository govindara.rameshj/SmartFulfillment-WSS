﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public enum Modes
    {
        Ready,
        AlreadyProcessed,
        NotLoadded,
        Loaded,
        InAction,
        Finished,
        FatalError
    }
}
