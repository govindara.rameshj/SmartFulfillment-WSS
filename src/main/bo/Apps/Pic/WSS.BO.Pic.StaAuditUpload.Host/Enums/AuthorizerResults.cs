using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.Pic.StaAuditUpload.Host.Enums
{
    public enum AuthorizerResults
    {
        OK,
        NotAuthorized,
        FailUserIsNotManager,
        FailServiceDeskAuthorisation
    }
}
