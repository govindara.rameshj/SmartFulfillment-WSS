﻿namespace WSS.BO.Pic.StaAuditUpload.Host
{
    partial class StaAuditUploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttons = new WSS.BO.Pic.StaAuditUpload.Host.ActionButtons();
            this.status = new WSS.BO.Pic.StaAuditUpload.Host.StatusInfo();
            this.progress = new WSS.BO.Pic.StaAuditUpload.Host.ProgressInfo();
            this.SuspendLayout();
            // 
            // buttons
            // 
            this.buttons.Location = new System.Drawing.Point(7, 121);
            this.buttons.Name = "buttons";
            this.buttons.Size = new System.Drawing.Size(552, 47);
            this.buttons.TabIndex = 1;
            // 
            // status
            // 
            this.status.FullFileName = null;
            this.status.Location = new System.Drawing.Point(12, 10);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(540, 32);
            this.status.TabIndex = 4;
            this.status.TabStop = false;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(2, 43);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(560, 83);
            this.progress.TabIndex = 3;
            this.progress.TabStop = false;
            // 
            // StaAuditUploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 169);
            this.ControlBox = false;
            this.Controls.Add(this.buttons);
            this.Controls.Add(this.status);
            this.Controls.Add(this.progress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StaAuditUploadForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WX Process Stocktake Adjustments";
            this.ResumeLayout(false);

        }

        #endregion

        private ProgressInfo progress;
        private StatusInfo status;
        private ActionButtons buttons;
    }
}

