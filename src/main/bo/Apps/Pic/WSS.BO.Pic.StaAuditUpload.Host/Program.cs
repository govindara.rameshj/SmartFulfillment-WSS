using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Model;
using slf4net;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Constants;
using System.Diagnostics;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Filter;
using log4net.Core;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;
using System.Configuration;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    static class Program
    {
        private static readonly slf4net.ILogger Logger = LoggerFactory.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                var repositoriesFactory = GetFactory();
                SetUpLogger(repositoriesFactory);
                Run(repositoriesFactory);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static IDataLayerFactory GetFactory()
        {
            var resolver = new SimpleResolver();
            var repositoriesFactory = resolver.Resolve<IDataLayerFactory>();
            return repositoriesFactory;
        }

        private static void Run(IDataLayerFactory repositoriesFactory)
        {
            Logger.Info("Starting application: WSS.BO.Pic.StaAuditUpload.Host");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new StaAuditUploadForm(repositoriesFactory));

            Logger.Info("Application is exited: WSS.BO.Pic.StaAuditUpload.Host");
        }
        
        private static void SetUpLogger(IDataLayerFactory repositoriesFactory)
        {
            var logAppenderCreator = new LogAppenderCreator(
                repositoriesFactory.Create<IDictionariesRepository>()
                    .GetSettingStringValue(ParameterId.WickesCommsPath));
            logAppenderCreator.Create("AuditErrorFile",
                ConfigurationManager.AppSettings["auditErrorFileNamePattern"],
                Level.Error, 
                "%date %level [thread: %thread] %message%newline");
            logAppenderCreator.Create("StFile",
                ConfigurationManager.AppSettings["stFileNamePattern"],
                Level.Info, 
                "%message%newline");
        }
    }
}
