using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using WSS.BO.Pic.StaAuditUpload.BL;
using WSS.BO.DataLayer.Model;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;
using slf4net;
using System.Windows.Forms;
using System.Diagnostics;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public class AsyncExecuter
    {
        private static readonly slf4net.ILogger Logger = LoggerFactory.GetLogger(typeof(AsyncExecuter));

        public event Action<bool, Exception> Completed;
        public event Action<int, int, int> UpdateProgress;
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly BackgroundWorker executer = new BackgroundWorker();

        private Status lastStatus;

        public AsyncExecuter(IDataLayerFactory dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
            executer.WorkerReportsProgress = true;
            executer.RunWorkerCompleted += (sender, e) => OnCompleted(e);
            executer.ProgressChanged += (sender, e) => OnProgressChanged(e);
            executer.DoWork += (sender, e) => DoWork(e);
        }

        public void Execute(List<Adjustment> adjustments)
        {
            executer.RunWorkerAsync(adjustments);
        }

        private void DoWork(DoWorkEventArgs e)
        {
            var processor = new AdjustmentsProcessor(dataLayerFactory);
            processor.Process((List<Adjustment>)e.Argument,
                (total, completed, error) => executer.ReportProgress(0,
                    new Status { Total = total, Completed = completed, Error = error }));
        }

        private void OnProgressChanged(ProgressChangedEventArgs e)
        {
            lastStatus = (Status)e.UserState;
            if (UpdateProgress != null)
            {
                UpdateProgress(lastStatus.Total, lastStatus.Completed, lastStatus.Error);
            }
        }

        private void OnCompleted(RunWorkerCompletedEventArgs e)
        {
            if (Completed != null)
            {
                Completed(HaveLogicError(e), e.Error);
            }
        }

        private bool HaveLogicError(RunWorkerCompletedEventArgs e) 
        {
            if (e.Error != null)
            {
                return true;
            }
            if (lastStatus == null)
            {
                return true;
            }
            return lastStatus.Error != 0;
        }
    }
}
