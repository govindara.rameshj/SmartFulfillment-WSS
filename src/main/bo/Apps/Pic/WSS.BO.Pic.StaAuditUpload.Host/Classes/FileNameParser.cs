﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public class FileNameParser
    {
        private readonly string extractorPattern;
        private readonly string dateFormat;

        public FileNameParser(string extractorPattern, string dateFormat)
        {
            this.extractorPattern = extractorPattern;
            this.dateFormat = dateFormat;
        }

        public DateTime GetDate(string fileName)
        {
            var dateAsString = GetDateAsString(fileName);
            return DateTime.ParseExact(dateAsString, dateFormat , CultureInfo.InvariantCulture);
        }

        public bool IsValid(string fileName)
        {
            var dateAsString = GetDateAsString(fileName);
            DateTime result;
            return DateTime.TryParseExact(dateAsString, dateFormat, 
                CultureInfo.InvariantCulture, DateTimeStyles.None, out result);
        }

        private string GetDateAsString(string fileName)
        {
            var match = Regex.Match(fileName, extractorPattern, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return match.Groups[1].Value;
            }
            return string.Empty;
        }
    }
}
