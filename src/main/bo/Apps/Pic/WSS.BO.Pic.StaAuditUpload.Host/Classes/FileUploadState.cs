﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public class FileUploadState
    {
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly FileNameParser fileNameParser;

        public FileUploadState(IDataLayerFactory dataLayerFactory,
            FileNameParser dateTimeExtractor)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.fileNameParser = dateTimeExtractor;
        }

        public bool IsProcessed(string fileName)
        {
            IList<ControlFile> controlFiles = dataLayerFactory.Create<IControlFileRepository>().GetControlFile(
                ControlFileName.ExternalAudit,
                fileNameParser.GetDate(fileName),
                true);

            return controlFiles.Any();
        }

        public void MarkAsProcessed(string fileName, decimal total, bool isSuccess)
        {
            var controlFile = new ControlFile();
            controlFile.FileName = ControlFileName.ExternalAudit;
            controlFile.TransactionDate = fileNameParser.GetDate(fileName);
            controlFile.TransactionTime = DateTime.Now.ToString("HH:mm:ss");
            controlFile.RecordType1 = ControlFileType.ExternalAudit;
            controlFile.RecordHash1 = total;
            controlFile.Flag = isSuccess;
            InsertControlFile(controlFile);
        }

        private void InsertControlFile(ControlFile controlFile)
        {
            using (var unitOfWork = dataLayerFactory.CreateUnitOfWork())
            {
                var controlFileRepository = unitOfWork.Create<IControlFileRepository>();

                var lastAvailableVersion = controlFileRepository.GetLatestAvailableVersionRecord(controlFile.FileName);

                controlFile.VersionNumber = GetNextVersion(lastAvailableVersion.LastestAvailableVersionNumber);
                controlFile.SequenceNumber = GetNextSequence(lastAvailableVersion.LastestAvailableSequenceNumber);

                controlFileRepository.Delete(controlFile.FileName, controlFile.VersionNumber);
                controlFileRepository.InsertControlFile(controlFile);

                controlFileRepository.UpdateLatestAvailableVersionRecord(controlFile);

                unitOfWork.Commit();
            }
        }

        private string GetNextSequence(string sequence)
        {
            var sequenceAsInt = int.Parse(sequence);
            sequenceAsInt++;
            if (sequenceAsInt > 999999)
            {
                sequenceAsInt = 1;
            }
            return sequenceAsInt.ToString("000000");
        }

        private string GetNextVersion(string version)
        {
            var versionAsInt = int.Parse(version);
            versionAsInt++;
            if (versionAsInt > 31)
            {
                versionAsInt = 1;
            }
            return versionAsInt.ToString("00");
        }
    }
}
