﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Repositories;
using log4net.Core;
using log4net;
using WSS.BO.DataLayer.Model.Constants;
using log4net.Appender;
using log4net.Layout;
using log4net.Filter;
using System.Diagnostics;
using WSS.BO.Pic.StaAuditUpload.BL;

namespace WSS.BO.Pic.StaAuditUpload.Host.Classes
{
    public class LogAppenderCreator
    {
        private readonly string commsPath;
        private readonly ILog logHost;
        private readonly ILog logBl;

        public LogAppenderCreator(string commsPath)
        {
            this.commsPath = commsPath;
            this.logBl = log4net.LogManager.GetLogger(GetLibAssemblyName());
            this.logHost = log4net.LogManager.GetLogger(GetProgramAssemblyName());
        }

        public void Create(string name, string filePathPattern, Level level, string layoutPattern)
        {
            var appender = new RollingFileAppender();
            appender.Name = name;
            appender.File = string.Format(filePathPattern, commsPath, DateTime.Now);
            appender.AppendToFile = true;

            var layout = new PatternLayout();
            layout.ConversionPattern = layoutPattern;
            layout.ActivateOptions();
            appender.Layout = layout;

            var levelRangeFilter = new LevelRangeFilter();
            levelRangeFilter.LevelMin = level;
            appender.AddFilter(levelRangeFilter);
            appender.ActivateOptions();

            ((log4net.Repository.Hierarchy.Logger)logBl.Logger).AddAppender(appender);
            ((log4net.Repository.Hierarchy.Logger)logHost.Logger).AddAppender(appender);
        }

        private string GetProgramAssemblyName()
        {
            return GetFirstPartOnly(typeof(Program).Module.Assembly.FullName);
        }

        private string GetLibAssemblyName()
        {
            return GetFirstPartOnly(typeof(AdjustmentsProcessor).Module.Assembly.FullName);
        }

        private string GetFirstPartOnly(string fullName)
        {
            return fullName.Substring(0, fullName.IndexOf(','));
        }
    }
}
