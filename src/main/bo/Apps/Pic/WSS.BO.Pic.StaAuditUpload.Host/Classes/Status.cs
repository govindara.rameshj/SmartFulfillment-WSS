﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.Pic.StaAuditUpload.Host.Classes
{
    public class Status
    {
        public int Total { get; set; }
        public int Completed { get; set; }
        public int Error { get; set; }

        public bool IsFinal
        {
            get 
            {
                return Total == Completed + Error;
            }
        }
    }
}
