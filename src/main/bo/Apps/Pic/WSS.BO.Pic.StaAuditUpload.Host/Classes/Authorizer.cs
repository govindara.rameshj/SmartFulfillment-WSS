using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cts.Oasys.WinForm.User;
using Cts.Oasys.Core.System.User;
using WSS.BO.Pic.StaAuditUpload.Host.Enums;

namespace WSS.BO.Pic.StaAuditUpload.Host.Classes
{
    public class Authorizer
    {
        public AuthorizerResults Authorize()
        {
            AuthorizerResults result = AuthorizerResults.NotAuthorized;
            var user = GetUser();
            if (user != null)
            {
                if (!user.IsManager)
                {
                    result = AuthorizerResults.FailUserIsNotManager;
                }
                else
                {
                    result = PerformServiceDeskAuthorization()
                        ? AuthorizerResults.OK
                        : AuthorizerResults.FailServiceDeskAuthorisation;
                }
            }
            return result;
        }

        private bool PerformServiceDeskAuthorization()
        {
            var serviceDeskAuthorise = new ServiceDeskAuthorise();
            serviceDeskAuthorise.ShowDialog();
            return serviceDeskAuthorise.IsAuthorized;
        }

        private User GetUser()
        {
            var userAuthorise = new UserAuthorise(LoginLevel.Manager);
            userAuthorise.ShowDialog();
            return userAuthorise.User;
        }
    }
}
