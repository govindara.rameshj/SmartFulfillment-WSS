﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public class ModeAccessor
    {
        public event Action<Modes, Modes, bool> ModeChanged;

        private Modes mode;

        public Modes Value
        {
            get
            {
                return mode;
            }
        }

        public ModeAccessor(Modes state)
        {
            this.mode = state;
        }
        
        public void Set(Modes newMode)
        {
            Set(newMode, true);
        }

        public void Set(Modes newMode, bool haveErrors)
        {
            OnModeChanged(this.mode, newMode, haveErrors);
            this.mode = newMode;
        }
        
        private void OnModeChanged(Modes from, Modes to, bool haveErrors)
        {
            var hander = ModeChanged;
            if (hander != null)
            {
                hander(from, to, haveErrors);
            }
        }
    }
}
