using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using System.Diagnostics;

namespace WSS.BO.Pic.StaAuditUpload.Host.Classes
{
    public class Settings
    {
        public string FolderPath { get; private set; }
        public string FileMask { get; private set; }
        public string DateFormat { get; private set; }
        public string DateExtractor { get; private set; }

        public Settings(IDictionariesRepository repository)
        {
            var fullPath = repository.GetSettingStringValue(ParameterId.StaAuditUploadFolderPath);
            var storeNumber = GetStoreNumber(repository);
            var maskPattern = GetMaskPattern(fullPath);

            FolderPath = GetFolderPath(fullPath);
            FileMask = ReplaceStoreAndDate(maskPattern, storeNumber, FilePathPart.All);
            DateExtractor = ReplaceStoreAndDate(maskPattern, storeNumber, FilePathPart.GroupExtractor);
            DateFormat = repository.GetSettingStringValue(ParameterId.StaAuditUploadDateFormat);
        }

        private static string GetMaskPattern(string fullPath)
        {
            var splitPosition = GetSplitPosition(fullPath);
            return fullPath.Substring(splitPosition, fullPath.Length - splitPosition);
        }

        private static int GetSplitPosition(string fullPath)
        {
            return fullPath.LastIndexOf('\\') + 1;
        }

        private static string GetStoreNumber(IDictionariesRepository repository)
        {
            var retailOptions = repository.SelectRetailOptions();
            var storeNumber = RetailOptions.GetAbsoluiteStoreId(retailOptions.StoreId).ToString();
            return storeNumber;
        }

        private string GetFolderPath(string fullPath)
        {
            var splitPosition = GetSplitPosition(fullPath);
            return fullPath.Substring(0, splitPosition);
        }

        private string ReplaceStoreAndDate(string pattern, string store, string date)
        {
            return pattern.Replace(FilePathPart.StorePlaceHolder, store)
                .Replace(FilePathPart.DatePlaceHolder, date);
        }
    }
}
