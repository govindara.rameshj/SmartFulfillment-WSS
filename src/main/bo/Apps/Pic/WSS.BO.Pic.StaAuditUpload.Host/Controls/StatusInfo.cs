using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public partial class StatusInfo : UserControl
    {
        private const string LOADED_AUDIT = "Loaded Audit: {0}";
        private const string PROCESSING_AUDIT = "Processing Audit: {0}";
        private const string FINISHED_AUDIT = "Processing finished : {0}";
        private const string ALREADY_PROCESSED_AUDIT = "Audit Processed, please contact IT Support. File: {0}";
        private const string FILE_HAS_NOT_ADJUSTMENTS = "The file '{0}' has no adjustment to process. Please, chose other file and try again.";

        public string FullFileName { set; get; }

        public string FileNameOnly
        {
            get
            {
                return Path.GetFileName(FullFileName).ToUpperInvariant();
            }
        }

        public StatusInfo()
        {
            InitializeComponent();
        }

        public void UpdateStatus(Modes from, Modes to)
        {
            if (from == Modes.Ready && to == Modes.Ready)
            {
                setStatus(string.Empty);
            }
            else if (to == Modes.Loaded)
            {
                setStatus(GetMessage(LOADED_AUDIT));
            }
            else if (from == Modes.Loaded && to == Modes.InAction)
            {
                setStatus(GetMessage(PROCESSING_AUDIT));
            }
            else if (from == Modes.InAction && to == Modes.Finished)
            {
                setStatus(GetMessage(FINISHED_AUDIT));
            }
            else if (to == Modes.AlreadyProcessed)
            {
                setStatus(GetMessage(ALREADY_PROCESSED_AUDIT));
            }
            else if (to == Modes.NotLoadded)
            {
                setStatus(GetMessage(FILE_HAS_NOT_ADJUSTMENTS));
            }
        }

        private void setStatus(string message)
        {
            lblStatus.Text = message;
        }

        private string GetMessage(string pattern)
        {
            return string.Format(pattern, FileNameOnly);
        }
    }
}
