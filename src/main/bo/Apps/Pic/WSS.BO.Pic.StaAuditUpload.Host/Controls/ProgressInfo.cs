using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public partial class ProgressInfo : UserControl
    {
        private int totalAdjustments;

        public int TotalAdjustments
        {
            private set
            {
                Set(txtTotalAdjustments, value);
                progressBar.Value = 0;
                progressBar.Maximum = value;
                totalAdjustments = value;
            }
            get
            {
                return totalAdjustments;
            }
        }

        public ProgressInfo()
        {
            InitializeComponent();
            progressBar.Step = 1;
        }

        public void Init(int total)
        {
            TotalAdjustments = total;
            SetCompletedAdjustments(0);
            SetRemainingAdjustments(total);
            SetErrorAdjustments(0);
        }

        public void UpdateProgress(int total, int completed, int error)
        {
            SetErrorAdjustments(error);
            SetCompletedAdjustments(completed);

            var newRemaining = total - error - completed;
            SetRemainingAdjustments(newRemaining);
            UpdateProgressBar(newRemaining);
        }

        private void UpdateProgressBar(int remaining)
        {
            while (progressBar.Value < totalAdjustments - remaining)
            {
                progressBar.PerformStep();
            }
        }

        private void Set(TextBox target, int value)
        {
            target.Text = value.ToString();
        }

        private void SetRemainingAdjustments(int count)
        {
            Set(txtRemainingAdjustments, count);
        }

        private void SetCompletedAdjustments(int count)
        {
            Set(txtCompletedAdjustments, count);
        }

        private void SetErrorAdjustments(int count)
        {
            Set(txtErrorAdjustments, count);
        }
    }
}
