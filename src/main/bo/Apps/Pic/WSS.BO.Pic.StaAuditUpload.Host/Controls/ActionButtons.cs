using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WSS.BO.Pic.StaAuditUpload.Host.Classes;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public partial class ActionButtons : UserControl
    {
        public delegate void ActionHandler(object sender, EventArgs e);
        public event ActionHandler LoadAction;
        public event ActionHandler ActionAction;
        public event ActionHandler ExitAction;

        public ActionButtons()
        {
            InitializeComponent();
        }

        public void UpdateState(Modes to)
        {
            switch (to)
            {
                case Modes.Loaded:
                    btnAction.Enabled = true;
                    btnExit.Enabled = true;
                    btnLoad.Enabled = true;
                    break;
                case Modes.InAction:
                    btnAction.Enabled = false;
                    btnExit.Enabled = false;
                    btnLoad.Enabled = false;
                    break;
                default:
                    btnAction.Enabled = false;
                    btnExit.Enabled = true;
                    btnLoad.Enabled = true;
                    break;
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (LoadAction != null)
            {
                LoadAction(sender, e);
            }
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            if (ActionAction != null)
            {
                ActionAction(sender, e);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (ExitAction != null)
            {
                ExitAction(sender, e);
            }
        }
    }
}
