﻿namespace WSS.BO.Pic.StaAuditUpload.Host
{
    partial class ProgressInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtErrorAdjustments = new System.Windows.Forms.TextBox();
            this.txtCompletedAdjustments = new System.Windows.Forms.TextBox();
            this.txtRemainingAdjustments = new System.Windows.Forms.TextBox();
            this.txtTotalAdjustments = new System.Windows.Forms.TextBox();
            this.lblErrorAdjustments = new System.Windows.Forms.Label();
            this.lblCompletedAdjustments = new System.Windows.Forms.Label();
            this.lblRemainingAdjustments = new System.Windows.Forms.Label();
            this.lblTotalAdjustments = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // txtErrorAdjustments
            // 
            this.txtErrorAdjustments.BackColor = System.Drawing.SystemColors.Window;
            this.txtErrorAdjustments.Enabled = false;
            this.txtErrorAdjustments.Location = new System.Drawing.Point(478, 46);
            this.txtErrorAdjustments.Name = "txtErrorAdjustments";
            this.txtErrorAdjustments.ReadOnly = true;
            this.txtErrorAdjustments.Size = new System.Drawing.Size(74, 20);
            this.txtErrorAdjustments.TabIndex = 22;
            // 
            // txtCompletedAdjustments
            // 
            this.txtCompletedAdjustments.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompletedAdjustments.Enabled = false;
            this.txtCompletedAdjustments.Location = new System.Drawing.Point(341, 46);
            this.txtCompletedAdjustments.Name = "txtCompletedAdjustments";
            this.txtCompletedAdjustments.ReadOnly = true;
            this.txtCompletedAdjustments.Size = new System.Drawing.Size(75, 20);
            this.txtCompletedAdjustments.TabIndex = 21;
            // 
            // txtRemainingAdjustments
            // 
            this.txtRemainingAdjustments.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemainingAdjustments.Enabled = false;
            this.txtRemainingAdjustments.Location = new System.Drawing.Point(194, 46);
            this.txtRemainingAdjustments.Name = "txtRemainingAdjustments";
            this.txtRemainingAdjustments.ReadOnly = true;
            this.txtRemainingAdjustments.Size = new System.Drawing.Size(87, 20);
            this.txtRemainingAdjustments.TabIndex = 20;
            // 
            // txtTotalAdjustments
            // 
            this.txtTotalAdjustments.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalAdjustments.Enabled = false;
            this.txtTotalAdjustments.Location = new System.Drawing.Point(60, 46);
            this.txtTotalAdjustments.Name = "txtTotalAdjustments";
            this.txtTotalAdjustments.ReadOnly = true;
            this.txtTotalAdjustments.Size = new System.Drawing.Size(74, 20);
            this.txtTotalAdjustments.TabIndex = 19;
            // 
            // lblErrorAdjustments
            // 
            this.lblErrorAdjustments.AutoSize = true;
            this.lblErrorAdjustments.Location = new System.Drawing.Point(422, 49);
            this.lblErrorAdjustments.Name = "lblErrorAdjustments";
            this.lblErrorAdjustments.Size = new System.Drawing.Size(48, 13);
            this.lblErrorAdjustments.TabIndex = 18;
            this.lblErrorAdjustments.Text = "A-ERRO";
            // 
            // lblCompletedAdjustments
            // 
            this.lblCompletedAdjustments.AutoSize = true;
            this.lblCompletedAdjustments.Location = new System.Drawing.Point(287, 49);
            this.lblCompletedAdjustments.Name = "lblCompletedAdjustments";
            this.lblCompletedAdjustments.Size = new System.Drawing.Size(48, 13);
            this.lblCompletedAdjustments.TabIndex = 17;
            this.lblCompletedAdjustments.Text = "A-COMP";
            // 
            // lblRemainingAdjustments
            // 
            this.lblRemainingAdjustments.AutoSize = true;
            this.lblRemainingAdjustments.Location = new System.Drawing.Point(140, 49);
            this.lblRemainingAdjustments.Name = "lblRemainingAdjustments";
            this.lblRemainingAdjustments.Size = new System.Drawing.Size(48, 13);
            this.lblRemainingAdjustments.TabIndex = 16;
            this.lblRemainingAdjustments.Text = "A-REMA";
            // 
            // lblTotalAdjustments
            // 
            this.lblTotalAdjustments.AutoSize = true;
            this.lblTotalAdjustments.Location = new System.Drawing.Point(9, 49);
            this.lblTotalAdjustments.Name = "lblTotalAdjustments";
            this.lblTotalAdjustments.Size = new System.Drawing.Size(45, 13);
            this.lblTotalAdjustments.TabIndex = 15;
            this.lblTotalAdjustments.Text = "A-TOTL";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 10);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(540, 23);
            this.progressBar.TabIndex = 14;
            // 
            // ProgressInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtErrorAdjustments);
            this.Controls.Add(this.txtCompletedAdjustments);
            this.Controls.Add(this.txtRemainingAdjustments);
            this.Controls.Add(this.txtTotalAdjustments);
            this.Controls.Add(this.lblErrorAdjustments);
            this.Controls.Add(this.lblCompletedAdjustments);
            this.Controls.Add(this.lblRemainingAdjustments);
            this.Controls.Add(this.lblTotalAdjustments);
            this.Controls.Add(this.progressBar);
            this.Name = "ProgressInfo";
            this.Size = new System.Drawing.Size(560, 81);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtErrorAdjustments;
        private System.Windows.Forms.TextBox txtCompletedAdjustments;
        private System.Windows.Forms.TextBox txtRemainingAdjustments;
        private System.Windows.Forms.TextBox txtTotalAdjustments;
        private System.Windows.Forms.Label lblErrorAdjustments;
        private System.Windows.Forms.Label lblCompletedAdjustments;
        private System.Windows.Forms.Label lblRemainingAdjustments;
        private System.Windows.Forms.Label lblTotalAdjustments;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}
