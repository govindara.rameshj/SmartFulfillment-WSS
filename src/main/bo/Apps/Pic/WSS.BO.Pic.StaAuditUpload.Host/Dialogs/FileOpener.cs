using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.AccessControl;
using slf4net;

namespace WSS.BO.Pic.StaAuditUpload.Host
{
    public partial class FileOpener : Form
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(FileOpener));
        private readonly FileNameParser fileNameParser;
        private readonly string mask;

        public string FolderPath { get; private set; }
        
        public string SelectedFile 
        {
            get
            {
                if (string.IsNullOrEmpty(txtFileName.Text))
                {
                    return string.Empty;
                }
                return Path.Combine(FolderPath, txtFileName.Text);
            }
        }

        public bool IsFolderAccessible
        {
            get
            {
                return IsFolderExistAndReadable(FolderPath);
            }
        }

        public FileOpener(string folderPath, string mask, FileNameParser fileNameParser) : this()
        {
            this.FolderPath = folderPath;
            this.Text = folderPath;
            this.mask = mask;
            this.fileNameParser = fileNameParser;
        }

        public FileOpener()
        {
            InitializeComponent();

            lvFiles.MouseDoubleClick += (sender, e) => SelectFile();
            lvFiles.SelectedIndexChanged += (sender, e) => UpdateSelectedFile();

            VisibleChanged += (sender, e) => UpdateSelectedFile();
            Load += (sender, e) => GetFileNames(mask);
        }

        private void SetSelectedFile(string file)
        {
            txtFileName.Text = file;
            btnOpen.Enabled = !string.IsNullOrEmpty(txtFileName.Text);
        }
        
        private void GetFileNames(string pattern)
        {
            lvFiles.Items.Clear();

            foreach (string file in Directory.GetFiles(FolderPath, pattern))
            {
                var fileName = Path.GetFileName(file);
                if (fileNameParser.IsValid(fileName))
                {
                    lvFiles.Items.Add(fileName);
                }
            }
        }

        private void UpdateSelectedFile()
        {
            SetSelectedFile(GetSelectedFile());
        }

        private string GetSelectedFile()
        {
            return this.lvFiles.FocusedItem != null && this.lvFiles.SelectedItems.Count > 0
                ? this.lvFiles.FocusedItem.Text 
                :  null;
        }

        private void SelectFile()
        {
            UpdateSelectedFile();
            this.btnOpen.PerformClick();
        }

        private bool IsFolderExistAndReadable(string path)
        {   
            if (!Directory.Exists(path))
            {
                return false;
            }
            return IsFolderReadable(path);
        }

        private static bool IsFolderReadable(string path)
        {
            try
            {
                Directory.GetFiles(path);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                return false;
            }
        }
    }
}
