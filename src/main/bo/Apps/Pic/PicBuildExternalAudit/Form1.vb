﻿Public Class Form1
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private WithEvents _HhtHeader As BOStockTake.cHhtHeader

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Trace.WriteLine(My.Resources.AppExit, Name)

    End Sub

    Private Sub Form1_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try
            Cursor = Cursors.WaitCursor
            Trace.WriteLine(My.Resources.AppStart, Name)
            lbl.Text = My.Resources.PerformBuild

            Refresh()
            _HhtHeader = New BOStockTake.cHhtHeader(_Oasys3DB)
            AddHandler _HhtHeader.UpdateProgess, AddressOf UpdateProgess
            _HhtHeader.BuildExternalAuditCount(Now.Date)

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Name)
            lbl.Text = ex.Message
        Finally
            Cursor = Cursors.Default
            Close()
        End Try

    End Sub

    Public Sub UpdateProgess(ByVal percent As Integer, ByVal message As String)

        pgb.Value = percent
        lbl.Text = message
        Refresh()

    End Sub

End Class
