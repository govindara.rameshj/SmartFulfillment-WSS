Imports Cts.Oasys.Core.System

Public Class Audit
    Private Enum col
        SkuNumber
        Description
        Category
        Price
        NormalStart
        NormalCount
        NormalVar
        NormalVarValue
        MarkdownStart
        MarkdownCount
        MarkdownVar
        MarkdownVarValue
        TotalStart
        TotalCount
        TotalVar
        TotalVarValue
    End Enum
    Private Enum colWork
        SkuNumber
        Description
        Category
        Price
        NormalShop
        NormalWare
        NormalTotal
        NormalStart
        NormalVar
        MarkdownCount
        MarkdownStart
        MarkdownVar
        Manager
    End Enum
    Private Enum colReport
        SkuNumber
        Description
        Category
        PackSize
        Price
        NormalStart
        NormalCount
        MarkdownStart
        MarkdownCount
        TotalStart
        TotalCount
        AdjustNorm
        AdjustMark
        AdjustTotal
    End Enum
    Private Enum index
        normStart
        normCount
        markStart
        markCount
        adjustNorm
        adjustMark
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _HieCategory As New BOHierarchy.cHierachyCategory(_Oasys3DB)
    Private _Audit As BOStockTake.cAudit
    Private _storeIdName As String = String.Empty


    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnPopulate.PerformClick()
            Case Keys.F6 : btnAccept.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        _HieCategory.Load()
        dtpDate_ValueChanged(Me, New EventArgs)
        _storeIdName = Store.GetIdName

    End Sub



    Private Sub spdItems_Initialise()

        Try
            If spdItems.Sheets.Count > 0 Then
                spdItems.ActiveSheet.Rows.Remove(0, spdItems.ActiveSheet.RowCount)
                Exit Sub
            End If

            Dim sheet As New SheetView
            sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
            sheet.SelectionUnit = Model.SelectionUnit.Row
            sheet.SelectionPolicy = Model.SelectionPolicy.Single
            sheet.RowCount = 0
            sheet.RowHeader.ColumnCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
            sheet.ColumnHeader.RowCount = 2
            sheet.ColumnHeaderVisible = True

            'default properties
            sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
            sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
            sheet.DefaultStyle.CellType = New CellType.TextCellType
            sheet.DefaultStyle.Locked = True

            'make F keys available in spread
            Dim im As InputMap = spdItems.GetInputMap(InputMapMode.WhenAncestorOfFocused)
            im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            Dim imFocused As InputMap = spdItems.GetInputMap(InputMapMode.WhenFocused)
            imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            'columns types
            Dim typeInt As New CellType.NumberCellType
            typeInt.DecimalPlaces = 0

            'column headers
            sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
            sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)

            sheet.ColumnHeader.Rows(0, 1).HorizontalAlignment = CellHorizontalAlignment.Center
            sheet.ColumnHeader.Rows(1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
            sheet.ColumnHeader.Cells(1, col.SkuNumber, 1, col.Description).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.ColumnHeader.Cells(0, col.NormalStart).Value = "Normal Stock"
            sheet.ColumnHeader.Cells(0, col.NormalStart).ColumnSpan = 4
            sheet.ColumnHeader.Cells(0, col.MarkdownStart).Value = "Markdown"
            sheet.ColumnHeader.Cells(0, col.MarkdownStart).ColumnSpan = 4
            sheet.ColumnHeader.Cells(0, col.TotalStart).Value = "Total"
            sheet.ColumnHeader.Cells(0, col.TotalStart).ColumnSpan = 4

            'columns
            sheet.Columns(col.SkuNumber, col.Category).AllowAutoSort = True
            sheet.Columns(col.SkuNumber, col.Category).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.Columns(col.SkuNumber).Label = "SKU"
            sheet.Columns(col.Description).Label = "Description"
            sheet.Columns(col.Category).Label = "Category"
            sheet.Columns(col.Price).Label = "Price"
            sheet.Columns(col.Price).CellType = New CellType.NumberCellType
            sheet.Columns(col.NormalStart, col.TotalVar).Width = 60
            sheet.Columns(col.NormalStart, col.TotalVar).Resizable = False
            sheet.Columns(col.NormalStart, col.NormalVar).CellType = typeInt
            sheet.Columns(col.NormalStart).Label = "Start"
            sheet.Columns(col.NormalCount).Label = "Count"
            sheet.Columns(col.NormalCount).Locked = False
            sheet.Columns(col.NormalVar).Label = "Variance"
            sheet.Columns(col.NormalVarValue).Label = "Value"
            sheet.Columns(col.NormalVarValue).CellType = New CellType.NumberCellType
            sheet.Columns(col.MarkdownStart, col.MarkdownVar).CellType = typeInt
            sheet.Columns(col.MarkdownStart).Label = "Start"
            sheet.Columns(col.MarkdownCount).Label = "Count"
            sheet.Columns(col.MarkdownCount).Locked = False
            sheet.Columns(col.MarkdownVar).Label = "Variance"
            sheet.Columns(col.MarkdownVarValue).Label = "Value"
            sheet.Columns(col.MarkdownVarValue).CellType = New CellType.NumberCellType
            sheet.Columns(col.TotalStart, col.TotalVar).CellType = typeInt
            sheet.Columns(col.TotalStart).Label = "Start"
            sheet.Columns(col.TotalCount).Label = "Count"
            sheet.Columns(col.TotalVar).Label = "Variance"
            sheet.Columns(col.TotalVarValue).Label = "Value"
            sheet.Columns(col.TotalVarValue).CellType = New CellType.NumberCellType

            spdItems.EditModeReplace = True
            spdItems.Sheets.Add(sheet)
            AddHandler spdItems.Resize, AddressOf spd_Resize

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdItems_AddRows()

        Try
            Dim sheet As SheetView = spdItems.ActiveSheet
            Dim counted As Integer = 0
            Dim adjusted As Integer = 0
            Dim labelOK As Integer = 0
            Dim entered As Boolean = True

            For Each detail As BOStockTake.cAudit In _Audit.Details
                Dim category As String = "No Category found"
                If _HieCategory.Hierarchy(detail.Stock.HierCategory.Value) IsNot Nothing Then
                    category = _HieCategory.Hierarchy(detail.Stock.HierCategory.Value).Description.Value
                End If
                If Not detail.EntryMade.Value Then entered = False

                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Cells(sheet.RowCount - 1, col.SkuNumber).Value = detail.SkuNumber.Value
                sheet.Cells(sheet.RowCount - 1, col.Description).Value = detail.Stock.Description.Value
                sheet.Cells(sheet.RowCount - 1, col.Category).Value = category
                sheet.Cells(sheet.RowCount - 1, col.Price).Value = detail.Stock.NormalSellPrice.Value
                sheet.Cells(sheet.RowCount - 1, col.NormalStart).Value = detail.NormalStock.Value
                sheet.Cells(sheet.RowCount - 1, col.NormalCount).Value = detail.NormalCount.Value
                sheet.Cells(sheet.RowCount - 1, col.NormalVar).Formula = "RC[-2]-RC[-1]"
                sheet.Cells(sheet.RowCount - 1, col.NormalVarValue).Formula = "RC[-4]*RC[-1]"
                sheet.Cells(sheet.RowCount - 1, col.MarkdownStart).Value = detail.MarkdownStock.Value
                sheet.Cells(sheet.RowCount - 1, col.MarkdownCount).Value = detail.MarkdownCount.Value
                sheet.Cells(sheet.RowCount - 1, col.MarkdownVar).Formula = "RC[-2]-RC[-1]"
                sheet.Cells(sheet.RowCount - 1, col.MarkdownVarValue).Formula = "RC[-8]*RC[-1]"
                sheet.Cells(sheet.RowCount - 1, col.TotalStart).Formula = "RC[-8]+RC[-4]"
                sheet.Cells(sheet.RowCount - 1, col.TotalCount).Formula = "RC[-8]+RC[-4]"
                sheet.Cells(sheet.RowCount - 1, col.TotalVar).Formula = "RC[-8]+RC[-4]"
                sheet.Cells(sheet.RowCount - 1, col.TotalVarValue).Formula = "RC[-12]*RC[-1]"
            Next

            If sheet.RowCount > 0 Then
                If entered Then
                    btnPrint.Text = My.Resources.F9PrintReport
                Else
                    btnPrint.Text = My.Resources.F9PrintWorksheet
                End If

                sheet.ColumnHeaderVisible = True
                btnAccept.Visible = True
                btnPrint.Visible = True
                spd_Resize(spdItems, New EventArgs)
            Else
                btnAccept.Visible = False
                btnPrint.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdItems_AutoSortingColumn(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.AutoSortingColumnEventArgs) Handles spdItems.AutoSortingColumn

        e.Sheet.ClearSelection()

    End Sub


    Private Sub spdWorksheet_Initialise()

        Try
            If spdWorksheet.Sheets.Count > 0 Then
                spdWorksheet.ActiveSheet.Rows.Remove(0, spdWorksheet.ActiveSheet.RowCount)
                Exit Sub
            End If

            Dim sheet As New SheetView
            sheet.RowCount = 0
            sheet.RowHeader.ColumnCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(colWork)).Length
            sheet.ColumnHeader.RowCount = 2
            sheet.VerticalGridLine = New GridLine(GridLineType.None)

            'Input maps
            Dim im As InputMap = spdItems.GetInputMap(InputMapMode.WhenAncestorOfFocused)
            im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
            im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
            im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

            'Print info.
            sheet.PrintInfo.Header = "/c/fz""14""Auditors Worksheet"
            sheet.PrintInfo.Header &= "/n/c/fz""10""Store for: " & _storeIdName
            sheet.PrintInfo.Header &= "/n/c/fz""10""Date for: " & _Audit.AuditDate.Value.ToShortDateString
            sheet.PrintInfo.Header &= "/n "
            sheet.PrintInfo.Footer = "/lPrinted: " & Now
            sheet.PrintInfo.Footer &= "/cPage /p of /pc"
            sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            sheet.PrintInfo.Margin.Left = 40
            sheet.PrintInfo.Margin.Right = 40
            'sheet.PrintInfo.ShowBorder = False
            sheet.PrintInfo.ShowPrintDialog = True
            sheet.PrintInfo.UseMax = False
            sheet.PrintInfo.Orientation = PrintOrientation.Landscape

            'columns types
            Dim typeInt As New CellType.NumberCellType
            typeInt.DecimalPlaces = 0

            'column headers
            sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
            sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
            sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
            sheet.ColumnHeader.Rows(1).Border = New LineBorder(Color.Black, 1, False, False, False, True)

            sheet.ColumnHeader.Cells(0, colWork.NormalShop).Value = "Normal Stock"
            sheet.ColumnHeader.Cells(0, colWork.NormalShop).ColumnSpan = 5
            sheet.ColumnHeader.Cells(0, colWork.MarkdownCount).Value = "Markdown Stock"
            sheet.ColumnHeader.Cells(0, colWork.MarkdownCount).ColumnSpan = 4

            'columns
            sheet.Columns.Default.Resizable = False
            sheet.Columns(colWork.SkuNumber, colWork.Category).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.Columns(colWork.SkuNumber).Label = "SKU"
            sheet.Columns(colWork.SkuNumber).Width = 50
            sheet.Columns(colWork.Description).Label = "Description"
            sheet.Columns(colWork.Description).Width = 245
            sheet.Columns(colWork.Category).Label = "Category"
            sheet.Columns(colWork.Category).Width = 100
            sheet.Columns(colWork.Price).Label = "Price"
            sheet.Columns(colWork.Price).CellType = New CellType.NumberCellType
            sheet.Columns(colWork.Price).Width = 50
            sheet.Columns(colWork.NormalShop, colWork.Manager).Width = 70
            sheet.Columns(colWork.NormalShop, colWork.MarkdownVar).CellType = typeInt
            sheet.Columns(colWork.NormalShop).Border = New LineBorder(Color.Black, 1, True, False, False, False)
            sheet.Columns(colWork.NormalShop).Label = "Shop Flr"
            sheet.Columns(colWork.NormalWare).Label = "Warehouse"
            sheet.Columns(colWork.NormalTotal).Label = "Total"
            sheet.Columns(colWork.NormalStart).Label = "On Hand"
            sheet.Columns(colWork.NormalVar).Label = "Variance"
            sheet.Columns(colWork.MarkdownCount).Border = New LineBorder(Color.Black, 1, True, False, False, False)
            sheet.Columns(colWork.MarkdownCount).Label = "Total"
            sheet.Columns(colWork.MarkdownStart).Label = "On Hand"
            sheet.Columns(colWork.MarkdownVar).Label = "Variance"
            sheet.Columns(colWork.Manager).Label = "Manager"

            spdWorksheet.Visible = True
            spdWorksheet.Sheets.Add(sheet)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdWorksheet_AddRows()

        Try
            Dim sheet As SheetView = spdItems.ActiveSheet
            Dim worksheet As SheetView = spdWorksheet.ActiveSheet

            'loop through spdItems and copy any visible rows to report sheet
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                worksheet.Rows.Add(worksheet.RowCount, 1)
                worksheet.Cells(worksheet.RowCount - 1, colWork.SkuNumber).Value = sheet.Cells(rowIndex, col.SkuNumber).Value
                worksheet.Cells(worksheet.RowCount - 1, colWork.Description).Value = sheet.Cells(rowIndex, col.Description).Value
                worksheet.Cells(worksheet.RowCount - 1, colWork.Category).Value = sheet.Cells(rowIndex, col.Category).Value
                worksheet.Cells(worksheet.RowCount - 1, colWork.Price).Value = sheet.Cells(rowIndex, col.Price).Value
                worksheet.Cells(worksheet.RowCount - 1, colWork.NormalStart).Value = sheet.Cells(rowIndex, col.NormalStart).Value
                worksheet.Cells(worksheet.RowCount - 1, colWork.MarkdownStart).Value = sheet.Cells(rowIndex, col.MarkdownStart).Value
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub spdReport_Initialise()

        Try
            If spdReport.Sheets.Count > 0 Then
                spdReport.ActiveSheet.Rows.Remove(0, spdReport.ActiveSheet.RowCount)
                Exit Sub
            End If

            Dim sheet As New SheetView
            sheet.RowCount = 0
            sheet.RowHeader.ColumnCount = 0
            sheet.ColumnCount = [Enum].GetValues(GetType(colReport)).Length
            sheet.ColumnHeader.RowCount = 2
            sheet.VerticalGridLine = New GridLine(GridLineType.None)

            'Print info.
            sheet.PrintInfo.Header = "/c/fz""14""Auditors Report"
            sheet.PrintInfo.Header &= "/n/c/fz""10""Store for: " & _storeIdName
            sheet.PrintInfo.Header &= "/n/c/fz""10""Date for: " & _Audit.AuditDate.Value.ToShortDateString
            sheet.PrintInfo.Header &= "/n "
            sheet.PrintInfo.Footer = "/lPrinted: " & Now
            sheet.PrintInfo.Footer &= "/cPage /p of /pc"
            sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
            sheet.PrintInfo.Margin.Left = 40
            sheet.PrintInfo.Margin.Right = 40
            sheet.PrintInfo.ShowBorder = False
            sheet.PrintInfo.ShowPrintDialog = True
            sheet.PrintInfo.UseMax = False
            sheet.PrintInfo.Orientation = PrintOrientation.Landscape

            'columns types
            Dim typeInt As New CellType.NumberCellType
            typeInt.DecimalPlaces = 0

            'column headers
            sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
            sheet.ColumnHeader.DefaultStyle.CellType = New CellType.TextCellType
            sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
            sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
            sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
            sheet.ColumnHeader.Rows(1).Border = New LineBorder(Color.Black, 1, False, False, False, True)

            sheet.ColumnHeader.Cells(0, colReport.NormalStart).Value = "Normal Stock"
            sheet.ColumnHeader.Cells(0, colReport.NormalStart).ColumnSpan = 2
            sheet.ColumnHeader.Cells(0, colReport.MarkdownStart).Value = "Markdown Stock"
            sheet.ColumnHeader.Cells(0, colReport.MarkdownStart).ColumnSpan = 2
            sheet.ColumnHeader.Cells(0, colReport.TotalStart).Value = "All Stock"
            sheet.ColumnHeader.Cells(0, colReport.TotalStart).ColumnSpan = 2
            sheet.ColumnHeader.Cells(0, colReport.AdjustNorm).Value = "Audit Adjustments"
            sheet.ColumnHeader.Cells(0, colReport.AdjustNorm).ColumnSpan = 3

            'columns
            sheet.Columns.Default.Resizable = False
            sheet.Columns(colReport.SkuNumber, colWork.Category).HorizontalAlignment = CellHorizontalAlignment.Left
            sheet.Columns(colReport.SkuNumber).Label = "SKU"
            sheet.Columns(colReport.SkuNumber).Width = 50
            sheet.Columns(colReport.Description).Label = "Description"
            sheet.Columns(colReport.Description).Width = 245
            sheet.Columns(colReport.Category).Label = "Category"
            sheet.Columns(colReport.Category).Width = 100
            sheet.Columns(colReport.Category).Visible = False
            sheet.Columns(colReport.PackSize).Label = "Pack"
            sheet.Columns(colReport.PackSize).Width = 50
            sheet.Columns(colReport.Price).Label = "Price"
            sheet.Columns(colReport.Price).CellType = New CellType.NumberCellType
            sheet.Columns(colReport.Price).Width = 50
            sheet.Columns(colReport.NormalStart, colReport.AdjustTotal).Width = 70
            sheet.Columns(colReport.NormalStart, colReport.AdjustTotal).CellType = typeInt
            sheet.Columns(colReport.NormalStart).Label = "Start"
            sheet.Columns(colReport.NormalCount).Label = "Count"
            sheet.Columns(colReport.MarkdownStart).Label = "Start"
            sheet.Columns(colReport.MarkdownCount).Label = "Count"
            sheet.Columns(colReport.TotalStart).Label = "Start"
            sheet.Columns(colReport.TotalCount).Label = "Count"
            sheet.Columns(colReport.AdjustNorm).Label = "Normal"
            sheet.Columns(colReport.AdjustMark).Label = "Markdown"
            sheet.Columns(colReport.AdjustTotal).Label = "All"

            spdReport.Visible = True
            spdReport.Sheets.Add(sheet)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdReport_AddRows()

        Try
            Dim sheet As SheetView = spdItems.ActiveSheet
            Dim repsheet As SheetView = spdReport.ActiveSheet
            If sheet.RowCount = 0 Then Exit Sub

            'initialise variables
            Dim counts([Enum].GetValues(GetType(index)).Length) As Integer
            Dim values([Enum].GetValues(GetType(index)).Length) As Decimal
            Dim totalCounts([Enum].GetValues(GetType(index)).Length) As Integer
            Dim totalValues([Enum].GetValues(GetType(index)).Length) As Decimal
            Dim category = CStr(sheet.Cells(0, col.Category).Value)

            For rowIndex As Integer = 0 To sheet.RowCount - 1
                Dim cat = CStr(sheet.Cells(rowIndex, col.Category).Value)
                If cat <> category Then
                    spdReport_AddTotals(category, counts, values, totalCounts, totalValues)
                    category = cat
                End If

                Dim sku As String = CStr(sheet.Cells(rowIndex, col.SkuNumber).Value)
                Dim price As Decimal = CDec(sheet.Cells(rowIndex, col.Price).Value)
                Dim nStart As Integer = CInt(sheet.Cells(rowIndex, col.NormalStart).Value)
                Dim nCount As Integer = CInt(sheet.Cells(rowIndex, col.NormalCount).Value)
                Dim mStart As Integer = CInt(sheet.Cells(rowIndex, col.MarkdownStart).Value)
                Dim mCount As Integer = CInt(sheet.Cells(rowIndex, col.MarkdownCount).Value)

                'add item to report
                repsheet.Rows.Add(repsheet.RowCount, 1)
                repsheet.Cells(repsheet.RowCount - 1, colReport.SkuNumber).Value = sku
                repsheet.Cells(repsheet.RowCount - 1, colReport.Description).Value = sheet.Cells(rowIndex, col.Description).Value
                repsheet.Cells(repsheet.RowCount - 1, colReport.Category).Value = cat
                repsheet.Cells(repsheet.RowCount - 1, colReport.PackSize).Value = _Audit.Detail(sku).Stock.SupplierPackSize.Value
                repsheet.Cells(repsheet.RowCount - 1, colReport.Price).Value = price

                repsheet.Cells(repsheet.RowCount - 1, colReport.NormalStart).Value = nStart
                repsheet.Cells(repsheet.RowCount - 1, colReport.NormalCount).Value = nCount
                repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownStart).Value = mStart
                repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownCount).Value = mCount
                repsheet.Cells(repsheet.RowCount - 1, colReport.TotalStart).Value = nStart + mStart
                repsheet.Cells(repsheet.RowCount - 1, colReport.TotalCount).Value = nCount + mCount
                repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = nCount - nStart
                repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = mCount - mStart
                repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustTotal).Value = nCount + mCount - nStart - mStart

                'update values
                counts(index.normStart) += nStart
                counts(index.normCount) += nCount
                counts(index.markStart) += mStart
                counts(index.markCount) += mCount
                counts(index.adjustNorm) += Math.Abs(nCount - nStart)
                counts(index.adjustMark) += Math.Abs(mCount - mStart)

                values(index.normStart) += (nStart * price)
                values(index.normCount) += (nCount * price)
                values(index.markStart) += (mStart * price)
                values(index.markCount) += (mCount * price)
                values(index.adjustNorm) += (Math.Abs(nCount - nStart) * price)
                values(index.adjustMark) += (Math.Abs(mCount - mStart) * price)
            Next

            spdReport_AddTotals(category, counts, values, totalCounts, totalValues)
            spdReport_AddTotals("Sample", totalCounts, totalValues, totalCounts, totalValues)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub spdReport_AddTotals(ByVal category As String, ByRef counts() As Integer, ByRef values() As Decimal, ByRef totalCounts() As Integer, ByRef totalValues() As Decimal)

        Try
            Dim repsheet As SheetView = spdReport.ActiveSheet
            repsheet.Rows.Add(repsheet.RowCount, 1)
            repsheet.Cells(repsheet.RowCount - 1, colReport.NormalStart, repsheet.RowCount - 1, colReport.AdjustTotal).Border = New LineBorder(Color.Black, 1, False, True, False, False)
            repsheet.Cells(repsheet.RowCount - 1, colReport.Description).Value = "Totals for " & category
            repsheet.Cells(repsheet.RowCount - 1, colReport.NormalStart).Value = counts(index.normStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.NormalCount).Value = counts(index.normCount)
            repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownStart).Value = counts(index.markStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownCount).Value = counts(index.markCount)
            repsheet.Cells(repsheet.RowCount - 1, colReport.TotalStart).Value = counts(index.normStart) + counts(index.markStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.TotalCount).Value = counts(index.normCount) + counts(index.markCount)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = counts(index.adjustNorm)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = counts(index.adjustMark)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustTotal).Value = counts(index.adjustNorm) + counts(index.adjustMark)

            'insert total values
            repsheet.Rows.Add(repsheet.RowCount, 1)
            repsheet.Rows(repsheet.RowCount - 1).CellType = New CellType.NumberCellType
            repsheet.Cells(repsheet.RowCount - 1, colReport.NormalStart).Value = values(index.normStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.NormalCount).Value = values(index.normCount)
            repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownStart).Value = values(index.markStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.MarkdownCount).Value = values(index.markCount)
            repsheet.Cells(repsheet.RowCount - 1, colReport.TotalStart).Formula = CStr(values(index.normStart) + values(index.markStart))
            repsheet.Cells(repsheet.RowCount - 1, colReport.TotalCount).Formula = CStr(values(index.normCount) + values(index.markCount))
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = values(index.adjustNorm)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = values(index.adjustMark)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustTotal).Formula = CStr(values(index.adjustNorm) + values(index.adjustMark))

            'insert accuracy units
            repsheet.Rows.Add(repsheet.RowCount, 1)
            repsheet.Rows(repsheet.RowCount - 1).CellType = New CellType.PercentCellType
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = 0
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = 0
            If counts(index.normStart) <> 0 Then repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = (counts(index.normStart) - counts(index.adjustNorm)) / counts(index.normStart)
            If counts(index.markStart) <> 0 Then repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = (counts(index.markStart) - counts(index.adjustMark)) / counts(index.markStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustTotal).Value = CInt(repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value) + CInt(repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value)

            'insert accuracy values
            repsheet.Rows.Add(repsheet.RowCount, 1)
            repsheet.Rows(repsheet.RowCount - 1).Border = New LineBorder(Color.Black, 1, False, False, False, True)
            repsheet.Rows(repsheet.RowCount - 1).CellType = New CellType.PercentCellType
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = 0
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = 0
            If values(index.normStart) <> 0 Then repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value = (values(index.normStart) - values(index.adjustNorm)) / values(index.normStart)
            If values(index.markStart) <> 0 Then repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value = (values(index.markStart) - values(index.adjustMark)) / values(index.markStart)
            repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustTotal).Value = CInt(repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustNorm).Value) + CInt(repsheet.Cells(repsheet.RowCount - 1, colReport.AdjustMark).Value)

            For Each i As index In [Enum].GetValues(GetType(index))
                totalCounts(i) += counts(i)
                totalValues(i) += values(i)
                counts(i) = 0
                values(i) = 0
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs)

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub



    Private Sub dtpDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpAuditDate.ValueChanged

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus()

            _Audit = New BOStockTake.cAudit(_Oasys3DB)
            _Audit.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, _Audit.AuditDate, dtpAuditDate.Value)
            If _Audit.LoadMatches.Count = 0 Then _Audit.AuditDate.Value = dtpAuditDate.Value.Date
            If _Audit.Details.Count = 0 Then Exit Sub

            spdItems_Initialise()
            spdItems_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub



    Private Sub btnPopulate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPopulate.Click

        Try
            Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoPopulate, Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If result = DialogResult.No Then Exit Sub

            'get number of items required
            Dim number As Integer = 0
            Dim percent As Decimal = 0
            Dim entry As String = InputBox(My.Resources.InputItems, Me.FindForm.Text)

            If entry.Length = 0 Then
                DisplayStatus(My.Resources.WarnNumericValue)
                Exit Sub
            End If

            If entry.Last = "%"c Then
                If Not Decimal.TryParse(entry.Remove(entry.Length - 1, 1), percent) Then
                    DisplayStatus(My.Resources.WarnValidPercentage)
                    Exit Sub
                End If
            Else
                If Not Integer.TryParse(entry, number) Then
                    DisplayStatus(My.Resources.WarnValidNumber)
                    Exit Sub
                End If
            End If

            DisplayStatus(My.Resources.PopAudit)
            Cursor = Cursors.WaitCursor

            _Audit.Details.Clear()
            Dim count As Integer = 0
            count += _Audit.GetHhtItems(number, percent)
            count += CInt(IIf(_Audit.GetPicAuditItems(), 1, 0))

            If count = 0 Then
                MessageBox.Show(My.Resources.NoItemsReturned, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            spdItems_Initialise()
            spdItems_AddRows()

            DisplayStatus()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.AcceptAudit)

            Dim sheet As SheetView = spdItems.ActiveSheet
            For rowIndex As Integer = 0 To sheet.RowCount - 1
                Dim skuNumber As String = sheet.Cells(rowIndex, col.SkuNumber).Value.ToString
                _Audit.Detail(skuNumber).NormalCount.Value = CInt(sheet.Cells(rowIndex, col.NormalCount).Value)
                _Audit.Detail(skuNumber).MarkdownCount.Value = CInt(sheet.Cells(rowIndex, col.MarkdownCount).Value)
            Next

            _Audit.Update()

            MessageBox.Show(My.Resources.AuditSaved, Me.FindForm.Text)

            btnPrint.Text = My.Resources.F9PrintReport
            DisplayStatus()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Select Case btnPrint.Text
            Case My.Resources.F9PrintWorksheet
                spdWorksheet_Initialise()
                spdWorksheet_AddRows()
                spdWorksheet.PrintSheet(-1)

            Case My.Resources.F9PrintReport
                spdReport_Initialise()
                spdReport_AddRows()
                spdReport.PrintSheet(-1)
        End Select

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class
