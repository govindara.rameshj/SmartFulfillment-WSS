<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Audit
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance2 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Dim TipAppearance3 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.btnExit = New System.Windows.Forms.Button
        Me.spdItems = New FarPoint.Win.Spread.FpSpread
        Me.btnAccept = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.spdWorksheet = New FarPoint.Win.Spread.FpSpread
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtpAuditDate = New System.Windows.Forms.DateTimePicker
        Me.btnPopulate = New System.Windows.Forms.Button
        Me.spdReport = New FarPoint.Win.Spread.FpSpread
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spdWorksheet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spdReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(821, 558)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'spdItems
        '
        Me.spdItems.About = "3.0.2004.2005"
        Me.spdItems.AccessibleDescription = "spdOrder, Count Items, Row 0, Column 0, "
        Me.spdItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdItems.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.HorizontalScrollBarHeight = 15
        Me.spdItems.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.Location = New System.Drawing.Point(3, 29)
        Me.spdItems.Name = "spdItems"
        Me.spdItems.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.Size = New System.Drawing.Size(894, 523)
        Me.spdItems.TabIndex = 2
        Me.spdItems.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdItems.TextTipAppearance = TipAppearance1
        Me.spdItems.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.VerticalScrollBarWidth = 15
        Me.spdItems.ActiveSheetIndex = -1
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAccept.Location = New System.Drawing.Point(85, 558)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 4
        Me.btnAccept.Text = "F6 Accept Audit"
        Me.btnAccept.UseVisualStyleBackColor = True
        Me.btnAccept.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPrint.Location = New System.Drawing.Point(167, 558)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 5
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'spdWorksheet
        '
        Me.spdWorksheet.About = "3.0.2004.2005"
        Me.spdWorksheet.AccessibleDescription = "spdOrder, Count Items, Row 0, Column 0, "
        Me.spdWorksheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdWorksheet.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdWorksheet.HorizontalScrollBarHeight = 15
        Me.spdWorksheet.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdWorksheet.Location = New System.Drawing.Point(714, 29)
        Me.spdWorksheet.Name = "spdWorksheet"
        Me.spdWorksheet.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdWorksheet.Size = New System.Drawing.Size(183, 98)
        Me.spdWorksheet.TabIndex = 9
        Me.spdWorksheet.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance2.BackColor = System.Drawing.SystemColors.Info
        TipAppearance2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdWorksheet.TextTipAppearance = TipAppearance2
        Me.spdWorksheet.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdWorksheet.VerticalScrollBarWidth = 15
        Me.spdWorksheet.Visible = False
        Me.spdWorksheet.ActiveSheetIndex = -1
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(367, 20)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Audit Date"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAuditDate
        '
        Me.dtpAuditDate.Location = New System.Drawing.Point(85, 3)
        Me.dtpAuditDate.Name = "dtpAuditDate"
        Me.dtpAuditDate.Size = New System.Drawing.Size(146, 20)
        Me.dtpAuditDate.TabIndex = 1
        '
        'btnPopulate
        '
        Me.btnPopulate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPopulate.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPopulate.Location = New System.Drawing.Point(3, 558)
        Me.btnPopulate.Name = "btnPopulate"
        Me.btnPopulate.Size = New System.Drawing.Size(76, 39)
        Me.btnPopulate.TabIndex = 3
        Me.btnPopulate.Text = "F5 Populate Audit"
        Me.btnPopulate.UseVisualStyleBackColor = True
        '
        'spdReport
        '
        Me.spdReport.About = "3.0.2004.2005"
        Me.spdReport.AccessibleDescription = "spdOrder, Count Items, Row 0, Column 0, "
        Me.spdReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdReport.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdReport.HorizontalScrollBarHeight = 15
        Me.spdReport.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdReport.Location = New System.Drawing.Point(714, 133)
        Me.spdReport.Name = "spdReport"
        Me.spdReport.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdReport.Size = New System.Drawing.Size(183, 98)
        Me.spdReport.TabIndex = 11
        Me.spdReport.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance3.BackColor = System.Drawing.SystemColors.Info
        TipAppearance3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance3.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdReport.TextTipAppearance = TipAppearance3
        Me.spdReport.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdReport.VerticalScrollBarWidth = 15
        Me.spdReport.Visible = False
        Me.spdReport.ActiveSheetIndex = -1
        '
        'Audit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnPopulate)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.spdItems)
        Me.Controls.Add(Me.dtpAuditDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.spdWorksheet)
        Me.Controls.Add(Me.spdReport)
        Me.DoubleBuffered = True
        Me.Name = "Audit"
        Me.Size = New System.Drawing.Size(900, 600)
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spdWorksheet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spdReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents spdItems As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents spdWorksheet As FarPoint.Win.Spread.FpSpread
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpAuditDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPopulate As System.Windows.Forms.Button
    Friend WithEvents spdReport As FarPoint.Win.Spread.FpSpread

End Class
