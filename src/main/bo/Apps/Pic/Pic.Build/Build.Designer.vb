<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Build
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Build))
        Me.btnExit = New System.Windows.Forms.Button()
        Me.dtpHeader = New System.Windows.Forms.DateTimePicker()
        Me.spdItems = New FarPoint.Win.Spread.FpSpread()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnSelectAll = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSku = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnPic = New System.Windows.Forms.Button()
        Me.dtpRerun = New System.Windows.Forms.DateTimePicker()
        Me.btnPicExternal = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCountFile = New System.Windows.Forms.Button()
        Me.btnStockEnquiry = New System.Windows.Forms.Button()
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(821, 558)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'dtpHeader
        '
        Me.dtpHeader.Location = New System.Drawing.Point(134, 19)
        Me.dtpHeader.Name = "dtpHeader"
        Me.dtpHeader.Size = New System.Drawing.Size(136, 20)
        Me.dtpHeader.TabIndex = 0
        '
        'spdItems
        '
        Me.spdItems.About = "3.0.2004.2005"
        Me.spdItems.AccessibleDescription = "spdOrder, Count Items, Row 0, Column 0, "
        Me.spdItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spdItems.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.HorizontalScrollBarHeight = 15
        Me.spdItems.HorizontalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.Location = New System.Drawing.Point(3, 81)
        Me.spdItems.Name = "spdItems"
        Me.spdItems.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never
        Me.spdItems.Size = New System.Drawing.Size(894, 471)
        Me.spdItems.TabIndex = 2
        Me.spdItems.TabStrip.ButtonPolicy = FarPoint.Win.Spread.TabStripButtonPolicy.Never
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.spdItems.TextTipAppearance = TipAppearance1
        Me.spdItems.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never
        Me.spdItems.VerticalScrollBarWidth = 15
        Me.spdItems.ActiveSheetIndex = -1
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAccept.Enabled = False
        Me.btnAccept.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAccept.Location = New System.Drawing.Point(167, 558)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 5
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnRemove.Enabled = False
        Me.btnRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRemove.Location = New System.Drawing.Point(85, 558)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(76, 39)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "F4 Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSelectAll.Location = New System.Drawing.Point(3, 558)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(76, 39)
        Me.btnSelectAll.TabIndex = 3
        Me.btnSelectAll.Text = "F3"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(6, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(149, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "SKU Number (F2 for look up)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSku
        '
        Me.txtSku.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSku.Location = New System.Drawing.Point(161, 19)
        Me.txtSku.Name = "txtSku"
        Me.txtSku.Size = New System.Drawing.Size(349, 20)
        Me.txtSku.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(6, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 20)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Rerun Previous Build"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnPic
        '
        Me.btnPic.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPic.Location = New System.Drawing.Point(290, 27)
        Me.btnPic.Name = "btnPic"
        Me.btnPic.Size = New System.Drawing.Size(76, 39)
        Me.btnPic.TabIndex = 2
        Me.btnPic.Text = "F7 Pic Build"
        Me.btnPic.UseVisualStyleBackColor = True
        '
        'dtpRerun
        '
        Me.dtpRerun.Location = New System.Drawing.Point(134, 45)
        Me.dtpRerun.Name = "dtpRerun"
        Me.dtpRerun.Size = New System.Drawing.Size(136, 20)
        Me.dtpRerun.TabIndex = 1
        '
        'btnPicExternal
        '
        Me.btnPicExternal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPicExternal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPicExternal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPicExternal.Location = New System.Drawing.Point(657, 558)
        Me.btnPicExternal.Name = "btnPicExternal"
        Me.btnPicExternal.Size = New System.Drawing.Size(76, 39)
        Me.btnPicExternal.TabIndex = 6
        Me.btnPicExternal.Text = "F8 External Pic Build"
        Me.btnPicExternal.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtSku)
        Me.GroupBox1.Location = New System.Drawing.Point(381, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(516, 72)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enter Stock Items"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.dtpHeader)
        Me.GroupBox2.Controls.Add(Me.btnPic)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.dtpRerun)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(372, 72)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Select Dates"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(6, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 20)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Date to Maintain"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCountFile
        '
        Me.btnCountFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCountFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCountFile.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCountFile.Location = New System.Drawing.Point(739, 558)
        Me.btnCountFile.Name = "btnCountFile"
        Me.btnCountFile.Size = New System.Drawing.Size(76, 39)
        Me.btnCountFile.TabIndex = 7
        Me.btnCountFile.Text = "F10 Export Count File"
        Me.btnCountFile.UseVisualStyleBackColor = True
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnStockEnquiry.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnStockEnquiry.Location = New System.Drawing.Point(575, 558)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(76, 39)
        Me.btnStockEnquiry.TabIndex = 9
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        Me.btnStockEnquiry.UseVisualStyleBackColor = True
        '
        'Build
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.btnCountFile)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSelectAll)
        Me.Controls.Add(Me.btnPicExternal)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.spdItems)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnExit)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Build"
        Me.Size = New System.Drawing.Size(900, 600)
        CType(Me.spdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dtpHeader As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpRerun As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtSku As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents spdItems As FarPoint.Win.Spread.FpSpread
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnPic As System.Windows.Forms.Button
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnPicExternal As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCountFile As System.Windows.Forms.Button
    Friend WithEvents btnStockEnquiry As System.Windows.Forms.Button

End Class
