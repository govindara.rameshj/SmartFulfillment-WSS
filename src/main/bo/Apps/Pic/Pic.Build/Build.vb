Imports System.IO
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Export

Public Class Build

    Private Enum col
        Selected
        SkuNumber
        Description
        SupplierNumber
        SupplierName
        HieCategory
        HieGroup
        HieSubgroup
        HieStyle
        NonStock
        OnHand
        Markdown
        Labelled
        Origin
    End Enum

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _hierarchy As New BOHierarchy.HierarchyMasterCollection(_Oasys3DB)
    Private _supplier As New BOPurchases.vwSuppliersWithStockItems(_Oasys3DB)
    Private _canDeleteItems As Boolean

    ''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author       : Partha Dutta
    ' Create date  : 18/08/2011
    ' Referral No  : RF0861
    ' Description  : Select existing or new implementation of StockTake HandHeldTerminal business object
    ''''''''''''''''''''''''''''''''''''''''''''''''

    Private WithEvents _hhtHeader As New BOStockTake.cHhtHeader(_Oasys3DB)
    'Private _hhtHeader As IHHTHeader

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        btnSelectAll.Text = My.Resources.F3SelectNone
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnStockEnquiry.PerformClick()
            Case Keys.F3 : btnSelectAll.PerformClick()
            Case Keys.F4 : btnRemove.PerformClick()
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F7 : btnPic.PerformClick()
            Case Keys.F8 : btnPicExternal.PerformClick()
            Case Keys.F10 : btnCountFile.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 18/05/2011
    ' Referral No : 560
    ' Notes       : Modifed to Reload the records from STKMAS before nightly build.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Reloads the data after update 
    ''' </summary>
    ''' <remarks></remarks>
    '''  
    Protected Overrides Sub DoProcessing()

        ''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author       : Partha Dutta
        ' Create date  : 18/08/2011
        ' Referral No  : RF0861
        ' Description  : Select existing or new implementation of HandHeldTerminal business object
        ''''''''''''''''''''''''''''''''''''''''''''''''

        ' _hhtHeader = HHTHeaderFactory.FactoryGet(_Oasys3DB)
        'check for overnight process here
        If RunParameters.StartsWith("OVERNIGHT") Then
            Try
                Cursor = Cursors.WaitCursor
                DisplayStatus(My.Resources.BuildingCount)

                Using sysDates As New BOSystem.cSystemDates(_Oasys3DB)
                    sysDates.Load()

                    'build Pic and external audit count
                    _hhtHeader.Load(sysDates.Tomorrow.Value)
                    _hhtHeader.ReloadValues()
                    _hhtHeader.BuildCount(sysDates.Tomorrow.Value)
                    _hhtHeader.Update()
                    _hhtHeader.BuildExternalAuditCount(sysDates.Tomorrow.Value)

                    ExportDetialsIntoCsv(sysDates.Tomorrow.Value)
                End Using

            Finally
                Cursor = Cursors.Default
                FindForm.Close()
            End Try
        End If

        _canDeleteItems = (SecurityLevel >= Parameter.GetInteger(4401))
        _hierarchy.LoadAll()

        spdItems_Initialise()
        dtpHeader.Value = Dates.GetTomorrow
        dtpHeader.MinDate = Now.Date
        txtSku.Focus()

    End Sub

#Region "Other Event Handlers"

    Private Sub dtpHeader_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpHeader.ValueChanged

        Try
            Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author       : Partha Dutta
            ' Create date  : 18/08/2011
            ' Referral No  : RF0861
            ' Description  : Select existing or new implementation of HandHeldTerminal business object
            ''''''''''''''''''''''''''''''''''''''''''''''''


            _hhtHeader = New BOStockTake.cHhtHeader(_Oasys3DB)
            '_hhtHeader = HHTHeaderFactory.FactoryGet(_Oasys3DB)







            _hhtHeader.Load(dtpHeader.Value)

            If _hhtHeader.IsLocked.Value Then
                MessageBox.Show(My.Resources.BuildBeingCounted, Me.FindForm.Text)
                Exit Sub
            End If

            AddHandler _hhtHeader.UpdateProgess, AddressOf DisplayProgress
            AddHandler _hhtHeader.ClearProgress, AddressOf DisplayProgress

            spdItems_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub txtSku_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSku.KeyPress

        Try
            DisplayStatus()

            e.Handled = True
            If e.KeyChar = ChrW(Keys.Enter) Then
                If txtSku.Text.Trim = String.Empty Then Exit Sub

                'load stock item
                Using stock As New BOStock.cStock(_Oasys3DB)
                    stock.LoadStockItem(txtSku.Text.Trim)
                    _hhtHeader.AddDetail(stock)
                    spdItems_AddRows()
                End Using

                'clear and refocus sku entry
                txtSku.Clear()
                txtSku.Focus()
            End If

            If Char.IsControl(e.KeyChar) Then e.Handled = False
            If Char.IsDigit(e.KeyChar) Then e.Handled = False

        Catch ex As Exception
            txtSku.SelectAll()
            txtSku.Focus()
        End Try

    End Sub

#End Region

#Region "Button Events"

    Private Sub btnPic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPic.Click

        Try
            Cursor = Cursors.WaitCursor
            _hhtHeader.BuildCount(dtpRerun.Value)
            spdItems_AddRows()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnPicExternal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPicExternal.Click

        Try
            Cursor = Cursors.WaitCursor
            _hhtHeader.BuildExternalAuditCount(dtpRerun.Value)
            MessageBox.Show(My.Resources.ExternalBuildSuccessful, Me.FindForm.Text)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click

        Dim sheet As SheetView = spdItems.ActiveSheet

        Select Case btnSelectAll.Text
            Case My.Resources.F3SelectAll
                btnSelectAll.Text = My.Resources.F3SelectNone
                For rowIndex As Integer = 0 To sheet.RowCount - 1
                    sheet.Cells(rowIndex, col.Selected).Value = True
                Next

            Case My.Resources.F3SelectNone
                btnSelectAll.Text = My.Resources.F3SelectAll
                For rowIndex As Integer = 0 To sheet.RowCount - 1
                    sheet.Cells(rowIndex, col.Selected).Value = False
                Next
        End Select

        CheckSelected()

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 18/05/2011
    ' Referral No : 560
    ' Notes       : Modifed to Reload the records in the grid.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Reloads the data after update 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.SavingSelectedDetails)

            'first remove any non selected items from details collection and sheet
            Dim sheet As SheetView = spdItems.ActiveSheet
            For rowIndex As Integer = sheet.RowCount - 1 To 0 Step -1
                If Not CBool(sheet.Cells(rowIndex, col.Selected).Value) Then
                    _hhtHeader.Details.Remove(CType(sheet.Rows(rowIndex).Tag, cHhtDetail))
                    sheet.Rows.Remove(rowIndex, 1)
                End If
            Next

            'save header and details

            If (Date.Parse(Date.Parse(CStr(dtpHeader.Value)).ToShortDateString) > Today) Then
                _hhtHeader.Update(True)
            ElseIf (Date.Parse(Date.Parse(CStr(dtpHeader.Value)).ToShortDateString) = Today _
                Or Date.Parse(Date.Parse(CStr(dtpRerun.Value)).ToShortDateString) <= Today) Then
                _hhtHeader.Update()
            End If

            _hhtHeader.Details.Clear()
            _hhtHeader.LoadDetails()

            For rowIndex As Integer = sheet.RowCount - 1 To 0 Step -1
                If Not CBool(sheet.Cells(rowIndex, col.Selected).Value) Then
                    _hhtHeader.Details.Remove(CType(sheet.Rows(rowIndex).Tag, cHhtDetail))
                    sheet.Rows.Remove(rowIndex, 1)
                End If
            Next


            spdItems_AddRows()

            CheckSelected()
            DisplayStatus()

            MessageBox.Show(My.Resources.HhtSavedSuccessfully, Me.FindForm.Text)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        Try
            'get confirmation
            Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoDelete, Me.FindForm.Text, MessageBoxButtons.YesNo)
            If result <> DialogResult.Yes Then Exit Sub

            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.RemovingSelectedDetails)

            'delete all checked records and then remove from sheet
            Dim sheet As SheetView = spdItems.ActiveSheet
            For rowIndex As Integer = sheet.RowCount - 1 To 0 Step -1
                If CBool(sheet.Cells(rowIndex, col.Selected).Value) Then
                    _hhtHeader.Details.Remove(CType(sheet.Rows(rowIndex).Tag, cHhtDetail))
                    sheet.Rows.Remove(rowIndex, 1)
                End If
            Next

        Finally
            CheckSelected()
            Cursor = Cursors.Default
            DisplayStatus()
        End Try

    End Sub

    Private Sub btnCountFile_Click(sender As System.Object, e As System.EventArgs) Handles btnCountFile.Click

        Try
            Cursor = Cursors.WaitCursor
            ExportDetialsIntoCsv(dtpRerun.Value.Date)
            MessageBox.Show(My.Resources.ExportCountFileSuccessful, Me.FindForm.Text)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(UserId, WorkstationId, SecurityLevel, String.Empty)

            lookup.SetSelectMultiple(True)
            lookup.ShowAccept()

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Dim skuNumbers As ArrayList = lookup.SkuNumbers

                    For Each SkuNumber As String In skuNumbers
                        Using stock As New BOStock.cStock(_Oasys3DB)
                            stock.LoadStockItem(SkuNumber)
                            _hhtHeader.AddDetail(stock)
                        End Using
                    Next

                    spdItems_AddRows()
                End If
            End Using
        End Using
    End Sub

#End Region

#Region "Spread Control"

    Private Sub spdItems_Initialise()

        'make F keys available in spread
        Dim im As InputMap = spdItems.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim imFocused As InputMap = spdItems.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        imFocused.Put(New Keystroke(Keys.Tab, Keys.None), SpreadActions.None)

        Dim sheet As New SheetView
        sheet.ReferenceStyle = Model.ReferenceStyle.R1C1
        sheet.SelectionUnit = Model.SelectionUnit.Row
        sheet.SelectionPolicy = Model.SelectionPolicy.Single
        sheet.RowCount = 0
        sheet.RowHeader.ColumnCount = 0
        sheet.ColumnCount = [Enum].GetValues(GetType(col)).Length
        sheet.ColumnHeader.RowCount = 1
        sheet.ColumnHeaderVisible = False

        'default properties
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.Locked = True

        'columns types
        Dim typeInt As New CellType.NumberCellType
        typeInt.DecimalPlaces = 0

        'column headers
        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Right
        sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.Columns(col.SkuNumber, col.Description).HorizontalAlignment = CellHorizontalAlignment.Left

        'columns
        sheet.Columns(col.Selected).Label = " "
        sheet.Columns(col.Selected).CellType = New CellType.CheckBoxCellType
        sheet.Columns(col.Selected).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(col.Selected).Width = 40
        sheet.Columns(col.Selected).Resizable = False
        sheet.Columns(col.Selected).Locked = False
        sheet.Columns(col.SkuNumber, col.HieStyle).HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.Columns(col.SkuNumber, col.HieStyle).AllowAutoSort = True
        sheet.Columns(col.SkuNumber).Label = "SKU"
        sheet.Columns(col.Description).Label = "Description"
        sheet.Columns(col.SupplierNumber).Label = "Supplier"
        sheet.Columns(col.SupplierName).Label = "Name"
        sheet.Columns(col.HieCategory).Label = "Category"
        sheet.Columns(col.HieGroup).Label = "Group"
        sheet.Columns(col.HieSubgroup).Label = "Subgroup"
        sheet.Columns(col.HieStyle).Label = "Style"
        sheet.Columns(col.NonStock).Label = "Non Stock"
        sheet.Columns(col.NonStock).CellType = New CellType.CheckBoxCellType
        sheet.Columns(col.NonStock).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(col.NonStock).Width = 60
        sheet.Columns(col.NonStock).Resizable = False
        sheet.Columns(col.OnHand, col.Markdown).CellType = typeInt
        sheet.Columns(col.OnHand, col.Markdown).Width = 65
        sheet.Columns(col.OnHand, col.Markdown).Resizable = False
        sheet.Columns(col.OnHand).Label = "On Hand"
        sheet.Columns(col.Markdown).Label = "Markdown"
        sheet.Columns(col.Labelled).CellType = New CellType.CheckBoxCellType
        sheet.Columns(col.Labelled).HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.Columns(col.Labelled).Label = "Labelled"
        sheet.Columns(col.Labelled).Width = 50
        sheet.Columns(col.Labelled).Resizable = False
        sheet.Columns(col.Origin).Label = "Origin"
        sheet.Columns(col.Origin).Width = 50
        sheet.Columns(col.Origin).Resizable = False

        spdItems.Sheets.Add(sheet)
        AddHandler spdItems.Resize, AddressOf spd_Resize

    End Sub

    Private Sub spdItems_AddRows()

        Dim sheet As SheetView = spdItems.ActiveSheet

        For Each detail As BOStockTake.cHhtDetail In _hhtHeader.Details
            'check that detail not already added
            Dim found As Boolean = False
            If sheet.RowCount > 0 Then
                For rowIndex As Integer = 0 To sheet.RowCount - 1
                    If CStr(sheet.Cells(rowIndex, col.SkuNumber).Value) = detail.SkuNumber.Value Then
                        sheet.Cells(rowIndex, col.NonStock).Value = detail.NonStockItem.Value
                        sheet.Cells(rowIndex, col.OnHand).Value = detail.StockOnHand.Value
                        sheet.Cells(rowIndex, col.Labelled).Value = detail.LabelDetailOK.Value
                        sheet.Cells(rowIndex, col.Origin).Value = detail.SkuOrigin.Value
                        sheet.Cells(rowIndex, col.Markdown).Value = detail.MarkdownQty.Value
                        found = True
                        Exit For
                    End If
                Next
            End If

            If Not found Then
                Dim curStock As BOStock.cStock = detail.Stock
                If Not Equals(detail.Stock.SkuNumber.Value, detail.SkuNumber.Value) Then Continue For

                sheet.Rows.Add(sheet.RowCount, 1)
                sheet.Rows(sheet.RowCount - 1).Tag = detail
                sheet.Cells(sheet.RowCount - 1, col.Selected).Value = True
                sheet.Cells(sheet.RowCount - 1, col.SkuNumber).Value = detail.SkuNumber.Value
                sheet.Cells(sheet.RowCount - 1, col.Description).Value = curStock.Description.Value
                sheet.Cells(sheet.RowCount - 1, col.SupplierNumber).Value = curStock.SupplierNo.Value
                sheet.Cells(sheet.RowCount - 1, col.SupplierName).Value = _supplier.GetName(curStock.SupplierNo.Value)
                sheet.Cells(sheet.RowCount - 1, col.HieCategory).Value = _hierarchy.GetCategoryName(curStock.HierCategory.Value)
                sheet.Cells(sheet.RowCount - 1, col.HieGroup).Value = _hierarchy.GetGroupName(curStock.HierGroup.Value)
                sheet.Cells(sheet.RowCount - 1, col.HieSubgroup).Value = _hierarchy.GetSubgroupName(curStock.HierSubGroup.Value)
                sheet.Cells(sheet.RowCount - 1, col.HieStyle).Value = _hierarchy.GetStyleName(curStock.HierStyle.Value)
                sheet.Cells(sheet.RowCount - 1, col.NonStock).Value = detail.NonStockItem.Value
                sheet.Cells(sheet.RowCount - 1, col.OnHand).Value = detail.StockOnHand.Value
                sheet.Cells(sheet.RowCount - 1, col.Markdown).Value = detail.MarkdownQty.Value
                sheet.Cells(sheet.RowCount - 1, col.Labelled).Value = detail.LabelDetailOK.Value
                sheet.Cells(sheet.RowCount - 1, col.Origin).Value = detail.SkuOrigin.Value
            End If
        Next

        If sheet.RowCount > 0 Then
            sheet.ColumnHeaderVisible = True
            CheckSelected()
            spd_Resize(spdItems, New EventArgs)
        End If

    End Sub

    Private Sub spdItems_AutoSortingColumn(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.AutoSortingColumnEventArgs) Handles spdItems.AutoSortingColumn

        Select Case e.Column
            Case col.HieCategory
                e.Cancel = True
                Dim sort(3) As SortInfo
                If e.Ascending Then
                    sort(0) = New SortInfo(col.HieCategory, False)
                    sort(1) = New SortInfo(col.HieGroup, False)
                    sort(2) = New SortInfo(col.HieSubgroup, False)
                    sort(3) = New SortInfo(col.HieStyle, False)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieCategory, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieGroup, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Descending)
                Else
                    sort(0) = New SortInfo(col.HieCategory, True)
                    sort(1) = New SortInfo(col.HieGroup, True)
                    sort(2) = New SortInfo(col.HieSubgroup, True)
                    sort(3) = New SortInfo(col.HieStyle, True)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieCategory, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieGroup, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Ascending)
                End If

            Case col.HieGroup
                e.Cancel = True
                Dim sort(2) As SortInfo
                If e.Ascending Then
                    sort(0) = New SortInfo(col.HieGroup, True)
                    sort(1) = New SortInfo(col.HieSubgroup, True)
                    sort(2) = New SortInfo(col.HieStyle, True)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieGroup, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Descending)
                Else
                    sort(0) = New SortInfo(col.HieGroup, False)
                    sort(1) = New SortInfo(col.HieSubgroup, False)
                    sort(2) = New SortInfo(col.HieStyle, False)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieGroup, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Ascending)
                End If

            Case col.HieSubgroup
                e.Cancel = True
                Dim sort(1) As SortInfo
                If e.Ascending Then
                    sort(0) = New SortInfo(col.HieSubgroup, True)
                    sort(1) = New SortInfo(col.HieStyle, True)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Descending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Descending)
                Else
                    sort(0) = New SortInfo(col.HieSubgroup, False)
                    sort(1) = New SortInfo(col.HieStyle, False)
                    e.Sheet.SortRows(0, e.Sheet.RowCount, sort)
                    e.Sheet.SetColumnSortIndicator(col.HieSubgroup, Model.SortIndicator.Ascending)
                    e.Sheet.SetColumnSortIndicator(col.HieStyle, Model.SortIndicator.Ascending)
                End If
        End Select

        e.Sheet.ClearSelection()

    End Sub

    Private Sub spdItems_ButtonClicked(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.EditorNotifyEventArgs) Handles spdItems.ButtonClicked

        If e.Column = col.Selected Then
            CheckSelected()
        End If

    End Sub

    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs)

        Dim spread As FpSpread = CType(sender, FpSpread)
        If spread.Sheets.Count = 0 Then Exit Sub

        'Reset scrollbar policy.
        spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
        spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

        'check for tab strip and border style
        Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
        Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
        Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
        Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
        Dim tabStripHeight As Integer = 10
        Dim dataAreaWidth As Single = spread.Width
        Dim dataAreaHeight As Single = spread.Height

        If spread.TabStripPolicy = TabStripPolicy.Always Then dataAreaHeight -= tabStripHeight
        If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
            dataAreaHeight -= (borderHeight * 2)
            dataAreaWidth -= (borderWidth * 2)
        End If

        'Get row header width
        For Each sheet As SheetView In spread.Sheets
            If sheet.RowHeaderVisible Then
                For Each header As Column In sheet.RowHeader.Columns
                    If header.Visible Then dataAreaWidth -= header.Width
                Next
            End If

            'Get column header height
            If sheet.ColumnHeaderVisible Then
                For Each header As Row In sheet.ColumnHeader.Rows
                    If header.Visible Then dataAreaHeight -= header.Height
                Next
            End If


            'If number rows * row heights greater than available area then display scrollbar
            If sheet.RowCount * sheet.Rows.Default.Height > dataAreaHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
            If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then dataAreaWidth -= scrollWidth


            'get columns to resize and new widths
            Dim colsTotal As Single = 0
            Dim colsResizable As Integer = 0
            For Each c As Column In sheet.Columns
                If c.Visible And c.Resizable Then
                    c.Width = c.GetPreferredWidth
                    colsResizable += 1
                End If
                If c.Visible Then colsTotal += c.Width
            Next


            'If total width of colums greater than available area then show scrollbar else
            'increase resizable columns to fill up grey area of spread.
            If colsResizable = 0 Then Exit Sub
            If colsTotal > dataAreaWidth Then
                spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
            Else
                Dim increase As Integer = CInt((dataAreaWidth - colsTotal) / colsResizable)
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then c.Width += increase
                Next
            End If
        Next

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub CheckSelected()

        Dim sheet As SheetView = spdItems.ActiveSheet
        Select Case sheet.RowCount
            Case 0
                btnSelectAll.Enabled = False
                btnAccept.Enabled = False
                btnRemove.Enabled = False

                sheet.ColumnHeaderVisible = False
                spdItems.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
                spdItems.VerticalScrollBarPolicy = ScrollBarPolicy.Never

            Case Else
                For rowIndex As Integer = 0 To sheet.RowCount - 1
                    If CBool(sheet.Cells(rowIndex, col.Selected).Value) Then
                        btnSelectAll.Text = My.Resources.F3SelectNone
                        btnAccept.Enabled = True
                        If _canDeleteItems Then btnRemove.Enabled = True
                        Exit Select
                    End If
                Next

                btnSelectAll.Text = My.Resources.F3SelectAll
                btnAccept.Enabled = False
                btnRemove.Enabled = False
        End Select

    End Sub

    Private Sub ExportDetialsIntoCsv(ByVal dateForDetails As Date)

        Dim lstDetails As List(Of cHhtDetail) = GetDetails(dateForDetails)
        Dim strStoreNumber As String = GetStoreNumber()
        Dim strCommsFolder As String = GetCommsFolder()
        Dim fileName As String = String.Format("Count_{0}.csv", strStoreNumber)

        Dim strTempFilePath As String = GetTempFilePath(strCommsFolder, fileName)
        Dim strTargetPath As String = GetFullTortiPath(strCommsFolder, fileName)

        TextFile.Save(strTempFilePath, lstDetails.Select(Function(x) String.Format("{0},{1}", strStoreNumber, x.SkuNumber.Value)))
        TextFile.Move(strTempFilePath, strTargetPath)
    End Sub

    Private Function GetDetails(ByVal dateForDetails As Date) As List(Of cHhtDetail)
        Dim hhtDetail As New cHhtDetail(_Oasys3DB)
        Return hhtDetail.GetDetails(dateForDetails)
    End Function

    Private Function GetStoreNumber() As String
        Dim storeNumber As String
        Using retopt As New BOSystem.cRetailOptions(_Oasys3DB)
            retopt.AddLoadField(retopt.Store)
            retopt.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, retopt.RetailOptionsID, "01")
            retopt.LoadMatches()
            storeNumber = String.Format("8{0}", retopt.Store.Value.ToString.PadLeft(3, "0"c))
        End Using
        Return storeNumber
    End Function

    Private Function GetFullTortiPath(ByVal folderName As String, ByVal fileName As String) As String
        Return Path.Combine(Path.Combine(folderName, "TORTI"), fileName)
    End Function

    Private Function GetTempFilePath(ByVal folderName As String, ByVal fileName As String) As String
        Return Path.Combine(folderName, fileName)
    End Function

    Private Function GetCommsFolder() As String
        If GlobalVars.IsTestEnvironment Then
            Return GlobalVars.SystemEnvironment.GetCommsDirectoryPath()
        Else
            Dim oParam As New BOSystem.cParameter(_Oasys3DB)
            Return oParam.GetParameterString(914)
        End If

    End Function

#End Region

End Class
