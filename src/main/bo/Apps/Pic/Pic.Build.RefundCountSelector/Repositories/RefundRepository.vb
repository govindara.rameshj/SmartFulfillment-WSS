﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Pic.Build.RefundCountSelector_IntegrationTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                        "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                        "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                        "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                        "54e0a4a4")> 

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Pic.Build.RefundCountSelector.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                        "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                        "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                        "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                        "54e0a4a4")> 

Public Class RefundRepository

    Friend _parameterID_LineLimit As Integer = 4420
    Friend _parameterID_SkuQuantityLimit As Integer = 4410
    Friend _parameterID_TransactionLimit As Integer = 4422
    Friend _tableName As String = "SortedRefundRepository"
    Friend _fieldName_TranDate As String = "Transaction Date"
    Friend _fieldName_TranTill As String = "Transaction Till"
    Friend _fieldName_TranNumber As String = "Transaction Number"
    Friend _fieldName_TranLineNumber As String = "Transaction Line Number"
    Friend _fieldName_SkuNumber As String = "Item Sku Number"
    Friend _fieldName_SkuPrice As String = "Item Price"
    Friend _fieldName_SkuQuantity As String = "Item Quantity"
    Friend _fieldName_OriginalTranNumber As String = "Original Transaction Number"
    Friend _fieldName_NumberCashAndGiftTokenRefundPayments As String = "Total Number Of Cash And Gift Token Refund Payments"

    Friend _refundTransactions As List(Of RefundTransaction)
    Private _transactionLimit As Decimal
    Private _lineLimit As Decimal
    Private _skuQuantityLimit As Integer

    Friend _sortedRefundRepository As DataTable
    Friend _sortedAndFilteredRefundRepository As DataTable
    Private _getRefundTransactions As List(Of RefundTransaction)

    Friend _refundTransactionBeingProcessed As RefundTransaction

    Public Sub New()

        _refundTransactions = New List(Of RefundTransaction)
        _lineLimit = GetLineLimit()
        _skuQuantityLimit = GetSkuQuantityLimit()
        _transactionLimit = GetTransactionLimit()
    End Sub

    Public ReadOnly Property LineLimit() As Decimal
        Get
            Return _lineLimit
        End Get
    End Property

    Public ReadOnly Property RefundTransactions() As List(Of RefundTransaction)
        Get
            Return _refundTransactions
        End Get
    End Property

    Public ReadOnly Property SkuQuantityLimit() As Integer
        Get
            Return _skuQuantityLimit
        End Get
    End Property

    Friend ReadOnly Property TableName() As String
        Get
            Return _tableName
        End Get
    End Property

    Public ReadOnly Property TransactionLimit() As Decimal
        Get
            Return _transactionLimit
        End Get
    End Property

    Public Overridable Function GetRefundTransactions(ByVal StartDate As Date, ByVal EndDate As Date) As List(Of RefundTransaction)

        InitialiseSortedRefundRepository(StartDate, EndDate)
        InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
        InitialiseRefundTransactionsList()
        If SortedAndFilteredRefundRepositoryHasData() Then
            ProcessSortedAndFilteredRefundRepository()
        End If

        Return _refundTransactions
    End Function

    Friend Overridable Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = GetSortedRefundTransactionLines(StartDate, EndDate)
    End Sub

    Friend Overridable Sub InitialiseFilteredRefundRepositoryFromSortedRefundRepository()

        _sortedAndFilteredRefundRepository = GetFilteredRefundRepositoryFromSortedRefundRepository()
    End Sub

    Friend Overridable Function SortedRefundRepositoryHasData() As Boolean

        If _sortedRefundRepository IsNot Nothing Then
            If _sortedRefundRepository.Rows.Count > 0 Then
                Return True
            End If
        End If
    End Function

    Friend Overridable Function SortedAndFilteredRefundRepositoryHasData() As Boolean

        If _sortedAndFilteredRefundRepository IsNot Nothing Then
            If _sortedAndFilteredRefundRepository.Rows.Count > 0 Then
                Return True
            End If
        End If
    End Function

    Friend Overridable Sub InitialiseRefundTransactionsList()

        _refundTransactions = New List(Of RefundTransaction)
    End Sub

    Friend Overridable Sub ProcessSortedAndFilteredRefundRepository()

        InitialiseRefundTran()
        ProcessSortedAndFilteredRefundRepositoryRows()
    End Sub

    Friend Overridable Sub InitialiseRefundTran()

        _refundTransactionBeingProcessed = Nothing
    End Sub

    Friend Overridable Sub ProcessSortedAndFilteredRefundRepositoryRows()

        For Each RefundRow As DataRow In _sortedAndFilteredRefundRepository.Rows
            If RefundTransactionBeingProcessedIsNothing() Then
                SetRefundTransactionBeingProcessedToNewRefundTransaction(RefundRow)
            Else
                ProcessRefundRepositoryRow(RefundRow)
            End If
        Next
        If Not RefundTransactionBeingProcessedIsNothing() Then
            AddRefundTransactionBeingProcessedToRefundTransactionsList()
        End If
    End Sub

    Friend Overridable Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return _refundTransactionBeingProcessed Is Nothing
    End Function

    Friend Overridable Sub ProcessRefundRepositoryRow(ByVal RefundRow As DataRow)

        If RowSameAsTransaction(_refundTransactionBeingProcessed, RefundRow) Then
            AddRefundLineToRefundTransactionBeingProcessedsLines(RefundRow)
        Else
            AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(RefundRow)
        End If
    End Sub

    Friend Overridable Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As DataRow)

        AddRefundTransactionBeingProcessedToRefundTransactionsList()
        SetRefundTransactionBeingProcessedToNewRefundTransaction(RefundRow)
    End Sub

    Friend Overridable Sub SetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As DataRow)

        _refundTransactionBeingProcessed = CreateRefundTransactionBeingProcessedFromRefundRow(RefundRow)
    End Sub

    Friend Overridable Sub AddRefundTransactionBeingProcessedToRefundTransactionsList()

        If _refundTransactionBeingProcessed IsNot Nothing Then
            _refundTransactions.Add(_refundTransactionBeingProcessed)
        End If
    End Sub

    Friend Overridable Sub AddRefundLineToRefundTransactionBeingProcessedsLines(ByVal RefundRow As DataRow)
        Dim CreatedRefundLine As RefundLine

        CreatedRefundLine = NewRefundLine(RefundRow)
        If CreatedRefundLine IsNot Nothing Then
            _refundTransactionBeingProcessed.TransactionLines.Lines.Add(CreatedRefundLine)
        End If
    End Sub

    Friend Function GetSortColumn(ByVal SortColumnIndex As Integer) As String

        Select Case SortColumnIndex
            Case 1
                Return _fieldName_TranDate
            Case 2
                Return _fieldName_TranTill
            Case 3
                Return _fieldName_TranNumber
            Case Else
                Return String.Empty
        End Select
    End Function

    Friend Function GetInitialisedDataTable() As DataTable

        GetInitialisedDataTable = New DataTable(_tableName)
        With GetInitialisedDataTable
            .Columns.Add(_fieldName_TranDate, GetType(Date))
            .Columns.Add(_fieldName_TranTill, GetType(String))
            .Columns.Add(_fieldName_TranNumber, GetType(String))
            .Columns.Add(_fieldName_TranLineNumber, GetType(Int16))
            .Columns.Add(_fieldName_SkuNumber, GetType(String))
            .Columns.Add(_fieldName_SkuPrice, GetType(Decimal))
            .Columns.Add(_fieldName_SkuQuantity, GetType(Decimal))
            .Columns.Add(_fieldName_OriginalTranNumber, GetType(String))
            .Columns.Add(_fieldName_NumberCashAndGiftTokenRefundPayments, GetType(Integer))
        End With
    End Function

    Friend Overridable Function GetRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As DataTable

        Using con = GetConnection()
            Using com = GetConnectionCommand(con)
                AddGetRefundTransactionLinesParametersToCommand(com, StartDate, EndDate)
                SetCommandStoredProcedureNameToGetRefundTransactionLines(com)

                GetRefundTransactionLines = com.ExecuteDataTable
                GetRefundTransactionLines.TableName = _tableName
            End Using
        End Using
    End Function

    Friend Overridable Function GetConnection() As Connection

        Return New Connection
    End Function

    Friend Overridable Function GetConnectionCommand(ByRef con As Connection) As Command
        Dim iConnection As IConnection = con
        Return New Command(iConnection)
    End Function

    Friend Overridable Sub AddGetRefundTransactionLinesParametersToCommand(ByRef com As Command, ByVal StartDate As Date, ByVal EndDate As Date)

        If com IsNot Nothing Then
            With com
                .ClearParamters()
                .AddParameter(My.Resources.Parameters.StartDate, StartDate)
                .AddParameter(My.Resources.Parameters.EndDate, EndDate)
            End With
        End If
    End Sub

    Friend Overridable Sub SetCommandStoredProcedureNameToGetRefundTransactionLines(ByRef com As Command)

        If com IsNot Nothing Then
            com.StoredProcedureName = My.Resources.Procedures.PicRefundCount_GetRefundTransactionLines
        End If
    End Sub

    Friend Overridable Function GetSortedRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As DataTable
        Dim NewRefTranLine As DataRow

        GetSortedRefundTransactionLines = GetInitialisedDataTable()
        With GetSortedRefundTransactionLines
            For Each refTranLine In (From tranLine In GetRefundTransactionLines(StartDate, EndDate) Select tranLine Order By tranLine(Me.GetSortColumn(1)), tranLine(Me.GetSortColumn(2)), tranLine(Me.GetSortColumn(3))).ToList
                NewRefTranLine = .NewRow()
                For Each col As DataColumn In .Columns
                    NewRefTranLine(col.ColumnName) = refTranLine(col.ColumnName)
                Next
                .Rows.Add(NewRefTranLine)
            Next
        End With
    End Function

    Friend Overridable Function GetFilteredRefundRepositoryFromSortedRefundRepository() As DataTable
        Dim NewRefTranLine As DataRow
        Dim FilteredAndSortedRefundTransactionLines As DataTable = GetInitialisedDataTable()
        Dim IncludeTheRow As Boolean = False

        If SortedRefundRepositoryHasData() Then
            For Each RefundRow As DataRow In _sortedRefundRepository.Rows
                IncludeTheRow = False
                If RowIsNoProofOfPurchaseRefund(RefundRow) Then
                    IncludeTheRow = True
                Else
                    If RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(RefundRow) Then
                        IncludeTheRow = True
                    End If
                End If
                If IncludeTheRow Then
                    With FilteredAndSortedRefundTransactionLines
                        NewRefTranLine = .NewRow()
                        For Each col As DataColumn In .Columns
                            NewRefTranLine(col.ColumnName) = RefundRow(col.ColumnName)
                        Next
                        .Rows.Add(NewRefTranLine)
                    End With
                End If
            Next
        End If
        Return FilteredAndSortedRefundTransactionLines
    End Function

    Friend Overridable Function RowIsNoProofOfPurchaseRefund(ByVal RefundRow As DataRow) As Boolean

        Try
            RowIsNoProofOfPurchaseRefund = False
            If RefundRow IsNot Nothing _
            AndAlso RefundRow(_fieldName_OriginalTranNumber) IsNot Nothing Then
                If String.Compare(CStr(RefundRow(_fieldName_OriginalTranNumber)) & "", "0000", True) = 0 Then
                    RowIsNoProofOfPurchaseRefund = True
                End If
            End If
        Catch ex As Exception
            RowIsNoProofOfPurchaseRefund = False
        End Try
    End Function

    Friend Overridable Function RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(ByVal RefundRow As DataRow) As Boolean

        Try
            RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens = False
            If RefundRow IsNot Nothing AndAlso RefundRow(_fieldName_NumberCashAndGiftTokenRefundPayments) IsNot Nothing Then
                Dim NumberCashAndGiftTokenRefundPayments As Integer = -1

                If Integer.TryParse(CStr(RefundRow(_fieldName_NumberCashAndGiftTokenRefundPayments)), NumberCashAndGiftTokenRefundPayments) Then
                    If NumberCashAndGiftTokenRefundPayments > 0 Then
                        RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens = True
                    End If
                End If
            End If
        Catch ex As Exception
            RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens = False
        End Try
    End Function

    Friend Overridable Function CreateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As DataRow) As RefundTransaction

        SetRefundTransactionBeingProcessedToBlankRefundTransaction()
        If _refundTransactionBeingProcessed IsNot Nothing And RefundRowContainsRefundTransactionFields(RefundRow) Then
            PopulateRefundTransactionBeingProcessedFromRefundRow(RefundRow)
        End If

        Return _refundTransactionBeingProcessed
    End Function

    Friend Overridable Sub SetRefundTransactionBeingProcessedToBlankRefundTransaction()

        _refundTransactionBeingProcessed = New RefundTransaction()
    End Sub

    Friend Overridable Sub PopulateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As DataRow)

        CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(RefundRow)
        AddRefundLineToRefundTransactionBeingProcessedsLines(RefundRow)
    End Sub

    Friend Overridable Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(ByVal RefundRow As DataRow)

        With _refundTransactionBeingProcessed
            .TransactionDate = CDate(IIf(RefundRow.Item(_fieldName_TranDate) Is DBNull.Value, .TransactionDate, RefundRow.Item(_fieldName_TranDate)))
            .Till = CStr(IIf(RefundRow.Item(_fieldName_TranTill) Is DBNull.Value, .Till, RefundRow.Item(_fieldName_TranTill)))
            .TransactionNumber = CStr(IIf(RefundRow.Item(_fieldName_TranNumber) Is DBNull.Value, .TransactionNumber, RefundRow.Item(_fieldName_TranNumber)))
        End With
    End Sub

    Friend Overridable Function RefundRowContainsRefundTransactionFields(ByVal RefundRow As DataRow) As Boolean

        If RefundRow IsNot Nothing Then
            Try
                If RefundRow.Item(Me._fieldName_TranDate) IsNot Nothing Then
                    If RefundRow.Item(Me._fieldName_TranTill) IsNot Nothing Then
                        If RefundRow.Item(Me._fieldName_TranNumber) IsNot Nothing Then
                            Return True
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Function

    Friend Overridable Function RowSameAsTransaction(ByVal ToCompare As RefundTransaction, ByVal CompareWith As DataRow) As Boolean

        If TransactionAndRowDataAreComparable(ToCompare, CompareWith) Then
            Return ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(ToCompare, CompareWith)
        End If
    End Function

    Friend Overridable Function TransactionAndRowDataAreComparable(ByVal ToCompare As RefundTransaction, ByVal CompareWith As DataRow) As Boolean

        If ToCompare IsNot Nothing AndAlso CompareWith IsNot Nothing Then
            Try
                If CompareWith.Item((New RefundRepository)._fieldName_TranDate) IsNot Nothing Then
                    If CompareWith.Item((New RefundRepository)._fieldName_TranTill) IsNot Nothing Then
                        If CompareWith.Item((New RefundRepository)._fieldName_TranNumber) IsNot Nothing Then
                            Return True
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
    End Function

    Friend Overridable Function ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(ByVal ToCompare As RefundTransaction, ByVal CompareWith As DataRow) As Boolean

        With ToCompare
            Return DateDiff(DateInterval.Day, .TransactionDate, CDate(CompareWith.Item(_fieldName_TranDate))) = 0 _
               And .Till = CStr(CompareWith.Item(_fieldName_TranTill)) _
               And .TransactionNumber = CStr(CompareWith.Item(_fieldName_TranNumber))
        End With
    End Function

    Friend Overridable Function NewRefundLine(ByVal RefundRow As DataRow) As RefundLine

        NewRefundLine = New RefundLine()
        With NewRefundLine
            .ItemPrice = CDec(IIf(RefundRow.Item(_fieldName_SkuPrice) Is DBNull.Value, .ItemPrice, RefundRow.Item(_fieldName_SkuPrice)))
            .LineNumber = CShort(IIf(RefundRow.Item(_fieldName_TranLineNumber) Is DBNull.Value, .LineNumber, RefundRow.Item(_fieldName_TranLineNumber)))
            .OriginalTransactionNumber = CStr(IIf(RefundRow.Item(_fieldName_OriginalTranNumber) Is DBNull.Value, .OriginalTransactionNumber, RefundRow.Item(_fieldName_OriginalTranNumber)))
            If RefundRow.Item(_fieldName_SkuQuantity) IsNot DBNull.Value Then
                Dim ParsedQuantity As Integer = .Quantity

                If Integer.TryParse(RefundRow.Item(_fieldName_SkuQuantity).ToString, ParsedQuantity) Then
                    .Quantity = ParsedQuantity * -1
                End If
            End If
            .SkuNumber = CStr(IIf(RefundRow.Item(_fieldName_SkuNumber) Is DBNull.Value, .SkuNumber, RefundRow.Item(_fieldName_SkuNumber)))
        End With
    End Function

    Friend Overridable Function GetLineLimit() As Decimal

        GetLineLimit = Parameter.GetDecimal(_parameterID_LineLimit)
    End Function

    Friend Overridable Function GetSkuQuantityLimit() As Integer

        GetSkuQuantityLimit = Parameter.GetInteger(_parameterID_SkuQuantityLimit)
    End Function

    Friend Overridable Function GetTransactionLimit() As Decimal

        GetTransactionLimit = Parameter.GetDecimal(_parameterID_TransactionLimit)
    End Function
End Class
