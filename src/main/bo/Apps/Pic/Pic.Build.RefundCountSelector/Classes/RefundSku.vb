﻿Public Class RefundSku

    Private _number As String
    Private _value As Decimal

    Public Sub New()

        _number = String.Empty
        _value = 0D
    End Sub

    Public Sub New(ByVal IntialiseFrom As RefundLine)

        Me.New()
        If IntialiseFrom IsNot Nothing Then
            With IntialiseFrom
                _number = .SkuNumber
                _value = .TotalValue
            End With
        End If
    End Sub

    Public Sub New(ByVal SkuNumber As String, ByVal Value As Decimal)

        Me.New()
        _number = SkuNumber
        _value = Value
    End Sub

    Public Property Number() As String
        Get
            Return _number
        End Get
        Set(ByVal value As String)
            _number = value
        End Set
    End Property

    Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property

    Public Sub AddToValue(ByVal ValueToAdd As Decimal)

        _value += ValueToAdd
    End Sub
End Class
