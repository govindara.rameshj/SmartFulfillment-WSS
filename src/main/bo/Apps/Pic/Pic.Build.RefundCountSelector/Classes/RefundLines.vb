﻿Public Class RefundLines

    Private _lines As List(Of RefundLine)

    Public Sub New()

        _lines = New List(Of RefundLine)
    End Sub

    Public Sub New(ByVal Lines As List(Of RefundLine))

        Me.New()
        If Lines IsNot Nothing Then
            For Each NextRefundLine As RefundLine In Lines
                _lines.Add(New RefundLine(NextRefundLine))
            Next
        End If
    End Sub

    Public ReadOnly Property Lines() As List(Of RefundLine)
        Get
            Return _lines
        End Get
    End Property

    Public Overridable Function TotalIsOverLimit(ByVal Limit As Decimal) As Boolean

        Return GetTotal() > Limit
    End Function

    Public Overridable Function TotalIsOnOrOverLimit(ByVal Limit As Decimal) As Boolean

        Return GetTotal() >= Limit
    End Function

    Public Overridable Function GetAllLinesForSku(ByVal SkuNumber As String) As List(Of RefundLine)

        GetAllLinesForSku = New List(Of RefundLine)
        For Each NextRefLine In Lines.Where(Function(x) x.SkuNumber = SkuNumber).ToList
            GetAllLinesForSku.Add(New RefundLine(NextRefLine))
        Next
    End Function

    Friend Overridable Function GetTotal() As Decimal
        Dim LinesTotal As Decimal = 0

        For Each Line As RefundLine In _lines
            LinesTotal += Line.TotalValue
        Next
        Return LinesTotal
    End Function
End Class
