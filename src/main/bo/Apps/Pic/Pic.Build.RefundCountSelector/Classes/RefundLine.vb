Imports System
Imports System.Text
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

<Assembly: InternalsVisibleTo("Pic.Build.RefundCountSelector.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                        "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                        "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                        "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                        "54e0a4a4")> 

Public Class RefundLine

    Private _lineNumber As Int16
    Private _skuNumber As String
    Private _quantity As Integer
    Private _itemPrice As Decimal
    Private _originalTransactionNumber As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal CopyFrom As RefundLine)

        If CopyFrom IsNot Nothing Then
            With CopyFrom
                _itemPrice = .ItemPrice
                _lineNumber = .LineNumber
                _originalTransactionNumber = .OriginalTransactionNumber
                _quantity = .Quantity
                _skuNumber = .SkuNumber
            End With
        End If
    End Sub

    Public Property ItemPrice() As Decimal
        Get
            Return _itemPrice
        End Get
        Set(ByVal value As Decimal)
            _itemPrice = value
        End Set
    End Property

    Public Property LineNumber() As Int16
        Get
            Return _lineNumber
        End Get
        Set(ByVal value As Int16)
            _lineNumber = value
        End Set
    End Property

    Public Property OriginalTransactionNumber() As String
        Get
            Return _originalTransactionNumber
        End Get
        Set(ByVal value As String)
            _originalTransactionNumber = value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Return _quantity
        End Get
        Set(ByVal value As Integer)
            _quantity = value
        End Set
    End Property

    Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property

    Public Function NoOriginalReceiptProvided() As Boolean

        Return _originalTransactionNumber = "0000"
    End Function

    Public Function TotalIsOverLimit(ByVal Limit As Decimal) As Boolean

        Return TotalValue() > Limit
    End Function

    Friend Overridable Function TotalValue() As Decimal

        Return _quantity * _itemPrice
    End Function
End Class
