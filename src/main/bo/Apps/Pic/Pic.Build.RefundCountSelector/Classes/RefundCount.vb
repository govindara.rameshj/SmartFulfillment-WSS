﻿Imports System.Data

<Assembly: InternalsVisibleTo("Pic.Build.RefundCountSelector.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                        "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                        "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                        "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                        "54e0a4a4")> 

<Assembly: InternalsVisibleTo("Pic.Build.RefundCountSelector_IntegrationTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                               "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                               "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                               "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                               "54e0a4a4")> 

Public Class RefundCount

    Private _startDate As Date
    Private _endDate As Date
    Private _transactionLimit As Decimal
    Private _lineLimit As Decimal
    Private _skuQuantityLimit As Integer
    Private _transactions As List(Of RefundTransaction)
    Private _skus As ArrayList

#Region "Constructors"

    Public Sub New()

        _startDate = Date.MinValue
        _endDate = Date.MinValue
        _transactions = New List(Of RefundTransaction)
        _skus = New ArrayList

        With GetLimits
            _transactionLimit = .TransactionLimit
            _lineLimit = .LineLimit
            _skuQuantityLimit = .SkuQuantityLimit
        End With
    End Sub

    Public Sub New(ByVal StartDate As Date, ByVal EndDate As Date)

        Me.New()
        _startDate = StartDate
        _endDate = EndDate
    End Sub
#End Region

#Region "Properties"

    Public Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    Public Property Skus() As ArrayList
        Get
            Return _skus
        End Get
        Set(ByVal value As ArrayList)
            _skus = value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    Public Property TransactionLimit() As Decimal
        Get
            Return _transactionLimit
        End Get
        Set(ByVal value As Decimal)
            _transactionLimit = value
        End Set
    End Property

    Public Property LineLimit() As Decimal
        Get
            Return _lineLimit
        End Get
        Set(ByVal value As Decimal)
            _lineLimit = value
        End Set
    End Property

    Public Property SkuQuantityLimit() As Integer
        Get
            Return _skuQuantityLimit
        End Get
        Set(ByVal value As Integer)
            _skuQuantityLimit = value
        End Set
    End Property

    Public Property Transactions() As List(Of RefundTransaction)
        Get
            Return _transactions
        End Get
        Set(ByVal value As List(Of RefundTransaction))
            _transactions = value
        End Set
    End Property
#End Region

#Region "Methods"

    Public Overridable Sub Load()

        If _startDate.CompareTo(Date.MinValue) = 0 Then
        Else
            If _endDate.CompareTo(Date.MinValue) = 0 Then
            Else
                Dim RefundRep As RefundRepository = GetRefundRepository()

                Transactions = RefundRep.GetRefundTransactions(_startDate, _endDate)
                Skus = GetSkuList()
            End If
        End If
    End Sub

    Public Sub Load(ByVal StartDate As Date, ByVal EndDate As Date)

        _startDate = StartDate
        _endDate = EndDate
        Load()
    End Sub
#End Region

#Region "Internals"

    Friend Overridable Function GetLimits() As RefundRepository

        Return New RefundRepository
    End Function

    Friend Overridable Function GetRefundSkuList() As List(Of RefundSku)
        Dim SkuList As New List(Of RefundSku)

        If Transactions IsNot Nothing Then
            For Each refTran As RefundTransaction In Transactions
                If refTran.TransactionLines.TotalIsOverLimit(TransactionLimit) Then
                    AddAllTransactionLinesToSkuList(SkuList, refTran)
                Else
                    AddTransactionLinesOverLimitToSkuList(SkuList, refTran)
                End If
            Next
        End If

        Return SkuList
    End Function

    Friend Overridable Function GetDistinctRefundSkuValueList() As List(Of RefundSku)

        GetDistinctRefundSkuValueList = New List(Of RefundSku)
        For Each NextRefSku In (From refSku In GetRefundSkuList() Group By refSku.Number Into Group Select Number, Value = Group.Sum(Function(x) x.Value)).ToList
            GetDistinctRefundSkuValueList.Add(New RefundSku(NextRefSku.Number, NextRefSku.Value))
        Next
    End Function

    Friend Overridable Function GetSortedDistinctRefundSkuValueList() As List(Of RefundSku)

        GetSortedDistinctRefundSkuValueList = New List(Of RefundSku)
        For Each NextRefSku In (From refSku In GetDistinctRefundSkuValueList() Select refSku Order By refSku.Value Descending).ToList
            GetSortedDistinctRefundSkuValueList.Add(New RefundSku(NextRefSku.Number, NextRefSku.Value))
        Next
    End Function

    Friend Overridable Function GetSkuList() As ArrayList
        Dim RefundSkuList As List(Of RefundSku) = GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList()

        GetSkuList = New ArrayList
        GetSkuList.AddRange(ExtractSkuNumbersFromRefundSkuList(RefundSkuList))
    End Function

    Friend Overridable Function GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList() As List(Of RefundSku)

        GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList = New List(Of RefundSku)
        Dim a = (From refSku In GetSortedDistinctRefundSkuValueList() Take SkuQuantityLimit).ToList
        For Each NextRefSku In (From refSku In GetSortedDistinctRefundSkuValueList() Take SkuQuantityLimit).ToList
            GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList.Add(New RefundSku(NextRefSku.Number, NextRefSku.Value))
        Next
    End Function

    Friend Overridable Sub AddAllTransactionLinesToSkuList(ByRef SkuList As List(Of RefundSku), ByVal refTran As RefundTransaction)

        If SkuList IsNot Nothing Then
            If refTran IsNot Nothing Then
                For Each refline As RefundLine In refTran.TransactionLines.Lines
                    AddLineToSkuList(SkuList, refline)
                Next
            End If
        End If
    End Sub

    Friend Overridable Sub AddTransactionLinesOverLimitToSkuList(ByRef SkuList As List(Of RefundSku), ByVal refTran As RefundTransaction)

        If SkuList IsNot Nothing Then
            If refTran IsNot Nothing Then
                Dim SkusAlreadyChecked As New List(Of String)

                For Each refline As RefundLine In refTran.TransactionLines.Lines
                    If Not SkusAlreadyChecked.Contains(refline.SkuNumber) Then
                        Dim LinesForSku As RefundLines = refTran.GetTransactionLinesForSku(refline.SkuNumber)

                        If LinesForSku.TotalIsOnOrOverLimit(LineLimit) Then
                            For Each refLineToAdd As RefundLine In LinesForSku.Lines
                                AddLineToSkuList(SkuList, refLineToAdd)
                            Next
                        End If
                        SkusAlreadyChecked.Add(refline.SkuNumber)
                    End If
                Next
            End If
        End If
    End Sub

    Friend Overridable Sub AddLineToSkuList(ByRef SkuList As List(Of RefundSku), ByVal refLine As RefundLine)

        If SkuList IsNot Nothing Then
            If refLine IsNot Nothing Then
                SkuList.Add(New RefundSku(refLine))
            End If
        End If
    End Sub

    Friend Overridable Function ExtractSkuNumbersFromRefundSkuList(ByVal RefundSkuList As List(Of RefundSku)) As ArrayList

        ExtractSkuNumbersFromRefundSkuList = New ArrayList
        If RefundSkuList IsNot Nothing Then
            For Each NextRefSku As RefundSku In RefundSkuList
                ExtractSkuNumbersFromRefundSkuList.Add(NextRefSku.Number)
            Next
        End If
    End Function

    Friend Overridable Function GetRefundRepository() As RefundRepository

        Return New RefundRepository
    End Function
#End Region
End Class
