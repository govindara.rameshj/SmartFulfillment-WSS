Imports System
Imports System.Text
Imports System.Collections.Generic
Imports System.ComponentModel

<Assembly: InternalsVisibleTo("Pic.Build.RefundCountSelector.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                        "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                        "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                        "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                        "54e0a4a4")> 

Public Class RefundTransaction

    Private _transactionDate As Date
    Private _till As String
    Private _transactionNumber As String
    Private _transactionLines As RefundLines

    Public Sub New()

        _transactionLines = New RefundLines
    End Sub

    Public Property Till() As String
        Get
            Return _till
        End Get
        Set(ByVal value As String)
            _till = value
        End Set
    End Property

    Public Property TransactionDate() As Date
        Get
            Return _transactionDate
        End Get
        Set(ByVal value As Date)
            _transactionDate = value
        End Set
    End Property

    Public Property TransactionNumber() As String
        Get
            Return _transactionNumber
        End Get
        Set(ByVal value As String)
            _transactionNumber = value
        End Set
    End Property

    Public ReadOnly Property TransactionLines() As RefundLines
        Get
            Return _transactionLines
        End Get
    End Property

    Public Function GetTransactionLinesForSku(ByVal SkuNumber As String) As RefundLines

        GetTransactionLinesForSku = New RefundLines(TransactionLines.GetAllLinesForSku(SkuNumber))
    End Function

    Friend Sub SetRefundLines(ByVal InitialLines As RefundLines)

        _transactionLines = New RefundLines(InitialLines.Lines)
    End Sub
End Class
