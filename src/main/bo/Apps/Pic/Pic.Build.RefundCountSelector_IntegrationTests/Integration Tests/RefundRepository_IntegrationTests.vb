﻿Imports System
Imports System.Globalization
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class RefundRepository_IntegrationTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"

    Private Shared _sku1WithRelatedItemPPOS As String
    Private Shared _sku2RelatedToSku1BySku1sPPOS As String
    Private Shared _sku3WithRelatedItemSPOS As String
    Private Shared _sku4RelatedToSku3BySku3sSPOS As String
    Private Shared _sku5WithNoRelatedItem As String
    Private Shared _nonRelatedItemSku As String
    Private Shared _earliestPreExistingTransactionDate As DateTime
    Private Shared _deliveryChargeSkus As DeliveryChargeSkuList

    Private Shared _dateTooEarlyTransactionNumber As String = "9999"
    Private Shared _dateTooLateTransactionNumber As String = "9998"
    Private Shared _voidTransactionNumber As String = "9997"
    Private Shared _parkedTransactionNumber As String = "9996"
    Private Shared _trainingModeTransactionNumber As String = "9995"
    Private Shared _lineReversedTransactionNumber As String = "9994"
    Private Shared _allPositiveQuantitiesTransactionNumber As String = "9993"
    Private Shared _allNegativeQuantitiesTransactionNumber As String = "9992"
    Private Shared _2PositiveAnd2NegativeQuantitiesTransactionNumber As String = "9991"
    Private Shared _nullOriginalTranNumberTransactionNumber As String = "9990"
    Private Shared _nonZeroOriginalTranNumberTransactionNumber As String = "9989"
    Private Shared _nonZeroOriginalTranNumberTransactionLineNumber As Integer = 4
    Private Shared _nonZeroOriginalTranNumber As String = "6952"
    Private Shared _relatedItemSkusTestsTransactionNumber As String = "9988"
    Private Shared _deliverySkusLinesTransactionNumber As String = "9987"
    Private Shared _twoCashAndThreeGiftTokenPaymentsTransactionNumber As String = "9986"
    Private Shared _orderNumberTransactionNumber As String = "9985"
    Private Shared _relatedItemSkusTestsSku1TransactionLineNumber As Integer = 1
    Private Shared _relatedItemSkusTestsSku3TransactionLineNumber As Integer = 2
    Private Shared _relatedItemSkusTestsNoRelatedItemTransactionLineNumber As Integer = 3

#Region "ClassInitialize Procedures"
    <ClassInitialize()> _
    Public Shared Sub SetUpTestDataInDatabase(ByVal testContext As TestContext)

        GetSkuNumbers()
        GetEarliestPreExistingTransactionDate()
        GetDeliverySkuList()
        Try
            PopulateTablesWithTestData()
        Catch ex As Exception
            CleanupTestDataFomDatabase()
            Assert.Fail((ex.Message).Trim)
        End Try
    End Sub

    Private Shared Sub GetSkuNumbers()

        GetRelatedItemSkuNumbers()
        GetNonRelatedItemSkuNumber()
    End Sub

    Private Shared Sub GetRelatedItemSkuNumbers()
        Dim Top2RelatedItems As DataTable

        Top2RelatedItems = ExecuteSQL("Select Top 2 SPOS, PPOS From RELITM", "intialising Sku Numbers.")
        If Top2RelatedItems IsNot Nothing Then
            With Top2RelatedItems.Rows
                If .Count > 1 Then
                    _sku1WithRelatedItemPPOS = .Item(0)("SPOS").ToString
                    _sku2RelatedToSku1BySku1sPPOS = .Item(0)("PPOS").ToString
                    _sku3WithRelatedItemSPOS = .Item(1)("PPOS").ToString
                    _sku4RelatedToSku3BySku3sSPOS = .Item(1)("SPOS").ToString
                Else
                    Assert.Inconclusive("Failed to get enough related item skus for testing.")
                End If
            End With
        Else
            Assert.Inconclusive("Failed to get any related item skus for testing.")
        End If
    End Sub

    Private Shared Sub GetDeliverySkuList()

        _deliveryChargeSkus = New DeliveryChargeSkuList
        _deliveryChargeSkus.Load()
    End Sub

    Private Shared Sub GetNonRelatedItemSkuNumber()
        Dim NonRelatedItemSkuNumber As DataTable

        NonRelatedItemSkuNumber = ExecuteSQL("Select Top 1 SKUN From STKMAS Left Outer Join RELITM On (SKUN = SPOS Or SKUN = PPOS) Where SPOS Is Null And PPOS Is Null Order By SKUN", "intialising non related item Sku Number.")
        If NonRelatedItemSkuNumber IsNot Nothing Then
            With NonRelatedItemSkuNumber.Rows
                If .Count > 0 Then
                    _nonRelatedItemSku = .Item(0)("SKUN").ToString
                Else
                    Assert.Inconclusive("Failed to get non-related item sku for testing.")
                End If
            End With
        Else
            Assert.Inconclusive("Failed to get non-related item sku for testing.")
        End If
    End Sub

    Private Shared Sub GetEarliestPreExistingTransactionDate()
        Dim EarliestDLLINEDate1 As DataTable

        EarliestDLLINEDate1 = ExecuteSQL("Select Top 1 Date1 From  DLLINE Order By DATE1 Asc", "intialising earliest pre-existing transaction date.")
        If EarliestDLLINEDate1 IsNot Nothing Then
            With EarliestDLLINEDate1.Rows
                If .Count > 0 Then
                    Dim theDate As String

                    theDate = .Item(0)("DATE1").ToString
                    If Not Date.TryParse(theDate, _earliestPreExistingTransactionDate) Then
                        _earliestPreExistingTransactionDate = Nothing
                        Assert.Inconclusive("Failed to get earliest pre-existing transaction date for testing.")
                    End If
                Else
                    Assert.Inconclusive("Failed to get earliest pre-existing transaction date for testing.")
                End If
            End With
        Else
            Assert.Inconclusive("Failed to get earliest pre-existing transaction date for testing.")
        End If
    End Sub

    Private Shared Function ExecuteSQL(ByVal SQL As String, ByVal ErrMesage As String) As DataTable

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = SQL
                        ExecuteSQL = .ExecuteDataTable()
                    End With
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Failed initialising tests from the database whilst " & ErrMesage)
            Return New DataTable
        End Try
    End Function

    Private Shared Sub PopulateTablesWithTestData()

        Try
            PopulateDLLINETestData()
            PopulateDLTOTSTestData()
            PopulateDLRCUSTestData()
            PopulateDLPAIDTestData()
            PopulateDLGIFTTestData()
        Catch ex As Exception
            CleanupTestDataFomDatabase()
            If ex.InnerException IsNot Nothing Then
                Throw New Exception((ex.Message & ".  " & ex.InnerException.Message).Trim)
            Else
                Throw New Exception(ex.Message & ".")
            End If
        End Try
    End Sub

    Private Shared Function GetValidTestDate() As DateTime

        Return DateAdd(DateInterval.Day, -2, _earliestPreExistingTransactionDate)
    End Function

    Private Shared Function GetTooEarlyDate() As DateTime

        Return DateAdd(DateInterval.Day, -3, _earliestPreExistingTransactionDate)
    End Function

    Private Shared Function GetTooLateDate() As DateTime

        Return DateAdd(DateInterval.Day, -1, _earliestPreExistingTransactionDate)
    End Function

    Private Shared Sub PopulateDLLINETestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetDLLINETestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLLINETestData()
            Throw New Exception("Failed setting up DLLINE test data.", ex)
        End Try
    End Sub

    Private Shared Function GetDLLINETestData() As StringBuilder
        Dim DLLINETestData As New StringBuilder
        Dim FormattedTestDate As String = GetValidTestDate().ToString("yyyy-MM-dd")
        Dim FormattedTooEarlyDate As String = GetTooEarlyDate().ToString("yyyy-MM-dd")
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DLLINETestData
            .Append("Insert Into ")
            .Append("	DLLINE").Append("		(DATE1,TILL, [TRAN], NUMB, SKUN, QUAN, PRIC, LREV, SALT, RITM, ITAG, CATA, BDCOInd)")
            .Append("Values")
            .Append("	('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 1, '170620', -1, 23.98, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 2, '170694', -1, 6.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 3, '170694', -1, 6.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 1, '166240', -1, 0.47, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 2, '221098', -1, 3.9, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 3, '235357', -1, 2.75, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 4, '166113', -1, 4.09, 0, 'B', 1, 1, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 5, '166240', -1, 0.47, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 6, '233619', -1, 1.92, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 7, '166123', -1, 2.02, 0, 'B', 1, 1, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 8, '214541', -1, 3.05, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 9, '215525', -1, 1.77, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 10, '230129', -1, 1.01, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 11, '231523', -1, 1.52, 0, 'B', 1, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsSku1TransactionLineNumber.ToString & ", '" & _sku1WithRelatedItemPPOS & "', -2, 13.68, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsSku3TransactionLineNumber.ToString & ", '" & _sku3WithRelatedItemSPOS & "', -1, 2.29, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsNoRelatedItemTransactionLineNumber.ToString & ", '" & _nonRelatedItemSku & "', -1, 4.79, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _parkedTransactionNumber & "', 1, '195609', -1, 25.03, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', 1, '214407', -1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', 2, '214406', -1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', 3, '214204', -1, 10.44, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', 4, '214407', -1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', 5, '214407', -1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _trainingModeTransactionNumber & "', 1, '209992', -1, 99.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', 1, '199903', -1, 26.65, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', 2, '205431', -1, 3.99, 0, 'B', 0, 1, 0, 0),")
            .Append("	('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', 3, '162665', -1, 5.79, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _lineReversedTransactionNumber & "', 1, '773337', 1, 39.06, 1, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _lineReversedTransactionNumber & "', 2, '773337', -1, 39.06, 1, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _lineReversedTransactionNumber & "', 3, '194477', -1, 5.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _lineReversedTransactionNumber & "', 4, '540053', -1, 18.48, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nullOriginalTranNumberTransactionNumber & "', 1, '159795', -1, 15.99, 0, 'B', 0, 1, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allPositiveQuantitiesTransactionNumber & "', 1, '213756', 1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allPositiveQuantitiesTransactionNumber & "', 2, '213756', 1, 14.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allPositiveQuantitiesTransactionNumber & "', 3, '186934', 1, 1.99, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 1, '540053', -6, 18.48, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 2, '548024', -5, 12.25, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 3, '206976', -1, 2.54, 0, 'B', 1, 1, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', " & _nonZeroOriginalTranNumberTransactionLineNumber & ", '206975', -1, 24.5, 0, 'B', 0, 1, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 5, '542500', -1, 5.59, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 6, '542500', -4, 5.59, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 7, '170711', -1, 33.59, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 1, '154449', -2, 2.80, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 2, '166071', 1, 10.71, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 3, '230255', -2, 4.00, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 4, '166084', 1, 12.99, 0, 'B', 0, 0, 0, 0),")

            .Append("	('" & FormattedTestDate & "', '01', '" & _deliverySkusLinesTransactionNumber & "', 1, '194477', -1, 5.99, 0, 'B', 0, 0, 0, 0),")
            Dim SkuCount As Integer = 1
            For Each NextDeliveryChargeItem As DeliveryChargeSku In _deliveryChargeSkus.List
                SkuCount += 1
                .Append("	('" & FormattedTestDate & "', '01', '" & _deliverySkusLinesTransactionNumber & "', " & SkuCount.ToString & ", '" & NextDeliveryChargeItem.SkuNumber & "', -1, " & NextDeliveryChargeItem.PriceAsString & ", 0, 'D', 0, 0, 0, 0),")
            Next
            SkuCount += 1
            .Append("	('" & FormattedTestDate & "', '01', '" & _deliverySkusLinesTransactionNumber & "', " & SkuCount.ToString & ", '542500', -4, 5.59, 0, 'B', 0, 0, 0, 0),")

            .Append("	('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 1, '170711', -2, 33.59, 0, 'B', 0, 0, 0, 0),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _orderNumberTransactionNumber & "', 1, '193104', -1, 78.64, 0, 'B', 0, 0, 0, 0)")

            '.Append("	('" & FormattedTestDate & "', '01', '0675', 1, '213851', -1, 28.59, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 2, '600235', 1, 9.58, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 3, '218931', 1, 1.52, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 4, '154903', 1, 3.08, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 5, '601065', 1, 0.99, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 6, '166077', 1, 10.71, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 7, '607267', 1, 4.49, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 8, '607024', 1, 3.49, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0675', 9, '601065', 1, 0.99, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0706', 1, '209992', -1, 99.99, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0725', 1, '152830', -1, 2.19, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0725', 2, '189594', 1, 149.99, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0726', 2, '152830', -1, 2.19, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '01', '0726', 1, '189594', -1, 149.99, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1074', 1, '224595', -3, 1.98, 0, 'B', 1, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1076', 1, '166167', -1, 5.91, 0, 'B', 0, 1, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1076', 2, '166167', 1, 5.91, 0, 'B', 0, 1, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1082', 1, '235300', -1, 5.58, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1082', 2, '235300', -1, 5.58, 0, 'B', 0, 0, 0, 0),")
            '.Append("	('" & FormattedTestDate & "', '02', '1082', 3, '235301', -6, 0.81, 0, 'B', 1, 0, 0, 0)")
        End With

        Return DLLINETestData
    End Function

    Private Shared Sub PopulateDLTOTSTestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetDLTOTSTestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLLINETestData()
            CleanupDLTOTSTestData()
            Throw New Exception("Failed setting up DLTOTS test data.", ex)
        End Try
    End Sub

    Private Shared Function GetDLTOTSTestData() As StringBuilder
        Dim DLTOTSTestData As New StringBuilder
        Dim FormattedTestDate As String = GetValidTestDate().ToString("yyyy-MM-dd")
        Dim FormattedTooEarlyDate As String = GetTooEarlyDate().ToString("yyyy-MM-dd")
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DLTOTSTestData
            .Append("Insert Into ")
            .Append("	DLTOTS")
            .Append("		(DATE1, TILL, [TRAN], ORDN, VOID, TMOD, [PROC], SUSE, AUPD, BACK, ICOM, IEMP, PARK, PKRC, REMO, RTI)")
            .Append("Values")
            .Append("	('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', '000000', 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _parkedTransactionNumber & "', '000000', 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 'C'),")
            .Append("	('" & FormattedTooEarlyDate & "', '01', '" & _dateTooEarlyTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _trainingModeTransactionNumber & "', '000000', 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _lineReversedTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nullOriginalTranNumberTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _allPositiveQuantitiesTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', '000000', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _deliverySkusLinesTransactionNumber & "', '000000', 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', NULL, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            .Append("	('" & FormattedTestDate & "', '01', '" & _orderNumberTransactionNumber & "', '123456', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C')")
            '.Append("	('" & FormattedTestDate & "', '01', '0693', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '01', '0706', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '01', '0725', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '01', '0726', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '02', '1074', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '02', '1076', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 'C'),")
            '.Append("	('" & FormattedTestDate & "', '02', '1082', 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 'C')")
        End With

        Return DLTOTSTestData
    End Function

    Private Shared Sub PopulateDLRCUSTestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetDLRCUSTestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLLINETestData()
            CleanupDLTOTSTestData()
            CleanupDLRCUSTestData()
            Throw New Exception("Failed setting up DLRCUS test data.", ex)
        End Try
    End Sub

    Private Shared Function GetDLRCUSTestData() As StringBuilder
        Dim DLRCUSTestData As New StringBuilder
        Dim FormattedTestDate As String = GetValidTestDate().ToString("yyyy-MM-dd")
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DLRCUSTestData
            .Append("Insert Into ")
            .Append("	DLRCUS")
            .Append("		(DATE1,TILL, [TRAN], NUMB, OVAL, OTRN)")
            .Append("	Values ")
            .Append("		('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 1, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 2, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _voidTransactionNumber & "', 3, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 1, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 2, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 3, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 4, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 5, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 6, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 7, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 8, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 9, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 10, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _allNegativeQuantitiesTransactionNumber & "', 11, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsSku1TransactionLineNumber & ", 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsSku3TransactionLineNumber & ", 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _relatedItemSkusTestsTransactionNumber & "', " & _relatedItemSkusTestsNoRelatedItemTransactionLineNumber & ", 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _parkedTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _parkedTransactionNumber & "', 1, 1, '0000'),")
            .Append("		('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTooLateDate & "', '01', '" & _dateTooLateTransactionNumber & "', 1, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _nullOriginalTranNumberTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _nullOriginalTranNumberTransactionNumber & "', 1, 1, NULL),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _nonZeroOriginalTranNumberTransactionNumber & "', " & _nonZeroOriginalTranNumberTransactionLineNumber & ", 0, '" & _nonZeroOriginalTranNumber & "'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 1, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _2PositiveAnd2NegativeQuantitiesTransactionNumber & "', 3, 1, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _orderNumberTransactionNumber & "', 0, 0, '0000'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _orderNumberTransactionNumber & "', 1, 1, '1234')")
            '.Append("		('" & FormattedTestDate & "', '01', '0596', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0645', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0675', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0675', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0693', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0693', 1, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0706', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0706', 1, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0725', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0725', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0725', 2, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0726', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '01', '0726', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1074', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1074', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1076', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1076', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1082', 0, 0, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1082', 1, 1, '0000'),")
            '.Append("		('" & FormattedTestDate & "', '02', '1082', 2, 1, '0000')")
        End With

        Return DLRCUSTestData
    End Function

    Private Shared Sub PopulateRELITMTestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetRELITMTestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLLINETestData()
            CleanupDLTOTSTestData()
            CleanupDLRCUSTestData()
            CleanupRELITMTestData()
            Throw New Exception("Failed setting up RELITM test data.", ex)
        End Try
    End Sub

    Private Shared Function GetRELITMTestData() As StringBuilder
        Dim RELITMTestData As New StringBuilder

        With RELITMTestData
            .Append("Insert Into ")
            .Append("	RELITM")
            .Append("		(SPOS,PPOS,DELC)")
            .Append("	Values ")
            .Append("		('157979', '199903', 0),")
            .Append("		('166113', '166112', 0),")
            .Append("		('166123', '166122', 0),")
            .Append("		('166240', '166239', 0),")
            .Append("		('206976', '206975', 0),")
            .Append("		('214541', '213656', 0),")
            .Append("		('215525', '214540', 0),")
            .Append("		('221098', '221076', 0),")
            .Append("		('224595', '224596', 0),")
            .Append("		('230129', '230128', 0),")
            .Append("		('231523', '231323', 0),")
            .Append("		('233619', '233618', 0),")
            .Append("		('235301', '235300', 0),")
            .Append("		('235357', '235356', 0)")
        End With

        Return RELITMTestData
    End Function

    Private Shared Sub PopulateDLPAIDTestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetDLPAIDTestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLPAIDTestData()
            Throw New Exception("Failed setting up DLPAID test data.", ex)
        End Try
    End Sub

    Private Shared Function GetDLPAIDTestData() As StringBuilder
        Dim DLPAIDTestData As New StringBuilder
        Dim FormattedTestDate As String = GetValidTestDate().ToString("yyyy-MM-dd")
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DLPAIDTestData
            .Append("Insert Into ")
            .Append("	DLPAID")
            .Append("		(DATE1, TILL, [TRAN], NUMB, [TYPE], AMNT, [CARD], EXDT, COPN, CLAS, CKEY, SUPV, CKAC, CKSC, CKNO, DBRF, SEQN, CBAM, DIGC, ECOM, STDT, MPOW, MRAT, TENV, RTI)")
            .Append("	Values ")
            .Append("		('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 1, 1, -8.68, '0000000000000000000', '0000', '000000', '00', 0, '000', '0000000000', '000000', '000000', 0, '0000', 0.00, 0, 1, '0000', 0, 0.00000, 0.00, 'C'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 2, 1, -20.00, '0000000000000000000', '0000', '000000', '00', 0, '000', '0000000000', '000000', '000000', 0, '0000', 0.00, 0, 1, '0000', 0, 0.00000, 0.00, 'C')")
        End With

        Return DLPAIDTestData
    End Function

    Private Shared Sub PopulateDLGIFTTestData()

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .CommandText = GetDLGIFTTestData.ToString
                        .ExecuteNonQuery()
                    End With
                End Using
            End Using
        Catch ex As Exception
            CleanupDLGIFTTestData()
            Throw New Exception("Failed setting up DLGIFT test data.", ex)
        End Try
    End Sub

    Private Shared Function GetDLGIFTTestData() As StringBuilder
        Dim DLGIFTTestData As New StringBuilder
        Dim FormattedTestDate As String = GetValidTestDate().ToString("yyyy-MM-dd")
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DLGIFTTestData
            .Append("Insert Into ")
            .Append("	DLGIFT")
            .Append("		(DATE1, TILL, [TRAN], LINE, SERI, EEID, [TYPE], AMNT, COMM, RTI)")
            .Append("	Values ")
            .Append("		('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 1, '08273826', '007', 'TR', -15.00, 1, 'C'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 2, '08273833', '007', 'TR', -15.00, 1, 'C'),")
            .Append("		('" & FormattedTestDate & "', '01', '" & _twoCashAndThreeGiftTokenPaymentsTransactionNumber & "', 3, '08273840', '007', 'TR', -18.50, 1, 'C')")
        End With

        Return DLGIFTTestData
    End Function
#End Region

#Region "ClassCleanup Procedures"

    <ClassCleanup()> _
    Public Shared Sub CleanupTestDataFomDatabase()

        CleanupDLLINETestData()
        CleanupDLTOTSTestData()
        CleanupDLRCUSTestData()
        CleanupDLPAIDTestData()
        CleanupDLGIFTTestData()
    End Sub

    Private Shared Sub CleanupDLTOTSTestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteDLTOTSTestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteDLTOTSTestData() As StringBuilder
        Dim DeleteDLTOTSTestData As New StringBuilder
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DeleteDLTOTSTestData
            .Append("Delete ")
            .Append("	DLTOTS ")
            .Append("Where ")
            .Append("   DATE1 <= '" & FormattedTooLateDate & "'")
        End With

        Return DeleteDLTOTSTestData
    End Function

    Private Shared Sub CleanupDLLINETestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteDLLINETestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteDLLINETestData() As StringBuilder
        Dim DeleteDLLINETestData As New StringBuilder
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DeleteDLLINETestData
            .Append("Delete ")
            .Append("	DLLINE ")
            .Append("Where ")
            .Append("   DATE1 <= '" & FormattedTooLateDate & "'")
        End With

        Return DeleteDLLINETestData
    End Function

    Private Shared Sub CleanupDLRCUSTestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteDLRCUSTestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteDLRCUSTestData() As StringBuilder
        Dim DeleteDLRCUSTestData As New StringBuilder
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DeleteDLRCUSTestData
            .Append("Delete ")
            .Append("	DLRCUS ")
            .Append("Where ")
            .Append("   DATE1 <= '" & FormattedTooLateDate & "'")
        End With

        Return DeleteDLRCUSTestData
    End Function

    Private Shared Sub CleanupRELITMTestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteRELITMTestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteRELITMTestData() As StringBuilder
        Dim DeleteRELITMTestData As New StringBuilder

        With DeleteRELITMTestData
        End With

        Return DeleteRELITMTestData
    End Function

    Private Shared Sub CleanupDLPAIDTestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteDLPAIDTestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteDLPAIDTestData() As StringBuilder
        Dim DeleteDLPAIDTestData As New StringBuilder
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DeleteDLPAIDTestData
            .Append("Delete ")
            .Append("	DLPAID ")
            .Append("Where ")
            .Append("   DATE1 <= '" & FormattedTooLateDate & "'")
        End With

        Return DeleteDLPAIDTestData
    End Function

    Private Shared Sub CleanupDLGIFTTestData()

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .CommandText = GetDeleteDLGIFTTestData.ToString
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Shared Function GetDeleteDLGIFTTestData() As StringBuilder
        Dim DeleteDLGIFTTestData As New StringBuilder
        Dim FormattedTooLateDate As String = GetTooLateDate().ToString("yyyy-MM-dd")

        With DeleteDLGIFTTestData
            .Append("Delete ")
            .Append("	DLGIFT ")
            .Append("Where ")
            .Append("   DATE1 <= '" & FormattedTooLateDate & "'")
        End With

        Return DeleteDLGIFTTestData
    End Function
#End Region
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "GetRefundTransactionLines Tests"

#Region "Datatable structure tests"

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsSomething()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()

        Assert.IsTrue(testRefundRep.GetRefundTransactionLines(TestDate, TestDate) IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTable()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()

        Assert.AreEqual("System.Data.DataTable", testRefundRep.GetRefundTransactionLines(TestDate, TestDate).GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsCorrectlyNamedDataTable()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testData As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.AreEqual(testRefundRep.TableName, testData.TableName)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8Columns()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns.Count = 9)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith1stColumnNamedAfterTransactionDateField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(0).ColumnName, testRefundRep._fieldName_TranDate) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith1stColumnOfTypeDateTime()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(0).DataType Is GetType(DateTime))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith2ndColumnNamedAfterTransactionTillField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(1).ColumnName, testRefundRep._fieldName_TranTill) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith2ndColumnOfTypeString()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(1).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith3rdColumnNamedAfterTransactionNumberField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(2).ColumnName, testRefundRep._fieldName_TranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith3rdColumnOfTypeString()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(2).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith4thColumnNamedAfterTransactionLineNumber()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(3).ColumnName, testRefundRep._fieldName_TranLineNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith4thColumnOfTypeInt16()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(3).DataType Is GetType(Int16))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith5thColumnNamedAfterSkuNumberField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(4).ColumnName, testRefundRep._fieldName_SkuNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith5thColumnOfTypeString()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(4).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith6thColumnNamedAfterSkuPriceField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(5).ColumnName, testRefundRep._fieldName_SkuPrice) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith6thColumnOfTypeDecimal()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(5).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith7thColumnNamedAfterSkuQuantityField()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(6).ColumnName, testRefundRep._fieldName_SkuQuantity) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith7thColumnOfTypeDecimal()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(6).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8thColumnNamedAfterOriginalTransactionNumber()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(7).ColumnName, testRefundRep._fieldName_OriginalTranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8thColumnOfTypeString()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(7).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith9thColumnNamedAfterNumberCashAndGiftTokenRefundPayments()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(String.Compare(testDataTable.Columns(8).ColumnName, testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith9thColumnOfTypeInteger()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        Assert.IsTrue(testDataTable.Columns(8).DataType Is GetType(Integer))
    End Sub
#End Region

#Region "Data selection tests"

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackTransactionsBeforeStartDate()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim earlyCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _dateTooEarlyTransactionNumber).ToList()
            earlyCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And earlyCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackTransactionsAfterEndDate()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim lateCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _dateTooLateTransactionNumber).ToList()
            lateCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And lateCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackVoidTransactions()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim voidCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _voidTransactionNumber).ToList()
            voidCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And voidCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackParkedTransactions()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim parkedCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _parkedTransactionNumber).ToList()
            parkedCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And parkedCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackTrainingModeTransactions()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim trainingCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _trainingModeTransactionNumber).ToList()
            trainingCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And trainingCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackQODOrderRefundTransactions()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim orderNoCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _orderNumberTransactionNumber).ToList()
            orderNoCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And orderNoCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_OnlyBringsBackTransactionsThatHaveAllNegativeQuantityLines_TestTranWithNoNegativeQuantityLines()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim refundedCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _allPositiveQuantitiesTransactionNumber).ToList()
            refundedCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And refundedCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_OnlyBringsBackTransactionLinesWithNegativeQuantity_TestTranWithOnlySomeNegativeQuantityLines()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim refundedCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _2PositiveAnd2NegativeQuantitiesTransactionNumber).ToList()
            refundedCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And refundedCount = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotOnlyBringBackAnyLineReversedTransactionLines()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim refundedCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _lineReversedTransactionNumber).ToList()
            refundedCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And refundedCount = 2)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_DoesNotBringBackAnyDeliverySkuRefundTransactionLines()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim refundedCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _deliverySkusLinesTransactionNumber).ToList()
            refundedCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And refundedCount = 2)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_BringsBackAllTransactionLinesWithNegativeQuantityForTransaction()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim refundedCount As Integer = 0
        Dim lineCount As Integer = 0

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _allNegativeQuantitiesTransactionNumber).ToList()
            If NextTranLine.Item(testRefundRep._fieldName_SkuQuantity) < 0 Then
                refundedCount += 1
            End If
            lineCount += 1
        Next

        Assert.IsTrue(testDataTable.Rows.Count > 0 And refundedCount = lineCount)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ConvertsNullOriginalTransactionNumberToAllZeroes()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim ConvertedNullOriginalTransactionNumberToAllZeroes As Boolean = False
        Dim FoundTestRow As Boolean = False

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _nullOriginalTranNumberTransactionNumber).ToList()
            FoundTestRow = True
            If NextTranLine.Item(testRefundRep._fieldName_OriginalTranNumber) = "0000" Then
                ConvertedNullOriginalTransactionNumberToAllZeroes = True
                Exit For
            End If
        Next
        If FoundTestRow Then
            Assert.IsTrue(ConvertedNullOriginalTransactionNumberToAllZeroes)
        Else
            Assert.Inconclusive("Could not find the test row to check the conversion process.")
        End If
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_RetrievesOriginalTransactionNumberCorrectly()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)
        Dim NonZeroOriginalTransactionNumber As String = ""
        Dim FoundTestRow As Boolean = False

        For Each NextTranLine As DataRow In (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _nonZeroOriginalTranNumberTransactionNumber And tranLine.Item(testRefundRep._fieldName_TranLineNumber) = _nonZeroOriginalTranNumberTransactionLineNumber).ToList()
            FoundTestRow = True
            NonZeroOriginalTransactionNumber = NextTranLine.Item(testRefundRep._fieldName_OriginalTranNumber)
            Exit For
        Next
        If FoundTestRow Then
            Assert.IsTrue(NonZeroOriginalTransactionNumber = _nonZeroOriginalTranNumber)
        Else
            Assert.Inconclusive("Could not find the test row to check the 'Original Transaction Number'.")
        End If
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_AddsUpTotalNumberOfCashAndGiftTokenRefundPaymentsCorrectly_2CashAnd3GiftToken_TotalIs5()
        Dim testRefundRep As New RefundRepository
        Dim TestDate As DateTime = GetValidTestDate()
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(TestDate, TestDate)

        With (From tranLine As DataRow In testDataTable.Rows Select tranLine Where tranLine.Item(testRefundRep._fieldName_TranNumber) = _twoCashAndThreeGiftTokenPaymentsTransactionNumber).ToList()
            If .Count = 1 Then
                Assert.IsTrue(.Item(0).Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 5)
            Else
                Assert.Inconclusive("Could not find the test row to check the 'Total Number of Cash And Gift Toekn Refund Payments'")
            End If
        End With
    End Sub
#End Region
#End Region

#Region "Classes to Create Test Data"

    Private Class DeliveryChargeSku

        Private _skuNumber As String
        Private _price As Decimal

        Public Sub New(ByVal SkuNumber As String, ByVal Price As Decimal)

            _skuNumber = SkuNumber
            _price = Price
        End Sub

        ReadOnly Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
        End Property

        ReadOnly Property PriceAsString()
            Get
                Return _price.ToString("F2", CultureInfo.InvariantCulture)
            End Get
        End Property
    End Class

    Private Class DeliveryChargeSkuList

        Private _deliveryChargeSkus As List(Of DeliveryChargeSku)
        Private Const STKMASTABLENAME As String = "STKMAS"
        Private Const STKMASFIELDNAME_SKU As String = "SKUN"
        Private Const STKMASFIELDNAME_PRICE As String = "PRIC"
        Private Const STKMASFIELDNAME_SALETYPEATTRIB As String = "SALT"
        Private Const SALETYPEATTRIB_DELIVERYCHARGE As String = "D"

        Public Sub New()

            _deliveryChargeSkus = New List(Of DeliveryChargeSku)
        End Sub

        Public Sub Load()
            Dim DeliverChargeStockItems As DataTable = Nothing

            Try
                Using con As New Connection
                    Using com As New Command(con)
                        With com
                            .CommandText = GetDeliveryChargeSkuQuery.ToString
                            DeliverChargeStockItems = .ExecuteDataTable
                        End With
                    End Using
                End Using

                If DeliverChargeStockItems IsNot Nothing And DeliverChargeStockItems.Rows.Count > 0 Then
                    Dim DeliveryChargeSkuToAdd As DeliveryChargeSku = Nothing

                    With DeliverChargeStockItems
                        For Each Row As DataRow In .Rows
                            With Row
                                DeliveryChargeSkuToAdd = New DeliveryChargeSku(.Item(STKMASFIELDNAME_SKU), .Item(STKMASFIELDNAME_PRICE))
                            End With
                            _deliveryChargeSkus.Add(DeliveryChargeSkuToAdd)
                        Next
                    End With
                End If
            Catch ex As Exception
                Throw New Exception("Failed setting up Delivery Charge Sku test data.", ex)
            End Try
        End Sub

        Public ReadOnly Property List() As List(Of DeliveryChargeSku)
            Get
                Return _deliveryChargeSkus
            End Get
        End Property

        Private Function GetDeliveryChargeSkuQuery() As StringBuilder
            Dim DeliveryChargeSkuQuery As New StringBuilder

            With DeliveryChargeSkuQuery
                .Append("Select " & STKMASFIELDNAME_SKU & ", " & STKMASFIELDNAME_PRICE & " From " & STKMASTABLENAME & " Where " & STKMASFIELDNAME_SALETYPEATTRIB & " = '" & SALETYPEATTRIB_DELIVERYCHARGE & "'")
            End With

            Return DeliveryChargeSkuQuery
        End Function
    End Class
#End Region
End Class
