﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProcessTransmissionsProgress
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ProcessName = New System.Windows.Forms.TextBox
        Me.ProgressTextBox = New System.Windows.Forms.TextBox
        Me.RecordCountLabel = New System.Windows.Forms.Label
        Me.RecordCountTextBox = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'ProcessName
        '
        Me.ProcessName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProcessName.Location = New System.Drawing.Point(12, 27)
        Me.ProcessName.Name = "ProcessName"
        Me.ProcessName.Size = New System.Drawing.Size(432, 20)
        Me.ProcessName.TabIndex = 0
        '
        'ProgressTextBox
        '
        Me.ProgressTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProgressTextBox.Location = New System.Drawing.Point(12, 96)
        Me.ProgressTextBox.Name = "ProgressTextBox"
        Me.ProgressTextBox.Size = New System.Drawing.Size(432, 22)
        Me.ProgressTextBox.TabIndex = 1
        '
        'RecordCountLabel
        '
        Me.RecordCountLabel.AutoSize = True
        Me.RecordCountLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecordCountLabel.Location = New System.Drawing.Point(12, 157)
        Me.RecordCountLabel.Name = "RecordCountLabel"
        Me.RecordCountLabel.Size = New System.Drawing.Size(94, 16)
        Me.RecordCountLabel.TabIndex = 2
        Me.RecordCountLabel.Text = "Record Count"
        '
        'RecordCountTextBox
        '
        Me.RecordCountTextBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecordCountTextBox.Location = New System.Drawing.Point(128, 147)
        Me.RecordCountTextBox.Name = "RecordCountTextBox"
        Me.RecordCountTextBox.Size = New System.Drawing.Size(153, 26)
        Me.RecordCountTextBox.TabIndex = 3
        '
        'ProcessTransmissionsProgress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(457, 187)
        Me.ControlBox = False
        Me.Controls.Add(Me.RecordCountTextBox)
        Me.Controls.Add(Me.RecordCountLabel)
        Me.Controls.Add(Me.ProgressTextBox)
        Me.Controls.Add(Me.ProcessName)
        Me.Name = "ProcessTransmissionsProgress"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Process Transmissions Progress"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProcessName As System.Windows.Forms.TextBox
    Friend WithEvents ProgressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RecordCountLabel As System.Windows.Forms.Label
    Friend WithEvents RecordCountTextBox As System.Windows.Forms.TextBox
End Class
