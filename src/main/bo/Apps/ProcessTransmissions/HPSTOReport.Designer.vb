﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HPSTOReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.OrderNo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SupplierNo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.OrderDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.DueDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.OrderStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SKU = New DevExpress.XtraGrid.Columns.GridColumn
        Me.OldQty = New DevExpress.XtraGrid.Columns.GridColumn
        Me.NewQty = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Reason = New DevExpress.XtraGrid.Columns.GridColumn
        Me.cmdPrint = New System.Windows.Forms.Button
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(12, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(724, 200)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.AppearancePrint.FooterPanel.BackColor = System.Drawing.Color.White
        Me.GridView1.AppearancePrint.FooterPanel.Options.UseBackColor = True
        Me.GridView1.AppearancePrint.GroupFooter.BackColor = System.Drawing.Color.White
        Me.GridView1.AppearancePrint.GroupFooter.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.OrderNo, Me.SupplierNo, Me.OrderDate, Me.DueDate, Me.OrderStatus, Me.SKU, Me.OldQty, Me.NewQty, Me.Reason})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowQuickHideColumns = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsPrint.UsePrintStyles = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'OrderNo
        '
        Me.OrderNo.Caption = "Order No"
        Me.OrderNo.FieldName = "OrderNo"
        Me.OrderNo.MaxWidth = 80
        Me.OrderNo.MinWidth = 80
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.Visible = True
        Me.OrderNo.VisibleIndex = 0
        Me.OrderNo.Width = 80
        '
        'SupplierNo
        '
        Me.SupplierNo.Caption = "Supplier Number and Name"
        Me.SupplierNo.FieldName = "Supplier"
        Me.SupplierNo.MaxWidth = 180
        Me.SupplierNo.MinWidth = 180
        Me.SupplierNo.Name = "SupplierNo"
        Me.SupplierNo.Visible = True
        Me.SupplierNo.VisibleIndex = 1
        Me.SupplierNo.Width = 180
        '
        'OrderDate
        '
        Me.OrderDate.Caption = "Order Date"
        Me.OrderDate.FieldName = "OrderDate"
        Me.OrderDate.MaxWidth = 100
        Me.OrderDate.MinWidth = 100
        Me.OrderDate.Name = "OrderDate"
        Me.OrderDate.Visible = True
        Me.OrderDate.VisibleIndex = 2
        Me.OrderDate.Width = 100
        '
        'DueDate
        '
        Me.DueDate.Caption = "Due Date"
        Me.DueDate.FieldName = "DueDate"
        Me.DueDate.MaxWidth = 75
        Me.DueDate.MinWidth = 75
        Me.DueDate.Name = "DueDate"
        Me.DueDate.Visible = True
        Me.DueDate.VisibleIndex = 3
        '
        'OrderStatus
        '
        Me.OrderStatus.Caption = "Order Status"
        Me.OrderStatus.FieldName = "OrderStatus"
        Me.OrderStatus.MaxWidth = 100
        Me.OrderStatus.MinWidth = 100
        Me.OrderStatus.Name = "OrderStatus"
        Me.OrderStatus.Visible = True
        Me.OrderStatus.VisibleIndex = 4
        Me.OrderStatus.Width = 100
        '
        'SKU
        '
        Me.SKU.Caption = "SKU"
        Me.SKU.FieldName = "SKU"
        Me.SKU.MaxWidth = 60
        Me.SKU.MinWidth = 60
        Me.SKU.Name = "SKU"
        Me.SKU.Visible = True
        Me.SKU.VisibleIndex = 5
        Me.SKU.Width = 60
        '
        'OldQty
        '
        Me.OldQty.Caption = "Old Qty"
        Me.OldQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.OldQty.FieldName = "OldQty"
        Me.OldQty.MaxWidth = 70
        Me.OldQty.MinWidth = 70
        Me.OldQty.Name = "OldQty"
        Me.OldQty.Visible = True
        Me.OldQty.VisibleIndex = 6
        Me.OldQty.Width = 70
        '
        'NewQty
        '
        Me.NewQty.Caption = "New Qty"
        Me.NewQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.NewQty.FieldName = "NewQty"
        Me.NewQty.MaxWidth = 70
        Me.NewQty.MinWidth = 70
        Me.NewQty.Name = "NewQty"
        Me.NewQty.Visible = True
        Me.NewQty.VisibleIndex = 7
        Me.NewQty.Width = 70
        '
        'Reason
        '
        Me.Reason.Caption = "Reason"
        Me.Reason.FieldName = "Reason"
        Me.Reason.MaxWidth = 200
        Me.Reason.MinWidth = 200
        Me.Reason.Name = "Reason"
        Me.Reason.Visible = True
        Me.Reason.VisibleIndex = 8
        Me.Reason.Width = 200
        '
        'cmdPrint
        '
        Me.cmdPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdPrint.Location = New System.Drawing.Point(661, 218)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(75, 23)
        Me.cmdPrint.TabIndex = 1
        Me.cmdPrint.Text = "F9-Print"
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'HPSTOReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(742, 246)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "HPSTOReport"
        Me.Text = "HPSTOReport"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents OrderNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SupplierNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents OrderDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DueDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents OrderStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SKU As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Value As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NewQty As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents OldQty As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Reason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdPrint As System.Windows.Forms.Button

End Class
