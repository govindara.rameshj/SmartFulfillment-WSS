﻿Public Interface ICoupons

    Sub ProcessUCRecords(ByVal transmissionData As String)
    Sub ProcessUMRecords(ByVal transmissionData As String)
    Sub ProcessUTRecords(ByVal transmissionData As String)
    ReadOnly Property CreateEventChange() As Boolean
    ReadOnly Property MMGEventChange() As Boolean
    ReadOnly Property EventChangeNumber() As String
    ReadOnly Property EventChangeSkuNumber() As String
    ReadOnly Property EventChangePrice() As Decimal
    ReadOnly Property EventChangeDelete() As Boolean
    ReadOnly Property EventNumber() As String
    ReadOnly Property EventHeaderStartDate() As Nullable(Of Date)
    ReadOnly Property EventHeaderEndDate() As Nullable(Of Date)
    ReadOnly Property EventHeaderPriority() As String


End Interface
