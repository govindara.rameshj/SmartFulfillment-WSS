﻿'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Dhanesh Ramachandran
' Date     : 17/08/2011
' Referral : CR0045
' Notes    : Create STHOO file
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Namespace TpWickes

    Public Interface IStock

        Function OnHandStock() As DataSet
    End Interface

End Namespace