﻿'DATA    A2  : Trailer Record
'DATA+2  A3  : Store Number
'DATA+5  A8  : Date
'DATA+13 A5  : File Type
'DATA+18 A2  : Version Number
'DATA+20 A6  : Sequence Number
'DATA+26 A6  : Time
'DATA+32 A2  : Bucket 1 Record Type
'DATA+34 A7  : Bucket 1 Record Count  6.0
'DATA+41 A12 : Bucket 1 Record Value 10.2
'
'              Repeats Every 21 Characters for Buckets 2 - 8

Public Interface ITrailerRecord

    ReadOnly Property RecordType() As String
    ReadOnly Property StoreNumber() As String
    ReadOnly Property [Date]() As String
    ReadOnly Property FileType() As String
    ReadOnly Property VersionNumber() As String
    ReadOnly Property SequenceNumber() As String
    ReadOnly Property Time() As String

    ReadOnly Property Bucket1RecordType() As String
    ReadOnly Property Bucket1RecordCount() As String
    ReadOnly Property Bucket1RecordHashValue() As String

    ReadOnly Property Bucket2RecordType() As String
    ReadOnly Property Bucket2RecordCount() As String
    ReadOnly Property Bucket2RecordHashValue() As String

    ReadOnly Property Bucket3RecordType() As String
    ReadOnly Property Bucket3RecordCount() As String
    ReadOnly Property Bucket3RecordHashValue() As String

    ReadOnly Property Bucket4RecordType() As String
    ReadOnly Property Bucket4RecordCount() As String
    ReadOnly Property Bucket4RecordHashValue() As String

    ReadOnly Property Bucket5RecordType() As String
    ReadOnly Property Bucket5RecordCount() As String
    ReadOnly Property Bucket5RecordHashValue() As String

    ReadOnly Property Bucket6RecordType() As String
    ReadOnly Property Bucket6RecordCount() As String
    ReadOnly Property Bucket6RecordHashValue() As String

    ReadOnly Property Bucket7RecordType() As String
    ReadOnly Property Bucket7RecordCount() As String
    ReadOnly Property Bucket7RecordHashValue() As String

    ReadOnly Property Bucket8RecordType() As String
    ReadOnly Property Bucket8RecordCount() As String
    ReadOnly Property Bucket8RecordHashValue() As String

End Interface

Public Interface ITrailerRecordStructure

    ReadOnly Property RecordType() As IFieldStructure
    ReadOnly Property StoreNumber() As IFieldStructure
    ReadOnly Property [Date]() As IFieldStructure
    ReadOnly Property FileType() As IFieldStructure
    ReadOnly Property VersionNumber() As IFieldStructure
    ReadOnly Property SequenceNumber() As IFieldStructure
    ReadOnly Property Time() As IFieldStructure

    ReadOnly Property Bucket1RecordType() As IFieldStructure
    ReadOnly Property Bucket1RecordCount() As IFieldStructure
    ReadOnly Property Bucket1RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket2RecordType() As IFieldStructure
    ReadOnly Property Bucket2RecordCount() As IFieldStructure
    ReadOnly Property Bucket2RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket3RecordType() As IFieldStructure
    ReadOnly Property Bucket3RecordCount() As IFieldStructure
    ReadOnly Property Bucket3RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket4RecordType() As IFieldStructure
    ReadOnly Property Bucket4RecordCount() As IFieldStructure
    ReadOnly Property Bucket4RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket5RecordType() As IFieldStructure
    ReadOnly Property Bucket5RecordCount() As IFieldStructure
    ReadOnly Property Bucket5RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket6RecordType() As IFieldStructure
    ReadOnly Property Bucket6RecordCount() As IFieldStructure
    ReadOnly Property Bucket6RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket7RecordType() As IFieldStructure
    ReadOnly Property Bucket7RecordCount() As IFieldStructure
    ReadOnly Property Bucket7RecordHashValue() As IFieldStructure

    ReadOnly Property Bucket8RecordType() As IFieldStructure
    ReadOnly Property Bucket8RecordCount() As IFieldStructure
    ReadOnly Property Bucket8RecordHashValue() As IFieldStructure

End Interface