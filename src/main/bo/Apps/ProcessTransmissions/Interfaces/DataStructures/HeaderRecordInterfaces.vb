﻿'DATA    A2  : Header Record
'DATA+2  A3  : Store Number
'DATA+5  A8  : Date
'DATA+13 A5  : File Type
'DATA+18 A2  : Version Number
'DATA+20 A6  : Sequence Number
'DATA+26 A6  : Time
'DATA+32 A10 : File  - LOAD or UPDATE

Public Interface IHeaderRecord

    ReadOnly Property RecordType() As String
    ReadOnly Property StoreNumber() As String
    ReadOnly Property [Date]() As String
    ReadOnly Property FileType() As String
    ReadOnly Property VersionNumber() As String
    ReadOnly Property SequenceNumber() As String
    ReadOnly Property Time() As String
    ReadOnly Property FileDescription() As String

End Interface

Public Interface IHeaderReaderStructure

    ReadOnly Property RecordType() As IFieldStructure
    ReadOnly Property StoreNumber() As IFieldStructure
    ReadOnly Property [Date]() As IFieldStructure
    ReadOnly Property FileType() As IFieldStructure
    ReadOnly Property VersionNumber() As IFieldStructure
    ReadOnly Property SequenceNumber() As IFieldStructure
    ReadOnly Property Time() As IFieldStructure
    ReadOnly Property FileDescription() As IFieldStructure

End Interface