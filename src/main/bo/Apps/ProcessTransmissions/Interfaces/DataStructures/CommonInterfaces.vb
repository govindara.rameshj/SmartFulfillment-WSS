﻿Public Interface IFieldStructure

    Property StartPosition() As Integer
    Property Length() As Integer

End Interface

Public Interface IRecordFormat

    Function FormattedValue(ByVal record As String, ByVal field As IFieldStructure) As String
    Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As IFieldStructure) As Boolean

End Interface