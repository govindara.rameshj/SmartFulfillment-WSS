﻿Namespace HPSTV.Interface

    'HR    Header Record
    'VM    Supplier Master Record
    'VC    Supplier Closedown Record
    'VN    Supplier Narrative Record
    'VD    Supplier Order Depot Record
    'VH    Delivery Center Update Record
    'VR    Supplier Returns Depot Record
    'VQ    Supplier Minimum Order Qty Record
    'VT    Supplier order Type Detail Record
    'TR    Trailer Record

#Region "Record Type - VM | Not Implemented"

    Public Interface IVmRecord

        'not implemented

    End Interface

    Public Interface IVmRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VC | Not Implemented"

    Public Interface IVcRecord

        'not implemented

    End Interface

    Public Interface IVcRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VN | Not Implemented"

    Public Interface IVnRecord

        'not implemented

    End Interface

    Public Interface IVnRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VD | Not Implemented"

    Public Interface IVdRecord

        'not implemented

    End Interface

    Public Interface IVdRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VH | Not Implemented"

    Public Interface IVhRecord

        'not implemented

    End Interface

    Public Interface IVhRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VR | Not Implemented"

    Public Interface IVrRecord

        'not implemented

    End Interface

    Public Interface IVrRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VQ | Not Implemented"

    Public Interface IVqRecord

        'not implemented

    End Interface

    Public Interface IVqRecordStructure

        'not implemented

    End Interface

#End Region

#Region "Record Type - VT"

    'DATA     A2  : Record Type
    'DATA+2   A8  : Update Date
    'DATA+10  A12 : 10.2 Signed numeric - For Trailer Hash
    'DATA+22  A1  : Deletion indicator
    'DATA+23  A5  : Supplier Number
    'DATA+28  A8  : Effective Date
    'DATA+36  A1  : Order Type:  Y = Tradanet
    'DATA+37  A3  : BBC Site Number (173)
    '
    'DATA+40  A1  : BBC Order Type:
    '                    D     = Discreet              
    '                    C     = Consolidated          
    '                    W     = Warehouse             
    '                    A     = Alternative Supplier  
    '                    Blank = Direct                
    '
    'DATA+41  A4  : Order Day Code - Binary Hash (1 - 127)
    '                    1  = Monday
    '                    2  = Tuesday
    '                    4  = Wednesday
    '                    8  = Thursday
    '                    16 = Friday
    '                    32 = Saturday
    '                    64 = Sunday
    '
    '                    Any comination of days is shown as a sum of the relevant day numbers.                  
    '
    'DATA+45  A4  : Lead Time - Monday      (1 - 999)
    'DATA+49  A4  : Lead Time - Tuesday     (1 - 999)
    'DATA+53  A4  : Lead Time - Wednesday   (1 - 999)
    'DATA+57  A4  : Lead Time - Thursday    (1 - 999)
    'DATA+61  A4  : Lead Time - Friday      (1 - 999)
    'DATA+65  A4  : Lead Time - Saturday    (1 - 999)
    'DATA+69  A4  : Lead Time - Sunday      (1 - 999)
    '
    '                    The Lead Time of days is shown as a sum      
    '
    'DATA+73  A1  : Delivery Check Method
    '                    1 = Cartons
    '                    2 = Detail Check

    Public Interface IVtRecord

        ReadOnly Property RecordType() As String
        ReadOnly Property UpdateDate() As String
        ReadOnly Property Hash() As String
        ReadOnly Property DeletionIndicator() As String
        ReadOnly Property SupplierNumber() As String
        ReadOnly Property EffectiveDate() As String
        ReadOnly Property OrderType() As String
        ReadOnly Property BbcSiteNumber() As String
        ReadOnly Property BbcOrderType() As String
        ReadOnly Property OrderDayCode() As String
        ReadOnly Property LeadTimeMonday() As String
        ReadOnly Property LeadTimeTuesday() As String
        ReadOnly Property LeadTimeWednesday() As String
        ReadOnly Property LeadTimeThursday() As String
        ReadOnly Property LeadTimeFriday() As String
        ReadOnly Property LeadTimeSaturday() As String
        ReadOnly Property LeadTimeSunday() As String
        ReadOnly Property DeliveryCheckMethod() As String

    End Interface

    Public Interface IVtRecordStructure

        ReadOnly Property RecordType() As IFieldStructure
        ReadOnly Property UpdateDate() As IFieldStructure
        ReadOnly Property Hash() As IFieldStructure
        ReadOnly Property DeletionIndicator() As IFieldStructure
        ReadOnly Property SupplierNumber() As IFieldStructure
        ReadOnly Property EffectiveDate() As IFieldStructure
        ReadOnly Property OrderType() As IFieldStructure
        ReadOnly Property BbcSiteNumber() As IFieldStructure
        ReadOnly Property BbcOrderType() As IFieldStructure
        ReadOnly Property OrderDayCode() As IFieldStructure
        ReadOnly Property LeadTimeMonday() As IFieldStructure
        ReadOnly Property LeadTimeTuesday() As IFieldStructure
        ReadOnly Property LeadTimeWednesday() As IFieldStructure
        ReadOnly Property LeadTimeThursday() As IFieldStructure
        ReadOnly Property LeadTimeFriday() As IFieldStructure
        ReadOnly Property LeadTimeSaturday() As IFieldStructure
        ReadOnly Property LeadTimeSunday() As IFieldStructure
        ReadOnly Property DeliveryCheckMethod() As IFieldStructure

    End Interface

#End Region

End Namespace