﻿Public Interface IDateAdjuster
    Function GetAdjustedDate(ByVal input As String, ByVal formatType As String, ByVal startPosition As Integer, ByVal dateLength As Integer) As String
End Interface
