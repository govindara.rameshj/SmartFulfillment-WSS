﻿'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Partha
' Date     : 01/06/2011
' Referral : 811
' Notes    : Calculate total float variance
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Interface IBanking

    Function UpdateFloatVariance(ByRef DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal PeriodID As Integer, ByVal CurrencyID As String) As Decimal

End Interface