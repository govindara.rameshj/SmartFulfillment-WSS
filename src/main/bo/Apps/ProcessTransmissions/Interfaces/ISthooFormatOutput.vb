﻿Namespace TpWickes

    Public Interface ISthooFormatOutput

        Property TodayDate() As Date
        Property StoreNumber() As String

        Function FormatData(ByRef DR As DataRow, ByVal DecimalFormatSpecifier As String, ByVal HashPaddingLength As Integer, ByVal HashPaddingChar As Char) As String

    End Interface

End Namespace