﻿Public Interface ISTHPO

    Function GetPurchaseLines(ByRef PurchaseHeader As BOPurchases.cPurchaseHeader, ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB) As List(Of BOPurchases.cPurchaseLine)
    Function FormatODSkuNumber(ByVal SkuNumber As OasysDBBO.ColField(Of String)) As String
    Function FormatODPurchaseOrderNumber(ByVal PurchaseOrderNumber As OasysDBBO.ColField(Of String)) As String
    Function FormatODLineNumber(ByVal LineNumber As Integer) As String
End Interface
