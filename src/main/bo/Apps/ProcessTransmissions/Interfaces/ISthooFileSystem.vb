﻿Namespace TpWickes

    Public Interface ISthooFileSystem

        ReadOnly Property FileName() As String

        Sub WriteToFile(ByVal Line As String)

    End Interface

End Namespace