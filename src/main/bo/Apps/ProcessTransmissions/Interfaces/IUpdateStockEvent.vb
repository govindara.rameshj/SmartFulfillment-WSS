﻿
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Dhanesh Ramachandran
' Date     : 21/09/2011
' Referral : 874
' Notes    : Stock Event Value Update
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Interface IUpdateStockEvent

    Sub StockEventUpdate(ByRef stock As BOStock.cStock, ByVal RetailPriceEventNo As String, ByVal RetailPricePriority As String)

End Interface
