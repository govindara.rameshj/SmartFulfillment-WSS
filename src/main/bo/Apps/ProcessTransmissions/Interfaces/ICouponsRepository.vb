﻿Public Interface ICouponsRepository

    Function GetStock(ByVal skuNumber As String) As DataTable
    Function GetCouponMaster(ByVal couponId As String) As DataTable
    Function GetCouponText(ByVal couponId As String, ByVal sequenceNo As String) As DataTable
    Function GetEventHeader(ByVal number As String) As DataTable
    Function GetEventMas(ByVal eventType As String, ByVal eventKey1 As String, ByVal eventKey2 As String, ByVal eventNumber As String) As DataTable
    Sub UpdateCouponMaster(ByVal couponId As String, ByVal description As String, ByVal storeGenerated As Boolean, ByVal serialNumber As Boolean, ByVal exclusiveCoupon As Boolean, ByVal reusable As Boolean, ByVal collecInfo As Boolean, ByVal deleted As Boolean)
    Sub UpdateCouponText(ByVal couponId As String, ByVal sequenceNumber As String, ByVal printSize As String, ByVal textAlign As String, ByVal printText As String, ByVal deleted As Boolean)
    Sub UpdateEventMas(ByVal eventType As String, ByVal eventKey1 As String, ByVal eventKey2 As String, ByVal eventNumber As String, ByVal priority As String, ByVal deleted As Boolean, ByVal timeOrDayRelated As Boolean, ByVal startdate As Nullable(Of Date), ByVal enddate As Nullable(Of Date), ByVal specialPrice As Decimal, ByVal buyQuantity As Decimal, ByVal getQuantity As Decimal, ByVal discountAmount As Decimal, ByVal percentageDiscount As Decimal, ByVal buyCoupon As String, ByVal sellCoupon As String)

End Interface
