﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ProcessTransmissions.UnitTest")> 
Namespace TpWickes

    Module StringFormatting

#Region "Hash Formatting"

        Friend Function HashingValue(ByVal Value As Decimal, ByVal DecimalFormatSpecifier As String, _
                                     ByVal PaddingLength As Integer, ByVal PaddingChar As Char, _
                                     ByVal ShowSignSymbol As Boolean, ByVal PositiveSignSymbol As String, ByVal NegativeSignSymbol As String) As String

            Dim SignSymbol As String

            SignSymbol = IIf(Value < 0, NegativeSignSymbol, PositiveSignSymbol).ToString

            Return PaddingLeftFormatSpecifier(Math.Abs(Value), DecimalFormatSpecifier, PaddingLength, PaddingChar, False) & _
                   IIf(ShowSignSymbol = True, SignSymbol, String.Empty).ToString

        End Function

#End Region

#Region "Date Formatting"

        Friend Function FormatDateDDMMYY(ByVal DateValue As Date) As String

            Return (DateValue.ToString("dd/MM/yy"))

        End Function

        Friend Function FormatDateDDMMYYYY(ByVal DateValue As Date) As String

            Return (DateValue.ToString("dd/MM/yyyy"))

        End Function

#End Region

#Region "Pad Left"

        Friend Function PaddingLeft(ByVal Value As String, ByVal PaddingLength As Integer, ByVal PaddingChar As Char) As String

            Return Value.PadLeft(PaddingLength, PaddingChar)

        End Function

        Friend Function PaddingLeft(ByVal Value As Decimal, ByVal PaddingLength As Integer, ByVal PaddingChar As Char, ByVal ShowSign As Boolean) As String

            Dim Sign As String

            Sign = IIf(Value < 0, "-", "+").ToString

            Return IIf(ShowSign = True, Sign, String.Empty).ToString & Math.Abs(Value).ToString.PadLeft(PaddingLength, PaddingChar)

        End Function

        Friend Function PaddingLeftFormatSpecifier(ByVal Value As Decimal, ByVal DecimalFormatSpecifier As String, _
                                                   ByVal PaddingLength As Integer, ByVal PaddingChar As Char, ByVal ShowSign As Boolean) As String

            Dim Sign As String

            Sign = IIf(Value < 0, "-", "+").ToString

            Return IIf(ShowSign = True, Sign, String.Empty).ToString & Math.Abs(Value).ToString(DecimalFormatSpecifier).PadLeft(PaddingLength, PaddingChar)

        End Function

#End Region

#Region "Pad Right"

        Friend Function PaddingRight(ByVal Value As String, ByVal PaddingLength As Integer, ByVal PaddingChar As Char) As String

            Return Value.PadRight(PaddingLength, PaddingChar)

        End Function

        Friend Function PaddingRight(ByVal Value As Decimal, ByVal PaddingLength As Integer, ByVal PaddingChar As Char, ByVal ShowSign As Boolean) As String

            Dim Sign As String

            Sign = IIf(Value < 0, "-", "+").ToString

            Return IIf(ShowSign = True, Sign, String.Empty).ToString & Math.Abs(Value).ToString.PadRight(PaddingLength, PaddingChar)

        End Function

#End Region

    End Module

End Namespace