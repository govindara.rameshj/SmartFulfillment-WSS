﻿Public Class SthotFormat
    Implements ISthotFormat

    Private Const PaddingLength As Integer = 8
    Private Const PaddingCharacter As String = " "


    Public Function FormatSthotDocumentNumber(ByVal formatString As String) As String Implements ISthotFormat.FormatSthotDocumentNumber
        Return formatString.PadRight(PaddingLength, CChar(PaddingCharacter))
    End Function

End Class
