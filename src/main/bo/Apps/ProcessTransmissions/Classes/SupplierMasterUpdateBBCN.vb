﻿
Public Class SupplierMasterUpdateBBCN
    Implements ISupplierMaster

    Public Sub UpdateBBCN(ByVal VtRecord As String) Implements ISupplierMaster.UpdateBBCN

        Dim VT As HPSTV.Interface.IVtRecord
        Dim Repository As ISupplierMasterRepository

        VT = New HPSTV.Classes.VtRecord(VtRecord)

        Repository = New SupplierMasterRepository
        Repository.UpdateBBCN(VT.SupplierNumber, VT.BbcSiteNumber)

    End Sub

End Class