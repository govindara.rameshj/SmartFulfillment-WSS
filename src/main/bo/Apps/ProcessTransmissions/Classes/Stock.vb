﻿Namespace TpWickes

    Public Class Stock
        Implements IStock


        Public Function OnHandStock() As System.Data.DataSet Implements IStock.OnHandStock

            Dim Repository As IStockRepository

            Repository = StockRepositoryFactory.FactoryGet

            Return Repository.OnHandStock

        End Function
    End Class

End Namespace