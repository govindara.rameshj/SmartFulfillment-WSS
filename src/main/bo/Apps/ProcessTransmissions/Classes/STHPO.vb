﻿Public Class STHPO
    Implements ISTHPO

    Public Function FormatODLineNumber(ByVal LineNumber As Integer) As String Implements ISTHPO.FormatODLineNumber

        Return LineNumber.ToString.PadLeft(4, "0"c)
    End Function

    Public Function FormatODPurchaseOrderNumber(ByVal PurchaseOrderNumber As OasysDBBO.ColField(Of String)) As String Implements ISTHPO.FormatODPurchaseOrderNumber

        Return PurchaseOrderNumber.Value.ToString.PadLeft(6, "0"c)
    End Function

    Public Function FormatODSkuNumber(ByVal SkuNumber As OasysDBBO.ColField(Of String)) As String Implements ISTHPO.FormatODSkuNumber

        Return SkuNumber.Value.ToString.PadLeft(6, "0"c)
    End Function

    Public Function GetPurchaseLines(ByRef PurchaseHeader As BOPurchases.cPurchaseHeader, ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB) As System.Collections.Generic.List(Of BOPurchases.cPurchaseLine) Implements ISTHPO.GetPurchaseLines

        Try
            Dim PurchaseOrderDetail As New BOPurchases.cPurchaseLine(Oasys3DB)

            GetPurchaseLines = RetrievePurchaseOrderDetailLines(PurchaseOrderDetail, PurchaseOrderDetail.HeaderIdentity, PurchaseHeader.PurchaseHeaderID.Value)
        Catch ex As Exception
            GetPurchaseLines = New List(Of BOPurchases.cPurchaseLine)
        End Try
    End Function

    Friend Overridable Function RetrievePurchaseOrderDetailLines(ByRef PurchaseOrderDetail As BOPurchases.cPurchaseLine, ByRef HeaderIdentityFieldID As OasysDBBO.ColField(Of Integer), ByVal HeaderId As Integer) As System.Collections.Generic.List(Of BOPurchases.cPurchaseLine)

        Try
            PurchaseOrderDetail.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, HeaderIdentityFieldID, HeaderId)
            RetrievePurchaseOrderDetailLines = PurchaseOrderDetail.LoadMatches
        Catch ex As Exception
            RetrievePurchaseOrderDetailLines = New List(Of BOPurchases.cPurchaseLine)
        End Try
    End Function
End Class
