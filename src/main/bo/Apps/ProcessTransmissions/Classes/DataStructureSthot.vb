﻿Imports ProcessTransmissions.DataStructureSthot.Implementation
Imports System.Text

Namespace DataStructureSthot.Interface
    Public Interface IFieldStructure
        Property PaddingCharacter() As String
        Property Length() As Int16
        Property PadLeft() As Boolean
        Property PadRight() As Boolean
    End Interface
    Public Interface IOutgoingRecord
        ReadOnly Property GetRecord() As String
    End Interface
    Public Interface IIncomingRecord
        WriteOnly Property SetRecord() As String
    End Interface
    Public Interface IHeaderStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileName() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property PolingPcName() As [Interface].IFieldStructure
    End Interface
    Public Interface ITrailerStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileType() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure
    End Interface

    Public Interface IDdRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property TillId() As [Interface].IFieldStructure
        ReadOnly Property TransactionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property DepartmentNumber() As [Interface].IFieldStructure
        ReadOnly Property InputByScannerFlag() As [Interface].IFieldStructure
        ReadOnly Property SupervisorCashierNumber() As [Interface].IFieldStructure
        ReadOnly Property QuantitySold() As [Interface].IFieldStructure
        ReadOnly Property SystemLookUpPrice() As [Interface].IFieldStructure
        ReadOnly Property ItemPrice() As [Interface].IFieldStructure
        ReadOnly Property ItemPriceVatExclusive() As [Interface].IFieldStructure
        ReadOnly Property ExtendedValue() As [Interface].IFieldStructure
        ReadOnly Property ExtendedCost() As [Interface].IFieldStructure
        ReadOnly Property RelatedItemsSingleFlag() As [Interface].IFieldStructure
        ReadOnly Property PriceOverrideReasonCodeNumber() As [Interface].IFieldStructure
        ReadOnly Property PriceOverrideReason() As [Interface].IFieldStructure
        ReadOnly Property TaggedItemFlag() As [Interface].IFieldStructure
        ReadOnly Property CatchAllItemFlag() As [Interface].IFieldStructure
        ReadOnly Property VatSymbolUser() As [Interface].IFieldStructure
        ReadOnly Property TemporaryPriceChangePrice() As [Interface].IFieldStructure
        ReadOnly Property TemporaryPriceChangeMargin() As [Interface].IFieldStructure
        ReadOnly Property PriceOverridePriceDifference() As [Interface].IFieldStructure
        ReadOnly Property PriceOverrideMarginErosionCode() As [Interface].IFieldStructure
        ReadOnly Property QuanityBreakErosion() As [Interface].IFieldStructure
        ReadOnly Property QuantityBreakMarginErosionCode() As [Interface].IFieldStructure
        ReadOnly Property DealGroupErosion() As [Interface].IFieldStructure
        ReadOnly Property DealGroupErosionCode() As [Interface].IFieldStructure
        ReadOnly Property MultiBuyErosion() As [Interface].IFieldStructure
        ReadOnly Property MultiBuyMarginErosionCode() As [Interface].IFieldStructure
        ReadOnly Property HierarchySpendLeverlErosion() As [Interface].IFieldStructure
        ReadOnly Property HierarchySpendLevelErosionCode() As [Interface].IFieldStructure
        ReadOnly Property EmployeeSalePriceDifference() As [Interface].IFieldStructure
        ReadOnly Property EmployeeSaleMarginErosionCode() As [Interface].IFieldStructure
        ReadOnly Property LineReversalFlag() As [Interface].IFieldStructure
        ReadOnly Property LastEventSequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property HierarchyCategory() As [Interface].IFieldStructure
        ReadOnly Property HierarchyGroup() As [Interface].IFieldStructure
        ReadOnly Property HierarchySubGroup() As [Interface].IFieldStructure
        ReadOnly Property HierarchyStyle() As [Interface].IFieldStructure
        ReadOnly Property PartialQuarantine() As [Interface].IFieldStructure
        ReadOnly Property SecondarySaleErosionValue() As [Interface].IFieldStructure
        ReadOnly Property SoldFromIndicator() As [Interface].IFieldStructure
        ReadOnly Property SaleTypeIndicator() As [Interface].IFieldStructure
        ReadOnly Property VatCode() As [Interface].IFieldStructure
        ReadOnly Property VatValue() As [Interface].IFieldStructure
        ReadOnly Property CollectedAtBackDoorFlag() As [Interface].IFieldStructure
        ReadOnly Property ItemCollectedIndicator() As [Interface].IFieldStructure
        ReadOnly Property LineReversalReasonCode() As [Interface].IFieldStructure
    End Interface
    Public Interface IHeader
        Function GetRecordType(ByVal record As String) As String
        Function GetStoreNumber(ByVal record As String) As String
        Function GetDate(ByVal record As String) As String
        Function GetFileName(ByVal record As String) As String
        Function GetVersionNumber(ByVal record As String) As String
        Function GetSequenceNumber(ByVal record As String) As String
        Function GetTime(ByVal record As String) As String
        Function GetPollingPcName(ByVal record As String) As String
    End Interface
    Public Interface ITrailer
        Function GetRecordType(ByVal record As String) As String
        Function GetStoreNumber(ByVal record As String) As String
        Function GetDate(ByVal record As String) As String
        Function GetFileName(ByVal record As String) As String
        Function GetVersionNumber(ByVal record As String) As String
        Function GetSequenceNumber(ByVal record As String) As String
        Function GetTime(ByVal record As String) As String
        Function GetBucket1RecordType(ByVal record As String) As String
        Function GetBucket1RecordCount(ByVal record As String) As String
        Function GetBucket1RecordHashValue(ByVal record As String) As String
    End Interface
    Public Interface IDdRecord
        Inherits IOutgoingRecord
        Property RecordType() As String
        Property [Date]() As String
        Property Hash() As String
        Property TillId() As String
        Property TransactionNumber() As String
        Property SequenceNumber() As String
        Property SkuNumber() As String
        Property DepartmentNumber() As String
        Property InputByScanner() As String
        Property SupervisorCashierNumber() As String
        Property QuantitySold() As String
        Property SystemLookUpPrice() As String
        Property ItemPrice() As String
        Property ItemPriceVatExclusive() As String
        Property ExtendedValue() As String
        Property ExtendedCost() As String
        Property RelatedItemsSingle() As String
        Property PriceOverrideReasonCodeNumber() As String
        Property PriceOverrideReason() As String
        Property TaggedItem() As String
        Property CatchAllItem() As String
        Property VatSymbolUser() As String
        Property TemporaryPriceChangePrice() As String
        Property TemporaryPriceChangeMargin() As String
        Property PriceOverridePriceDifference() As String
        Property PriceOverrideMarginErosionCode() As String
        Property QuanityBreakErosion() As String
        Property QuantityBreakMarginErosionCode() As String
        Property DealGroupErosion() As String
        Property DealGroupErosionCode() As String
        Property MultiBuyErosion() As String
        Property MultiBuyMarginErosionCode() As String
        Property HierarchySpendLevelErosion() As String
        Property HierarchySpendLevelErosionCode() As String
        Property EmployeeSalePriceDifference() As String
        Property EmployeeSaleMarginErosionCode() As String
        Property LineReversal() As String
        Property LastEventSequenceNumber() As String
        Property HierarchyCategory() As String
        Property HierarchyGroup() As String
        Property HierarchySubGroup() As String
        Property HierarchyStyle() As String
        Property PartialQuarantine() As String
        Property SecondarySaleErosionValue() As String
        Property SoldFromIndicator() As String
        Property SaleTypeIndicator() As String
        Property VatCode() As String
        Property VatValue() As String
        Property CollectedAtBackDoorFlag() As String
        Property ItemCollectedIndicator() As String
        Property LineReversalReasonCode() As String
    End Interface

End Namespace

Namespace DataStructureSthot

    Public Structure SthotHeaderStructure
        Implements [Interface].IHeaderStructure

        Private _sthotHeaderType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileNameStart As Int16 = 14
        Private Const FileNameLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const PolingPcNameStart As Int16 = 33
        Private Const PolingPcNameLength As Int16 = 10
        Private Const ZeroPaddingCharacter As String = "0"
        Private Const SingleSpacePaddingCharacter As String = " "
        Private Const padLeftFlagTrue As Boolean = True
        Private Const padLeftFlagFalse As Boolean = False
        Private Const padRightFlagTrue As Boolean = True
        Private Const padRightFlagFalse As Boolean = False


#End Region

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Date
            Get
                Return New Field(DateLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property FileName() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.FileName
            Get
                Return New Field(FileNameLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property PolingPcName() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.PolingPcName
            Get
                Return New Field(PolingPcNameLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.RecordType
            Get
                Return New Field(RecordTypeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.StoreNumber
            Get
                Return New Field(StoreNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Time() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Time
            Get
                Return New Field(TimeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property VersionNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.VersionNumber
            Get
                Return New Field(VersionNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property
    End Structure
    Public Structure SthotTrailerStructure
        Implements [Interface].ITrailerStructure

        Private _sthotTrailerType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileTypeStart As Int16 = 14
        Private Const FileTypeLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const Bucket1RecordTypeStart As Int16 = 33
        Private Const Bucket1RecordTypeLength As Int16 = 2
        Private Const Bucket1RecordCountStart As Int16 = 35
        Private Const Bucket1RecordCountLength As Int16 = 7
        Private Const Bucket1RecordHashValueStart As Int16 = 42
        Private Const Bucket1RecordHashValueLength As Int16 = 12
        Private Const ZeroPaddingCharacter As String = "0"
        Private Const SingleSpacePaddingCharacter As String = " "
        Private Const padLeftFlagTrue As Boolean = True
        Private Const padLeftFlagFalse As Boolean = False
        Private Const padRightFlagTrue As Boolean = True
        Private Const padRightFlagFalse As Boolean = False


#End Region

        Public ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordHashValue
            Get
                Return New Field(Bucket1RecordHashValueLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordType
            Get
                Return New Field(Bucket1RecordTypeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Date
            Get
                Return New Field(DateLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property FileType() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.FileType
            Get
                Return New Field(FileTypeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.RecordType
            Get
                Return New Field(RecordTypeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property StoreNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.StoreNumber
            Get
                Return New Field(StoreNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Time1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Time
            Get
                Return New Field(TimeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property VersionNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.VersionNumber
            Get
                Return New Field(VersionNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordCount
            Get
                Return New Field(Bucket1RecordCountLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property
    End Structure
    Public Structure SthotDdRecordStructure
        Implements [Interface].IDdRecordStructure

        Private _sthotDdRecordType As String
#Region "Constants"


        Private Const ZeroPaddingCharacter As String = "0"
        Private Const SingleSpacePaddingCharacter As String = " "
        Private Const padLeftFlagTrue As Boolean = True
        Private Const padLeftFlagFalse As Boolean = False
        Private Const padRightFlagTrue As Boolean = True
        Private Const padRightFlagFalse As Boolean = False
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateLength As Int16 = 8
        Private Const HashValueLength As Int16 = 12
        Private Const TillIdLength As Int16 = 2
        Private Const TransactionNumberLength As Int16 = 4
        Private Const SkuNumberLength As Int16 = 6
        Private Const SequenceNumberLength As Int16 = 4
        Private Const DepartmentNumberLength As Int16 = 2
        Private Const InputByScannerFlagLength As Int16 = 1
        Private Const SupervisorCashierNumberLength As Int16 = 3
        Private Const QuantitySoldLength As Int16 = 7
        Private Const SystemLookUpPriceLength As Int16 = 10
        Private Const ItemPriceLength As Int16 = 10
        Private Const ItemPriceVatExclusiveLength As Int16 = 10
        Private Const ExtendedValueLength As Int16 = 10
        Private Const ExtendedCostLength As Int16 = 11
        Private Const RelatedItemsSingleFlagLength As Int16 = 1
        Private Const PriceOverrideReasonCodeNumberLength As Int16 = 2
        Private Const PriceOverrideReasonLength As Int16 = 20
        Private Const TaggedItemFlagLength As Int16 = 1
        Private Const CatchAllItemFlagLength As Int16 = 1
        Private Const VatSymbolUsedFlagLength As Int16 = 1
        Private Const TemporaryPriceChangePriceDifferenceLength As Int16 = 10
        Private Const TemporaryPriceChangeMarginErosionCodeLength As Int16 = 6
        Private Const PriceOverridePriceDifferenceLength As Int16 = 10
        Private Const PriceOverrideMarginErosionCodeLength As Int16 = 6
        Private Const QuantityBreakErosionLength As Int16 = 10
        Private Const QuantityBreakMarginErosionCodeLength As Int16 = 6
        Private Const DealGroupErosionLength As Int16 = 10
        Private Const DealGroupMarginErosionCodeLength As Int16 = 6
        Private Const MultiBuyErosionLength As Int16 = 10
        Private Const MultiBuyMarginErosionCodeLength As Int16 = 6
        Private Const HierarchySpendLevelErosionLength As Int16 = 10
        Private Const HierarchySpendLevelErosionCodeLength As Int16 = 6
        Private Const EmployeeSalePriceDifferenceLength As Int16 = 10
        Private Const EmployeeSaleMarginErosionCodeLength As Int16 = 6
        Private Const LineReversalFlagLength As Int16 = 1
        Private Const LastEventSequenceNumberLength As Int16 = 6
        Private Const HierarchyCategoryLength As Int16 = 6
        Private Const HierarchyGroupLength As Int16 = 6
        Private Const HierarchySubGroupLength As Int16 = 6
        Private Const HierarchyStyleLength As Int16 = 6
        Private Const PartialQuarantineLength As Int16 = 3
        Private Const SecondarySaleErosionValueLength As Int16 = 10
        Private Const SoldIndicatorFlagLength As Int16 = 1
        Private Const SaleTypeAttributeLength As Int16 = 1
        Private Const VatCodeLength As Int16 = 1
        Private Const VatValueLength As Int16 = 11
        Private Const CollectedAtBackDoorFlagLength As Int16 = 1
        Private Const ItemCollectedIndicatorLength As Int16 = 1
        Private Const LineReversalReasonCodeLength As Int16 = 2




#End Region

        Public ReadOnly Property CatchAllItemFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.CatchAllItemFlag
            Get
                Return New Field(CatchAllItemFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property CollectedAtBackDoorFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.CollectedAtBackDoorFlag
            Get
                Return New Field(CollectedAtBackDoorFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.Date
            Get
                Return New Field(DateLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property DealGroupErosion() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.DealGroupErosion
            Get
                Return New Field(DealGroupErosionLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property DealGroupErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.DealGroupErosionCode
            Get
                Return New Field(DealGroupMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property DepartmentNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.DepartmentNumber
            Get
                Return New Field(DepartmentNumberLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property EmployeeSaleMarginErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.EmployeeSaleMarginErosionCode
            Get
                Return New Field(EmployeeSaleMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property EmployeeSalePriceDifference() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.EmployeeSalePriceDifference
            Get
                Return New Field(EmployeeSalePriceDifferenceLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property ExtendedCost() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.ExtendedCost
            Get
                Return New Field(ExtendedCostLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property ExtendedValue() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.ExtendedValue
            Get
                Return New Field(ExtendedValueLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.Hash
            Get
                Return New Field(HashValueLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchyCategory() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchyCategory
            Get
                Return New Field(HierarchyCategoryLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchyGroup() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchyGroup
            Get
                Return New Field(HierarchyGroupLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchySpendLeverlErosion() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchySpendLeverlErosion
            Get
                Return New Field(HierarchySpendLevelErosionLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchyStyle() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchyStyle
            Get
                Return New Field(HierarchyStyleLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchySubGroup() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchySubGroup
            Get
                Return New Field(HierarchySubGroupLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property InputByScannerFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.InputByScannerFlag
            Get
                Return New Field(InputByScannerFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property ItemCollectedIndicator() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.ItemCollectedIndicator
            Get
                Return New Field(ItemCollectedIndicatorLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property ItemPrice() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.ItemPrice
            Get
                Return New Field(ItemPriceLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property ItemPriceVatExclusive() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.ItemPriceVatExclusive
            Get
                Return New Field(ItemPriceVatExclusiveLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property LastEventSequenceNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.LastEventSequenceNumber
            Get
                Return New Field(LastEventSequenceNumberLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property LineReversalFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.LineReversalFlag
            Get
                Return New Field(LineReversalFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property LineReversalReasonCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.LineReversalReasonCode
            Get
                Return New Field(LineReversalReasonCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property MultiBuyErosion() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.MultiBuyErosion
            Get
                Return New Field(MultiBuyErosionLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property MultiBuyMarginErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.MultiBuyMarginErosionCode
            Get
                Return New Field(MultiBuyMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property PartialQuarantine() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.PartialQuarantine
            Get
                Return New Field(PartialQuarantineLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property PriceOverrideMarginErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.PriceOverrideMarginErosionCode
            Get
                Return New Field(PriceOverrideMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property PriceOverridePriceDifference() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.PriceOverridePriceDifference
            Get
                Return New Field(PriceOverridePriceDifferenceLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property PriceOverrideReason() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.PriceOverrideReason
            Get
                Return New Field(PriceOverrideReasonLength, SingleSpacePaddingCharacter, padLeftFlagFalse, padRightFlagTrue)
            End Get
        End Property

        Public ReadOnly Property PriceOverrideReasonCodeNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.PriceOverrideReasonCodeNumber
            Get
                Return New Field(PriceOverrideReasonCodeNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property QuanityBreakErosion() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.QuanityBreakErosion
            Get
                Return New Field(QuantityBreakErosionLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property QuantityBreakMarginErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.QuantityBreakMarginErosionCode
            Get
                Return New Field(QuantityBreakMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property QuantitySold() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.QuantitySold
            Get
                Return New Field(QuantitySoldLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.RecordType
            Get
                Return New Field(RecordTypeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property RelatedItemsSingleFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.RelatedItemsSingleFlag
            Get
                Return New Field(RelatedItemsSingleFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SaleTypeIndicator() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SaleTypeIndicator
            Get
                Return New Field(SaleTypeAttributeLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SecondarySaleErosionValue() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SecondarySaleErosionValue
            Get
                Return New Field(SecondarySaleErosionValueLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SoldFromIndicator() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SoldFromIndicator
            Get
                Return New Field(SoldIndicatorFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SupervisorCashierNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SupervisorCashierNumber
            Get
                Return New Field(SupervisorCashierNumberLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property SystemLookUpPrice() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.SystemLookUpPrice
            Get
                Return New Field(SystemLookUpPriceLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property TaggedItemFlag() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.TaggedItemFlag
            Get
                Return New Field(TaggedItemFlagLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property TemporaryPriceChangeMargin() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.TemporaryPriceChangeMargin
            Get
                Return New Field(TemporaryPriceChangeMarginErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property TemporaryPriceChangePrice() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.TemporaryPriceChangePrice
            Get
                Return New Field(TemporaryPriceChangePriceDifferenceLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property TillId() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.TillId
            Get
                Return New Field(TillIdLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property TransactionNumber() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.TransactionNumber
            Get
                Return New Field(TransactionNumberLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property VatCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.VatCode
            Get
                Return New Field(VatCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property VatSymbolUser() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.VatSymbolUser
            Get
                Return New Field(VatSymbolUsedFlagLength, SingleSpacePaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property VatValue() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.VatValue
            Get
                Return New Field(VatValueLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property HierarchySpendLevelErosionCode() As [Interface].IFieldStructure Implements [Interface].IDdRecordStructure.HierarchySpendLevelErosionCode
            Get
                Return New Field(HierarchySpendLevelErosionCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property
    End Structure
End Namespace


Namespace DataStructureSthot.Implementation

    Public Structure Field
        Implements DataStructureSthot.Interface.IFieldStructure

        Private _name As String
        Private _length As Int16
        Private _paddingCharacter As String
        Private _padLeft As Boolean
        Private _padRight As Boolean

        Friend Sub New(ByVal length As Int16, ByVal paddingCharacter As String, ByVal padLeft As Boolean, ByVal padRight As Boolean)
            Me._length = length
            Me._paddingCharacter = paddingCharacter
            Me._padLeft = padLeft
            Me._padRight = padRight
        End Sub
        Public Property Length() As Short Implements DataStructureSthot.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property PaddingCharacter() As String Implements DataStructureSthot.Interface.IFieldStructure.PaddingCharacter
            Get
                Return _paddingCharacter
            End Get
            Set(ByVal value As String)
                _paddingCharacter = value
            End Set
        End Property

        Public Property PadLeft() As Boolean Implements [Interface].IFieldStructure.PadLeft
            Get
                Return _padLeft
            End Get
            Set(ByVal value As Boolean)
                _padLeft = value
            End Set
        End Property

        Public Property PadRight() As Boolean Implements [Interface].IFieldStructure.PadRight
            Get
                Return _padRight
            End Get
            Set(ByVal value As Boolean)
                _padRight = value
            End Set
        End Property

    End Structure
    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = String.Empty
            If record.Length >= field.Length Then
                record = record.Substring(0, field.Length)
            End If
            If field.PadLeft Then
                val = record.PadLeft(field.Length, CChar(field.PaddingCharacter))
            ElseIf field.PadRight Then
                val = record.PadRight(field.Length, CChar(field.PaddingCharacter))
            End If
            Return val
        End Function

    End Class


    Public Class SthotRecordHeader
        Inherits RecordFormat
        Implements [Interface].IHeader

        Private _record As String
        Private _recordStructure As SthotHeaderStructure

#Region "Public Functions"

        Public Function GetStoreNumber(ByVal record As String) As String Implements [Interface].IHeader.GetStoreNumber
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.StoreNumber)
        End Function

        Public Function GetDate(ByVal record As String) As String Implements [Interface].IHeader.GetDate
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.Date)
        End Function
        Public Function GetFileName(ByVal record As String) As String Implements [Interface].IHeader.GetFileName
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.FileName)
        End Function

        Public Function GetVersionNumber(ByVal record As String) As String Implements [Interface].IHeader.GetVersionNumber
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.VersionNumber)
        End Function

        Public Function GetSequenceNumber(ByVal record As String) As String Implements [Interface].IHeader.GetSequenceNumber
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.SequenceNumber)
        End Function

        Public Function GetTime(ByVal record As String) As String Implements [Interface].IHeader.GetTime
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.Time)
        End Function

        Public Function GetPollingPcName(ByVal record As String) As String Implements [Interface].IHeader.GetPollingPcName
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.PolingPcName)
        End Function


        Public Function GetRecordType(ByVal record As String) As String Implements [Interface].IHeader.GetRecordType
            _record = record
            _recordStructure = New SthotHeaderStructure()
            Return Me.FormattedValue(record, _recordStructure.RecordType)
        End Function
#End Region
    End Class
    Public Class SthotRecordTrailer
        Inherits RecordFormat
        Implements [Interface].ITrailer


        Private _record As String
        Private _recordStructure As SthotTrailerStructure

#Region "Public Functions"

        Public Function GetBucket1RecordCount(ByVal record As String) As String Implements [Interface].ITrailer.GetBucket1RecordCount
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.Bucket1RecordCount)
        End Function

        Public Function GetBucket1RecordHashValue(ByVal record As String) As String Implements [Interface].ITrailer.GetBucket1RecordHashValue
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.Bucket1RecordHashValue)
        End Function

        Public Function GetBucket1RecordType(ByVal record As String) As String Implements [Interface].ITrailer.GetBucket1RecordType
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.Bucket1RecordType)
        End Function

        Public Function GetDate1(ByVal record As String) As String Implements [Interface].ITrailer.GetDate
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.Date1)
        End Function

        Public Function GetFileName1(ByVal record As String) As String Implements [Interface].ITrailer.GetFileName
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.FileType)
        End Function

        Public Function GetRecordType1(ByVal record As String) As String Implements [Interface].ITrailer.GetRecordType
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.RecordType1)
        End Function

        Public Function GetSequenceNumber1(ByVal record As String) As String Implements [Interface].ITrailer.GetSequenceNumber
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.SequenceNumber1)
        End Function

        Public Function GetStoreNumber1(ByVal record As String) As String Implements [Interface].ITrailer.GetStoreNumber
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.StoreNumber1)
        End Function

        Public Function GetTime1(ByVal record As String) As String Implements [Interface].ITrailer.GetTime
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.Time1)
        End Function

        Public Function GetVersionNumber1(ByVal record As String) As String Implements [Interface].ITrailer.GetVersionNumber
            _record = record
            _recordStructure = New SthotTrailerStructure()
            Return Me.FormattedValue(record, _recordStructure.VersionNumber1)
        End Function
#End Region
    End Class
    Public Class SthotDdRecord
        Inherits RecordFormat
        Implements [Interface].IDdRecord

        Private _RecordType As String
        Private _Date As String
        Private _Hash As String
        Private _TillId As String
        Private _TransactionNumber As String
        Private _SequenceNumber As String
        Private _SkuNumber As String
        Private _DepartmentNumber As String
        Private _InputByScanner As String
        Private _SupervisorCashierNumber As String
        Private _QuantitySold As String
        Private _SystemLookUpPrice As String
        Private _ItemPrice As String
        Private _ItemPriceVatExclusive As String
        Private _ExtendedValue As String
        Private _ExtendedCost As String
        Private _RelatedItemsSingle As String
        Private _PriceOverrideReasonCodeNumber As String
        Private _PriceOverrideReason As String
        Private _TaggedItem As String
        Private _CatchAllItem As String
        Private _VatSymbolUser As String
        Private _TemporaryPriceChangePrice As String
        Private _TemporaryPriceChangeMargin As String
        Private _PriceOverridePriceDifference As String
        Private _PriceOverrideMarginErosionCode As String
        Private _QuanityBreakErosion As String
        Private _QuantityBreakMarginErosionCode As String
        Private _DealGroupErosion As String
        Private _DealGroupErosionCode As String
        Private _MultiBuyErosion As String
        Private _MultiBuyMarginErosionCode As String
        Private _HierarchySpendLevelErosion As String
        Private _HierarchySpendLevelErosionCode As String
        Private _EmployeeSalePriceDifference As String
        Private _EmployeeSaleMarginErosionCode As String
        Private _LineReversal As String
        Private _LastEventSequenceNumber As String
        Private _HierarchyCategory As String
        Private _HierarchyGroup As String
        Private _HierarchySubGroup As String
        Private _HierarchyStyle As String
        Private _PartialQuarantine As String
        Private _SecondarySaleErosionValue As String
        Private _SoldFromIndicator As String
        Private _SaleTypeIndicator As String
        Private _VatCode As String
        Private _VatValue As String
        Private _CollectedAtBackDoorFlag As String
        Private _ItemCollectedIndicator As String
        Private _LineReversalReasonCode As String

        Private strStockAdjustmentSign As String = "+"
        Private strDllineFormatStringReturnValue As String
        Private Const formatStringLength As Integer = 9
        Private Const formatStringCharacter As String = "0.00"



        Private _recordStructure As New SthotDdRecordStructure


        Public ReadOnly Property Record() As String Implements [Interface].IOutgoingRecord.GetRecord
            Get
                Dim sb As New StringBuilder
                sb.Append(RecordType)
                sb.Append([Date])
                sb.Append(Hash)
                sb.Append(TillId)
                sb.Append(TransactionNumber)
                sb.Append(SequenceNumber)
                sb.Append(SkuNumber)
                sb.Append(DepartmentNumber)
                sb.Append(InputByScanner)
                sb.Append(SupervisorCashierNumber)
                sb.Append(QuantitySold)
                sb.Append(SystemLookUpPrice)
                sb.Append(ItemPrice)
                sb.Append(ItemPriceVatExclusive)
                sb.Append(ExtendedValue)
                sb.Append(ExtendedCost)
                sb.Append(RelatedItemsSingle)
                sb.Append(PriceOverrideReasonCodeNumber)
                sb.Append(PriceOverrideReason)
                sb.Append(TaggedItem)
                sb.Append(CatchAllItem)
                sb.Append(VatCode)
                sb.Append(TemporaryPriceChangePrice)
                sb.Append(TemporaryPriceChangeMargin)
                sb.Append(PriceOverridePriceDifference)
                sb.Append(PriceOverrideMarginErosionCode)
                sb.Append(QuanityBreakErosion)
                sb.Append(QuantityBreakMarginErosionCode)
                sb.Append(DealGroupErosion)
                sb.Append(DealGroupErosionCode)
                sb.Append(MultiBuyErosion)
                sb.Append(MultiBuyMarginErosionCode)
                sb.Append(HierarchySpendLevelErosion)
                sb.Append(HierarchySpendLevelErosionCode)
                sb.Append(EmployeeSalePriceDifference)
                sb.Append(EmployeeSaleMarginErosionCode)
                sb.Append(LineReversal)
                sb.Append(LastEventSequenceNumber)
                sb.Append(HierarchyCategory)
                sb.Append(HierarchyGroup)
                sb.Append(HierarchySubGroup)
                sb.Append(HierarchyStyle)
                sb.Append(PartialQuarantine)
                sb.Append(SecondarySaleErosionValue)
                sb.Append(SoldFromIndicator)
                sb.Append(SaleTypeIndicator)
                sb.Append(VatSymbolUser)
                'sb.Append(VatCode)
                sb.Append(VatValue)
                sb.Append(CollectedAtBackDoorFlag)
                sb.Append(CollectedAtBackDoorFlag)
                sb.Append(LineReversalReasonCode)
                'sb.Append(HierarchyStyle)



                '& strSthoRecordType & strSthoRecordDate & strSthoRecordHashValue & strDllineTillId & strDllineTransactionNumber 
                '& strDllineTransactionintLineNumber & strDllineSkuNumber & strDllineDepartmentNumber & strDllineInputWasByBarcode & strDllineSupervisorNumber 
                '& strDllineQuantitySold & strDllineSystemLookupPrice & strDllineItemSellingPrice & strDllineItemSellingPriceExclusiveOfVat 
                '& strDllineExtendedValue & strDllineExtendedCost & strDllineItemIsRelatedSingle & strDllinePriceOverrideReasonCode 
                '& strDllinePriceOverrideReasonText & strDllineTaggedItem & strDllineCatchAllItem & strDllineVatSymbol 
                '& strDllineTemporaryPriceChangePriceDifference & strDllineTemporaryPriceChangeMarginErosionCode 
                '& strDllinePriceOverridePriceDifference & strDllinePriceOverrideMarginErosionCode & strDllineQuantityBreakErosionValue 
                '& strDllineQuantityBreakMarginErosionCode & strDllineDealGroupErosionValue & strDllineDealGroupMarginErosionCode
                ' & strDllineMultiBuyErosionValue & strDllineMultiBuyMarginErosionCode & strDllineHierarchySpendErosionValue 
                '& strDllineHierarchySpendMarginErosionCode & strDllinePrimaryEmployeeDiscountErosionValue 
                '& strDllinePrimaryEmployeeDiscountMarginErosionCode & strDllineLineWasReversed & strDllineLastEventSequenceNumberUsedForThisLine'
                ' & strDllineHierarchyCategoryNumber & strDllineHierarchyGroupNumber & strDllineHierarchySubGroupNumber 
                '& strDllineHierarchyStyleNumber & strDllineQuarantineSupervisorNumber & strDllineSecondaryEmployeeDiscountMarginErosionValue 
                '& strDllineSoldFromMarkdownStock & strDllineSaleTypeAttribute & strDllineVatCodeNumber & strDllineVatValueForThisLine & DDBdco & DDBdcoInd & DDRcod

                Return sb.ToString
            End Get
        End Property


        Public Property CatchAllItem() As String Implements [Interface].IDdRecord.CatchAllItem
            Get
                If Not _CatchAllItem Is Nothing Then
                    If (CDbl(Me.FormattedValue(_CatchAllItem, _recordStructure.CatchAllItemFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _CatchAllItem = value
            End Set
        End Property

        Public Property CollectedAtBackDoorFlag() As String Implements [Interface].IDdRecord.CollectedAtBackDoorFlag
            Get
                If Not _CollectedAtBackDoorFlag Is Nothing Then
                    If (CDbl(Me.FormattedValue(_CollectedAtBackDoorFlag, _recordStructure.CollectedAtBackDoorFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _CollectedAtBackDoorFlag = value
            End Set
        End Property

        Public Property [Date]() As String Implements [Interface].IDdRecord.Date
            Get
                Return Me.FormattedValue(_Date, _recordStructure.Date)
            End Get
            Set(ByVal value As String)
                _Date = value
            End Set
        End Property

        Public Property DealGroupErosion() As String Implements [Interface].IDdRecord.DealGroupErosion
            Get
                If Not _DealGroupErosion Is Nothing Then
                    Me.FormatDecToString(CDec(_DealGroupErosion), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _DealGroupErosion = value
            End Set
        End Property

        Public Property DealGroupErosionCode() As String Implements [Interface].IDdRecord.DealGroupErosionCode
            Get
                If Not _DealGroupErosionCode Is Nothing Then
                    Return Me.FormattedValue(_DealGroupErosionCode, _recordStructure.DealGroupErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _DealGroupErosionCode = value
            End Set
        End Property

        Public Property DepartmentNumber() As String Implements [Interface].IDdRecord.DepartmentNumber
            Get
                If Not _DepartmentNumber Is Nothing Then
                    Return Me.FormattedValue(_DepartmentNumber, _recordStructure.DepartmentNumber)
                End If
            End Get
            Set(ByVal value As String)
                _DepartmentNumber = value
            End Set
        End Property

        Public Property EmployeeSaleMarginErosionCode() As String Implements [Interface].IDdRecord.EmployeeSaleMarginErosionCode
            Get
                If Not _EmployeeSaleMarginErosionCode Is Nothing Then
                    Return Me.FormattedValue(_EmployeeSaleMarginErosionCode, _recordStructure.EmployeeSaleMarginErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _EmployeeSaleMarginErosionCode = value
            End Set
        End Property

        Public Property EmployeeSalePriceDifference() As String Implements [Interface].IDdRecord.EmployeeSalePriceDifference
            Get
                If Not _EmployeeSalePriceDifference Is Nothing Then
                    Me.FormatDecToString(CDec(_EmployeeSalePriceDifference), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _EmployeeSalePriceDifference = value
            End Set
        End Property

        Public Property ExtendedCost() As String Implements [Interface].IDdRecord.ExtendedCost
            Get
                If Not _ExtendedCost Is Nothing Then
                    Me.FormatDecToString(CDec(_ExtendedCost), strDllineFormatStringReturnValue, 10, Space(1), "0.000")
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _ExtendedCost = value
            End Set
        End Property

        Public Property ExtendedValue() As String Implements [Interface].IDdRecord.ExtendedValue
            Get
                If Not _ExtendedValue Is Nothing Then
                    Me.FormatDecToString(CDec(_ExtendedValue), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _ExtendedValue = value
            End Set
        End Property

        Public Property Hash() As String Implements [Interface].IDdRecord.Hash
            Get
                If Not _TransactionNumber Is Nothing Then
                    FormatDecToString(CDec(TransactionNumber), strDllineFormatStringReturnValue, 11, " ", "0.00")
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _TransactionNumber = value
            End Set
        End Property

        Public Property HierarchyCategory() As String Implements [Interface].IDdRecord.HierarchyCategory
            Get
                If Not _HierarchyCategory Is Nothing Then
                    Return Me.FormattedValue(_HierarchyCategory, _recordStructure.HierarchyCategory)
                End If
            End Get
            Set(ByVal value As String)
                _HierarchyCategory = value
            End Set
        End Property

        Public Property HierarchyGroup() As String Implements [Interface].IDdRecord.HierarchyGroup
            Get
                If Not _HierarchyGroup Is Nothing Then
                    Return Me.FormattedValue(_HierarchyGroup, _recordStructure.HierarchyGroup)
                End If
            End Get
            Set(ByVal value As String)
                _HierarchyGroup = value
            End Set
        End Property

        Public Property HierarchySpendLevelErosion() As String Implements [Interface].IDdRecord.HierarchySpendLevelErosion
            Get
                If Not _HierarchySpendLevelErosion Is Nothing Then
                    Me.FormatDecToString(CDec(_HierarchySpendLevelErosion), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _HierarchySpendLevelErosion = value
            End Set
        End Property

        Public Property HierarchyStyle() As String Implements [Interface].IDdRecord.HierarchyStyle
            Get
                If Not _HierarchyStyle Is Nothing Then
                    Return Me.FormattedValue(_HierarchyStyle, _recordStructure.HierarchyStyle)
                End If
            End Get
            Set(ByVal value As String)
                _HierarchyStyle = value
            End Set
        End Property

        Public Property HierarchySubGroup() As String Implements [Interface].IDdRecord.HierarchySubGroup
            Get
                If Not _HierarchySubGroup Is Nothing Then
                    Return Me.FormattedValue(_HierarchySubGroup, _recordStructure.HierarchySubGroup)
                End If
            End Get
            Set(ByVal value As String)
                _HierarchySubGroup = value
            End Set
        End Property

        Public Property InputByScanner() As String Implements [Interface].IDdRecord.InputByScanner
            Get
                If Not _InputByScanner Is Nothing Then
                    If (CDbl(Me.FormattedValue(_InputByScanner, _recordStructure.InputByScannerFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _InputByScanner = value
            End Set
        End Property

        Public Property ItemCollectedIndicator() As String Implements [Interface].IDdRecord.ItemCollectedIndicator
            Get
                If Not _ItemCollectedIndicator Is Nothing Then
                    Return Me.FormattedValue(_ItemCollectedIndicator, _recordStructure.ItemCollectedIndicator)
                End If
            End Get
            Set(ByVal value As String)
                _ItemCollectedIndicator = value
            End Set
        End Property

        Public Property ItemPrice() As String Implements [Interface].IDdRecord.ItemPrice
            Get
                If CDec(_ItemPrice) < +0 Then
                    _ItemPrice = CStr(CDec(_ItemPrice) * -1)
                End If
                _ItemPrice = Me.FormattedValue(CDec(_ItemPrice).ToString("#####0.00") & strStockAdjustmentSign, _recordStructure.ItemPrice)
                Return _ItemPrice
            End Get
            Set(ByVal value As String)
                _ItemPrice = value
            End Set
        End Property

        Public Property ItemPriceVatExclusive() As String Implements [Interface].IDdRecord.ItemPriceVatExclusive
            Get
                If CDec(_ItemPriceVatExclusive) < +0 Then
                    _ItemPriceVatExclusive = CStr(CDec(_ItemPriceVatExclusive) * -1)
                End If
                _ItemPriceVatExclusive = Me.FormattedValue(CDec(_ItemPriceVatExclusive).ToString("#####0.00") & strStockAdjustmentSign, _recordStructure.ItemPriceVatExclusive)
                Return _ItemPriceVatExclusive
            End Get
            Set(ByVal value As String)
                _ItemPriceVatExclusive = value
            End Set
        End Property

        Public Property LastEventSequenceNumber() As String Implements [Interface].IDdRecord.LastEventSequenceNumber
            Get
                If Not _LastEventSequenceNumber Is Nothing Then
                    Return Me.FormattedValue(_LastEventSequenceNumber, _recordStructure.LastEventSequenceNumber)
                End If
            End Get
            Set(ByVal value As String)
                _LastEventSequenceNumber = value
            End Set
        End Property

        Public Property LineReversal() As String Implements [Interface].IDdRecord.LineReversal
            Get
                If Not _LineReversal Is Nothing Then
                    If (CDbl(Me.FormattedValue(_LineReversal, _recordStructure.LineReversalFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _LineReversal = value
            End Set
        End Property

        Public Property LineReversalReasonCode() As String Implements [Interface].IDdRecord.LineReversalReasonCode
            Get
                If Not _LineReversalReasonCode Is Nothing Then
                    Return Me.FormattedValue(_LineReversalReasonCode, _recordStructure.LineReversalReasonCode)
                End If
            End Get
            Set(ByVal value As String)
                _LineReversalReasonCode = value
            End Set
        End Property

        Public Property MultiBuyErosion() As String Implements [Interface].IDdRecord.MultiBuyErosion
            Get
                If Not _MultiBuyErosion Is Nothing Then
                    Me.FormatDecToString(CDec(_MultiBuyErosion), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _MultiBuyErosion = value
            End Set
        End Property

        Public Property MultiBuyMarginErosionCode() As String Implements [Interface].IDdRecord.MultiBuyMarginErosionCode
            Get
                If Not _MultiBuyMarginErosionCode Is Nothing Then
                    Return Me.FormattedValue(_MultiBuyMarginErosionCode, _recordStructure.MultiBuyMarginErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _MultiBuyMarginErosionCode = value
            End Set
        End Property

        Public Property PartialQuarantine() As String Implements [Interface].IDdRecord.PartialQuarantine
            Get
                If Not _PartialQuarantine Is Nothing Then
                    Return Me.FormattedValue(_PartialQuarantine, _recordStructure.PartialQuarantine)
                End If
            End Get
            Set(ByVal value As String)
                _PartialQuarantine = value
            End Set
        End Property

        Public Property PriceOverrideMarginErosionCode() As String Implements [Interface].IDdRecord.PriceOverrideMarginErosionCode
            Get
                If Not _PriceOverrideMarginErosionCode Is Nothing Then
                    Return Me.FormattedValue(_PriceOverrideMarginErosionCode, _recordStructure.PriceOverrideMarginErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _PriceOverrideMarginErosionCode = value
            End Set
        End Property

        Public Property PriceOverridePriceDifference() As String Implements [Interface].IDdRecord.PriceOverridePriceDifference
            Get
                If Not _PriceOverridePriceDifference Is Nothing Then
                    Me.FormatDecToString(CDec(_PriceOverridePriceDifference), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _PriceOverridePriceDifference = value
            End Set
        End Property

        Public Property PriceOverrideReason() As String Implements [Interface].IDdRecord.PriceOverrideReason
            Get
                If Not _PriceOverrideReason Is Nothing Then
                    Return Me.FormattedValue(_PriceOverrideReason, _recordStructure.PriceOverrideReason)
                End If
            End Get
            Set(ByVal value As String)
                _PriceOverrideReason = value
            End Set
        End Property

        Public Property PriceOverrideReasonCodeNumber() As String Implements [Interface].IDdRecord.PriceOverrideReasonCodeNumber
            Get
                If Not _PriceOverrideReasonCodeNumber Is Nothing Then
                    Return Me.FormattedValue(_PriceOverrideReasonCodeNumber, _recordStructure.PriceOverrideReasonCodeNumber)
                End If
            End Get
            Set(ByVal value As String)
                _PriceOverrideReasonCodeNumber = value
            End Set
        End Property

        Public Property QuanityBreakErosion() As String Implements [Interface].IDdRecord.QuanityBreakErosion
            Get
                If Not _QuanityBreakErosion Is Nothing Then
                    Me.FormatDecToString(CDec(_QuanityBreakErosion), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _QuanityBreakErosion = value
            End Set
        End Property

        Public Property QuantityBreakMarginErosionCode() As String Implements [Interface].IDdRecord.QuantityBreakMarginErosionCode
            Get
                If Not _QuantityBreakMarginErosionCode Is Nothing Then
                    Return Me.FormattedValue(_QuantityBreakMarginErosionCode, _recordStructure.QuantityBreakMarginErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _QuantityBreakMarginErosionCode = value
            End Set
        End Property

        Public Property QuantitySold() As String Implements [Interface].IDdRecord.QuantitySold
            Get
                If Not _QuantitySold Is Nothing Then
                    Me.FormatDecToString(CInt(_QuantitySold), strDllineFormatStringReturnValue, 6, Space(1))
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _QuantitySold = value
            End Set
        End Property

        Public Property RecordType() As String Implements [Interface].IDdRecord.RecordType
            Get
                If Not _RecordType Is Nothing Then
                    Return Me.FormattedValue(_RecordType, _recordStructure.RecordType)
                End If
            End Get
            Set(ByVal value As String)
                _RecordType = value
            End Set
        End Property

        Public Property RelatedItemsSingle() As String Implements [Interface].IDdRecord.RelatedItemsSingle
            Get
                If Not _RelatedItemsSingle Is Nothing Then
                    If (CDbl(Me.FormattedValue(_RelatedItemsSingle, _recordStructure.RelatedItemsSingleFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _RelatedItemsSingle = value
            End Set
        End Property

        Public Property SaleTypeIndicator() As String Implements [Interface].IDdRecord.SaleTypeIndicator
            Get
                If Not _SaleTypeIndicator Is Nothing Then
                    Return Me.FormattedValue(_SaleTypeIndicator, _recordStructure.SaleTypeIndicator)
                End If
            End Get
            Set(ByVal value As String)
                _SaleTypeIndicator = value
            End Set
        End Property

        Public Property SecondarySaleErosionValue() As String Implements [Interface].IDdRecord.SecondarySaleErosionValue
            Get
                If Not _SecondarySaleErosionValue Is Nothing Then
                    Me.FormatDecToString(CDec(_SecondarySaleErosionValue), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _SecondarySaleErosionValue = value
            End Set
        End Property

        Public Property SequenceNumber() As String Implements [Interface].IDdRecord.SequenceNumber
            Get
                If Not _SequenceNumber Is Nothing Then
                    Return Me.FormattedValue(_SequenceNumber, _recordStructure.SequenceNumber)
                End If
            End Get
            Set(ByVal value As String)
                _SequenceNumber = value
            End Set
        End Property

        Public Property SkuNumber() As String Implements [Interface].IDdRecord.SkuNumber
            Get
                If Not _SkuNumber Is Nothing Then
                    Return Me.FormattedValue(_SkuNumber, _recordStructure.SkuNumber)
                End If
            End Get
            Set(ByVal value As String)
                _SkuNumber = value
            End Set
        End Property

        Public Property SoldFromIndicator() As String Implements [Interface].IDdRecord.SoldFromIndicator
            Get
                If Not _SoldFromIndicator Is Nothing Then
                    If (CDbl(Me.FormattedValue(_SoldFromIndicator, _recordStructure.SoldFromIndicator)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _SoldFromIndicator = value
            End Set
        End Property

        Public Property SupervisorCashierNumber() As String Implements [Interface].IDdRecord.SupervisorCashierNumber
            Get
                If Not _SupervisorCashierNumber Is Nothing Then
                    Return Me.FormattedValue(_SupervisorCashierNumber, _recordStructure.SupervisorCashierNumber)
                End If
            End Get
            Set(ByVal value As String)
                _SupervisorCashierNumber = value
            End Set
        End Property

        Public Property SystemLookUpPrice() As String Implements [Interface].IDdRecord.SystemLookUpPrice
            Get
                If Not _SystemLookUpPrice Is Nothing Then
                    If CDec(_SystemLookUpPrice) < +0 Then
                        _SystemLookUpPrice = CStr(CDec(_SystemLookUpPrice) * -1)
                    End If
                    _SystemLookUpPrice = Me.FormattedValue(CDec(_SystemLookUpPrice).ToString("#####0.00") & strStockAdjustmentSign, _recordStructure.SystemLookUpPrice)
                    Return _SystemLookUpPrice
                End If
            End Get
            Set(ByVal value As String)
                _SystemLookUpPrice = value
            End Set
        End Property

        Public Property TaggedItem() As String Implements [Interface].IDdRecord.TaggedItem
            Get
                If Not _TaggedItem Is Nothing Then
                    If (CDbl(Me.FormattedValue(_TaggedItem, _recordStructure.TaggedItemFlag)) = 1) Then
                        Return "Y"
                    Else
                        Return "N"
                    End If
                End If
            End Get
            Set(ByVal value As String)
                _TaggedItem = value
            End Set
        End Property

        Public Property TemporaryPriceChangeMargin() As String Implements [Interface].IDdRecord.TemporaryPriceChangeMargin
            Get
                If Not _TemporaryPriceChangeMargin Is Nothing Then
                    Return Me.FormattedValue(_TemporaryPriceChangeMargin, _recordStructure.TemporaryPriceChangeMargin)
                End If
            End Get
            Set(ByVal value As String)
                _TemporaryPriceChangeMargin = value
            End Set
        End Property

        Public Property TemporaryPriceChangePrice() As String Implements [Interface].IDdRecord.TemporaryPriceChangePrice
            Get
                If Not _TemporaryPriceChangePrice Is Nothing Then
                    Me.FormatDecToString(CDec(_TemporaryPriceChangePrice), strDllineFormatStringReturnValue, formatStringLength, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _TemporaryPriceChangePrice = value
            End Set
        End Property

        Public Property TillId() As String Implements [Interface].IDdRecord.TillId
            Get
                If Not _TillId Is Nothing Then
                    Return Me.FormattedValue(_TillId, _recordStructure.TillId)
                End If
            End Get
            Set(ByVal value As String)
                _TillId = value
            End Set
        End Property

        Public Property TransactionNumber() As String Implements [Interface].IDdRecord.TransactionNumber
            Get
                If Not _TransactionNumber Is Nothing Then
                    Return Me.FormattedValue(_TransactionNumber, _recordStructure.TransactionNumber)
                End If
            End Get
            Set(ByVal value As String)
                _TransactionNumber = value
            End Set
        End Property

        Public Property VatCode() As String Implements [Interface].IDdRecord.VatCode
            Get
                If Not _VatCode Is Nothing Then
                    Return Me.FormattedValue(_VatCode, _recordStructure.VatCode)
                End If
            End Get
            Set(ByVal value As String)
                _VatCode = value
            End Set
        End Property

        Public Property VatSymbolUser() As String Implements [Interface].IDdRecord.VatSymbolUser
            Get
                If Not _VatSymbolUser Is Nothing Then
                    Return Me.FormattedValue(_VatSymbolUser, _recordStructure.VatSymbolUser)
                End If
            End Get
            Set(ByVal value As String)
                _VatSymbolUser = value
            End Set
        End Property

        Public Property VatValue() As String Implements [Interface].IDdRecord.VatValue
            Get
                If Not _VatValue Is Nothing Then
                    Me.FormatDecToString(CDec(_VatValue), strDllineFormatStringReturnValue, 11, Space(1), formatStringCharacter)
                    Return strDllineFormatStringReturnValue
                End If
            End Get
            Set(ByVal value As String)
                _VatValue = value
            End Set
        End Property

        Private Sub FormatDecToString(ByVal decValue As Decimal, ByRef stringValue As String, ByVal intPadDigit As Integer, ByVal strPadcharacter As String, Optional ByVal strWorkMask As String = "0")

            strStockAdjustmentSign = "+"
            If decValue < 0 Then
                decValue = decValue * -1
                strStockAdjustmentSign = "-"
            End If
            stringValue = decValue.ToString(strWorkMask).PadLeft(intPadDigit, CChar(strPadcharacter)) & strStockAdjustmentSign

        End Sub

        Public Property HierarchySpendLevelErosionCode() As String Implements [Interface].IDdRecord.HierarchySpendLevelErosionCode
            Get
                If Not _HierarchySpendLevelErosionCode Is Nothing Then
                    Return Me.FormattedValue(_HierarchySpendLevelErosionCode, _recordStructure.HierarchySpendLevelErosionCode)
                End If
            End Get
            Set(ByVal value As String)
                _HierarchySpendLevelErosionCode = value
            End Set
        End Property
    End Class

End Namespace