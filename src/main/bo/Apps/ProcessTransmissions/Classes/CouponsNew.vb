﻿Imports ProcessTransmissions.DataStructure.Implementation

Public Class CouponsNew
    Implements ICoupons

    Dim strEventNumber As String = String.Empty
    Dim strEventType As String = String.Empty
    Dim strEventKey1 As String = String.Empty
    Dim strEventKey2 As String = String.Empty
    Dim deleteIndicator As Boolean
    Dim boolCreateEventChange As Boolean
    Dim boolIsAMMGEventChange As Boolean
    Dim boolEventChangeDelete As Boolean
    Dim strEventChangeNumber As String = String.Empty
    Dim strEventChangeSkuNumber As String = String.Empty
    Dim decEventChangePrice As Decimal = 0
    Dim eventBuyQuantity As Decimal = 0
    Dim eventGetQuantity As Decimal = 0
    Dim eventDiscountAmount As Decimal = 0
    Dim eventDiscountPercentage As Decimal = 0
    Dim eventHeaderStartDate As Nullable(Of Date)
    Dim eventHeaderEndDate As Nullable(Of Date)
    Dim eventMasTimeOrDayRelated As Boolean
    Dim eventHeaderPriority As String = String.Empty
    Dim eventHeaderdt As DataTable
    Dim strBuyCoupon As String = String.Empty
    Dim strSellCoupon As String = String.Empty
    Dim couponsFactoryInstance As ICouponsRepository = CouponsRepositoryFactory.FactoryGet()

    Public Sub ProcessUCRecords(ByVal transmissionData As String) Implements ICoupons.ProcessUCRecords
        Dim ucRecord As New DataStructure.Implementation.UcRecord(transmissionData)
        InitialiseUCClassVariables(ucRecord)
        ProcessEventHeaders()
    End Sub

    Public Sub InitialiseUCClassVariables(ByVal ucRecord As UcRecord)
        InitialiseUCContentVariables(ucRecord)
        InitialiseUCHeaderVariables(ucRecord)
    End Sub

    Public Sub InitialiseUCContentVariables(ByVal ucRecord As UcRecord)

        strEventNumber = ucRecord.EventNumber
        strEventType = ucRecord.EventType
        strEventKey1 = ucRecord.ItemKey1
        strEventKey2 = ucRecord.ItemKey2
        strBuyCoupon = ucRecord.BuyCouponNumber
        strSellCoupon = ucRecord.GetCouponNumber
        If strBuyCoupon.Trim.Length = 0 Then
            strBuyCoupon = "0000000"
        End If
        If strSellCoupon.Trim.Length = 0 Then
            strSellCoupon = "0000000"
        End If
        eventBuyQuantity = CDec(ucRecord.BuyQuantity)
        eventGetQuantity = CDec(ucRecord.GetQuantity)
        eventDiscountAmount = CDec(ucRecord.DiscountValue)
        decEventChangePrice = CDec(ucRecord.Price)
        deleteIndicator = ExtractBooleanValue(ucRecord.DeleteIndicator)
        eventDiscountPercentage = CDec(ucRecord.GetQuantityDiscountPercentage)
    End Sub

    Public Sub InitialiseUCHeaderVariables(ByVal ucRecord As UcRecord)

        eventHeaderdt = couponsFactoryInstance.GetEventHeader(ucRecord.EventNumber)
        If Not eventHeaderdt Is Nothing Then

            CheckUCHeaderVariables()
        End If
        If EvntHeaderStartDate <> Date.MinValue Or EvntHeaderEndDate <> Date.MinValue Then
            eventMasTimeOrDayRelated = True
        End If
    End Sub

    Public Sub ProcessUMRecords(ByVal transmissionData As String) Implements ICoupons.ProcessUMRecords
        Dim umRecord As New DataStructure.Implementation.UmRecord(transmissionData)
        Dim couponsMasterStoreGenIndicator As Boolean = ExtractBooleanValue(umRecord.StoreGenIndicator)
        Dim couponsMasterExclusiveIndicator As Boolean = ExtractBooleanValue(umRecord.ExclusiveIndicator)
        Dim couponsMasterCollectCustomerInformationIndicator As Boolean = ExtractBooleanValue(umRecord.CollectCustomerInformationIndicator)
        Dim couponsSerialNumberIndicator As Boolean = ExtractBooleanValue(umRecord.SerialNumberIndicator)
        Dim couponsReusableIndicator As Boolean = ExtractBooleanValue(umRecord.ReusableIndicator)
        Dim couponsDeletedIndicator As Boolean = ExtractBooleanValue(umRecord.DeletedIndicator)


        couponsFactoryInstance.UpdateCouponMaster(umRecord.CouponNumber, umRecord.CouponNumberDescription, couponsMasterStoreGenIndicator _
                                                    , couponsSerialNumberIndicator, couponsMasterExclusiveIndicator, _
                                                        couponsReusableIndicator, couponsMasterCollectCustomerInformationIndicator, _
                                                         couponsDeletedIndicator)


    End Sub

    Public Sub ProcessUTRecords(ByVal transmissionData As String) Implements ICoupons.ProcessUTRecords
        Dim utRecord As New DataStructure.Implementation.UtRecord(transmissionData)
        Dim couponsTextIsDeleted As Boolean = ExtractBooleanValue(utRecord.Deleted)

        couponsFactoryInstance.UpdateCouponText(utRecord.CouponNumber, utRecord.TextLineDisplaySequence, utRecord.PrintSize, _
                                                  utRecord.Alignment, utRecord.Text, couponsTextIsDeleted)

    End Sub

#Region "Private Functions and Sub"



    Private Sub ProcessEventHeaders()

        If Not eventHeaderdt Is Nothing Then

            If eventHeaderdt.Rows.Count > 0 Then ' EVENT HEADER EXISTS?

                If StockMasterRecordExists() And EventKey2IsNotEmpty() Then
                    CreateEventChangeForPSRecords()
                    CreateEventChangeForPMRecords()
                    ProcessEventMasRecords()
                End If

            End If
        End If

    End Sub

    Private Function StockMasterRecordExists() As Boolean
        Dim stockMasterDataTable As DataTable
        If strEventType = "PS" Or strEventType = "TS" Or strEventType = "QS" Or strEventType = "MS" Then
            stockMasterDataTable = couponsFactoryInstance.GetStock(strEventKey1)
            If stockMasterDataTable Is Nothing Then
                Return False
            End If
            Return True
        End If
        Return True
    End Function

    Private Function EventKey2IsNotEmpty() As Boolean

        If strEventType = "PS" Or strEventType = "PM" Or strEventType = "TS" Or strEventType = "TM" Then
            'Key2 must be empty
            If strEventKey2 <> "00000000" Then Return False
        Else
            If strEventKey2 = "00000000" Then Return False
        End If
        Return True
    End Function

    Private Sub CreateEventChangeForPSRecords()
        strEventChangeNumber = EventNumber
        strEventChangeSkuNumber = strEventKey1
        If strEventType = "PS" Then
            If deleteIndicator Then boolEventChangeDelete = True
            boolCreateEventChange = True
            boolIsAMMGEventChange = False
        End If

    End Sub

    Private Sub CreateEventChangeForPMRecords()
        If strEventType = "PM" Then
            boolCreateEventChange = True
            boolIsAMMGEventChange = True
        End If
    End Sub

    Private Sub ProcessEventMasRecords()

        If Not CreateEventChange Then


            couponsFactoryInstance.UpdateEventMas(strEventType, strEventKey1, strEventKey2, EventNumber, eventHeaderPriority, _
                                                  deleteIndicator, eventMasTimeOrDayRelated, EvntHeaderStartDate, EvntHeaderEndDate, EventChangePrice, _
                                                  eventBuyQuantity, eventGetQuantity, eventDiscountAmount, eventDiscountPercentage, _
                                                  strBuyCoupon, strSellCoupon)
        End If
    End Sub

    Private Function ExtractBooleanValue(ByVal value As String) As Boolean
        If value = "Y" Then
            Return True
        Else
            Return False
        End If


    End Function

    Private Sub CheckUCHeaderVariables()

        If TypeOf (eventHeaderdt.Rows(0)(3)) Is Date Then
            eventHeaderStartDate = CDate(eventHeaderdt.Rows(0)(3))
        Else
            eventHeaderStartDate = Nothing
        End If

        If TypeOf (eventHeaderdt.Rows(0)(5)) Is Date Then
            eventHeaderEndDate = CDate(eventHeaderdt.Rows(0)(5))
        Else
            eventHeaderEndDate = Nothing
        End If

        eventHeaderPriority = eventHeaderdt.Rows(0)(2).ToString

    End Sub

#End Region

#Region "Properties"



    Public ReadOnly Property CreateEventChange() As Boolean Implements ICoupons.CreateEventChange
        Get
            Return boolCreateEventChange
        End Get
    End Property

    Public ReadOnly Property EventChangeDelete() As Boolean Implements ICoupons.EventChangeDelete
        Get
            Return boolEventChangeDelete
        End Get
    End Property

    Public ReadOnly Property EventChangeNumber() As String Implements ICoupons.EventChangeNumber
        Get
            Return strEventChangeNumber
        End Get
    End Property

    Public ReadOnly Property EventChangePrice() As Decimal Implements ICoupons.EventChangePrice
        Get
            Return decEventChangePrice
        End Get
    End Property

    Public ReadOnly Property EventChangeSkuNumber() As String Implements ICoupons.EventChangeSkuNumber
        Get
            Return strEventChangeSkuNumber
        End Get
    End Property

    Public ReadOnly Property EvntHeaderEndDate() As Nullable(Of Date) Implements ICoupons.EventHeaderEndDate
        Get
            Return eventHeaderEndDate
        End Get
    End Property

    Public ReadOnly Property EvntHeaderPriority() As String Implements ICoupons.EventHeaderPriority
        Get
            Return eventHeaderPriority
        End Get
    End Property

    Public ReadOnly Property EvntHeaderStartDate() As Nullable(Of Date) Implements ICoupons.EventHeaderStartDate
        Get
            Return eventHeaderStartDate
        End Get
    End Property

    Public ReadOnly Property EventNumber() As String Implements ICoupons.EventNumber
        Get
            Return strEventNumber
        End Get
    End Property

    Public ReadOnly Property MMGEventChange() As Boolean Implements ICoupons.MMGEventChange
        Get
            Return boolIsAMMGEventChange
        End Get
    End Property
#End Region

End Class


