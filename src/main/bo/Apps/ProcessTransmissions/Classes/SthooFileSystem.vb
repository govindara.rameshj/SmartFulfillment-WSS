﻿Namespace TpWickes

    Public Class SthooFileSystem
        Implements ISthooFileSystem

        Private _FileName As String

#Region "Properties"

        Public ReadOnly Property FileName() As String Implements ISthooFileSystem.FileName
            Get

                Return _FileName

            End Get
        End Property

#End Region

#Region "Constructors"

        Public Sub New()

        End Sub

        Public Sub New(ByVal FileName As String)

            _FileName = FileName

        End Sub

#End Region

#Region "Public Procedures And Functions"

        Public Sub WriteToFile(ByVal Line As String) Implements ISthooFileSystem.WriteToFile

            Dim Writer As New StreamWriter(_FileName, True)

            Writer.WriteLine(Line.Trim)
            Writer.Close()

        End Sub

#End Region

    End Class

End Namespace