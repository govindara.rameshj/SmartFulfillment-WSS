﻿Namespace TpWickes

    Public Class SthooFormatOutput
        Implements ISthooFormatOutput

        Private _StoreNumber As String
        Private _TodayDate As Date

#Region "Properties"

        Public Property StoreNumber() As String Implements ISthooFormatOutput.StoreNumber
            Get
                Return _StoreNumber
            End Get
            Set(ByVal value As String)
                _StoreNumber = value
            End Set
        End Property

        Public Property TodayDate() As Date Implements ISthooFormatOutput.TodayDate
            Get
                Return _TodayDate
            End Get
            Set(ByVal value As Date)
                _TodayDate = value
            End Set
        End Property

#End Region

#Region "Constructors"

        Public Sub New()

        End Sub

        Public Sub New(ByVal StoreNumber As String, ByVal TodayDate As Date)

            _StoreNumber = StoreNumber
            _TodayDate = TodayDate

        End Sub

#End Region

#Region "Public Procedures And Functions"

        Public Function FormatData(ByRef DR As System.Data.DataRow, ByVal DecimalFormatSpecifier As String, _
                                   ByVal HashPaddingLength As Integer, ByVal HashPaddingChar As Char) As String Implements ISthooFormatOutput.FormatData

            Dim PaddingChar As Char = CType("0", Char)

            Return "AL" & _
                   TpWickes.StringFormatting.FormatDateDDMMYY(Now.Date) & _
                   TpWickes.StringFormatting.HashingValue(CType(DR("OnOrderQuantity"), Decimal), DecimalFormatSpecifier, HashPaddingLength, HashPaddingChar, True, " ", "-") & _
                   TpWickes.StringFormatting.FormatDateDDMMYY(_TodayDate.Date) & _
                   TpWickes.StringFormatting.PaddingLeft(_StoreNumber, 3, PaddingChar) & _
                   TpWickes.StringFormatting.PaddingLeft(CType(DR("SkuNumber"), String), 6, PaddingChar) & _
                   TpWickes.StringFormatting.PaddingLeft(CType(DR("PrimarySupplierNumber"), String), 5, PaddingChar) & _
                   TpWickes.StringFormatting.PaddingLeft(CType(DR("OnOrderQuantity"), String), 6, PaddingChar)

        End Function

#End Region

    End Class

End Namespace