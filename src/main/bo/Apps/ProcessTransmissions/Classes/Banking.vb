﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ProcessTransmissions.UnitTest")> 
Public Class BankingCurrentNew
    Implements IBanking

    'NOTE
    '
    'Would like to test the friend function FloatVariance
    '
    'This requires the ability to configure the "requirement switch" via the stub implementation in TpWickes.Library.dll
    'Unfortunately that assembly is strongly signed
    'This will result in making this unit test project strongly signed 
    'This will result in making all references strongly signed including ProcessTransmission.exe
    'ProcessTransmission has a LOT of references!!!!
    '
    'If signing was not an issue would do the following tests
    '
    'A. Old implementation with NULL float variances will result in an exception
    '
    'B. New implementation with NULL float variances will result in zero being returned

    Public Function UpdateFloatVariance(ByRef DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal PeriodID As Integer, ByVal CurrencyID As String) As Decimal Implements IBanking.UpdateFloatVariance

        Dim DS As System.Data.DataSet

        DS = DB.ExecuteSql("select FloatVariance = dbo.NewBankingTotalFloatVariance(" & PeriodID.ToString.Trim & ", '" & CurrencyID & "')")

        'Return CType(DS.Tables(0).Rows(0).Item("FloatVariance"), Decimal)
        Return FloatVariance(DS)

    End Function

#Region "Private, Friend Procedures And Functions"

    Friend Function FloatVariance(ByRef DS As DataSet) As Decimal
        Dim Float As IBankingFloat

        Float = (New BankingFloatFactory).GetImplementation

        Return Float.FloatVariance(DS).Value

    End Function

#End Region

End Class