﻿Imports System.Globalization

Public Class DateAdjusted
    Implements IDateAdjuster

    Private dateAdjusterRepositoryInstance As New DateAdjustRepository
    Private Const dateAdjusterParameterId = 6005

    Friend Function AdjustedDate(ByVal dateIn As Date) As Date
        Return dateIn.AddDays(GetDaysToAdjustBy)
    End Function

    Friend Overridable Function GetDaysToAdjustBy() As Integer
        Dim daysToBeAdjusted As DataTable = dateAdjusterRepositoryInstance.GetDateToBeAdjusted(dateAdjusterParameterId)
        If Not daysToBeAdjusted Is Nothing Then
            Return CInt(daysToBeAdjusted.Rows(0)(0))
        End If
    End Function

    Public Function GetAdjustedDate(ByVal input As String, ByVal formatType As String, ByVal startPosition As Integer, ByVal dateLength As Integer) As String Implements IDateAdjuster.GetAdjustedDate

        If Not (input.Substring(startPosition, dateLength).Trim = String.Empty _
                Or input.Substring(startPosition, dateLength).Trim = "/  /" Or _
                input.Substring(startPosition, dateLength).Trim = "00000000") Then
            input = input.Remove(startPosition, dateLength).Insert(startPosition, AdjustedDate(Date.ParseExact(input.Substring(startPosition, dateLength), formatType, CultureInfo.InvariantCulture)).ToString(formatType))
        End If
        Return input

    End Function
End Class
