﻿Public Class SthoaAdRecordNew
    Implements ISthoaAdRecord

    Private strStockAdjustmentSign As String = "+"

    Public Sub FormatDecToString(ByVal decValue As Decimal, ByRef stringValue As String, ByVal intPadDigit As Integer, ByVal strPadcharacter As String, ByVal strWorkMask As String) Implements ISthoaAdRecord.FormatDecToString

        strStockAdjustmentSign = "+"
        If decValue < 0 Then
            decValue = decValue * -1
            strStockAdjustmentSign = "-"
        End If
        stringValue = decValue.ToString(strWorkMask).PadLeft(intPadDigit, CChar(strPadcharacter)) & strStockAdjustmentSign

    End Sub
End Class
