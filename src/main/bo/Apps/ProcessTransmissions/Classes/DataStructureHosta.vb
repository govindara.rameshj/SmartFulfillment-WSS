﻿Imports ProcessTransmissions.DataStructureHosta.Implementation

Namespace DataStructureHosta.Interface
    Public Interface IFieldStructure
        Property StartPosition() As Int16
        Property Length() As Int16
    End Interface
    Public Interface IHeaderStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileType() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property FileDescription() As [Interface].IFieldStructure
    End Interface
    Public Interface ITrailerStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileType() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure
    End Interface
    Public Interface ITrailer
        ReadOnly Property RecordType() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property FileType() As String
        ReadOnly Property VersionNumber() As String
        ReadOnly Property SequenceNumber() As String
        ReadOnly Property Time() As String
        ReadOnly Property Bucket1RecordType() As String
        ReadOnly Property Bucket1RecordCount() As String
        ReadOnly Property Bucket1RecordHashValue() As String
    End Interface
    Public Interface IHeader
        ReadOnly Property RecordType() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property FileType() As String
        ReadOnly Property VersionNumber() As String
        ReadOnly Property SequenceNumber() As String
        ReadOnly Property Time() As String
        ReadOnly Property FileDescription() As String
    End Interface
    Public Interface ICwRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property ColleagueCardNumber() As [Interface].IFieldStructure
        ReadOnly Property PaddingCharacter() As [Interface].IFieldStructure
        ReadOnly Property CollegueName() As [Interface].IFieldStructure
        ReadOnly Property AddressLine1() As [Interface].IFieldStructure
        ReadOnly Property AddressLine2() As [Interface].IFieldStructure
        ReadOnly Property AddressLine3() As [Interface].IFieldStructure
        ReadOnly Property AddressLine4() As [Interface].IFieldStructure
        ReadOnly Property PostCode() As [Interface].IFieldStructure
        ReadOnly Property HideAddress() As [Interface].IFieldStructure
    End Interface
    Public Interface ICwRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property ColleagueCardNumber() As String
        ReadOnly Property PaddingCharacter() As String
        ReadOnly Property CollegueName() As String
        ReadOnly Property AddressLine1() As String
        ReadOnly Property AddressLine2() As String
        ReadOnly Property AddressLine3() As String
        ReadOnly Property AddressLine4() As String
        ReadOnly Property PostCode() As String
        ReadOnly Property HideAddress() As String
    End Interface
End Namespace

Namespace DataStructureHosta
    Public Structure HostaHeaderStructure
        Implements [Interface].IHeaderStructure

        Private _hostaHeaderType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileTypeStart As Int16 = 14
        Private Const FileTypeLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const FileDescriptionStart As Int16 = 33
        Private Const FileDescriptionLength As Int16 = 10

#End Region
#Region "Properties"


        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property FileDescription() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.FileDescription
            Get
                Return New Field(FileDescriptionStart, FileDescriptionLength)
            End Get
        End Property

        Public ReadOnly Property FileType() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.FileType
            Get
                Return New Field(FileTypeStart, FileTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberStart, SequenceNumberLength)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.StoreNumber
            Get
                Return New Field(StoreNumberStart, StoreNumberLength)
            End Get
        End Property

        Public ReadOnly Property Time() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Time
            Get
                Return New Field(TimeStart, TimeLength)
            End Get
        End Property

        Public ReadOnly Property VersionNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.VersionNumber
            Get
                Return New Field(VersionNumberStart, VersionNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostaTrailerStructure
        Implements [Interface].ITrailerStructure

        Private _hostaTrailerType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileTypeStart As Int16 = 14
        Private Const FileTypeLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const Bucket1RecordTypeStart As Int16 = 33
        Private Const Bucket1RecordTypeLength As Int16 = 2
        Private Const Bucket1RecordCountStart As Int16 = 35
        Private Const Bucket1RecordCountLength As Int16 = 7
        Private Const Bucket1RecordHashValueStart As Int16 = 42
        Private Const Bucket1RecordHashValueLength As Int16 = 12

#End Region
#Region "Properties"

        Public ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordCount
            Get
                Return New Field(Bucket1RecordCountStart, Bucket1RecordCountLength)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordHashValue
            Get
                Return New Field(Bucket1RecordHashValueStart, Bucket1RecordHashValueLength)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordType
            Get
                Return New Field(Bucket1RecordTypeStart, Bucket1RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property FileType1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.FileType
            Get
                Return New Field(FileTypeStart, FileTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberStart, SequenceNumberLength)
            End Get
        End Property

        Public ReadOnly Property StoreNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.StoreNumber
            Get
                Return New Field(StoreNumberStart, StoreNumberLength)
            End Get
        End Property

        Public ReadOnly Property Time1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Time
            Get
                Return New Field(TimeStart, TimeLength)
            End Get
        End Property

        Public ReadOnly Property VersionNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.VersionNumber
            Get
                Return New Field(VersionNumberStart, VersionNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostaCwRecordStructure
        Implements [Interface].ICwRecordStructure

        Private _hostaCwRecordType As String
#Region "Constants"

        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const ColleagueCardNumberStart As Int16 = 23
        Private Const ColleagueCardNumberLength As Int16 = 16
        Private Const PaddingCharacterStart As Int16 = 39
        Private Const PaddingCharacterLength As Int16 = 1
        Private Const ColleagueNameStart As Int16 = 40
        Private Const ColleagueNameLength As Int16 = 40
        Private Const AddressLine1Start As Int16 = 80
        Private Const AddressLine1Length As Int16 = 40
        Private Const AddressLine2Start As Int16 = 120
        Private Const AddressLine2Length As Int16 = 40
        Private Const AddressLine3Start As Int16 = 160
        Private Const AddressLine3Length As Int16 = 40
        Private Const AddressLine4Start As Int16 = 200
        Private Const AddressLine4Length As Int16 = 40
        Private Const PostCodeStart As Int16 = 240
        Private Const PostCodeLength As Int16 = 10
        Private Const HideAddressStart As Int16 = 250
        Private Const HideAddressLength As Int16 = 1
#End Region
#Region "Properties"


        Public ReadOnly Property AddressLine1() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.AddressLine1
            Get
                Return New Field(AddressLine1Start, AddressLine1Length)
            End Get
        End Property

        Public ReadOnly Property AddressLine2() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.AddressLine2
            Get
                Return New Field(AddressLine2Start, AddressLine2Length)
            End Get
        End Property

        Public ReadOnly Property AddressLine3() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.AddressLine3
            Get
                Return New Field(AddressLine3Start, AddressLine3Length)
            End Get
        End Property

        Public ReadOnly Property AddressLine4() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.AddressLine4
            Get
                Return New Field(AddressLine4Start, AddressLine4Length)
            End Get
        End Property

        Public ReadOnly Property ColleagueCardNumber() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.ColleagueCardNumber
            Get
                Return New Field(ColleagueCardNumberStart, ColleagueCardNumberLength)
            End Get
        End Property

        Public ReadOnly Property CollegueName() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.CollegueName
            Get
                Return New Field(ColleagueNameStart, ColleagueNameLength)
            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property HideAddress() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.HideAddress
            Get
                Return New Field(HideAddressStart, HideAddressLength)
            End Get
        End Property

        Public ReadOnly Property PaddingCharacter() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.PaddingCharacter
            Get
                Return New Field(PaddingCharacterStart, PaddingCharacterLength)
            End Get
        End Property

        Public ReadOnly Property PostCode() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.PostCode
            Get
                Return New Field(PostCodeStart, PostCodeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].ICwRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region
    End Structure

End Namespace

Namespace DataStructureHosta.Implementation
    Public Structure Field
        Implements DataStructureHosta.Interface.IFieldStructure

        Private _name As String
        Private _startPosition As Int16
        Private _length As Int16
        Friend Sub New(ByVal startPosition As Int16, ByVal length As Int16)
            Me._startPosition = startPosition
            Me._length = length
        End Sub
        Public Property Length() As Short Implements DataStructureHosta.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property StartPosition() As Short Implements DataStructureHosta.Interface.IFieldStructure.StartPosition
            Get
                Return _startPosition
            End Get
            Set(ByVal value As Short)
                _startPosition = value
            End Set
        End Property
    End Structure
    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = ""
            If RecordIsLongEnoughToContainField(record, field) Then
                val = record.Substring(field.StartPosition - 1, field.Length)
            Else
                val = ""
            End If
            Return val
        End Function

        Public Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As [Interface].IFieldStructure) As Boolean
            Return record.Length >= field.StartPosition + field.Length - 1
        End Function

        'Private Function RecordIsLongEnoughToextractField(ByVal field As Field) As Integer
        '    Return
        'End Function
    End Class
    Public Class HostaRecordHeader
        Inherits RecordFormat
        Implements [Interface].IHeader


        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostaHeaderStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostaHeaderStructure
        End Sub
#Region "Properties"


        Public ReadOnly Property [Date]() As String Implements [Interface].IHeader.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property FileDescription() As String Implements [Interface].IHeader.FileDescription
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileDescription)
            End Get
        End Property

        Public ReadOnly Property FileType() As String Implements [Interface].IHeader.FileType
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileType)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IHeader.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As String Implements [Interface].IHeader.SequenceNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SequenceNumber)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As String Implements [Interface].IHeader.StoreNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreNumber)
            End Get
        End Property

        Public ReadOnly Property Time() As String Implements [Interface].IHeader.Time
            Get
                Return Me.FormattedValue(_record, _recordStructure.Time)
            End Get
        End Property

        Public ReadOnly Property VersionNumber() As String Implements [Interface].IHeader.VersionNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.VersionNumber)
            End Get
        End Property
#End Region
    End Class
    Public Class HostaRecordTrailer
        Inherits RecordFormat
        Implements [Interface].ITrailer

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostaTrailerStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostaTrailerStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property Bucket1RecordCount() As String Implements [Interface].ITrailer.Bucket1RecordCount
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordCount)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordHashValue() As String Implements [Interface].ITrailer.Bucket1RecordHashValue
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordHashValue)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordType() As String Implements [Interface].ITrailer.Bucket1RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordType)
            End Get
        End Property

        Public ReadOnly Property Date1() As String Implements [Interface].ITrailer.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property FileType1() As String Implements [Interface].ITrailer.FileType
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileType1)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].ITrailer.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber1() As String Implements [Interface].ITrailer.SequenceNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SequenceNumber1)
            End Get
        End Property

        Public ReadOnly Property StoreNumber1() As String Implements [Interface].ITrailer.StoreNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreNumber1)
            End Get
        End Property

        Public ReadOnly Property Time1() As String Implements [Interface].ITrailer.Time
            Get
                Return Me.FormattedValue(_record, _recordStructure.Time1)
            End Get
        End Property

        Public ReadOnly Property VersionNumber1() As String Implements [Interface].ITrailer.VersionNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.VersionNumber1)
            End Get
        End Property
#End Region
    End Class
    Public Class HostaCwRecord
        Inherits RecordFormat
        Implements [Interface].ICwRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostaCwRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostaCwRecordStructure
        End Sub
#Region "Properties"


        Public ReadOnly Property AddressLine1() As String Implements [Interface].ICwRecord.AddressLine1
            Get
                Return Me.FormattedValue(_record, _recordStructure.AddressLine1)
            End Get
        End Property

        Public ReadOnly Property AddressLine2() As String Implements [Interface].ICwRecord.AddressLine2
            Get
                Return Me.FormattedValue(_record, _recordStructure.AddressLine2)
            End Get
        End Property

        Public ReadOnly Property AddressLine3() As String Implements [Interface].ICwRecord.AddressLine3
            Get
                Return Me.FormattedValue(_record, _recordStructure.AddressLine3)
            End Get
        End Property

        Public ReadOnly Property AddressLine4() As String Implements [Interface].ICwRecord.AddressLine4
            Get
                Return Me.FormattedValue(_record, _recordStructure.AddressLine4)
            End Get
        End Property

        Public ReadOnly Property ColleagueCardNumber() As String Implements [Interface].ICwRecord.ColleagueCardNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.ColleagueCardNumber)
            End Get
        End Property

        Public ReadOnly Property CollegueName() As String Implements [Interface].ICwRecord.CollegueName
            Get
                Return Me.FormattedValue(_record, _recordStructure.CollegueName)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].ICwRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].ICwRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property HideAddress() As String Implements [Interface].ICwRecord.HideAddress
            Get
                Return Me.FormattedValue(_record, _recordStructure.HideAddress)
            End Get
        End Property

        Public ReadOnly Property PaddingCharacter() As String Implements [Interface].ICwRecord.PaddingCharacter
            Get
                Return Me.FormattedValue(_record, _recordStructure.PaddingCharacter)
            End Get
        End Property

        Public ReadOnly Property PostCode() As String Implements [Interface].ICwRecord.PostCode
            Get
                Return Me.FormattedValue(_record, _recordStructure.PostCode)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].ICwRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property
#End Region
    End Class
   
End Namespace


