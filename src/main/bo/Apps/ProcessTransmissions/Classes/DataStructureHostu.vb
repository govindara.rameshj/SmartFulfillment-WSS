﻿Imports ProcessTransmissions.DataStructureHostu.Implementation

Namespace DataStructureHostu.Interface
    Public Interface IFieldStructure
        Property StartPosition() As Int16
        Property Length() As Int16
    End Interface
    Public Interface IU1RecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property RecordTypeNumber() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property CheckDigit() As [Interface].IFieldStructure
        ReadOnly Property RetailSellingPrice() As [Interface].IFieldStructure
        ReadOnly Property PriceEffectiveDate() As [Interface].IFieldStructure
        ReadOnly Property PriorPrice() As [Interface].IFieldStructure
        ReadOnly Property StatusCode() As [Interface].IFieldStructure
        ReadOnly Property Filler() As [Interface].IFieldStructure
        ReadOnly Property SupplierCode() As [Interface].IFieldStructure
        ReadOnly Property SupplierItemNumber() As [Interface].IFieldStructure
        ReadOnly Property SupplierPackaged() As [Interface].IFieldStructure
        ReadOnly Property SupplierUnitOfMeasureCode() As [Interface].IFieldStructure
        ReadOnly Property Filler1() As [Interface].IFieldStructure
        ReadOnly Property SellingUnitOfMeasure() As [Interface].IFieldStructure
        ReadOnly Property ProductGroupCode() As [Interface].IFieldStructure
        ReadOnly Property PackItemNumber() As [Interface].IFieldStructure
        ReadOnly Property StoreMaintained() As [Interface].IFieldStructure
        ReadOnly Property FullItemDescription() As [Interface].IFieldStructure
        ReadOnly Property ShortDescriptionPosLookup() As [Interface].IFieldStructure
        ReadOnly Property PriceBookSequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property VatCode() As [Interface].IFieldStructure
        ReadOnly Property SystemUpdateDate() As [Interface].IFieldStructure
        ReadOnly Property SinglesPerPack() As [Interface].IFieldStructure
        ReadOnly Property ForecastFactor() As [Interface].IFieldStructure
        ReadOnly Property Cost() As [Interface].IFieldStructure
        ReadOnly Property EanNumberFirstEightDigits() As [Interface].IFieldStructure
        ReadOnly Property EanNumberLastFiveDigits() As [Interface].IFieldStructure
        ReadOnly Property Weight() As [Interface].IFieldStructure
        ReadOnly Property Volume() As [Interface].IFieldStructure
        ReadOnly Property NoOrderFlag() As [Interface].IFieldStructure
        ReadOnly Property DisplayFactor() As [Interface].IFieldStructure
        ReadOnly Property ShelfCapacity() As [Interface].IFieldStructure
        ReadOnly Property AlternateSupplierNumber() As [Interface].IFieldStructure
        ReadOnly Property SeasonalityPatternNumber() As [Interface].IFieldStructure
        ReadOnly Property BankHolidayPatternNumber() As [Interface].IFieldStructure
        ReadOnly Property PromotionalPatternNumber() As [Interface].IFieldStructure
        ReadOnly Property NonStockIndicator() As [Interface].IFieldStructure
        ReadOnly Property ParticipateInMcp() As [Interface].IFieldStructure
        ReadOnly Property SoqServiceLevel() As [Interface].IFieldStructure
        ReadOnly Property InitialOrderDate() As [Interface].IFieldStructure
        ReadOnly Property FinalOrderDate() As [Interface].IFieldStructure
        ReadOnly Property PromotionalMinimumStock() As [Interface].IFieldStructure
        ReadOnly Property PromotionalMinimumStartDate() As [Interface].IFieldStructure
        ReadOnly Property PromotionalMinimumEndDate() As [Interface].IFieldStructure
        ReadOnly Property TaggedItemIndicator() As [Interface].IFieldStructure
        ReadOnly Property CategoryNumber() As [Interface].IFieldStructure
        ReadOnly Property GroupNumber() As [Interface].IFieldStructure
        ReadOnly Property SubGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property StyleNumber() As [Interface].IFieldStructure
        ReadOnly Property RetailPriceEventNumber() As [Interface].IFieldStructure
        ReadOnly Property RetailPriceEventPriority() As [Interface].IFieldStructure
        ReadOnly Property WarrantyItem() As [Interface].IFieldStructure
        ReadOnly Property OffensiveWeaponAgeRestriciton() As [Interface].IFieldStructure
        ReadOnly Property SolventUserAgeRestriction() As [Interface].IFieldStructure
        ReadOnly Property QuarantineIndicator() As [Interface].IFieldStructure
        ReadOnly Property PriceDiscrepencyInPlace() As [Interface].IFieldStructure
        ReadOnly Property EquivalentPriceMultiplier() As [Interface].IFieldStructure
        ReadOnly Property EquivalentPriceUnit() As [Interface].IFieldStructure
        ReadOnly Property SaleTypeAttribute() As [Interface].IFieldStructure
        ReadOnly Property ModelTypeAttribute() As [Interface].IFieldStructure
        ReadOnly Property AutoApplyPriceChanges() As [Interface].IFieldStructure
        ReadOnly Property PallatedSku() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentSetter() As [Interface].IFieldStructure
        ReadOnly Property InsulationItem() As [Interface].IFieldStructure
        ReadOnly Property TimberContent() As [Interface].IFieldStructure
        ReadOnly Property ElectricalItem() As [Interface].IFieldStructure
        ReadOnly Property StockOffSale() As [Interface].IFieldStructure
        ReadOnly Property BallastItem() As [Interface].IFieldStructure
        ReadOnly Property ProducersRecyclingFundSku() As [Interface].IFieldStructure
    End Interface
    Public Interface IU5RecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property RecordTypeNumber() As [Interface].IFieldStructure
        ReadOnly Property InformationType() As [Interface].IFieldStructure
        ReadOnly Property DeletionIndicator() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property TextLineNumber() As [Interface].IFieldStructure
        ReadOnly Property Text() As [Interface].IFieldStructure
    End Interface
    Public Interface IU6RecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property ProductEquivalence() As [Interface].IFieldStructure

    End Interface
    Public Interface IUaRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property MixAndMatchGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property DeletionIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUbRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property EventNumber() As [Interface].IFieldStructure
        ReadOnly Property EventDescription() As [Interface].IFieldStructure
        ReadOnly Property EventPriority() As [Interface].IFieldStructure
        ReadOnly Property StartDate() As [Interface].IFieldStructure
        ReadOnly Property DailyStartTime() As [Interface].IFieldStructure
        ReadOnly Property EndDate() As [Interface].IFieldStructure
        ReadOnly Property DailyEndTime() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnMondays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnTuesdays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnWednesdays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnThursdays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnFridays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnSaturdays() As [Interface].IFieldStructure
        ReadOnly Property ActiveOnSundays() As [Interface].IFieldStructure
        ReadOnly Property DeletionIndicator() As [Interface].IFieldStructure
    End Interface

    Public Interface IUcRecordStructure
        Inherits [Interface].IUcRecordCouponStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property EventNumber() As [Interface].IFieldStructure
        ReadOnly Property EventType() As [Interface].IFieldStructure
        ReadOnly Property ItemKey1() As [Interface].IFieldStructure
        ReadOnly Property ItemKey2() As [Interface].IFieldStructure
        ReadOnly Property Price() As [Interface].IFieldStructure
        ReadOnly Property BuyQuantity() As [Interface].IFieldStructure
        ReadOnly Property GetQuantity() As [Interface].IFieldStructure
        ReadOnly Property DiscountValue() As [Interface].IFieldStructure
        ReadOnly Property GetQuantityDiscountPercentage() As [Interface].IFieldStructure
        ReadOnly Property DeleteIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUcRecordCouponStructure
        ReadOnly Property BuyCouponNumber() As [Interface].IFieldStructure
        ReadOnly Property GetCouponNumber() As [Interface].IFieldStructure
    End Interface
    Public Interface IUdRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property DealGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property MixAndMatchGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property Quantity() As [Interface].IFieldStructure
        ReadOnly Property DiscountValue() As [Interface].IFieldStructure
        ReadOnly Property DiscountPercentage() As [Interface].IFieldStructure
        ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUeRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property EventNumber() As [Interface].IFieldStructure
        ReadOnly Property CategoryOrGroupOrSubGroupOrStyle() As [Interface].IFieldStructure
        ReadOnly Property ExcludedSkuNumber() As [Interface].IFieldStructure
        ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUmRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property CouponNumber() As [Interface].IFieldStructure
        ReadOnly Property CouponDescription() As [Interface].IFieldStructure
        ReadOnly Property StoreGenIndicator() As [Interface].IFieldStructure
        ReadOnly Property SerialNumberIndicator() As [Interface].IFieldStructure
        ReadOnly Property ExclusiveIndicator() As [Interface].IFieldStructure
        ReadOnly Property ReusableIndicator() As [Interface].IFieldStructure
        ReadOnly Property CollectCustomerInformationIndicator() As [Interface].IFieldStructure
        ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUtRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property CouponNumber() As [Interface].IFieldStructure
        ReadOnly Property TextLineDisplaySequence() As [Interface].IFieldStructure
        ReadOnly Property PrintSize() As [Interface].IFieldStructure
        ReadOnly Property Alignment() As [Interface].IFieldStructure
        ReadOnly Property Text() As [Interface].IFieldStructure
        ReadOnly Property Deleted() As [Interface].IFieldStructure
    End Interface

    Public Interface IU1Record
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property RecordTypeNumber() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property CheckDigit() As String
        ReadOnly Property RetailSellingPrice() As String
        ReadOnly Property PriceEffectiveDate() As String
        ReadOnly Property PriorPrice() As String
        ReadOnly Property StatusCode() As String
        ReadOnly Property Filler() As String
        ReadOnly Property SupplierCode() As String
        ReadOnly Property SupplierItemNumber() As String
        ReadOnly Property SupplierPackaged() As String
        ReadOnly Property SupplierUnitOfMeasureCode() As String
        ReadOnly Property Filler1() As String
        ReadOnly Property SellingUnitOfMeasure() As String
        ReadOnly Property ProductGroupCode() As String
        ReadOnly Property PackItemNumber() As String
        ReadOnly Property StoreMaintained() As String
        ReadOnly Property FullItemDescription() As String
        ReadOnly Property ShortDescriptionPosLookup() As String
        ReadOnly Property PriceBookSequenceNumber() As String
        ReadOnly Property VatCode() As String
        ReadOnly Property SystemUpdateDate() As String
        ReadOnly Property SinglesPerPack() As String
        ReadOnly Property ForecastFactor() As String
        ReadOnly Property Cost() As String
        ReadOnly Property EanNumberFirstEightDigits() As String
        ReadOnly Property EanNumberLastFiveDigits() As String
        ReadOnly Property Weight() As String
        ReadOnly Property Volume() As String
        ReadOnly Property NoOrderFlag() As String
        ReadOnly Property DisplayFactor() As String
        ReadOnly Property ShelfCapacity() As String
        ReadOnly Property AlternateSupplierNumber() As String
        ReadOnly Property SeasonalityPatternNumber() As String
        ReadOnly Property BankHolidayPatternNumber() As String
        ReadOnly Property PromotionalPatternNumber() As String
        ReadOnly Property NonStockIndicator() As String
        ReadOnly Property ParticipateInMcp() As String
        ReadOnly Property SoqServiceLevel() As String
        ReadOnly Property InitialOrderDate() As String
        ReadOnly Property FinalOrderDate() As String
        ReadOnly Property PromotionalMinimumStock() As String
        ReadOnly Property PromotionalMinimumStartDate() As String
        ReadOnly Property PromotionalMinimumEndDate() As String
        ReadOnly Property TaggedItemIndicator() As String
        ReadOnly Property CategoryNumber() As String
        ReadOnly Property GroupNumber() As String
        ReadOnly Property SubGroupNumber() As String
        ReadOnly Property StyleNumber() As String
        ReadOnly Property RetailPriceEventNumber() As String
        ReadOnly Property RetailPriceEventPriority() As String
        ReadOnly Property WarrantyItem() As String
        ReadOnly Property OffensiveWeaponAgeRestriciton() As String
        ReadOnly Property SolventUserAgeRestriction() As String
        ReadOnly Property QuarantineIndicator() As String
        ReadOnly Property PriceDiscrepencyInPlace() As String
        ReadOnly Property EquivalentPriceMultiplier() As String
        ReadOnly Property EquivalentPriceUnit() As String
        ReadOnly Property SaleTypeAttribute() As String
        ReadOnly Property ModelTypeAttribute() As String
        ReadOnly Property AutoApplyPriceChanges() As String
        ReadOnly Property PallatedSku() As String
        ReadOnly Property AdjustmentSetter() As String
        ReadOnly Property InsulationItem() As String
        ReadOnly Property TimberContent() As String
        ReadOnly Property ElectricalItem() As String
        ReadOnly Property StockOffSale() As String
        ReadOnly Property BallastItem() As String
        ReadOnly Property ProducersRecyclingFundSku() As String
    End Interface
    Public Interface IU5Record
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property RecordTypeNumber() As String
        ReadOnly Property InformationType() As String
        ReadOnly Property DeletionIndicator() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property TextLineNumber() As String
        ReadOnly Property Text() As String
    End Interface
    Public Interface IU6Record
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property ProductEquivalence() As String
    End Interface
    Public Interface IUaRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property MixAndMatchGroupNumber() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property DeletionIndicator() As String
    End Interface
    Public Interface IUbRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property EventNumber() As String
        ReadOnly Property EventDescription() As String
        ReadOnly Property EventPriority() As String
        ReadOnly Property StartDate() As String
        ReadOnly Property DailyStartTime() As String
        ReadOnly Property EndDate() As String
        ReadOnly Property DailyEndTime() As String
        ReadOnly Property ActiveOnMondays() As String
        ReadOnly Property ActiveOnTuesdays() As String
        ReadOnly Property ActiveOnWednesdays() As String
        ReadOnly Property ActiveOnThursdays() As String
        ReadOnly Property ActiveOnFridays() As String
        ReadOnly Property ActiveOnSaturdays() As String
        ReadOnly Property ActiveOnSundays() As String
        ReadOnly Property DeletionIndicator() As String
    End Interface
    Public Interface IUdRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property DealGroupNumber() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property MixAndMatchGroupNumber() As String
        ReadOnly Property Quantity() As String
        ReadOnly Property DiscountValue() As String
        ReadOnly Property DiscountPercentage() As String
        ReadOnly Property DeletedIndicator() As String
    End Interface
    Public Interface IUeRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property EventNumber() As String
        ReadOnly Property CategoryOrGroupOrSubGroupOrStyle() As String
        ReadOnly Property ExcludedSkuNumber() As String
        ReadOnly Property DeletedIndicator() As String
    End Interface
    Public Interface IUcRecord
        Inherits [Interface].IUcRecordCoupon
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property EventNumber() As String
        ReadOnly Property EventType() As String
        ReadOnly Property ItemKey1() As String
        ReadOnly Property ItemKey2() As String
        ReadOnly Property Price() As String
        ReadOnly Property BuyQuantity() As String
        ReadOnly Property GetQuantity() As String
        ReadOnly Property DiscountValue() As String
        ReadOnly Property GetQuantityDiscountPercentage() As String
        ReadOnly Property DeleteIndicator() As String
    End Interface
    Public Interface IUcRecordCoupon
        ReadOnly Property BuyCouponNumber() As String
        ReadOnly Property GetCouponNumber() As String
    End Interface
    Public Interface IUmRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property CouponNumber() As String
        ReadOnly Property CouponDescription() As String
        ReadOnly Property StoreGenIndicator() As String
        ReadOnly Property SerialNumberIndicator() As String
        ReadOnly Property ExclusiveIndicator() As String
        ReadOnly Property ReusableIndicator() As String
        ReadOnly Property CollectCustomerInformationIndicator() As String
        ReadOnly Property DeletedIndicator() As String
    End Interface
    Public Interface IUtRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property CouponNumber() As String
        ReadOnly Property TextLineDisplaySequence() As String
        ReadOnly Property PrintSize() As String
        ReadOnly Property Alignment() As String
        ReadOnly Property Text() As String
        ReadOnly Property Deleted() As String
    End Interface


End Namespace

Namespace DataStructureHostu
    Public Structure U1RecordStructure
        Implements [Interface].IU1RecordStructure


        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const EventTypeStart As Int16 = 29
        Private Const EventTypeLength As Int16 = 2
        Private Const ItemKey1Start As Int16 = 31
        Private Const ItemKey1Length As Int16 = 6
        Private Const ItemKey2Start As Int16 = 37
        Private Const ItemKey2Length As Int16 = 8
        Private Const PriceStart As Int16 = 45
        Private Const PriceLength As Int16 = 10
        Private Const BuyQuantityStart As Int16 = 55
        Private Const BuyQuantityLength As Int16 = 7
        Private Const GetQuantityStart As Int16 = 62
        Private Const GetQuantityLength As Int16 = 7
        Private Const DiscountValueStart As Int16 = 69
        Private Const DiscountValueLength As Int16 = 10
        Private Const GetQuantityDiscountPercentageStart As Int16 = 79
        Private Const GetQuantityDiscountPercentageLength As Int16 = 7
        Private Const DeleteIndicatorStart As Int16 = 86
        Private Const DeleteIndicatorLength As Int16 = 1
        Private Const BuyCouponNumberStart As Int16 = 87
        Private Const BuyCouponNumberLength As Int16 = 7
        Private Const GetCouponNumberStart As Int16 = 94
        Private Const GetCouponNumberLength As Int16 = 7
#End Region
#Region "Properties"



        Public ReadOnly Property AdjustmentSetter() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.AdjustmentSetter
            Get

            End Get
        End Property

        Public ReadOnly Property AlternateSupplierNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.AlternateSupplierNumber
            Get

            End Get
        End Property

        Public ReadOnly Property AutoApplyPriceChanges() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.AutoApplyPriceChanges
            Get

            End Get
        End Property

        Public ReadOnly Property BallastItem() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.BallastItem
            Get

            End Get
        End Property

        Public ReadOnly Property BankHolidayPatternNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.BankHolidayPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property CategoryNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.CategoryNumber
            Get

            End Get
        End Property

        Public ReadOnly Property CheckDigit() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.CheckDigit
            Get

            End Get
        End Property

        Public ReadOnly Property Cost() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Cost
            Get

            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Date
            Get

            End Get
        End Property

        Public ReadOnly Property DisplayFactor() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.DisplayFactor
            Get

            End Get
        End Property

        Public ReadOnly Property EanNumberFirstEightDigits() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.EanNumberFirstEightDigits
            Get

            End Get
        End Property

        Public ReadOnly Property EanNumberLastFiveDigits() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.EanNumberLastFiveDigits
            Get

            End Get
        End Property

        Public ReadOnly Property ElectricalItem() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ElectricalItem
            Get

            End Get
        End Property

        Public ReadOnly Property EquivalentPriceMultiplier() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.EquivalentPriceMultiplier
            Get

            End Get
        End Property

        Public ReadOnly Property EquivalentPriceUnit() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.EquivalentPriceUnit
            Get

            End Get
        End Property

        Public ReadOnly Property Filler() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Filler
            Get

            End Get
        End Property

        Public ReadOnly Property Filler1() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Filler1
            Get

            End Get
        End Property

        Public ReadOnly Property FinalOrderDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.FinalOrderDate
            Get

            End Get
        End Property

        Public ReadOnly Property ForecastFactor() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ForecastFactor
            Get

            End Get
        End Property

        Public ReadOnly Property FullItemDescription() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.FullItemDescription
            Get

            End Get
        End Property

        Public ReadOnly Property GroupNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.GroupNumber
            Get

            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Hash
            Get

            End Get
        End Property

        Public ReadOnly Property InitialOrderDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.InitialOrderDate
            Get

            End Get
        End Property

        Public ReadOnly Property InsulationItem() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.InsulationItem
            Get

            End Get
        End Property

        Public ReadOnly Property ModelTypeAttribute() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ModelTypeAttribute
            Get

            End Get
        End Property

        Public ReadOnly Property NonStockIndicator() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.NonStockIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property NoOrderFlag() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.NoOrderFlag
            Get

            End Get
        End Property

        Public ReadOnly Property OffensiveWeaponAgeRestriciton() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.OffensiveWeaponAgeRestriciton
            Get

            End Get
        End Property

        Public ReadOnly Property PackItemNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PackItemNumber
            Get

            End Get
        End Property

        Public ReadOnly Property PallatedSku() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PallatedSku
            Get

            End Get
        End Property

        Public ReadOnly Property ParticipateInMcp() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ParticipateInMcp
            Get

            End Get
        End Property

        Public ReadOnly Property PriceBookSequenceNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PriceBookSequenceNumber
            Get

            End Get
        End Property

        Public ReadOnly Property PriceDiscrepencyInPlace() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PriceDiscrepencyInPlace
            Get

            End Get
        End Property

        Public ReadOnly Property PriceEffectiveDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PriceEffectiveDate
            Get

            End Get
        End Property

        Public ReadOnly Property PriorPrice() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PriorPrice
            Get

            End Get
        End Property

        Public ReadOnly Property ProducersRecyclingFundSku() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ProducersRecyclingFundSku
            Get

            End Get
        End Property

        Public ReadOnly Property ProductGroupCode() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ProductGroupCode
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumEndDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PromotionalMinimumEndDate
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumStartDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PromotionalMinimumStartDate
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumStock() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PromotionalMinimumStock
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalPatternNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.PromotionalPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property QuarantineIndicator() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.QuarantineIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.RecordType
            Get

            End Get
        End Property

        Public ReadOnly Property RecordTypeNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.RecordTypeNumber
            Get

            End Get
        End Property

        Public ReadOnly Property RetailPriceEventNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.RetailPriceEventNumber
            Get

            End Get
        End Property

        Public ReadOnly Property RetailPriceEventPriority() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.RetailPriceEventPriority
            Get

            End Get
        End Property

        Public ReadOnly Property RetailSellingPrice() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.RetailSellingPrice
            Get

            End Get
        End Property

        Public ReadOnly Property SaleTypeAttribute() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SaleTypeAttribute
            Get

            End Get
        End Property

        Public ReadOnly Property SeasonalityPatternNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SeasonalityPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SellingUnitOfMeasure() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SellingUnitOfMeasure
            Get

            End Get
        End Property

        Public ReadOnly Property ShelfCapacity() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ShelfCapacity
            Get

            End Get
        End Property

        Public ReadOnly Property ShortDescriptionPosLookup() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.ShortDescriptionPosLookup
            Get

            End Get
        End Property

        Public ReadOnly Property SinglesPerPack() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SinglesPerPack
            Get

            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SkuNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SolventUserAgeRestriction() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SolventUserAgeRestriction
            Get

            End Get
        End Property

        Public ReadOnly Property SoqServiceLevel() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SoqServiceLevel
            Get

            End Get
        End Property

        Public ReadOnly Property StatusCode() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.StatusCode
            Get

            End Get
        End Property

        Public ReadOnly Property StockOffSale() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.StockOffSale
            Get

            End Get
        End Property

        Public ReadOnly Property StoreMaintained() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.StoreMaintained
            Get

            End Get
        End Property

        Public ReadOnly Property StoreNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.StoreNumber
            Get

            End Get
        End Property

        Public ReadOnly Property StyleNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.StyleNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SubGroupNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SubGroupNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierCode() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SupplierCode
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierItemNumber() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SupplierItemNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierPackaged() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SupplierPackaged
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierUnitOfMeasureCode() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SupplierUnitOfMeasureCode
            Get

            End Get
        End Property

        Public ReadOnly Property SystemUpdateDate() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.SystemUpdateDate
            Get

            End Get
        End Property

        Public ReadOnly Property TaggedItemIndicator() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.TaggedItemIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property TimberContent() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.TimberContent
            Get

            End Get
        End Property

        Public ReadOnly Property VatCode() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.VatCode
            Get

            End Get
        End Property

        Public ReadOnly Property Volume() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Volume
            Get

            End Get
        End Property

        Public ReadOnly Property WarrantyItem() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.WarrantyItem
            Get

            End Get
        End Property

        Public ReadOnly Property Weight() As [Interface].IFieldStructure Implements [Interface].IU1RecordStructure.Weight
            Get

            End Get
        End Property
#End Region
    End Structure
    Public Structure U5RecordStructure
        Implements [Interface].IU5RecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const StoreNumberStart As Int16 = 23
        Private Const StoreNumberLength As Int16 = 3
        Private Const RecordTypeNumberStart As Int16 = 26
        Private Const RecordTypeNumberLength As Int16 = 1
        Private Const InformationTypeStart As Int16 = 27
        Private Const InformationTypeLength As Int16 = 1
        Private Const DeletionIndicatorStart As Int16 = 28
        Private Const DeletionIndicatorLength As Int16 = 1
        Private Const SkuNumberStart As Int16 = 29
        Private Const SkuNumberLength As Int16 = 6
        Private Const TextLineNumberStart As Int16 = 35
        Private Const TextLineNumberLength As Int16 = 3
        Private Const TextStart As Int16 = 38
        Private Const TextLength As Int16 = 75
#End Region
#Region "Properties"



        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.DeletionIndicator
            Get
                Return New Field(DeletionIndicatorStart, DeletionIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property InformationType() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.InformationType
            Get
                Return New Field(InformationTypeStart, InformationTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordTypeNumber() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.RecordTypeNumber
            Get
                Return New Field(RecordTypeNumberStart, RecordTypeNumberLength)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberStart, SkuNumberLength)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.StoreNumber
            Get
                Return New Field(StoreNumberStart, StoreNumberLength)
            End Get
        End Property

        Public ReadOnly Property Text() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.Text
            Get
                Return New Field(TextStart, TextLength)
            End Get
        End Property

        Public ReadOnly Property TextLineNumber() As [Interface].IFieldStructure Implements [Interface].IU5RecordStructure.TextLineNumber
            Get
                Return New Field(TextLineNumberStart, TextLineNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure U6RecordStructure
        Implements [Interface].IU6RecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const SkuNumberStart As Int16 = 23
        Private Const SkuNumberLength As Int16 = 6
        Private Const ProductEquivalenceStart As Int16 = 29
        Private Const ProductEquivalenceLength As Int16 = 40
#End Region
#Region "Properties"

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IU6RecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property ProductEquivalence() As [Interface].IFieldStructure Implements [Interface].IU6RecordStructure.ProductEquivalence
            Get
                Return New Field(ProductEquivalenceStart, ProductEquivalenceLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IU6RecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IU6RecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].IU6RecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberStart, SkuNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure UaRecordStructure
        Implements [Interface].IUaRecordStructure


        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const MixAndMatchGroupNumberStart As Int16 = 23
        Private Const MixAndMatchGroupNumberLength As Int16 = 6
        Private Const SkuNumberStart As Int16 = 29
        Private Const SkuNumberLength As Int16 = 6
        Private Const DeletionIndicatorStart As Int16 = 35
        Private Const DeletionIndicatorLength As Int16 = 1
#End Region
#Region "Properties"

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator1() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.DeletionIndicator
            Get
                Return New Field(DeletionIndicatorStart, DeletionIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property Hash1() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property MixAndMatchGroupNumber() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.MixAndMatchGroupNumber
            Get
                Return New Field(MixAndMatchGroupNumberStart, MixAndMatchGroupNumberLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SkuNumber1() As [Interface].IFieldStructure Implements [Interface].IUaRecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberStart, SkuNumberLength)

            End Get
        End Property
#End Region
    End Structure
    Public Structure UbRecordStructure
        Implements [Interface].IUbRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const EventDescriptionStart As Int16 = 29
        Private Const EventDescriptionLength As Int16 = 40
        Private Const EventPriorityStart As Int16 = 69
        Private Const EventPriorityLength As Int16 = 2
        Private Const StartDateStart As Int16 = 71
        Private Const StartDateLength As Int16 = 8
        Private Const DailyStartTimeStart As Int16 = 79
        Private Const DailyStartTimeLength As Int16 = 4
        Private Const EndDateStart As Int16 = 83
        Private Const EndDateLength As Int16 = 8
        Private Const DailyEndTimeStart As Int16 = 91
        Private Const DailyEndTimeLength As Int16 = 4
        Private Const ActiveOnMondaysStart As Int16 = 95
        Private Const ActiveOnMondaysLength As Int16 = 1
        Private Const ActiveOnTuesdaysStart As Int16 = 96
        Private Const ActiveOnTuesdaysLength As Int16 = 1
        Private Const ActiveOnWednesdaysStart As Int16 = 97
        Private Const ActiveOnWednesdaysLength As Int16 = 1
        Private Const ActiveOnThursdaysStart As Int16 = 98
        Private Const ActiveOnThursdaysLength As Int16 = 1
        Private Const ActiveOnFridaysStart As Int16 = 99
        Private Const ActiveOnFridaysLength As Int16 = 1
        Private Const ActiveOnSaturdaysStart As Int16 = 100
        Private Const ActiveOnSaturdaysLength As Int16 = 1
        Private Const ActiveOnSundaysStart As Int16 = 101
        Private Const ActiveOnSundaysLength As Int16 = 1
        Private Const DeletionIndicatorStart As Int16 = 102
        Private Const DeletionIndicatorLength As Int16 = 1

#End Region
#Region "Properties"

        Public ReadOnly Property ActiveOnFridays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnFridays
            Get
                Return New Field(ActiveOnFridaysStart, ActiveOnFridaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnMondays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnMondays
            Get
                Return New Field(ActiveOnMondaysStart, ActiveOnMondaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnSaturdays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnSaturdays
            Get
                Return New Field(ActiveOnSaturdaysStart, ActiveOnSaturdaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnSundays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnSundays
            Get
                Return New Field(ActiveOnSundaysStart, ActiveOnSundaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnThursdays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnThursdays
            Get
                Return New Field(ActiveOnThursdaysStart, ActiveOnThursdaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnTuesdays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnTuesdays
            Get
                Return New Field(ActiveOnTuesdaysStart, ActiveOnTuesdaysLength)
            End Get
        End Property

        Public ReadOnly Property ActiveOnWednesdays() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.ActiveOnWednesdays
            Get
                Return New Field(ActiveOnWednesdaysStart, ActiveOnWednesdaysLength)
            End Get
        End Property

        Public ReadOnly Property DailyEndTime() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.DailyEndTime
            Get
                Return New Field(DailyEndTimeStart, DailyEndTimeLength)
            End Get
        End Property

        Public ReadOnly Property DailyStartTime() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.DailyStartTime
            Get
                Return New Field(DailyStartTimeStart, DailyStartTimeLength)
            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.DeletionIndicator
            Get
                Return New Field(DeletionIndicatorStart, DeletionIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property EndDate() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.EndDate
            Get
                Return New Field(EndDateStart, EndDateLength)
            End Get
        End Property

        Public ReadOnly Property EventDescription() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.EventDescription
            Get
                Return New Field(EventDescriptionStart, EventDescriptionLength)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.EventNumber
            Get
                Return New Field(EventNumberStart, EventNumberLength)
            End Get
        End Property

        Public ReadOnly Property EventPriority() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.EventPriority
            Get
                Return New Field(EventPriorityStart, EventPriorityLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property StartDate() As [Interface].IFieldStructure Implements [Interface].IUbRecordStructure.StartDate
            Get
                Return New Field(StartDateStart, StartDateLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure UdRecordStructure
        Implements [Interface].IUdRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const DealGroupNumberStart As Int16 = 29
        Private Const DealGroupNumberLength As Int16 = 6
        Private Const SkuNumberStart As Int16 = 35
        Private Const SkuNumberLength As Int16 = 6
        Private Const MixAndMatchGroupNumberStart As Int16 = 41
        Private Const MixAndMatchGroupNumberLength As Int16 = 6
        Private Const QuantityStart As Int16 = 47
        Private Const QuantityLength As Int16 = 7
        Private Const DiscountValueStart As Int16 = 54
        Private Const DiscountValueLength As Int16 = 10
        Private Const DiscountPercentageStart As Int16 = 64
        Private Const DiscountPercentageLength As Int16 = 7
        Private Const DeletedIndicatorStart As Int16 = 71
        Private Const DeletedIndicatorLength As Int16 = 1
#End Region
#Region "Properties"

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DealGroupNumber() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.DealGroupNumber
            Get
                Return New Field(DealGroupNumberStart, DealGroupNumberLength)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.DeletedIndicator
            Get
                Return New Field(DeletedIndicatorStart, DeletedIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property DiscountPercentage() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.DiscountPercentage
            Get
                Return New Field(DiscountPercentageStart, DiscountPercentageLength)
            End Get
        End Property

        Public ReadOnly Property DiscountValue() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.DiscountValue
            Get
                Return New Field(DiscountValueStart, DiscountValueLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property MixAndMatchGroupNumber1() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.MixAndMatchGroupNumber
            Get
                Return New Field(MixAndMatchGroupNumberStart, MixAndMatchGroupNumberLength)
            End Get
        End Property

        Public ReadOnly Property Quantity() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.Quantity
            Get
                Return New Field(QuantityStart, QuantityLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].IUdRecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberStart, SkuNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure UeRecordStructure
        Implements [Interface].IUeRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const CategoryOrGroupOrSubGroupOrStyleStart As Int16 = 29
        Private Const CategoryOrGroupOrSubGroupOrStyleLength As Int16 = 6
        Private Const ExcludedSkuNumberStart As Int16 = 35
        Private Const ExcludedSkuNumberLength As Int16 = 6
        Private Const DeletedIndicatorStart As Int16 = 41
        Private Const DeletedIndicatorLength As Int16 = 1
#End Region
#Region "Properties"


        Public ReadOnly Property CategoryOrGroupOrSubGroupOrStyle() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.CategoryOrGroupOrSubGroupOrStyle
            Get
                Return New Field(CategoryOrGroupOrSubGroupOrStyleStart, CategoryOrGroupOrSubGroupOrStyleLength)
            End Get
        End Property

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator1() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.DeletedIndicator
            Get
                Return New Field(DeletedIndicatorStart, DeletedIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.EventNumber
            Get
                Return New Field(EventNumberStart, EventNumberLength)
            End Get
        End Property

        Public ReadOnly Property ExcludedSkuNumber() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.ExcludedSkuNumber
            Get
                Return New Field(ExcludedSkuNumberStart, ExcludedSkuNumberLength)
            End Get
        End Property

        Public ReadOnly Property Hash1() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].IUeRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure UcRecordStructure
        Implements [Interface].IUcRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const EventTypeStart As Int16 = 29
        Private Const EventTypeLength As Int16 = 2
        Private Const ItemKey1Start As Int16 = 31
        Private Const ItemKey1Length As Int16 = 6
        Private Const ItemKey2Start As Int16 = 37
        Private Const ItemKey2Length As Int16 = 8
        Private Const PriceStart As Int16 = 45
        Private Const PriceLength As Int16 = 10
        Private Const BuyQuantityStart As Int16 = 55
        Private Const BuyQuantityLength As Int16 = 7
        Private Const GetQuantityStart As Int16 = 62
        Private Const GetQuantityLength As Int16 = 7
        Private Const DiscountValueStart As Int16 = 69
        Private Const DiscountValueLength As Int16 = 10
        Private Const GetQuantityDiscountPercentageStart As Int16 = 79
        Private Const GetQuantityDiscountPercentageLength As Int16 = 7
        Private Const DeleteIndicatorStart As Int16 = 86
        Private Const DeleteIndicatorLength As Int16 = 1
        Private Const BuyCouponNumberStart As Int16 = 87
        Private Const BuyCouponNumberLength As Int16 = 7
        Private Const GetCouponNumberStart As Int16 = 94
        Private Const GetCouponNumberLength As Int16 = 7
#End Region
#Region "Properties"
        Public ReadOnly Property BuyCouponNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordCouponStructure.BuyCouponNumber
            Get
                Return New Field(BuyCouponNumberStart, BuyCouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property GetCouponNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordCouponStructure.GetCouponNumber
            Get
                Return New Field(GetCouponNumberStart, GetCouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property BuyQuantity() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.BuyQuantity
            Get
                Return New Field(BuyQuantityStart, BuyQuantityLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property DeleteIndicator() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.DeleteIndicator
            Get
                Return New Field(DeleteIndicatorStart, DeleteIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property DiscountValue() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.DiscountValue
            Get
                Return New Field(DiscountValueStart, DiscountValueLength)
            End Get
        End Property
        Public ReadOnly Property EventNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.EventNumber
            Get
                Return New Field(EventNumberStart, EventNumberLength)
            End Get
        End Property
        Public ReadOnly Property EventType() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.EventType
            Get
                Return New Field(EventTypeStart, EventTypeLength)
            End Get
        End Property
        Public ReadOnly Property GetQuantity() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.GetQuantity
            Get
                Return New Field(GetQuantityStart, GetQuantityLength)
            End Get
        End Property
        Public ReadOnly Property GetQuantityDiscountPercentage() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.GetQuantityDiscountPercentage
            Get
                Return New Field(GetQuantityDiscountPercentageStart, GetQuantityDiscountPercentageLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property ItemKey1() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.ItemKey1
            Get
                Return New Field(ItemKey1Start, ItemKey1Length)
            End Get
        End Property
        Public ReadOnly Property ItemKey2() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.ItemKey2
            Get
                Return New Field(ItemKey2Start, ItemKey2Length)
            End Get
        End Property
        Public ReadOnly Property Price() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Price
            Get
                Return New Field(PriceStart, PriceLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region

    End Structure
    Public Structure UmRecordStructure
        Implements [Interface].IUmRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const CouponNumberStart As Int16 = 23
        Private Const CouponNumberLength As Int16 = 7
        Private Const CouponDescriptionStart As Int16 = 30
        Private Const CouponDescriptionLength As Int16 = 20
        Private Const StoreGenIndicatorStart As Int16 = 50
        Private Const StoreGenIndicatorLength As Int16 = 1
        Private Const SerialNumberIndicatorStart As Int16 = 51
        Private Const SerialNumberIndicatorLength As Int16 = 1
        Private Const ExclusiveIndicatorStart As Int16 = 52
        Private Const ExclusiveIndicatorLength As Int16 = 1
        Private Const ReusableIndicatorStart As Int16 = 53
        Private Const ReusableIndicatorLength As Int16 = 1
        Private Const CollectCustomerInformationIndicatorStart As Int16 = 54
        Private Const CollectCustomerInformationIndicatorLength As Int16 = 1
        Private Const DeletedIndicatorStart As Int16 = 55
        Private Const DeletedIndicatorLength As Int16 = 1
#End Region
#Region "Properties"
        Public ReadOnly Property CollectCustomerInformationIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CollectCustomerInformationIndicator
            Get
                Return New Field(CollectCustomerInformationIndicatorStart, CollectCustomerInformationIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property CouponNumber() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CouponNumber
            Get
                Return New Field(CouponNumberStart, CouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property CouponDescription() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CouponDescription
            Get
                Return New Field(CouponDescriptionStart, CouponDescriptionLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.DeletedIndicator
            Get
                Return New Field(DeletedIndicatorStart, DeletedIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property ExclusiveIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.ExclusiveIndicator
            Get
                Return New Field(ExclusiveIndicatorStart, ExclusiveIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
        Public ReadOnly Property ReusableIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.ReusableIndicator
            Get
                Return New Field(ReusableIndicatorStart, ReusableIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property SerialNumberIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.SerialNumberIndicator
            Get
                Return New Field(SerialNumberIndicatorStart, SerialNumberIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property StoreGenIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.StoreGenIndicator
            Get
                Return New Field(StoreGenIndicatorStart, StoreGenIndicatorLength)
            End Get
        End Property

#End Region
    End Structure
    Public Structure UtRecordStructure
        Implements [Interface].IUtRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const CouponNumberStart As Int16 = 23
        Private Const CouponNumberLength As Int16 = 7
        Private Const TextLineDisplaySequenceStart As Int16 = 30
        Private Const TextLineDisplaySequenceLength As Int16 = 2
        Private Const PrintSizeStart As Int16 = 32
        Private Const PrintSizeLength As Int16 = 1
        Private Const AlignmentStart As Int16 = 33
        Private Const AlignmentLength As Int16 = 1
        Private Const TextStart As Int16 = 34
        Private Const TextLength As Int16 = 52
        Private Const DeletedStart As Int16 = 86
        Private Const DeletedLength As Int16 = 1
#End Region
#Region "Properties"
        Public ReadOnly Property Alignment() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Alignment
            Get
                Return New Field(AlignmentStart, AlignmentLength)
            End Get
        End Property
        Public ReadOnly Property CouponNumber() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.CouponNumber
            Get
                Return New Field(CouponNumberStart, CouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property Deleted() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Deleted
            Get
                Return New Field(DeletedStart, DeletedLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property PrintSize() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.PrintSize
            Get
                Return New Field(PrintSizeStart, PrintSizeLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
        Public ReadOnly Property Text() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Text
            Get
                Return New Field(TextStart, TextLength)
            End Get
        End Property
        Public ReadOnly Property TextLineDisplaySequence() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.TextLineDisplaySequence
            Get
                Return New Field(TextLineDisplaySequenceStart, TextLineDisplaySequenceLength)
            End Get
        End Property
#End Region

    End Structure

End Namespace

Namespace DataStructureHostu.Implementation
    Public Structure Field
        Implements DataStructureHostu.Interface.IFieldStructure

        Private _name As String
        Private _startPosition As Int16
        Private _length As Int16
        Friend Sub New(ByVal startPosition As Int16, ByVal length As Int16)
            Me._startPosition = startPosition
            Me._length = length
        End Sub
        Public Property Length() As Short Implements DataStructureHostu.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property StartPosition() As Short Implements DataStructureHostu.Interface.IFieldStructure.StartPosition
            Get
                Return _startPosition
            End Get
            Set(ByVal value As Short)
                _startPosition = value
            End Set
        End Property
    End Structure
    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = ""
            If RecordIsLongEnoughToContainField(record, field) Then
                val = record.Substring(field.StartPosition - 1, field.Length)
            Else
                val = ""
            End If
            Return val
        End Function

        Public Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As [Interface].IFieldStructure) As Boolean
            Return record.Length >= field.StartPosition + field.Length - 1
        End Function

        'Private Function RecordIsLongEnoughToextractField(ByVal field As Field) As Integer
        '    Return
        'End Function
    End Class

    Public Class U1Record
        Inherits RecordFormat
        Implements [Interface].IU1Record

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As U1RecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New U1RecordStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property AdjustmentSetter() As String Implements [Interface].IU1Record.AdjustmentSetter
            Get

            End Get
        End Property

        Public ReadOnly Property AlternateSupplierNumber() As String Implements [Interface].IU1Record.AlternateSupplierNumber
            Get

            End Get
        End Property

        Public ReadOnly Property AutoApplyPriceChanges() As String Implements [Interface].IU1Record.AutoApplyPriceChanges
            Get

            End Get
        End Property

        Public ReadOnly Property BallastItem() As String Implements [Interface].IU1Record.BallastItem
            Get

            End Get
        End Property

        Public ReadOnly Property BankHolidayPatternNumber() As String Implements [Interface].IU1Record.BankHolidayPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property CategoryNumber() As String Implements [Interface].IU1Record.CategoryNumber
            Get

            End Get
        End Property

        Public ReadOnly Property CheckDigit() As String Implements [Interface].IU1Record.CheckDigit
            Get

            End Get
        End Property

        Public ReadOnly Property Cost() As String Implements [Interface].IU1Record.Cost
            Get

            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IU1Record.Date
            Get

            End Get
        End Property

        Public ReadOnly Property DisplayFactor() As String Implements [Interface].IU1Record.DisplayFactor
            Get

            End Get
        End Property

        Public ReadOnly Property EanNumberFirstEightDigits() As String Implements [Interface].IU1Record.EanNumberFirstEightDigits
            Get

            End Get
        End Property

        Public ReadOnly Property EanNumberLastFiveDigits() As String Implements [Interface].IU1Record.EanNumberLastFiveDigits
            Get

            End Get
        End Property

        Public ReadOnly Property ElectricalItem() As String Implements [Interface].IU1Record.ElectricalItem
            Get

            End Get
        End Property

        Public ReadOnly Property EquivalentPriceMultiplier() As String Implements [Interface].IU1Record.EquivalentPriceMultiplier
            Get

            End Get
        End Property

        Public ReadOnly Property EquivalentPriceUnit() As String Implements [Interface].IU1Record.EquivalentPriceUnit
            Get

            End Get
        End Property

        Public ReadOnly Property Filler() As String Implements [Interface].IU1Record.Filler
            Get

            End Get
        End Property

        Public ReadOnly Property Filler1() As String Implements [Interface].IU1Record.Filler1
            Get

            End Get
        End Property

        Public ReadOnly Property FinalOrderDate() As String Implements [Interface].IU1Record.FinalOrderDate
            Get

            End Get
        End Property

        Public ReadOnly Property ForecastFactor() As String Implements [Interface].IU1Record.ForecastFactor
            Get

            End Get
        End Property

        Public ReadOnly Property FullItemDescription() As String Implements [Interface].IU1Record.FullItemDescription
            Get

            End Get
        End Property

        Public ReadOnly Property GroupNumber() As String Implements [Interface].IU1Record.GroupNumber
            Get

            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IU1Record.Hash
            Get

            End Get
        End Property

        Public ReadOnly Property InitialOrderDate() As String Implements [Interface].IU1Record.InitialOrderDate
            Get

            End Get
        End Property

        Public ReadOnly Property InsulationItem() As String Implements [Interface].IU1Record.InsulationItem
            Get

            End Get
        End Property

        Public ReadOnly Property ModelTypeAttribute() As String Implements [Interface].IU1Record.ModelTypeAttribute
            Get

            End Get
        End Property

        Public ReadOnly Property NonStockIndicator() As String Implements [Interface].IU1Record.NonStockIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property NoOrderFlag() As String Implements [Interface].IU1Record.NoOrderFlag
            Get

            End Get
        End Property

        Public ReadOnly Property OffensiveWeaponAgeRestriciton() As String Implements [Interface].IU1Record.OffensiveWeaponAgeRestriciton
            Get

            End Get
        End Property

        Public ReadOnly Property PackItemNumber() As String Implements [Interface].IU1Record.PackItemNumber
            Get

            End Get
        End Property

        Public ReadOnly Property PallatedSku() As String Implements [Interface].IU1Record.PallatedSku
            Get

            End Get
        End Property

        Public ReadOnly Property ParticipateInMcp() As String Implements [Interface].IU1Record.ParticipateInMcp
            Get

            End Get
        End Property

        Public ReadOnly Property PriceBookSequenceNumber() As String Implements [Interface].IU1Record.PriceBookSequenceNumber
            Get

            End Get
        End Property

        Public ReadOnly Property PriceDiscrepencyInPlace() As String Implements [Interface].IU1Record.PriceDiscrepencyInPlace
            Get

            End Get
        End Property

        Public ReadOnly Property PriceEffectiveDate() As String Implements [Interface].IU1Record.PriceEffectiveDate
            Get

            End Get
        End Property

        Public ReadOnly Property PriorPrice() As String Implements [Interface].IU1Record.PriorPrice
            Get

            End Get
        End Property

        Public ReadOnly Property ProducersRecyclingFundSku() As String Implements [Interface].IU1Record.ProducersRecyclingFundSku
            Get

            End Get
        End Property

        Public ReadOnly Property ProductGroupCode() As String Implements [Interface].IU1Record.ProductGroupCode
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumEndDate() As String Implements [Interface].IU1Record.PromotionalMinimumEndDate
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumStartDate() As String Implements [Interface].IU1Record.PromotionalMinimumStartDate
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalMinimumStock() As String Implements [Interface].IU1Record.PromotionalMinimumStock
            Get

            End Get
        End Property

        Public ReadOnly Property PromotionalPatternNumber() As String Implements [Interface].IU1Record.PromotionalPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property QuarantineIndicator() As String Implements [Interface].IU1Record.QuarantineIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IU1Record.RecordType
            Get

            End Get
        End Property

        Public ReadOnly Property RecordTypeNumber() As String Implements [Interface].IU1Record.RecordTypeNumber
            Get

            End Get
        End Property

        Public ReadOnly Property RetailPriceEventNumber() As String Implements [Interface].IU1Record.RetailPriceEventNumber
            Get

            End Get
        End Property

        Public ReadOnly Property RetailPriceEventPriority() As String Implements [Interface].IU1Record.RetailPriceEventPriority
            Get

            End Get
        End Property

        Public ReadOnly Property RetailSellingPrice() As String Implements [Interface].IU1Record.RetailSellingPrice
            Get

            End Get
        End Property

        Public ReadOnly Property SaleTypeAttribute() As String Implements [Interface].IU1Record.SaleTypeAttribute
            Get

            End Get
        End Property

        Public ReadOnly Property SeasonalityPatternNumber() As String Implements [Interface].IU1Record.SeasonalityPatternNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SellingUnitOfMeasure() As String Implements [Interface].IU1Record.SellingUnitOfMeasure
            Get

            End Get
        End Property

        Public ReadOnly Property ShelfCapacity() As String Implements [Interface].IU1Record.ShelfCapacity
            Get

            End Get
        End Property

        Public ReadOnly Property ShortDescriptionPosLookup() As String Implements [Interface].IU1Record.ShortDescriptionPosLookup
            Get

            End Get
        End Property

        Public ReadOnly Property SinglesPerPack() As String Implements [Interface].IU1Record.SinglesPerPack
            Get

            End Get
        End Property

        Public ReadOnly Property SkuNumber() As String Implements [Interface].IU1Record.SkuNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SolventUserAgeRestriction() As String Implements [Interface].IU1Record.SolventUserAgeRestriction
            Get

            End Get
        End Property

        Public ReadOnly Property SoqServiceLevel() As String Implements [Interface].IU1Record.SoqServiceLevel
            Get

            End Get
        End Property

        Public ReadOnly Property StatusCode() As String Implements [Interface].IU1Record.StatusCode
            Get

            End Get
        End Property

        Public ReadOnly Property StockOffSale() As String Implements [Interface].IU1Record.StockOffSale
            Get

            End Get
        End Property

        Public ReadOnly Property StoreMaintained() As String Implements [Interface].IU1Record.StoreMaintained
            Get

            End Get
        End Property

        Public ReadOnly Property StoreNumber() As String Implements [Interface].IU1Record.StoreNumber
            Get

            End Get
        End Property

        Public ReadOnly Property StyleNumber() As String Implements [Interface].IU1Record.StyleNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SubGroupNumber() As String Implements [Interface].IU1Record.SubGroupNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierCode() As String Implements [Interface].IU1Record.SupplierCode
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierItemNumber() As String Implements [Interface].IU1Record.SupplierItemNumber
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierPackaged() As String Implements [Interface].IU1Record.SupplierPackaged
            Get

            End Get
        End Property

        Public ReadOnly Property SupplierUnitOfMeasureCode() As String Implements [Interface].IU1Record.SupplierUnitOfMeasureCode
            Get

            End Get
        End Property

        Public ReadOnly Property SystemUpdateDate() As String Implements [Interface].IU1Record.SystemUpdateDate
            Get

            End Get
        End Property

        Public ReadOnly Property TaggedItemIndicator() As String Implements [Interface].IU1Record.TaggedItemIndicator
            Get

            End Get
        End Property

        Public ReadOnly Property TimberContent() As String Implements [Interface].IU1Record.TimberContent
            Get

            End Get
        End Property

        Public ReadOnly Property VatCode() As String Implements [Interface].IU1Record.VatCode
            Get

            End Get
        End Property

        Public ReadOnly Property Volume() As String Implements [Interface].IU1Record.Volume
            Get

            End Get
        End Property

        Public ReadOnly Property WarrantyItem() As String Implements [Interface].IU1Record.WarrantyItem
            Get

            End Get
        End Property

        Public ReadOnly Property Weight() As String Implements [Interface].IU1Record.Weight
            Get
                'Return Me.FormattedValue(_record, _recordStructure.Weight)
            End Get
        End Property
#End Region
    End Class
    Public Class U5Record
        Inherits RecordFormat
        Implements [Interface].IU5Record

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As U5RecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New U5RecordStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property [Date]() As String Implements [Interface].IU5Record.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator() As String Implements [Interface].IU5Record.DeletionIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletionIndicator)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IU5Record.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property InformationType() As String Implements [Interface].IU5Record.InformationType
            Get
                Return Me.FormattedValue(_record, _recordStructure.InformationType)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IU5Record.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property RecordTypeNumber() As String Implements [Interface].IU5Record.RecordTypeNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordTypeNumber)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As String Implements [Interface].IU5Record.SkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SkuNumber)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As String Implements [Interface].IU5Record.StoreNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreNumber)
            End Get
        End Property

        Public ReadOnly Property Text() As String Implements [Interface].IU5Record.Text
            Get
                Return Me.FormattedValue(_record, _recordStructure.Text)
            End Get
        End Property

        Public ReadOnly Property TextLineNumber() As String Implements [Interface].IU5Record.TextLineNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.TextLineNumber)
            End Get
        End Property
#End Region
    End Class
    Public Class U6Record
        Inherits RecordFormat
        Implements [Interface].IU6Record


        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As U6RecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New U6RecordStructure
        End Sub

        Public ReadOnly Property [Date]() As String Implements [Interface].IU6Record.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IU6Record.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IU6Record.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As String Implements [Interface].IU6Record.SkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SkuNumber)
            End Get
        End Property

        Public ReadOnly Property ProductEquivalence() As String Implements [Interface].IU6Record.ProductEquivalence
            Get
                Return Me.FormattedValue(_record, _recordStructure.ProductEquivalence)
            End Get
        End Property
    End Class
    Public Class UaRecord
        Inherits RecordFormat
        Implements [Interface].IUaRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UaRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UaRecordStructure
        End Sub

        Public ReadOnly Property Date1() As String Implements [Interface].IUaRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator1() As String Implements [Interface].IUaRecord.DeletionIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletionIndicator1)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].IUaRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash1)
            End Get
        End Property

        Public ReadOnly Property MixAndMatchGroupNumber() As String Implements [Interface].IUaRecord.MixAndMatchGroupNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.MixAndMatchGroupNumber)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].IUaRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property

        Public ReadOnly Property SkuNumber1() As String Implements [Interface].IUaRecord.SkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SkuNumber1)
            End Get
        End Property
    End Class
    Public Class UbRecord
        Inherits RecordFormat
        Implements [Interface].IUbRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UbRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UbRecordStructure
        End Sub


        Public ReadOnly Property ActiveOnFridays() As String Implements [Interface].IUbRecord.ActiveOnFridays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnFridays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnMondays() As String Implements [Interface].IUbRecord.ActiveOnMondays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnMondays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnSaturdays() As String Implements [Interface].IUbRecord.ActiveOnSaturdays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnSaturdays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnSundays() As String Implements [Interface].IUbRecord.ActiveOnSundays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnSundays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnThursdays() As String Implements [Interface].IUbRecord.ActiveOnThursdays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnThursdays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnTuesdays() As String Implements [Interface].IUbRecord.ActiveOnTuesdays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnTuesdays)
            End Get
        End Property

        Public ReadOnly Property ActiveOnWednesdays() As String Implements [Interface].IUbRecord.ActiveOnWednesdays
            Get
                Return Me.FormattedValue(_record, _recordStructure.ActiveOnWednesdays)
            End Get
        End Property

        Public ReadOnly Property DailyEndTime() As String Implements [Interface].IUbRecord.DailyEndTime
            Get
                Return Me.FormattedValue(_record, _recordStructure.DailyEndTime)
            End Get
        End Property

        Public ReadOnly Property DailyStartTime() As String Implements [Interface].IUbRecord.DailyStartTime
            Get
                Return Me.FormattedValue(_record, _recordStructure.DailyStartTime)
            End Get
        End Property

        Public ReadOnly Property Date1() As String Implements [Interface].IUbRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator1() As String Implements [Interface].IUbRecord.DeletionIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletionIndicator)
            End Get
        End Property

        Public ReadOnly Property EndDate() As String Implements [Interface].IUbRecord.EndDate
            Get
                Return Me.FormattedValue(_record, _recordStructure.EndDate)
            End Get
        End Property

        Public ReadOnly Property EventDescription() As String Implements [Interface].IUbRecord.EventDescription
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventDescription)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As String Implements [Interface].IUbRecord.EventNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventNumber)
            End Get
        End Property

        Public ReadOnly Property EventPriority() As String Implements [Interface].IUbRecord.EventPriority
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventPriority)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].IUbRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].IUbRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property StartDate() As String Implements [Interface].IUbRecord.StartDate
            Get
                Return Me.FormattedValue(_record, _recordStructure.StartDate)
            End Get
        End Property
    End Class
    Public Class UdRecord
        Inherits RecordFormat
        Implements [Interface].IUdRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UdRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UdRecordStructure
        End Sub

        Public ReadOnly Property Date1() As String Implements [Interface].IUdRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DealGroupNumber() As String Implements [Interface].IUdRecord.DealGroupNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.DealGroupNumber)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator() As String Implements [Interface].IUdRecord.DeletedIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletedIndicator)
            End Get
        End Property

        Public ReadOnly Property DiscountPercentage() As String Implements [Interface].IUdRecord.DiscountPercentage
            Get
                Return Me.FormattedValue(_record, _recordStructure.DiscountPercentage)
            End Get
        End Property

        Public ReadOnly Property DiscountValue() As String Implements [Interface].IUdRecord.DiscountValue
            Get
                Return Me.FormattedValue(_record, _recordStructure.DiscountValue)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].IUdRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property MixAndMatchGroupNumber() As String Implements [Interface].IUdRecord.MixAndMatchGroupNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.MixAndMatchGroupNumber1)
            End Get
        End Property

        Public ReadOnly Property Quantity() As String Implements [Interface].IUdRecord.Quantity
            Get
                Return Me.FormattedValue(_record, _recordStructure.Quantity)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].IUdRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SkuNumber1() As String Implements [Interface].IUdRecord.SkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SkuNumber)
            End Get
        End Property
    End Class
    Public Class UeRecord
        Inherits RecordFormat
        Implements [Interface].IUeRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UeRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UeRecordStructure
        End Sub

        Public ReadOnly Property CategoryOrGroupOrSubGroupOrStyle() As String Implements [Interface].IUeRecord.CategoryOrGroupOrSubGroupOrStyle
            Get
                Return Me.FormattedValue(_record, _recordStructure.CategoryOrGroupOrSubGroupOrStyle)
            End Get
        End Property

        Public ReadOnly Property Date1() As String Implements [Interface].IUeRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator() As String Implements [Interface].IUeRecord.DeletedIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletedIndicator1)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As String Implements [Interface].IUeRecord.EventNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventNumber)
            End Get
        End Property

        Public ReadOnly Property ExcludedSkuNumber() As String Implements [Interface].IUeRecord.ExcludedSkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.ExcludedSkuNumber)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].IUeRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash1)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].IUeRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property
    End Class
    Public Class UcRecord
        Inherits RecordFormat
        Implements [Interface].IUcRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UcRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UcRecordStructure
        End Sub
#Region "Properties"
        Public ReadOnly Property BuyQuantity() As String Implements [Interface].IUcRecord.BuyQuantity
            Get
                Return Me.FormattedValue(_record, _recordStructure.BuyQuantity)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUcRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeleteIndicator() As String Implements [Interface].IUcRecord.DeleteIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeleteIndicator)
            End Get
        End Property

        Public ReadOnly Property DiscountValue() As String Implements [Interface].IUcRecord.DiscountValue
            Get
                Return Me.FormattedValue(_record, _recordStructure.DiscountValue)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As String Implements [Interface].IUcRecord.EventNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventNumber)
            End Get
        End Property

        Public ReadOnly Property EventType() As String Implements [Interface].IUcRecord.EventType
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventType)
            End Get
        End Property

        Public ReadOnly Property GetQuantity() As String Implements [Interface].IUcRecord.GetQuantity
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetQuantity)
            End Get
        End Property

        Public ReadOnly Property GetQuantityDiscountPercentage() As String Implements [Interface].IUcRecord.GetQuantityDiscountPercentage
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetQuantityDiscountPercentage)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUcRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property ItemKey1() As String Implements [Interface].IUcRecord.ItemKey1
            Get
                Return Me.FormattedValue(_record, _recordStructure.ItemKey1)
            End Get
        End Property

        Public ReadOnly Property ItemKey2() As String Implements [Interface].IUcRecord.ItemKey2
            Get
                Return Me.FormattedValue(_record, _recordStructure.ItemKey2)
            End Get
        End Property

        Public ReadOnly Property Price() As String Implements [Interface].IUcRecord.Price
            Get
                Return Me.FormattedValue(_record, _recordStructure.Price)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUcRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property BuyCouponNumber() As String Implements [Interface].IUcRecordCoupon.BuyCouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.BuyCouponNumber)
            End Get
        End Property

        Public ReadOnly Property GetCouponNumber() As String Implements [Interface].IUcRecordCoupon.GetCouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetCouponNumber)
            End Get
        End Property
#End Region
    End Class
    Public Class UmRecord
        Inherits RecordFormat
        Implements [Interface].IUmRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UmRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UmRecordStructure
        End Sub
#Region "Properties"
        Public ReadOnly Property CollectCustomerInformationIndicator() As String Implements [Interface].IUmRecord.CollectCustomerInformationIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.CollectCustomerInformationIndicator)
            End Get
        End Property

        Public ReadOnly Property CouponNumber() As String Implements [Interface].IUmRecord.CouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.CouponNumber)
            End Get
        End Property
        Public ReadOnly Property CouponNumberDescription() As String Implements [Interface].IUmRecord.CouponDescription
            Get
                Return Me.FormattedValue(_record, _recordStructure.CouponDescription)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUmRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator() As String Implements [Interface].IUmRecord.DeletedIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletedIndicator)
            End Get
        End Property

        Public ReadOnly Property ExclusiveIndicator() As String Implements [Interface].IUmRecord.ExclusiveIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.ExclusiveIndicator)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUmRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUmRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property ReusableIndicator() As String Implements [Interface].IUmRecord.ReusableIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.ReusableIndicator)
            End Get
        End Property

        Public ReadOnly Property SerialNumberIndicator() As String Implements [Interface].IUmRecord.SerialNumberIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.SerialNumberIndicator)
            End Get
        End Property

        Public ReadOnly Property StoreGenIndicator() As String Implements [Interface].IUmRecord.StoreGenIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreGenIndicator)
            End Get
        End Property
#End Region
    End Class
    Public Class UtRecord
        Inherits RecordFormat
        Implements [Interface].IUtRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UtRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UtRecordStructure
        End Sub

        Public ReadOnly Property Alignment() As String Implements [Interface].IUtRecord.Alignment
            Get
                Return Me.FormattedValue(_record, _recordStructure.Alignment)
            End Get
        End Property

        Public ReadOnly Property CouponNumber() As String Implements [Interface].IUtRecord.CouponNumber
            Get
                Return FormattedValue(_record, _recordStructure.CouponNumber)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUtRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property Deleted() As String Implements [Interface].IUtRecord.Deleted
            Get
                Return Me.FormattedValue(_record, _recordStructure.Deleted)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUtRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property PrintSize() As String Implements [Interface].IUtRecord.PrintSize
            Get
                Return Me.FormattedValue(_record, _recordStructure.PrintSize)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUtRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property Text() As String Implements [Interface].IUtRecord.Text
            Get
                Return Me.FormattedValue(_record, _recordStructure.Text)
            End Get
        End Property

        Public ReadOnly Property TextLineDisplaySequence() As String Implements [Interface].IUtRecord.TextLineDisplaySequence
            Get
                Return Me.FormattedValue(_record, _recordStructure.TextLineDisplaySequence)
            End Get
        End Property
    End Class

End Namespace

