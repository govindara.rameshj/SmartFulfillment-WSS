﻿Imports ProcessTransmissions.DataStructure.Implementation

Namespace DataStructure.Interface
    Public Interface IFieldStructure
        Property StartPosition() As Int16
        Property Length() As Int16
    End Interface
    Public Interface ICouponBarcodeStructure
        ReadOnly Property StartCharacter() As [Interface].IFieldStructure
        ReadOnly Property CouponId() As [Interface].IFieldStructure
        ReadOnly Property MarketingReference() As [Interface].IFieldStructure
        ReadOnly Property SerialNumber() As [Interface].IFieldStructure
        ReadOnly Property StopCharacter() As [Interface].IFieldStructure
    End Interface
    Public Interface IUcRecordStructure
        Inherits [Interface].IUcRecordCouponStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property EventNumber() As [Interface].IFieldStructure
        ReadOnly Property EventType() As [Interface].IFieldStructure
        ReadOnly Property ItemKey1() As [Interface].IFieldStructure
        ReadOnly Property ItemKey2() As [Interface].IFieldStructure
        ReadOnly Property Price() As [Interface].IFieldStructure
        ReadOnly Property BuyQuantity() As [Interface].IFieldStructure
        ReadOnly Property GetQuantity() As [Interface].IFieldStructure
        ReadOnly Property DiscountValue() As [Interface].IFieldStructure
        ReadOnly Property GetQuantityDiscountPercentage() As [Interface].IFieldStructure
        ReadOnly Property DeleteIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUcRecordCouponStructure
        ReadOnly Property BuyCouponNumber() As [Interface].IFieldStructure
        ReadOnly Property GetCouponNumber() As [Interface].IFieldStructure
    End Interface
    Public Interface IUmRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property CouponNumber() As [Interface].IFieldStructure
        ReadOnly Property CouponDescription() As [Interface].IFieldStructure
        ReadOnly Property StoreGenIndicator() As [Interface].IFieldStructure
        ReadOnly Property SerialNumberIndicator() As [Interface].IFieldStructure
        ReadOnly Property ExclusiveIndicator() As [Interface].IFieldStructure
        ReadOnly Property ReusableIndicator() As [Interface].IFieldStructure
        ReadOnly Property CollectCustomerInformationIndicator() As [Interface].IFieldStructure
        ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure
    End Interface
    Public Interface IUtRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property CouponNumber() As [Interface].IFieldStructure
        ReadOnly Property TextLineDisplaySequence() As [Interface].IFieldStructure
        ReadOnly Property PrintSize() As [Interface].IFieldStructure
        ReadOnly Property Alignment() As [Interface].IFieldStructure
        ReadOnly Property Text() As [Interface].IFieldStructure
        ReadOnly Property Deleted() As [Interface].IFieldStructure
    End Interface
    Public Interface ICouponBarcode
        ReadOnly Property StartCharacter() As String
        ReadOnly Property CouponId() As String
        ReadOnly Property MarketingReference() As String
        ReadOnly Property SerialNumber() As String
        ReadOnly Property StopCharacter() As String
    End Interface
    Public Interface IUcRecord
        Inherits [Interface].IUcRecordCoupon
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property EventNumber() As String
        ReadOnly Property EventType() As String
        ReadOnly Property ItemKey1() As String
        ReadOnly Property ItemKey2() As String
        ReadOnly Property Price() As String
        ReadOnly Property BuyQuantity() As String
        ReadOnly Property GetQuantity() As String
        ReadOnly Property DiscountValue() As String
        ReadOnly Property GetQuantityDiscountPercentage() As String
        ReadOnly Property DeleteIndicator() As String
    End Interface
    Public Interface IUcRecordCoupon
        ReadOnly Property BuyCouponNumber() As String
        ReadOnly Property GetCouponNumber() As String
    End Interface
    Public Interface IUmRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property CouponNumber() As String
        ReadOnly Property CouponDescription() As String
        ReadOnly Property StoreGenIndicator() As String
        ReadOnly Property SerialNumberIndicator() As String
        ReadOnly Property ExclusiveIndicator() As String
        ReadOnly Property ReusableIndicator() As String
        ReadOnly Property CollectCustomerInformationIndicator() As String
        ReadOnly Property DeletedIndicator() As String
    End Interface
    Public Interface IUtRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property CouponNumber() As String
        ReadOnly Property TextLineDisplaySequence() As String
        ReadOnly Property PrintSize() As String
        ReadOnly Property Alignment() As String
        ReadOnly Property Text() As String
        ReadOnly Property Deleted() As String
    End Interface
End Namespace

Namespace DataStructure
    Public Structure CouponBarcodeStructure
        Implements [Interface].ICouponBarcodeStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const StartCharacterStart As Int16 = 1
        Private Const StartCharacterLength As Int16 = 1
        Private Const CouponIdStart As Int16 = 2
        Private Const CouponIdLength As Int16 = 7
        Private Const MarketingReferenceStart As Int16 = 9
        Private Const MarketingReferenceLength As Int16 = 4
        Private Const SerialNumberStart As Int16 = 13
        Private Const SerialNumberLength As Int16 = 6
        Private Const StopCharacterStart As Int16 = 19
        Private Const StopCharacterLength As Int16 = 1
#End Region
#Region "Properties"
        Public ReadOnly Property CouponId() As [Interface].IFieldStructure Implements [Interface].ICouponBarcodeStructure.CouponId
            Get
                Return New Field(CouponIdStart, CouponIdLength)
            End Get
        End Property

        Public ReadOnly Property MarketingReference() As [Interface].IFieldStructure Implements [Interface].ICouponBarcodeStructure.MarketingReference
            Get
                Return New Field(MarketingReferenceStart, MarketingReferenceLength)
            End Get
        End Property

        Public ReadOnly Property SerialNumber() As [Interface].IFieldStructure Implements [Interface].ICouponBarcodeStructure.SerialNumber
            Get
                Return New Field(SerialNumberStart, SerialNumberLength)
            End Get
        End Property

        Public ReadOnly Property StartCharacter() As [Interface].IFieldStructure Implements [Interface].ICouponBarcodeStructure.StartCharacter
            Get
                Return New Field(StartCharacterStart, StartCharacterLength)
            End Get
        End Property

        Public ReadOnly Property StopCharacter() As [Interface].IFieldStructure Implements [Interface].ICouponBarcodeStructure.StopCharacter
            Get
                Return New Field(StopCharacterStart, StopCharacterLength)
            End Get
        End Property
#End Region

    End Structure
    Public Structure UcRecordStructure
        Implements [Interface].IUcRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const EventNumberStart As Int16 = 23
        Private Const EventNumberLength As Int16 = 6
        Private Const EventTypeStart As Int16 = 29
        Private Const EventTypeLength As Int16 = 2
        Private Const ItemKey1Start As Int16 = 31
        Private Const ItemKey1Length As Int16 = 6
        Private Const ItemKey2Start As Int16 = 37
        Private Const ItemKey2Length As Int16 = 8
        Private Const PriceStart As Int16 = 45
        Private Const PriceLength As Int16 = 10
        Private Const BuyQuantityStart As Int16 = 55
        Private Const BuyQuantityLength As Int16 = 7
        Private Const GetQuantityStart As Int16 = 62
        Private Const GetQuantityLength As Int16 = 7
        Private Const DiscountValueStart As Int16 = 69
        Private Const DiscountValueLength As Int16 = 10
        Private Const GetQuantityDiscountPercentageStart As Int16 = 79
        Private Const GetQuantityDiscountPercentageLength As Int16 = 7
        Private Const DeleteIndicatorStart As Int16 = 86
        Private Const DeleteIndicatorLength As Int16 = 1
        Private Const BuyCouponNumberStart As Int16 = 87
        Private Const BuyCouponNumberLength As Int16 = 7
        Private Const GetCouponNumberStart As Int16 = 94
        Private Const GetCouponNumberLength As Int16 = 7
#End Region
#Region "Properties"
        Public ReadOnly Property BuyCouponNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordCouponStructure.BuyCouponNumber
            Get
                Return New Field(BuyCouponNumberStart, BuyCouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property GetCouponNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordCouponStructure.GetCouponNumber
            Get
                Return New Field(GetCouponNumberStart, GetCouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property BuyQuantity() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.BuyQuantity
            Get
                Return New Field(BuyQuantityStart, BuyQuantityLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property DeleteIndicator() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.DeleteIndicator
            Get
                Return New Field(DeleteIndicatorStart, DeleteIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property DiscountValue() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.DiscountValue
            Get
                Return New Field(DiscountValueStart, DiscountValueLength)
            End Get
        End Property
        Public ReadOnly Property EventNumber() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.EventNumber
            Get
                Return New Field(EventNumberStart, EventNumberLength)
            End Get
        End Property
        Public ReadOnly Property EventType() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.EventType
            Get
                Return New Field(EventTypeStart, EventTypeLength)
            End Get
        End Property
        Public ReadOnly Property GetQuantity() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.GetQuantity
            Get
                Return New Field(GetQuantityStart, GetQuantityLength)
            End Get
        End Property
        Public ReadOnly Property GetQuantityDiscountPercentage() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.GetQuantityDiscountPercentage
            Get
                Return New Field(GetQuantityDiscountPercentageStart, GetQuantityDiscountPercentageLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property ItemKey1() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.ItemKey1
            Get
                Return New Field(ItemKey1Start, ItemKey1Length)
            End Get
        End Property
        Public ReadOnly Property ItemKey2() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.ItemKey2
            Get
                Return New Field(ItemKey2Start, ItemKey2Length)
            End Get
        End Property
        Public ReadOnly Property Price() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.Price
            Get
                Return New Field(PriceStart, PriceLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUcRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region

    End Structure
    Public Structure UmRecordStructure
        Implements [Interface].IUmRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const CouponNumberStart As Int16 = 23
        Private Const CouponNumberLength As Int16 = 7
        Private Const CouponDescriptionStart As Int16 = 30
        Private Const CouponDescriptionLength As Int16 = 40
        Private Const StoreGenIndicatorStart As Int16 = 70
        Private Const StoreGenIndicatorLength As Int16 = 1
        Private Const SerialNumberIndicatorStart As Int16 = 71
        Private Const SerialNumberIndicatorLength As Int16 = 1
        Private Const ExclusiveIndicatorStart As Int16 = 72
        Private Const ExclusiveIndicatorLength As Int16 = 1
        Private Const ReusableIndicatorStart As Int16 = 73
        Private Const ReusableIndicatorLength As Int16 = 1
        Private Const CollectCustomerInformationIndicatorStart As Int16 = 74
        Private Const CollectCustomerInformationIndicatorLength As Int16 = 1
        Private Const DeletedIndicatorStart As Int16 = 75
        Private Const DeletedIndicatorLength As Int16 = 1
#End Region
#Region "Properties"
        Public ReadOnly Property CollectCustomerInformationIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CollectCustomerInformationIndicator
            Get
                Return New Field(CollectCustomerInformationIndicatorStart, CollectCustomerInformationIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property CouponNumber() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CouponNumber
            Get
                Return New Field(CouponNumberStart, CouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property CouponDescription() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.CouponDescription
            Get
                Return New Field(CouponDescriptionStart, CouponDescriptionLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property DeletedIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.DeletedIndicator
            Get
                Return New Field(DeletedIndicatorStart, DeletedIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property ExclusiveIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.ExclusiveIndicator
            Get
                Return New Field(ExclusiveIndicatorStart, ExclusiveIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
        Public ReadOnly Property ReusableIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.ReusableIndicator
            Get
                Return New Field(ReusableIndicatorStart, ReusableIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property SerialNumberIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.SerialNumberIndicator
            Get
                Return New Field(SerialNumberIndicatorStart, SerialNumberIndicatorLength)
            End Get
        End Property
        Public ReadOnly Property StoreGenIndicator() As [Interface].IFieldStructure Implements [Interface].IUmRecordStructure.StoreGenIndicator
            Get
                Return New Field(StoreGenIndicatorStart, StoreGenIndicatorLength)
            End Get
        End Property

#End Region
    End Structure
    Public Structure UtRecordStructure
        Implements [Interface].IUtRecordStructure

        Private _dummy As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const CouponNumberStart As Int16 = 23
        Private Const CouponNumberLength As Int16 = 7
        Private Const TextLineDisplaySequenceStart As Int16 = 30
        Private Const TextLineDisplaySequenceLength As Int16 = 2
        Private Const PrintSizeStart As Int16 = 32
        Private Const PrintSizeLength As Int16 = 1
        Private Const AlignmentStart As Int16 = 33
        Private Const AlignmentLength As Int16 = 1
        Private Const TextStart As Int16 = 34
        Private Const TextLength As Int16 = 52
        Private Const DeletedStart As Int16 = 86
        Private Const DeletedLength As Int16 = 1
#End Region
#Region "Properties"
        Public ReadOnly Property Alignment() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Alignment
            Get
                Return New Field(AlignmentStart, AlignmentLength)
            End Get
        End Property
        Public ReadOnly Property CouponNumber() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.CouponNumber
            Get
                Return New Field(CouponNumberStart, CouponNumberLength)
            End Get
        End Property
        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property
        Public ReadOnly Property Deleted() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Deleted
            Get
                Return New Field(DeletedStart, DeletedLength)
            End Get
        End Property
        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property
        Public ReadOnly Property PrintSize() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.PrintSize
            Get
                Return New Field(PrintSizeStart, PrintSizeLength)
            End Get
        End Property
        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
        Public ReadOnly Property Text() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.Text
            Get
                Return New Field(TextStart, TextLength)
            End Get
        End Property
        Public ReadOnly Property TextLineDisplaySequence() As [Interface].IFieldStructure Implements [Interface].IUtRecordStructure.TextLineDisplaySequence
            Get
                Return New Field(TextLineDisplaySequenceStart, TextLineDisplaySequenceLength)
            End Get
        End Property
#End Region

    End Structure
End Namespace

Namespace DataStructure.Implementation
    Public Structure Field
        Implements DataStructure.Interface.IFieldStructure

        Private _name As String
        Private _startPosition As Int16
        Private _length As Int16
        Friend Sub New(ByVal startPosition As Int16, ByVal length As Int16)
            Me._startPosition = startPosition
            Me._length = length
        End Sub
        Public Property Length() As Short Implements DataStructure.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property StartPosition() As Short Implements DataStructure.Interface.IFieldStructure.StartPosition
            Get
                Return _startPosition
            End Get
            Set(ByVal value As Short)
                _startPosition = value
            End Set
        End Property
    End Structure
    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = ""
            If RecordIsLongEnoughToContainField(record, field) Then
                val = record.Substring(field.StartPosition - 1, field.Length)
            Else
                val = ""
            End If
            Return val
        End Function

        Public Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As [Interface].IFieldStructure) As Boolean
            Return record.Length >= field.StartPosition + field.Length - 1
        End Function
    End Class

    Public Class UcRecord
        Inherits RecordFormat
        Implements [Interface].IUcRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UcRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UcRecordStructure
        End Sub
#Region "Properties"
        Public ReadOnly Property BuyQuantity() As String Implements [Interface].IUcRecord.BuyQuantity
            Get
                Return Me.FormattedValue(_record, _recordStructure.BuyQuantity)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUcRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeleteIndicator() As String Implements [Interface].IUcRecord.DeleteIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeleteIndicator)
            End Get
        End Property

        Public ReadOnly Property DiscountValue() As String Implements [Interface].IUcRecord.DiscountValue
            Get
                Return Me.FormattedValue(_record, _recordStructure.DiscountValue)
            End Get
        End Property

        Public ReadOnly Property EventNumber() As String Implements [Interface].IUcRecord.EventNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventNumber)
            End Get
        End Property

        Public ReadOnly Property EventType() As String Implements [Interface].IUcRecord.EventType
            Get
                Return Me.FormattedValue(_record, _recordStructure.EventType)
            End Get
        End Property

        Public ReadOnly Property GetQuantity() As String Implements [Interface].IUcRecord.GetQuantity
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetQuantity)
            End Get
        End Property

        Public ReadOnly Property GetQuantityDiscountPercentage() As String Implements [Interface].IUcRecord.GetQuantityDiscountPercentage
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetQuantityDiscountPercentage)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUcRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property ItemKey1() As String Implements [Interface].IUcRecord.ItemKey1
            Get
                Return Me.FormattedValue(_record, _recordStructure.ItemKey1)
            End Get
        End Property

        Public ReadOnly Property ItemKey2() As String Implements [Interface].IUcRecord.ItemKey2
            Get
                Return Me.FormattedValue(_record, _recordStructure.ItemKey2)
            End Get
        End Property

        Public ReadOnly Property Price() As String Implements [Interface].IUcRecord.Price
            Get
                Return Me.FormattedValue(_record, _recordStructure.Price)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUcRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property BuyCouponNumber() As String Implements [Interface].IUcRecordCoupon.BuyCouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.BuyCouponNumber)
            End Get
        End Property

        Public ReadOnly Property GetCouponNumber() As String Implements [Interface].IUcRecordCoupon.GetCouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.GetCouponNumber)
            End Get
        End Property
#End Region
    End Class
    Public Class UmRecord
        Inherits RecordFormat
        Implements [Interface].IUmRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UmRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UmRecordStructure
        End Sub
#Region "Properties"
        Public ReadOnly Property CollectCustomerInformationIndicator() As String Implements [Interface].IUmRecord.CollectCustomerInformationIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.CollectCustomerInformationIndicator)
            End Get
        End Property

        Public ReadOnly Property CouponNumber() As String Implements [Interface].IUmRecord.CouponNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.CouponNumber)
            End Get
        End Property
        Public ReadOnly Property CouponNumberDescription() As String Implements [Interface].IUmRecord.CouponDescription
            Get
                Return Me.FormattedValue(_record, _recordStructure.CouponDescription)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUmRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DeletedIndicator() As String Implements [Interface].IUmRecord.DeletedIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.DeletedIndicator)
            End Get
        End Property

        Public ReadOnly Property ExclusiveIndicator() As String Implements [Interface].IUmRecord.ExclusiveIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.ExclusiveIndicator)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUmRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUmRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property ReusableIndicator() As String Implements [Interface].IUmRecord.ReusableIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.ReusableIndicator)
            End Get
        End Property

        Public ReadOnly Property SerialNumberIndicator() As String Implements [Interface].IUmRecord.SerialNumberIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.SerialNumberIndicator)
            End Get
        End Property

        Public ReadOnly Property StoreGenIndicator() As String Implements [Interface].IUmRecord.StoreGenIndicator
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreGenIndicator)
            End Get
        End Property
#End Region
    End Class
    Public Class UtRecord
        Inherits RecordFormat
        Implements [Interface].IUtRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As UtRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New UtRecordStructure
        End Sub

        Public ReadOnly Property Alignment() As String Implements [Interface].IUtRecord.Alignment
            Get
                Return Me.FormattedValue(_record, _recordStructure.Alignment)
            End Get
        End Property

        Public ReadOnly Property CouponNumber() As String Implements [Interface].IUtRecord.CouponNumber
            Get
                Return FormattedValue(_record, _recordStructure.CouponNumber)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].IUtRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property Deleted() As String Implements [Interface].IUtRecord.Deleted
            Get
                Return Me.FormattedValue(_record, _recordStructure.Deleted)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].IUtRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property PrintSize() As String Implements [Interface].IUtRecord.PrintSize
            Get
                Return Me.FormattedValue(_record, _recordStructure.PrintSize)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IUtRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property Text() As String Implements [Interface].IUtRecord.Text
            Get
                Return Me.FormattedValue(_record, _recordStructure.Text)
            End Get
        End Property

        Public ReadOnly Property TextLineDisplaySequence() As String Implements [Interface].IUtRecord.TextLineDisplaySequence
            Get
                Return Me.FormattedValue(_record, _recordStructure.TextLineDisplaySequence)
            End Get
        End Property
    End Class
End Namespace

