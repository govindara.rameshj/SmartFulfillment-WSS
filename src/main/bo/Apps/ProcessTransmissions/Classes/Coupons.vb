﻿Public Class Coupons
    Implements ICoupons


    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)

    Private EventHDR As New BOEvent.cEventHeader(_Oasys3DB)
    Private ColEh As New List(Of BOEvent.cEventHeader)
    Private EventMAS As New BOEvent.cEvent(_Oasys3DB)
    Private ColEm As New List(Of BOEvent.cEvent)
    Private StockMaster As New BOStock.cStock(_Oasys3DB)
    Private CouponBO As New BOEvent.cCouponMaster(_Oasys3DB)
    Private CouponTextBO As New BOEvent.cCouponText(_Oasys3DB)

    Private strEventNumber As String = String.Empty
    Private strEventType As String = String.Empty
    Private strEventKey1 As String = String.Empty
    Private strEventKey2 As String = String.Empty
    Private strEventChangeNumber As String = String.Empty
    Private strEventChangeSkuNumber As String = String.Empty
    Private decEventChangePrice As Decimal = 0
    Private boolEventChangeDelete As Boolean = False
    Private boolCanAddOrUpdateEvent As Boolean = False
    Private boolCreateEventChange As Boolean = False
    Private boolIsAMMGEventChange As Boolean = False
    'Coupon's working variables
    Private CouponNo As String = String.Empty
    Private CouponSeq As String = String.Empty
    Private ChangesMade As Boolean = False
    Public Sub ProcessUCRecords(ByVal strTransmissionFileData As String) Implements ICoupons.ProcessUCRecords

        strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
        strEventNumber = strTransmissionFileData.Substring(22, 6).PadLeft(6, "0"c)
        EventHDR = New BOEvent.cEventHeader(_Oasys3DB)
        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            EventHDR.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHDR.EventNo, strEventNumber)
        Else
            EventHDR.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventHDR.EventNoID, strEventNumber)
        End If
        ColEh = EventHDR.LoadMatches()
        boolCanAddOrUpdateEvent = True

        If ColEh.Count > 0 Then ' EVENT HEADER EXISTS?
            strEventType = strTransmissionFileData.Substring(28, 2).ToUpper
            strEventKey1 = strTransmissionFileData.Substring(30, 6).PadLeft(6, "0"c)
            strEventKey2 = strTransmissionFileData.Substring(36, 8).PadLeft(8, "0"c)
            If strEventType = "PS" Or strEventType = "TS" Or strEventType = "QS" Or strEventType = "MS" Then
                StockMaster.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, StockMaster.SkuNumber, strEventKey1)
                StockMaster.Stocks = StockMaster.LoadMatches()
                If StockMaster.Stocks.Count < 1 Then 'Stock master does not exist so do not save
                    boolCanAddOrUpdateEvent = False
                End If
            End If
            If boolCanAddOrUpdateEvent = True Then
                If strEventType = "PS" Or strEventType = "PM" Or strEventType = "TS" Or strEventType = "TM" Then
                    'Key2 must be empty
                    If strEventKey2 <> "00000000" Then boolCanAddOrUpdateEvent = False
                Else
                    If strEventKey2 = "00000000" Then boolCanAddOrUpdateEvent = False
                End If
            End If
            If boolCanAddOrUpdateEvent = True Then
                strEventChangeNumber = strEventNumber
                strEventChangeSkuNumber = strEventKey1
                decEventChangePrice = CDec(strTransmissionFileData.Substring(44, 10))
                boolEventChangeDelete = False
                If strEventType = "PS" Then
                    If strTransmissionFileData.Substring(85, 1) = "Y" Then boolEventChangeDelete = True
                    boolCreateEventChange = True
                    boolIsAMMGEventChange = False
                End If
                If strEventType = "PM" Then
                    boolCreateEventChange = True
                    boolIsAMMGEventChange = True
                End If
                If boolCreateEventChange = False Then
                    Try
                        EventMAS.ClearLoadFilter()
                        EventMAS.ClearLists()
                        EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventNo, strEventNumber)
                        EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventKey1, strEventKey1)
                        EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventKey2, strEventKey2)
                        EventMAS.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
                        EventMAS.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, EventMAS.EventType, strEventType)
                        ColEm = EventMAS.LoadMatches()
                        If ColEm.Count < 1 Then
                            EventMAS.EventType.Value = strEventType
                            EventMAS.EventKey1.Value = strEventKey1
                            EventMAS.EventKey2.Value = strEventKey2
                            EventMAS.EventNo.Value = strEventNumber
                        End If
                        EventMAS.SpecialPrice.Value = CDec(strTransmissionFileData.Substring(44, 10))
                        EventMAS.BuyQuantity.Value = CDec(strTransmissionFileData.Substring(54, 7))
                        EventMAS.GetQuantity.Value = CDec(strTransmissionFileData.Substring(61, 7))
                        EventMAS.DiscountAmount.Value = CDec(strTransmissionFileData.Substring(68, 10))
                        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
                            EventMAS.DiscountPercentage.Value = CDec(strTransmissionFileData.Substring(78, 7))
                        Else
                            EventMAS.PercentageDiscount.Value = CDec(strTransmissionFileData.Substring(78, 7))
                        End If
                        EventMAS.Deleted.Value = False
                        If strTransmissionFileData.Substring(85, 1) = "Y" Then EventMAS.Deleted.Value = True
                        EventMAS.StartDate.Value = EventHDR.StartDate.Value
                        EventMAS.EndDate.Value = EventHDR.EndDate.Value
                        EventMAS.Priority.Value = EventHDR.Priority.Value
                        EventMAS.BuyCouponID.Value = strTransmissionFileData.Substring(86, 7)
                        EventMAS.SellCouponID.Value = strTransmissionFileData.Substring(93, 7)
                        If (EventMAS.BuyCouponID.Value.Trim.Length = 0) Then EventMAS.BuyCouponID.Value = "0000000"
                        If (EventMAS.SellCouponID.Value.Trim.Length = 0) Then EventMAS.SellCouponID.Value = "0000000"
                        If EventMAS.StartDate.Value <> Date.MinValue Or EventMAS.EndDate.Value <> Date.MinValue Then EventMAS.TimeOrDayRelated.Value = True
                        If ColEm.Count < 1 Then
                            EventMAS.SaveIfNew()
                        Else
                            EventMAS.SaveIfExists()
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If
        Else
            OutputProductUpdateError(strTransmissionFileData, "UC: No Event HDR".PadRight(18, " "c))
        End If ' EVENT HEADER EXISTS?
    End Sub

    Public Sub ProcessUMRecords(ByVal strTransmissionFileData As String) Implements ICoupons.ProcessUMRecords


        strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
        CouponNo = String.Empty & strTransmissionFileData.Substring(22, 7).PadLeft(7, "0"c)
        CouponBO.ClearLoadFilter()
        CouponBO.ClearLists()
        CouponBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponBO.CouponID, CouponNo)
        CouponBO.LoadBORecords()



        If CouponBO.Description.Value <> strTransmissionFileData.Substring(29, 20).ToUpper.Trim Then ChangesMade = True
        CouponBO.Description.Value = strTransmissionFileData.Substring(29, 20).ToUpper.Trim
        If CouponBO.StoreGenerated.Value <> (strTransmissionFileData.Substring(49, 1) = "Y") Then ChangesMade = True
        CouponBO.StoreGenerated.Value = (strTransmissionFileData.Substring(49, 1) = "Y")
        If CouponBO.SerialNo.Value <> (strTransmissionFileData.Substring(50, 1).ToUpper.Trim = "Y") Then ChangesMade = True
        CouponBO.SerialNo.Value = strTransmissionFileData.Substring(50, 1).ToUpper.Trim = "Y"
        If CouponBO.ExclusiveCoupon.Value <> (strTransmissionFileData.Substring(51, 1).ToUpper.Trim = "Y") Then ChangesMade = True
        CouponBO.ExclusiveCoupon.Value = (strTransmissionFileData.Substring(51, 1).ToUpper.Trim = "Y")
        If CouponBO.ReUsable.Value <> (strTransmissionFileData.Substring(52, 1).ToUpper.Trim = "Y") Then ChangesMade = True
        CouponBO.ReUsable.Value = strTransmissionFileData.Substring(52, 1).ToUpper.Trim = "Y"
        If CouponBO.CollectInfo.Value <> (strTransmissionFileData.Substring(53, 1).ToUpper.Trim = "Y") Then ChangesMade = True
        CouponBO.CollectInfo.Value = (strTransmissionFileData.Substring(53, 1).ToUpper.Trim = "Y")
        If CouponBO.Deleted.Value <> (strTransmissionFileData.Substring(54, 1).ToUpper.Trim = "Y") Then ChangesMade = True
        CouponBO.Deleted.Value = strTransmissionFileData.Substring(54, 1).ToUpper.Trim = "Y"
        Try
            If CouponBO.BORecords.Count = 0 Then
                CouponBO.CouponID.Value = CouponNo
                CouponBO.SaveIfNew()
            Else
                If ChangesMade Then CouponBO.SaveIfExists()
            End If
        Catch
            Throw
        End Try


    End Sub

    Public Sub ProcessUTRecords(ByVal strTransmissionFileData As String) Implements ICoupons.ProcessUTRecords

        strTransmissionFileData = strTransmissionFileData.PadRight(102, " "c)
        CouponNo = strTransmissionFileData.Substring(22, 7).PadLeft(7, "0"c)
        CouponSeq = strTransmissionFileData.Substring(29, 2).PadLeft(2, "0"c)
        CouponTextBO.ClearLoadFilter()
        CouponTextBO.ClearLists()
        CouponTextBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponTextBO.CouponID, CouponNo)
        CouponTextBO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        CouponTextBO.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, CouponTextBO.SequenceNo, CouponSeq)
        CouponTextBO.LoadBORecords()

        If CouponTextBO.PrintSize.Value <> strTransmissionFileData.Substring(31, 1).ToUpper Then ChangesMade = True
        CouponTextBO.PrintSize.Value = strTransmissionFileData.Substring(31, 1).ToUpper
        If CouponTextBO.TextAlignment.Value <> strTransmissionFileData.Substring(32, 1).ToUpper Then ChangesMade = True
        CouponTextBO.TextAlignment.Value = strTransmissionFileData.Substring(32, 1).ToUpper
        If CouponTextBO.PrintText.Value <> strTransmissionFileData.Substring(33, 52).Trim Then ChangesMade = True
        CouponTextBO.PrintText.Value = strTransmissionFileData.Substring(33, 51).Trim
        If CouponTextBO.Deleted.Value <> (strTransmissionFileData.Substring(89, 1).ToUpper = "Y") Then ChangesMade = True
        CouponTextBO.Deleted.Value = (strTransmissionFileData.Substring(89, 1).ToUpper = "Y")
        Try
            If CouponTextBO.BORecords.Count <= 0 Then
                CouponTextBO.CouponID.Value = CouponNo
                CouponTextBO.SequenceNo.Value = CouponSeq
                CouponTextBO.SaveIfNew()
            Else
                If ChangesMade Then CouponTextBO.SaveIfExists()
            End If
        Catch
            Throw
        End Try

    End Sub


#Region "Properties"


    Public ReadOnly Property CreateEventChange() As Boolean Implements ICoupons.CreateEventChange
        Get
            Return boolCreateEventChange
        End Get
    End Property

    Public ReadOnly Property EventChangeDelete() As Boolean Implements ICoupons.EventChangeDelete
        Get
            Return boolEventChangeDelete
        End Get
    End Property

    Public ReadOnly Property EventChangeNumber() As String Implements ICoupons.EventChangeNumber
        Get
            Return strEventChangeNumber
        End Get
    End Property

    Public ReadOnly Property EventChangePrice() As Decimal Implements ICoupons.EventChangePrice
        Get
            Return decEventChangePrice
        End Get
    End Property

    Public ReadOnly Property EventChangeSkuNumber() As String Implements ICoupons.EventChangeSkuNumber
        Get
            Return strEventChangeSkuNumber
        End Get
    End Property

    Public ReadOnly Property EventHeaderEndDate() As Nullable(Of Date) Implements ICoupons.EventHeaderEndDate
        Get
            Return EventHDR.EndDate.Value
        End Get
    End Property

    Public ReadOnly Property EventHeaderPriority() As String Implements ICoupons.EventHeaderPriority
        Get
            Return EventHDR.Priority.Value
        End Get
    End Property

    Public ReadOnly Property EventHeaderStartDate() As Nullable(Of Date) Implements ICoupons.EventHeaderStartDate
        Get
            Return EventHDR.StartDate.Value
        End Get
    End Property

    Public ReadOnly Property EventNumber() As String Implements ICoupons.EventNumber
        Get
            Return strEventNumber
        End Get
    End Property

    Public ReadOnly Property MMGEventChange() As Boolean Implements ICoupons.MMGEventChange
        Get
            Return boolIsAMMGEventChange
        End Get
    End Property
#End Region

End Class
