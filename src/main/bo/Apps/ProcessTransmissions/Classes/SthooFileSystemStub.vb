﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ProcessTransmissions.UnitTest")> 
Namespace TpWickes

    Public Class SthooFileSystemStub
        Implements ISthooFileSystem

#Region "Stub Code"

        Private _LineText As New List(Of String)
        Private _FileName As String

        Friend ReadOnly Property StubLineText() As List(Of String)

            Get
                Return _LineText
            End Get

        End Property

        Friend Property StubFileName() As String

            Get
                Return _FileName
            End Get

            Set(ByVal value As String)
                _FileName = value
            End Set

        End Property

#End Region

#Region "Interface"

        Public ReadOnly Property FileName() As String Implements ISthooFileSystem.FileName
            Get

                Return _FileName

            End Get
        End Property

        Public Sub WriteToFile(ByVal Line As String) Implements ISthooFileSystem.WriteToFile

            _LineText.Add(Line)

        End Sub

#End Region

    End Class

End Namespace