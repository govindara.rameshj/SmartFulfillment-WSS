﻿Public Class SthotFormatNew
    Implements ISthotFormat

    Private Const PaddingLength As Integer = 8
    Private Const PaddingCharacter As String = "0"

    Public Function FormatSthotDocumentNumber(ByVal formatString As String) As String Implements ISthotFormat.FormatSthotDocumentNumber
        formatString = formatString.Trim()
        If formatString.Length >= PaddingLength Then
            formatString = formatString.Substring(0, PaddingLength)
        End If

        Return formatString.PadLeft(PaddingLength, CChar(PaddingCharacter))
    End Function
End Class
