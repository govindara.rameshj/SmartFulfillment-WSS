﻿Public Class FieldStructure
    Implements IFieldStructure

    Private _startPosition As Integer
    Private _length As Integer

    Friend Sub New(ByVal startPosition As Integer, ByVal length As Integer)
        Me._startPosition = startPosition
        Me._length = length
    End Sub

    Public Property Length() As Integer Implements IFieldStructure.Length
        Get
            Return _length
        End Get
        Set(ByVal value As Integer)
            _length = value
        End Set
    End Property

    Public Property StartPosition() As Integer Implements IFieldStructure.StartPosition
        Get
            Return _startPosition
        End Get
        Set(ByVal value As Integer)
            _startPosition = value
        End Set
    End Property

End Class

Public Class RecordFormat
    Implements IRecordFormat

    Public Function FormattedValue(ByVal record As String, ByVal field As IFieldStructure) As String Implements IRecordFormat.FormattedValue

        Dim val As String

        If RecordIsLongEnoughToContainField(record, field) Then
            val = record.Substring(field.StartPosition - 1, field.Length).Trim
        Else

            'val = String.Empty
            val = record.Substring(field.StartPosition - 1).Trim

        End If

        Return val

    End Function

    Private Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As IFieldStructure) As Boolean Implements IRecordFormat.RecordIsLongEnoughToContainField

        Return record.Length >= field.StartPosition + field.Length - 1

    End Function

End Class