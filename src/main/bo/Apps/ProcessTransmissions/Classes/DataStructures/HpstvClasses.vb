﻿Namespace HPSTV.Classes

    'HR    Header Record
    'VM    Supplier Master Record
    'VC    Supplier Closedown Record
    'VN    Supplier Narrative Record
    'VD    Supplier Order Depot Record
    'VH    Delivery Center Update Record
    'VR    Supplier Returns Depot Record
    'VQ    Supplier Minimum Order Qty Record
    'VT    Supplier order Type Detail Record
    'TR    Trailer Record

#Region "Record Type - VM | Not Implemented"

    Public Class VmRecord

        'not implemented

    End Class

    Public Class VmRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VC | Not Implemented"

    Public Class VcRecord

        'not implemented

    End Class

    Public Class VcRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VN | Not Implemented"

    Public Class VnRecord

        'not implemented

    End Class

    Public Class VnRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VD | Not Implemented"

    Public Class VdRecord

        'not implemented

    End Class

    Public Class VdRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VH | Not Implemented"

    Public Class VhRecord

        'not implemented

    End Class

    Public Class VhRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VR | Not Implemented"

    Public Class VrRecord

        'not implemented

    End Class

    Public Class VrRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VQ | Not Implemented"

    Public Class VqRecord

        'not implemented

    End Class

    Public Class VqRecordStructure

        'not implemented

    End Class

#End Region

#Region "Record Type - VT"

    'DATA     A2  : Record Type
    'DATA+2   A8  : Update Date
    'DATA+10  A12 : 10.2 Signed numeric - For Trailer Hash
    'DATA+22  A1  : Deletion indicator
    'DATA+23  A5  : Supplier Number
    'DATA+28  A8  : Effective Date
    'DATA+36  A1  : Order Type:  Y = Tradanet
    'DATA+37  A3  : BBC Site Number (173)
    '
    'DATA+40  A1  : BBC Order Type:
    '                    D     = Discreet              
    '                    C     = Consolidated          
    '                    W     = Warehouse             
    '                    A     = Alternative Supplier  
    '                    Blank = Direct                
    '
    'DATA+41  A4  : Order Day Code - Binary Hash (1 - 127)
    '                    1  = Monday
    '                    2  = Tuesday
    '                    4  = Wednesday
    '                    8  = Thursday
    '                    16 = Friday
    '                    32 = Saturday
    '                    64 = Sunday
    '
    '                    Any comination of days is shown as a sum of the relevant day numbers.                  
    '
    'DATA+45  A4  : Lead Time - Monday      (1 - 999)
    'DATA+49  A4  : Lead Time - Tuesday     (1 - 999)
    'DATA+53  A4  : Lead Time - Wednesday   (1 - 999)
    'DATA+57  A4  : Lead Time - Thursday    (1 - 999)
    'DATA+61  A4  : Lead Time - Friday      (1 - 999)
    'DATA+65  A4  : Lead Time - Saturday    (1 - 999)
    'DATA+69  A4  : Lead Time - Sunday      (1 - 999)
    '
    '                    The Lead Time of days is shown as a sum      
    '
    'DATA+73  A1  : Delivery Check Method
    '                    1 = Cartons
    '                    2 = Detail Check

    Public Class VtRecord
        Inherits RecordFormat
        Implements HPSTV.Interface.IVtRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As VtRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New VtRecordStructure
        End Sub

        Public ReadOnly Property BbcOrderType() As String Implements HPSTV.Interface.IVtRecord.BbcOrderType
            Get
                Return FormattedValue(_record, _recordStructure.BbcOrderType)
            End Get
        End Property

        Public ReadOnly Property BbcSiteNumber() As String Implements HPSTV.Interface.IVtRecord.BbcSiteNumber
            Get
                Return FormattedValue(_record, _recordStructure.BbcSiteNumber)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator() As String Implements HPSTV.Interface.IVtRecord.DeletionIndicator
            Get
                Return FormattedValue(_record, _recordStructure.DeletionIndicator)
            End Get
        End Property

        Public ReadOnly Property DeliveryCheckMethod() As String Implements HPSTV.Interface.IVtRecord.DeliveryCheckMethod
            Get
                Return FormattedValue(_record, _recordStructure.DeliveryCheckMethod)
            End Get
        End Property

        Public ReadOnly Property EffectiveDate() As String Implements HPSTV.Interface.IVtRecord.EffectiveDate
            Get
                Return FormattedValue(_record, _recordStructure.EffectiveDate)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements HPSTV.Interface.IVtRecord.Hash
            Get
                Return FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property LeadTimeFriday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeFriday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeFriday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeMonday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeMonday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeMonday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeSaturday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeSaturday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeSaturday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeSunday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeSunday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeSunday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeThursday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeThursday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeThursday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeTuesday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeTuesday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeTuesday)
            End Get
        End Property

        Public ReadOnly Property LeadTimeWednesday() As String Implements HPSTV.Interface.IVtRecord.LeadTimeWednesday
            Get
                Return FormattedValue(_record, _recordStructure.LeadTimeWednesday)
            End Get
        End Property

        Public ReadOnly Property OrderDayCode() As String Implements HPSTV.Interface.IVtRecord.OrderDayCode
            Get
                Return FormattedValue(_record, _recordStructure.OrderDayCode)
            End Get
        End Property

        Public ReadOnly Property OrderType() As String Implements HPSTV.Interface.IVtRecord.OrderType
            Get
                Return FormattedValue(_record, _recordStructure.OrderType)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements HPSTV.Interface.IVtRecord.RecordType
            Get
                Return FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SupplierNumber() As String Implements HPSTV.Interface.IVtRecord.SupplierNumber
            Get
                Return FormattedValue(_record, _recordStructure.SupplierNumber)
            End Get
        End Property

        Public ReadOnly Property UpdateDate() As String Implements HPSTV.Interface.IVtRecord.UpdateDate
            Get
                Return FormattedValue(_record, _recordStructure.UpdateDate)
            End Get
        End Property

    End Class

    Public Class VtRecordStructure
        Implements HPSTV.Interface.IVtRecordStructure

        Private Const RecordTypeStart As Integer = 1                      'DATA     A2  : Record Type
        Private Const RecordTypeLength As Integer = 2
        Private Const UpdateDateStart As Integer = 3                      'DATA+2   A8  : Update Date
        Private Const UpdateDateLength As Integer = 8
        Private Const HashStart As Integer = 11                           'DATA+10  A12 : 10.2 Signed numeric - For Trailer Hash
        Private Const HashLength As Integer = 12
        Private Const DeletionIndicatorStart As Integer = 23              'DATA+22  A1  : Deletion indicator
        Private Const DeletionIndicatorLength As Integer = 1
        Private Const SupplierNumberStart As Integer = 24                 'DATA+23  A5  : Supplier Number
        Private Const SupplierNumberLength As Integer = 5
        Private Const EffectiveDateStart As Integer = 29                  'DATA+28  A8  : Effective Date
        Private Const EffectiveDateLength As Integer = 8
        Private Const OrderTypeStart As Integer = 37                      'DATA+36  A1  : Order Type:  Y = Tradanet
        Private Const OrderTypeLength As Integer = 1
        Private Const BbcSiteNumberStart As Integer = 38                  'DATA+37  A3  : BBC Site Number (173)
        Private Const BbcSiteNumberLength As Integer = 3
        Private Const BbcOrderTypeStart As Integer = 41                   'DATA+40  A1  : BBC Order Type:
        Private Const BbcOrderTypeLength As Integer = 1
        Private Const OrderDayCodeStart As Integer = 42                   'DATA+41  A4  : Order Day Code - Binary Hash (1 - 127)
        Private Const OrderDayCodeLength As Integer = 4
        Private Const LeadTimeMondayStart As Integer = 46                 'DATA+45  A4  : Lead Time - Monday      ( 1 - 999 )
        Private Const LeadTimeMondayLength As Integer = 4
        Private Const LeadTimeTuesdayStart As Integer = 50                'DATA+49  A4  : Lead Time - Tuesday     (1 - 999)
        Private Const LeadTimeTuesdayLength As Integer = 4
        Private Const LeadTimeWednesdayStart As Integer = 54              'DATA+53  A4  : Lead Time - Wednesday   (1 - 999)
        Private Const LeadTimeWednesdayLength As Integer = 4
        Private Const LeadTimeThursdayStart As Integer = 58               'DATA+57  A4  : Lead Time - Thursday    (1 - 999)
        Private Const LeadTimeThursdayLength As Integer = 4
        Private Const LeadTimeFridayStart As Integer = 62                 'DATA+61  A4  : Lead Time - Friday      (1 - 999)
        Private Const LeadTimeFridayLength As Integer = 4
        Private Const LeadTimeSaturdayStart As Integer = 66               'DATA+65  A4  : Lead Time - Saturday    (1 - 999)
        Private Const LeadTimeSaturdayLength As Integer = 4
        Private Const LeadTimeSundayStart As Integer = 70                 'DATA+69  A4  : Lead Time - Sunday      (1 - 999)
        Private Const LeadTimeSundayLength As Integer = 4
        Private Const DeliveryCheckMethodStart As Integer = 74            'DATA+73  A1  : Delivery Check Method
        Private Const DeliveryCheckMethodLength As Integer = 1

        Public ReadOnly Property BbcOrderType() As IFieldStructure Implements [Interface].IVtRecordStructure.BbcOrderType
            Get
                Return New FieldStructure(BbcOrderTypeStart, BbcOrderTypeLength)
            End Get
        End Property

        Public ReadOnly Property BbcSiteNumber() As IFieldStructure Implements [Interface].IVtRecordStructure.BbcSiteNumber
            Get
                Return New FieldStructure(BbcSiteNumberStart, BbcSiteNumberLength)
            End Get
        End Property

        Public ReadOnly Property DeletionIndicator() As IFieldStructure Implements [Interface].IVtRecordStructure.DeletionIndicator
            Get
                Return New FieldStructure(DeletionIndicatorStart, DeletionIndicatorLength)
            End Get
        End Property

        Public ReadOnly Property DeliveryCheckMethod() As IFieldStructure Implements [Interface].IVtRecordStructure.DeliveryCheckMethod
            Get
                Return New FieldStructure(DeliveryCheckMethodStart, DeliveryCheckMethodLength)
            End Get
        End Property

        Public ReadOnly Property EffectiveDate() As IFieldStructure Implements [Interface].IVtRecordStructure.EffectiveDate
            Get
                Return New FieldStructure(EffectiveDateStart, EffectiveDateLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As IFieldStructure Implements [Interface].IVtRecordStructure.Hash
            Get
                Return New FieldStructure(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeFriday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeFriday
            Get
                Return New FieldStructure(LeadTimeFridayStart, LeadTimeFridayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeMonday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeMonday
            Get
                Return New FieldStructure(LeadTimeMondayStart, LeadTimeMondayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeSaturday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeSaturday
            Get
                Return New FieldStructure(LeadTimeSaturdayStart, LeadTimeSaturdayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeSunday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeSunday
            Get
                Return New FieldStructure(LeadTimeSundayStart, LeadTimeSundayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeThursday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeThursday
            Get
                Return New FieldStructure(LeadTimeThursdayStart, LeadTimeThursdayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeTuesday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeTuesday
            Get
                Return New FieldStructure(LeadTimeTuesdayStart, LeadTimeTuesdayLength)
            End Get
        End Property

        Public ReadOnly Property LeadTimeWednesday() As IFieldStructure Implements [Interface].IVtRecordStructure.LeadTimeWednesday
            Get
                Return New FieldStructure(LeadTimeWednesdayStart, LeadTimeWednesdayLength)
            End Get
        End Property

        Public ReadOnly Property OrderDayCode() As IFieldStructure Implements [Interface].IVtRecordStructure.OrderDayCode
            Get
                Return New FieldStructure(OrderDayCodeStart, OrderDayCodeLength)
            End Get
        End Property

        Public ReadOnly Property OrderType() As IFieldStructure Implements [Interface].IVtRecordStructure.OrderType
            Get
                Return New FieldStructure(OrderTypeStart, OrderTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As IFieldStructure Implements [Interface].IVtRecordStructure.RecordType
            Get
                Return New FieldStructure(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SupplierNumber() As IFieldStructure Implements [Interface].IVtRecordStructure.SupplierNumber
            Get
                Return New FieldStructure(SupplierNumberStart, SupplierNumberLength)
            End Get
        End Property

        Public ReadOnly Property UpdateDate() As IFieldStructure Implements [Interface].IVtRecordStructure.UpdateDate
            Get
                Return New FieldStructure(UpdateDateStart, UpdateDateLength)
            End Get
        End Property

    End Class

#End Region

End Namespace