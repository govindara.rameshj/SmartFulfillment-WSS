﻿Public Class TrailerRecord
    Inherits RecordFormat
    Implements ITrailerRecord

    Private ReadOnly _record As String
    Private ReadOnly _recordStructure As TrailerRecordStructure

    Public Sub New(ByVal record As String)
        _record = record
        _recordStructure = New TrailerRecordStructure
    End Sub

    Public ReadOnly Property Bucket1RecordCount() As String Implements ITrailerRecord.Bucket1RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket1RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket1RecordHashValue() As String Implements ITrailerRecord.Bucket1RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket1RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket1RecordType() As String Implements ITrailerRecord.Bucket1RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket1RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordCount() As String Implements ITrailerRecord.Bucket2RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket2RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordHashValue() As String Implements ITrailerRecord.Bucket2RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket2RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordType() As String Implements ITrailerRecord.Bucket2RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket2RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordCount() As String Implements ITrailerRecord.Bucket3RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket3RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordHashValue() As String Implements ITrailerRecord.Bucket3RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket3RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordType() As String Implements ITrailerRecord.Bucket3RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket3RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordCount() As String Implements ITrailerRecord.Bucket4RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket4RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordHashValue() As String Implements ITrailerRecord.Bucket4RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket4RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordType() As String Implements ITrailerRecord.Bucket4RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket4RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordCount() As String Implements ITrailerRecord.Bucket5RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket5RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordHashValue() As String Implements ITrailerRecord.Bucket5RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket5RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordType() As String Implements ITrailerRecord.Bucket5RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket5RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordCount() As String Implements ITrailerRecord.Bucket6RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket6RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordHashValue() As String Implements ITrailerRecord.Bucket6RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket6RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordType() As String Implements ITrailerRecord.Bucket6RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket6RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordCount() As String Implements ITrailerRecord.Bucket7RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket7RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordHashValue() As String Implements ITrailerRecord.Bucket7RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket7RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordType() As String Implements ITrailerRecord.Bucket7RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket7RecordType)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordCount() As String Implements ITrailerRecord.Bucket8RecordCount
        Get
            Return FormattedValue(_record, _recordStructure.Bucket8RecordCount)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordHashValue() As String Implements ITrailerRecord.Bucket8RecordHashValue
        Get
            Return FormattedValue(_record, _recordStructure.Bucket8RecordHashValue)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordType() As String Implements ITrailerRecord.Bucket8RecordType
        Get
            Return FormattedValue(_record, _recordStructure.Bucket8RecordType)
        End Get
    End Property

    Public ReadOnly Property [Date]() As String Implements ITrailerRecord.Date
        Get
            Return FormattedValue(_record, _recordStructure.Date)
        End Get
    End Property

    Public ReadOnly Property FileType() As String Implements ITrailerRecord.FileType
        Get
            Return FormattedValue(_record, _recordStructure.FileType)
        End Get
    End Property

    Public ReadOnly Property RecordType() As String Implements ITrailerRecord.RecordType
        Get
            Return FormattedValue(_record, _recordStructure.RecordType)
        End Get
    End Property

    Public ReadOnly Property SequenceNumber() As String Implements ITrailerRecord.SequenceNumber
        Get
            Return FormattedValue(_record, _recordStructure.SequenceNumber)
        End Get
    End Property

    Public ReadOnly Property StoreNumber() As String Implements ITrailerRecord.StoreNumber
        Get
            Return FormattedValue(_record, _recordStructure.StoreNumber)
        End Get
    End Property

    Public ReadOnly Property Time() As String Implements ITrailerRecord.Time
        Get
            Return FormattedValue(_record, _recordStructure.Time)
        End Get
    End Property

    Public ReadOnly Property VersionNumber() As String Implements ITrailerRecord.VersionNumber
        Get
            Return FormattedValue(_record, _recordStructure.VersionNumber)
        End Get
    End Property

End Class

Public Class TrailerRecordStructure
    Implements ITrailerRecordStructure

    Private Const RecordTypeStart As Integer = 1                     'DATA    A2  : Trailer Record
    Private Const RecordTypeLength As Integer = 2
    Private Const StoreNumberStart As Integer = 3                    'DATA+2  A3  : Store Number
    Private Const StoreNumberLength As Integer = 3
    Private Const DateStart As Integer = 6                           'DATA+5  A8  : Date
    Private Const DateLength As Integer = 8
    Private Const FileTypeStart As Integer = 14                      'DATA+13 A5  : File Type
    Private Const FileTypeLength As Integer = 5
    Private Const VersionNumberStart As Integer = 19                 'DATA+18 A2  : Version Number
    Private Const VersionNumberLength As Integer = 2
    Private Const SequenceNumberStart As Integer = 21                'DATA+20 A6  : Sequence Number
    Private Const SequenceNumberLength As Integer = 6
    Private Const TimeStart As Integer = 27                          'DATA+26 A6  : Time
    Private Const TimeLength As Integer = 6

    Private Const Bucket1RecordTypeStart As Integer = 33             'DATA+32 A2  : Bucket 1 Record Type
    Private Const Bucket1RecordTypeLength As Integer = 2
    Private Const Bucket1RecordCountStart As Integer = 35            'DATA+34 A7  : Bucket 1 Record Count  6.0
    Private Const Bucket1RecordCountLength As Integer = 7
    Private Const Bucket1RecordHashValueStart As Integer = 42        'DATA+41 A12 : Bucket 1 Record Value 10.2
    Private Const Bucket1RecordHashValueLength As Integer = 12

    Private Const Bucket2RecordTypeStart As Integer = 54             'DATA+32+(21*1) A2  : Bucket 2 Record Type
    Private Const Bucket2RecordTypeLength As Integer = 2
    Private Const Bucket2RecordCountStart As Integer = 56            'DATA+34+(21*1) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket2RecordCountLength As Integer = 7
    Private Const Bucket2RecordHashValueStart As Integer = 63        'DATA+41+(21*1) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket2RecordHashValueLength As Integer = 12

    Private Const Bucket3RecordTypeStart As Integer = 75             'DATA+32+(21*2) A2  : Bucket 2 Record Type
    Private Const Bucket3RecordTypeLength As Integer = 2
    Private Const Bucket3RecordCountStart As Integer = 77            'DATA+34+(21*2) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket3RecordCountLength As Integer = 7
    Private Const Bucket3RecordHashValueStart As Integer = 84        'DATA+41+(21*2) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket3RecordHashValueLength As Integer = 12

    Private Const Bucket4RecordTypeStart As Integer = 96             'DATA+32+(21*3) A2  : Bucket 2 Record Type
    Private Const Bucket4RecordTypeLength As Integer = 2
    Private Const Bucket4RecordCountStart As Integer = 98            'DATA+34+(21*3) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket4RecordCountLength As Integer = 7
    Private Const Bucket4RecordHashValueStart As Integer = 105       'DATA+41+(21*3) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket4RecordHashValueLength As Integer = 12

    Private Const Bucket5RecordTypeStart As Integer = 117             'DATA+32+(21*4) A2  : Bucket 2 Record Type
    Private Const Bucket5RecordTypeLength As Integer = 2
    Private Const Bucket5RecordCountStart As Integer = 119            'DATA+34+(21*4) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket5RecordCountLength As Integer = 7
    Private Const Bucket5RecordHashValueStart As Integer = 126        'DATA+41+(21*4) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket5RecordHashValueLength As Integer = 12

    Private Const Bucket6RecordTypeStart As Integer = 138             'DATA+32+(21*5) A2  : Bucket 2 Record Type
    Private Const Bucket6RecordTypeLength As Integer = 2
    Private Const Bucket6RecordCountStart As Integer = 140            'DATA+34+(21*5) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket6RecordCountLength As Integer = 7
    Private Const Bucket6RecordHashValueStart As Integer = 147        'DATA+41+(21*5) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket6RecordHashValueLength As Integer = 12

    Private Const Bucket7RecordTypeStart As Integer = 159             'DATA+32+(21*6) A2  : Bucket 2 Record Type
    Private Const Bucket7RecordTypeLength As Integer = 2
    Private Const Bucket7RecordCountStart As Integer = 161            'DATA+34+(21*6) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket7RecordCountLength As Integer = 7
    Private Const Bucket7RecordHashValueStart As Integer = 168        'DATA+41+(21*6) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket7RecordHashValueLength As Integer = 12

    Private Const Bucket8RecordTypeStart As Integer = 180             'DATA+32+(21*7) A2  : Bucket 2 Record Type
    Private Const Bucket8RecordTypeLength As Integer = 2
    Private Const Bucket8RecordCountStart As Integer = 182            'DATA+34+(21*7) A7  : Bucket 2 Record Count  6.0
    Private Const Bucket8RecordCountLength As Integer = 7
    Private Const Bucket8RecordHashValueStart As Integer = 189        'DATA+41+(21*7) A12 : Bucket 2 Record Value 10.2
    Private Const Bucket8RecordHashValueLength As Integer = 12

    Public ReadOnly Property Bucket1RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket1RecordCount
        Get
            Return New FieldStructure(Bucket1RecordCountStart, Bucket1RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket1RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket1RecordHashValue
        Get
            Return New FieldStructure(Bucket1RecordHashValueStart, Bucket1RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket1RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket1RecordType
        Get
            Return New FieldStructure(Bucket1RecordTypeStart, Bucket1RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket2RecordCount
        Get
            Return New FieldStructure(Bucket2RecordCountStart, Bucket2RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket2RecordHashValue
        Get
            Return New FieldStructure(Bucket2RecordHashValueStart, Bucket2RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket2RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket2RecordType
        Get
            Return New FieldStructure(Bucket2RecordTypeStart, Bucket2RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket3RecordCount
        Get
            Return New FieldStructure(Bucket3RecordCountStart, Bucket3RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket3RecordHashValue
        Get
            Return New FieldStructure(Bucket3RecordHashValueStart, Bucket3RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket3RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket3RecordType
        Get
            Return New FieldStructure(Bucket3RecordTypeStart, Bucket3RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket4RecordCount
        Get
            Return New FieldStructure(Bucket4RecordCountStart, Bucket4RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket4RecordHashValue
        Get
            Return New FieldStructure(Bucket4RecordHashValueStart, Bucket4RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket4RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket4RecordType
        Get
            Return New FieldStructure(Bucket4RecordTypeStart, Bucket4RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket5RecordCount
        Get
            Return New FieldStructure(Bucket5RecordCountStart, Bucket5RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket5RecordHashValue
        Get
            Return New FieldStructure(Bucket5RecordHashValueStart, Bucket5RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket5RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket5RecordType
        Get
            Return New FieldStructure(Bucket5RecordTypeStart, Bucket5RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket6RecordCount
        Get
            Return New FieldStructure(Bucket6RecordCountStart, Bucket6RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket6RecordHashValue
        Get
            Return New FieldStructure(Bucket6RecordHashValueStart, Bucket6RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket6RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket6RecordType
        Get
            Return New FieldStructure(Bucket6RecordTypeStart, Bucket6RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket7RecordCount
        Get
            Return New FieldStructure(Bucket7RecordCountStart, Bucket7RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket7RecordHashValue
        Get
            Return New FieldStructure(Bucket7RecordHashValueStart, Bucket7RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket7RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket7RecordType
        Get
            Return New FieldStructure(Bucket7RecordTypeStart, Bucket7RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordCount() As IFieldStructure Implements ITrailerRecordStructure.Bucket8RecordCount
        Get
            Return New FieldStructure(Bucket8RecordCountStart, Bucket8RecordCountLength)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordHashValue() As IFieldStructure Implements ITrailerRecordStructure.Bucket8RecordHashValue
        Get
            Return New FieldStructure(Bucket8RecordHashValueStart, Bucket8RecordHashValueLength)
        End Get
    End Property

    Public ReadOnly Property Bucket8RecordType() As IFieldStructure Implements ITrailerRecordStructure.Bucket8RecordType
        Get
            Return New FieldStructure(Bucket8RecordTypeStart, Bucket8RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property [Date]() As IFieldStructure Implements ITrailerRecordStructure.Date
        Get
            Return New FieldStructure(DateStart, DateLength)
        End Get
    End Property

    Public ReadOnly Property FileType() As IFieldStructure Implements ITrailerRecordStructure.FileType
        Get
            Return New FieldStructure(FileTypeStart, FileTypeLength)
        End Get
    End Property

    Public ReadOnly Property RecordType() As IFieldStructure Implements ITrailerRecordStructure.RecordType
        Get
            Return New FieldStructure(RecordTypeStart, RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property SequenceNumber() As IFieldStructure Implements ITrailerRecordStructure.SequenceNumber
        Get
            Return New FieldStructure(SequenceNumberStart, SequenceNumberLength)
        End Get
    End Property

    Public ReadOnly Property StoreNumber() As IFieldStructure Implements ITrailerRecordStructure.StoreNumber
        Get
            Return New FieldStructure(StoreNumberStart, StoreNumberLength)
        End Get
    End Property

    Public ReadOnly Property Time() As IFieldStructure Implements ITrailerRecordStructure.Time
        Get
            Return New FieldStructure(TimeStart, TimeLength)
        End Get
    End Property

    Public ReadOnly Property VersionNumber() As IFieldStructure Implements ITrailerRecordStructure.VersionNumber
        Get
            Return New FieldStructure(VersionNumberStart, VersionNumberLength)
        End Get
    End Property

End Class