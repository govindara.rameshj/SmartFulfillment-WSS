﻿Public Class HeaderRecord
    Inherits RecordFormat
    Implements IHeaderRecord

    Private ReadOnly _record As String
    Private ReadOnly _recordStructure As HeaderRecordStructure

    Public Sub New(ByVal record As String)
        _record = record
        _recordStructure = New HeaderRecordStructure
    End Sub

    Public ReadOnly Property [Date]() As String Implements IHeaderRecord.Date
        Get
            Return Me.FormattedValue(_record, _recordStructure.Date)
        End Get
    End Property

    Public ReadOnly Property FileDescription() As String Implements IHeaderRecord.FileDescription
        Get
            Return Me.FormattedValue(_record, _recordStructure.FileDescription)
        End Get
    End Property

    Public ReadOnly Property FileType() As String Implements IHeaderRecord.FileType
        Get
            Return Me.FormattedValue(_record, _recordStructure.FileType)
        End Get
    End Property

    Public ReadOnly Property RecordType() As String Implements IHeaderRecord.RecordType
        Get
            Return Me.FormattedValue(_record, _recordStructure.RecordType)
        End Get
    End Property

    Public ReadOnly Property SequenceNumber() As String Implements IHeaderRecord.SequenceNumber
        Get
            Return Me.FormattedValue(_record, _recordStructure.SequenceNumber)
        End Get
    End Property

    Public ReadOnly Property StoreNumber() As String Implements IHeaderRecord.StoreNumber
        Get
            Return Me.FormattedValue(_record, _recordStructure.StoreNumber)
        End Get
    End Property

    Public ReadOnly Property Time() As String Implements IHeaderRecord.Time
        Get
            Return Me.FormattedValue(_record, _recordStructure.Time)
        End Get
    End Property

    Public ReadOnly Property VersionNumber() As String Implements IHeaderRecord.VersionNumber
        Get
            Return Me.FormattedValue(_record, _recordStructure.VersionNumber)
        End Get
    End Property

End Class

Public Class HeaderRecordStructure
    Implements IHeaderReaderStructure

    Private Const RecordTypeStart As Integer = 1                'DATA    A2  : Header Record
    Private Const RecordTypeLength As Integer = 2
    Private Const StoreNumberStart As Integer = 3               'DATA+2  A3  : Store Number
    Private Const StoreNumberLength As Integer = 3
    Private Const DateStart As Integer = 6                      'DATA+5  A8  : Date
    Private Const DateLength As Integer = 8
    Private Const FileTypeStart As Integer = 14                 'DATA+13 A5  : File Type
    Private Const FileTypeLength As Integer = 5
    Private Const VersionNumberStart As Integer = 19            'DATA+18 A2  : Version Number
    Private Const VersionNumberLength As Integer = 2
    Private Const SequenceNumberStart As Integer = 21           'DATA+20 A6  : Sequence Number
    Private Const SequenceNumberLength As Integer = 6
    Private Const TimeStart As Integer = 27                     'DATA+26 A6  : Time
    Private Const TimeLength As Integer = 6
    Private Const FileDescriptionStart As Integer = 33          'DATA+32 A10 : File  - LOAD or UPDATE
    Private Const FileDescriptionLength As Integer = 10

    Public ReadOnly Property [Date]() As IFieldStructure Implements IHeaderReaderStructure.Date
        Get
            Return New FieldStructure(DateStart, DateLength)
        End Get
    End Property

    Public ReadOnly Property FileDescription() As IFieldStructure Implements IHeaderReaderStructure.FileDescription
        Get
            Return New FieldStructure(FileDescriptionStart, FileDescriptionLength)
        End Get
    End Property

    Public ReadOnly Property FileType() As IFieldStructure Implements IHeaderReaderStructure.FileType
        Get
            Return New FieldStructure(FileTypeStart, FileTypeLength)
        End Get
    End Property

    Public ReadOnly Property RecordType() As IFieldStructure Implements IHeaderReaderStructure.RecordType
        Get
            Return New FieldStructure(RecordTypeStart, RecordTypeLength)
        End Get
    End Property

    Public ReadOnly Property SequenceNumber() As IFieldStructure Implements IHeaderReaderStructure.SequenceNumber
        Get
            Return New FieldStructure(SequenceNumberStart, SequenceNumberLength)
        End Get
    End Property

    Public ReadOnly Property StoreNumber() As IFieldStructure Implements IHeaderReaderStructure.StoreNumber
        Get
            Return New FieldStructure(StoreNumberStart, StoreNumberLength)
        End Get
    End Property

    Public ReadOnly Property Time() As IFieldStructure Implements IHeaderReaderStructure.Time
        Get
            Return New FieldStructure(TimeStart, TimeLength)
        End Get
    End Property

    Public ReadOnly Property VersionNumber() As IFieldStructure Implements IHeaderReaderStructure.VersionNumber
        Get
            Return New FieldStructure(VersionNumberStart, VersionNumberLength)
        End Get
    End Property

End Class