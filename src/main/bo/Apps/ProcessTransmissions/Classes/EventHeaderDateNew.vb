﻿Public Class EventHeaderDateNew
    Implements ICouponsEventHeaderDate

    Friend Shared Sub SetEventHeaderStartDate(ByVal startDate As Date?, ByRef eventHeader As BOEvent.cEventHeader)
        With eventHeader.StartDate
            If startDate.HasValue Then
                .Value = startDate.Value
            Else
                .Value = Nothing
            End If
        End With
    End Sub
    Friend Shared Sub SetEventHeaderEndDate(ByVal endDate As Date?, ByRef eventHeader As BOEvent.cEventHeader)
        With eventHeader.EndDate
            If endDate.HasValue Then
                .Value = endDate.Value
            Else
                .Value = Nothing
            End If
        End With
    End Sub
    Public Sub SetEventHeaderDate(ByVal startDate As Date?, ByVal endDate As Date?, ByRef eventHeader As BOEvent.cEventHeader) Implements ICouponsEventHeaderDate.SetEventHeaderDate

        SetEventHeaderStartDate(startDate, eventHeader)
        SetEventHeaderEndDate(endDate, eventHeader)

    End Sub
End Class
