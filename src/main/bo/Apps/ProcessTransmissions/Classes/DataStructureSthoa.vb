﻿Imports ProcessTransmissions.DataStructureSthoa.Implementation


Namespace DataStructureSthoa.Interface

    Public Interface IFieldStructure
        Property PaddingCharacter() As String
        Property Length() As Int16
        Property PadLeft() As Boolean
        Property PadRight() As Boolean
    End Interface

    Public Interface IOutgoingRecord
        ReadOnly Property GetRecord() As String
    End Interface

    Public Interface IAdRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentSkuNumber() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentCode() As [Interface].IFieldStructure
        ReadOnly Property AdjustedUnits() As [Interface].IFieldStructure
        ReadOnly Property AdjustedValue() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentReference() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentSellingPrice() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentAssemblyDepot() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentTransferValue() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentType() As [Interface].IFieldStructure
        ReadOnly Property AdjustmentWriteOffId() As [Interface].IFieldStructure
    End Interface

    Public Interface IAdrecord

        Property RecordType() As String
        Property [Date]() As String
        Property Hash() As String
        Property AdjustmentSkuNumber() As String
        Property AdjustmentCode() As String
        Property AdjustedUnits() As String
        Property AdjustedValue() As String
        Property AdjustmentReference() As String
        Property AdjustmentSellingPrice() As String
        Property AdjustmentAssemblyDepot() As String
        Property AdjustmentTransferValue() As String
        Property AdjustmentType() As String
        Property AdjustmentWriteOffId() As String

    End Interface
End Namespace

Namespace DataStructureSthoa
    Public Structure SthoaAdRecordStructure
        Implements [Interface].IAdRecordStructure


        Private _sthotHeaderType As String
#Region "Constants"
        Private Const ZeroPaddingCharacter As String = "0"
        Private Const SingleSpacePaddingCharacter As String = " "
        Private Const NoPaddingCharacter As String = ""
        Private Const padLeftFlagTrue As Boolean = True
        Private Const padLeftFlagFalse As Boolean = False
        Private Const padRightFlagTrue As Boolean = True
        Private Const padRightFlagFalse As Boolean = False
        Private Const RecordTypeLength As Int16 = 2
        Private Const AdjustmentDateLength As Int16 = 8
        Private Const AdjustmentHashValueLength As Int16 = 12
        Private Const AdjustmentSkuNumberLength As Int16 = 6
        Private Const AdjustmentCodeLength As Int16 = 2
        Private Const AdjustedUnitsLength As Int16 = 8
        Private Const AdjustedValueLength As Int16 = 12
        Private Const AdjustmentReferenceLength As Int16 = 6
        Private Const AdjustmentSellingPriceLength As Int16 = 12
        Private Const AdjustmentAssemblyDepotLength As Int16 = 3
        Private Const AdjustmentTransferValueLength As Int16 = 12
        Private Const AdjustmentTypeLength As Int16 = 1
        Private Const AdjustmentWriteOffIdLength As Int16 = 3
        

#End Region

        Public ReadOnly Property AdjustedUnits() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustedUnits
            Get
                Return New Field(AdjustedUnitsLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustedValue() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustedValue
            Get
                Return New Field(AdjustedValueLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentAssemblyDepot() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentAssemblyDepot
            Get
                Return New Field(AdjustmentAssemblyDepotLength, ZeroPaddingCharacter, padLeftFlagFalse, padRightFlagTrue)
            End Get
        End Property

        Public ReadOnly Property AdjustmentCode() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentCode
            Get
                Return New Field(AdjustmentCodeLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentReference() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentReference
            Get
                Return New Field(AdjustmentReferenceLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentSellingPrice() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentSellingPrice
            Get
                Return New Field(AdjustmentSellingPriceLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentSkuNumber() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentSkuNumber
            Get
                Return New Field(AdjustmentSkuNumberLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentTransferValue() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentTransferValue
            Get
                Return New Field(AdjustmentTransferValueLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentType() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentType
            Get
                Return New Field(AdjustmentTypeLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property AdjustmentWriteOffId() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.AdjustmentWriteOffId
            Get
                Return New Field(AdjustmentWriteOffIdLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.Date
            Get
                Return New Field(AdjustmentDateLength, ZeroPaddingCharacter, padLeftFlagTrue, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.Hash
            Get
                Return New Field(AdjustmentHashValueLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IAdRecordStructure.RecordType
            Get
                Return New Field(RecordTypeLength, NoPaddingCharacter, padLeftFlagFalse, padRightFlagFalse)
            End Get
        End Property
    End Structure

End Namespace

Namespace DataStructureSthoa.Implementation
    Public Structure Field
        Implements DataStructureSthoa.Interface.IFieldStructure

        Private _name As String
        Private _length As Int16
        Private _paddingCharacter As String
        Private _padLeft As Boolean
        Private _padRight As Boolean

        Friend Sub New(ByVal length As Int16, ByVal paddingCharacter As String, ByVal padLeft As Boolean, ByVal padRight As Boolean)
            Me._length = length
            Me._paddingCharacter = paddingCharacter
            Me._padLeft = padLeft
            Me._padRight = padRight
        End Sub
        Public Property Length() As Short Implements DataStructureSthoa.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property PaddingCharacter() As String Implements DataStructureSthoa.Interface.IFieldStructure.PaddingCharacter
            Get
                Return _paddingCharacter
            End Get
            Set(ByVal value As String)
                _paddingCharacter = value
            End Set
        End Property

        Public Property PadLeft() As Boolean Implements [Interface].IFieldStructure.PadLeft
            Get
                Return _padLeft
            End Get
            Set(ByVal value As Boolean)
                _padLeft = value
            End Set
        End Property

        Public Property PadRight() As Boolean Implements [Interface].IFieldStructure.PadRight
            Get
                Return _padRight
            End Get
            Set(ByVal value As Boolean)
                _padRight = value
            End Set
        End Property
    End Structure

    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = String.Empty
            If record.Length >= field.Length Then
                record = record.Substring(0, field.Length)
            End If
            If field.PadLeft Then
                val = record.PadLeft(field.Length, CChar(field.PaddingCharacter))
            ElseIf field.PadRight Then
                val = record.PadRight(field.Length, CChar(field.PaddingCharacter))
            Else
                'If Not field.PadRight And Not field.PadLeft Then
                val = record
            End If

            Return val
        End Function

    End Class

    Public Class SthotAdRecord
        Inherits RecordFormat
        Implements [Interface].IAdrecord

        Private _RecordType As String
        Private _Date As String
        Private _Hash As String
        Private _AdjustmentSkuNumber As String
        Private _AdjustmentCode As String
        Private _AdjustedUnits As String
        Private _AdjustedValue As String
        Private _AdjustmentReference As String
        Private _AdjustmentSellingPrice As String
        Private _AdjustmentAssemblyDepot As String
        Private _AdjustmentTransferValue As String
        Private _AdjustmentType As String
        Private _AdjustmentWriteOffId As String
        Private strStockAdjustmentSign As String = "+"
        Private strDllineFormatStringReturnValue As String
        Private Const formatStringLength As Integer = 9
        Private Const formatStringCharacter As String = "0.00"
        Private _recordStructure As New SthoaAdRecordStructure

        Public Property AdjustedUnits() As String Implements [Interface].IAdrecord.AdjustedUnits
            Get
                If Not _AdjustedUnits Is Nothing Then
                    Return Me.FormattedValue(_AdjustedUnits, _recordStructure.AdjustedUnits)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustedUnits = value
            End Set
        End Property

        Public Property AdjustedValue() As String Implements [Interface].IAdrecord.AdjustedValue
            Get
                If Not _AdjustedValue Is Nothing Then
                    Return Me.FormattedValue(_AdjustedValue, _recordStructure.AdjustedValue)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustedValue = value
            End Set
        End Property

        Public Property AdjustmentAssemblyDepot() As String Implements [Interface].IAdrecord.AdjustmentAssemblyDepot
            Get
                If Not _AdjustmentAssemblyDepot Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentAssemblyDepot.Substring(7, 3), _recordStructure.AdjustmentAssemblyDepot)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentAssemblyDepot = value
            End Set
        End Property

        Public Property AdjustmentCode() As String Implements [Interface].IAdrecord.AdjustmentCode
            Get
                If Not _AdjustmentCode Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentCode, _recordStructure.AdjustmentCode)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentCode = value
            End Set
        End Property

        Public Property AdjustmentReference() As String Implements [Interface].IAdrecord.AdjustmentReference
            Get
                If Not _AdjustmentReference Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentReference.Substring(0, 6), _recordStructure.AdjustmentReference)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentReference = value
            End Set
        End Property

        Public Property AdjustmentSellingPrice() As String Implements [Interface].IAdrecord.AdjustmentSellingPrice
            Get
                If Not _AdjustmentSellingPrice Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentSellingPrice, _recordStructure.AdjustmentSellingPrice)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentSellingPrice = value
            End Set
        End Property

        Public Property AdjustmentSkuNumber() As String Implements [Interface].IAdrecord.AdjustmentSkuNumber
            Get
                If Not _AdjustmentSkuNumber Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentSkuNumber, _recordStructure.AdjustmentSkuNumber)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentSkuNumber = value
            End Set
        End Property

        Public Property AdjustmentTransferValue() As String Implements [Interface].IAdrecord.AdjustmentTransferValue
            Get
                If Not _AdjustmentTransferValue Is Nothing Then
                    Return Me.FormattedValue(CStr((0 - CDbl(_AdjustmentTransferValue))), _recordStructure.AdjustmentTransferValue)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentTransferValue = value
            End Set
        End Property

        Public Property AdjustmentType() As String Implements [Interface].IAdrecord.AdjustmentType
            Get
                If Not _AdjustmentType Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentType, _recordStructure.AdjustmentType)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentType = value
            End Set
        End Property

        Public Property AdjustmentWriteOffId() As String Implements [Interface].IAdrecord.AdjustmentWriteOffId
            Get
                If Not _AdjustmentWriteOffId Is Nothing Then
                    Return Me.FormattedValue(_AdjustmentWriteOffId, _recordStructure.AdjustmentWriteOffId)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustmentWriteOffId = value
            End Set
        End Property

        Public Property [Date]() As String Implements [Interface].IAdrecord.Date
            Get
                If Not _Date Is Nothing Then
                    Return Me.FormattedValue(_Date, _recordStructure.Date)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _Date = value
            End Set
        End Property

        Public Property Hash() As String Implements [Interface].IAdrecord.Hash
            Get
                If Not _AdjustedValue Is Nothing Then
                    FormatDecToString(CDec(_AdjustedValue), strDllineFormatStringReturnValue, 11, " ", "0.00")
                    Return strDllineFormatStringReturnValue
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _AdjustedValue = value
            End Set
        End Property

        Public Property RecordType() As String Implements [Interface].IAdrecord.RecordType
            Get
                If Not _RecordType Is Nothing Then
                    Return Me.FormattedValue(_RecordType, _recordStructure.RecordType)
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                _RecordType = value
            End Set
        End Property

        Private Sub FormatDecToString(ByVal decValue As Decimal, ByRef stringValue As String, ByVal intPadDigit As Integer, ByVal strPadcharacter As String, Optional ByVal strWorkMask As String = "0")

            strStockAdjustmentSign = "+"
            If decValue < 0 Then
                decValue = decValue * -1
                strStockAdjustmentSign = "-"
            End If
            stringValue = decValue.ToString(strWorkMask).PadLeft(intPadDigit, CChar(strPadcharacter)) & strStockAdjustmentSign

        End Sub
    End Class


End Namespace

