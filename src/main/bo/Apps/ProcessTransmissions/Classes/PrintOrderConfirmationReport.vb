﻿Public Class PrintOrderConfirmationReport
    Implements IReportPrintingHandler

    Public Sub PrintReport(ByVal ReportData As System.Data.DataTable, ByVal StoreNumber As String, ByVal StoreName As String) Implements IReportPrintingHandler.PrintReport
        Dim HPSTOForm As New HPSTOReport
        With HPSTOForm
            .ReportSource = ReportData
            .StoreID = StoreNumber & "-" & StoreName
            .ShowDialog()
            .Dispose()
        End With
    End Sub
End Class
