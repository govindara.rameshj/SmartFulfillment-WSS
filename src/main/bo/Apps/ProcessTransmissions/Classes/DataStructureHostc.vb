﻿Imports ProcessTransmissions.DataStructureHostc.Implementation

Namespace DataStructureHostc.Interface
    Public Interface IFieldStructure
        Property StartPosition() As Int16
        Property Length() As Int16
    End Interface
    Public Interface IHeaderStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileType() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property FileDescription() As [Interface].IFieldStructure
    End Interface
    Public Interface ITrailerStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property StoreNumber() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property FileType() As [Interface].IFieldStructure
        ReadOnly Property VersionNumber() As [Interface].IFieldStructure
        ReadOnly Property SequenceNumber() As [Interface].IFieldStructure
        ReadOnly Property Time() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure
        ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure
    End Interface
    Public Interface ICyRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property NumberOfWeeksInCycle() As [Interface].IFieldStructure
    End Interface
    Public Interface ICcRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property DayNumberWithinCycle() As [Interface].IFieldStructure
        ReadOnly Property HierarchyCategoryNumber() As [Interface].IFieldStructure
        ReadOnly Property HierarchyGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property HierarchySubGroupNumber() As [Interface].IFieldStructure
        ReadOnly Property HierarchyStyleNumber() As [Interface].IFieldStructure
    End Interface
    Public Interface ICmRecordStructure
        ReadOnly Property RecordType() As [Interface].IFieldStructure
        ReadOnly Property [Date]() As [Interface].IFieldStructure
        ReadOnly Property Hash() As [Interface].IFieldStructure
        ReadOnly Property DateToAddSkuToCountFile() As [Interface].IFieldStructure
        ReadOnly Property SkuNumber() As [Interface].IFieldStructure
        ReadOnly Property AddOrDeleteRecord() As [Interface].IFieldStructure
    End Interface
    Public Interface IHeader
        ReadOnly Property RecordType() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property FileType() As String
        ReadOnly Property VersionNumber() As String
        ReadOnly Property SequenceNumber() As String
        ReadOnly Property Time() As String
        ReadOnly Property FileDescription() As String
    End Interface
    Public Interface ITrailer
        ReadOnly Property RecordType() As String
        ReadOnly Property StoreNumber() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property FileType() As String
        ReadOnly Property VersionNumber() As String
        ReadOnly Property SequenceNumber() As String
        ReadOnly Property Time() As String
        ReadOnly Property Bucket1RecordType() As String
        ReadOnly Property Bucket1RecordCount() As String
        ReadOnly Property Bucket1RecordHashValue() As String
    End Interface
    Public Interface ICyRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property NumberOfWeeksInCycle() As String
    End Interface
    Public Interface ICcRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property DayNumberWithinCycle() As String
        ReadOnly Property HierarchyCategoryNumber() As String
        ReadOnly Property HierarchyGroupNumber() As String
        ReadOnly Property HierarchySubGroupNumber() As String
        ReadOnly Property HierarchyStyleNumber() As String
    End Interface
    Public Interface ICmRecord
        ReadOnly Property RecordType() As String
        ReadOnly Property [Date]() As String
        ReadOnly Property Hash() As String
        ReadOnly Property DateToAddSkuToCountFile() As String
        ReadOnly Property SkuNumber() As String
        ReadOnly Property AddOrDeleteRecord() As String
    End Interface
End Namespace

Namespace DataStructureHostc
    Public Structure HostcHeaderStructure
        Implements [Interface].IHeaderStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileTypeStart As Int16 = 14
        Private Const FileTypeLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const FileDescriptionStart As Int16 = 33
        Private Const FileDescriptionLength As Int16 = 10

#End Region
#Region "Properties"


        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property FileDescription() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.FileDescription
            Get
                Return New Field(FileDescriptionStart, FileDescriptionLength)
            End Get
        End Property

        Public ReadOnly Property FileType() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.FileType
            Get
                Return New Field(FileTypeStart, FileTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberStart, SequenceNumberLength)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.StoreNumber
            Get
                Return New Field(StoreNumberStart, StoreNumberLength)
            End Get
        End Property

        Public ReadOnly Property Time() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.Time
            Get
                Return New Field(TimeStart, TimeLength)
            End Get
        End Property

        Public ReadOnly Property VersionNumber() As [Interface].IFieldStructure Implements [Interface].IHeaderStructure.VersionNumber
            Get
                Return New Field(VersionNumberStart, VersionNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostcTrailerStructure
        Implements [Interface].ITrailerStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const StoreNumberStart As Int16 = 3
        Private Const StoreNumberLength As Int16 = 3
        Private Const DateStart As Int16 = 6
        Private Const DateLength As Int16 = 8
        Private Const FileTypeStart As Int16 = 14
        Private Const FileTypeLength As Int16 = 5
        Private Const VersionNumberStart As Int16 = 19
        Private Const VersionNumberLength As Int16 = 2
        Private Const SequenceNumberStart As Int16 = 21
        Private Const SequenceNumberLength As Int16 = 6
        Private Const TimeStart As Int16 = 27
        Private Const TimeLength As Int16 = 6
        Private Const Bucket1RecordTypeStart As Int16 = 33
        Private Const Bucket1RecordTypeLength As Int16 = 2
        Private Const Bucket1RecordCountStart As Int16 = 35
        Private Const Bucket1RecordCountLength As Int16 = 7
        Private Const Bucket1RecordHashValueStart As Int16 = 42
        Private Const Bucket1RecordHashValueLength As Int16 = 12

#End Region
#Region "Properties"

        Public ReadOnly Property Bucket1RecordCount() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordCount
            Get
                Return New Field(Bucket1RecordCountStart, Bucket1RecordCountLength)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordHashValue() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordHashValue
            Get
                Return New Field(Bucket1RecordHashValueStart, Bucket1RecordHashValueLength)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordType() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Bucket1RecordType
            Get
                Return New Field(Bucket1RecordTypeStart, Bucket1RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property FileType1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.FileType
            Get
                Return New Field(FileTypeStart, FileTypeLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.SequenceNumber
            Get
                Return New Field(SequenceNumberStart, SequenceNumberLength)
            End Get
        End Property

        Public ReadOnly Property StoreNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.StoreNumber
            Get
                Return New Field(StoreNumberStart, StoreNumberLength)
            End Get
        End Property

        Public ReadOnly Property Time1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.Time
            Get
                Return New Field(TimeStart, TimeLength)
            End Get
        End Property

        Public ReadOnly Property VersionNumber1() As [Interface].IFieldStructure Implements [Interface].ITrailerStructure.VersionNumber
            Get
                Return New Field(VersionNumberStart, VersionNumberLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostcCyRecordStructure
        Implements [Interface].ICyRecordStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const NumberOfWeeksInCycleStart As Int16 = 23
        Private Const NumberOfWeeksInCycleLength As Int16 = 2

#End Region
#Region "Properties"


        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].ICyRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].ICyRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property NumberOfWeeksInCycle() As [Interface].IFieldStructure Implements [Interface].ICyRecordStructure.NumberOfWeeksInCycle
            Get
                Return New Field(NumberOfWeeksInCycleStart, NumberOfWeeksInCycleLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].ICyRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostcCcRecordStructure
        Implements [Interface].ICcRecordStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const DayNumberWithinCycleStart As Int16 = 23
        Private Const DayNumberWithinCycleLength As Int16 = 2
        Private Const HierarchyCategoryNumberStart As Int16 = 25
        Private Const HierarchyCategoryNumberLength As Int16 = 6
        Private Const HierarchyGroupNumberStart As Int16 = 31
        Private Const HierarchyGroupNumberLength As Int16 = 6
        Private Const HierarchySubGroupNumberStart As Int16 = 37
        Private Const HierarchySubGroupNumberLength As Int16 = 6
        Private Const HierarchyStyleNumberStart As Int16 = 43
        Private Const HierarchyStyleNumberLength As Int16 = 6

#End Region
#Region "Properties"

        Public ReadOnly Property Date1() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DayNumberWithinCycle() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.DayNumberWithinCycle
            Get
                Return New Field(DayNumberWithinCycleStart, DayNumberWithinCycleLength)
            End Get
        End Property

        Public ReadOnly Property Hash() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property HierarchyCategoryNumber() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.HierarchyCategoryNumber
            Get
                Return New Field(HierarchyCategoryNumberStart, HierarchyCategoryNumberLength)
            End Get
        End Property

        Public ReadOnly Property HierarchyGroupNumber() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.HierarchyGroupNumber
            Get
                Return New Field(HierarchyGroupNumberStart, HierarchyGroupNumberLength)
            End Get
        End Property

        Public ReadOnly Property HierarchyStyleNumber() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.HierarchyStyleNumber
            Get
                Return New Field(HierarchyStyleNumberStart, HierarchyStyleNumberLength)
            End Get
        End Property

        Public ReadOnly Property HierarchySubGroupNumber() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.HierarchySubGroupNumber
            Get
                Return New Field(HierarchySubGroupNumberStart, HierarchySubGroupNumberLength)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As [Interface].IFieldStructure Implements [Interface].ICcRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property
#End Region
    End Structure
    Public Structure HostcCmRecordStructure
        Implements [Interface].ICmRecordStructure

        Private _barCodeType As String
#Region "Constants"
        Private Const RecordTypeStart As Int16 = 1
        Private Const RecordTypeLength As Int16 = 2
        Private Const DateStart As Int16 = 3
        Private Const DateLength As Int16 = 8
        Private Const HashStart As Int16 = 11
        Private Const HashLength As Int16 = 12
        Private Const DateToAddSkuToCountFileStart As Int16 = 23
        Private Const DateToAddSkuToCountFileLength As Int16 = 10
        Private Const SkuNumberStart As Int16 = 33
        Private Const SkuNumberLength As Int16 = 6
        Private Const AddOrDeleteRecordStart As Int16 = 39
        Private Const AddOrDeleteRecordLength As Int16 = 1

#End Region
#Region "Properties"


        Public ReadOnly Property AddOrDeleteRecord() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.AddOrDeleteRecord
            Get
                Return New Field(AddOrDeleteRecordStart, AddOrDeleteRecordLength)
            End Get
        End Property

        Public ReadOnly Property [Date]() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.Date
            Get
                Return New Field(DateStart, DateLength)
            End Get
        End Property

        Public ReadOnly Property DateToAddSkuToCountFile() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.DateToAddSkuToCountFile
            Get
                Return New Field(DateToAddSkuToCountFileStart, DateToAddSkuToCountFileLength)
            End Get
        End Property

        Public ReadOnly Property Hash1() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.Hash
            Get
                Return New Field(HashStart, HashLength)
            End Get
        End Property

        Public ReadOnly Property RecordType() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.RecordType
            Get
                Return New Field(RecordTypeStart, RecordTypeLength)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As [Interface].IFieldStructure Implements [Interface].ICmRecordStructure.SkuNumber
            Get
                Return New Field(SkuNumberStart, SkuNumberLength)
            End Get
        End Property
#End Region
    End Structure

End Namespace

Namespace DataStructureHostc.Implementation
    Public Structure Field
        Implements DataStructureHostc.Interface.IFieldStructure

        Private _name As String
        Private _startPosition As Int16
        Private _length As Int16
        Friend Sub New(ByVal startPosition As Int16, ByVal length As Int16)
            Me._startPosition = startPosition
            Me._length = length
        End Sub
        Public Property Length() As Short Implements DataStructureHostc.Interface.IFieldStructure.Length
            Get
                Return _length
            End Get
            Set(ByVal value As Short)
                _length = value
            End Set
        End Property
        Public Property StartPosition() As Short Implements DataStructureHostc.Interface.IFieldStructure.StartPosition
            Get
                Return _startPosition
            End Get
            Set(ByVal value As Short)
                _startPosition = value
            End Set
        End Property
    End Structure
    Public Class RecordFormat
        Public Function FormattedValue(ByVal record As String, ByVal field As [Interface].IFieldStructure) As String
            Dim val As String = ""
            If RecordIsLongEnoughToContainField(record, field) Then
                val = record.Substring(field.StartPosition - 1, field.Length)
            Else
                val = ""
            End If
            Return val
        End Function

        Public Function RecordIsLongEnoughToContainField(ByVal record As String, ByVal field As [Interface].IFieldStructure) As Boolean
            Return record.Length >= field.StartPosition + field.Length - 1
        End Function

        'Private Function RecordIsLongEnoughToextractField(ByVal field As Field) As Integer
        '    Return
        'End Function
    End Class
    Public Class HostcRecordHeader
        Inherits RecordFormat
        Implements [Interface].IHeader


        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostcHeaderStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostcHeaderStructure
        End Sub
#Region "Properties"


        Public ReadOnly Property [Date]() As String Implements [Interface].IHeader.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property FileDescription() As String Implements [Interface].IHeader.FileDescription
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileDescription)
            End Get
        End Property

        Public ReadOnly Property FileType() As String Implements [Interface].IHeader.FileType
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileType)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].IHeader.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber() As String Implements [Interface].IHeader.SequenceNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SequenceNumber)
            End Get
        End Property

        Public ReadOnly Property StoreNumber() As String Implements [Interface].IHeader.StoreNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreNumber)
            End Get
        End Property

        Public ReadOnly Property Time() As String Implements [Interface].IHeader.Time
            Get
                Return Me.FormattedValue(_record, _recordStructure.Time)
            End Get
        End Property

        Public ReadOnly Property VersionNumber() As String Implements [Interface].IHeader.VersionNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.VersionNumber)
            End Get
        End Property
#End Region
    End Class
    Public Class HostcRecordTrailer
        Inherits RecordFormat
        Implements [Interface].ITrailer

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostcTrailerStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostcTrailerStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property Bucket1RecordCount() As String Implements [Interface].ITrailer.Bucket1RecordCount
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordCount)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordHashValue() As String Implements [Interface].ITrailer.Bucket1RecordHashValue
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordHashValue)
            End Get
        End Property

        Public ReadOnly Property Bucket1RecordType() As String Implements [Interface].ITrailer.Bucket1RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.Bucket1RecordType)
            End Get
        End Property

        Public ReadOnly Property Date1() As String Implements [Interface].ITrailer.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property FileType1() As String Implements [Interface].ITrailer.FileType
            Get
                Return Me.FormattedValue(_record, _recordStructure.FileType1)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].ITrailer.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property

        Public ReadOnly Property SequenceNumber1() As String Implements [Interface].ITrailer.SequenceNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SequenceNumber1)
            End Get
        End Property

        Public ReadOnly Property StoreNumber1() As String Implements [Interface].ITrailer.StoreNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.StoreNumber1)
            End Get
        End Property

        Public ReadOnly Property Time1() As String Implements [Interface].ITrailer.Time
            Get
                Return Me.FormattedValue(_record, _recordStructure.Time1)
            End Get
        End Property

        Public ReadOnly Property VersionNumber1() As String Implements [Interface].ITrailer.VersionNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.VersionNumber1)
            End Get
        End Property
#End Region
    End Class
    Public Class HostcCyRecord
        Inherits RecordFormat
        Implements [Interface].ICyRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostcCyRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostcCyRecordStructure
        End Sub
#Region "Properties"



        Public ReadOnly Property Date1() As String Implements [Interface].ICyRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property Hash() As String Implements [Interface].ICyRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property NumberOfWeeksInCycle() As String Implements [Interface].ICyRecord.NumberOfWeeksInCycle
            Get
                Return Me.FormattedValue(_record, _recordStructure.NumberOfWeeksInCycle)
            End Get
        End Property

        Public ReadOnly Property RecordType1() As String Implements [Interface].ICyRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property
#End Region
    End Class
    Public Class HostcCcRecord
        Inherits RecordFormat
        Implements [Interface].ICcRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostcCcRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostcCcRecordStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property [Date]() As String Implements [Interface].ICcRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date1)
            End Get
        End Property

        Public ReadOnly Property DayNumberWithinCycle() As String Implements [Interface].ICcRecord.DayNumberWithinCycle
            Get
                Return Me.FormattedValue(_record, _recordStructure.DayNumberWithinCycle)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].ICcRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash)
            End Get
        End Property

        Public ReadOnly Property HierarchyCategoryNumber() As String Implements [Interface].ICcRecord.HierarchyCategoryNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.HierarchyCategoryNumber)
            End Get
        End Property

        Public ReadOnly Property HierarchyGroupNumber() As String Implements [Interface].ICcRecord.HierarchyGroupNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.HierarchyGroupNumber)
            End Get
        End Property

        Public ReadOnly Property HierarchyStyleNumber() As String Implements [Interface].ICcRecord.HierarchyStyleNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.HierarchyStyleNumber)
            End Get
        End Property

        Public ReadOnly Property HierarchySubGroupNumber() As String Implements [Interface].ICcRecord.HierarchySubGroupNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.HierarchySubGroupNumber)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].ICcRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType1)
            End Get
        End Property
#End Region
    End Class
    Public Class HostcCmRecord
        Inherits RecordFormat
        Implements [Interface].ICmRecord

        Private ReadOnly _record As String
        Private ReadOnly _recordStructure As HostcCmRecordStructure

        Public Sub New(ByVal record As String)
            _record = record
            _recordStructure = New HostcCmRecordStructure
        End Sub
#Region "Properties"

        Public ReadOnly Property AddOrDeleteRecord() As String Implements [Interface].ICmRecord.AddOrDeleteRecord
            Get
                Return Me.FormattedValue(_record, _recordStructure.AddOrDeleteRecord)
            End Get
        End Property

        Public ReadOnly Property [Date]() As String Implements [Interface].ICmRecord.Date
            Get
                Return Me.FormattedValue(_record, _recordStructure.Date)
            End Get
        End Property

        Public ReadOnly Property DateToAddSkuToCountFile() As String Implements [Interface].ICmRecord.DateToAddSkuToCountFile
            Get
                Return Me.FormattedValue(_record, _recordStructure.DateToAddSkuToCountFile)
            End Get
        End Property

        Public ReadOnly Property Hash1() As String Implements [Interface].ICmRecord.Hash
            Get
                Return Me.FormattedValue(_record, _recordStructure.Hash1)
            End Get
        End Property

        Public ReadOnly Property RecordType() As String Implements [Interface].ICmRecord.RecordType
            Get
                Return Me.FormattedValue(_record, _recordStructure.RecordType)
            End Get
        End Property

        Public ReadOnly Property SkuNumber() As String Implements [Interface].ICmRecord.SkuNumber
            Get
                Return Me.FormattedValue(_record, _recordStructure.SkuNumber)
            End Get
        End Property
#End Region
    End Class

End Namespace


