﻿Public Class DateAdjustRepository
    Implements IDateAdjustRepository

    Public Function GetDateToBeAdjusted(ByVal parameterId As Integer) As System.Data.DataTable Implements IDateAdjustRepository.GetDateToBeAdjusted
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedures.GetDaysToAdjust
                com.AddParameter("@ParameterId", parameterId, SqlDbType.Int)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using
        Return Nothing
    End Function
End Class
