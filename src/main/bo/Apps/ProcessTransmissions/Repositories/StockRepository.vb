﻿Namespace TpWickes

    Public Class StockRepository
        Implements IStockRepository

        Public Function OnHandStock() As System.Data.DataSet Implements IStockRepository.OnHandStock

            Dim DS As DataSet

            Using con As New Connection
                Using com As New Command(con)

                    com.StoredProcedureName = My.Resources.Procedures.OnOrderQuantities
                    DS = com.ExecuteDataSet
                    Return DS

                End Using

            End Using

        End Function

    End Class

End Namespace