﻿Public Class SupplierMasterRepository
    Implements ISupplierMasterRepository

    Public Sub UpdateBBCN(ByVal SupplierNumber As String, ByVal BBCN As String) Implements ISupplierMasterRepository.UpdateBBCN

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_SupplierUpdateBBCN"
                com.AddParameter("@SupplierNumber", SupplierNumber, SqlDbType.Char, 5)
                com.AddParameter("@BBCN", BBCN, SqlDbType.Char, 3)

                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

End Class