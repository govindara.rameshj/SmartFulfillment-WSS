﻿<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ProcessTransmissions.UnitTest")> 
Namespace TpWickes

    Public Class StockRepositoryStub
        Implements IStockRepository

#Region "Stub Code"

        Private _DS As System.Data.DataSet

        Friend Property SetDataSet() As DataSet
            Get
                Return _DS
            End Get
            Set(ByVal value As DataSet)
                _DS = value
            End Set
        End Property

#End Region

#Region "Interface"

        Public Function OnHandStock() As System.Data.DataSet Implements IStockRepository.OnHandStock

            Return _DS

        End Function

#End Region

    End Class

End Namespace
