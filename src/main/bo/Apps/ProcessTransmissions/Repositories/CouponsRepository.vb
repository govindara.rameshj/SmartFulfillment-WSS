﻿Public Class CouponsRepository
    Implements ICouponsRepository

    Public Function GetStock(ByVal skuNumber As String) As DataTable Implements ICouponsRepository.GetStock
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.StockGetStock
                com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber, SqlDbType.Char)
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt

            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetCouponMaster(ByVal couponId As String) As DataTable Implements ICouponsRepository.GetCouponMaster

        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetCouponMaster
                com.AddParameter(My.Resources.Parameters.CouponID, couponId, SqlDbType.Char)
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt

            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetCouponText(ByVal couponId As String, ByVal sequenceNo As String) As DataTable Implements ICouponsRepository.GetCouponText
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetCouponText
                com.AddParameter(My.Resources.Parameters.CouponID, couponId, SqlDbType.Char)
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt

            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetEventHeader(ByVal number As String) As DataTable Implements ICouponsRepository.GetEventHeader
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetEventHeader
                com.AddParameter(My.Resources.Parameters.Number, number, SqlDbType.Char)
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt

            End Using
        End Using

        Return Nothing
    End Function

    Public Function GetEventMas(ByVal eventType As String, ByVal eventKey1 As String, ByVal eventKey2 As String, ByVal eventNumber As String) As DataTable Implements ICouponsRepository.GetEventMas
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.GetEventMas
                com.AddParameter(My.Resources.Parameters.EventType, eventType, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventKey1, eventKey1, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventKey2, eventKey2, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventNumber, eventNumber, SqlDbType.Char)
                dt = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then Return dt

            End Using
        End Using

        Return Nothing
    End Function

    Public Sub UpdateCouponMaster(ByVal couponId As String, ByVal description As String, ByVal storeGenerated As Boolean, ByVal serialNumber As Boolean, ByVal exclusiveCoupon As Boolean, ByVal reusable As Boolean, ByVal collecInfo As Boolean, ByVal deleted As Boolean) Implements ICouponsRepository.UpdateCouponMaster

        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.UpdateCouponMaster
                com.AddParameter(My.Resources.Parameters.CouponID, couponId, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.Description, description, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.StoreGenerated, storeGenerated, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.SerialNumber, serialNumber, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.ExclusiveCoupon, exclusiveCoupon, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.Reusable, reusable, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.CollecInfo, collecInfo, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.Deleted, deleted, SqlDbType.Bit)

                dt = com.ExecuteDataTable

            End Using
        End Using

    End Sub

    Public Sub UpdateCouponText(ByVal couponId As String, ByVal sequenceNumber As String, ByVal printSize As String, ByVal textAlign As String, ByVal printText As String, ByVal deleted As Boolean) Implements ICouponsRepository.UpdateCouponText
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.UpdateCouponText
                com.AddParameter(My.Resources.Parameters.CouponID, couponId, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.SequenceNo, sequenceNumber, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.PrintSize, printSize, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.TextAlign, textAlign, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.PrintText, printText, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.Deleted, deleted, SqlDbType.Bit)

                dt = com.ExecuteDataTable


            End Using
        End Using

    End Sub
    Public Sub UpdateEventMas(ByVal eventType As String, ByVal eventKey1 As String, ByVal eventKey2 As String, ByVal eventNumber As String, ByVal priority As String, ByVal deleted As Boolean, ByVal timeOrDayRelated As Boolean, ByVal startdate As Nullable(Of Date), ByVal enddate As Nullable(Of Date), ByVal specialPrice As Decimal, ByVal buyQuantity As Decimal, ByVal getQuantity As Decimal, ByVal discountAmount As Decimal, ByVal percentageDiscount As Decimal, ByVal buyCoupon As String, ByVal sellCoupon As String) Implements ICouponsRepository.UpdateEventMas
        Dim dt As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = My.Resources.Procedures.UpdateEventMas
                com.AddParameter(My.Resources.Parameters.EventType, eventType, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventKey1, eventKey1, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventKey2, eventKey2, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.EventNumber, eventNumber, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.Priority, priority, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.StartDate, startdate, SqlDbType.Date)
                com.AddParameter(My.Resources.Parameters.EndDate, enddate, SqlDbType.Date)
                com.AddParameter(My.Resources.Parameters.Deleted, deleted, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.TimeOrDayRelated, timeOrDayRelated, SqlDbType.Bit)
                com.AddParameter(My.Resources.Parameters.SpecialPrice, specialPrice, SqlDbType.Decimal)
                com.AddParameter(My.Resources.Parameters.BuyQuantity, buyQuantity, SqlDbType.Decimal)
                com.AddParameter(My.Resources.Parameters.GetQuantity, getQuantity, SqlDbType.Decimal)
                com.AddParameter(My.Resources.Parameters.DiscountAmount, discountAmount, SqlDbType.Decimal)
                com.AddParameter(My.Resources.Parameters.PercentageDiscount, percentageDiscount, SqlDbType.Decimal)
                com.AddParameter(My.Resources.Parameters.BuyCouponID, buyCoupon, SqlDbType.Char)
                com.AddParameter(My.Resources.Parameters.SellCouponID, sellCoupon, SqlDbType.Char)



                dt = com.ExecuteDataTable


            End Using
        End Using


    End Sub
End Class
