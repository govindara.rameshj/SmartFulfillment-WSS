﻿Module ImportCardList

    Public Sub ProcessProductUpdates(ByVal ImportFilePath As String) ' Process data in HOSTA files
        Dim _oasys3Db As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        Dim strWorkString As String = String.Empty
        Dim TransmissionFileReader As StreamReader
        Dim FileData As String = String.Empty
        Dim NoOfRecords As Integer = 0
        Dim messageText As String = String.Empty
        Dim strTransmissionFileName As String = "HOSTA"
        Dim FileToOpen As String = ImportFilePath & "\" & strTransmissionFileName
        Dim strSaveToFileName As String = FileToOpen.ToString & ".old"

        strWorkString = "Processing Transmissions In - " & FileToOpen & " Started : " & TimeOfDay.ToString("hh:mm:ss")
        UpdateProgress(String.Empty, String.Empty, strWorkString)

        If File.Exists(FileToOpen) = False Then
            Trace.WriteLine("Could not locate : " & FileToOpen)
            MainModule.OutputSthoLog("Could not locate: " & FileToOpen)
            Exit Sub
        End If

        'Now Process the transmission files in date sequence from the Files to process array
        Try
            _oasys3Db.BeginTransaction()
            strWorkString = "Validating :" & strSaveToFileName & " Started : " & TimeOfDay.ToString("hh:mm:ss")
            ProcessTransmissionsProgress.ProgressTextBox.Text = String.Empty
            ProcessTransmissionsProgress.RecordCountTextBox.Text = String.Empty
            ProcessTransmissionsProgress.ProcessName.Text = strWorkString
            ProcessTransmissionsProgress.Show()
            TransmissionFileReader = New StreamReader(FileToOpen, True) ' Open the file for calculations

            Dim CardListBO As New BOSales.cCardList(_oasys3Db)
            CardListBO.DeleteAllRecords()

            While TransmissionFileReader.EndOfStream = False ' Calculate Hash & Record counts
                FileData = TransmissionFileReader.ReadLine.PadRight(250, " "c)
                If Trim(FileData).Length >= 16 AndAlso Trim(FileData.Substring(17, 40)) <> "" AndAlso (Trim(FileData.Substring(57, 40)) <> "" Or Trim(FileData.Substring(217, 10)) <> "") Then
                    ProcessTransmissionsProgress.ProgressTextBox.Text = FileData
                    NoOfRecords = NoOfRecords + 1
                    ProcessTransmissionsProgress.RecordCountTextBox.Text = NoOfRecords.ToString & " - HOSTA"
                    ProcessTransmissionsProgress.Show()
                    DataIntegrityHosta(FileData, _oasys3Db, NoOfRecords)
                Else
                    NoOfRecords = NoOfRecords + 1
                    messageText = "Loading file : " & FileToOpen & ". Error line: " & NoOfRecords.ToString & ". "

                    If Trim(FileData).Length <> 0 Then
                        messageText = messageText & vbCrLf & "String: " & FileData.ToString
                    Else
                        messageText = messageText & "String is empty."
                    End If
                    Trace.WriteLine(messageText)
                    MainModule.OutputSthoLog(messageText)

                    Continue While
                End If
            End While

            strWorkString = "Validation of :" & strSaveToFileName & " Completed: " & TimeOfDay.ToString("hh:mm:ss")
            UpdateProgress(String.Empty, String.Empty, strWorkString)
            _oasys3Db.CommitTransaction()

            TransmissionFileReader.Close() ' Close the transmission file - Hashes & record counts calculated
        Catch ex As Exception
            _oasys3Db.RollBackTransaction()
            messageText = "File could not load : " & FileToOpen & ". Error line: " & NoOfRecords.ToString & ". " & vbCrLf & "Error message: " & ex.Message
            Trace.WriteLine(messageText)
            MainModule.OutputSthoLog(messageText)
            Exit Sub
        End Try

        messageText = "File was loaded: " & FileToOpen & ". Record count: " & NoOfRecords.ToString & "."
        Trace.WriteLine(messageText)
        MainModule.OutputSthoLog(messageText)

        If File.Exists(strSaveToFileName) Then
            My.Computer.FileSystem.DeleteFile(strSaveToFileName)
        End If

        My.Computer.FileSystem.CopyFile(FileToOpen, strSaveToFileName)
        My.Computer.FileSystem.DeleteFile(FileToOpen)
    End Sub ' Process data in HOSTA files

    Public Sub DataIntegrityHosta(ByVal strTestString As String, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, Optional ByVal numRecord As Integer = 0) ' Validate HOSTA data
        Dim CardListBO As New BOSales.cCardList(Oasys3DB)
        Dim messageText As String = String.Empty

        strTestString = strTestString.PadRight(114, " "c)
        Trace.WriteLine("DataIntegrityHostA:" & strTestString & "*")

        Try
            CardListBO.CardNumber.Value = CStr(CDec(strTestString.Substring(0, 17))) 'We need to check that data inserted is numeric only
            CardListBO.CustomerName.Value = strTestString.Substring(17, 40)
            CardListBO.AddressLine1.Value = strTestString.Substring(57, 40).PadRight(40, " "c)
            CardListBO.AddressLine2.Value = strTestString.Substring(97, 40).PadRight(40, " "c)
            CardListBO.AddressLine3.Value = strTestString.Substring(137, 40).PadRight(40, " "c)
            CardListBO.AddressLine4.Value = strTestString.Substring(177, 40).PadRight(40, " "c)
            CardListBO.PostCode.Value = strTestString.Substring(217, 10).PadRight(10, " "c)
            CardListBO.HideAddress.Value = strTestString.Substring(227, 1)
            If strTestString.Substring(228, 1) = "Y" Then
                CardListBO.IsDeleted.Value = True
            Else
                CardListBO.IsDeleted.Value = False
            End If
            CardListBO.SaveIfNew()
        Catch ex As Exception
            If InStr(ex.Message, "Violation of PRIMARY KEY constraint", CompareMethod.Text) > 0 Then
                messageText = "Loading file HOSTA. Error line: " & numRecord.ToString & vbCrLf & "Error message: " & ex.Message
                Trace.WriteLine(messageText)
                MainModule.OutputSthoLog(messageText)
            ElseIf InStr(ex.Message, "Conversion from string", CompareMethod.Text) > 0 Then
                messageText = "Loading file HOSTA. Error line: " & numRecord.ToString & vbCrLf & "Error message: " & ex.Message
                Trace.WriteLine(messageText)
                MainModule.OutputSthoLog(messageText)
            Else
                Throw
            End If
        End Try
    End Sub ' Validate HOSTA data

    Private Sub UpdateProgress(ByVal ProgressText As String, ByVal RecordCount As String, ByVal ProcessName As String)
        ProcessTransmissionsProgress.ProgressTextBox.Text = ProgressText
        ProcessTransmissionsProgress.RecordCountTextBox.Text = RecordCount
        ProcessTransmissionsProgress.ProcessName.Text = ProcessName
        ProcessTransmissionsProgress.Show()
    End Sub
End Module
