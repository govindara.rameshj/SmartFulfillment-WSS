﻿Public Class CouponsFactory

    Private Shared _mFactoryMember As ICoupons
    Private Const requirementID As Integer = -2733

    Public Shared Function RequirementEnabled() As Boolean

        Return Cts.Oasys.Core.System.Parameter.GetBoolean(requirementID)

    End Function

    Public Shared Function FactoryGet() As ICoupons

        If _mFactoryMember Is Nothing Then

            If RequirementEnabled() Then

                Return New CouponsNew()
            Else
                Return New Coupons()
            End If

        Else

            Return _mFactoryMember   'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ICoupons)

        _mFactoryMember = obj

    End Sub

End Class


