﻿Imports TpWickes.Library

Public Class STHPOFactory
    Inherits BaseFactory(Of ISTHPO)

    Public Overrides Function Implementation() As ISTHPO

        Return New STHPO
    End Function
End Class
