﻿Namespace TpWickes

    Public Class StockFactory
        Inherits BaseFactory(Of IStock)

        Public Overrides Function Implementation() As IStock

            Return New Stock         'live implementation

        End Function

    End Class

End Namespace