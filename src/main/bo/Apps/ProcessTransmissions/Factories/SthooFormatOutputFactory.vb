﻿Namespace TpWickes

    Public Class SthooFormatOutputFactory

        Private Shared m_FactoryMember As ISthooFormatOutput

        Public Shared Function FactoryGet() As ISthooFormatOutput

            If m_FactoryMember Is Nothing Then
                Return New SthooFormatOutput          'live implementation: existing
            Else
                Return m_FactoryMember                'stub implementation
            End If

        End Function

        Public Shared Function FactoryGet(ByVal StoreName As String, ByVal TodayDate As Date) As ISthooFormatOutput

            If m_FactoryMember Is Nothing Then
                Return New SthooFormatOutput(StoreName, TodayDate)         'live implementation: existing
            Else
                Return m_FactoryMember                                     'stub implementation
            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As ISthooFormatOutput)
            m_FactoryMember = obj
        End Sub

    End Class

End Namespace