﻿Public Class UpdateSthocFactory
    Inherits RequirementSwitchFactory(Of IUpdateSthoc)

    Private Const _RequirementSwitchRF0983 As Integer = -983

    Public Overrides Function ImplementationA() As IUpdateSthoc

        Return New SthocPreventUpdate

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchRF0983)

    End Function

    Public Overrides Function ImplementationB() As IUpdateSthoc

        Return New SthocAllowUpdate

    End Function

End Class