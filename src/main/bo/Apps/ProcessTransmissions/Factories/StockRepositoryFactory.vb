﻿Namespace TpWickes

    Public Class StockRepositoryFactory

        Private Shared m_FactoryMember As IStockRepository

        Public Shared Function FactoryGet() As IStockRepository

            If m_FactoryMember Is Nothing Then

                Return New StockRepository          'live implementation

            Else

                Return m_FactoryMember              'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IStockRepository)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace
