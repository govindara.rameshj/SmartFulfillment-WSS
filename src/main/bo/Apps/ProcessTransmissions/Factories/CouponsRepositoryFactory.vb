﻿Public Class CouponsRepositoryFactory

    Private Shared _mFactoryMember As ICouponsRepository

    Public Shared Function FactoryGet() As ICouponsRepository

        If _mFactoryMember Is Nothing Then

            Return New CouponsRepository

        Else

            Return _mFactoryMember              'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ICouponsRepository)

        _mFactoryMember = obj

    End Sub


End Class
