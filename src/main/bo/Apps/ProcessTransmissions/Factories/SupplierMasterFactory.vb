﻿Public Class SupplierMasterFactory
    Inherits RequirementSwitchFactory(Of ISupplierMaster)

    Private Const _RequirementSwitchRF0892 As Integer = -892

    Public Overrides Function ImplementationA() As ISupplierMaster

        Return New SupplierMasterUpdateBBCN

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchRF0892)

    End Function

    Public Overrides Function ImplementationB() As ISupplierMaster

        Return New SupplierMasterNotUpdateBBCN

    End Function

End Class