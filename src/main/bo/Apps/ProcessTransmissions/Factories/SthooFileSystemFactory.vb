﻿Namespace TpWickes

    Public Class SthooFileSystemFactory

        Private Shared m_FactoryMember As ISthooFileSystem

        Public Shared Function FactoryGet() As ISthooFileSystem

            If m_FactoryMember Is Nothing Then
                Return New SthooFileSystem          'live implementation: existing
            Else
                Return m_FactoryMember              'stub implementation
            End If

        End Function

        Public Shared Function FactoryGet(ByVal FileName As String) As ISthooFileSystem

            If m_FactoryMember Is Nothing Then
                Return New SthooFileSystem(FileName)          'live implementation: existing
            Else
                Return m_FactoryMember                        'stub implementation
            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As ISthooFileSystem)
            m_FactoryMember = obj
        End Sub

    End Class

End Namespace