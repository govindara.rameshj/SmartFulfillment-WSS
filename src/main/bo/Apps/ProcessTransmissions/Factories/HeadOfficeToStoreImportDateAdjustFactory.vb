﻿Public Class HeadOfficeToStoreImportDateAdjustFactory
    Inherits RequirementSwitchFactory(Of IDateAdjuster)

    Private Const _RequirementId As Integer = -101


    Public Overrides Function ImplementationA() As IDateAdjuster
        Return New DateAdjusted
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementSwitch As IRequirementRepository

        RequirementSwitch = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(_RequirementId)
    End Function

    Public Overrides Function ImplementationB() As IDateAdjuster
        Return New DateNotAdjusted
    End Function


End Class
