﻿Public Class SthotFormatFactory

    Private Shared m_FactoryMember As ISthotFormat
    Private Const requirementID As Integer = -3045

    Public Shared Function RequirementEnabled() As Boolean

        Return Cts.Oasys.Core.System.Parameter.GetBoolean(requirementID)

    End Function

    Public Shared Function FactoryGet() As ISthotFormat

        If m_FactoryMember Is Nothing Then
            If RequirementEnabled() Then
                Return New SthotFormatNew
            Else
                Return New SthotFormat
            End If
        Else
            Return m_FactoryMember                'stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ISthotFormat)
        m_FactoryMember = obj
    End Sub


End Class
