﻿Imports TpWickes.Library

Public Class GetOrderConfirmationReportPrintHandlerFactory
    Inherits RequirementSwitchFactory(Of IReportPrintingHandler)
    Private Const ParameterID As Integer = -22019

    Public Overrides Function ImplementationA() As IReportPrintingHandler

        Return New NonPrintingOrderConfirmationReport
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(ParameterID))
    End Function

    Public Overrides Function ImplementationB() As IReportPrintingHandler

        Return New PrintOrderConfirmationReport
    End Function

End Class

