﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using BOStock;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using Cts.Oasys.Core.Locking;

namespace Adjust.WriteOff
{
    public partial class WriteOff : Cts.Oasys.WinForm.Form
    {
        private OasysDBBO.Oasys3.DB.clsOasys3DB _oasys3DB;
        private cStockAdjust _adjust;

        private string _storeIdName = string.Empty;
        public WriteOff(int userId, int workstationId, int securityLevel, string RunParameters)
            : base(userId, workstationId, securityLevel, RunParameters)
        {
            InitializeComponent();

            _oasys3DB = new OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0);
            _adjust = new cStockAdjust(ref _oasys3DB);

            btnSelectAll.Text = Properties.Resources.F4SelectAll;
        }

        protected override void Form_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            e.Handled = true;
            switch (e.KeyData)
            {
                case Keys.F6:
                    btnCancel.PerformClick();
                    break;
                case Keys.F5:
                    btnSave.PerformClick();
                    break;
                case Keys.F4:
                    btnSelectAll.PerformClick();
                    break;
                case Keys.F12:
                    btnExit.PerformClick();
                    break;
                default:
                    e.Handled = false;
                    break;
            }
        }

        protected override void DoProcessing()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                _storeIdName = Cts.Oasys.Core.System.Store.SharedFunctions.GetIdName();

                DisplayStatus(Properties.Resources.GetAdjustments);
                _adjust.WriteOffsLoad();

                //check that there are any write off adjustments
                if (_adjust.TableAdjusts.Rows.Count == 0)
                {
                    MessageBox.Show(Properties.Resources.NoAdjustsSystemExit, FindForm().Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FindForm().Close();
                    return;
                }

                xgvAdjusts_LoadAdjustments();
                DisplayStatus();

            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void xgvAdjusts_LoadAdjustments()
        {
            var dt = _adjust.DataSet.Tables[_adjust.TableAdjusts.TableName];
            DataColumn dc = new DataColumn("Adjustment amount", typeof(Decimal));
            dc.Expression = "QUAN * PRIC";
            dt.Columns.Add(dc);

            xgdAdjusts.DataSource = dt;

            //set up columns
            foreach (DevExpress.XtraGrid.Columns.GridColumn col in xgvAdjusts.Columns)
            {
                col.OptionsColumn.AllowEdit = false;

                if (col.FieldName == _adjust.Column(cStockAdjust.ColumnNames.Selected))
                {
                    col.Caption = "Select";
                    RepositoryItemCheckEdit riCheck = new RepositoryItemCheckEdit();
                    xgdAdjusts.RepositoryItems.Add(riCheck);
                    col.ColumnEdit = riCheck;
                    col.OptionsColumn.AllowEdit = true;
                }
                else if (col.FieldName == _adjust.DateCreated.ColumnName)
                    col.Caption = "Date";
                else if (col.FieldName == _adjust.Code.ColumnName)
                    col.Caption = _adjust.Code.HeaderText;
                else if (col.FieldName == _adjust.SkuNumber.ColumnName)
                    col.Caption = "SKU";
                else if (col.FieldName == _adjust.Column(cStockAdjust.ColumnNames.SkuDescription))
                    col.Caption = "Description";
                else if (col.FieldName == _adjust.Sequence.ColumnName)
                    col.Caption = _adjust.Sequence.HeaderText;
                else if (col.FieldName == _adjust.QtyAdjusted.ColumnName)
                    col.Caption = "Qty";
                else if (col.FieldName == _adjust.EmployeeID.ColumnName)
                    col.Caption = "Actioned by";
                else if (col.FieldName == _adjust.Comment.ColumnName)
                    col.Caption = _adjust.Comment.HeaderText;
                else if (col.FieldName == "Adjustment amount")
                {
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                    col.SummaryItem.DisplayFormat = "Total: {0:c2}";
                }
                else
                    col.Visible = false;
            }

            xgvAdjusts.BestFitColumns();
            xgvAdjusts.Focus();

            GridView view = new GridView(xgdAdjusts);
            xgdAdjusts.LevelTree.Nodes.Add(_adjust.DataSet.Relations[0].RelationName, view);
            view.Columns.AddField(_adjust.AmendId.ColumnName).VisibleIndex = 0;
            view.Columns.AddField(_adjust.QtyAdjusted.ColumnName).VisibleIndex = 1;
            view.Columns.AddField(_adjust.EmployeeID.ColumnName).VisibleIndex = 2;
            view.Columns.AddField(_adjust.Comment.ColumnName).VisibleIndex = 3;
            view.OptionsView.ShowGroupPanel = false;

            foreach (DevExpress.XtraGrid.Columns.GridColumn col in view.Columns)
            {
                if (col.FieldName == _adjust.AmendId.ColumnName)
                    col.Caption = "Amend ID";
                else if (col.FieldName == _adjust.SkuNumber.ColumnName)
                    col.Caption = "SKU";
                else if (col.FieldName == _adjust.Column(cStockAdjust.ColumnNames.SkuDescription))
                    col.Caption = "Description";
                else if (col.FieldName == _adjust.QtyAdjusted.ColumnName)
                    col.Caption = "Qty";
                else if (col.FieldName == _adjust.EmployeeID.ColumnName)
                    col.Caption = "Actioned by";
                else if (col.FieldName == _adjust.Comment.ColumnName)
                    col.Caption = _adjust.Comment.HeaderText;
            }
        }

        private void Report_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            //draw store number and name
            e.Graph.Modifier = BrickModifier.MarginalHeader;
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Near, StringAlignment.Far);
            TextBrick tb = e.Graph.DrawString("Store: " + _storeIdName, Color.Black, new RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), DevExpress.XtraPrinting.BorderSide.None);
        }

        private void Report_CreateMarginalFooterArea(object sender, CreateAreaEventArgs e)
        {
            PageInfoBrick pib = default(PageInfoBrick);
            e.Graph.Modifier = BrickModifier.MarginalFooter;

            RectangleF r = RectangleF.Empty;
            r.Height = 20;

            //draw printed footer
            pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:MM/dd/yyyy HH:mm}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None);
            pib.Alignment = BrickAlignment.Near;

            //draw page number
            pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None);
            pib.Alignment = BrickAlignment.Center;

            //draw version number
            pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " + Application.ProductVersion, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None);
            pib.Alignment = BrickAlignment.Far;

        }

        private void ReportTitle_CreateReportDetailArea(System.Object sender, CreateAreaEventArgs e)
        {
            //draw report title
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
            e.Graph.Font = new Font("Tahoma", 14, FontStyle.Bold);

            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30);
            e.Graph.DrawString("Stock Adjustments Write Offs", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None);

        }

        private void btnSelectAll_Click(System.Object sender, System.EventArgs e)
        {
            if (btnSelectAll.Text == Properties.Resources.F4SelectAll)
            {
                btnSelectAll.Text = Properties.Resources.F4UnselectAll;
                foreach (DataRow dr in _adjust.TableAdjusts.Rows)
                {
                    dr[_adjust.Column(cStockAdjust.ColumnNames.Selected)] = true;
                }
            }
            else
            {
                btnSelectAll.Text = Properties.Resources.F4SelectAll;
                foreach (DataRow dr in _adjust.TableAdjusts.Rows)
                {
                    dr[_adjust.Column(cStockAdjust.ColumnNames.Selected)] = false;
                }
            }
        }

        private void btnSave_Click(System.Object sender, System.EventArgs e)
        {
            try
            {
                //check there are any write offs selected
                if (_adjust.AdjustmentsSelected() == 0)
                {
                    MessageBox.Show(Properties.Resources.WarnNoWriteOffsSelected, FindForm().Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                DialogResult result = MessageBox.Show(Properties.Resources.PromptAuthoriseWriteOffs, FindForm().Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result != DialogResult.Yes)
                    return;

                Cursor = Cursors.WaitCursor;
                DisplayStatus("Authorising write offs");

                _adjust.WriteOffsAuthorise(UserId);

                //check that there are any write off adjustments
                if (_adjust.TableAdjusts.Rows.Count == 0)
                {
                    MessageBox.Show(Properties.Resources.NoAdjustsSystemExit, FindForm().Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FindForm().Close();
                    return;
                }

            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }


        private void btnCancel_Click(System.Object sender, System.EventArgs e)
        {
            try
            {
                //check there are any write offs selected
                if (_adjust.AdjustmentsSelected() == 0)
                {
                    MessageBox.Show(Properties.Resources.WarnNoWriteOffsSelected, FindForm().Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                DialogResult result = MessageBox.Show(Properties.Resources.PromptCancelWriteOffs, FindForm().Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result != DialogResult.Yes)
                    return;

                Cursor = Cursors.WaitCursor;
                DisplayStatus("Cancelling write offs");

                _adjust.WriteOffsCancel(UserId);

                //check that there are any write off adjustments
                if (_adjust.TableAdjusts.Rows.Count == 0)
                {
                    MessageBox.Show(Properties.Resources.NoAdjustsSystemExit, FindForm().Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FindForm().Close();
                    return;
                }

            }
            finally
            {
                Cursor = Cursors.Default;
                DisplayProgress();
            }

        }


        private void btnPrint_Click(System.Object sender, System.EventArgs e)
        {
            DisplayStatus();

            //hide select column
            xgvAdjusts.Columns[(int)cStockAdjust.ColumnNames.Selected].Visible = false;

            //set up supplier copy report
            CompositeLink report = new CompositeLink(new PrintingSystem());
            report.Margins.Bottom = 70;
            report.Margins.Top = 50;
            report.Margins.Left = 50;
            report.Margins.Right = 50;
            report.CreateMarginalHeaderArea += Report_CreateMarginalHeaderArea;
            report.CreateMarginalFooterArea += Report_CreateMarginalFooterArea;

            Link reportTitle = new Link();
            reportTitle.CreateDetailArea += ReportTitle_CreateReportDetailArea;

            //set up grid component
            PrintableComponentLink reportGrid = new PrintableComponentLink();
            reportGrid.Component = xgdAdjusts;

            //populate the collection of links in the composite link in order and show preview
            report.Links.Add(reportTitle);
            report.Links.Add(reportGrid);
            report.ShowPreviewDialog();

            //unhide select column
            xgvAdjusts.Columns[(int)cStockAdjust.ColumnNames.Selected].Visible = true;

        }

        private void btnExit_Click(System.Object sender, System.EventArgs e)
        {
            FindForm().Close();
        }
    }
}
