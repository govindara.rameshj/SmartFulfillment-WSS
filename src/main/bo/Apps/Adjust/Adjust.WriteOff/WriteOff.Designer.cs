﻿namespace Adjust.WriteOff
{
    partial class WriteOff
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WriteOff));
            this.xgdAdjusts = new DevExpress.XtraGrid.GridControl();
            this.xgvAdjusts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.xgdAdjusts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xgvAdjusts)).BeginInit();
            this.SuspendLayout();
            // 
            // xgdAdjusts
            // 
            this.xgdAdjusts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xgdAdjusts.Location = new System.Drawing.Point(3, 3);
            this.xgdAdjusts.MainView = this.xgvAdjusts;
            this.xgdAdjusts.Name = "xgdAdjusts";
            this.xgdAdjusts.Size = new System.Drawing.Size(894, 499);
            this.xgdAdjusts.TabIndex = 0;
            this.xgdAdjusts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.xgvAdjusts});
            // 
            // xgvAdjusts
            // 
            this.xgvAdjusts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xgvAdjusts.Appearance.HeaderPanel.Options.UseFont = true;
            this.xgvAdjusts.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.xgvAdjusts.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.xgvAdjusts.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.xgvAdjusts.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xgvAdjusts.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.xgvAdjusts.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.xgvAdjusts.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.xgvAdjusts.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.xgvAdjusts.GridControl = this.xgdAdjusts;
            this.xgvAdjusts.Name = "xgvAdjusts";
            this.xgvAdjusts.OptionsDetail.ShowDetailTabs = false;
            this.xgvAdjusts.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.xgvAdjusts.OptionsPrint.UsePrintStyles = true;
            this.xgvAdjusts.OptionsView.ShowFooter = true;
            this.xgvAdjusts.OptionsView.ShowGroupPanel = false;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(822, 508);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 39);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "F12 Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectAll.Appearance.Options.UseTextOptions = true;
            this.btnSelectAll.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnSelectAll.Location = new System.Drawing.Point(4, 508);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 39);
            this.btnSelectAll.TabIndex = 1;
            this.btnSelectAll.Text = "F4 Select All";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Appearance.Options.UseTextOptions = true;
            this.btnSave.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnSave.Location = new System.Drawing.Point(86, 508);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 39);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "F5 Authorise Write Off";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Appearance.Options.UseTextOptions = true;
            this.btnCancel.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCancel.Location = new System.Drawing.Point(167, 508);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 39);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "F6 Cancel Write Off";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Appearance.Options.UseTextOptions = true;
            this.btnPrint.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnPrint.Location = new System.Drawing.Point(741, 508);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 39);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "F9 Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // WriteOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.xgdAdjusts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WriteOff";
            this.Size = new System.Drawing.Size(900, 550);
            ((System.ComponentModel.ISupportInitialize)(this.xgdAdjusts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xgvAdjusts)).EndInit();
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraGrid.GridControl xgdAdjusts;
        internal DevExpress.XtraGrid.Views.Grid.GridView xgvAdjusts;
        internal DevExpress.XtraEditors.SimpleButton btnExit;
        internal DevExpress.XtraEditors.SimpleButton btnSelectAll;
        internal DevExpress.XtraEditors.SimpleButton btnSave;
        internal DevExpress.XtraEditors.SimpleButton btnCancel;

        internal DevExpress.XtraEditors.SimpleButton btnPrint;
        public WriteOff()
        {
            InitializeComponent();
        }
    }
}