Imports DevExpress.XtraGrid.Views.BandedGrid
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class Markdowns
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _retOptions As New BOSystem.cRetailOptions(_Oasys3DB)
    Private _markdown As New BOStock.cMarkdownStock(_Oasys3DB)

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        btnSelectAll.Text = My.Resources.F4SelectAll
        dteSoldStart.EditValue = Now
        dteSoldEnd.EditValue = Now
        dteSoldStart.Properties.MaxValue = Now
        dteSoldEnd.Properties.MaxValue = Now

        dteWrittenOffEnd.EditValue = Now
        dteWrittenOffStart.EditValue = Now
        dteWrittenOffStart.Properties.MaxValue = Now
        dteWrittenOffEnd.Properties.MaxValue = Now
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3
                Select Case tabcMarkdowns.SelectedTabPage.Name
                    Case tabpSold.Name : btnSold.PerformClick()
                    Case tabpWrittenOff.Name : btnWrittenOff.PerformClick()
                End Select
            Case Keys.F4 : btnSelectAll.PerformClick()
            Case Keys.F8 : btnPrintLabels.PerformClick()
            Case Keys.F9 : btnPrintSheet.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            DisplayStatus(My.Resources.GetActiveMarkdowns)
            _retOptions.LoadStoreAndAccountability()
            gridvActive_LoadData()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub dteSoldStart_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteSoldStart.DateTimeChanged
        dteSoldEnd.Properties.MinValue = dteSoldStart.DateTime
    End Sub

    Private Sub dteSoldEnd_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteSoldEnd.DateTimeChanged
        dteSoldStart.Properties.MaxValue = dteSoldEnd.DateTime
    End Sub

    Private Sub dteWrittenOffStart_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteWrittenOffStart.DateTimeChanged
        dteWrittenOffEnd.Properties.MinValue = dteWrittenOffStart.DateTime
    End Sub

    Private Sub dteWrittenOffEnd_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteWrittenOffEnd.DateTimeChanged
        dteWrittenOffStart.Properties.MaxValue = dteWrittenOffEnd.DateTime
    End Sub




    Private Sub tabcMarkdowns_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabcMarkdowns.SelectedPageChanged
        Select Case tabcMarkdowns.SelectedTabPage.Name
            Case tabpActive.Name
                btnPrintLabels.Visible = True
                btnSelectAll.Visible = True
            Case Else
                btnPrintLabels.Visible = False
                btnSelectAll.Visible = False
        End Select
    End Sub

    Private Sub gridvActive_LoadData()

        'load active markdowns and set up datasource
        _markdown.ActiveLoad()
        gridcActive.DataSource = _markdown.ActiveTable

        'creating the bands layout
        gridvActive.Bands.Clear()
        Dim bandItem As GridBand = gridvActive.Bands.AddBand("Item")
        bandItem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandHierarchy As GridBand = gridvActive.Bands.AddBand("Hierarchy")
        bandHierarchy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandPrice As GridBand = gridvActive.Bands.AddBand("Price")
        bandPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandMarkdown As GridBand = gridvActive.Bands.AddBand("Markdown Details")
        bandMarkdown.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Dim bandReductions As GridBand = gridvActive.Bands.AddBand("Weekly Reductions")
        bandReductions.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        'set up columns
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In gridvActive.Columns
            col.OptionsColumn.AllowEdit = False
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Select Case True
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.Selected))
                    col.Caption = "Select"
                    col.OptionsColumn.AllowEdit = True
                    col.Width = 50
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.SkuNumber.ColumnName)
                    col.Caption = "SKU"
                    col.Width = 60
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuDescription))
                    col.Caption = "Description"
                    col.BestFit()
                    bandItem.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.CategoryDescription))
                    col.Caption = "Category"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.GroupDescription))
                    col.Caption = "Group"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SubgroupDescription))
                    col.Caption = "Subgroup"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.StyleDescription))
                    col.Caption = "Style"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Price.ColumnName)
                    col.Caption = "Creation"
                    col.MinWidth = 50
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuPrice))
                    col.Caption = "Retail"
                    col.MinWidth = 50
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.DateCreated.ColumnName)
                    col.Caption = "Created"
                    col.MinWidth = 60
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Serial.ColumnName)
                    col.Caption = "Serial"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.IsLabelled.ColumnName)
                    col.Caption = "Label"
                    col.MinWidth = 30
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.StartsWith(_markdown.Column(cMarkdownStock.ColumnNames.WeekCurrent))
                    If col.FieldName = _markdown.Column(cMarkdownStock.ColumnNames.WeekCurrent) Then
                        col.Caption = "Current"
                    Else
                        col.Caption = "+ " & col.FieldName.Substring(11)
                    End If
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandReductions.Columns.Add(CType(col, BandedGridColumn))
            End Select
        Next

        bandHierarchy.Visible = False
        gridcActive.Visible = True

    End Sub

    Private Sub gridvActive_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles gridvActive.CustomColumnDisplayText
        Select Case True
            Case e.Column.FieldName.StartsWith(_markdown.Column(cMarkdownStock.ColumnNames.WeekCurrent))
                If Not IsDBNull(e.Value) AndAlso CInt(e.Value) = 0 Then
                    e.DisplayText = ""
                End If
        End Select
    End Sub

    Private Sub gridvSold_LoadData()

        'load markdowns and set up datasource
        _markdown.SoldLoad(dteSoldStart.DateTime, dteSoldEnd.DateTime)
        gridcSold.DataSource = _markdown.SoldTable

        'creating the bands layout
        gridvSold.Bands.Clear()
        Dim bandItem As GridBand = gridvSold.Bands.AddBand("Item")
        Dim bandPrice As GridBand = gridvSold.Bands.AddBand("Price")
        Dim bandHierarchy As GridBand = gridvSold.Bands.AddBand("Hierarchy")
        Dim bandMarkdown As GridBand = gridvSold.Bands.AddBand("Markdown Details")
        Dim bandSold As GridBand = gridvSold.Bands.AddBand("Sold Details")

        'set up columns
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In gridvSold.Columns
            col.BestFit()
            Select Case True
                Case col.FieldName.Equals(_markdown.SkuNumber.ColumnName)
                    col.Caption = "SKU"
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuDescription))
                    col.Caption = "Description"
                    bandItem.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.CategoryDescription))
                    col.Caption = "Category"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.GroupDescription))
                    col.Caption = "Group"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SubgroupDescription))
                    col.Caption = "Subgroup"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.StyleDescription))
                    col.Caption = "Style"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Price.ColumnName)
                    col.Caption = "Creation"
                    col.MinWidth = 50
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuPrice))
                    col.Caption = "Retail"
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.DateCreated.ColumnName)
                    col.Caption = "Date"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Serial.ColumnName)
                    col.Caption = "Serial"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.WeekCurrent))
                    col.Caption = "Price"
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandSold.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.SoldDate.ColumnName)
                    col.Caption = "Date"
                    bandSold.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.SoldTill.ColumnName)
                    col.Caption = "Till"
                    bandSold.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.SoldTransaction.ColumnName)
                    col.Caption = "Transaction"
                    bandSold.Columns.Add(CType(col, BandedGridColumn))
            End Select
        Next

        gridcSold.Visible = True


    End Sub

    Private Sub gridvWriteOff_LoadData()

        'load markdowns and set up datasource
        _markdown.WrittenOffLoad(dteSoldStart.DateTime, dteSoldEnd.DateTime)
        gridcWrittenOff.DataSource = _markdown.WrittenOffTable

        'creating the bands layout
        gridvWrittenOff.Bands.Clear()
        Dim bandItem As GridBand = gridvWrittenOff.Bands.AddBand("Item")
        Dim bandPrice As GridBand = gridvWrittenOff.Bands.AddBand("Price")
        Dim bandHierarchy As GridBand = gridvWrittenOff.Bands.AddBand("Hierarchy")
        Dim bandMarkdown As GridBand = gridvWrittenOff.Bands.AddBand("Markdown Details")
        Dim bandWriteOff As GridBand = gridvWrittenOff.Bands.AddBand("Written Off")

        'set up columns
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In gridvWrittenOff.Columns
            col.BestFit()
            Select Case True
                Case col.FieldName.Equals(_markdown.SkuNumber.ColumnName)
                    col.Caption = "SKU"
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuDescription))
                    col.Caption = "Description"
                    bandItem.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.CategoryDescription))
                    col.Caption = "Category"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.GroupDescription))
                    col.Caption = "Group"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SubgroupDescription))
                    col.Caption = "Subgroup"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.StyleDescription))
                    col.Caption = "Style"
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Price.ColumnName)
                    col.Caption = "Creation"
                    col.MinWidth = 50
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.SkuPrice))
                    col.Caption = "Retail"
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandPrice.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.DateCreated.ColumnName)
                    col.Caption = "Date"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))
                Case col.FieldName.Equals(_markdown.Serial.ColumnName)
                    col.Caption = "Serial"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    bandMarkdown.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.Column(cMarkdownStock.ColumnNames.WeekCurrent))
                    col.Caption = "Price"
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "c2"
                    bandWriteOff.Columns.Add(CType(col, BandedGridColumn))

                Case col.FieldName.Equals(_markdown.DateWrittenOff.ColumnName)
                    col.Caption = "Date"
                    bandWriteOff.Columns.Add(CType(col, BandedGridColumn))
            End Select
        Next

        gridcWrittenOff.Visible = True


    End Sub



    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw store number and name
        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _retOptions.StoreIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:MM/dd/yyyy HH:mm}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'draw report title
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Tahoma", 14, FontStyle.Bold)

        Select Case tabcMarkdowns.SelectedTabPage.Name
            Case tabpActive.Name
                Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
                e.Graph.DrawString("All Active Markdowns", Color.Black, rec, BorderSide.None)

            Case tabpSold.Name
                Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
                e.Graph.DrawString("All Sold Markdowns", Color.Black, rec, BorderSide.None)

                rec = New RectangleF(0, 30, e.Graph.ClientPageSize.Width, 20)
                e.Graph.Font = New Font("Tahoma", 10, FontStyle.Bold)
                e.Graph.DrawString("Data for: " & dteSoldStart.DateTime.ToShortDateString & " - " & dteSoldEnd.DateTime.ToShortDateString, Color.Black, rec, BorderSide.None)

            Case tabpWrittenOff.Name
                Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
                e.Graph.DrawString("All Written Off Markdowns", Color.Black, rec, BorderSide.None)

                rec = New RectangleF(0, 30, e.Graph.ClientPageSize.Width, 20)
                e.Graph.Font = New Font("Tahoma", 10, FontStyle.Bold)
                e.Graph.DrawString("Data for: " & dteWrittenOffStart.DateTime.ToShortDateString & " - " & dteWrittenOffEnd.DateTime.ToShortDateString, Color.Black, rec, BorderSide.None)
        End Select

    End Sub

    Private Sub ReportNote_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'get report title
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50)
        e.Graph.DrawString(lblNote.Text, Color.Black, rec, BorderSide.None)

    End Sub



    Private Sub btnSold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSold.Click

        Try
            DisplayStatus()
            Cursor = Cursors.WaitCursor
            gridvSold_LoadData()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnWrittenOff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWrittenOff.Click

        Try
            DisplayStatus()
            Cursor = Cursors.WaitCursor
            gridvWriteOff_LoadData()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click

        DisplayStatus()

        Select Case btnSelectAll.Text
            Case My.Resources.F4SelectAll
                btnSelectAll.Text = My.Resources.F4UnselectAll
                For Each dr As DataRow In _markdown.ActiveTable.Rows
                    dr(_markdown.Column(cMarkdownStock.ColumnNames.Selected)) = True
                Next

            Case My.Resources.F4UnselectAll
                btnSelectAll.Text = My.Resources.F4SelectAll
                For Each dr As DataRow In _markdown.ActiveTable.Rows
                    dr(_markdown.Column(cMarkdownStock.ColumnNames.Selected)) = False
                Next
        End Select

    End Sub

    Private Sub btnPrintSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintSheet.Click

        DisplayStatus()

        'get grid to be printed and hide select column for active page
        Dim grid As DevExpress.XtraGrid.GridControl = Nothing
        Select Case tabcMarkdowns.SelectedTabPage.Name
            Case tabpActive.Name
                grid = gridcActive
                gridvActive.Columns(_markdown.Column(cMarkdownStock.ColumnNames.Selected)).Visible = False
            Case tabpSold.Name : grid = gridcSold
            Case tabpWrittenOff.Name : grid = gridcWrittenOff
        End Select

        'set up supplier copy report
        Dim report As New CompositeLink(New PrintingSystem)
        report.Landscape = True
        report.Margins.Bottom = 70
        report.Margins.Top = 50
        report.Margins.Left = 50
        report.Margins.Right = 50
        AddHandler report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = grid

        'populate the collection of links in the composite link in order
        report.Links.Add(reportTitle)
        report.Links.Add(reportGrid)

        If tabcMarkdowns.SelectedTabPage.Name = tabpActive.Name Then
            Dim reportNote As New Link
            AddHandler reportNote.CreateDetailArea, AddressOf ReportNote_CreateReportDetailArea
            report.Links.Add(reportNote)
        End If

        'show preview
        report.ShowPreviewDialog()

        'reshow select column if active page
        If tabcMarkdowns.SelectedTabPage.Name = tabpActive.Name Then
            gridvActive.Columns(_markdown.Column(cMarkdownStock.ColumnNames.Selected)).Visible = True
        End If

    End Sub

    Private Sub btnPrintLabels_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintLabels.Click

        Try
            'generate text file to send to label print routine
            Dim labelsNum As Integer = 0
            Dim labels As String = _markdown.ActivePrintLabelString(labelsNum)

            'get labelInfo object from cmbLabelType selected Index and write text file
            Dim param As New BOSystem.cParameter(_Oasys3DB)
            Dim labelsPath As String = param.GetParameterString(500).Trim
            Dim filePath As String = labelsPath & Format(WorkstationId, "000") & "\"

            Dim _SysLabel As New BOSystem.cSystemLabels(_Oasys3DB)
            _SysLabel.LoadMarkdownLabel()
            Dim fileNameTxt As String = _SysLabel.Filename.Value.Trim & ".txt"
            Dim fileNameSgn As String = _SysLabel.Filename.Value.Trim & ".sgn"

            'check valid path string
            If labelsPath <> String.Empty Then
                If labelsPath.Chars(labelsPath.Length - 1) <> "\c" Then labelsPath.Insert(labelsPath.Length - 1, "\")
            End If

            'check that path exists and that sgn file is there
            Directory.CreateDirectory(filePath)
            If Not File.Exists(filePath & fileNameSgn) Then
                File.Copy(labelsPath & fileNameSgn, filePath & "\" & fileNameSgn)
            End If

            'write labels text file
            File.WriteAllText(filePath & fileNameTxt, labels)

            'get number of labels can fit onto one page
            Dim pagesRequired As Integer = CInt(Math.Ceiling(labelsNum / _SysLabel.LabelsPerPage.Value))
            MessageBox.Show("Number of pages required: " & pagesRequired, "Print Labels", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Dim ensignExe As String = param.GetParameterString(502).Trim
            Dim ensignLog As String = param.GetParameterString(503).Trim
            Dim cmd As String = ensignExe & Space(1) & filePath & fileNameSgn & " /PD" & filePath

            Trace.WriteLine(cmd, Name)
            Shell(cmd, AppWinStyle.Hide, True)

            If File.Exists(ensignLog) Then
                If File.ReadAllLines(ensignLog).Length = 0 Then
                    Dim result As DialogResult = MessageBox.Show("Please confirm that labels have printed correctly", "Confirm Labels Printed", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                    If result = DialogResult.No Then
                        btnPrintLabels.PerformClick()
                        Exit Sub
                    End If

                    'printed ok so set label printed flag for each markdown concerned
                    _markdown.ActiveUpdateLabelled()
                End If
            End If

        Catch ex As FileNotFoundException
            DisplayWarning("File not found: " & ex.FileName)

        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class