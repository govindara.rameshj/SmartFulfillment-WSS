<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Markdowns
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Markdowns))
        Me.gridcActive = New DevExpress.XtraGrid.GridControl
        Me.gridvActive = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.gridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.tabcMarkdowns = New DevExpress.XtraTab.XtraTabControl
        Me.tabpActive = New DevExpress.XtraTab.XtraTabPage
        Me.lblNote = New DevExpress.XtraEditors.LabelControl
        Me.tabpSold = New DevExpress.XtraTab.XtraTabPage
        Me.btnSold = New DevExpress.XtraEditors.SimpleButton
        Me.dteSoldEnd = New DevExpress.XtraEditors.DateEdit
        Me.dteSoldStart = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.gridcSold = New DevExpress.XtraGrid.GridControl
        Me.gridvSold = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.tabpWrittenOff = New DevExpress.XtraTab.XtraTabPage
        Me.btnWrittenOff = New DevExpress.XtraEditors.SimpleButton
        Me.dteWrittenOffEnd = New DevExpress.XtraEditors.DateEdit
        Me.dteWrittenOffStart = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.gridcWrittenOff = New DevExpress.XtraGrid.GridControl
        Me.gridvWrittenOff = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridBand3 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton
        Me.btnPrintSheet = New DevExpress.XtraEditors.SimpleButton
        Me.btnPrintLabels = New DevExpress.XtraEditors.SimpleButton
        Me.btnSelectAll = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gridcActive, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridvActive, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabcMarkdowns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabcMarkdowns.SuspendLayout()
        Me.tabpActive.SuspendLayout()
        Me.tabpSold.SuspendLayout()
        CType(Me.dteSoldEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteSoldEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteSoldStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteSoldStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridcSold, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridvSold, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpWrittenOff.SuspendLayout()
        CType(Me.dteWrittenOffEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteWrittenOffEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteWrittenOffStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteWrittenOffStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridcWrittenOff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridvWrittenOff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridcActive
        '
        Me.gridcActive.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridcActive.Location = New System.Drawing.Point(0, 52)
        Me.gridcActive.MainView = Me.gridvActive
        Me.gridcActive.Name = "gridcActive"
        Me.gridcActive.Size = New System.Drawing.Size(885, 416)
        Me.gridcActive.TabIndex = 1
        Me.gridcActive.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridvActive})
        Me.gridcActive.Visible = False
        '
        'gridvActive
        '
        Me.gridvActive.AppearancePrint.Row.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.gridvActive.AppearancePrint.Row.Options.UseFont = True
        Me.gridvActive.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand1})
        Me.gridvActive.GridControl = Me.gridcActive
        Me.gridvActive.Name = "gridvActive"
        Me.gridvActive.OptionsPrint.UsePrintStyles = True
        Me.gridvActive.OptionsView.ColumnAutoWidth = False
        '
        'gridBand1
        '
        Me.gridBand1.Caption = "gridBand1"
        Me.gridBand1.Name = "gridBand1"
        '
        'tabcMarkdowns
        '
        Me.tabcMarkdowns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabcMarkdowns.Location = New System.Drawing.Point(3, 3)
        Me.tabcMarkdowns.Name = "tabcMarkdowns"
        Me.tabcMarkdowns.SelectedTabPage = Me.tabpActive
        Me.tabcMarkdowns.Size = New System.Drawing.Size(894, 499)
        Me.tabcMarkdowns.TabIndex = 0
        Me.tabcMarkdowns.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabpActive, Me.tabpSold, Me.tabpWrittenOff})
        '
        'tabpActive
        '
        Me.tabpActive.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpActive.Controls.Add(Me.lblNote)
        Me.tabpActive.Controls.Add(Me.gridcActive)
        Me.tabpActive.Name = "tabpActive"
        Me.tabpActive.Size = New System.Drawing.Size(885, 468)
        Me.tabpActive.Text = "Active"
        '
        'lblNote
        '
        Me.lblNote.Appearance.Options.UseTextOptions = True
        Me.lblNote.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.lblNote.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblNote.Location = New System.Drawing.Point(0, 3)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(883, 43)
        Me.lblNote.TabIndex = 0
        Me.lblNote.Text = resources.GetString("lblNote.Text")
        '
        'tabpSold
        '
        Me.tabpSold.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpSold.Controls.Add(Me.btnSold)
        Me.tabpSold.Controls.Add(Me.dteSoldEnd)
        Me.tabpSold.Controls.Add(Me.dteSoldStart)
        Me.tabpSold.Controls.Add(Me.LabelControl2)
        Me.tabpSold.Controls.Add(Me.LabelControl1)
        Me.tabpSold.Controls.Add(Me.gridcSold)
        Me.tabpSold.Name = "tabpSold"
        Me.tabpSold.Size = New System.Drawing.Size(885, 468)
        Me.tabpSold.Text = "Sold"
        '
        'btnSold
        '
        Me.btnSold.Location = New System.Drawing.Point(240, 3)
        Me.btnSold.Name = "btnSold"
        Me.btnSold.Size = New System.Drawing.Size(75, 39)
        Me.btnSold.TabIndex = 4
        Me.btnSold.Text = "F3 Select"
        '
        'dteSoldEnd
        '
        Me.dteSoldEnd.EditValue = Nothing
        Me.dteSoldEnd.EnterMoveNextControl = True
        Me.dteSoldEnd.Location = New System.Drawing.Point(89, 26)
        Me.dteSoldEnd.Name = "dteSoldEnd"
        Me.dteSoldEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteSoldEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteSoldEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteSoldEnd.Size = New System.Drawing.Size(133, 20)
        Me.dteSoldEnd.TabIndex = 3
        '
        'dteSoldStart
        '
        Me.dteSoldStart.EditValue = Nothing
        Me.dteSoldStart.EnterMoveNextControl = True
        Me.dteSoldStart.Location = New System.Drawing.Point(89, 3)
        Me.dteSoldStart.Name = "dteSoldStart"
        Me.dteSoldStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteSoldStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteSoldStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteSoldStart.Size = New System.Drawing.Size(133, 20)
        Me.dteSoldStart.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(3, 26)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Date &To"
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Date &From"
        '
        'gridcSold
        '
        Me.gridcSold.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridcSold.Location = New System.Drawing.Point(0, 52)
        Me.gridcSold.MainView = Me.gridvSold
        Me.gridcSold.Name = "gridcSold"
        Me.gridcSold.Size = New System.Drawing.Size(885, 416)
        Me.gridcSold.TabIndex = 5
        Me.gridcSold.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridvSold, Me.GridView2})
        Me.gridcSold.Visible = False
        '
        'gridvSold
        '
        Me.gridvSold.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand2})
        Me.gridvSold.GridControl = Me.gridcSold
        Me.gridvSold.Name = "gridvSold"
        Me.gridvSold.OptionsBehavior.Editable = False
        Me.gridvSold.OptionsView.ColumnAutoWidth = False
        '
        'gridBand2
        '
        Me.gridBand2.Caption = "gridBand2"
        Me.gridBand2.Name = "gridBand2"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gridcSold
        Me.GridView2.Name = "GridView2"
        '
        'tabpWrittenOff
        '
        Me.tabpWrittenOff.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpWrittenOff.Controls.Add(Me.btnWrittenOff)
        Me.tabpWrittenOff.Controls.Add(Me.dteWrittenOffEnd)
        Me.tabpWrittenOff.Controls.Add(Me.dteWrittenOffStart)
        Me.tabpWrittenOff.Controls.Add(Me.LabelControl3)
        Me.tabpWrittenOff.Controls.Add(Me.LabelControl4)
        Me.tabpWrittenOff.Controls.Add(Me.gridcWrittenOff)
        Me.tabpWrittenOff.Name = "tabpWrittenOff"
        Me.tabpWrittenOff.Size = New System.Drawing.Size(885, 468)
        Me.tabpWrittenOff.Text = "Written Off"
        '
        'btnWrittenOff
        '
        Me.btnWrittenOff.Location = New System.Drawing.Point(240, 3)
        Me.btnWrittenOff.Name = "btnWrittenOff"
        Me.btnWrittenOff.Size = New System.Drawing.Size(75, 39)
        Me.btnWrittenOff.TabIndex = 4
        Me.btnWrittenOff.Text = "F3 Select"
        '
        'dteWrittenOffEnd
        '
        Me.dteWrittenOffEnd.EditValue = Nothing
        Me.dteWrittenOffEnd.EnterMoveNextControl = True
        Me.dteWrittenOffEnd.Location = New System.Drawing.Point(89, 26)
        Me.dteWrittenOffEnd.Name = "dteWrittenOffEnd"
        Me.dteWrittenOffEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteWrittenOffEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteWrittenOffEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteWrittenOffEnd.Size = New System.Drawing.Size(133, 20)
        Me.dteWrittenOffEnd.TabIndex = 3
        '
        'dteWrittenOffStart
        '
        Me.dteWrittenOffStart.EditValue = Nothing
        Me.dteWrittenOffStart.EnterMoveNextControl = True
        Me.dteWrittenOffStart.Location = New System.Drawing.Point(89, 3)
        Me.dteWrittenOffStart.Name = "dteWrittenOffStart"
        Me.dteWrittenOffStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteWrittenOffStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteWrittenOffStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteWrittenOffStart.Size = New System.Drawing.Size(133, 20)
        Me.dteWrittenOffStart.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(3, 26)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Date &To"
        '
        'LabelControl4
        '
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Date &From"
        '
        'gridcWrittenOff
        '
        Me.gridcWrittenOff.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridcWrittenOff.Location = New System.Drawing.Point(0, 52)
        Me.gridcWrittenOff.MainView = Me.gridvWrittenOff
        Me.gridcWrittenOff.Name = "gridcWrittenOff"
        Me.gridcWrittenOff.Size = New System.Drawing.Size(885, 416)
        Me.gridcWrittenOff.TabIndex = 5
        Me.gridcWrittenOff.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridvWrittenOff, Me.GridView1})
        Me.gridcWrittenOff.Visible = False
        '
        'gridvWrittenOff
        '
        Me.gridvWrittenOff.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand3})
        Me.gridvWrittenOff.GridControl = Me.gridcWrittenOff
        Me.gridvWrittenOff.Name = "gridvWrittenOff"
        Me.gridvWrittenOff.OptionsBehavior.Editable = False
        Me.gridvWrittenOff.OptionsView.ColumnAutoWidth = False
        '
        'GridBand3
        '
        Me.GridBand3.Caption = "gridBand2"
        Me.GridBand3.Name = "GridBand3"
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.gridcWrittenOff
        Me.GridView1.Name = "GridView1"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(822, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        '
        'btnPrintSheet
        '
        Me.btnPrintSheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSheet.Appearance.Options.UseTextOptions = True
        Me.btnPrintSheet.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnPrintSheet.Location = New System.Drawing.Point(741, 508)
        Me.btnPrintSheet.Name = "btnPrintSheet"
        Me.btnPrintSheet.Size = New System.Drawing.Size(75, 39)
        Me.btnPrintSheet.TabIndex = 3
        Me.btnPrintSheet.Text = "F9 Print Sheet"
        '
        'btnPrintLabels
        '
        Me.btnPrintLabels.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrintLabels.Appearance.Options.UseTextOptions = True
        Me.btnPrintLabels.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnPrintLabels.Location = New System.Drawing.Point(660, 508)
        Me.btnPrintLabels.Name = "btnPrintLabels"
        Me.btnPrintLabels.Size = New System.Drawing.Size(75, 39)
        Me.btnPrintLabels.TabIndex = 2
        Me.btnPrintLabels.Text = "F8 Print Labels"
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.Appearance.Options.UseTextOptions = True
        Me.btnSelectAll.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnSelectAll.Location = New System.Drawing.Point(3, 508)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(75, 39)
        Me.btnSelectAll.TabIndex = 1
        Me.btnSelectAll.Text = "F4"
        '
        'Markdowns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnSelectAll)
        Me.Controls.Add(Me.btnPrintLabels)
        Me.Controls.Add(Me.btnPrintSheet)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.tabcMarkdowns)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Markdowns"
        Me.Size = New System.Drawing.Size(900, 550)
        CType(Me.gridcActive, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridvActive, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabcMarkdowns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabcMarkdowns.ResumeLayout(False)
        Me.tabpActive.ResumeLayout(False)
        Me.tabpSold.ResumeLayout(False)
        CType(Me.dteSoldEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteSoldEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteSoldStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteSoldStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridcSold, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridvSold, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpWrittenOff.ResumeLayout(False)
        CType(Me.dteWrittenOffEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteWrittenOffEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteWrittenOffStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteWrittenOffStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridcWrittenOff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridvWrittenOff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridcActive As DevExpress.XtraGrid.GridControl
    Friend WithEvents tabcMarkdowns As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabpActive As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabpSold As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabpWrittenOff As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gridcSold As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gridvActive As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents dteSoldEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteSoldStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gridvSold As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents gridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents dteWrittenOffEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteWrittenOffStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gridcWrittenOff As DevExpress.XtraGrid.GridControl
    Friend WithEvents gridvWrittenOff As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents GridBand3 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lblNote As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrintSheet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrintLabels As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSold As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnWrittenOff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSelectAll As DevExpress.XtraEditors.SimpleButton

End Class
