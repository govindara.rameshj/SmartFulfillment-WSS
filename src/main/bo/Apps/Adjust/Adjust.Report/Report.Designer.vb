<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report))
        Me.gridcSummary = New DevExpress.XtraGrid.GridControl
        Me.gridvSummary = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.tabcAdjusts = New DevExpress.XtraTab.XtraTabControl
        Me.tabpSummary = New DevExpress.XtraTab.XtraTabPage
        Me.btnSelect = New DevExpress.XtraEditors.SimpleButton
        Me.dteEnd = New DevExpress.XtraEditors.DateEdit
        Me.dteStart = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.tabpDetail = New DevExpress.XtraTab.XtraTabPage
        Me.btnFilter = New DevExpress.XtraEditors.SimpleButton
        Me.txtSkuNumber = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.gridcDetail = New DevExpress.XtraGrid.GridControl
        Me.gridvDetail = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.btnPrint = New DevExpress.XtraEditors.SimpleButton
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gridcSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabcAdjusts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabcAdjusts.SuspendLayout()
        Me.tabpSummary.SuspendLayout()
        CType(Me.dteEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpDetail.SuspendLayout()
        CType(Me.txtSkuNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridcDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridvDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridcSummary
        '
        Me.gridcSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridcSummary.Location = New System.Drawing.Point(0, 52)
        Me.gridcSummary.MainView = Me.gridvSummary
        Me.gridcSummary.Name = "gridcSummary"
        Me.gridcSummary.Size = New System.Drawing.Size(985, 466)
        Me.gridcSummary.TabIndex = 5
        Me.gridcSummary.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridvSummary})
        Me.gridcSummary.Visible = False
        '
        'gridvSummary
        '
        Me.gridvSummary.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvSummary.Appearance.HeaderPanel.Options.UseFont = True
        Me.gridvSummary.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.gridvSummary.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridvSummary.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvSummary.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvSummary.AppearancePrint.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvSummary.AppearancePrint.HeaderPanel.Options.UseFont = True
        Me.gridvSummary.AppearancePrint.HeaderPanel.Options.UseTextOptions = True
        Me.gridvSummary.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvSummary.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvSummary.ColumnPanelRowHeight = 35
        Me.gridvSummary.GridControl = Me.gridcSummary
        Me.gridvSummary.Name = "gridvSummary"
        Me.gridvSummary.OptionsBehavior.Editable = False
        Me.gridvSummary.OptionsPrint.UsePrintStyles = True
        Me.gridvSummary.OptionsView.ShowFooter = True
        '
        'tabcAdjusts
        '
        Me.tabcAdjusts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabcAdjusts.Location = New System.Drawing.Point(3, 3)
        Me.tabcAdjusts.Name = "tabcAdjusts"
        Me.tabcAdjusts.SelectedTabPage = Me.tabpSummary
        Me.tabcAdjusts.Size = New System.Drawing.Size(994, 549)
        Me.tabcAdjusts.TabIndex = 0
        Me.tabcAdjusts.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabpSummary, Me.tabpDetail})
        '
        'tabpSummary
        '
        Me.tabpSummary.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpSummary.Controls.Add(Me.btnSelect)
        Me.tabpSummary.Controls.Add(Me.dteEnd)
        Me.tabpSummary.Controls.Add(Me.dteStart)
        Me.tabpSummary.Controls.Add(Me.LabelControl2)
        Me.tabpSummary.Controls.Add(Me.LabelControl1)
        Me.tabpSummary.Controls.Add(Me.gridcSummary)
        Me.tabpSummary.Name = "tabpSummary"
        Me.tabpSummary.Size = New System.Drawing.Size(985, 518)
        Me.tabpSummary.Text = "Summary"
        '
        'btnSelect
        '
        Me.btnSelect.Location = New System.Drawing.Point(245, 3)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(75, 39)
        Me.btnSelect.TabIndex = 4
        Me.btnSelect.Text = "F3 Select"
        '
        'dteEnd
        '
        Me.dteEnd.EditValue = Nothing
        Me.dteEnd.Location = New System.Drawing.Point(89, 26)
        Me.dteEnd.Name = "dteEnd"
        Me.dteEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteEnd.Size = New System.Drawing.Size(133, 20)
        Me.dteEnd.TabIndex = 3
        '
        'dteStart
        '
        Me.dteStart.EditValue = Nothing
        Me.dteStart.Location = New System.Drawing.Point(89, 3)
        Me.dteStart.Name = "dteStart"
        Me.dteStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.dteStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteStart.Size = New System.Drawing.Size(133, 20)
        Me.dteStart.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(3, 26)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Date &To"
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Date &From"
        '
        'tabpDetail
        '
        Me.tabpDetail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tabpDetail.Controls.Add(Me.btnFilter)
        Me.tabpDetail.Controls.Add(Me.txtSkuNumber)
        Me.tabpDetail.Controls.Add(Me.LabelControl3)
        Me.tabpDetail.Controls.Add(Me.gridcDetail)
        Me.tabpDetail.Name = "tabpDetail"
        Me.tabpDetail.Size = New System.Drawing.Size(985, 518)
        Me.tabpDetail.Text = "Detail"
        '
        'btnFilter
        '
        Me.btnFilter.Location = New System.Drawing.Point(245, 3)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(75, 39)
        Me.btnFilter.TabIndex = 9
        Me.btnFilter.Text = "F3 Select"
        '
        'txtSkuNumber
        '
        Me.txtSkuNumber.EnterMoveNextControl = True
        Me.txtSkuNumber.Location = New System.Drawing.Point(89, 3)
        Me.txtSkuNumber.Name = "txtSkuNumber"
        Me.txtSkuNumber.Properties.MaxLength = 6
        Me.txtSkuNumber.Size = New System.Drawing.Size(133, 20)
        Me.txtSkuNumber.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(80, 20)
        Me.LabelControl3.TabIndex = 7
        Me.LabelControl3.Text = "&SKU Number"
        '
        'gridcDetail
        '
        Me.gridcDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridcDetail.Location = New System.Drawing.Point(0, 52)
        Me.gridcDetail.MainView = Me.gridvDetail
        Me.gridcDetail.Name = "gridcDetail"
        Me.gridcDetail.Size = New System.Drawing.Size(985, 466)
        Me.gridcDetail.TabIndex = 6
        Me.gridcDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridvDetail})
        '
        'gridvDetail
        '
        Me.gridvDetail.Appearance.BandPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvDetail.Appearance.BandPanel.Options.UseFont = True
        Me.gridvDetail.Appearance.BandPanel.Options.UseTextOptions = True
        Me.gridvDetail.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridvDetail.Appearance.BandPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvDetail.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvDetail.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvDetail.Appearance.HeaderPanel.Options.UseFont = True
        Me.gridvDetail.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.gridvDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridvDetail.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvDetail.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvDetail.AppearancePrint.BandPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvDetail.AppearancePrint.BandPanel.Options.UseFont = True
        Me.gridvDetail.AppearancePrint.BandPanel.Options.UseTextOptions = True
        Me.gridvDetail.AppearancePrint.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridvDetail.AppearancePrint.BandPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvDetail.AppearancePrint.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvDetail.AppearancePrint.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gridvDetail.AppearancePrint.HeaderPanel.Options.UseFont = True
        Me.gridvDetail.AppearancePrint.HeaderPanel.Options.UseTextOptions = True
        Me.gridvDetail.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridvDetail.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.gridvDetail.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gridvDetail.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.gridvDetail.ColumnPanelRowHeight = 35
        Me.gridvDetail.GridControl = Me.gridcDetail
        Me.gridvDetail.Name = "gridvDetail"
        Me.gridvDetail.OptionsBehavior.Editable = False
        Me.gridvDetail.OptionsPrint.UsePrintStyles = True
        Me.gridvDetail.OptionsView.ColumnAutoWidth = False
        Me.gridvDetail.OptionsView.ShowFooter = True
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Name = "GridBand1"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(841, 558)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 39)
        Me.btnPrint.TabIndex = 1
        Me.btnPrint.Text = "F9 Print"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(922, 558)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "F12 Exit"
        '
        'Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.tabcAdjusts)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Report"
        Me.Size = New System.Drawing.Size(1000, 600)
        CType(Me.gridcSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabcAdjusts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabcAdjusts.ResumeLayout(False)
        Me.tabpSummary.ResumeLayout(False)
        CType(Me.dteEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpDetail.ResumeLayout(False)
        CType(Me.txtSkuNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridcDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridvDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridcSummary As DevExpress.XtraGrid.GridControl
    Friend WithEvents gridvSummary As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tabcAdjusts As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabpDetail As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabpSummary As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gridcDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents gridvDetail As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents dteEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dteStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSelect As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtSkuNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnFilter As DevExpress.XtraEditors.SimpleButton

End Class
