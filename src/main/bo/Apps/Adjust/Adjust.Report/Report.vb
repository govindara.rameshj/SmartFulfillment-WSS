Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.BandedGrid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class Report
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _adjust As New BOStock.cStockAdjust(_Oasys3DB)
    Private _storeIdName As String = String.Empty

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        dteStart.EditValue = Now
        dteEnd.EditValue = Now
        dteStart.Properties.MaxValue = Now
        dteEnd.Properties.MaxValue = Now
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3
                If btnSelect.Visible Then btnSelect.PerformClick()
                If btnFilter.Visible Then btnFilter.PerformClick()
            Case Keys.F9 : If btnPrint.Enabled Then btnPrint.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Using retOptions As New BOSystem.cRetailOptions(_Oasys3DB)
            retOptions.LoadStoreAndAccountability()
            _storeIdName = retOptions.StoreIdName
        End Using

        btnSelect.PerformClick()

    End Sub



    Private Sub dteStart_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteStart.DateTimeChanged
        dteEnd.Properties.MinValue = dteStart.DateTime
    End Sub

    Private Sub dteEnd_DateTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteEnd.DateTimeChanged
        dteStart.Properties.MaxValue = dteEnd.DateTime
    End Sub



    Private Sub gridvSummary_LoadData()

        'load summary and assign datasource
        _adjust.AdjustmentsCodeSummaryLoad(dteStart.DateTime, dteEnd.DateTime)
        gridcSummary.DataSource = _adjust.TableCodeSummary

        'set up columns
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In gridvSummary.Columns
            Select Case col.FieldName
                Case _adjust.Column(cStockAdjust.ColumnNames.CodeNumber) : col.Caption = "Code"
                Case _adjust.Column(cStockAdjust.ColumnNames.CodeDescription) : col.Caption = "Description"
                Case _adjust.Column(cStockAdjust.ColumnNames.QtyAdjust)
                    col.Caption = "Adjust Qty"
                    col.SummaryItem.FieldName = col.FieldName
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                Case _adjust.Column(cStockAdjust.ColumnNames.QtyMarkdown)
                    col.Caption = "Adjust MD"
                    col.SummaryItem.FieldName = col.FieldName
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                Case _adjust.Column(cStockAdjust.ColumnNames.QtyWrittenOff)
                    col.Caption = "Adjust WO"
                    col.SummaryItem.FieldName = col.FieldName
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                Case _adjust.Column(cStockAdjust.ColumnNames.Value)
                    col.Caption = "Value"
                    col.SummaryItem.FieldName = col.FieldName
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End Select
        Next

        gridcSummary.Visible = True
        gridvSummary.BestFitColumns()

    End Sub

    Private Sub gridvDetail_LoadData()

        'set up datasource
        _adjust.AdjustmentsCodeDetailLoad(dteStart.DateTime, dteEnd.DateTime)
        gridcDetail.DataSource = _adjust.TableCodeDetail

        'creating the bands layout
        gridvDetail.Bands.Clear()
        Dim bandItem As GridBand = gridvDetail.Bands.AddBand("Adjustment Item")
        Dim bandAdjustments As GridBand = gridvDetail.Bands.AddBand("Adjustments")
        Dim bandcode As GridBand = gridvDetail.Bands.AddBand("Code")
        Dim bandHierarchy As GridBand = gridvDetail.Bands.AddBand("Hierarchy")
        bandcode.Visible = False
        bandHierarchy.Visible = False

        'set up columns
        For Each col As DevExpress.XtraGrid.Columns.GridColumn In gridvDetail.Columns
            Select Case col.FieldName
                Case _adjust.Column(cStockAdjust.ColumnNames.CodeNumber)
                    col.Caption = "Code"
                    col.BestFit()
                    bandcode.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Column(cStockAdjust.ColumnNames.CodeDescription)
                    col.Caption = "Description"
                    col.BestFit()
                    bandcode.Columns.Add(CType(col, BandedGridColumn))

                Case _adjust.Column(cStockAdjust.ColumnNames.CategoryDescription)
                    col.Caption = "Category"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Column(cStockAdjust.ColumnNames.GroupDescription)
                    col.Caption = "Group"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Column(cStockAdjust.ColumnNames.SubgroupDescription)
                    col.Caption = "Subgroup"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Column(cStockAdjust.ColumnNames.StyleDescription)
                    col.Caption = "Style"
                    col.BestFit()
                    bandHierarchy.Columns.Add(CType(col, BandedGridColumn))

                Case _adjust.DateCreated.ColumnName
                    col.Caption = "Created"
                    col.MinWidth = 80
                    col.BestFit()
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.SkuNumber.ColumnName
                    col.Caption = "SKU"
                    col.MinWidth = 50
                    col.Width = 50
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Column(cStockAdjust.ColumnNames.SkuDescription)
                    col.Caption = "Description"
                    col.BestFit()
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.EmployeeID.ColumnName
                    col.Caption = "Employee ID"
                    col.MinWidth = 70
                    col.Width = 70
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.Price.ColumnName
                    col.Caption = "Price"
                    col.MinWidth = 65
                    col.Width = 65
                    bandItem.Columns.Add(CType(col, BandedGridColumn))
                Case _adjust.StartStock.ColumnName
                    col.Caption = "Start Stock"
                    col.MinWidth = 65
                    col.Width = 65
                    bandItem.Columns.Add(CType(col, BandedGridColumn))

                Case _adjust.Column(cStockAdjust.ColumnNames.QtyAdjust)
                    col.Caption = "Adjusts Qty"
                    col.MinWidth = 65
                    col.Width = 65
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    bandAdjustments.Columns.Add(CType(col, BandedGridColumn))

                    Dim item As New GridGroupSummaryItem
                    item.FieldName = col.FieldName
                    item.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    item.ShowInGroupColumnFooter = col
                    gridvDetail.GroupSummary.Add(item)

                Case _adjust.Column(cStockAdjust.ColumnNames.QtyMarkdown)
                    col.Caption = "Adjusts MD"
                    col.MinWidth = 65
                    col.Width = 65
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    bandAdjustments.Columns.Add(CType(col, BandedGridColumn))

                    Dim item As New GridGroupSummaryItem
                    item.FieldName = col.FieldName
                    item.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    item.ShowInGroupColumnFooter = col
                    gridvDetail.GroupSummary.Add(item)

                Case _adjust.Column(cStockAdjust.ColumnNames.QtyWrittenOff)
                    col.Caption = "Adjusts WO"
                    col.MinWidth = 65
                    col.Width = 65
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    bandAdjustments.Columns.Add(CType(col, BandedGridColumn))

                    Dim item As New GridGroupSummaryItem
                    item.FieldName = col.FieldName
                    item.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    item.ShowInGroupColumnFooter = col
                    gridvDetail.GroupSummary.Add(item)

                Case _adjust.Column(cStockAdjust.ColumnNames.Value)
                    col.Caption = "Value"
                    col.MinWidth = 65
                    col.Width = 65
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    bandAdjustments.Columns.Add(CType(col, BandedGridColumn))

                    Dim item As New GridGroupSummaryItem
                    item.FieldName = col.FieldName
                    item.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    item.ShowInGroupColumnFooter = col
                    gridvDetail.GroupSummary.Add(item)

                Case _adjust.Comment.ColumnName
                    col.Caption = "Comment"
                    col.MinWidth = 70
                    col.BestFit()
                    bandAdjustments.Columns.Add(CType(col, BandedGridColumn))
            End Select
        Next

        gridvDetail.BeginSort()
        gridvDetail.ClearGrouping()
        gridvDetail.Columns(_adjust.Column(cStockAdjust.ColumnNames.CodeNumber)).GroupIndex = 0
        gridvDetail.EndSort()

    End Sub


    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw store number and name
        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _storeIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:MM/dd/yyyy HH:mm}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportTitle_CreateReportDetailArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)

        'draw report title
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Tahoma", 14, FontStyle.Bold)

        Select Case tabcAdjusts.SelectedTabPage.Name
            Case tabpSummary.Name
                Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
                e.Graph.DrawString("Stock Adjustments Summary", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

                rec = New RectangleF(0, 30, e.Graph.ClientPageSize.Width, 20)
                e.Graph.Font = New Font("Tahoma", 10, FontStyle.Bold)
                e.Graph.DrawString("Data for: " & dteStart.DateTime.ToShortDateString & " - " & dteEnd.DateTime.ToShortDateString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

            Case tabpDetail.Name
                Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30)
                e.Graph.DrawString("Stock Adjustments Details", Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

                rec = New RectangleF(0, 30, e.Graph.ClientPageSize.Width, 20)
                e.Graph.Font = New Font("Tahoma", 10, FontStyle.Bold)
                e.Graph.DrawString("Data for: " & dteStart.DateTime.ToShortDateString & " - " & dteEnd.DateTime.ToShortDateString, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)
        End Select

    End Sub


    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click

        If txtSkuNumber.EditValue IsNot Nothing Then
            Dim val As Object = txtSkuNumber.EditValue.ToString.Trim
            Dim columnCountry As GridColumn = gridvDetail.Columns(_adjust.SkuNumber.ColumnName)
            columnCountry.FilterInfo = New ColumnFilterInfo(columnCountry, val)
        End If

    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click

        Try
            Cursor = Cursors.WaitCursor
            gridvSummary_LoadData()
            gridvDetail_LoadData()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        DisplayStatus()

        'set up supplier copy report
        Dim report As New CompositeLink(New PrintingSystem)
        report.Margins.Bottom = 70
        report.Margins.Top = 50
        report.Margins.Left = 50
        report.Margins.Right = 50
        report.Landscape = True
        AddHandler report.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler report.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        'get grid to be printed
        Dim grid As DevExpress.XtraGrid.GridControl = Nothing
        Select Case tabcAdjusts.SelectedTabPage.Name
            Case tabpSummary.Name : grid = gridcSummary
            Case tabpDetail.Name
                grid = gridcDetail
                report.Landscape = True
        End Select

        Dim reportTitle As New Link
        AddHandler reportTitle.CreateDetailArea, AddressOf ReportTitle_CreateReportDetailArea

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = grid

        'populate the collection of links in the composite link in order and show preview
        report.Links.Add(reportTitle)
        report.Links.Add(reportGrid)
        report.ShowPreviewDialog()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub



End Class
