﻿Public Class AdjustRollMarkdowns

    Private Sub Me_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        For Each s As String In My.Application.CommandLineArgs
            If s = "AUTO" Then
                Main()
                End
            End If
        Next
    End Sub

    Private Sub Main()

        Try
            Cursor = Cursors.WaitCursor

            Using oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
                Using markdownStock As New BOStock.cMarkdownStock(oasys3DB)

                    lblStatus.Text = "Retrieving all active markdowns"
                    markdownStock.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, markdownStock.SoldTill, "00")
                    markdownStock.Markdowns = markdownStock.LoadMatches

                    lblStatus.Text = "Performing markdown rollover"
                    Dim rolloverDay As Integer = 0
                    Using param As New BOSystem.cParameter(oasys3DB)
                        rolloverDay = param.GetParameterInteger(4120)
                    End Using

                    lblStatus.Text = "Performing rollover"
                    Trace.WriteLine("Performing rollover of markdowns", "AdjustService")

                    If rolloverDay = -1 Then
                        For Each markdown As BOStock.cMarkdownStock In markdownStock.Markdowns
                            If markdown.DateLastRollover IsNot Nothing Then
                                If markdown.DateLastRollover.Value.Date = Now.Date Then Continue For
                            End If
                            If markdown.DateCreated.Value.DayOfWeek = Now.DayOfWeek Then markdown.UpdateRollover()
                        Next
                    Else

                        If Now.DayOfWeek = rolloverDay Then
                            For Each markdown As BOStock.cMarkdownStock In markdownStock.Markdowns
                                If markdown.DateLastRollover IsNot Nothing Then
                                    If markdown.DateLastRollover.Value.Date = Now.Date Then Continue For
                                End If
                                markdown.UpdateRollover()
                            Next
                        End If
                    End If

                End Using
            End Using

            lblStatus.Text = "Process Complete"
            Trace.WriteLine("Process Complete", "AdjustService")
            Console.WriteLine("Complete")

        Catch ex As Exception
            lblStatus.Text = "Error Encountered: " & ex.ToString
            Trace.WriteLine("Error Encountered: " & ex.ToString, "AdjustService")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Close()
    End Sub

    Private Sub PerformButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerformButton.Click
        Main()
    End Sub

End Class
