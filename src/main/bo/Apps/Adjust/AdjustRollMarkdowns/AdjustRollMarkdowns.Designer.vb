﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdjustRollMarkdowns
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblStatus = New System.Windows.Forms.Label
        Me.ExitButton = New System.Windows.Forms.Button
        Me.PerformButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.Location = New System.Drawing.Point(12, 9)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(359, 44)
        Me.lblStatus.TabIndex = 0
        '
        'ExitButton
        '
        Me.ExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ExitButton.Location = New System.Drawing.Point(12, 56)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(73, 39)
        Me.ExitButton.TabIndex = 1
        Me.ExitButton.Text = "F10 Exit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'PerformButton
        '
        Me.PerformButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PerformButton.Location = New System.Drawing.Point(298, 56)
        Me.PerformButton.Name = "PerformButton"
        Me.PerformButton.Size = New System.Drawing.Size(73, 39)
        Me.PerformButton.TabIndex = 2
        Me.PerformButton.Text = "F5 Rollover"
        Me.PerformButton.UseVisualStyleBackColor = True
        '
        'AdjustRollMarkdowns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(383, 107)
        Me.Controls.Add(Me.PerformButton)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.lblStatus)
        Me.Name = "AdjustRollMarkdowns"
        Me.ShowIcon = False
        Me.Text = "Rollover Markdowns"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents PerformButton As System.Windows.Forms.Button

End Class
