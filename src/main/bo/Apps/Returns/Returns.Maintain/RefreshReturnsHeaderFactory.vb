﻿Public Class RefreshReturnsHeaderFactory

    Inherits RequirementSwitchFactory(Of IRefreshReturnsHeader)

    Friend _requirementSwitchParameterId As Integer = -824

    Public Overrides Function ImplementationA() As IRefreshReturnsHeader
        Return New RefreshReturnsHeader
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet

        If RequirementRepository IsNot Nothing Then
            ImplementationA_IsActive = RequirementRepository.IsSwitchPresentAndEnabled(_requirementSwitchParameterId)
        End If
    End Function

    Public Overrides Function ImplementationB() As IRefreshReturnsHeader

        Return New RefreshReturnsHeaderOldWay
    End Function

End Class
