﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Maintain
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Maintain))
        Me.grpReturn = New System.Windows.Forms.GroupBox()
        Me.lstPolicy = New System.Windows.Forms.ListBox()
        Me.xgdHeader = New DevExpress.XtraGrid.GridControl()
        Me.xgvHeader = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.xgdDetail = New DevExpress.XtraGrid.GridControl()
        Me.xgvDetail = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.riMaskInteger = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.btnAddItem = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSaveChanges = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReset = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDeleteItem = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCommand = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPrintSummary = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPrintAll = New DevExpress.XtraEditors.SimpleButton()
        Me.grpReturn.SuspendLayout()
        CType(Me.xgdHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgvHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgdDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgvDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riMaskInteger, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpReturn
        '
        Me.grpReturn.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpReturn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpReturn.Controls.Add(Me.lstPolicy)
        Me.grpReturn.Controls.Add(Me.xgdHeader)
        Me.grpReturn.Controls.Add(Me.xgdDetail)
        Me.grpReturn.Controls.Add(Me.btnAddItem)
        Me.grpReturn.Controls.Add(Me.btnSaveChanges)
        Me.grpReturn.Controls.Add(Me.btnReset)
        Me.grpReturn.Controls.Add(Me.btnDeleteItem)
        Me.grpReturn.Location = New System.Drawing.Point(3, 3)
        Me.grpReturn.Name = "grpReturn"
        Me.grpReturn.Size = New System.Drawing.Size(894, 599)
        Me.grpReturn.TabIndex = 0
        Me.grpReturn.TabStop = False
        Me.grpReturn.Text = "Select Return to Print"
        '
        'lstPolicy
        '
        Me.lstPolicy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstPolicy.BackColor = System.Drawing.SystemColors.Control
        Me.lstPolicy.FormattingEnabled = True
        Me.lstPolicy.Location = New System.Drawing.Point(6, 469)
        Me.lstPolicy.Name = "lstPolicy"
        Me.lstPolicy.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lstPolicy.Size = New System.Drawing.Size(882, 121)
        Me.lstPolicy.TabIndex = 41
        Me.lstPolicy.TabStop = False
        '
        'xgdHeader
        '
        Me.xgdHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgdHeader.Location = New System.Drawing.Point(6, 19)
        Me.xgdHeader.MainView = Me.xgvHeader
        Me.xgdHeader.Name = "xgdHeader"
        Me.xgdHeader.Size = New System.Drawing.Size(882, 264)
        Me.xgdHeader.TabIndex = 42
        Me.xgdHeader.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xgvHeader})
        '
        'xgvHeader
        '
        Me.xgvHeader.GridControl = Me.xgdHeader
        Me.xgvHeader.Name = "xgvHeader"
        Me.xgvHeader.OptionsBehavior.Editable = False
        Me.xgvHeader.OptionsView.ShowFooter = True
        '
        'xgdDetail
        '
        Me.xgdDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgdDetail.Location = New System.Drawing.Point(6, 289)
        Me.xgdDetail.MainView = Me.xgvDetail
        Me.xgdDetail.Name = "xgdDetail"
        Me.xgdDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riMaskInteger})
        Me.xgdDetail.Size = New System.Drawing.Size(882, 174)
        Me.xgdDetail.TabIndex = 4
        Me.xgdDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xgvDetail})
        '
        'xgvDetail
        '
        Me.xgvDetail.GridControl = Me.xgdDetail
        Me.xgvDetail.Name = "xgvDetail"
        Me.xgvDetail.OptionsBehavior.Editable = False
        Me.xgvDetail.OptionsView.ShowFooter = True
        Me.xgvDetail.OptionsView.ShowGroupPanel = False
        '
        'riMaskInteger
        '
        Me.riMaskInteger.AutoHeight = False
        Me.riMaskInteger.Name = "riMaskInteger"
        '
        'btnAddItem
        '
        Me.btnAddItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddItem.Enabled = False
        Me.btnAddItem.Location = New System.Drawing.Point(6, 424)
        Me.btnAddItem.Name = "btnAddItem"
        Me.btnAddItem.Size = New System.Drawing.Size(97, 39)
        Me.btnAddItem.TabIndex = 43
        Me.btnAddItem.Text = "Add Item"
        '
        'btnSaveChanges
        '
        Me.btnSaveChanges.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSaveChanges.Enabled = False
        Me.btnSaveChanges.Location = New System.Drawing.Point(233, 424)
        Me.btnSaveChanges.Name = "btnSaveChanges"
        Me.btnSaveChanges.Size = New System.Drawing.Size(97, 39)
        Me.btnSaveChanges.TabIndex = 44
        Me.btnSaveChanges.Text = "Save"
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Enabled = False
        Me.btnReset.Location = New System.Drawing.Point(336, 424)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(97, 39)
        Me.btnReset.TabIndex = 45
        Me.btnReset.Text = "Reset"
        '
        'btnDeleteItem
        '
        Me.btnDeleteItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteItem.Enabled = False
        Me.btnDeleteItem.Location = New System.Drawing.Point(109, 424)
        Me.btnDeleteItem.Name = "btnDeleteItem"
        Me.btnDeleteItem.Size = New System.Drawing.Size(97, 39)
        Me.btnDeleteItem.TabIndex = 46
        Me.btnDeleteItem.Text = "Delete Item"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(822, 608)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "F12 Exit"
        '
        'btnCommand
        '
        Me.btnCommand.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCommand.Location = New System.Drawing.Point(6, 608)
        Me.btnCommand.Name = "btnCommand"
        Me.btnCommand.Size = New System.Drawing.Size(97, 39)
        Me.btnCommand.TabIndex = 5
        '
        'btnPrintSummary
        '
        Me.btnPrintSummary.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSummary.Location = New System.Drawing.Point(109, 608)
        Me.btnPrintSummary.Name = "btnPrintSummary"
        Me.btnPrintSummary.Size = New System.Drawing.Size(104, 39)
        Me.btnPrintSummary.TabIndex = 6
        Me.btnPrintSummary.Text = "F6 Print Summary"
        Me.btnPrintSummary.Visible = False
        '
        'btnPrintAll
        '
        Me.btnPrintAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintAll.Location = New System.Drawing.Point(219, 608)
        Me.btnPrintAll.Name = "btnPrintAll"
        Me.btnPrintAll.Size = New System.Drawing.Size(104, 39)
        Me.btnPrintAll.TabIndex = 7
        Me.btnPrintAll.Text = "F7 Print All"
        Me.btnPrintAll.Visible = False
        '
        'Maintain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnPrintAll)
        Me.Controls.Add(Me.btnPrintSummary)
        Me.Controls.Add(Me.btnCommand)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.grpReturn)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Maintain"
        Me.Size = New System.Drawing.Size(900, 650)
        Me.grpReturn.ResumeLayout(False)
        CType(Me.xgdHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgvHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgdDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgvDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riMaskInteger, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpReturn As System.Windows.Forms.GroupBox
    Friend WithEvents lstPolicy As System.Windows.Forms.ListBox
    Friend WithEvents xgdDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents xgvDetail As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents xgdHeader As DevExpress.XtraGrid.GridControl
    Friend WithEvents xgvHeader As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCommand As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrintSummary As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrintAll As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents riMaskInteger As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnAddItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSaveChanges As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDeleteItem As DevExpress.XtraEditors.SimpleButton

End Class
