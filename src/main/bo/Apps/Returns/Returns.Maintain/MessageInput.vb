﻿Public Class MessageInput
    Private _message As String
    Public ReadOnly Property Message() As String
        Get
            Return _message
        End Get
    End Property


    Public Sub New(ByVal messageLabel As String)
        InitializeComponent()
        lblMessage.Text = messageLabel
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        If txtMessage.Text.Length = 0 Then
            lblWarning.Visible = True
        Else
            _message = txtMessage.Text.Trim
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

End Class